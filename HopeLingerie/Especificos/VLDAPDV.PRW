#include 'protheus.ch'
#include 'parmtype.ch'
#include 'topconn.ch'

/*/{Protheus.doc} VLDAPDV 
Relatorio de vendas PDV
@author Weskley Silva
@since 02/10/2019
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function VLDAPDV()

Private oReport
Private cPergCont	:= 'VLDAPDV' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 02 de Outubro de 2019
@version 1.0
/*/
//_____________________________________________________________________________
	
Static Function ReportDef()

	Local oReport
	Local oSection1

	oReport := TReport():New( 'PDV', 'RELAT�RIO DE VENDAS POR CART�O', , {|oReport| ReportPrint( oReport ), 'RELAT�RIO DE VENDAS POR CART�O' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELAT�RIO DE VENDAS POR CART�O', {'PDV','SE1'})
			
	TRCell():New( oSection1, 'ADMINISTRADORA'	    ,'PDV', 		'ADMINISTRADORA',					"@!"                                    ,50)
	TRCell():New( oSection1, 'JAN'					,'PDV', 		'JAN',       						"@E 9999,999,999.99"		            ,30)
	TRCell():New( oSection1, 'FEV'					,'PDV', 		'FEV',       						"@E 9999,999,999.99"		            ,30)
	TRCell():New( oSection1, 'MAR'					,'PDV', 		'MAR',       						"@E 9999,999,999.99"		            ,30)
	TRCell():New( oSection1, 'ABR'					,'PDV', 		'ABR',       						"@E 9999,999,999.99"		            ,30)
	TRCell():New( oSection1, 'MAI'					,'PDV', 		'MAI',       						"@E 9999,999,999.99"		            ,30)
	TRCell():New( oSection1, 'JUN'					,'PDV', 		'JUN',       						"@E 9999,999,999.99"		            ,30)
	TRCell():New( oSection1, 'JUL'					,'PDV', 		'JUL',       						"@E 9999,999,999.99"		            ,30)
	TRCell():New( oSection1, 'AGO'					,'PDV', 		'AGO',       						"@E 9999,999,999.99"		            ,30)
	TRCell():New( oSection1, 'SET'					,'PDV', 		'SET',       						"@E 9999,999,999.99"		            ,30)
	TRCell():New( oSection1, 'OUT'					,'PDV', 		'OUT',       						"@E 9999,999,999.99"		            ,30)
	TRCell():New( oSection1, 'NOV'					,'PDV', 		'NOV',       						"@E 9999,999,999.99"		            ,30)
    TRCell():New( oSection1, 'DEZ'					,'PDV', 		'DEZ',       						"@E 9999,999,999.99"		            ,30)
	
Return( oReport )
	
//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 02 de Outubro de 2019
@version 1.0
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("PDV") > 0
		PDV->(dbCloseArea())
	Endif
       
    cQuery := " SELECT * " 
    cQuery += "      FROM ( "
    cQuery += "         SELECT coalesce(E1_NOMCLI , 'Z_TOTAL MES =>') AS ADMINISTRADORA , SUM(E1_VALOR) AS VALOR,  "
    cQuery += "          CASE WHEN MONTH(E1_VENCREA) = 01 THEN 'JAN'  " 
	cQuery += "  	     WHEN MONTH(E1_VENCREA) = 02 THEN 'FEV'  "
	cQuery += "			 WHEN MONTH(E1_VENCREA) = 03 THEN 'MAR'  "
	cQuery += " 	     WHEN MONTH(E1_VENCREA) = 04 THEN 'ABR'  "
	cQuery += "	         WHEN MONTH(E1_VENCREA) = 05 THEN 'MAI'  "
	cQuery += "			 WHEN MONTH(E1_VENCREA) = 06 THEN 'JUN'  "
	cQuery += "			 WHEN MONTH(E1_VENCREA) = 07 THEN 'JUL'  "
	cQuery += "			 WHEN MONTH(E1_VENCREA) = 08 THEN 'AGO'  "
	cQuery += "			 WHEN MONTH(E1_VENCREA) = 09 THEN 'SET'  "
	cQuery += "			 WHEN MONTH(E1_VENCREA) = 10 THEN 'OUT'  "
	cQuery += "			 WHEN MONTH(E1_VENCREA) = 11 THEN 'NOV'  "
	cQuery += "			 WHEN MONTH(E1_VENCREA) = 12 THEN 'DEZ'  "
	cQuery += "			 END AS MES"
    cQuery += " FROM "+RetSqlName("SE1")+" " 
    cQuery += " WHERE D_E_L_E_T_ = '' AND E1_TIPO IN('CC','CD') "
    cQuery += " AND E1_VENCREA BETWEEN '"+ DTOS(MV_PAR01) +"' AND '"+ DTOS(MV_PAR02) + "' " 
    cQuery += " GROUP BY grouping sets ((E1_NOMCLI,MONTH(E1_VENCREA)) , (MONTH(E1_VENCREA))) "
    cQuery += " ) P "
    cQuery += " PIVOT (SUM(VALOR) FOR P.MES IN ([JAN],[FEV],[MAR],[ABR],[MAI],[JUN],[JUL],[AGO],[SET],[OUT],[NOV],[DEZ])) L "
    cQuery += " ORDER BY 1 "
   

	TCQUERY cQuery NEW ALIAS PDV
	
	While PDV->(!EOF())
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("ADMINISTRADORA"):SetValue(PDV->ADMINISTRADORA)
		oSection1:Cell("ADMINISTRADORA"):SetAlign("LEFT")
		
		oSection1:Cell("JAN"):SetValue(PDV->JAN)
		oSection1:Cell("JAN"):SetAlign("LEFT")		

		oSection1:Cell("FEV"):SetValue(PDV->FEV)
		oSection1:Cell("FEV"):SetAlign("LEFT")		
		
		oSection1:Cell("MAR"):SetValue(PDV->MAR)
		oSection1:Cell("MAR"):SetAlign("LEFT")
		
		oSection1:Cell("ABR"):SetValue(PDV->ABR)
		oSection1:Cell("ABR"):SetAlign("LEFT")
		
		oSection1:Cell("MAI"):SetValue(PDV->MAI)
		oSection1:Cell("MAI"):SetAlign("LEFT")
				
		oSection1:Cell("JUN"):SetValue(PDV->JUN)
		oSection1:Cell("JUN"):SetAlign("LEFT")
		
		oSection1:Cell("JUL"):SetValue(PDV->JUL)
		oSection1:Cell("JUL"):SetAlign("LEFT")
		
		oSection1:Cell("AGO"):SetValue(PDV->AGO)
		oSection1:Cell("AGO"):SetAlign("LEFT")
				
		oSection1:Cell("SET"):SetValue(PDV->SET)
		oSection1:Cell("SET"):SetAlign("LEFT")
		
		oSection1:Cell("OUT"):SetValue(PDV->OUT)
		oSection1:Cell("OUT"):SetAlign("LEFT")
				
		oSection1:Cell("NOV"):SetValue(PDV->NOV)
		oSection1:Cell("NOV"):SetAlign("LEFT")

        oSection1:Cell("DEZ"):SetValue(PDV->DEZ)
		oSection1:Cell("DEZ"):SetAlign("LEFT")
										
		oSection1:PrintLine()
		
		PDV->(DBSKIP()) 
	enddo
	PDV->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 02 de Outubro de 2019
@version 1.0
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Data de ?"		        		,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Data ate ?"                     ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	
return