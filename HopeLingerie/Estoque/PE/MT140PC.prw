#include 'protheus.ch'
#include 'parmtype.ch'

user function MT140PC()

Local ExpL1 := PARAMIXB[1] 

IF FUNNAME() == "RPC" .OR. FUNNAME() == "HCOMP001"
	
	ExpL1 := .F.
	
ENDIF

Return ExpL1