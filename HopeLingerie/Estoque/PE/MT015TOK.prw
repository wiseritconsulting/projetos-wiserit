#include 'protheus.ch'
#include 'parmtype.ch'

user function MT015TOK()

Local cQuery := " "
Local lRet := .T.
Local cCodPro := M->BE_CODPRO

cQuery := " SELECT BE_CODPRO FROM "+RetSqlName("SBE")+" WHERE BE_CODPRO = '"+Alltrim(cCodPro)+"' AND D_E_L_E_T_ = '' "


dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP1",.T.,.T.)
DbSelectArea("TMP1")

IF !EMPTY(TMP1->BE_CODPRO)
	
	MsgAlert("Produto j� possui endere�o!","HOPE")
	lRet := .F.
	
ELSE
	lRet := .T.
	
ENDIF

TMP1->(DbCloseArea())
	
return lRet 