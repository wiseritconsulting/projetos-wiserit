<<<<<<< Updated upstream
//Bibliotecas
=======
>>>>>>> Stashed changes
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio Transfer�ncia Pendente.
Rotina para analisar as transf�rencia pendentes do Abastecimento;

@author Ronaldo Pereira
@since 27 de Agosto de 2019
@version V.01
/*/
//_____________________________________________________________________________      

<<<<<<< Updated upstream
User Function HPREL056()
=======
User Function HPREL057()
>>>>>>> Stashed changes

	Local aRet := {}
	Local aPerg := {}

	aAdd(aPerg,{1,"Data Transf. de: "	,dDataBase,PesqPict("SZM", "ZM_DTTRANS"),'.T.'	,""		,'.T.'	,50	,.F.})
	aAdd(aPerg,{1,"Data Transf. at�: "	,dDataBase,PesqPict("SZM", "ZM_DTTRANS"),'.T.'	,""		,'.T.'	,50	,.F.})
	aAdd(aPerg,{1,"Lote DAP:" 			,Space(06),""   ,""		,""	    ,""		,0	,.F.})
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| Geradados(aRet)}, "Exportando dados")		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    	
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'ABASTECIMENTO'+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Base"
    Local cTable1    	:= "TRANSFERENCIAS PENDENTES" 


    //Dados tabela 1          
    cQry := " SELECT CONVERT(CHAR, CAST(ZM_DATA AS SMALLDATETIME), 103) AS DATA_DAP,
    cQry += "        ZM_FILIAL AS FILIAL,
    cQry += "        ZM_LOTDAP AS LOTE_DAP,
    cQry += "        ZM_PRODUTO AS PRODUTO,
    cQry += "        ZM_LOTECTL AS LOTE,
    cQry += "        ZM_NUMLOTE AS SUBLOTE,
    cQry += "        ZM_QUANT AS QTDE,
    cQry += "        ZM_QTDTRAN AS QTDE_TRASNF,
    cQry += "        ZM_QTDCANC AS QTDE_CANCELADA,
    cQry += "        ZM_AMZORIG AS ARMAZEM_ORIGEM,
    cQry += "        ZM_ENDORIG AS ENDERECO_ORIGEM,
    cQry += "        ZM_AMZDEST AS ARMAZEM_DESTINO,
    cQry += "        ZM_ENDDEST AS ENDERECO_DESTINO,
    cQry += "        CASE
    cQry += "            WHEN ZM_DTTRANS <> '' THEN CONVERT(CHAR, CAST(ZM_DTTRANS AS SMALLDATETIME), 103)
    cQry += "            ELSE ''
    cQry += "        END DATA_TRASNF,
    cQry += "        ZM_HRTRANS AS HORA_TRANSF,
    cQry += "        ZM_USENAME AS USUARIO
    cQry += " FROM SZM010 AS ZM WITH (NOLOCK)
    cQry += " WHERE D_E_L_E_T_ = '' AND ZM_DTTRANS <> ''
    cQry += "   AND ZM_DTTRANS BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"' "

    If !Empty(aRet[3])
    	cQry += "  AND ZM_LOTDAP = '"+Alltrim(aRet[3])+"' "
    EndIf

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DATA_DAP",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"LOTE_DAP",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PRODUTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"LOTE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SUBLOTE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE_TRASNF",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE_CANCELADA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ARMAZEM_ORIGEM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ENDERECO_ORIGEM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ARMAZEM_DESTINO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ENDERECO_DESTINO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DATA_TRASNF",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"HORA_TRANSF",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"USUARIO",1)
        
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;                                             
                                               DATA_DAP,;
                                               FILIAL,;
                                               LOTE_DAP,;
                                               PRODUTO,;
                                               LOTE,;
                                               SUBLOTE,;
                                               QTDE,;
                                               QTDE_TRASNF,;
                                               QTDE_CANCELADA,;
                                               ARMAZEM_ORIGEM,;
                                               ENDERECO_ORIGEM,;
                                               ARMAZEM_DESTINO,;
                                               ENDERECO_DESTINO,; 
                                               DATA_TRASNF,; 
                                               HORA_TRANSF,;                                             
                                               USUARIO;                                               
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return