//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de Estoque Anal�tico
Rotina para gerar as informa��es de Estoque;

@author Ronaldo Pereira
@since 05 de Fevereiro de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL046()

	Local aRet := {}
	Local aPerg := {}

	aAdd(aPerg,{1,"Filial de: " 		,Space(04),""   ,""		,""	    ,""		,0	,.F.})
	aAdd(aPerg,{1,"Filial at�: "		,Space(04),""	,""		,""		,""		,0	,.F.})
	aAdd(aPerg,{1,"Aramz�m de: "		,Space(02),""	,""		,""		,""		,0	,.F.})
	aAdd(aPerg,{1,"Armaz�m at�: "		,Space(02),""	,""		,""		,""		,0	,.F.})
	aAdd(aPerg,{1,"Tipo Produto: "		,Space(02),""	,""		,""		,""		,0	,.F.})
	aAdd(aPerg,{1,"Grupo Produto de: "	,Space(04),""	,""		,""		,""		,0	,.F.})	
	aAdd(aPerg,{1,"Grupo Produto at�: "	,Space(04),""	,""		,""		,""		,0	,.F.})
		
	If ParamBox(aPerg,"Par�metros...",@aRet) 

	 	Processa({|| Geradados(aRet)}, "Exportando dados")
		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Estoque '+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Estoque"
    Local cTable1    	:= "ESTOQUE ANAL�TICO" 

        
    //Dados tabela 1          
    cQry := " SELECT B2.B2_COD AS PRODUTO, "
    cQry += "        B1.B1_TIPO AS TIPO, "
    cQry += "        B1.B1_GRUPO AS GRUPO, "
    cQry += " 	   BM.BM_DESC AS DESC_GRUPO, "
    cQry += "        B1.B1_DESC AS DESCRICAO, "
    cQry += "        B1.B1_UM AS UNID, "
    cQry += "        B2.B2_FILIAL AS FILIAL, "
    cQry += "        B2.B2_LOCAL AS ARMAZEM, "
    cQry += " 	   UPPER(NR.NNR_DESCRI) AS DESC_ARMAZEM, "
    cQry += "        B2.B2_QATU AS SALDO, "
    cQry += "        (B2.B2_XRES + B2.B2_RESERVA) AS EMPENHO, "
    cQry += "        (B2.B2_QATU - (B2.B2_XRES + B2.B2_RESERVA)) AS DISPONIVEL, "	   
    cQry += " 	   B1.B1_CUSTD AS CUSTO, "
    cQry += " 	   (B2.B2_QATU * B1.B1_CUSTD) AS CUSTO_TOTAL, "
    cQry += " 	   B1.B1_YSITUAC AS SITUACAO, "
    cQry += " 	   ISNULL(AI.ZAI_MARCA,'') AS MARCA, "
    cQry += " 	   B1.B1_YDESCIC AS CICLO, "
    cQry += " 		ZAG.ZAG_DESCRI AS SUBGRUPO, "
    cQry += " 		ZAA.ZAA_DESCRI AS COLECAO, "
    cQry += " 		ZAH.ZAH_DESCRI AS SUBCOLECAO, "
    cQry += " 	   CASE  "
    cQry += " 	       WHEN B1.B1_ORIGEM = 0 THEN 'FABRICADO' "
    cQry += " 		   WHEN B1.B1_ORIGEM = 1 THEN 'IMPORTADO' "
    cQry += " 		   WHEN B1.B1_ORIGEM = 2 THEN 'IMPORTADO' "
    cQry += " 		   WHEN B1.B1_ORIGEM = 3 THEN 'IMPORTADO' "
    cQry += " 		   WHEN B1.B1_ORIGEM = 4 THEN 'FABRICADO' "
    cQry += " 		   WHEN B1.B1_ORIGEM = 5 THEN 'FABRICADO' "
    cQry += " 		   WHEN B1.B1_ORIGEM = 6 THEN 'IMPORTADO' "
    cQry += " 		   WHEN B1.B1_ORIGEM = 7 THEN 'IMPORTADO' "
    cQry += " 		   WHEN B1.B1_ORIGEM = 8 THEN 'IMPORTADO' "
    cQry += " 	   END AS ORIGEM "
    cQry += " FROM SB2010 AS B2 WITH (NOLOCK) "
    cQry += " INNER JOIN SB1010 AS B1 WITH (NOLOCK) ON (B1_COD = B2_COD "
    cQry += "                                           AND B1.D_E_L_E_T_ = '') "
    cQry += " LEFT JOIN ZAI010 AS AI WITH (NOLOCK) ON (AI.ZAI_CODIGO = B1.B1_YMARCA " 
    cQry += " 										 AND AI.D_E_L_E_T_ = '') "
    cQry += " INNER JOIN SBM010 AS BM WITH (NOLOCK) ON (BM.BM_GRUPO = B1.B1_GRUPO "
    cQry += " 										 AND BM.D_E_L_E_T_ = '') "
    cQry += " INNER JOIN NNR010 AS NR WITH (NOLOCK) ON (NR.NNR_FILIAL = B2.B2_FILIAL AND NR.NNR_CODIGO = B2.B2_LOCAL "
    cQry += " 										 AND NR.D_E_L_E_T_ = '') "
    cQry += " LEFT JOIN ZAA010 AS ZAA WITH (NOLOCK) ON (B1_YCOLECA = ZAA_CODIGO " 
    cQry += " 										 AND ZAA.D_E_L_E_T_ = '') "
    cQry += " LEFT JOIN ZAH010 AS ZAH WITH (NOLOCK) ON (ZAH.ZAH_CODIGO = B1.B1_YSUBCOL " 
    cQry += " 										 AND ZAH.D_E_L_E_T_ = '') "
    cQry += " LEFT JOIN ZAG010 AS ZAG WITH (NOLOCK) ON (ZAG.ZAG_CODIGO = B1.B1_YSUBGRP "
    cQry += " 										 AND ZAG.D_E_L_E_T_ = '') "    
    cQry += " WHERE B2.D_E_L_E_T_ = '' "
    cQry += "   AND B2_QATU > 0 "
    
    If !Empty(aRet[1])
    	cQry += " AND B2.B2_FILIAL BETWEEN '"+Alltrim(aRet[1])+"' AND '"+Alltrim(aRet[2])+"' "
    EndIf

    If !Empty(aRet[3])    
    	cQry += " AND B2.B2_LOCAL BETWEEN '"+Alltrim(aRet[3])+"' AND '"+Alltrim(aRet[4])+"' "
    EndIf
    
    If !Empty(aRet[5])          
    	cQry += " AND B1.B1_TIPO = '"+Alltrim(aRet[5])+"' "
    EndIf
    
    If !Empty(aRet[6])    
    	cQry += " AND B1.B1_GRUPO BETWEEN '"+Alltrim(aRet[6])+"' AND '"+Alltrim(aRet[7])+"' "
    EndIf    

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PRODUTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"GRUPO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESC_GRUPO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESCRICAO",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"UNID",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ARMAZEM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESC_ARMAZEM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SALDO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"EMPENHO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DISPONIVEL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CUSTO",1)                
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CUSTO_TOTAL",1)         
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SITUACAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"MARCA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CICLO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SUBGRUPO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COLECAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SUBCOLECAO",1)                        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ORIGEM",1)
                                               
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                               QRY->PRODUTO,;
                                               QRY->TIPO,;
                                               QRY->GRUPO,;
                                               QRY->DESC_GRUPO,;
                                               QRY->DESCRICAO,;
                                               QRY->UNID,;
                                               QRY->FILIAL,;                                               
                                               QRY->ARMAZEM,;
                                               QRY->DESC_ARMAZEM,;
                                               QRY->SALDO,;
                                               QRY->EMPENHO,;
                                               QRY->DISPONIVEL,;
                                               QRY->CUSTO,;
                                               QRY->CUSTO_TOTAL,;
                                               QRY->SITUACAO,;
                                               QRY->MARCA,;
                                               QRY->CICLO,;
                                               QRY->SUBGRUPO,;
                                               QRY->COLECAO,;
                                               QRY->SUBCOLECAO,;
                                               QRY->ORIGEM;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return