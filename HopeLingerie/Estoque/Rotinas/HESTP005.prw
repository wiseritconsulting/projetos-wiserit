#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "TBICONN.CH" 
#include "topconn.ch"
#include "rwmake.ch"


user function HESTP005()

	Processa({|| U_Proces() },"Processando...","Processando pre-nota...")

return

User Function Proces()

	Local nOpc := 3 
	Local cQuery := ""
	Local _qry := ""
	Local _cId
    Private aCabec      := {}
    Private aItens      := {}
    Private aLinha      := {}
    Private lMsErroAuto := .F.
    
    
    Prepare Environment Empresa "01" Filial "0101"
    
    
    IF Select("TMP") > 0
       TMP->(dbCloseArea())
    Endif
     
    cQuery := " SELECT SAI_COD_FORNECEDOR, SAI_LOJA_FORNECEDOR, ORM_NUM_ORDEM, NF ,REM_ID,ORM_NUM_ORDEM,CONVERT(varchar, REM_DATA,103) AS DATA  FROM SAT_PNE_VIEW_REMESSAS WHERE ISNULL(REM_INTEGRADO,'') NOT IN ('S','E')  GROUP BY SAI_COD_FORNECEDOR, SAI_LOJA_FORNECEDOR, ORM_NUM_ORDEM,REM_ID,NF,REM_DATA "
     
    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)

    DbSelectArea("TMP")
    DbGoTop()  
           
    _cId := cValtochar(TMP->REM_ID)       
        
    BEGIN TRANSACTION    
           
    aAdd(aCabec,{'F1_TIPO','N',NIL})
    aAdd(aCabec,{'F1_FORMUL','N',NIL})
    aAdd(aCabec,{'F1_DOC',PADL(ALLTRIM(TMP->NF),9,'0'),NIL})
    aAdd(aCabec,{"F1_SERIE","1",NIL})
    aAdd(aCabec,{"F1_EMISSAO",CTOD(TMP->DATA),NIL})
    aAdd(aCabec,{'F1_FORNECE',ALLTRIM(TMP->SAI_COD_FORNECEDOR),NIL})
    aAdd(aCabec,{'F1_LOJA',ALLTRIM(TMP->SAI_LOJA_FORNECEDOR),NIL})
    aAdd(aCabec,{"F1_ESPECIE","SPED",NIL})
    aAdd(aCabec,{"F1_COND",'',NIL})
    aAdd(aCabec,{"F1_STATUS",'',NIL})
    aAdd(aCabec,{"F1_XREMID",_cId,NIL})    
    
    IF Select("TMP1") > 0
       TMP1->(dbCloseArea())
    Endif
    
    cQuery1 := " SELECT SAI_COD_FORNECEDOR, SAI_LOJA_FORNECEDOR, NF,ITE_COD_PRODUTO,SAI_QUANTIDADE,SAI_PRECO_UNIT,SAI_DOCUMENTO,SAI_QUANTIDADE * SAI_PRECO_UNIT AS TOTAL,ITE_NUM_ORDEM_PRODUCAO  FROM SAT_PNE_VIEW_REMESSAS "
    cQuery1 += " WHERE  SAI_COD_FORNECEDOR = '"+TMP->SAI_COD_FORNECEDOR+"' AND SAI_LOJA_FORNECEDOR = '"+TMP->SAI_LOJA_FORNECEDOR+"' AND ORM_NUM_ORDEM = '"+TMP->ORM_NUM_ORDEM+"' AND REM_ID = '"+_cId+"' " 
    cQuery1 += " GROUP BY SAI_COD_FORNECEDOR, SAI_LOJA_FORNECEDOR, NF,ITE_COD_PRODUTO,SAI_QUANTIDADE,SAI_PRECO_UNIT,SAI_DOCUMENTO,ITE_NUM_ORDEM_PRODUCAO "  
    
    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMP1",.T.,.T.)

    DbSelectArea("TMP1")
    DbGoTop()  
           
     
    While TMP1->(!EOF())
    
    aItens := {}
 
    aAdd(aItens,{'D1_COD',TMP1->ITE_COD_PRODUTO,NIL})
    aAdd(aItens,{"D1_QUANT",TMP1->SAI_QUANTIDADE,Nil})
    aAdd(aItens,{"D1_VUNIT",TMP1->SAI_PRECO_UNIT,Nil})
    aAdd(aItens,{"D1_TOTAL",TMP1->TOTAL,Nil})
    aAdd(aItens,{"D1_TES",'001',NIL})
    aAdd(aItens,{"D1_LOCAL",'AP',NIL})
    aAdd(aItens,{"D1_NFORI",TMP1->SAI_DOCUMENTO,NIL})
    aAdd(aItens,{"D1_SERIORI","2",NIL})
    //aAdd(aItens,{"D1_OP",TMP1->ITE_NUM_ORDEM_PRODUCAO,NIL})
    
    aAdd(aLinha,aItens)
     
    TMP1->(DBSKIP())
    end 
            
    MSExecAuto({|x,y,z,a,b| MATA140(x,y,z,a,b)}, aCabec, aLinha, nOpc,,)
      
    If lMsErroAuto 
       // mostraerro()
     _qry := " UPDATE SAT_PNE_REMESSAS SET REM_INTEGRADO = 'E' WHERE REM_ID = '"+_cId+"'  "
     TcSqlExec(_qry) 
        
    Else 
    	
    _qry1 := " UPDATE SD1010 SET D1_IDENTB6 = D2_IDENTB6 , D1_CLASFIS = D2_CLASFIS, D1_ITEMORI = D2_ITEM FROM "+RetSqlName("SF1")+" " 
    _qry1 += " JOIN "+RetSqlName("SD1")+" ON F1_DOC = D1_DOC AND F1_FILIAL = D1_FILIAL AND F1_FORNECE = D1_FORNECE AND F1_LOJA = D1_LOJA AND SD1010.D_E_L_E_T_ = '' " 
    _qry1 += " JOIN "+RetSqlName("SD2")+" ON D1_NFORI = D2_DOC AND D1_SERIORI = D2_SERIE AND D1_COD = D2_COD AND SD2010.D_E_L_E_T_ = '' "  
    _qry1 += " WHERE F1_XREMID = '"+_cId+"' AND SF1010.D_E_L_E_T_ = '' " 

    TcSqlExec(_qry1)	
    	
     _qry := " UPDATE SAT_PNE_REMESSAS SET REM_INTEGRADO = 'S' WHERE REM_ID = '"+_cId+"'  "
     TcSqlExec(_qry) 
       
    EndIf 
    
    END TRANSACTION

    Reset Environment
return