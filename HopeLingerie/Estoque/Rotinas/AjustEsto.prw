#include "protheus.ch"
#INCLUDE "TBICONN.CH" 
#include "TOTVS.CH"
#INCLUDE "rwmake.ch"


/*/{Protheus.doc} AjustEsto
Ajuste de estoque
@author Weskley Silva
@since 09/06/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User function AjustEsto()

	Processa( {|| U_Import1() }, "Aguarde...", "Importando os Saldos\Custos",.F.)

return

User Function Import1()

	Local ExpA1 := {}
	Local ExpN2 := 3
	Local cTPMovimento := ""
	Local nQtd 	   := 0
	Local cProd	   := ""
	Local cUnidade     := ""
	Local cArmazem     := "AP"
	Local cOP := ""
	Local dEmissao     := dDatabase
	
	Local cLote := " "
	Local cSubLote := " "
	Local cEnderer := "PADRAO"
	Local nVlrAtu := 0
	Local aRet := {}
	Local aParamBox := {}

	PRIVATE lMsErroAuto := .F.

	aAdd(aParamBox,{6,"Buscar arquivo",Space(50),"","","",50,.F.,"Todos os arquivos (*.txt) |*.txt"})
	
	If !ParamBox(aParamBox,"Par�metros...",@aRet)
	      MsgInfo("Cancelado pelo usu�rio.","HOPE")
	      Return
	Endif
	
	_cArq := @aRet[1]

	If !File(_cArq)
		MsgStop("O arquivo " +_cArq + " n�o foi encontrado.","ATENCAO")
		Return
	EndIf

	FT_FUse( Alltrim(_cArq) )
	FT_FGoTop()

	ProcRegua(RecCount())

	While !(FT_FEof())
	
	
		cLinha := FT_FREADLN()	
		aLinha := StrTokArr(cLinha,";")
		cProd := PADR(aLinha[1],15)
		//cOP := Alltrim(aLinha[1])
		//nVlrInf := val(aLinha[4])
		cArmazem := Alltrim(aLinha[2])
		nQtd := val(aLinha[3])
		//cLote := Alltrim(aLinha[4])
		//cSubLote := Alltrim(aLinha[5])
		
		//IF !Empty(aLinha[4])
		//	cEnderer := Alltrim(aLinha[4])
		//endif
		
					
			dbSelectArea("SB1")
			dbSetOrder(1)
			if !dbSeek(xFilial("SB1")+cProd)
				Alert("Produto ("+cProd+") N�o Cadastrado.")
				FT_FSkip()
				LOOP				
			endif
			cProd := SB1->B1_COD
			cUnidade := Posicione("SB1",1,xFilial("SB1")+cProd,"B1_UM")
			dEmissao := dDatabase
			
			dbSelectArea("SB2")
			dbSetOrder(1)
			if !dbSeek(xFilial("SB2") + cProd + cArmazem)
				Alert("Produto ("+cProd+") N�o Cadastrado na Filial:"+xFilial("SB2")+ chr(10)+ chr(13)+"(Realizar ajuste manual.)")
				FT_FSkip()
				LOOP				
			endif
			
			nVlrAtu := SB2->B2_VATU1
			
			//nVlr := nVlrInf 
			nQtd1 := nQtd
			
			IF nQtd1 < 0
				cTPMovimento := '580'
				nQtd1 := nQtd1 * -1
				nVlr := 0
			//Else
			//	cTPMovimento := '030'	
			//	nVlr := 0	
			EndIF
				
				Begin Transaction
					
					IncProc()
					lMsErroAuto := .F.
				   	ExpA1 := {} 		
				   	aadd(ExpA1,{"D3_FILIAL",xFilial("SD3"),})	
				   	aadd(ExpA1,{"D3_TM","580",})	
				   	aadd(ExpA1,{"D3_COD",cProd,})	
				   	aadd(ExpA1,{"D3_UM",cUnidade,})			
				   	aadd(ExpA1,{"D3_LOCAL","AP",})	
				   	aadd(ExpA1,{"D3_QUANT",nQtd1,})	
				   	aadd(ExpA1,{"D3_CUSTO1",0,})
				   	aadd(ExpA1,{"D3_DOC","AJUST_AP",})		
				   	aadd(ExpA1,{"D3_EMISSAO",dEmissao,})
				   	//aadd(ExpA1,{"D3_LOTECTL",cLote,})
				   	//aadd(ExpA1,{"D3_NUMLOTE",cSubLote,})
				   	//aadd(ExpA1,{"D3_OP",cOP,})
				  // 	if cEnderer == '""""'
				  // 		aadd(ExpA1,{"D3_LOCALIZ"," ",})		
				   //	else 
				   	//aadd(ExpA1,{"D3_LOCALIZ","PADRAO",})					   		        
				   	//Endif
				   	MSExecAuto({|x,y| mata240(x,y)},ExpA1,ExpN2)		
				   	
				   	If !lMsErroAuto		
				   		ConOut("Incluido com sucesso! "+cTPMovimento)		
				   	Else		
					   	ConOut("Erro na inclusao!"+cLinha)	
				   		AutoGrLog("Linha:"+cLinha+" -- Valor:"+str(0))
						MostraErro()
				   	EndIf	
				   	
				   	ConOut("Fim  : "+cLinha)	         
				   	
			   	End Transaction
			   			   	
		FT_FSkip()
	End Do
		
Return Nil