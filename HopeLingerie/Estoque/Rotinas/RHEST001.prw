#Include "Protheus.ch"
#Include "TopConn.ch"
#INCLUDE "rwmake.ch"
#include "shell.ch"

#DEFINE cEOL	Chr(13)+Chr(10)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RHESTP001 �Autor  �DANIEL R. MELO      � Data �  08/07/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Relatorio de Consumos Ficha T�cnica                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function RHEST001(cOrigem)

	Local cDesc1	:= "Este programa tem como objetivo imprimir relatorio "
	Local cDesc2	:= "de acordo com os parametros informados pelo usuario."
	Local cDesc3	:= ""
	Local titulo	:= "Relatorio de Consumos Ficha T�cnica"
	Local nLin		:= 80
	Local Cabec1	:= " "
	Local Cabec2	:= " CODIGO          DESCRI��O DA MP                                          COR_PA  DESC_COR_PA         COR_MP  DESC_COR_MP          UN  TAM.NOVO"
	Local aOrd		:= {}

	Private lAbortPrint	:= .F.
	Private tamanho		:= "G"
	Private nomeprog	:= "RHEST001" // Coloque aqui o nome do programa para impressao no cabecalho
	Private nTipo		:= 18
	Private aReturn		:= { "Zebrado", 1, "Estoque/Custos", 2, 2, 1, "", 1}
	Private nLastKey	:= 0
	Private cPerg		:= PADR("RHEST001",10)
	Private m_pag		:= 01
	Private wnrel		:= "RHEST001" // Coloque aqui o nome do arquivo usado para impressao em disco
	Private cString 	:= ""
	Private cOrdemServ := ""

	If cOrigem
		
		cOrdemServ	:= SB4->B4_COD

	Else
	
		ValidPerg()
	
		Pergunte(cPerg,.T.)
		
		cOrdemServ	:= 	AllTrim(MV_PAR01)

	End
	
	
	If MsgYesNo("Deseja gerar em Excel ?")
		Processa({|| fGeraExcel()},"Gerando Excel...")
	Else

		wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.T.,Tamanho,,.T.)

		If nLastKey == 27
			Return
		Endif

		SetDefault(aReturn,cString)

		If nLastKey == 27
			Return
		Endif

		nTipo := If(aReturn[4]==1,15,18)

		RptStatus({|| RunReport(Cabec1,Cabec2,Titulo,nLin) },Titulo)

	EndIf

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RUNREPORT �Autor  �DANIEL R. MELO      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Impress�o pelo Prothueus                                    ���
�������������������������������������������������������������������������ͼ��          '
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RunReport(Cabec1,Cabec2,Titulo,nLin)

	Local cQry1
	Local cRevisa		:= ""
	Local _Ficha		:= ""
	Local _DesPro		:= ""
	Local nTotReg		:= 0
	Local nContar		:= 0

	cRevisa	:= Revisa()
	
	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf

	cQry1			 := fQuery1(cRevisa)
	
	If AllTrim(cQry1) == ""
		Alert("N�o existem registros para gerar o arquivo!")
		Return
	End

	TCQUERY cQry1 New Alias "TMP"
	TMP->(dbGoTop())

	DbSelectArea("TMP")
	TMP->(DbEval({ || nTotReg++ },,{|| !Eof()}))
	TMP->(DbGoTop())
	
	For _i := 1 to Len(aHeaderEx)
		Cabec2 += " "+AHEADEREX[_i][1]+"  "
	Next
 
	Cabec1	:= "FICHA_TECNICA = "+AllTrim(TMP->FICHA_TECNICA)+" - REVISAO = "+cRevisa+"  -  DESCRICAO = "+AllTrim(TMP->DESC_PRODUTO)
	
	While !TMP->(Eof())

		ProcRegua(nTotReg)

		If lAbortPrint
			@nLin,00 PSAY "*** CANCELADO PELO OPERADOR ***"
			Exit
		Endif

		If nLin > 55
			Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)
			nLin := 9
		Endif

		_nSoma := 0
		
		For _i := 1 to Len(aHeaderEx)
			_cCampo := "TMP->"+(AllTrim(AHEADEREX[_i][2]))
			_nSoma  += &_cCampo
		Next

		If _nSoma > 0
		
			IncProc("Gerando relatorio, regs : "+StrZero(++nContar,6)+" de "+StrZero(nTotReg,6))
		
			nCol := 1
			@nLin, nCol PSAY TMP->COD_COMP
			nCol += 1+15
			@nLin, nCol PSAY SUBSTR(AllTrim(TMP->DESC_COMPOSICAO),1,55)
			nCol += 3+55
			@nLin, nCol PSAY TMP->COR_PA
			nCol += 1+6
			@nLin, nCol PSAY SUBSTR(AllTrim(TMP->DESC_COR_PA),1,20)
			nCol += 1+20
			@nLin, nCol PSAY TMP->COR_MP
			nCol += 1+6
			@nLin, nCol PSAY SUBSTR(AllTrim(TMP->DESC_COR_MP),1,20)
			nCol += 1+20
			@nLin, nCol PSAY TMP->UN
			nCol += 1+5
			@nLin, nCol PSAY TMP->TAM_NOVO
			nCol += 1+6
	
			For _i := 1 to Len(aHeaderEx)
				_cCampo	:= "TMP->"+(AllTrim(AHEADEREX[_i][2]))
				@nLin, nCol PSAY &_cCampo PICTURE "@E 999.999999"
				nCol	:= nCol+14
			Next
			
			nLin++
			If SUBSTR(AllTrim(TMP->DESC_COMPOSICAO),56,100)<>''
				@nLin, 018 PSAY AllTrim(SUBSTR(AllTrim(TMP->DESC_COMPOSICAO),56,100))
				nLin++
			End

		EndIf
		
		TMP->(dbSkip())

	EndDo

	TMP->(dbCloseArea())

	SET DEVICE TO SCREEN

	If aReturn[5]==1
		dbCommitAll()
		SET PRINTER TO
		OurSpool(wnrel)
	Endif

	MS_FLUSH()

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FGERAEXCEL�Autor  �DANIEL R. MELO      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Geracao do arquivo Excel                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function fGeraExcel()

	Local cQry1
	Local cRevisa		:= ""
	Local _cArq			:= ""
	Local _nHdl			:= 0
	Local _cTempPath	:= Alltrim(GetTempPath())
	Local _cDirDocs		:= MsDocPath()
	Local _style		:= ""
	Local _cCabec		:= ""
	Local _cHtml		:= ""
	Local nTotReg		:= 0
	Local nContar		:= 0

	IncProc()
	
	cRevisa	:= Revisa()
	
	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf

	cQry1	 := fQuery1(cRevisa)
	
	If AllTrim(cQry1) == ""
		Alert("N�o existem registros para gerar o arquivo!")
		Return
	End

	_style := "<html>"
	_style += "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=iso-8859-1'>"
	_style += "<style type='text/css'>"
	_style += "td.Cabec1{ "
	_style += 		"font-size: 12px; "
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"text-align: Center;"
	_style += 		"vertical-align: middle; "
	_style +=  		"border-style: dotted; "
	_style +=  		"border-color: gray; "
	_style += 		"color: '#000066'; "
	_style += "} "
	_style += "td.tabela1{ "
	_style += 		"text-align: center; "
	_style += 		"font-size: 10px; "
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"border-width: 1px; "
	_style += 		"vertical-align: middle; "
	_style += 		"padding: 0px; "
	_style += 		"border-style: dotted; "
	_style += 		"border-color: gray; "
	_style += 		"-moz-border-radius: ; "
	_style += "} "
	_style += "td.tabela2{ "
	_style += 		"font-size: 10px; "
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"border-width: 1px; "
	_style += 		"vertical-align: middle; "
	_style += 		"padding: 0px; "
	_style += 		"border-style: dotted; "
	_style += 		"border-color: gray; "
	_style += 		"-moz-border-radius: ; "
	_style += "} "
	_style += "td.tabela3{ "
	_style += 		"font-size: 10px; "
	_style += 		"width: 500px; "   //tamanho da coluna
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"border-width: 1px; "
	_style += 		"vertical-align: middle; "
	_style += 		"padding: 0px; "
	_style += 		"border-style: dotted; "
	_style += 		"border-color: gray; "
	_style += 		"-moz-border-radius: ; "
	_style += "} "
	_style += "td.codigo{ "
	_style += 		'mso-number-format:"\@"; '
	_style += 		"text-align: center; "
	_style +=  		"font-size: 10px; "
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"border-width: 1px; "
	_style += 		"vertical-align: middle;"
	_style +=  		"padding: 0px; "
	_style +=  		"border-style: dotted; "
	_style +=  		"border-color: gray; "
	_style +=  		"-moz-border-radius: ; "
	_style += "} "
	_style += " td.valor{"
	_style += 		'mso-number-format:"\#\,\#\#0\.000000"; '
	_style += 		"width: 80px; "   //tamanho da coluna
	_style += 		"text-align: right; "
	_style += 		"font-size: 10px; "
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"border-width: 1px; "
	_style += 		"vertical-align: middle;"
	_style += 		"padding: 0px; "
	_style += 		"border-style: dotted; "
	_style += 		"border-color: gray; "
	_style += 		"-moz-border-radius: ; "
	_style += "} "
	_style += "</style>"

	TCQUERY cQry1 New Alias "TMP"
	TMP->(dbGoTop())

	DbSelectArea("TMP")
	TMP->(DbEval({ || nTotReg++ },,{|| !Eof()}))
	TMP->(DbGoTop())

	_Colspan	:= AllTrim(STR(8 + Len(aHeaderEx)))
	
	_cCabec := "<body>"
	_cCabec += "<table>"
	_cCabec += "<tr><td class=Cabec1 Colspan='"+_Colspan+"'><b>" +SM0->(UPPER(AllTrim(M0_NOME))) + " / " + SM0->(UPPER(AllTrim(M0_FILIAL)))+ "</b></td></tr>"
	_cCabec += "<tr><td class=Cabec1 Colspan='"+_Colspan+"'><b>RELATORIO DE FICHA T�CNICA (RHEST001)</b></td></tr>"
	_cCabec += "<tr><td class=tabela1 Colspan='"+_Colspan+"'><b>  FICHA_TECNICA = "+AllTrim(TMP->FICHA_TECNICA)+" - REVISAO = "+cRevisa+"  -  DESCRICAO = "+AllTrim(TMP->DESC_PRODUTO)+"</b></td></tr>"

	_cCabec += "<tr>"
	_cCabec += "<td class=tabela1><b>CODIGO</b></td>"
	_cCabec += "<td class=tabela1><b>DESCRI��O DA MP</b></td>"
	_cCabec += "<td class=tabela1><b>COR_PA</b></td>"
	_cCabec += "<td class=tabela1><b>DESC_COR_PA</b></td>"
	_cCabec += "<td class=tabela1><b>COR_MP</b></td>"
	_cCabec += "<td class=tabela1><b>DESC_COR_MP</b></td>"
	_cCabec += "<td class=tabela1><b>UN</b></td>"
	_cCabec += "<td class=tabela1><b>TAM NOVO</b></td>"

	For _i := 1 to Len(aHeaderEx)
		_cCabec += "<td class=tabela1><b>"+AHEADEREX[_i][1]+"</b></td>"
	Next
	_cCabec += "</tr>"

	_cArq := CriaTrab(,.f.)
	_nHdl := nHdl := FCreate(_cDirDocs + "\" + _cArq + ".xls")

	fWrite(_nHdl,_style,Len(_style))
	fWrite(_nHdl,_cCabec,Len(_cCabec))
	
	While !TMP->(Eof())

		ProcRegua(nTotReg)

		_nSoma := 0
		
		For _i := 1 to Len(aHeaderEx)
			_cCampo := "TMP->"+(AllTrim(AHEADEREX[_i][2]))
			_nSoma  += &_cCampo
		Next

		If _nSoma > 0

			IncProc("Gerando relatorio, regs : "+StrZero(++nContar,6)+" de "+StrZero(nTotReg,6))

			_cHtml := "<tr>"
			_cHtml += "<td class=codigo>"+TMP->COD_COMP+"</td>"
			_cHtml += "<td class=tabela3>"+SUBSTR(AllTrim(TMP->DESC_COMPOSICAO),1,55)+"</td>"
			_cHtml += "<td class=codigo>"+TMP->COR_PA+"</td>"
			_cHtml += "<td class=tabela2>"+SUBSTR(AllTrim(TMP->DESC_COR_PA),1,20)+"</td>"
			_cHtml += "<td class=codigo>"+TMP->COR_MP+"</td>"
			_cHtml += "<td class=tabela2>"+SUBSTR(AllTrim(TMP->DESC_COR_MP),1,20)+"</td>"
			_cHtml += "<td class=codigo>"+TMP->UN+"</td>"
			_cHtml += "<td class=codigo>"+TMP->TAM_NOVO+"</td>"
	
			For _i := 1 to Len(aHeaderEx)
				_cCampo := "TMP->"+(AllTrim(AHEADEREX[_i][2]))
				_cHtml  += "<td class=valor>"+AllTrim(TRANSFORM(&_cCampo, "@E 999.999999"))+"</td>"
			Next
	
			_cHtml += "</tr>"
		
			fWrite(_nHdl,_cHtml,Len(_cHtml))
		
		EndIf
		
		TMP->(dbSkip())
		
	End
		
	FClose(_nHdl)

	If !Empty(_cArq) .and. _cHtml !=""
		CpyS2T(_cDirDocs + "\" + _cArq + ".xls", _cTempPath, .T.)
		Ferase(_cDirDocs + "\" + _cArq + ".xls")
		ShellExecute("open",_cTempPath + "\" + _cArq + ".xls","","",SW_SHOW)
	Else
		Alert("N�o existem registros para gerar o arquivo!")
	EndIf

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FQUERY1   �Autor  �DANIEL R. MELO      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Query para Grupos Utilizados no Relat�rio                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function fQuery1(cRevisa)

	Local cQry1
	Local cQuery1
	Local cQuery2
	Public aFields   := {}
	Public aHeaderEx := {}

	cColuna	:= POSICIONE("SB4",1,xFilial("SB4")+AllTrim(cOrdemServ),"B4_COLUNA")
 
//	Define field properties
	If alltrim(cColuna) <> ""
		cQuery1 := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI "+cEOL
		cQuery1 += "FROM "+RetSQLName("SBV")+" "+cEOL
		cQuery1 += "WHERE D_E_L_E_T_ = '' "+cEOL
		cQuery1 += "		AND BV_FILIAL = '" +xFilial("SBV") +"' "+cEOL
		cQuery1 += "		AND BV_TABELA = '"+SB4->B4_COLUNA+"' "+cEOL
		cQuery1 += "ORDER BY R_E_C_N_O_ "+cEOL
	  
		If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMPSBV",.T.,.T.)

		cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" "+cEOL
		cQuery2  += "WHERE D_E_L_E_T_ = ' ' "+cEOL
		cQuery2  += "		AND ZD_FILIAL = '"+xFilial("SZD")+"' "+cEOL
		cQuery2  += "		AND ZD_PRODUTO='"+AllTrim(cOrdemServ)+"' "+cEOL

		If Select("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)
		DbSelectArea("TMPSZD")
		TMPSZD->(DbGoTop())
		
		While TMPSZD->(!EOF())
			For _i := 1 to 200
				If &("TMPSZD->ZD_TAM"+StrZero(_i,3))=="X"
					_campo	:= "ZF_TAM"+StrZero(_i,3)
					
					DbSelectArea("TMPSBV")
					TMPSBV->(DbGoTop())
					For _x := 1 to _i
						_desc	:= TMPSBV->BV_DESCRI
						_chave	:= TMPSBV->BV_CHAVE
						TMPSBV->(DbSkip())
					Next nX
					DbSelectArea("SX3")
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo))
						nX := ASCAN( aFields, {|x| alltrim(x) == alltrim(_campo)})
						If nX = 0
							Aadd(aFields,_campo) //TMPSBV->BV_CHAVE)
							Aadd(aHeaderEx, {Space(5)+ alltrim(_desc)+ Space(5),SX3->X3_CAMPO,alltrim(_chave),"@E 99",2,0,"",SX3->X3_USADO,"N","","R","",""})
						Endif
					Endif
				Endif
			Next
			TMPSZD->(DbSkip())
		EndDo
  	
		cQry1 := "SELECT FICHA_TECNICA, DESC_PRODUTO, COD_COMP, DESC_COMPOSICAO, COR_PA, DESC_COR_PA, COR_MP, DESC_COR_MP, "+cEOL
		cQry1 += "		"
		For _i := 1 to Len(aHeaderEx)
			cQry1 += "SUM("+AllTrim(aHeaderEx[_i][2])+") AS '"+AllTrim(aHeaderEx[_i][2])+"', "
		Next
		cQry1 += ""+cEOL
		cQry1 += "		UN, CORTE, TAM_NOVO "+cEOL
		cQry1 += "FROM ( "+cEOL
		cQry1 += "	SELECT ZE_PRODUTO AS 'FICHA_TECNICA', SB4A.B4_DESC AS 'DESC_PRODUTO', ZE_COMP AS 'COD_COMP', "+cEOL
		cQry1 += "				SB4B.B4_DESC AS 'DESC_COMPOSICAO', ZG_COR AS 'COR_PA', SBV1.BV_DESCRI AS 'DESC_COR_PA', "+cEOL
		cQry1 += "				ZG_CORNV AS 'COR_MP', SBV2.BV_DESCRI AS 'DESC_COR_MP', "+cEOL
		For _i := 1 to Len(aHeaderEx)
			cQry1 += "				"
			cQry1 += "ISNULL((SELECT SUM("+AllTrim(aHeaderEx[_i][2])+") FROM "+RetSqlName("SZF")+" AS SZF (NOLOCK) "+cEOL
			cQry1 += "		WHERE SZF.D_E_L_E_T_<>'*' AND ZF_PRODUTO=ZE_PRODUTO AND ZF_COMP=ZE_COMP AND ZF_COR=ZG_COR AND ZE_INDICE=ZF_INDICE "+cEOL
			cQry1 += "		AND ZF_REVISAO=ZE_REVISAO AND '"+AllTrim(aHeaderEx[_i][3])+"'=ZK_TAM),0) AS '"+AllTrim(aHeaderEx[_i][2])+"', "+cEOL
		Next
		cQry1 += "				(SELECT TOP 1 B1_UM FROM "+RetSqlName("SB1")+" AS SB1 (NOLOCK) WHERE SUBSTRING(B1_COD,1,8) = ZE_COMP) AS 'UN', "+cEOL
		cQry1 += "				ZE_CORTE AS 'CORTE', ZK_TAMNV AS 'TAM_NOVO' "+cEOL
		cQry1 += "	FROM "+RetSqlName("SZE")+" SZE (NOLOCK) "+cEOL
		cQry1 += "			INNER JOIN "+RetSqlName("SB4")+" AS SB4A (NOLOCK) ON SB4A.B4_COD=ZE_PRODUTO AND SB4A.D_E_L_E_T_<>'*' "+cEOL
		cQry1 += "			INNER JOIN "+RetSqlName("SB4")+" AS SB4B (NOLOCK) ON SB4B.B4_COD=ZE_COMP AND SB4B.D_E_L_E_T_<>'*' "+cEOL
		cQry1 += "			INNER JOIN "+RetSqlName("SZG")+" AS SZG (NOLOCK) ON ZG_PRODUTO=ZE_PRODUTO AND ZG_REVISAO=ZE_REVISAO "+cEOL
		cQry1 += "					AND ZE_COMP=ZG_COMP AND ZE_INDICE=ZG_INDICE AND SZG.D_E_L_E_T_<>'*' "+cEOL
		cQry1 += "			INNER JOIN "+RetSqlName("SBV")+" AS SBV1 (NOLOCK) ON SBV1.BV_CHAVE=ZG_COR "+cEOL
		cQry1 += "					AND SBV1.BV_TABELA='COR' AND SBV1.D_E_L_E_T_<>'*' "+cEOL
		cQry1 += "			INNER JOIN "+RetSqlName("SBV")+" AS SBV2 (NOLOCK) ON SBV2.BV_CHAVE=ZG_CORNV AND SBV2.BV_TABELA='COR' "+cEOL
		cQry1 += "					AND SBV2.D_E_L_E_T_<>'*' "+cEOL
		cQry1 += "			INNER JOIN "+RetSqlName("SZF")+" AS SZF (NOLOCK) ON ZF_PRODUTO=ZE_PRODUTO AND ZF_REVISAO=ZE_REVISAO "+cEOL
		cQry1 += "					AND ZF_COMP=ZG_COMP AND ZG_COR=ZF_COR AND ZE_INDICE=ZF_INDICE AND SZF.D_E_L_E_T_<>'*' "+cEOL
		cQry1 += "			INNER JOIN "+RetSqlName("SZK")+" AS SZK (NOLOCK) ON ZK_PRODUTO=ZE_PRODUTO AND ZK_REVISAO=ZE_REVISAO "+cEOL
		cQry1 += "					AND ZK_COMP=ZG_COMP AND ZE_INDICE=ZK_INDICE AND SZK.D_E_L_E_T_<>'*' "+cEOL
		cQry1 += "	WHERE SZE.D_E_L_E_T_<>'*' "+cEOL
		cQry1 += "			AND ZE_PRODUTO='"+AllTrim(cOrdemServ)+"' "+cEOL
		cQry1 += "			AND ZG_CORNV<>'' "+cEOL
		cQry1 += "			AND ZE_COMP NOT LIKE 'SERV%' "+cEOL
		cQry1 += "			AND ZE_REVISAO='"+cRevisa+"' "+cEOL
		//Verificacao de erro em item
		//cQry1 += "		AND ZE_COMP IN ('MPEL0183') "+cEOL
		//Fim verificacao
		cQry1 += "	GROUP BY ZE_PRODUTO, SB4A.B4_DESC, ZE_COMP, SB4B.B4_DESC, ZG_COR, SBV1.BV_DESCRI, ZG_CORNV, SBV2.BV_DESCRI, ZE_REVISAO, "+cEOL
		cQry1 += "				"
		For _i := 1 to Len(aHeaderEx)
			cQry1 += ""+AllTrim(aHeaderEx[_i][2])+", "
		Next
		cQry1 += ""+cEOL
		cQry1 += "				ZE_CORTE, ZK_TAMNV, ZK_TAM, ZE_INDICE "+cEOL
		cQry1 += ") AS X "+cEOL
		cQry1 += "GROUP BY FICHA_TECNICA, DESC_PRODUTO, COD_COMP, DESC_COMPOSICAO, COR_PA, DESC_COR_PA, COR_MP, DESC_COR_MP, UN, CORTE, TAM_NOVO "+cEOL
		cQry1 += "ORDER BY CORTE DESC,DESC_PRODUTO,COD_COMP "+cEOL
		
	End

Return (cQry1)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �REVISA   �Autor  �DANIEL R. MELO      � Data �  07/02/2018 ���
�������������������������������������������������������������������������͹��
���Desc.     �Verifica��o de Revis�o da grade                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function Revisa()
	
	Local aRevisa	:= {}
	Local oSay1
	Local oButton1
	Local oComboBo1
	Local oButton2
	Local cRevisa := "   "

	Static oDlg
	
	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf
	
	cQuery  := "SELECT ZE_REVISAO FROM "+RetSQLName("SZE")+" SZE WHERE D_E_L_E_T_<>'*' AND ZE_PRODUTO='"+AllTrim(cOrdemServ)+"' GROUP BY ZE_REVISAO "+cEOL

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	DbSelectArea("TMP")
	TMP->(DbGoTop())
		
	While TMP->(!EOF())
		Aadd(aRevisa, TMP->ZE_REVISAO)
		TMP->(DbSkip())
	EndDo
	
	If Len(aRevisa) <> 1

		DEFINE MSDIALOG oDlg TITLE "Grade com Revis�o" FROM 000, 000  TO 150, 400 COLORS 0, 16777215 PIXEL
	
		@ 015, 033 SAY oSay1 PROMPT "Selecione a Revis�o para impress�o" SIZE 140, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 029, 034 MSCOMBOBOX oComboBo1 VAR cRevisa ITEMS aRevisa SIZE 072, 010 OF oDlg COLORS 0, 16777215 PIXEL
		@ 057, 152 BUTTON oButton2 PROMPT "Confirma" SIZE 037, 012 ACTION (Close(oDlg)) OF oDlg PIXEL
	
		ACTIVATE MSDIALOG oDlg CENTERED
	Else
		cRevisa	:= aRevisa[1]
	End

Return (cRevisa)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Fun��o    �VALIDPERG � Autor � AP5 IDE            � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Descri��o � Verifica a existencia das perguntas criando-as caso seja   ���
���          � necessario (caso nao existam).                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ValidPerg()

	Local _sAlias := Alias()
	Local aRegs := {}
	Local i,j

	dbSelectArea("SX1")
	dbSetOrder(1)
	cPerg := PADR(cPerg,10)
//��������������������������������������������������������������Ŀ
//� Variaveis utilizadas para parametros                         �
//� mv_par01              Produto                                �
//� mv_par02              Servi�o Linha                          �
//����������������������������������������������������������������

	//Grupo/Ordem/Pergunta/Variavel/Tipo/Tamanho/Decimal/Presel/GSC/Valid/                             Var01/ Def01     /Cnt01/Var02/Def02/Cnt02/Var03/Def03/Cnt03/Var04/Def04/Cnt04/Var05/Def05/Cnt05/F3
	/*1*/aAdd(aRegs,{cPerg,"01","Produto                   ? ","","","MV_CH1","C",15,0,0,"G","","MV_PAR01",""   ,"","",""        ,"",""   ,"","","","","","","","","","","","","","","","","","","",""   })
//	/*2*/aAdd(aRegs,{cPerg,"02","Servi�o Linha             ? ","","","MV_CH2","C",15,0,0,"G","","MV_PAR02",""   ,"","","SERVLINH","",""   ,"","","","","","","","","","","","","","","","","","","",""   })

	For i:=1 to Len(aRegs)
		If !dbSeek(cPerg+aRegs[i,2])
			RecLock("SX1",.T.)
			For j:=1 to FCount()
				If j <= Len(aRegs[i])
					FieldPut(j,aRegs[i,j])
				Endif
			Next
			MsUnlock()
		Endif
	Next

	DbSelectArea(_sAlias)

Return Nil