#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

User Function HESTP002()                        
Local oCancela
Local oGet1
Local oOk
Local oSay1
Private cGet1 := Space(11)
Private aHeaderEx := {}
Private aColsEx := {}
Static oDlg

  DEFINE MSDIALOG oDlg TITLE "Ajuste de Empenho" FROM -040, 000  TO 600, 1100 COLORS 0, 16777215 PIXEL

    @ 012, 012 SAY oSay1 PROMPT "Ordem de Produ豫o:" SIZE 054, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 011, 065 MSGET oGet1 VAR cGet1 SIZE 105, 010 OF oDlg VALID u_atusd4() COLORS 0, 16777215 F3 "SC2" PIXEL
    fMSNewGe1()
    @ 273, 497 BUTTON oOk PROMPT "Ok" SIZE 037, 012 ACTION (AtuEmp(),oDlg:End()) OF oDlg PIXEL
    @ 273, 450 BUTTON oCancela PROMPT "Cancela" SIZE 037, 012 ACTION Close(oDlg) OF oDlg PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED

Return

//------------------------------------------------ 
Static Function fMSNewGe1()
//------------------------------------------------ 
Local nX
Local aFieldFill := {}
Local aFields := {"D4_COD","B1_DESC","B1_UM","D4_QTSEGUM","D4_QUANT"}
Local aAlterFields := {"D4_QUANT"}
Static oMSNewGe1

  // Define field properties
  DbSelectArea("SX3")
  SX3->(DbSetOrder(2))
  For nX := 1 to Len(aFields)
    If SX3->(DbSeek(aFields[nX]))
      Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,,;
                       SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,,SX3->X3_CBOX,SX3->X3_RELACAO})
    Endif
  Next nX

  // Define field values
  For nX := 1 to Len(aFields)
    If DbSeek(aFields[nX])
      Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
    Endif
  Next nX
  Aadd(aFieldFill, .F.)
  Aadd(aColsEx, aFieldFill)

  oMSNewGe1 := MsNewGetDados():New( 031, 011, 251, 537, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return

User Function AtuSD4()

Local i, nX
aEstrutura := {}

	If !Empty(cGet1)
		dbSelectArea("SC2")
		dbSetOrder(1)
		dbSeek(xFilial()+left(cGet1,11))

		// Calcula qual o percentual da baixa de insumos
		dbSelectArea("SDC")
		dbSetOrder(2)
		dbSelectArea("SD4")
		dbSetOrder(2)
		dbSeek(xFilial()+SubStr(cGet1,1,11))
		While SD4->(!Eof()) .And. SD4->D4_FILIAL+SubStr(SD4->D4_OP,1,11)==xFilial("SD4")+SubStr(cGet1,1,11)
			//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
			//| Produtos de apropriacao indireta, quando ja possuem saldo no armazem de processo |
			//| a baixa e automatica pela producao, quando ainda nao possuem saldo no armazem de |
			//| de processo devera ser feita requisicao manual do armazem padrao.                |
			//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
			//If A260ApropI(SD4->D4_COD)
			//	SD4->(dbSkip())
			//	Loop
			//EndIf
			
			//DbSelectArea("SB1")
			//SB1->(DbSetOrder(1))
			//SB1->(DbSeek(xFilial("SB1")+SD4->D4_COD))
			
			If SD4->D4_QUANT > 0 
				nPos  := AScan(aEstrutura,{|x|AllTrim(x[1])==SD4->D4_COD})
				
				If nPos = 0
					AADD(aEstrutura,{SD4->D4_COD,SD4->D4_QTDEORI})
				Else
					aEstrutura[nPos,2] += SD4->D4_QTDEORI
				Endif
			Endif

			dbSelectArea("SD4")
			SD4->(dbSkip())
		End
	EndIf

	aEstrutura := aSort( aEstrutura,,, { |x,y| x[1] < y[1] } )
	_n := 0
	If Len(oMSNewGe1:aCols) > 0
		For i:=1 to Len(oMSNewGe1:aCols)
			oMSNewGe1:aCols[i,4] := 0
			oMSNewGe1:aCols[i,5] := 0
		Next
	Endif

	
	For i:=1 to Len(aEstrutura)

		If Empty(oMSNewGe1:aCols[1,1])
			ADEL(oMSNewGe1:aCols,1)
			ASIZE(oMSNewGe1:aCols,Len(oMSNewGe1:aCols)-1)
		EndIf
		// Adiciona item no acols
		_loc := aScan(oMSNewGe1:aCols,{|x| AllTrim(x[1]) == alltrim(aEstrutura[i,1])})
		
		If _loc = 0
			AADD(oMSNewGe1:aCols,Array(Len(oMSNewGe1:aHeader)+1))
			_n++
			// Preenche conteudo do acols
			For nx:=1 to Len(oMSNewGe1:aHeader)
				cCampo:=Alltrim(oMSNewGe1:aHeader[nx,2])
				If IsHeadRec(cCampo)
					oMSNewGe1:aCols[Len(oMSNewGe1:aCols)][nx] := 0
				Else
					oMSNewGe1:aCols[Len(oMSNewGe1:aCols)][nx] := CriaVar(cCampo,.F.)
				EndIf
			Next nx
			oMSNewGe1:aCOLS[Len(oMSNewGe1:aCols)][Len(oMSNewGe1:aHeader)+1] := .F.
			_loc := i
			// Preenche campos especificos
			SB1->(dbSetOrder(1))
			SB1->(dbSeek(xFilial("SB1")+aEstrutura[i,1]))
		
			oMSNewGe1:aCols[_n,1] := aEstrutura[i,1]
			oMSNewGe1:aCols[_n,2] := SB1->B1_DESC
			oMSNewGe1:aCols[_n,3] := SB1->B1_UM
			oMSNewGe1:aCols[_n,4] := aEstrutura[i,2]
			oMSNewGe1:aCols[_n,5] := 0
		Else
			oMSNewGe1:aCols[_loc,4] += aEstrutura[i,2]
		Endif
		
	Next i

	oMSNewGe1:aCols := aSort( oMSNewGe1:aCols,,, { |x,y| x[2] < y[2] } )
	oMSNewGe1:Refresh()

Return

Static Function AtuEmp()

Local _i

For _i := 1 to len(oMSNewGe1:aCols)

	If (oMSNewGe1:aCols[_i,5] > oMSNewGe1:aCols[_i,4] .or. oMSNewGe1:aCols[_i,5] < oMSNewGe1:aCols[_i,4]) .and. oMSNewGe1:aCols[_i,5] > 0
		DbSelectArea("SB2")
		DbSetOrder(1)
		DbSeek(xfilial("SB2")+oMSNewGe1:aCols[_i,1]+"AP")
	
		If Found()
			RecLock("SB2",.F.)
				Replace B2_QEMP		with B2_QEMP + (oMSNewGe1:aCols[_i,5]-oMSNewGe1:aCols[_i,4]) 
			MsUnLock()
		Endif
		
		DbSelectArea("SD4")
		DbSetOrder(1)
		DbSeek(xfilial("SD4")+oMSNewGe1:aCols[_i,1]+alltrim(cGet1))
		
		While !EOF() .and. SD4->D4_COD = oMSNewGe1:aCols[_i,1] .and. SubStr(SD4->D4_OP,1,11) = alltrim(cGet1)
			
			_qtd :=  ((oMSNewGe1:aCols[_i,5]-oMSNewGe1:aCols[_i,4]))*(D4_QTDEORI/oMSNewGe1:aCols[_i,4])
			
			If SD4->D4_QUANT > 0 
				RecLock("SD4",.F.)
					Replace D4_QTDEORI	with D4_QTDEORI + _qtd
					Replace D4_QUANT	with D4_QUANT + _qtd
				MsUnLock()
			Endif
			
			DbSelectArea("SD4")
			DbSkip()
		End
	Endif
Next

Return()