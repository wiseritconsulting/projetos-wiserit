#include 'protheus.ch'
#include 'parmtype.ch'
#include "dbtree.ch"
#include "topconn.ch"

/*/{Protheus.doc} SaldIni
Saldo Inicial Automatico
@author Weskley Silva
@since 13/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function SaldoIni()

	Local cAreaAnterior := getarea()
	Local lMsErroAuto  := .F.
	
	//TODO Query que verifica os produtos que n�o possui saldo inicial
	
	cQuery := " SELECT * FROM " + RETSQLNAME("SB1")+  " (NOLOCK) SB1  WHERE SB1.D_E_L_E_T_ = '' AND NOT EXISTS (SELECT * FROM " + RETSQLNAME('SB9')+ " (NOLOCK) SB9  " 
	cQuery += " WHERE SB9.B9_COD = SB1.B1_COD AND  SB1.D_E_L_E_T_ = '' AND SB9.D_E_L_E_T_ = '' AND SB9.B9_LOCAL NOT IN ('E0') ) " 
	cQuery += " AND SB1.D_E_L_E_T_ = '' "
	
	TCQuery cQuery new alias T01
	
	WHILE !T01->(EOF())
	dbSelectArea("SM0")
	dbGOTOP() 
	
	ProcRegua(0)
	
    WHILE !SM0->(EOF())
	
					cFilanterior := cfilant
					//TODO Preenchimento do vetor para inclus�o via msExecauto na tabela SB9 - Rotina MATA220  
					Begin Transaction
						PARAMIXB1 := {}
						aadd(PARAMIXB1,{"B9_FILIAL",xfilial("SB9",SM0->M0_CODFIL),})
						aadd(PARAMIXB1,{"B9_COD",alltrim(T01->B1_COD),})
						aadd(PARAMIXB1,{"B9_LOCAL","E0",})
						aadd(PARAMIXB1,{"B9_QINI",0,})
						cfilant := SM0->M0_CODFIL
						MSExecAuto({|x,y| mata220(x,y)},PARAMIXB1,3)
						cfilant := cFilanterior
						If lMsErroAuto
							mostraerro()
						EndIf
					End Transaction

					SM0->(DbSkip())
		ENDDO	
		T01->(DbSkip())
	ENDDO
	T01->(DBCLOSEAREA())
	
	MsgInfo("Saldos Iniciais executado","HOPE")
	RestArea(cAreaAnterior)
return
