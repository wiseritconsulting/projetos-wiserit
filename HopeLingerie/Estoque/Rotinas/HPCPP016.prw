#Include 'Protheus.ch'
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

User Function HPCPP016()

_qry := "update "+RetSqlName("SB1")+" set B1_MRP = 'N' from "+RetSqlName("SB1")+" SB1 where SB1.D_E_L_E_T_ = '' and B1_MRP = 'S' and "
_qry += "B1_TIPO in ('PF','KT') and B1_YSITUAC in (Select ZA8_CODIGO from "+RetSqlName("ZA8")+" "
_qry += "ZA8 where ZA8.D_E_L_E_T_ = '' and ZA8_PREFAS = 'F')"

TcSqlExec(_qry)

_qry := "update "+RetSqlName("SB1")+" set B1_MRP = 'S' from "+RetSqlName("SB1")+" SB1 where SB1.D_E_L_E_T_ = '' and B1_MRP <> 'S' and "
_qry += "B1_TIPO in ('PF','KT') and B1_YSITUAC in (Select ZA8_CODIGO from "+RetSqlName("ZA8")+" "
_qry += "ZA8 where ZA8.D_E_L_E_T_ = '' and ZA8_PREFAS = 'T')"

TcSqlExec(_qry)

Return