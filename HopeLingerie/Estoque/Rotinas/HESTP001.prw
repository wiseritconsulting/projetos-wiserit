#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
//#include "apwebsrv.ch" 
//#include "apwebex.ch"
//#include "ap5mail.ch"
#include "TOTVS.CH"
#define DS_MODALFRAME   128
#Define CRLF  CHR(13)+CHR(10)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HESTP001  �Autor  � Actual Trend       � Data �  13/02/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Rotina de cadastro de Produtos e Estrutura por Grade        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HESTP001()
	
	local cVldAlt	:= ".T."
	local cVldExc	:= ".T."
	local cAliasd
	
	cAlias	:= "SB4"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	private cCadastro := "Cadastro de Grade de Produtos"
	Private cLinha		:= "COR"
	Private wperg := 0
	Private aEST := {}
	Private aEST2:= {}
	Private aEST3:= {}
	Private aEST4:= {}
	Private cTabela		:= Space(03)

	aRotina := {;
		{ "Pesquisar" 				, "AxPesqui"			, 0, 1},;
		{ "Vis. Grade"				, "u_GrdProd(4)"		, 0, 2},;
		{ "Inc. Grade"				, "u_GrdProd(1)"		, 0, 3},;
		{ "Alt. Grade"				, "u_GrdProd(2)"		, 0, 4},;
		{ "Inc. Ficha"				, "u_GrdProd(11)"		, 0, 2},;
		{ "Alt. Ficha"				, "u_GrdProd(12)"		, 0, 3},;
		{ "Inc. Roteiro"			, "u_GrdProd(13)"		, 0, 2},;
		{ "Alt. Roteiro"			, "u_GrdProd(14)"		, 0, 3},;
		{ "Inc. Opcionais"			, "u_GrdProd(15)"		, 0, 2},;
		{ "Alt. Opcionais"			, "u_GrdProd(16)"		, 0, 3},;
		{ "Cod.Barras"				, "u_GrdProd(7)"		, 0, 3},;
		{ "Inc. Mix  "				, "u_GrdProd(8)"		, 0, 3},;
		{ "Alt. Mix  "				, "u_GrdProd(9)"		, 0, 3},;
		{ "Alt.Status"				, "u_GrdProd(10)"		, 0, 3},;
		{ "Inc. Pre�o"				, "u_PrecPro(1)"		, 0, 3},;
		{ "Vis. Pre�o"				, "u_PrecPro(3)"		, 0, 3},;
		{ "Ajusta Cor"				, "u_AjuCor()"			, 0, 3},;
		{ "Estrutura" 				, "u_EstPro1(1)"		, 0, 3},;
		{ "Limpa Registros"			, "u_Exclui()"			, 0, 3},;
		{ "Relat. Ficha T�cnica"	, "u_RHEST001(.T.)"		, 0, 3},;
		{ "Relat. Medid/Tama"		, "u_RHEST002(.T.)"		, 0, 3}}

	dbSelectArea(cAlias)
	mBrowse( 6, 1, 22, 75, cAlias)
	
return

User Function GrdProd(_tp)

// Esta rotina � a respons�vel pelo cadastro da grade de produto, criando SB4 (Grade de Produtos) e SB1 (Produtos)
// SX5 criado: Z3
// Consulta Padr�o: SBM-02/SX5-Z3

	Local cCabecalho

	Local oSay01
	Local oSay02
	Local oSay03
	Local oSay04
	Local oSay05
	Local oSay06
	Local oSay07
	Local oSay08
	Local oSay09
	Local oSay10
	Local oSay11
	Local oSay12
	Local oSay13
	Local oSay14
	Local oSay15
	Local oSay16
	Local oSay17
	Local oSay18
	Local oSay19
	Local oSay20
	Local oSay21
	Local oSay22
	Local oSay23
	Local oSay24
	Local oSay25
	Local oSay26
	Local oSay27
	Local oSay28
	Local oSay29
	Local oSay30
	Local oSay31
	Local oSay32
	Local oSay33
	Local oSay34
	Local oSay35
	Local oSay36
	Local oSay37
	Local oSay38
	Local oSay39
	Local oSay40
	Local oSay41
	Local oSay42
	Local oSay43
	Local oSay44
	Local oSay45
	Local oSay46

	Local oTipoDesc
	Local oOrigemDesc
	Local oGrupoDesc
	Local oCodigoDesc
	Local oUM
	Local oMarcaDesc
	Local oCicloDesc
	Local oGpProd
	Local oGpProdDesc
	Local oSGrupo
	Local oSGrupoDesc
	Local oColecao
	Local oColecaoDesc
	Local oSubCol
	Local oSubColDesc
	Local oTpProd
	Local oTpProdDesc
	Local oCatProd
	Local oCatProdDesc
	Local oOcasiao
	Local oOcasiaoDesc
	Local oTecido
	Local oRastro
	Local oEnd
	Local oLinha
	Local oPeso
	Local oDtCad
	Local oWEB
	Local oDescriPTB
	Local oDescriENG
	Local oDescriSPA
	Local oObsProd
	Local oFoto

	Local oButton1
	Local oButton2
	Local oButton3
	Local lSave			:= .F.

	Private oFolder1
	Private nLin
	Private oTipo
	Private oOrigem
	Private oGrupo
	Private oCodigo
	Private oMarca
	Private oCiclo
	Private oTipo
	Private oGpProd
	Private oSGrupo
	Private oColecao
	Private oSubCol
	Private oTpProd
	Private oCatProd
	Private oOcasiao
	Private oArm
	Private oColuna
	Private oPosIPI
	Private oTempo
	Private cTipo			:= Space(02)
	Private cTipoDesc		:= Space(60)
	Private cClass			:= Space(05)
//	Private cClassDesc		:= Space(60)
	Private cOrigem			:= Space(1)
	Private cOrigemDesc		:= POSICIONE("SX5",1,xFilial("SX5")+'S0'+cOrigem,"X5_DESCRI")
	Private cGrupo			:= Space(04)
	Private cGrupoDesc		:= Space(60)
	Private cCodigo			:= Space(26)
	Private cCodigoDesc		:= Space(120)
	Private cUM				:= Space(02)
	Private cMarca			:= Space(05)
	Private cMarcaDesc		:= Space(60)
	Private cCiclo			:= Space(05)
	Private cCicloDesc		:= Space(60)
	Private cGpProd			:= Space(05)
	Private cGpProdDesc		:= Space(60)
	Private cSGrupo			:= Space(05)
	Private cSGrupoDesc		:= Space(60)
	Private cColecao		:= Space(05)
	Private cColecaoDesc	:= Space(60)
	Private cSubCol			:= Space(05)
	Private cSubColDesc		:= Space(60)
	Private cTpProd			:= Space(05)
	Private cTpProdDesc		:= Space(60)
	Private cCatProd		:= Space(05)
	Private cCatProdDesc	:= Space(60)
	Private cOcasiao		:= Space(200)
	Private cOcasiaoDesc	:= Space(60)
	Private cTecido			:= Space(20)
	Private cArm			:= Space(02)
//	Private cRastro			:= 2
	Private cRastro			:= "SubLote"
//	Private cEnd			:= 1
	Private cEnd			:= "Sim"
	Private cColuna			:= Space(03)
	Private cColunx			:= Space(03)
	Private cPosIPI			:= Space(10)
	Private nPeso			:= 0
	Private dDtCad			:= Date()
	Private cWEB			:= 2
	Private cTempo			:= 0					//Daniel 21/03/2018		//Private cTempo	:= "0"
	Private cTrib 			:= " "
	Private cDescriPTB		:= ""
	Private cDescriENG		:= ""
	Private cDescriSPA		:= ""
	Private cObsProd		:= ""
	Private Foto			:= "LGRL01.bmp"
	Private cCodBarras		:= 0

	Static oDlg

	If _tp = 1 .or. _tp = 2 .or. _tp = 4
		SetKey( VK_F11, { || hCompo() } )
		SetKey( VK_F12, { || hLavage()  } )
	End

	If _tp = 1 .or. _tp = 2 
		DbSelectArea("ZAU")
		DbSetOrder(1)
		DbSeek(xfilial("ZAU")+SB4->B4_COD)
		If Found()
			Alert("Este produto possui cadastro de Opcionais e n�o pode ser alterado por esta rotina!")
			Return()
		Endif
	if !(__cuserid) $ GETMV("HP_USERGRA") .AND. (_tp = 1 .or. _tp = 2 )
		Alert("Apenas o setor de produtos, pode realizar altera��es na grade!!")
		return()
	EndIF
	Endif
	If _tp = 7 .and. !SB4->B4_TIPO $ "PF|PL|KT|ME|PI"
		Alert("Este produto n�o possui C�digo de Barras!")
		Return()
	Endif

	IF _tp = 2 .or. _tp = 4 .or. _tp = 7 .or. _tp = 8 .or. _tp = 9 .or. _tp = 10 .or. _tp = 11 .or. _tp = 12 .or. _tp = 13 .or. _tp = 14 .or. _tp = 15 .or. _tp = 16
		cTipo			:= SB4->B4_TIPO
		cTipoDesc		:= POSICIONE("SX5",1,xFilial("SX5")+'02'+cTipo,"X5_DESCRI")
//		cClass			:= SB4->B4_YCLASPR
//		cClassDesc		:= POSICIONE("ZAE",1,xFilial("ZAE")+cClass,"ZAE_DESCRI")
		cOrigem			:= If(Alltrim(SB4->B4_YORIGEM)<>"",SB4->B4_YORIGEM,"0")
		cOrigemDesc		:= POSICIONE("SX5",1,xFilial("SX5")+'S0'+cOrigem,"X5_DESCRI")
		cGrupo			:= SB4->B4_GRUPO
		cGrupoDesc		:= POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_DESC")
		cCodigo 		:= SB4->B4_COD
		cCodigoDesc		:= SB4->B4_DESC
		cUM				:= SB4->B4_UM
		cMarca			:= SB4->B4_YMARCA
		cMarcaDesc		:= SB4->B4_YNOMARC
		cCiclo			:= SubString(SB4->B4_YCICLO,1,5)
		cCicloDesc		:= SB4->B4_YNCICLO
		cGpProd			:= SubString(SB4->B4_YGPRODU,1,5)
		cGpProdDesc		:= SB4->B4_YNGPCOM
		cSGrupo			:= SB4->B4_YSUBGRP
		cSGrupoDesc		:= SB4->B4_YNSUBGR
		cColecao		:= SB4->B4_YCOLECA
		cColecaoDesc	:= SB4->B4_YNCOLEC
		cSubCol			:= SB4->B4_YSUBCOL
		cSubColDesc		:= SB4->B4_YNSUBCO
		cTpProd			:= SB4->B4_YTPPROD
		cTpProdDesc		:= SB4->B4_YNTPCOM
		cCatProd		:= SB4->B4_YCATEGO
		cCatProdDesc	:= SB4->B4_YNCATEG
		cOcasiao		:= SB4->B4_YOCASIA //SubString(SB4->B4_YOCASIA,1,5)
		If alltrim(cOcasiao) <> ""
			xocasiao := STRTRAN(cOcasiao,"*","")
			if alltrim(xocasiao) = ""
				cOcasiaoDesc	:= ""
			Else
				cOcasiaoDesc	:= POSICIONE("ZAD",1,xFilial("ZAD")+AllTrim(SubStr(xOcasiao,1,5)),"ZAD_DESCRI")
			Endif
		Endif
		cTecido			:= SB4->B4_YTECIDO
		cArm 			:= SB4->B4_LOCPAD
		cRastro 		:= If(SB4->B4_RASTRO="N",3,If(SB4->B4_RASTRO="L",1,2))
		cEnd 			:= If(SB4->B4_LOCALIZ="S",1,2)
		cLinha 			:= SB4->B4_LINHA
		cColuna 		:= SB4->B4_COLUNA
		nPeso 			:= SB4->B4_PESO
		cPosIPI 		:= SB4->B4_POSIPI
		dDtCad 			:= SB4->B4_YDATCAD
		cWeb 			:= If(SB4->B4_DWEB="S",1,2)
//		cTempo			:= SB4->B4_YTEMPO					//Daniel 21/03/2018  
		cTrib			:= SB4->B4_SITTRIB
		cDescriPTB 		:= SB4->B4_YDESCPT
		cDescriSPA 		:= SB4->B4_YDESCES
		cDescriENG 		:= SB4->B4_YDESCIE
		cObsProd		:= SB4->B4_YOBS
	Endif

	If _tp == 1
		cCabecalho := "Inclus�o no Cadastro de Grades"
	ElseIf _tp == 2
		cCabecalho := "Altera��o do Cadastro de Grades"
	ElseIf _tp == 4
		cCabecalho := "Visualizar Cadastro de Grades"
	ElseIf _tp == 7
		cCabecalho := "Gera��o de codigo de Barras"
	ElseIf _tp == 8
		cCabecalho := "Inclus�o de Cadastro de Mix para OP"
	ElseIf _tp == 9
		cCabecalho := "Altera��o de Cadastro de Mix para OP"
	ElseIf _tp == 10
		cCabecalho := "Ajuste de Status"
	ElseIf _tp == 11
		cCabecalho := "Inclus�o de Ficha T�cnica"
	ElseIf _tp == 12
		cCabecalho := "Altera��o de Ficha T�cnica"
	ElseIf _tp == 13
		cCabecalho := "Inclus�o de Roteiro"
	ElseIf _tp == 14
		cCabecalho := "Altera��o de Roteiro"
	ElseIf _tp == 15
		cCabecalho := "Inclus�o de Opcionais"
	ElseIf _tp == 16
		cCabecalho := "Altera��o de Opcionais"
	End

	DEFINE MSDIALOG oDlg TITLE cCabecalho FROM 000, 000  TO 600, 1250 COLORS 0, 16777215 PIXEL

	@ 004, 004 FOLDER oFolder1 SIZE 620, 181 OF oDlg ITEMS "Cadastro","Outros" COLORS 0, 16777215 PIXEL

	//FOLDER 01
	nLin	:= 004
	If _tp == 1
		@ nLin	, 005 SAY oSay01 PROMPT "Tipo" 							SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oTipo VAR cTipo			 					SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. GatTela(1,_tp) PICTURE "@!" COLORS 0, 16777215 F3 "02" 	PIXEL
		@ nLin	, 045 SAY oSay02 PROMPT "Desc. do Tipo" 				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oTipoDesc VAR cTipoDesc 					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
	Else
		GatTela(0,_tp)
	End

	//FOLDER 02
	nLin	:= 004
	@ nLin	, 005 SAY oSay40 PROMPT "Foto"							SIZE 055, 007 OF oFolder1:aDialogs[2] COLORS 0, 16777215 PIXEL
	@ nLin+8, 005 BITMAP oFoto	 									SIZE 112, 116 OF oFolder1:aDialogs[2] PIXEL
	@ nLin	, 130 SAY oSay41 PROMPT "Descri��o Portugu�s"			SIZE 055, 007 OF oFolder1:aDialogs[2] COLORS 0, 16777215 PIXEL
	@ nLin+8, 130 GET oDescriPTB VAR cDescriPTB			MULTILINE 	SIZE 235, 050 OF oFolder1:aDialogs[2] COLORS 0, 16777215 HSCROLL 		PIXEL
	@ nLin	, 378 SAY oSay42 PROMPT "Descri��o Espanhol"			SIZE 055, 007 OF oFolder1:aDialogs[2] COLORS 0, 16777215 PIXEL
	@ nLin+8, 378 GET oDescriSPA VAR cDescriSPA			MULTILINE 	SIZE 235, 050 OF oFolder1:aDialogs[2] COLORS 0, 16777215 HSCROLL 		PIXEL
    
	nLin	:= nLin+65
	@ nLin	, 130 SAY oSay43 PROMPT "Descri��o Ingl�s"				SIZE 055, 007 OF oFolder1:aDialogs[2] COLORS 0, 16777215 PIXEL
	@ nLin+8, 130 GET oDescriENG VAR cDescriENG			MULTILINE 	SIZE 235, 050 OF oFolder1:aDialogs[2] COLORS 0, 16777215 HSCROLL 		PIXEL
	@ nLin	, 378 SAY oSay44 PROMPT "Observa�oes do Produto"		SIZE 065, 007 OF oFolder1:aDialogs[2] COLORS 0, 16777215 PIXEL
	@ nLin+8, 378 GET oObsProd 	VAR cObsProd			MULTILINE 	SIZE 235, 050 OF oFolder1:aDialogs[2] COLORS 0, 16777215 HSCROLL 		PIXEL

// Rotinas para cria��o dos acols para v�rias op��es do menu
	If _tp = 7
		fMSCBar(_tp, cColuna)
	ElseIf _tp = 8 .or. _tp = 9
		fMSMix(_tp, cColuna)
	ElseIf _tp = 10
		fMSSta(_tp, cColuna)
	ElseIf _tp = 11 .or. _tp = 12
		fMSFicha(_tp, cColuna)
	ElseIf _tp = 13 .or. _tp = 14
		fMSFich1(_tp, cColuna)
	ElseIf _tp = 15 .or. _tp = 16
		fMSOPC(_tp, cColuna)
	Else
		fMSGrade(_tp, cColuna)
	Endif

	If _tp = 1 .or. _tp = 2 .or. _tp = 4
		@ 279, 005 SAY oSay45 PROMPT "F11 - Composi��o"						SIZE 100, 008 OF oDlg COLORS 0, 16777215 PIXEL
		@ 287, 005 SAY oSay46 PROMPT "F12 - Procedimentos de Lavagem"		SIZE 100, 008 OF oDlg COLORS 0, 16777215 PIXEL
	ElseIf _tp = 13 .or. _tp = 14
		@ 283, 005 SAY oSay38 PROMPT "Tempo Estimado de Produ��o"			SIZE 075, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 280, 085 MSGET oTempo VAR cTempo									SIZE 055, 010 OF oDlg VALID !VAZIO() PICTURE "9999.99" COLORS 0, 16777215 PIXEL
	End
	If _tp = 1 .or. _tp = 2
		@ 282, 450 BUTTON oButton3 PROMPT "Replicar"						SIZE 040, 012 ACTION ReplyLn(_tp) OF oDlg PIXEL
	ElseIf _tp = 7
		@ 282, 450 BUTTON oButton3 PROMPT "Gera C.Barras"					SIZE 040, 012 ACTION CriacBar(_tp) OF oDlg PIXEL
	End
	@ 282, 500 BUTTON oButton1 PROMPT "Ok" 									SIZE 040, 012 ACTION Save(_tp) OF oDlg PIXEL
	@ 282, 550 BUTTON oButton2 PROMPT "Cancelar" 							SIZE 040, 012 ACTION Close(oDlg) OF oDlg PIXEL

	ACTIVATE MSDIALOG oDlg CENTERED

	If _tp = 1 .or. _tp = 2 .or. _tp = 4
		Set Key VK_F11 To
		Set Key VK_F12 To
	End

Return

Static Function Gatilhos(cCodGat)
// Rotina para preenchimento dos campos de descri��o
	If cCodGat==2
		cOrigemDesc		:= POSICIONE("SX5",1,xFilial("SX5")+'S0'+cOrigem,"X5_DESCRI")
		If Alltrim(cOrigemDesc)==""
			oOrigem:SetFocus()
		End
	ElseIf cCodGat==3
		cMarcaDesc		:= POSICIONE("ZAI",1,xFilial("ZAI")+cMarca,"ZAI_MARCA")
		If Alltrim(cMarcaDesc)==""
			oMarca:SetFocus()
		End
	ElseIf cCodGat==4
		cCicloDesc		:= POSICIONE("ZAJ",1,xFilial("ZAJ")+cCiclo,"ZAJ_CICLO")
		If Alltrim(cCicloDesc)==""
			oCiclo:SetFocus()
		End
	ElseIf cCodGat==5
		cGpProdDesc		:= POSICIONE("ZAF",1,xFilial("ZAF")+cGpProd,"ZAF_DESCRI")
		If Alltrim(cGpProdDesc)==""
			oGpProd:SetFocus()
		End
	ElseIf cCodGat==6
		cSGrupoDesc		:= POSICIONE("ZAG",1,xFilial("ZAG")+cSGrupo,"ZAG_DESCRI")
		If Alltrim(cSGrupoDesc)==""
			oSGrupo:SetFocus()
		End
	ElseIf cCodGat==7
		cColecaoDesc	:= POSICIONE("ZAA",1,xFilial("ZAA")+cColecao,"ZAA_DESCRI")
		If Alltrim(cColecaoDesc)==""
			oColecao:SetFocus()
		End
	ElseIf cCodGat==8
		cSubColDesc		:= POSICIONE("ZAH",1,xFilial("ZAH")+cSubCol,"ZAH_DESCRI")
		If Alltrim(cSubColDesc)==""
			oSubCol:SetFocus()
		End
	ElseIf cCodGat==9
		cTpProdDesc		:= POSICIONE("ZAB",1,xFilial("ZAB")+cTpProd,"ZAB_DESCRI")
		If Alltrim(cTpProdDesc)==""
			oTpProd:SetFocus()
		End
	ElseIf cCodGat==10
		cCatProdDesc	:= POSICIONE("ZAC",1,xFilial("ZAC")+cCatProd,"ZAC_DESCRI")
		If Alltrim(cCatProdDesc)==""
			oCatProd:SetFocus()
		End
	ElseIf cCodGat==11
		If Alltrim(cOcasiao) <> ""
			xocasiao 	:= STRTRAN(cOcasiao,"*","")
			if alltrim(xocasiao) = ""
				cOcasiaoDesc	:= ""
			Else
				cOcasiaoDesc	:= POSICIONE("ZAD",1,xFilial("ZAD")+SubStr(xOcasiao,1,5),"ZAD_DESCRI")
			Endif
//		If Alltrim(cOcasiaoDesc)=="" 
//			oOcasiao:SetFocus()
//		End
		Endif
	ElseIf cCodGat==12
		If !EXISTCPO("NNR",cArm)
			oArm:SetFocus()
		End
	ElseIf cCodGat==13
		cTipo		:= Upper(cTipo)
		cTipoDesc	:= POSICIONE("SX5",1,xFilial("SX5")+'02'+cTipo,"X5_DESCRI")
		If Alltrim(cTipoDesc)==""
			oTipo:SetFocus()
		End
	End

Return

Static Function GatTela(cCodGat,_tp)
// Rotina para tratamento de habilitar campos para produtos do tipo PF

	If cCodGat==1
	//TODO Valida��o para inclus�o de servi�o {Weskley Silva 25/06/2018}
	  if cTipo == "SV" .AND. !(__cUserID $ GETMV("HP_INCPROD"))
	  		MsgAlert("Usuario sem permiss�o")
	  		return 
	  endif
	
		cTipoDesc	:= POSICIONE("SX5",1,xFilial("SX5")+'02'+cTipo,"X5_DESCRI")
		If Alltrim(cTipoDesc)==""
			oTipo:SetFocus()
		Else
			cArm 		:= IF(cTipo$"KT|PF|PL|ME","E1",IF(cTipo$"MP|MB","A1",If(cTipo$"PI|MI","AP","  ")))
			cUM			:= IF(cTipo$"PF","PC",Space(02))
			cWEB		:= IF(cTipo$"KT|ME|PF|PL",1,2)
		End
	Else
		@ nLin	, 005 SAY oSay01 PROMPT "Tipo" 								SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oTipo VAR cTipo 								SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(13) COLORS 0, 16777215 F3 "02"  PIXEL
	End
	If cTipo $ "MP-PF-ME-KT-MB-PL"
		//cEnd := 1
		cEnd := "Sim"
	Else
		//cEnd := 2
		cEnd := "N�o"
	Endif
	
	If cTipo $ "PF-PL-ME-KT"
		//cRastro := 1
		cRastro := "SubLote"
	Else
		//cRastro := 3
		cRastro := "Nao Utiliza"
	Endif
		

	If cTipo $ "PF|KT|ME|PL"
		nLin	:= 004
		@ nLin	, 045 SAY oSay02 PROMPT "Desc. do Tipo" 				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oTipoDesc VAR cTipoDesc 					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 212 SAY oSay03 PROMPT "Origem Prod." 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 212 MSGET oOrigem VAR cOrigem 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(2) COLORS 0, 16777215 F3 "S0" 	PIXEL
		@ nLin	, 252 SAY oSay04 PROMPT "Desc. Origem Prod."			SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 252 MSGET oOrigemDesc VAR cOrigemDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 419 SAY oSay05 PROMPT "Grupo" 						SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oGrupo VAR cGrupo							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. VldCod() PICTURE "@!" COLORS 0, 16777215 F3 "SBM-02" 	PIXEL
		@ nLin	, 459 SAY oSay06 PROMPT "Desc. Grupo"					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 459 MSGET oGrupoDesc VAR cGrupoDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		
		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay07 PROMPT "C�digo" 						SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		IF _tp = 2 .OR. _tp = 4
			@ nLin+8, 005 MSGET oCodigo VAR cCodigo 					SIZE 050, 010 OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 READONLY 		PIXEL
		Else
			@ nLin+8, 005 MSGET oCodigo VAR cCodigo 					SIZE 050, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .and. VldCod2(cCodigo) PICTURE "@!" COLORS 0, 16777215 PIXEL
		End
		@ nLin	, 060 SAY oSay08 PROMPT "Descri��o"						SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
//	    IF _tp = 2 .OR. _tp = 4
//	       @ nLin+8, 060 MSGET oCodigoDesc VAR cCodigoDesc				SIZE 300, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
//	    Else
		@ nLin+8, 060 MSGET oCodigoDesc VAR cCodigoDesc					SIZE 300, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() PICTURE "@!" COLORS 0, 16777215 PIXEL
//	    End
		@ nLin	, 372 SAY oSay09 PROMPT "Unid.Med."						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 372 MSGET oUM VAR cUM 								SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() COLORS 0, 16777215 F3 "SAH" 		PIXEL
		@ nLin	, 419 SAY oSay10 PROMPT "Marca Produto"					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oMarca VAR cMarca							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(3) COLORS 0, 16777215 F3 "ZAI" 	PIXEL
		@ nLin	, 459 SAY oSay11 PROMPT "Desc. Marca Prod."				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 459 MSGET oMarcaDesc VAR cMarcaDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		
		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay12 PROMPT "Ciclo" 						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oCiclo VAR cCiclo 							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(4) COLORS 0, 16777215 F3 "ZAJ" 	PIXEL
		@ nLin	, 045 SAY oSay13 PROMPT "Desc. Ciclo"					SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oCicloDesc VAR cCicloDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 212 SAY oSay14 PROMPT "Grupo Prod." 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 212 MSGET oGpProd VAR cGpProd							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(5) COLORS 0, 16777215 F3 "ZAF" 	PIXEL
		@ nLin	, 252 SAY oSay15 PROMPT "Desc. Grupo Prod."				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 252 MSGET oGpProdDesc VAR cGpProdDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 419 SAY oSay16 PROMPT "Sub Grupo"	 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oSGrupo VAR cSGrupo 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(6) COLORS 0, 16777215 F3 "ZAG" 	PIXEL
		@ nLin	, 459 SAY oSay17 PROMPT "Desc. Sub Grupo"				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 459 MSGET oSGrupoDesc VAR cSGrupoDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		
		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay18 PROMPT "Cole��o" 						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oColecao VAR cColecao 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(7) COLORS 0, 16777215 F3 "ZAA" 	PIXEL
		@ nLin	, 045 SAY oSay19 PROMPT "Desc. Cole��o"					SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oColecaoDesc VAR cColecaoDesc				SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 212 SAY oSay20 PROMPT "Sub Cole��o" 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 212 MSGET oSubCol VAR cSubCol							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(8) COLORS 0, 16777215 F3 "ZAH" 	PIXEL
		@ nLin	, 252 SAY oSay21 PROMPT "Desc. Sub Cole��o"				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 252 MSGET oSubColDesc VAR cSubColDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 419 SAY oSay22 PROMPT "Tipo Prod." 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oTpProd VAR cTpProd							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(9) COLORS 0, 16777215 F3 "ZAB" 	PIXEL
		@ nLin	, 459 SAY oSay23 PROMPT "Desc. Tipo Prod."				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 459 MSGET oTpProdDesc VAR cTpProdDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
	    
		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay24 PROMPT "Cat.Prod." 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oCatProd VAR cCatProd 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(10) COLORS 0, 16777215 F3 "ZAC" 	PIXEL
		@ nLin	, 045 SAY oSay25 PROMPT "Desc. Cat.Prod"				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oCatProdDesc VAR cCatProdDesc				SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 212 SAY oSay26 PROMPT "Ocasi�o" 						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 212 MSGET oOcasiao VAR cOcasiao 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID Gatilhos(11) COLORS 0, 16777215 F3 "ZAD" 	PIXEL
		@ nLin	, 252 SAY oSay27 PROMPT "Desc. Ocasi�o"					SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 252 MSGET oOcasiaoDesc VAR cOcasiaoDesc				SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 419 SAY oSay28 PROMPT "Tecido" 						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oTecido VAR cTecido 						SIZE 195, 010 OF oFolder1:aDialogs[1] VALID PICTURE "@!" COLORS 0, 16777215 PIXEL
	   
		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay30 PROMPT "Armaz�m" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oArm VAR cArm 								SIZE 055, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(12) PICTURE "@!" COLORS 0, 16777215 F3 "NNR" 		PIXEL
		@ nLin	, 075 SAY oSay31 PROMPT "Lote/SubLote"					SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 075 MSCOMBOBOX oRastro VAR cRastro		ITEMS {"Lote","SubLote","Nao Utiliza"}		SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin	, 145 SAY oSay32 PROMPT "Endere�o" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 145 MSCOMBOBOX oEnd VAR cEnd 				ITEMS {"Sim","N�o"}							SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin	, 215 SAY oSay33 PROMPT "Linha" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 215 MSGET oLinha VAR cLinha 							SIZE 055, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY			PIXEL
		@ nLin	, 285 SAY oSay34 PROMPT "Coluna" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		IF _tp == 1
			@ nLin+8, 285 MSGET oColuna VAR cColuna							SIZE 055, 010 VALID !VAZIO() .AND. u_valacols(_tp, cColuna) OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 F3 "SX5-Z3" PIXEL
		Else
			@ nLin+8, 285 MSGET oColuna VAR cColuna							SIZE 055, 010 VALID !VAZIO() .AND. u_valacols(_tp, cColuna) OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 F3 "SX5-Z3" READONLY PIXEL
		Endif
		@ nLin	, 355 SAY oSay35 PROMPT "Peso Liquido"					SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 355 MSGET oPeso VAR nPeso 							SIZE 055, 010 PICTURE "@E 999,999.9999" OF oFolder1:aDialogs[1] VALID !VAZIO() COLORS 0, 16777215 PIXEL
		@ nLin	, 425 SAY oSay36 PROMPT "Pos. IPI/NCM"					SIZE 040, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 425 MSGET oPosIPI VAR cPosIPI	 						SIZE 050, 010 PICTURE "@R 9999.99.99" OF oFolder1:aDialogs[1] VALID U_ExistCod(1,cPosIPI) COLORS 0, 16777215 F3 "SYD" PIXEL
		@ nLin	, 490 SAY oSay37 PROMPT "Data de cadastro"				SIZE 055, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 490 MSGET oDtCad VAR dDtCad 							SIZE 055, 010 PICTURE "@D 99/99/9999" OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin	, 559 SAY oSay39 PROMPT "Disponivel na WEB"				SIZE 055, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 559 MSCOMBOBOX oWeb VAR cWeb				ITEMS {"Sim","N�o"}							SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
	 
	Else

		nLin	:= 004
		@ nLin	, 045 SAY oSay02 PROMPT "Desc. do Tipo" 				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oTipoDesc VAR cTipoDesc 					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 212 SAY oSay03 PROMPT "Origem Prod." 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 212 MSGET oOrigem VAR cOrigem 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(2) COLORS 0, 16777215 F3 "S0" 	PIXEL
		@ nLin	, 252 SAY oSay04 PROMPT "Desc. Origem Prod."			SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 252 MSGET oOrigemDesc VAR cOrigemDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 419 SAY oSay05 PROMPT "Grupo" 						SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oGrupo VAR cGrupo							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. VldCod() PICTURE "@!" COLORS 0, 16777215 F3 "SBM-02" 	PIXEL
		@ nLin	, 459 SAY oSay06 PROMPT "Desc. Grupo"					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 459 MSGET oGrupoDesc VAR cGrupoDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		
		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay07 PROMPT "C�digo" 						SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		IF _tp = 2 .OR. _tp = 4
			@ nLin+8, 005 MSGET oCodigo VAR cCodigo 					SIZE 050, 010 OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 READONLY 		PIXEL
		Else
			@ nLin+8, 005 MSGET oCodigo VAR cCodigo 					SIZE 050, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() PICTURE "@!"  COLORS 0, 16777215 PIXEL
		End
		@ nLin	, 060 SAY oSay08 PROMPT "Descri��o"						SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
//	    IF _tp = 2 .OR. _tp = 4
//	       @ nLin+8, 060 MSGET oCodigoDesc VAR cCodigoDesc				SIZE 300, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
//	    Else
		@ nLin+8, 060 MSGET oCodigoDesc VAR cCodigoDesc					SIZE 300, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() PICTURE "@!" COLORS 0, 16777215 PIXEL
//	    End
		@ nLin	, 372 SAY oSay09 PROMPT "Unid.Med."						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 372 MSGET oUM VAR cUM 								SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() COLORS 0, 16777215 F3 "SAH" 		PIXEL
	   
		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay30 PROMPT "Armaz�m" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oArm VAR cArm 								SIZE 055, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(12) PICTURE "@!" COLORS 0, 16777215 F3 "NNR" 		PIXEL
		@ nLin	, 075 SAY oSay31 PROMPT "Lote/SubLote"					SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 075 MSCOMBOBOX oRastro VAR cRastro		ITEMS {"Lote","SubLote","Nao Utiliza"}		SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin	, 145 SAY oSay32 PROMPT "Endere�o" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 145 MSCOMBOBOX oEnd VAR cEnd 				ITEMS {"Sim","N�o"}							SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin	, 215 SAY oSay33 PROMPT "Linha" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 215 MSGET oLinha VAR cLinha 							SIZE 055, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY			PIXEL
		@ nLin	, 285 SAY oSay34 PROMPT "Coluna" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		IF _tp == 1
			@ nLin+8, 285 MSGET oColuna VAR cColuna		 				SIZE 055, 010 VALID !VAZIO() .AND. u_valacols(_tp, cColuna) OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 F3 "SX5-Z3" PIXEL
		Else
			@ nLin+8, 285 MSGET oColuna VAR cColuna		 				SIZE 055, 010 VALID !VAZIO() .AND. u_valacols(_tp, cColuna) OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 F3 "SX5-Z3" READONLY PIXEL
		Endif
		@ nLin	, 425 SAY oSay36 PROMPT "Pos. IPI/NCM"					SIZE 040, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 425 MSGET oPosIPI VAR cPosIPI	 						SIZE 050, 010 PICTURE "@R 9999.99.99" OF oFolder1:aDialogs[1] VALID U_ExistCod(1,cPosIPI) COLORS 0, 16777215 F3 "SYD" PIXEL
		@ nLin	, 490 SAY oSay37 PROMPT "Data de cadastro"				SIZE 055, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 490 MSGET oDtCad VAR dDtCad 							SIZE 055, 010 PICTURE "@D 99/99/9999" OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
	 
	Endif
 
	oDlg:refresh()

Return

//Carrega a grade da tabela
//------------------------------------------------
Static Function fMSGrade(_tp, cColuna)
//------------------------------------------------
// Monta acols para cadastro da Grade de Produtos
// Tabelas envolvidas: SZD

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZD_COR","ZD_DESCRIC","ZD_COMPOS","ZD_DESCCP2","ZD_DESCLAV","ZD_LAVAGE"} //"ZD_DTLIB","ZD_SITUAC",
	Local aAlterFields := {"ZD_COR","ZD_DESCRIC","ZD_COMPOS","ZD_DESCCP2","ZD_LAVAGE"}
	Static oMSGrade

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZD_COR"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_COR","@!",3,0,,SX3->X3_USADO,"C","","R","",""})
	Endif
	If SX3->(DbSeek("ZD_DESCRIC"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_DESCRIC","@!",20,0,,SX3->X3_USADO,"C","","R","",""})
	Endif
	If SX3->(DbSeek("ZD_COMPOS"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_COMPOS","@!",20,0,,SX3->X3_USADO,"C","","R","",""})
	Endif
	If SX3->(DbSeek("ZD_DESCCP2"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_DESCCP2","@!",20,0,,SX3->X3_USADO,"C","","R","",""})
	Endif

	If alltrim(cColuna) <> ""
      // Busca os tamanhos dispon�veis selecionados na tabela SBV
      
		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)
	  
		If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

	  //Conta total e registros     
		Count To nRecTMPSBV
		If nRecTMPSBV == 0
			oColuna:SetFocus()
		EndIf
 	  
		DbSelectArea("TMPSBV")
		DbGoTop()
 	   	
		While !EOF() .and. TMPSBV->BV_TABELA == cColuna
			DbSelectArea("SX3")
		// Habilita as colunas com os tamanhos no Acols utilizando a tabela SZD at� o tamanho m�ximo de 200
			_campo := "ZD_TAM"+StrZero(len(aFields)-5,3)
			Aadd(aFields,_campo) //TMPSBV->BV_CHAVE)
			Aadd(aAlterFields,_campo)

			SX3->(DbSetOrder(2))
			If SX3->(DbSeek(_campo))
				Aadd(aHeaderEx, {alltrim(TMPSBV->BV_DESCRI)+Space(4),SX3->X3_CAMPO,"@!",1,0,"",SX3->X3_USADO,"C","","R","",""})
			Endif
  	
			DbSelectArea("TMPSBV")
			DbSkip()
		End
		DbClosearea()
	Else
  	// Caso n�o tenha um tamanho cadastrado, habilita um tamanho �nico
		Aadd(aFields,"ZD_TAM001")
		Aadd(aAlterFields,"ZD_TAM001")

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		If SX3->(DbSeek("ZD_TAM001"))
			Aadd(aHeaderEx, {AllTrim("Unico"),SX3->X3_CAMPO,"@!",1,0,"",SX3->X3_USADO,"C","","R","",""})
		Endif
	Endif
	If SX3->(DbSeek("ZD_DESCLAV"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo())+Space(4),"ZD_DESCLAV",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZD_LAVAGE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo())+Space(4),"ZD_LAVAGE",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
  // Define field values
	If _tp = 1
		For nX := 1 to Len(aFields)
			If nX == 1
				Aadd(aFieldFill, Space(3))
			ElseIf nX == 2 .or. nX == 3
				Aadd(aFieldFill, Space(20))
			ElseIf nX == Len(aFields)
				Aadd(aFieldFill, Space(0))
			Else
				Aadd(aFieldFill, Space(1))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
	Else
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+SubString(SB4->B4_COD,1,15))
  		
		While !EOF() .and. SubString(SZD->ZD_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)
  		
			Aadd(aFieldFill, SZD->ZD_COR)
			Aadd(aFieldFill, SZD->ZD_DESCRIC)
			Aadd(aFieldFill, SZD->ZD_COMPOS)
			Aadd(aFieldFill, SZD->ZD_DESCCP2)
			For nX := 6 to (Len(aFields)-1)
				_campo := "SZD->ZD_TAM"+StrZero(nX-5,3)
				Aadd(aFieldFill,&_campo) //TMPSBV->BV_CHAVE)
			Next
			Aadd(aFieldFill, SZD->ZD_DESCLAV)
			Aadd(aFieldFill, SZD->ZD_LAVAGE)
			Aadd(aFieldFill, .F.)
			Aadd(aColsEx, aFieldFill)
			aFieldFill :={}
			DbSkip()
		End
	Endif

	oMSGrade := MsNewGetDados():New( 189, 005, 275, 624, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)
    
Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � hLavage  �Autor  �Daniel R. Melo      � Data � 27/02/2017  ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function hLavage()

	Local oBitmap1
	Local oBitmap2
	Local oBitmap3
	Local oBitmap4
	Local oBitmap5
	Local oBitmap6
	Local oBitmap7
	Local oBitmap8
	Local oBitmap9
	Local oBitmap10
	Local oBitmap11
	Local oBitmap12
	Local oBitmap14
	Local oBitmap15
	Local oBitmap16
	Local oBitmap17
	Local oBitmap18
	Local oBitmap19
	Local oBitmap20
	Local oBitmap21
	Local oBitmap22
	Local oBitmap23
	Local oBitmap24
	Local oBitmap25
	Local oBitmap26
	Local oBitmap27
	Local oBitmap28
	Local oBitmap29
	Local oBitmap30
	Local oBitmap31
	Local oBitmap32
	Local oBitmap33
	Local oBitmap34
	Local oBitmap35
	Local oBitmap36
	Local oBitmap37
	Local oBitmap38
	Local oBitmap39
	Local oBitmap41
	Local oBitmap42
	Local oBitmap43
	Local oBitmap44
	Local oBitmap45
	Local oBitmap46
	Local oBitmap47
	Local oBitmap48
	Local oBitmap49
	Local oBitmap50
	Local oBitmap51
	Local oBitmap52
	Local oBitmap53
	Local oBitmap54
	Local oBitmap55

	Local oCheckBo1
	Local oCheckBo2
	Local oCheckBo3
	Local oCheckBo4
	Local oCheckBo5
	Local oCheckBo6
	Local oCheckBo7
	Local oCheckBo8
	Local oCheckBo9
	Local oCheckBo10
	Local oCheckBo11
	Local oCheckBo12
	Local oCheckBo13
	Local oCheckBo14
	Local oCheckBo15
	Local oCheckBo16
	Local oCheckBo17
	Local oCheckBo18
	Local oCheckBo19
	Local oCheckBo20
	Local oCheckBo21
	Local oCheckBo22
	Local oCheckBo23
	Local oCheckBo24
	Local oCheckBo25
	Local oCheckBo26
	Local oCheckBo27
	Local oCheckBo28
	Local oCheckBo29
	Local oCheckBo30
	Local oCheckBo31
	Local oCheckBo32
	Local oCheckBo33
	Local oCheckBo34
	Local oCheckBo35
	Local oCheckBo36
	Local oCheckBo37
	Local oCheckBo38
	Local oCheckBo39
	Local oCheckBo40
	Local oCheckBo41
	Local oCheckBo42
	Local oCheckBo43
	Local oCheckBo44
	Local oCheckBo45
	Local oCheckBo46
	Local oCheckBo47
	Local oCheckBo48
	Local oCheckBo49
	Local oCheckBo50
	Local oCheckBo51
	Local oCheckBo52
	Local oCheckBo53
	Local oCheckBo54
	Local oCheckBo55

	Local oButton1
	Local oButton2
	Local oButton3
	Local nx

	Private lCheckBo1 := .F.
	Private lCheckBo2 := .F.
	Private lCheckBo3 := .F.
	Private lCheckBo4 := .F.
	Private lCheckBo5 := .F.
	Private lCheckBo6 := .F.
	Private lCheckBo7 := .F.
	Private lCheckBo8 := .F.
	Private lCheckBo9 := .F.
	Private lCheckBo10 := .F.
	Private lCheckBo11 := .F.
	Private lCheckBo12 := .F.
	Private lCheckBo13 := .F.
	Private lCheckBo14 := .F.
	Private lCheckBo15 := .F.
	Private lCheckBo16 := .F.
	Private lCheckBo17 := .F.
	Private lCheckBo18 := .F.
	Private lCheckBo19 := .F.
	Private lCheckBo20 := .F.
	Private lCheckBo21 := .F.
	Private lCheckBo22 := .F.
	Private lCheckBo23 := .F.
	Private lCheckBo24 := .F.
	Private lCheckBo25 := .F.
	Private lCheckBo26 := .F.
	Private lCheckBo27 := .F.
	Private lCheckBo28 := .F.
	Private lCheckBo29 := .F.
	Private lCheckBo30 := .F.
	Private lCheckBo31 := .F.
	Private lCheckBo32 := .F.
	Private lCheckBo33 := .F.
	Private lCheckBo34 := .F.
	Private lCheckBo35 := .F.
	Private lCheckBo36 := .F.
	Private lCheckBo37 := .F.
	Private lCheckBo38 := .F.
	Private lCheckBo39 := .F.
	Private lCheckBo40 := .F.
	Private lCheckBo41 := .F.
	Private lCheckBo42 := .F.
	Private lCheckBo43 := .F.
	Private lCheckBo44 := .F.
	Private lCheckBo45 := .F.
	Private lCheckBo46 := .F.
	Private lCheckBo47 := .F.
	Private lCheckBo48 := .F.
	Private lCheckBo49 := .F.
	Private lCheckBo50 := .F.
	Private lCheckBo51 := .F.
	Private lCheckBo52 := .F.
	Private lCheckBo53 := .F.
	Private lCheckBo54 := .F.
	Private lCheckBo55 := .F.

	Private oSay1
	Private oSay2
	Private oSay3
	Private oSay4
	Private oSay5
	Private oSay6
	Private oSay7
	Private oSay8
	Private oSay9
	Private oSay10
	Private oSay11
	Private oSay12
	Private oSay13
	Private oSay14
	Private oSay15
	Private oSay16
	Private oSay17
	Private oSay18
	Private oSay19
	Private oSay20
	Private oSay21
	Private oSay22
	Private oSay23
	Private oSay24
	Private oSay25
	Private oSay26
	Private oSay27
	Private oSay28
	Private oSay29
	Private oSay30
	Private oSay31
	Private oSay32
	Private oSay33
	Private oSay34
	Private oSay35
	Private oSay36
	Private oSay37
	Private oSay38
	Private oSay39
	Private oSay40
	Private oSay41
	Private oSay42
	Private oSay43
	Private oSay44
	Private oSay45
	Private oSay46
	Private oSay47
	Private oSay48
	Private oSay49
	Private oSay50
	Private oSay51
	Private oSay52
	Private oSay53
	Private oSay54
	Private oSay55

	Private MvParCod := Alltrim(oMSGrade:aCols[oMSGrade:nat,len(OMSGrade:aHeader)])+","   // Carrega Nome da Variavel do Get em Questao
	Private MvParTxt := ""

	If MvParCod <> ","
		For nx := 1 to len(MvParCod) step 3
			cAux2 	:= SubStr(MvParCod,nx,2)
			cCheck		:= "lCheckBo"+cValToChar(Val(cAux2))
			&cCheck		:= .T.
		Next
	Else
		MvParCod := ""
	End

	Static oDlgLav

	DEFINE FONT oFont1 NAME "Arial" SIZE 0,12

	DEFINE MSDIALOG oDlgLav TITLE "New Dialog" FROM 000, 000  TO 520, 1300 COLORS 0, 16777215 PIXEL

	nCol := 7
	@ 007, nCol BITMAP oBitmap1  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\01.jpg" NOBORDER PIXEL
	@ 030, nCol BITMAP oBitmap2  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\02.jpg" NOBORDER PIXEL
	@ 052, nCol BITMAP oBitmap3  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\03.jpg" NOBORDER PIXEL
	@ 075, nCol BITMAP oBitmap4  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\04.jpg" NOBORDER PIXEL
	@ 097, nCol BITMAP oBitmap5  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\05.jpg" NOBORDER PIXEL
	@ 120, nCol BITMAP oBitmap6  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\06.jpg" NOBORDER PIXEL
	@ 142, nCol BITMAP oBitmap7  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\07.jpg" NOBORDER PIXEL
	@ 165, nCol BITMAP oBitmap8  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\08.jpg" NOBORDER PIXEL
	@ 187, nCol BITMAP oBitmap9  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\09.jpg" NOBORDER PIXEL
	@ 210, nCol BITMAP oBitmap10 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\10.jpg" NOBORDER PIXEL
	@ 232, nCol BITMAP oBitmap11 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\11.jpg" NOBORDER PIXEL
	nCol := nCol+22
	@ 007, nCol CHECKBOX oCheckBo1  VAR lCheckBo1  PROMPT "" SIZE 010, 008 VALID SelCheck(1)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol CHECKBOX oCheckBo2  VAR lCheckBo2  PROMPT "" SIZE 010, 008 VALID SelCheck(2)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol CHECKBOX oCheckBo3  VAR lCheckBo3  PROMPT "" SIZE 010, 008 VALID SelCheck(3)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol CHECKBOX oCheckBo4  VAR lCheckBo4  PROMPT "" SIZE 010, 008 VALID SelCheck(4)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol CHECKBOX oCheckBo5  VAR lCheckBo5  PROMPT "" SIZE 010, 008 VALID SelCheck(5)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol CHECKBOX oCheckBo6  VAR lCheckBo6  PROMPT "" SIZE 010, 008 VALID SelCheck(6)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol CHECKBOX oCheckBo7  VAR lCheckBo7  PROMPT "" SIZE 010, 008 VALID SelCheck(7)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol CHECKBOX oCheckBo8  VAR lCheckBo8  PROMPT "" SIZE 010, 008 VALID SelCheck(8)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol CHECKBOX oCheckBo9  VAR lCheckBo9  PROMPT "" SIZE 010, 008 VALID SelCheck(9)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol CHECKBOX oCheckBo10 VAR lCheckBo10 PROMPT "" SIZE 010, 008 VALID SelCheck(10) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 232, nCol CHECKBOX oCheckBo11 VAR lCheckBo11 PROMPT "" SIZE 010, 008 VALID SelCheck(11) OF oDlgLav COLORS 0, 16777215 PIXEL
	nCol := nCol+11
	@ 007, nCol SAY oSay1  PROMPT "LAVAGEM NORMAL" 						   					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol SAY oSay2  PROMPT "TEMP.MAXIMA 95�C - TRATATAMENTO BRANDO" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol SAY oSay3  PROMPT "TEMP.MAXIMA 90�C - NORMAL" 			  	  				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol SAY oSay4  PROMPT "TEMP.MAXIMA 90�C - TRATATAMENTO BRANDO" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol SAY oSay5  PROMPT "TEMP.MAXIMA 70�C - NORMAL" 								FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol SAY oSay6  PROMPT "TEMP.MAXIMA 60�C - NORMAL" 								FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol SAY oSay7  PROMPT "TEMP.MAXIMA 60�C - TRATATAMENTO BRANDO" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol SAY oSay8  PROMPT "TEMP.MAXIMA 50�C - TRATATAMENTO BRANDO" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol SAY oSay9  PROMPT "TEMP.MAXIMA 40�C - NORMAL" 								FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol SAY oSay10 PROMPT "TEMP.MAXIMA 40�C - TRATATAMENTO BRANDO" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 232, nCol SAY oSay11 PROMPT "TEMP.MAXIMA 40�C - TRATATAMENTO MUITO BRANDO" 			FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL

	nCol := nCol+95
	@ 007, nCol BITMAP oBitmap12 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\12.jpg" NOBORDER PIXEL
	@ 030, nCol BITMAP oBitmap13 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\13.jpg" NOBORDER PIXEL
	@ 052, nCol BITMAP oBitmap14 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\14.jpg" NOBORDER PIXEL
	@ 075, nCol BITMAP oBitmap15 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\15.jpg" NOBORDER PIXEL
	@ 097, nCol BITMAP oBitmap16 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\16.jpg" NOBORDER PIXEL
	@ 120, nCol BITMAP oBitmap17 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\17.jpg" NOBORDER PIXEL
	@ 142, nCol BITMAP oBitmap18 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\18.jpg" NOBORDER PIXEL
	@ 165, nCol BITMAP oBitmap19 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\19.jpg" NOBORDER PIXEL
	@ 187, nCol BITMAP oBitmap20 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\20.jpg" NOBORDER PIXEL
	@ 210, nCol BITMAP oBitmap21 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\21.jpg" NOBORDER PIXEL
	@ 232, nCol BITMAP oBitmap22 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\22.jpg" NOBORDER PIXEL
	nCol := nCol+22
	@ 007, nCol CHECKBOX oCheckBo12 VAR lCheckBo12 PROMPT "" SIZE 010, 008 VALID SelCheck(12) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol CHECKBOX oCheckBo13 VAR lCheckBo13 PROMPT "" SIZE 010, 008 VALID SelCheck(13) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol CHECKBOX oCheckBo14 VAR lCheckBo14 PROMPT "" SIZE 010, 008 VALID SelCheck(14) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol CHECKBOX oCheckBo15 VAR lCheckBo15 PROMPT "" SIZE 010, 008 VALID SelCheck(15) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol CHECKBOX oCheckBo16 VAR lCheckBo16 PROMPT "" SIZE 010, 008 VALID SelCheck(16) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol CHECKBOX oCheckBo17 VAR lCheckBo17 PROMPT "" SIZE 010, 008 VALID SelCheck(17) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol CHECKBOX oCheckBo18 VAR lCheckBo18 PROMPT "" SIZE 010, 008 VALID SelCheck(18) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol CHECKBOX oCheckBo19 VAR lCheckBo19 PROMPT "" SIZE 010, 008 VALID SelCheck(19) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol CHECKBOX oCheckBo20 VAR lCheckBo20 PROMPT "" SIZE 010, 008 VALID SelCheck(20) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol CHECKBOX oCheckBo21 VAR lCheckBo21 PROMPT "" SIZE 010, 008 VALID SelCheck(21) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 232, nCol CHECKBOX oCheckBo22 VAR lCheckBo22 PROMPT "" SIZE 010, 008 VALID SelCheck(22) OF oDlgLav COLORS 0, 16777215 PIXEL
	nCol := nCol+11
	@ 007, nCol SAY oSay12 PROMPT "TEMP.MAXIMA 30�C - PROCESSO SUAVE" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol SAY oSay13 PROMPT "SOMENTE LAVAGEM MANUAL" 									FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol SAY oSay14 PROMPT "NAO LAVAR" 												FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol SAY oSay15 PROMPT "ALVEJAMENTO A BASE DE CLORO" 						   	FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol SAY oSay16 PROMPT "PERMITIDO ALVEJAMENTO COM CLORO DILUIDO"	 				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol SAY oSay17 PROMPT "NAO ALVEJAR" 											FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol SAY oSay18 PROMPT "SECAGEM" 												FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol SAY oSay19 PROMPT "SECAGEM NA HORIZONTAL A SOMBRA" 							FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol SAY oSay20 PROMPT "PERMITIDO TAMBOR ROTATIVO COM TEMP.MAXIMA" 				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol SAY oSay21 PROMPT "PERMITIDO TAMBOR ROTATIVO COM TEMP.MINIMA"	 			FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 232, nCol SAY oSay22 PROMPT "NAO SECAR EM TAMBOR" 									FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL

	nCol := nCol+95
	@ 007, nCol BITMAP oBitmap23 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\23.jpg" NOBORDER PIXEL
	@ 030, nCol BITMAP oBitmap24 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\24.jpg" NOBORDER PIXEL
	@ 052, nCol BITMAP oBitmap25 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\25.jpg" NOBORDER PIXEL
	@ 075, nCol BITMAP oBitmap26 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\26.jpg" NOBORDER PIXEL
	@ 097, nCol BITMAP oBitmap27 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\27.jpg" NOBORDER PIXEL
	@ 120, nCol BITMAP oBitmap28 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\28.jpg" NOBORDER PIXEL
	@ 142, nCol BITMAP oBitmap29 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\29.jpg" NOBORDER PIXEL
	@ 165, nCol BITMAP oBitmap30 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\30.jpg" NOBORDER PIXEL
	@ 187, nCol BITMAP oBitmap31 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\31.jpg" NOBORDER PIXEL
	@ 210, nCol BITMAP oBitmap32 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\32.jpg" NOBORDER PIXEL
//    @ 232, nCol BITMAP oBitmap33 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\??.jpg" NOBORDER PIXEL
	nCol := nCol+22
	@ 007, nCol CHECKBOX oCheckBo23 VAR lCheckBo23 PROMPT "" SIZE 010, 008 VALID SelCheck(23) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol CHECKBOX oCheckBo24 VAR lCheckBo24 PROMPT "" SIZE 010, 008 VALID SelCheck(24) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol CHECKBOX oCheckBo25 VAR lCheckBo25 PROMPT "" SIZE 010, 008 VALID SelCheck(25) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol CHECKBOX oCheckBo26 VAR lCheckBo26 PROMPT "" SIZE 010, 008 VALID SelCheck(26) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol CHECKBOX oCheckBo27 VAR lCheckBo27 PROMPT "" SIZE 010, 008 VALID SelCheck(27) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol CHECKBOX oCheckBo28 VAR lCheckBo28 PROMPT "" SIZE 010, 008 VALID SelCheck(28) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol CHECKBOX oCheckBo29 VAR lCheckBo29 PROMPT "" SIZE 010, 008 VALID SelCheck(29) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol CHECKBOX oCheckBo30 VAR lCheckBo30 PROMPT "" SIZE 010, 008 VALID SelCheck(30) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol CHECKBOX oCheckBo31 VAR lCheckBo31 PROMPT "" SIZE 010, 008 VALID SelCheck(31) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol CHECKBOX oCheckBo32 VAR lCheckBo32 PROMPT "" SIZE 010, 008 VALID SelCheck(32) OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 232, nCol CHECKBOX oCheckBo33 VAR lCheckBo33 PROMPT "" SIZE 010, 008 VALID SelCheck(33) OF oDlgLav COLORS 0, 16777215 PIXEL
	nCol := nCol+11
	@ 007, nCol SAY oSay23 PROMPT "PERMITIDO SECAR NA VERTICAL" 							FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol SAY oSay24 PROMPT "PERMITIDO NA VERTICAL SEM TORCER" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol SAY oSay25 PROMPT "SECAGEM NA HORIZONTAL" 									FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol SAY oSay26 PROMPT "PASSADORIA A FERRO" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol SAY oSay27 PROMPT "CHAPA DA BASE DO FERRO ATE 200�C" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol SAY oSay28 PROMPT "CHAPA DA BASE DO FERRO ATE 150�C" 	 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol SAY oSay29 PROMPT "TEMPERATURA MAXIMA BASE DO FERRO 110�C SEM VAPOR" 		FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol SAY oSay30 PROMPT "NAO PASSAR OU UTILIZAR VAPORIZACAO" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol SAY oSay31 PROMPT "LIMPEZA A SECO" 											FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol SAY oSay32 PROMPT "LIMPEZA A SECO COM TODOS OS SOLVENTES" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 232, nCol SAY oSay33 PROMPT "" 														FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL

	nCol := nCol+95
	@ 007, nCol BITMAP oBitmap34 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\33.jpg" NOBORDER PIXEL
	@ 030, nCol BITMAP oBitmap35 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\34.jpg" NOBORDER PIXEL
	@ 052, nCol BITMAP oBitmap36 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\35.jpg" NOBORDER PIXEL
	@ 075, nCol BITMAP oBitmap37 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\36.jpg" NOBORDER PIXEL
	@ 097, nCol BITMAP oBitmap38 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\37.jpg" NOBORDER PIXEL
	@ 120, nCol BITMAP oBitmap39 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\38.jpg" NOBORDER PIXEL
	@ 142, nCol BITMAP oBitmap40 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\39.jpg" NOBORDER PIXEL
	@ 165, nCol BITMAP oBitmap41 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\40.jpg" NOBORDER PIXEL
	@ 187, nCol BITMAP oBitmap42 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\41.jpg" NOBORDER PIXEL
	@ 210, nCol BITMAP oBitmap43 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\42.jpg" NOBORDER PIXEL
//    @ 232, nCol BITMAP oBitmap44 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\??.jpg" NOBORDER PIXEL
	nCol := nCol+22
	@ 007, nCol CHECKBOX oCheckBo34 VAR lCheckBo34 PROMPT "" SIZE 010, 008 VALID SelCheck(34) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol CHECKBOX oCheckBo35 VAR lCheckBo35 PROMPT "" SIZE 010, 008 VALID SelCheck(35) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol CHECKBOX oCheckBo36 VAR lCheckBo36 PROMPT "" SIZE 010, 008 VALID SelCheck(36) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol CHECKBOX oCheckBo37 VAR lCheckBo37 PROMPT "" SIZE 010, 008 VALID SelCheck(37) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol CHECKBOX oCheckBo38 VAR lCheckBo38 PROMPT "" SIZE 010, 008 VALID SelCheck(38) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol CHECKBOX oCheckBo39 VAR lCheckBo39 PROMPT "" SIZE 010, 008 VALID SelCheck(39) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol CHECKBOX oCheckBo40 VAR lCheckBo40 PROMPT "" SIZE 010, 008 VALID SelCheck(40) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol CHECKBOX oCheckBo41 VAR lCheckBo41 PROMPT "" SIZE 010, 008 VALID SelCheck(41) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol CHECKBOX oCheckBo42 VAR lCheckBo42 PROMPT "" SIZE 010, 008 VALID SelCheck(42) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol CHECKBOX oCheckBo43 VAR lCheckBo43 PROMPT "" SIZE 010, 008 VALID SelCheck(43) OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 232, nCol CHECKBOX oCheckBo44 VAR lCheckBo44 PROMPT "" SIZE 010, 008 VALID SelCheck(44) OF oDlgLav COLORS 0, 16777215 PIXEL
	nCol := nCol+11
	@ 007, nCol SAY oSay34 PROMPT "LIMPEZA A SECO COM TRICLOROETILENO" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol SAY oSay35 PROMPT "LIMPEZA A SECO TRICLOROETILENO SEM AGUA/ACAO MECANICA" 	FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol SAY oSay36 PROMPT "LIMPEZA A SECO COM TRIFLUORTRICLOROETANO" 				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol SAY oSay37 PROMPT "LIMPEZA A SECO COM TRIFLUORTRICLOROETANO SEM AGUA" 		FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol SAY oSay38 PROMPT "NAO LIMPAR A SECO" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol SAY oSay39 PROMPT "NAO PERMITIDO" 											FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol SAY oSay40 PROMPT "TRATAMENTO BRANDO" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol SAY oSay41 PROMPT "TRATAMENTO MUITO BRANDO" 								FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol SAY oSay42 PROMPT "LAVAGEM A MAO" 											FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol SAY oSay43 PROMPT "SECAGEM EM VARAL" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 232, nCol SAY oSay44 PROMPT "" 														FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL

	nCol := nCol+95
	@ 007, nCol BITMAP oBitmap45 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\43.jpg" NOBORDER PIXEL
	@ 030, nCol BITMAP oBitmap46 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\44.jpg" NOBORDER PIXEL
	@ 052, nCol BITMAP oBitmap47 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\45.jpg" NOBORDER PIXEL
	@ 075, nCol BITMAP oBitmap48 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\46.jpg" NOBORDER PIXEL
	@ 097, nCol BITMAP oBitmap49 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\47.jpg" NOBORDER PIXEL
	@ 120, nCol BITMAP oBitmap50 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\48.jpg" NOBORDER PIXEL
	@ 142, nCol BITMAP oBitmap51 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\49.jpg" NOBORDER PIXEL
	@ 165, nCol BITMAP oBitmap52 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\50.jpg" NOBORDER PIXEL
	@ 187, nCol BITMAP oBitmap53 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\51.jpg" NOBORDER PIXEL
	@ 210, nCol BITMAP oBitmap54 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\52.jpg" NOBORDER PIXEL
//    @ 232, nCol BITMAP oBitmap55 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\??.jpg" NOBORDER PIXEL
	nCol := nCol+22
	@ 007, nCol CHECKBOX oCheckBo45 VAR lCheckBo45 PROMPT "" SIZE 010, 008 VALID SelCheck(45) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol CHECKBOX oCheckBo46 VAR lCheckBo46 PROMPT "" SIZE 010, 008 VALID SelCheck(46) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol CHECKBOX oCheckBo47 VAR lCheckBo47 PROMPT "" SIZE 010, 008 VALID SelCheck(47) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol CHECKBOX oCheckBo48 VAR lCheckBo48 PROMPT "" SIZE 010, 008 VALID SelCheck(48) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol CHECKBOX oCheckBo49 VAR lCheckBo49 PROMPT "" SIZE 010, 008 VALID SelCheck(49) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol CHECKBOX oCheckBo50 VAR lCheckBo50 PROMPT "" SIZE 010, 008 VALID SelCheck(50) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol CHECKBOX oCheckBo51 VAR lCheckBo51 PROMPT "" SIZE 010, 008 VALID SelCheck(51) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol CHECKBOX oCheckBo52 VAR lCheckBo52 PROMPT "" SIZE 010, 008 VALID SelCheck(52) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol CHECKBOX oCheckBo53 VAR lCheckBo53 PROMPT "" SIZE 010, 008 VALID SelCheck(53) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol CHECKBOX oCheckBo54 VAR lCheckBo54 PROMPT "" SIZE 010, 008 VALID SelCheck(54) OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 210, nCol CHECKBOX oCheckBo55 VAR lCheckBo55 PROMPT "" SIZE 010, 008 OVALID SelCheck(55) OFF oDlgLav COLORS 0, 16777215 PIXEL
	nCol := nCol+11
	@ 007, nCol SAY oSay45 PROMPT "SECAGEM EM VARAL POR GOTEJAMENTO" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol SAY oSay46 PROMPT "SECAGEM A SOMBRA" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol SAY oSay47 PROMPT "SECAGEM NA HORIZONTAL" 									FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol SAY oSay48 PROMPT "LIMPEZA A UMIDO PRO-NORMAL" 								FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol SAY oSay49 PROMPT "LIMPEZA A UMIDO PRO-SUAVE" 						   		FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol SAY oSay50 PROMPT "LIMPEZA A UMIDO PRO-MUITO SUAVE" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol SAY oSay51 PROMPT "NAO LIMPAR A UMIDO" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol SAY oSay52 PROMPT "SECAGEM EM VARAL POR GOTEJAMENTO A SOMBRA" 				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol SAY oSay53 PROMPT "PERMITIDO ALVEJAMENTO SOMENTE COM OXIGENIO" 				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol SAY oSay54 PROMPT "SECAR PENDURADO, A SOMBRA E SEM TORCER" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 210, nCol SAY oSay55 PROMPT "" 														FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL

	@ 240, 496 BUTTON oButton3 PROMPT "Limpar" 		SIZE 043, 012 ACTION LimpaLav() OF oDlgLav PIXEL
	@ 240, 548 BUTTON oButton2 PROMPT "Cancelar" 	SIZE 043, 012 ACTION Close(oDlgLav) OF oDlgLav PIXEL
	@ 240, 599 BUTTON oButton1 PROMPT "Gravar" 		SIZE 043, 012 ACTION SalvaLav() OF oDlgLav PIXEL

	ACTIVATE MSDIALOG oDlgLav CENTERED

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � LimpaLav �Autor  �Daniel R. Melo      � Data � 27/02/2017  ���
�������������������������������������������������������������������������͹��
���Desc.     � Limpar marcados.	                                          ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function LimpaLav()

	Local nX

	For nX := 1 to 55
		cCheck		:= "lCheckBo"+cValToChar(nX)
		&cCheck		:= .F.
	Next
	MvParCod	:= ""		//CODIGO
	MvParTxt	:= ""		//TEXTO
	oDlgLav:Refresh()

Return()

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � SelCheck �Autor  �Daniel R. Melo      � Data � 27/02/2017  ���
�������������������������������������������������������������������������͹��
���Desc.     � SELECIONA CHECKBOX E GRAVA NO MULTIGET                     ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function SelCheck(nNum)

	cCheck	:= "lCheckBo"+cValToChar(nNum)
	cSay	:= "oSay"+cValToChar(nNum)

	If &cCheck == .T.

		MvParCod	:= MvParCod+StrZero(nNum,2)+","		//CODIGO

	end

	If &cCheck == .F. .and. AT(StrZero(nNum,2),MvParCod)>0
	
		MvParCod  := StrTran(MvParCod,StrZero(nNum,2)+",","")

	end

	oDlgLav:Refresh()

Return()

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � SalvaLav �Autor  �Daniel R. Melo      � Data � 27/02/2017  ���
�������������������������������������������������������������������������͹��
���Desc.     � SALVA O PROCESSO DE LAVAGEM                                ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function SalvaLav()

	Local nx

	MvParTxt2 := ""

	If MvParCod <> ""

		For nx := 1 to len(MvParCod) step 3
			cAux2 	:= SubStr(MvParCod,nx,2)
			cSay2	:= "oSay"+cValToChar(Val(cAux2))
			MvParTxt2	:= MvParTxt2+&cSay2:CCAPTION+" |"
		Next
	    
		MvParCod	:= SubStr(MvParCod,1,len(MvParCod)-1)	//CODIGO
		MvParTxt	:= SubStr(MvParTxt2,1,len(MvParTxt2)-2)	//TEXTO

	ElseIf MvParCod == ""
		Alert("Processo de Lavagem em Branco!")

		MvParCod	:= ""
		MvParTxt	:= ""

	EndIf

	oMSGrade:aCols[oMSGrade:nat,len(OMSGrade:aHeader)]	:= MvParCod
	oMSGrade:aCols[oMSGrade:nat,len(OMSGrade:aHeader)-1]	:= MvParTxt
	Close(oDlgLav)
	oMsGrade:refresh()

Return()

Return()


Static Function ReplyLn(_tp)
// Rotina para replica��o das linhas da grade para outras cores escolhidas

	Local nX
	Local nX2

	IF _tp = 2 .OR. _tp = 1
		If MsgYesNo("Ser�o replicadas as informa��es da Cor: "+oMSGrade:aCols[oMSGrade:nat,1]+" para as outras cores?")
			For nX := 1 to len(oMSGrade:aCols)
				for nX2 := 1 to len(oMSGrade:aCols[oMSGrade:nat])-1
					If nX <> oMSGrade:nat .and. !AllTrim(Str(nX2)) $ "1|2|" .and. alltrim(oMSGrade:aCols[nX,nX2])==""
						oMSGrade:aCols[nX,nX2] := oMSGrade:aCols[oMSGrade:nat,nX2]
					Endif

				Next nX2
			Next nX
		End
	End

	oMsGrade:refresh()

Return


//------------------------------------------------
Static Function fMSMix(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de Mix para regra de aglutina��o de Ordem de Produ��o
// Tabela criada: SZH

	Local nX
	Local aHeaderEx		:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {}
	Local aAlterFields	:= {}
	Local cAchou			:= .F.
	Local _i
	Static oMSGrade

  // Define field properties
	If alltrim(cColuna) <> ""
		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)
		If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
		DbSelectArea("TMPSBV")

		cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" (nolock) WHERE D_E_L_E_T_ = ' ' AND ZD_FILIAL = '"+xFilial("SZD")+"' AND ZD_PRODUTO='"+SubString(SB4->B4_COD,1,15)+"'"

		If Select("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)
		DbSelectArea("TMPSBV")
  	
//  	While !EOF() .and. TMPSBV->BV_TABELA == cColuna
		DbSelectArea("TMPSZD")
		TMPSZD->(DbGoTop())
		While TMPSZD->(!EOF())
			For _i := 1 to 200
				If &("TMPSZD->ZD_TAM"+StrZero(_i,3))=="X"
					_campo		:= "ZH_TAM"+StrZero(_i,3)
					_desc := rettam(_i,cColuna,"D")
					DbSelectArea("SX3")
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo))
						nX := ASCAN( aFields, {|x| alltrim(x) == alltrim(_campo)})
						If nX = 0
							Aadd(aFields,_campo) //TMPSBV->BV_CHAVE)
							Aadd(aAlterFields,_campo)

							Aadd(aHeaderEx, {Space(5)+ alltrim(_desc)+ Space(5),SX3->X3_CAMPO,"@E 99",2,0,"",SX3->X3_USADO,"N","","R","",""})
						Endif
				
					Endif
				Endif
			Next
			TMPSZD->(DbSkip())
		EndDo
  	
		aSort(aFields)
		aSort(aAlterFields)
		ASORT(aHeaderEx, , , { |x,y| x[2] < y[2] } )
		  	
//  		DbSelectArea("TMPSBV")
//  		DbSkip()
//  	End
//  	DbClosearea()

	Else
		Aadd(aFields,"ZH_TAM001")
		Aadd(aAlterFields,"ZH_TAM001")

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		If SX3->(DbSeek("ZH_TAM001"))
			Aadd(aHeaderEx, {AllTrim("Unico"),SX3->X3_CAMPO,"@E 99",2,0,"",SX3->X3_USADO,"N","","R","",""})
		Endif

	Endif

	If _tp = 8 	//Inclusao
		DbSelectArea("SZH")
		DbSetOrder(1)
		DbSeek(xfilial("SZH")+SubString(SB4->B4_COD,1,15))
	  
		While !EOF() .and. SubString(SZH->ZH_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)
	  
		  // Define field values
			If !Found()
				For nX := 1 to Len(aHeaderEx)
					DbSelectArea("SX3")
					SX3->(DbSetOrder(2))
					If DbSeek(aFields[nX])
						Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
					End
				Next nX
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill :={}
		  
			ElseIf Found()
				Alert("Favor clicar em alterar, o produto em quest�o j� tem MIX cadastrado!")
				TMPSZD->(DbCloseArea())
				Close(oDlg)
				Return()
			EndIf
			DbSkip()
		EndDo
	EndIf

	If _tp = 9 	//Altera��o
		DbSelectArea("SZH")
		DbSetOrder(1)
		DbSeek(xfilial("SZH")+SubString(SB4->B4_COD,1,15))

		While !EOF() .and. SubString(SZH->ZH_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)
	  		
			If Found()
				For nX := 1 to Len(aHeaderEx)
					_campo := aHeaderEx[Nx,2]//"SZH->ZH_TAM"+StrZero(nX,3)
					Aadd(aFieldFill,&_campo) //TMPSBV->BV_CHAVE)
				Next
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill	:={}
				cAchou		:= .T.
			Endif
			DbSkip()
		EndDo
            	
		If !Found() .and. cAchou == .F.
			Alert("Favor clicar em incluir, o produto em quest�o n�o tem MIX cadastrado!")
			TMPSZD->(DbCloseArea())
			Close(oDlg)
			Return()
		Endif
	EndIf

	oMSGrade := MsNewGetDados():New( 189, 005, 275, 624, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

	TMPSZD->(DbCloseArea())
    
Return


//------------------------------------------------
Static Function fMSFicha(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de Ficha T�cnica
// Tabela criada: SZN

	Local nX
	Local aHeaderEx		:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {}
	Local aAlterFields	:= {}
	Local cAchou			:= .F.
	Static oMSGrade

  // Define field properties
	Aadd(aFields,"ZN_PARTES") //TMPSBV->BV_CHAVE)
	Aadd(aAlterFields,"ZN_PARTES")

	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZN_PARTES"))
		Aadd(aHeaderEx, {alltrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO-70,SX3->X3_DECIMAL,"",SX3->X3_USADO,"C","","R","",""})
	Endif

	If alltrim(cColuna) <> ""
		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)
		If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
		DbSelectArea("TMPSBV")

		cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" (nolock) WHERE D_E_L_E_T_ = ' ' AND ZD_FILIAL = '"+xFilial("SZD")+"' AND ZD_PRODUTO='"+SubString(SB4->B4_COD,1,15)+"'"
		If Select("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)
		DbSelectArea("TMPSZD")
  	
		While !EOF() .and. TMPSBV->BV_TABELA == cColuna
			DbSelectArea("SX3")
			_campo		:= "ZN_TAM"+StrZero(len(aFields),3)
			cZdVerif	:= 0
		
			Aadd(aFields,_campo) //TMPSBV->BV_CHAVE)
			Aadd(aAlterFields,_campo)

			TMPSZD->(DbGoTop())

			While TMPSZD->(!EOF())
				If &("TMPSZD->ZD_TAM"+StrZero(len(aFields)-1,3))=="X"
					cZdVerif += 1
				
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo)) .and. cZdVerif==1
						Aadd(aHeaderEx, {alltrim(TMPSBV->BV_DESCRI),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO-40,SX3->X3_DECIMAL,"",SX3->X3_USADO,"C","","R","",""})
					Endif
				Endif
	 		
				TMPSZD->(DbSkip())
			EndDo
  	

			DbSelectArea("TMPSBV")
			DbSkip()
		End
		TMPSZD->(DbCloseArea())

		DbClosearea()

	Else
		Aadd(aFields,"ZN_TAM001")
		Aadd(aAlterFields,"ZN_TAM001")

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		If SX3->(DbSeek("ZN_TAM001"))
			Aadd(aHeaderEx, {AllTrim("Unico"),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,"N","","R","",""})
		Endif

	Endif

	If _tp = 11 	//Inclusao
		DbSelectArea("SZN")
		DbSetOrder(1)
		DbSeek(xfilial("SZN")+PADR(SB4->B4_COD,15))
	  
		If Found()
			Alert("Favor clicar em alterar, o produto em quest�o j� tem Ficha cadastrada!")
			Close(oDlg)
			Return()
		Endif
	  
		For nX := 1 to Len(aHeaderEx)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			End
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		  
	EndIf

	If _tp = 12 	//Altera��o
		DbSelectArea("SZN")
		DbSetOrder(1)
		DbSeek(xfilial("SZN")+PADR(SB4->B4_COD,15))

		If !Found()
			Alert("Favor clicar em incluir, o produto em quest�o n�o tem Ficha cadastrado!")
			Close(oDlg)
			Return()
		Else
			While !EOF() .and. SubString(SZN->ZN_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)
	  		
				For nX := 1 to Len(aHeaderEx)
					_campo := "SZN->"+aHeaderEx[nX,2]
					Aadd(aFieldFill,&_campo) //TMPSBV->BV_CHAVE)
				Next
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill	:={}
				DbSkip()
			EndDo
		Endif
	EndIf

	oMSGrade := MsNewGetDados():New( 189, 005, 275, 624, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return


//------------------------------------------------
Static Function fMSFich1(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de Ficha T�cnica
// Tabela criada: SZN

	Local nX
	Local aHeaderEx		:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {"ZO_TEMPO","ZO_DESCRI","ZO_OBS","ZO_OBSMAQ"}
	Local aAlterFields 		:= {"ZO_TEMPO","ZO_DESCRI","ZO_OBS","ZO_OBSMAQ"}
	Local cAchou			:= .F.
//	Local _nTpTotal			:= 0
	Static oMSGrade


	SX3->(DbSetOrder(2))
	For nX := 1 to len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			If aFields[nX] = "ZO_DESCRI"
				Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(5),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"ZAO","R","",""})
			ElseIf aFields[nX] = "ZO_OBSMAQ"
				Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(5),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"ZAP","R","",""})
			Else
				Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(5),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"","R","",""})
			EndIf
		Endif
	Next

	If _tp = 13 	//Inclusao
		DbSelectArea("SZO")
		DbSetOrder(1)
		DbSeek(xfilial("SZO")+PADR(SB4->B4_COD,15))
	  
		If Found()
			Alert("Favor clicar em alterar, o produto em quest�o j� tem Ficha cadastrada!")
			Close(oDlg)
			Return()
		Endif
	  
		For nX := 1 to Len(aHeaderEx)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			End
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		  
	EndIf

	If _tp = 14 	//Altera��o
		DbSelectArea("SZO")
		DbSetOrder(1)
		DbSeek(xfilial("SZO")+PADR(SB4->B4_COD,15))

		If !Found()
			Alert("Favor clicar em incluir, o produto em quest�o n�o tem Ficha cadastrado!")
			Close(oDlg)
			Return()
		Else
			While !EOF() .and. SubString(SZO->ZO_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)
	  		
				Aadd(aFieldFill,SZO->ZO_TEMPO)
				Aadd(aFieldFill,SZO->ZO_DESCRI)
				Aadd(aFieldFill,SZO->ZO_OBS)
				Aadd(aFieldFill,SZO->ZO_OBSMAQ)
				Aadd(aFieldFill,SZO->(RECNO()))
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill	:={}
				DbSkip()
			EndDo
		Endif
	EndIf

	oMSGrade := MsNewGetDados():New( 189, 005, 275, 624, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

	oMSGrade:bchange	:= {|| U_AtuTempo() }
	
Return


User Function AtuTempo()
	
	Local _i
	cTempo		:= 0
	
	For _i := 1 to len(oMSGrade:aCols)
		
		If len(oMSGrade:aCols[_i]) == 6
			If oMSGrade:aCols[_i,1] <> 0 .and. !oMSGrade:aCols[_i,6]
				cTempo	+= oMSGrade:aCols[_i,1]
			Endif
		Else
			If oMSGrade:aCols[_i,1] <> 0 .and. !oMSGrade:aCols[_i,5]
				cTempo	+= oMSGrade:aCols[_i,1]
			Endif
		End

	Next

	oMSGrade:Refresh()
	oTempo:refresh()
	oDlg:refresh()

Return

//------------------------------------------------
Static Function fMSOPC(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de Ficha T�cnica
// Tabela criada: SZN

	Local nX
	Local aHeaderEx		:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {}
	Local aAlterFields	:= {}
	Local cAchou			:= .F.
	Static oMSOPC

  // Define field properties
	aFields := {"ZAU_COR","ZAU_PRODUT","ZAU_GRUPO","ZAU_ITEM"}
	aAlterFields := {"ZAU_COR","ZAU_PRODUT","ZAU_GRUPO","ZAU_ITEM"}

	SX3->(DbSetOrder(2))
	For nX := 1 to len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(5),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"","R","",""})
		Endif
	Next

	If _tp = 15 	//Inclusao
		DbSelectArea("ZAU")
		DbSetOrder(1)
		DbSeek(xfilial("ZAU")+LEFT(SB4->B4_COD,8))
	  
		If Found()
			Alert("Favor clicar em alterar, o produto em quest�o j� tem Opcionais!")
			Close(oDlg)
			Return()
		Endif
	  
		For nX := 1 to Len(aHeaderEx)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			End
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		  
	EndIf

	If _tp = 16 	//Altera��o
		DbSelectArea("ZAU")
		DbSetOrder(1)
		DbSeek(xfilial("ZAU")+left(SB4->B4_COD,8))

		If !Found()
			Alert("Favor clicar em incluir, o produto em quest�o n�o tem Opcionais cadastrado!")
			Close(oDlg)
			Return()
		Else
			While !EOF() .and. SubString(ZAU->ZAU_CODREF,1,8) = SubString(SB4->B4_COD,1,8)
	  		
				Aadd(aFieldFill,ZAU->ZAU_COR)
				Aadd(aFieldFill,ZAU->ZAU_PRODUT)
				Aadd(aFieldFill,ZAU->ZAU_GRUPO)
				Aadd(aFieldFill,ZAU->ZAU_ITEM)
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill	:={}
				DbSkip()
			EndDo
		Endif
	EndIf

	oMSOPC := MsNewGetDados():New( 189, 005, 275, 624, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

	oDlg:refresh()

Return

//------------------------------------------------
Static Function fMSSta(_tp, cColuna)
//------------------------------------------------
// Rotina para manute��o de cadastro de Status do Produto
// Tabela criada: SZL

	Local nX
	Local aHeaderEx		:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {"ZL_PRODUTO","ZL_COR","BV_DESCRI","ZL_TAM","ZL_BLQDEM","ZL_STATUS","ZL_DTLIBER","ZL_DTENCER"}
	Local aAlterFields	    := {"ZL_BLQDEM" ,"ZL_STATUS","ZL_DTLIBER","ZL_DTENCER"}

	Local aCicEstFields		:= {"ZL_PRODUTO","ZL_COR","BV_DESCRI","ZL_TAM","ZL_BLQDEM","ZL_STATUS","ZL_DTLIBER","ZL_DTENCER","ZL_CODCIC","ZL_DSCCIC","ZL_CODEST","ZL_DSCEST"}
	Local aAlterCicEst	    := {"ZL_BLQDEM" ,"ZL_STATUS","ZL_DTLIBER","ZL_DTENCER","ZL_CODCIC","ZL_CODEST"}


	Public nCountSta		:= 0
	Static oMSSta

	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZL_PRODUTO"))
		Aadd(aHeaderEx, {Space(16)+AllTrim(X3Titulo())+ Space(16),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZL_COR"))
		Aadd(aHeaderEx, {Space(16)+AllTrim(X3Titulo())+ Space(16),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("BV_DESCRI"))
		Aadd(aHeaderEx, {Space(40)+AllTrim(X3Titulo())+ Space(40),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZL_TAM"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZL_BLQDEM"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZL_STATUS"))
		Aadd(aHeaderEx, {Space(16)+AllTrim(X3Titulo())+ Space(16),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_ExistCod(4,M->ZL_STATUS)",;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZL_DTLIBER"))
		Aadd(aHeaderEx, {Space(16)+AllTrim(X3Titulo())+ Space(16),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,; // "U_AltStat(nCountSta,M->ZL_STATUS,M->ZL_DTLIBER,M->ZL_DTENCER)",;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZL_DTENCER"))
		Aadd(aHeaderEx, {Space(16)+AllTrim(X3Titulo())+ Space(16),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	

  // Define field values
	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+SubString(cCodigo,1,8))
	aFieldFill :={}
	//Adiciona no aHeader os campos de Cliclo x Esta��o apenas para os tipos solicitados pela equipe de Produtos
	If SB1->B1_TIPO $ ("PF|KT|ME") 		
		If SX3->(DbSeek("ZL_CODCIC"))
			Aadd(aHeaderEx, {Space(3)+AllTrim(X3Titulo())+ Space(3),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
		If SX3->(DbSeek("ZL_DSCCIC"))
			Aadd(aHeaderEx, {Space(30)+AllTrim(X3Titulo())+ Space(30),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
		If SX3->(DbSeek("ZL_CODEST"))
			Aadd(aHeaderEx, {Space(6)+AllTrim(X3Titulo())+ Space(6),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
		If SX3->(DbSeek("ZL_DSCEST"))
			Aadd(aHeaderEx, {Space(30)+AllTrim(X3Titulo())+ Space(30),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif

		aAlterFields:= aClone(aAlterCicEst)
	Endif

	While !EOF() .and. SubString(SB1->B1_COD,1,8) = SubString(cCodigo,1,8)
  		
		Aadd(aFieldFill, SubStr(SB1->B1_COD,1,8))
		Aadd(aFieldFill, SubStr(SB1->B1_COD,9,3))
		Aadd(aFieldFill, POSICIONE("SBV",1,xFilial("SBV")+'COR'+SubStr(SB1->B1_COD,9,3),"BV_DESCRI"))
		Aadd(aFieldFill, SubStr(SB1->B1_COD,12,4))
		Aadd(aFieldFill, SB1->B1_YBLQDEM)
		Aadd(aFieldFill, SB1->B1_YSITUAC)
		Aadd(aFieldFill, SB1->B1_YDATLIB)
		Aadd(aFieldFill, SB1->B1_YDATENC)
		If SB1->B1_TIPO $ ("PF|KT|ME")
			Aadd(aFieldFill, SB1->B1_XCODCIC)
			Aadd(aFieldFill, SB1->B1_XDSCCIC)
			Aadd(aFieldFill, SB1->B1_XCODEST)
			Aadd(aFieldFill, SB1->B1_XDSCEST)
		Endif
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	End

	oMSSta := MsNewGetDados():New( 189, 005, 275, 624, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)
    
Return

/*
User Function AltStat(nCountSta,Status,DtLiber,DtEncer)

If nCountSta > 0
	If DtEncer < dDatabase .or. DtEncer <> Ctod("  /  /    ")
		If DtLiber >= dDatabase 
			M->ZL_STATUS := "PV"
		End
	ElseIf DtEncer < dDatabase  
		M->ZL_STATUS := "NPV"
	EndIf
EndIf

nCountSta += 1
Return
*/

//------------------------------------------------
Static Function fMSCBar(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de C�dito de Barras EAN13
// Par�metro criado: MV_HCODBAR

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZD_COR","ZD_DESCRIC"}
	Local aAlterFields := {}
	Static oMSGrade

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZD_COR"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_COR","@!",3,0,,SX3->X3_USADO,"C","","R","",""})
	Endif
	If SX3->(DbSeek("ZD_DESCRIC"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_DESCRIC","@!",20,0,,SX3->X3_USADO,"C","","R","",""})
	Endif

	If alltrim(cColuna) <> ""
		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)
		If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
	  
		DbSelectArea("TMPSBV")

		cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" (nolock) WHERE D_E_L_E_T_ = ' ' AND ZD_FILIAL = '"+xFilial("SZD")+"' AND ZD_PRODUTO='"+SubString(SB4->B4_COD,1,15)+"'"

		If Select("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)

		DbSelectArea("TMPSZD")
  	
		While !EOF() .and. TMPSBV->BV_TABELA == cColuna
			DbSelectArea("SX3")
			_campo		:= "ZH_TAM"+StrZero(len(aFields)-1,3)
			cZdVerif	:= 0
		
			Aadd(aFields,_campo) //TMPSBV->BV_CHAVE)
			Aadd(aAlterFields,_campo)

			TMPSZD->(DbGoTop())

			While TMPSZD->(!EOF())
//				If &("TMPSZD->ZD_TAM"+StrZero(len(aFields)-2,3))=="X"
					cZdVerif += 1
				
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo)) .and. cZdVerif==1
						Aadd(aHeaderEx, {alltrim(TMPSBV->BV_DESCRI)+Space(4),SX3->X3_CAMPO,"@!",12,0,"",SX3->X3_USADO,"C","","R","",""})
					Endif
//				Endif
	 		
				TMPSZD->(DbSkip())
			EndDo
  	
			DbSelectArea("TMPSBV")
			DbSkip()
		End
		DbClosearea()
	Else
		Aadd(aFields,"ZD_TAM001")
		Aadd(aAlterFields,"ZD_TAM001")

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		If SX3->(DbSeek("ZD_TAM001"))
			Aadd(aHeaderEx, {AllTrim("Unico"),SX3->X3_CAMPO,"@!",12,0,"",SX3->X3_USADO,"C","","R","",""})
		Endif
	Endif
  // Define field values
//	_campw := GetMV("MV_HCODBAR")
  
	DbSelectArea("SZD")
	DbSetOrder(1)
	DbSeek(xfilial("SZD")+SubString(SB4->B4_COD,1,15))
    		
	While !EOF() .and. AllTrim(SZD->ZD_PRODUTO) = AllTrim(SB4->B4_COD)
  		
		Aadd(aFieldFill, SZD->ZD_COR)
		Aadd(aFieldFill, SZD->ZD_DESCRIC)
		For nX := 3 to Len(aheaderex)
			_campo := "ZD_TAM"+StrZero(nX-2,3)
//			IF SZD->&_campo == "X"
				_tam := rettam(nx-2,cColuna,"C")
				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+AllTrim(SB4->B4_COD)+SZD->ZD_COR+_tam)
				If Found() .and. alltrim(SB1->B1_CODBAR)<>""
					_campx := SubStr(SB1->B1_CODBAR,1,12)
					Aadd(aFieldFill,_campx)
				Else
					//_campw  += 1
					//_campwx := VerCodBar(_campw)
					//Aadd(aFieldFill,cValToChar(ROUND(_campwx, 0)))
					Aadd(aFieldFill,"")
				Endif
//			Else
//				Aadd(aFieldFill,"")
//			Endif
		Next
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSelectArea("SZD")
		DbSkip()
	End

	oMSGrade := MsNewGetDados():New( 189, 005, 275, 624, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)
    
Return

User Function valacols(_tp, cColuna)
// Rotina para atualiza��o de Acols com base no grupo de tamanhos selecionado na tabela SBV

	cColuna	:= UPPER(cColuna)
	fMSGrade(_tp, cColuna)
	oMSGrade:Refresh()
	oDlg:refresh()

Return

Static Function Save(_tp)
// Rotina para saltar a grade de produtos no SB4 e no SB1, Mix de Produto, C�digo de Barras e Status do Produto

	Local I,W

	Local cParCicPad:= AllTrim(SuperGetMV("HP_CICPAD",.F.,"002"))		
	Local cDscCicPad:= POSICIONE("ZB7",1,xFilial("ZB7")+cParCicPad,"ZB7_DSCCIC")

	If alltrim(cColuna) = ""
		Alert("Campo Coluna n�o preenchido!")
		Return()
	Endif 

	If SubString(cValToChar(cRastro),1,1)="L" .or. SubString(cValToChar(cRastro),1,1)="1"
		cRastroBD	:= "L"
	ElseIf SubString(cValToChar(cRastro),1,1)="S" .or. SubString(cValToChar(cRastro),1,1)="2"
		cRastroBD	:= "S"
	ElseIf SubString(cValToChar(cRastro),1,1)="N" .or. SubString(cValToChar(cRastro),1,1)="3"
		cRastroBD	:= "N"
	End

	If SubString(cValToChar(cEnd),1,1)="S" .or. SubString(cValToChar(cEnd),1,1)="1"
		cEndBD		:= "S"
	ElseIf SubString(cValToChar(cEnd),1,1)="N" .or. SubString(cValToChar(cEnd),1,1)="2"
		cEndBD		:= "N"
	End

	If cTipo $ "KT|ME|PF|PL"
		If SubString(cValToChar(cWeb),1,1)="S" .or. SubString(cValToChar(cWeb),1,1)="1"
			cWebBD		:= "S"
		ElseIf SubString(cValToChar(cWeb),1,1)="N" .or. SubString(cValToChar(cWeb),1,1)="2"
			cWebBD		:= "N"
		End
	Else
		cWebBD		:= "N"
	End

	If _tp == 1
		If AllTrim(cTipo)=""
			Alert("Favor preencher a Tipo do Produto!")
			return(.F.)
		End
		If AllTrim(cOrigem)=""
			Alert("Favor preencher a Origem do Produto!")
			return(.F.)
		End
		If AllTrim(cGrupo)=""
			Alert("Favor preencher o Grupo do Produto!")
			return(.F.)
		End
		If AllTrim(cCodigo)=""
			Alert("Favor preencher o C�digo do Produto!")
			return(.F.)
		End
		If AllTrim(cCodigoDesc)=""
			Alert("Favor preencher o Descri��o do Produto!")
			return(.F.)
		End		
		If AllTrim(cUM)=""
			Alert("Favor preencher a Unidade de Medida do Produto!")
			return(.F.)
		End
		If cTipo $ "PF|KT|ME|PL"
			If AllTrim(cMarca)=""
				Alert("Favor preencher a Marca do Produto!")
				return(.F.)
			End
			If AllTrim(cCiclo)=""
				Alert("Favor preencher o Ciclo do Produto!")
				return(.F.)
			End
			If AllTrim(cGpProd)=""
				Alert("Favor preencher o Grupo do Produto!")
				return(.F.)
			End
			If AllTrim(cSGrupo)=""
				Alert("Favor preencher o Sub Grupo do Produto!")
				return(.F.)
			End
			If AllTrim(cColecao)=""
				Alert("Favor preencher a Cole��o do Produto!")
				return(.F.)
			End
			If AllTrim(cSubCol)=""
				Alert("Favor preencher a Sub Cole��o do Produto!")
				return(.F.)
			End
			If AllTrim(cTpProd)=""
				Alert("Favor preencher o Tipo do Produto!")
				return(.F.)
			End
			If AllTrim(cCatProd)=""
				Alert("Favor preencher a Categoria do Produto!")
				return(.F.)
			End
			If nPeso <= 0
				Alert("Favor preencher o Peso Liquido do Produto!")
				return(.F.)
			End		
		End
		If AllTrim(cArm)=""
			Alert("Favor preencher o Armaz�m do Produto!")
			return(.F.)
		End
		If AllTrim(cColuna)=""
			Alert("Favor preencher o campo Coluna do Produto!")
			return(.F.)
		End
		If 	Len(OMSGrade:ACOLS)<=1 .and. AllTrim(OMSGrade:ACOLS[1,1])==""
			Alert("Favor incluir as Cores e Tamanhos para Produto!")
			return(.F.)
		End
		EMail(cCodigo)
		RecLock("SB4",.T.)
		Replace B4_FILIAL 		with xfilial("SB4")
		Replace B4_COD			with cCodigo
		Replace B4_TIPO			with cTipo
		Replace B4_YORIGEM		with cOrigem
		Replace B4_YCLASPR		with cClass
		Replace B4_GRUPO			with cGrupo
		Replace B4_DESC			with cCodigoDesc
		Replace B4_UM				with cUM
		Replace B4_YMARCA			with cMarca
		Replace B4_YNOMARC		with cMarcaDesc
		Replace B4_YCICLO			with cCiclo
		Replace B4_YNCICLO		with cCicloDesc
		Replace B4_YGPRODU		with cGpProd
		Replace B4_YNGPCOM		with cGpProdDesc
		Replace B4_YSUBGRP		with cSGrupo
		Replace B4_YNSUBGR		with cSGrupoDesc
		Replace B4_YCOLECA		with cColecao
		Replace B4_YNCOLEC		with cColecaoDesc
		Replace B4_YSUBCOL		with cSubCol
		Replace B4_YNSUBCO		with cSubColDesc
		Replace B4_YTPPROD		with cTpProd
		Replace B4_YNTPCOM		with cTpProdDesc
		Replace B4_YCATEGO		with cCatProd
		Replace B4_YNCATEG		with cCatProdDesc
		Replace B4_YOCASIA		with cOcasiao
		Replace B4_YTECIDO		with cTecido
		Replace B4_LOCPAD			with cArm
		Replace B4_RASTRO			with cRastroBD
		Replace B4_LOCALIZ		with cEndBD
		Replace B4_LINHA			with cLinha
		Replace B4_COLUNA			with cColuna
		Replace B4_PESO			with nPeso
		Replace B4_POSIPI			with cPosIPI
		Replace B4_YDATCAD		with dDtCad
		Replace B4_DWEB			with cWebBD
//		Replace B4_YTEMPO			with cTempo
		Replace B4_SITTRIB		with cTrib
		Replace B4_YDESCPT		with cDescriPTB
		Replace B4_YDESCES		with cDescriSPA
		Replace B4_YDESCIE		with cDescriENG
		Replace B4_YOBS			with cObsProd
		MsUnLock()
	
		For i := 1 to Len(OMSGrade:ACOLS)
			If OMSGrade:ACOLS[i,len(OMSGrade:AHEADER)+1] == .F. .and. AllTrim(OMSGrade:ACOLS[i,1])<>""
				DbSelectArea("SZD")
				DbSetOrder(1)
				DbSeek(xfilial("SZD")+SubString(cCodigo,1,15)+OMSGrade:ACOLS[i,1])

				If !Found() .and. AllTrim(OMSGrade:ACOLS[i,1])<>""
					RecLock("SZD",.T.)
					Replace ZD_FILIAL	with xfilial("SZD")
					Replace ZD_PRODUTO		with SubString(cCodigo,1,15)
					Replace ZD_COR			with OMSGrade:ACOLS[i,1]
					Replace ZD_DESCRIC		with OMSGrade:ACOLS[i,2]
					Replace ZD_COMPOS			with OMSGrade:ACOLS[i,3]
					Replace ZD_DESCCP2		with OMSGrade:ACOLS[i,4]
					For w := 5 to len(OMSGrade:aHeader)-2
						_cpo := "ZD_TAM"+StrZero((w-4),3)
						Replace &(_cpo) with OMSGrade:ACOLS[i,w]
					Next
//					Replace ZD_DTLIB			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//					Replace ZD_SITUAC			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
					Replace ZD_DESCLAV		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-1]
					Replace ZD_LAVAGE			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
					MsUnLock()
				Else
					RecLock("SZD",.F.)
					Replace ZD_DESCRIC		with OMSGrade:ACOLS[i,2]
					Replace ZD_COMPOS			with OMSGrade:ACOLS[i,3]
					Replace ZD_DESCCP2		with OMSGrade:ACOLS[i,4]
					For w := 5 to len(OMSGrade:aHeader)-2
						_cpo := "ZD_TAM"+StrZero((w-4),3)
						Replace &(_cpo) with OMSGrade:ACOLS[i,w]
					Next
//					Replace ZD_DTLIB			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//					Replace ZD_SITUAC			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
					Replace ZD_DESCLAV		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-1]
					Replace ZD_LAVAGE			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
					MsUnLock()
				Endif
			
				For w := 5 to len(OMSGrade:aHeader)-2
				// O que estiver com a quantidade marcada com o X, cria o produto
					If OMSGrade:ACOLS[i,w] == "X"
						_tam := rettam(w-4,cColuna,"C")
						_Dtam := rettam(w-4,cColuna,"D")

						DbSelectArea("SB1")
						DbSetOrder(1)
						DbSeek(xfilial("SB1")+AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)
						If !Found()
							RecLock("SB1",.T.)
							Replace B1_FILIAL			with xfilial("SB1")
							Replace B1_COD			with AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam
							Replace B1_DESC			with AllTrim(cCodigoDesc)+" - "+AllTrim(OMSGrade:ACOLS[i,2])+" - "+_Dtam
							Replace B1_TIPO			with cTipo
							Replace B1_GRUPO			with cGrupo
							Replace B1_UM				with cUM
							Replace B1_LOCPAD			with cArm
							Replace B1_SEGUM			with cUM
							Replace B1_CONV			with 1
							Replace B1_TIPCONV		with "M"
							Replace B1_PESO			with nPeso
							Replace B1_TIPODEC		with "N"
							Replace B1_ORIGEM			with cOrigem
							Replace B1_REVATU		with "001"
							Replace B1_POSIPI			with cPosIPI
							Replace B1_GRADE			with "S"
							Replace B1_RASTRO			with cRastroBD
							Replace B1_LOCALIZ		with cEndBD
							Replace B1_VEREAN			with "13"
							Replace B1_TIPOBN			with cTipo
							Replace B1_GARANT			with "N"
							Replace B1_YMARCA			with cMarca
							Replace B1_YCICLO			with cCiclo
							Replace B1_YGPRODU		with cGpProd
							Replace B1_YSUBGRP		with cSGrupo
							Replace B1_YCOLECA		with cColecao
							Replace B1_YSUBCOL		with cSubCol
							Replace B1_YDESCOL   	with POSICIONE("ZAA",1,xFilial("ZAA")+cColecao,"ZAA_DESCRI")
							Replace B1_YDESSCO   	with POSICIONE("ZAH",1,xFilial("ZAH")+cSubCol,"ZAH_DESCRI")
							Replace B1_YTPPROD		with cTpProd
							Replace B1_YCATEGO		with cCatProd
							Replace B1_YOCASIA		with cOcasiao
							Replace B1_YTECIDO		with cTecido
							Replace B1_YDATCAD		with dDtCad
							Replace B1_YDWEB			with cWebBD
//							Replace B1_YTEMPO			with cTempo
							Replace B1_YCLASPR		with cClass
							Replace B1_YCOLECA		with cColecao
							Replace B1_YSUBCOL		with cSubCol
							Replace B1_SITTRIB		with cTrib
							Replace B1_YDESCPT		with cDescriPTB
							Replace B1_YDESCES		with cDescriSPA
							Replace B1_YDESCIE		with cDescriENG
							Replace B1_YOBS			with cObsProd
//							Replace B1_YDATLIB		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
							Replace B1_YSITUAC		with "PNV" // Na inclusao grava Como produto em desenvolvimento
							Replace B1_YLAVAGE		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
							Replace B1_OPERPAD		with "01"
							If cTipo = "ME" .and. cOrigem = "0"
								Replace B1_GRTRIB with "006"
							ElseIf cOrigem = "1"
								Replace B1_GRTRIB with "006"
							ElseIf cOrigem = "0" .and. cTipo <> "ME"
								Replace B1_GRTRIB with "007"
							Endif
							If cTipo = "ME"
								Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
							Else
								Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTACUS")
							EndIf
							Replace B1_CTADESP 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
							IF cTipo $ "MP|MI|PF|PI|KT|ME|PL"
								Replace B1_MRP			with "S"
							Endif

							If cTipo $ ("PF|KT|ME")
								Replace B1_XCODCIC		with cParCicPad
								Replace B1_XDSCCIC		with cDscCicPad
							Endif

							MsUnLock()
							u_HPDISPLJ(SB1->B1_COD)

						
						Else
							RecLock("SB1",.F.)
							Replace B1_FILIAL			with xfilial("SB1")
							Replace B1_DESC			with AllTrim(cCodigoDesc)+" - "+AllTrim(OMSGrade:ACOLS[i,2])+" - "+_Dtam
							Replace B1_TIPO			with cTipo
							Replace B1_GRUPO			with cGrupo
							Replace B1_UM				with cUM
							Replace B1_LOCPAD			with cArm
							Replace B1_SEGUM			with cUM
							Replace B1_CONV			with 1
							Replace B1_TIPCONV		with "M"
							Replace B1_PESO			with nPeso
							Replace B1_TIPODEC		with "N"
							Replace B1_ORIGEM			with cOrigem
							Replace B1_POSIPI			with cPosIPI
							Replace B1_GRADE			with "S"
							Replace B1_RASTRO			with cRastroBD
							Replace B1_LOCALIZ		with cEndBD
							Replace B1_VEREAN			with "13"
							Replace B1_TIPOBN			with cTipo
							Replace B1_GARANT			with "N"
							Replace B1_YMARCA			with cMarca
							Replace B1_YCICLO			with cCiclo
							Replace B1_YGPRODU		with cGpProd
							Replace B1_YSUBGRP		with cSGrupo
							Replace B1_YCOLECA		with cColecao
							Replace B1_YSUBCOL		with cSubCol
							Replace B1_YDESCOL   	with POSICIONE("ZAA",1,xFilial("ZAA")+cColecao,"ZAA_DESCRI")
							Replace B1_YDESSCO   	with POSICIONE("ZAH",1,xFilial("ZAH")+cSubCol,"ZAH_DESCRI")
							Replace B1_YTPPROD		with cTpProd
							Replace B1_YCATEGO		with cCatProd
							Replace B1_YOCASIA		with cOcasiao
							Replace B1_YTECIDO		with cTecido
							Replace B1_YDATCAD		with dDtCad
							Replace B1_YDWEB			with cWebBD
//							Replace B1_YTEMPO			with cTempo
							Replace B1_YCLASPR		with cClass
							Replace B1_SITTRIB		with cTrib
							Replace B1_YDESCPT		with cDescriPTB
							Replace B1_YDESCES		with cDescriSPA
							Replace B1_YDESCIE		with cDescriENG
							Replace B1_YOBS			with cObsProd
//							Replace B1_YDATLIB		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//							Replace B1_YSITUAC		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
							Replace B1_YLAVAGE		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
							If cTipo = "ME" .and. cOrigem = "0"
								Replace B1_GRTRIB with "006"
							ElseIf cOrigem = "1"
								Replace B1_GRTRIB with "006"
							ElseIf cOrigem = "0" .and. cTipo <> "ME"
								Replace B1_GRTRIB with "007"
							Endif
							If cTipo = "ME"
								Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
							Else
								Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTACUS")
							EndIf
							Replace B1_CTADESP 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
							IF cTipo $ "MP|MI|PF|PL|PI|KT|ME"
								Replace B1_MRP			with "S"
							Endif

							//Replace B1_XCODCIC		with oMSSta:ACOLS[i,9]
							//Replace B1_XDSCCIC		with oMSSta:ACOLS[i,10]

							MsUnLock()
							u_HPDISPLJ(SB1->B1_COD)
						Endif
					Endif
				Next
			Endif
		Next
	ElseIf _tp == 2
		DbSelectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+cCodigo)
	
		If Found()
			RecLock("SB4",.F.)
			Replace B4_FILIAL 		with xfilial("SB4")
			Replace B4_COD			with cCodigo
			Replace B4_DESC			with AllTrim(cCodigoDesc)
			Replace B4_TIPO			with cTipo
			Replace B4_YORIGEM		with cOrigem
			Replace B4_YCLASPR		with cClass
			Replace B4_GRUPO			with cGrupo
			Replace B4_DESC			with cCodigoDesc
			Replace B4_UM				with cUM
			Replace B4_YMARCA			with cMarca
			Replace B4_YNOMARC		with cMarcaDesc
			Replace B4_YCICLO			with cCiclo
			Replace B4_YNCICLO		with cCicloDesc
			Replace B4_YGPRODU		with cGpProd
			Replace B4_YNGPCOM		with cGpProdDesc
			Replace B4_YSUBGRP		with cSGrupo
			Replace B4_YNSUBGR		with cSGrupoDesc
			Replace B4_YCOLECA		with cColecao
			Replace B4_YNCOLEC		with cColecaoDesc
			Replace B4_YSUBCOL		with cSubCol
			Replace B4_YNSUBCO		with cSubColDesc
			Replace B4_YTPPROD		with cTpProd
			Replace B4_YNTPCOM		with cTpProdDesc
			Replace B4_YCATEGO		with cCatProd
			Replace B4_YNCATEG		with cCatProdDesc
			Replace B4_YOCASIA		with cOcasiao
			Replace B4_YTECIDO		with cTecido
			Replace B4_LOCPAD			with cArm
			Replace B4_RASTRO			with cRastroBD
			Replace B4_LOCALIZ		with cEndBD
			Replace B4_LINHA			with cLinha
			Replace B4_COLUNA			with cColuna
			Replace B4_PESO			with nPeso
			Replace B4_POSIPI			with cPosIPI
			Replace B4_YDATCAD		with dDtCad
			Replace B4_DWEB			with cWebBD
//			Replace B4_YTEMPO			with cTempo
			Replace B4_SITTRIB		with cTrib
			Replace B4_YDESCPT		with cDescriPTB
			Replace B4_YDESCES		with cDescriSPA
			Replace B4_YDESCIE		with cDescriENG
			Replace B4_YOBS			with cObsProd
			MsUnLock()
	
			For i := 1 to Len(OMSGrade:ACOLS)
				If OMSGrade:ACOLS[i,len(OMSGrade:AHEADER)+1] == .F. .and. AllTrim(OMSGrade:ACOLS[i,1])<>""
					DbSelectArea("SZD")
					DbSetOrder(1)
					DbSeek(xfilial("SZD")+SubString(cCodigo,1,15)+OMSGrade:ACOLS[i,1])

					If !Found()
						RecLock("SZD",.T.)
						Replace ZD_FILIAL	with xfilial("SZD")
						Replace ZD_PRODUTO		with SubString(cCodigo,1,15)
						Replace ZD_COR			with OMSGrade:ACOLS[i,1]
						Replace ZD_DESCRIC		with OMSGrade:ACOLS[i,2]
						Replace ZD_COMPOS			with OMSGrade:ACOLS[i,3]
						Replace ZD_DESCCP2		with OMSGrade:ACOLS[i,4]
						For w := 5 to len(OMSGrade:aHeader)-2
							_cpo := "ZD_TAM"+StrZero((w-4),3)
							Replace &(_cpo) with OMSGrade:ACOLS[i,w]
						Next
//						Replace ZD_DTLIB			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//						Replace ZD_SITUAC			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
						Replace ZD_DESCLAV		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-1]
						Replace ZD_LAVAGE			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
						MsUnLock()
					Else
						RecLock("SZD",.F.)
						Replace ZD_COMPOS			with OMSGrade:ACOLS[i,3]
						Replace ZD_DESCCP2		with OMSGrade:ACOLS[i,4]
						For w := 5 to len(OMSGrade:aHeader)-2
							_cpo := "SZD->ZD_TAM"+StrZero((w-4),3)
							Replace &(_cpo) with OMSGrade:ACOLS[i,w]
							If OMSGrade:ACOLS[i,w] = " " .AND. &(_cpo) = "X"
								_tam := rettam(w-4,cColuna,"C") 
								_Dtam := rettam(w-4,cColuna,"D")						
							If Select("TMPSG1") > 0
								TMPSG1->(DbCloseArea())
							EndIf

							cQry := " SELECT TOP 1 * FROM "+RetSqlName("SG1")+" WITH(NOLOCK) WHERE G1_COD = '"+Alltrim(AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)+"'  AND D_E_L_E_T_ = '' "
							cQry += " UNION "
							cQry += " SELECT TOP 1 * FROM "+RetSqlName("SG1")+" WITH(NOLOCK) WHERE G1_COMP = '"+Alltrim(AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)+"'  AND D_E_L_E_T_ = '' "
					
							dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPSG1",.T.,.T.)
							dbSelectArea("TMPSG1")
							
							IF !EMPTY(TMPSG1->G1_COD) 
								Msginfo("O Produto "+Alltrim(AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)+" n�o poder� ter seu tamanho "+Alltrim(_tam)+"  excluido da Grade, pois existe Estrutura ","HOPE")
							ELSE
								Replace &(_cpo) with OMSGrade:ACOLS[i,w]
							ENDIF
							
							ENDIF
						Next x
//						Replace ZD_DTLIB			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//						Replace ZD_SITUAC			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
						Replace SZD->ZD_DESCLAV		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-1]
						Replace SZD->ZD_LAVAGE		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
						MsUnLock()
					Endif

					For w := 5 to len(OMSGrade:aHeader)-2
						If OMSGrade:ACOLS[i,w] == "X"
							_tam := rettam(w-4,cColuna,"C") 
							_Dtam := rettam(w-4,cColuna,"D")
							DbSelectArea("SB1")
							DbSetOrder(1)
							DbSeek(xfilial("SB1")+AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)
	
							If !Found()
								RecLock("SB1",.T.)
								Replace B1_FILIAL			with xfilial("SB1")
								Replace B1_COD			with AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam
								Replace B1_DESC			with AllTrim(cCodigoDesc)+" - "+AllTrim(OMSGrade:ACOLS[i,2])+" - "+_Dtam
								Replace B1_TIPO			with cTipo
								Replace B1_GRUPO			with cGrupo
								Replace B1_UM				with cUM
								Replace B1_LOCPAD			with cArm
								Replace B1_SEGUM			with cUM
								Replace B1_CONV			with 1
								Replace B1_TIPCONV		with "M"
								Replace B1_PESO			with nPeso
								Replace B1_TIPODEC		with "N"
								Replace B1_ORIGEM			with cOrigem
								Replace B1_REVATU       with "001"
								Replace B1_POSIPI			with cPosIPI
								Replace B1_GRADE			with "S"
								Replace B1_RASTRO			with cRastroBD
								Replace B1_LOCALIZ		with cEndBD
								Replace B1_VEREAN			with "13"
								Replace B1_TIPOBN			with cTipo
								Replace B1_GARANT			with "N"
								Replace B1_YMARCA			with cMarca
								Replace B1_YCICLO			with cCiclo
								Replace B1_YGPRODU		with cGpProd
								Replace B1_YSUBGRP		with cSGrupo
								Replace B1_YCOLECA		with cColecao
								Replace B1_YSUBCOL		with cSubCol
								Replace B1_YTPPROD		with cTpProd
								Replace B1_YCATEGO		with cCatProd
								Replace B1_YOCASIA		with cOcasiao
								Replace B1_YTECIDO		with cTecido
								Replace B1_YDATCAD		with dDtCad
								Replace B1_YDWEB			with cWebBD
//								Replace B1_YTEMPO			with cTempo
								Replace B1_YCLASPR		with cClass
								Replace B1_SITTRIB		with cTrib
								Replace B1_YDESCPT		with cDescriPTB
								Replace B1_YDESCES		with cDescriSPA
								Replace B1_YDESCIE		with cDescriENG
								Replace B1_YOBS			with cObsProd
//								Replace B1_YDATLIB		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
								Replace B1_YSITUAC		with  "PNV" // Na inclusao grava Como produto em desenvolvimento
								Replace B1_YLAVAGE		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
								Replace B1_OPERPAD		with "01"
								If cTipo = "ME" .and. cOrigem = "0"
									Replace B1_GRTRIB with "006"
								ElseIf cOrigem = "1"
									Replace B1_GRTRIB with "006"
								ElseIf cOrigem = "0" .and. cTipo <> "ME"
									Replace B1_GRTRIB with "007"
								Endif
								If cTipo = "ME"
									Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
								Else
									Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTACUS")
								EndIf
								Replace B1_CTADESP 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
								IF cTipo $ "MP|MI|PF|PL|PI|KT|ME"
									Replace B1_MRP			with "S"
								Endif

								If cTipo $ ("PF|KT|ME")
									Replace B1_XCODCIC		with cParCicPad
									Replace B1_XDSCCIC		with cDscCicPad
								Endif
								MsUnLock()
								u_HPDISPLJ(SB1->B1_COD)
							Else
								RecLock("SB1",.F.)
								Replace B1_FILIAL			with xfilial("SB1")
								Replace B1_DESC			with AllTrim(cCodigoDesc)+" - "+AllTrim(OMSGrade:ACOLS[i,2])+" - "+_dtam
								Replace B1_TIPO			with cTipo
								Replace B1_GRUPO			with cGrupo
								Replace B1_UM				with cUM
								Replace B1_LOCPAD			with cArm
								Replace B1_SEGUM			with cUM
								Replace B1_CONV			with 1
								Replace B1_TIPCONV		with "M"
								Replace B1_PESO			with nPeso
								Replace B1_TIPODEC		with "N"
								Replace B1_ORIGEM			with cOrigem
								Replace B1_POSIPI			with cPosIPI
								Replace B1_GRADE			with "S"
								Replace B1_RASTRO			with cRastroBD
								Replace B1_LOCALIZ		with cEndBD
								Replace B1_VEREAN			with "13"
								Replace B1_TIPOBN			with cTipo
								Replace B1_GARANT			with "N"
								Replace B1_YMARCA			with cMarca
								Replace B1_YCICLO			with cCiclo
								Replace B1_YGPRODU		with cGpProd
								Replace B1_YSUBGRP		with cSGrupo
								Replace B1_YCOLECA		with cColecao
								Replace B1_YSUBCOL		with cSubCol
								Replace B1_YTPPROD		with cTpProd
								Replace B1_YCATEGO		with cCatProd
								Replace B1_YOCASIA		with cOcasiao
								Replace B1_YTECIDO		with cTecido
								Replace B1_YDATCAD		with dDtCad
								Replace B1_YDWEB			with cWebBD
//								Replace B1_YTEMPO			with cTempo
								Replace B1_YCLASPR		with cClass
								Replace B1_SITTRIB		with cTrib
								Replace B1_YDESCPT		with cDescriPTB
								Replace B1_YDESCES		with cDescriSPA
								Replace B1_YDESCIE		with cDescriENG
								Replace B1_YOBS			with cObsProd
//								Replace B1_YDATLIB		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//								Replace B1_YSITUAC		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
								Replace B1_YLAVAGE		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
								If cTipo = "ME" .and. cOrigem = "0"
									Replace B1_GRTRIB with "006"
								ElseIf cOrigem = "1"
									Replace B1_GRTRIB with "006"
								ElseIf cOrigem = "0" .and. cTipo <> "ME"
									Replace B1_GRTRIB with "007"
								Endif
								If cTipo = "ME"
									Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
								Else
									Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTACUS")
								EndIf
								Replace B1_CTADESP 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
								IF cTipo $ "MP|MI|PF|PL|PI|KT|ME"
									Replace B1_MRP			with "S"
								Endif

								//Replace B1_XCODCIC		with cParCicPad
								//Replace B1_XDSCCIC		with cDscCicPad

								MsUnLock()
								u_HPDISPLJ(SB1->B1_COD)
							Endif
						Endif
					Next
				Else
					DbSelectArea("SZD")
					DbSetOrder(1)
					DbSeek(xfilial("SZD")+SubString(cCodigo,1,15)+OMSGrade:ACOLS[i,1])
					// TODO Valida��o se a referencia existe na estrutura. Weskley Silva 29.01.2019
					If Select("TMPSG1") > 0
						TMPSG1->(DbCloseArea())
					EndIf

					cQry := " SELECT TOP 1 * FROM "+RetSqlName("SG1")+" WITH(NOLOCK) WHERE LEFT(G1_COD,11) = '"+SubString(alltrim(cCodigo),1,15)+OMSGrade:ACOLS[i,1]+"'  AND D_E_L_E_T_ = '' "
					cQry += " UNION "
					cQry += " SELECT TOP 1 * FROM "+RetSqlName("SG1")+" WITH(NOLOCK) WHERE LEFT(G1_COMP,11) = '"+SubString(alltrim(cCodigo),1,15)+OMSGrade:ACOLS[i,1]+"'  AND D_E_L_E_T_ = '' "
					
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPSG1",.T.,.T.)
					dbSelectArea("TMPSG1")
					
					IF !EMPTY(TMPSG1->G1_COD)
						Msginfo(" A refencia "+SubString(alltrim(cCodigo),1,15)+" na cor "+OMSGrade:ACOLS[i,1]+" possui estrutura e n�o poder� ser excluida!! ","HOPE")
					else
					
						If Found()
							RecLock("SZD",.F.)
							DbDelete()
							MsUnLock()
						Endif
					ENDIF
				Endif
			Next
		Endif
	ElseIf _tp == 7
		cCodBar	:= ""
		cCodBarMV	:= ""
		For i := 1 to Len(OMSGrade:ACOLS)
			If OMSGrade:ACOLS[i,len(OMSGrade:AHEADER)+1] == .F.
				For w := 3 to len(OMSGrade:aHeader)
					If alltrim(OMSGrade:ACOLS[i,w]) <> ""
						_tam := rettam2(AllTrim(OMSGrade:AHEADER[w,1]),cColuna,"C")
						DbSelectArea("SB1")
						DbSetOrder(1)
						DbSeek(xfilial("SB1")+AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)

						_codbar := alltrim(OMSGrade:ACOLS[i,w]) + cValToChar(dig13(alltrim(OMSGrade:ACOLS[i,w])))
						If Found() .and. alltrim(SB1->B1_CODBAR) == ""
							RecLock("SB1",.F.)
							Replace B1_CODBAR		with _codbar
							MsUnLock()
						
							If cCodBarMV < alltrim(OMSGrade:ACOLS[i,w])
								cCodBarMV := alltrim(OMSGrade:ACOLS[i,w])
							End
						Endif
					Endif
				Next
			Endif
		Next
		If cCodBarMV <> ""
			PutMv ("MV_HCODBAR",cCodBarMV)
		End
	ElseIf _tp = 8 .or. _tp = 9
		For i := 1 to Len(oMSGrade:ACOLS)
			If oMSGrade:ACOLS[i,len(oMSGrade:AHEADER)+1] == .F.
				DbSelectArea("SZH")
				DbSetOrder(1)
				DbSeek(xfilial("SZH")+SubString(cCodigo,1,15))

				If !Found()
					RecLock("SZH",.T.)
					Replace ZH_FILIAL			with 	xfilial("SZD")
					Replace ZH_PRODUTO		with SubString(cCodigo,1,15)
				Else
					RecLock("SZH",.F.)
				Endif
				For w := 1 to len(oMSGrade:aHeader)
					_cpo := oMSGrade:AHEADER[w,2]
					Replace &(_cpo)		with oMSGrade:ACOLS[i,w]
				Next
				MsUnLock()
			Endif
		Next
	ElseIf _tp = 11 .or. _tp = 12
		For i := 1 to Len(oMSGrade:ACOLS)
			If oMSGrade:ACOLS[i,len(oMSGrade:AHEADER)+1] == .F.
				DbSelectArea("SZN")
				DbSetOrder(1)
				DbSeek(xfilial("SZN")+PADR(cCodigo,15)+oMSGrade:ACOLS[i,1])

				If !Found()
					RecLock("SZN",.T.)
					Replace ZN_FILIAL		with xfilial("SZN")
					Replace ZN_PRODUTO		with SubString(cCodigo,1,15)
					Replace ZN_PARTES		with oMSGrade:ACOLS[i,1]
				Else
					RecLock("SZN",.F.)
					Replace ZN_PARTES		with oMSGrade:ACOLS[i,1]
				Endif
				For w := 2 to len(oMSGrade:aHeader)
					_cpo := oMSGrade:AHEADER[w,2]
					Replace &(_cpo)		with oMSGrade:ACOLS[i,w]
				Next
				MsUnLock()
			Else
				DbSelectArea("SZN")
				DbSetOrder(1)
				DbSeek(xfilial("SZN")+PADR(cCodigo,15)+oMSGrade:ACOLS[i,1])

				If Found()
					Reclock("SZN")
					DbDelete()
					MsUnLock()
				Endif
			Endif
		Next
	ElseIf _tp = 13 .or. _tp = 14
		nTempo	:= 0
		For i := 1 to Len(oMSGrade:ACOLS)
			If len(oMSGrade:ACOLS[i]) == 5
				If oMSGrade:ACOLS[i,len(oMSGrade:AHEADER)+1] == .F.
					DbSelectArea("SZO")
					DbSetOrder(1)
					DbSeek(xfilial("SZO")+PADR(cCodigo,15)+oMSGrade:ACOLS[i,2])
	
					If !Found()
						RecLock("SZO",.T.)
						Replace ZO_FILIAL			with xfilial("SZO")
						Replace ZO_PRODUTO			with SubString(cCodigo,1,15)
						Replace ZO_DESCRI			with oMSGrade:ACOLS[i,2]
						Replace ZO_OBS				with oMSGrade:ACOLS[i,3]
						Replace ZO_OBSMAQ			with oMSGrade:ACOLS[i,4]
						Replace ZO_TEMPO			with oMSGrade:ACOLS[i,1]
						MsUnLock()
					Endif
				Endif

			ElseIf oMSGrade:ACOLS[i,len(oMSGrade:AHEADER)+2] == .F.

				DbSelectArea("SZO")
				DbSetOrder(1)
				DbSeek(xfilial("SZO")+PADR(cCodigo,15)+oMSGrade:ACOLS[i,2])

				If !Found()
					RecLock("SZO",.T.)
					Replace ZO_FILIAL			with xfilial("SZO")
					Replace ZO_PRODUTO			with SubString(cCodigo,1,15)
					Replace ZO_DESCRI			with oMSGrade:ACOLS[i,2]
					Replace ZO_OBS				with oMSGrade:ACOLS[i,3]
					Replace ZO_OBSMAQ			with oMSGrade:ACOLS[i,4]
					Replace ZO_TEMPO			with oMSGrade:ACOLS[i,1]
				Else
					RecLock("SZO",.F.)
					Replace ZO_DESCRI			with oMSGrade:ACOLS[i,2]
					Replace ZO_OBS				with oMSGrade:ACOLS[i,3]
					Replace ZO_OBSMAQ			with oMSGrade:ACOLS[i,4]
					Replace ZO_TEMPO			with oMSGrade:ACOLS[i,1]
				Endif
				MsUnLock()
				
			ElseIf oMSGrade:ACOLS[i,len(oMSGrade:AHEADER)+2] == .T.

				DbSelectArea("SZO")
				DbSetOrder(1)
				DbSeek(xfilial("SZO")+PADR(cCodigo,15)+oMSGrade:ACOLS[i,2])

				If Found()
					RecLock("SZO",.F.)
					DbDelete()
					MsUnLock()
				Endif
			
			End
			
			nTempo	+= oMSGrade:ACOLS[i,1]
			
		Next
			
		DbSelectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+cCodigo)

		If Found()
			RecLock("SB4",.F.)
			Replace B4_YTEMPO			with ALLtRIM(STR(nTempo))			//Daniel 21/03/2018	//Replace B4_YTEMPO	with ALLtRIM(STR(cTempo)
			MsUnLock()
		End

		_qry := "UPDATE "+RetSqlName("SG1")+" SET G1_QUANT = '"+ALLtRIM(STR(nTempo))+"' " 
		_qry += "FROM "+RetSqlName("SG1")+" SG1 " 
		_qry += "	INNER JOIN "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = G1_COMP "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(G1_COD,8) = '"+alltrim(cCodigo)+"' AND B1_TIPO = 'BN' "

		nStatus := TcSqlExec(_qry)

		if (nStatus < 0)
  			MsgBox(TCSQLError(), "Erro no comando", "Stop") 
  		endif
		
		DbSelectArea("SG1")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SG1")+alltrim(cCodigo))
		
		While !EOF() .and. SubStr(SG1->G1_COD,1,8) == alltrim(cCodigo)
			DbSelectArea("SB1")
			DbSetOrder(1)
			Dbseek(xfilial("SB1")+SG1->G1_COMP)
			
			If SB1->B1_TIPO = "BN"
	
				DbSelectArea("SB4")
				DbSetOrder(1)
				Dbseek(xfilial("SB4")+SubStr(SG1->G1_COD,1,8))
				
				_wtam := rettam3(SB4->B4_COLUNA,SubStr(SG1->G1_COD,12,4),"C")
				_campo:= "ZF_TAM"+_wtam

				DbSelectarea("SG5")
				DbSetOrder(1)
				DbSeek(xfilial("SG5")+alltrim(SB4->B4_COD))
  
				If Found()
					_cod := SG5->G5_PRODUTO
					While !EOF() .and. SG5->G5_PRODUTO == _cod
						DbSelectArea("SZF")
						DbSetOrder(2)
						DbSeek(xfilial("SZF")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG5->G5_REVISAO+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))
			
						If Found() .and. &_campo > 0 
							RecLock("SZF",.F.)
								Replace &_campo			with SG1->G1_QUANT
							MsUnLock()
						Endif
						DbSelectArea("SG5")
						DbSkip()
					End
				Else
					DbSelectArea("SZF")
					DbSetOrder(2)
					DbSeek(xfilial("SZF")+Padr(SubStr(SG1->G1_COD,1,8),15)+"001"+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))
			
					If Found() .and. &_campo > 0
						RecLock("SZF",.F.)
							Replace &_campo			with SG1->G1_QUANT
						MsUnLock()
					Endif
							
				Endif
			Endif
			DbSelectArea("SG1")
			DbSkip()
		End
	ElseIf _tp = 15 .or. _tp = 16
		For i := 1 to Len(oMSOPC:ACOLS)
			If oMSOPC:ACOLS[i,len(oMSOPC:AHEADER)+1] == .F.
				DbSelectArea("ZAU")
				DbSetOrder(1)
				DbSeek(xfilial("ZAU")+alltrim(cCodigo)+oMSOPC:ACOLS[i,3]+oMSOPC:ACOLS[i,1]+oMSOPC:ACOLS[i,4]+oMSOPC:ACOLS[i,2])
				If !Found()
					RecLock("ZAU",.T.)
					Replace ZAU_FILIAL			with xfilial("ZAU")
					Replace ZAU_CODREF			with ALLTRIM(cCodigo)
					Replace ZAU_COR				with oMSOPC:ACOLS[i,1]
					Replace ZAU_PRODUT			with oMSOPC:ACOLS[i,2]
					Replace ZAU_GRUPO			with oMSOPC:ACOLS[i,3]
					Replace ZAU_ITEM			with oMSOPC:ACOLS[i,4]
				Else
					RecLock("ZAU",.F.)
					Replace ZAU_COR				with oMSOPC:ACOLS[i,1]
					Replace ZAU_PRODUT			with oMSOPC:ACOLS[i,2]
					Replace ZAU_GRUPO			with oMSOPC:ACOLS[i,3]
					Replace ZAU_ITEM			with oMSOPC:ACOLS[i,4]
				Endif
				MsUnLock()
				
				DbSelectArea("SGA")
				DbSetOrder(1)
				DbSeek(xfilial("SGA")+oMSOPC:ACOLS[i,3]+oMSOPC:ACOLS[i,4])
					
				If !Found()
					RecLock("SGA",.T.)
						Replace GA_FILIAL		with xfilial("SGA")
						Replace GA_GROPC		with oMSOPC:ACOLS[i,3]
						Replace GA_DESCGRP		with ALLTRIM(cCodigo)
						Replace GA_OPC			with oMSOPC:ACOLS[i,4]
						Replace GA_DESCOPC		with SubStr(oMSOPC:ACOLS[i,2],1,8)+" - "+SubStr(oMSOPC:ACOLS[i,2],9,3)
					MsUnLock()
				Endif

				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+ALLTRIM(cCodigo)+oMSOPC:ACOLS[i,1])
				
				While !EOF() .and. SubStr(SB1->B1_COD,1,11) = ALLTRIM(cCodigo)+oMSOPC:ACOLS[i,1]
					
					DbSelectArea("SG1")
					DbSetOrder(1)
					DbSeek(Xfilial("SG1")+SB1->B1_COD+SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4)+oMSOPC:ACOLS[i,3]) 

					If !Found() .and. len(alltrim(SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4))) = 15
/*						RecLock("SG1",.T.)
							Replace G1_FILIAL		with xfilial("SG1")
							Replace G1_COD			with SB1->B1_COD
							Replace G1_COMP			with SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4)
							Replace G1_TRT			with oMSOPC:ACOLS[i,3]
							Replace G1_QUANT		with 1
							Replace G1_INI			with ctod("01/01/1950")
							Replace G1_FIM			with ctod("31/12/2049")
							Replace G1_NIV			with "01"
							Replace G1_NIVINV		with "99"
							Replace G1_REVINI		with "001"
							Replace G1_REVFIM		with "001"
							Replace G1_GROPC		with oMSOPC:ACOLS[i,3]
							Replace G1_OPC			with oMSOPC:ACOLS[i,4]
						MsUnLock()
*/
						_qry := "Insert into "+RetSqlName("SG1")+" (G1_FILIAL, G1_COD, G1_COMP, G1_TRT, G1_QUANT, G1_INI, G1_FIM, G1_NIV, G1_NIVINV, G1_REVINI," 
						_qry += "G1_REVFIM, G1_GROPC, G1_OPC, R_E_C_N_O_) Values ('"+xfilial("SG1")+"','"+SB1->B1_COD+"','"
						_qry += SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4)+"','"+oMSOPC:ACOLS[i,3]+"',1,'19500101'," 
						_qry += "'20491231', '01', '99', '001', '001','"+oMSOPC:ACOLS[i,3]+"','"+oMSOPC:ACOLS[i,4]+"',(Select max(R_E_C_N_O_)+1 from SG1010))"

						nStatus := TcSqlExec(_qry)
				
						if (nStatus < 0)
				  			MsgBox(TCSQLError(), "Erro no comando", "Stop") 
				  		endif

					Endif					
					DbSelectArea("SB1")
					DbSkip()
				End
			ELSE
				DbSelectArea("ZAU")
				DbSetOrder(1)
				DbSeek(xfilial("ZAU")+alltrim(cCodigo)+oMSOPC:ACOLS[i,3]+oMSOPC:ACOLS[i,1]+oMSOPC:ACOLS[i,4])
				If Found()
					RecLock("ZAU",.F.)
						DBDELETE()
					MsUnLock()
				Endif

				DbSelectArea("SGA")
				DbSetOrder(1)
				DbSeek(xfilial("SGA")+oMSOPC:ACOLS[i,3]+oMSOPC:ACOLS[i,4])
					
				If Found()
					RecLock("SGA",.F.)
						DBDELETE()
					MsUnLock()
				Endif

				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+ALLTRIM(cCodigo)+oMSOPC:ACOLS[i,1])
				
				While !EOF() .and. SubStr(SB1->B1_COD,1,11) = ALLTRIM(cCodigo)+oMSOPC:ACOLS[i,1]
					
					DbSelectArea("SG1")
					DbSetOrder(1)
					DbSeek(Xfilial("SG1")+SB1->B1_COD+SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4)+oMSOPC:ACOLS[i,3]) 

					If Found()
						RecLock("SG1",.F.)
							DBDELETE()
						MsUnLock()
					Endif					
					DbSelectArea("SB1")
					DbSkip()
				End
			Endif
		Next
	ElseIf _tp = 10
		For i := 1 to Len(oMSSta:ACOLS)
			If oMSSta:ACOLS[i,len(oMSSta:AHEADER)+1] == .F.
				DbSelectArea("SZL")
				DbSetOrder(1)
				DbSeek(xfilial("SZL")+oMSSta:ACOLS[i,1]+oMSSta:ACOLS[i,2]+oMSSta:ACOLS[i,4])
                                       
				If !Found()
					RecLock("SZL",.T.) 
					Replace ZL_FILIAL		with xfilial("SB1")
					Replace ZL_PRODUTO		with oMSSta:ACOLS[i,1]
					Replace ZL_COR			with oMSSta:ACOLS[i,2]
					Replace ZL_TAM			with oMSSta:ACOLS[i,4]
					Replace ZL_BLQDEM		with oMSSta:ACOLS[i,5]
					Replace ZL_STATUS		with ALLTRIM(oMSSta:ACOLS[i,6])
					Replace ZL_DTLIBER		with oMSSta:ACOLS[i,7]
					Replace ZL_DTENCER		with oMSSta:ACOLS[i,8]
					If cTipo $ ("PF|KT|ME")
						Replace ZL_CODCIC		with cParCicPad //oMSSta:ACOLS[i,9]
						Replace ZL_DSCCIC		with cDscCicPad //oMSSta:ACOLS[i,10]
						Replace ZL_CODEST		with oMSSta:ACOLS[i,11]
						Replace ZL_DSCEST		with oMSSta:ACOLS[i,12]
					Endif
				Else
					RecLock("SZL",.F.)
					Replace ZL_BLQDEM		with oMSSta:ACOLS[i,5]
					Replace ZL_STATUS		with ALLTRIM(oMSSta:ACOLS[i,6])
					Replace ZL_DTLIBER		with oMSSta:ACOLS[i,7]
					Replace ZL_DTENCER		with oMSSta:ACOLS[i,8]
					If cTipo $ ("PF|KT|ME")
						Replace ZL_CODCIC		with oMSSta:ACOLS[i,9]
						Replace ZL_DSCCIC		with oMSSta:ACOLS[i,10]
						Replace ZL_CODEST		with oMSSta:ACOLS[i,11]
						Replace ZL_DSCEST		with oMSSta:ACOLS[i,12]
					Endif
				Endif
				MsUnLock()
			
				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+oMSSta:ACOLS[i,1]+oMSSta:ACOLS[i,2]+oMSSta:ACOLS[i,4])
				If Found()
					RecLock("SB1",.F.)
					Replace B1_YBLQDEM		with oMSSta:ACOLS[i,5]
					Replace B1_YSITUAC		with ALLTRIM(oMSSta:ACOLS[i,6])
					Replace B1_YDATLIB		with oMSSta:ACOLS[i,7]
					Replace B1_YDATENC		with oMSSta:ACOLS[i,8]
					If cTipo $ ("PF|KT|ME")
						Replace B1_XCODCIC		with oMSSta:ACOLS[i,9]
						Replace B1_XDSCCIC		with oMSSta:ACOLS[i,10]
						Replace B1_XCODEST		with oMSSta:ACOLS[i,11]
						Replace B1_XDSCEST		with oMSSta:ACOLS[i,12]
					Endif
					MsUnLock()
					u_HPDISPLJ(SB1->B1_COD)
				End
			End
		Next
		u_HPCPP016()
	Endif

	oDlg:End()

Return

Static Function rettam(ordtam,xcoluna,_tp)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
	_BVTAM := ""

	cQuery  := "SELECT Top "+AllTrim(str(ordtam))+" BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
	cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' "
	cQuery += "ORDER BY R_E_C_N_O_"

	cQuery := ChangeQuery(cQuery)
	If Select("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

	DbSelectArea("TMPSBV")
	DbGoTop()
	While !EOF()
		If _TP == "C"
			_BVTAM := TMPSBV->BV_CHAVE
		Else
			_BVTAM := TMPSBV->BV_DESCRI
		Endif
		DbSkip()
	End

	DbCloseArea()

Return(_BVTAM)


Static Function rettam2(xTamSbv,xcoluna,_tp)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
	_BVTAM := ""

	cQuery  := "SELECT Top 1 BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
	cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' AND BV_DESCRI='"+xTamSbv+"'"
	cQuery += "ORDER BY R_E_C_N_O_"

	cQuery := ChangeQuery(cQuery)
	If Select("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

	DbSelectArea("TMPSBV")
	DbGoTop()
	While !EOF()
		If _TP == "C"
			_BVTAM := TMPSBV->BV_CHAVE
		Else
			_BVTAM := TMPSBV->BV_DESCRI
		Endif
		DbSkip()
	End

	DbCloseArea()

Return(_BVTAM)


Static Function VerCodBar(codBar)
// Rotina para verificar se o ultimo numero de codigo de barras e o correto.
//_codBar := ""

	cQuery  := "SELECT COUNT(B1_CODBAR) AS COUNT FROM "+RetSQLName("SB1")+" (nolock) "
	cQuery  += "WHERE D_E_L_E_T_='' AND B1_CODBAR LIKE '"+AllTrim(STR(codBar))+"%'"

	cQuery := ChangeQuery(cQuery)
	If Select("TMPSB1a") > 0
		TMPSB1a->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSB1a",.T.,.T.)
	DbSelectArea("TMPSB1a")

	If TMPSB1a->COUNT > 0

		cQuery  := " SELECT TOP 1 B1_CODBAR AS CODBAR FROM "+RetSQLName("SB1")+" (nolock) "
		cQuery  += " WHERE D_E_L_E_T_='' AND B1_CODBAR <>'' "
		cQuery  += " ORDER BY 1 DESC "
	
		cQuery := ChangeQuery(cQuery)
		If Select("TMPSB1b") > 0
			TMPSB1b->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSB1b",.T.,.T.)
		DbSelectArea("TMPSB1b")

		lRet := VAL(SubStr(AllTrim(TMPSB1b->CODBAR),1,12))+1

		TMPSB1b->(DbCloseArea())

	Else

		lRet := codBar

	End

	TMPSB1a->(DbCloseArea())

Return(lRet)


Static Function hCompo()
//Criar e Visualizar composi��o

	Local oButton1
	Local oButton2
	Local xopc		:= 0
	Local cSoma	:= 0
	Local cRet		:= ""
	Local cRet2	:= ""
	Local nX
	Static oDlx

	DEFINE MSDIALOG oDlx TITLE "Composi��o" FROM 000, 000  TO 350, 700 COLORS 0, 16777215 PIXEL

	fCompo()
	@ 143, 305 BUTTON oButton1 PROMPT "Ok" SIZE 037, 012 ACTION (xopc := 1,oDlx:End()) OF oDlx PIXEL
	@ 143, 263 BUTTON oButton2 PROMPT "Cancelar" SIZE 037, 012 ACTION (xopc := 0,oDlx:End()) OF oDlx PIXEL

	ACTIVATE MSDIALOG oDlx CENTERED

	If xopc == 1
		For nX := 1 to Len(oMSCompo:aCols)
			If OMSCompo:ACOLS[nX,len(OMSCompo:AHEADER)+1] == .F. .and. AllTrim(oMSCompo:aCols[nX,1])<>''
				If nx<>1
					cRet	+= ";"
					cRet2	+= ";"
				End

				cRet  += oMSCompo:aCols[nX,1]+"-"+strzero(val(oMSCompo:aCols[nX,3]),3)
				cRet2 += AllTrim(oMSCompo:aCols[nX,2])+"-"+strzero(val(oMSCompo:aCols[nX,3]),3)
			Endif
		Next
	ElseIf xopc == 0
		cRet	:= AllTrim(oMSGrade:aCols[oMSGrade:nat,3])
		cRet2	:= AllTrim(oMSGrade:aCols[oMSGrade:nat,4])
	Endif

	If AllTrim(cRet) == "-000"
		cRet	:=""
		cRet2	:=""
	End

/*	//retirado porque nao deve calcular o valor total da porcentagem
  	cSoma	:= 0	
	For nX := 1 to Len(oMSCompo:aCols)
		cSoma := cSoma + val(oMSCompo:aCols[nX,3])
		If cSoma > 100
			Alert("Revisar percentuais a soma ultrapassa 100%")
			Return(.F.)
		End
	Next
*/

	oMSGrade:aCols[oMSGrade:nat,3] := cRet
	oMSGrade:aCols[oMSGrade:nat,4] := cRet2
	oMsGrade:refresh()

Return

//------------------------------------------------
Static Function fCompo()
//------------------------------------------------
// Acols para composi��o
// ZAQ
// Utiliza estes campos (SF4) para n�o precisar criar uma tabela espec�fica para isso

	Local nX
	Local aHeaderEx 		:= {}
	Local aColsEx 		:= {}
	Local aFieldFill 		:= {}
	Local aFields 		:= {"F4_CODIGO","F4_TEXTO","F4_CFEXT"}
	Local aAlterFields 	:= {"F4_CODIGO","F4_CFEXT"}
	Static oMSCompo

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			If nX == 1
				Aadd(aHeaderEx, {"C�digo",SX3->X3_CAMPO,SX3->X3_PICTURE,/*TAMANHO*/5,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,"ZAQ",SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			ElseIf nX == 2
				Aadd(aHeaderEx, {"Descri��o",SX3->X3_CAMPO,SX3->X3_PICTURE,/*TAMANHO*/50,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,"",SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Else
				Aadd(aHeaderEx, {"Percentual",SX3->X3_CAMPO,SX3->X3_PICTURE,/*TAMANHO*/3,SX3->X3_DECIMAL,"VAL(M->F4_CFEXT) <= 100",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		Endif
	Next nX

  // Define field values
	If oMSGrade:aCols[oMSGrade:nat,3] = ""
		For nX := 1 to Len(aFields)
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
	Else
		For nX := 1 to len(alltrim(oMSGrade:aCols[oMSGrade:nat,3]))
			Aadd(aFieldFill, SubStr(oMSGrade:aCols[oMSGrade:nat,3],nx,5))
			Aadd(aFieldFill, POSICIONE("ZAQ",1,xFilial("ZAQ")+SubStr(oMSGrade:aCols[oMSGrade:nat,3],nx,5),"ZAQ_DESC"))
			Aadd(aFieldFill, Strzero(Val(SubStr(oMSGrade:aCols[oMSGrade:nat,3],nx+6,3)),3))
			Aadd(aFieldFill, .F.)

			Aadd(aColsEx, aFieldFill)
			aFieldFill := {}
			nX += 9
		Next
	Endif

	oMSCompo := MsNewGetDados():New( 008, 006, 130, 344, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlx, aHeaderEx, aColsEx)

Return

						
Static function VldCod()
// Rotina para cria��o de c�digo autom�tico do produto

	If Alltrim(cCodigo) == ""
		DbSelectArea("SBM")
		DbSetOrder(1)
		DbSeek(xfilial("SBM")+cGrupo)
		 
		_cod := Space(26)
		
		IF AllTrim(cGrupo)<>""
			_ini := cTipo+SBM->BM_YCODPRO
		Else
			_ini := Space(4)
		End
			
		IF !SubStr(_ini,1,2) $ "KT|PF|PL|ME|PI|SV|MO|  "
		
			cQuery := "select Top 1 B4_COD from "+RetSqlName("SB4")+" SB4 (nolock) "
			cQuery += "where SB4.D_E_L_E_T_ = '' "
			cQuery += "and B4_COD like '"+alltrim(_ini)+"%' "
			cQuery += "order by B4_COD desc "
	    
			cQuery := ChangeQuery(cQuery)
	
			If Select("TMP3") > 0
				TMP3->(dbclosearea())
			EndIf
		    
			DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),"TMP3",.F.,.F.)
		
			DbSelectArea("TMP3")
			DbGoTop()
			If EOF()
				_cod := _ini+"0001"
			Else
				_cod := _ini+Soma1(SubStr(TMP3->B4_COD,5,4))
			Endif
	
			TMP3->(dbclosearea())
		Endif
	Else
		_cod := cCodigo
	Endif
			
	cGrupoDesc		:= POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_DESC")
	cCodigo 		:= _cod

	If Alltrim(cGrupoDesc)==""
		oGrupo:SetFocus()
	End

	oDlg:Refresh()
return()

Static function VldCod2(vCod)
// Rotina para cria��o de c�digo autom�tico do produto

	cQuery := "SELECT COUNT(B1_COD) AS COUNT_COD FROM SB1010 (nolock) WHERE D_E_L_E_T_<>'*' AND B1_COD LIKE '"+Alltrim(vCod)+"%' "

	TCQuery cQuery New Alias "QRY_08"
	DbSelectArea("QRY_08")
	QRY_08->(DbGotop())
	vCodCount	:= QRY_08->COUNT_COD
	QRY_08->(DBCloseArea())
	
	If vCodCount >= 1
		Alert("Este codigo de produto j� foi utilizado, favor alterar")
	ElseIf len(AllTrim(vCod)) <> 8
		Alert("Corrigir quantidade de caracteres no codigo Codigo.")
		oCodigo:SetFocus()
	End

return()


Static Function dig13(_codb)
// C�lculo do d�gito verificador do C�digo de Barras EAN13

	_dig := ""
	_calc := Val(SubStr(_codb,1,1))*1 + Val(SubStr(_codb,2,1))*3 + Val(SubStr(_codb,3,1))*1 + Val(SubStr(_codb,4,1))*3 + Val(SubStr(_codb,5,1))*1 + Val(SubStr(_codb,6,1))*3 + Val(SubStr(_codb,7,1))*1 + Val(SubStr(_codb,8,1))*3 + Val(SubStr(_codb,9,1))*1 + Val(SubStr(_codb,10,1))*3 + Val(SubStr(_codb,11,1))*1 + Val(SubStr(_codb,12,1))*3
	_dig := ((Int(_calc/10)+1)*10)-_calc

	iF _dig =10
		_dig :=0
	End

Return(_dig)

User Function PrecPro(_tp)
// Rotina para forma��o de pre�o
	aEST := {}
	aEST2:= {}
	aEST3:= {}
	aEST4:= {}

	_xpar01 := ""
	If wperg == 0
		_xret := u_perg1()
		_xpar01 := SubStr(_xret,4,len(alltrim(_xret))-3)
		cTabela := SubStr(_xret,1,3)
		_wperg := 1
		u_prec1(_tp,_xpar01)
	Else
		u_prec1(_tp,_xpar01)
	Endif
	
Return

User Function prec1(_tp,_sta)

	Local cCabecalho

	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local oSay6
	Local oSay7
	Local oSay8

	Local oButton1
	Local oButton2

	Local oCodigo
	Local oDesc
	Local oTabela
	Local odescTab
	Local oDtDe
	Local oDtAte
	Local oVrMP
	Local oVrMO
	Local oVrMPA
	Local oVrMOA
	Local _i

	Private cCodigo 		:= SubString(SB4->B4_COD,1,15)
	Private cDesc 		:= SB4->B4_DESC
	Private cdescTab		:= Space(60)
	Private cDtDe			:= ddatabase
	Private cTipo		:= Space(10)
	Private cVrMP			:= 0
	Private cVrGeral		:= 0
	Private cVrGerAnt		:= 0
	Private cVrMO			:= 0
	Private cVrMPA		:= 0
	Private cVrMOA		:= 0
	Private cCusto		:= 0
	Private cCusA			:= 0

	Private cStatus		:= ""
	Private cStatu2		:= ""
	Private nAjuste 		:= 0
	Static oDlg1

	If _tp == 1
//		AjuSX1("HPRECO")
		_areaa := GetArea()
		_xpar01 := _sta
			For _i:=1 to len(_xPAR01) step 4
				If SubStr(_xPAR01,_i,1) <> "*"
					cStatus += "'"+SubStr(_xPAR01,_i,4)+"',"
					cStatu2 += SubStr(_xPAR01,_i,4)+"#"
				Endif
			Next

		RestArea(_areaa)
		cStatus := SubStr(cStatus,1,len(alltrim(cStatus))-1)
		cCabecalho := "Inclus�o da Forma��o de Pre�o de Produtos"
	ElseIf  _tp == 2
		cCabecalho := "Altera��o da Forma��o de Pre�o de Produtos"
	ElseIf  _tp == 3
		cCabecalho := "Visualizar da Forma��o de Pre�o de Produtos"
	End

	DEFINE MSDIALOG oDlg1 TITLE cCabecalho FROM 000, 000  TO 550, 1250 COLORS 0, 16777215 PIXEL

	@ 010, 012 SAY oSay1 PROMPT "C�digo" 				SIZE 021, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 008, 043 MSGET oCodigo VAR cCodigo 				SIZE 077, 010 OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL

	@ 010, 135 SAY oSay2 PROMPT "Descri��o" 			SIZE 025, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 008, 170 MSGET oDesc VAR cDesc 					SIZE 300, 010 OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 010, 532 SAY oSay7 PROMPT "Ajuste"				SIZE 025, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 008, 560 MSGET oAju VAR nAjuste 					SIZE 060, 010 PICTURE "@E 99.9" OF odlg1 COLORS 0, 16777215 		 	PIXEL

	@ 032, 012 SAY oSay3 PROMPT "Tabela" 				SIZE 021, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 030, 040 MSGET oTabela VAR cTabela 				SIZE 060, 010 OF odlg1 VALID !VAZIO() .AND. valTab(_tp, cTabela) COLORS 0, 16777215 	F3 "DA0" 	PIXEL
        
	@ 032, 115 SAY oSay4 PROMPT "Descri��o Tabela" 	SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 030, 162 MSGET odescTab VAR cdescTab 			SIZE 242, 010 OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL

	@ 032, 429 SAY oSay5 PROMPT "Data De" 			SIZE 021, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 030, 457 MSGET oDtDe VAR cDtDe	 				SIZE 060, 010 OF odlg1 VALID !VAZIO() COLORS 0, 16777215 			PIXEL
	@ 032, 532 SAY oSay6 PROMPT "Tipo" 			SIZE 021, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 030, 560 MSGET oDtAte VAR cTipo 				SIZE 060, 010 OF odlg1 VALID !VAZIO() COLORS 0, 16777215 		 	PIXEL

	
	@ 235, 012 SAY oSay7 PROMPT "Totais Ant:"			SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 255, 012 SAY oSay7 PROMPT "Totais Atu:"			SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 235, 044 SAY oSay8 PROMPT "MP:"					SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL

	@ 233, 065 MSGET oVrMPA VAR cVrMPA	 				SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 253, 065 MSGET oVrMP VAR cVrMP	 				SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 235, 155 SAY oSay8 PROMPT "MO:"					SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 233, 166 MSGET oVrMOA VAR cVrMOA 				SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 253, 166 MSGET oVrMO VAR cVrMO 					SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 235, 260 SAY oSay9 PROMPT "Ajuste:"				SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL

	@ 233, 279 MSGET oCustA VAR cCusA 					SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 253, 279 MSGET oCusto VAR cCusto 				SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
    
//    @ 255, 279 SAY oSay10 PROMPT "MO:"				SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 235, 410 SAY oSay11 PROMPT "Total:"				SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 233, 432 MSGET oVrGerA VAR cVrGerAnt			SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 253, 432 MSGET oVrGer VAR cVrGeral				SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL

	fMSPreco(_tp)
	@ 253, 520 BUTTON oButton1 PROMPT "Ok" 			SIZE 045, 012 ACTION (SavePrc(_tp),oDlg1:End()) 		OF oDlg1	PIXEL
	@ 253, 575 BUTTON oButton2 PROMPT "Cancelar" 	SIZE 045, 012 ACTION Close(oDlg1) OF oDlg1 	PIXEL

	ACTIVATE MSDIALOG oDlg1 CENTERED
	
Return

USer Function perg1()

Pergunte("HPRECO")

lret := MV_PAR02+MV_PAR01

Return(lret)
//------------------------------------------------
Static Function fMSPreco(_tp)
//------------------------------------------------
// Acols para forma��o de pre�o
// Tabela: SZI

	Local nX
	Local aHeader1 		:= {}
	Local aCols1 			:= {}
	Local aFieldFill 	:= {}
	Local aFields 		:= {"ZI_COMP","ZI_DESCCOM","ZI_UM","ZI_QTD","ZI_CANT","ZI_CATU","ZI_TOTAL","ZI_TOTANT"}
	Local aAlterFields 	:= {"ZI_CATU"}
	Local _vrmo  := 0
	Local _vrmp  := 0
	Local _vrmoa := 0
	Local _vrmpa := 0
	Local _i
	Static oMSPreco

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Else
			If aFields[nX]=="ZI_DESCCOM" .AND. SX3->(DbSeek("B1_DESC"))
				Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,70,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			ElseIf aFields[nX]=="ZI_UM" .AND. SX3->(DbSeek("B1_UM"))
				Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			ElseIf aFields[nX]=="ZI_QTD" .AND. SX3->(DbSeek("G1_QUANT"))
				Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			ElseIf aFields[nX]=="ZI_TOTAL" .AND. SX3->(DbSeek("ZI_CANT"))
				Aadd(aHeader1, {"Total ",SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			ElseIf aFields[nX]=="ZI_TOTANT" .AND. SX3->(DbSeek("ZI_CANT"))
				Aadd(aHeader1, {"Tot Ant",SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		Endif
	Next nX
  
	IF _tp = 1
	  // Query para c�lculo de m�dia de quantidades de Mat�ria Prima por grade

		_akit := {}
		_qtdprc := 0
	
		If SB4->B4_TIPO = "KT"
/*		
			_xcod := "in ('"
			DbSelectArea("SG1")
			DbSetOrder(1)
			DbSeek(xfilial("SG1")+SubStr(SB4->B4_COD,1,8))
			While !EOF() .and. SubStr(SG1->G1_COD,1,8) = SubStr(SB4->B4_COD,1,8)
				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+SG1->G1_COD)
				If SB1->B1_YSITUAC $ cStatu2
					_tem := ASCAN( _aKit, {|x| x[1] == SubStr(SG1->G1_COMP,1,8)})
					If _tem = 0
						aadd(_akit,{SubStr(SG1->G1_COMP,1,8)})
					Endif
				Endif
				DbSelectArea("SG1")
				DbSkip()
			End
			For _i := 1 to len(_akit)
				_xcod += _akit[_i,1]+"','"
			Next
			_xcod := SubStr(_xcod,1,len(_xcod)-2)+")"
*/
			_qry := "Select Count(QTD) as QTD from ( "
			_qry += "Select SB1.B1_COD as QTD from "+RetSqlName("SB1")+" SB1 " 
			_qry += "inner join "+RetSqlName("SG1")+" SG1 on SG1.D_E_L_E_T_ = '' and G1_COD = B1_COD and SG1.G1_FILIAL = '"+xfilial("SG1")+"' "
			_qry += "inner join "+RetSqlName("SB1")+" SB1A on SB1A.D_E_L_E_T_ = '' and G1_COMP = SB1A.B1_COD and SB1A.B1_YSITUAC in ("+cStatus+") and SB1A.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "inner join "+RetSqlName("SG1")+" SG1A on SG1A.D_E_L_E_T_ = '' and SG1A.G1_COD = SB1A.B1_COD and SG1A.G1_FILIAL = '"+xfilial("SG1")+"' "
			_qry += "where SB1.D_E_L_E_T_ = '' and left(SB1.B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' and SB1.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "group by SB1.B1_COD) as X "

			If Select("TMPPRC") > 0
				TMPPRC->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
			DbSelectArea("TMPPRC")
			DbGoTop()
			_qtdprc := TMPPRC->QTD
			TMPPRC->(DbCloseArea())

			_qry := "Select SB1.B1_REVATU, SB1.B1_COD, SG1A.G1_COMP, SG1A.G1_QUANT*SG1.G1_QUANT as G1_QUANT from "+RetSqlName("SB1")+" SB1 " 
			_qry += "inner join "+RetSqlName("SG1")+" SG1 on SG1.D_E_L_E_T_ = '' and G1_COD = B1_COD and SG1.G1_FILIAL = '"+xfilial("SG1")+"' AND G1_REVINI = B1_REVATU "
			_qry += "inner join "+RetSqlName("SB1")+" SB1A on SB1A.D_E_L_E_T_ = '' and G1_COMP = SB1A.B1_COD and SB1A.B1_YSITUAC in ("+cStatus+") and SB1A.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "inner join "+RetSqlName("SG1")+" SG1A on SG1A.D_E_L_E_T_ = '' and SG1A.G1_COD = SG1.G1_COMP and SG1A.G1_FILIAL = '"+xfilial("SG1")+"' "
			_qry += "inner join "+RetSqlName("SB1")+" SB1B on SB1B.D_E_L_E_T_ = '' and SB1B.B1_COD = SG1A.G1_COMP and SB1B.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "where SB1.D_E_L_E_T_ = '' and left(SB1.B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' and SB1.B1_FILIAL = '"+xfilial("SB1")+"' "

			If Select("TMPPRC") > 0
				TMPPRC->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
			DbSelectArea("TMPPRC")
			DbGoTop()

			While !EOF()
				dbSelectArea('SG1')
				DbSetOrder(1)
				MsSeek(xFilial('SG1')+TMPPRC->G1_COMP)
			
				If Found()
					MR225ExplG(TMPPRC->G1_COMP,TMPPRC->G1_QUANT,TMPPRC->B1_REVATU)
				Else
					DbSelectArea("DA1")
					DbSetOrder(1)
					DbSeek(xfilial("DA1")+cTabela+TMPPRC->G1_COMP)
					
					If !Found() .or. DA1->DA1_PRCVEN = 0
						MsgInfo("Produto "+TMPPRC->G1_COMP+" n�o possui pre�o cadastrado na tabela informada!","Diverg�ncia de Pre�o")
					Endif
				
					_tem := ASCAN( aEST, {|x| ALLTRIM(x[1]) == SubStr(ALLTRIM(TMPPRC->G1_COMP),1,8)})
					If _tem = 0
						Aadd(aEST, {substr(ALLTRIM(TMPPRC->G1_COMP),1,8),TMPPRC->G1_QUANT})
					Else
						aEST[_tem,2] += TMPPRC->G1_QUANT
					Endif
					Aadd(aEST2, {TMPPRC->G1_COMP,TMPPRC->B1_COD, TMPPRC->G1_QUANT})
				EndIf
			
				DbSelectArea("TMPPRC")
				DbSkip()
			End
			
			TMPPRC->(DbCloseArea())

		Else
//			_xcod := "= '"+SubStr(SB4->B4_COD,1,8)+"'"
//			_qry := "Select count(B1_COD) as QTD from "+RetSqlName("SB1")+" SB1 where SB1.D_E_L_E_T_ = '' and left(B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' and B1_YSITUAC in ("+cStatus+") and SB1.B1_FILIAL = '"+xfilial("SB1")+"'"
			_qry := "Select Count(B1_COD) as QTD from (Select B1_COD from "+RetSqlName("SB1")+" SB1 "
			_qry += "inner join "+RetSqlName("SG1")+" SG1 on SG1.D_E_L_E_T_ = '' and G1_COD = B1_COD and SG1.G1_FILIAL = '"+xfilial("SG1")+"' AND G1_REVINI = B1_REVATU "
			_qry += "where SB1.D_E_L_E_T_ = '' and left(B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' " 
			If alltrim(cstatus) <> "" 
				_qry += "and B1_YSITUAC in ("+cStatus+") "
			Endif
			_qry += "and SB1.B1_FILIAL = '"+xfilial("SB1")+"' " 
			_qry += "group by B1_COD) as x "

			If Select("TMPPRC") > 0
				TMPPRC->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
			DbSelectArea("TMPPRC")
			DbGoTop()
			_qtdprc := TMPPRC->QTD
			TMPPRC->(DbCloseArea())
			
			_qry := "Select B1_REVATU, G1_COD, G1_COMP, G1_QUANT from "+RetSqlName("SG1")+" SG1 " 
			_qry += "inner join "+RetSqlName("SB1")+" SB1 on SB1.D_E_L_E_T_ = '' and B1_COD = G1_COMP and SB1.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "where SG1.D_E_L_E_T_ = '' and SG1.G1_FILIAL = '"+xfilial("SG1")+"' and G1_REVINI+G1_COD in ( "
			_qry += "Select B1_REVATU+B1_COD from "+RetSqlName("SB1")+" SB1 where SB1.D_E_L_E_T_ = '' and left(B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' "
			If alltrim(cSTATUS) <> "" 
				_qry += "and B1_YSITUAC in ("+cStatus+") "
			Endif
			_qry += "and SB1.B1_FILIAL = '"+xfilial("SB1")+"' )"
//			_qry += "and B1_TIPO not in ('MO','BN') "

			If Select("TMPPRC") > 0
				TMPPRC->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
			DbSelectArea("TMPPRC")
			DbGoTop()

			While !EOF()
				dbSelectArea('SG1')
				DbSetOrder(1)
				MsSeek(xFilial('SG1')+TMPPRC->G1_COMP)
			
				If Found()
					MR225ExplG(TMPPRC->G1_COMP,TMPPRC->G1_QUANT,TMPPRC->B1_REVATU)
				Else
					DbSelectArea("DA1")
					DbSetOrder(1)
					DbSeek(xfilial("DA1")+cTabela+TMPPRC->G1_COMP)
					
					If !Found() .or. DA1->DA1_PRCVEN = 0
						MsgInfo("Produto "+TMPPRC->G1_COMP+" n�o possui pre�o cadastrado na tabela informada!","Diverg�ncia de Pre�o")
					Endif
				
					_tem := ASCAN( aEST, {|x| x[1] == SubStr(TMPPRC->G1_COMP,1,8)})
					If _tem = 0
						Aadd(aEST, {substr(TMPPRC->G1_COMP,1,8),TMPPRC->G1_QUANT})
					Else
						aEST[_tem,2] += TMPPRC->G1_QUANT
					Endif
					Aadd(aEST2, {TMPPRC->G1_COMP,TMPPRC->G1_COD,TMPPRC->G1_QUANT})
				EndIf
			
				DbSelectArea("TMPPRC")
				DbSkip()
			End
			
			TMPPRC->(DbCloseArea())
		Endif

/*
		If SB4->B4_TIPO <> "KT"

			_qry := "Select G1_COMP, sum(TOT) as TOT, B4_DESC, B4_UM from ( " 
			_qry += "sELECT G1_COMP AS G1_COMP, AVG(TOT) AS TOT FROM "
			_qry += "( Select left(SG1A.G1_COD,8) as COD, left(G1_COMP,8) as G1_COMP, Sum(G1_QUANT) /(Select Count(*) as Tot " 
			_qry += "from ( Select SG1D.G1_COD from "+RetSqlName("SG1")+" SG1D (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1B (nolock) on SB1B.D_E_L_E_T_ = '' and SB1B.B1_COD = SG1D.G1_COD " 
			If SB4->B4_TIPO <> "MI"
				_qry += "and B1_YSITUAC in ("+cStatus+") "
			Endif
			_qry += "where lEFT(SG1D.G1_COD,8) = LEFT(SG1A.G1_COD,8) and SG1D.D_E_L_E_T_ = '' " 
			_qry += "group by SG1D.G1_COD) as X) AS TOT " 
			_qry += "from "+RetSqlName("SG1")+" SG1A (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1 (nolock) on SB1.D_E_L_E_T_ = '' and SB1.B1_COD = SG1A.G1_COD "
			If SB4->B4_TIPO <> "MI"
				_qry += "and B1_YSITUAC in ("+cStatus+") "
			Endif 
			_qry += "inner join "+RetSqlName("SB1")+" SB1X (nolock) on SB1X.D_E_L_E_T_ = '' and SB1X.B1_COD = SG1A.G1_COMP "
			_qry += "where LEFT(G1_COD,8) "+_xcod+" and SG1A.D_E_L_E_T_ = '' and SB1X.B1_TIPO not in ('PI','MI') " 
			_qry += "Group by left(G1_COD,8), left(G1_COMP,8)) AS W " 
			_qry += "GROUP BY G1_COMP " 
			_qry += "union all "
			_qry += "sELECT G1_COMP, SUM(TOT) FROM ( "
			_qry += "Select left(X.G1_COMP,8) G1_COMP, (X.QTD/ "
			_qry += "(Select count(*) FROM ( "
			_qry += "SELECT SG1I.G1_COD "
			_qry += "from "+RetSqlName("SG1")+" SG1I (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1W (nolock) on SB1W.D_E_L_E_T_ = '' and SB1W.B1_COD = SG1I.G1_COD "
			If SB4->B4_TIPO <> "MI"
				_qry += "and SB1W.B1_YSITUAC IN ("+cStatus+") "
			Endif
			_qry += "WHERE SG1I.D_E_L_E_T_ = '' and LEFT(SG1I.G1_COD,8) "+_xcod+" GROUP BY SG1I.G1_COD ) AS T)) AS TOT "
			_qry += "from ( "
			If SB4->B4_TIPO = "MI"
				_qry += "Select G1_COD, SG1A.G1_COMP, (SELECT TOP 1 SG1X.G1_QUANT FROM "+RetSqlName("SG1")+" SG1X (nolock) WHERE SG1X.D_E_L_E_T_ = '' AND SG1X.G1_COMP = SG1A.G1_COD) * G1_QUANT as QTD "
			Else
				_qry += "Select G1_COD, SG1A.G1_COMP, G1_QUANT as QTD "
			Endif
			_qry += "from "+RetSqlName("SG1")+" SG1A (nolock) "
			_qry += "where SG1A.D_E_L_E_T_ = '' AND SG1A.G1_COD in ( Select SG1B.G1_COMP from "+RetSqlName("SG1")+" SG1B (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1 (nolock) on SB1.D_E_L_E_T_ = '' and SB1.B1_COD = SG1B.G1_COD "
			If SB4->B4_TIPO <> "MI"
				_qry += "and B1_YSITUAC in ("+cStatus+") "
			Endif 
			_qry += "inner join "+RetSqlName("SB1")+" SB1X (nolock) on SB1X.D_E_L_E_T_ = '' and SB1X.B1_COD = SG1B.G1_COMP " 
			_qry += "where "
			_qry += "SB1X.B1_TIPO in ('PI','MI') and "
			_qry += "SG1B.D_E_L_E_T_ = '' and LEFT(SG1B.G1_COD,8) "+_xcod+" " 
			_qry += "group by SG1B.G1_COMP) " 
			_qry += ") as X "
			_qry += "inner Join "+RetSqlName("SG1")+" SG1T (nolock) on SG1T.D_E_L_E_T_ = '' and X.G1_COD = SG1T.G1_COMP and LEFT(SG1T.G1_COD,8) "+_xcod+" " 
			_qry += "inner join "+RetSqlName("SB1")+" SB1Z (nolock) on SB1Z.D_E_L_E_T_ = '' and SB1Z.B1_COD = SG1T.G1_COD "
			If SB4->B4_TIPO <> "MI"
				_qry += "and SB1Z.B1_YSITUAC IN ("+cStatus+") "
			Endif
			_qry += ") AS Q "
			_qry += "group by Q.G1_COMP "
			_qry += ") as P "
			_qry += "inner join "+RetSqlName("SB4")+" SB4 (nolock) on SB4.D_E_L_E_T_ = '' and left(G1_COMP,8) = B4_COD " 
			_qry += "GROUP BY G1_COMP, B4_DESC, B4_UM " 
			_qry += "order by G1_COMP " 			
		eLSE
			_qry := "Select G1_COMP, sum(TOT) as TOT, B4_DESC, B4_UM from ( "
			_qry += "sELECT G1_COMP AS G1_COMP, sum(TOT) AS TOT FROM ( " 
			_qry += "Select left(SG1A.G1_COD,8) as COD, " 
			_qry += "left(G1_COMP,8) as G1_COMP "
			_qry += ",avg(G1_QUANT) as TOT "
			_qry += "from "+RetSqlName("SG1")+" SG1A (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1 (nolock) on SB1.D_E_L_E_T_ = '' and SB1.B1_COD = '"+SubStr(SB4->B4_COD,1,8)+"'+Right(SG1A.G1_COD,7) and SB1.B1_YSITUAC in ("+cStatus+") "
			_qry += "inner join "+RetSqlName("SB1")+" SB1X (nolock) on SB1X.D_E_L_E_T_ = '' and SB1X.B1_COD = SG1A.G1_COMP where LEFT(G1_COD,8) "+_xcod+" and "
			_qry += "SG1A.D_E_L_E_T_ = '' and SB1X.B1_TIPO not in ('PI','MI') "
			_qry += "Group by left(G1_COD,8), left(G1_COMP,8)) AS W GROUP BY G1_COMP " 
			_qry += "union all "
			_qry += "Select Q.G1_COMP, sum(TOTAL) as TOT from ( "
			_qry += "Select Left(X.G1_COD,8) as G1_COD, Left(X.G1_COMP,8) as G1_COMP, " 
			_qry += "avg(TOT) as TOTAL "
			_qry += "from "
			_qry += "(Select SG1A.G1_COD, SG1A.G1_COMP, SG1A.G1_QUANT "
			_qry += "*(Select X.QTD from ( "
			_qry += "Select top 1 right(G1_COD,7) as COD ,count(*) as QTD from "+RetSqlName("SG1")+" SG1Z (nolock) where SG1Z.D_E_L_E_T_ = '' and " 
			_qry += "left(SG1Z.G1_COD,8) "+_xcod+" and left(SG1Z.G1_COMP,8) = left(SG1A.G1_COD,8) "
			_qry += "group by right(G1_COD,7)) as X) as TOT "
			_qry += "from "+RetSqlName("SG1")+" SG1A (nolock) where SG1A.D_E_L_E_T_ = '' AND SG1A.G1_COD in ( " 
			_qry += "Select SG1B.G1_COMP from "+RetSqlName("SG1")+" SG1B (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1 (nolock) on SB1.D_E_L_E_T_ = '' and SB1.B1_COD = '"+SubStr(SB4->B4_COD,1,8)+"'+Right(SG1B.G1_COD,7) and SB1.B1_YSITUAC in ("+cStatus+") "
			_qry += "inner join "+RetSqlName("SB1")+" SB1X (nolock) on SB1X.D_E_L_E_T_ = '' and SB1X.B1_COD = SG1B.G1_COMP where SB1X.B1_TIPO in ('PI','MI') and SG1B.D_E_L_E_T_ = '' and " 
			_qry += "LEFT(SG1B.G1_COD,8) "+_xcod+" " 
			_qry += "group by SG1B.G1_COMP) " 
			_qry += ") as X "
			_qry += "group by left(X.G1_COD,8), Left(X.G1_COMP,8) " 
			_qry += ") as Q group by Q.G1_COMP "
			_qry += ") as P "
			_qry += "inner join "+RetSqlName("SB4")+" SB4 (nolock) on SB4.D_E_L_E_T_ = '' and left(G1_COMP,8) = B4_COD GROUP BY G1_COMP, B4_DESC, B4_UM order by G1_COMP" 		
		Endif
		
		MemoWrite("HESTP001_preco0.txt",_Qry)
		If Select("TMPSG1") > 0
			TMPSG1->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSG1",.T.,.T.)

		DbSelectArea("TMPSG1")
		DbGoTop()

		While !EOF()
		
			DbSelectArea("SG1")
			DbSetOrder(1)
			DbSeek(xfilial("SG1")+SubStr(TMPSG1->G1_COMP,1,8))

			IF Found()
				While !EOF() .and. SubStr(SG1->G1_COD,1,8) = SubStr(TMPSG1->G1_COMP,1,8)
					_ultprc := pegprg1(ccodigo,SubStr(SG1->G1_COMP,1,8),1,_tp,cDtDe,cTipo)
					_antprc := pegprg1(ccodigo,SubStr(SG1->G1_COMP,1,8),2,_tp,cDtDe,cTipo)
					DbSelectArea("SB4")
					DbSetOrder(1)
					DbSeek(xfilial("SB4")+SubStr(SG1->G1_COMP,1,8))

					_i := ASCAN( aCols1, {|x| SubStr(x[1],1,8) == SubStr(SG1->G1_COMP,1,8)})
	    		
					If _i = 0
						If _tp == 1
							Aadd(aCols1, {SubStr(SG1->G1_COMP,1,8),SB4->B4_DESC,SB4->B4_UM,TMPSG1->TOT*SG1->G1_QUANT,_ultprc,0,0,0,.F.})
						Else
							Aadd(aCols1, {SubStr(SG1->G1_COMP,1,8),SB4->B4_DESC,SB4->B4_UM,TMPSG1->TOT*SG1->G1_QUANT,_antprc,_ultprc,(TMPSG1->TOT*SG1->G1_QUANT)*_ultprc,(TMPSG1->TOT*SG1->G1_QUANT)*_antprc,.F.})
						Endif
					Endif
				
					DbSelectArea("SG1")
					DbSkip()
				End
			Else
*/

		DbSelectArea("SB4")
		_areaB4 := GetArea()

		For _t := 1 to len(aEst)
			DbSelectArea("SB4")
			DbSetOrder(1)
			DbSeek(xfilial("SB4")+aEst[_t,1])
				
			_ultprc := pegprg1(ccodigo,aEst[_t,1],1,_tp,cDtDe,cTipo)
			_antprc := pegprg1(ccodigo,aEst[_t,1],2,_tp,cDtDe,cTipo)
			If _tp == 1
				Aadd(aCols1, {aEst[_t,1],SB4->B4_DESC,SB4->B4_UM,round(aEst[_t,2]/_qtdprc,6),_ultprc,0,0,0,.F.})
			Else
				Aadd(aCols1, {aEst[_t,1],SB4->B4_DESC,SB4->B4_UM,round(aEst[_t,2]/_qtdprc,6),_antprc,_ultprc,round(aEst[_t,2]/_qtdprc,6)*_ultprc,round(aEst[_t,2]/_qtdprc,6)*_antprc,.F.})
			Endif
		Next
  
		RestArea(_areaB4)
//			DbSelectArea("TMPSG1")
//			DbSkip()
//		End
//		TMPSG1->(dbCloseArea())
  
	Else
		AjuSX1()
		Pergunte("HESTC1",.T.)
		_qry := "Select top 1 * from "+RetSqlName("SZI")+" (nolock) where D_E_L_E_T_ = '' and ZI_PRODUTO = '"+SB4->B4_COD+"' and ZI_TIPO = '"+MV_PAR01+"' order by ZI_DTDE desc "
	
		If Select("TMPSZI") > 0
			TMPSZI->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZI",.T.,.T.)
		DbSelectArea("TMPSZI")
		DbGoTop()

		_dtde := TMPSZI->ZI_DTDE
		DbCloseArea()
	
		_qry := "Select * from "+RetSqlName("SZI")+" (nolock) where D_E_L_E_T_ = '' and ZI_PRODUTO = '"+SB4->B4_COD+"' and ZI_TIPO = '"+MV_PAR01+"' and ZI_DTDE = '"+_dtde+"' order by ZI_PRODUTO "
	
		If Select("TMPSZI") > 0
			TMPSZI->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZI",.T.,.T.)
		DbSelectArea("TMPSZI")
		DbGoTop()
		_tipo := ""
		_ctab  := ""
		While !EOF()
			_tipo := TMPSZI->ZI_TIPO
			_ctab  := TMPSZI->ZI_TABELA
			DbSelectArea("SB4")
			_areaB4 := GetArea()
			DbSetOrder(1)
			DbSeek(Xfilial("SB4")+alltrim(TMPSZI->ZI_COMP))
  		
			Aadd(aCols1, {TMPSZI->ZI_COMP,SB4->B4_DESC,SB4->B4_UM,TMPSZI->ZI_QTD,TMPSZI->ZI_CANT,TMPSZI->ZI_CATU,TMPSZI->ZI_QTD*TMPSZI->ZI_CATU,TMPSZI->ZI_QTD*TMPSZI->ZI_CANT,.F.})
  	
			If SB4->B4_TIPO == "MO" .or. SB4->B4_TIPO == "BN"
				_vrmo += ROUND(TMPSZI->ZI_QTD*TMPSZI->ZI_CATU,6)
				_vrmoa+= ROUND(TMPSZI->ZI_QTD*TMPSZI->ZI_CANT,6)
			Else
				_vrmp += ROUND(TMPSZI->ZI_QTD*TMPSZI->ZI_CATU,6)
				_vrmpa+= ROUND(TMPSZI->ZI_QTD*TMPSZI->ZI_CANT,6)
			Endif
			RestArea(_areaB4)
			DbSelectArea("TMPSZI")
			DbSkip()
		End
		TMPSZI->(DbCloseArea())

		cVrMO := _vrmo		//oDlg:cVrMO := str(_vrmo)
		cVrMP := _vrmp		//oDlg:cVrMP := str(_vrmp)
		cVrMOA:= _vrmoa		//oDlg:cVrMO := str(_vrmo)
		cVrMPA:= _vrmpa		//oDlg:cVrMP := str(_vrmp)
		cCusto := ROUND((_vrmo*(najuste/100))+(_vrmP*(najuste/100)),6)		//oDlg:cVrMO := str(_vrmo)
		cCusA  := ROUND((_vrmoa*(najuste/100))+(_vrmPa*(najuste/100)),6)
		cVrGeral:= _vrmo+_vrmp+cCusto
		cVrGerAnt:= cVrMOA+cVrMPA+cCusA
		cTabela			:= _ctab
		cdescTab		:= POSICIONE("DA0",1,xFilial("DA0")+_ctab,"DA0_DESCRI")
		cTipo			:= _tipo
		cDtDe			:= stod(_dtde)
		oDlg1:refresh()
  	
	Endif

	oMSPreco := MsNewGetDados():New( 050, 005, 225, 624, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg1, aHeader1, aCols1)
  
Return

Static Function pegprg1(ccodigo,_COMP,_tpprc,_tp,cDtDe,cTipo)
// Busca o Pre�o dos produtos para forma��o de pre�o

	_prc	:= 0
  
  // Pega o �ltimo pre�o
	_qry := "Select Top 1 * from  "+RetSqlName("SZI")+" SZI (nolock) where SZI.D_E_L_E_T_ = '' and "
	_qry += "ZI_COMP = '"+_COMP+"' and ZI_PRODUTO = '"+cCodigo+"' " //and ZI_TABELA = '"+cTabela+"' "
//  If _tp <> 1
//  	_qry += "and ZI_DTDE <= '"+Dtos(Date())+"' and ZI_DTATE >= '"+Dtos(Date())+"' " 
//  Endif
	_qry += "order by R_E_C_N_O_ desc "

	If Select("TMPSZI") > 0
		TMPSZI->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZI",.T.,.T.)
	DbSelectArea("TMPSZI")
	DbGoTop()

	IF _tpprc == 1
		_prc := TMPSZI->ZI_CATU
	Else
		_prc := TMPSZI->ZI_CANT
	Endif
  
	TMPSZI->(dbCloseArea())
  
Return(_prc)

Static Function valTab(_tp, cTabela)
// Valida tabela selecionada e atualiza os pre�os

	Local _i	:= 0
	Local _vrmo	:= 0
	Local _vrmp	:= 0
	Local _vrmoa:= 0
	Local _vrmpa:= 0
	Local _i

	DbSelectArea("SB4")
	DbSetOrder(1)
	DbSeek(xfilial("SB4")+SubSt(cCodigo,1,8))

	If SB4->B4_TIPO = "KT"
		_xcod := "in ('"
		DbSelectArea("SG1")
		DbSetOrder(1)
		DbSeek(xfilial("SG1")+SubStr(SB4->B4_COD,1,8))
		_akit := {}
		While !EOF() .and. SubStr(SG1->G1_COD,1,8) = SubStr(SB4->B4_COD,1,8)
			_tem := ASCAN( _aKit, {|x| x[1] == SubStr(SG1->G1_COMP,1,8)})
			If _tem = 0
				aadd(_akit,{SubStr(SG1->G1_COMP,1,8)})
			Endif
			DbSelectArea("SG1")
			DbSkip()
		End
		For _i := 1 to len(_akit)
			_xcod += _akit[_i,1]+"','"
		Next
		_xcod := SubStr(_xcod,1,len(_xcod)-2)+")"
	Else
		_xcod := "= '"+SubStr(SB4->B4_COD,1,8)+"'"
	Endif

	If SB4->B4_TIPO <> "KT"

		_qry := "SELECT * FROM (  "
		_qry += "SELECT LEFT(G1_COMP,8) as PRODUTO, AVG(DA1_PRCVEN) as PRECO FROM "+RetSqlName("SG1")+" SG1 (nolock) "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1 (nolock) ON SB1.D_E_L_E_T_ = '' AND SB1.B1_COD = G1_COD "
		If alltrim(cStatus) <> ""
			_qry += "AND SB1.B1_YSITUAC in ("+cStatus+") "
		Endif
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1A (nolock) ON SB1A.D_E_L_E_T_ = '' AND SB1A.B1_COD = G1_COMP AND SB1A.B1_TIPO NOT in ('PI','MI') "
		_qry += "INNER JOIN "+RetSqlName("DA1")+" DA1 (nolock) ON DA1.D_E_L_E_T_ = '' AND DA1_CODTAB = '"+cTabela+"' AND DA1_CODPRO = G1_COMP "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(G1_COD,8) "+_xcod+" "
		_qry += "GROUP BY LEFT(SG1.G1_COMP,8) "
		_qry += "UNION ALL "
		_qry += "SELECT LEFT(SG1A.G1_COMP,8) as PRODUTO, AVG(DA1_PRCVEN) as PRECO FROM "+RetSqlName("SG1")+" SG1 (nolock) "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1 (nolock) ON SB1.D_E_L_E_T_ = '' AND SB1.B1_COD = SG1.G1_COD "
		If alltrim(cStatus) <> ""
			_qry += "AND SB1.B1_YSITUAC in ("+cStatus+") "
		Endif
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1A (nolock) ON SB1A.D_E_L_E_T_ = '' AND SB1A.B1_COD = SG1.G1_COMP AND SB1A.B1_TIPO in ('PI','MI') "
		_qry += "INNER JOIN "+RetSqlName("SG1")+" SG1A (nolock) ON SG1A.D_E_L_E_T_ = '' AND SG1.G1_COMP = SG1A.G1_COD "
		_qry += "INNER JOIN "+RetSqlName("DA1")+" DA1 (nolock) ON DA1.D_E_L_E_T_ = '' AND DA1_CODTAB = '"+cTabela+"' AND DA1_CODPRO = SG1A.G1_COMP "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(SG1.G1_COD,8) "+_xcod+" "
		_qry += "GROUP BY LEFT(SG1A.G1_COMP,8) "
		_qry += ") AS X "
		_qry += "ORDER BY PRODUTO "
/*
		_qry := "Select Count(QTD) as QTD from ( "
		_qry += "Select SB1.B1_COD as QTD from "+RetSqlName("SB1")+" SB1 " 
		_qry += "inner join "+RetSqlName("SG1")+" SG1 on SG1.D_E_L_E_T_ = '' and G1_COD = B1_COD and SG1.G1_FILIAL = '"+xfilial("SG1")+"' "
		_qry += "inner join "+RetSqlName("SB1")+" SB1A on SB1A.D_E_L_E_T_ = '' and G1_COMP = SB1A.B1_COD and SB1A.B1_YSITUAC in ("+cStatus+") and SB1A.B1_FILIAL = '"+xfilial("SB1")+"' "
		_qry += "where SB1.D_E_L_E_T_ = '' and left(SB1.B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' and SB1.B1_FILIAL = '"+xfilial("SB1")+"' "
		_qry += "group by SB1.B1_COD) as X "

		If Select("TMPPRC") > 0
			TMPPRC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
		DbSelectArea("TMPPRC")
		DbGoTop()
		_qtdprc := TMPPRC->QTD
		TMPPRC->(DbCloseArea())

		_qry := "Select SB1.B1_COD, SG1A.G1_COMP, SG1A.G1_QUANT*SG1.G1_QUANT as G1_QUANT from "+RetSqlName("SB1")+" SB1 " 
		_qry += "inner join "+RetSqlName("SG1")+" SG1 on SG1.D_E_L_E_T_ = '' and G1_COD = B1_COD and SG1.G1_FILIAL = '"+xfilial("SG1")+"' "
		_qry += "inner join "+RetSqlName("SB1")+" SB1A on SB1A.D_E_L_E_T_ = '' and G1_COMP = SB1A.B1_COD and SB1A.B1_YSITUAC in ("+cStatus+") and SB1A.B1_FILIAL = '"+xfilial("SB1")+"' "
		_qry += "inner join "+RetSqlName("SG1")+" SG1A on SG1A.D_E_L_E_T_ = '' and SG1A.G1_COD = SG1.G1_COMP and SG1A.G1_FILIAL = '"+xfilial("SG1")+"' "
		_qry += "inner join "+RetSqlName("SB1")+" SB1B on SB1B.D_E_L_E_T_ = '' and SB1B.B1_COD = SG1A.G1_COMP and SB1B.B1_FILIAL = '"+xfilial("SB1")+"' "
		_qry += "where SB1.D_E_L_E_T_ = '' and left(SB1.B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' and SB1.B1_FILIAL = '"+xfilial("SB1")+"' "

		If Select("TMPPRC") > 0
			TMPPRC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
		DbSelectArea("TMPPRC")
		DbGoTop()

		While !EOF()
			dbSelectArea('SG1')
			DbSetOrder(1)
			MsSeek(xFilial('SG1')+TMPPRC->G1_COMP)
			
			If Found()
				MP225ExplG(TMPPRC->G1_COMP,TMPPRC->G1_QUANT)
			Else
				_tem := ASCAN( aEST3, {|x| x[1] == substr(TMPPRC->G1_COMP,1,8) })
				If _tem = 0
					Aadd(aEST3, {substr(TMPPRC->G1_COMP,1,8),POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")})
				Else
					aEST3[_tem,2] += POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")
				Endif
				Aadd(aEST4, {TMPPRC->G1_COMP,TMPPRC->B1_COD, POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")})
					
			EndIf
			
			DbSelectArea("TMPPRC")
			DbSkip()
		End
			
		TMPPRC->(DbCloseArea())
*/
	ELSE
	
		_qry := "Select left(G1_COMP,8) as PRODUTO, Preco2 as PRECO from ( "
		_qry += "Select Left(G1_COMP,8) as G1_COMP, Avg(Preco2) as Preco2 from ( "
		_qry += "Select isnull(DA1_PRCVEN,0) as precoa, G1_QUANT, isnull(DA1_PRCVEN,0)*G1_QUANT as Preco, G1_COMP, "
		_qry += "(isnull(DA1_PRCVEN,0)) "
		_qry += "as Preco2 "
		_qry += "from "+RetSqlName("SG1")+" SG1 WITH (NOLOCK) "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1X WITH (NOLOCK) ON SB1X.D_E_L_E_T_ = '' AND SG1.G1_COD = SB1X.B1_COD " // AND SB1X.B1_YSITUAC in ("+cStatus+") "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1W WITH (NOLOCK) ON SB1W.D_E_L_E_T_ = '' AND SG1.G1_COMP = SB1W.B1_COD "
		_qry += "left join "+RetSqlName("DA1")+" DA1 WITH (NOLOCK) on DA1.D_E_L_E_T_ = '' and DA1_CODTAB = '"+cTabela+"' and DA1_CODPRO = G1_COMP "
		_qry += "where SG1.D_E_L_E_T_ = '' and LEFT(G1_COD,8) "+_xcod+" and SB1W.B1_TIPO NOT in ('PI','MI') "
		_qry += ") as X "
		_qry += "group by left(G1_COMP,8) "
		_qry += ") as w "
		_qry += "group by left(G1_COMP,8), Preco2 "
		_qry += "union all "
		_qry += "Select left(G1_COMP,8), avg(Preco2) from ( "
		_qry += "Select (isnull(DA1_PRCVEN,0)) as Preco2, "
		_qry += "G1_COMP from ( "
		_qry += "Select "
		_qry += "(Select Count(*) from "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) where SB1.D_E_L_E_T_ = '' " // and B1_YSITUAC in ("+cStatus+") "
		_qry += "and LEFT(B1_COD,8) "+_xcod+"  and (Select top 1 G1_COD from "+RetSqlName("SG1")+" SG1D WITH (NOLOCK) where SG1D.D_E_L_E_T_ = '' and SG1D.G1_COD = B1_COD) is not null) as qtditem "
		_qry += ",G1_COMP, "
		_qry += "(Select Top 1 SG1B.G1_QUANT from "+RetSqlName("SG1")+" SG1B WITH (NOLOCK) where SG1B.D_E_L_E_T_ = '' and SG1B.G1_COMP = SG1A.G1_COD ) * G1_QUANT as Quant "
		_qry += "from "+RetSqlName("SG1")+" SG1A WITH (NOLOCK) "
		_qry += "where SG1A.D_E_L_E_T_ = '' and SG1A.G1_COD in ( "
		_qry += "Select G1_COMP "
		_qry += "from "+RetSqlName("SG1")+" SG1 WITH (NOLOCK) "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1X WITH (NOLOCK) ON SB1X.D_E_L_E_T_ = '' AND SG1.G1_COD = SB1X.B1_COD " //AND SB1X.B1_YSITUAC in ("+cStatus+") "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1W WITH (NOLOCK) ON SB1W.D_E_L_E_T_ = '' AND SG1.G1_COMP = SB1W.B1_COD "
		_qry += "where SG1.D_E_L_E_T_ = '' and LEFT(G1_COD,8) "+_xcod+" and SB1W.B1_TIPO in ('PI','MI') ) "
		_qry += ") as w "
		_qry += "left join "+RetSqlName("DA1")+" DA1 WITH (NOLOCK) on DA1.D_E_L_E_T_ = '' and DA1_CODTAB = '"+cTabela+"' and DA1_CODPRO = G1_COMP "
		_qry += ") as t "
		_qry += "group by Left(G1_COMP,8) order by left(G1_COMP,8) "
/*		
		_qry := "Select count(B1_COD) as QTD from "+RetSqlName("SB1")+" SB1 where SB1.D_E_L_E_T_ = '' and left(B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' and B1_YSITUAC in ("+cStatus+") and SB1.B1_FILIAL = '"+xfilial("SB1")+"'"

		If Select("TMPPRC") > 0
			TMPPRC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
		DbSelectArea("TMPPRC")
		DbGoTop()
		_qtdprc := TMPPRC->QTD
		TMPPRC->(DbCloseArea())
			
		_qry := "Select G1_COD, G1_COMP, G1_QUANT from "+RetSqlName("SG1")+" SG1 " 
		_qry += "inner join "+RetSqlName("SB1")+" SB1 on SB1.D_E_L_E_T_ = '' and B1_COD = G1_COMP and SB1.B1_FILIAL = '"+xfilial("SB1")+"' "
		_qry += "where SG1.D_E_L_E_T_ = '' and SG1.G1_FILIAL = '"+xfilial("SG1")+"' and G1_COD in ( "
		_qry += "Select B1_COD from "+RetSqlName("SB1")+" SB1 where SB1.D_E_L_E_T_ = '' and left(B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' and B1_YSITUAC in ("+cStatus+")) and SB1.B1_FILIAL = '"+xfilial("SB1")+"'"

		If Select("TMPPRC") > 0
			TMPPRC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
		DbSelectArea("TMPPRC")
		DbGoTop()

		While !EOF()
			dbSelectArea('SG1')
			DbSetOrder(1)
			MsSeek(xFilial('SG1')+TMPPRC->G1_COMP)
			
			If Found()
				MP225ExplG(TMPPRC->G1_COMP,TMPPRC->G1_QUANT)
			Else
				_tem := ASCAN( aEST3, {|x| x[1] == SubStr(TMPPRC->G1_COMP,1,8)})
				If _tem = 0
					Aadd(aEST3, {substr(TMPPRC->G1_COMP,1,8),POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")})
				Else
					aEST3[_tem,2] += POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")
				Endif
				Aadd(aEST4, {TMPPRC->G1_COMP,TMPPRC->G1_COD,POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")})
			EndIf
			
			DbSelectArea("TMPPRC")
			DbSkip()
		End
			
		TMPPRC->(DbCloseArea())
*/
	ENDIF
	cQuery := ChangeQuery(_qry)

	MemoWrite("HESTP001_preco1.txt",cQuery)
	If Select("TMPPRC") > 0
		TMPPRC->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)

	cdescTab		:= POSICIONE("DA0",1,xFilial("DA0")+cTabela,"DA0_DESCRI")

	DbSelectArea("TMPPRC")
	DbGoTop()
	While !EOF()

//	For _t := 1 to len(aEST3)
//		_i := ASCAN( OMSPreco:ACOLS, {|x| SubStr(x[1],1,8) == SubStr(aEST3[_t,1],1,8)})
		_i := ASCAN( OMSPreco:ACOLS, {|x| SubStr(x[1],1,8) == SubStr(TMPPRC->PRODUTO,1,8)})
		_prc := 0

		If _i > 0
//			_prc := round(aEst3[_t,2] / _qtdprc,6) 
			_prc := TMPPRC->PRECO
			
			IF _tp <> 1
				OMSPreco:ACols[_i,5] := pegprg1(ccodigo,OMSPreco:ACOLS[_i,1],1,_tp,Dtos(Date()),Dtos(Date()))
				OMSPreco:ACols[_i,6] := _prc //Posicione("DA1",1,xfilial("DA1")+cTabela+OMSPreco:aCols[_i,1],"DA1_PRCVEN")
//				OMSPreco:ACols[_i,7] := ROUND(Posicione("DA1",1,xfilial("DA1")+cTabela+OMSPreco:aCols[_i,1],"DA1_PRCVEN")*OMSPreco:aCols[_i,4],6)
				OMSPreco:ACols[_i,7] := ROUND(_prc*OMSPreco:aCols[_i,4],6)
				OMSPreco:ACols[_i,8] := ROUND(OMSPreco:ACols[_i,5]*OMSPreco:aCols[_i,4],6)
			Else
			//OMSPreco:ACols[_i,5] := 0
				OMSPreco:ACols[_i,6] := _prc
				OMSPreco:ACols[_i,7] := ROUND(_prc*OMSPreco:aCols[_i,4],6)
				OMSPreco:ACols[_i,8] := ROUND(OMSPreco:aCols[_i,5]*OMSPreco:aCols[_i,4],6)
			Endif

			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+OMSPreco:aCols[_i,1])
		
			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmo += OMSPreco:ACols[_i,7]
			Else
				_vrmp += OMSPreco:ACols[_i,7]
			Endif
			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmoa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			Else
				_vrmpa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			Endif
		Endif

		DbSelectArea("TMPPRC")
		DbSkip()
	End
//	Next
	DbSelectArea("TMPPRC")
	DbCloseArea()
	_vrmo := 0 
	_vrmp := 0
	_vrmoa:= 0
	_vrmpa:= 0
	
	For _i := 1 to len(OMSPreco:ACols)
	
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+OMSPreco:aCols[_i,1])
	
		If OMSPreco:ACols[_i,6] = 0
			_prc := Posicione("DA1",1,xfilial("DA1")+cTabela+OMSPreco:aCols[_i,1],"DA1_PRCVEN")
			OMSPreco:ACols[_i,6] := _prc
			OMSPreco:ACols[_i,7] := ROUND(_prc*OMSPreco:aCols[_i,4],6)
			OMSPreco:ACols[_i,8] := ROUND(OMSPreco:aCols[_i,5]*OMSPreco:aCols[_i,4],6)

			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmo += OMSPreco:ACols[_i,7]
			Else
				_vrmp += OMSPreco:ACols[_i,7]
			Endif
			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmoa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			Else
				_vrmpa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			Endif
		Else
			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmo += OMSPreco:ACols[_i,7]
			Else
				_vrmp += OMSPreco:ACols[_i,7]
			Endif
			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmoa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			Else
				_vrmpa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			Endif
		Endif
	Next

	oMSPreco:Refresh()

	cVrMO := _vrmo		//oDlg:cVrMO := str(_vrmo)
	cVrMP := _vrmp		//oDlg:cVrMP := str(_vrmp)
	cVrMOA:= _vrmoa		//oDlg:cVrMO := str(_vrmo)
	cVrMPA:= _vrmpa		//oDlg:cVrMP := str(_vrmp)
	cCusto := ROUND((_vrmo*(najuste/100))+(_vrmP*(najuste/100)),6)		//oDlg:cVrMO := str(_vrmo)
	cCusA  := ROUND((_vrmoa*(najuste/100))+(_vrmPa*(najuste/100)),6)
	cVrGeral:= _vrmo+_vrmp+cCusto
	cVrGerAnt:= cVrMOA+cVrMPA+cCusA


//cVrMO := _vrmo*(1+(najuste/100))		//oDlg:cVrMO := str(_vrmo)
//cVrMP := _vrmp*(1+(najuste/100))		//oDlg:cVrMP := str(_vrmp)
//cVrMOA:= _vrmoa		//oDlg:cVrMO := str(_vrmo)
//cVrMPA:= _vrmpa		//oDlg:cVrMP := str(_vrmp)
//cVrGeral:= cVrMO+cVrMP
	oDlg1:refresh()

Return


Static Function SavePrc(_tp)
// Salva forma��o de pre�o cadastrada

	Local I

	If Alltrim(cTabela)<>"" .and. (cDtDe <= Date() .and. cDtDe <> ctod("  /  /    "))
		_custd := MsgYesNo("Atualiza Custo Standard? ","Atualiza Custo Standard")
		_custo := 0
		If _tp == 1
			For i := 1 to Len(OMSPreco:ACOLS)
				If OMSPreco:ACOLS[i,len(OMSPreco:AHEADER)+1] == .F.
					_custo += OMSPreco:ACOLS[i,4] * OMSPreco:ACOLS[i,6]
					DbSelectArea("SZI")
					DbSetOrder(1)
					DbSeek(xfilial("SZI")+cCodigo+DTOS(cDtDe)+PADR(cTipo,10)+cTabela+OMSPreco:ACOLS[i,1])
	
					If !Found()
						RecLock("SZI",.T.)
						Replace ZI_FILIAL			with xfilial("SZI")
						Replace ZI_PRODUTO		with cCodigo
						Replace ZI_DTDE			with cDtDe
						Replace ZI_TIPO			with cTipo
						Replace ZI_TABELA			with cTabela
						Replace ZI_COMP			with OMSPreco:ACOLS[i,1]
						Replace ZI_QTD			with OMSPreco:ACOLS[i,4]
						Replace ZI_CANT			with OMSPreco:ACOLS[i,5]
						Replace ZI_CATU			with OMSPreco:ACOLS[i,6]
						MsUnLock()
					Else
						RecLock("SZI",.F.)
						Replace ZI_TIPO			with cTipo
						Replace ZI_QTD			with OMSPreco:ACOLS[i,4]
						Replace ZI_CANT			with OMSPreco:ACOLS[i,5]
						Replace ZI_CATU			with OMSPreco:ACOLS[i,6]
						MsUnLock()
					Endif
				Endif
			Next
		ElseIf _tp == 2
			For i := 1 to Len(OMSPreco:ACOLS)
				If OMSPreco:ACOLS[i,len(OMSPreco:AHEADER)+1] == .F.
					_custo += OMSPreco:ACOLS[i,4] * OMSPreco:ACOLS[i,6]
					DbSelectArea("SZI")
					DbSetOrder(1)
					DbSeek(xfilial("SZI")+cCodigo+DTOS(cDtDe)+PADR(cTipo,10)+cTabela+OMSPreco:ACOLS[i,1])
	
					If !Found()
						RecLock("SZI",.T.)
						Replace ZI_FILIAL			with xfilial("SZE")
						Replace ZI_PRODUTO		with cCodigo
						Replace ZI_DTDE			with cDtDe
						Replace ZI_TIPO			with ctipo
						Replace ZI_TABELA			with cTabela
						Replace ZI_COMP			with OMSPreco:ACOLS[i,1]
						Replace ZI_QTD			with OMSPreco:ACOLS[i,4]
						Replace ZI_CANT			with OMSPreco:ACOLS[i,5]
						Replace ZI_CATU			with OMSPreco:ACOLS[i,6]
						MsUnLock()
					Else
						RecLock("SZI",.F.)
						Replace ZI_TIPO			with cTipo
						Replace ZI_QTD			with OMSPreco:ACOLS[i,4]
						Replace ZI_CANT			with OMSPreco:ACOLS[i,5]
						Replace ZI_CATU			with OMSPreco:ACOLS[i,6]
						MsUnLock()
					Endif
				Endif
			Next
		Endif
		If _custd
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+alltrim(cCodigo))
		
			While !EOF() .and. SubStr(SB1->B1_COD,1,8) == SubStr(cCodigo,1,8)
				IF SB1->B1_YSITUAC $ cStatu2 .or. SB1->B1_TIPO = "MI"
					RecLock("SB1",.f.)
					Replace B1_CUSTD 	with cVrGeral //_custo
					IF SB1->B1_TIPO = "MI"
						Replace B1_PRV1 	with cVrGeral //_custo					
					Endif
					MsUnLock()
					u_HPDISPLJ(SB1->B1_COD)
				Endif
				DbSelectArea("SB1")
				DbSkip()
			End
		Endif
	Else
		If Alltrim(cTabela)==""
			Alert("Favor incluir o codigo da tabela a ser utilizada!")
			//return(oDlg1)
			return()
		End
		If 	cDtDe > Date() .or. cDtDe == ctod("  /  /    ")
			Alert("Data De deve ser inferior a "+ dtoc(Date()) +" !")
			//return(oDlg1)
			return()
		End
	Endif

Return

User Function EstPro1(_tp)
// Cadastro de estrutura de produtos - Controle de vers�o

	Local oCancela
	Local oCodProd
	Local oDescProd
	Local cDescProd := SB4->B4_DESC
	Local oOk
	Local oSay1
	Private cCodProd := SB4->B4_COD
	Static oDlg

	Public cColuna		:= Space(03)
	Public cColunx		:= Space(03)
	Public cCor			:= Space(500)
	Public OMSRevisao

	cColuna := SB4->B4_COLUNA
	DEFINE MSDIALOG oDlg TITLE "Controle de Vers�o" FROM 000, 000  TO 300, 800 COLORS 0, 16777215 PIXEL

	fMSRevisao()
	@ 011, 007 SAY oSay1 PROMPT "Produto" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 010, 042 MSGET oCodProd VAR cCodProd SIZE 066, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
	@ 010, 113 MSGET oDescProd VAR cDescProd SIZE 278, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 124, 332 BUTTON oOk PROMPT "Seleciona" SIZE 037, 012 ACTION Seleciona(_tp, OMSRevisao:ACOLS[OMSRevisao:nat,1]) OF oDlg PIXEL
	@ 124, 289 BUTTON oCancela PROMPT "Cancela" SIZE 037, 012 ACTION oDlg:End() OF oDlg PIXEL
	@ 124, 246 BUTTON oInclui PROMPT "Salva" SIZE 037, 012 ACTION Salva() OF oDlg PIXEL
    
	ACTIVATE MSDIALOG oDlg CENTERED

Return

//------------------------------------------------ 
Static Function fMSRevisao()
//------------------------------------------------ 
// Acols da Revis�o

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"G5_REVISAO","G5_OBS","G5_STATUS"}
	Local aAlterFields := {"G5_REVISAO","G5_OBS","G5_STATUS"}

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	DbSelectarea("SG5")
	DbSetOrder(1)
	DbSeek(xfilial("SG5")+alltrim(SB4->B4_COD))
  
	If Found()
		_cod := SG5->G5_PRODUTO
		While !EOF() .and. SG5->G5_PRODUTO == _cod
			Aadd(aColsEx, {SG5->G5_REVISAO, SG5->G5_OBS,SG5->G5_STATUS,.f.})
			DbSelectArea("SG5")
			DbSkip()
		End
	Else
		Aadd(aColsEx, {"001","REVISAO INICIAL","1",.F.})
	Endif

	oMSRevisao := MsNewGetDados():New( 028, 003, 106, 394, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return

Static Function Salva
// Salva uma nova revis�o

	Local _i

	For _i := 1 to Len(OMSRevisao:ACOLS)
		If OMSRevisao:ACOLS[_i,len(OMSRevisao:AHEADER)+1] == .F.
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+SubStr(SB4->B4_COD,1,8))
		
			While !EOF() .and. SubStr(SB1->B1_COD,1,8) == SubStr(SB4->B4_COD,1,8)
				IF OMSRevisao:ACOLS[_i,3] = "1"
					RecLock("SB1",.F.)
						Replace B1_REVATU 	with OMSRevisao:ACOLS[_i,1]
					MsUnLock()
				Endif
				
				DbSelectArea("SG5")
				DbSetOrder(1)
				DbSeek(xfilial("SG5")+SB1->B1_COD+OMSRevisao:ACOLS[_i,1])
			
				If !Found()

					Reclock("SG5",.T.)
					G5_FILIAL		:= xfilial("SG5")
					G5_PRODUTO		:= SB1->B1_COD
					G5_REVISAO		:= OMSRevisao:ACOLS[_i,1]
					G5_OBS			:= OMSRevisao:ACOLS[_i,2]
					G5_USER			:= __cUserID
					G5_DATAVIG		:= dDatabase
					G5_STATUS		:= OMSRevisao:ACOLS[_i,3]
					MsUnLock()
				Else
					RecLock("SG5",.F.)
					Replace G5_OBS			with OMSRevisao:ACOLS[_i,2]
					Replace G5_STATUS		with OMSRevisao:ACOLS[_i,3]
					MsUnLock()
				Endif
				DbSelectArea("SB1")
				DbSkip()
			End
			
			
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+"B"+SubStr(SB4->B4_COD,1,7))
		
			While !EOF() .and. SubStr(SB1->B1_COD,1,8) == "B"+SubStr(SB4->B4_COD,1,7)
				IF OMSRevisao:ACOLS[_i,3] = "1"
					RecLock("SB1",.F.)
						Replace B1_REVATU 	with OMSRevisao:ACOLS[_i,1]
					MsUnLock()
				Endif
				
				DbSelectArea("SG5")
				DbSetOrder(1)
				DbSeek(xfilial("SG5")+SB1->B1_COD+OMSRevisao:ACOLS[_i,1])
			
				If !Found()

					Reclock("SG5",.T.)
					G5_FILIAL		:= xfilial("SG5")
					G5_PRODUTO		:= SB1->B1_COD
					G5_REVISAO		:= OMSRevisao:ACOLS[_i,1]
					G5_OBS			:= OMSRevisao:ACOLS[_i,2]
					G5_USER			:= __cUserID
					G5_DATAVIG		:= dDatabase
					G5_STATUS		:= OMSRevisao:ACOLS[_i,3]
					MsUnLock()
				Else
					RecLock("SG5",.F.)
					Replace G5_OBS			with OMSRevisao:ACOLS[_i,2]
					Replace G5_STATUS		with OMSRevisao:ACOLS[_i,3]
					MsUnLock()
				Endif
				DbSelectArea("SB1")
				DbSkip()
			End
			
		Endif
	Next
Return()

Static Function Seleciona(_tp, _Rev)
// Seleciona a revis�o e abre a tela para cadastro de estrutura

	Local oButton1
	Local oCancela
	Local oDescProd
	Local cDescProd := SB4->B4_DESC
	Local oProduto
	Local oRevisao
	Local oSay1
	Local oSay2
	Private cProduto := cCodProd
	Private cRevisao := _Rev
	Private _fecha := .T.
	Static oEst


	IF __cuserid $ GETMV("HP_USERGRA")

		DbSelectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+cProduto)
		cDescProd := SB4->B4_DESC
		cColuna := SB4->B4_COLUNA
	
	DbSelectArea("SZE")
		DbSetOrder(1)
		DbSeek(xfilial("SZE")+substr(cProduto,1,15)+cRevisao)

		//	BEGIN TRANSACTION
		
		If Found()
			_qry := "delete "+RetSqlName("ZB0")+" where ZB0_PRODUT = '"+SUBSTR(cProduto,1,8)+"' and ZB0_REVISA = '"+cRevisao+"'  "
			nStatus := TcSqlExec(_qry)
	
			if (nStatus < 0)
				MsgBox(TCSQLError(), "Erro no comando", "Stop") 
			endif
			_qry := "delete "+RetSqlName("ZB1")+" where ZB1_PRODUT = '"+SUBSTR(cProduto,1,8)+"' and ZB1_REVISA = '"+cRevisao+"' "
			nStatus := TcSqlExec(_qry)
	
			if (nStatus < 0)
				MsgBox(TCSQLError(), "Erro no comando", "Stop") 
			endif
			_qry := "delete "+RetSqlName("ZB2")+" where ZB2_PRODUT = '"+SUBSTR(cProduto,1,8)+"' and ZB2_REVISA = '"+cRevisao+"' "
			nStatus := TcSqlExec(_qry)
	
			if (nStatus < 0)
				MsgBox(TCSQLError(), "Erro no comando", "Stop") 
			endif
			_qry := "delete "+RetSqlName("ZB3")+" where ZB3_PRODUT = '"+SUBSTR(cProduto,1,8)+"' and ZB3_REVISA = '"+cRevisao+"' "
			nStatus := TcSqlExec(_qry)
	
			if (nStatus < 0)
				MsgBox(TCSQLError(), "Erro no comando", "Stop") 
			endif
	
		//		aResult := {}
		//		_QRY := "BKP_SZE" //" '"+SUBSTR(cProduto,1,8)+"'"
		//		aResult := TCSPEXEC(_QRY,SUBSTR(cProduto,1,8))
		
		//		_qry := "Select count(*) as QTD1 from "+RetSqlName("ZB0")+" where D_E_L_E_T_ = '' and ZB0_PRODUT = '"+SUBSTR(cProduto,1,8)+"' "
		//		If Select("TMPZB0") > 0
		//			TMPZB0->(DbCloseArea())
		//		EndIf
		//		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_QRY),"TMPZB0",.T.,.T.)
		//		DbSelectArea("TMPZB0")
		//		DbGoTop()
	
		//		_erro := alltrim(TCSQLError())
		//		_qtderro := TMPZB0->QTD1
		//		TMPZB0->(DbCloseArea())
		//	Else
		//		_qtderro := 1
			DbSelectArea("SZE")
			DbSetOrder(1)
			DbGoTop()
			DbSeek(xfilial("SZE")+substr(cProduto,1,8)+Space(7)+cRevisao)
		
			While !EOF() .and. SubStr(SZE->ZE_PRODUTO,1,8) = substr(cProduto,1,8) .and. SZE->ZE_REVISAO = cRevisao
				RecLock("ZB0",.t.)
					Replace ZB0_FILIAL	with SZE->ZE_FILIAL
					Replace ZB0_PRODUT	with SZE->ZE_PRODUTO
					Replace ZB0_DESCPR	with SZE->ZE_DESCPRO
					Replace ZB0_COMP	with SZE->ZE_COMP
					Replace ZB0_DESCCO	with SZE->ZE_DESCCOM
					Replace ZB0_UM		with SZE->ZE_UM
					Replace ZB0_CORTE	with SZE->ZE_CORTE
					Replace ZB0_REVISA	with SZE->ZE_REVISAO
					Replace ZB0_INDICE	with SZE->ZE_INDICE
				MsUnLock()
			
				DbSelectArea("SZE")
				DbSkip()
			End
		
			DbSelectArea("SZF")
			DbSetOrder(1)
			DbGoTop()
			DbSeek(xfilial("SZF")+substr(cProduto,1,8)+space(7)+cRevisao)
		
			While !EOF() .and. SubStr(SZF->ZF_PRODUTO,1,8) = substr(cProduto,1,8) .and. SZF->ZF_REVISAO = cRevisao
				RecLock("ZB1",.t.)
					Replace ZB1_FILIAL	with SZF->ZF_FILIAL
					Replace ZB1_PRODUT	with SZF->ZF_PRODUTO
					Replace ZB1_COMP	with SZF->ZF_COMP
					Replace ZB1_REVISA	with SZF->ZF_REVISAO
					Replace ZB1_INDICE	with SZF->ZF_INDICE
					Replace ZB1_COR		with SZF->ZF_COR
					For _w := 1 to 200
						_campo1 := "ZB1_TAM"+StrZero(_w,3)
						_campo2 := "SZF->ZF_TAM"+StrZero(_w,3)
						Replace &(_campo1) with &(_campo2)
					Next
				MsUnLock()
			
				DbSelectArea("SZF")
				DbSkip()
			End

			DbSelectArea("SZG")
			DbSetOrder(1)
			DbGoTop()
			DbSeek(xfilial("SZG")+substr(cProduto,1,8)+Space(7)+cRevisao)
		
			While !EOF() .and. SubStr(SZG->ZG_PRODUTO,1,8) = substr(cProduto,1,8) .and. SZG->ZG_REVISAO = cRevisao
				RecLock("ZB2",.t.)
					Replace ZB2_FILIAL	with SZG->ZG_FILIAL
					Replace ZB2_PRODUT	with SZG->ZG_PRODUTO
					Replace ZB2_COMP	with SZG->ZG_COMP
					Replace ZB2_REVISA	with SZG->ZG_REVISAO
					Replace ZB2_INDICE	with SZG->ZG_INDICE
					Replace ZB2_COR		with SZG->ZG_COR
				Replace ZB2_CORNV	with SZG->ZG_CORNV
			MsUnLock()
			
			DbSelectArea("SZG")
			DbSkip()
		End

		DbSelectArea("SZK")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SZK")+substr(cProduto,1,8)+space(7)+cRevisao)
		
		While !EOF() .and. substr(SZK->ZK_PRODUTO,1,8) = substr(cProduto,1,8) .and. SZK->ZK_REVISAO = cRevisao
			RecLock("ZB3",.t.)
				Replace ZB3_FILIAL	with SZK->ZK_FILIAL
				Replace ZB3_PRODUT	with SZK->ZK_PRODUTO
				Replace ZB3_COMP	with SZK->ZK_COMP
				Replace ZB3_REVISA	with SZK->ZK_REVISAO
				Replace ZB3_INDICE	with SZK->ZK_INDICE
				Replace ZB3_TAM		with SZK->ZK_TAM
				Replace ZB3_TAMNV	with SZK->ZK_TAMNV
			MsUnLock()
			
			DbSelectArea("SZK")
			DbSkip()
		End
  		//MsgBox(_erro,"Erro no comando",  "Stop") 
    Endif
		_salva := 0
		//checacor()
	
		//	DEFINE MSDIALOG oEst TITLE "Estrutura de Produtos" FROM 000, 000  TO 600, 1300 COLORS 0, 16777215 PIXEL
		DEFINE MsDialog oEst From 000,000 To 600,1300 Title "Estrutura de Produtos" Pixel Style DS_MODALFRAME // Cria Dialog sem o bot�o de Fechar.
	
		@ 007, 012 SAY oSay1 PROMPT "Produto" SIZE 025, 007 OF oEst COLORS 0, 16777215 PIXEL
		@ 005, 045 MSGET oProduto VAR cProduto SIZE 060, 010 OF oEst COLORS 0, 16777215 READONLY PIXEL
		@ 005, 112 MSGET oDescProd VAR cDescProd SIZE 345, 010 OF oEst COLORS 0, 16777215 READONLY PIXEL
		@ 007, 467 SAY oSay2 PROMPT "Revis�o" SIZE 025, 007 OF oEst COLORS 0, 16777215 PIXEL
		@ 005, 495 MSGET oRevisao VAR cRevisao SIZE 029, 010 OF oEst COLORS 0, 16777215 READONLY PIXEL
		fMSProduto(_tp)
		fMSCor()
		fMSQtd()
		fMSTam()
		@ 280, 530 BUTTON oButton1 PROMPT "Ok" 						SIZE 050, 012 ACTION (EstSav(),Iif(_fecha,(oEst:End(),oDlg:End()),Alert("kanhos n�o cadastrados!"))) OF oEst PIXEL
		//@ 280, 590 BUTTON oCancela PROMPT "Cancela" 				SIZE 050, 012 ACTION (oEst:End(),oDlg:End()) OF oEst PIXEL Aguardando Valida��o Marly
		@ 280, 590 BUTTON oCancela PROMPT "Cancela"                 SIZE 050, 012 ACTION (IIf(RESBKP(),(oEst:End(),oDlg:End()),Alert("Erro na restaura��o do backup da estrutura. Tente novamente!"))) OF oEst PIXEL
		@ 280, 210 BUTTON oButton6 PROMPT "Limpa Cor"				SIZE 050, 012 ACTION LimpaCor() OF oEst PIXEL
		@ 280, 270 BUTTON oButton3 PROMPT "Repl.Cor"				SIZE 050, 012 ACTION ReplyCor() OF oEst PIXEL
		@ 280, 330 BUTTON oButton4 PROMPT "Repl.Tam"				SIZE 050, 012 ACTION ReplyTam() OF oEst PIXEL
		@ 280, 390 BUTTON oButton5 PROMPT "Repl.Qtd x Grade"		SIZE 050, 012 ACTION ReplyQtdLn() OF oEst PIXEL
		@ 280, 450 BUTTON oButton6 PROMPT "Repl.Qtd x Linha"		SIZE 050, 012 ACTION ReplyQtdCl() OF oEst PIXEL
	
		ACTIVATE MSDIALOG oEst CENTERED

		//		END TRANSACTION
else
	 MsgAlert("Apenas o setor de produtos, pode realizar altera��es na estrutura","Hope")
	return()
endif

Return

STATIC FUNCTION RESBKP

//	_QRY := "EXEC RESTBKP_SZE '"+SUBSTR(CPRODUTO,1,8)+"'"
//	nStatus := TCSQLEXEC(_QRY)

//	aResult := {}
//	_QRY := "RESTBKP_SZE" //" '"+SUBSTR(cProduto,1,8)+"'"
//	aResult := TCSPEXEC(_QRY,SUBSTR(cProduto,1,8))
	
//	_erro := alltrim(TCSQLError())
//	IF _erro <> ""
//	if (nStatus < 0)
//		lret1 := .T.
  		//MsgBox(_erro, "Erro no comando", "Stop")
  		
  		_qry := "UPDATE "+RetSqlName("SZE")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where ZE_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' and ZE_REVISAO = '"+cRevisao+"'  "
		nStatus := TcSqlExec(_qry)
	
		if (nStatus < 0)
			MsgBox(TCSQLError(), "Erro no comando", "Stop") 
		endif
  		_qry := "UPDATE "+RetSqlName("SZF")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where ZF_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' and ZF_REVISAO = '"+cRevisao+"' "
		nStatus := TcSqlExec(_qry)
	
		if (nStatus < 0)
			MsgBox(TCSQLError(), "Erro no comando", "Stop") 
		endif
  		_qry := "UPDATE "+RetSqlName("SZG")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where ZG_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' and ZG_REVISAO = '"+cRevisao+"'  "
		nStatus := TcSqlExec(_qry)
	
		if (nStatus < 0)
			MsgBox(TCSQLError(), "Erro no comando", "Stop") 
		endif
  		_qry := "UPDATE "+RetSqlName("SZK")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where ZK_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' and ZK_REVISAO = '"+cRevisao+"' "
		nStatus := TcSqlExec(_qry)
	
		if (nStatus < 0)
			MsgBox(TCSQLError(), "Erro no comando", "Stop") 
		endif
  
		DbSelectArea("ZB0")
		DbSetOrder(1)
		ZB0->(DbGotop())
		DbSeek(xfilial("ZB0")+substr(cProduto,1,15)+cRevisao)
		
		While !EOF() .and. ZB0->ZB0_PRODUT = substr(cProduto,1,15) .and. ZB0->ZB0_REVISA = cRevisao
			RecLock("SZE",.t.)
				Replace ZE_FILIAL	with ZB0->ZB0_FILIAL
				Replace ZE_PRODUTO	with ZB0->ZB0_PRODUT
				Replace ZE_DESCPRO	with ZB0->ZB0_DESCPR
				Replace ZE_COMP		with ZB0->ZB0_COMP
				Replace ZE_DESCCOM	with ZB0->ZB0_DESCCO
				Replace ZE_UM		with ZB0->ZB0_UM
				Replace ZE_CORTE	with ZB0->ZB0_CORTE
				Replace ZE_REVISAO	with ZB0->ZB0_REVISA
				Replace ZE_INDICE	with ZB0->ZB0_INDICE
			MsUnLock()
			
			DbSelectArea("ZB0")
			DbSkip()
		End
		
		DbSelectArea("ZB1")
		DbSetOrder(1)
		DbSeek(xfilial("ZB1")+substr(cProduto,1,15)+cRevisao)
		
		While !EOF() .and. ZB1->ZB1_PRODUT = substr(cProduto,1,15) .and. ZB1->ZB1_REVISA = cRevisao
			RecLock("SZF",.t.)
				Replace ZF_FILIAL	with ZB1->ZB1_FILIAL
				Replace ZF_PRODUTO	with ZB1->ZB1_PRODUT
				Replace ZF_COMP		with ZB1->ZB1_COMP
				Replace ZF_REVISAO	with ZB1->ZB1_REVISA
				Replace ZF_INDICE	with ZB1->ZB1_INDICE
				Replace ZF_COR		with ZB1->ZB1_COR
				For _w := 1 to 200
					_campo1 := "ZF_TAM"+StrZero(_w,3)
					_campo2 := "ZB1->ZB1_TAM"+StrZero(_w,3)
					Replace &(_campo1) with &(_campo2)
				Next
			MsUnLock()
			
			DbSelectArea("ZB1")
			DbSkip()
		End

		DbSelectArea("ZB2")
		DbSetOrder(1)
		DbSeek(xfilial("ZB2")+substr(cProduto,1,15)+cRevisao)
		
		While !EOF() .and. ZB2->ZB2_PRODUT = substr(cProduto,1,15) .and. ZB2->ZB2_REVISA = cRevisao
			RecLock("SZG",.t.)
				Replace ZG_FILIAL	with ZB2->ZB2_FILIAL
				Replace ZG_PRODUTO	with ZB2->ZB2_PRODUT
				Replace ZG_COMP		with ZB2->ZB2_COMP
				Replace ZG_REVISAO	with ZB2->ZB2_REVISA
				Replace ZG_INDICE	with ZB2->ZB2_INDICE
				Replace ZG_COR		with ZB2->ZB2_COR
				Replace ZG_CORNV	with ZB2->ZB2_CORNV
			MsUnLock()
			
			DbSelectArea("ZB2")
			DbSkip()
		End

		DbSelectArea("ZB3")
		DbSetOrder(1)
		DbSeek(xfilial("ZB3")+substr(cProduto,1,15)+cRevisao)
		
		While !EOF() .and. ZB3->ZB3_PRODUT = substr(cProduto,1,15) .and. ZB3->ZB3_REVISA = cRevisao
			RecLock("SZK",.t.)
				Replace ZK_FILIAL	with ZB3->ZB3_FILIAL
				Replace ZK_PRODUTO	with ZB3->ZB3_PRODUT
				Replace ZK_COMP		with ZB3->ZB3_COMP
				Replace ZK_REVISAO	with ZB3->ZB3_REVISA
				Replace ZK_INDICE	with ZB3->ZB3_INDICE
				Replace ZK_TAM		with ZB3->ZB3_TAM
				Replace ZK_TAMNV	with ZB3->ZB3_TAMNV
			MsUnLock()
			
			DbSelectArea("ZB3")
			DbSkip()
		End
//  	Else
  		lret1 := .T.
//  	endif
RETURN(lret1)
 
//------------------------------------------------ 
Static Function fMSProduto(_tp)
//------------------------------------------------ 
// Acols para componentes das estruturas
// Tabela: SZE

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZE_COMP","ZE_DESCCOM","ZE_UM","ZE_CORTE","ZE_INDICE"}
	Local aAlterFields := {"ZE_COMP","ZE_CORTE"}
	Public oMSNewGe1

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZE_COMP"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"ExistCpo('SB4')",;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZE_DESCCOM"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZE_UM"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZE_CORTE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
	If SX3->(DbSeek("ZE_INDICE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
  
  // Define field values
	DbSelectArea("SZE")
	DbSetOrder(1)
	DbSeek(xfilial("SZE")+substr(cProduto,1,15)+cRevisao)
  	
	If Found()
		While !EOF() .and. alltrim(SZE->ZE_PRODUTO) == alltrim(cProduto) .and. SZE->ZE_REVISAO == cRevisao
			Aadd(aColsEx, {SZE->ZE_COMP,Posicione("SB4",1,xfilial("SB4")+SZE->ZE_COMP,"B4_DESC"),Posicione("SB4",1,xfilial("SB4")+SZE->ZE_COMP,"B4_UM"),SZE->ZE_CORTE,SZE->ZE_INDICE,.F.})
  		
			DbSelectArea("SZE")
			DbSkip()
		End
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
	Endif

	oMSNewGe1 := MsNewGetDados():New( 027, 006, 185, 470, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)

  // Chamada de rotina para atualiza��o de demais acols quando da mudan�a de linha no acols de componentes
	oMSNewGe1:bchange	:= {|| u_AtuAcols() }

Return

//------------------------------------------------ 
Static Function fMSCor()
//------------------------------------------------ 
// Monta acols de Cores
// Tabela: SZG

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZG_COR","ZG_CORNV","ZG_COMP","ZG_INDICE"}
	Local aAlterFields := {"ZG_CORNV"}
	Static oMSCor

	cCor := ""
  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If aFields[nX] <> "ZG_CORNV"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		Else
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_ExistCod(2,M->ZG_CORNV)",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		Endif
	Next nX

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)

			DbSelectArea("SZG")
			DbSetOrder(2)
			DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+PADR(oMSNewGe1:aCols[oMSNewGe1:nAT,1],15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SZD->ZD_COR)
	
			IF Found()
		
//			While !EOF() .and. alltrim(ZG_PRODUTO) == alltrim(cProduto) .and. alltrim(ZG_COMP) == alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) .and. ZG_REVISAO == cRevisao
				Aadd(aColsEx, {SZG->ZG_COR,SZG->ZG_CORNV,SZG->ZG_COMP,SZG->ZG_INDICE,.F.})
//				DbSelectArea("SZG")
//				DbSkip()
//			End
			Else
				Aadd(aColsEx, {SZD->ZD_COR,"   ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
			Endif
/*
	Else
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			Aadd(aColsEx, {SZD->ZD_COR,"   ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],.F.})

			DbSelectArea("SZD")
			DbSkip()
		End

	Endif
*/
			DbSelectArea("SZD")
			DbSkip()
		End

	Else
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			Aadd(aColsEx, {SZD->ZD_COR,"   ",space(15),space(3),.F.})

			DbSelectArea("SZD")
			DbSkip()
		End
	Endif

	oMSCor := MsNewGetDados():New( 027, 475, 185, 645, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)

Return

//------------------------------------------------ 
Static Function fMSQtd()
//------------------------------------------------ 
// Acols para Quandidades por tamanho
// Tabela: SZF

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZF_COR","ZF_COR","ZF_INDICE"}
	Local aAlterFields := {}
	Local _i
	Static oMSQtd

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZF_COR"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZF_COR","@!",3,0,,SX3->X3_USADO,"C","","R","",""})
		Aadd(aHeaderEx, {"Descri��o"+Space(4),"ZF_COR","@!",10,0,,SX3->X3_USADO,"C","","R","",""})
	Endif

	DbSelectArea("SZD")
	DbSetOrder(1)
	DbSeek(xfilial("SZD")+alltrim(cProduto))
	_XTAM := {}
	While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
		For _i := 1 to 200
			_campo := "SZD->ZD_TAM"+StrZero(_i,3)
			If &_campo == "X"
				_tem := ASCAN( _xtam, {|x| x == _i})
				If _tem == 0
					aadd(_xtam,_i)
				Endif
			Endif
		Next

		DbSelectarea("SZD")
		DbSkip()
	End

	aSort(_xtam)
  
	For _i := 1 to len(_xtam)
		DbSelectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+alltrim(cproduto))
		_desc := rettam(_xtam[_i],SB4->B4_COLUNA,"D")
		DbSelectArea("SX3")
		_campo := "ZF_TAM"+StrZero(_xtam[_i],3)
		Aadd(aFields,_campo)
		Aadd(aAlterFields,_campo)

		SX3->(DbSetOrder(2))
		If SX3->(DbSeek(_campo))
			Aadd(aHeaderEx, {alltrim(_desc)+Space(4),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
				SX3->X3_USADO,SX3->X3_TIPO,"",SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
  
	Next

	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZF_COMP"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZF_COMP","@!",15,0,,SX3->X3_USADO,"C","","R","",""})
	Endif
//  Aadd(aFields,"ZF_COMP") 

	If SX3->(DbSeek("ZF_INDICE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	Endif
//  Aadd(aFields,"ZF_INDICE")

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""

		DbSelectArea("SZF")
		DbSetOrder(2)
		DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[oMSNewGe1:nAT,1]+oMSNewGe1:aCols[oMSNewGe1:nAT,5])
	
		IF Found()
		
			While !EOF() .and. alltrim(ZF_PRODUTO) == alltrim(cProduto) .and. alltrim(ZF_COMP) == alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) .and. ZF_REVISAO == cRevisao .AND. ZF_INDICE = oMSNewGe1:aCols[oMSNewGe1:nAT,5]
				aFieldFill := {}
				Aadd(aFieldFill, SZF->ZF_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZF->ZF_COR,"BV_DESCRI"))
				For nX := 3 to Len(aHeaderEx)-2
					_campo := "SZF->"+aheaderEx[nX][2]//ZF_TAM"+StrZero(nX,3)
					Aadd(aFieldFill,&_campo)
				Next
				Aadd(aFieldFill, SZF->ZF_COMP)
				Aadd(aFieldFill, SZF->ZF_INDICE)
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
	
				DbSelectArea("SZF")
				DbSkip()
			End
		Else
			DbSelectArea("SZD")
			DbSetOrder(1)
			DbSeek(xfilial("SZD")+alltrim(cProduto))

			While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
				aFieldFill := {}
				Aadd(aFieldFill, SZD->ZD_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))

				For nX := 3 to Len(aHeaderEx)-2
					Aadd(aFieldFill,0)
				Next
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,5])
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)

				DbSelectArea("SZD")
				DbSkip()
			End
		Endif
	Else
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			aFieldFill := {}
			Aadd(aFieldFill, SZD->ZD_COR)
			Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))

			For nX := 3 to Len(aHeaderEx)-2
				Aadd(aFieldFill,0)
			Next
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,2])
			Aadd(aFieldFill, .F.)
			Aadd(aColsEx, aFieldFill)

			DbSelectArea("SZD")
			DbSkip()
		End
	Endif

	oMSQtd := MsNewGetDados():New( 190, 006, 275, 500, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)

Return

//------------------------------------------------ 
Static Function fMSTam()
//------------------------------------------------ 
// Acols para Tamanho dos componentes 
// Tabela: SZK

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZK_TAM","ZK_TAMNV","ZK_COMP","ZK_INDICE"}
	Local aAlterFields := {"ZK_TAMNV"}
	Static oMSTAM

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If aFields[nX] <> "ZK_TAMNV"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		Else
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_ExistCod(3,M->ZK_TAMNV)",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		Endif
	Next nX

	_ac := {}

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SubStr(cProduto,1,8))

		While !EOF() .and. SubStr(B1_COD,1,8) == SubStr(cProduto,1,8)
			_tem := ASCAN(_aC, {|x| Alltrim(x[2]) == SubStr(SB1->B1_COD,12,4)})
			If _tem == 0
				DbSelectArea("SZK")
				DbSetOrder(2)
				DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+padr(oMSNewGe1:aCols[oMSNewGe1:nAT,1],15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SubStr(SB1->B1_COD,12,4))
	
				IF Found()
//				While !EOF() .and. alltrim(ZK_PRODUTO) == alltrim(cProduto) .and. alltrim(ZK_COMP) == alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) .and. ZK_REVISAO == cRevisao
					Aadd(_aC, {0,SZK->ZK_TAM,SZK->ZK_TAMNV,oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
//				DbSelectArea("SZK")
//				DbSkip()
//				End
				Else
					DbSelectArea("SB4")
					DbSetOrder(1)
					DbSeek(xfilial("SB4")+oMSNewGe1:aCols[oMSNewGe1:nAT,1])
				
					DbSelectArea("SBV")
					DbSetOrder(1)
					DbSeek(xfilial("SBV")+SB4->B4_COLUNA)
				
					IF SBV->BV_CHAVE = '00TU'
						Aadd(_aC, {0,SubStr(SB1->B1_COD,12,4),"00TU",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
					ELSE
						Aadd(_aC, {0,SubStr(SB1->B1_COD,12,4),"   ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
					ENDIF
				Endif
			Endif

			DbSelectArea("SB1")
			DbSkip()
		End
	Else
		Aadd(aColsEx, {Space(3),"   ",Space(15),Space(3),.F.})
	Endif

	if Len(_ac) > 0
		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna+ "' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"
	
		cQuery := ChangeQuery(cQuery)
		If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		For nX:= 1 to len(_ac)
			DbSelectArea("TMPSBV")
			DbGoTop()
			_w := 0
			While !EOF()
				_w++
				If alltrim(TMPSBV->BV_CHAVE) == alltrim(_ac[nX,2])
					_ac[nX,1] := _w
				Endif
				DbSkip()
			End
		
		Next
		DbSelectArea("TMPSBV")
		DbCloseArea()
	
		ASORT(_ac, , , { |x,y| x[1] < y[1] } )
	
		For nX:=1 to len(_ac)
			If ASCAN(aColsEx,_ac[nX,2])=0
				Aadd(aColsEx, {_ac[nX,2],_ac[nX,3],_ac[nX,4],_ac[nX,5],.F.})
			End
		Next
	Endif

	oMSTAM := MsNewGetDados():New( 190, 505, 275, 645, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)

Return


Static Function estsav()
// Rotina para gerar a estrutura (SG1)
// Primeiro salva as tabelas espec�ficas
// Depois gera a estrutura de Produtos

	Local _i, _w

	//_fecha := .T.

	For _i := 1 to len(oMSNewGe1:aCols)
		If oMSNewGe1:aCols[_i,len(oMSNewGe1:aHeader)+1] = .F. .and. alltrim(oMSNewGe1:aCols[_i,1]) <> ""
			DbSelectArea("SZE")
			DbSetOrder(2)
			DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[_i,5]+oMSNewGe1:aCols[_i,1])
		
			If alltrim(oMSNewGe1:aCols[_i,1]) <> ""
				If !Found()
					RecLock("SZE",.T.)
					Replace ZE_FILIAL			with xfilial("SZE")
					Replace ZE_PRODUTO			with SubStr(cProduto,1,15)
					Replace ZE_COMP				with oMSNewGe1:aCols[_i,1]
					Replace ZE_REVISAO			with cRevisao
					Replace ZE_CORTE			with oMSNewGE1:aCols[_i,4]
					Replace ZE_INDICE			with oMSNewGE1:aCols[_i,5]
					MsUnLock()
				Else
					If SZE->ZE_CORTE <> oMSNewGE1:aCols[_i,4]
						u_ajustac(SubStr(cProduto,1,15),cRevisao,oMSNewGe1:aCols[_i,5],oMSNewGe1:aCols[_i,1],SZE->ZE_CORTE)
					Endif
					RecLock("SZE",.F.)
					Replace ZE_CORTE			with oMSNewGE1:aCols[_i,4]
					MsUnLock()
				Endif
			Endif
		Else
			DbSelectArea("SZE")
			DbSetOrder(2)
			DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[_i,5]+oMSNewGe1:aCols[_i,1])
		
			If Found() 
				RecLock("SZE",.F.)
				DbDelete()
				SZE->(MsUnLock("SZE"))
			Endif
		
//		DbSelectArea("SG1")
//		DbSetOrder(2)
//		DbSeek(xfilial("SG1")+oMSNewGe1:aCols[_i,1])
		
//		While !EOF() .and. SubStr(SG1->G1_COMP,1,8) == alltrim(oMSNewGe1:aCols[_i,1])
//			If SubStr(SG1->G1_COD,1,8) == SubStr(cProduto,1,8)
//				RecLock("SG1",.F.)
//					DbDelete()
//				MsUnLock()
//			Endif
			
//			DbSelectArea("SG1")
//			DbSkip()
//		End
		/*	IF oMSNewGE1:aCols[_i,4] == "S"
				_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where D_E_L_E_T_ = '' and G1_REVINI = '"+cRevisao+"' and SubString(G1_COD,1,8) = 'B"+SubStr(cProduto,2,7)+"' and SubString(G1_COMP,1,8) = '"+alltrim(oMSNewGe1:aCols[_i,1])+"'"
			Else
				_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where D_E_L_E_T_ = '' and G1_REVINI = '"+cRevisao+"' and SubString(G1_COD,1,8) = '"+SubStr(cProduto,1,8)+"' and SubString(G1_COMP,1,8) = '"+alltrim(oMSNewGe1:aCols[_i,1])+"'"
			Endif
			nStatus := TcSqlExec(_qry)
			
			if (nStatus < 0)
  				MsgBox(TCSQLError(), "Erro no comando", "Stop")
	  		endif
			 */ 
		Endif
	
		DbSelectArea("SZE")
	
	Next

	For _i := 1 to len(oMSCor:aCols)
		DbSelectArea("SZG")
		DbSetOrder(2)
		DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+oMsCor:aCols[_i,3]+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+oMsCor:aCols[_i,1])

		If alltrim(oMsCor:aCols[_i,3]) <> ""
			_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsCor:aCols[_i,3]) .AND.  Alltrim(x[5]) == alltrim(oMsCor:aCols[_i,4])})

			IF Found()
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZG",.F.)
					Replace ZG_CORNV	with oMsCor:aCols[_i,2]
					MsUnlock()
				Else
					RecLock("SZG",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Else
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZG",.T.)
					Replace ZG_FILIAL		with xfilial("SZG")
					Replace ZG_PRODUTO 		with cProduto
					Replace ZG_COMP			with oMsCor:aCols[_i,3]
					Replace ZG_COR			with oMsCor:aCols[_i,1]
					Replace ZG_CORNV		with oMsCor:aCols[_i,2]
					Replace ZG_REVISAO		with cRevisao
					Replace ZG_INDICE		with oMSCor:aCols[_i,4]
					MsUnlock()
				Endif
			Endif
		Endif
	Next

	For _i := 1 to len(oMSTam:aCols)
		DbSelectArea("SZK")
		DbSetOrder(2)
		SZK->(DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+oMsTam:aCols[_i,3]+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+oMsTam:aCols[_i,1]))
		
		If alltrim(oMsTam:aCols[_i,3]) <> ""
			_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsTam:aCols[_i,3]) .AND. Alltrim(x[5]) == alltrim(oMsTam:aCols[_i,4])})

			IF Found()
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.				
					RecLock("SZK",.F.)
					Replace ZK_TAMNV	with oMsTam:aCols[_i,2]
					MsUnlock()	
				Else
					RecLock("SZK",.F.)
					DbDelete()
					MsUnLock()				
				Endif
			Else
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZK",.T.)
					Replace ZK_FILIAL	with xfilial("SZK")
					Replace ZK_PRODUTO 	with cProduto
					Replace ZK_COMP		with oMsTam:aCols[_i,3]
					Replace ZK_TAM		with oMsTam:aCols[_i,1]
					Replace ZK_TAMNV		with oMsTam:aCols[_i,2]
					Replace ZK_REVISAO	with cRevisao
					Replace ZK_INDICE		with oMSTam:aCols[_i,4]
					MsUnlock()
				Endif
			Endif
		Endif
	Next

	For _i := 1 to len(oMSQtd:aCols)
		DbSelectArea("SZF")
		DbSetOrder(2)
		DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMsQtd:aCols[_i,Len(oMsQtd:aHeader)-1],1,15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+alltrim(oMsQtd:aCols[_i,1]))

		_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsQtd:aCols[_i,len(oMsQtd:aHeader)-1]) .AND. Alltrim(x[5]) == alltrim(oMsQtd:aCols[_i,len(oMsQtd:aHeader)])})

		IF Found()
			If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
				RecLock("SZF",.F.)
				For _w := 3 to Len(oMsQtd:aHeader)-2
					_campo := "SZF->"+alltrim(oMsQtd:aheader[_w][2])  //ZF_TAM"+StrZero(_w,3)
					Replace &(_campo) with oMsQtd:ACOLS[_i,_w]
				Next
				MsUnlock()
			Else
				RecLock("SZF",.F.)
				DbDelete()
				MsUnLock()
			Endif
		Else
			If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F. .and. alltrim(oMsQtd:aCols[_i,len(oMsQtd:aHeader)-1]) <> ""
				RecLock("SZF",.T.)
				Replace ZF_FILIAL	with xfilial("SZF")
				Replace ZF_PRODUTO 	with cProduto
				Replace ZF_COMP		with oMsQtd:aCols[_i,len(oMsQtd:aHeader)-1]
				Replace ZF_COR		with oMsQtd:aCols[_i,1]
				Replace ZF_REVISAO	with cRevisao
				Replace ZF_INDICE		with oMSQtd:aCols[_i,len(oMsQtd:aHeader)]
				For _w := 3 to Len(oMsQtd:aHeader)-2
					_campo := "SZF->"+oMsQtd:aheader[_w][2] //ZF_TAM"+StrZero(_w,3)
					Replace &(_campo) with oMsQtd:ACOLS[_i,_w]
				Next
				MsUnlock()
			Endif
		Endif
	Next

	For _i := 1 to len(oMSNewGe1:aCols)
		If oMSNewGe1:aCols[_i,len(oMSNewGe1:aHeader)+1] = .F.
			DbSelectArea("SZF")
			DbSetOrder(2)
			DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5])

			IF found()
				While !EOF() .and. SubStr(cProduto,1,8) == SubStr(SZF->ZF_PRODUTO,1,8) .and. alltrim(cRevisao) == alltrim(SZF->ZF_REVISAO) .and. SubStr(oMSNewGe1:aCols[_i,1],1,8) == SubStr(SZF->ZF_COMP,1,8) .and. _fecha .and. alltrim(SZF->ZF_INDICE) == alltrim(oMSNewGe1:aCols[_i,5])
					_temqtd := 0
					For _w := 3 to len(oMsQtd:aheader)-2
						_campo := "SZF->"+oMsQtd:aheader[_w][2] //ZF_TAM"+StrZero(_w,3)

						DbSelectArea("SZG")
						DbSetOrder(2)
						DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)
				
						If &_campo > 0
							_temqtd++
							DbSelectArea("SZG")
							DbSetOrder(2)
							DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)
				
							If !Found() .or. alltrim(SZG->ZG_CORNV) = ""
								alert(SubStr(oMSNewGe1:aCols[_i,1],1,15)+"-"+oMSNewGe1:aCols[_i,5]+"-"+SZF->ZF_COR)
								_fecha := .F.
							Endif

				
							DbSelectArea("SZK")
							DbSetOrder(2)
							DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMsNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5])

							If Found()
								While !EOF() .and. SubStr(cProduto,1,8) == SubStr(SZK->ZK_PRODUTO,1,8) .and. alltrim(cRevisao) == alltrim(SZK->ZK_REVISAO) .and. SubStr(oMSNewGe1:aCols[_i,1],1,8) == SubStr(SZK->ZK_COMP,1,8) .and. _fecha .and. alltrim(SZK->ZK_INDICE) == alltrim(oMSNewGe1:aCols[_i,5])
									DbSelectArea("SB4")
									DbSetOrder(1)
									DbSeek(xfilial("SB4")+SubStr(cProduto,1,8))

									cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
									cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +SB4->B4_COLUNA +"' AND D_E_L_E_T_ = '' "
									cQuery += "ORDER BY R_E_C_N_O_"
							
									cQuery := ChangeQuery(cQuery)
								  
									If Select("TMPSBV") > 0
										TMPSBV->(DbCloseArea())
									EndIf
									dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
									
									DbSelectArea("TMPSBV")
									DbGoTop()
									_xlin := 0
									While !EOF()
										_xlin++
										If alltrim(SZK->ZK_TAM) = alltrim(TMPSBV->BV_CHAVE)
											Exit
										Endif
										DbSkip()
									End
									DbCloseArea()
									
									If _xlin > 0
										_qry := "Select top 1 * from "+RetSqlName("SZF")+" where D_E_L_E_T_ = '' and ZF_PRODUTO = '"+SubStr(cProduto,1,15)+"' and ZF_COMP = '"+SubStr(oMsNewGe1:aCols[_i,1],1,15)+"' and ZF_TAM"+StrZero(_xlin,3)+" > 0"
										dbUseArea( .T., "TOPCONN", TcGenQry(,,_Qry), "XQRY", .T., .F. )
										dbSelectArea("XQRY")
										dbGoTop()
										If !EOF() 
											If alltrim(SZK->ZK_TAMNV) == ""
											
												Alert(SubStr(oMSNewGe1:aCols[_i,1],1,8)+"-"+SZK->ZK_TAM)
												_fecha := .F.
											Endif
										Endif
										DbCloseArea()
									Else
										_fecha := .F.
									Endif
									DbSelectArea("SZK")
									DbSkip()
								End
							Else
								_fecha := .F.
							Endif
						Endif
					Next
					// Obrigat�rio a ter pelo menos uma quantidade preenchida caso tenha a cor preenchida!
					If _temqtd = 0
						DbSelectArea("SZG")
						DbSetOrder(2)
						DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)
				
						If Found() .and. alltrim(SZG->ZG_CORNV) <> ""
							alert(SubStr(oMSNewGe1:aCols[_i,1],1,15)+"-"+oMSNewGe1:aCols[_i,5]+"-"+SZF->ZF_COR)
							_fecha := .F.
						Endif
					Endif
					
					DbSelectArea("SZF")
					DbSkip()
				End
			Else
				_fecha := .F.
			Endif
		Endif
	Next

	If _fecha
/*	
		_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ " 
		_qry += "from "+RetSqlName("SG1")+" SG1A "
		_qry += "where SG1A.D_E_L_E_T_ = '' and SG1A.G1_REVINI = '"+cRevisao+"' and G1_COD in "
		_qry += "(Select G1_COMP from "+RetSqlName("SG1")+" SG1 where SG1.D_E_L_E_T_ = '' and " 
		_qry += "left(SG1.G1_COD,8) = '"+substr(cProduto,1,8)+"' and left(SG1.G1_COMP,1) = 'B' "
		_qry += "group by G1_COMP) "
		nStatus := TcSqlExec(_qry)

		if (nStatus < 0)
  			MsgBox(TCSQLError(), "Erro no comando", "Stop")
  		endif
*/

		_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ from "+RetSqlName("SG1")+" SG1 " 
		_qry += " where left(G1_COD,8) = 'B"+substr(cProduto,1,7)+"' and SG1.D_E_L_E_T_ = '' and "  
		_qry += "(Select top 1 ZE_COMP from SZE010 SZE where SZE.D_E_L_E_T_ = ''  and ZE_PRODUTO = '"+substr(cProduto,1,8)+"' and " 
		_qry += "left(ZE_COMP,8) = left(G1_COMP,8) and ZE_CORTE = 'S' and ZE_REVISAO = G1_REVINI) is null "

		TcSqlExec(_qry)

		_qry := "SELECT 'update "+RetSqlName("SG1")+" set D_E_L_E_T_ = ''*'', R_E_C_D_E_L_ = R_E_C_N_O_ where R_E_C_N_O_ in ('+ ( "
		_qry += "Select cast(R_E_C_N_O_ as varchar) + ',' AS [text()] "
		_qry += "from "+RetSqlName("SG1")+" SG1A (nolock) "
		_qry += "where SG1A.D_E_L_E_T_ = '' and SG1A.G1_REVINI = '"+cRevisao+"' and G1_COD in "
		_qry += "(Select G1_COMP from "+RetSqlName("SG1")+" SG1 where SG1.D_E_L_E_T_ = '' and " 
		_qry += "left(SG1.G1_COD,8) = '"+substr(cProduto,1,8)+"' and left(SG1.G1_COMP,1) = 'B' "
		_qry += "group by G1_COMP) "
		_qry += "FOR XML PATH ('')) as UPD "

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPUPD",.T.,.T.)
		DbselectArea("TMPUPD")
		DbGoTop()
		
		_QRY := SUBSTR(TMPUPD->UPD,1,LEN(ALLTRIM(TMPUPD->UPD))-1)+")"

		IF len(alltrim(_qry)) > 10
			nStatus := TcSqlExec(_qry)
		
			if (nStatus < 0)
	  			MsgBox(TCSQLError(), "Erro no comando", "Stop")
	  		endif
		ENDIF
		DbSelectArea("TMPUPD")
		DbCloseArea()

/*
		_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ " 
		_qry += "where D_E_L_E_T_ = '' and G1_REVINI = '"+cRevisao+"' and left(G1_COD,8) = '"+substr(cProduto,1,8)+"' "
		nStatus := TcSqlExec(_qry)
		
		if (nStatus < 0)
  			MsgBox(TCSQLError(), "Erro no comando", "Stop")
  		endif
*/

		_qry := "SELECT 'update "+RetSqlName("SG1")+" set D_E_L_E_T_ = ''*'', R_E_C_D_E_L_ = R_E_C_N_O_ where R_E_C_N_O_ in ('+ ( "
		_qry += "Select cast(R_E_C_N_O_ as varchar) + ',' AS [text()] "
		_qry += "from "+RetSqlName("SG1")+" SG1 (nolock) "
		_qry += "where D_E_L_E_T_ = '' and G1_REVINI = '"+cRevisao+"' and left(G1_COD,8) = '"+substr(cProduto,1,8)+"' "
		_qry += "FOR XML PATH ('')) as UPD "

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPUPD",.T.,.T.)
		DbselectArea("TMPUPD")
		DbGoTop()
		
		_QRY := SUBSTR(TMPUPD->UPD,1,LEN(ALLTRIM(TMPUPD->UPD))-1)+")"

		IF len(alltrim(_qry)) > 10
			nStatus := TcSqlExec(_qry)
		
			if (nStatus < 0)
	  			MsgBox(TCSQLError(), "Erro no comando", "Stop")
	  		endif
		ENDIF
		DbSelectArea("TMPUPD")
		DbCloseArea()

		DbSelectArea("SZE")
		DbSetOrder(1)
		DbSeek(xfilial("SZE")+substr(cProduto,1,15)+cRevisao)
	
		_seq1 := SZE->ZE_INDICE
  	
		While !EOF() .and. alltrim(SZE->ZE_PRODUTO) == ALLTRIM(SubStr(cProduto,1,15)) .and. Alltrim(SZE->ZE_REVISAO) == cRevisao
			DbSelectArea("SB4")
			DbSetOrder(1)
			DbSeek(xfilial("SB4")+SubStr(SZE->ZE_COMP,1,8))
	
			If !Found()
				_seq := 1
				DbSelectArea("SZF")
				DbSetOrder(2)
				DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+SZE->ZE_INDICE+SZE->ZE_COMP)

				DbSelectArea("SZG")
				DbSetOrder(2)
				DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SZE->ZE_COMP+SZE->ZE_INDICE)
		
				If Found()
					For _i := 3 to len(oMsQtd:aHeader)-2
			
						_campo := "SZF->"+oMsQtd:aheader[_i][2]//ZF_TAM"+StrZero(_i,3)
				
						If &_campo > 0
							DbSelectArea("SB4")
							DbSetOrder(1)
							DbSeek(xfilial("SB4")+alltrim(SubStr(cProduto,1,15)))
							cColuna	:= SB4->B4_COLUNA
							_comp := SZE->ZE_COMP
							_tam 		:= rettam(Val(SubStr(_campo,12,3)),cColuna,"C")
							_qtd  := &_campo
							_niv := "01"
							_xnv := "99"
					
							IF SZE->ZE_CORTE == "S"
								_niv := "02"
								_xnv := "98"
								_prod := u_cadEst(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_CORNV)+_tam ,cColuna,SZE->ZE_INDICE)
								u_EstSup(PADR(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam,15),PADR(_prod,15),SZE->ZE_INDICE)
							Else
								_niv := "01"
								_xnv := "99"
								_prod := alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam
							Endif
					
//					If PADR(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam,15) <> PADR(alltrim(_prod),15)
//					Endif
					
							DbSelectArea("SB1")
							DbSetOrder(1)
							DbSeek(xfilial("SB1")+_prod)
		
							If Found()
								DbSelectArea("SG1")
								//DbOrderNickName("SG1GRD1")
								DbSetOrder(6)
								DbSeek(xfilial("SG1")+PADR(_prod,15)+PADR(_comp,15)+_seq1+cRevisao)
					
								If !Found() 
									If len(alltrim(_comp)) = 15
/*
										RecLock("SG1",.T.)
										Replace G1_FILIAL		with xfilial("SG1")
										Replace G1_COD			with _prod
										Replace G1_COMP			with _comp
										Replace G1_TRT			with _seq1
										Replace G1_QUANT		with _qtd
										Replace G1_INI			with ddatabase
										Replace G1_FIM			with ctod("31/12/2049")
										Replace G1_NIV			with _niv
										Replace G1_NIVINV		with _xnv
										Replace G1_REVINI		with cRevisao
										Replace G1_REVFIM		with cRevisao
										MsUnLock()
*/
										_qry := "Insert into "+RetSqlName("SG1")+" (G1_FILIAL, G1_COD, G1_COMP, G1_TRT, G1_QUANT, G1_INI, G1_FIM, G1_NIV, G1_NIVINV, G1_REVINI," 
										_qry += "G1_REVFIM, R_E_C_N_O_) Values ('"+xfilial("SG1")+"','"+_prod+"','"
										_qry += _comp+"','"+oMSOPC:ACOLS[i,3]+"',"+alltrim(str(_qtd))+",'"+dtos(ddatabase)+"'," 
										_qry += "'20491231', '"+_niv+"', '"+_xnv+"', '"+cRevisao+"', '"+cRevisao+"',(Select max(R_E_C_N_O_)+1 from SG1010))"
				
										nStatus := TcSqlExec(_qry)
								
										if (nStatus < 0)
								  			MsgBox(TCSQLError(), "Erro no comando", "Stop") 
								  		endif

									Endif
								Else
									RecLock("SG1",.F.)
									Replace G1_QUANT		with _qtd
									MsUnLock()
								Endif
							Else
								Alert("O produto "+_prod+" n�o est� cadastrado no cadastro de produtos!")
							Endif
						Endif
					Next
				Endif
			Else
				DbSelectArea("SZG")
				DbSetOrder(2)
				DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SZE->ZE_COMP+SZE->ZE_INDICE)
				_seq1 := SZE->ZE_INDICE
				_seq := 0
				While !EOF() .and. SZG->ZG_COMP = SZE->ZE_COMP .and. SZG->ZG_PRODUTO = SZE->ZE_PRODUTO .and. SZG->ZG_REVISAO == cRevisao .AND. SZE->ZE_INDICE = SZG->ZG_INDICE
					DbSelectArea("SZF")
					DbSetOrder(2)
					DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+SZG->ZG_COMP+SZE->ZE_INDICE+SZG->ZG_COR)
		
					If Found()
						_seq++
			
						For _i := 3 to len(oMsQtd:aHeader)-2
				
							_campo := "SZF->"+oMsQtd:aheader[_i][2]//ZF_TAM"+StrZero(_i,3)
				
							If &_campo > 0
								DbSelectArea("SB4")
								DbSetOrder(1)
								DbSeek(xfilial("SB4")+alltrim(SubStr(cProduto,1,15)))
								cColuna	:= SB4->B4_COLUNA
								_tam 		:= rettam(Val(SubStr(_campo,12,3)),cColuna,"C")
								DbSelectArea("SZK")
								DbSetOrder(2)
								DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+SZG->ZG_COMP+SZE->ZE_INDICE+_tam)
								If Found()
									_tamcomp := SZK->ZK_TAMNV
								Else
									_tamcomp := ""
								Endif
								_comp := alltrim(SZG->ZG_COMP)+alltrim(SZG->ZG_CORNV)+_tamcomp
								_qtd  := &_campo
								_niv := "01"
								_xnv := "99"
					
								IF SZE->ZE_CORTE == "S"
									_niv := "02"
									_xnv := "98"
									_prod := u_cadEst(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam, ccoluna,SZE->ZE_INDICE)
								Else
									_niv := "01"
									_xnv := "99"
									_prod := alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam
								Endif
					
								If PADR(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam,15) <> PADR(alltrim(_prod),15)
									u_EstSup(PADR(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam,15),PADR(_prod,15),SZG->ZG_INDICE)
								Endif
					
								DbSelectArea("SB1")
								DbSetOrder(1)
								DbSeek(xfilial("SB1")+_prod)
					
								If Found()
									DbSelectArea("SG1")
									//DbOrderNickName("SG1GRD1")
									DbSetOrder(6)
									DbSeek(xfilial("SG1")+PADR(_prod,15)+PADR(_comp,15)+_seq1+cRevisao)
						
									If !Found() 
										If len(alltrim(_comp)) = 15
/*
											RecLock("SG1",.T.) 
											Replace G1_FILIAL		with xfilial("SG1")
											Replace G1_COD			with _prod
											Replace G1_COMP			with _comp
											Replace G1_TRT			with _seq1
											Replace G1_QUANT		with _qtd
											Replace G1_INI			with ddatabase
											Replace G1_FIM			with ctod("31/12/2049")
											Replace G1_NIV			with _niv
											Replace G1_NIVINV		with _xnv
											Replace G1_REVINI		with cRevisao
											Replace G1_REVFIM		with cRevisao
											MsUnLock()
*/
											_qry := "Insert into "+RetSqlName("SG1")+" (G1_FILIAL, G1_COD, G1_COMP, G1_TRT, G1_QUANT, G1_INI, G1_FIM, G1_NIV, G1_NIVINV, G1_REVINI," 
											_qry += "G1_REVFIM, R_E_C_N_O_) Values ('"+xfilial("SG1")+"','"+_prod+"','"
											_qry += _comp+"','"+_seq1+"',"+alltrim(str(_qtd))+",'"+dtos(ddatabase)+"'," 
											_qry += "'20491231', '"+_niv+"', '"+_xnv+"', '"+cRevisao+"', '"+cRevisao+"',(Select max(R_E_C_N_O_)+1 from SG1010))"
					
											nStatus := TcSqlExec(_qry)
									
											if (nStatus < 0)
									  			MsgBox(TCSQLError(), "Erro no comando", "Stop") 
									  		endif
										
										Endif
									Else
										RecLock("SG1",.F.)
										Replace G1_QUANT		with _qtd
										MsUnLock()
									Endif
								Else
									Alert("O produto "+_prod+" n�o est� cadastrado no cadastro de produtos!")
								Endif
							Endif
						Next
					Endif
		
					DbSelectArea("SZG")
					DbSkip()
				End
			Endif
			DbSelectArea("SZE")
			DbSkip()
		End
/*
		DbSelectArea("SG1")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SG1")+substr(cProduto,1,8))
	
		While !EOF() .and. SubStr(SG1->G1_COD,1,8) == substr(cProduto,1,8)
			_achou := .T.
			DbSelectArea("SZE")
			DbSetOrder(2)
			DbSeek(xfilial("SZE")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+SG1->G1_TRT+Padr(SubStr(SG1->G1_COMP,1,8),15))
			If !Found()
				_achou := .f.
			Endif
		
			DbSelectArea("SZG")
			DbSetOrder(2)
			DbSeek(xfilial("SZG")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))
			If !Found() .or. SZG->ZG_CORNV <> SubStr(SG1->G1_COMP,9,3)
				_achou := .f.
			Endif
			
			DbSelectArea("SZK")
			DbSetOrder(2)
			DbSeek(xfilial("SZK")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,12,4))
			If !Found() .or. SZK->ZK_TAMNV <> SubStr(SG1->G1_COMP,12,4)
				_achou := .f.
			Endif
	
			If !_achou .and. SubStr(SG1->G1_COMP,1,1) <> "B"
				RecLock("SG1",.F.)
				DbDelete()
				MsUnLock()
			Endif
			DbSelectArea("SG1")
			DbSkip()
		End
*/

_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ from "+RetSqlName("SG1")+" SG1 "
_qry += "where SG1.D_E_L_E_T_ = '' and Left(G1_COD,8) = '"+substr(cProduto,1,8)+"' "
_qry += "and (Select ZE_COMP from SZE010 SZE where SZE.D_E_L_E_T_ = '' and ZE_FILIAL = '"+xfilial("SZE")+"' and "
_qry += "ZE_PRODUTO = SubString(G1_COD,1,8) and ZE_REVISAO = G1_REVINI and ZE_INDICE = G1_TRT and ZE_COMP = SubString(G1_COMP,1,8)) "
_qry += "is null and SubString(G1_COMP,1,1) <> 'B' "

TcSqlExec(_qry)

_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ from "+RetSqlName("SG1")+" SG1 "
_qry += "where SG1.D_E_L_E_T_ = '' and Left(G1_COD,8) = '"+substr(cProduto,1,8)+"' "
_qry += "and (Select ZG_COMP from SZG010 SZG where SZG.D_E_L_E_T_ = '' and ZG_FILIAL = '"+xfilial("SZG")+"' and "
_qry += "ZG_PRODUTO = SubString(G1_COD,1,8) and ZG_REVISAO = G1_REVINI and ZG_INDICE = G1_TRT and ZG_COMP = SubString(G1_COMP,1,8) "
_qry += "and ZG_CORNV = SubString(G1_COMP,9,3)) is null and SubString(G1_COMP,1,1) <> 'B' "

TcSqlExec(_qry)
//caio 
_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ from "+RetSqlName("SG1")+" SG1 "
_qry += "where SG1.D_E_L_E_T_ = '' and Left(G1_COD,8) = '"+substr(cProduto,1,8)+"' "
_qry += "and (Select ZK_COMP from SZK010 SZK where SZK.D_E_L_E_T_ = '' and ZL_FILIAL = '"+xfilial("SZK")+"' and "
_qry += "ZK_PRODUTO = SubString(G1_COD,1,8) and ZK_REVISAO = G1_REVINI and ZK_INDICE = G1_TRT and ZK_COMP = SubString(G1_COMP,1,8) "
_qry += "and ZK_TAMNV = SubString(G1_COMP,12,4)) is null and SubString(G1_COMP,1,1) <> 'B' "

TcSqlExec(_qry)

_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ from "+RetSqlName("SG1")+" SG1 where SG1.D_E_L_E_T_ = '' and left(G1_COD,8) = '"+substr(cProduto,1,8)+"' and Left(G1_COMP,1) <> 'B' "
_qry += "and (Select top 1 ZG_CORNV from "+RetSqlName("SZG")+" SZG where SZG.D_E_L_E_T_ = '' and Left(G1_COD,8) = left(ZG_PRODUTO,8) and Left(G1_COMP,8) = Left(ZG_COMP,8) and SubString(G1_COD,9,3) = ZG_COR and SubString(G1_COMP,9,3) = ZG_CORNV) is null "

TcSqlExec(_qry)

//_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ from "+RetSqlName("SG1")+" SG1 where SG1.D_E_L_E_T_ = '' and left(G1_COD,8) = 'B"+substr(cProduto,1,7)+"' "
//_qry += "and (Select top 1 ZG_CORNV from "+RetSqlName("SZG")+" SZG where SZG.D_E_L_E_T_ = '' and Left(G1_COD,8) = 'B'+left(ZG_PRODUTO,7) and Left(G1_COMP,8) = Left(ZG_COMP,8) and SubString(G1_COD,9,3) = ZG_COR and SubString(G1_COMP,9,3) = ZG_CORNV) is null "

//TcSqlExec(_qry)
// Aguardando Valida��o 


//_qry := " UPDATE "+RetSqlName("SG1")+"  SET D_E_L_E_T_ = '*' , R_E_C_D_E_L_ = R_E_C_N_O_  WHERE NOT EXISTS(SELECT * FROM "+RetSqlName("SZK")+" WHERE ZK_TAMNV = RIGHT(G1_COMP,4) AND ZK_COMP = LEFT(G1_COMP,8)  "
//_qry += " AND ZK_TAM = RIGHT(G1_COD,4) AND ZK_PRODUTO = left(G1_COD,8) AND SZK010.D_E_L_E_T_ ='') AND left(G1_COD,8) = '"+substr(cProduto,1,8)+"' AND D_E_L_E_T_ = ''   "

//TcSqlExec(_qry)

//	If MsgYesNo("Deseja recalcular N�veis?")
//		MA320Nivel(Nil,.T.,.F.)
//	Endif
	
	Endif
/* //1

	DbSelectarea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+substr(cProduto,1,8))

	While !EOF() .and. Left(B1_COD,8) == substr(cProduto,1,8)
/*
/*
		_qry := "Select G1_COD, G1_COMP, count(*) as QTD from "+RetSqlName("SG1")+" (nolock) where left(G1_COMP,1) = 'B' and D_E_L_E_T_ = '' and G1_COD = '"+SB1->B1_COD+"' and G1_REVINI = '"+cRevisao+"' "
		_qry += "group by G1_COD, G1_COMP "
		_qry += "having count(*) > 1 "

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPDUP",.T.,.T.)
		DbselectArea("TMPDUP")
		DbGoTop()
	
//	_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ from "+RetSqlName("SG1")+" SG1 where R_E_C_N_O_ in (
//	_qry += "Select top "+str(TMPDUP->QTD-1)+" R_E_C_N_O_ from "+RetSqlName("SG1")+" where left(G1_COMP,1) = 'B' and D_E_L_E_T_ = '' and G1_COD like '"+SB1->B1_COD+"%' "
//	_qry += "order by R_E_C_N_O_ desc) "

		If TMPDUP->QTD-1 > 0
			_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ from "+RetSqlName("SG1")+" SG1 (nolock) where G1_REVINI = '"+cRevisao+"' and R_E_C_N_O_ in ( "
			_qry += "Select top "+ alltrim(str(TMPDUP->QTD-1))+" R_E_C_N_O_ from "+RetSqlName("SG1")+" (nolock) where G1_REVINI = '"+cRevisao+"' and left(G1_COMP,1) = 'B' and D_E_L_E_T_ = '' and G1_REVINI = '"+cRevisao+"' and G1_COD like '"+SB1->B1_COD+"%' "
			_qry += "order by R_E_C_N_O_ desc) "
			_qry += "and ( "
			_qry += "Select top 1 R_E_C_N_O_ from "+RetSqlName("SG1")+" (nolock) where left(G1_COMP,1) = 'B' and D_E_L_E_T_ = '' and G1_REVINI = '"+cRevisao+"' and G1_COD like '"+SB1->B1_COD+"%' "
			_qry += "order by R_E_C_N_O_ desc) is not null"
			nStatus := TcSqlExec(_qry)
			if (nStatus < 0)
	  			MsgBox(TCSQLError(), "Erro no comando", "Stop")
  			endif
		Endif 
		DbSelectArea("TMPDUP")
		DbCloseArea()
*/	
		//_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ from "+RetSqlName("SG1")+" SG1 (nolock) where G1_COD = 'B"+SubStr(SB1->B1_COD,1,7)+SubStr(SB1->B1_COD,9,7)+"' and SG1.D_E_L_E_T_ = '' and G1_REVINI = '"+cRevisao+"' and "
		//_qry += "((Select top 1 ZE_COMP from "+RetSqlName("SZE")+" SZE (nolock) where SZE.D_E_L_E_T_ = '' and G1_REVINI = ZE_REVISAO and left(G1_COMP,8) = left(ZE_COMP,8) and left(ZE_PRODUTO,8) = '"+SubStr(SB1->B1_COD,1,8)+"' ) is null or "
		//_qry += "(Select top 1 ZG_COMP from "+RetSqlName("SZG")+" SZG (nolock) where SZG.D_E_L_E_T_ = '' and G1_REVINI = ZG_REVISAO and left(G1_COMP,8) = left(ZG_COMP,8) and left(ZG_PRODUTO,8) = '"+SubStr(SB1->B1_COD,1,8)+"' and ZG_COR = SubString(G1_COD,9,3) and ZG_CORNV = SubString(G1_COMP,9,3) ) is null or "
		//_qry += "(Select top 1 ZK_COMP from "+RetSqlName("SZK")+" SZK (nolock) where SZK.D_E_L_E_T_ = '' and G1_REVINI = ZK_REVISAO and left(G1_COMP,8) = left(ZK_COMP,8) and left(ZK_PRODUTO,8) = '"+SubStr(SB1->B1_COD,1,8)+"' and ZK_TAM = SubString(G1_COD,12,4) and ZK_TAMNV = SubString(G1_COMP,12,4) ) is null) "

/*  //1		
		_qry := "SELECT 'update "+RetSqlName("SG1")+" set D_E_L_E_T_ = ''*'', R_E_C_D_E_L_ = R_E_C_N_O_ where R_E_C_N_O_ in ('+ ( "
		_qry += "Select cast(R_E_C_N_O_ as varchar) + ',' AS [text()] "
		_qry += "from "+RetSqlName("SG1")+" SG1 (nolock) "
		_qry += "where G1_COD = 'B"+SubStr(SB1->B1_COD,1,7)+SubStr(SB1->B1_COD,9,7)+"' and SG1.D_E_L_E_T_ = '' and G1_REVINI = '"+cRevisao+"' and "
		_qry += "((Select top 1 ZE_COMP from "+RetSqlName("SZE")+" SZE (nolock) where SZE.D_E_L_E_T_ = '' and G1_REVINI = ZE_REVISAO and left(G1_COMP,8) = left(ZE_COMP,8) and left(ZE_PRODUTO,8) = '"+SubStr(SB1->B1_COD,1,8)+"' ) is null or "
		_qry += " (Select top 1 ZG_COMP from "+RetSqlName("SZG")+" SZG (nolock) where SZG.D_E_L_E_T_ = '' and G1_REVINI = ZG_REVISAO and left(G1_COMP,8) = left(ZG_COMP,8) and left(ZG_PRODUTO,8) = '"+SubStr(SB1->B1_COD,1,8)+"' and ZG_COR = SubString(G1_COD,9,3) and ZG_CORNV = SubString(G1_COMP,9,3) ) is null or "
		_qry += " (Select top 1 ZK_COMP from "+RetSqlName("SZK")+" SZK (nolock) where SZK.D_E_L_E_T_ = '' and G1_REVINI = ZK_REVISAO and left(G1_COMP,8) = left(ZK_COMP,8) and left(ZK_PRODUTO,8) = '"+SubStr(SB1->B1_COD,1,8)+"' and ZK_TAM = SubString(G1_COD,12,4) and ZK_TAMNV = SubString(G1_COMP,12,4) ) is null) "
		_qry += "FOR XML PATH ('')) as UPD "

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPUPD",.T.,.T.)
		DbselectArea("TMPUPD")
		DbGoTop()
		
		_QRY := SUBSTR(TMPUPD->UPD,1,LEN(ALLTRIM(TMPUPD->UPD))-1)+")"

		IF len(alltrim(_qry)) > 10
			nStatus := TcSqlExec(_qry)
		
			if (nStatus < 0)
	  			MsgBox(TCSQLError(), "Erro no comando", "Stop")
	  		endif
		ENDIF
		DbSelectArea("TMPUPD")
		DbCloseArea()
		
		DbSelectArea("SB1")
		DbSkip()
	End
*/
Return

User Function AtuAcols()
// Rotina para atualizar acols na mudan�a de linha de acols de componentes
// Salva cada acol antes de mudar de linha e atualizar

	Local _i,_w, nx

	For _i := 1 to len(oMSNewGe1:aCols)
		If oMSNewGe1:aCols[_i,len(oMSNewGe1:aHeader)+1] = .F.
			DbSelectArea("SZE")
			DbSetOrder(2)
			DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[_i,5]+oMSNewGe1:aCols[_i,1])
		
			If alltrim(oMSNewGe1:aCols[_i,1]) <> "" .and. alltrim(oMSNewGe1:aCols[_i,1]) <> ""
				If !Found()
					RecLock("SZE",.T.)
					Replace ZE_FILIAL			with xfilial("SZE")
					Replace ZE_PRODUTO		with SubStr(cProduto,1,15)
					Replace ZE_COMP			with oMSNewGe1:aCols[_i,1]
					Replace ZE_REVISAO		with cRevisao
					Replace ZE_CORTE			with oMSNewGE1:aCols[_i,4]
					Replace ZE_INDICE			with oMSNewGE1:aCols[_i,5]
					MsUnLock()
				Else
					If SZE->ZE_CORTE <> oMSNewGE1:aCols[_i,4]
						u_ajustac(SubStr(cProduto,1,15),cRevisao,oMSNewGe1:aCols[_i,5],oMSNewGe1:aCols[_i,1],SZE->ZE_CORTE)
					Endif
					RecLock("SZE",.F.)
					Replace ZE_CORTE			with oMSNewGE1:aCols[_i,4]
					MsUnLock()
				Endif
			Endif
		Else
			DbSelectArea("SZE")
			DbSetOrder(2)
			DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[_i,5]+oMSNewGe1:aCols[_i,1])
		
			If Found() //oMSNewGe1:aCols[_i,5]
				RecLock("SZE",.F.)
				DbDelete()
				SZE->(MsUnLock("SZE"))
			Endif
		
			_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where D_E_L_E_T_ = '' and SubString(G1_COD,1,8) = '"+SubStr(cProduto,1,8)+"' and G1_REVINI = '"+cRevisao+"' and SubString(G1_COMP,1,8) = '"+alltrim(oMSNewGe1:aCols[_i,1])+"'"
			nStatus := TcSqlExec(_qry)
			
			if (nStatus < 0)
	  			MsgBox(TCSQLError(), "Erro no comando", "Stop")
  			endif
		Endif
	
		DbSelectArea("SZE")
	
	Next
 

 	For _i := 1 to len(oMSCor:aCols)
		DbSelectArea("SZG")
		DbSetOrder(2)
		DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+oMsCor:aCols[_i,3]+oMsCor:aCols[_i,4]+oMsCor:aCols[_i,1])

		If alltrim(oMsCor:aCols[_i,3]) <> "" .and. alltrim(oMsCor:aCols[_i,3]) <> ""
			_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsCor:aCols[_i,3]) .AND. Alltrim(x[5]) == alltrim(oMsCor:aCols[_i,4])})

			IF Found()
			// TODO Alterado a variavel _tem por _i. Para buscar no array de cores (Weskley Silva 28.08.2019)
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZG",.F.)
					Replace ZG_CORNV	with oMsCor:aCols[_i,2]
					MsUnlock()
				Else
					Reclock("SZG",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Else
			// TODO Alterado a variavel _tem por _i. Para buscar no array de cores (Weskley Silva 28.08.2019)
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZG",.T.)
					Replace ZG_FILIAL	with xfilial("SZG")
					Replace ZG_PRODUTO 	with cProduto
					Replace ZG_COMP		with oMsCor:aCols[_i,3]
					Replace ZG_COR		with oMsCor:aCols[_i,1]
					Replace ZG_CORNV		with oMsCor:aCols[_i,2]
					Replace ZG_REVISAO	with cRevisao
					Replace ZG_INDICE		with oMsCor:aCols[_i,4]
					MsUnlock()
				Endif
			Endif
		Endif
	Next

 
	For _i := 1 to len(oMSTam:aCols)
		DbSelectArea("SZK")
		DbSetOrder(2)
		DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+oMsTam:aCols[_i,3]+oMsTam:aCols[_i,4]+oMsTam:aCols[_i,1])

		If alltrim(oMsTam:aCols[_i,3]) <> "" .and. alltrim(oMsTam:aCols[_i,3]) <> ""
			_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsTam:aCols[_i,3]) .AND. Alltrim(x[5]) == alltrim(oMsTam:aCols[_i,4])})
			IF Found()
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZK",.F.)
					Replace ZK_TAMNV	with oMsTam:aCols[_i,2]
					MsUnlock()
				Else
					RecLock("SZK",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Else
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZK",.T.)
					Replace ZK_FILIAL	with xfilial("SZG")
					Replace ZK_PRODUTO 	with cProduto
					Replace ZK_COMP		with oMsTam:aCols[_i,3]
					Replace ZK_TAM		with oMsTam:aCols[_i,1]
					Replace ZK_TAMNV		with oMsTam:aCols[_i,2]
					Replace ZK_REVISAO	with cRevisao
					Replace ZK_INDICE		with oMsTam:aCols[_i,4]
					MsUnlock()
				Endif
			Endif
		Endif
	Next


	For _i := 1 to len(oMSQtd:aCols)
		DbSelectArea("SZF")
		DbSetOrder(2)
		DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+oMsQtd:aCols[_i,LEN(oMsQtd:aHeader)-1]+SubStr(oMsQtd:aCols[_i,Len(oMsQtd:aHeader)],1,15)+alltrim(oMsQtd:aCols[_i,1]))
	
		If Alltrim(oMsQtd:aCols[_i,len(oMsQtd:aHeader)-1]) <> ""
			_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsQtd:aCols[_i,LEN(OMSQTD:AHEADER)-1]) .AND. Alltrim(x[5]) == alltrim(oMsQtd:aCols[_i,Len(oMsQtd:aHeader)])})
			IF Found()
			// TODO Alterado a variavel _tem por _i. Para buscar no array de cores (Weskley Silva 28.08.2019)
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZF",.F.)
					For _w := 3 to Len(oMsQtd:aHeader)-2
						_campo := "SZF->"+oMsQtd:aheader[_w][2]		//ZF_TAM"+StrZero(_w,3)
						Replace &(_campo) with oMsQtd:ACOLS[_i,_w]
					Next
					MsUnlock()
				Else
					RecLock("SZF",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Else
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZF",.T.)
					Replace ZF_FILIAL	with xfilial("SZF")
					Replace ZF_PRODUTO 	with cProduto
					Replace ZF_COMP		with oMsQtd:aCols[_i,len(oMsQtd:aHeader)-1]
					Replace ZF_COR		with oMsQtd:aCols[_i,1]
					Replace ZF_REVISAO	with cRevisao
					Replace ZF_INDICE		with oMsQtd:aCols[_i,Len(oMsQtd:aHeader)]
					For _w := 3 to Len(oMsQtd:aHeader)-2
						_campo := "SZF->"+oMsQtd:aheader[_w][2]//ZF_TAM"+StrZero(_w,3)
						Replace &(_campo) with oMsQtd:ACOLS[_i,_w]
					Next
					MsUnlock()
				Endif
			Endif
		Endif
	Next

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""
		DbselectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]))
		cColunx 		:= SB4->B4_COLUNA
	
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+alltrim(SB4->B4_COD))
	
		aCor := {}
		While !EOF() .and. SubStr(B1_COD,1,8) == SubStr(SB4->B4_COD,1,8)
	
			_tem := ASCAN(aCor, {|x| x == SubStr(SB1->B1_COD,9,3)})
		
			If _tem == 0
				aadd(aCor, SubStr(SB1->B1_COD,9,3))
			Endif
			DbSelectArea("SB1")
			DbSkip()
		End
	
		For _w := 1 to len(aCor)
			cCor += aCor[_w]+"|"
		Next
//	If len(alltrim(ccor)) > 100 
//		cCor := ""			
//	Endif	
	Endif

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""
		oMSCor:aCols := {}
		oMSCor:Refresh()

		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)

			DbSelectArea("SZG")
			DbSetOrder(2)
			DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+PADR(oMSNewGe1:aCols[oMSNewGe1:nAT,1],15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SZD->ZD_COR)
	
			IF Found()
				Aadd(oMSCor:aCols, {SZG->ZG_COR,SZG->ZG_CORNV,SZG->ZG_COMP,SZG->ZG_INDICE,.F.})
			Else
				Aadd(oMSCor:aCols, {SZD->ZD_COR,"   ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
			Endif
			DbSelectArea("SZD")
			DbSkip()
		End

		oMSTam:aCols := {}
		oMSTam:Refresh()

		_ac := {}

		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SubStr(cProduto,1,8))
		oMSTam:aCols := {}

		While !EOF() .and. SubStr(B1_COD,1,8) == SubStr(cProduto,1,8)
			_tem := ASCAN( oMSTam:aCols, {|x| Alltrim(x[2]) == SubStr(SB1->B1_COD,12,4)})
			If _tem == 0
				DbSelectArea("SZK")
				DbSetOrder(2)
				DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+padr(oMSNewGe1:aCols[oMSNewGe1:nAT,1],15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SubStr(SB1->B1_COD,12,4))
	
				IF Found()
//				While !EOF() .and. alltrim(ZK_PRODUTO) == alltrim(cProduto) .and. alltrim(ZK_COMP) == alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) .and. alltrim(ZK_REVISAO) == cRevisao
					Aadd(_aC, {0,SZK->ZK_TAM,SZK->ZK_TAMNV,SZK->ZK_COMP,SZK->ZK_INDICE,.F.})
//					DbSelectArea("SZK")
//					DbSkip()
//				End
				Else
					DbSelectArea("SB4")
					DbSetOrder(1)
					DbSeek(xfilial("SB4")+oMSNewGe1:aCols[oMSNewGe1:nAT,1])
				
					DbSelectArea("SBV")
					DbSetOrder(1)
					DbSeek(xfilial("SBV")+SB4->B4_COLUNA)
				
					IF SBV->BV_CHAVE = '00TU'
						Aadd(_aC, {0,SubStr(SB1->B1_COD,12,4),"00TU",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
					ELSE
						Aadd(_aC, {0,SubStr(SB1->B1_COD,12,4),"    ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
					Endif
				Endif
			Endif

			DbSelectArea("SB1")
			DbSkip()
		End

		if Len(_ac) > 0
			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna+ "' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY R_E_C_N_O_"
		
			cQuery := ChangeQuery(cQuery)
			If Select("TMPSBV") > 0
				TMPSBV->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

			For nX:= 1 to len(_ac)
				DbSelectArea("TMPSBV")
				DbGoTop()
				_w := 0
				While !EOF()
					_w++
					If alltrim(TMPSBV->BV_CHAVE) == alltrim(_ac[nX,2])
						_ac[nX,1] := _w
					Endif
					DbSkip()
				End
		
			Next
			DbSelectArea("TMPSBV")
			DbCloseArea()
	
			ASORT(_ac, , , { |x,y| x[1] < y[1] } )
	
			For nX:=1 to len(_ac)
				If ASCAN( oMSTam:aCols, {|x| alltrim(x[1]) == alltrim(_ac[nX,2])})=0
					Aadd(oMSTam:aCols, {_ac[nX,2],_ac[nX,3],_ac[nX,4],_ac[nX,5],.F.})
				Endif
			Next
		Endif

		oMSQtd:aCols := {}
		oMSQtd:Refresh()

		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))
		oMSQtd:aCols := {}

		While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)

			DbSelectArea("SZF")
			DbSetOrder(2)
			DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[oMSNewGe1:nAT,1]+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SZD->ZD_COR)
	
			IF Found()
				aFieldFill := {}
				Aadd(aFieldFill, SZF->ZF_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZF->ZF_COR,"BV_DESCRI"))

				For nX := 3 to Len(oMsQtd:aHeader)-2
					_campo := "SZF->"+oMsQtd:aheader[nX][2]//ZF_TAM"+StrZero(nX,3)
					Aadd(aFieldFill,&_campo)
				Next
				Aadd(aFieldFill, SZF->ZF_COMP)
				Aadd(aFieldFill, SZF->ZF_INDICE)
				Aadd(aFieldFill, .F.)
				Aadd(oMsQtd:aCols, aFieldFill)
			Else
				aFieldFill := {}
				Aadd(aFieldFill, SZD->ZD_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))
			
				For nX := 3 to Len(oMsQtd:aHeader)-2
					Aadd(aFieldFill,0)
				Next
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,5])
				Aadd(aFieldFill, .F.)
				Aadd(oMsQtd:aCols, aFieldFill)
			Endif

			DbSelectArea("SZD")
			DbSkip()
		End

	Else
		oMSCor:aCols := {}
		oMSCor:Refresh()

		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			Aadd(oMSCor:aCols, {SZD->ZD_COR,"   ",space(15),Space(3),.F.})

			DbSelectArea("SZD")
			DbSkip()
		End
		oMSCor:Refresh()

		oMSTam:aCols := {}
		oMSTam:Refresh()

		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SubStr(cProduto,1,8))

		While !EOF() .and. SubStr(B1_COD,1,8) == SubStr(cProduto,1,8)
			_tem := ASCAN( oMSTam:aCols, {|x| Alltrim(x[1]) == SubStr(SB1->B1_COD,12,4)})
			If _tem == 0
				Aadd(oMSTam:aCols, {SubStr(SB1->B1_COD,12,4),"    ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
			Endif

			DbSelectArea("SB1")
			DbSkip()
		End

		oMSTam:Refresh()

		oMSQtd:aCols := {}
		oMSQtd:Refresh()

		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			aFieldFill := {}
			Aadd(aFieldFill, SZD->ZD_COR)
			Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))
		
			For nX := 3 to Len(oMsQtd:aHeader)-2
				Aadd(aFieldFill,0)
			Next
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,5])
//  		Aadd(aFieldFill, Space(15))
			Aadd(aFieldFill, .F.)
			Aadd(oMsQtd:aCols, aFieldFill)

			DbSelectArea("SZD")
			DbSkip()
		End
		oMSQtd:Refresh()

	Endif

	u_vldfecha(oMSNewGe1:nAT)

	oMSCor:Refresh()
	oMsQtd:Refresh()
	oMsTam:Refresh()
Return

user Function cadEst(_cod,_coluna,_ind)
// Cadastra produto de Corte para usar na estrutura de produtos

	cParCicPad:= AllTrim(SuperGetMV("HP_CICPAD",.F.,"002"))		
	cDscCicPad:= POSICIONE("ZB7",1,xFilial("ZB7")+cParCicPad,"ZB7_DSCCIC")

	DbSelectArea("SB4")
	DbSetOrder(1)
	DbSeek(xfilial("SB4")+PADR("B"+SubStr(_cod,1,7),15))

	If !Found()
		EMail(PADR("B"+SubStr(_cod,1,7),15))
		RecLock("SB4",.T.)
		Replace B4_FILIAL 		with xfilial("SB4")
		Replace B4_COD			with PADR("B"+SubStr(_cod,1,7),15)
		Replace B4_TIPO			with "MI"
		Replace B4_LOCPAD		with "AP"
		Replace B4_GRUPO		with "MP13"
		Replace B4_DESC			with "CORTE TECIDO - REF "+SubString(_COD,1,8)
		Replace B4_UM			with "UN"
		Replace B4_POSIPI		With "60041092"
		Replace B4_YORIGEM		with "0"
		Replace B4_CLASFIS		with "00"
		Replace B4_RASTRO		with "N"
		Replace B4_LINHA		with "COR"
		Replace B4_COLUNA		with _coluna
		MsUnLock()
	Endif

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbGoTop()
	DbSeek(xfilial("SB1")+PADR("B"+SubStr(_cod,1,7)+SubStr(_cod,9,7),15))

	If !Found()
		RecLock("SB1",.T.)
		Replace B1_FILIAL			with xfilial("SB1")
		Replace B1_COD 				with "B"+SubStr(_cod,1,7)+SubStr(_cod,9,7)
		Replace B1_DESC				with "CORTE TECIDO - REF "+SubString(_COD,1,15)
		Replace B1_TIPO				with "MI"
		Replace B1_GRUPO			with "MP13"
		Replace B1_UM				with "UN"
		Replace B1_LOCPAD			with "AP"
		Replace B1_SEGUM			with "UN"
		Replace B1_CONV				with 1
		Replace B1_TIPCONV			with "M"
		Replace B1_CONTA			with "101030201001"
		Replace B1_POSIPI			with "60041092"
		Replace B1_TIPODEC			with "N"
		Replace B1_ORIGEM			with "0"
		Replace B1_REVATU			with "001"
		Replace B1_CLASFIS			with "00"
		Replace B1_GRADE			with "S"
		Replace B1_LOCALIZ			with "N"
		Replace B1_RASTRO			with "N"
		Replace B1_VEREAN			with "13"
		Replace B1_TIPOBN			with "MI"
		Replace B1_GARANT			with "N"
		Replace B1_OPERPAD			with "01"
		Replace B1_GRTRIB with "007"
		Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+"MP13","BM_CTACUS")
		Replace B1_CTADESP 		with POSICIONE("SBM",1,xFilial("SBM")+"MP13","BM_CTADES")
		Replace B1_MRP			with "S"
		Replace B1_XCODCIC		with cParCicPad
		Replace B1_XDSCCIC		with cDscCicPad
		
		MsUnLock()
	Endif

Return("B"+SubStr(_cod,1,7)+SubStr(_cod,9,7))

user Function EstSup(_oldprod,_nwprod,_IND)
//Cadastra estrutura de produto para produto de Corte

	DbSelectArea("SG1")
	DbSetOrder(1)
	DbSeek(xfilial("SG1")+PADR(_oldprod,15))

	While !EOF() .and. SG1->G1_COD = PADR(_oldprod,15)

		IF SG1->G1_REVINI = cRevisao
			_ind := SG1->G1_TRT
		Endif
		DbSelectarea("SG1")
		DbSkip()
	End

//	DbSelectArea("SG1")
//	DbOrderNickName("SG1GRD1")
//	DbSeek(xfilial("SG1")+PADR(_oldprod,15)+PADR(_nwprod,15)+_IND+cRevisao)

	_qry := "Select * from "+RetSqlName("SG1")+" where D_E_L_E_T_ = '' and G1_FILIAL = '"+xfilial("SG1")+"' and G1_COD = '"+_oldprod+"' and G1_COMP = '"+_nwprod+"' and G1_REVINI = '"+cRevisao+"' "
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSG1A",.T.,.T.)
	
	DbSelectarea("TMPSG1A")
	DbGoTop()
	
	If EOF()

If len(alltrim(_nwprod)) = 15
/*
			RecLock("SG1",.T.)
			Replace G1_FILIAL		with xfilial("SG1")
			Replace G1_TRT			with _IND
			Replace G1_COD			with _oldprod
			Replace G1_COMP			with _nwprod
			Replace G1_QUANT		with 1
			Replace G1_INI			with ddatabase
			Replace G1_FIM			with ctod("31/12/2049")
			Replace G1_NIV			with "01"
			Replace G1_NIVINV		with "99"
			Replace G1_REVINI		with cRevisao
			Replace G1_REVFIM		with cRevisao
			MsUnLock()
*/
			_qry := "Insert into "+RetSqlName("SG1")+" (G1_FILIAL, G1_COD, G1_COMP, G1_TRT, G1_QUANT, G1_INI, G1_FIM, G1_NIV, G1_NIVINV, G1_REVINI," 
			_qry += "G1_REVFIM, R_E_C_N_O_) Values ('"+xfilial("SG1")+"','"+_oldprod+"','"
			_qry += _nwprod+"','"+_IND+"',1,'"+dtos(ddatabase)+"'," 
			_qry += "'20491231', '01', '99', '"+cRevisao+"', '"+cRevisao+"',(Select max(R_E_C_N_O_)+1 from SG1010))"

			nStatus := TcSqlExec(_qry)
	
			if (nStatus < 0)
	  			MsgBox(TCSQLError(), "Erro no comando", "Stop") 
	  		endif

		Endif
	Endif
  DbSelectarea("TMPSG1A")
    DbCloseArea()
    DbSelectArea("SG1")
Return()

Static Function LimpaCor()
// Replica tamanhos para cols de tamanho

	Local nX
	Local nX2

	If MsgYesNo("Deseja limpar a estrutura da cor "+oMSQtd:aCols[oMSQtd:nat,1]+" para o componente "+oMsQtd:aCols[oMsQtd:nat,len(oMsQtd:aHeader)-1]+"?")


		If oMSNewGE1:aCols[oMSNewGE1:nat,4] <> "S"
			_qry := "update "+RetSqlName("SG1'")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where D_E_L_E_T_ = '' and "
			_qry += "G1_FILIAL = '"+xfilial("SG1")+"' and left(G1_COD,11) = '"+SubStr(CProduto,1,8)+oMsQtd:aCols[oMsQtd:nat,1]+"' and "
			_qry += "left(G1_COMP,8) = '"+alltrim(oMsQtd:aCols[oMsQtd:nat,len(oMsQtd:aHeader)-1])+"' and G1_REVINI = '"+cRevisao+"' "
		Else
			_qry := "update "+RetSqlName("SG1'")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where D_E_L_E_T_ = '' and "
			_qry += "G1_FILIAL = '"+xfilial("SG1")+"' and left(G1_COD,11) = 'B"+SubStr(CProduto,1,7)+oMsQtd:aCols[oMsQtd:nat,1]+"' and "
			_qry += "left(G1_COMP,8) = '"+alltrim(oMsQtd:aCols[oMsQtd:nat,len(oMsQtd:aHeader)-1])+"' and G1_REVINI = '"+cRevisao+"' "
		Endif
		
		MemoWrite("HESTP001_limpacor.txt",_Qry)
		TcSqlExec(_qry)
		
		If MsgYesNo("Deseja limpar a cor: "+oMSQtd:aCols[oMSQtd:nat,1]+" para TODOS os tamanhos?")
			For nX := 3 to len(oMsQtd:aHeader)-2
				oMSQtd:aCols[oMSQtd:nat,nX] := 0
			Next nX
		End
		oMsQtd:refresh()
	Endif

Return


Static Function ReplyCor()
// Replica a cor para acols de cor

	Local nX
	Local nX2

	If MsgYesNo("Ser�o replicadas as informa��es da Cor: "+oMSCor:aCols[oMSCor:nat,1]+" para as outras cores?")
		For nX := 1 to len(oMSCor:aCols)
			If nX <> oMSCor:nat
				oMSCor:aCols[nX,2] := oMSCor:aCols[oMSCor:nat,2]
			Endif
		Next nX
	End

	oMsCor:refresh()

Return

Static Function ReplyTam()
// Replica tamanhos para cols de tamanho

	Local nX
	Local nX2

	If MsgYesNo("Ser�o replicadas as informa��es do Tamanho: "+oMSTam:aCols[oMSTam:nat,1]+" para os outros tamanhos?")
		For nX := 1 to len(oMSTam:aCols)
			If nX <> oMSTam:nat
				oMSTam:aCols[nX,2] := oMSTam:aCols[oMSTam:nat,2]
			Endif
		Next nX
	End

	oMsTam:refresh()

Return

Static Function ReplyQtdLn()
// Replica as quantidades por linha

	Local nX
	Local nX2

	If MsgYesNo("Ser�o replicadas as informa��es da Cor: "+oMSQtd:aCols[oMSQtd:nat,1]+" para as outras cores?")
		For nX := 1 to len(oMSQtd:aCols)
			for nX2 := 3 to len(oMSQtd:aCols[oMSQtd:nat])-1
				If nX <> oMSQtd:nat .and. !AllTrim(Str(nX2)) $ "1"
					oMSQtd:aCols[nX,nX2] := oMSQtd:aCols[oMSQtd:nat,nX2]
				Endif
			Next nX2
		Next nX
	End

	oMsQtd:refresh()

Return

Static Function ReplyQtdCl()
// Copia as quatidades para as colunas

	Local nX
	Local nX2

	If MsgYesNo("Ser�o replicadas as informa��es do Tamanho: "+oMSQtd:aCols[oMSQtd:nat,1]+" para outros Tamanhos?")
		for nX2 := omsqtd:obrowse:colpos+1 to len(oMSQtd:aCols[oMSQtd:nat])-3 //3
			If nX <> oMSQtd:nat
				oMSQtd:aCols[oMSQtd:nat,nX2] := oMSQtd:aCols[oMSQtd:nat,omsqtd:obrowse:colpos] //2
			Endif
		Next nX2
	End

	oMsQtd:refresh()

Return

/*
Static Function ChecaCor()
// Rotina para cadastro de Estrutura automaticamente com base em cor j� existente

Local i      
	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+alltrim(cProduto))
	
	While !EOF() .and. SubStr(cProduto,1,8) == SubStr(SB1->B1_COD,1,8)
		DbSelectArea("SZE")
		DbSetOrder(1)
		DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao)
		
		If Found()
			DbSelectArea("SZG")
			DbSetOrder(1)
			DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SZG->ZG_COMP+SubStr(SB1->B1_COD,9,3))
		
			If !Found()
				IF MsgYesNo("Cor "+SubStr(SB1->B1_COD,9,3)+" n�o tem estrutura cadastrada. Deseja copiar de outra cor?")
					Pergunte("CORNV",.T.)
					DbSelectArea("SZE")
					DbSetOrder(1)
					DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao)
					While !EOF() .and. SubStr(cProduto,1,8) = SubStr(SZE->ZE_PRODUTO,1,8) .and. SZE->ZE_REVISAO = cRevisao
						DbSelectArea("SZG")
						DbSetOrder(1)
						DbSeek(xfilial("SZG")+SZE->ZE_PRODUTO+cRevisao+SZE->ZE_COMP+MV_PAR01)
						_cor := SZG->ZG_CORNV
						RecLock("SZG",.T.)
							Replace ZG_FILIAL		with xfilial("SZG")
							Replace ZG_PRODUTO		with SZE->ZE_PRODUTO
							Replace ZG_COMP			with SZE->ZE_COMP
							Replace ZG_COR			with SubStr(SB1->B1_COD,9,3)
							Replace ZG_CORNV		with _cor
							Replace ZG_REVISAO		with cRevisao
						MsUnLock()
						
						DbSelectarea("SZF")
						DbSetOrder(1)
						DbSeek(xfilial("SZF")+SZE->ZE_PRODUTO+cRevisao+SZE->ZE_COMP+MV_PAR01)
						_atam := {}
						For i := 1 to 200
							_campo := "SZF->ZF_TAM"+strzero(i,3)
							Aadd(_atam,&_campo)
						Next
						
						RecLock("SZF",.T.)
							Replace ZF_FILIAL	with xfilial("SZF")
							Replace ZF_PRODUTO 	with SZE->ZE_PRODUTO
							Replace ZF_COMP		with SZE->ZE_COM
							Replace ZF_COR		with SubStr(SB1->B1_COD,9,3)
							Replace ZF_REVISAO	with cRevisao
						  	For i := 1 to 200
								_campo := "SZF->ZF_TAM"+StrZero(_w,3)
								Replace &(_campo) with _atam[i]
							Next
						MsUnLock()
						DbSelectArea("SZE")
						DbSkip()
					End
				Endif			
			Endif
		Endif
		
		DbSelectArea("SB1")
		DbSkip()
	End

Return
*/


User Function VldCor(_cor,_n)

	Local lRet := .T.
	Local _i

	lRet	:= U_ExistCod(5,_cor)

	For _i:=1 to len(oMSGrade:aCols)
		If alltrim(_cor) == alltrim(oMSGrade:aCols[_i,1]) .and. _i <> _n
			Alert("A COR j� foi incluida no produto, selecione outra COR.")
			lRet := .F.
			Return(lRet)
		ElseIf alltrim(_cor) == ""
			Alert("N�o poder� incluir uma linha em branco.")
			lRet := .F.
			Return(lRet)
		Endif
	Next

Return(lRet)

User Function ExistCod(nCod,cCod)
// Rotina para preenchimento dos campos de descri��o

	If nCod==1   // valida��o do NCM
		If alltrim(cCod) <> ""
	
			cCod := Alltrim(StrTran(cCod,".",""))
				
			If Select("VLDDB") > 0
				VLDDB->(DbCloseArea())
			EndIf
	 	
			cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SYD")
			cQuery  += " (nolock) WHERE D_E_L_E_T_<>'*' AND YD_TEC='"+cCod+"'"
		
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
		
			If VLDDB->COUNT==0 .and. cCod<>""
				Alert("O Codigo de NCM/POS.IPI Digitado n�o existe no cadastro.")
				oPosIPI:SetFocus()
			End
		
			VLDDB->(DbCloseArea())
		Endif
	ElseIf nCod==2  //valida��o de cor

		lRet 	:= .T.
		cLinha	:= If (cLinha<>"COR","COR",cLinha)

		cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SB1")
		cQuery  += " (nolock) WHERE D_E_L_E_T_<>'*' AND B1_FILIAL = '"+xFilial("SB1")+"'"
		cQuery  += " 		AND SubString(B1_COD,1,8)='"+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+"'"
		cQuery  += " 		AND SubString(B1_COD,9,3) ='"+cCod+"'"

		If Select("VLDDB") > 0
			VLDDB->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
		If VLDDB->COUNT==0 .and. AllTrim(cCod)<>""
			Alert("A COR selecionada n�o existe para o componete "+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+".")
			lRet := .F.
		End
		
		VLDDB->(DbCloseArea())
	
		Return(lRet)

	ElseIf nCod==3  //valida��o de Tamanho

		lRet := .T.
	
		cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SB1")
		cQuery  += " (nolock) WHERE D_E_L_E_T_<>'*' AND B1_FILIAL = '"+xFilial("SB1")+"'"
		cQuery  += " 		AND SubString(B1_COD,1,8)='"+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+"'"
		cQuery  += " 		AND SubString(B1_COD,12,4)='"+cCod+"'"

		If Select("VLDDB") > 0
			VLDDB->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
		If VLDDB->COUNT==0 .and. AllTrim(cCod)<>""
			Alert("O Tamanho Selecionado n�o existe para o componete "+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+".")
			lRet := .F.
		End
		
		VLDDB->(DbCloseArea())
	
		Return(lRet)

	ElseIf nCod==4  //Valida Status
	//"U_ExistCod(4,M->ZL_STATUS)"

		lRet := .T.
	
		cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("ZA8")
		cQuery  += " (nolock) WHERE D_E_L_E_T_ = '' AND ZA8_FILIAL = '"+xFilial("ZA8")+"' AND ZA8_CODIGO = '"+cCod+"'"
			
		If Select("VLDDB") > 0
			VLDDB->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
		If VLDDB->COUNT==0
			Alert("O codigo selecionado n�o existe no cadastro.")
			lRet := .F.
		End
		
		VLDDB->(DbCloseArea())
	
		Return(lRet)

	ElseIf nCod==5  //valida��o de cor

		lRet 	:= .T.
		cLinha	:= If (cLinha<>"COR","COR",cLinha)
	
		cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SBV")
		cQuery  += " (nolock) WHERE BV_FILIAL = '"+xFilial("SBV")+"' AND BV_TABELA = '"+cLinha+"' "
		cQuery  += " AND D_E_L_E_T_ = ''  AND BV_CHAVE='"+cCod+"'"
			
		If Select("VLDDB") > 0
			VLDDB->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
		If VLDDB->COUNT==0
			Alert("A COR selecionada n�o existe no cadastro.")
			lRet := .F.
		End
		
		VLDDB->(DbCloseArea())
	
		Return(lRet)

	End

Return


Static Function AjuSX1(cPerg)

	Local aAreaAtu	:= GetArea()
	Local aAreaSX1	:= SX1->( GetArea() )

	PutSx1(cPerg,"01","Status de Produtos","Status de Produtos","Status de Produtos","Mv_ch1","C",99,0,0,"G","U_HE001TP()",   "","","N","Mv_par01","","","","","","","","","","","","","","","","","","","","","","","",{"Selecione os status de produtos.",""},{""},{""},"")
	PutSx1("HESTC1","01","Tipo","Tipo","Tipo","Mv_ch1","C",10,0,0,"G","",   "","","N","Mv_par01","","","","","","","","","","","","","","","","","","","","","","","",{"Selecione o Tipo.",""},{""},{""},"")

	RestArea( aAreaSX1 )
	RestArea( aAreaAtu )

Return(cPerg)

User Function HE001TP() //l1Elem,lTipoRet)

	Local xvPar
	Local aArea  := GetArea()
	Local cTitulo := ""
	Local lRet  := .T.
	Local aBox  := {}
	Local nTam  := 4
	Local MvParDef := ""

	xvPar:=&(Alltrim(ReadVar()))   // Carrega Nome da Variavel do Get em Questao
	mvRet:=Alltrim(ReadVar())    // Iguala Nome da Variavel ao Nome variavel de Retorno

	dbSelectArea("ZA8")
	DbSetOrder(1)
	DbGoTop()
	cTitulo := "Status de Produtos"
	While !Eof()
		Aadd(aBox,ZA8->ZA8_CODIGO + " - " + Alltrim(ZA8->ZA8_DESCRI))
		MvParDef += SubStr(ZA8->ZA8_CODIGO,1,4)
		dbSkip()
	EndDo

	If f_Opcoes( @xvPar,;  // uVarRet
		cTitulo,;  // cTitulo
		@aBox,;   // aOpcoes
		MvParDef,;  // cOpcoes
		,;    // nLin1
		,;    // nCol1
		,;    // l1Elem
		nTam,;    // nTam
		Len( aBox ),; // nElemRet
		,;    // lMultSelect
		,;    // lComboBox
		,;    // cCampo
		,;    // lNotOrdena
		,;    // NotPesq
		,;   // ForceRetArr
		)    // F3
       
		&MvRet := xvpar                                                                          // Devolve Resultado
	EndIF

	RestArea(aArea)

Return(.T.)

User Function HE001OC(l1Elem,lTipoRet)

	Local MvPar
	Local aArea  := GetArea()
	Local cTitulo := ""
	Local lRet  := .T.
	Local aBox  := {}
	Local nTam  := 6
	Local MvParDef := ""
	Public hocasiao := ""

	MvPar:=&(Alltrim(ReadVar()))   // Carrega Nome da Variavel do Get em Questao
	mvRet:=Alltrim(ReadVar())    // Iguala Nome da Variavel ao Nome variavel de Retorno

	dbSelectArea("ZAD")
	DbSetOrder(1)
	DbGoTop()
	cTitulo := "Ocasiao"
	While !Eof()
		Aadd(aBox,ZAD->ZAD_CODIGO + "- " + Alltrim(ZAD->ZAD_DESCRI))
		MvParDef += SubStr(ZAD->ZAD_CODIGO,1,5)+"-"
		dbSkip()
	EndDo

	If f_Opcoes( @MvPar,;  // uVarRet
		cTitulo,;  // cTitulo
		@aBox,;   // aOpcoes
		MvParDef,;  // cOpcoes
		,;    // nLin1
		,;    // nCol1
		,;    // l1Elem
		nTam,;    // nTam
		Len( aBox ),; // nElemRet
		,;    // lMultSelect
		,;    // lComboBox
		,;    // cCampo
		,;    // lNotOrdena
		,;    // NotPesq
		,;   // ForceRetArr
		)    // F3
       
		&MvRet := mvpar                                                                          // Devolve Resultado
	EndIF

	hocasiao := mvpar

	RestArea(aArea)

Return lRet


User function buscasb1()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	Pergunte("DESCSB1",.T.)

	_qry := "Select B1_DESC, B1_COD from "+RetSqlName("SB1")+" SB1 "
	_qry += "(nolock) where SB1.D_E_L_E_T_ = '' and (B1_DESC like '%"+upper(alltrim(MV_PAR01))+"%' or B1_COD like '%"+upper(alltrim(MV_PAR01))+"%')  "
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->B1_DESC,XXX->B1_COD})
		DbSkip()
	End
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()
	     
	If Len(aItems) <= 0
		Alert("Produto n�o encontrado")
		Return(lRet)
	Endif
	
	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Produto" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Descri��o","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2]}}

	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
	DEFINE SBUTTON FROM 88,105 TYPE 4 ENABLE OF oDlgSu9 ACTION (AxInclui("SB1"))
	DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (u_cadsB1("V",aItems[oLbx1:nAt][2]))
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SB1")
		DbSetorder(1)
		DbSeek(xFilial("SB1")+aItems[nPos][2])
	Endif

Return(lRet)


user function buscasa1()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	Pergunte("DESCSA1",.T.)

	_qry := "Select A1_NOME, A1_NREDUZ, A1_COD, A1_CGC from "+RetSqlName("SA1")+" SA1 (nolock) "
	_qry += "where SA1.D_E_L_E_T_ = '' and (A1_NOME like '%"+upper(alltrim(MV_PAR01))+"%' or A1_COD like '%"+upper(alltrim(MV_PAR01))+"%' or "
	_qry += "A1_NREDUZ like '%"+upper(alltrim(MV_PAR01))+"%' or A1_CGC like '%"+upper(alltrim(MV_PAR01))+"%' ) "
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->A1_NOME,XXX->A1_NREDUZ,XXX->A1_CGC,XXX->A1_COD})
		DbSkip()
	End
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()
	     
	If Len(aItems) <= 0
		Alert("Cliente n�o encontrado")
		Return(lRet)
	Endif
	
	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Cliente" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Raz�o","Fantasia","CNPJ","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2],;
		aItems[oLbx1:nAt,3],;
		aItems[oLbx1:nAt,4]}}
					 


	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
	DEFINE SBUTTON FROM 88,105 TYPE 4 ENABLE OF oDlgSu9 ACTION (AxInclui("SA1"))
	DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (u_cadsA1("V",aItems[oLbx1:nAt][4]))
//    DEFINE SBUTTON FROM 88,140 TYPE 4 ENABLE OF oDlgSu9 ACTION (u_cadsz1("I","")) //4
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SA1")
		DbSetorder(1)
		DbSeek(xFilial("SA1")+aItems[nPos][4])
	Endif

Return(lRet)

User Function CadSA1(_tp,_cod)

	_rarea := Getarea()
	DbSelectArea("SA1")
	cCadastro := "Cadastro de Cliente"
	DbSetOrder(1)
	DbSeek(xfilial("SA1")+_cod)
	nReg := Recno()
	AxVisual("SA1",nReg,4)
	RestArea(_rarea)

Return

User Function CadSA2(_tp,_cod)

	_rarea := Getarea()
	DbSelectArea("SA2")
	cCadastro := "Cadastro de Fornecedor"
	DbSetOrder(1)
	DbSeek(xfilial("SA2")+_cod)
	nReg := Recno()
	AxVisual("SA2",nReg,4)
	RestArea(_rarea)

Return

User Function CadSB1(_tp,_cod)

	_rarea := Getarea()
	DbSelectArea("SB1")
	cCadastro := "Cadastro de Produto"
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+_cod)
	nReg := Recno()
	AxVisual("SB1",nReg,4)
	RestArea(_rarea)

Return


user function buscasa2()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	Pergunte("DESCSA2",.T.)

	_qry := "Select A2_NOME, A2_NREDUZ, A2_COD, A2_CGC from "+RetSqlName("SA2")+" SA2 (nolock) "
	_qry += "where SA2.D_E_L_E_T_ = '' and (A2_NOME like '%"+upper(alltrim(MV_PAR01))+"%' or A2_COD like '%"+upper(alltrim(MV_PAR01))+"%' or "
	_qry += "A2_NREDUZ like '%"+upper(alltrim(MV_PAR01))+"%' or A2_CGC like '%"+upper(alltrim(MV_PAR01))+"%' ) "
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->A2_NOME,XXX->A2_NREDUZ,XXX->A2_CGC,XXX->A2_COD})
		DbSkip()
	End
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()
	     
	If Len(aItems) <= 0
		Alert("Fornecedor n�o encontrado")
		Return(lRet)
	Endif
	
	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Fornecedor" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Raz�o","Fantasia","CNPJ","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2],;
		aItems[oLbx1:nAt,3],;
		aItems[oLbx1:nAt,4]}}
					 


	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
	DEFINE SBUTTON FROM 88,105 TYPE 4 ENABLE OF oDlgSu9 ACTION (AxInclui("SA2"))
	DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (u_cadsA2("V",aItems[oLbx1:nAt][4]))
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SA2")
		DbSetorder(1)
		DbSeek(xFilial("SA2")+aItems[nPos][4])
	Endif

Return(lRet)



user function buscased()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	Pergunte("DESCSED",.T.)

	_qry := "Select ED_DESCRIC, ED_CODIGO from "+RetSqlName("SED")+" SED (nolock) "
	_qry += "where SED.D_E_L_E_T_ = '' and (ED_DESCRIC like '%"+upper(alltrim(MV_PAR01))+"%' or ED_CODIGO like '%"+upper(alltrim(MV_PAR01))+"%')  "
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->ED_DESCRIC,XXX->ED_CODIGO})
		DbSkip()
	End
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()
	     
	If Len(aItems) <= 0
		Alert("Natureza n�o encontrada")
		Return(lRet)
	Endif
	
	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Natureza" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Descri��o","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2]}}

	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
//    DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (u_cadsB1("V",aItems[oLbx1:nAt][2]))
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SED")
		DbSetorder(1)
		DbSeek(xFilial("SED")+aItems[nPos][2])
	Endif

Return(lRet)


user function buscacor()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	_qry := "Select SubString(B1_COD,9,3) as COD, BV_DESCRI from "+RetSqlName("SB1")+" SB1 (nolock) "
	_qry += "inner join "+RetSqlName("SBV")+" SBV (nolock) on SBV.D_E_L_E_T_ = '' and BV_TABELA = 'COR' and BV_CHAVE = SubString(B1_COD,9,3) "
	_qry += "where SB1.D_E_L_E_T_ = '' and B1_COD like '"+alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+"%' "
	_qry += "group by SubString(B1_COD,9,3), BV_DESCRI "
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->BV_DESCRI,XXX->COD})
		DbSkip()
	End
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()
	     
	If Len(aItems) <= 0
		Alert("Cor n�o encontrada")
		Return(lRet)
	Endif
	
	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Cores" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Descri��o","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2]}}

	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
//    DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (u_cadsB1("V",aItems[oLbx1:nAt][2]))
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SBV")
		DbSetorder(1)
		DbSeek(xFilial("SBV")+"COR"+aItems[nPos][2])
	Endif

Return(lRet)

user function buscatam()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	DbSelectArea("SB4")
	DbSetOrder(1)
	DbSeek(xfilial("SB4")+alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]))
	
	_qry := "Select SubString(B1_COD,12,4) as COD, BV_DESCRI from "+RetSqlName("SB1")+" SB1 (nolock) "
	_qry += "inner join "+RetSqlName("SBV")+" SBV (nolock) on SBV.D_E_L_E_T_ = '' and BV_TABELA = '"+SB4->B4_COLUNA+"' and BV_CHAVE = SubString(B1_COD,12,4) "
	_qry += "where SB1.D_E_L_E_T_ = '' and B1_COD like '"+alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+"%' "
	_qry += "group by SubString(B1_COD,12,4), BV_DESCRI "
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->BV_DESCRI,XXX->COD})
		DbSkip()
	End
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()
	     
	If Len(aItems) <= 0
		Alert("Tamanho n�o encontrada")
		Return(lRet)
	Endif
	
	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Tamanhos" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Descri��o","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2]}}

	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
//    DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (u_cadsB1("V",aItems[oLbx1:nAt][2]))
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SBV")
		DbSetorder(1)
		DbSeek(xFilial("SBV")+SB4->B4_COLUNA+aItems[nPos][2])
	Endif

Return(lRet)

User Function AjuCor

	IF Pergunte("AJUCOR",.t.)
		DbSelectArea("SBV")
		DbSetOrder(1)
		DbSeek(xfilial("SBV")+"COR"+MV_PAR01)
		
		If Found()
				
			If Select("TMP") > 0
				TMP->(dbclosearea())
			EndIf	
				
			_qry := "SELECT TOP 1 ZD_DESCRIC FROM "+RETSQLNAME("SZD")+ " WHERE ZD_COR = '"+MV_PAR01+"' AND D_E_L_E_T_ = '' "	
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMP",.F.,.T.)
			
			
			_QRY1 := "UPDATE "+RETSQLNAME("SB1")+" SET B1_DESC = REPLACE(B1_DESC,'"+ALLTRIM(TMP->ZD_DESCRIC)+"','"+ALLTRIM(SBV->BV_DESCRI)+"') WHERE D_E_L_E_T_ = '' AND B1_DESC LIKE '%"+ALLTRIM(TMP->ZD_DESCRIC)+"%' AND SUBSTRING(B1_COD,9,3) = '"+MV_PAR01+"' 
			nSta := TCSQLEXEC(_QRY1)
			
			
			_QRY2 := "UPDATE "+RETSQLNAME("SZD")+" SET ZD_DESCRIC = '"+ALLTRIM(SBV->BV_DESCRI)+"' WHERE D_E_L_E_T_ = '' AND ZD_COR = '"+MV_PAR01+"'"
			nStatus := TCSQLEXEC(_QRY2)
			
			if (nStatus < 0) .OR. (nSta < 0)
  				MsgBox(TCSQLError(), "Erro no comando", "Stop")
	  		endif
		Endif
			
		Alert("As cores foram ajustadas para todos os Produtos!")
	
	Endif

Return


User Function Exclui()

	cCodProd	:= AllTrim(SB4->B4_COD)
	cExistSB1	:= .F.
	cExistSG1	:= .F.

	cTexto1 := "Este processo ir� limpar todos os registros nas "+CRLF
	cTexto1 += " tabelas customizadas!"+CRLF

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+AllTrim(cCodProd))
	If Found()
		cExistSB1 := .T.
	End
	
	DbSelectArea("SG1")
	DbSetOrder(1)
	DbSeek(xfilial("SG1")+AllTrim(cCodProd))
	If Found()
		cExistSG1 := .T.
	End
	
	If MsgYesNo(cTexto1)
		
		If cExistSB1 == .F. .AND. cExistSG1 == .F.
	
			DbSelectArea("SZD")
			DbSetOrder(1)
			DbSeek(xfilial("SZD")+cCodProd)
			While !EOF() .and. SubString(SZD->ZD_PRODUTO,1,8) = cCodProd
				RecLock("SZD",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			End
	
			DbSelectArea("SZE")
			DbSetOrder(1)
			DbSeek(xfilial("SZE")+cCodProd)
			While !EOF() .and. SubString(SZE->ZE_PRODUTO,1,8) = cCodProd
				RecLock("SZE",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			End
	
			DbSelectArea("SZF")
			DbSetOrder(1)
			DbSeek(xfilial("SZF")+cCodProd)
			While !EOF() .and. SubString(SZF->ZF_PRODUTO,1,8) = cCodProd
				RecLock("SZF",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			End
			
			DbSelectArea("SZG")
			DbSetOrder(1)
			DbSeek(xfilial("SZG")+cCodProd)
			While !EOF() .and. SubString(SZG->ZG_PRODUTO,1,8) = cCodProd
				RecLock("SZG",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			End
	
			DbSelectArea("SZH")
			DbSetOrder(1)
			DbSeek(xfilial("SZH")+cCodProd)
			While !EOF() .and. SubString(SZH->ZH_PRODUTO,1,8) = cCodProd
				RecLock("SZH",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			End
	
			DbSelectArea("SZI")
			DbSetOrder(1)
			DbSeek(xfilial("SZI")+cCodProd)
			While !EOF() .and. SubString(SZI->ZI_PRODUTO,1,8) = cCodProd
				RecLock("SZI",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			End
	
			DbSelectArea("SZK")
			DbSetOrder(1)
			DbSeek(xfilial("SZK")+cCodProd)
			While !EOF() .and. SubString(SZK->ZK_PRODUTO,1,8) = cCodProd
				RecLock("SZK",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			End
	
			DbSelectArea("SZL")
			DbSetOrder(1)
			DbSeek(xfilial("SZL")+cCodProd)
			While !EOF() .and. SubString(SZL->ZL_PRODUTO,1,8) = cCodProd
				RecLock("SZL",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			End
	
			DbSelectArea("SZN")
			DbSetOrder(1)
			DbSeek(xfilial("SZN")+cCodProd)
			While !EOF() .and. SubString(SZN->ZN_PRODUTO,1,8) = cCodProd
				RecLock("SZN",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			End
	
			DbSelectArea("SZO")
			DbSetOrder(1)
			DbSeek(xfilial("SZO")+cCodProd)
			While !EOF() .and. SubString(SZO->ZO_PRODUTO,1,8) = cCodProd
				RecLock("SZO",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			End
			
			MsgInfo("Limpeza efetuada com sucesso!","Limpeza Ok")
	
		Else
			If 	cExistSB1 == .T. .AND. cExistSG1 == .T.
				Alert("Favor efetuar a limpeza do Cadastro de Produto (padr�o) e Cadastro de Grades (padr�o), antes de efetuar o processo de Limpeza")
			ElseIf cExistSB1 == .T.
				Alert("Favor efetuar a limpeza do Cadastro de Produto (padr�o), antes de efetuar o processo de Limpeza")
			ElseIf cExistSG1 == .T.
				Alert("Favor efetuar a limpeza do Cadastro de Grades (padr�o), antes de efetuar o processo de Limpeza")
			End
		End
	End
	
Return


Static Function EMail(_ref)

	Local cUser := "", cPass := "", cSendSrv := ""
	Local cMsg := ""
	Local nSendPort := 0, nSendSec := 0, nTimeout := 0
	Local xRet
	Local oServer, oMessage
   
	cUser		:= Trim(GetMV("MV_EMCONTA"))		//Trim(GetMV("MV_RELACNT"))		//define the e-mail account username
	cPass		:= Trim(GetMV("MV_EMSENHA"))		//Trim(GetMV("MV_RELPSW"))		//define the e-mail account password
	cSendSrv	:= Trim(GetMV("MV_RELSERV"))		//Trim(GetMV("MV_RELSERV"))		// define the send server
	cFromMv	:= Trim(GetMV("MV_EMCONTA"))		//Trim(GetMV("MV_RELFROM"))
	cToEnv		:= Trim(GetMV("MV_WCADPRD"))
	nTimeout	:= 60 									// define the timout to 60 seconds
   
//	Cria a conex�o com o server STMP ( Envio de e-mail )
	oServer := TMailManager():New()
   
	oServer:SetUseSSL( .F. )
	oServer:SetUseTLS( .F. )
   
	if nSendSec == 0
		nSendPort := 25 //default port for SMTP protocol
	elseif nSendSec == 1
		nSendPort := 465 //default port for SMTP protocol with SSL
		oServer:SetUseSSL( .T. )
	else
		nSendPort := 587 //default port for SMTPS protocol with TLS
		oServer:SetUseTLS( .T. )
	endif
   
	xRet := oServer:Init( "", cSendSrv, cUser, cPass, , nSendPort )
	if xRet != 0
		cMsg := "Could not initialize SMTP server: " + oServer:GetErrorString( xRet )
		conout( cMsg )
		Return .F.
	endif
   
//	seta um tempo de time out com servidor
	xRet := oServer:SetSMTPTimeout( nTimeout )
	if xRet != 0
		cMsg := "Falha ao setar " + cProtocol + " timeout to " + cValToChar( nTimeout )
		conout( cMsg )
		Return .F.
	endif
   
//	realiza a conex�o SMTP
	xRet := oServer:SMTPConnect()
	if xRet <> 0
		cMsg := "Falha ao conectar: " + oServer:GetErrorString( xRet )
		conout( cMsg )
		Return .F.
	endif
   
//	authenticate on the SMTP server (if needed)
	xRet := oServer:SmtpAuth( cUser, cPass )
	if xRet <> 0
		cMsg := "Could not authenticate on SMTP server: " + oServer:GetErrorString( xRet )
		alert( cMsg )			//conout( cMsg )
		oServer:SMTPDisconnect()
		Return .F.
	endif
   
//	Apos a conex�o, cria o objeto da mensagem
	oMessage := TMailMessage():New()
   
//	Limpa o objeto
	oMessage:Clear()
   
//	Popula com os dados de envio
	oMessage:cFrom		:= cFromMv
	oMessage:cTo			:= cToEnv
//	OMessage:cCc			:= "daniel.melo@actualtrend.com.br; contato@marcoscaio.com"
//	oMessage:cBcc			:= "microsiga@microsiga.com.br"
	oMessage:cSubject		:= "Novo Cadastro de Produto"
	oMessage:cBody		:= " <p><b><font face='ARIAL' size='5'>Produto "+_ref+" cadastrado</font></b></p>"

//	Envia o e-mail
	xRet := oMessage:Send( oServer )
	if xRet <> 0
		cMsg := "Erro ao enviar o e-mail: " + oServer:GetErrorString( xRet )
		conout( cMsg )
		Return .F.
	endif
  
//	Desconecta do servidor
	xRet := oServer:SMTPDisconnect()
	if xRet <> 0
		cMsg := "Erro ao disconectar do servidor SMTP: " + oServer:GetErrorString( xRet )
		conout( cMsg )
		Return .F.
	endif

Return .T.

User Function AtuSG5(_linha,_status)
	For _i := 1 to Len(OMSRevisao:ACOLS)
		If OMSRevisao:ACOLS[_i,len(OMSRevisao:AHEADER)+1] == .F.
			If _i <> _linha
				OMSRevisao:ACOLS[_i,3] := "2"
			Endif
		Endif
	Next
	oMSRevisao:refresh()
	
Return(_status)

User Function HVLDOPC(_grupo,_linha,_tp)
lret := .t.

	DbSelectArea("SGA")
	DbSetOrder(1)
	DbSeek(xfilial("SGA")+_grupo)
	
	If Found()
		If _tp = 15
			lret := .f.
		Else
			lret := .f.

			For i := 1 to Len(oMSOPC:ACOLS)
				If oMSOPC:ACOLS[i,3] = _grupo .and. i <> _linha
					lret := .t.
				endif
			Next 
		Endif
	Endif

Return(lret)

User Function HEXISTP(_prod)
lret := .t.

DbSelectArea("SB1")
DbSetOrder(1)
DbSeek(xfilial("SB1")+alltrim(_prod))

If !Found()
	lret := .F.
	Alert("Produto inexistente!")
Endif

Return(lret)

User Function ajustac(_Produto,_Revisao,_indice,_compon,_corte)
	
	Local cEOL  := CHR(13)+CHR(10)
	
	IF _corte = 'S'
		_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ "+cEOL
		_qry += "where D_E_L_E_T_ = '' and G1_REVINI = '"+_Revisao+"' and SubString(G1_COD,1,8) = 'B"+SubStr(_Produto,2,7)+"' "+cEOL 
		_qry += "		and SubString(G1_COMP,1,8) = '"+alltrim(SubStr(_compon,1,8))+"' "+cEOL
	Else		
		_qry := "update "+RetSqlName("SG1")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ "+cEOL
		_qry += "where D_E_L_E_T_ = '' and G1_REVINI = '"+_revisao+"' and Left(G1_COD,8) = '"+SubStr(_Produto,1,8)+"' "+cEOL
		_qry += "		and left(G1_COMP,8) = '"+SubStr(_compon,1,8)+"' and G1_TRT = '"+_indice+"' "+cEOL
	Endif
	
	nStatus := TcSqlExec(_qry)
	
	if (nStatus < 0)
 		MsgBox(TCSQLError(), "Erro no comando", "Stop")
  	endif
Return

Static Function CriacBar(_tp)
// Rotina para gera��o de codigo de barras
	
	Local nx
	Local _campw := GetMV("MV_HCODBAR")

	IF _tp = 7
		for nx := 3 to len(oMSGrade:aCols[oMSGrade:nat])-1
			If AllTrim(oMSGrade:aCols[oMSGrade:nat,nx])=""
			
				_tam := rettam2(AllTrim(OMSGrade:AHEADER[nx,1]),cColuna,"C")
				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+AllTrim(cCodigo)+OMSGrade:ACOLS[oMSGrade:nat,1]+_tam)
				
				If Found()
					If cCodBarras = 0
						_campwx 	:= VerCodBar(_campw)
						cCodBarras	:= _campwx
					Else
						cCodBarras	:= cCodBarras+1
						_campwx		:= cCodBarras
					End
					oMSGrade:aCols[oMSGrade:nat,nx] := cValToChar(ROUND(_campwx, 0))
				Endif
			End
		Next nx
	End

	oMsGrade:refresh()

Return


Static Function MR225ExplG(cProduto,nQuantPai,xrev)
Local nReg 		  := 0

dbSelectArea('SG1')
DbSetOrder(1)
MsSeek(xFilial('SG1')+cProduto)

While !Eof() .And. G1_FILIAL+G1_COD == xFilial('SG1')+cProduto
	If SG1->G1_REVINI <> xrev
		DbSkip()
		Loop
	Endif
	
	nReg       := Recno()
	nQuantItem := ExplEstr(nQuantPai,,,SG1->G1_REVINI)

	//�������������������������������������������������Ŀ
	//� Verifica se existe sub-estrutura                �
	//���������������������������������������������������
	dbSelectArea('SG1')
	MsSeek(xFilial('SG1')+G1_COMP)

	If Found()
		MR225ExplG(G1_COD,nQuantItem,G1_REVINI)
	Else
		DbSelectArea("SG1")
		dbGoto(nReg)

		DbSelectArea("DA1")
		DbSetOrder(1)
		DbSeek(xfilial("DA1")+cTabela+SG1->G1_COMP)
		
		If !Found() .or. DA1->DA1_PRCVEN = 0
			MsgInfo("Produto "+SG1->G1_COMP+" n�o possui pre�o cadastrado na tabela informada!","Diverg�ncia de Pre�o")
		Endif

		_tem := ASCAN( aEST, {|x| ALLTRIM(x[1]) == SubStr(ALLTRIM(SG1->G1_COMP),1,8)})
		If _tem = 0
			Aadd(aEST, {substr(ALLTRIM(SG1->G1_COMP),1,8),nQuantItem}) //nQuantPai*SG1->G1_QUANT})
		Else
			aEST[_tem,2] += nQuantItem //nQuantPai*SG1->G1_QUANT
		Endif
		Aadd(aEST2, {SG1->G1_COMP,SG1->G1_COD,nQuantItem}) //nQuantPai*SG1->G1_QUANT})
	EndIf

	DbSelectArea("SG1")
	dbGoto(nReg)

	dbSkip()
EndDo
Return

Static Function MP225ExplG(cProduto,nQuantPai,xrev)
Local nReg 		  := 0

dbSelectArea('SG1')
DbSetOrder(1)
MsSeek(xFilial('SG1')+cProduto)

While !Eof() .And. G1_FILIAL+G1_COD == xFilial('SG1')+cProduto
	If SG1->G1_REVINI <> xrev
		DbSkip()
		Loop
	Endif

	nReg       := Recno()
	//nQuantItem := ExplEstr(nQuantPai,,,"001")

	//�������������������������������������������������Ŀ
	//� Verifica se existe sub-estrutura                �
	//���������������������������������������������������
	dbSelectArea('SG1')
	MsSeek(xFilial('SG1')+G1_COMP)

	If Found()
		MP225ExplG(G1_COD,G1_QUANT,G1_REVINI)
	Else
		DbSelectArea("SG1")
		dbGoto(nReg)
		DbSelectArea("DA1")
		DbSetOrder(1)
		DbSeek(xfilial("DA1")+cTabela+SG1->G1_COMP)
		
		If !Found() .or. DA1->DA1_PRCVEN = 0
			MsgInfo("Produto "+SG1->G1_COMP+" n�o possui pre�o cadastrado na tabela informada!","Diverg�ncia de Pre�o")
		Endif
		_tem := ASCAN( aEST3, {|x| x[1] == SubStr(SG1->G1_COMP,1,8)})
		If _tem = 0
			Aadd(aEST3, {substr(SG1->G1_COMP,1,8),POSICIONE("DA1",1,xFilial("DA1")+cTabela+SG1->G1_COMP,"DA1_PRCVEN")}) //nQuantPai*SG1->G1_QUANT})
		Else
			aEST3[_tem,2] += POSICIONE("DA1",1,xFilial("DA1")+cTabela+SG1->G1_COMP,"DA1_PRCVEN")
		Endif
		Aadd(aEST4, {SG1->G1_COMP,SG1->G1_COD,POSICIONE("DA1",1,xFilial("DA1")+cTabela+SG1->G1_COMP,"DA1_PRCVEN")}) //nQuantPai*SG1->G1_QUANT})
	EndIf

	DbSelectArea("SG1")
	dbGoto(nReg)

	dbSkip()
EndDo
Return

Static Function rettam3(xcoluna,_tam)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
_BVTAM := ""

cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " WHERE "
cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' " 
cQuery += "ORDER BY R_E_C_N_O_"

cQuery := ChangeQuery(cQuery)
If Select("TMPSBV") > 0 
   	 TMPSBV->(DbCloseArea()) 
EndIf 
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

DbSelectArea("TMPSBV")
DbGoTop()
_xtam := 0
_achou := .F.
While !EOF() .and. !_achou
	_xtam++
	If alltrim(TMPSBV->BV_CHAVE) == alltrim(_tam)
		_achou := .T.
	Endif
	DbSkip()
End

DbCloseArea()
_BVTAM := StrZero(_xtam,3)
Return(_BVTAM)

user function vldfecha(_linha)

	_fecha := .T.
	_i := _linha 
	If oMSNewGe1:aCols[_i,len(oMSNewGe1:aHeader)+1] = .F.
		DbSelectArea("SZF")
		DbSetOrder(2)
		DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5])

		IF found()
			While !EOF() .and. SubStr(cProduto,1,8) == SubStr(SZF->ZF_PRODUTO,1,8) .and. alltrim(cRevisao) == alltrim(SZF->ZF_REVISAO) .and. SubStr(oMSNewGe1:aCols[_i,1],1,8) == SubStr(SZF->ZF_COMP,1,8) .and. _fecha .and. alltrim(SZF->ZF_INDICE) == alltrim(oMSNewGe1:aCols[_i,5])
				_temqtd := 0
				For _w := 3 to len(oMsQtd:aheader)-2
					_campo := "SZF->"+oMsQtd:aheader[_w][2] //ZF_TAM"+StrZero(_w,3)

					DbSelectArea("SZG")
					DbSetOrder(2)
					DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)
			
					If &_campo > 0
						_temqtd++
						DbSelectArea("SZG")
						DbSetOrder(2)
						DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)
			
						If !Found() .or. alltrim(SZG->ZG_CORNV) = ""
							alert(SubStr(oMSNewGe1:aCols[_i,1],1,15)+"-"+oMSNewGe1:aCols[_i,5]+"-"+SZF->ZF_COR)
							_fecha := .F.
						Endif

						DbSelectArea("SZK")
						DbSetOrder(2)
						DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMsNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5])

						If Found()
							While !EOF() .and. SubStr(cProduto,1,8) == SubStr(SZK->ZK_PRODUTO,1,8) .and. alltrim(cRevisao) == alltrim(SZK->ZK_REVISAO) .and. SubStr(oMSNewGe1:aCols[_i,1],1,8) == SubStr(SZK->ZK_COMP,1,8) .and. _fecha .and. alltrim(SZK->ZK_INDICE) == alltrim(oMSNewGe1:aCols[_i,5])
								DbSelectArea("SB4")
								DbSetOrder(1)
								DbSeek(xfilial("SB4")+SubStr(cProduto,1,8))

								cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
								cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +SB4->B4_COLUNA +"' AND D_E_L_E_T_ = '' "
								cQuery += "ORDER BY R_E_C_N_O_"
						
								cQuery := ChangeQuery(cQuery)
							  
								If Select("TMPSBV") > 0
									TMPSBV->(DbCloseArea())
								EndIf
								dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
								
								DbSelectArea("TMPSBV")
								DbGoTop()
								_xlin := 0
								While !EOF()
									_xlin++
									If alltrim(SZK->ZK_TAM) = alltrim(TMPSBV->BV_CHAVE)
										Exit
									Endif
									DbSkip()
								End
								DbCloseArea()
								
								If _xlin > 0
									_qry := "Select top 1 * from "+RetSqlName("SZF")+" where D_E_L_E_T_ = '' and ZF_PRODUTO = '"+SubStr(cProduto,1,15)+"' and ZF_COMP = '"+SubStr(oMsNewGe1:aCols[_i,1],1,15)+"' and ZF_TAM"+StrZero(_xlin,3)+" > 0"
									dbUseArea( .T., "TOPCONN", TcGenQry(,,_Qry), "XQRY", .T., .F. )
									dbSelectArea("XQRY")
									dbGoTop()
									If !EOF() 
										If alltrim(SZK->ZK_TAMNV) == ""
										
											Alert(SubStr(oMSNewGe1:aCols[_i,1],1,8)+"-"+SZK->ZK_TAM)
											_fecha := .F.
										Endif
									Endif
									DbCloseArea()
								Else
									_fecha := .F.
								Endif
								DbSelectArea("SZK")
								DbSkip()
							End
						Else
							_fecha := .F.
						Endif
					Endif
				Next
				// Obrigat�rio a ter pelo menos uma quantidade preenchida caso tenha a cor preenchida!
				If _temqtd = 0
					DbSelectArea("SZG")
					DbSetOrder(2)
					DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)
			
					If Found() .and. alltrim(SZG->ZG_CORNV) <> ""
						alert(SubStr(oMSNewGe1:aCols[_i,1],1,15)+"-"+oMSNewGe1:aCols[_i,5]+"-"+SZF->ZF_COR)
						_fecha := .F.
					Endif
				Endif
				
				DbSelectArea("SZF")
				DbSkip()
			End
		Else
			_fecha := .F.
		Endif
	Endif

	_qry := "Select rtrim(ltrim(ZG_COMP))+ZG_CORNV+ZK_TAMNV as SKU from "+RetSqlName("SZE")+" SZE WITH(NOLOCK) "
	_qry += "left join "+RetSqlName("SZG")+" SZG WITH(NOLOCK) on SZG.D_E_L_E_T_ = '' and ZG_PRODUTO = ZE_PRODUTO and ZG_COMP = ZE_COMP and ZG_REVISAO = ZE_REVISAO and ZG_INDICE = ZE_INDICE "
	_qry += "left join "+RetSqlName("SZK")+" SZK WITH(NOLOCK) on SZK.D_E_L_E_T_ = '' and ZK_PRODUTO = ZE_PRODUTO and ZK_COMP = ZE_COMP and ZK_REVISAO = ZE_REVISAO and ZK_INDICE = ZE_INDICE "
	_qry += "left join "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) on SB1.D_E_L_E_T_ = '' and B1_COD =  rtrim(ltrim(ZG_COMP))+ZG_CORNV+ZK_TAMNV "
	_qry += "where SZE.D_E_L_E_T_ = '' and ZE_PRODUTO = '"+SubStr(cProduto,1,15)+"' and ZE_REVISAO = '"+cRevisao+"' and ZG_CORNV <> '' and ZK_TAMNV <> '' and ZE_COMP = '"+SubStr(oMsNewGe1:aCols[_linha,1],1,8)+"' "
	_qry += "and ZE_INDICE = '"+oMsNewGe1:aCols[_linha,5]+"' "
	_qry += "and B1_COD is null "
	_qry += "group by rtrim(ltrim(ZG_COMP))+ZG_CORNV+ZK_TAMNV "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSZ",.T.,.T.)
	
	DbSelectArea("TMPSZ")
	DbGoTop()
	_msg := ""
	
	While !EOF()
		_fecha := .F.
		_msg += TMPSZ->SKU + " - "
		DbSelectArea("TMPSZ")
		DbSkip()
	End
	
	If alltrim(_msg) <> ""
		MsgBox(_msg, "SKUs n�o cadastrados", "Stop")
	Endif
	
	DbCloseArea()

Return

