#include 'totvs.ch'

USER FUNCTION HESTP010

Private cStatus  := "F"
Private cCor	 := ""
Private aColsEx  := {}
Private aHeaderEx := {}
Private oMSCOR

IF PERGUNTE("HESTP010",.T.)
	DbSelectArea("SB4")
	DbSetOrder(1)
	DbSeek(xfilial("SB4")+MV_PAR01)

	u_SelCor()
	If cStatus == "T"
	
		If MV_PAR03 = 1
			DbSelectArea("SB4")
			DbSetOrder(1)
			DbSeek(xfilial("SB4")+MV_PAR02)
			
			If !Found()
				_qry := "exec dbo.CRIA_SB4 '"+MV_PAR01+"','"+MV_PAR02+"'"
				nstatus := TcSqlExec(_qry)
				
				if (nStatus < 0)
		    			conout("TCSQLError() " + TCSQLError())
		  		endif

				_qry := "Update "+RetSqlName("SB4")+" set B4_YDATCAD = '"+DtoS(DDATABASE)+"', B4_YSITUAC = 'PNV', B4_YDATLIB = '', B4_YDESCPT = '', B4_YDESCES = '', B4_YDESCIE = '' "
				_qry += CRLF + "where D_E_L_E_T_ = '' and left(B4_COD,8) = '"+MV_PAR02+"' "
				
				MemoWrite("HESTP010_SB4.txt",_qry)
				
				nStatus := TcSqlExec(_qry)
	
				if (nStatus < 0)
					conout("TCSQLError() " + TCSQLError())
				endif	  				
		  	Endif

	  		_dupl := .F.
	  		For _i := 1 to len(oMSCOR:aCols)
	  			If oMSCOR:aCols[_i,len(aheaderEX)+1] = .F.
  					If alltrim(oMSCOR:aCols[_i,3]) = ""
  						_xcor := oMSCOR:aCols[_i,1]
  					Else
  						_xcor := oMSCOR:aCols[_i,3]
  					Endif
	  	
					DbSelectArea("SB1")
					DbSetOrder(1)
					DbSeek(xfilial("SB1")+SubStr(alltrim(MV_PAR02),1,8)+_xcor)
					
					//Alert(alltrim(oMSCOR:aCols[_i,1]))
					If !Found()
						_qry := "exec dbo.CRIA_SB1 '"+MV_PAR01+"','"+MV_PAR02+"','"+alltrim(oMSCOR:aCols[_i,1])+"','"+_xcor+"'"
						MemoWrite("HESTP010_SB1A.txt",_qry)
						nstatus := TcSqlExec(_qry)
						
						if (nStatus < 0)
				    		conout("TCSQLError() " + TCSQLError())
				  		endif
				  	Else
				  		_dupl := .T.
				  	Endif
				Endif
			Next
	
		  	For _i := 1 to len(oMSCOR:aCols)
		  		If oMSCOR:aCols[_i,len(aheaderEX)+1] = .F.
		  			If oMSCOR:aCols[_i,1] <> oMSCOR:aCols[_i,3]
		  				_qry := "Update "+RetSqlName("SB1")+" set B1_COD = left(B1_COD,8)+'"+oMSCOR:aCols[_i,3]+"'+SubString(B1_COD,12,4), "
		  				_qry += CRLF + "B1_DESC = ltrim(rtrim(B4_DESC))+' - '+ltrim(rtrim(SBV.BV_DESCRI))+' - '+ltrim(rtrim(SBVA.BV_DESCRI)) "
		  				_qry += CRLF + "from "+RetSqlName("SB1")+" SB1 " 
		  				_qry += CRLF + "inner join "+RetSqlName("SB4")+" SB4 on SB4.D_E_L_E_T_ = '' and B4_COD = left(B1_COD,8) "
		  				_qry += CRLF + "inner join "+RetSqlName("SBV")+" SBV on SBV.D_E_L_E_T_ = '' and SBV.BV_TABELA = 'COR' and SBV.BV_CHAVE = '"+oMSCOR:aCols[_i,3]+"' "
		  				_qry += CRLF + "inner join "+RetSqlName("SBV")+" SBVA on SBVA.D_E_L_E_T_ = '' and SBVA.BV_TABELA = B4_COLUNA and SBVA.BV_CHAVE = Substring(B1_COD,12,4) "
		  				_qry += CRLF + "where SB1.D_E_L_E_T_ = '' and left(B1_COD,11) = '"+MV_PAR02+oMSCOR:aCols[_i,1]+"' "
		  				
		  				MemoWrite("HESTP010_SB1.txt",_qry)
		  					
		  				nstatus := TcSqlExec(_qry)
				
		  				if (nStatus < 0)
		  					conout("TCSQLError() " + TCSQLError())
		  				endif
	
		  			Endif 
		  		Endif
		  	Next
		
	  		For _i := 1 to len(oMSCOR:aCols)
	  			If oMSCOR:aCols[_i,len(aheaderEX)+1] = .F.
	  					If alltrim(oMSCOR:aCols[_i,3]) = ""
	  						_xcor := oMSCOR:aCols[_i,1]
	  					Else
	  						_xcor := oMSCOR:aCols[_i,3]
	  					Endif

					  	DbSelectArea("SZD")
					  	DbSetOrder(1)
					  	DbSeek(xfilial("SZD")+alltrim(MV_PAR02)+Space(7)+_xCor)
		  	
					  	
					  	IF !Found()
					  		_qry := "exec dbo.CRIA_SZD '"+MV_PAR01+"','"+MV_PAR02+"','"+alltrim(oMSCOR:aCols[_i,1])+"','"+_xcor+"'"
					  		MemoWrite("HESTP010_SZDA.txt",_qry)
							nstatus := TcSqlExec(_qry)
					
							if (nStatus < 0)
					    		conout("TCSQLError() " + TCSQLError())
					  		endif

							If Select("TMPSZD") > 0
								TMPSZD->(DbCloseArea())
							EndIf

							_qry1 := " SELECT COUNT(ZD_COR),ZD_DESCRIC,ZD_COR FROM "+RetSqlName("SZD")+" WHERE ZD_PRODUTO = '"+MV_PAR02+"' AND D_E_L_E_T_ = ''  "
  							_qry1 += " GROUP BY ZD_DESCRIC,ZD_COR HAVING COUNT(ZD_COR) > 1 "	 
							
							dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry1),"TMPSZD",.T.,.T.)
 					
 							_quy2 := " UPDATE "+RetSqlName("SZD")+" SET D_E_L_E_T_ = '*' , R_E_C_D_E_L_ = R_E_C_N_O_  WHERE ZD_PRODUTO = '"+MV_PAR02+"' AND ZD_COR = '"+ALLTRIM(TMPSZD->ZD_COR)+"' AND D_E_L_E_T_ = '' " 
							nstatus := TcSqlExec(_quy2)

							if (nStatus < 0)
		  						conout("TCSQLError() " + TCSQLError())
		  					endif
	
	//	  				If oMSCOR:aCols[_i,1] <> oMSCOR:aCols[_i,3]
		  					_qry := "Update "+RetSqlName("SZD")+" set ZD_DESCCOM = '', ZD_LAVAGE = '', ZD_DESCCP2 = '',ZD_COMPOS = '' , ZD_DESCLAV = '', ZD_COR = '"+_xcor+"', "
		  					_qry += CRLF + "ZD_DESCRIC = ltrim(rtrim(SBV.BV_DESCRI)) "
		  					_qry += CRLF + "from "+RetSqlName("SZD")+" SZD " 
		  					_qry += CRLF + "inner join "+RetSqlName("SB4")+" SB4 on SB4.D_E_L_E_T_ = '' and B4_COD = left(ZD_PRODUTO,8) "
		  					_qry += CRLF + "inner join "+RetSqlName("SBV")+" SBV on SBV.D_E_L_E_T_ = '' and SBV.BV_TABELA = 'COR' and SBV.BV_CHAVE = '"+_xcor+"' "
		  					_qry += CRLF + "where SZD.D_E_L_E_T_ = '' and left(ZD_PRODUTO,8) = '"+MV_PAR02+"' and ZD_COR = '"+oMSCOR:aCols[_i,1]+"' "
		  					
		  					MemoWrite("HESTP010_SZD.txt",_qry)
		  					
		  					nstatus := TcSqlExec(_qry)
				
		  					if (nStatus < 0)
		  						conout("TCSQLError() " + TCSQLError())
		  					endif
	
		  				Endif 
		  			Endif
		  		Next
		  		
		  		_qry := "Update "+RetSqlName("SB1")+" set B1_YFORMAT = '', B1_YSITUAC = 'PNV', B1_YDATLIB = '', B1_YDESCPT = cast('' as varbinary), B1_YDESCES = cast('' as varbinary), B1_YDESCIE = cast('' as varbinary), B1_CODBAR = '', B1_YDATCAD = '"+dtos(ddatabase)+"' "
				_qry += CRLF + "where D_E_L_E_T_ = '' and left(B1_COD,8) = '"+MV_PAR02+"' "
				
				MemoWrite("HESTP010_SB1.txt",_qry)
				
				nStatus := TcSqlExec(_qry)
	
				if (nStatus < 0)
					conout("TCSQLError() " + TCSQLError())
				endif	  					
//			Endif
		Endif
		
		If MV_PAR04 = 1
			DbSelectArea("SG1")
			DbSetOrder(1)
			DbSeek(xfilial("SG1")+MV_PAR02)
			
			If !Found()
				_qry := "exec dbo.CRIA_SZE '"+MV_PAR01+"','"+MV_PAR02+"','"+ccor+"'"
				memoWrite("HESTP010_SZEA.txt",_qry)
				nstatus := TcSqlExec(_qry)
		
				if (nStatus < 0)
		    		conout("TCSQLError() " + TCSQLError())
		  		endif
		
		  		For _i := 1 to len(oMSCOR:aCols)
		  			If oMSCOR:aCols[_i,len(aheaderEX)+1] = .F.
		  				If oMSCOR:aCols[_i,1] <> oMSCOR:aCols[_i,3]
		  					_qry := "Update "+RetSqlName("SZF")+" set ZF_COR = '"+oMSCOR:aCols[_i,3]+"', "
		  					_qry += CRLF + "ZF_TAM001=0,ZF_TAM002=0,ZF_TAM003=0,ZF_TAM004=0,ZF_TAM005=0,ZF_TAM006=0,ZF_TAM007=0,ZF_TAM008=0,ZF_TAM009=0,ZF_TAM010=0, "
		  					_qry += CRLF + "ZF_TAM011=0,ZF_TAM012=0,ZF_TAM013=0,ZF_TAM014=0,ZF_TAM015=0,ZF_TAM016=0,ZF_TAM017=0,ZF_TAM018=0,ZF_TAM019=0,ZF_TAM020=0, "
		  					_qry += CRLF + "ZF_TAM021=0,ZF_TAM022=0,ZF_TAM023=0,ZF_TAM024=0,ZF_TAM025=0,ZF_TAM026=0,ZF_TAM027=0,ZF_TAM028=0,ZF_TAM029=0,ZF_TAM030=0, "
		  					_qry += CRLF + "ZF_TAM031=0,ZF_TAM032=0,ZF_TAM033=0,ZF_TAM034=0,ZF_TAM035=0,ZF_TAM036=0,ZF_TAM037=0,ZF_TAM038=0,ZF_TAM039=0,ZF_TAM040=0, "
		  					_qry += CRLF + "ZF_TAM041=0,ZF_TAM042=0,ZF_TAM043=0,ZF_TAM044=0,ZF_TAM045=0,ZF_TAM046=0,ZF_TAM047=0,ZF_TAM048=0,ZF_TAM049=0,ZF_TAM050=0, "
		  					_qry += CRLF + "ZF_TAM051=0,ZF_TAM052=0,ZF_TAM053=0,ZF_TAM054=0,ZF_TAM055=0,ZF_TAM056=0,ZF_TAM057=0,ZF_TAM058=0,ZF_TAM059=0,ZF_TAM060=0, "
		  					_qry += CRLF + "ZF_TAM061=0,ZF_TAM062=0,ZF_TAM063=0,ZF_TAM064=0,ZF_TAM065=0,ZF_TAM066=0,ZF_TAM067=0,ZF_TAM068=0,ZF_TAM069=0,ZF_TAM070=0, "
		  					_qry += CRLF + "ZF_TAM071=0,ZF_TAM072=0,ZF_TAM073=0,ZF_TAM074=0,ZF_TAM075=0,ZF_TAM076=0,ZF_TAM077=0,ZF_TAM078=0,ZF_TAM079=0,ZF_TAM080=0, "
		  					_qry += CRLF + "ZF_TAM081=0,ZF_TAM082=0,ZF_TAM083=0,ZF_TAM084=0,ZF_TAM085=0,ZF_TAM086=0,ZF_TAM087=0,ZF_TAM088=0,ZF_TAM089=0,ZF_TAM090=0, "
		  					_qry += CRLF + "ZF_TAM091=0,ZF_TAM092=0,ZF_TAM093=0,ZF_TAM094=0,ZF_TAM095=0,ZF_TAM096=0,ZF_TAM097=0,ZF_TAM098=0,ZF_TAM099=0,ZF_TAM100=0, "
		  					_qry += CRLF + "ZF_TAM101=0,ZF_TAM102=0,ZF_TAM103=0,ZF_TAM104=0,ZF_TAM105=0,ZF_TAM106=0,ZF_TAM107=0,ZF_TAM108=0,ZF_TAM109=0,ZF_TAM110=0, "
		  					_qry += CRLF + "ZF_TAM111=0,ZF_TAM112=0,ZF_TAM113=0,ZF_TAM114=0,ZF_TAM115=0,ZF_TAM116=0,ZF_TAM117=0,ZF_TAM118=0,ZF_TAM119=0,ZF_TAM120=0, "
		  					_qry += CRLF + "ZF_TAM121=0,ZF_TAM122=0,ZF_TAM123=0,ZF_TAM124=0,ZF_TAM125=0,ZF_TAM126=0,ZF_TAM127=0,ZF_TAM128=0,ZF_TAM129=0,ZF_TAM130=0, "
		  					_qry += CRLF + "ZF_TAM131=0,ZF_TAM132=0,ZF_TAM133=0,ZF_TAM134=0,ZF_TAM135=0,ZF_TAM136=0,ZF_TAM137=0,ZF_TAM138=0,ZF_TAM139=0,ZF_TAM140=0, "
		  					_qry += CRLF + "ZF_TAM141=0,ZF_TAM142=0,ZF_TAM143=0,ZF_TAM144=0,ZF_TAM145=0,ZF_TAM146=0,ZF_TAM147=0,ZF_TAM148=0,ZF_TAM149=0,ZF_TAM150=0, "
		  					_qry += CRLF + "ZF_TAM151=0,ZF_TAM152=0,ZF_TAM153=0,ZF_TAM154=0,ZF_TAM155=0,ZF_TAM156=0,ZF_TAM157=0,ZF_TAM158=0,ZF_TAM159=0,ZF_TAM160=0, "
		  					_qry += CRLF + "ZF_TAM161=0,ZF_TAM162=0,ZF_TAM163=0,ZF_TAM164=0,ZF_TAM165=0,ZF_TAM166=0,ZF_TAM167=0,ZF_TAM168=0,ZF_TAM169=0,ZF_TAM170=0, "
		  					_qry += CRLF + "ZF_TAM171=0,ZF_TAM172=0,ZF_TAM173=0,ZF_TAM174=0,ZF_TAM175=0,ZF_TAM176=0,ZF_TAM177=0,ZF_TAM178=0,ZF_TAM179=0,ZF_TAM180=0, "
		  					_qry += CRLF + "ZF_TAM181=0,ZF_TAM182=0,ZF_TAM183=0,ZF_TAM184=0,ZF_TAM185=0,ZF_TAM186=0,ZF_TAM187=0,ZF_TAM188=0,ZF_TAM189=0,ZF_TAM190=0, "
		  					_qry += CRLF + "ZF_TAM191=0,ZF_TAM192=0,ZF_TAM193=0,ZF_TAM194=0,ZF_TAM195=0,ZF_TAM196=0,ZF_TAM197=0,ZF_TAM198=0,ZF_TAM199=0,ZF_TAM200=0 "
		  					_qry += CRLF + "from "+RetSqlName("SZF")+" SZF " 
		  					_qry += CRLF + "inner join "+RetSqlName("SB4")+" SB4 on SB4.D_E_L_E_T_ = '' and B4_COD = left(ZF_PRODUTO,8) "
		  					_qry += CRLF + "inner join "+RetSqlName("SBV")+" SBV on SBV.D_E_L_E_T_ = '' and SBV.BV_TABELA = 'COR' and SBV.BV_CHAVE = '"+oMSCOR:aCols[_i,3]+"' "
		  					_qry += CRLF + "where SZF.D_E_L_E_T_ = '' and left(ZF_PRODUTO,8) = '"+MV_PAR02+"' and ZF_COR = '"+oMSCOR:aCols[_i,1]+"' "
		  					
		  					MemoWrite("HESTP010_SZF.txt",_qry)
		  					
		  					nstatus := TcSqlExec(_qry)
				
		  					if (nStatus < 0)
		  						conout("TCSQLError() " + TCSQLError())
		  					endif
	
		  				Endif 
		  			Endif
		  		Next
		  	Endif
		Endif
	
/*		If MV_PAR05 = 1
			_qry := "exec dbo.CRIA_SZN '"+MV_PAR01+"','"+MV_PAR02+"'"
			nstatus := TcSqlExec(_qry)
	
			if (nStatus < 0)
	    		conout("TCSQLError() " + TCSQLError())
	  		endif
		Endif
*/		
		MsgInfo("Produto "+MV_PAR02+" cadastrado com sucesso!","Aten��o")
	Endif
ENDIF

Return

User Function SelCor()
// Cadastro de estrutura de produtos - Controle de vers�o

	Local oCancela
	Local oCodProd
	Local oDescProd
	Local cDescProd := SB4->B4_DESC
	Local oOk
	Local oSay1
	Private cCodProd := SB4->B4_COD
	Static oDlg

	Public cColuna		:= Space(03)
	Public cColunx		:= Space(03)
	Public cCor			:= Space(500)

	cColuna := SB4->B4_COLUNA
	DEFINE MSDIALOG oDlg TITLE "Sele��o de Cores para C�pia" FROM 000, 000  TO 300, 800 COLORS 0, 16777215 PIXEL

	fMSCor()
	@ 011, 007 SAY oSay1 PROMPT "Produto" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 010, 042 MSGET oCodProd VAR cCodProd SIZE 066, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
	@ 010, 113 MSGET oDescProd VAR cDescProd SIZE 278, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 124, 289 BUTTON oCancela PROMPT "Cancela" SIZE 037, 012 ACTION oDlg:End() OF oDlg PIXEL
	@ 124, 332 BUTTON oInclui PROMPT "Salva" SIZE 037, 012 ACTION (Salva(),oDlg:End()) OF oDlg PIXEL
    
	ACTIVATE MSDIALOG oDlg CENTERED

Return

//------------------------------------------------ 
Static Function fMSCor()
//------------------------------------------------ 
// Acols da Revis�o

	Local nX
	Local aFieldFill := {}
	Local aFields := {"ZD_COR","ZD_DESCRIC","ZF_COR"}
	Local aAlterFields := {"ZF_COR"}

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			If alltrim(SX3->X3_CAMPO) = "ZF_COR"
				Aadd(aHeaderEx, {"Nv Cor",SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"u_vldsbv(M->ZF_COR)",;
					SX3->X3_USADO,SX3->X3_TIPO,"SBV-02",SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Else
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		Endif
	Next nX

  // Define field values
	DbSelectarea("SZD")
	DbSetOrder(1)
	DbSeek(xfilial("SZD")+alltrim(SB4->B4_COD))
  
	If Found()
		While !EOF() .and. alltrim(SZD->ZD_PRODUTO) == Alltrim(SB4->B4_COD)
			Aadd(aColsEx, {SZD->ZD_COR,SZD->ZD_DESCRIC,SPACE(3),.f.})
			DbSelectArea("SZD")
			DbSkip()
		End
	Endif

	oMSCOR := MsNewGetDados():New( 028, 003, 106, 394, GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return

Static Function Salva()

cStatus:="T"
cCor := ""

For _i := 1 to len(oMSCOR:aCols)
	If oMSCOR:aCols[_i,len(aheaderEX)+1] = .F.
		cCor += '"'+oMSCOR:aCols[_i,1]+'",'
	Endif
Next
cCor := SubStr(alltrim(cCor),1,len(alltrim(cCor))-1)
Return

User Function VLDSBV(_cor)
IF ExistCpo("SBV","COR"+_cor,1) 
	lret := .T.
Else
	lRet := .F.
Endif

Return(lret)