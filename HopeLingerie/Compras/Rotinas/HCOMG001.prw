// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : HCOMG001
// ---------+-------------------+-----------------------------------------------------------
// Date     | Author             | Description
// ---------+-------------------+-----------------------------------------------------------
// 05/04/18 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo
Permite a manuten��o de dados armazenados em .

@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     5/04/2018
/*/
//------------------------------------------------------------------------------------------
user function HCOMG001()

_cFor:= GETSXENUM("SA2","A2_COD")

DbSelectArea("SA2")
DbSetOrder(1)
DbSeek(xfilial("SA2")+_cFor)
 
While Found()
	CONFIRMSX8() 
	_cFor:= GETSXENUM("SA2","A2_COD")
	DbSeek(xfilial("SA2")+_cFor)
Enddo
		
return(_cFor)
