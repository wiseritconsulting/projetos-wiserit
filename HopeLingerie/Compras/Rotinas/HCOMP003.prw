#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#Include "Xmlxfun.ch"
#INCLUDE "bitmap.ch"
#INCLUDE "ap5mail.ch"
#INCLUDE "shell.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HCOMP001    �Autor  � Actual Trend     � Data �  23/10/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Importacao arquivo xml nota eletronica de DEVOLU��O        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HCOMP003()

	Local i, w
	Private cXml:= '',oXml
	Private _dtQuebra :=SuperGetMV("MV_DIMPXML",.F.,"20170101")
//���������������������������������������������������������������������Ŀ
//� Fontes do windows usadas                                            �
//�����������������������������������������������������������������������
	DEFINE FONT oFont1 NAME "Arial Black" SIZE 6,17
	DEFINE FONT oFont2 NAME "Courier New" SIZE 8,14
	DEFINE FONT oFont3 NAME "Arial Black" SIZE 13,20
	DEFINE FONT oFont4 NAME "Arial Black" SIZE 13,15
	DEFINE FONT oFont5 NAME "Arial Black" SIZE 7,17
	DEFINE FONT oFont6 NAME "Courier New" SIZE 6,20
	DEFINE FONT oFont7 NAME "Courier New" SIZE 7,20

	_cUsuario:=ALLTRIM(UPPER(SUBSTR(CUSUARIO,7,15)))
	_cEmpresa:=SM0->M0_CODIGO
	_cCorrente:=SM0->M0_CODFIL

	cNCM:=''
	cDecQtd:="2"
	cDecUni:="7"

//���������������������������������������������������������������������Ŀ
//� Filial e empresa atual												�
//�����������������������������������������������������������������������
	DbSelectarea("SM0")
	Dbsetorder(1)
	Dbgotop()
	Dbseek(_cEmpresa+_cCorrente)

	IF !File("\xml")
		msgbox("N�o existe o diret�rio XML no ROOTPATH")
		Return
	Endif
	IF !File("\xml\importados")
		msgbox("N�o existe o diret�rio IMPORTADOS no diret�rio \XML")
		Return
	Endif
	IF !File("\xml\duplicados")
		msgbox("N�o existe o diret�rio DUPLICADOS no diret�rio \XML")
		Return
	Endif
	IF !File("\xml\corrompidos")
		msgbox("N�o existe o diret�rio CORROMPIDOS no diret�rio \XML")
		Return
	Endif

	cSerie:=''
	cEspecie:='SPED'
	cAlmox:=''
	cUnidades:='UNI'
	cPedCom:=.F.
	cAlmoPed:=space(02)
	cZeros:=.T.
	cDecUni:="7"
	cDecQtd:="2"
	cSerieNF:=cSerie

//���������������������������������������������������������������������Ŀ
//� Apagando arquivos diferentes de XML									�
//�����������������������������������������������������������������������
	aXML:={}
	ADir("\xml\*.*",aXML)

	For i:=1 to len(aXML)
		If !"XML" $ UPPER(ALLTRIM(aXML[i]))
			ferase("\xml\"+lower(ALLTRIM(aXML[i])))
		Endif
	Next

//���������������������������������������������������������������������Ŀ
//� Resolucao da tela													�
//�����������������������������������������������������������������������
	aSize := MsAdvSize()
	IF aSize[5] >=1220
		_nTop:=760
		_nRight:=1225
		_nSize:=590
	Else
		@ 120,040 TO 750,1010 DIALOG oTela TITLE "Importa��o nota fiscal eletr�nica de Devolu��o- "+SM0->M0_CODIGO+"/"+SM0->M0_CODFIL+"-"+SM0->M0_FILIAL
		_nTop:=750
		_nRight:=1010
		_nSize:=485
	Endif

//���������������������������������������������������������������������Ŀ
//� Lista dos XML dos fornecedores										�
//�����������������������������������������������������������������������
	aXML:={}
	ADir("\xml\*.xml",aXML)

	If Len(aXml)==0
		Msgbox("N�o existem arquivos para serem importados no momento...","Aten��o...","INFO")
		Return
	Endif

//���������������������������������������������������������������������Ŀ
//� Produto alterados													�
//�����������������������������������������������������������������������
	aCampos5:= {{"PRODUTO","C",15,0 }}

	cArqTrab5  := UPPER(CriaTrab(aCampos5))
	DBUSEAREA(.T.,'DBFCDX', CARQTRAB5, 'LS5', .F., .F. )

	IndRegua("LS5",cArqTrab5,"PRODUTO",,,)
	dbSetIndex( cArqTrab5 +OrdBagExt())
	dbSelectArea("LS5")

	cDecUni:=val(cDecUni)
	cDecQtd:=val(cDecQtd)

	aCampos	:= {{"SEQ","N",5,0 },;
		{"OK","C",1,0 },;
		{"CODBAR","C",15,0 },;
		{"PRODUTO","C",15,0 },;
		{"PRODCLI","C",15,0 },;
		{"DESCRICAO","C",50,0 },;
		{"DESCORI","C",50,0 },;
		{"UM","C",2,0 },;
		{"QE","N",4,0 },;
		{"CAIXAS","N",11,cDecQtd },;
		{"NCM","C",10,0 },;
		{"QUANTIDADE","N",11,cDecQtd},;
		{"PRECO","N",18,cDecUni },;
		{"PRECOCLI","N",18,cDecUni},;
		{"TOTAL","N",14,2 },;
		{"EMISSAO","C",8,0 },;
		{"PEDIDO","C",6,0 },;
		{"ITEM","C",4,0 },;
		{"TES","C",3,0 },;
		{"ALMOX","C",2,0 },;
		{"ALTERADO","C",1,0 },;
		{"NOME","C",35,0 },;
		{"NOTA","C",9,0 }}

	cArqTrab  := CriaTrab(aCampos)
	dbUseArea( .T.,, cArqTrab, "LS1", .F., .F. )
	IndRegua("LS1",cArqTrab,"SEQ",,,)
	dbSetIndex( cArqTrab +OrdBagExt())
	dbSelectArea("LS1")

	aCampos3:= {{"EMISSAO","D",8,0 },;
		{"CLIENTE","C",6,0 },;
		{"LOJA","C",4,0 },;
		{"NOTA","C",9,0 },;
		{"NOME","C",35,0 },;
		{"VENDEDOR","C",30,0 },;
		{"TELEFONE","C",20,0 },;
		{"XML","C",150,0 },;
		{"CHAVE","C",60,0 }}

	cArqTrab3  := CriaTrab(aCampos3)
	dbUseArea( .T.,, cArqTrab3, "LS3", .F., .F. )
	IndRegua("LS3",cArqTrab3,"NOME+NOTA",,,)
	dbSetIndex( cArqTrab3 +OrdBagExt())
	dbSelectArea("LS3")

	_cCNPJ:= ''
	lAchou:=.F.

	#IFDEF WINDOWS
		Processa({|| XMLFOUND()})
		Return
	Static Function XMLFOUND()
	#ENDIF

	Local i, w
	aCampos4:= {{"NOTA","C",9,0 },;
		{"CLIENTE","C",6,0 },;
		{"LOJA","C",4,0 }}

	cArqTrab4  := CriaTrab(aCampos4)
	dbUseArea( .T.,, cArqTrab4, "LS4", .F., .F. )
	IndRegua("LS4",cArqTrab4,"CLIENTE+LOJA+NOTA",,,)
	dbSetIndex( cArqTrab4 +OrdBagExt())
	dbSelectArea("LS4")

	cNota		:=''
	cEmissao	:=''
	cChave		:=''
	_cOpcao	:=''
	cFoneXML	:= ''
	cNomeXML	:= ''

	Procregua(len(aXML))
	For i:=1 to len(aXML)
	
	//���������������������������������������������������������������������Ŀ
	//� Dados do XML														�
	//�����������������������������������������������������������������������
		XML(i)
	
	//���������������������������������������������������������������������Ŀ
	//� Cliente															�
	//�����������������������������������������������������������������������
		If !Empty(_cCNPJ)
			DbSelectarea("SA1")
			DbSetorder(3)
			Dbgotop()
			Dbseek(xFilial("SA1")+_cCNPJ)
			If Found()
			
			//���������������������������������������������������������������������Ŀ
			//� Verificando grupo													�
			//�����������������������������������������������������������������������
				lCliente:=.T.
			
				If lCliente
					Incproc(SA1->A1_NREDUZ)
				
				//���������������������������������������������������������������������Ŀ
				//� Verifico arquivos XML duplicados									�
				//�����������������������������������������������������������������������
					DbSelectarea("LS4")
					DbSetorder(1)
					Dbgotop()
					dbseek(SA1->A1_COD+SA1->A1_LOJA+cNota)
					If !Found()
						Reclock("LS4",.T.)
						LS4->NOTA:=cNota
						LS4->CLIENTE:=SA1->A1_COD
						LS4->LOJA:=SA1->A1_LOJA
						MsUnlock()
					
						Reclock("LS3",.T.)
						LS3->EMISSAO:=STOD(cEmissao)
						LS3->CLIENTE:=SA1->A1_COD
						LS3->LOJA:=SA1->A1_LOJA
						LS3->VENDEDOR:= POSICIONE('SA3',1,XFILIAL('SA3')+POSICIONE('ZA4',1,XFILIAL('ZA4')+SA1->A1_XMICRRE, 'ZA4_REPRE'), 'A3_NOME')
						LS3->TELEFONE:=alltrim(SA1->A1_DDD)+" "+alltrim(SUBSTR(SA1->A1_TEL,1,20))
						LS3->NOME:=SA1->A1_NREDUZ
						LS3->XML:=UPPER(aXML[i])
						LS3->NOTA:=cNota
						LS3->CHAVE:=cChave
						MsUnlock()
						lAchou:=.T.
					Endif
				Endif
			Else
				DbSelectarea("LS4")
				DbSetorder(1)
				Dbgotop()
				dbseek('XXXXXX'+'XX'+cNota)

				Reclock("LS4",.T.)
				LS4->NOTA			:= cNota
				LS4->CLIENTE	:= 'XXXXXX'
				LS4->LOJA			:= 'XX'
				MsUnlock()
					
				Reclock("LS3",.T.)
				LS3->EMISSAO	:= STOD(cEmissao)
				LS3->CLIENTE	:= "XXXXXX"
				LS3->LOJA		:= "XX"
				LS3->NOME		:= cNomeXML
				LS3->VENDEDOR	:= ""
				LS3->TELEFONE	:= cFoneXML
				LS3->XML 		:= UPPER(aXML[i])
				LS3->NOTA		:= cNota
				LS3->CHAVE		:= cChave
				MsUnlock()
				lAchou:=.T.
			Endif
		Endif
	Next

	Dbselectarea("LS4")
	dbCloseArea()
	fErase( cArqTrab4+ ".DBF" )
	fErase( cArqTrab4+ OrdBagExt() )

	If lAchou==.F.
		Msgbox("N�o existem arquivos para serem importados no momento...","Aten��o...","ALERT")
		Dbselectarea("LS1")
		dbCloseArea()
		fErase( cArqTrab+ ".DBF" )
		fErase( cArqTrab+ OrdBagExt() )
	
		Dbselectarea("LS5")
		dbCloseArea()
		fErase( cArqTrab5+ ".DBF" )
		fErase( cArqTrab5+ OrdBagExt() )
	
		Dbselectarea("LS3")
		dbCloseArea()
		fErase( cArqTrab3+ ".DBF" )
		fErase( cArqTrab3+ OrdBagExt() )
		Return
	Endif

	cNota:=space(09)
	cNatOp:=''
	_cCNPJ:= space(18)
	_cMensag:=''
	nTotalNF:=0
	nTotIt:=0
	_cCliente:=''
	_cTelefone:=''
	_cInscr:=''
	_cEnd:=''
	_cCidade:=''
	_cEmissao:=''
	cUm:=''
	nDescont:=0

//���������������������������������������������������������������������Ŀ
//� aHeaders 															�
//�����������������������������������������������������������������������
	cPict1:="@E 99,999."
	For w:=1 to cDecQtd
		cPict1:=alltrim(cPict1)+"9"
	Next
	cPict2:="@E 99,999."
	For w:=1 to cDecUni
		cPict2:=alltrim(cPict2)+"9"
	Next

	DbSelectarea("LS3")
	Dbgotop()

//���������������������������������������������������������������������Ŀ
//� legenda de cores													�
//�����������������������������������������������������������������������
	aCores := {	{ 'LS1->OK=="X" '  , 'BR_VERMELHO'  },;
		{ 'LS1->OK<>"X" '  , 'BR_VERDE'  	}}

	aRotina	:= {{ , , 0 , 2 }}

	cMarca := GetMark()
	linverte:=.f.
	aTitulo := {}
	aTituloX := {}

	bColor := &("{||IIF(LS1->OK=='O',"+Str(CLR_HBLUE)+","+Str(CLR_BLACK)+")}")

//���������������������������������������������������������������������Ŀ
//� Tela principal da rotina											�
//�����������������������������������������������������������������������
	@ 120,040 TO _nTop,_nRight DIALOG oTela TITLE "Importa��o nota fiscal eletr�nica de Devolu��o- "+SM0->M0_CODIGO+"/"+SM0->M0_CODFIL+"-"+SM0->M0_FILIAL
	@ 004,005 BITMAP ResName "OPEN" OF oTela Size 15,15 ON CLICK (MsgRun("Executando Importa��o...",,{||IMPORTA()})) NoBorder  Pixel
	@ 005,025 BUTTON "Re_fazer Nota Fiscal" SIZE 65,10 ACTION MsgRun("Restaurando informa��es originais...",,{||REFAZER()})
	@ 005,095 BUTTON "Excluir Identifica��o" SIZE 65,10 ACTION EXCAMA()
	@ 005,165 BUTTON "Cons_ultar SEFAZ" SIZE 65,10 ACTION MsgRun("Processando NFE no site da SEFAZ...",,{||SEFAZ()})
	@ 005,390 say "NOTA FISCAL ELETR�NICA DE DEVOLU��O" FONT oFont5 OF oTela PIXEL COLOR CLR_HRED

//���������������������������������������������������������������������Ŀ
//� Principal 															�
//�����������������������������������������������������������������������
	@ 020,005 TO 110,_nSize BROWSE "LS3" OBJECT OBRWP FIELDS aTituloX
	OBRWP:oBrowse:BCHANGE := {||PROCESS()}
	OBRWP:oBrowse:oFont := TFont():New ("Arial", 05, 18)

	OBRWP:oBrowse:AddColumn(TCColumn():New("Emiss�o",   	{||LS3->EMISSAO},"@D 99/99/9999",,,"LEFT",40))
	OBRWP:oBrowse:AddColumn(TCColumn():New("Cliente",		{||LS3->CLIENTE},,,,"LEFT",30))
	OBRWP:oBrowse:AddColumn(TCColumn():New("Loja",      	{||LS3->LOJA},,,,"LEFT",20))
	OBRWP:oBrowse:AddColumn(TCColumn():New("Nome",      	{||LS3->NOME},,,,"LEFT",120))
	OBRWP:oBrowse:AddColumn(TCColumn():New("Nota",  		{||LS3->NOTA},"@!",,,"LEFT",50))
	OBRWP:oBrowse:AddColumn(TCColumn():New("Telefone",  	{||LS3->TELEFONE},"@!",,,"LEFT",60))
	OBRWP:oBrowse:AddColumn(TCColumn():New("Representante",	{||LS3->VENDEDOR},"@!",,,"LEFT",70))
	OBRWP:oBrowse:AddColumn(TCColumn():New("Nota Fiscal Eletr�nica",{||LS3->CHAVE},"@!",,,"LEFT",80))

//���������������������������������������������������������������������Ŀ
//� Secundaria															�
//�����������������������������������������������������������������������
	OBRWI:=MsSelect():New("LS1","","",aTitulo,@lInverte,@cMarca,{125,005,240,_nSize},,,,,aCores)
	OBRWI:oBrowse:bLDblClick := {||CORRIGE()}
	OBRWI:oBrowse:oFont := TFont():New ("Arial", 05, 18)

	OBRWI:oBrowse:AddColumn(TCColumn():New("C�d.Cli." ,{||LS1->PRODCLI},,,,"LEFT", 60))
	OBRWI:oBrowse:AddColumn(TCColumn():New("Produto"  ,{||LS1->PRODUTO},,,,"LEFT", 60))
	OBRWI:oBrowse:AddColumn(TCColumn():New("Descri��o",{||LS1->DESCRICAO},,,,"LEFT",200))
	OBRWI:oBrowse:AddColumn(TCColumn():New("UM"       ,{||LS1->UM},,,,"LEFT", 25))
	OBRWI:oBrowse:AddColumn(TCColumn():New("Emb."     ,{||LS1->QE},"@E 9999",,,"LEFT", 25))
	OBRWI:oBrowse:AddColumn(TCColumn():New("Caixas"   ,{||LS1->CAIXAS},cPict1,,,"RIGHT", 25))
	OBRWI:oBrowse:AddColumn(TCColumn():New("Quant."   ,{||LS1->QUANTIDADE},cPict1,,,"RIGHT", 45))
	OBRWI:oBrowse:AddColumn(TCColumn():New("Pre�o R$" ,{||LS1->PRECO},cPict2,,,"RIGHT", 45))
	OBRWI:oBrowse:AddColumn(TCColumn():New("Total R$",{||LS1->TOTAL},"@E 99,999.99",,,"RIGHT", 45))
	OBRWI:oBrowse:SetBlkColor(bColor)

	@ 245,003 TO 315,235
	@ 250,005 say "CLIENTE" SIZE 150,40 FONT oFont4 OF oTela PIXEL COLOR CLR_GREEN
	@ 260,005 say _cCliente size 200,20 FONT oFont3 OF oTela PIXEL COLOR CLR_HBLUE
	@ 270,005 say "CNPJ" FONT oFont1 OF oTela PIXEL
	@ 270,040 say _cCNPJ size 80,20 size 50,20 FONT oFont2 OF oTela PIXEL
	@ 280,005 say "Endere�o" FONT oFont1 OF oTela PIXEL
	@ 280,040 say _cEnd size 170,20 FONT oFont2 OF oTela PIXEL
	@ 300,005 say "Cidade/UF" FONT oFont1 OF oTela PIXEL
	@ 300,040 say _cCidade size 150,20 size 100,20 FONT oFont2 OF oTela PIXEL

	@ 245,240 TO 315,435
	@ 250,250 say "NOTA FISCAL" FONT oFont4 OF oTela PIXEL COLOR CLR_GREEN
	@ 260,250 say "Emiss�o" FONT oFont1 OF oTela PIXEL
	@ 260,290 say _cEmissao size 80,40 picture "@D 99/99/9999" FONT oFont3 OF oTela PIXEL
	@ 270,250 say "Total R$" FONT oFont1 OF oTela PIXEL
	@ 270,290 say nTotalNF size 80,40 picture "@E 99,999.99" FONT oFont3 OF oTela PIXEL
	@ 280,250 say "Qtd.Itens" FONT oFont1 OF oTela PIXEL
	@ 280,290 say nTotIt size 40,40 picture "@E 9999" FONT oFont3 OF oTela PIXEL
	@ 290,250 say "Nat.Opera��o" FONT oFont1 OF oTela PIXEL
	@ 290,290 say SUBSTR(alltrim(cNatOP),1,32) size 180,40 picture "@!" FONT oFont2 OF oTela PIXEL COLOR CLR_HRED
	@ 300,250 say "S�rie/Nota Fiscal" FONT oFont1 OF oTela PIXEL
	@ 300,310 say ALLTRIM(cSerie)+"-"+cNota size 80,40 picture "@!" FONT oFont3 OF oTela PIXEL COLOR CLR_MAGENTA
	@ 112,005 BUTTON "Legenda" SIZE 65,10 ACTION LEGENDA()
	@ 112,075 BUTTON "C�d.Barras" SIZE 65,10 ACTION CODBAR()
	@ 112,145 BUTTON "_Mensagem Nota" SIZE 65,10 ACTION MSGNF(_cMensag)

	If aSize[5] >=1220
		@ 018,055 BITMAP SIZE 110,110 FILE "NFE.BMP" NOBORDER
	Endif
	ACTIVATE DIALOG oTela CENTER

//���������������������������������������������������������������������Ŀ
//� Apagando arquivos temporarios										�
//�����������������������������������������������������������������������
	Dbselectarea("LS1")
	dbCloseArea()
	fErase( cArqTrab+ ".DBF" )
	fErase( cArqTrab+ OrdBagExt() )

	Dbselectarea("LS5")
	dbCloseArea()
	fErase( cArqTrab5+ ".DBF" )
	fErase( cArqTrab5+ OrdBagExt() )

	Dbselectarea("LS3")
	dbCloseArea()
	fErase( cArqTrab3+ ".DBF" )
	fErase( cArqTrab3+ OrdBagExt() )
Return

//���������������������������������������������������������������������Ŀ
//� Gerando pre nota													�
//�����������������������������������������������������������������������
Static Function IMPORTA()

//���������������������������������������������������������������������Ŀ
//� Verifico se existe a nota fiscal									�
//�����������������������������������������������������������������������
	IF !file("\xml\"+lower(LS3->XML))
		msgbox("Este arquivo j� foi processado por outro usu�rio!","Aten��o...","ALERT")
		Reclock("LS3",.F.)
		dbdelete()
		MsUnlock()
	
		DbSelectarea("LS3")
		Dbgotop()
		PROCESS()
		Return
	Endif

//���������������������������������������������������������������������Ŀ
//� Verificando se todas as variaveis foram preenchidas					�
//�����������������������������������������������������������������������
	If Empty(cNota)
		Msgbox("Numero de nota fiscal n�o encontrada!")
		Return
	Endif
	If Empty(_cCNPJ)
		Msgbox("Dados do Cliente n�o encontrados. Numero de nota fiscal n�o encontrada!")
		Return
	Endif
	If nTotIt<=0
		Msgbox("Nota fiscal n�o contem itens!")
		Return
	Endif
	If nTotalNF<=0
		Msgbox("Nota fiscal sem valores das mercadorias!")
		Return
	Endif

//���������������������������������������������������������������������Ŀ
//� Verificando se todos os produtos foram identificados				�
//�����������������������������������������������������������������������
	lIdent:=.F.
	DbSelectarea("LS1")
	Dbgotop()
	While !Eof()
		IF LS1->OK=="X"
			lIdent:=.T.
		Endif
		Dbskip()
	End
	Dbgotop()

	If lIdent
		Msgbox("Existem produtos n�o identificados, corrija primeiro!","Aten��o...","ALERT")
		Return
	Endif


//���������������������������������������������������������������������Ŀ
//� Valida se o preco esta proximo do correto							�
//�����������������������������������������������������������������������
	cMsg:=''

	If LS3->CLIENTE=='XXXXXX'
		cMsgFor	:= "Favor cadastrar o Cliente para continuar a importa��o do XML."
		DEFINE MSDIALOG oProdd FROM 0,0 TO 330,420 PIXEL TITLE "Cliente N�o cadastrado..."
		@ 005,005 GET oMemo VAR cMsgFor MEMO SIZE 200,135 FONT oFont6 PIXEL OF oProdd
		@ 150,005 BUTTON "<< Voltar" SIZE 55,10 ACTION oProdd:end()
		ACTIVATE MSDIALOG oProdd CENTER
		Return
	Endif
	If !Empty(cMsg)
		lSaida:=.F.
		DEFINE MSDIALOG oProdd FROM 0,0 TO 330,420 PIXEL TITLE "Produtos com diverg�ncia de pre�os..."
		@ 005,005 GET oMemo VAR cMsg MEMO SIZE 200,135 FONT oFont6 PIXEL OF oProdd
		@ 150,005 BUTTON "<< Voltar" SIZE 55,10 ACTION oProdd:end()
		@ 150,070 BUTTON "Continuar >>" SIZE 55,10 ACTION (lsaida:=.T.,oProdd:end())
		ACTIVATE MSDIALOG oProdd CENTER
	
		If lSaida==.F.
			Return
		Endif
	Endif

	//���������������������������������������������������������������������Ŀ
	//� Nao controla pedidos de compras										�
	//�����������������������������������������������������������������������
	cRet:=.T.
	
	//���������������������������������������������������������������������Ŀ
	//� Manipulando numero da nota fiscal									�
	//�����������������������������������������������������������������������
	If cZeros
		cNota:=strzero(val(cNota),9)
	Endif
	cSpaco:=9-len(alltrim(cNota))
	
	cResp:=msgbox("Deseja gerar a pr�-nota fiscal "+cNota+" agora?","Aten��o...","YESNO")
	
	If cResp
		//���������������������������������������������������������������������Ŀ
		//� Verifico se a pre nota ja existe									�
		//�����������������������������������������������������������������������
		dbSelectArea("SF1")
		DbSetorder(2)
		Dbgotop()
		Dbseek(xFilial("SF1")+LS3->CLIENTE+LS3->LOJA+alltrim(cNota)+Space(cSpaco))
		If Found() .and. SF1->F1_TIPO=="D"
			
			Msgbox("Nota fiscal j� existe!","Aten��o...","ALERT")
			
			//���������������������������������������������������������������������Ŀ
			//� Dados do Cliente       													�
			//�����������������������������������������������������������������������
			DbSelectarea("SA1")
			DbSetorder(1)
			Dbgotop()
			Dbseek(xFilial("SA1")+LS3->CLIENTE+LS3->LOJA)
			
			FRename("\xml\"+ALLTRIM(LS3->XML),"\xml\"+ALLTRIM(SA1->A1_CGC)+"-nf"+ALLTRIM(LS3->NOTA)+"-"+ALLTRIM(LS3->CHAVE)+"xml.imp")
			__CopyFile("\xml\*.imp","\xml\importados\")
			ferase("\xml\"+ALLTRIM(SA1->A1_CGC)+"-nf"+ALLTRIM(LS3->NOTA)+"-"+ALLTRIM(LS3->CHAVE)+"xml.imp")
			
			Reclock("LS3",.F.)
			dbdelete()
			MsUnlock()
			
			DbSelectarea("LS3")
			Dbgotop()
			
			DbSelectarea("LS1")
			Dbsetorder(1)
			Dbgotop()
			While !Eof()
				Reclock("LS1",.F.)
				dbdelete()
				MsUnlock()
				Dbskip()
			End
			
			DbSelectarea("LS5")
			Dbsetorder(1)
			Dbgotop()
			While !Eof()
				Reclock("LS5",.F.)
				dbdelete()
				MsUnlock()
				Dbskip()
			End
			
			PROCESS()
			Return
		Endif
		
		//���������������������������������������������������������������������Ŀ
		//� Gravando pre nota entrada											�
		//�����������������������������������������������������������������������
		MsgRun("Gerando pr� nota entrada No.:"+cNota,,{||PRENOTA()})
		cNotaAtu:=cNota
		
		If cRet

			DbSelectarea("LS3")
				
			//���������������������������������������������������������������������Ŀ
			//� Dados do fornecedor													�
			//�����������������������������������������������������������������������
			DbSelectarea("SA1")
			DbSetorder(1)
			Dbgotop()
			Dbseek(xFilial("SA1")+LS3->CLIENTE+LS3->LOJA)
			
			FRename("\xml\"+ALLTRIM(LS3->XML),"\xml\"+ALLTRIM(SA1->A1_CGC)+"-nf"+ALLTRIM(LS3->NOTA)+"-"+ALLTRIM(LS3->CHAVE)+"xml.imp")
			__CopyFile("\xml\*.imp","\xml\importados\")
			ferase("\xml\"+ALLTRIM(SA1->A1_CGC)+"-nf"+ALLTRIM(LS3->NOTA)+"-"+ALLTRIM(LS3->CHAVE)+"xml.imp")
			
			Reclock("LS3",.F.)
			dbdelete()
			MsUnlock()
			
			DbSelectarea("LS3")
			Dbgotop()
			Msgbox("Pr�-Nota "+cNotaAtu+" gerada com sucesso!","Aten��o...","INFO")
			PROCESS()
		Endif
	Else
		//���������������������������������������������������������������������Ŀ
		//� Apagando Flag dos pedidos											�
		//�����������������������������������������������������������������������
		DbSelectarea("LS1")
		DbSetorder(1)
		Dbgotop()
		While !Eof()
			IF !Empty(LS1->PEDIDO)
				Reclock("LS1",.F.)
				LS1->PEDIDO:=""
				LS1->ITEM:=""
				LS1->ALTERADO:=""
				MsUnlock()
			Endif
			Dbskip()
		End
		DbSelectarea("LS1")
		DbSetorder(1)
		Dbgotop()
	Endif

	Dbselectarea("LS1")
	Dbgotop()
Return

//���������������������������������������������������������������������Ŀ
//� Corrigir produto													�
//�����������������������������������������������������������������������
Static Function CORRIGE()

	nSeek:=LS1->SEQ

	If LS1->OK=="X"
		aCampos6:= {{"PRODUTO","C",15,0 },;
			{"DESCRICAO","C",45,0 },;
			{"QE","N",6,2 },;
			{"SALDO","N",12,2 },;
			{"PEDIDO","C",3,0 },;
			{"BLQ","C",5,0 }}
	
		cArqTrab6  := CriaTrab(aCampos6)

		dbUseArea(.T.,         , cArqTrab6         ,"LS4", .F., .F. )
		IndRegua("LS4",cArqTrab6,"DESCRICAO",,,)
		dbSetIndex( cArqTrab6 +OrdBagExt())

		cQuery:="SELECT top 100 case when B1_MSBLQL = '1' then 'Bloq.' else 'Ativo' end as BLQ,B1_COD PRODUTO,B1_DESC DESCRICAO, "
		cQuery+="(B2_QATU-B2_RESERVA-B2_QEMP) as SALDO, B1_QE as QE, case when ( "
		cQuery+="SELECT COUNT(*) QTD FROM "+RetSqlName("SC7")+" SC7 WHERE C7_FILIAL='"+xfilial("SC7")+"' "
		cQuery+="AND C7_PRODUTO=B1_COD "
		cQuery+="AND C7_FORNECE='"+LS3->CLIENTE+"' "
		cQuery+="AND (C7_QUANT-C7_QUJE-C7_QTDACLA)>0 "
		cQuery+="AND C7_RESIDUO<>'S' "
		cQuery+="AND SC7.D_E_L_E_T_<>'*' ) > 0 then 'Sim' else 'N�o' end as PEDIDO "
		cQuery+="FROM "+RetSqlName("SB1")+" SB1 "
		cQuery+="left join "+RetSqlName("SB2")+" SB2 on SB2.D_E_L_E_T_ = '' and B2_COD = B1_COD and B2_LOCAL = B1_LOCPAD and B2_FILIAL = '"+xfilial("SB2")+"' "
		cQuery+="WHERE B1_FILIAL='"+Xfilial("SB1")+"' "
		cQuery+="AND SB1.D_E_L_E_T_<>'*' "
		cQuery+="order by B1_DESC"

		lTem:=.F.
	
		TCQUERY cQuery NEW ALIAS "TCQ"
	
		DbSelectarea("TCQ")
		DbGoTop()
		While !Eof()
			If Empty(cAlmox)
				cAlmox:=Posicione("SB1",1,xFilial("SB1")+TCQ->PRODUTO,"B1_LOCPAD")
			Endif
		
			Reclock("LS4",.T.)
			LS4->PRODUTO:=TCQ->PRODUTO
			IF "SAIU" $ TCQ->DESCRICAO
				LS4->DESCRICAO:=SUBSTR(TCQ->DESCRICAO,6,45)
			Else
				LS4->DESCRICAO:=TCQ->DESCRICAO
			Endif
			LS4->SALDO:=POSICIONE("SB2",2,xFilial("SB2")+cAlmox+TCQ->PRODUTO,"B2_QATU-B2_RESERVA-B2_QEMP")
			LS4->QE:=POSICIONE("SB1",1,xFilial("SB1")+TCQ->PRODUTO,"B1_QE")
			LS4->BLQ:=IIF(TCQ->BLQ=="1","Bloq.","Ativo")
			MsUnlock()
			DbSelectarea("TCQ")
			Dbskip()
		End
		DbClosearea()

		_cProduto:=Space(15)
	
		aTitulo6 := {}
		AADD(aTitulo6,{"BLQ","Sit."})
		AADD(aTitulo6,{"PRODUTO","Produto"})
		AADD(aTitulo6,{"DESCRICAO","Descri��o"})
		AADD(aTitulo6,{"QE","Qtd.Emb.","@E 99999"})
		AADD(aTitulo6,{"SALDO","Saldo Atual","@E 9999.99"})
	
		DbSelectarea("LS4")
		Dbgotop()
	
		_cFiltrox:=SUBSTR(LS1->DESCRICAO,1,4)+space(30)
	
		@ 120,040 TO 450,880 DIALOG oAmarra TITLE "Produto do Cliente..."
		@ 005,005 say LS1->DESCRICAO SIZE 200,40 FONT oFont1 OF oAmarra PIXEL
		@ 020,005 TO 140,417 BROWSE "LS4" OBJECT OBRWX FIELDS aTitulo6
		OBRWX:OBROWSE:bLDblClick   := {|| SELECIONA(LS4->PRODUTO,2) }
		OBRWX:oBrowse:oFont := TFont():New ("Arial", 07, 18)
	
		@ 005,210 say "Filtro" SIZE 200,40 FONT oFont1 OF oAmarra PIXEL COLOR CLR_HRED
		@ 005,230 get _cFiltrox SIZE 70,20 Picture "@!"
		@ 005,300 BUTTON "_Filtrar" SIZE 35,10 ACTION MsgRun("Processando produtos...",,{||FILTRE()})
		@ 150,010 BUTTON "Reativar Produto" SIZE 60,12 ACTION DESBLOQ()
		ACTIVATE DIALOG oAmarra CENTER
	
		Dbselectarea("LS4")
		dbCloseArea()
		Return
	Endif

//���������������������������������������������������������������������Ŀ
//� Corrigir produtos encontrados automaticamente						�
//�����������������������������������������������������������������������
	If Empty(LS1->OK) .OR. LS1->OK=="O"
		SELECIONA(LS1->PRODUTO,1)
	Endif
Return

//���������������������������������������������������������������������Ŀ
//� Processando arquivos												�
//�����������������������������������������������������������������������
Static Function XML(i)

	private _oXml    := NIL
	private cError   := ''
	private cWarning := ''
	private lNotaDev := ''
	nXmlStatus := XMLError()
	cFile:="\xml\"+lower(ALLTRIM(aXML[i]))
	oXml := XmlParserFile(cFile,"_",@cError, @cWarning )
	lTipo:=3
	_cCNPJ2:=''

	If ALLTRIM(TYPE("oxml:_NFE:_INFNFE"))=="O"
		lTipo     := 1
		lNotaDev  := oxml:_NFE:_INFNFE:_IDE:_FINNFE:TEXT
	Endif
	If ALLTRIM(TYPE("oxml:_NFEPROC:_NFE:_INFNFE"))=="O"
		lTipo:=2
		lNotaDev  := oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_FINNFE:TEXT
	Endif

	If Empty(@cError) .and. lTipo<>3 .and. lNotaDev=='4'
	//���������������������������������������������������������������������Ŀ
	//� Com _NFEPROC														�
	//�����������������������������������������������������������������������
		If lTipo==2
			cEmissao2:=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT
			cEmissao2:=SUBSTR(cEmissao2,1,4)+SUBSTR(cEmissao2,6,2)+SUBSTR(cEmissao2,9,2)
			If cEmissao2<=_dtQuebra
				FRename("\xml\"+lower(ALLTRIM(aXML[i])),"\xml\"+ALLTRIM(alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_EMIT:_CNPJ:TEXT))+"-nf"+ALLTRIM(strzero(val(oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_NNF:TEXT),9))+"-"+ALLTRIM(ALLTRIM(SUBSTR(oxml:_NFEPROC:_NFE:_INFNFE:_ID:TEXT,4,200)))+".xml.imp")
				__CopyFile("\xml\*.imp","\xml\importados\")
				ferase("\xml\"+ALLTRIM(alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_EMIT:_CNPJ:TEXT))+"-nf"+ALLTRIM(strzero(val(oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_NNF:TEXT),9))+"-"+ALLTRIM(ALLTRIM(SUBSTR(oxml:_NFEPROC:_NFE:_INFNFE:_ID:TEXT,4,200)))+".xml.imp")
				Return
			Endif
			IF ALLTRIM(TYPE("oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_IE:TEXT"))=="C"
				If SUBSTR(alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_IE:TEXT),1,1) $ "0/1/2/3/4/5/6/7/8/9"
					_cCNPJ2:=alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
				Else
					_cCNPJ2:=''
				Endif
			Else
				_cCNPJ2:=''
			Endif
			_cCNPJ:= alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_EMIT:_CNPJ:TEXT)
			cEmissao:=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT
			cEmissao:=SUBSTR(cEmissao,1,4)+SUBSTR(cEmissao,6,2)+SUBSTR(cEmissao,9,2)
			cNota:=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_NNF:TEXT
			If Empty(cSerieNF)
				cSerie:=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_SERIE:TEXT
			Endif
			cNatOp:=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_NATOP:TEXT
			cChave:=ALLTRIM(SUBSTR(oxml:_NFEPROC:_NFE:_INFNFE:_ID:TEXT,4,200))
			If ALLTRIM(TYPE("oxml:_NFEPROC:_NFE:_INFNFE:_EMIT:_ENDEREMIT:_FONE"))=="O"
				cFoneXML		:= alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_EMIT:_ENDEREMIT:_FONE:TEXT)
			Else
				cFoneXML		:= ""
			Endif
			cNomeXML		:= UPPER(ALLTRIM(oxml:_NFEPROC:_NFE:_INFNFE:_EMIT:_XNOME:TEXT))
		
		//���������������������������������������������������������������������Ŀ
		//� Manipulando numero da nota fiscal									�
		//�����������������������������������������������������������������������
			If len(alltrim(cNota))<=6
				cNota:=strzero(val(cNota),6)
			Endif
			If cZeros
				cNota:=strzero(val(cNota),9)
			Endif
			nTam:=len(alltrim(cNota))
			cSpaco:=(9-nTam)
		
		//���������������������������������������������������������������������Ŀ
		//� Empresa atual														�
		//�����������������������������������������������������������������������
			If alltrim(_cCNPJ2)<>alltrim(SM0->M0_CGC)
				_cCNPJ:=''
			Endif
		Else
		//���������������������������������������������������������������������Ŀ
		//� Sem _NFEPROC															�
		//�����������������������������������������������������������������������
			cEmissao2:=oxml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT
			cEmissao2:=SUBSTR(cEmissao2,1,4)+SUBSTR(cEmissao2,6,2)+SUBSTR(cEmissao2,9,2)
			If cEmissao2<=_dtQuebra
				FRename("\xml\"+lower(ALLTRIM(aXML[i])),"\xml\"+ALLTRIM(alltrim(oxml:_NFE:_INFNFE:_EMIT:_CNPJ:TEXT))+"-nf"+ALLTRIM(strzero(val(oxml:_NFE:_INFNFE:_IDE:_NNF:TEXT),9))+"-"+ALLTRIM(ALLTRIM(SUBSTR(oxml:_NFE:_INFNFE:_ID:TEXT,4,200)))+".xml.imp")
				__CopyFile("\xml\*.imp","\xml\importados\")
				ferase("\xml\"+ALLTRIM(alltrim(oxml:_NFE:_INFNFE:_EMIT:_CNPJ:TEXT))+"-nf"+ALLTRIM(strzero(val(oxml:_NFE:_INFNFE:_IDE:_NNF:TEXT),9))+"-"+ALLTRIM(ALLTRIM(SUBSTR(oxml:_NFE:_INFNFE:_ID:TEXT,4,200)))+".xml.imp")
				Return
			Endif
			If SUBSTR(alltrim(oxml:_NFE:_INFNFE:_DEST:_IE:TEXT),1,1) $ "0/1/2/3/4/5/6/7/8/9"
				_cCNPJ2:=alltrim(oxml:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
			Endif
			_cCNPJ:= alltrim(oxml:_NFE:_INFNFE:_EMIT:_CNPJ:TEXT)
			cEmissao:=oxml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT
			cEmissao:=SUBSTR(cEmissao,1,4)+SUBSTR(cEmissao,6,2)+SUBSTR(cEmissao,9,2)
			cNota:=oxml:_NFE:_INFNFE:_IDE:_NNF:TEXT
			If Empty(cSerieNF)
				cSerie:=oxml:_NFE:_INFNFE:_IDE:_SERIE:TEXT
			Endif
			cNatOp:=oxml:_NFE:_INFNFE:_IDE:_NATOP:TEXT
			cChave:=ALLTRIM(SUBSTR(oxml:_NFE:_INFNFE:_ID:TEXT,4,200))
			If ALLTRIM(TYPE("oxml:_NFE:_INFNFE:_EMIT:_ENDEREMIT:_FONE"))=="O"
				cFoneXML		:= alltrim(oxml:_NFE:_INFNFE:_EMIT:_ENDEREMIT:_FONE:TEXT)
			Else
				cFoneXML		:= ""
			Endif
			cNomeXML		:= UPPER(ALLTRIM(oxml:_NFE:_INFNFE:_EMIT:_XNOME:TEXT))
		
		//���������������������������������������������������������������������Ŀ
		//� Manipulando numero da nota fiscal									�
		//�����������������������������������������������������������������������
			If len(alltrim(cNota))<=6
				cNota:=strzero(val(cNota),6)
			Endif
			If cZeros
				cNota:=strzero(val(cNota),9)
			Endif
			nTam:=len(alltrim(cNota))
			cSpaco:=(9-nTam)
		
		//���������������������������������������������������������������������Ŀ
		//� Empresa atual														�
		//�����������������������������������������������������������������������
			If alltrim(_cCNPJ2)<>alltrim(SM0->M0_CGC)
				_cCNPJ:=''
			Endif
		Endif
	
	//���������������������������������������������������������������������Ŀ
	//� Verifico se a nota ja foi importada									�
	//�����������������������������������������������������������������������
		If !Empty(_cCNPJ) .and. lNotaDev=='4'
			DbSelectarea("SA1")
			DbSetorder(3)
			Dbgotop()
			Dbseek(xFilial("SA1")+_cCNPJ)
			If Found()
				dbSelectArea("SF1")
				DbSetorder(2)
				Dbgotop()
				Dbseek(xFilial("SF1")+SA1->A1_COD+SA1->A1_LOJA+alltrim(cNota)+Space(cSpaco))
				If Found() .and. SF1->F1_TIPO=="D"
				
					FRename("\xml\"+lower(ALLTRIM(aXML[i])),"\xml\"+ALLTRIM(_cCNPJ)+"-nf"+ALLTRIM(cNota)+"-"+ALLTRIM(cChave)+".xml.imp")
					__CopyFile("\xml\*.imp","\xml\importados\")
					ferase("\xml\"+ALLTRIM(_cCNPJ)+"-nf"+ALLTRIM(cNota)+"-"+ALLTRIM(cChave)+".xml.imp")
				
					_cCNPJ:=''
				Endif
			Endif
		Endif

	Else
		If lNotaDev=='4'
			FRename("\xml\"+lower(ALLTRIM(aXML[i])),"\xml\"+lower(ALLTRIM(aXML[i]))+".err")
			__CopyFile("\xml\*.err","\xml\corrompidos\")
			ferase("\xml\"+lower(ALLTRIM(aXML[i]))+".err")
		End
	Endif
Return

//��������������������������������������������������������������Ŀ
//| Pre nota entrada											 |
//����������������������������������������������������������������
Static Function PRENOTA()

	Local aCabec := {}
	Local aItens := {}
	Local aLinha := {}
	Private lMsErroAuto := .f.
	cRet:=.F.

	DbSelectarea("LS1")
	dbgotop()
	_dEmissao:=STOD(LS1->EMISSAO)

//��������������������������������������������������������������Ŀ
//| Grava status no fornecedor que manda o XML					 |
//����������������������������������������������������������������
	DbSelectarea("SA1")
	DbSetorder(1)
	Dbgotop()
	Dbseek(xFilial("SA1")+LS3->CLIENTE+LS3->LOJA)

	aadd(aCabec,{"F1_TIPO","D"})
	aadd(aCabec,{"F1_SERIE",cSerie})
	aadd(aCabec,{"F1_FORMUL","N"})
	aadd(aCabec,{"F1_DOC",(cNota)})
	aadd(aCabec,{"F1_EMISSAO",_dEmissao})
	aadd(aCabec,{"F1_FORNECE",LS3->CLIENTE})
	aadd(aCabec,{"F1_LOJA",LS3->LOJA})
	aadd(aCabec,{"F1_ESPECIE",cEspecie})
	aadd(aCabec,{"F1_CHVNFE",LS3->CHAVE})
	aadd(aCabec,{"F1_HORA",LEFT(TIME(),5)})
	aadd(aCabec,{"F1_MENNOTA", SUBSTRING(_cMensag,1,60)})

	DbSelectarea("LS1")
	DbSetorder(1)
	Dbgotop()
	While !Eof()
		_cPedido		:= LS1->PEDIDO
	
		_nPrecoU	:= LS1->PRECO
		_nTotalIt	:= LS1->TOTAL
	
		aLinha := {}
	
		aadd(aLinha,{"D1_COD"		, LS1->PRODUTO					, Nil})
		aadd(aLinha,{"D1_UM"			, LS1->UM							, Nil})
		aadd(aLinha,{"D1_QUANT"		, LS1->QUANTIDADE					, Nil})
		aadd(aLinha,{"D1_VUNIT"		, ROUND(_nPrecoU,8)				, Nil})
		aadd(aLinha,{"D1_TOTAL"		, ROUND(_nTotalIt,2)				, Nil})
		aadd(aLinha,{"D1_DOC"		, LS1->NOTA						, Nil})
		aadd(aLinha,{"D1_LOCAL"		, POSICIONE("SB1",1,xFilial("SB1")+LS1->PRODUTO,"B1_LOCPAD") , Nil})
	
		aadd(aItens,aLinha)
		DbSelectarea("LS1")
		Dbskip()
	End

	If Len(aCabec)>0 .and. Len(aItens)>0
		nOpc := 3
		MSExecAuto({|x,y,z| MATA140(x,y,z)}, aCabec, aItens, nOpc)
	Endif

	If lMsErroAuto
		MostraErro()
	Else
	//���������������������������������������������������������������������Ŀ
	//� Calculando o total do item na nota									�
	//�����������������������������������������������������������������������
		cQuere:=" UPDATE "+RetSqlName("SD1")+" SET D1_TOTAL=(D1_QUANT*D1_VUNIT) WHERE D1_FILIAL='"+xFilial("SD1")+"' AND D1_DOC='"+cNota+"' AND D1_FORNECE='"+LS3->CLIENTE+"' AND D1_LOJA='"+LS3->LOJA+"' AND D1_SERIE='"+cSerie+"' "
		TCSQLEXEC(cQuere)
		cRet:=.T.
	Endif
Return(cRet)


//���������������������������������������������������������������������Ŀ
//� Processando arquivo													�
//�����������������������������������������������������������������������
Static Function PROCESS()

	local i, w
	private _oXml    := NIL
	private cError    := ''
	private cWarning := ''

	If LS3->(eof())
		msgbox("N�o existem notas fiscais eletr�nicas para serem importadas!")
		OBRWI:obrowse:refresh()
		OBRWP:obrowse:refresh()
		OBRWI:obrowse:setfocus()
		OBRWP:obrowse:setfocus()
		ObjectMethod(oTela,"Refresh()")
		Return
	Endif

//���������������������������������������������������������������������Ŀ
//� Verifico se existe a nota fiscal									�
//�����������������������������������������������������������������������
	IF !file("\xml\"+lower(LS3->XML))
		msgbox("Este arquivo j� foi processado por outro usu�rio!","Aten��o...","ALERT")
		Reclock("LS3",.F.)
		dbdelete()
		MsUnlock()
	
		DbSelectarea("LS3")
		Dbgotop()
		PROCESS()
		Return
	Endif

//���������������������������������������������������������������������Ŀ
//� Verifico se foi alterado algum item									�
//�����������������������������������������������������������������������
	lAltera:=.F.
	DbSelectarea("LS1")
	Dbgotop()
	While !Eof()
		IF LS1->ALTERADO=="S"
			_cChave:=LS1->NOME+LS1->NOTA
			lAltera:=.T.
		Endif
		Dbskip()
	End

	IF lAltera
		cResp:=msgbox("Deseja perder todas as altera��es realizadas?","Aten��o...","YESNO")
	
		If cResp==.F.
			DbSelectarea("LS3")
			Dbsetorder(1)
			dbgotop()
			Dbseek(_cChave)
		
			DbSelectarea("LS1")
			Dbgotop()
			OBRWI:obrowse:refresh()
			OBRWP:obrowse:refresh()
			OBRWI:obrowse:setfocus()
			OBRWP:obrowse:setfocus()
			ObjectMethod(oTela,"Refresh()")
			Return
		Endif
	Endif

	nXmlStatus := XMLError()
	cFile:="\xml\"+lower(ALLTRIM(LS3->XML))

//���������������������������������������������������������������������Ŀ
//� Apagando dados da tabela temporaria									�
//�����������������������������������������������������������������������
	DbSelectarea("LS1")
	Dbsetorder(1)
	Dbgotop()
	While !Eof()
		Reclock("LS1",.F.)
		dbdelete()
		MsUnlock()
		Dbskip()
	End

//���������������������������������������������������������������������Ŀ
//� Apagando produtos temporarios										�
//�����������������������������������������������������������������������
	DbSelectarea("LS5")
	Dbsetorder(1)
	Dbgotop()
	While !Eof()
		Reclock("LS5",.F.)
		dbdelete()
		MsUnlock()
		Dbskip()
	End

	oXml := XmlParserFile(cFile,"_",@cError, @cWarning )
	aCols:={}
	nTotIt:=0
	nTotalNF:=0

	IF ALLTRIM(TYPE("oxml:_NFE:_INFNFE"))=="O"
		lTipo:=1
	Else
		lTipo:=2
	Endif

	nDescont:=0

	If ( nXmlStatus == XERROR_SUCCESS )
	
		If lTipo==2
			aCols:=aClone(oXml:_NFEPROC:_NFE:_INFNFE:_DET)
		Else
			aCols:=aClone(oXml:_NFE:_INFNFE:_DET)
		Endif
	
		If aCols==NIL
			nItens:=1
		Else
			nItens:=len(aCols)
		Endif
	
		For i:=1 to nItens
		//���������������������������������������������������������������������Ŀ
		//� Com _NFEPROC														�
		//�����������������������������������������������������������������������
			If lTipo==2
			
				If ALLTRIM(TYPE("oxml:_NFEPROC:_NFE:_INFNFE:_INFADIC:_INFCPL:TEXT"))=="C"
					_cMensag:=alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_INFADIC:_INFCPL:TEXT)
				Endif
			
				If nItens>1
					cCodbar :=oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_CEAN:TEXT
					cProdCli:=oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_CPROD:TEXT
					nQuant	:=val(oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_QCOM:TEXT)
					xDesc	:=oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_XPROD:TEXT
					If ALLTRIM(TYPE("oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_NCM:TEXT"))=="C"
						cNCM	:=oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_NCM:TEXT
					Endif
					nPreco	:=val(oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_VUNCOM:TEXT)
					If ALLTRIM(TYPE("oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_VDESC:TEXT"))=="C"
						nDescont:=val(oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_VDESC:TEXT)
					Endif
					nTotal	:=val(oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_VPROD:TEXT)
					cNota	:=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_NNF:TEXT
					If Empty(cSerieNF)
						cSerie  :=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_SERIE:TEXT
					Endif
					cNatOp  :=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_NATOP:TEXT
					cUM		:=oxml:_NFEPROC:_NFE:_INFNFE:_DET[i]:_PROD:_UCOM:TEXT
					cEmissao:=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT
					If SUBSTR(alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_IE:TEXT),1,1) $ "0/1/2/3/4/5/6/7/8/9"
						_cCNPJ	:=alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
						_cEmpresa:=alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
					Else
						_cCNPJ	:=alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_CPF:TEXT)
						_cEmpresa:=alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_CPF:TEXT)
					Endif
					cEmissao:=SUBSTR(cEmissao,1,4)+SUBSTR(cEmissao,6,2)+SUBSTR(cEmissao,9,2)
					cProd:=''
				Else
					cCodbar :=oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_CEAN:TEXT
					cProdCli:=oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_CPROD:TEXT
					nQuant	:=val(oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_QCOM:TEXT)
					If ALLTRIM(TYPE("oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_VDESC:TEXT"))=="C"
						nDescont:=val(oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_VDESC:TEXT)
					Endif
					xDesc	:=oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_XPROD:TEXT
					If ALLTRIM(TYPE("oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_NCM:TEXT"))=="C"
						cNCM	:=oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_NCM:TEXT
					Endif
					nPreco	:=val(oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_VUNCOM:TEXT)
					nTotal	:=val(oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_VPROD:TEXT)
					cNota	:=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_NNF:TEXT
					If Empty(cSerieNF)
						cSerie  :=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_SERIE:TEXT
					Endif
					cNatOP	:=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_NATOP:TEXT
					cEmissao:=oxml:_NFEPROC:_NFE:_INFNFE:_IDE:_DHEMI:TEXT
					cUM		:=oxml:_NFEPROC:_NFE:_INFNFE:_DET:_PROD:_UCOM:TEXT
					If SUBSTR(alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_IE:TEXT),1,1) $ "0/1/2/3/4/5/6/7/8/9"
						_cCNPJ	:=alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
						_cEmpresa:=alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
					Else
						_cCNPJ	:=alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_CPF:TEXT)
						_cEmpresa:=alltrim(oxml:_NFEPROC:_NFE:_INFNFE:_DEST:_CPF:TEXT)
					Endif
					cEmissao:=SUBSTR(cEmissao,1,4)+SUBSTR(cEmissao,6,2)+SUBSTR(cEmissao,9,2)
					cProd:=''
				Endif
			Else
			//���������������������������������������������������������������������Ŀ
			//� Sem _NFEPROC														�
			//�����������������������������������������������������������������������
				If ALLTRIM(TYPE("oxml:_NFE:_INFNFE:_INFADIC:_INFCPL:TEXT"))=="C"
					_cMensag:=alltrim(oxml:_NFE:_INFNFE:_INFADIC:_INFCPL:TEXT)
				Endif
			
				If nItens>1
					cCodbar :=oxml:_NFE:_INFNFE:_DET[i]:_PROD:_CEAN:TEXT
					cProdCli:=oxml:_NFE:_INFNFE:_DET[i]:_PROD:_CPROD:TEXT
					nQuant	:=val(oxml:_NFE:_INFNFE:_DET[i]:_PROD:_QCOM:TEXT)
					xDesc	:=oxml:_NFE:_INFNFE:_DET[i]:_PROD:_XPROD:TEXT
					If ALLTRIM(TYPE("oxml:_NFE:_INFNFE:_DET[i]:_PROD:_NCM:TEXT"))=="C"
						cNCM	:=oxml:_NFE:_INFNFE:_DET[i]:_PROD:_NCM:TEXT
					Endif
					cUM		:=oxml:_NFE:_INFNFE:_DET[i]:_PROD:_UCOM:TEXT
					If ALLTRIM(TYPE("oxml:_NFE:_INFNFE:_DET[i]:_PROD:_VDESC:TEXT"))=="C"
						nDescont:=val(oxml:_NFE:_INFNFE:_DET[i]:_PROD:_VDESC:TEXT)
					Endif
					nPreco	:=val(oxml:_NFE:_INFNFE:_DET[i]:_PROD:_VUNCOM:TEXT)
					nTotal	:=val(oxml:_NFE:_INFNFE:_DET[i]:_PROD:_VPROD:TEXT)
					cNota	:=oxml:_NFE:_INFNFE:_IDE:_NNF:TEXT
					If Empty(cSerieNF)
						cSerie  :=oxml:_NFE:_INFNFE:_IDE:_SERIE:TEXT
					Endif
					cNatOP	:=oxml:_NFE:_INFNFE:_IDE:_NATOP:TEXT
					cEmissao:=oxml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT
					If SUBSTR(alltrim(oxml:_NFE:_INFNFE:_DEST:_IE:TEXT),1,1) $ "0/1/2/3/4/5/6/7/8/9"
						_cCNPJ	:=alltrim(oxml:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
						_cEmpresa:=alltrim(oxml:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
					Else
						_cCNPJ	:=alltrim(oxml:_NFE:_INFNFE:_DEST:_CPF:TEXT)
						_cEmpresa:=alltrim(oxml:_NFE:_INFNFE:_DEST:_CPF:TEXT)
					Endif
					cEmissao:=SUBSTR(cEmissao,1,4)+SUBSTR(cEmissao,6,2)+SUBSTR(cEmissao,9,2)
					cProd:=''
				Else
					cCodbar :=oxml:_NFE:_INFNFE:_DET:_PROD:_CEAN:TEXT
					cProdCli:=oxml:_NFE:_INFNFE:_DET:_PROD:_CPROD:TEXT
					nQuant	:=val(oxml:_NFE:_INFNFE:_DET:_PROD:_QCOM:TEXT)
					xDesc	:=oxml:_NFE:_INFNFE:_DET:_PROD:_XPROD:TEXT
					If ALLTRIM(TYPE("oxml:_NFE:_INFNFE:_DET:_PROD:_NCM:TEXT"))=="C"
						cNCM	:=oxml:_NFE:_INFNFE:_DET:_PROD:_NCM:TEXT
					Endif
					If ALLTRIM(TYPE("oxml:_NFE:_INFNFE:_DET:_PROD:_VDESC:TEXT"))=="C"
						nDescont:=val(oxml:_NFE:_INFNFE:_DET:_PROD:_VDESC:TEXT)
					Endif
					cUM		:=oxml:_NFE:_INFNFE:_DET:_PROD:_UCOM:TEXT
					nPreco	:=val(oxml:_NFE:_INFNFE:_DET:_PROD:_VUNCOM:TEXT)
					nTotal	:=val(oxml:_NFE:_INFNFE:_DET:_PROD:_VPROD:TEXT)
					cNota	:=oxml:_NFE:_INFNFE:_IDE:_NNF:TEXT
					If Empty(cSerieNF)
						cSerie  :=oxml:_NFE:_INFNFE:_IDE:_SERIE:TEXT
					Endif
					cNatOP	:=oxml:_NFE:_INFNFE:_IDE:_NATOP:TEXT
					cEmissao:=oxml:_NFE:_INFNFE:_IDE:_DHEMI:TEXT
					If SUBSTR(alltrim(oxml:_NFE:_INFNFE:_DEST:_IE:TEXT),1,1) $ "0/1/2/3/4/5/6/7/8/9"
						_cCNPJ	:=alltrim(oxml:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
						_cEmpresa:=alltrim(oxml:_NFE:_INFNFE:_DEST:_CNPJ:TEXT)
					Else
						_cCNPJ	:=alltrim(oxml:_NFE:_INFNFE:_DEST:_CPF:TEXT)
						_cEmpresa:=alltrim(oxml:_NFE:_INFNFE:_DEST:_CPF:TEXT)
					Endif
					cEmissao:=SUBSTR(cEmissao,1,4)+SUBSTR(cEmissao,6,2)+SUBSTR(cEmissao,9,2)
					cProd:=''
				Endif
			Endif
		
		//���������������������������������������������������������������������Ŀ
		//� codigo barras														�
		//�����������������������������������������������������������������������
			If !Empty(cCodbar) .and. SUBSTR(cCodbar,1,8)<>"00000000"
				DbSelectarea("SB1")
				DbSetorder(5)
				Dbgotop()
				Dbseek(xFilial("SB1")+cCodbar,.t.)
				If Found() .and. SB1->B1_MSBLQL<>"1"
					cProd:=SB1->B1_COD
				Endif
			
				If Empty(cProd)
					DbSelectarea("SLK")
					DbSetorder(1)
					Dbgotop()
					Dbseek(xFilial("SLK")+cCodbar,.t.)
					If Found()
						cProd:=SLK->LK_CODIGO
					
					//���������������������������������������������������������������������Ŀ
					//� Verifico se esta bloqueado											�
					//�����������������������������������������������������������������������
						DbSelectarea("SB1")
						DbSetorder(1)
						Dbgotop()
						Dbseek(xFilial("SB1")+cProd,.t.)
						If Found() .and. SB1->B1_MSBLQL=="1"
							cProd:=''
						Endif
					Endif
				Endif
			Endif
		
			If len(alltrim(cProdCli))<=5
				cProdCli:=strzero(val(cProdCli),6)
			Endif
		
			If len(alltrim(cProdCli))>15
				cProdCli:=SUBSTR(cProdCli,1,15)
			Endif
			
			_nQE:=1
		
			If Empty(cProd) .or. Empty(cCodbar)
				cProd:="999999"
				_cDescricao:=xDesc
			Else
				_cDescricao:=POSICIONE("SB1",1,xFilial("SB1")+cProd,"B1_DESC")
				_nQE:=POSICIONE("SB1",1,xFilial("SB1")+cProd,"B1_QE")
			Endif
		
		//���������������������������������������������������������������������Ŀ
		//� Unidade de medidas unitarias										�
		//�����������������������������������������������������������������������
			IF cUM $ cUnidades
				_nQE:=1
			Endif
		
			If alltrim(cProd)<>"999999"
				DbSelectarea("LS5")
				DbSetorder(1)
				Dbgotop()
				Dbseek(cProd)
				if !Found()
					Reclock("LS5",.T.)
					LS5->PRODUTO:=cProd
					MsUnlock()
				Endif
			Endif
		
			_nPreco:=((nTotal-nDescont)/(nQuant*_nQE))
		
		//���������������������������������������������������������������������Ŀ
		//� Gravando produtos do XML											�
		//�����������������������������������������������������������������������
			_cCodCli:=''
			For w:=1 to len(alltrim(cProdCli))
				IF SUBSTR(UPPER(cProdCli),w,1) $ "A/B/C/D/E/F/G/H/I/J/K/L/M/N/O/P/Q/R/S/T/U/V/X/Z/W/Y/0/1/2/3/4/5/6/7/8/9"
					_cCodCli:=alltrim(_cCodCli)+SUBSTR(UPPER(cProdCli),w,1)
				Endif
			Next
			cProdCli:=_cCodCli
		
			Reclock("LS1",.T.)
			LS1->SEQ:=nTotIt
			LS1->CODBAR:=cCodbar
			LS1->PRODUTO:=cProd
			LS1->PRODCLI:=cProdCli
			LS1->DESCRICAO:=UPPER(_cDescricao)
			LS1->DESCORI:=UPPER(_cDescricao)
			LS1->QUANTIDADE:=(nQuant*_nQE)
			LS1->PRECO:=_nPreco
			//LS1->CUSTO:=_nCusto
			LS1->NCM:=IIF(LEN(ALLTRIM(cNCM))>=8,SUBSTR(cNCM,1,10),"")
			LS1->PRECOCLI:=(nTotal/(nQuant*_nQE))
			LS1->TOTAL:=(nTotal-nDescont)
			IF alltrim(cProd)=="999999"
				LS1->OK:="X"
			Else
				LS1->OK:="O"
			Endif
			LS1->EMISSAO:=cEmissao
			LS1->ALTERADO:="N"
			LS1->UM:=UPPER(cUM)
			LS1->NOTA:=LS3->NOTA
			LS1->NOME:=LS3->NOME
			LS1->QE:=_nQE
			LS1->CAIXAS:=nQuant
			MsUnlock()
			nTotIt:=nTotIt+1
			nTotalNF:=nTotalNF+(nTotal-nDescont)
		Next
	Else
		Msgbox("Problema ao abrir o arquivo!","Aten��o...","ALERT")
	Endif

	If nTotalNF==0
		Msgbox("Problema ao abrir o arquivo!","Aten��o...","ALERT")
	Endif

//���������������������������������������������������������������������Ŀ
//� Fornecedor															�
//�����������������������������������������������������������������������
	DbSelectarea("SA1")
	DbSetorder(1)
	Dbgotop()
	Dbseek(xFilial("SA1")+LS3->CLIENTE+LS3->LOJA)
	If Found()
		_cFornecedor:=SUBSTR(SA1->A1_NREDUZ,1,25)
		_cEnd:=ALLTRIM(SA1->A1_END)+" - "+SA1->A1_BAIRRO
		_cCidade:=ALLTRIM(SA1->A1_MUN)+"/"+SA1->A1_EST
		_cEmissao:=dtoc(stod(LS1->EMISSAO))
		_cCNPJ:=SA1->A1_CGC
		_cTelefone:=SA1->A1_TEL
	Endif

	If len(alltrim(cNota))<=5
		cNota:=strzero(val(cNota),6)
	Endif
	If cZeros
		cNota:=strzero(val(cNota),9)
	Endif

	DbSelectarea("LS3")
	DbSelectarea("LS1")
	Dbsetorder(1)
	Dbgotop()
	OBRWI:obrowse:refresh()
	OBRWP:obrowse:refresh()
	OBRWI:obrowse:setfocus()
	OBRWP:obrowse:setfocus()
	ObjectMethod(oTela,"Refresh()")
Return

//���������������������������������������������������������������������Ŀ
//� Seleciona produto													�
//�����������������������������������������������������������������������
Static Function SELECIONA(_cProduto,lOpcao)

	local w
//���������������������������������������������������������������������Ŀ
//� Verifico se o produto esta bloqueado para uso						�
//�����������������������������������������������������������������������
	Dbselectarea("SB1")
	DbSetorder(1)
	Dbgotop()
	Dbseek(xFilial("SB1")+_cProduto)
	If Found() .and. SB1->B1_MSBLQL=="1"
		msgbox("Produto bloqueado para uso!","Aten��o...","ALERT")
		return
	Endif

	_nQE:=LS1->QE
	cMemo:=''

	_nQuantPed:=0
	_nVlrPed:=0

	_nCaixas:=LS1->CAIXAS
	_nQuant:=(_nQE*_nCaixas)
	_nTotal:=LS1->TOTAL
	_nPreco:=(LS1->TOTAL/(_nQE*_nCaixas))
	_nQuantNF:=LS1->QUANTIDADE

	If _nCaixas==0
		_nQuant:=_nQuantNF
		_nPreco:=(_nTotal/_nQuant)
	Else
		_nQuant:=(_nQE*_nCaixas)
		_nPreco:=(_nTotal/_nQuant)
	Endif

//���������������������������������������������������������������������Ŀ
//� Tela de parametros													�
//�����������������������������������������������������������������������
	lGravou:=.F.

	cPict1:="@E 99,999."
	For w:=1 to cDecQtd
		cPict1:=alltrim(cPict1)+"9"
	Next

	cPict2:="@E 99,999."
	For w:=1 to cDecUni
		cPict2:=alltrim(cPict2)+"9"
	Next

	@ 120,040 TO 280,370 DIALOG oDef TITLE "Produto "+_cProduto
	@ 005,005 say "Qtd.Emb."  FONT oFont1 OF oDef PIXEL
	@ 015,005 get _nQE size 20,20 Picture "@E 9999" valid CALCULA()
	@ 005,040 say "Cx.Nota"  FONT oFont1 OF oDef PIXEL
	@ 015,040 get _nCaixas when .f. size 50,40 Picture cPict1
	@ 005,100 say "Quantidade"  FONT oFont1 OF oDef PIXEL COLOR CLR_GREEN
	@ 015,100 get _nQuant size 50,40 when .f. Picture cPict1

	@ 025,005 say "Pre�o R$"  FONT oFont1 OF oDef PIXEL COLOR CLR_GREEN
	@ 035,005 get _nPreco size 50,40 WHEN .F. Picture cPict2
	@ 025,055 say "Total R$"  FONT oFont1 OF oDef PIXEL
	@ 035,055 get _nTotal when .f. size 50,40 Picture "@E 99,999.99"

	@ 060,005 BUTTON "Confirmar" SIZE 50,10 ACTION GRAVANDO(lOpcao)
	@ 060,060 BUTTON "Sair" SIZE 50,10 ACTION oDef:end()
	ACTIVATE DIALOG oDef CENTER

	If lGravou .and. lOpcao==2
		oAmarra:end()
	Endif
Return


//���������������������������������������������������������������������Ŀ
//� Gravando produto...													�
//�����������������������������������������������������������������������
Static function GRAVANDO(lOpcao)

	lDifer:=.F.
	If _nQuantPed>0
		If _nQuant<>_nQuantPed
			Msgbox("Quantidade da Nota fiscal, diferente da quantidade do ultimo pedido!")
			lDifer:=.T.
		Endif
	
		If _nPreco>=(_nVlrPed+0.01)
			Msgbox("Pre�o unit�rio da Nota fiscal, diferente do pre�o do ultimo pedido!")
			lDifer:=.T.
		Endif
	
		If (_nPreco)>(_nVlrPed+1)
			Msgbox("Pre�o da nota fiscal, muito maior que do ultimo pedido!")
			lDifer:=.T.
		Endif
	Endif

	If lDifer
		cResp:=Msgbox("Deseja gravar o produto mesmo assim?","Aten��o...","YESNO")
		If cResp==.F.
			Return
		Endif
	Endif

//���������������������������������������������������������������������Ŀ
//� Verifico se o codigo de barras existe								�
//�����������������������������������������������������������������������
	If lOpcao==2
		_cProduto:=LS4->PRODUTO
	Else
		_cProduto:=LS1->PRODUTO
	Endif

	_cCodBarras:=''
	DbSelectarea("SB1")
	DbSetorder(1)
	Dbgotop()
	Dbseek(xFilial("SB1")+_cProduto)
	If Found()
		_cCodBarras:=SB1->B1_CODBAR
	Endif

	_nPrecoA:=(LS1->TOTAL/_nQuant)

	If lOpcao==2
		DbSelectarea("LS1")
		Reclock("LS1",.F.)
		LS1->OK:="O"
		LS1->PRODUTO:=_cProduto
		LS1->DESCRICAO:=LS4->DESCRICAO
		LS1->ALTERADO:="S"
		LS1->CAIXAS:=_nCaixas
		LS1->QUANTIDADE:=_nQuant
		LS1->PRECOCLI:=_nPreco
		LS1->PRECO:=(LS1->TOTAL/_nQuant)
		LS1->QE:=_nQE
		MsUnlock()
	
		DbSelectarea("LS5")
		Reclock("LS5",.T.)
		LS5->PRODUTO:=_cProduto
		MsUnlock()
	Else
		DbSelectarea("LS1")
		Reclock("LS1",.F.)
		LS1->ALTERADO:="S"
		LS1->OK:="O"
		LS1->CAIXAS:=_nCaixas
		LS1->QUANTIDADE:=_nQuant
		LS1->PRECOCLI:=_nPreco
		LS1->PRECO:=(LS1->TOTAL/_nQuant)
		LS1->QE:=_nQE
		MsUnlock()
	Endif

	OBRWI:obrowse:refresh()
	OBRWP:obrowse:refresh()
	ObjectMethod(oTela,"Refresh()")

	DbSelectarea("LS1")
	DbSetorder(1)
	Dbgotop()
	Dbseek(nSeek)

	lGravou:=.T.
	oDef:end()
Return

//���������������������������������������������������������������������Ŀ
//� Filtrar produtos													�
//�����������������������������������������������������������������������
Static Function FILTRE()

	Local w
	If Len(alltrim(_cFiltrox))>2
		Dbselectarea("LS4")
		Dbgotop()

		While !Eof()
			Reclock("LS4",.F.)
			dbdelete()
			MsUnlock()
			Dbskip()
		End
	
	//���������������������������������������������������������������
	//�Verifica se a pesquisa do produto foi sub-dividida			�
	//���������������������������������������������������������������
		_cDesc1:=''
		_cDesc2:=''
		_cDesc3:=''
	
		For w:=1 to len(alltrim(_cFiltrox))
			If SUBSTR(ALLTRIM(_cFiltrox),w,1) $ ";/,"
				cont2:=(w-1)
				_cDesc1:=SUBSTR(_cFiltrox,1,cont2)
				w:=100
			Endif
		Next
	
		If !Empty(_cDesc1)
			_nInicio:=(cont2+2)
			_cString:=SUBSTR(ALLTRIM(_cFiltrox),_nInicio,50)
			If !empty(_cString)
				For w:=1 to len(alltrim(_cString))
					If SUBSTR(ALLTRIM(_cString),w,1) $ ";/,"
						cont2:=(w-1)
						_cDesc2:=SUBSTR(_cString,1,cont2)
						w:=100
					Endif
				Next
			
				If Empty(_cDesc2)
					_cDesc2:=alltrim(_cString)
				Endif
			Endif
		Endif
	
		If !Empty(_cDesc2)
			_nInicio:=(cont2+2)
			_cString2:=SUBSTR(ALLTRIM(_cString),_nInicio,50)
			If !empty(_cString2)
				For w:=1 to len(alltrim(_cString2))
					If SUBSTR(ALLTRIM(_cString2),w,1) $ ";/,"
						cont2:=(w-1)
						_cDesc3:=SUBSTR(_cString2,1,cont2)
						w:=100
					Endif
				Next
			
				If Empty(_cDesc3)
					_cDesc3:=alltrim(_cString2)
				Endif
			Endif
		Endif
	
		If Empty(_cDesc1)
			_cDescp1:="%"+alltrim(_cFiltrox)+"%"
		Else
			_cDescp1:="%"+alltrim(_cDesc1)+"%"
			_cDescp2:="%"+alltrim(_cDesc2)+"%"
			_cDescp3:="%"+alltrim(_cDesc3)+"%"
		Endif
	
		cQuery:=" SELECT B1_MSBLQL BLQ,B1_CODBAR CODBAR,B1_COD PRODUTO,B1_DESC DESCRICAO FROM "+RetSqlName("SB1")+" "
		cQuery:=cQuery + " 	WHERE B1_FILIAL='"+xFilial("SB1")+"' AND (B1_DESC LIKE '"+_cDescp1+"' OR B1_COD LIKE'"+_cDescp1+"') "
		IF !Empty(_cDesc2)
			cQuery:=cQuery + " AND B1_DESC LIKE '"+_cDescp2+"' "
		Endif
		IF !Empty(_cDesc3)
			cQuery:=cQuery + " AND B1_DESC LIKE '"+_cDescp3+"' "
		Endif
		cQuery:=cQuery + " AND D_E_L_E_T_<>'*' "
		TCQUERY cQuery NEW ALIAS "TCQ"
		DbSelectarea("TCQ")
		
		While !Eof()
			lPedido:="N�o"
			
			DbSelectarea("SB1")
			DbSetorder(1)
			Dbgotop()
			Dbseek(xFilial("SB1")+TCQ->PRODUTO)
			
			If Empty(cAlmox)
				cAlmox:=SB1->B1_LOCPAD
			Endif
			
			Reclock("LS4",.T.)
			LS4->PRODUTO:=TCQ->PRODUTO
			IF "SAIU" $ TCQ->DESCRICAO
				LS4->DESCRICAO:=SUBSTR(TCQ->DESCRICAO,6,45)
			Else
				LS4->DESCRICAO:=TCQ->DESCRICAO
			Endif
			nQE := 0//TRANSFORM(SB1->B1_QE, "@E 9999.99")
			
			LS4->SALDO:=POSICIONE("SB2",2,xFilial("SB2")+cAlmox+TCQ->PRODUTO,"B2_QATU-B2_RESERVA-B2_QEMP")
			LS4->QE :=  0  //val(nQE) //ROUND(SB1->B1_QE,2)
			LS4->BLQ:=IIF(TCQ->BLQ=="1","Bloq.","Ativo")
			MsUnlock()

			DbSelectarea("TCQ")
			Dbskip()
		End
		DbClosearea()
	
		Dbselectarea("LS4")
		Dbgotop()
	Endif
Return

//���������������������������������������������������������������������Ŀ
//� Calcula produto														�
//�����������������������������������������������������������������������
Static Function CALCULA()

	If _nCaixas==0
		_nQuant:=_nQuantNF
		_nPreco:=(_nTotal/_nQuant)
	Else
		_nQuant:=(_nQE*_nCaixas)
		_nPreco:=(_nTotal/_nQuant)
	Endif
Return

//���������������������������������������������������������������������Ŀ
//� Refazer Nota fiscal													�
//�����������������������������������������������������������������������
Static Function REFAZER()

	cResp:=msgbox("Deseja refazer toda a nota fiscal?","Aten��o...","YESNO")

	If cResp
		PROCESS()
	Endif
Return

//���������������������������������������������������������������������Ŀ
//� Excluir amarracao													�
//�����������������������������������������������������������������������
Static Function EXCAMA()

	If Empty(LS1->OK) .OR. LS1->OK=="O"
		cResp:=msgbox("Deseja excluir a amarra��o do produto?","Aten��o...","YESNO")
	
		If cResp
		
		//���������������������������������������������������������������������Ŀ
		//� Excluindo da tabela de produtos identificados						�
		//�����������������������������������������������������������������������
			DbSelectarea("LS5")
			DbSetorder(1)
			Dbgotop()
			Dbseek(LS1->PRODUTO)
			If Found()
				Reclock("LS5",.F.)
				dbdelete()
				MsUnlock()
			Endif
		
			Reclock("LS1",.F.)
			LS1->DESCRICAO:=LS1->DESCORI
			LS1->PRODUTO:="999999"
			LS1->OK:="X"
			MsUnlock()
		Endif
	Else
		Msgbox("N�o existe amarra��o para este produto!")
	Endif
Return

//���������������������������������������������������������������������Ŀ
//� Mensagem nota fiscal												�
//�����������������������������������������������������������������������
Static Function MSGNF(_cMensag)

	If !Empty(_cMensag)
		DEFINE MSDIALOG oMensNF FROM 0,0 TO 290,415 PIXEL TITLE "Mensagem da Nota Fiscal"
		@ 005,005 GET oMemo VAR _cMensag MEMO SIZE 200,135 FONT oFont2 PIXEL OF oMensNF
		ACTIVATE MSDIALOG oMensNF CENTER
	Endif
Return

//���������������������������������������������������������������������Ŀ
//� Funcao Legenda														�
//�����������������������������������������������������������������������
Static Function LEGENDA()
	_cLegenda := "Legenda dos produtos"

	aCorLegen := { 	{ 'BR_VERDE'   ,"Produto OK!" 		},;
		{ 'BR_VERMELHO',"Sem identifica��o" }}
	BrwLegenda(_cLegenda,"Status do Produto",aCorLegen)
Return

//���������������������������������������������������������������������Ŀ
//� Funcao Consulta SEFAZ												�
//�����������������������������������������������������������������������
Static Function SEFAZ()
//cChave:="http://www.nfe.fazenda.gov.br/portal/formulariodepesquisa.aspx?tipoconsulta=completa&chaveacesso="+LS3->CHAVE
	cChave:="http://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=&nfe="+LS3->CHAVE
	ShellExecute("open",cChave,"","",0)
Return

//���������������������������������������������������������������������Ŀ
//� Codigo de barra														�
//�����������������������������������������������������������������������
Static Function CODBAR()

	cCodbar:=''

	If !Empty(LS1->CODBAR)
		cCodbar:=alltrim(cCodbar)+"C�DIGO BARRA DO XML"+chr(13)+chr(10)
		cCodbar:=alltrim(cCodbar)+alltrim(LS1->CODBAR)+chr(13)+chr(10)
		cCodbar:=alltrim(cCodbar)+chr(13)+chr(10)
	Endif

	If ALLTRIM(LS1->PRODUTO)<>"999999"
		DbSelectarea("SB1")
		DbSetorder(1)
		Dbgotop()
		Dbseek(xFilial("SB1")+LS1->PRODUTO,.t.)
		If Found()
			cCodbar:=alltrim(cCodbar)+"CADASTRO DE PRODUTO"+chr(13)+chr(10)
			cCodbar:=alltrim(cCodbar)+alltrim(SB1->B1_CODBAR)+chr(13)+chr(10)
			cCodbar:=alltrim(cCodbar)+chr(13)+chr(10)
		Endif
	
		DbSelectarea("SLK")
		DbSetorder(2)
		Dbgotop()
		Dbseek(xFilial("SLK")+LS1->PRODUTO,.t.)
		If Found()
			cCodbar:=alltrim(cCodbar)+"CADASTRO ALTERNATIVO"+chr(13)+chr(10)
			DbSelectarea("SLK")
			DbSetorder(2)
			Dbgotop()
			Dbseek(xFilial("SLK")+LS1->PRODUTO,.t.)
			While !Eof() .and. ALLTRIM(SLK->LK_CODIGO)==ALLTRIM(LS1->PRODUTO)
				cCodbar:=alltrim(cCodbar)+alltrim(SLK->LK_CODBAR)+chr(13)+chr(10)
				Dbskip()
			End
		Endif
	Endif

	If !Empty(cCodbar)
		Msgbox(cCodbar,"Aten��o...","INFO")
	Endif
Return

//���������������������������������������������������������������������Ŀ
//� Desbloquear															�
//�����������������������������������������������������������������������
Static Function DESBLOQ()

	If UPPER(ALLTRIM(LS4->BLQ))=="ATIVO"
		Msgbox("O Produto j� est� ativo!","Aten��o...","ALERT")
		Return
	Endif

	cResp:=msgbox("Deseja DESBLOQUEAR o produto novamente?","Aten��o...","YESNO")

	If cResp
		DbSelectarea("LS4")
		Reclock("LS4",.F.)
		LS4->BLQ:="Ativo"
		MsUnlock()
	
		Dbselectarea("SB1")
		DbSetorder(1)
		Dbgotop()
		Dbseek(xFilial("SB1")+LS4->PRODUTO)
		If Found()
			Reclock("SB1",.F.)
			SB1->B1_MSBLQL:="2"
			IF "SAIU" $ ALLTRIM(SB1->B1_DESC)
				SB1->B1_DESC:=SUBSTR(SB1->B1_DESC,6,45)
			Endif
			MsUnlock()
		End
		msgbox("O Produto foi reativado com sucesso!","Aten��o...","INFO")
	Endif
Return
