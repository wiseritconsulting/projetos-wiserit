#include "rwmake.ch"        

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA003  �Autor  �Bruno Parreira      � Data �  23/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     � Esta rotina atualiza os dados da expedicao da nota fiscal  ���
���          � ex: peso liquido, volume, transportadora..                 ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico HOPE - www.actualtrend.com.br                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HCOMP004()

cOpcao:="ALTERAR"
nOpcE:=2
nOpcG:=3
cCadastro := "Saida de Produtos na Expedicao"

aRotina := {	{ "Pesquisa"		,"AxPesqui"			,0, 1},;
				{ "Visualiza"		,'U_HCOMP4E(.F.)'	,0, 2},;
				{ "Expedi��o NF"	,'U_HCOMP4E(.T.)'	,0, 4}}

MBrowse(01,01,30,70,"SF1")

Return


/*
//���������������������������������������������������������������������������Ŀ
//�Rotina utilizada para alterar os dados da expedicao da nota fiscal de saida�
//�����������������������������������������������������������������������������
*/

User Function HCOMP4E(lExp)
				
Local   _aAlias        		:= Alias()
Private _nFiscal         	:= SF1->F1_DOC
Private _cTransportadora 	:= SF1->F1_TRANSP
Private _cData           	:= SF1->F1_EMISSAO
Private d_dtExp				:= DDATABASE
Private c_horas				:= SF1->F1_HORA
Private _xTamdt          	:= PesqPict("SF1","F1_EMISSAO",8)
Private nPesoBruto			:= SF1->F1_PBRUTO
Private nPesoLiq				:= SF1->F1_PLIQUI
Private cVolume				:= SF1->F1_VOLUME1
Private cCombo 				:= SF1->F1_ESPECI1
Private aCombo				:= {"CAIXA","PALLET","SACO","UNIDADE","CAIXA PAPELAO","BAU METAL","ROLOS"}
Private mmsgfis				:= FwCutOff(AllTrim(SF1->F1_MENNOTA), .T.)

DbSelectarea("SF1")

//���������������������������������������������������������������������������Ŀ
//� Criacao da Interface                                                      �
//�����������������������������������������������������������������������������

@ 156,367 To 700,934 Dialog mkwdlg Title OemToAnsi("Expedi��o de Nota Fiscal e Mensagem para Nota")
n_linha	:= 015
n_latu		:= 013

@ n_latu  ,018	Say OemToAnsi("Nota Fiscal:") Size 100,12
@ n_latu  ,063	say SF1->F1_DOC Size 100,12
n_latu +=  n_linha

@ n_latu  ,018	Say OemToAnsi("Data de Sa�da:") Size 100,12
@ n_latu+8,018	Get d_dtExp Size 100,12
@ n_latu  ,167	Say OemToAnsi("Hora de Sa�da:") Size 100,12
@ n_latu+8,167	Get c_horas Size 100,12
n_latu +=  n_linha+010

@ n_latu  ,018	Say OemToAnsi("Transportadora:") Size 100,12
@ n_latu+8,018	Get _cTransportadora pict "@!" F3 "SA4" Size 100,12
n_latu +=  n_linha+010
@ n_latu  ,018	Say OemToAnsi("Peso Bruto:") Size 100,12
@ n_latu+8,018	Get nPesoBruto Picture "999999.9999" Size 100,12
@ n_latu  ,167	Say OemToAnsi("Peso Liquido:") Size 100,12
@ n_latu+8,167	Get nPesoLiq  Picture "999999.9999" Size 100,12
n_latu +=  n_linha+010

@ n_latu  ,018	Say OemToAnsi("Volume:") Size 100,12
@ n_latu+8,018	Get cVolume Picture "999999" Size 100,12
@ n_latu  ,167	Say OemToAnsi("Especie:") Size 100,12
@ n_latu+8,167	ComboBox cCombo Items aCombo Size 100,12
n_latu +=  n_linha+010

@ n_latu  ,018	Say OemToAnsi("Mensagem complementar para Nota:") Size 100,12
@ n_latu+8,018	Get mmsgfis MEMO Size 249,80

If	lExp
	@ 249,18	Button OemToAnsi("_Salvar") Size 36,16 Action SALVAR() //VALID lExp
End
@ 249,65	Button OemToAnsi("_Cancelar") Size 36,16 Action CLOSE(mkwdlg)

Activate Dialog mkwdlg CENTERED 

DbSelectArea(_aAlias)

Return()

                
/*
//���������������������������������������������������������������������������Ŀ
//�Atualiza dados de expedicao da NF.                                         �
//�����������������������������������������������������������������������������
*/

Static Function SALVAR()

Local _Alias          := Alias()

DbSelectarea("SF1")

RecLock("SF1",.F.)
	SF1->F1_TRANSP 		:= _cTransportadora
	SF1->F1_EMISSAO 		:= _cData
	SF1->F1_PBRUTO		:= nPesoBruto		
	SF1->F1_PLIQUI		:= nPesoLiq		
	SF1->F1_VOLUME1		:= cVolume
	SF1->F1_ESPECI1		:= cCombo 
	SF1->F1_HORA			:= c_horas
	SF1->F1_MENNOTA		:= mmsgfis
MSUNLOCK()

DbSelectArea(_Alias)

CLOSE(mkwdlg)

Return()