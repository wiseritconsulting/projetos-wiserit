#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
#include "tbiconn.ch"
#include "ap5mail.ch"
#include "xmlxfun.ch"

#Define CRLF  CHR(13)+CHR(10)

/*
������������������������������������������������������������������������������������������������
������������������������������������������������������������������������������������������������
��������������������������������������������������������������������������������������������ͻ��
���Programa  � HCOMJ001 �Autor  � Mauro Nagata                          � Data �   12/08/2017���
��������������������������������������������������������������������������������������������͹��
���          � Importa��o do XML CTe                                                         ���
���          �                                                                               ���
���          �                                                                               ���
��������������������������������������������������������������������������������������������͹��
���Uso       � Hope - Actual Trend                                                           ���
��������������������������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������������������������
������������������������������������������������������������������������������������������������
*/
User Function HCOMJ001(pMetodo)

	Local x
	Local nArq
	Local aRecnoSM0 	:= {}
	Local lJobCte       := .T. 				//SuperGetMV("HO_CTEJOB", .F.,.T.)						//permite job da importacao do XML CTe
    Local cPerg         := "HIMPXMLCTE"
	Private lManual     := pMetodo != NIL
	Private cPastaPen := "\interface\NF_Cte\"
//	Private cPastaPen  	:= "\interface\Validacao\"
	Private aLog 		:= {}
	Private nErro    	:= 0
	Private cLog		:= ""
	Private cTime    	:= ""
	Private cDtTime  	:= ""
	//msgstop("TESTE MARCILIO")
    //AjustaSX1(cPerg)
//	If !Pergunte(cPerg,.T.)
 //      Return()
  //  Endif   
  //  dDataBase := mv_par01
//daniel em 23/05/2018
//	If !lJobCTe
//		If lManual
//			MsgAlert("HO_CTeJOB est� definida para n�o executar o job da importacao do XML CTe")
//		EndIf
//		Return(.F.)
//	EndIf

	lOpen := .F.
    lOpen := .t.
//	If ( lOpen := MyOpenSm0(.T.) )

		DbSelectArea( "SM0" )

//		Do While !SM0->( EOF() )
		// So adiciona no aRecnoSM0 se a empresa for diferente
//			If aScan( aRecnoSM0, { |x| x[2] == SM0->M0_CODIGO .And. x[3] == SM0->M0_CODFIL } ) == 0 .And. !Empty(SM0->M0_CGC)
				aAdd( aRecnoSM0, { SM0->(Recno()), SM0->M0_CODIGO, SM0->M0_CODFIL } )
//			EndIf
//			SM0->( DbSkip() )      
//		EndDo
//		SM0->( DbCloseArea() )

		If lOpen
	
			aDir := Directory(cPastaPen+"*.XML")
			aArqs := {}
			For x:=1 To Len(aDir)
				aAdd(aArqs, { cPastaPen, aDir[x][1] })
			Next x
			For nIEmp := 1 To Len( aRecnoSM0 )
			
	//			If !lManual
	//				RpcSetType( 3 )
	//				RpcSetEnv( aRecnoSM0[nIEmp,2], Alltrim(aRecnoSM0[nIEmp,3]) 	,,,"COM" )
					//RpcSetEnv( aRecnoSM0[nIEmp,2], aRecnoSM0[nIEmp,3] )
	//			EndIf
			
				For nArq:=1 To Len(aArqs)
					Conout("Processando Cte - " + aArqs[nArq,2] + " - " + Time() + " - " + DtoC(Date()) + " - Empresa: " + aRecnoSM0[nIEmp,2] + " Filial: " + AllTrim(aRecnoSM0[nIEmp,3]) + " Aguarde...")
					U_IMPCTE(aArqs[nArq,1]+aArqs[nArq,2])
				Next
		
	//			If !lManual
	//				RpcClearEnv()
	//			EndIf
			Next nIEmp

		EndIf

	//EndIf

Return Nil


/*
������������������������������������������������������������������������������������������������
������������������������������������������������������������������������������������������������
��������������������������������������������������������������������������������������������ͻ��
���Programa  � ImpXmlCte�Autor  � Mauro Nagata                          � Data �   12/08/2017���
��������������������������������������������������������������������������������������������͹��
���          � Importa��o do XML CTe                                                         ���
���          �                                                                               ���
���          �                                                                               ���
��������������������������������������������������������������������������������������������͹��
���Uso       � Hope - Actual Trend                                                           ���
��������������������������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������������������������
������������������������������������������������������������������������������������������������
*/
User Function IMPCTE(cArquivo)

	Local nI
	Local cLogErro := ""
	Local cCnpjReme
	Local cCnpjDest
	Local cTomador
	Local cTes
	Local cAno
	Local cMes
	Local cDirSucesso
	Local cDirErro
	Local cDirLogErro
	Local cDirNaoToma
	Local cDirErroArqv
	Local cArquivo
	Local lProc
	Local cAviso
	Local cErro
	Local cXmlTab1
	Local cLogErro
	Private cTime    	:= ""
	Private cDtTime  	:= ""
	dDataBase := date()
	cAno  := StrZero(Year(dDataBase),4)
	cMes  := StrZero(Month(dDataBase),2)
	
	cTime   	:= Time()
	cDtTime 	:= DTOS(date()) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

	cDirSucesso		:= 'Proc_JOB\'
	cDirErro 	 	:= 'Erro_JOB\'
	cDirLogErro	 	:= 'Erro_JOB_Log\'
	cDirNaoToma	 	:= 'Erro_NaoTomador\'
	cDirErroArqv 	:= 'Erro_Arquivo\'
	
	If !File(cArquivo)
		Conout("Arquivo " + cArquivo + " N�o Localizado ou J� Processado !")
		Return .T.
	Endif
	lProc := .T.
	
	//Converte String XML em Objeto XML
	If lProc
	
		cAviso   := ""
		cErro    := ""
		oXmlTab1 := Nil
		oXmlTab1 := XmlParserFile( cArquivo ,"_" , @cAviso,@cErro )
		Do Case
		Case !Empty(cErro)
			Conout("Erro Na Leitura do Arquivo " + cArquivo + ' Erro: '+cErro)
			cLogErro 	:= "Erro Na Leitura do Arquivo " + cArquivo + ' Erro: '+cErro
			lProc 		:= .F.
			
		Case !Empty(cAviso)
			Conout("Erro Na Leitura do Arquivo " + cArquivo + ' Aviso: '+cAviso)
			cLogErro	:= "Erro Na Leitura do Arquivo " + cArquivo + ' Aviso: '+cAviso
			lProc 		:= .F.
			
		Case Empty(oXmlTab1)
			//Alert("Erro Na Leitura do Arquivo " + cArquivo + ' Objeto Xml Vazio ')
			//Conout("Erro Na Leitura do Arquivo " + cArquivo + ' Objeto Xml Vazio ')
			cLogErro	:= "Erro Na Leitura do Arquivo " + cArquivo + ' Objeto Xml Vazio '
			lProc 		:= .F.

		OtherWise

			//Prepara Objeto pra receber XML
			oCTe := oXmlTab1
			If Type("oCTE:_CTE") <> "U"
				oCT := oCTE:_CTE:_INFCTE
			ElseIf Type("oCTE:_CTEPROC") <> "U"
				oCT  := oCTE:_CTEPROC:_CTE
				oCTE := oCTE:_CTEPROC
			Else
				cDirDest := Upper(AllTrim(cPastaPen)+cDirErroArqv+cAno+"\"+cMes+"\")
				xFile 	 := StrTran(Upper(cArquivo),Upper(AllTrim(cPastaPen)),cDirDest)
				lProc    := .F.
	
				//Caso nao exista, cria o diretorio no servidor
				MontaDir(cDirDest)

				If !File(xFile)
					Copy File &cArquivo To &xFile
				EndIf

				lDelOk := .F.
				Do While !lDelOk
					fErase(cArquivo)
					lDelOk := !File(cArquivo)
					If !lDelOk
						Conout("O XML est� aberto, feche o mesmo para que ele seja movido de pasta!")
					EndIf
				EndDo
				
				Return
				
			Endif

			cTomador	:= ""
			cCNPJ  	 := ""
			cDoc   	 := ""
			cSerie 	 := ""
			_Mod   	 := "CTE"
			_cData 	 := ""
			_dData 	 := ""
			
			_cData := SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,9,2)+SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,6,2)+SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,1,4)
			_dData := ALLTRIM(SUBSTR(_cData,1,2)+'/'+Substr(_cData,3,2)+'/'+SUBSTR(_cData,5,4))

			//tomador
			//0=remetente
			//1=expedidor
			//2=recebedor
			//3=destinatario
			
			If Type("oCTE:_CTE:_INFCTE:_IDE:_TOMA3:_TOMA:TEXT") <> "U"
				cTomador := oCTE:_CTE:_INFCTE:_IDE:_TOMA3:_TOMA:TEXT
			ElseIf Type("oCTE:_CTE:_INFCTE:_IDE:_TOMA03:_TOMA:TEXT") <> "U"
				cTomador := oCTE:_CTE:_INFCTE:_IDE:_TOMA03:_TOMA:TEXT
			ElseIf Type("oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_TOMA:TEXT") <> "U"
				cTomador := oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_TOMA:TEXT
			Endif
					
			cCNPJDest 		:= ""
			cCNPJReme 		:= ""
			cCnpjTomador	:= ""
	
			//// Testa a onde deve buscar o CNPJ
			//CNPJ Destinatario
			If Type("oCTE:_CTE:_INFCTE:_DEST:_CNPJ:TEXT") <> "U"
				cCNPJDest := oCTE:_CTE:_INFCTE:_DEST:_CNPJ:TEXT
			Endif
			
			//CNPJ Remetente
			If Type("oCTE:_CTE:_INFCTE:_REM:_CNPJ:TEXT") <> "U"
				cCNPJReme := oCTE:_CTE:_INFCTE:_REM:_CNPJ:TEXT
			Endif
			
			//CPF Destinatario
			If Type("oCTE:_CTE:_INFCTE:_DEST:_CPF:TEXT") <> "U"
				cCnpjDest := oCTE:_CTE:_INFCTE:_DEST:_CPF:TEXT
			Endif
			
			//CPF Remetente
			If Type("oCTE:_CTE:_INFCTE:_REM:_CPF:TEXT") <> "U"
				cCnpjReme := oCTE:_CTE:_INFCTE:_REM:_CPF:TEXT
			Endif
			
			//CNPJ tomado4
			If Type("oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_CNPJ:TEXT") <> "U"
				If  oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_CNPJ:TEXT == cCNPJReme   // oCTE:_CTE:_INFCTE:_REM:_CNPJ:TEXT
					cCnpjReme := oCTE:_CTE:_INFCTE:_REM:_CNPJ:TEXT
				ElseIf oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_CNPJ:TEXT == cCnpjDest  // oCTE:_CTE:_INFCTE:_DEST:_CNPJ:TEXT
					cCnpjDest := oCTE:_CTE:_INFCTE:_DEST:_CNPJ:TEXT
				End
			Endif
			
			//CNPJ Expedidor
			If Type("oCTE:_CTE:_INFCTE:_EXPED:_CNPJ:TEXT") <> "U"
				cCnpjExped := oCTE:_CTE:_INFCTE:_EXPED:_CNPJ:TEXT
			EndIf
			
			//CNPJ Recebedor
			If Type("oCTE:_CTE:_INFCTE:_RECEB:_CNPJ:TEXT") <> "U"
				cCnpjReceb := oCTE:_CTE:_INFCTE:_RECEB:_CNPJ:TEXT
			EndIf
			
			Do Case
			Case cTomador = "0"
				cCnpjTomador := cCnpjReme
			Case cTomador = "1"
				cCnpjTomador := cCnpjExped
			Case cTomador = "2"
				cCnpjTomador := cCnpjReceb
			Case cTomador = "3"
				cCnpjTomador := cCnpjDest
			Case cTomador = "4"
				If oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_CNPJ:TEXT == cCnpjReme
					cCnpjTomador := cCnpjReme
				ElseIf oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_CNPJ:TEXT == cCnpjDest
					cCnpjTomador := cCnpjDest
				ElseIf SubStr(oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_CNPJ:TEXT,1,8) == SubStr(SM0->M0_CGC,1,8)
					cCnpjTomador := oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_CNPJ:TEXT
				End
			EndCase
			
			DbSelectArea("SA1")
			DbSetOrder(3)
			DbSeek(xfilial("SA1")+cCnpjDest)
			
			DbSelectArea("SED")
			DbSetOrder(1)
			DbSeek(xfilial("SED")+SA1->A1_NATUREZ)
			
			_cc := SED->ED_CCD
     		If SM0->M0_ESTCOB = 'SP'
     		   lProc := .T.
     		Else   
        		//Hope � o tomador do servi�o, processa CT-e
	    		//lProc := (SubStr(cCnpjTomador,1,8) = '03007414') .and. !(cTomador $ '3')
		    	If (SubStr(cCnpjTomador,1,8) = '03007414') //.and. (cTomador $ '3')
			    	If SM0->M0_CGC == cCnpjTomador
				    	lProc := .T.
				    Else
					   Return
				    End
			    Else
				    lProc := .F.
			    EndIf
     		Endif
     		//CNPJ Emitente
			cCNPJ  := oCTE:_CTE:_INFCTE:_EMIT:_CNPJ:TEXT
			
			//dados da nota fiscal
			cDoc   := PadL(ALLTRIM(oCTE:_CTE:_INFCTE:_IDE:_NCT:TEXT),9,'0')
			cSerie := PadR(oCTE:_CTE:_INFCTE:_IDE:_SERIE:TEXT,3)
			
			_cData := Substr(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,9,2)+SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,6,2)+SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,1,4)
			_dData := AllTrim(Substr(_cData,1,2)+'/'+Substr(_cData,3,2)+'/'+Substr(_cData,5,4))
			
			//Verifica se o CNPJ existe
			SA2->(DbSetOrder(3))
			lProcF := SA2->(DbSeek(xFilial("SA2")+cCNPJ))
			if 	lProcF 
		//	   msgStop('Achei o Cliente')
			Else   
		//	   msgStop('N�o Achei o Cliente')
			Endif   
			//Verifica se a Nota j� existe
						
			SF1->(DbSetOrder(1))
			lProcN := !SF1->(DbSeek(xFilial("SF1")+cDoc+cSerie+SA2->A2_COD+SA2->A2_LOJA))
			
			// Se Gerou Cte Com Sucesso Ent�o Joga para Pasta Processados
			If !lProc .or. !lProcF

				//cDirDest := Upper(AllTrim(cPastaPen)+cDirNaoToma+cAno+"\"+cMes+"\")
				cDirDest := Upper(AllTrim(cPastaPen)+cDirNaoToma+cAno+"\"+cMes+"\")
				xFile 	 := StrTran(Upper(cArquivo),Upper(AllTrim(cPastaPen)),cDirDest)
				lProc    := .F.
	
				//Caso nao exista, cria o diretorio no servidor
				MontaDir(cDirDest)

				If !File(xFile)
					Copy File &cArquivo To &xFile
				EndIf

				lDelOk := .F.
				Do While !lDelOk
					fErase(cArquivo)
					lDelOk := !File(cArquivo)
					If !lDelOk
						Conout("O XML est� aberto, feche o mesmo para que ele seja movido de pasta!")
					EndIf
				EndDo

			EndIf
	
		EndCase
		
	EndIf
		if lProc
     //	   msgStop("Irei processar ok!")
     	Else
     //	   msgStop("n�o Irei processar ok!")   
     	Endif   
     	
     	//TODO VALIDA��O DO FORNECEDOR [Weskley Silva 28/08/2018]
     	IF SA2->A2_COD $ GETMV("MV_XCTFOR")
     		lProc := .F.
     	ELSE
     		lProc := .T.
     	ENDIF
     
     	

	//Verifica se produtos do XML estao cadastrados na tabela de relacionamento
	If lProc
		
		cCodPrd  :=  GetNewPar("MV_XCTPRD",'FRETE')
		cCondPag :=  GetNewPar("MV_XCTPAG",'001')
	
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+cCodPrd)

		If !Found()
			Conout("Produto Nao Localizado " + cCodPrd)
			lProc := .F.
		EndIf
		if lProc
     	 //  msgStop("Agora Irei processar ok!")
     	Else
     	  // msgStop("Agora n�o Irei processar ok!")   
     	Endif   
		If lProc

			aItens := {}

			If Type("oCTE:_CTEPROC") = "O"
				oCT := oCTE:_CTEPROC:_CTE:_INFCTE
				oChaveNFE := oCTE:_CTEPROC:_PROTCTE:_INFPROT:_CHCTE:TEXT
			ElseIf Type("oCTE:_CTE:_INFCTE") = "O"
				oCT       := oCTE:_CTE:_INFCTE
				oChaveNFE := SUBSTR(oCTE:_CTE:_SIGNATURE:_SIGNEDINFO:_REFERENCE:_URI:TEXT,5,48)
			EndIf
		
			Do Case
			Case cCnpjTomador = '03007414000130'		//matriz
				If SubStr(cCnpjReme,1,8) = SubStr(cCnpjDest,1,8)
					cTes := SUPERGETMV("MV_XCTTES")
				Else
					cTes := SUPERGETMV("MV_XCTTES")
				EndIf
			Case cCnpjTomador = '03007414000300'	.OR. cCnpjTomador = '03007414000726' 	//sp
			
			    // altera��o conforme marcilio/Francisco CE
				//If SubStr(cCnpjReme,1,8) = SubStr(cCnpjDest,1,8)
				//	cTes = "108" // 113  - teste marcilio   
				//Else
				//	cTes = "109"
				//EndIf
				cTes := SUPERGETMV("MV_XCTTES")
			EndCase
		
			cData := SubStr(oCT:_IDE:_DHEMI:TEXT,9,2)+Substr(oCT:_IDE:_DHEMI:TEXT,6,2)+Substr(oCT:_IDE:_DHEMI:TEXT,1,4)
			dData := Ctod(Substr(cData,1,2)+'/'+Substr(cData,3,2)+'/'+Substr(cData,5,4))
            dDataBase := dData
			if TYPE("oCT:_IMP:_ICMS:_ICMS00:_PICMS:TEXT")<>"U"
				cCTS  := "0" + AllTrim(oCT:_IMP:_ICMS:_ICMS00:_CST:TEXT)
				nPICM := VAL(oCT:_IMP:_ICMS:_ICMS00:_PICMS:TEXT)
				nICMS := VAL(oCT:_IMP:_ICMS:_ICMS00:_VICMS:TEXT)
			Else
				cCTS  := "000"
				nPICM := 0
				nICMS := 0
			Endif

			cNfOri := ''
			If Type("oCT:_infCTeNorm:_infDoc:_infNF") = "O"
				cNfOri := oCT:_infCTeNorm:_infDoc:_infNF:_NDOC:TEXT
			Else
				If Type("oCT:_infCTeNorm:_infDoc:_infNFe") <> "U"
					If Type("oCT:_infCTeNorm:_infDoc:_infNFe") <> "A"
						cNfOri := substr(oCT:_infCTeNorm:_infDoc:_infNFe:_chave:text,26,9) +"-"+substr(oCT:_infCTeNorm:_infDoc:_infNFe:_chave:text,35,1)
					Else
						For nI := 1 To Len(oCT:_infCTeNorm:_infDoc:_infNFe)
							cNfOri := cNfOri += " "+Substr(oCT:_infCTeNorm:_infDoc:_infNFe[nI]:_CHAVE:TEXT,26,9)+"-"+Substr(oCT:_infCTeNorm:_infDoc:_infNFe[nI]:_CHAVE:TEXT,35,1)
						Next
					Endif
				Else
					If Type("oCT:_infCTeNorm:_infDoc:_infOutros") <> "U"
						If Type("oCT:_infCTeNorm:_infDoc:_infOutros") <> "A"
							If Type("oCT:_infCTeNorm:_infDoc:_infOutros:_nDOC") <> "U"
								cNfOri := AllTrim(oCT:_infCTeNorm:_infDoc:_infOutros:_nDOC:text)
							Else
								cNfOri	:= ""
							End
						Else
							For nI := 1 To Len(oCT:_infCTeNorm:_infDoc:_infOutros)
								cNfOri := cNfOri += " "+AllTrim(oCT:_infCTeNorm:_infDoc:_infOutros[nI]:_NDOC:TEXT)
							Next
						EndIf
					EndIf
				EndIf
			EndIf
	
	
			//Integra Ct-e automaticamente
			If SM0->M0_ESTCOB = 'SP'
			   lClassif := !Empty(cTes)  //( SubString(cCnpjTomador,1,8) == SubString(cCnpjReme,1,8) .Or. ( SubString(cCnpjTomador,1,8) != SubString(cCnpjReme,1,8) .And. SubString(cCnpjTomador,1,8) != SubString(cCnpjDest,1,8) )) .And. !Empty(cTes)
 			
			Else
			   lClassif := ( SubString(cCnpjTomador,1,8) == SubString(cCnpjReme,1,8) .Or. ( SubString(cCnpjTomador,1,8) != SubString(cCnpjReme,1,8) .And. SubString(cCnpjTomador,1,8) != SubString(cCnpjDest,1,8) )) .And. !Empty(cTes)
			Endif
			If lClassif 
			 //  msgStop("passei aqui classifica��o esta ok!")   
			Else
			 //  msgStop("passei aqui classifica��o N�ooooooo esta ok!")   
	        Endif
			aCabec  := {}
			aItens  := {}
			aItensA := {}
			
			aAdd(aCabec,{"F1_TIPO"		, "N"											})
			aAdd(aCabec,{"F1_SERIE"		, cSerie										})
			aAdd(aCabec,{"F1_FORMUL"	, "N"											})
			aAdd(aCabec,{"F1_DOC"		, PadL(AllTrim(cDoc),9,'0')						})
			aAdd(aCabec,{"F1_EMISSAO"	, dData											})
			aAdd(aCabec,{"F1_FORNECE"	, SA2->A2_COD									})
			aAdd(aCabec,{"F1_LOJA"		, SA2->A2_LOJA									})
			aAdd(aCabec,{"F1_EST"		, SA2->A2_EST									})
			aAdd(aCabec,{"F1_ESPECIE"	, "CTE"											})
			aAdd(aCabec,{"F1_CHVNFE"	, oChaveNFE										})
			aAdd(aCabec,{"F1_HORA"		, Left(TIME(),5)								})
			aAdd(aCabec,{"F1_DTDIGT"	, dData  										})
			aAdd(aCabec,{"F1_MENNOTA"	, "Referente a Nota Fiscal: "+cNfOri			})
			aAdd(aCabec,{"F1_COND"		, cCondPag										})
			aAdd(aCabec,{"F1_TPCTE"		, "N"											})
			aAdd(aCabec,{"F1_VALBRUT"	, Val(oCT:_VPREST:_VTPREST:TEXT)				})
			aAdd(aCabec,{"F1_VALMERC"	, Val(oCT:_VPREST:_VTPREST:TEXT)				})
		
			aAdd(aItensA,{"D1_COD" 		, SB1->B1_COD							, Nil	})
			aAdd(aItensA,{"D1_UM"	  	, SB1->B1_UM							, Nil	})
			aAdd(aItensA,{"D1_LOCAL"	, SB1->B1_LOCPAD						, Nil	})
			aAdd(aItensA,{"D1_QUANT"	, 1										, Nil	}) 
			aAdd(aItensA,{"D1_CC"		, _cc									, Nil	})
			aAdd(aItensA,{"D1_VUNIT"	, Val(oCT:_VPREST:_VTPREST:TEXT)		, Nil	})
			aAdd(aItensA,{"D1_TOTAL"	, Val(oCT:_VPREST:_VTPREST:TEXT)		, Nil	})
		
			If lClassif
				aAdd(aItensA,{"D1_TES"		, cTes								, Nil	})
		    Else
		       	aAdd(aItensA,{"D1_TES"		, cTes								, Nil	})
			EndIf
			if nPICM > 0
				aAdd(aItensA,{"D1_BASEICM"	, Val(oCT:_VPREST:_VTPREST:TEXT)	, Nil	})
				aAdd(aItensA,{"D1_VALICM"	, nICMS								, Nil	})	//Valor ICMS
			Endif
			
			IF SA2->A2_COD $ SUPERGETMV("MV_XCTEFOR") 
					aAdd(aItensA,{"D1_PICM"		, 0								, Nil	})	//Aliquota
			ELSE
					aAdd(aItensA,{"D1_PICM"		, nPICM							, Nil	})
			ENDIF
			
			
			aAdd(aItens,aItensA)
	        dbselectArea("SF1")
	        SF1->(DBsetOrder(1)) // F1_FILIAL, F1_DOC, F1_SERIE, F1_FORNECE, F1_LOJA, F1_TIPO, R_E_C_N_O, D_E_L_E_T
	        IF SF1->(!DBseek(xfilial("SF1")+PadL(AllTrim(cDoc),9,'0')+cSerie+SA2->A2_COD+SA2->A2_LOJA+"N"))	 
	        If Len(aCabec) > 0 .And. Len(aItens) > 0
				lMSErroAuto := .F.
				//If lClassif
					//Inclusao de Documento de Entrada
					MSExecAuto({|x,y| MATA103(x,y)},aCabec,aItens)

				If lMsErroAuto
                    cERROR := MostraErro()
                    //MostraErro()
				    cDirDest := Upper(AllTrim(cPastaPen)+cDirLogErro+cAno+"\"+cMes+"\")
					MontaDir(cDirDest)
			       	GravaLog := ("Erro na importa��o de CTE: "+oChaveNFE)
					Conout(GravaLog)
					cLog += cDtTime+" : HCOMJ001 - "+GravaLog
					//MostraErro(cDirDest,cDtTime+alltrim(str(nErro))+"_CTE.log")
					nErro++
					lMsErroAuto := .T.
					DisarmTransaction()
					lProc := .F.
					//MsgAlert("Erro ao Processar o XML","Aten��o")
					cLogErro := "Erro ao Processar o XML"
					cArq:=cDtTime+alltrim(str(nErro))+"_CTE.log"
					
                    if cERROR $ "AJUDA:EXISTNF "
                       lProc := .t.
                       Conout("Nota Gerada anteriormente  !!! - Cte " + PadL(AllTrim(oCT:_IDE:_NCT:TEXT),9,'0') + " - Arquivo: " + cArquivo)
                    Endif
				Else
					Conout("Nota Gerada Com Sucesso !!! - Cte " + PadL(AllTrim(oCT:_IDE:_NCT:TEXT),9,'0') + " - Arquivo: " + cArquivo)
					lProc := .T.
				EndIf
            Endif
			EndIf
		EndIf
		
		// Se Gerou Cte Com Sucesso Ent�o Joga para Pasta Processados
		If !lProc .or. !lClassif
			cDirDest := Upper(AllTrim(cPastaPen)+cDirErro+cAno+"\"+cMes+"\")
			xFile    := StrTran(Upper(cArquivo),Upper(AllTrim(cPastaPen)),cDirDest)
		Else
			cDirDest := Upper(AllTrim(cPastaPen)+cDirSucesso+cAno+"\"+cMes+"\")
			xFile    := StrTran(Upper(cArquivo),Upper(AllTrim(cPastaPen)),cDirDest)
		EndIf

		//Caso nao exista, cria o diretorio no servidor
		MontaDir(cDirDest)

		If !File(xFile)
			Copy File &cArquivo To &xFile
		EndIf

		lDelOk := .F.
		Do While !lDelOk
			fErase(cArquivo)
			lDelOk := !File(cArquivo)
			If !lDelOk
				Conout("O XML est� aberto, feche o mesmo para que ele seja movido de pasta!")
			EndIf
		EndDo
	EndIf

Return .T.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
��� Programa � MyOpenSM0� Autor � TOTVS Protheus     � Data �  04/07/2011 ���
�������������������������������������������������������������������������͹��
��� Descricao� Funcao de processamento abertura do SM0 modo exclusivo     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
��� Uso      � MyOpenSM0  - Gerado por EXPORDIC / Upd. V.4.7.2 EFS        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function MyOpenSM0(lShared)

	Local lOpen := .F.
	Local nLoop := 0

	For nLoop := 1 To 20
		DbUseArea( .T., , "SIGAMAT.EMP", "SM0", lShared, .F. )

		If !Empty( Select( "SM0" ) )
			lOpen := .T.
			DbSetIndex( "SIGAMAT.IND" )
			Exit
		EndIf

		Sleep( 500 )

	Next nLoop

	If !lOpen
		cLogErro := "N�o foi poss�vel a abertura da tabela " + 	IIf( lShared, "de empresas (SM0).", "de empresas (SM0) de forma exclusiva.")
	EndIf

Return lOpen


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)
Local aAreaAtu	:= GetArea()
Local _sAlias	:= Alias()
Local aCposSX1	:= {}
Local aPergs	:= {}
Local nX 		:= 0
Local lAltera	:= .F.
Local nCondicao
Local cKey		:= ""
Local nJ		:= 0

aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

	Aadd(aPergs,{"Data base  ","","","mv_ch1","D",8,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})

dbSelectArea("SX1")
dbSetOrder(1)
For nX:=1 to Len(aPergs)
	lAltera := .F.
	If MsSeek(cPerg+Right(aPergs[nX][11], 2))
		If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
			Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
			aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
			lAltera := .T.
		Endif
	Endif
	
	If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
		lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
	Endif
	
	If ! Found() .Or. lAltera
		RecLock("SX1",If(lAltera, .F., .T.))
		Replace X1_GRUPO with cPerg
		Replace X1_ORDEM with Right(aPergs[nX][11], 2)
		For nj:=1 to Len(aCposSX1)
			If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
				FieldPos(AllTrim(aCposSX1[nJ])) > 0
				Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
			Endif
		Next nj
		MsUnlock()
		cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."
		
		
		If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
			aHelpSpa := aPergs[nx][Len(aPergs[nx])]
		Else
			aHelpSpa := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
			aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
		Else
			aHelpEng := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
			aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
		Else
			aHelpPor := {}
		Endif
		
		PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
	Endif
Next

RestArea( aAreaAtu )

Return(cPerg)