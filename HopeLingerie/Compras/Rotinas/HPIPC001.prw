#include "totvs.ch"
#include "protheus.ch"
 
User Function HPIPC001()
 
Local cArq    := "pedido_compra.txt"
Local cLinha  := ""
Local lPrim   := .T.
Local aCampos := {}
Local aDados  := {}
 
Private aErro := {}
 
If !File(cDir+cArq)
	MsgStop("O arquivo " +cDir+cArq + " n�o foi encontrado. A importa��o ser� abortada!","[HPIPC001] - ATENCAO")
	Return
EndIf
 
FT_FUSE(cDir+cArq)
ProcRegua(FT_FLASTREC())
FT_FGOTOP()
While !FT_FEOF()
 
	IncProc("Lendo arquivo texto...")
 
	cLinha := FT_FREADLN()
 
	If lPrim
		aCampos := Separa(cLinha,";",.T.)
		lPrim := .F.
	Else
		AADD(aDados,Separa(cLinha,";",.T.))
	EndIf
 
	FT_FSKIP()
EndDo
 
Begin Transaction
	ProcRegua(Len(aDados))
	For i:=1 to Len(aDados)
 
		IncProc("Importando Pedido...")
 
		dbSelectArea("SC7")
		dbSetOrder(1)
		dbGoTop()
		If !dbSeek(xFilial("SC7")+aDados[i,1])
			Reclock("SC7",.T.)
			SC7->A1_FILIAL := xFilial("SC7")
			For j:=1 to Len(aCampos)
				cCampo  := "SC7->" + aCampos[j]
				&cCampo := aDados[i,j]
			Next j
			SC7->(MsUnlock())
		EndIf
	Next i
End Transaction
 
FT_FUSE()
 
ApMsgInfo("Importa��o dos Pedido conclu�da com sucesso!","[HPIPC001] - SUCESSO")
 
Return