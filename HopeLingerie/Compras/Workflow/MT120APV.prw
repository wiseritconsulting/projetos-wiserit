#Include 'Protheus.ch'

User Function MT120APV()
Local cPerg  := "MT120APV"
Local lPerg  := .F.
Local cRet   := ""
Local cGrupo := SuperGetMV("MV_XGPAPRO",.F.,"000200") //Grupo de Compras com popup

DbSelectArea("SY1")
DbSetOrder(3)
If DbSeek(xFilial("SY1")+__cUserID)
	If SY1->Y1_GRUPCOM $ cGrupo 
		AjustaSX1(cPerg)
		While !lPerg
			If Pergunte(cPerg)
				If !Empty(mv_par01)
					DbSelectArea("SAL")
					DbSetOrder(1)
					If DbSeek(xFilial("SAL")+AllTrim(mv_par01))
						lPerg := .T.
						cRet  := mv_par01 
					Else
						MsgAlert("Selecione um grupo v�lido!","Aten��o")	
					EndIf	
				EndIf
			EndIf
		EndDo
	Else
		cRet := 0 //Retorna numero para assumir o que estava antes.
	EndIf
Else
	cRet := 0	
EndIf

Return cRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )
  
PutSx1(cPerg,"01","Grupo de Aprovacao ? ","","","Mv_ch1",TAMSX3("AL_COD")[3],TAMSX3("AL_COD")[1],TAMSX3("AL_COD")[2],0,"G","","SALZ","","N","mv_par01","","","","","","","","","","","","","","","","",{"Informe o grupo de aprovacao.",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)