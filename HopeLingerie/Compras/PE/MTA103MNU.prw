#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'

User Function MTA103MNU()

Private oOrigem
Private cCod
Private cLoja
Private cNome
Private oCod
Private oLoja
Private oNome

aAdd(aRotina,{ "Selo Fiscal", "U_HSELO", 0 , 2, 0, .F.})
aAdd(aRotina,{ "Endere�ar"  , "U_HENDE", 0 , 2, 0, .F.})
aAdd(aRotina,{ "Nota Fornecedor"  , "U_NotaForn", 0 , 2, 0, .F.})
aAdd(aRotina,{ "OP Triangular Simbolica"  , "U_OPTrian", 0 , 2, 0, .F.})
aAdd(aRotina,{ "Alter Pre-Nota"  , "U_AltPre", 0 , 2, 0, .F.})

Return()

User Function HSELO

Local oButton1
Local oButton2
Local oFont1 := TFont():New("MS Sans Serif",,022,,.T.,,,,,.F.,.F.)
Local oGet1
Local cGet1 := Space(16)
Local oSay1
Static oDlg

	DEFINE MSDIALOG oDlg TITLE "Selo Fiscal" FROM 000, 000  TO 250, 600 COLORS 0, 16777215 PIXEL

    @ 099, 248 BUTTON oButton1 PROMPT "Ok" 			SIZE 037, 012 ACTION (u_atuselo(cGet1,SF1->F1_DOC,SF1->F1_SERIE,SF1->F1_FORNECE,SF1->F1_LOJA),oDlg:End()) OF oDlg PIXEL
    @ 099, 204 BUTTON oButton2 PROMPT "Cancela" 	SIZE 037, 012 ACTION Close(oDlg) OF oDlg PIXEL

    @ 018, 013 SAY oSay1 PROMPT "Informe o Selo Fiscal:" SIZE 172, 019 OF oDlg FONT oFont1 COLORS 0, 16777215 PIXEL
    @ 054, 013 MSGET oGet1 VAR cGet1 SIZE 266, 010 OF oDlg COLORS 0, 16777215 PIXEL

	ACTIVATE MSDIALOG oDlg CENTERED

Return

User Function AtuSelo(_par1,_par2,_par3,_par4,_par5)
	_qry := "Update "+RetSqlname("SF1")+" Set F1_XSELO = '"+_par1+"' where F1_DOC = '"+_par2+"' and F1_SERIE = '"+_par3+"' and F1_FORNECE = '"+_par4+"' and F1_LOJA = '"+_par5+"' and F1_FILIAL = '"+xfilial("SF1")+"' "
	TcSqlExec(_qry)
	
	_qry1 := "UPDATE "+RetSqlName("SF3")+" Set F3_XSELO = '"+_par1+"' WHERE F3_NFISCAL = '"+_par2+"' AND F3_SERIE = '"+_par3+"' AND F3_CLIEFOR = '"+_par4+"' AND F3_LOJA = '"+_par5+"' AND F3_FILIAL = '"+xFilial("SF3")+"' "
	TcSqlExec(_qry1)
	
Return

User Function HENDE()

_area := GetArea()

_doc   := SF1->F1_DOC
_Serie := SF1->F1_SERIE
_forne := SF1->F1_FORNECE
_loja  := SF1->F1_LOJA
_Tipo  := AllTrim(SF1->F1_TIPO)

_qry := "Select * from "+RetSqlName("SDA")+" where D_E_L_E_T_ = '' and DA_DOC = '"+_doc+"' and DA_SERIE = '"+_serie+"' and DA_SALDO > 0 "
_Qry := ChangeQuery(_qry)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSDA",.T.,.T.)

DbSelectArea("TMPSDA")
DbGoTop()

If !EOF()
	While !EOF()
		_acab  := {}
		_aitem := {}
		
			Aadd(_acab, {"DA_PRODUTO"		, TMPSDA->DA_PRODUTO		,NIL})
			Aadd(_acab, {"DA_LOTECTL"		, TMPSDA->DA_LOTECTL		,NIL})
			Aadd(_acab, {"DA_NUMLOTE"		, TMPSDA->DA_NUMLOTE		,NIL})
			Aadd(_acab, {"DA_LOCAL"			, TMPSDA->DA_LOCAL		,NIL})
			Aadd(_acab, {"DA_DOC"			, TMPSDA->DA_DOC			,NIL})
			Aadd(_acab, {"DA_SERIE"			, TMPSDA->DA_SERIE		,NIL})
			Aadd(_acab, {"DA_CLIFOR"		, TMPSDA->DA_CLIFOR		,NIL})
			Aadd(_acab, {"DA_LOJA"			, TMPSDA->DA_LOJA			,NIL})
			Aadd(_acab, {"DA_NUMSEQ"		, TMPSDA->DA_NUMSEQ		,NIL})

			DbSelectArea("SDB")
			DbSetOrder(1)
			DbSeek(xfilial("SDB")+TMPSDA->DA_PRODUTO+TMPSDA->DA_LOCAL+TMPSDA->DA_NUMSEQ+TMPSDA->DA_DOC)

			Aadd(_aitem,{"DB_ITEM"			, "0001"				,NIL})
			Aadd(_aitem,{"DB_ESTORNO"		, " "					,NIL})
			Aadd(_aitem,{"DB_DATA"			, ddatabase			,NIL})
			Aadd(_aitem,{"DB_QUANT"			, TMPSDA->DA_SALDO	,NIL})
			Aadd(_aitem,{"DB_LOTECTL"		, SDB->DB_LOTECTL		,NIL})
			Aadd(_aitem,{"DB_NUMLOTE"		, SDB->DB_NUMLOTE		,NIL})
			If _Tipo == "D"
				Aadd(_aitem,{"DB_LOCALIZ"	, "DEV"				,NIL})
			Else
				Aadd(_aitem,{"DB_LOCALIZ"	, "PADRAO"				,NIL})
			End
			Aadd(_aitem,{"DB_PRODUTO"		, SDB->DB_PRODUTO		,NIL})
			Aadd(_aitem,{"DB_LOCAL"			, SDB->DB_LOCAL		,NIL})
			Aadd(_aitem,{"DB_DOC"			, SDB->DB_DOC			,NIL})
			Aadd(_aitem,{"DB_SERIE"			, SDB->DB_SERIE		,NIL})
			Aadd(_aitem,{"DB_CLIFOR"			, SDB->DB_CLIFOR		,NIL})
			Aadd(_aitem,{"DB_LOJA"			, SDB->DB_LOJA		,NIL})
			Aadd(_aitem,{"DB_NUMSEQ"			, SDB->DB_NUMSEQ		,NIL})
			Aadd(_aitem,{"DB_ATUEST"			, "S"					,NIL})

			lmsErroAuto := .F.
			msExecAuto({|X,Y,Z|MATA265(X,Y,Z)},_acab,{_aitem},3)

			if lMsErroAuto      // variavel do sigaauto indicando erro na operacao
				MostraErro() //MS 14.01.04
			Endif

		DbSelectArea("TMPSDA")
		DbSkip()
	End
	Alert("Endere�amento efetuado!")
Else
	Alert("Nota Fiscal j� endere�ada")
Endif
DbCloseArea()

RestArea(_area)
Return

User Function NotaForn()

Private Cancelar
Private Confirmar
Private oCod
Private cCod := Space(6)
Private oGet2
Private cLoja := Space(4)
Private oGet3
Private cNome := Space(30)
Private oLoja
Private oNome
Static oDlg

  DEFINE MSDIALOG oDlg2 TITLE "Nota Fornecedor" FROM 000, 000  TO 200, 500 COLORS 0, 16777215 PIXEL

    @ 019, 017 SAY oCod PROMPT "Cod" SIZE 015, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
    @ 018, 039 MSGET oCod VAR cCod SIZE 027, 010 OF oDlg2 VALID Gatilhos(2) COLORS 0, 16777215 F3 "SA2" PIXEL
    @ 030, 017 SAY oLoja PROMPT "Loja" SIZE 016, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
    @ 031, 039 MSGET oGet2 VAR cLoja SIZE 025, 010 OF oDlg2 VALID  Gatilhos(3) COLORS 0, 16777215 PIXEL
    @ 043, 017 SAY oNome PROMPT "Nome" SIZE 017, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
    @ 043, 039 MSGET oGet3 VAR cNome SIZE 192, 010 OF oDlg2 COLORS 0, 16777215 READONLY PIXEL
    
    
    @ 069, 057 BUTTON Confirmar PROMPT "Confirmar" SIZE 037, 012 OF oDlg2  ACTION (SALVAR(cCod,cLoja), oDlg2:End()) PIXEL
    @ 069, 112 BUTTON Cancelar PROMPT "Cancelar" SIZE 037, 012 OF oDlg2  Action oDlg2:End() PIXEL

  ACTIVATE MSDIALOG oDlg2 CENTERED

Return


Static Function Salvar(cCod,cLoja)
	
	_qry := "Update "+RetSqlname("SA5")+" Set A5_NOTA = '5' where A5_FORNECE = '"+cCod+"' and A5_LOJA = '"+cLoja+"'  and A5_FILIAL = '"+xfilial("SA5")+"' AND D_E_L_E_T_ = '' "
	TcSqlExec(_qry)

return

Static Function Gatilhos(cCodGat)
	
	If cCodGat==2
		cLoja	:= POSICIONE("SA2",1,xFilial("SA2")+cCod,"A2_LOJA")
		If Alltrim(cLoja)==""
			oLoja:SetFocus()
		End
	endif
	
	If cCodGat==3
		cNome	:= POSICIONE("SA2",1,xFilial("SA2")+cCod,"A2_NOME")
		If Alltrim(cNome)==""
			oNome:SetFocus()
		End
	endif
	
return

User Function OPTrian()

Private Cancelar
Private Confirmar
Private oCod
Private cCod := Space(6)
Private oGet2
Private cLoja := Space(4)
Private oGet3
Private cNome := Space(30)
Private oLoja
Private oNome
Static oDlg

  DEFINE MSDIALOG oDlg TITLE "OP Triangular " FROM 000, 000  TO 200, 500 COLORS 0, 16777215 PIXEL

    @ 019, 017 SAY oCod PROMPT "Cod" SIZE 015, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 018, 039 MSGET oCod VAR cCod SIZE 027, 010 OF oDlg VALID Gatilhos(2) COLORS 0, 16777215 F3 "SA2" PIXEL
    @ 030, 017 SAY oLoja PROMPT "Loja" SIZE 016, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 031, 039 MSGET oGet2 VAR cLoja SIZE 025, 010 OF oDlg VALID  Gatilhos(3) COLORS 0, 16777215 PIXEL
    @ 043, 017 SAY oNome PROMPT "Nome" SIZE 017, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 043, 039 MSGET oGet3 VAR cNome SIZE 192, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
    
    
    @ 069, 057 BUTTON Confirmar PROMPT "Confirmar" SIZE 037, 012 OF oDlg  ACTION (Gerar(cCod,cLoja), oDlg:End()) PIXEL
    @ 069, 112 BUTTON Cancelar PROMPT "Cancelar" SIZE 037, 012 OF oDlg  Action oDlg:End() PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED

Return

Static Function Gerar(cCod,cLoja)

Local _area := GetArea()
Local aCabec := {}
Local aItens := {}
Local aLinha := {}
Local nX     := 0
Local nY     := 0
Local cDoc   := ""
Local lOk    := .T.
Local cQuery := ""
Local cFornece := SF1->F1_FORNECE
Local cLoja    := SF1->F1_LOJA
Local cDoc     := SF1->F1_DOC
Local cItem    := "01"
Local cNewFor  := cCod
Local cLojaf   := cLoja
 
PRIVATE lMsErroAuto := .F.

        cNum := GetSxeNum("SC5","C5_NUM")
        RollBAckSx8()
        aCabec := {}
        aItens := {}
        aadd(aCabec,{"C5_NUM"   ,cNum,Nil})
        aadd(aCabec,{"C5_TIPO" ,"B",Nil})
        aadd(aCabec,{"C5_CLIENTE",cNewFor,Nil})
        aadd(aCabec,{"C5_LOJACLI",cLojaf,Nil})
        aadd(aCabec,{"C5_LOJAENT",cLojaf,Nil})
        aadd(aCabec,{"C5_POLCOM","099",Nil})
        aadd(aCabec,{"C5_TPPED","070",Nil})
        aadd(aCabec,{"C5_CONDPAG","B01",Nil})
        aadd(aCabec,{"C5_XOBSINT",cDoc,Nil})
              
        
        IF Select("TMPSD1") > 0
        	TMPSD1->(dbCloseArea())
        Endif

        cQuery := " SELECT * FROM "+RetSqlName("SD1")+" (NOLOCK) WHERE D1_DOC = '"+cDoc+"' AND D1_FORNECE = '"+cFornece+"' AND D1_LOJA = '"+cLoja+"' AND D_E_L_E_T_ = '' "

        dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSD1",.T.,.T.)

        DbSelectArea("TMPSD1")
        DbGoTop()
        
        
      WHILE TMPSD1->(!EOF())
    
            aLinha := {}
            aadd(aLinha,{"C6_ITEM",cItem,Nil})
            aadd(aLinha,{"C6_PRODUTO",TMPSD1->D1_COD,Nil})
            aadd(aLinha,{"C6_QTDVEN",TMPSD1->D1_QUANT,Nil})
            aadd(aLinha,{"C6_PRCVEN",TMPSD1->D1_VUNIT,Nil})
            aadd(aLinha,{"C6_PRUNIT",TMPSD1->D1_VUNIT,Nil})
            aadd(aLinha,{"C6_VALOR",TMPSD1->D1_TOTAL,Nil})
            aadd(aLinha,{"C6_TES","777",Nil})
            aadd(aLinha,{"C6_LOCAL",TMPSD1->D1_LOCAL,Nil})
            aadd(aItens,aLinha)
            
            cItem := SOMA1(cItem)
            TMPSD1->(DBSKIP())
        END
        
        MSExecAuto({|x,y,z|mata410(x,y,z)},aCabec,aItens,3)
        
       If !lMsErroAuto
           Msginfo("Pedido "+cNum+" gerado com sucesso! ","HOPE")
           
           DbSelectArea("SC6")
           DbSetOrder(1)
            
           if DBSeek(xFilial("SC6")+cNum)
           		While SC6->C6_NUM == cNum .AND. SC6->(!EOF())
           			MaLibDoFat(SC6->(RecNo()),SC6->C6_QTDVEN,.F.,.F.,.T.,1,.T.,.F.,,,)
           			SC6->(dBSkip())
           		enddo
	
           endif
	
        Else
        	MostraErro()
            Alert("Erro na gera��o do Pedido")
        EndIf
     RestArea(_area)          
Return 

User Function AltPre()
Local oButton1
Local oGet1
Local cGet1 := SPACE(9)
Local oGet2
Local cGet2 := SPACE(4)
Local oSay1
Local oSay2
Local oSay3
Static oDlg3

  DEFINE MSDIALOG oDlg3 TITLE "Novo Documento " FROM 000, 000  TO 200, 350 COLORS 0, 16777215 PIXEL

    @ 030, 017 SAY oSay1 PROMPT "Documento: " SIZE 036, 007 OF oDlg3 COLORS 0, 16777215 PIXEL
    @ 029, 060 MSGET oGet1 VAR cGet1 SIZE 064, 010 OF oDlg3 COLORS 0, 16777215 PIXEL
    @ 050, 018 SAY oSay2 PROMPT "S�rie:" SIZE 025, 007 OF oDlg3 COLORS 0, 16777215 PIXEL
    @ 049, 060 MSGET oGet2 VAR cGet2 SIZE 025, 010 OF oDlg3 COLORS 0, 16777215 PIXEL
    @ 009, 043 SAY oSay3 PROMPT "Novo N�mero de Documento " SIZE 092, 007 OF oDlg3 COLORS 0, 16777215 PIXEL
    @ 072, 058 BUTTON oButton1 PROMPT "Efetivar" SIZE 037, 012 ACTION (u_AtuNum(cGet1,cGet2),oDlg3:End()) OF oDlg3 PIXEL

  ACTIVATE MSDIALOG oDlg3 CENTERED

return 

User Function AtuNum(_par1,_par2)
	_qry := "Update "+RetSqlname("SF1")+" Set F1_DOC = '"+_par1+"' , F1_SERIE = '"+_par2+"'  where F1_DOC = '"+SF1->F1_DOC+"' and F1_SERIE = '"+SF1->F1_SERIE+"' and F1_FORNECE = '"+SF1->F1_FORNECE+"' and F1_LOJA = '"+SF1->F1_LOJA+"' and F1_FILIAL = '"+xfilial("SF1")+"' "
	TcSqlExec(_qry)
	
	_qry1 := "UPDATE "+RetSqlName("SD1")+" Set D1_DOC = '"+_par1+"', D1_SERIE = '"+_par2+"'  WHERE D1_DOC = '"+SF1->F1_DOC+"' AND D1_SERIE = '"+SF1->F1_SERIE+"' AND D1_FORNECE = '"+SF1->F1_FORNECE+"' AND D1_LOJA = '"+SF1->F1_LOJA+"' AND D1_FILIAL = '"+xFilial("SD1")+"' "
	TcSqlExec(_qry1)
	
Return
