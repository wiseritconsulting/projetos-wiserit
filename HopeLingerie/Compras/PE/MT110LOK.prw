#include 'protheus.ch'

/*/{Protheus.doc} MT110LOK
// Obrigatoriedade do centro de custo
@author Weskley Silva
@since 20/06/2018
@version 1.0
@type function
/*/

User Function MT110LOK()

	// Declara��o de variaveis
	Local _lRet	:= .T.
	Local i := 1
	
	// Busca posi�ao do campos
	Local _nPosRat	:= aScan(aHeader,{|x| AllTrim(x[2]) == 'C1_RATEIO'})
	Local _nPosCc	:= aScan(aHeader,{|x| AllTrim(x[2]) == 'C1_CC'})
	
	// Valida�ao a ser executada
	/*If Alltrim(aCols[i][_nPosRat]) == '1' // Caso n�o tenha rateio
		IF LEN(ACPISCX) == 0 	
			MsgStop("Preencha o rateio de Centro de Custo"	,"A T E N � A O !!")
			_lRet := .F. // Quando false o sistema n�o permitir� que o usu�rio prossiga para a proxima linha
		EndIf
	EndIf*/

	nRat:= 0

	//Verifica quantos centro de custo n�o foram preenchidos
	For nCols:= 1 to Len(aCols)
		If Empty(Alltrim(aCols[n][_nPosCC]))
			nRat+= 1
		Endif	
	Next

	If Alltrim(aCols[n][_nPosRat]) <> '1'
      If Empty(Alltrim(aCols[n][_nPosCC]))
         _lRet:= .F.
			MsgStop('Preencha o Centro de Custo deste Item!')
      Endif
	//Else
		//A quantidade de rateios informados deve ser a mesma da variavel nRat que representa a quantidade de centro de custo n�o informado
		//IF LEN(ACPISCX) < nRat //== 0 	
			//MsgStop("Preencha o rateio de Centro de Custo")
			//_lRet := .F. // Quando false o sistema n�o permitir� que o usu�rio prossiga para a proxima linha
	//	EndIf
   Endif


Return _lRet