#include 'protheus.ch'
#include 'parmtype.ch'

user function MT103EXC()

Local lRet

	if __cuserid $ GETMV("HP_DENTEXC")
	
		  _qry := " UPDATE SAT_PNE_REMESSAS SET REM_INTEGRADO = '' WHERE REM_ID = '"+SF1->F1_XREMID+"'  "
		  TcSqlExec(_qry) 

		lRet := .T.

	else 

		msgAlert("Nao � permitida exclus�o do documento de Entrada, Favor falar com o setor Fiscal ","MT103EXC") 	
		lRet := .F. 
	endif 
	
return lRet 