#Include 'Rwmake.ch'

/*
�����������������������������������������������������������������������������
���Programa  �MT150LEG  �Autor  �Pedro Augusto       � Data �  20/07/2012 ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao para adicionar uma cor no browse                      ��
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�����������������������������������������������������������������������������
*/
//////////////////// fazer uma cor a mais para cotacao respondida? considerar a possibilidade no forn recusar a cotacao. ////////

User Function MT150LEG
	Local _aRet := {}
	Local _nOpc := ParamIXB[1]
	If _nOpc == 1 // Cores
		Aadd( _aRet,{'C8_XWF="1" .and. Alltrim(C8_NUMPED) =""','BR_LARANJA'})
		Aadd( _aRet,{'C8_XWF="2" .and. Alltrim(C8_NUMPED) =""','BR_AZUL'   })
		Aadd( _aRet,{'C8_XWF="3" .and. Alltrim(C8_NUMPED) =""','BR_PRETO'   })
	ElseIf _nOpc == 2  // Legenda
		Aadd( _aRet,{'BR_LARANJA','Enviado e-mail p/ fornec.'     } )
		Aadd( _aRet,{'BR_AZUL'   ,'E-mail resp. pelo fornec. - Cotado'} )
		Aadd( _aRet,{'BR_PRETO'  ,'Recusado a participar da Cota��o'} )
	Endif
	Return _aRet
