#Include 'Rwmake.ch'
#Include 'AP5Mail.ch'
#include "Protheus.Ch"
#include "TbiConn.ch"
#Define CRLF  CHR(13)+CHR(10)

/*
�����������������������������������������������������������������������������
���Programa  �MT150ROT  �Autor  �      � Data �  26/09/2017               ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao para adicionar um botao na cotacao                    ��
�������������������������������������������������������������������������͹��
���Uso       � Hope Lingerie                                              ���
�����������������������������������������������������������������������������
*/

User Function MT150ROT()
	aAdd(aRotina,{"Enviar e-mail","U_MT150EM", 0 , 4})
Return aRotina

User Function MT150EM()
	U_ListBoxMar(SC8->C8_NUM)
Return .T.

///////////////////////////////////////////////////////////////////////////////////
//+-----------------------------------------------------------------------------+//
//| DATA     | AUTOR                | DESCRICAO                                 |//
//+-----------------------------------------------------------------------------+//
//| 20/07/12 |                      | Uso Exclusivo Hope Lingerie               |//
//+-----------------------------------------------------------------------------+//
///////////////////////////////////////////////////////////////////////////////////
User Function ListBoxMar(cCotacao)

Local cVar      := Nil
Local oDlg      := Nil
Local cTitulo   := "Selecionar Fornecedores para envio"
Local lMark     := .F.
Local oOk       := LoadBitmap( GetResources(), "CHECKED" )   //CHECKED    //LBOK  //LBTIK
Local oNo       := LoadBitmap( GetResources(), "UNCHECKED" ) //UNCHECKED  //LBNO
Local oChk      := Nil
Local aAreaSC8	:= GetArea("SC8")
Local _cFornece := ""
Local _cLoja    := ""

Private lChk     := .F.
Private oLbx := Nil
Private aVetor := {}

dbSelectArea("SC8")
dbSetOrder(1) //C8_FILIAL+C8_NUM+C8_FORNECE+C8_LOJA+C8_ITEM+C8_NUMPRO+C8_ITEMGRD
dbSeek(xFilial("SC8") + cCotacao)

//+-------------------------------------+
//| Carrega o vetor conforme a condicao |
//+-------------------------------------+

While !Eof() .And. C8_FILIAL == xFilial("SC8") .AND. C8_NUM == cCotacao
	If SC8->C8_FORNECE + SC8->C8_LOJA <> _cFornece + _cLoja
   		aAdd( aVetor, { lMark, 	C8_NUM, ;
   								C8_FORNECE, ;
   								C8_LOJA, ;
   								Posicione("SA2",1,xFilial("SA2")+SC8->(C8_FORNECE+C8_LOJA),"A2_NREDUZ"),;
   								Posicione("SA2",1,xFilial("SA2")+SC8->(C8_FORNECE+C8_LOJA),"A2_EMAIL") })
		_cFornece := SC8->C8_FORNECE
		_cLoja    := SC8->C8_LOJA

   	Endif
	dbSkip()
End

//+-----------------------------------------------+
//| Monta a tela para usuario visualizar consulta |
//+-----------------------------------------------+
If Len( aVetor ) == 0
   Aviso( cTitulo, "Nao existe Fornecedores a consultar", {"Ok"} )
   Return
Endif

DEFINE MSDIALOG oDlg TITLE cTitulo FROM 0,0 TO 240,500 PIXEL

@ 10,10 LISTBOX oLbx VAR cVar FIELDS HEADER ;
   " ", "Numero","Fornecedor", "Loja", "Nome Fornecedor", "E-mail Fornecedor" ;
   SIZE 230,095 OF oDlg PIXEL ON dblClick(aVetor[oLbx:nAt,1] := !aVetor[oLbx:nAt,1],oLbx:Refresh())

oLbx:SetArray( aVetor )
oLbx:bLine := {|| {Iif(aVetor[oLbx:nAt,1],oOk,oNo),;
                       aVetor[oLbx:nAt,2],;
                       aVetor[oLbx:nAt,3],;
                       aVetor[oLbx:nAt,4],;
                       aVetor[oLbx:nAt,5],;
                       aVetor[oLbx:nAt,6]}}

@ 110,10 CHECKBOX oChk VAR lChk PROMPT "Marca/Desmarca" SIZE 60,007 PIXEL OF oDlg ;
         ON CLICK(aEval(aVetor,{|x| x[1]:=lChk}),oLbx:Refresh())

DEFINE SBUTTON FROM 107,213 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg
ACTIVATE MSDIALOG oDlg CENTER

// Enviar e-mail para os elementos do aVetor que estejam TRUE //

If Len(aVetor) <> 0
	For i = 1 to Len(aVetor)
        If aVetor[i][1] == .T.
			IF EMPTY(aVetor[i][6])
				cTitle  := "Administrador do Workflow : NOTIFICACAO"
				aMsg	:= {}
				AADD(aMsg, Dtoc(MSDate()) + " - " + Time() )
				AADD(aMsg, "Cota��o No: " + aVetor[i][2] + " Filial : " + cFilAnt + " Fornecedor : " + aVetor[i][5] + " (" + aVetor[i][3] +"-" + aVetor[i][4] + ")")
			   	DbSelectArea("SA2")
			   	DbSetOrder(1)
			   	DbSeek(xFilial("SA2") + aVetor[i][3] + aVetor[i][4])
				AADD(aMsg, IIF(SA2->A2_RECMAIL <> "1", "Fornecedor n�o recebe EMAIL", " E-MAIL n�o preenchido no cadastro"))
				IF !EMPTY(SA2->A2_FAX)
					AADD(aMsg, "Favor enviar pelo FAX : " + SA2->A2_FAX )
				ELSE
					AADD(aMsg, "O FAX n�o est� informado no cadastro. ")
				ENDIF
				xmensa:= ""
				for xi := 1 to len(aMsg)
				    xmensa+= aMsg[xi]+" "+CRLF
				    if xi = 1
				      xmensa+= "  " +   CRLF
   				      xmensa+= "  " +   CRLF
				      xmensa+= "  " +   CRLF
				      xmensa+= "  " +   CRLF
                    Endif
                Next xi
                
                xCPara:= alltrim(getmv("HP_EMADMWS" )) //E-MAIL DOS RESPONS�VEIS DE WORKFLOW COMPRAS SEPARADO POR ;
                npos1 = 1
                while nPos1 == 0
                   npos2 := len(xCPara)
                   nPos1 := At(';',xCPara)
                   If npos1 <> 0
                      xcPara_1 := substr(xcPara,1,npos1-1)
                      xCPara   := substr(xcPara,npos1+1,npos2-npos1)
                   endif     
				   EnvMail(xCPara,cTitle, xmensa, "" )
				Enddo   
				//   EnvMail('daniel.souza@hopelingerie.com.br;marclio.piza@hopelingerie.com.br' ,cTitle, xmensa, "" )
			     //U_MailNotify("marcilio.piza@hopelingerie.com.br", cTitle, aMsg)
//				ConfirmaSC8(IIF(SA2->A2_RECMAIL<>"1", "2", "3"))
			ELSE
				U_EnviaCT(aVetor[i][2],aVetor[i][3],aVetor[i][4]) // elementos do array: 	C8_NUM, C8_FORNECE, C8_LOJA
			ENDIF
		Endif
	Next i
Endif

/*
�����������������������������������������������������������������������������
���Programa  �EnviaCT   �Autor  �Microsiga           � Data �  20/07/2012 ���
�������������������������������������������������������������������������͹��
���Uso       � Hope Lingerie                                              ���
�����������������������������������������������������������������������������
*/
User Function EnviaCT(_cNum, _cFornece, _cLoja)
	Local _cUser
	Private _aCond := {}

	DBSelectArea("SC8")
	DBSetOrder(1)
	DBSeek(xFilial("SC8")+_cNum + _cFornece + _cLoja)

   	DbSelectArea("SA2")
   	DbSetOrder(1)
   	DbSeek(xFilial("SA2") + _cFornece + _cLoja)

	// CONDI��O DE PAGAMENTO
	dbSelectArea("SE4")
	dbSetOrder(1)
	IF dbSeek(xFilial("SE4") + SA2->A2_COND )	// DO CLIENTE
		aAdd( _aCond, SE4->E4_CODIGO + " - " + SE4->E4_DESCRI )
	endif

	dbGoTop()
	while !SE4->(Eof()) .and. xFilial("SE4") == SE4->E4_FILIAL
		aAdd( _aCond, SE4->E4_CODIGO + " - " + SE4->E4_DESCRI )
		SE4->(dbSkip())
	enddo

	oProcess:= TWFProcess():New( "000001", "Cotacao Eletronica" )
	oProcess:NewTask( "Cotacao de Precos", "\WORKFLOW\HTML\CTFORN_HOPE.HTM" )
	oProcess:bReturn  		:= "U_RecCT()"
	oProcess:nEncodeMime 	:= 0
	oProcess:cSubject 		:= "Cota��o Eletr�nica de Precos No." + _cNum
	oProcess:cTo      		:= SA2->A2_EMAIL
	oProcess:NewVersion(.T.)

 	oHtml     				:= oProcess:oHTML

	oHtml:ValByName( "C8_NUM"		, _cNum )
	oHtml:ValByName( "A2_NREDUZ"	, SA2->A2_NREDUZ )
	oHtml:ValByName( "A2_COD"		, SA2->A2_COD )
	oHtml:ValByName( "A2_LOJA"		, SA2->A2_LOJA )
	oHtml:ValByName( "A2_END"		, SA2->A2_END )
	oHtml:ValByName( "A2_BAIRRO"	, SA2->A2_BAIRRO )
	oHtml:ValByName( "A2_MUN"		, SA2->A2_MUN )
	oHtml:ValByName( "A2_EST"		, SA2->A2_EST )
	oHtml:ValByName( "A2_TEL"		, SA2->A2_TEL )
	oHtml:ValByName( "A2_FAX"		, SA2->A2_FAX )
	oHtml:ValByName( "E4_COND"   	, _aCond      )
	oHtml:ValByName( "CONTATO"   	, SC8->C8_CONTATO   )
//	oHtml:ValByName( "TOTCOT"    	, TRANSFORM( 0.00 ,'@R 9,999,999.99' ) )


	//------------------------------------
	// ALIMENTA A TELA DE ITENS DA COTACAO
	//------------------------------------
	While !SC8->(EOF()) .AND. SC8->(C8_FILIAL +  C8_NUM + C8_FORNECE + C8_LOJA) == xFilial("SC8") + _cNum + _cFornece + _cLoja

		DBSELECTAREA("SB1")
		DBSetOrder(1)
		DBSeek(xFilial()+SC8->C8_PRODUTO)

		DBSELECTAREA("SBM")
		DBSetOrder(1)
		DBSeek(xFilial()+SB1->B1_GRUPO)

		DBSELECTAREA("SB5")
		dbSetOrder(1)
		If SB5->(dbSeek( xFilial("SB5") + SC8->C8_PRODUTO ))
			_cDescPro := SB5->B5_CEME
		Else
			_cDescPro := SB1->B1_DESC
		EndIf

		aAdd( (oHtml:ValByName( "t.1"     )), SC8->C8_ITEM    )
		AAdd( (oHtml:ValByName( "t.2"    )), SC8->C8_PRODUTO)
		AAdd( (oHtml:ValByName( "t.3"    )), _cDescPro)
		AAdd( (oHtml:ValByName( "t.4"    )), SC8->C8_OBS)
		AAdd( (oHtml:ValByName( "t.5"    )), TRANSFORM(SC8->C8_QUANT,'@E 999,999.99'))
		AAdd( (oHtml:ValByName( "t.6"    )), SC8->C8_UM)
		AAdd( (oHtml:ValByName( "t.7"    )), "")
		AAdd( (oHtml:ValByName( "t.8"    )), "")
		AAdd( (oHtml:ValByName( "t.9"    )), "")
		AAdd( (oHtml:ValByName( "t.10"   )), "")
		AAdd( (oHtml:ValByName( "t.11"   )), Dtoc(SC8->C8_DATPRF))
		SC8->(dbSkip())
	Enddo


	aAdd( oProcess:aParams, xFilial("SC8"))
	aAdd( oProcess:aParams, _cNum)
	aAdd( oProcess:aParams, _cFornece)
	aAdd( oProcess:aParams, _cLoja)

	oHtml:ValByName( "datetime"     , DTOC(MSDATE()) + " �s " + left(time(),5) )
	oHtml:ValByName( "procid"       , oProcess:fProcessID  )

	//Uso um endereco invalido, apenas para criar o processo de workflow, mas sem envia-lo
	oProcess:nEncodeMime := 0
	oProcess:cTo  := NIL
	//cMailId    := oProcess:Start("\workflow\emp"+cEmpAnt+"\wfct\")  // Crio o processo e gravo o ID do processo de Workflow
	cMailId    := oProcess:Start("\workflow\http\messenger\confirmacao\")
    //N:\PROTHEUS 12\Protheus\protheus_data\workflow\http\messenger\confirmacao
	// ARRAY DE RETORNO
	_aReturn := {}
	AADD(_aReturn, oProcess:fProcessId)

	chtmlfile  := cmailid + ".htm"

	oProcess:= TWFProcess():New( "000002", "Cotacao Eletronica" )
	oProcess:NewTask( "Cotacao de Precos", "\WORKFLOW\HTML\CVFORN_HOPE.HTM" )
	oProcess:cSubject 		:= "Cota��o Eletr�nica de Precos No." + _cNum
	oProcess:nEncodeMime 	:= 0
	oProcess:cTo      		:= SA2->A2_EMAIL
	oProcess:NewVersion(.T.)

 	oHtml     				:= oProcess:oHTML
    // Preencher os campos do convite : CVFORN_HOPE.HTM //

 	oHtml:ValByName( "C8_NUM"		, _cNum )
	oHtml:ValByName( "A2_NOME"		, SA2->A2_NREDUZ )
	oHtml:ValByName( "C8_FORNECE"	, SA2->A2_COD )
	oHtml:ValByName( "C8_LOJA"		, SA2->A2_LOJA )
	oHtml:ValByName( "cMailID"		, Alltrim(GetMV("MV_WFDHTTP"))+chtmlfile)
	//oHtml:ValByName( "cMailID"		, Alltrim(GetMV("MV_WFDHTTP"))+"\workflow\emp"+cEmpAnt+"\wfct\"+chtmlfile)

	oProcess:Start()

	If Len(_aReturn) > 0
		_lProcesso 	:= .T.
		DBSELECTAREA("SC8")
		DBSetOrder(1)
		DBSeek(xFilial("SC8")+_cNum + _cFornece + _cLoja)
		While !SC8->(EOF()) .AND. SC8->(C8_FILIAL +  C8_NUM + C8_FORNECE + C8_LOJA) == xFilial("SC8") + _cNum + _cFornece + _cLoja

			Reclock("SC8",.F.)
			SC8->C8_XWF			:= IIF(EMPTY(_aReturn[1])," ","1")  	// Status 1 - envio para aprovadores / branco-nao houve envio
  			SC8->C8_WFID		:= _aReturn[1]							// Rastreabilidade
			MSUnlock()
			SC8->(dbSkip())

		Enddo
	Endif

	Return


/*
�����������������������������������������������������������������������������
���Programa  �RecCT   �Autor  �Microsiga           � Data �  20/07/2012 ���
�������������������������������������������������������������������������͹��
���Uso       � Hope Lingerie                                              ���
�����������������������������������������������������������������������������
*/
User function RecCT(oProcess)
	ChkFile("SC8")
	ChkFile("SE4")

//	U_CONSOLE("2 - Processa O RETORNO DO EMAIL")
//	U_CONSOLE("2 - EmpFil:" + cEmpAnt + cFilAnt)

//	U_CONSOLE("2 - Semaforo Vermelho" )
//	nWFCT 		:= U_Semaforo("WFCT")

	_cNUM    	:= oProcess:aParams[2]
	_cFORNECE	:= oProcess:aParams[3]
	_cLOJA 		:= oProcess:aParams[4]
	_cEMAIL 	:= ALLTRIM(oProcess:cTo)
	_cWFID		:= oProcess:fProcessID

	U_CONSOLE("2 - Cotacao : " + _cNum + " - Forn.: " + _cFornece +  " - WFID: " + _cWFID)

	_cOPC	     := oProcess:oHtml:RetByName("OPC")
	_cOBS	     := oProcess:oHtml:RetByName("OBS")
	_cContato    := oProcess:oHtml:RetByName("CONTATO")
	_cCond  	 := oProcess:oHtml:RetByName("E4_COND")
//	U_CONSOLE("ValType(_cCond) : " + ValType(_cCond))
//	U_CONSOLE("_cCond : " + Substr(_cCond,1,3))
	_lOk 		:= .F.
	_lEncerrado	:= .F.
	aProd		:= {}

	if valtype(oProcess:oHtml:RetByName("t.1")) <> "U"
		nQuant := LEN(oProcess:oHtml:RetByName("t.1"))
	endif

//	If _cOpc == "N" // recusou participar

	FOR _nInd := 1 TO nQuant
		_cITEM  :=   iif(valtype(oProcess:oHtml:RetByName("t.1"))  <>"U",     oProcess:oHtml:RetByName("t.1" )[_nind] ,"")
		_nQUANT :=   iif(valtype(oProcess:oHtml:RetByName("t.5" )) <>"U", val(oProcess:oHtml:RetByName("t.5" )[_nind]), 0)
		_nPRECO :=   iif(valtype(oProcess:oHtml:RetByName("t.7" )) <>"U", val(oProcess:oHtml:RetByName("t.7" )[_nind]), 0)
		_nVLDESC:=   iif(valtype(oProcess:oHtml:RetByName("t.8" )) <>"U", val(oProcess:oHtml:RetByName("t.8" )[_nind]), 0)
		_nALIIPI:=   iif(valtype(oProcess:oHtml:RetByName("t.9" )) <>"U", val(oProcess:oHtml:RetByName("t.9" )[_nind]), 0)
		_nPRAZO :=   iif(valtype(oProcess:oHtml:RetByName("t.10")) <>"U", val(oProcess:oHtml:RetByName("t.10")[_nind]), 0)
		dbSelectArea("SC8")
		dbSetOrder(1)
		IF SC8->(dbSeek( xFilial("SC8") + _cNUM + _cFORNECE + _cLOJA + _cITEM))
			_cUser := 'Teste' //SC8->C8_XUSER
//			CONOUT("_cWFID       " + _cWFID )
//			CONOUT("SC8->C8_WFID " + SC8->C8_WFID )
//			CONOUT("_nPRECO      " + TRANSFORM( _nPRECO,'@R 9,999,999.99' ) )
//			CONOUT("_nQuant      " + TRANSFORM( _nQuant,'@R 9,999,999.99' ) )

			IF Alltrim(_cWFID) == Alltrim(SC8->C8_WFID) .AND. Alltrim(SC8->C8_NUMPED) == ""
				CONOUT("Atualizando dados do item: " + _cItem )
				RecLock("SC8",.F.)
				If _cOpc == "N" // Recusa de participacao
		            SC8->C8_XWF := "3"		
					SC8->C8_OBS  	:= "Recusou participar: "+_cOBS
				Else
                    SC8->C8_XWF := "2"
					SC8->C8_QUANT  	:= _nQUANT
					SC8->C8_PRECO  	:= _nPRECO
					SC8->C8_TOTAL  	:= _nQUANT * _nPRECO
					SC8->C8_ALIIPI 	:= _nALIIPI
					SC8->C8_PRAZO  	:= _nPRAZO
					SC8->C8_COND   	:= _cCOND
	//				SC8->C8_OBS    	:= LEFT(_cOBS,30)	//ALTERADO PARA 30 ESTAVA 60 POSICOES
					SC8->C8_CONTATO	:= LEFT(_cCONTATO,15)
					SC8->C8_TPFRETE	:= "C"
	//				SC8->C8_VALFRE 	:= 0
					SC8->C8_VLDESC 	:= _nVLDESC
	//				SC8->C8_VALIPI 	:= _nVALIPI
				Endif
				MsUnlock()
				aItem := {}
				aAdd( aItem , SC8->C8_ITEM    )
				aAdd( aItem , SB1->B1_DESC )
				aAdd( aItem , TRANSFORM( SC8->C8_QUANT		,'@R 999,999.99'   ) )
				aAdd( aItem , TRANSFORM( SC8->C8_PRECO		,'@R 9,999,999.99' ) )
				aAdd( aItem , TRANSFORM( _nVLDESC			,'@R 9,999,999.99' ) )
				aAdd( aItem , TRANSFORM( SC8->C8_ALIIPI		,'@R 9,999,999.99' ) )
				aAdd( aItem , TRANSFORM( SC8->C8_VALIPI		,'@R 9,999,999.99' ) )
				aAdd( aItem , TRANSFORM( SC8->C8_PRAZO  	,'@R 9999' 	       ) )
				aAdd( aItem , TRANSFORM( _nQUANT * _nPRECO	,'@R 9,999,999.99' ) )
				aADD( aProd, aItem )

			ELSE
				CONOUT("Item: " + _cItem + " nao processado, verifique cotacao... ")
			ENDIF
		Endif
	NEXT
	oProcess:Finish() // FINALIZA O PROCESSO

	U_EnviaAV(_cNUM, _cFORNECE, _cLOJA, _cUser) // elementos do array: 	C8_NUM, C8_FORNECE, C8_LOJA

	RETURN

/*
�����������������������������������������������������������������������������
���Programa  �EnviaAV   �Autor  Marcilio/Daniel      � Data �  04/08/2012 ���
�������������������������������������������������������������������������͹��
���Uso       � Hope                                                       ���
�����������������������������������������������������������������������������
*/
User Function EnviaAV(_cNum, _cFornece, _cLoja, _cUser)

   	DbSelectArea("SA2")
   	DbSetOrder(1)
   	DbSeek(xFilial("SA2") + _cFornece + _cLoja)

	oProcess:= TWFProcess():New( "000003", "Aviso - Cotacao de Precos respondida" )
	oProcess:NewTask( "Aviso - Cotacao de Precos respondida", "\WORKFLOW\HTML\AVFORN_HOPE.HTM" )
	oProcess:nEncodeMime 	:= 0
	oProcess:cSubject 		:= "Aviso - Cotacao Eletr�nica de Precos No." + _cNum + " respondida."
	oProcess:cTo      		:= "daniel.souza@hopelingerie.com.br;marclio.piza@hopelingerie.com.br" ///// -> Mandar para o usuario que gerou a cotacao
	oProcess:NewVersion(.T.)

 	oHtml     				:= oProcess:oHTML

	oHtml:ValByName( "A2_NOME"		, SA2->A2_NREDUZ )
 	oHtml:ValByName( "C8_NUM"		, _cNum )
	oHtml:ValByName( "C8_FORNECE"   , SA2->A2_COD )
	oHtml:ValByName( "C8_LOJA"		, SA2->A2_LOJA )
	oHtml:ValByName( "datetime"     , DTOC(MSDATE()) + " �s " + left(time(),5) )

	oProcess:Start()

	Return

Static Function EnvMail(cEmail ,cAssunto, cMensagem, aAttach)

Local cEmailTo := ""							// E-mail de destino
Local cEmailBcc:= ""							// E-mail de copia
Local lResult  := .F.							// Se a conexao com o SMPT esta ok
Local cError   := ""							// String de erro
Local lRelauth := SuperGetMv("MV_RELAUTH")  //.T. //SuperGetMv("MV_RELAUTH")		// Parametro que indica se existe autenticacao no e-mail
Local lRet	   := .F.							// Se tem autorizacao para o envio de e-mail
Local cServer  := Rtrim(SuperGetMv("MV_RELSERV"))
Local cConta   := GetMV("MV_RELACNT") //ALLTRIM(cAccount)				// Conta de acesso
Local cSenhaTK := GetMV("MV_RELPSW") //ALLTRIM(cPassword)	        // Senha de acesso
Local cFrom    := cConta
//cConta	   :=	Iif(Empty(cConta),'lalberto@3lsystems.com.br',cConta)
//cSenhaTK	   :=	Iif(Empty(cSenhaTK),'iso9002infor',cSenhaTK)

//�����������������������������������������������������������������������������Ŀ
//�Envia o mail para a lista selecionada. Envia como BCC para que a pessoa pense�
//�que somente ela recebeu aquele email, tornando o email mais personalizado.   �
//�������������������������������������������������������������������������������

cEmailTo := cEmail
If At(";",cEmail) > 0 // existe um segundo e-mail.
	cEmailBcc:= SubStr(cEmail,At(";",cEmail)+1,Len(cEmail))
Endif

CONNECT SMTP SERVER cServer ACCOUNT cConta PASSWORD cSenhaTK RESULT lResult

// Se a conexao com o SMPT esta ok
If lResult

	// Se existe autenticacao para envio valida pela funcao MAILAUTH
	If lRelauth
		lRet := Mailauth(cConta,cSenhaTK)
	Else
		lRet := .T.
    Endif

	cAnexos:=''

	If lRet
		SEND MAIL FROM cFrom TO cEmailTo SUBJECT 	cAssunto		BODY    	cMensagem		RESULT lResult
					//danfe_000055024_000055024.pdf
//		ATTACHMENT  cAnexos  ;
		If !lResult
			//Erro no envio do email
			GET MAIL ERROR cError
			Help(" ",1,'Erro no Envio do Email',,cError+ " " + cEmailTo,4,5)	//Aten��o
		Else

 		    Help(" ",1,'Envio do Email com Sucesso',,"  Para " + cEmailTo,4,5)	//Aten��o

		Endif

	Else
		GET MAIL ERROR cError
		Help(" ",1,'Autentica��o',,cError,4,5)  //"Autenticacao"
		MsgStop('Erro de Autentica��o','Verifique a conta e a senha para envio') 		 //"Erro de autentica��o","Verifique a conta e a senha para envio"
	Endif

	DISCONNECT SMTP SERVER

Else
	//Erro na conexao com o SMTP Server
	GET MAIL ERROR cError
	Help(" ",1,'Erro no Envio do Email',,cError,4,5)      //Atencao
Endif

Return(lResult)

