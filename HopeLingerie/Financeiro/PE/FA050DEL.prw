#Include "Rwmake.ch"
#Include "Protheus.ch"
#INCLUDE "TBICONN.CH"
#Include "topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FA050DEL  �Autor  �Marcio Medeiros Junior� Data �  07/13/18 ���
�������������������������������������������������������������������������͹��
���Desc.     � Totalit                                                    ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function FA050DEL

lRet := .T.

DbSelectArea("SA2")
SA2->(DbSetOrder(1))
SA2->( DbSeek( xFilial("SA2") + SE2->E2_FORNECE + SE2->E2_LOJA ) )

DbSelectArea("SA3")
SA3->(DbSetOrder(3))
If SA3->( DbSeek( xFilial("SA3") + SA2->A2_CGC ) )
	cQuery := "UPDATE " + RetSqlName("SE3") + " SET E3_DATA = '' "
	cQuery += "WHERE E3_VEND = '" + SA3->A3_COD + "' AND E3_SERIE = '" + SE2->E2_PREFIXO + "' "
	cQuery += "AND E3_NUM = '" + SE2->E2_NUM + "' AND E3_PARCELA = '" + SE2->E2_PARCELA	+ "' "
	cQuery += "AND D_E_L_E_T_ = ''"
					
	If TcSqlExec(cQuery) > 0
		MsgAlert("Erro na atualiza��o do SE3!!")
		lRet := .F.
	Endif   
	
Endif
Return lRet