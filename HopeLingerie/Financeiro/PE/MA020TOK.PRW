#include "RWMAKE.CH"
#include "TOPCONN.CH"
#Include "PROTHEUS.CH"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MA020TOK  �Autor  �DANIEL       � Data �  15/12/2016 ���
�������������������������������������������������������������������������͹��
���Desc.     �PEs executados na chamada da Inclusao/alteracao/Exclus�o    ���
���          �no Cadastro de fornecedores para inclus�o do Item Cont�bil. ���
���          �															  ���
�������������������������������������������������������������������������͹��
���Uso       �(PEs) MA020TOK - Cadastro de fornecedores                 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User function MA020TOK ()
Local cForParam:= Alltrim(SuperGetMV("HP_CONTFOR",.F.,"2"))
//Local aAreaCTD := CTD->(GetArea())

dbSelectArea("CTD")
CTD->(dbSetOrder(1))
CTD->(dbGoTop())

If !CTD->(Dbseek(xFilial("CTD")+cForParam+M->A2_COD+M->A2_LOJA))
	RecLock("CTD",.T.)
	CTD->CTD_FILIAL	:= xFilial("CTD")					//Filial		
	CTD->CTD_ITEM	:= cForParam+M->A2_COD+M->A2_LOJA			 		//Item Conta		
	CTD->CTD_CLASSE	:= '2'								//Classe				1=Sintetico;2=Analitico
	CTD->CTD_NORMAL	:= '1'								//Cond Normal			0=Nenhum;1=Despesa;2=Receita
	CTD->CTD_DESC01	:= M->A2_NOME						//Desc Moeda 1		
	CTD->CTD_BLOQ	:= "2"								//Item Bloq				1=Bloqueado;2=N�o Bloqueado
	CTD->CTD_DTEXIS	:= cToD("01/01/2000")				//Dt Ini Exist		
	CTD->(MsUnlock())
EndIf

Return(.t.)