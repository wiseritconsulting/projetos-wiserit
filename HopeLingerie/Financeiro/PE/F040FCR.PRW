#Include "PROTHEUS.CH"
/*/{Protheus.doc} F040FCR 
Fun��o de Teste
@Desc Grava��es adicionais em T�tulos a Receber
@type function
@author R. Melo 
@since 16/10/2018
@version 1.0
/*/
USER FUNCTION F040FCR()
Local aAreaSA1:= SA1->(GetArea())
Local cCodMil := ""
Local cCgc    := ""
Local cNome   := ""
Local cUf     := ""
Local cMregiao:= ""
Local cRepres := "" 
Local cTypeSE1:= SuperGetMV("HP_TPZAX",.F.,"NCC")

DbSelectArea('SA1')
SA1->(Dbgotop())
SA1->(DbSetOrder(1)) 
IF SA1->(DbSeek(xFilial("SA1")+SE1->E1_CLIENTE+SE1->E1_LOJA))
	cCodMil := SA1->A1_XCOD 
	cCgc    := SA1->A1_CGC  
	cNome   := SA1->A1_NOME
	cUf     := SA1->A1_EST
	cMregiao:= SA1->A1_XMICRDE
	cRepres := SA1->A1_VEND    
ELSE
	cCodMil := "N/Encontrado"
	cCgc    := ""
ENDIF

If SE1->E1_TIPO $ cTypeSE1 
	ZAX->(Reclock("ZAX",.T.))
	ZAX->ZAX_FILIAL := xFilial("ZAX") 
	ZAX->ZAX_CLINFE := cNome
	ZAX->ZAX_DTAREG := dDatabase 
	ZAX->ZAX_NFENTR := SE1->E1_NUM 
	ZAX->ZAX_DTEMIS := SE1->E1_EMISSAO
	ZAX->ZAX_ASSIST := ""
	ZAX->ZAX_CODCLI := SE1->E1_CLIENTE
	ZAX->ZAX_LOJCLI := SE1->E1_LOJA
	ZAX->ZAX_UF     := cUf
	ZAX->ZAX_MREGIA := cMregiao 
	ZAX->ZAX_REPRES := cRepres
	ZAX->ZAX_VOLUME := 0
	ZAX->ZAX_VALOR  := SE1->E1_VALOR 
	ZAX->ZAX_TRANSP := ''
	ZAX->ZAX_FRETE  := ''
	ZAX->ZAX_SERIE  := SE1->E1_PREFIXO 
	ZAX->ZAX_STATUS := '04'
	ZAX->ZAX_OBSERV := SE1->E1_HIST
	ZAX->(MsUnlock())
Endif

Return