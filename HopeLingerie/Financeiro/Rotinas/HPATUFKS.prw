#Include 'Protheus.ch'
USER Function HPATUFKS()
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
/*/
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪目北
北矲un噭o    矯UBEM020   � Rev   � Geraldo Sabino        � Data �04.01.2017 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪拇北
北�          砇otina para ajuste do erro com a Reconc Automatica  FK5 Divs 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪拇北
北砈intaxe   矯UBM020                                                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪拇北
北砅arametros�                                                             潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪拇北
北砇etorno   砃enhum                                                       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪拇北
北矰escri噭o 矱m ocorrencia em Paralelo, erros com a reconciliacao automati潮�
北�          砪a onde estamos sendo obrigado manualmente a limpar campos do潮�
北�          砈E5-Campos com link da tabela FKS (NOVAS DA TOTVS)           潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪拇北
北砋so       � Materiais                                                   潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁北
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
USER Function HPATUFA()
Private aParamBox := {}
Private aRet      := {}
Private cCadastro := "Ajusta Reconciliacao"
Private cAlias    := "SE5"
Private aCampos   :={}

aAdd(aParamBox,{1,"Data Mov  de ",Ctod(Space(8)),"","","","",50,.F.})
aAdd(aParamBox,{1,"Data Mov at� ",Ctod(Space(8)),"","","","",50,.F.})

aAdd(aParamBox,{1,"Banco      ",Space(3)                   ,"","","","",0,.F.})
aAdd(aParamBox,{1,"Agencia    ",Space(len(SA6->A6_AGENCIA)),"","","","",0,.F.})
aAdd(aParamBox,{1,"Conta      ",Space(len(SA6->A6_NUMCON)) ,"","","","",0,.F.})

If !ParamBox(aParamBox, "Ajusta Reconciliacao", aRet)
 Return Nil
EndIf


cQuery := "UPDATE "+RetSqlName("SE5")+" SET E5_MOVFKS='',E5_IDORIG='',E5_TABORI = ''  "
cQuery += "WHERE E5_FILIAL='"+xFilial("SE5")+"' AND  E5_DATA BETWEEN '"+DTOS(aRet[1])+"' AND '"+DTOS(aRet[2])+"' "
cQuery += " AND  E5_BANCO = '"+aRet[3]+"' AND E5_AGENCIA = '"+aRet[4]+"' AND E5_CONTA = '"+aRet[5]+"'  "
cQuery += "AND D_E_L_E_T_=' ' "

cAuxMsg  :=""
nResult  := tcSQLExec(cQuery)
If nResult < 0
 ALERT(" Erro de Aplicacao da Reconciliacao (1) -> "+TCSQLError()+" de Update SE5 ")
Else
 Alert("Processamento ok  Reconciliacao 1")
EndIf

cQuery := "UPDATE "+RetSqlName("SE5")+" SET  E5_VENCTO = E5_DATA   "
cQuery += "WHERE E5_FILIAL='"+xFilial("SE5")+"' AND  E5_DATA BETWEEN '"+DTOS(aRet[1])+"' AND '"+DTOS(aRet[2])+"' "
cQuery += " AND  E5_BANCO = '"+aRet[3]+"' AND E5_AGENCIA = '"+aRet[4]+"' AND E5_CONTA = '"+aRet[5]+"'  "
cQuery += " AND D_E_L_E_T_=' ' AND E5_VENCTO =' '  "

cAuxMsg  :=""
nResult  := tcSQLExec(cQuery)
If nResult < 0
 ALERT(" Erro de Aplicacao da Reconciliacao(2) -> "+TCSQLError()+" de Update SE5 ")
Else
 Alert("Processamento ok Reconciliacao 2")
EndIf

Alert("Executar� o Migrador Reconcilia玢o  !")
finxse5()
Alert("Finalizado o Migrador ! SE5 ")

Return

    

USER Function HPATUFB()
Private aParamBox := {}
Private aRet      := {}
Private cCadastro := "Ajusta Contabil FKA "
Private cAlias    := "SE5"
Private aCampos   :={}

aAdd(aParamBox,{1,"Data Mov  de ",Ctod(Space(8)),"","","","",50,.F.})
aAdd(aParamBox,{1,"Data Mov at� ",Ctod(Space(8)),"","","","",50,.F.})

If !ParamBox(aParamBox, "Ajusta Contabil FKA? ", aRet)
 Return Nil
EndIf


cQuery := "UPDATE "+RetSqlName("SE5")+" SET E5_MOVFKS='',E5_IDORIG='',E5_TABORI = ''  "
cQuery += "WHERE E5_FILIAL='"+xFilial("SE5")+"' AND  E5_DATA BETWEEN '"+DTOS(aRet[1])+"' AND '"+DTOS(aRet[2])+"' "
cQuery += "AND D_E_L_E_T_=' ' "

cAuxMsg  :=""
nResult  := tcSQLExec(cQuery)
If nResult < 0
 ALERT(" Erro de Aplicacao ajuste FKA-Contabil -> "+TCSQLError()+" de Update SE5 ")
Else
 //Alert("Processamento ok ajuste FKA-Contabil" )
EndIf

cQuery := "UPDATE "+RetSqlName("FKA")+" SET  D_E_L_E_T_= '*' ,R_E_C_D_E_L_ = R_E_C_N_O_ "
cQuery += "WHERE FKA_FILIAL='"+xFilial("FKA")+"' "
cQuery += " AND D_E_L_E_T_=' '  "

cAuxMsg  :=""
nResult  := tcSQLExec(cQuery)
If nResult < 0
 ALERT(" Erro de Aplicacao de Ajuste FKA - Contabil -> "+TCSQLError()+" de Update FKA ")
Else//
 Alert("Aplica玢o ok de Ajuste FKA " )
EndIf                                                  

//Alert("Executar� o Migrador AJUSTE FKA !")
finxse5()
//Alert("Finalizado o Migrador FKA !")
Return
Return


