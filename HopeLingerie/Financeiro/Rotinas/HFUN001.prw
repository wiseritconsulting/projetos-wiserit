#INCLUDE "RWMAKE.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWIZARD.CH"
#INCLUDE "FILEIO.CH"
#INCLUDE "APWEBSRV.CH"
#include "TbiConn.ch"
#include "TbiCode.ch"
#include "topconn.ch"
#Include 'ApWebEx.ch'
#INCLUDE "AP5MAIL.CH"
//FUN��O para envio de arquivos xml de Notas Fiscais, para FTP das Lojas Hope
//           Espec�fico HOPE
//           Daniel Pereira de Souza   -    20/05/2018
User Function HFUN001()
Local lMenu     := .f.
Local aRetDir   := {}
Local _Ftpsrv   := ""
Local _Ftppor   := ""
Local _Ftpusr   := ""
Local _Ftppwd   := ""
Local _Numten   := ""
Local lNot      := .f.
Local lYes      := .t.
Local lConnec   := .f.
Local cHoraFim  := ""
Local cHoraIni  := Time()
Local cCnpj     := ""
Local cQuery    := ""
Local cData     := ""
Local cDirFtp   := ""
Local cDirLoc   := "\TEMP\"
Local cArqXmlA  := ""
Local cArqXmlb  := ""
Local cXmlDados := ""
Local nConTss   := -1
Local cDbCfg    := "MSSQL/TSS"    //GetMv("HP_DBSQNM")//Tipodobanco/nomedobanco
Local cTopServer:= "192.168.200.5" //GetMv("HP_DBSQID")//IP do topserver
Local nPorta    := 7990          //GetMv("HP_DBPort")//Porta do dbAccess
Local nConERP   := AdvConnection()   // conex�o Atual (normal) BAnco TMPRD
Local cQryzb4   := "SELECT SUBSTRING(ZB4_DIRXML,1,6)+'\' AS _DIR, A1_CGC AS _CNPJ   FROM TMPRD.dbo.ZB4010 ZB4 , TMPRD.dbo.SA1010 SA1 WITH (NOLOCK) WHERE ZB4_CODIGO+ZB4_LOJA = A1_COD+A1_LOJA AND ZB4.D_E_L_E_T_ = '' AND SA1.D_E_L_E_T_ = ''"

TCSetConn(nConERP)
//---------- PREPARA��O DO AMBIENTE ----------//
If Select("SX2") <> 0
	lMenu := .T.
Else
	Prepare Environment Empresa "01" Filial "0101"
Endif
U_criamv("HP_FTPSRV1")  // Servidor FTP
U_criamv("HP_FTPPOR1")  // Porta    FTP
U_criamv("HP_FTPUSR1")  // Usuario  FTP
U_criamv("HP_FTPPWD1")  // Senha    FTP
U_criamv("HP_NUMTEN1")  // Tentativas

_Ftpsrv := GetMV("HP_FTPSRV1")
_Ftppor := GetMV("HP_FTPPOR1")
_Ftpusr := GetMV("HP_FTPUSR1")
_Ftppwd := GetMV("HP_FTPPWD1")
_Numten := GetMV("HP_NUMTEN1")
cData   := '20180607' // '20180614'
	For nX := 1 To  10 // _Numten
		//	If FTPConnect(_Ftpsrv ,_Ftppor  ,_Ftpusr  ,_Ftppwd  )
		If FTPConnect('192.168.100.27' ,21  ,'protheus'  ,'hope18*' )
			//FTPConnect(cFTPServ,nFTPPorta, cFTPUser, cFTPPass )
			FTPSetPasv( .t. )
			FTPSetType( 1 )
			aRetDir := FTPDIRECTORY ( "*.*" , "D")
			lRet  := .T.
			lConnec := .T.
			Exit
		EndIf
		//u_MsgConMon( 'Falhou ' + Alltrim( Str( nX, 2 ) ) + 'a. tentativa de conecao com FTP',,.F. ) //'Falhou '###'a. tentativa de conecao com FTP'
		Sleep( 5000 )
	Next
	//lConnec:=FTPConnect(_Ftpsrv,_Ftppor,_Ftpusr,_Ftppwd,.f.)

					//USE SPED050 ALIAS SPED050T SHARED NEW VIA "TOPCONN"
					TCCONTYPE('TCPIP')    //Linka com Outro Banco.
					conout("Iniciando LINK - Conectando com Sql do TSS")
					nConTss := TCLink(cDbCfg,cTopServer,nPorta)
					If nConTss < 0
						conout("Erro de Conex�o Link SQL tss")
		   				Return
					Endif
					TCSetConn(nConTSS)

If Select("TZB4") <> 0
	dbSelectArea("TZB4")
	TZB4->(dbCloseArea())
EndIf
dbUseArea(.T.,"TOPCONN",TCGenQry(,,cQryzb4),"TZB4",.F.,.T.)
TZB4->(DBGOTOP())
If TZB4->(!Eof())
	//lConnec:=FTPConnect(_Ftpsrv,_Ftppor,_Ftpusr,_Ftppwd,.f.)
	if lConnec  // .AND. nConTss >= 0
		MsgStop('Consegui ....  Connectar')
		dbSelectArea("TZB4")
		TZB4->(DBGOTOP())
		do WHILE TZB4->(!EOF())
			cCnpj   := TZB4->_CNPJ
			TCSETCONN(nConTss) //Preserva Link TSS.
			cQuery  := "SELECT R_E_C_N_O_ AS REC FROM SPED050 WITH (NOLOCK) WHERE CNPJDEST = '"+cCnpj+"' AND STATUS = 6 AND DATE_NFE = '"+cData+"'   AND D_E_L_E_T_ = '' "
			If Select("TSPED") <> 0
				dbSelectArea("TSPED")
				TSPED->(dbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TCGenQry(,,cQuery),"TSPED",.F.,.T.)
			TSPED->(DBGOTOP())
			If TSPED->(!Eof())
				do while TSPED->(!Eof())
					TCSetConn(nConTSS)
					DBSELECTAREA("SPED050")
					SPED050->(DBGOTO(TSPED->REC))
					cDirFtp := "\"+TZB4->_DIR   //'000216_MT_PANTANAL_SHOPPING'
					cXmlDados := SPED050->XML_SIG
					cArqXmla   :=Decode64(SPED050->XML_SIG)   // Encode64()
					cArqXmlb   :=Encode64(SPED050->XML_SIG)   // Encode64()
					cArqXmlA   := cDirLoc+TZB4->_DIR+"_"+SubStr(SpedNfeId(SPED050->XML_SIG,"Id"),4,44)+"-nfe.xml"
					cArqXmlB   := cDirLoc+TZB4->_DIR+"_"+SpedNfeId(SPED050->XML_SIG,"Id")+"-nfe.xml"
					nHdlXmla   := FCreate(cArqXmlA,0)
					nHdlXmlb   := FCreate(cArqXmlB,0)
					msgStop("Irei fazer ")
					If nHdlXmla > 0 .And. nHdlXmlb > 0
						FWrite(nHdlXmla,cXmlDados)
						FWrite(nHdlXmlb,SPED050->XML_SIG)
						FClose(nHdlXmla)
						FClose(nHdlXmlb)
						//Se n�o conseguir dar o upload do primeiro arquivo
						If !FTPUpload(cDirFtp, cArqXmla)
							//Realiza mais uma tentativa
							If !FTPUpload(cDirFtp, cArqXmla)
								lRet:=.F.
							EndIf
						EndIf
						//Se n�o conseguir dar o o upload do segundo arquivo
						If !FTPUpload(cDirFtp, cArqXmlb)
							//Realiza mais uma tentativa
							If !FTPUpload(cDirFtp, cArqXmlb)
								lRet:=.F.
							EndIf
						EndIf
					Endif
					DBSELECTAREA("TSPED")
					TSPED->(DBSKIP())
				Enddo
			endif
			TCSetConn(nConERP)  //retorna conexao para o banco do Microsiga
			DBSELECTAREA("TZB4")
			TZB4->(DBSKIP())
		Enddo
        iF nConTss > 0
            TcUnlink(cDbCfg,cTopServer,nporta)
        eNDIF
		if FTPDisconnect()
			MsgStop('E Tambem  Consegui ....  DESConnectar')
		Endif
	Endif
endif

If lMenu
	cHoraFim := Time()
	
	MsgStop("Termino do envio FTP "+cHoraIni+" - "+cHoraFim ,"Aten��o")
Else
	Reset Environment
Endif
Return
Static Function NfeProcNfe(cXMLNFe,cXMLProt,cVersao,cNFMod)

Local aArea     := GetArea()
Local nAt       := 0
Local nAy		:= 0
Local cXml      := ""
Local lDistrCanc:=.F.

cNFMod := IIf(Empty(cNFMod),"55",cNFMod)

//��������������������������������������������������������������Ŀ
//� Montagem da mensagem                                         �
//����������������������������������������������������������������
nAt := At("?>",cXmlProt)
If nAt > 0
	nAt +=2
Else
	nAt := 1
EndIf
Do Case
	Case cNFMod == "57"
		If !Empty(cXMLNFe)
			cXml := '<?xml version="1.0" encoding="UTF-8"?>'
			Do Case
				Case cVersao >= "1.03"
					cXml += '<cteProc xmlns="http://www.portalfiscal.inf.br/cte" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.portalfiscal.inf.br/cte procCTe_v1.00.xsd" versao="1.03">'
				OtherWise
					cXml += '<cteProc xmlns="http://www.portalfiscal.inf.br/cte" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.portalfiscal.inf.br/cte procCTe_v1.00.xsd" versao="'+cVersao+'">'
			EndCase
			cXml += cXmlNFe
		Else
			cXml := ""
		EndIf
		Do Case
			Case "retConsSitCTe" $ cXmlProt
				cXml += StrTran(SubStr(cXmlProt,nAt),"retConsSitCTe","protCTe")
			Case "retCancCTe" $ cXmlProt
				cXml += StrTran(SubStr(cXmlProt,nAt),"retCancCTe","protCTe")
			Case "retInutCTe" $ cXmlProt
				cXml += StrTran(SubStr(cXmlProt,nAt),"retInutCTe","protCTe")
			Case "protCTe" $ cXmlProt
				cXml += cXmlProt
			OtherWise
				cXml += "<protCTe>"
				cXml += cXmlProt
				cXml += "</protCTe>"
		EndCase
		If !Empty(cXMLNFe)
			cXml += '</cteProc>
		EndIf
	OtherWise
		If !Empty(cXMLNFe)
			cXml := '<?xml version="1.0" encoding="UTF-8"?>'
			Do Case
				Case cVersao <= "1.07"
					cXml += '<nfeProc xmlns="http://www.portalfiscal.inf.br/nfe" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.portalfiscal.inf.br/nfe procNFe_v1.00.xsd" versao="1.00">'
				Case cVersao >= "2.00" .And. "cancNFe" $ cXmlNfe
					cXml += '<procCancNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="'+cVersao+'">'
					lDistrCanc:= .T.
				OtherWise
					cXml += '<nfeProc xmlns="http://www.portalfiscal.inf.br/nfe" versao="'+cVersao+'">'
			EndCase
			cXml += cXmlNFe
		Else
			cXml := ""
		EndIf
		Do Case
			Case "retConsSitNFe" $ cXmlProt	 .And. cVersao<"2.00"
				cXmlProt := GeraProtNF(cXmlProt,cVersao,"retConsSitNFe")
				cXml += cXmlProt
			Case "retCancNFe" $ cXmlProt .And. cVersao<"2.00"
				cXml += StrTran(SubStr(cXmlProt,nAt),"retCancNFe","protNFe")
			Case "retInutNFe" $ cXmlProt .And. cVersao<"2.00"
				cXml += StrTran(SubStr(cXmlProt,nAt),"retInutNFe","protNFe")
			Case "protNFe" $ cXmlProt .And. cVersao<"2.00"
				cXmlProt:= GeraProtNF(cXmlProt,cVersao,"retInutNFe")
				cXml += cXmlProt
			Case "PROTNFE" $ Upper(cXmlProt) .And. cVersao>="2.00"
				nAt:= At("<PROTNFE",Upper(cXmlProt))
				nAy:= RAt("</PROTNFE>",Upper(cXmlProt))
				cXmlProt:= SubStr(cXmlProt,nAt,nAy-nAt+10)
				cXmlProt:= GeraProtNF(cXmlProt,cVersao,"PROTNFE")
				cXml += cXmlProt
			Case "RETCANCNFE" $ Upper(cXmlProt) .And. cVersao>="2.00"
				cXmlProt:= GeraProtNF(cXmlProt,cVersao,"RETCANCNFE")
				cXml += cXmlProt
			Case "RETINUTNFE" $ Upper(cXmlProt) .And. cVersao>="2.00"
				cXml += StrTran(SubStr(cXmlProt,nAt),"RETINUTNFE","protNFe")
			Case "RETCONSSITNFE" $ Upper(cXmlProt)	 .And. cVersao>="2.00"
				cXml += StrTran(SubStr(cXmlProt,nAt),"RETCONSSITNFE","protNFe")
			OtherWise
				cXml += "<protNFe>"
				cXml += cXmlProt
				cXml += "</protNFe>"
		EndCase
		If cVersao < "2.00"
			nAt := At("versao=",cXml)
			cXml := StrTran(cXml,SubStr(cXml,nAt,13),'versao="'+cVersao+'"',,1)
		EndIf
		If !Empty(cXMLNFe)
			If lDistrCanc
				cXml += '</procCancNFe>'
			Else
				cXml += '</nfeProc>'
			EndIf
		EndIf
EndCase
RestArea(aArea)
Return(AllTrim(cXml))

Static Function SpedNfeId(cXML,cAttId)
Local nAt  := 0
Local cURI := ""
Local nSoma:= Len(cAttId)+2

nAt := At(cAttId+'=',cXml)
cURI:= SubStr(cXml,nAt+nSoma)
nAt := At('"',cURI)
If nAt == 0
	nAt := At("'",cURI)
EndIf
cURI:= SubStr(cURI,1,nAt-1)
Return(cUri)

Static Function TSSXmlParser( cXml, cExp, cAviso, cErro )

Local oXml

DEFAULT cExp	:= ""
DEFAULT cAviso	:= ""
DEFAULT cErro	:= ""

/*Retira e valida algumas informa��es e caracteres indesejados para o parse*/
cXml := XmlClean(cXml)

/*Faz o parser do XML*/
oXml := XmlParser(cXml,"_",@cAviso,@cErro)

Return oXml

Static Function XmlClean( cXml )

Local cRetorno		:= ""

DEFAULT cXml		:= ""

If ( !Empty(cXml) )
	/*Retira caractere '&' substitui por '&amp;'*/
	cRetorno := StrTran(cXml,"&","&amp;amp;")
EndIf

Return cRetorno

