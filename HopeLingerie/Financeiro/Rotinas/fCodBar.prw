#INCLUDE "PROTHEUS.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} fCodBar
Preenche cod barras a partir da digitacao do lin digitavel boleto

@author		TOTALIT Soutions - Andre R. Esteves	
@since		12/05/2018
@version	P12
@obs		
/*/      
//-------------------------------------------------------------------

User Function fCodBar(cCodBar)
Local lOK := .F.
Local cSTR1, cSTR2, cSTR3, cSTR4, cSTR5, cSDV1, cSDV2, cSDV3, nBARVLR, dBARVCT , cRet
Local lRet := .T.,;
		nX			  ,;
		cCampo     ,;
		nRet := 0

    cCodBar := Alltrim(cCodBar)
    cLinDig := Alltrim(cCodBar)

// Completa o tamanho do codigo de barras se ele for menor que 44 por se tratar de uma
// linha digitavel.
If Len(cCodBar) < 44
	cCodBar := Left(cCodBar+Replicate("0", 48-Len(cCodBar)),47)
Endif

Do Case
Case Len(cCodBar)==44 // Validacao do codigo de Barras
	// Boletos
	nRet := 0
	// Se nao conseguir validar o DV do codigo de barras, tenta validar como
	// se fosse titulo de concessionaria.	
	If Dv_BarCode(Left(cCodBar,4)+SubStr(cCodBar,6))!=SubStr(cCodBar,5,1)
		nRet := 1 // Codigo de barras de concessionarias
		// Concessionarias
		If Mod10(Left(cCodBar,3)+SubStr(cCodBar,5))!=SubStr(cCodBar,4,1)
			lRet := .F. // Invalido
		Endif	
	Endif
   //	cCodBar := Dv_BarCode
Case Len(cCodBar)==47 // Validacao da linha digitavel 

	cSTR1 := SubStr(cCodBar,01,04) 							   		  		//PARTE 1 LD (1 CB) - 01A04
	cSTR2 := SubStr(cCodBar,05,05) 							   		  		//PARTE 2 LD (3 CB) - 05A09
	cSTR3 := SubStr(cCodBar,11,10) 									  		//PARTE 4 LD (4 CB) - 11A20
	cSTR4 := SubStr(cCodBar,22,10) 									  		//PARTE 6 LD (5 CB) - 22A31
	cSTR5 := SubStr(cCodBar,33,15) 									  		//PARTE 8 LD (2 CB) - 33A47
	cSDV1 := SubStr(cCodBar,10,01) 									  		//PARTE 3 LD DIGITO 1 - 10A10
	cSDV2 := SubStr(cCodBar,21,01) 									 		//PARTE 5 LD DIGITO 2 - 21A21
	cSDV3 := SubStr(cCodBar,32,01) 									 		//PARTE 7 LD DIGITO 3 - 32A32
	nSDV1 := CALCULADV(cSTR1+cSTR2,10,2,1)  								//CALCULA 1 DIGITO VERIFICADOR DA LD
	nSDV2 := CALCULADV(cSTR3,10,2,1)  								  		//CALCULA 2 DIGITO VERIFICADOR DA LD
	nSDV3 := CALCULADV(cSTR4,10,2,1)  								  		//CALCULA 3 DIGITO VERIFICADOR DA LD

	//VERIFICA SE A LINHA DIGITAVEL E VALIDA
	lOk := Iif(Val(cSDV1)==nSDV1 .And. Val(cSDV2)==nSDV2 .And. Val(cSDV3)==nSDV3,.T.,.F.)


	//AJUSTA PARTE 8 LD (2 CB) PARA CODIGO DE BARRAS NO FORMATO ANTIGO
	cSTR5   := SubStr(cSTR5,1,1)+Transform(StrZero(Val(AllTrim(SubStr(cSTR5,2,14))),14),"99999999999999")
	cCodBar := cSTR1+cSTR5+cSTR2+cSTR3+cSTR4   								//MONTA CODIGO DE BARRAS ATRAVES DA LINHA DIGITAVEL

OtherWise // Validacao da linha digitavel de concessionarias
	// Elimina os digitos
	cCodSemDv := Left(cCodBar,11)+SubStr(cCodBar,13,11)+SubStr(cCodBar,25,11)+SubStr(cCodBar,37,11)
	// Calcula os digitos e os compara com os digitos informados
	For nX := 1 To Len(cCodSemDv) Step 11
		cCampo := SubStr(cCodSemDv,nX,11)
		nPos :=  If(nX==1,12,If(nX==12,24,If(nX==23,36,48)))
		If VldMod(cCampo,cCodBar) != SubStr(cCodBar,nPos,1)
			Help(" ",1,"INVCDBAR",,OemToAnsi('Codigo de Barras Inv�lido.'),1,0) 
			lRet := .F.
				Exit
		Endif
	Next
EndCase

If lRet
	M->E2_CODBAR := cCODBAR	// Variavel retornando o conteudo do Cod. Barra conforme a quantidade de digitos.
EndIf	

Return (lRet)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Rotina    �Dv_BarCode�Autor  �Claudio D. de Souza � Data �  14/12/01   ���
�������������������������������������������������������������������������͹��
���Desc.     �Calcula o digito verificador de um codigo de barras padrao  ���
���          �Febraban.                                                   ���
�������������������������������������������������������������������������͹��
���Uso       � CodBarVl2                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function DV_BarCode( cBarCode )
Local cDig     ,;
      nPos     ,;
      nAux := 0

    For nPos := 1 To 43
        nAux += Val(SubStr(cBarCode,nPos,1)) * If( nPos<= 3, ( 5-nPos),     ;
                                                If( nPos<=11, (13-nPos),     ;
                                                If( nPos<=19, (21-nPos),     ;
                                                If( nPos<=27, (29-nPos),     ;
                                                If( nPos<=35, (37-nPos),     ;
                                                               (45-nPos) )))))
    Next
    nAux := nAux % 11
    cDig := If( (11-nAux)>9, 1, (11-nAux) )
    
Return StrZero( cDig, 1 )
/*����������������������������Ŀ
  �CALCULA O DIGITO VERIFICADOR|
  ������������������������������*/
  
Static Function CALCULADV(cSTRING,nMODULO,nBASMAX,nBASMIN)

Local nMULT := 2
Local nACUM := 0
Local cSEQU := AllTrim(cSTRING)
Local nDIGI, nCONT, nCHAR, cCHAR

For nCONT := Len(cSEQU) To 1 Step -1
	cCHAR := SubStr(cSEQU,nCONT,1)  							//SEPARA CADA CARACTER PARA CALCULO, DO FIM PARA O INICIO

	If IsAlpha(cCHAR)  									   		//VERIFICA SE EXISTE CARACTER ALFANUMERICO NA STRING
		Help(" ",1,"ONLYNUM")
		Return(.F.)
	EndIf
	nCHAR := Val(cCHAR)*nMULT  									//MULTIPLICA FATOR PELO VALOR CORRENTE

	If nMODULO == 10
		nACUM := Iif(nCHAR>9,nACUM+nCHAR-9,nACUM+nCHAR)  		//ACUMULA RESULTADOS PARA MODULO 10
	Else
		nACUM := nACUM+nCHAR  									//ACUMULA RESULTADOS PARA MODULOS DIFERENTE DE 10
	EndIf
	nMULT := Iif(nMULT==nBASMAX,nBASMIN,nMULT+1)  				//INICIALIZA FATOR DE MULTIPLICACAO CONFORME PARAMETROS PARA CADA MODULO

Next
nDIGI := nMODULO-Mod(nACUM,nMODULO)  							//SUBTRAI RESTO DA DIVISAO PELO VALOR DO MODULO
nDIGI := Iif(nMODULO==10 .And. nDIGI>9,0,nDIGI)  				//PARA MODULO 10 E DIGITO MAIOR QUE 9 UTILIZA DIGITO 0
nDIGI := Iif(nMODULO==11 .And. nDIGI<2 .Or. nDIGI>9,1,nDIGI)	//PARA MODULO 11 E DIGITO MENOR QUE 2 E MAIOR QUE 9 UTILIZA DIGITO 1


Return(nDIGI)





/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Rotina    �Mod10     �Autor  �Claudio D. de Souza � Data �  14/12/01   ���
�������������������������������������������������������������������������͹��
���Desc.     �Calcula o digito verificador de uma sequencia de numeros    ���
���          �baseando-se no modulo 10. Utilizado para verificar o digito ���
���          �em linhas digitaveis e codigo de barras de concessionarias  ���
���          �de servicos publicos                                        ���
�������������������������������������������������������������������������͹��
���Uso       � CodBarVl2                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function Mod10( cNum )
Local nFor    := 0,;
      nTot    := 0,;
      cNumAux

   If Len(cNum)%2 #0
      cNum := "0"+cNum
   EndIf

   For nFor := 1 To Len(cNum)
      If nFor%2 == 0
         cNumAux := StrZero(2 * Val(SubStr(cNum,nFor,1)), 2)
      Else
         cNumAux := StrZero(Val(SubStr(cNum,nFor,1))    , 2)
      Endif
      nTot += ( Val(LEFT(cNumAux,1)) + Val(Right(cNumAux,1)) )
   Next

   nTot := nTot % 10
   nTot := If( nTot#0, 10-nTot, nTot )

Return StrZero(nTot,1)                      
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Rotina    �VldMod    �Autor  �Adrianne Furtado    � Data �  28/08/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �Calcula o digito verificador de uma sequencia de numeros    ���
���          �verificando qual o modulo do padr�o FEBRABAN deve ser       ���
���          �utilizado                                                   ���
�������������������������������������������������������������������������͹��
���Uso       � CodBarVl2                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VldMod( cNum , cCodBar)
Local cCodSub3 := SubStr(cCodBar,3,1)
Local cRet := ""
If cCodSub3 == '6' .or. cCodSub3 == '7' 
	cRet :=Mod10(cNum)
ElseIf cCodSub3 == '8' .or. cCodSub3 == '9'
	cRet :=Mod11(cNum)
EndIf
Return cRet
