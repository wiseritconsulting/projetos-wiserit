#Include "Rwmake.ch"

User Function HFINA001
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFINA001  �Autor  � Mauro Nagata       � Data �  20/04/17   ���
�������������������������������������������������������������������������͹��
���Desc.     � Leitura de arquivo TXT e gera��o de fatura                 ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Hope                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
        
Local oDlgA
Private cString := "SE2"
Private cPerg   := "HFINA001  "

AjustaSX1()

// +-----------------------------------+
// | Montagem da Tela de Processamento |
// +-----------------------------------+

@ 200,001 to 380,380 Dialog oDlgA Title OemToAnsi("Importa��o CTE x Fatura")
@ 022,030 to 050,162
@ 029,038 Say " Este Programa far� a leitura de um arquivo Texto"
@ 038,038 Say " para gerar a fatura. "

@ 070,098 BmpButton Type 5 Action Pergunte(cPerg,.t.)
@ 070,128 BmpButton Type 1 Action Proc001()
@ 070,158 BmpButton Type 2 Action Close(oDlgA)

Activate Dialog oDlgA Centered

Return
    

Static Function Proc001

Private nHdl	
Private cEol := "Chr(13) + Chr(10)"

Pergunte(cPerg,.F.)

cDirFile  := MV_PAR01
nRatFile  := Rat("\",cDirFile)
cFileImp  := AllTrim(Substr(cDirFile,nRatFile+1,Len(cDirFile)))
nHdl 	     := fOpen(MV_PAR01,68)  													//arquivo a ser importtado
cPrefFat  := If(Empty(MV_PAR02),"FAT",MV_PAR02)								//definicao do prefixo da fatura
cNumFat   := MV_PAR03																	//definicao do numero da fatura
cTipoFat  := If(Empty(MV_PAR04),"NF ",MV_PAR04)								//definicao do tipo da fatura
cFornFat  := If(Empty(MV_PAR05),"",MV_PAR05)									//definicao do codigo do fornecedor da fatura
cLojaFat  := If(Empty(MV_PAR05).Or.Empty(MV_PAR06),"",MV_PAR06)	//definicao da loja da fatura
cNat      := If(Empty(MV_PAR07),"21611001  ",MV_PAR07)					//definicao da natureza da fatura
dDtDe     := If(Empty(MV_PAR09),Ctod("01/01/2001"),MV_PAR08)			//definicao do inicio da data de emissao do titulo
dDtAte    := If(Empty(MV_PAR09),Ctod("31/12/2049"),MV_PAR09)   	//definicao do final da data de emissao do titulo
cCondPgto := MV_PAR10

If Empty(cEol)
	cEol := Chr(13) + Chr(10)
Else
	cEol := Trim(cEol)
	cEol := &cEol
EndIf

If nHdl == -1
	MsgAlert("O Arquivo de Nome " + MV_PAR01 + " n�o pode ser aberto! Verifique os par�metros.","Aten��o!")
	Return
EndIf

// +-------------------------------------+
// | Inicializa a R�gua de Processamento |
// +-------------------------------------+

Processa({|| RunCont() },"Processando os registros do arquivo ...")

Return


Static Function RunCont()

// +-------------------------+
// | Declara��o de Vari�veis |
// +-------------------------+

Local y
Local x := z := 0
Local nTamFile, nTamLin, cBuffer, nBtLidos
Local aErro         := {}
Local cMsgErro      := ""
Private lMsErroAuto := .F.
Private _xTamanho   := 0
Private aArray      := {}
Private aTit          := {}
Private cForn 
Private cLoja								

nTamFile := fSeek(nHdl,0,2)
fSeek(nHdl,0,0)
nTamLin  := 170 + Len(cEol)
cBuffer  := Space(nTamLin) // Vari�vel para Cria��o da Linha do Registro para Leitura
nBtLidos := fRead(nHdl,@cBuffer,nTamLin) // Leitura da Primeira Linha do Arquivo Texto

//ProcRegua((nTamFile / nTamLin)) // N�mero de Registros a Processar
ProcRegua(-1) // N�mero de Registros a Processar
y   := cBuffer
lIp := .T.

Do While nBtLidos >= nTamLin .Or. nBtLidos > 0
	IncProc()
	If (z := At(Chr(13) + Chr(10), y)) == 0
		nBtLidos := fRead(nHdl,@cBuffer,nTamLin) // Leitura da pr�xima Linha do Arquivo Texto
		y        += cBuffer
		Loop
	EndIf

	If z > 0
		nEol   := At(Chr(13) + Chr(10),y)                             
		cIdReg := Substr(y,1,3)
			
		If cIdReg <>  "353"			
			y := Substr(y,z + 2,Len(y))
			Loop
		EndIf                                            						
					
		If cIdReg = "353"  //identificador de registro do documento de cobranca de conhecimento              
			If lIp  //inicializar a variaval lOk com .T.
				lIp := .F.
				lOk := .T.
			EndIf
			cCNPJ  := Substr(y,82,14)		//cnpj do documento de cobranca de conhecimento
			cSerie := Substr(y,14,3)		//serie  
			cDoc   := Substr(y,22,9)		//documento
			cForn  := ""							//fornecedor
			cLoja  := ""							//loja
			
			DbSelectArea("SA2")
			DbSetOrder(3) 
			SA2->(DbGoTop()) 														//filial+cnpj
			If DbSeek(xFilial("SA2")+cCNPJ)						//pesquisa pelo cnpj		
				cForn    := SA2->A2_COD										//fornecedor 
				cLoja    := SA2->A2_LOJA									//loja								
				//cForn:= cFornFat := If(Empty(cFornFat),cForn,cFornFat)	//fornecedor da fatura
				//cLoja:= cLojaFat := If(Empty(cLojaFat),cLoja,cLojaFat)	//loja da fatura
				nVlrFrt  := Val(Substr(y,31,13))/100   				//valor do frete
			Else                    
				aAdd(aErro,{"CNPJ",cCNPJ,cDoc,cSerie})   //array de erro
				y := Substr(y,z + 2,Len(y))
				Loop
			EndIf
		EndIf                                                   
		
		DbSelectArea("SE2")
		DbSetOrder(1)
		SE2->(DbGoTop()) 
		If DbSeek(xFilial("SE2") + cSerie + cDoc+"   NF "+cForn+cLoja,.F.)//DbSeek(xFilial("SE2") + cSerie + cDoc+"   NF "+cForn+cLoja,.F.)
			//prefixo+numero+parcela+tipo
			//aAdd(aTit,{ cSerie, cDoc, "   " , "NF ", cforn+cLoja, .F.})
			aAdd(aTit,{ cSerie, cDoc, "   " , "NF ", cForn+cLoja, .F.})
		Else
		   aAdd(aErro,{"SEMSE2",cCNPJ,cDoc,cSerie,cForn,cLoja,Posicione("SA2",3,xFilial("SA2")+cCNPJ,"A2_NOME")})   //array de erro nao encontrado titulo no SE2
		   lOk := .F.
		EndIf
	  
		y := SUBS(y,z + 2,Len(y))
	Else
		y := ""
	EndIf
Enddo 

If lOk == Nil 
	//nao achou nenhum idreg igual 353 
	Msgbox("N�o foi localizado nenhum registro para gerar a fatura","Resumo","ALERT")
ElseIf lOk    		

	   //prefixo+tipo+numerodafatura+natureza+datade+dataate+fornec+loja+fornecgerafatura+lojagerafatura+condpgto+moeda+arratitulos+vlracrs+vlrdescrs
		aArray := { cPrefFat, cTipoFat, cNumFat, cNat, dDtDe, dDtAte, cForn, cLoja, cFornFat, cLojaFat, cCondPgto, 01, aTit , 0, 0 }
		
		DbSelectArea("SE2")
		DbSetOrder(1)
		//MsExecAuto( { |x,y| FINA290(x,y)},3,aArray,)
		//substituida linha acima pela abaixo por motivo de nao funcionar em ambiente de producao [Mauro Nagata, Actual Trend, 20170515]
		lMsErroAuto := xA290Aut("SE2",aArray)
		If lMsErroAuto
			cMsgErro := MostraErro()	//motivo de nao ter gerado
		Else			
			MsgBox("Fatura: "+cPrefFat+"-"+cNumFat+" gerada com sucesso"+Chr(13)+Chr(13)+"Qtde.T�tulos:"+Str(Len(aTit),4),"Resumo","INFO")
			If fClose(nHdl)		//fechar o arquivo TXT
				nRet:= fRename(AllTrim(cDirFile),AllTrim(Substr(cDirFile,1,RAt(".",cDirFile)-1)+".###"))		//renomear arquivo TXT para ###
			EndIf	
		Endif
Else
	MsgBox("T�tulo:"+Str(Len(aTit),4)+" + Diverg�ncia:"+Str(Len(aErro),4)+" = Total:"+Str(Len(aTit)+Len(aErro),4)+Chr(13)+"Esta importa��o n�o gerar� fatura at� que todos os t�tulos referente a este arquivo TXT estejam dispon�veis no contas a pagar","Resumo","INFO")
	cDirErro   := Substr(cDirFile,1,Rat("\",cDirFile))
	cArqErro   := "Diverg.TXT"		//arquivo contendo as divergencias da geracao da fatura a ser tratada para que possa gera-la
		      
	For nI := 1 To Len(aErro)
		If nI = 1
			cTextoErro := "CNPJ:"+aErro[nI,2]
			cTextoErro += Chr(13)+Chr(10)+"Diverg�ncias:"+Str(Len(aErro),4)			
		EndIf	
		Do Case
		   Case aErro[nI,1] = "CNPJ"
				cTextoErro += Chr(13)+Chr(10)+"CNPJ n�o encontrado. Tipo:"+aErro[nI,1]+" CNPJ:"+aErro[nI,2]+" Doc:"+aErro[nI,3]+" Serie:"+aErro[nI,4]
		   Case aErro[nI,1] = "SEMSE2"
		   		cTextoErro += Chr(13)+Chr(10)+"T�tulo n�o encontrado. Tipo:"+aErro[nI,1]+" Prefixo:"+aErro[nI,4]+" T�tulo:"+aErro[nI,3]+" Forn:"+aErro[nI,5]+" Loja:"+aErro[nI,6]
		EndCase   		
	Next
	MsgBox("Gera o arquivo:"+cArqErro+" na mesma pasta que disponibilizou o arquivo para importar, pasta:"+cDirErro,"Arquivo de Diverg�ncia","INFO")	
	MemoWrite(cDirErro+cArqErro,cTextoErro)
					
EndIf
	
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AjustaSX1    �Autor �  Mauro Nagata        �Data� 21/04/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Ajusta perguntas do SX1                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function AjustaSX1()
          
Local cAlias := Alias(), aPerg := {}
Local nI,nJ

//             01		,02	,03								,04	,05	,06		,07	,08	,09,10,11	,12,13			,14,15,16,17.18,19 ,20,21,22,23,24,25,26 ,27,28,29,30,31,32,33,34,35 ,36,37,38                                                        
aAdd(aPerg, {cPerg	,"01"	,"Arquivo a ser Gravado ?" ,"?"	,"?"	,"mv_ch1","C"	, 60 	, 0, 0,"G"	,"","mv_par01"	,"","","","","","","","","","","","","","","","","","","","","","","","","DIR"})
aAdd(aPerg, {cPerg	,"02"	,"Prefixo Fatura ?"        ,"?"	,"?"	,"mv_ch2","C"	, 03 	, 0, 0,"G"	,"","mv_par02"	,"","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aPerg, {cPerg	,"03"	,"Numero Fatura ?"         ,"?"	,"?"	,"mv_ch3","C"	, 09 	, 0, 0,"G"	,"","mv_par03"	,"","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aPerg, {cPerg	,"04"	,"Tipo Fatura ?"           ,"?"	,"?"	,"mv_ch4","C"	, 03 	, 0, 0,"G"	,"","mv_par04"	,"","","","FT","","","","","","","","","","","","","","","","","","","","","05"})
aAdd(aPerg, {cPerg	,"05"	,"Fornecedor Fatura ?"     ,"?"	,"?"	,"mv_ch5","C"	, 06 	, 0, 0,"G"	,"","mv_par05"	,"","","","","","","","","","","","","","","","","","","","","","","","","SA2"})
aAdd(aPerg, {cPerg	,"06"	,"Loja Fatura ?"           ,"?"	,"?"	,"mv_ch6","C"	, 04 	, 0, 0,"G"	,"","mv_par06"	,"","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aPerg, {cPerg	,"07"	,"Natureza Fatura ?"       ,"?"	,"?"	,"mv_ch7","C"	, 10 	, 0, 0,"G"	,"","mv_par07"	,"","","","21611001  ","","","","","","","","","","","","","","","","","","","","","SED"})
aAdd(aPerg, {cPerg	,"08"	,"Data Emiss�o De ?"       ,"?"	,"?"	,"mv_ch8","D"	, 08 	, 0, 0,"G"	,"","mv_par08"	,"","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aPerg, {cPerg	,"09"	,"Data Emiss�o ate ?"      ,"?"	,"?"	,"mv_ch9","D"	, 08 	, 0, 0,"G"	,"","mv_par09"	,"","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aPerg, {cPerg	,"10"	,"Vencimento ?"            ,"?"	,"?"	,"mv_cha","D"	, 08 	, 0, 0,"G"	,"","mv_par10"	,"","","","","","","","","","","","","","","","","","","","","","","","",""})
//aAdd(aPerg, {cPerg	,"11"	,"E-mail Divergencias ?"   ,"?"	,"?"	,"mv_chb","C"	, 40 	, 0, 0,"G"	,"","mv_par11"	,"","","","","","","","","","","","","","","","","","","","","","","","",""})

nPerg := Len(aPerg)

DbSelectArea("SX1")
DbSetOrder(1)        

For nI := 1 To Len(aPerg)                                                
	If !DbSeek(cPerg+aPerg[nI,2])		
		RecLock("SX1",.T.)
		For nJ := 1 to FCount()
			If nJ <= Len(aPerg[nI])
				FieldPut(nJ,aPerg[nI,nJ])
			Endif
		Next
		MsUnlock()
	Endif
Next

DbSelectArea(cAlias)

Return                  


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � xA290Aut     �Autor �  Mauro Nagata        �Data� 15/05/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function xA290Aut(cAlias,aFatPag)   

nval := 0
For nW:=1 To Len(atit)
	cPrefix	 := aarray[1]
//	cParcela := aarray[nW][3]
	cTipo    := aarray[2]
//	cVencmto := aCols[nW][5]
//	nValDup	:= aCols[nW][6]
//	nValCruz := xMoeda(aCols[nW,6],nMoeda,1)
//	cBanco	:= aCols[nW][7]

	DbSelectArea("SE2")
	DbSetOrder(1)
	If DbSeek(xFilial("SE2") + aTit[nW,1] + aTit[nW,2]+"   NF "+aTit[nW,5],.F.)
		nval += SE2->E2_VALOR
		RecLock("SE2",.F.)
			Replace E2_BAIXA		With  dDataBase
			Replace E2_MOVIMEN 		With  dDataBase
			Replace E2_SALDO		With  0						
			Replace E2_FATPREF 		With  cPrefix
			Replace E2_FATURA  		With  aarray[3]
			Replace E2_DTFATUR 		With  dDataBase
			Replace E2_OCORREN  	With  "01"
			Replace E2_TIPOFAT 		With  aarray[2]
			Replace E2_FLAGFAT 		With  "S"
			Replace E2_FATFOR		with  cForn
			Replace E2_FATLOJ		with  cLoja
		MsUnlock()
	Endif

	RecLock("SE5",.T.)
		Replace E5_FILIAL		With  xFilial("SE5")
		Replace E5_DATA			With  dDataBase
		Replace E5_TIPO			With  SE2->E2_TIPO
		Replace E5_MOEDA		With  "01"
		Replace E5_VALOR		With  SE2->E2_VALOR
		Replace E5_NATUREZ		With  SE2->E2_NATUREZ						
		Replace E5_RECPAG		With  "P"
		Replace E5_BENEF		With  SE2->E2_NOMFOR
		Replace E5_HISTOR		With  "Bx.p/Emiss.Fatura "+aarray[3]
		Replace E5_TIPODOC		With  "BA"
		Replace E5_VLMOED2		With  SE2->E2_VALOR
		Replace E5_LA			With  "S"
		Replace E5_PREFIXO		With  SE2->E2_PREFIXO						
		Replace E5_NUMERO		With  SE2->E2_NUM
		Replace E5_PARCELA		With  SE2->E2_PARCELA
		Replace E5_CLIFOR		With  SE2->E2_FORNECE
		Replace E5_LOJA			With  SE2->E2_LOJA
		Replace E5_DTDIGIT		With  dDataBase
		Replace E5_MOTBX		With  "FAT"
		Replace E5_SEQ			With  "01"
		Replace E5_DTDISPO		With  dDataBase
		Replace E5_FILORIG		With  SE2->E2_FILORIG
		Replace E5_FORNECE		With  SE2->E2_FORNECE
		Replace E5_ORIGEM 		With  "FINA750"
		Replace E5_MSFIL	 	With  cFilAnt
	MsUnlock()
Next

//��������������������������������Ŀ
//� Implantacao da Fatura			  �
//����������������������������������

//aParc := aarray[11]

//For _i:=1 to len(aParc)

	RecLock("SE2",.T.)
		Replace E2_FILIAL 		With  xFilial("SE2")
		Replace E2_PREFIXO		With  aarray[1]
		Replace E2_NUM 			With  aarray[3]
		Replace E2_PARCELA		With  "001" //strzero(_i,3)
		Replace E2_TIPO			With  aarray[2]
		Replace E2_NATUREZ		With  aarray[4]
		Replace E2_FORNECE		With  cForn
		Replace E2_LOJA			With  cLoja
		Replace E2_NOMFOR 		With  SA2->A2_NREDUZ
		Replace E2_EMISSAO		With  dDatabase
		Replace E2_VENCTO 		With  aArray[11]
		Replace E2_VENCREA		With  DataValida(aArray[11],.T.)
		Replace E2_VALOR		With  nVal
		Replace E2_EMIS1		With  dDatabase
		Replace E2_DATAAGE		With  dDatabase
		Replace E2_SALDO		With  nVal
		Replace E2_VENCORI		With  aArray[11]					
		Replace E2_MOEDA		With  1
		Replace E2_FATURA 		With  "NOTFAT"
		Replace E2_VLCRUZ		With  nVal
		Replace E2_ORIGEM 		With  "FINA290"
		Replace E2_MULTNAT		With  "2"	   								
		Replace E2_FILORIG	 	With  cFilAnt									
		Replace E2_MSFIL		With  cFilAnt									
	MsUnlock()
//Next
					
Return
