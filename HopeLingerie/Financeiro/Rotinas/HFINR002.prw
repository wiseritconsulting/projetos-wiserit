#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} HFINR002
Relat�rio de Titulos a Receber com Valor l�quido e saldo a receber.

@author TOTALIT Solutions - Andre R. Esteves
@since 19/06/2018
@version 1.0

@type report
/*/

user function HFINR002()
Local oReport
Local cPerg := "HFINR001"

	CriaSX1( cPerg )
	Pergunte( cPerg, .F. )
	
	oReport := RptDef(cPerg)
	oReport:PrintDialog()

return

Static Function CriaSx1( cPerg )

xPutSx1( cPerg, "01", "Filial de?"  	 , " ", " "	 , "MV_CH01" , "C", 4 ,0,0,"G"	,""          , "XM0"    ,, ,"MV_PAR01","","","",""		    ,"","","","","","","","","","","","" )
xPutSx1( cPerg, "02", "Filial ate?" 	 , " ", " "	 , "MV_CH02" , "C", 4 ,0,0,"G"	,"NAOVAZIO()", "XM0"    ,,	,"MV_PAR02","","","","ZZ"	,"","","","","","","","","","","","" )
xPutSx1( cPerg, "03", "Dt. Vento de?"    , " ", " "  , "MV_CH03" , "D", 8 ,0,0,"G"	,"NAOVAZIO()", ""       ,,	,"MV_PAR03","","","",""		   ,"","","","","","","","","","","","" )    
xPutSx1( cPerg, "04", "Dt. Vento ate?"   , " ", " " , "MV_CH04" , "D", 8 ,0,0,"G"	,"NAOVAZIO()", ""       ,,	,"MV_PAR04","","","",DTOS(Date())		    ,"","","","","","","","","","","","" )
xPutSx1( cPerg, "05", "Emiss�o de?"      , " ", " "	 , "MV_CH05" , "D", 8 ,0,0,"G"	,"NAOVAZIO()", ""       ,,	,"MV_PAR05","","","",""		    ,"","","","","","","","","","","","" )    
xPutSx1( cPerg, "06", "Emiss�o ate?"     , " ", " "	 , "MV_CH06" , "D", 8 ,0,0,"G"	,"NAOVAZIO()", ""       ,,	,"MV_PAR06","         ","         ","         ",DTOS(Date())		    ,"","        ","        ","        ","","","     ","     ","     ","","","" )
xPutSx1( cPerg, "07", "Status?"          , " ", " "	 , "MV_CH07" , "C",40 ,0,3,"C"	,""          , ""       ,,	,"MV_PAR07","Em aberto","Em aberto","Em aberto",""                   	,"Baixados","Baixados","Baixados","Ambos","Ambos","Ambos","","","","","","" ) 
xPutSx1( cPerg, "08", "Situa��es?"     	 , " ", " "	 , "MV_CH08" , "C",40 ,0,0,"G"	,"NAOVAZIO()"          , "FRVSL"  ,,	,"MV_PAR08","","","",""	,"","","","","","","","","","","","" )


Return

Static Function RptDef(cPerg)
Local oReport   := Nil
Local oSection1 := Nil
	
	oReport := TReport():New("HFINR002","Relat�rio de Situa��o de T�tulos a Receber",cPerg,{|oReport| ReportPrint(oReport)},"Relat�rio usado para visualizar as situa��o dos t�tulos a receber.")
	//oReport:SetPortrait()    
	oReport:SetLandscape()
	oReport:DisableOrientation() 
			
	oSection1:= TRSection():New(oReport, "Situa��o de T�tulos a Recebr", {"SE1"}, , .F., .T.)
	oSection1:SetAutoSize()
	
	TRCell():New(oSection1,"E1_FILIAL"	  ,"SE1","Filial"  		,X3Picture("E1_FILIAL")	,TamSX3("E1_FILIAL")[1]+2)
	TRCell():New(oSection1,"E1_PREFIXO"	  ,"SE1","Prefixo"		,X3Picture("E1_PREFIXO"),TamSX3("E1_PREFIXO")[1]+2)
	TRCell():New(oSection1,"E1_NUM"	      ,"SE1","N�mero T�tulo" ,X3Picture("E1_NUM")    ,TamSX3("E1_NUM")[1]+2)
	TRCell():New(oSection1,"E1_PARCELA"   ,"SE1","Parcela"		,X3Picture("E1_PARCELA"),TamSX3("E1_PARCELA")[1]+2)
	TRCell():New(oSection1,"E1_TIPO"      ,"SE1","Tipo"		    ,X3Picture("E1_TIPO")   ,TamSX3("E1_TIPO")[1]+2)
	TRCell():New(oSection1,"E1_NATUREZ"   ,"SE1","Natureza"		,X3Picture("E1_NATUREZ"),TamSX3("E1_NATUREZ")[1]+2)
	TRCell():New(oSection1,"E1_PORTADO"   ,"SE1","Portador"		,X3Picture("E1_PORTADO"),TamSX3("E1_PORTADO")[1]+2)
	TRCell():New(oSection1,"E1_AGEDEP "   ,"SE1","Deposit�ria"	,X3Picture("E1_AGEDEP"),TamSX3("E1_AGEDEP")[1]+2)	
	TRCell():New(oSection1,"E1_CLIENTE"   ,"SE1","C�digo"		,X3Picture("E1_CLIENTE"),TamSX3("E1_CLIENTE")[1]+2)
	TRCell():New(oSection1,"E1_LOJA"	   ,"SE1","Loja"	    ,X3Picture("E1_LOJA")   ,TamSX3("E1_LOJA")[1]+2)
	TRCell():New(oSection1,"E1_NOMCLI"    ,"SE1","Nome Cliente"	,X3Picture("E1_NOMCLI") ,TamSX3("E1_NOMCLI")[1]+2) 
	TRCell():New(oSection1,"E1_XTPPAG"    ,"SE1","Tp Pagamento"	,X3Picture("E1_XTPPAG") ,TamSX3("E1_XFORREC")[1]+2)	 //CAMPO ESPECIFICO - PEGAR NO CLIENTE
	TRCell():New(oSection1,"E1_EMISSAO"   ,"SE1","Dt. Emiss�o"	,X3Picture("E1_EMISSAO"),TamSX3("E1_EMISSAO")[1]+4)	
	TRCell():New(oSection1,"E1_VENCREA"   ,"SE1","Vencto Real"	,X3Picture("E1_VENCREA"),TamSX3("E1_VENCREA")[1]+4)
	TRCell():New(oSection1,"E1_VALOR"     ,"SE1","Valor T�tulo"	,X3Picture("E1_VALOR")  ,TamSX3("E1_VALOR")[1]+2)     
	TRCell():New(oSection1,"E1_XTXCC"     ,"SE1","Taxa Cart C" ,X3Picture("E1_XTXCC")  ,TamSX3("E1_XTXCC")[1]+2)//CAMPO ESPECIFICO - PEGAR NO CLIENTE
	TRCell():New(oSection1,"E1_VLTXCC"    ,"SE1","Vlr Tx Cart.",X3Picture("E1_VLTXCC")  ,TamSX3("E1_VLTXCC")[1]+2)//CAMPO ESPECIFICO - PEGAR NO CLIENTE
	TRCell():New(oSection1,"E1_VALLIQ"    ,"SE1","Val. Liquido"	,X3Picture("E1_VALLIQ") ,TamSX3("E1_VALLIQ")[1]+2)
	TRCell():New(oSection1,"E1_XFORREC"   ,"SE1","Tipo Pagto"  ,X3Picture("E1_XFORREC") ,TamSX3("E1_XFORREC")[1]+2)	
	TRCell():New(oSection1,"E1_BAIXA"     ,"SE1","DT Baixa" 	,X3Picture("E1_BAIXA"),TamSX3("E1_BAIXA")[1]+4)		
	TRCell():New(oSection1,"FRV_DESCRI"   ,"FRV","Situa��o" 	,X3Picture("FRV_DESCRI"),TamSX3("FRV_DESCRI")[1]+4)			
	TRCell():New(oSection1,"E1_NUMBOR"    ,"SE1","Num.Bordero" 	,X3Picture("E1_NUMBOR"),TamSX3("E1_NUMBOR")[1]+4)				
	TRCell():New(oSection1,"E1_HIST"      ,"SE1","hist�rico" 	,X3Picture("E1_HIST"),TamSX3("E1_HIST")[1]+4)					
	TRCell():New(oSection1,"E1_MOVIMEN"   ,"SE1","Ult.Moviment"	,X3Picture("E1_MOVIMEN"),TamSX3("E1_MOVIMEN")[1]+4)			
	TRCell():New(oSection1,"E1_SALDO"     ,"SE1","Saldo"       ,X3Picture("E1_SALDO")  ,TamSX3("E1_SALDO")[1]+2)
	
	TRPage():New(oReport)
			
Return(oReport)
//daniel
Static Function ReportPrint(oReport)
Local oSection1	 := oReport:Section(1)                    				  
Local lFooter  	 := .T.			
Local cAlias1    := GetNextAlias()	
Local cDeFilial  := MV_PAR01
Local cAteFilial := MV_PAR02
Local dDeVencto  := MV_PAR03
Local dAteVencto := MV_PAR04
Local dDeEmissao := MV_PAR05
Local dAteEmissao:= MV_PAR06
Local cStatus    := MV_PAR07   //1=Em Aberto | 2=Baixado | 3=Ambos
Local cSituaca   := MV_PAR08 
Local nSaldo     := 0
Local cIn        := ""
Local nValSal    := 0
Local nValRA     := 0
Local nValor     := 0
Local cItemCta	 := ""

cSituaca := "% (" + AllTrim(cSituaca) + ") %" 
	 
Do Case
	Case cStatus == 1
		cWhere := "% AND E1_SALDO > 0 %"
	Case cStatus == 2
		cWhere := "% AND E1_SALDO = 0 %"
	Case cStatus == 3
		cWhere := "% %"
EndCase

	
	//����������������������������������������Ŀ
	//�Query com os resultados a serem exibidos�
	//�na Secao 1                             �
	//������������������������������������������
	BEGIN REPORT QUERY oSection1
		BeginSQL alias cAlias1    
	
			SELECT DISTINCT
					E1_FILIAL,
					E1_PREFIXO,
					E1_NUM,
					E1_PARCELA,
					E1_TIPO,
					E1_NATUREZ,
					E1_PORTADO,
					E1_AGEDEP,
					E1_CLIENTE,
					E1_LOJA,
					E1_NOMCLI, 
					E1_XFORREC,
					E1_XTPPAG,
					E1_EMISSAO,
					E1_VENCREA,
					E1_VALOR,
					E1_XTXCC, 
					E1_VLTXCC,
					E1_VALLIQ,
					E1_BAIXA,
					E1_SITUACA,
					FRV_DESCRI,
					E1_NUMBOR,
					E1_HIST,
					E1_MOVIMEN,
					E1_SALDO
			FROM   %Table:SE1% SE1
			INNER JOIN %Table:FRV% FRV ON
					FRV_FILIAL = %Exp:xFilial("FRV")%
					AND E1_SITUACA = FRV_CODIGO
					AND FRV.%NotDel%

			WHERE   E1_FILIAL BETWEEN %Exp:cDeFilial% AND %Exp:cAteFilial% 
					AND E1_VENCTO  BETWEEN %Exp:DtoS(dDeVencto)% AND %Exp:DtoS(dAteVencto)%
					AND E1_EMISSAO BETWEEN %Exp:DtoS(dDeEmissao)% AND %Exp:DtoS(dAteEmissao)%
					AND E1_SITUACA IN %Exp:cSituaca% 
					AND SE1.%NotDel%
					%exp:cWhere%			
		EndSQL
	END REPORT QUERY oSection1

If	!oReport:Cancel()
	oReport:StartPage()
	oSection1:Init()
	oReport:SkipLine()	
	While (cAlias1)->(!Eof())
		
			oSection1:Printline()
			nValor    := 0
			nValorLiq := 0
			nSaldo    := 0
		(cAlias1)->(DbSkip())  
		
	End
	oSection1:Finish()
	oReport:ThinLine()
		
EndIf

Return

/*/{Protheus.doc} xPutSx1
Cria perguntas no SX1

@author TOTALIT Solutions - Andre R. Esteves
@since 19/06/2018
@version 1.0

@type FUNCTION
/*/
Static Function xPutSx1(cGrupo,cOrdem,cPergunt,cPerSpa,cPerEng,cVar,; 
     cTipo ,nTamanho,nDecimal,nPresel,cGSC,cValid,; 
     cF3, cGrpSxg,cPyme,; 
     cVar01,cDef01,cDefSpa1,cDefEng1,cCnt01,; 
     cDef02,cDefSpa2,cDefEng2,; 
     cDef03,cDefSpa3,cDefEng3,; 
     cDef04,cDefSpa4,cDefEng4,; 
     cDef05,cDefSpa5,cDefEng5,; 
     aHelpPor,aHelpEng,aHelpSpa,cHelp) 

LOCAL aArea := GetArea() 
Local cKey 
Local lPort := .f. 
Local lSpa := .f. 
Local lIngl := .f. 

cKey := "P." + AllTrim( cGrupo ) + AllTrim( cOrdem ) + "." 

cPyme    := Iif( cPyme           == Nil, " ", cPyme          ) 
cF3      := Iif( cF3           == NIl, " ", cF3          ) 
cGrpSxg := Iif( cGrpSxg     == Nil, " ", cGrpSxg     ) 
cCnt01   := Iif( cCnt01          == Nil, "" , cCnt01      ) 
cHelp      := Iif( cHelp          == Nil, "" , cHelp          ) 

dbSelectArea( "SX1" ) 
dbSetOrder( 1 ) 

// Ajusta o tamanho do grupo. Ajuste emergencial para valida��o dos fontes. 
// RFC - 15/03/2007 
cGrupo := PadR( cGrupo , Len( SX1->X1_GRUPO ) , " " ) 

If !( DbSeek( cGrupo + cOrdem )) 

    cPergunt:= If(! "?" $ cPergunt .And. ! Empty(cPergunt),Alltrim(cPergunt)+" ?",cPergunt) 
     cPerSpa     := If(! "?" $ cPerSpa .And. ! Empty(cPerSpa) ,Alltrim(cPerSpa) +" ?",cPerSpa) 
     cPerEng     := If(! "?" $ cPerEng .And. ! Empty(cPerEng) ,Alltrim(cPerEng) +" ?",cPerEng) 

     Reclock( "SX1" , .T. ) 

     Replace X1_GRUPO   With cGrupo 
     Replace X1_ORDEM   With cOrdem 
     Replace X1_PERGUNT With cPergunt 
     Replace X1_PERSPA With cPerSpa 
     Replace X1_PERENG With cPerEng 
     Replace X1_VARIAVL With cVar 
     Replace X1_TIPO    With cTipo 
     Replace X1_TAMANHO With nTamanho 
     Replace X1_DECIMAL With nDecimal 
     Replace X1_PRESEL With nPresel 
     Replace X1_GSC     With cGSC 
     Replace X1_VALID   With cValid 

     Replace X1_VAR01   With cVar01 

     Replace X1_F3      With cF3 
     Replace X1_GRPSXG With cGrpSxg 

     If Fieldpos("X1_PYME") > 0 
          If cPyme != Nil 
               Replace X1_PYME With cPyme 
          Endif 
     Endif 

     Replace X1_CNT01   With cCnt01 
     If cGSC == "C"               // Mult Escolha 
          Replace X1_DEF01   With cDef01 
          Replace X1_DEFSPA1 With cDefSpa1 
          Replace X1_DEFENG1 With cDefEng1 

          Replace X1_DEF02   With cDef02 
          Replace X1_DEFSPA2 With cDefSpa2 
          Replace X1_DEFENG2 With cDefEng2 

          Replace X1_DEF03   With cDef03 
          Replace X1_DEFSPA3 With cDefSpa3 
          Replace X1_DEFENG3 With cDefEng3 

          Replace X1_DEF04   With cDef04 
          Replace X1_DEFSPA4 With cDefSpa4 
          Replace X1_DEFENG4 With cDefEng4 

          Replace X1_DEF05   With cDef05 
          Replace X1_DEFSPA5 With cDefSpa5 
          Replace X1_DEFENG5 With cDefEng5 
     Endif 

     Replace X1_HELP With cHelp 

     PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa) 

     MsUnlock() 
Else 

   lPort := ! "?" $ X1_PERGUNT .And. ! Empty(SX1->X1_PERGUNT) 
   lSpa := ! "?" $ X1_PERSPA .And. ! Empty(SX1->X1_PERSPA) 
   lIngl := ! "?" $ X1_PERENG .And. ! Empty(SX1->X1_PERENG) 

   If lPort .Or. lSpa .Or. lIngl 
          RecLock("SX1",.F.) 
          If lPort 
        SX1->X1_PERGUNT:= Alltrim(SX1->X1_PERGUNT)+" ?" 
          EndIf 
          If lSpa 
               SX1->X1_PERSPA := Alltrim(SX1->X1_PERSPA) +" ?" 
          EndIf 
          If lIngl 
               SX1->X1_PERENG := Alltrim(SX1->X1_PERENG) +" ?" 
          EndIf 
          SX1->(MsUnLock()) 
     EndIf 
Endif 

RestArea( aArea ) 

Return
