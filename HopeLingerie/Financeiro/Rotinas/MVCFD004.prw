#INCLUDE "RWMAKE.CH"
#include "colors.ch"
#INCLUDE "AP5MAIL.CH"
#INCLUDE "PROTHEUS.CH"
#include "tbiconn.ch"
#include "topconn.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"

user function MVCFD004()
	Local aArea := GetArea()
//IF MsgYesno("Confirma gera��o de T�tulos de Fundo de Marketing? " )
	Processa({|| u_fdmkem()})
//Endif
	RestArea(aArea)                              
	
Return

USER FUNCTION fdmkem()
	Local _astru     := {}
	Local _afields   := {}
	Local _carq      := ""
	Local cQuery     := ""
	Local xFornec    := ""
	Local xLoja      := ""
	Local xVecto     := ctod(space(8))
	Local nValor     := 0
	Local xItens     := ""
	Local xRazao     := ""
	Local cFiltraSCR
	Local ca097User  := RetCodUsr()
	Local Fazer      := .f.
	Local aRet       := {}
	Local nlin       := 0
	Local oFont1  := TFont():New("Verdana",,012,,.T.,,,,,.F.,.F.)
	Local oFont2  := TFont():New("Verdana",,012,,.F.,,,,,.F.,.F.)
	Local aColunas := {{"MK_OK"       ,,""       	           },;
		{"ZAV_CODIGO"  ,,"Cod.Cliente"  ,"@!" },;
		{"ZAV_LOJA"    ,,"Lj Cliente"   ,"@!" },;
		{"ZAV_CNPJ"    ,,"   CNPJ   "   ,"@!" },;
		{"ZAV_NOMECL"  ,,"Nome Cliente" ,"@!" },;
		{"ZAV_VLRLIQ"  ,,"Valor Liq."   ,"@E 99,999,999.99"},;
		{"ZAV_VLRMKT"  ,,"Valor FdMkg"  ,"@E 99,999,999.99"},;
		{"ZAV_MESREF"  ,,"Mes Ref."     ,"@!" },;
		{"ZAV_VENCTO"  ,,"Vencimento"   ,"@D" },;
		{"ZAV_TITULO"  ,,"Numero T�tulo","@!" },;
		{"ZAV_EMAIL"  ,,"E-mail"       ,"@!" }}

	Private aHeader := {}
	Private aCOLS := {}
	Private aGets := {}
	Private aTela := {}
	Private aREG := {}
	Private cCadastro := "Registros de Titulos de Fundo de Marketing "
	Private aRotina := {}
	Private oCliente
	Private oTotal
	Private cCliente := ""
	Private nTotal := 0
	Private bCampo := {|nField| FieldName(nField) }
	Private aSize := {}
	Private aInfo := {}
	Private aObj := {}
	Private aPObj := {}
	Private aPGet := {}
	Private cGet1 := Space(25)
	Private cGet2 := Space(25)
	Private lContinua := .F.
	Private atuar     := ""
	Private cMark     := GetMark()
	Private lInverte  := .F.
	Private oChk
	Private lChkSel   := .F.
	Private lRefresh  := .T.
	Private oDlgT
	Private onVlrCom
	Private onVlrSld
	Private onVlrMar
	Private onVlrfIL
	Private onVlrSob
	Private nVlrCom   := 0
	Private nVlrSld   := 0
	Private nVlrMar   := 0
	Private nVlrfIL   := 0
	Private nVlrSob   := 0
	Private lMarcar  := .F.
	Private oMark3
	Private cPerg     := "MVC_FDMKG0"
	Private aRotina   := {}
	Private lContinua := .F.
	Private atuar     := ""
	Private cMark2    := GetMark()
    

    DbSelectArea("ZAV")
    
// Retorna a �rea �til das janelas Protheus
	aSize := MsAdvSize()
// Ser� utilizado tr�s �reas na janela
// 1� - Enchoice, sendo 80 pontos pixel
// 2� - MsGetDados, o que sobrar em pontos pixel � para este objeto
// 3� - Rodap� que � a pr�pria janela, sendo 15 pontos pixel
	AADD( aObj, { 100, 080, .T., .F. })
	AADD( aObj, { 100, 100, .T., .T. })
	AADD( aObj, { 100, 015, .T., .F. })
	aInfo := { aSize[1], aSize[2], aSize[3], aSize[4], 3, 3 }
	aPObj := MsObjSize( aInfo, aObj )
// C�lculo autom�tico de dimens�es dos objetos MSGET
	aPGet := MsObjGetPos( (aSize[3] - aSize[1]), 315, { {004, 024, 240, 270} } )
	If Select("XTRB") > 0
		DbSelectArea("XTRB")
		XTRB->(DbCloseArea())
	EndIf
//AjustaSX1(cPerg)
	If !Pergunte(cPerg,.T.)
		Return
	EndIf
	

	cQuery := "SELECT  SPACE(2) AS MK_OK,ZAV_CODIGO,ZAV_LOJA,ZAV_CNPJ,ZAV_NOMECL,ZAV_END,ZAV_VLRLIQ,ZAV_VLRMKT,
	cQuery += CRLF + "ZAV_MESREF,ZAV_VENCTO,ZAV_TITULO,ZAV_EMAIL , R_E_C_N_O_  AS  REG "
	cQuery += CRLF + "from "+RetSqlName("ZAV")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' and ZAV_FILIAL = '"+xFilial("ZAV")+"' "
	cQuery += CRLF + "AND ZAV_MESREF = '"+ALLTRIM(MV_PAR01)+"' AND ZAV_STATUS >= '3'  ORDER BY ZAV_NOMECL "
	MemoWrite("HPGerTIT_2.txt",cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRB", .F., .T.)
	XTRB->(dbgotop())

	If XTRB->(Eof())
		MsgStop("Nenhum registro encontrado para Envio., Verifique")
		Return()
	Endif
	If Select("XMKT") > 0
		DbSelectArea("XMKT")
		XMKT->(DbCloseArea())
	EndIf

//Estrutura da tabela temporaria
	AADD(_astru,{"MK_OK"      ,"C",  2,0})
	AADD(_astru,{"ZAV_CODIGO" ,"C",  6,0})
	AADD(_astru,{"ZAV_LOJA"   ,"C",  4,0})
	AADD(_astru,{"ZAV_CNPJ"   ,"C", 18,0})
	AADD(_astru,{"ZAV_NOMECL" ,"C", 30,0})
	AADD(_astru,{"ZAV_EMAIL"  ,"C", 60,0})
	AADD(_astru,{"ZAV_VLRLIQ" ,"N", 16,2})
	AADD(_astru,{"ZAV_VLRMKT" ,"N", 16,2})
	AADD(_astru,{"ZAV_MESREF" ,"C",  8,6})
	AADD(_astru,{"ZAV_VENCTO" ,"D", 08,0})
	AADD(_astru,{"ZAV_TITULO" ,"C", 09,0})

	cArqTrab  := CriaTrab(_astru)
	dbUseArea( .T.,, cArqTrab, "XMKT", .F., .F. )

	While XTRB->(!EOF())
 		cxE_mail := POSICIONE("ZB4", 1, xFilial("ZB4")+XTRB->ZAV_CODIGO+XTRB->ZAV_LOJA , "ZB4_EMAILF" )
		IF ALLTRIM(XTRB->ZAV_EMAIL) <> ALLTRIM(cxE_mail)
		   DbSelectArea("ZAV")
		   ZAV->(DBGOTO(XTRB->REG)) 
		   ZAV->(RecLock("ZAV",.F.))
		   ZAV_EMAIL := cxE_mail
		   ZAV->(MsUnlock())
		   DbSelectArea("XMKT")
		Else
		   cxE_mail := XTRB->ZAV_EMAIL
		Endif   
		XMKT->(RecLock("XMKT",.T.))
		XMKT->ZAV_CODIGO := XTRB->ZAV_CODIGO
		XMKT->ZAV_LOJA   := XTRB->ZAV_LOJA
		XMKT->ZAV_CNPJ   := XTRB->ZAV_CNPJ
		XMKT->ZAV_NOMECL := XTRB->ZAV_NOMECL
		XMKT->ZAV_EMAIL  := cxE_mail // XTRB->ZAV_EMAIL
		XMKT->ZAV_VLRLIQ := XTRB->ZAV_VLRLIQ
		XMKT->ZAV_VLRMKT := XTRB->ZAV_VLRMKT
		XMKT->ZAV_MESREF := XTRB->ZAV_MESREF
		XMKT->ZAV_VENCTO := ctod(substr(XTRB->ZAV_VENCTO,7,2)+'/'+substr(XTRB->ZAV_VENCTO,5,2)+'/'+substr(XTRB->ZAV_VENCTO,3,2))
		XMKT->ZAV_TITULO := XTRB->ZAV_TITULO
		XMKT->(MsUnlock())
		XTRB->(DbSkip())
	Enddo
	DbSelectArea("XMKT")
	XMKT->(DbGotop())
	DEFINE MSDIALOG oDlgT TITLE "HOPE - Envio de Nota de D�bito/Boletos  por e-mail do Fundo de Marketing " FROM aSize[7],100 To aSize[6],aSize[5] COLORS 0, 16777215 PIXEL Style DS_MODALFRAME
	cMarca    := GetMark()
	@ 005, 005 SAY oSay2 PROMPT "Lista dos Titulos de Fundode marketing a serem Enviados." SIZE 242, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
	oMark := MsSelect():New("XMKT","MK_OK",,aColunas,lInverte,cMarca,{ 015, 003, 242, 570})
	oMark:bAval:= {||(HPFDK004E(cMarca),oMark:oBrowse:Refresh())}
	oMark:oBrowse:Refresh(.F.)
	oMark:oBrowse:lHasMark    := .T.
	oMark:oBrowse:lCanAllMark := .T.
	oMark:oBrowse:bAllMark := {|| U_HPFDK004F(cMarca),oMark:oBrowse:Refresh()}
	@ 250, 005 BUTTON oButton1 PROMPT " Enviar  E-mails " SIZE 055, 013 OF oDlgT ACTION lretG:= HPFDK004H(cMarca,1) PIXEL
	@ 250, 085 BUTTON oButton1 PROMPT "  Fechar   "       SIZE 035, 013 OF oDlgT ACTION LretG:= HPFDK004H(cMarca,5) PIXEL
	@ 270, 190 Say oSay7 prompt " Valor Total Marcado para Envio:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
	@ 270,315 MSGET onVlrMar VAR nVlrMar When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"

	ACTIVATE MSDIALOG oDlgT CENTERED
	if !Empty('XMKT')
		XMKT->(DbCloseArea())
	Endif
Return()

Static Function HPFDK004H(cMarca,xopc)
Local l_Env := .f.
	Private nSaldoComp:= 0
	If xOpc <> 5
		dbSelectArea("XMKT")
		XMKT->(dbGoTop())
		While XMKT->(!Eof())
			If Alltrim(XMKT->MK_OK) <> ""
				IF xOpc == 1
				   U_criamv("HP_NATFMK")
				   //msgstop("Irei enviar e-mail do Titulo "+XMKT->ZAV_TITULO) 
				l_Env:=EnvBol(XMKT->ZAV_TITULO, XMKT->ZAV_CODIGO, XMKT->ZAV_LOJA)   //Enviar e-mail
				Endif
			Endif
			XMKT->(DBSKIP())
		End
		msgStop("Processo Finalizado!")
	Endif
	If l_Env
	   msgstop('E-mail enviado com sucesso!','Parabens!')
	Endif  
	
	onVlrMar:Refresh()
	XMKT->(dbGoTop())
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
	oDlgT:End()
Return(Nil)


Static Function HPFDK004E(cMarca)
	If Alltrim(XMKT->MK_OK) == ""
		XMKT->(RecLock( "XMKT", .F. ))
		XMKT->MK_OK   := cMarca
		XMKT->(msUnlock())
		nVlrMar :=	nVlrMar+XMKT->ZAV_VLRMKT
	Else
		nVlrSob := 0
		XMKT->(RecLock( "XMKT", .F. ))
		XMKT->MK_OK   := Space(2)
		XMKT->(msUnlock())
		nVlrMar :=	nVlrMar-XMKT->ZAV_VLRMKT
	EndIf
	onVlrMar:Refresh()
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return

User Function HPFDK004F(cMarca)
	XMKT->(dbGoTop())
	While XMKT->(!Eof())
		If Alltrim(XMKT->MK_OK) == ""
			XMKT->(RecLock( "XMKT", .F. ))
			XMKT->MK_OK   := cMarca
			XMKT->(msUnlock())
			nVlrMar :=	nVlrMar+XMKT->ZAV_VLRMKT
		//msgStop("Diminuindo saldo Pois foi Marcado")
		Else
			XMKT->(RecLock( "XMKT", .F. ))
			XMKT->MK_OK   := Space(2)
			XMKT->(msUnlock())
			nVlrMar :=	nVlrMar-XMKT->ZAV_VLRMKT
		//msgStop("Somonado o saldo Pois foi desMarcado")
		EndIf
		XMKT->(DBSKIP())
	End
	onVlrMar:Refresh()
	XMKT->(dbGoTop())
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return(Nil)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Static function EnvBol(cNumero, cCod, cLoja)
Local  aArea   := GetArea()
Local cServer  := Rtrim(SuperGetMv("MV_RELSERV"))
Local cConta   := GetMV("MV_RELACNT") //ALLTRIM(cAccount)				// Conta de acesso
Local cSenhaTK := GetMV("MV_RELPSW") //ALLTRIM(cPassword)	        // Senha de acesso
Local cFrom    := cConta
lOk:= .f.

DbSelectArea("SE1")
SE1->(DbSetOrder(2))   //E1_FILIAL, E1_CLIENTE, E1_LOJA, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO, R_E_C_N_O_, D_E_L_E_T_
SE1->(DbGotop())     

If SE1->(DbSeek(xFilial("SE1")+cCod+cLoja+"FMK"+Padr(cNumero,9)+SPACE(3)+'NDC'))
	lOk:=.t.
Else
	DbSelectArea("SE1")
	SE1->(DbSetOrder(2))   //E1_FILIAL, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO, R_E_C_N_O_, D_E_L_E_T_
	SE1->(DbGotop())     
	If SE1->(DbSeek(xFilial("SE1")+cCod+cLoja+"RFT"+Padr(cNumero,9)+SPACE(3)+'NDC'))
		lOk:=.t.
	Endif
Endif

If lOk
   _cNome       :=  Alltrim(SE1->E1_NOMCLI)
   _cEmail      :=  Alltrim(xMKT->ZAV_EMAIL)
   _xrefer      :=  Alltrim(xMKT->ZAV_MESREF)
   _XnOME       :=  Alltrim(xMKT->ZAV_NOMECL)
   _xrefer   := RefExt(_xrefer)  // 02/2018
    cBoas:= Iif(time()>= '12:00;00','Boa Tarde','Bom Dia ' )  
	cTexto := "<html>"
	cTexto += "<head>"
	cTexto += " <title></title>"
	cTexto += "</head>"
	cTexto += "<body>"
	cTexto += " <br>"   
	cTexto += '<font color="#000000" face="Arial">'
	cTexto += " <br>"
	cTexto += " <br>"
	cTexto += "      "+ cBoas+"!,   <br><br>"
	//cTexto += "      EMAIL ORIGINAL"+ _cEmail+"!,   <br><br>" //RBM
	cTexto += "      Segue ND e boleto de cobran�a do fundo de Marketing referente ao faturamento de  "+_xrefer+".  <br><br>"
	cTexto += '<font color="#FF0000" face="Arial">'
	cTexto += '<font color="#000000" face="Arial">'
	cTexto += "      Quaisquer esclarecimento que se fa�am necess�rios, estamos � disposi��o. <br><br>"
	cTexto += '<font color="#FF0000" face="Arial">'
	cTexto += '<font color="#000000" face="Arial">'
	cTexto += "       <br><br>"
	cTexto += "Obrigada! <br><br>"
	cTexto += "HOPE DO Nordeste Ltda.  <br>"
	cTexto += "    Atendimento - Financeiro <br>"
	cTexto += "    Ana Lima e/ou Dayse Monti  <br>"
	cTexto += "Fone : (11) 2169-2200 / Ramais: 2268 e 2338  <br>"  
	cTexto += "</body>"
	cTexto += "</html>"
	cRef :=  "REF_"+SUBSTR(DTOS(SE1->E1_EMISSAO),5,2)+SUBSTR(DTOS(SE1->E1_EMISSAO),3,2)
	cNomArqErro := "C:\TEMP\protocolo.pdf"
	cNomNovArq  := "\spool\pdf\fundomkt"+AllTrim(SE1->E1_NUM)+'_'+AllTrim(SE1->E1_CLIENTE)+'_'+cRef+".pdf"
	cDiretorio:="\spool\pdf\"
    l_Env:= .f.
    lTemArq:= .f.
  //  msgStop("Paseei aqui o 02  fundomkt"+AllTrim(SE1->E1_NUM)+"_"+AllTrim(SE1->E1_CLIENTE)+"_"+cRef+".pdf" ) 
	If File(cDiretorio+"fundomkt"+AllTrim(SE1->E1_NUM)+'_'+AllTrim(SE1->E1_CLIENTE)+'_'+cRef+ ".pdf")
	    laTach := .t.
		cAnexos := cDiretorio+"fundomkt"+AllTrim(SE1->E1_NUM)+'_'+AllTrim(SE1->E1_CLIENTE)+'_'+cRef + ".pdf"
		cProc:=""
		nErro:= 0            
	    cTo :=  _cEmail//"dayse.monti@hopelingerie.com.br; robson.melo90@gmail.com"//
		oMail := TMailManager():New()
		oMail:SetUseSSL(.F.) 
		//Add autentica��o abaixo ap�s troca de servidor de e-mail (office365)
		oMail:SetUseTLS(.T.)
		x_smtp:= substr(Alltrim(GetMv("MV_WFSMTP")),1,len(alltrim(GetMv("MV_WFSMTP"))) )
		oMail:Init( '', x_smtp,Alltrim(GetMv("MV_RELACNT")),Alltrim(GetMv("MV_RELPSW")), 0, 587 )   //465
		oMail:SetSmtpTimeOut( 120 )
		nErro := oMail:SmtpConnect()
		If nErro <> 0
			conout( "ERROR: Conectando - " + oMail:GetErrorString( nErro ) )
			oMail:SMTPDisconnect()
			Iif(len(cProc)<> 0,cProc:= "Sem Conexao SMTP  ",cProc:=cProc )
		Endif
		nErro := oMail:SmtpAuth(Alltrim(GetMv("MV_RELACNT")) ,Alltrim(GetMv("MV_RELPSW")))
		If nErro <> 0
			conout( "ERROR:2 autenticando - " + oMail:GetErrorString( nErro ) )
			oMail:SMTPDisconnect()
		    Iif(len(cProc)<> 0,cProc:= "Sem Autenticacao no E-mail  " ,cProc:=cProc )
		Endif                    
	    //msgStop('Paseei aqui o 04') 
		oMessage := TMailMessage():New()
		oMessage:Clear()
		oMessage:cFrom 		    := Alltrim(GetMv("MV_RELACNT"))
		oMessage:cTo	 		:= cTo
		oMessage:cCc 			:= GetMV("HP_CMAILFI") //"daniel.souza@hopelingerie.com.br"
		oMessage:cSubject    	:=  "FUNDO DE MKT - HOPE - "+ _xrefer+"  -  "+_XnOME    
		oMessage:cBody 	    	:= cTexto
		oMessage:MsgBodyType( "text/html" )
		If oMessage:AttachFile(cAnexos ) < 0
		    laTach := .f.
			Conout( "Erro ao atachar o arquivo" )
			Iif(len(cProc)<> 0,cProc:= "Sem anexos  " ,cProc:=cProc )
		Else
			oMessage:AddAtthTag( 'Content-Disposition: attachment; filename=' + cNomNovArq)
		EndIf
		nErro := oMessage:Send( oMail )
		oMail:SMTPDisconnect()
        l_Env:= .f.
		If nErro == 0
		    l_Env:= .t.
    		Conout( " E-mail ENVIADO COM SUCESSO" )
			cQuery := "UPDATE "+RETSQLNAME("ZAV")+" SET ZAV_STATUS = '4', ZAV_TITULO = '"+SE1->E1_NUM+"' WHERE ZAV_CODIGO = '"+SE1->E1_CLIENTE+"' AND ZAV_LOJA = '"+SE1->E1_LOJA+"' AND ZAV_MESREF = '"+XMKT->ZAV_MESREF+"' "
			MemoWrite("HPGerTIT_3.txt",cQuery)
			TcSqlExec( cQuery )
			TcSqlExec( "COMMIT" )
			lRet := .T.
		Else
			Conout( "Erro ao enviar e-mail" )
			l_Env:= .f.
		Endif
		lTemArq:= .t.
	else
		Conout( "Arquivo PDF,n�o encontrado Erro ao atachar o arquivo" )
		laTach := .f.
	Endif
	If !lTemArq
	    msgstop('Aten��o...., n�o Tem arquivo para anexar!','Verifique!')
	Endif    
	If !laTach 
	    msgstop('Aten��o...., n�o foi poss�vel atachar o arquivo!','Verifique!')
	Endif    
EndIf

RestArea(aArea)
Return(l_Env)



Static Function AjustaSX1(cPerg, aPergs)

	Local _sAlias	:= Alias()
	Local aCposSX1	:= {}
	Local nX 		:= 0
	Local lAltera	:= .F.
	Local cKey		:= ""
	Local nJ		:= 0
	Local nCondicao

	cPerg := Padr(cPerg,10)

	aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
		"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
		"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
		"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
		"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
		"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
		"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
		"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

	dbSelectArea("SX1")
	dbSetOrder(1)
	For nX:=1 to Len(aPergs)
		lAltera := .F.
		If MsSeek(cPerg+Right(aPergs[nX][11], 2))
			If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
					Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
				aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
				lAltera := .T.
			Endif
		Endif
	
		If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
			lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
		Endif
	
		If ! Found() .Or. lAltera
			RecLock("SX1",If(lAltera, .F., .T.))
			Replace X1_GRUPO with cPerg
			Replace X1_ORDEM with Right(aPergs[nX][11], 2)
			For nj:=1 to Len(aCposSX1)
				If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
						FieldPos(AllTrim(aCposSX1[nJ])) > 0
					Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
				Endif
			Next nj
			MsUnlock()
			cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."
		
			If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
				aHelpSpa := aPergs[nx][Len(aPergs[nx])]
			Else
				aHelpSpa := {}
			Endif
		
			If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
				aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
			Else
				aHelpEng := {}
			Endif
		
			If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
				aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
			Else
				aHelpPor := {}
			Endif
		
			PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
		Endif
	Next

Return()



static Function RefExt(_xrefer)  // 02/2018
Local cMes := substr(_xrefer,1,2)
Local cAno := substr(_xrefer,4,4)
Local cRet := "" 
do case
   case cMes = '01' 
       cRet := 'Janeiro de '+cAno  
   case cMes = '02' 
       cRet := 'Fevereiro de '+cAno
   case cMes = '03' 
       cRet := 'Mar�o de '+cAno
   case cMes = '04' 
       cRet := 'Abril de '+cAno
   case cMes = '05' 
       cRet := 'Maio de '+cAno
   case cMes = '06' 
       cRet := 'Junho de '+cAno
   case cMes = '07' 
       cRet := 'Julho de '+cAno
   case cMes = '08' 
       cRet := 'Agosto de '+cAno
   case cMes = '09' 
       cRet := 'Setembro de '+cAno
   case cMes = '10' 
       cRet := 'Outubro de '+cAno
   case cMes = '11' 
       cRet := 'Novembro de '+cAno
   case cMes = '12' 
       cRet := 'Dezembro de '+cAno
Endcase
Return(cRet)
   