// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : HFING002
// ---------+-------------------+-----------------------------------------------------------
// Date     | Author             | Description
// ---------+-------------------+-----------------------------------------------------------
// 05/04/18 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo


@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     5/04/2018
/*/
//------------------------------------------------------------------------------------------
user function HFING002(CP)
Local npos := len(CP)
Local Nrst := 0
IF NPOS <> 0
    Nrst := 9 - NPOS 
   _cTit:= CP+SUBSTR(GETSXENUM("SE1","E1_NUM"),nPos+1,nrst) //  '123456789'
Else
   _cTit:= GETSXENUM("SE1","E1_NUM")
Endif
   
DbSelectArea("SE1")
DbSetOrder(1)
DbSeek(xfilial("SE1")+_cTit)

While Found()
	CONFIRMSX8() 
    IF NPOS <> 0
        Nrst := 9 - NPOS 
        _cTit:= CP+SUBSTR(GETSXENUM("SE1","E1_NUM"),nPos+1,nrst) //  '123456789'
    Else
        _cTit:= GETSXENUM("SE1","E1_NUM")
    Endif
	DbSeek(xfilial("SE1")+_cTit)
Enddo
return(_cTit)
