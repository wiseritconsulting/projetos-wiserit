#INCLUDE "RWMAKE.CH"

/*
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
����������������������������������������������������������������������������ͻ��
���Programa  |   CNABRET    �Autor |Daniel R. Melo     �Data  | 13/10/2017   ���
����������������������������������������������������������������������������͹��
���Desc.     | PROGRAMA PARA VERIFICACAO E CONFIGURACAO DA RETORNO DO CNAB   ���
����������������������������������������������������������������������������͹��
���Uso       | Rotina incluida no X1_VALID do X1_GRUPO = AFI200.06/FIN650.03 ���
���          | Atualiza parametros do proprio AFI200 e FIN650                ���
���          | Filtro SX1 (X1_GRUPO+X1_ORDEM $ 'AFI200    06/FIN650    03')  ���
����������������������������������������������������������������������������͹��
���Revisoes  | 13/10/2017 - Daniel R. Melo (Actual Trend)                    ���
����������������������������������������������������������������������������ͼ��
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
*/

User Function CNABRET()

	cArqParam	:= ""
	cParam		:= ReadVar()
	cConfCnab	:= "1"
	cSubConta	:= "   " 

	If     &cParam == "001"
		cArqParam :="BRASIL"			//BANCO DO BRASIL
		cConfCnab := "1"
		cSubConta	:= "111"
	ElseIf &cParam == "033"
		cArqParam :="SANTA"				//SANTANDER
		cConfCnab := "1"
		cSubConta	:= "111"
	ElseIf &cParam == "237"
		cArqParam :="BRADESCO"			//BRADESCO
		cConfCnab := "1"
		cSubConta	:= "111"
	ElseIf &cParam == "341"
		cArqParam :="ITAU"				//ITAU
		cConfCnab := "1"
		cSubConta	:= "000"
	ElseIf &cParam == "422"
		cArqParam :="SAFRA"				//SAFRA
		cConfCnab := "1"
		cSubConta	:= "111"
	End

	If cParam == "MV_PAR06"
		MV_Par05 := cArqParam + ".RET"
		MV_Par05 += Space(24 - Len(cArqParam))
		MV_Par09 := cSubConta
		MV_Par12 := VAL(cConfCnab)

	ElseIf cParam == "MV_PAR03"
		MV_Par02 := cArqParam + ".RET"
		MV_Par02 += Space(24 - Len(cArqParam))
		MV_Par06 := cSubConta
		MV_Par08 := VAL(cConfCnab)
	EndIf

Return(.T.)
