#INCLUDE "RWMAKE.CH" 

User Function HPCPP007

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbGoTop()
//	DbSeek(xfilial("SB1")+"MPBJ0010")
	
	While !EOF() //.and. SubStr(SB1->B1_COD,1,8) == "MPBJ0010"
		DbSelectArea("SB4")
		DbSetOrder(1)
		Dbseek(xfilial("SB4")+SubStr(SB1->B1_COD,1,8))
			
		_wtam := rettam(SB4->B4_COLUNA,SubStr(SB1->B1_COD,12,4))
		_campo:= "ZD_TAM"+_wtam

		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+Padr(SubStr(SB1->B1_COD,1,8),15)+SubStr(SB1->B1_COD,9,3))
			
		If !Found()
			RecLock("SZD",.T.)
				Replace ZD_FILIAL		with xfilial("SZD")
				Replace ZD_PRODUTO		with SubStr(SB1->B1_COD,1,8)
				Replace ZD_COR			with SubStr(SB1->B1_COD,9,3)
				Replace &_campo			with "X"
			MsUnLock()
		Else
			RecLock("SZD",.F.)
				Replace &_campo			with "X"
			MsUnLock()
		Endif
			
		DbSelectArea("SB1")
		DbSkip()
	End
ALERT("FIM")
Return

Static Function rettam(xcoluna,_tam)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
_BVTAM := ""

cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " WHERE "
cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' " 
cQuery += "ORDER BY R_E_C_N_O_"

cQuery := ChangeQuery(cQuery)
If Select("TMPSBV") > 0 
   	 TMPSBV->(DbCloseArea()) 
EndIf 
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

DbSelectArea("TMPSBV")
DbGoTop()
_xtam := 0
_achou := .F.
While !EOF() .and. !_achou
	_xtam++
	If alltrim(TMPSBV->BV_CHAVE) == alltrim(_tam)
		_achou := .T.
	Endif
	DbSkip()
End

DbCloseArea()
_BVTAM := StrZero(_xtam,3)
Return(_BVTAM)
