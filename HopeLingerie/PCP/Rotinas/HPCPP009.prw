/*
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������ͻ��
���Programa		� HPCPPTXT	� Autor	� DANIEL R. MELO		� Data	� 29/06/2016	���
�����������������������������������������������������������������������������������͹��
���Descricao	� Tela referente a Leitura do Arquivo TXT                             ���
�����������������������������������������������������������������������������������͹��
���Uso			� ACTUAL TREND														���
�����������������������������������������������������������������������������������ͼ��
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
*/
User Function HPCPP009()

Local aSays 		:= {}
Local aButtons 		:= {}
Local nOpcA			:= 0
Local cCadastro 	:= "Importacao de TXT"
Private oProcesso 	:= Nil 
Private cTXTFile 	:= ""

AADD(aSays,OemToAnsi("Esta rotina tem por objetivo receber o arquivo de "))
AADD(aSays,OemToAnsi("retorno em .TXT para atualizar a tabela SH6.      "))

AADD(aButtons, { 19,.T.,{|o| nOpcA:= 1, o:oWnd:End() }} )//-- Botao Avancar
AADD(aButtons, {  2,.T.,{|o| nOpcA:= 0, o:oWnd:End() }} )//-- Botao Cancelar

FormBatch( cCadastro, aSays, aButtons,,200,405 )

If nOpcA == 1 //Confirmacao de processamento
	oProcesso := MsNewProcess():New({|lEnd| HPCPPTXTA() },"Lendo Arquivo(s)...")//Funcao de leitura do arquivo que transforma o conteudo lido em Array 
	oProcesso:Activate()
EndIf

Return nil

/* 	
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������ͻ��
���Programa		�HPCPPTXTA	� Autor	� DANIEL R. MELO		� Data	� 29/06/2016	���
�����������������������������������������������������������������������������������͹��
���Descricao	� Leitura e Confirma��o de Grava��o do arquivo                       	���
�����������������������������������������������������������������������������������ͼ��
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
*/
Static Function HPCPPTXTA()

Local alinha:= {}

	oProcesso:SetRegua1( 2 )
	alinha:= HPCPPTXTB() //Leitura do arquivo(TXT)

	If cErro1<>'1'
		If MsgYesNo("Confirma a importa��o dos registros do arquivo TXT?", "TXT")
			If Len(alinha) > 0	
				oProcesso:IncRegua1("Gravado Informacoes: ")
				aSort(alinha,,, { |x,y| y[1]+y[2] > x[1]+x[2]} )
				HPCPPTXTC(@alinha)
				MsgInfo("Processo executado com �xito !")
			Else
				MsgSTop("O Arquivo informado esta inv�lido !","TXT")
				Return nil
			EndIf
		EndIf
	EndIf

return nil

/* 	
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������ͻ��
���Programa		�HPCPPTXTB� Autor	� DANIEL R. MELO		� Data	� 29/06/2016	���
�����������������������������������������������������������������������������������͹��
���Descricao	� Leitura do arquivo enviado pela SEFAZ							     	���
�����������������������������������������������������������������������������������ͼ��
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
*/
Static Function HPCPPTXTB()

Local cTypeTXT	:=	"Arquivos TXT|*.TXT|Todos os Arquivos|*.*"
Local nHdlLog 	:= 0
Local alinha	:= {}
Local nLast		:= 0
Local nlinha	:= 0
Local aTemp		:= {}
Local nPos		:= 0
Local cErro		:= ""
Local dDateImp	:= "" 
Public cErro1	:= ""

cTXTFile := cGetFile("Arquivos TXT|*.TXT",OemToAnsi("Selecione o arquivo "),0,"C:\",,GETF_LOCALHARD+GETF_NETWORKDRIVE)

If Empty(cTXTFile)
	Return alinha
EndIf

oProcesso:IncRegua1("Lendo Arquivo: "+cTXTFile ) // Atualiza a barra de processamento

If File(cTXTFile)
	nHdlLog := FOpen(cTXTFile, 2) //Abertura do arquivo
	If nHdlLog == -1 //verifica se abriu com sucesso
		cErro += "N�o foi pross�vel abrir o Arquivo selecionado ["+Alltrim(cTXTFile)+"] !"+ Chr(13) + Chr(10)
		Return nil
	Else
		FT_FUSE(cTXTFile)
		FT_FGOTOP()
		nLast := FT_FLastRec()		// Retorna o numero de linhas do arquivo
		oProcesso:SetRegua2(nLast) //Define o limite da regua

		While !FT_FEOF()

		cLineCpos := AllTrim(FT_FREADLN())
		oProcesso:IncRegua2("Lendo "+alltrim(str(nlinha))+" de "+ alltrim(str( nLast )))

		/* Tratamento para limpar sujeiras */
		If  Empty(StrTran(cLineCpos,";;;;",""))
			nlinha++
			cErro +=" Linha [ "+alltrim(str(nlinha))+" ] em branco" + Chr(13) + Chr(10)
			FT_FSKIP() // Pula Linha em Branco
			Loop
		Else
			cLine := StrTran(cLineCpos,";"," ;")
			aTemp := StrTokArr(cLine,";")
				Aadd(alinha, {Alltrim(aTemp[01]),;							//N_ORDEM
							   	Alltrim(aTemp[02]),;							//COD_PROTHEUS 
							   	Alltrim(aTemp[03]),;							//QUANTIDADE
							   	Alltrim(aTemp[04]);							//FASE PROTHEUS
							})
		EndIf
		nlinha++
		FT_FSKIP()
		Enddo
	EndIf	
Else
	cErro += "Arquivo selecionado ["+Alltrim(cTXTFile)+"] n�o encontrado !"+Chr(13)+Chr(10)
	Return nil
EndIf

If !Empty(cErro)
	EECView(cErro)
EndIf

Return alinha

/* 	
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������ͻ��
���Programa		� HPCPPTXTC	� Autor	� DANIEL R. MELO		� Data	� 29/06/2016	���
�����������������������������������������������������������������������������������͹��
���Descricao	� Grava��o dos registros            										���
�����������������������������������������������������������������������������������ͼ��
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
*/

Static Function HPCPPTXTC(alinha)

Local nx := 0
Local ntam := len(alinha)

oProcesso:SetRegua2(ntam) //Define o limite da regua

aLinha := aSort( aLinha,,, { |x,y| x[1]+x[2]+x[4] < y[1]+y[2]+y[4] } )
		
For nx:=1 to ntam

	oProcesso:IncRegua2("Gravando item: "+alltrim(str( nx )) +" de "+ alltrim(str( ntam )))

	DbSelectArea("SC2")
	DbSetOrder(9)
	DbGoTop()
	//C2_FILIAL+C2_NUM+C2_ITEM+C2_PRODUTO
	DbSeek(xfilial("SC2")+alinha[nx][1]+'01'+alinha[nx][2])

	IF Found()
		DbSelectArea("SG2")
		DbSetOrder(8)
		DbSeek(xfilial("SG2")+Padr(SubStr(alinha[nx][2],1,8),26))
	
		While !EOF() .and. alltrim(SG2->G2_REFGRD) = SubStr(alinha[nx][2],1,8)
		
			DbSelectArea("SH6")
			SH6->(DbSetOrder(1))
			DbGoTop()
			DbSeek(xfilial("SH6")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+SC2->C2_PRODUTO+SG2->G2_OPERAC)
			
			If !Found()
				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+SC2->C2_PRODUTO)
				
				If SB1->B1_RASTRO = "S"
					_lote := "000000"
				Else
					_lote := ""
				Endif
				IF val(SG2->G2_OPERAC) < val(alinha[nx][4]) 

										
					aVetor := {	{"H6_FILIAL"		,xfilial("SH6")			,NIL},; 	
									{"H6_OP"	  		,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD	,NIL},;
									{"H6_PRODUTO" 	,SC2->C2_PRODUTO			,NIL},;
									{"H6_OPERAC" 		,SG2->G2_OPERAC			,NIL},;
									{"H6_RECURSO" 	,SG2->G2_RECURSO			,NIL},;
									{"H6_DTAPONT" 	,ddatabase					,NIL},;
									{"H6_DATAINI" 	,ddatabase		  			,NIL},;
									{"H6_HORAINI"		,"08:00"					,NIL},;
									{"H6_DATAFIN"		,ddatabase					,NIL},;
									{"H6_HORAFIN"		,"17:00"		  			,NIL},;
									{"H6_PT"     		,"T"      					,NIL},;
									{"H6_LOCAL"  		,SC2->C2_LOCAL			,NIL},;
									{"H6_QTDPROD"		,SC2->C2_QUANT			,NIL},;
									{"H6_LOTECTL"		,_lote					,NIL},;
									{"H6_NUMLOTE"		,""		 					,NIL}}                                                                                           	
	
					lMsErroAuto := .F.
				
					MSExecAuto({|x| mata681(x)},aVetor)  
	
					If lMsErroAuto
						Mostraerro()
					Endif
				ElseIf val(SG2->G2_OPERAC) = val(alinha[nx][4])
			
					aVetor := {	{"H6_FILIAL"		,xfilial("SH6")			,NIL},; 	
									{"H6_OP"	  		,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD	,NIL},;
									{"H6_PRODUTO" 	,SC2->C2_PRODUTO			,NIL},;
									{"H6_OPERAC" 		,SG2->G2_OPERAC			,NIL},;
									{"H6_RECURSO" 	,SG2->G2_RECURSO			,NIL},;
									{"H6_DTAPONT" 	,ddatabase					,NIL},;
									{"H6_DATAINI" 	,ddatabase		  			,NIL},;
									{"H6_HORAINI"		,"08:00"					,NIL},;
									{"H6_DATAFIN"		,ddatabase					,NIL},;
									{"H6_HORAFIN"		,"17:00"		  			,NIL},;
									{"H6_PT"     		,"P"      					,NIL},;
									{"H6_LOCAL"  		,SC2->C2_LOCAL			,NIL},;
									{"H6_QTDPROD"		,Val(Alltrim(alinha[nx][3]))	,NIL},;
									{"H6_LOTECTL"		,_lote					,NIL},;
									{"H6_NUMLOTE"		,""		 					,NIL}}                                                                                           	
	
					lMsErroAuto := .F.
				
					MSExecAuto({|x| mata681(x)},aVetor)  
	
					If lMsErroAuto
						Mostraerro()
					Endif
				Endif
			Endif
	
			DbSelectArea("SG2")
			DbSkip()
		End
	Endif
Next nx

Return nil
