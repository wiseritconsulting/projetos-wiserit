#INCLUDE "RWMAKE.CH"
#INCLUDE "TBICONN.CH"

User Function zVal2Hora( lRet, cProduto, cOpera, cQuant, cHoraIni, cHoraFin )

	DbSelectArea("SG2")
	DbSetOrder(3)
	DbGoTop()
	DbSeek(xfilial("SG2")+cProduto+cOpera)
	
	If !Found()
		DbSelectArea("SG2")
		DbSetOrder(8)
		DbSeek(xfilial("SG2")+Padr(SubStr(cProduto,1,8),26)+cOpera)
	Endif
	
	_hr    := (cQuant/SG2->G2_LOTEPAD)*SG2->G2_TEMPAD

	If lRet == "DATAINI" .OR. lRet == "1"
	
		If _hr > 8
			_dtini := dDataBase -(_hr/8)
			lRet := _dtini
		Else
			_dtini := dDataBase
			lRet := _dtini
		Endif
	
	ElseIf lRet == "HORAINI" .OR. lRet == "2"
			
		If _hr > 8
			_hrini := zVal2HoraX( 17-((_hr/8)-int(_hr/8)),":")
			lRet := _hrini
		Else
			_hrini := zVal2HoraX(17-_hr,":")
			lRet := _hrini
		Endif

	ElseIf lRet == "TEMPO" .OR. lRet == "3"

		_hrIni	 := cHoraIni +":00"
		_hrFim  := cHoraFin +":00"
		If _hr > 8
			_difDat := DateDiffDay(M->H6_DATAFIN,M->H6_DATAINI)*8
			_hrTemp := Substr(ElapTime( _hrIni, _hrFim),1,5)
			_hrTemp := SomaHoras(_hrTemp,_difDat)
			lRet    := Transform(_hrTemp, "@E 999.99")
		Else
			_hrTemp := ElapTime( _hrIni, _hrFim)
			_hrTemp := val(StrTran(Substr(_hrTemp,1,5),":","."))
			lRet 	 := Transform(_hrTemp, "@E 999.99")
		Endif
			
	Endif

Return lRet


Static Function zVal2HoraX(nValor, cSepar)

	Local cHora := ""
	Local cMinutos  := ""

	If nValor < 0
		cHora := SubStr(Time(), 1, 5)
		cHora := StrTran(cHora, ':', cSepar)
	Else
		cHora := Alltrim(Transform(nValor, "@E 99.99"))

		If Len(cHora) < 5
			cHora := Replicate('0', 5-Len(cHora)) + cHora
		EndIf

		cMinutos := SubStr(cHora, At(',', cHora)+1, 2)
		cMinutos := StrZero((Val(cMinutos)*60)/100, 2)
		cHora := SubStr(cHora, 1, At(',', cHora))+cMinutos
		cHora := StrTran(cHora, ',', cSepar)
	EndIf

Return cHora