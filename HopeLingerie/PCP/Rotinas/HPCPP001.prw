#INCLUDE "RWMAKE.CH" 
#INCLUDE "TBICONN.CH" 
#Include "Topconn.ch"
#Include 'Protheus.ch'

User Function HPCPP001() 

	Local aVetor := {} 
	Local _i
	Local _w                          
	Local aParam := {}
	Local aRetParm	:= {}                       
	Local _execauto := .T.
	dData:=dDataBase
	cPerg := "FPCP01"
	_CLOCAL:=ALLTRIM(UPPER(Getmv("MV_HENDPAD")))

	AjustaSX1(cPerg)

	IF Pergunte(cPerg,.T.)

		_par01 := MV_PAR01
		_par02 := MV_PAR02
		_par03 := MV_PAR03
		_par04 := MV_PAR04

		DbSelectArea("SC2")
		DbSetOrder(1)
		DbSeek(xfilial("SC2")+sUBsTR(alltrim(_PAR01),1,11))

		If SC2->C2_XRES = " "
			Alert("Ordem de Produ�ao n�o est� liberada!")
			Return
		ElseIf SC2->C2_XRES = "L"
			Alert("Ordem de Producao foi liberada mas ainda nao foi transferida!")
			Return
		Endif
		_ret := .T.
		_aOP := {}

		While !EOF() .and. C2_NUM+C2_ITEM+C2_SEQUEN >= alltrim(SubStr(_PAR01,1,11)) .and. C2_NUM+C2_ITEM+C2_SEQUEN <= alltrim(SubStr(_PAR01,1,11)) 
			if C2_QUJE < C2_QUANT 
				Aadd(_aOP, {SC2->C2_NUM, SC2->C2_ITEM, SC2->C2_SEQUEN, SC2->C2_PRODUTO, SC2->C2_ITEMGRD, ' '})
			endif
			
			DbSelectArea("SC2")
			DbSkip()
		End

		DbSelectArea("SC2")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SC2")+alltrim(_PAR01))

		_temrot := .F.

		DbSelectArea("SG2")
		DbSetOrder(3)
		DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

		If !Found()
			DbSelectArea("SG2")
			DbSetOrder(8)
			DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)

			If Found()
				_temrot := .T.
			Endif
		Else
			_temrot := .T.
		Endif

		If _temrot	
			DbSkip()
			If alltrim(SG2->G2_PRODUTO) == alltrim(SC2->C2_PRODUTO) .or. SubStr(SC2->C2_PRODUTO,1,8) == alltrim(SG2->G2_REFGRD)
				_ultimo := .F.
			Else
				_ultimo := .T.
			Endif

			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+SC2->C2_PRODUTO)

			_parx1 := 1
			If _ultimo .AND. (SB1->B1_TIPO = "PF" .or. SB1->B1_TIPO = "KT")
				If Pergunte("FPCPA1",.T.)
					_parx1 := MV_PAR01
				Endif		
			Endif

			aAdd(aParam,{1,"Cod Barra",Space(50) ,"",".T.",,".T.",80,.F.})
			For _i := 1 to _parx1
				lMsErroAuto := .F.
				
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+SC2->C2_PRODUTO)

				_ean := ""
				If _ultimo .AND. (SB1->B1_TIPO = "PF" .or. SB1->B1_TIPO = "KT")
					DbSelectArea("SC2")
					DbSetOrder(1)
					DbSeek(xfilial("SC2")+_aOP[1,1]+_aOP[1,2]+_aOP[1,3]+_aOP[1,5])

					If _ultimo .AND. (SB1->B1_TIPO = "PF" .or. SB1->B1_TIPO = "KT")
					
							If ParamBox(aParam,"Cod Barra",@aRetParm,{||.T.},,,,,,"U_HPCPP001",.F.,.F.)
								lOk	:= .T.
								_qtd := Substr(aRetParm[1],29,len(alltrim(aRetParm[1]))-28)
								_qtd := SubStr(_qtd,1,at("+",_qtd)-1)
								_qtd := val(_qtd)
								_sblote := SubStr(aRetParm[1],1,6)
								_lote := subStr(aRetParm[1],22,6)
								_xtp := "P"
								_ean := SubStr(aRetParm[1],8,13)
				
							else
								_qtd := SC2->C2_QUANT
								_sblote := ""
								_lote := SC2->C2_NUM
								_xtp := "T"
								lOk := .F.
							endif	
	
					Else
						_qtd := SC2->C2_QUANT
						_sblote := ""
						_lote := SC2->C2_NUM
						_xtp := "T"
					Endif

					_qry1 := "Select * from "+RetSqlName("SH6")+" WITH (NOLOCK) where D_E_L_E_T_ = '' and H6_XSLOTE = '"+_sblote+"' "
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry1),"TMPSBF",.T.,.T.)

					DbSelectArea("TMPSBF")
					DbGoTop()
					_prod := ""
					If EOF() 

						If _lote == SC2->C2_NUM

							If alltrim(_sblote) <> ""
								_qry := "Select * from "+RetSqlName("SZS")+" SZS WITH (NOLOCK) " 
								_qry += "inner join "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) on SB1.D_E_L_E_T_ = '' and B1_COD = ZS_PRODUTO "
								_qry += "where SZS.D_E_L_E_T_ = '' and ZS_NUMEROP = '"+SubStr(_par01,1,6)+"' and ZS_CODRECI = '"+_sblote+"' " // and B1_COD = '"+SC2->C2_PRODUTO+"' "
													
								dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZS",.T.,.T.)

								DbSelectArea("TMPSZS")
								DbGoTop()
								If EOF()
									_execauto := .F.
									Alert("Apontamento do Ba� n�o confere com as informa��es da Fac��o. A ordem n�o poder� ser concluida!")
								Else
									_prod := TMPSZS->ZS_PRODUTO
									DbSelectArea("SC2")
									DbSetOrder(9)
									DbSeek(xfilial("SC2")+TMPSZS->ZS_NUMEROP+"01"+TMPSZS->ZS_PRODUTO)
										
									If !Found()
										_execauto := .F.
										Alert("Apontamento do Ba� n�o confere com as informa��es da Fac��o. A ordem n�o poder� ser concluida!")
									Endif
								Endif

								DbSelectArea("TMPSZS")										
								DbCloseArea()
							Endif

							DbSelectArea("SG2")
							DbSetOrder(3)
							DbGoTop()
							DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

							If !Found()
								DbSelectArea("SG2")
								DbSetOrder(8)
								DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
							Endif

							DbSelectArea("SC2")
							DbSetOrder(9)
							DbGoTop()
							DbSeek(xfilial("SC2")+SubStr(_par01,1,6)+"01"+_prod)

							DbSelectArea("SH6")
							DbSetOrder(1)
							DbSeek(xfilial("SH6")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+SC2->C2_PRODUTO+_PAR03)

							If Found()
								While !EOF() .and. SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN = SubStr(_par01,1,11) .and. SC2->C2_PRODUTO = _prod
									DbSelectArea("SH6")
									DbSetOrder(1)
									DbSeek(xfilial("SH6")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+SC2->C2_PRODUTO+_PAR03)

									If  !Found()
										Exit
									Endif
									DbSelectArea("SC2")
									DbSkip()
								End
							Endif

							DbSelectArea("SC2")
							DbSetOrder(9)
							DbGoTop()
							DbSeek(xfilial("SC2")+SubStr(_par01,1,6)+"01"+_prod)

							If Found() .and. alltrim(_prod) <> "" .and. SC2->C2_NUM == SubStr(_par01,1,6)
							
								If _ultimo .and. alltrim(SC2->C2_YFORNEC) == ""
									_qry := "update "+RetSqlName("SD4")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where D4_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D4_COD like 'B"+SubStr(SC2->C2_PRODUTO,2,7)+"%'"
									TcSqlExec(_qry)
								Endif

								_hr := (_QTd/SG2->G2_LOTEPAD)*SG2->G2_TEMPAD
								If _hr > 8
									_dtini := _PAR02-(_hr/8)
									_hrini := zVal2Hora( 17-((_hr/8)-int(_hr/8)),":")
									_hrfim := "17:00"
								Else
									_dtini := _PAR02
									_hrini := zVal2Hora(17-_hr,":")
									_hrfim := "17:00"			
								Endif           

								If SC2->C2_QUANT-SC2->C2_QUJE <= _QTD
									_xtp := "T"
								Endif

								_execauto := .T.
								
								If SELECT("TMPSC2") > 0
									TMPSC2->(DBCLOSEAREA())
								endif

								_cQuery := " SELECT  C2_RECURSO FROM "+RetSqlName("SC2")+" WITH(NOLOCK)  JOIN "+RetSqlName("SB1")+" WITH(NOLOCK) ON C2_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = '' " 
								_cQuery += " WHERE C2_NUM = '"+SubStr(_par01,1,6)+"' AND SC2010.D_E_L_E_T_ = '' AND B1_TIPO IN ('PI','PF') " 
								_cQuery += " GROUP BY C2_RECURSO "
								
								dbUseArea(.T.,"TOPCONN",TcGenQry(,,_cQuery),"TMPSC2",.T.,.T.)
								
								If alltrim(TMPSC2->C2_RECURSO) <> "" .and. _PAR03 = "30" 
									_recurso := TMPSC2->C2_RECURSO
								
								Else
									If SC2->C2_STATUS = "S"
										DbSelectarea("SH8")
										DbSetOrder(1)
										DbSeek(xfilial("SH8")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+_PAR03)
										If Found()
											_recurso := SH8->H8_RECURSO
										Else
											_recurso := SG2->G2_RECURSO
										Endif
									Else
										_recurso := SG2->G2_RECURSO
									Endif
								Endif

								DbSelectArea("SB1")
								DbSetOrder(1)
								DbSeek(xfilial("SB1")+SC2->C2_PRODUTO)
						
								If _dtini = _par02 .and. _hrini = "17:00"
									_hrini := "16:59"
								Endif
								
								If SB1->B1_RASTRO = "S"

									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
									{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
									{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
									{"H6_OPERAC" 	,_PAR03  			,NIL},;
									{"H6_RECURSO" 	,_RECURSO			,NIL},;
									{"H6_DTAPONT"  	,dData				,NIL},;
									{"H6_DATAINI" 	,_dtini  			,NIL},;
									{"H6_HORAINI"	,_hrini  			,NIL},;
									{"H6_DATAFIN"	,_PAR02				,NIL},;
									{"H6_HORAFIN"	,_hrfim  			,NIL},;
									{"H6_PT"     	,_xtp      			,NIL},;
									{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
									{"H6_QTDPROD"	,_QTD				,NIL},;
									{"H6_LOTECTL"	,"000000"			,NIL},;
									{"H6_XSLOTE"	,_sblote 			,NIL}}                                                                                           	

								ElseIf SB1->B1_RASTRO = 'L'

									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
									{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
									{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
									{"H6_OPERAC" 	,_PAR03  			,NIL},;
									{"H6_RECURSO" 	,_RECURSO			,NIL},;
									{"H6_DTAPONT"  	,dData				,NIL},;
									{"H6_DATAINI" 	,_dtini  			,NIL},;
									{"H6_HORAINI"	,_hrini  			,NIL},;
									{"H6_DATAFIN"	,_PAR02				,NIL},;
									{"H6_HORAFIN"	,_hrfim  			,NIL},;
									{"H6_PT"     	,_xtp      			,NIL},;
									{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
									{"H6_QTDPROD"	,_QTD				,NIL},;
									{"H6_LOTECTL"	,"000000"			,NIL}}
								Else
									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
									{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
									{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
									{"H6_OPERAC" 	,_PAR03  			,NIL},;
									{"H6_RECURSO" 	,_RECURSO			,NIL},;
									{"H6_DTAPONT"  	,dData				,NIL},;
									{"H6_DATAINI" 	,_dtini  			,NIL},;
									{"H6_HORAINI"	,_hrini  			,NIL},;
									{"H6_DATAFIN"	,_PAR02				,NIL},;
									{"H6_HORAFIN"	,_hrfim  			,NIL},;
									{"H6_PT"     	,_xtp      			,NIL},;
									{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
									{"H6_QTDPROD"	,_QTD				,NIL}}

								Endif
								
								lMsErroAuto := .f.
								IF _PAR04 = 1
									_execauto := .T.
									If _ultimo
										// TODO N�o gravar o numero da OP quando for produ��o interna (Weskley Silva 30/05/2019)
										_qry := "Select C5_NUM from "+RetSqlName("SC5")+" WITH (NOLOCK) where D_E_L_E_T_ = '' and left(C5_XNUMOP,11) = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+"' AND C5_CLIENTE <> '002877' "
										
										dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC5",.T.,.T.)
					
										DbSelectArea("TMPSC5")
										DbGoTop()
										If !EOF()
										
											_qry := "Select QTD - (Select isnull(sum(H6_QTDPROD),0) from "+RetSqlName("SH6")+" SH6 WITH (NOLOCK) where SH6.D_E_L_E_T_ = '' and H6_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and H6_OPERAC = '"+_PAR03+"') as SALDO from ( "
											_qry += "Select isnull(Sum(D1_QUANT),0) as QTD "
											_qry += "from "+RetSqlName("SD1")+" SD1 WITH (NOLOCK) where SD1.D_E_L_E_T_ = '' and D1_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"') as x "
	
											dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSLD",.T.,.T.)
						
											DbSelectArea("TMPSLD")
											DbGoTop()
											IF TMPSLD->SALDO < _qtd
											Alert("A NF do Servico de Beneficiamento ainda n�o foi lan�ada, o que impede o apontamento desta Ordem de Produ��o!")
												_execauto := .F.
											Endif
										DbSelectArea("TMPSLD")
										DbCloseArea()
										Endif
										DbSelectArea("TMPSC5")
										DbCloseArea()
										
									Endif
									
									
									If _execauto										
										MSExecAuto({|x| mata681(x)},aVetor)
										
										if lMsErroAuto
											_ret := .f. 
											mostraerro()
										else
	
										If alltrim(_sblote) <> ""
											_qry := "Select H6_NUMLOTE,H6_PRODUTO from "+RetSqlName("SH6")+" WITH (NOLOCK) where D_E_L_E_T_ = '' and H6_NUMLOTE <> H6_XSLOTE and H6_XSLOTE = '"+_sblote+"' "
											dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSH6",.T.,.T.)
	
											DbSelectArea("TMPSH6")
											DbGoTop()
	
											If TMPSH6->(!EOF())
												_qry := "update "+RetSqlName("SDA")+" set DA_NUMLOTE = '"+_sblote+"' where D_E_L_E_T_ = '' and DA_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' and DA_PRODUTO = '"+TMPSH6->H6_PRODUTO+"' "
												TcSqlExec(_qry)
												_qry := "update "+RetSqlName("SD3")+" set D3_NUMLOTE = '"+_sblote+"' where D_E_L_E_T_ = '' and D3_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' and D3_COD = '"+TMPSH6->H6_PRODUTO+"' "
												TcSqlExec(_qry)
												DbSelectArea("SD5")
												DbSetOrder(1)
												DbSeek(xfilial("SD5")+TMPSH6->H6_NUMLOTE+TMPSH6->H6_PRODUTO)
												If Found()
													RecLock("SD5",.F.)
													Replace D5_NUMLOTE with _sblote
													MsUnLock()
												Endif
																				
												_qry := "update "+RetSqlName("SB8")+" set B8_NUMLOTE = '"+_sblote+"' where D_E_L_E_T_ = '' and B8_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' and B8_PRODUTO = '"+TMPSH6->H6_PRODUTO+"' "
												TcSqlExec(_qry)
												_qry := "update "+RetSqlName("SH6")+" set H6_NUMLOTE = '"+_sblote+"' where H6_XSLOTE = '"+_sblote+"' and H6_NUMLOTE <> H6_XSLOTE "
												TcSqlExec(_qry) 
											Endif
											TMPSH6->(DbCloseArea())
										Endif
									Endif 
								endif	 
								Else
									DbSelectArea("SG2")
									DbSetOrder(3)
									DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

									If !Found()
										DbSelectArea("SG2")
										DbSetOrder(8)
										DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
									Endif
									_maoobra := SG2->G2_MAOOBRA
									DbSkip()
									If alltrim(SG2->G2_PRODUTO) == alltrim(SC2->C2_PRODUTO) .or. SubStr(SC2->C2_PRODUTO,1,8) == alltrim(SG2->G2_REFGRD)
										_ultimo := .F.
									Else
										_ultimo := .T.
									Endif

									_opfut := .F.
									If !_ultimo
										DbSelectArea("SH6")
										DbSetOrder(1)
										DbSeek(xfilial("SH6")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+SC2->C2_PRODUTO+SG2->G2_OPERAC)

										If Found()
											_opfut := .T.
										Endif
									Endif

									If !_opfut
										MSExecAuto({|x,Z| MATA681(x,Z)},aVetor,5)

										_qry := "Delete "+RetSqlName("SD3")+" where D_E_L_E_T_ = '' and D3_COD = 'MODGGF' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' "
										TcSqlExec(_qry)
										_qry := "Update "+RetSqlName("SD3")+" set D_E_L_E_T_ = '*' where D_E_L_E_T_ = '' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' "
										TcSqlExec(_qry)
//										_qry := "update "+RetSqlName("SD3")+" set D3_QUANT = D3_QUANT * "+str(_maoobra)+" where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "

										// TODO RETIRAR UPDATE
										_qry := "update "+RetSqlName("SD3")+" set D3_QUANT = round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*H6_QTDPROD,2) "
										_qry += "from "+RetSqlName("SD3")+" SD3 "
										_qry += "inner join "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' and C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
										_qry += "inner join "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' and H6_OP = D3_OP and D3_OP = H6_OP and C2_PRODUTO = H6_PRODUTO and D3_EMISSAO = H6_DATAFIN and D3_IDENT = H6_IDENT "
										_qry += "inner join "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' and G2_REFGRD = left(C2_PRODUTO,8) and G2_OPERAC = H6_OPERAC "
										//_qry += "inner join "+RetSqlName("SH1")+" SH1 on SH1.D_E_L_E_T_ = '' and H1_CODIGO = G2_RECURSO "
										_qry += "where SD3.D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "

										TcSqlExec(_qry)
										_qry := "update "+RetSqlName("SD3")+" set D3_XGGF = '' where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = 'S' "
										TcSqlExec(_qry)

										_qry := "Select * from "+RetSqlName("SD3")+" WITH(NOLOCK) where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "
										cQuery := ChangeQuery(_qry)
										If Select("TMPSD3") > 0 
											TMPSD3->(DbCloseArea()) 
										EndIf 

										dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
										DbSelectArea("TMPSD3")
										DbGoTop()
										While !EOF()

											cDocSeq := PROXNUM(.F.) // Funcao que VERIFICA o MV_DOCSEQ
											cProduto:= GetMV("MV_XPRDGGF") 
											If GetMV("MV_DOCSEQ") <> cDocSeq 
												PutMV("MV_DOCSEQ",cDocSeq) 
											EndIf

											DbSelectArea("SD3")
											RecLock("SD3",.T.)
											SD3->D3_FILIAL  := xFilial("SD3")
											SD3->D3_TM      := TMPSD3->D3_TM
											SD3->D3_COD     := cProduto
											SD3->D3_UM      := TMPSD3->D3_UM
											SD3->D3_QUANT   := TMPSD3->D3_QUANT
											SD3->D3_OP		:= TMPSD3->D3_OP
											SD3->D3_CF      := TMPSD3->D3_CF
											SD3->D3_CONTA   := TMPSD3->D3_CONTA
											SD3->D3_LOCAL   := TMPSD3->D3_LOCAL
											SD3->D3_DOC     := SubStr(TMPSD3->D3_OP,1,6)
											SD3->D3_EMISSAO := stod(TMPSD3->D3_EMISSAO)
											SD3->D3_GRUPO   := TMPSD3->D3_GRUPO
											SD3->D3_CUSTO1  := TMPSD3->D3_CUSTO1
											SD3->D3_NUMSEQ  := cDocseq
											SD3->D3_TIPO    := TMPSD3->D3_TIPO
											SD3->D3_USUARIO := UsrRetName(__cUserID)
											SD3->D3_CHAVE   := TMPSD3->D3_CHAVE
											SD3->D3_IDENT	:= TMPSD3->D3_IDENT
											MsUnlock()

																			
											_qry := "Update "+RetSqlName("SD3")+" set D3_XGGF = 'S' where R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
											TcSqlExec(_qry)

											DbSelectArea("TMPSD3")
											DbSkip()
										End
										TMPSD3->(DbCloseArea())
									Else
										Alert("Existe operacao posterior ja apontada!")
									Endif
								Endif

								If lMsErroAuto
									_ret := .f.    
									Mostraerro()
								Else
									_qry := "Select DA_NUMSEQ, H6_IDENT, H6_PRODUTO, * from "+RetSqlName("SH6")+" SH6 WITH (NOLOCK) "
									_qry += "inner join "+RetSqlName("SDA")+" SDA WITH (NOLOCK) on SDA.D_E_L_E_T_ = '' and DA_PRODUTO = H6_PRODUTO and H6_LOTECTL = DA_LOTECTL and DA_NUMLOTE = H6_NUMLOTE and DA_DATA = H6_DTAPONT and DA_ORIGEM = 'SD3' "
									_qry += "where SH6.D_E_L_E_T_ = '' and DA_SALDO > 0 and H6_PRODUTO = '"+SC2->C2_PRODUTO+"' "
									
									If Select("TMPSDA") > 0 
										TMPSDA->(DbCloseArea()) 
									EndIf
									dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSDA",.T.,.T.)

									DbSelectArea("TMPSDA")
									DbGoTop()

									While !EOF()
										_ACAB := {}
										_AITEM:= {}
										Aadd(_acab, {"DA_PRODUTO"		, TMPSDA->DA_PRODUTO	,NIL})
										Aadd(_acab, {"DA_QTDORI"		, TMPSDA->DA_SALDO		,NIL})
										Aadd(_acab, {"DA_LOTECTL"		, TMPSDA->DA_LOTECTL	,NIL})
										Aadd(_acab, {"DA_NUMLOTE"		, TMPSDA->DA_NUMLOTE	,NIL})
										Aadd(_acab, {"DA_LOCAL"			, TMPSDA->DA_LOCAL		,NIL})
										Aadd(_acab, {"DA_DOC"			, TMPSDA->DA_DOC		,NIL})
										Aadd(_acab, {"DA_SERIE"			, TMPSDA->DA_SERIE		,NIL})
										Aadd(_acab, {"DA_CLIFOR"		, TMPSDA->DA_CLIFOR		,NIL})
										Aadd(_acab, {"DA_LOJA"			, TMPSDA->DA_LOJA		,NIL})
										Aadd(_acab, {"DA_TIPONF"		, TMPSDA->DA_TIPONF		,NIL})
										Aadd(_acab, {"DA_ORIGEM"		, "SD3"					,NIL})
										Aadd(_acab, {"DA_NUMSEQ"		, TMPSDA->DA_NUMSEQ		,NIL})

										//Aadd(_aitem,{"DB_ITEM"			, "0001"				,NIL})
										Aadd(_aitem,{"DB_ESTORNO"		, "N"					,NIL})
										Aadd(_aitem,{"DB_QUANT"			, TMPSDA->DA_SALDO		,NIL})
										Aadd(_aitem,{"DB_DATA"			, ddatabase				,NIL})
										Aadd(_aitem,{"DB_LOCALIZ"		, _CLOCAL				,NIL})

										lmsErroAuto := .F.
										If _execauto
											msExecAuto({|X,Y,Z|MATA265(X,Y,Z)},_acab,{_aitem},3) 

											if lMsErroAuto      // variavel do sigaauto indicando erro na operacao
												MsgBox("Ocorreu erro durante o Enderecamento, favor verificar enderecamentos!","Aviso","INFO")
												MostraErro() //MS 14.01.04
											Endif
										Endif
										
										DbSelectArea("TMPSDA")
										DbSkip()
									End

									TMPSDA->(DbCloseArea())

									DbSelectArea("SG2")
									DbSetOrder(3)
									DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

									If !Found()
										DbSelectArea("SG2")
										DbSetOrder(8)
										DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
									Endif
									_maoobra := SG2->G2_MAOOBRA

									
//									_qry := "update "+RetSqlName("SD3")+" set D3_QUANT = D3_QUANT * "+str(_maoobra)+" where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "
									_qry := "update "+RetSqlName("SD3")+" set D3_QUANT = round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*H6_QTDPROD,2) "
									_qry += "from "+RetSqlName("SD3")+" SD3 "
									_qry += "inner join "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' and C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
									_qry += "inner join "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' and H6_OP = D3_OP and D3_OP = H6_OP and C2_PRODUTO = H6_PRODUTO and D3_EMISSAO = H6_DATAFIN and D3_IDENT = H6_IDENT "
									_qry += "inner join "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' and G2_REFGRD = left(C2_PRODUTO,8) and G2_OPERAC = H6_OPERAC "
									//_qry += "inner join "+RetSqlName("SH1")+" SH1 on SH1.D_E_L_E_T_ = '' and H1_CODIGO = G2_RECURSO "
									_qry += "where SD3.D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "

									TcSqlExec(_qry)

									_qry := "Select * from "+RetSqlName("SD3")+" WITH (NOLOCK) where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "

									If Select("TMPSD3") > 0 
										TMPSD3->(DbCloseArea()) 
									EndIf 

									dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
									DbSelectArea("TMPSD3")
									DbGoTop()
									While !EOF()

										cDocSeq := PROXNUM(.F.) // Funcao que VERIFICA o MV_DOCSEQ
										cProduto:= GetMV("MV_XPRDGGF") 
										If GetMV("MV_DOCSEQ") <> cDocSeq 
											PutMV("MV_DOCSEQ",cDocSeq) 
										EndIf

										DbSelectArea("SD3")
										RecLock("SD3",.T.)
										SD3->D3_FILIAL  := xFilial("SD3")
										SD3->D3_TM      := TMPSD3->D3_TM
										SD3->D3_COD     := cProduto
										SD3->D3_UM      := TMPSD3->D3_UM
										SD3->D3_QUANT   := TMPSD3->D3_QUANT
										SD3->D3_OP		:= TMPSD3->D3_OP
										SD3->D3_CF      := TMPSD3->D3_CF
										SD3->D3_CONTA   := TMPSD3->D3_CONTA
										SD3->D3_LOCAL   := TMPSD3->D3_LOCAL
										SD3->D3_DOC     := SubStr(TMPSD3->D3_OP,1,6)
										SD3->D3_EMISSAO := stod(TMPSD3->D3_EMISSAO)
										SD3->D3_GRUPO   := TMPSD3->D3_GRUPO
										SD3->D3_CUSTO1  := TMPSD3->D3_CUSTO1
										SD3->D3_NUMSEQ  := cDocseq
										SD3->D3_TIPO    := TMPSD3->D3_TIPO
										SD3->D3_USUARIO := UsrRetName(__cUserID)
										SD3->D3_CHAVE   := TMPSD3->D3_CHAVE
										SD3->D3_IDENT	:= TMPSD3->D3_IDENT
										MsUnlock()

										
										_qry := "Update "+RetSqlName("SD3")+" set D3_XGGF = 'S' where R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
										TcSqlExec(_qry)

										DbSelectArea("TMPSD3")
										DbSkip()
									End
									TMPSD3->(DbCloseArea())
								Endif
							Else
								_i := _i-1
								Alert("O produto n�o pertence a essa Ordem de Produ��o!")
							Endif
						Else
							_i := _i-1
							Alert("O Lote  n�o pertence a essa Ordem de Produ��o!")
						Endif
					Else
						_i := _i-1
						Alert("O Lote  ja apontado!")
					Endif
					DbSelectArea("TMPSBF")
					DbCloseArea()
					//Next _w	
				Else
					For _w := 1 to len(_aOP)
						DbSelectArea("SC2")
						DbSetOrder(1)
						DbSeek(xfilial("SC2")+_aOP[_w,1]+_aOP[_w,2]+_aOP[_w,3]+_aOP[_w,5])

						DbSelectArea("SB1")
						DbSetOrder(1)
						DbSeek(xfilial("SB1")+_aOP[_w,4])
					
							_qtd := SC2->C2_QUANT
							_sblote := ""
							_lote := SC2->C2_NUM
							_xtp := "T"


						If _lote == SC2->C2_NUM
							If alltrim(_ean) <> ""
								DbSelectArea("SB1")
								DbSetOrder(5)
								DbSeek(xfilial("SB1")+SubStr(_ean,1,12))
							Endif

							DbSelectArea("SG2")
							DbSetOrder(3)
							DbGoTop()
							DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

							If !Found()
								DbSelectArea("SG2")
								DbSetOrder(8)
								DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
							Endif

							DbSelectArea("SC2")
							DbSetOrder(1)
							DbGoTop()
							DbSeek(xfilial("SC2")+_aOP[_w,1]+_aOP[_w,2]+_aOP[_w,3]+_aOP[_w,5])


							If Found()
								If _ultimo .and. alltrim(SC2->C2_YFORNEC) == ""
								
									_qry := "update "+RetSqlName("SD4")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where D4_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D4_COD like 'B"+SubStr(SC2->C2_PRODUTO,2,7)+"%'"
									TcSqlExec(_qry)
								Endif

								_hr := (_QTd/SG2->G2_LOTEPAD)*SG2->G2_TEMPAD
								If _hr > 8
									_dtini := _PAR02-(_hr/8)
									_hrini := zVal2Hora( 17-((_hr/8)-int(_hr/8)),":")
									_hrfim := "17:00"
								Else
									_dtini := _PAR02
									_hrini := zVal2Hora(17-_hr,":")
									_hrfim := "17:00"			
								Endif           

								If SC2->C2_QUANT-SC2->C2_QUJE <= _QTD
									_xtp := "T"
								Endif

								If alltrim(SC2->C2_RECURSO) <> "" .and. _PAR03 = "30"
									_recurso := SC2->C2_RECURSO
								Else	
									If SC2->C2_STATUS = "S"
										DbSelectarea("SH8")
										DbSetOrder(1)
										DbSeek(xfilial("SH8")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+_PAR03)
										If Found()
											_recurso := SH8->H8_RECURSO
										Else
											_recurso := SG2->G2_RECURSO
										Endif
									Else
										_recurso := SG2->G2_RECURSO
									Endif
								Endif

								DbSelectArea("SB1")
								DbSetOrder(1)
								DbSeek(xfilial("SB1")+SC2->C2_PRODUTO)

								If _dtini = _par02 .and. _hrini = "17:00"
									_hrini := "16:59"
								Endif

								If SB1->B1_RASTRO = "S"
									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
									{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
									{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
									{"H6_OPERAC" 	,_PAR03  			,NIL},;
									{"H6_RECURSO" 	,_RECURSO			,NIL},;
									{"H6_DTAPONT"  	,dData				,NIL},;
									{"H6_DATAINI" 	,_dtini  			,NIL},;
									{"H6_HORAINI"	,_hrini  			,NIL},;
									{"H6_DATAFIN"	,_PAR02				,NIL},;
									{"H6_HORAFIN"	,_hrfim  			,NIL},;
									{"H6_PT"     	,_xtp      			,NIL},;
									{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
									{"H6_QTDPROD"	,_QTD				,NIL},;
									{"H6_XSLOTE"	,_sblote 			,NIL},;
									{"H6_LOTECTL"	,"000000"			,NIL}}                                                                                           	
								ElseIf SB1->B1_RASTRO = "L"
									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
									{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
									{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
									{"H6_OPERAC" 	,_PAR03  			,NIL},;
									{"H6_RECURSO" 	,_RECURSO			,NIL},;
									{"H6_DTAPONT"  	,dData				,NIL},;
									{"H6_DATAINI" 	,_dtini  			,NIL},;
									{"H6_HORAINI"	,_hrini  			,NIL},;
									{"H6_DATAFIN"	,_PAR02				,NIL},;
									{"H6_HORAFIN"	,_hrfim  			,NIL},;
									{"H6_PT"     	,_xtp      			,NIL},;
									{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
									{"H6_QTDPROD"	,_QTD				,NIL},;
									{"H6_LOTECTL"	,"000000"			,NIL}}                                                                                           	

								Else
									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
									{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
									{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
									{"H6_OPERAC" 	,_PAR03  			,NIL},;
									{"H6_RECURSO" 	,_RECURSO			,NIL},;
									{"H6_DTAPONT"  	,dData				,NIL},;
									{"H6_DATAINI" 	,_dtini  			,NIL},;
									{"H6_HORAINI"	,_hrini  			,NIL},;
									{"H6_DATAFIN"	,_PAR02				,NIL},;
									{"H6_HORAFIN"	,_hrfim  			,NIL},;
									{"H6_PT"     	,_xtp      			,NIL},;
									{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
									{"H6_QTDPROD"	,_QTD				,NIL}}

								Endif
								lMsErroAuto := .f.
								IF _PAR04 = 1
									MSExecAuto({|x| mata681(x)},aVetor)

									If alltrim(_sblote) <> ""
										_qry := "Select H6_NUMLOTE,H6_PRODUTO from "+RetSqlName("SH6")+" WITH(NOLOCK) where D_E_L_E_T_ = '' and H6_NUMLOTE <> H6_XSLOTE and H6_XSLOTE = '"+_sblote+"' "
										dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSH6",.T.,.T.)

										DbSelectArea("TMPSH6")
										DbGoTop()
										
										If TMPSH6->(!EOF())
											_qry := "update "+RetSqlName("SDA")+" set DA_NUMLOTE = '"+_sblote+"' where D_E_L_E_T_ = '' and DA_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' and DA_PRODUTO = '"+TMPSH6->H6_PRODUTO+"' "
											TcSqlExec(_qry)
											_qry := "update "+RetSqlName("SD3")+" set D3_NUMLOTE = '"+_sblote+"' where D_E_L_E_T_ = '' and D3_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' and D3_COD = '"+TMPSH6->H6_PRODUTO+"' "
											TcSqlExec(_qry)
											//										_qry := "update "+RetSqlName("SD5")+" set D5_NUMLOTE = '"+_sblote+"' where D_E_L_E_T_ = '' and D5_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' and D5_PRODUTO = '"+TMPSH6->H6_PRODUTO+"' "
											//										TcSqlExec(_qry)
											DbSelectArea("SD5")
											DbSetOrder(1)
											DbSeek(xfilial("SD5")+TMPSH6->H6_NUMLOTE+TMPSH6->H6_PRODUTO)
											If Found()
												RecLock("SD5",.F.)
												Replace D5_NUMLOTE with _sblote
												MsUnLock()
											Endif
											
											_qry := "update "+RetSqlName("SB8")+" set B8_NUMLOTE = '"+_sblote+"' where D_E_L_E_T_ = '' and B8_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' and B8_PRODUTO = '"+TMPSH6->H6_PRODUTO+"' "
											TcSqlExec(_qry)
											_qry := "update "+RetSqlName("SH6")+" set H6_NUMLOTE = '"+_sblote+"' where H6_XSLOTE = '"+_sblote+"' and H6_NUMLOTE <> H6_XSLOTE "
											TcSqlExec(_qry) 
										Endif
										TMPSH6->(DbCloseArea())
									Endif  

								Else
									DbSelectArea("SG2")
									DbSetOrder(3)
									DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

									If !Found()
										DbSelectArea("SG2")
										DbSetOrder(8)
										DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
									Endif
									_maoobra := SG2->G2_MAOOBRA
									DbSkip()
									If alltrim(SG2->G2_PRODUTO) == alltrim(SC2->C2_PRODUTO) .or. SubStr(SC2->C2_PRODUTO,1,8) == alltrim(SG2->G2_REFGRD)
										_ultimo := .F.
									Else
										_ultimo := .T.
									Endif

									_opfut := .F.
									If !_ultimo
										DbSelectArea("SH6")
										DbSetOrder(1)
										DbSeek(xfilial("SH6")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+SC2->C2_PRODUTO+SG2->G2_OPERAC)

										If Found()
											_opfut := .T.
										Endif
									Endif

									If !_opfut
										MSExecAuto({|x,Z| MATA681(x,Z)},aVetor,5)
									
										_qry := "Delete "+RetSqlName("SD3")+" where D_E_L_E_T_ = '' and D3_COD = 'MODGGF' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' "
										TcSqlExec(_qry)
										_qry := "Update "+RetSqlName("SD3")+" set D_E_L_E_T_ = '*' where D_E_L_E_T_ = '' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' "
										TcSqlExec(_qry)
										_qry := "update "+RetSqlName("SD3")+" set D3_XGGF = '' where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = 'S' "
										TcSqlExec(_qry)
//										_qry := "update "+RetSqlName("SD3")+" set D3_QUANT = D3_QUANT * "+str(_maoobra)+" where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "
										_qry := "update "+RetSqlName("SD3")+" set D3_QUANT = round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*H6_QTDPROD,2) "
										_qry += "from "+RetSqlName("SD3")+" SD3 "
										_qry += "inner join "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' and C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
										_qry += "inner join "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' and H6_OP = D3_OP and D3_OP = H6_OP and C2_PRODUTO = H6_PRODUTO and D3_EMISSAO = H6_DATAFIN and D3_IDENT = H6_IDENT "
										_qry += "inner join "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' and G2_REFGRD = left(C2_PRODUTO,8) and G2_OPERAC = H6_OPERAC "
										//_qry += "inner join "+RetSqlName("SH1")+" SH1 on SH1.D_E_L_E_T_ = '' and H1_CODIGO = G2_RECURSO "
										_qry += "where SD3.D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "

										TcSqlExec(_qry)

										_qry := "Select * from "+RetSqlName("SD3")+" WITH (NOLOCK) where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "
							
										If Select("TMPSD3") > 0 
											TMPSD3->(DbCloseArea()) 
										EndIf 

										dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
										DbSelectArea("TMPSD3")
										DbGoTop()
										While !EOF()

											cDocSeq := PROXNUM(.F.) // Funcao que VERIFICA o MV_DOCSEQ
											cProduto:= GetMV("MV_XPRDGGF") 
											If GetMV("MV_DOCSEQ") <> cDocSeq 
												PutMV("MV_DOCSEQ",cDocSeq) 
											EndIf

											DbSelectArea("SD3")
											RecLock("SD3",.T.)
											SD3->D3_FILIAL  := xFilial("SD3")
											SD3->D3_TM      := TMPSD3->D3_TM
											SD3->D3_COD     := cProduto
											SD3->D3_UM      := TMPSD3->D3_UM
											SD3->D3_QUANT   := TMPSD3->D3_QUANT
											SD3->D3_OP		:= TMPSD3->D3_OP
											SD3->D3_CF      := TMPSD3->D3_CF
											SD3->D3_CONTA   := TMPSD3->D3_CONTA
											SD3->D3_LOCAL   := TMPSD3->D3_LOCAL
											SD3->D3_DOC     := SubStr(TMPSD3->D3_OP,1,6)
											SD3->D3_EMISSAO := stod(TMPSD3->D3_EMISSAO)
											SD3->D3_GRUPO   := TMPSD3->D3_GRUPO
											SD3->D3_CUSTO1  := TMPSD3->D3_CUSTO1
											SD3->D3_NUMSEQ  := cDocseq
											SD3->D3_TIPO    := TMPSD3->D3_TIPO
											SD3->D3_USUARIO := UsrRetName(__cUserID)
											SD3->D3_CHAVE   := TMPSD3->D3_CHAVE
											SD3->D3_IDENT	:= TMPSD3->D3_IDENT
											MsUnlock()
											
											_qry := "Update "+RetSqlName("SD3")+" set D3_XGGF = 'S' where R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
											TcSqlExec(_qry)

											DbSelectArea("TMPSD3")
											DbSkip()
										End
										TMPSD3->(DbCloseArea())
									Else
										Alert("Existe operacao posterior ja apontada!")
									Endif
								Endif

								If lMsErroAuto
									_ret := .f.    
									Mostraerro()
								Else
									DbSelectArea("SG2")
									DbSetOrder(3)
									DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

									If !Found()
										DbSelectArea("SG2")
										DbSetOrder(8)
										DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
									Endif
									_maoobra := SG2->G2_MAOOBRA

//									_qry := "update "+RetSqlName("SD3")+" set D3_QUANT = D3_QUANT * "+str(_maoobra)+" where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "
									_qry := "update "+RetSqlName("SD3")+" set D3_QUANT = round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*H6_QTDPROD,2) "
									_qry += "from "+RetSqlName("SD3")+" SD3 "
									_qry += "inner join "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' and C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
									_qry += "inner join "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' and H6_OP = D3_OP and D3_OP = H6_OP and C2_PRODUTO = H6_PRODUTO and D3_EMISSAO = H6_DATAFIN and D3_IDENT = H6_IDENT "
									_qry += "inner join "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' and G2_REFGRD = left(C2_PRODUTO,8) and G2_OPERAC = H6_OPERAC "
									//_qry += "inner join "+RetSqlName("SH1")+" SH1 on SH1.D_E_L_E_T_ = '' and H1_CODIGO = G2_RECURSO "
									_qry += "where SD3.D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "

									TcSqlExec(_qry)
									
									_qry := "Select * from "+RetSqlName("SD3")+" WITH (NOLOCK) where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "

									cQuery := ChangeQuery(_qry)
									If Select("TMPSD3") > 0 
										TMPSD3->(DbCloseArea()) 
									EndIf 

									dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
									DbSelectArea("TMPSD3")
									DbGoTop()
									While !EOF()

										cDocSeq := PROXNUM(.F.) // Funcao que VERIFICA o MV_DOCSEQ
										cProduto:= GetMV("MV_XPRDGGF") 
										If GetMV("MV_DOCSEQ") <> cDocSeq 
											PutMV("MV_DOCSEQ",cDocSeq) 
										EndIf

										DbSelectArea("SD3")
										RecLock("SD3",.T.)
										SD3->D3_FILIAL  := xFilial("SD3")
										SD3->D3_TM      := TMPSD3->D3_TM
										SD3->D3_COD     := cProduto
										SD3->D3_UM      := TMPSD3->D3_UM
										SD3->D3_QUANT   := TMPSD3->D3_QUANT
										SD3->D3_OP		:= TMPSD3->D3_OP
										SD3->D3_CF      := TMPSD3->D3_CF
										SD3->D3_CONTA   := TMPSD3->D3_CONTA
										SD3->D3_LOCAL   := TMPSD3->D3_LOCAL
										SD3->D3_DOC     := SubStr(TMPSD3->D3_OP,1,6)
										SD3->D3_EMISSAO := stod(TMPSD3->D3_EMISSAO)
										SD3->D3_GRUPO   := TMPSD3->D3_GRUPO
										SD3->D3_CUSTO1  := TMPSD3->D3_CUSTO1
										SD3->D3_NUMSEQ  := cDocseq
										SD3->D3_TIPO    := TMPSD3->D3_TIPO
										SD3->D3_USUARIO := UsrRetName(__cUserID)
										SD3->D3_CHAVE   := TMPSD3->D3_CHAVE
										SD3->D3_IDENT	:= TMPSD3->D3_IDENT
										MsUnlock()
										
										_qry := "Update "+RetSqlName("SD3")+" set D3_XGGF = 'S' where R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
										TcSqlExec(_qry)

										DbSelectArea("TMPSD3")
										DbSkip()
									End
									TMPSD3->(DbCloseArea())
								Endif
							Else
								_i := _i-1
								Alert("O produto  n�o pertence a essa Ordem de Produ��o!")
							Endif
						Else
							_i := _i-1
							Alert("O Lote  n�o pertence a essa Ordem de Produ��o!")
						Endif
					Next _w
				Endif
			Next _i

			IF _ret
				If _PAR04 = 1
					Alert("Apontamento realizado com sucesso!")
				Else
					Alert("Estorno realizado com sucesso!")
				Endif
			Endif
		Else
			Alert("Nao existe roteiro cadastrado!")
		Endif
	Endif

Return


Static Function AjustaSX1(cPerg)

	Local aAreaAtu	:= GetArea()
	Local aAreaSX1	:= SX1->( GetArea() )

	PutSx1(cPerg,"01","Da OP ?             ","Da OP ?             ","Da OP ?             ","Mv_ch1",TAMSX3("H6_OP" 	   )[3],TAMSX3("H6_OP"     )[1],TAMSX3("H6_OP"     )[2],0,"G","","SC2-02","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe o numero da Ordem de Producao.  ",""},{""},{""},"")
	
	PutSx1(cPerg,"02","Data Fim ?          ","Data Fim ?          ","Data Fim ?          ","Mv_ch2",TAMSX3("H6_DATAINI")[3],TAMSX3("H6_DATAINI")[1],TAMSX3("H6_DATAINI")[2],0,"G","",   "","","N","Mv_par02","","","","","","","","","","","","","","","","",{"",""},{""},{""},"")
	
	PutSx1(cPerg,"03","Operacao ?          ","Operacao ?          ","Operacao ?          ","Mv_ch3",TAMSX3("H6_OPERAC" )[3],TAMSX3("H6_OPERAC" )[1],TAMSX3("H6_OPERAC" )[2],0,"G","",   "","","N","Mv_par03","","","","","","","","","","","","","","","","",{"",""},{""},{""},"")
	PutSx1(cPerg,"04","Inclusao/Estorno ?"  ,"Inclusao/Estorno ?"  ,"Inclusao/Estorno ?"  ,"mv_ch4","N",1,0,1,"C","","","","","mv_par04","Inclusao","Inclusao","Inclusao","","Estorno","Estorno","Estorno","","","","","","","","","",{""},{"",""},{"",""}) 

	PutSx1("FPCPA1","01","Qte Baus ?         ","Qtd Baus ?          ","Qtd Baus ?          ","Mv_ch1",TAMSX3("C2_QUANT"  )[3],TAMSX3("C2_QUANT"  )[1],TAMSX3("C2_QUANT"  )[2],0,"G","","","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe a quantidade de Baus.  ",""},{""},{""},"")
	PutSx1("FPCPB1","01","C.Barra  ?         ","C.Barra  ?          ","C.Barra  ?          ","Mv_ch1","C",50,0,0,"G","","","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe o codigo de barras dos Baus.  ",""},{""},{""},"")

	RestArea( aAreaSX1 )
	RestArea( aAreaAtu )

Return(cPerg)


Static Function zVal2Hora(nValor, cSepar)

	Local cHora := ""
	Local cMinutos  := ""

	If nValor < 0
		cHora := SubStr(Time(), 1, 5)
		cHora := StrTran(cHora, ':', cSepar)
	Else
		cHora := Alltrim(Transform(nValor, "@E 99.99"))

		If Len(cHora) < 5
			cHora := Replicate('0', 5-Len(cHora)) + cHora
		EndIf

		cMinutos := SubStr(cHora, At(',', cHora)+1, 2)
		cMinutos := StrZero((Val(cMinutos)*60)/100, 2)
		cHora := SubStr(cHora, 1, At(',', cHora))+cMinutos
		cHora := StrTran(cHora, ',', cSepar)
	EndIf

Return cHora
