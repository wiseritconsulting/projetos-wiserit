#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} RF013 
Relat�rio de Fases
@author Weskley Silva
@since 06/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPPCP0002()

	Private oReport
	Private cPergCont	:= 'HPPCP0002' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'REL', 'RELATORIO DE FASES ', cPergCont, {|oReport| ReportPrint( oReport ), 'RELATORIO DE FASES' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATORIO DE FASES', { 'REL', 'SC6','SB1','SC5','SB2','SC2','ZAJ','ZAI','SBM'})
			
	TRCell():New( oSection1, 'COD'									,'REL', 		'COD',										"@!"                        ,20)
	TRCell():New( oSection1, 'COD_GRUPO'   							,'REL', 		'COD_GRUPO',    							"@!"				        ,3)
	TRCell():New( oSection1, 'GRUPO' 	    						,'REL', 		'GRUPO',	              					"@!"						,5)
	TRCell():New( oSection1, 'COD_SUBGRUPO'    						,'REL', 		'COD_SUBGRUPO',        						"@!"						,10)
	TRCell():New( oSection1, 'SUBGRUPO' 	    					,'REL', 		'SUBGRUPO',	        	    				"@!"		                ,10)
	TRCell():New( oSection1, 'REFERENCIA'							,'REL', 		'REFERENCIA',	        					"@!"		                ,5)
	TRCell():New( oSection1, 'TIPO' 	    						,'REL', 		'TIPO' ,	    							"@!"		                ,20)
	TRCell():New( oSection1, 'SITUACAO'								,'REL', 		'SITUACAO' ,		        				"@!"		                ,4)
	TRCell():New( oSection1, 'COD_COR'  							,'REL', 		'COD_COR' ,		       	 					"@!"		                ,4)
	TRCell():New( oSection1, 'COR' 									,'REL', 		'COR' ,				    					"@!"	                    ,4)
	TRCell():New( oSection1, 'TAMANHO'								,'REL', 		'TAMANHO',									"@!"		                ,4)
	TRCell():New( oSection1, 'SUBCOLECAO'							,'REL', 		'SUBCOLECAO',		    					"@!"		                ,4)
	TRCell():New( oSection1, 'COLECAO'								,'REL', 		'COLECAO',									"@!"	                    ,4)
	TRCell():New( oSection1, 'EAN'									,'REL', 		'EAN',										"@!"						,4)
	TRCell():New( oSection1, 'UTILIZACAO_PRODUTO'					,'REL', 		'UTILIZACAO_PRODUTO',						"@!"						,4)
	TRCell():New( oSection1, 'CICLO'								,'REL', 		'CICLO',									"@!"						,4)
	TRCell():New( oSection1, 'MARCA'								,'REL', 		'MARCA',									"@!"						,4)
	TRCell():New( oSection1, 'QTDE_PROCESSO'						,'REL', 		'QTDE_PROCESSO',							"@!"						,4)
	TRCell():New( oSection1, 'QTD_ESTOQUE'							,'REL', 		'QTD_ESTOQUE',								"@!"						,4)
	TRCell():New( oSection1, 'QTD_RESERVA'							,'REL', 		'QTD_RESERVA',								"@!"						,4)
	TRCell():New( oSection1, 'QTD_DISPONIVEL'						,'REL', 		'QTD_DISPONIVEL',							"@!"						,4)
	TRCell():New( oSection1, 'CARTEIRA_ATUAL'						,'REL', 		'CARTEIRA_ATUAL',							"@!"						,4)
	TRCell():New( oSection1, 'CARTEIRA_ATUAL_APROVADA'				,'REL', 		'CARTEIRA_ATUAL_APROVADA',					"@!"						,4)
	TRCell():New( oSection1, 'CARTEIRA_FUTURA'						,'REL', 		'CARTEIRA_FUTURA',							"@!"						,4)
	TRCell():New( oSection1, 'CARTEIRA_FUTURA_APROVADA'				,'REL', 		'CARTEIRA_FUTURA_APROVADA',					"@!"						,4)
	TRCell():New( oSection1, 'CARTEIRA_TOTAL'						,'REL', 		'CARTEIRA_TOTAL',							"@!"						,4)
	TRCell():New( oSection1, 'CARTEIRA_TOTAL_APROVADA'				,'REL', 		'CARTEIRA_TOTAL_APROVADA',					"@!"						,4)
	TRCell():New( oSection1, 'SEGUNDA_BLOQUEADO'					,'REL', 		'SEGUNDA_BLOQUEADO',						"@!"						,4)
	TRCell():New( oSection1, 'SEGUNDA_QUALIDADE'					,'REL', 		'SEGUNDA_QUALIDADE',						"@!"						,4)
	TRCell():New( oSection1, 'ESTOQUE_CART_ATUAL'					,'REL', 		'ESTOQUE_CART_ATUAL',						"@!"						,4)
	TRCell():New( oSection1, 'ESTOQUE+PROCESSO-CART_ATUAL'			,'REL', 		'ESTOQUE+PROCESSO-CART_ATUAL',				"@!"						,4)
	TRCell():New( oSection1, 'QTD_ESTOQUE-CART_ATUAL_APROVADA'		,'REL', 		'QTD_ESTOQUE-CART_ATUAL_APROVADA',			"@!"						,4)
	TRCell():New( oSection1, 'ESTOQUE+PROCESSO-CART_ATUAL_APROVADA'	,'REL',			'ESTOQUE+PROCESSO-CART_ATUAL_APROVADA',		"@!"						,4)
	TRCell():New( oSection1, 'ESTOQUE-CART_TOTAL_APROVADA' 			,'REL', 		'ESTOQUE-CART_TOTAL_APROVADA',				"@!"						,4)
	TRCell():New( oSection1, 'ESTOQUE+PROCESSO-CART_TOTAL_APROVADA'	,'REL',			'ESTOQUE+PROCESSO-CART_TOTAL_APROVADA',		"@!"      					,4)
	TRCell():New( oSection1, 'DISPONIVEL+RESERVA-CART_TOTAL'		,'REL',			'DISPONIVEL+RESERVA-CART_TOTAL',			"@!"      					,4)
	TRCell():New( oSection1, 'ESTOQUE+PROCESSO-CART_TOTAL'			,'REL',			'ESTOQUE+PROCESSO-CART_TOTAL',				"@!"      					,4)
	

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("REL") > 0
		REL->(dbCloseArea())
	Endif
      
    cQuery := " SELECT T.B1_COD AS COD, " 
	cQuery += " T.COD_GRUPO AS COD_GRUPO, " 
	cQuery += " T.GRUPO , " 
	cQuery += " T.COD_SUBGRUPO, " 
	cQuery += " T.SUBGRUPO, " 
	cQuery += " T.REFERENCIA, " 
	cQuery += " T.B1_TIPO AS TIPO, " 
	cQuery += " T.B1_YSITUAC AS SITUACAO, " 
	cQuery += " T.COD_COR AS COD_COR, " 
	cQuery += " T.COR AS COR, " 
	cQuery += " CASE WHEN T.TAMANHO LIKE '000%' THEN REPLACE(T.TAMANHO,'000','') " 
	cQuery += " WHEN T.TAMANHO LIKE '0%' THEN SUBSTRING(T.TAMANHO,2,4) " 
	cQuery += " ELSE REPLACE(T.TAMANHO,'00','') END TAMANHO, " 
	cQuery += " T.SUBCOLECAO, " 
	cQuery += " T.COLECAO, " 
	cQuery += " T.EAN, " 
	cQuery += " T.UTILIZACAO_PRODUTO, " 
	cQuery += " T.CICLO, " 
	cQuery += " T.MARCA, " 
	cQuery += " T.QTDE_PROCESSO, " 
	cQuery += " T.QTD_ESTOQUE, " 
	cQuery += " T.QTD_RESERVA, " 
	cQuery += " T.QTD_DISPONIVEL, " 
	cQuery += " T.CARTEIRA_ATUAL, " 
	cQuery += " T.CARTEIRA_ATUAL_APROVADA, " 
	cQuery += " T.CARTEIRA_FUTURA, " 
	cQuery += " T.CARTEIRA_FUTURA_APROVADA, " 
	cQuery += " T.CARTEIRA_ATUAL+T.CARTEIRA_FUTURA AS CARTEIRA_TOTAL, "
	cQuery += " T.CARTEIRA_ATUAL_APROVADA+T.CARTEIRA_FUTURA_APROVADA AS CARTEIRA_TOTAL_APROVADA, "
	cQuery += " T.SEGUNDA_BLOQUEADO, "
	cQuery += " T.SEGUNDA_QUALIDADE, "
	cQuery += " T.QTD_ESTOQUE - T.CARTEIRA_ATUAL AS 'ESTOQUE_CART_ATUAL', "
	cQuery += " T.QTD_ESTOQUE + (T.QTDE_PROCESSO - T.CARTEIRA_ATUAL) AS 'ESTOQUE_PROCESSO_CART_ATUAL', "
	cQuery += " T.QTD_ESTOQUE - T.CARTEIRA_ATUAL_APROVADA AS 'QTD_ESTOQUE_CART_ATUAL_APROVADA', "
	cQuery += " T.QTD_ESTOQUE + (T.QTDE_PROCESSO - CARTEIRA_ATUAL_APROVADA) AS 'ESTOQUE_PROCESSO_CART_ATUAL_APROVADA ', "
	cQuery += " T.QTD_ESTOQUE - (T.CARTEIRA_ATUAL_APROVADA+T.CARTEIRA_FUTURA_APROVADA) AS 'ESTOQUE_CART_TOTAL_APROVADA', "
	cQuery += " T.QTD_ESTOQUE + T.QTDE_PROCESSO - (T.CARTEIRA_ATUAL_APROVADA+T.CARTEIRA_FUTURA_APROVADA) AS 'ESTOQUE_PROCESSO_CART_TOTAL_APROVADA', "
	cQuery += " T.QTD_DISPONIVEL + T.QTD_RESERVA - (T.CARTEIRA_ATUAL+T.CARTEIRA_FUTURA) AS 'DISPONIVEL_RESERVA_CART_TOTAL', "
	cQuery += " T.QTD_ESTOQUE + T.QTDE_PROCESSO - (T.CARTEIRA_ATUAL+T.CARTEIRA_FUTURA) AS 'ESTOQUE_PROCESSO_CART_TOTAL' "
	   
	cQuery += " FROM ( " 
	
	cQuery += "	SELECT B1_COD, "
	cQuery += " B1_TIPO, "
	cQuery += " B1_YSITUAC, "
	cQuery += " ISNULL(BV_CHAVE,'-') AS COD_COR, "
	cQuery += " ISNULL(BV_DESCRI,'-') AS COR, "
	cQuery += " ISNULL(ZAJ_CICLO,'-') AS CICLO, "
	cQuery += " ISNULL(ZAH_DESCRI,'-' )AS SUBCOLECAO, "
	cQuery += " ISNULL(ZAA_DESCRI,'-') AS COLECAO, "
	cQuery += " B1_VEREAN AS EAN, "
	cQuery += " ISNULL((SELECT  ZAD_DESCRI FROM "+RetSqlName('ZAD')+"  WHERE ZAD_CODIGO = SUBSTRING(B4_YOCASIA,1,5)),'-') AS 'UTILIZACAO_PRODUTO', "
	cQuery += " ISNULL(ZAI_MARCA,'-') AS MARCA, "
	cQuery += " ISNULL(B1_GRUPO,'-') AS COD_GRUPO, "
	cQuery += " ISNULL(BM_DESC,'-') AS GRUPO, "
	cQuery += " ISNULL(ZAG_CODIGO,'-') AS COD_SUBGRUPO, "
	cQuery += " ISNULL(ZAG_DESCRI,'-') AS SUBGRUPO, "
	cQuery += " ISNULL(B4_COD,'-') AS REFERENCIA, "
	cQuery += " SUBSTRING(B1_COD,12,4) AS TAMANHO, "
	       
	cQuery += " (SELECT ISNULL(SUM(C2_QUANT-C2_QUJE),0) FROM "+RetSqlName('SC2')+" SC2010 (NOLOCK) WHERE B1_COD=C2_PRODUTO AND SC2010.D_E_L_E_T_ = '' AND C2_TPOP = 'F'  ) AS QTDE_PROCESSO, "

	cQuery += "	ISNULL((SELECT SUM(B2_QATU) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_='' AND B2_LOCAL IN ('E1','E0') AND B2_COD = B1_COD  "
	cQuery += "	GROUP BY B2_COD),0) AS QTD_ESTOQUE, "

	cQuery += "	ISNULL((SELECT SUM(B2_RESERVA + B2_XRESERV) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_='' AND B2_LOCAL IN ('E1','E0') AND B2_COD = B1_COD "
	cQuery += " GROUP BY B2_COD),0) AS QTD_RESERVA, "
			
	cQuery += "	ISNULL((SELECT (SUM(B2_QATU)-SUM(B2_RESERVA + B2_XRESERV)) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_='' AND B2_LOCAL IN ('E1','E0') AND B2_COD = B1_COD "
	cQuery += "	GROUP BY B2_COD),0) AS QTD_DISPONIVEL, "
						
	cQuery += "	ISNULL((SELECT SUM(C6_QTDVEN) AS C6_QTDVEN "
	cQuery += "	FROM "+RetSqlName('SC6')+" (NOLOCK) JOIN "+RetSqlName('SC5')+" (NOLOCK) ON C6_NUM = C5_NUM AND C6_CLI = C5_CLIENT AND C6_LOJA = C5_LOJACLI AND SC5010.D_E_L_E_T_ = '' AND SC6010.D_E_L_E_T_ = '' "
	cQuery += "	WHERE SC6010.D_E_L_E_T_='' AND C6_PRODUTO = B1_COD  AND  C6_NOTA = ''  and C5_EMISSAO BETWEEN '"+ DTOS(mv_par04) +"' AND '"+ DTOS(mv_par05) +"' "
	cQuery += "	GROUP BY C6_PRODUTO),0) AS CARTEIRA_ATUAL, "

	cQuery += "	ISNULL((SELECT SUM(C6_QTDVEN) AS C6_QTDVEN  FROM "+RetSqlName('SC5')+" (NOLOCK) , "+RetSqlName('SC6')+" (NOLOCK) WHERE C6_PRODUTO = B1_COD AND C5_NUM = C6_NUM AND C5_CLIENT = C6_CLI AND C5_LOJACLI = C6_LOJA AND SC5010.D_E_L_E_T_='' AND SC6010.D_E_L_E_T_='' AND C5_XBLQ = 'L' and  C6_NOTA = '' AND  C5_EMISSAO BETWEEN '"+ DTOS(mv_par08) +"' AND '"+ DTOS(mv_par09) +"' " 
	cQuery += "	GROUP BY C6_PRODUTO),0) AS CARTEIRA_ATUAL_APROVADA, "

	cQuery += "	ISNULL((SELECT SUM(C6_QTDVEN) AS C6_QTDVEN FROM "+RetSqlName('SC5')+" (NOLOCK), "+RetSqlName('SC6')+" (NOLOCK) WHERE C6_PRODUTO = B1_COD AND C5_NUM = C6_NUM AND C5_CLIENT = C6_CLI AND C5_LOJACLI = C6_LOJA AND SC5010.D_E_L_E_T_='' AND SC6010.D_E_L_E_T_='' AND  C6_NOTA = '' and C6_ENTREG BETWEEN '"+ DTOS(mv_par06) +"' AND '"+ DTOS(mv_par07) +"' "
	cQuery += "	GROUP BY C6_PRODUTO),0) AS CARTEIRA_FUTURA , "

	cQuery += "	ISNULL((SELECT SUM(C6_QTDVEN) AS C6_QTDVEN FROM "+RetSqlName('SC5')+" (NOLOCK), "+RetSqlName('SC6')+" (NOLOCK) WHERE C6_PRODUTO = B1_COD AND C5_NUM = C6_NUM AND C5_CLIENT = C6_CLI AND C5_LOJACLI = C6_LOJA AND SC5010.D_E_L_E_T_='' AND SC6010.D_E_L_E_T_='' AND C5_XBLQ=  'L' AND  C6_NOTA = '' AND C6_ENTREG BETWEEN '"+ DTOS(mv_par10) +"' AND '"+ DTOS(mv_par11) +"' " 
	cQuery += "	GROUP BY C6_PRODUTO),0) AS CARTEIRA_FUTURA_APROVADA, "

	cQuery += "	ISNULL((SELECT (SUM(B2_QATU)-SUM(B2_RESERVA + B2_XRESERV)) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_='' AND B2_LOCAL IN ('QB') AND B2_COD = B1_COD "
	cQuery += "	GROUP BY B2_COD),0) AS 'SEGUNDA_BLOQUEADO', "

	cQuery += "	ISNULL((SELECT (SUM(B2_QATU)-SUM(B2_RESERVA + B2_XRESERV)) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_='' AND B2_LOCAL IN ('QL') AND B2_COD = B1_COD "
	cQuery += "	GROUP BY B2_COD),0)AS 'SEGUNDA_QUALIDADE' "

	cQuery += "	FROM "+RetSqlName('SB1')+" (NOLOCK) "
	cQuery += "	LEFT JOIN "+RetSqlName('SBM')+" (NOLOCK) ON SB1010.B1_GRUPO=SBM010.BM_GRUPO AND SBM010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('ZAJ')+" (NOLOCK) ON B1_YCICLO = ZAJ_CODIGO AND ZAJ010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('ZAI')+" (NOLOCK) ON B1_YMARCA = ZAI_CODIGO AND ZAI010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('ZAA')+" (NOLOCK) ON B1_YCOLECA = ZAA_CODIGO AND ZAA010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('SBV')+" (NOLOCK) ON SUBSTRING(B1_COD,9,3) = BV_CHAVE  AND BV_TABELA = 'COR' AND SBV010.D_E_L_E_T_ = ''  "
	cQuery += "	LEFT JOIN "+RetSqlName('ZAH')+" (NOLOCK) ON B1_YSUBCOL = ZAH_CODIGO AND ZAH010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('SB4')+" (NOLOCK) ON LEFT(B1_COD,8) = B4_COD AND SB4010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('ZAG')+" ZAG (NOLOCK) ON  ZAG.ZAG_CODIGO = SB4010.B4_YSUBGRP AND ZAG.D_E_L_E_T_ = ''  "   
   
	cQuery += " WHERE SB1010.D_E_L_E_T_='' " 
	cQuery += "	AND B1_TIPO NOT IN ('MI','MP','SV') "
	cQuery += "	AND LEFT(B1_GRUPO,2) NOT IN ('AI','SV','MC','BN') " 
	 
	if EMPTY(mv_par01)
    	cQuery += " AND B1_COD <> ''  "
    else 
        cQuery += " AND B1_COD = '"+ Alltrim(mv_par01) +"'  "	
    endif
    
    if EMPTY(mv_par02)
    	cQuery += " AND B1_TIPO <> ''   "
    else
    	cQuery += " AND B1_TIPO = '"+ Alltrim(mv_par02) +"'   "
    endif
    
    if EMPTY(mv_par03)
    	cQuery += " AND B1_YCOLECA <> ''  "
    else
    	cQuery += " AND B1_YCOLECA = '"+ Alltrim(mv_par03) +"'  "
    ENDIF
	
	cQuery += "	GROUP BY B1_COD, B1_TIPO,B1_YSITUAC,ZAJ_CICLO,ZAI_MARCA,B1_GRUPO,BM_DESC,BV_CHAVE,BV_DESCRI,ZAH_DESCRI,ZAA_DESCRI,B1_YCOLECA,B1_VEREAN,B4_YOCASIA,B4_COD,ZAG_CODIGO,ZAG_DESCRI,B1_LOCPAD "
		
	cQuery += " ) T "	
		    
    TCQUERY cQuery NEW ALIAS REL

	While REL->(!EOF())      
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("COD"):SetValue(REL->COD)
		oSection1:Cell("COD"):SetAlign("LEFT")
		
		oSection1:Cell("COD_GRUPO"):SetValue(REL->COD_GRUPO)
		oSection1:Cell("COD_GRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO"):SetValue(REL->GRUPO)
		oSection1:Cell("GRUPO"):SetAlign("LEFT")	
		
		oSection1:Cell("COD_SUBGRUPO"):SetValue(REL->COD_SUBGRUPO)
		oSection1:Cell("COD_SUBGRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("SUBGRUPO"):SetValue(REL->SUBGRUPO)
		oSection1:Cell("SUBGRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(REL->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(REL->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("SITUACAO"):SetValue(REL->SITUACAO)
		oSection1:Cell("SITUACAO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(REL->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("COR"):SetValue(REL->COR)
		oSection1:Cell("COR"):SetAlign("LEFT")

		oSection1:Cell("TAMANHO"):SetValue(REL->TAMANHO)
		oSection1:Cell("TAMANHO"):SetAlign("LEFT")
		
		oSection1:Cell("SUBCOLECAO"):SetValue(REL->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")

		oSection1:Cell("COLECAO"):SetValue(REL->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("EAN"):SetValue(REL->EAN)
		oSection1:Cell("EAN"):SetAlign("LEFT")
		
		oSection1:Cell("UTILIZACAO_PRODUTO"):SetValue(REL->UTILIZACAO_PRODUTO)
		oSection1:Cell("UTILIZACAO_PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("CICLO"):SetValue(REL->CICLO)
		oSection1:Cell("CICLO"):SetAlign("LEFT")
		
		oSection1:Cell("MARCA"):SetValue(REL->MARCA)
		oSection1:Cell("MARCA"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PROCESSO"):SetValue(REL->QTDE_PROCESSO)
		oSection1:Cell("QTDE_PROCESSO"):SetAlign("LEFT")
		
		oSection1:Cell("QTD_ESTOQUE"):SetValue(REL->QTD_ESTOQUE)
		oSection1:Cell("QTD_ESTOQUE"):SetAlign("LEFT")
		
		oSection1:Cell("QTD_RESERVA"):SetValue(REL->QTD_RESERVA)
		oSection1:Cell("QTD_RESERVA"):SetAlign("LEFT")
		
		oSection1:Cell("QTD_DISPONIVEL"):SetValue(REL->QTD_DISPONIVEL)
		oSection1:Cell("QTD_DISPONIVEL"):SetAlign("LEFT")
		
		oSection1:Cell("CARTEIRA_ATUAL"):SetValue(REL->CARTEIRA_ATUAL)
		oSection1:Cell("CARTEIRA_ATUAL"):SetAlign("LEFT")
		
		oSection1:Cell("CARTEIRA_FUTURA"):SetValue(REL->CARTEIRA_FUTURA)
		oSection1:Cell("CARTEIRA_FUTURA"):SetAlign("LEFT")
		
		oSection1:Cell("CARTEIRA_FUTURA_APROVADA"):SetValue(REL->CARTEIRA_FUTURA_APROVADA)
		oSection1:Cell("CARTEIRA_FUTURA_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("CARTEIRA_TOTAL"):SetValue(REL->CARTEIRA_TOTAL)
		oSection1:Cell("CARTEIRA_TOTAL"):SetAlign("LEFT")
		
		oSection1:Cell("CARTEIRA_TOTAL_APROVADA"):SetValue(REL->CARTEIRA_TOTAL_APROVADA)
		oSection1:Cell("CARTEIRA_TOTAL_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("SEGUNDA_BLOQUEADO"):SetValue(REL->SEGUNDA_BLOQUEADO)
		oSection1:Cell("SEGUNDA_BLOQUEADO"):SetAlign("LEFT")
		
		oSection1:Cell("SEGUNDA_QUALIDADE"):SetValue(REL->SEGUNDA_QUALIDADE)
		oSection1:Cell("SEGUNDA_QUALIDADE"):SetAlign("LEFT")
		
		oSection1:Cell("ESTOQUE_CART_ATUAL"):SetValue(REL->ESTOQUE_CART_ATUAL)
		oSection1:Cell("ESTOQUE_CART_ATUAL"):SetAlign("LEFT")
		
		oSection1:Cell("ESTOQUE+PROCESSO-CART_ATUAL"):SetValue(REL->ESTOQUE_PROCESSO_CART_ATUAL)
		oSection1:Cell("ESTOQUE+PROCESSO-CART_ATUAL"):SetAlign("LEFT")
		
		oSection1:Cell("ESTOQUE+PROCESSO-CART_ATUAL"):SetValue(REL->ESTOQUE_PROCESSO_CART_ATUAL)
		oSection1:Cell("ESTOQUE+PROCESSO-CART_ATUAL"):SetAlign("LEFT")
		
		oSection1:Cell("QTD_ESTOQUE-CART_ATUAL_APROVADA"):SetValue(REL->QTD_ESTOQUE_CART_ATUAL_APROVADA)
		oSection1:Cell("QTD_ESTOQUE-CART_ATUAL_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("ESTOQUE+PROCESSO-CART_ATUAL_APROVADA"):SetValue(REL->ESTOQUE_PROCESSO_CART_ATUAL_APROVADA)
		oSection1:Cell("ESTOQUE+PROCESSO-CART_ATUAL_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("ESTOQUE-CART_TOTAL_APROVADA"):SetValue(REL->ESTOQUE_CART_TOTAL_APROVADA)
		oSection1:Cell("ESTOQUE-CART_TOTAL_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("ESTOQUE+PROCESSO-CART_TOTAL_APROVADA"):SetValue(REL->ESTOQUE_PROCESSO_CART_TOTAL_APROVADA)
		oSection1:Cell("ESTOQUE+PROCESSO-CART_TOTAL_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("DISPONIVEL+RESERVA-CART_TOTAL"):SetValue(REL->DISPONIVEL_RESERVA_CART_TOTAL)
		oSection1:Cell("DISPONIVEL+RESERVA-CART_TOTAL"):SetAlign("LEFT")
		
		oSection1:Cell("ESTOQUE+PROCESSO-CART_TOTAL"):SetValue(REL->ESTOQUE_PROCESSO_CART_TOTAL)
		oSection1:Cell("ESTOQUE+PROCESSO-CART_TOTAL"):SetAlign("LEFT")
		
		oSection1:PrintLine()
		
		REL->(DBSKIP()) 
	enddo
	REL->(DBCLOSEAREA())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Produto"		               ,""		,""		    ,"mv_ch1","C",20,0,1,"G",""	,"SB1","","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Tipo"		                   ,""		,""		    ,"mv_ch2","C",03,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Cole��o"		               ,""		,""	 	    ,"mv_ch3","C",15,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Carteira Atual de?"	           ,""		,""		    ,"mv_ch4","D",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Carteira Atual At�?"		   ,""		,""		    ,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "06","Carteira Futura de?"	       ,""		,""		    ,"mv_ch6","D",08,0,1,"G",""	,""	,"","","mv_par06"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "07","Carteira Futura At�?"	       ,""		,""		    ,"mv_ch7","D",08,0,1,"G",""	,""	,"","","mv_par07"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "08","Carteira Atual Aprovada de?"   ,""		,""		    ,"mv_ch8","D",08,0,1,"G",""	,""	,"","","mv_par08"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "09","Carteira Atual Aprovada At�?"  ,""		,""		    ,"mv_ch9","D",08,0,1,"G",""	,""	,"","","mv_par09"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "10","Carteira Futura Aprovada de?"  ,""		,""		    ,"mv_c10","D",08,0,1,"G","",""	,"","","mv_par10"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "11","Carteira Futura Aprovada At�?" ,""		,""		    ,"mv_c11","D",08,0,1,"G","",""	,"","","mv_par11"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "12","Armazem?"                      ,""		,""		    ,"mv_c12","C",03,0,1,"G","","NNR","","","mv_par12"," ","","","","","","","","","","","","","","","")
Return  
