#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL010 
Roteiros cadastrados 
@author Weskley Silva
@since 09/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL015()

Private oReport
Private cPergCont	:= 'HPREL015' 

/************************
*Monta pergunte do Log *
************************/

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 09 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'OFC1', 'CADASTRO DE ROTEIROS', , {|oReport| ReportPrint( oReport ), 'CADASTRO DE ROTEIROS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'CADASTRO DE ROTEIROS', { 'OFC1', 'SZR','SZJ','SA1','SA2','SC5','SZ1'})
			
	TRCell():New( oSection1, 'REFERENCIA'			,'OFC1', 		'REFERENCIA',					    "@!"                        ,10)
	TRCell():New( oSection1, 'DESCRICAO'			,'OFC1', 		'DESCRICAO',		  			    "@!"				        ,35)
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 09 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("OFC1") > 0
		OFC1->(dbCloseArea())
	Endif
      
    cQuery := " SELECT LEFT(C2_PRODUTO,8) AS REFERENCIA, B4_DESC AS DESCRICAO FROM "+RetSqlName('SC2')+" " 
    cQuery += " JOIN "+RetSqlName('SB4')+" SB4010 ON B4_COD=LEFT(SC2010.C2_PRODUTO,8) " 
    cQuery += " WHERE SC2010.D_E_L_E_T_='' " 
    cQuery += " AND LEFT(SC2010.C2_PRODUTO,8) NOT IN (SELECT G2_REFGRD FROM " +RetSqlName('SG2')+ " WHERE SG2010.D_E_L_E_T_='') " 
    cQuery += " GROUP BY LEFT(SC2010.C2_PRODUTO,8),B4_DESC " 
	
	TCQUERY cQuery NEW ALIAS OFC1

	While OFC1->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("REFERENCIA"):SetValue(OFC1->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")

		oSection1:Cell("DESCRICAO"):SetValue(OFC1->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
												
		oSection1:PrintLine()
		
		OFC1->(DBSKIP()) 
	enddo
	OFC1->(DBCLOSEAREA())
Return( Nil )

