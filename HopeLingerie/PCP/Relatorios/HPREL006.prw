#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL006 
Relatório de Necessidade  
@author Weskley Silva
@since 31/10/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL006()

	Private oReport

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 31 de Outubro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'MRP', 'RELATORIO NECESSIDADE ', , {|oReport| ReportPrint( oReport ), 'RELATORIO NECESSIDADE' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATORIO NECESSIDADE', { 'MRP', 'CZJ','CZK','SB4'})
			
	TRCell():New( oSection1, 'DOC'		    	    ,'MRP', 		'DOC',								"@!"                        ,10)
	TRCell():New( oSection1, 'MRP'		    	    ,'MRP', 		'MRP',								"@!"                        ,10)
	TRCell():New( oSection1, 'TIPO'		    	    ,'MRP', 		'TIPO',								"@!"                        ,10)
	TRCell():New( oSection1, 'CODIGO'      	        ,'MRP', 		'CODIGO',	    					"@!"				        ,15)
	TRCell():New( oSection1, 'SKU'          		,'MRP', 		'SKU',              				"@!"						,20)
	TRCell():New( oSection1, 'DESCRICAO'         	,'MRP', 		'DESCRICAO',	              		"@!"						,35)
	TRCell():New( oSection1, 'LOTE_MINIMO'		    ,'MRP', 		'LOTE_MINIMO',						"@!"                        ,10)
	TRCell():New( oSection1, 'LOTE_MULTIPLO'		,'MRP', 		'LOTE_MULTIPLO',     				"@!"                        ,10)
	TRCell():New( oSection1, 'ESTOQUE_SEGURANCA'	,'MRP', 		'ESTOQUE_SEGURANCA',   				"@E 999,99"                 ,10)
	TRCell():New( oSection1, 'PERIODO' 				,'MRP', 		'PERIODO' ,	        				"@!"		                ,04)
	TRCell():New( oSection1, 'SALDO_ESTOQUE'		,'MRP', 		'SALDO_ESTOQUE',       				"@E 999,99"			        ,15)
	TRCell():New( oSection1, 'ENTRADA' 				,'MRP', 		'ENTRADA' ,	    					"@E 999,99"			        ,15)
	TRCell():New( oSection1, 'SAIDAS'	        	,'MRP', 		'SAIDAS' ,		       				"@E 999,99"	        	    ,15)
	TRCell():New( oSection1, 'SAIDAS_ESTRUTURA'	    ,'MRP', 		'SAIDAS_ESTRUTURA',	   				"@E 999,99"					,15)
	TRCell():New( oSection1, 'SALDO_FINAL'			 ,'MRP', 		'SALDO_FINAL',			   			"@E 999,99"					,15)
	TRCell():New( oSection1, 'NECESSIDADE'			,'MRP', 		'NECESSIDADE' ,	    				"@E 999,99"	            	,15)
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 31 de Outubro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("MRP") > 0
		MRP->(dbCloseArea())
	Endif
      
	cQuery := " SELECT (SELECT CZI_DOC AS PREVISAO FROM CZI010 WHERE CZI_ALIAS IN ('SC4','SHC') AND D_E_L_E_T_='' " 
	cQuery += " GROUP BY CZI_DOC ) AS DOC,  CZJ_NRMRP AS MRP, SB4.B4_TIPO AS TIPO, LEFT(CZJ_PROD,8) AS CODIGO, CZJ_PROD AS SKU, B4_DESC AS DESCRICAO, B1_LE AS LOTE_MINIMO , B1_LM AS LOTE_MULTIPLO, B1_ESTSEG AS ESTOQUE_SEGURANCA, CZK_PERMRP AS PERIODO, " 
	cQuery += " CZK_QTSLES AS SALDO_ESTOQUE, CZK_QTENTR AS ENTRADAS, CZK_QTSAID AS SAIDAS, CZK_QTSEST AS SAIDAS_ESTRUTURA,CZK_QTSALD as SALDO_FINAL, CZK_QTNECE AS NECESSIDADE "
	cQuery += " FROM "+RetSqlName('CZJ')+"  CZJ "
	cQuery += " INNER JOIN "+RetSqlName('CZK')+" CZK ON CZK.D_E_L_E_T_ = '' AND CZK_NRMRP = CZJ_NRMRP AND CZK_RGCZJ = CZJ.R_E_C_N_O_ "
	cQuery += " INNER JOIN "+RetSqlName('SB4')+" SB4 ON SB4.D_E_L_E_T_ = '' AND B4_COD = LEFT(CZJ_PROD,8) "
	cQuery += " INNER JOIN "+RetSqlName('SB1')+" SB1 ON SB1.D_E_L_E_T_ ='' AND B1_COD= CZJ_PROD " 
	cQuery += " WHERE CZJ.D_E_L_E_T_ = '' " 
	
	TCQUERY cQuery NEW ALIAS MRP

	While MRP->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("DOC"):SetValue(MRP->DOC)
		oSection1:Cell("DOC"):SetAlign("LEFT")
		
		oSection1:Cell("MRP"):SetValue(MRP->MRP)
		oSection1:Cell("MRP"):SetAlign("LEFT")

		oSection1:Cell("TIPO"):SetValue(MRP->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")

		oSection1:Cell("CODIGO"):SetValue(MRP->CODIGO)
		oSection1:Cell("CODIGO"):SetAlign("LEFT")
		
		oSection1:Cell("SKU"):SetValue(MRP->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
		
		oSection1:Cell("DESCRICAO"):SetValue(MRP->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")

		oSection1:Cell("LOTE_MINIMO"):SetValue(MRP->LOTE_MINIMO)
		oSection1:Cell("LOTE_MINIMO"):SetAlign("LEFT")
		
		oSection1:Cell("LOTE_MULTIPLO"):SetValue(MRP->LOTE_MULTIPLO)
		oSection1:Cell("LOTE_MULTIPLO"):SetAlign("LEFT")
			
		oSection1:Cell("ESTOQUE_SEGURANCA"):SetValue(MRP->ESTOQUE_SEGURANCA)
		oSection1:Cell("ESTOQUE_SEGURANCA"):SetAlign("LEFT")
		
		oSection1:Cell("PERIODO"):SetValue(MRP->PERIODO)
		oSection1:Cell("PERIODO"):SetAlign("LEFT")
				
		oSection1:Cell("SALDO_ESTOQUE"):SetValue(MRP->SALDO_ESTOQUE)
		oSection1:Cell("SALDO_ESTOQUE"):SetAlign("LEFT")
				
		oSection1:Cell("SALDO_ESTOQUE"):SetValue(MRP->SALDO_ESTOQUE)
		oSection1:Cell("SALDO_ESTOQUE"):SetAlign("LEFT")		
				
		oSection1:Cell("SAIDAS"):SetValue(MRP->SAIDAS)
		oSection1:Cell("SAIDAS"):SetAlign("LEFT")
		
		oSection1:Cell("SAIDAS_ESTRUTURA"):SetValue(MRP->SAIDAS_ESTRUTURA)
		oSection1:Cell("SAIDAS_ESTRUTURA"):SetAlign("LEFT")
		
		oSection1:Cell("SALDO_FINAL"):SetValue(MRP->SALDO_FINAL)
		oSection1:Cell("SALDO_FINAL"):SetAlign("LEFT")
								
		oSection1:Cell("NECESSIDADE"):SetValue(MRP->NECESSIDADE)
		oSection1:Cell("NECESSIDADE"):SetAlign("LEFT")
								
		oSection1:PrintLine()
		
		MRP->(DBSKIP()) 
	enddo
	MRP->(DBCLOSEAREA())
Return( Nil )
