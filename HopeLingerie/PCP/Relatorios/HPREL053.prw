//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de analise FIFO C.Q.
Rotina para gerar as entradas pendentes de libera��o no C.Q. por dara de entrada;

@author Ronaldo Pereira
@since 29 de Junho de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL053()

	Local aRet := {}
	Local aPerg := {}
	 
	aAdd(aPerg,{1,"Nota Fiscal: " 	,Space(15),""   ,""		,""	    ,""		,0	,.F.})
	aAdd(aPerg,{1,"Produto: " 		,Space(15),""   ,""		,""	    ,""		,0	,.F.}) 
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| Geradados(aRet)}, "Exportando dados")		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'FIFO CQ'+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "FIFO"
    Local cTable1    	:= "Libera��o Pendente C.Q." 

        
    //Dados tabela 1          
    cQry := " SELECT T.D7_FILIAL AS FILIAL, "
    cQry += "      CONVERT(CHAR, CAST(T.D7_DATA AS SMALLDATETIME), 103) AS DATA_ENTRADA, "
    cQry += " 	   T.D7_DOC AS DOCUMENTO, "
    cQry += " 	   T.D7_NUMERO AS NUM_SK, "
    cQry += " 	   T.D7_LOCAL AS ARMAZEM, "
    cQry += " 	   T.B1_TIPO AS TIPO, "
    cQry += " 	   T.D7_PRODUTO AS PRODUTO, "
    cQry += " 	   T.B1_DESC AS DESCRICAO, "
    cQry += " 	   T.B1_UM AS UNID_MED, "
    cQry += " 	   T.GRUPO  AS GRUPO, "
    cQry += " 	   REPLACE(T.QTDE_NF,'.',',') AS QTDE_NF, "
    cQry += " 	   REPLACE(T.QTDE_LIBERADA,'.',',') AS QTDE_LIBERADA, "
    cQry += " 	   REPLACE((T.QTDE_NF - T.QTDE_LIBERADA),'.',',') AS QTDE_PENDENTE "
    cQry += " FROM ( "
    cQry += " SELECT D7.D7_FILIAL, "
    cQry += " 	   D7.D7_DATA, "
    cQry += " 	   D7.D7_DOC, "
    cQry += " 	   D7.D7_LOCAL, "
    cQry += " 	   B1.B1_TIPO, "
    cQry += " 	   D7.D7_PRODUTO, "
    cQry += " 	   B1.B1_DESC, "
    cQry += " 	   B1.B1_UM, "
    cQry += " 	   (B1.B1_GRUPO + ' - ' + BM.BM_DESC) AS GRUPO, "
    cQry += " 	   CONVERT(DECIMAL(12,2),D7_SALDO) AS QTDE_NF, "
    cQry += " 	   D7.D7_NUMERO, "
    cQry += " 	   (SELECT CONVERT(DECIMAL(12,2),SUM(Q.D7_QTDE))  FROM SD7010 AS Q (NOLOCK) WHERE Q.D_E_L_E_T_ = '' " 
    cQry += " 	                                                       AND Q.D7_FILIAL = D7.D7_FILIAL "
    cQry += " 	                                                       AND Q.D7_DOC = D7.D7_DOC " 
    cQry += " 														   AND Q.D7_NUMERO = D7.D7_NUMERO "
    cQry += " 														   AND Q.D7_FORNECE = D7.D7_FORNECE "
    cQry += " 														   AND Q.D7_SERIE = D7.D7_SERIE " 
    cQry += " 														   AND Q.D7_PRODUTO = D7.D7_PRODUTO "
    cQry += " 														   AND Q.D7_ESTORNO = D7.D7_ESTORNO) AS QTDE_LIBERADA "
    cQry += " FROM SD7010 AS D7 WITH (NOLOCK) " 
    cQry += " INNER JOIN SB1010 AS B1 WITH (NOLOCK) ON (B1.B1_COD = D7.D7_PRODUTO AND B1.D_E_L_E_T_ = '') "
    cQry += " INNER JOIN SBM010 AS BM WITH (NOLOCK) ON (BM.BM_GRUPO = B1.B1_GRUPO AND BM.D_E_L_E_T_ = '') "
    cQry += " WHERE D7.D_E_L_E_T_ = '' AND D7.D7_ESTORNO = '' AND D7.D7_FILIAL = '0101' AND D7.D7_TIPO = '0' "

    If !Empty(aRet[1])
    	cQry += " AND D7.D7_DOC = '"+Alltrim(aRet[1])+"' "
	Endif 
	
	If !Empty(aRet[2])
    	cQry += " AND D7.D7_PRODUTO = '"+Alltrim(aRet[2])+"' "
	Endif

    cQry += " GROUP BY D7.D7_FILIAL, "
    cQry += " 	   D7.D7_DATA, " 
    cQry += " 	   D7.D7_DOC, "
    cQry += " 	   D7.D7_NUMERO, "
    cQry += " 	   D7.D7_SERIE, "
    cQry += " 	   D7.D7_FORNECE, "
    cQry += " 	   D7.D7_LOCAL, "
    cQry += " 	   B1.B1_TIPO, "
    cQry += " 	   D7.D7_PRODUTO, "
    cQry += " 	   D7.D7_ESTORNO, "
    cQry += " 	   D7.D7_SALDO, "
    cQry += " 	   B1.B1_DESC, "
    cQry += " 	   B1.B1_UM, "
    cQry += " 	   B1.B1_GRUPO, "
    cQry += " 	   BM.BM_DESC "
    cQry += " ) T "
    cQry += " GROUP BY T.D7_FILIAL, "
    cQry += "      T.D7_DATA, "
    cQry += " 	   T.D7_DOC, "
    cQry += " 	   T.D7_NUMERO, "
    cQry += " 	   T.D7_LOCAL, "
    cQry += " 	   T.B1_TIPO, "
    cQry += " 	   T.D7_PRODUTO, "
    cQry += " 	   T.QTDE_NF, "
    cQry += " 	   T.QTDE_LIBERADA, "
    cQry += " 	   T.B1_DESC, "
    cQry += " 	   T.B1_UM, "
    cQry += " 	   T.GRUPO "
    cQry += " HAVING (T.QTDE_NF - T.QTDE_LIBERADA) > 0 "
    cQry += " ORDER BY T.D7_DATA "

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DATA_ENTRADA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DOCUMENTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"NUM_SK",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ARMAZEM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PRODUTO",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESCRICAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"UNID_MED",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"GRUPO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE_NF",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE_LIBERADA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE_PENDENTE",1)
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                               QRY->FILIAL,;
                                               QRY->DATA_ENTRADA	,;
                                               QRY->DOCUMENTO,;
                                               QRY->NUM_SK,;
                                               QRY->ARMAZEM,;
                                               QRY->TIPO,;
                                               QRY->PRODUTO,;
                                               QRY->DESCRICAO,;                                               
                                               QRY->UNID_MED,;
                                               QRY->GRUPO,;
                                               QRY->QTDE_NF,;
                                               QRY->QTDE_LIBERADA,;
                                               QRY->QTDE_PENDENTE;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return