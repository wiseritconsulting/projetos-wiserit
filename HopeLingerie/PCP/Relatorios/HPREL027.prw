#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL026
Acompanhamento Plano Mestre
@author Weskley Silva
@since 26/12/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL027()

	Private oReport
	Private cPergCont	:= 'HPREL027' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 14 de Dezembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'APS', 'ACOMPANHAMENTO PRODUCAO', cPergCont, {|oReport| ReportPrint( oReport ), 'ACOMPANHAMENTO PRODUCAO' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'ACOMPANHAMENTO PRODUCAO ', { 'APS', 'SHC','SB4','SBV','SC2'})
			
	TRCell():New( oSection1, 'OP'		    	    ,'APS', 		'OP',								"@!"                        ,08)
	TRCell():New( oSection1, 'DT_EMISSAO'     	    ,'APS', 		'DT_EMISSAO',						"@!"                        ,15)
	TRCell():New( oSection1, 'DATA_LIBERACAO'     	,'APS', 		'DT_LIBERACAO',						"@!"                        ,15)
	TRCell():New( oSection1, 'ITEM_GRD'     	    ,'APS', 		'ITEM_GRD',	    					"@!"				        ,10)
	TRCell():New( oSection1, 'STATUS_OP'  	        ,'APS', 		'STATUS_OP',           				"@!"						,25)
	TRCell():New( oSection1, 'SKU'  	        	,'APS', 		'SKU',	              				"@!"						,10)
	TRCell():New( oSection1, 'TIPO'      			,'APS', 		'TIPO',	   							"@!"						,10)
	TRCell():New( oSection1, 'CICLO'         		,'APS', 		'CICLO',	              			"@!"						,35)
	TRCell():New( oSection1, 'SUBCOLECAO'			,'APS', 		'SUBCOLECAO', 	    				"@!"		                ,25)
	TRCell():New( oSection1, 'REF'	 				,'APS', 		'REF',				       			"@!"		                ,25)
	TRCell():New( oSection1, 'COD_COR' 				,'APS', 		'COD_COR' ,    						"@!"		                ,10)
	TRCell():New( oSection1, 'DESC_COR' 			,'APS', 		'DESC_COR' ,    					"@!"		                ,35)
	TRCell():New( oSection1, 'DESCR'	   			,'APS', 		'DESCR' , 		       				"@!"		                ,20)
	TRCell():New( oSection1, 'TAM'	   				,'APS', 		'TAM' , 		       				"@!"		                ,20)
	TRCell():New( oSection1, 'QTDE_INICIAL'			,'APS', 		'QTDE_INICIAL',		   				"@E 999,999.99"				,20)
	TRCell():New( oSection1, 'QTDE_APONTADA'		,'APS', 		'QTDE_APONTADA' ,    				"@E 999,999.99"	            ,20)
	TRCell():New( oSection1, 'PERDA'				,'APS', 		'PERDA' ,    						"@E 999,999.99"	            ,20)
	TRCell():New( oSection1, 'PEND_APONT'			,'APS', 		'PEND_APONT' ,					  	"@E 999,999.99"		        ,20)
	TRCell():New( oSection1, 'OPERACAO'				,'APS', 		'OPERACAO',					    	"@!"		             	,15)
	TRCell():New( oSection1, 'DESC_OPERACAO'		,'APS', 		'DESC_OPERACAO',	    			"@!"	            		,40)
	TRCell():New( oSection1, 'DT_MOVIMENTO'		    ,'APS', 		'DT_MOVIMENTO',		   				"@!"     					,15)
	TRCell():New( oSection1, 'RECURSO'				,'APS', 		'RECURSO',			    			"@!"	            		,15)
	TRCell():New( oSection1, 'FORNECEDOR'			,'APS', 		'FORNECEDOR',	    				"@!"	            		,50)
	TRCell():New( oSection1, 'TOTAL_FASE'			,'APS', 		'TOTAL_FASE',	    				"@E 999,999.99"	            ,15)	
	TRCell():New( oSection1, 'PLANO'				,'APS', 		'PLANO',		    				"@!"	           	 		,15)
	TRCell():New( oSection1, 'DT_PLANO'				,'APS', 		'DT_PLANO',	    					"@!"	            		,15)
	TRCell():New( oSection1, 'PREVISAO'				,'APS', 		'PREVISAO',	    					"@!"	        		    ,15)
	TRCell():New( oSection1, 'DT_PREVISAO'			,'APS', 		'DT_PREVISAO',	    				"@!"	          			,15)								

Return( oReport )



//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 14 de Dezembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("APS") > 0
		APS->(dbCloseArea())
	Endif
      
     cQuery := " SELECT * FROM PCP_APS " 
     
     if !Empty(mv_par01)
     
     	cQuery += " WHERE  PCP_APS.OPERACAO = '"+ Alltrim(mv_par01) +"' " 
     
     else
     
     	cQuery += " "
     	
     endif
     
     	cQuery += "ORDER BY OP,SKU,OPERACAO " 

	 TCQUERY cQuery NEW ALIAS APS

	While APS->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("OP"):SetValue(APS->OP)
		oSection1:Cell("OP"):SetAlign("LEFT")
		
		oSection1:Cell("DT_EMISSAO"):SetValue(APS->DT_EMISSAO)
		oSection1:Cell("DT_EMISSAO"):SetAlign("LEFT")
		
		oSection1:Cell("DT_LIBERACAO"):SetValue(APS->DATA_LIBERACAO)
		oSection1:Cell("DT_LIBERACAO"):SetAlign("LEFT")		
		
		oSection1:Cell("ITEM_GRD"):SetValue(APS->ITEM_GRD)
		oSection1:Cell("ITEM_GRD"):SetAlign("LEFT")
			
		oSection1:Cell("STATUS_OP"):SetValue(APS->STATUS_OP)
		oSection1:Cell("STATUS_OP"):SetAlign("LEFT")
		
		oSection1:Cell("SKU"):SetValue(APS->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(APS->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("CICLO"):SetValue(APS->CICLO)
		oSection1:Cell("CICLO"):SetAlign("LEFT")
		
		oSection1:Cell("SUBCOLECAO"):SetValue(APS->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("REF"):SetValue(APS->REF)
		oSection1:Cell("REF"):SetAlign("LEFT")
		
	    oSection1:Cell("COD_COR"):SetValue(APS->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COR"):SetValue(APS->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESCR"):SetValue(APS->DESCR)
		oSection1:Cell("DESCR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(APS->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
				
		oSection1:Cell("QTDE_INICIAL"):SetValue(APS->QTDE_INICIAL)
		oSection1:Cell("QTDE_INICIAL"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_APONTADA"):SetValue(APS->QTDE_APONTADA)
		oSection1:Cell("QTDE_APONTADA"):SetAlign("LEFT")
		
		oSection1:Cell("PERDA"):SetValue(APS->PERDA)
		oSection1:Cell("PERDA"):SetAlign("LEFT")		
		
		oSection1:Cell("PEND_APONT"):SetValue(APS->PEND_APONT)
		oSection1:Cell("PEND_APONT"):SetAlign("LEFT")
					
		oSection1:Cell("OPERACAO"):SetValue(APS->OPERACAO)
		oSection1:Cell("OPERACAO"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_OPERACAO"):SetValue(APS->DESC_OPERACAO)
		oSection1:Cell("DESC_OPERACAO"):SetAlign("LEFT")
	
		oSection1:Cell("DT_MOVIMENTO"):SetValue(APS->DT_MOVIMENTO)
		oSection1:Cell("DT_MOVIMENTO"):SetAlign("LEFT")
	
		oSection1:Cell("RECURSO"):SetValue(APS->RECURSO)
		oSection1:Cell("RECURSO"):SetAlign("LEFT")
		
		oSection1:Cell("FORNECEDOR"):SetValue(APS->FORNECEDOR)
		oSection1:Cell("FORNECEDOR"):SetAlign("LEFT")
		
		oSection1:Cell("TOTAL_FASE"):SetValue(APS->TOTAL_FASE)
		oSection1:Cell("TOTAL_FASE"):SetAlign("LEFT")
		
		oSection1:Cell("PLANO"):SetValue(APS->PLANO)
		oSection1:Cell("PLANO"):SetAlign("LEFT")	
		
		oSection1:Cell("DT_PLANO"):SetValue(APS->DT_PLANO)
		oSection1:Cell("DT_PLANO"):SetAlign("LEFT")	
		
		oSection1:Cell("PREVISAO"):SetValue(APS->PREVISAO)
		oSection1:Cell("PREVISAO"):SetAlign("LEFT")	
		
		oSection1:Cell("DT_PREVISAO"):SetValue(APS->DT_PREVISAO)
		oSection1:Cell("DT_PREVISAO"):SetAlign("LEFT")		
			
		oSection1:PrintLine()
		
		APS->(DBSKIP()) 
	enddo
	APS->(DBCLOSEAREA())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 14 de Dezembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Operacao ? "		        ,""		,""		,"mv_ch1","C",02,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
return