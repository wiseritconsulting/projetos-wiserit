#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL025
Saldo por Armazem
@author Weskley Silva
@since 13/12/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL025()

	Private oReport
	Private cPergCont	:= 'HPREL025' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 11 de Agosto de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'DEP', 'DEFEITOS POR OP ', cPergCont, {|oReport| ReportPrint( oReport ), 'DEFEITOS POR OP' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'DEFEITOS POR OP ', { 'DEP', 'SB2','SX5','SB4','SBV'})
			
	TRCell():New( oSection1, 'FILIAL'	    	    ,'DEP', 		'FILIAL',							"@!"                        ,40)
	TRCell():New( oSection1, 'DATA'      	        ,'DEP', 		'DATA',		    					"@!"				        ,10)
	TRCell():New( oSection1, 'OP'  	        		,'DEP', 		'OP',	              				"@!"						,10)
	TRCell():New( oSection1, 'SKU' 		        	,'DEP', 		'SKU',			              		"@!"						,20)
	TRCell():New( oSection1, 'DESCRICAO' 			,'DEP', 		'DESCRICAO' ,	       				"@!"		                ,35)
	TRCell():New( oSection1, 'CICLO_PRODUCAO'	    ,'DEP', 		'CICLO_PRODUCAO',       			"@!"		                ,20)
	TRCell():New( oSection1, 'MARCA' 				,'DEP', 		'MARCA' ,	    					"@!"		                ,20)
	TRCell():New( oSection1, 'COLECAO'	       		,'DEP', 		'COLECAO' ,	        				"@!"		                ,10)
	TRCell():New( oSection1, 'COD_COLECAO'		    ,'DEP', 		'COD_COLECAO',		   				"@!"						,15)
	TRCell():New( oSection1, 'REFERENCIA'			,'DEP', 		'REFERENCIA' ,	    				"@!"	                    ,20)
	TRCell():New( oSection1, 'COD_COR'				,'DEP', 		'COD_COR' ,		  				  	"@!"		                ,20)
	TRCell():New( oSection1, 'DESC_COR'				,'DEP', 		'DESC_COR',					    	"@!"		                ,03)
	TRCell():New( oSection1, 'TAM'					,'DEP', 		'TAM' ,				    			"@!"	                    ,08)
	TRCell():New( oSection1, 'DEFEITO'		        ,'DEP', 		'DEFEITO',				   			"@!"						,15)
	TRCell():New( oSection1, 'QUANTIDADE'		    ,'DEP', 		'QUANTIDADE',			   			"@!"						,15)
									

Return( oReport )



//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 11 de Agosto de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("DEP") > 0
		DEP->(dbCloseArea())
	Endif
      
	 cQuery := " SELECT BC_FILIAL AS FILIAL, " 
	 cQuery += " RIGHT(BC_DATA,2)+'/'+SUBSTRING(BC_DATA,5,2)+'/'+LEFT(BC_DATA,4) AS DATA," 
	 cQuery += " LEFT(BC_OP,6) AS OP,  "
	 cQuery += " BC_PRODUTO AS SKU,  "
	 cQuery += " B4.B4_DESC AS DESCRICAO, "
	 cQuery += " B4.B4_YNCICLO AS CICLO_PRODUCAO, "
	 cQuery += " B4.B4_YNOMARC AS MARCA, "
	 cQuery += " B4.B4_YNCOLEC AS COLECAO, "
	 cQuery += " B4.B4_YCOLECA AS COD_COLECAO, "
	 cQuery += " B4.B4_COD AS REFERENCIA, "
	 cQuery += " SUBSTRING(BC_PRODUTO,9,3) AS COD_COR, "
	 cQuery += " SBV.BV_DESCRI AS DESC_COR,  "
	 cQuery += " RIGHT(BC_PRODUTO,4)  AS TAM, "
	 cQuery += " X5_DESCRI AS DEFEITO, BC_QUANT AS QUANTIDADE "

	 cQuery += " FROM "+ RetSqlName('SBC') + " SB "
	 cQuery += " JOIN "+ RetSqlName('SX5') + " SX ON SX.X5_CHAVE=SB.BC_MOTIVO " 
	 cQuery += " JOIN "+ RetSqlName('SB4') + " B4 ON B4.B4_COD=LEFT(SB.BC_PRODUTO,8) " 
	 cQuery += " JOIN "+ RetSqlName('SBV') + " SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(BC_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' " 
	 cQuery += " WHERE SX.X5_TABELA = '43' AND SX.D_E_L_E_T_ = '' AND B4.D_E_L_E_T_='' AND SB.D_E_L_E_T_=''  "
	 cQuery += " AND SB.BC_DATA BETWEEN '"+ DTOS(mv_par01)+ "' AND '" + DTOS(mv_par02) +"' "

	 TCQUERY cQuery NEW ALIAS DEP

	While DEP->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("FILIAL"):SetValue(DEP->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")
		
		oSection1:Cell("DATA"):SetValue(DEP->DATA)
		oSection1:Cell("DATA"):SetAlign("LEFT")
		
		oSection1:Cell("OP"):SetValue(DEP->OP)
		oSection1:Cell("OP"):SetAlign("LEFT")
		
		oSection1:Cell("SKU"):SetValue(DEP->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
		
		oSection1:Cell("DESCRICAO"):SetValue(DEP->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("CICLO_PRODUCAO"):SetValue(DEP->CICLO_PRODUCAO)
		oSection1:Cell("CICLO_PRODUCAO"):SetAlign("LEFT")
		
		oSection1:Cell("MARCA"):SetValue(DEP->MARCA)
		oSection1:Cell("MARCA"):SetAlign("LEFT")
		
		oSection1:Cell("COLECAO"):SetValue(DEP->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COLECAO"):SetValue(DEP->COD_COLECAO)
		oSection1:Cell("COD_COLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(DEP->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(DEP->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COR"):SetValue(DEP->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(DEP->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
		
		oSection1:Cell("DEFEITO"):SetValue(DEP->DEFEITO)
		oSection1:Cell("DEFEITO"):SetAlign("LEFT")
		
		oSection1:Cell("QUANTIDADE"):SetValue(DEP->QUANTIDADE)
		oSection1:Cell("QUANTIDADE"):SetAlign("LEFT")
		
			
		oSection1:PrintLine()
		
		DEP->(DBSKIP()) 
	enddo
	DEP->(DBCLOSEAREA())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 11 de Agosto de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Dt Inicial"		        ,""		,""		,"mv_ch1","D",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Dt Final"			    ,""		,""		,"mv_ch2","D",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
return