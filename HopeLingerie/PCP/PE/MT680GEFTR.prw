#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"

User Function MT680GEFTR()

	_qry := " UPDATE "+RetSqlName("SD3")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ "
	_qry += " WHERE D_E_L_E_T_ = '' AND D3_COD LIKE 'MOD%' "
//	_qry += " 		AND D3_IDENT= '"+SH6->H6_IDENT+"' "
	_qry += " 		AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' "
	TcSqlExec(_qry)

Return()