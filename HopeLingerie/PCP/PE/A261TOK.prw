#Include 'Protheus.ch'

User Function A261TOK( )

Local lRet := .T. 
Local _erro := ""
Local nPosOP    := aScan(aHeader,{|x| AllTrim(x[2]) == 'D3_XOP'})

_area := GetArea()
For _i := 1 to len(aCols)
	If ACOLS[_i,len(AHEADER)+1] == .F. .and. aCols[_i,9] = "AP"
		If alltrim(aCols[_i,nPosOP]) <> ""
			If POSICIONE("SC2",1,xFilial("SC2")+alltrim(aCols[_i,nPosOP]),"C2_XRES") <> "L"
				lret := .F.
				_erro += "Ordem de Produ��o n�o est� liberada! "+chr(13)
			Else
				DbSelectArea("SB2")
				_areaB2 := GetArea()
				DbSetOrder(1)
				DbSeek(xfilial("SB2")+aCols[_i,1]+aCols[_i,4])
				If Found()
					If (B2_QATU - B2_QEMP - B2_RESERVA) < aCols[_i,16]
						lret := .F.
						_erro += "Saldo insuficiente para atender a OP! "+chr(13)
					Endif
				Else
					lret := .F.
					_erro += "Produto n�o tem saldo cadastrado! "+chr(13)
				Endif
				RestArea(_areaB2)
			Endif
		Else
			DbSelectArea("SB2")
			_areaB2 := GetArea()
			DbSetOrder(1)
			DbSeek(xfilial("SB2")+aCols[_i,1]+aCols[_i,4])
			If Found()
				If (B2_QATU - B2_QEMP - B2_RESERVA - B2_XRES) < aCols[_i,16]
					lret := .F.
					_erro += "Saldo insuficiente! "+chr(13)
				Endif
			Else
				lret := .F.
				_erro += "Produto n�o tem saldo cadastrado! "+chr(13)
			Endif
			RestArea(_areaB2)
		Endif
	Endif
Next

If !lRet
	Alert(_erro)
Else
	For _i := 1 to len(aCols)
		If ACOLS[_i,len(AHEADER)+1] == .F. .and. aCols[_i,9] = "AP"
			_qry := "update "+RetSqlName("SC2")+" set C2_XRES = 'T' where D_E_L_E_T_ = '' and C2_FILIAL = '"+xfilial("SC2")+"' and C2_NUM+C2_ITEM+C2_SEQUEN = '"+left(aCols[_i,nPosOP],11)+"' "
			TcSqlExec(_qry)
			
			// TODO WESKLEY SILVA -- Update para todo produto do tipo KIT ficar como Transferido
			_qry := " UPDATE "+RetSqlName("SC2")+" SET C2_XRES = 'T' FROM "+RetSqlName("SC2")+"  JOIN "+RetSqlName("SB1")+" ON C2_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = '' WHERE C2_XRES = 'L' AND SC2010.D_E_L_E_T_ = '' AND B1_TIPO = 'KT' AND C2_NUM = '"+left(aCols[_i,nPosOP],6)+"' "
			TcSqlExec(_qry)
			
			_qry := " UPDATE "+RetSqlName("SC2")+" SET C2_XRES = 'T' WHERE LEFT(C2_PRODUTO,4) IN ('MIEE','MIEF','MIEC') AND C2_NUM = '"+left(aCols[_i,nPosOP],6)+"' AND D_E_L_E_T_ = '' AND C2_XRES = 'L' "
			TcSqlExec(_qry)
			
			DbSelectArea("SD4")
			_areaD4 := GetArea()
			
			 If Select("TMP") > 0
			 	TMP->(dbCloseArea())
			 EndIf
			
			_qry := " SELECT * FROM "+RetSqlName('SD4')+" SD4 WITH(NOLOCK) WHERE D4_FILIAL = '"+xfilial("SD4")+"' AND LEFT(D4_OP,11) = '"+left(aCols[_i,nPosOP],11)+"' AND D4_COD = '"+aCols[_i,1]+"' AND D_E_L_E_T_  = '' "	
				 
			 dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMP",.T.,.T.)
			 DbSelectArea("TMP")
			 DbGoTop()	
				
			While !EOF() .and. TMP->D4_COD = aCols[_i,1] .and. left(TMP->D4_OP,12) = left(aCols[_i,nPosOP],12)
				
			_qry1 := "Update "+RetSqlName("SD4")+" set D4_XQTRANS = '"+STR(TMP->D4_QUANT)+"' , D4_XDTTRA = '"+ DTOS(ddatabase) +"'  WHERE D4_FILIAL = '"+xfilial("SD4")+"' AND LEFT(D4_OP,11) = '"+left(aCols[_i,nPosOP],11)+"' AND D4_COD = '"+aCols[_i,1]+"' AND D_E_L_E_T_  = '' "
			TcSqlExec(_qry1)
		
			_qry := "Update "+RetSqlName("SB2")+" set B2_XRES = B2_XRES - "+str(TMP->D4_XRES)+" where B2_FILIAL = '"+xfilial("SB2")+"' and D_E_L_E_T_ = '' and B2_COD = '"+TMP->D4_COD+"' and B2_LOCAL = '"+aCols[_i,4]+"' "
			TcSqlExec(_qry)
				
				Dbskip()
			End
		Endif
	Next
Endif
RestArea(_area)
RestArea(_areaD4)
Return lRet