#Include 'Protheus.ch'

User Function MTSELEOP()
Local cRet 	 := ParamIxb[1]
Local cProd := ParamIxb[2]
Local cProg := ParamIxb[3]
Local lRet := .F.

If cProg = "MATA650" .or. cProg = "MATA750"
	lRet := .T.
Endif

Return(lRet)

