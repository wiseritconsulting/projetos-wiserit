#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

user function MT750BRW()

Local aRotUser := {}

AAdd( aRotUser, { 'Perda', 'U_Altperda()', 0, 4 } )

Return (aRotUser)


User Function Altperda()

Local oButton1
Local oButton2
Local cPerda
Local cPlano
Local cProduto
Private oGet1
Private cGet1 := SHC->HC_DOC
private oGet2
Private cGet2 := SHC->HC_PRODUTO
Private oGet3
Private cGet3 := Space(6)
Static oDlg

  DEFINE MSDIALOG oDlg TITLE "Perda - Plano Mestre" FROM 000, 000  TO 200, 280 COLORS 0, 16777215 PIXEL
    
    @ 026, 023 SAY cPlano PROMPT "Plano" SIZE 018, 008 OF oDlg COLORS 0, 16777215  PIXEL
    @ 025, 052 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
    @ 043, 023 SAY cProduto PROMPT "Produto" SIZE 022, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 041, 052 MSGET oGet2 VAR cGet2 SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
    @ 057, 023 SAY cPerda PROMPT "Perda" SIZE 018, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 056, 052 MSGET oGet3 VAR cGet3 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 074, 033 BUTTON oButton1 PROMPT "Confirma" SIZE 037, 012  OF oDlg  ACTION (u_SalPerd(cGet1,cGet2),oDlg:End()) PIXEL
    @ 074, 083 BUTTON oButton2 PROMPT "Cancela" SIZE 037, 012 OF  oDlg Action oDlg:End() PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED

return

User Function SalPerd(_par1,_par2)

Local _qry := ""

	_qry := "Update "+RetSqlname("SHC")+" Set HC_XPERDA = '"+cGet3+"' where HC_DOC = '"+_par1+"' and HC_PRODUTO = '"+_par2+"' AND HC_FILIAL = '"+xfilial("SHC")+"' "
	TcSqlExec(_qry)

return
