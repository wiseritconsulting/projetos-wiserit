#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL039 
Relat�rio de Absente�smo RH
@author Ronaldo Pereira
@since 07/12/2018
@version 1.0
/*/

user function HPREL039()

Private oReport
Private cPergCont	:= 'HPREL039' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Ronaldo Pereira
@since 07 de Dezembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
	
Static Function ReportDef()

	Local oReport
	Local oSection1

	oReport := TReport():New( 'RRH', 'RELATORIO DE ABSENTEISMO - RH', , {|oReport| ReportPrint( oReport ), 'RELATORIO DE ABSENTEISMO - RH' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATORIO DE ABSENTEISMO - RH', {'RRH','SPH','SRA','SRJ','SQB','SP9','SP6'})
	
	TRCell():New( oSection1, 'DATA'	                ,'RRH', 		'DATA',					            "@!"                        ,15)
	TRCell():New( oSection1, 'FILIAL'	    		,'RRH', 		'FILIAL',							"@!"                        ,04)			
	TRCell():New( oSection1, 'MATRICULA'	    	,'RRH', 		'MATRICULA',						"@!"                        ,06)
	TRCell():New( oSection1, 'NOME'			        ,'RRH', 		'NOME',       					    "@!"		                ,60)
	TRCell():New( oSection1, 'CARGO'				,'RRH', 	    'CARGO',       						"@!"		                ,40)
	TRCell():New( oSection1, 'CENTRO_CUSTO' 		,'RRH', 		'CENTRO_CUSTO',              		"@!"						,40)
	TRCell():New( oSection1, 'HORAS' 		    	,'RRH', 		'HORAS',              				"@!"						,15)
	TRCell():New( oSection1, 'COD_EVENTO'         	,'RRH', 		'COD_EVENTO',			           	"@!"		 				,03)
	TRCell():New( oSection1, 'EVENTO'         		,'RRH', 		'EVENTO',			           		"@!"						,40)
	TRCell():New( oSection1, 'ABONO' 				,'RRH', 	    'ABONO',     						"@!"		    		    ,40)

		
Return( oReport )
	
//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Ronaldo Pereira
@since 07 de Dezembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local cQuery    := ""
	Local cHoraNew  := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("RRH") > 0
		RRH->(dbCloseArea())
	Endif         
    
    cQuery := " SELECT CONVERT(CHAR, CAST(PH.PH_DATA AS SMALLDATETIME), 103) AS DATA, "
    cQuery += "        PH.PH_FILIAL AS FILIAL, "    
    cQuery += "        PH.PH_MAT AS MATRICULA, "
    cQuery += "        RA.RA_NOME AS NOME, "
    cQuery += "        RJ.RJ_DESC AS CARGO, "
    cQuery += "        CC.CTT_DESC01 AS CENTRO_CUSTO, "
    cQuery += "        PH.PH_QUANTC AS HORAS, "
    cQuery += " 	   PH.PH_PD AS COD_EVENTO, "
    cQuery += "        P9.P9_DESC AS EVENTO, "
    cQuery += "        ISNULL(P6.P6_DESC, 'DESCONTO EM FOLHA') AS ABONO "
    cQuery += " FROM "+RetSqlName("SPH")+" PH WITH (NOLOCK) "
    cQuery += " INNER JOIN "+RetSqlName("SRA")+" RA WITH (NOLOCK) ON (RA.RA_FILIAL = PH.PH_FILIAL "
    cQuery += "                                        AND RA.RA_MAT = PH.PH_MAT "
    cQuery += "                                        AND RA.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SRJ")+" RJ WITH (NOLOCK) ON (RJ.RJ_FUNCAO = RA.RA_CODFUNC "
    cQuery += "                                        AND RJ.RJ_FILIAL = '01' "
    cQuery += "                                        AND RJ.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("CTT")+" CC WITH (NOLOCK) ON (CC.CTT_CUSTO = RA.RA_CC "
    cQuery += "                                        AND CC.CTT_FILIAL = '01' "
    cQuery += "                                        AND CC.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SP9")+" P9 WITH (NOLOCK) ON (P9.P9_CODIGO = PH.PH_PD "
    cQuery += "                                        AND P9.D_E_L_E_T_ = '') "
    cQuery += " LEFT JOIN "+RetSqlName("SP6")+" P6 WITH (NOLOCK) ON (P6.P6_CODIGO = PH.PH_ABONO "
    cQuery += "                                       AND P6.D_E_L_E_T_ = '') "
    cQuery += " WHERE PH.D_E_L_E_T_ = '' "
    cQuery += "   AND RA.RA_SITFOLH <> 'D' "
    cQuery += "   AND PH.PH_PD IN ('006', "
    cQuery += "                    '008', "
    cQuery += " 				   '010', "
    cQuery += " 				   '012') "
    cQuery += "   AND PH_MAT LIKE '01%' "
    cQuery += "   AND PH_MAT NOT IN ('014778','014792','014793','014794','014814') "  
    cQuery += "   AND PH.PH_DATA BETWEEN '"+ DTOS(MV_PAR01) +"'  AND '"+ DTOS(MV_PAR02) + "' "  
    
    IF !EMPTY(MV_PAR03)	
    	cQuery += " AND PH_MAT = '"+MV_PAR03+"' "
    EndIF

    IF !EMPTY(MV_PAR04)	
    	cQuery += " AND CC.CTT_CUSTO = '"+MV_PAR04+"' "
    EndIF
    
    cQuery += " ORDER BY PH.PH_DATA, "
    cQuery += "          PH.PH_FILIAL, "    
    cQuery += "          CC.CTT_DESC01, "
    cQuery += "          RJ.RJ_DESC "    
       	
       	
	TCQUERY cQuery NEW ALIAS RRH
	
	While RRH->(!EOF())
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()
		
		cHoraNew := ConvtHora(RRH->HORAS)
		
		oSection1:Cell("DATA"):SetValue(RRH->DATA)
		oSection1:Cell("DATA"):SetAlign("LEFT")

		oSection1:Cell("FILIAL"):SetValue(RRH->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")

		oSection1:Cell("MATRICULA"):SetValue(RRH->MATRICULA)
		oSection1:Cell("MATRICULA"):SetAlign("LEFT")
		
		oSection1:Cell("NOME"):SetValue(RRH->NOME)
		oSection1:Cell("NOME"):SetAlign("LEFT")		
					
		oSection1:Cell("CARGO"):SetValue(RRH->CARGO)
		oSection1:Cell("CARGO"):SetAlign("LEFT")
		
		oSection1:Cell("CENTRO_CUSTO"):SetValue(RRH->CENTRO_CUSTO)
		oSection1:Cell("CENTRO_CUSTO"):SetAlign("LEFT")
		
		oSection1:Cell("HORAS"):SetValue(cHoraNew)
		oSection1:Cell("HORAS"):SetAlign("LEFT")
		
		oSection1:Cell("COD_EVENTO"):SetValue(RRH->COD_EVENTO)
		oSection1:Cell("COD_EVENTO"):SetAlign("LEFT")
		
		oSection1:Cell("EVENTO"):SetValue(RRH->EVENTO)
		oSection1:Cell("EVENTO"):SetAlign("LEFT")
		
		oSection1:Cell("ABONO"):SetValue(RRH->ABONO)
		oSection1:Cell("ABONO"):SetAlign("LEFT")
												
		oSection1:PrintLine()
		
		RRH->(DBSKIP()) 
	enddo
	RRH->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Ronaldo Pereira
@since 07 de Dezembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Data de ?"		        		,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Data ate ?"                     ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Matricula ?"		        	,""		,""		,"mv_ch3","C",08,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Setor ?"		        		,""		,""		,"mv_ch3","C",08,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	
return

Static Function ConvtHora(nValor)
Local cHora := ""
Local cMinutos := ""
Local cSepar := ":"
Default nValor := -1

//Se for valores negativos, retorna a hora atual
If nValor < 0
cHora := SubStr(Time(), 1, 5)
cHora := StrTran(cHora, ':', cSepar)

//Sen�o, transforma o valor num�rico
Else
cHora := Alltrim(Transform(nValor, "@E 99.99"))

//Se o tamanho da hora for menor que 5, adiciona zeros a esquerda
If Len(cHora) < 5
cHora := Replicate('0', 5-Len(cHora)) + cHora
EndIf

//Fazendo tratamento para minutos
cMinutos := SubStr(cHora, At(',', cHora)+1, 2)
//cMinutos := StrZero((Val(cMinutos)*60)/100, 2)

//Atualiza a hora com os novos minutos
cHora := SubStr(cHora, 1, At(',', cHora))+cMinutos

//Atualizando o separador
cHora := StrTran(cHora, ',', cSepar)
EndIf

ConOut("> ConvtHora: "+cHora)

Return cHora
	
