#INCLUDE 'PROTHEUS.CH'
	/*
	===============================================================================================
	===============================================================================================
	||   Arquivo:	LP_COMP01.prw
	===============================================================================================
	||   Funcao: 	LP_COMP01
	===============================================================================================
	||		      Contabilização de Entrada de Ativo usando analise especifica.
	||            Campo: CT5_DEBITO / CT2_DEBITO   
	||            LP:650
	===============================================================================================
	===============================================================================================
	||   Autor:	Rafael B. P. Parejjo
	||   Data:	07/03/2018
	===============================================================================================
	================================================================================================
	*/
User Function HopeLP01()
Local aArea	:= GetArea()
Local cRet	:= ""

 	If SD1->D1_TOTAL <= "1200" .AND. SD1->D1_RATEIO == "1" 

		 DbSelectArea("SDE")
		 DbSetOrder(1)
		 	
		 If DbSeek(XFILIAL("SDE") + SD1->D1_DOC + SD1->D1_SERIE + SD1->D1_FORNECE + SD1->D1_LOJA + SD1->D1_ITEM) .AND. LEFT(SDE->DE_CC,4) = "0011"
		 
				cRet := "101030203839"
		 Else
		    	cRet := "301070135001"
		
		 EndIf

     ElseIf SD1->D1_RATEIO <> "1"

		  DbSelectArea("SB1")
		  DbSetOrder(1)
			 
		  DbSeek(XFILIAL("SB1") + SD1->D1_COD)
			  
	             cRet := AllTrim(SB1->B1_CONTA)
	
	 EndIf
          RestArea(aArea)
	
Return(cRet)
