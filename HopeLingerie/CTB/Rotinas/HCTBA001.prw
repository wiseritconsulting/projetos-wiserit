#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#include "fileio.ch"
     
User Function HCTBA001
        Local aPergs    := {}
        Local lConv      := .F.
        
        Private cArq       := ""
		Private cTemp      := GetTempPath() //pega caminho do temp do client
		Private cSystem    := Upper(GetSrvProfString("STARTPATH",""))//Pega o caminho do sistema
        Private aResps     := {}    
        Private aArquivos  := {}    
 
        aAdd(aPergs,{6,"Arquivo: ",Space(150),"",'.T.','.T.',80,.F.,""})            
		  

        If ParamBox(aPergs,"Parametros", @aResps)

	      	aAdd(aArquivos, AllTrim(aResps[1]))
			cArq := AllTrim(aResps[1]) 
       		Processa({|| ProcMovs()})
		Endif           
Return
                         
Static Function ProcMovs()
        Local cError    	 := ""
        Local cWarning      := ""
        Local nFile         := 0
        Local aLinhas   	 := {}                          
        Local cFile     	 := AllTrim(aResps[1])
        Local nI            := 0
		Local cLinha  := ""
		Local nLintit    := 1 
		Local nLin 		 := 0
		Local nTotLin := 0
		Local aDados  := {}
		Local nHandle := 0

    	nFile := fOpen(cFile, FO_READ + FO_DENYWRITE)
   
        If nFile == -1
			If !Empty(cFile)
            	MsgAlert("O arquivo nao pode ser aberto!", "Atencao!")
            EndIf
            Return
        EndIf          

		nHandle := Ft_Fuse(cFile)
		Ft_FGoTop()                                                         
		nLinTot := FT_FLastRec()-1
		ProcRegua(nLinTot)

		While nLinTit > 0 .AND. !Ft_FEof()
			Ft_FSkip()
			nLinTit--
		EndDo

		Do While !Ft_FEof()
			IncProc("Carregando Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)))
			nLin++
			cLinha := Ft_FReadLn()
			If Empty(AllTrim(StrTran(cLinha,',','')))
				Ft_FSkip()
			    Loop
			EndIf
			cLinha := StrTran(cLinha,','," ")
			cLinha := StrTran(cLinha,';',",")
			cLinha := StrTran(cLinha,'"',"'")
			cLinha := '{"'+cLinha+'"}' 
			cLinha := StrTran(cLinha,',','","')
			aAdd(aDados, &cLinha)
   
			FT_FSkip()
		EndDo

		FT_FUse()             
       
       	_doc := aDados[1,2]
       	_data := stod(SubStr(aDados[1,7],1,4)+SubStr(aDados[1,7],6,2)+SubStr(aDados[1,7],9,2))
		nReg := 1
		Begin Transaction    
		    
			aItens := {}
			_xitem := "001"
			aCab := {{'DDATALANC' 	,_data		 			,NIL},;
           			 {'CLOTE' 		,'008800' 				,NIL},;
					 {'CSUBLOTE' 	,'001' 					,NIL},;
           			 {'CDOC' 		,STRZERO( seconds() ,6) ,NIL},;
           			 {'CPADRAO' 	,'' 					,NIL},;
           			 {'NTOTINF' 	,0 						,NIL},;
           			 {'NTOTINFLOT' 	,0 						,NIL}}
            ProcRegua(nLinTot)  
	  		For nI := 1 To Len(aDados)
		       	_data := stod(SubStr(aDados[nI,7],1,4)+SubStr(aDados[nI,7],6,2)+SubStr(aDados[nI,7],9,2))
			IncProc("Gravando Linha "+AllTrim(Str(nI))+" de "+AllTrim(Str(nLinTot)))	
				If _doc <> aDados[nI,2]

					lMsErroAuto := .F.
//					MSExecAuto( {|X,Y,Z| CTBA102(X,Y,Z)} ,aCab ,aItens, 3)

					If lMsErroAuto
//						mostraerro()
					EndIf

					_doc := aDados[ni,2]
					
					aItens := {}
					_xitem := "001"
					aCab := {{'DDATALANC' 	,_data		 			,NIL},;
                			 {'CLOTE' 		,'008800' 				,NIL},;
							 {'CSUBLOTE' 	,'001' 					,NIL},;
                 			 {'CDOC' 		,STRZERO( seconds() ,6) ,NIL},;
                 			 {'CPADRAO' 	,'' 					,NIL},;
                 			 {'NTOTINF' 	,0 						,NIL},;
                 			 {'NTOTINFLOT' 	,0 						,NIL}}
				Endif

				_ccd  := ""
				_ccc  := ""

				If alltrim(aDados[nI,8]) <> "NULL" .and. (alltrim(aDados[nI,9]) = "NULL" .or. alltrim(aDados[nI,9]) = "")
					_dc := "1"                   
					DbSelectArea("ZA0")
					DbSetOrder(2)
					DbSeek(xfilial("ZA0")+alltrim(aDados[nI,8]))
					If Found()
						_cdeb := ZA0->ZA0_CTAPRO
					Else
						_cdeb := '101020252901        '           
					Endif
					_ccre := ""
					If SubStr(_cdeb,1,1) $ "3|4"
						DbSelectArea("ZA6")
						DbSetOrder(2)
						DbSeek(xfilial("ZA6")+alltrim(aDados[nI,21]))
						_ccd  := ZA6->ZA6_PROTHE
						_ccc  := ""
					Endif
				ElseIf (alltrim(aDados[nI,8]) = "NULL" .or. alltrim(aDados[nI,8]) = "") .and. alltrim(aDados[nI,9]) <> "NULL"
					_dc := "2"
					DbSelectArea("ZA0")
					DbSetOrder(2)
					DbSeek(xfilial("ZA0")+alltrim(aDados[nI,9]))
					_cdeb := ""
					If Found()
						_ccre := ZA0->ZA0_CTAPRO
					Else
						_ccre := '101020252901        '           
					Endif
					If SubStr(_ccre,1,1) $ "3|4"
						DbSelectArea("ZA6")
						DbSetOrder(2)
						DbSeek(xfilial("ZA6")+alltrim(aDados[nI,21]))
						_ccd  := ""
						_ccc  := ZA6->ZA6_PROTHE
					Endif
				Else
					_dc := "3"
					DbSelectArea("ZA0")
					DbSetOrder(2)
					DbSeek(xfilial("ZA0")+alltrim(aDados[nI,8]))
					If Found()
						_cdeb := ZA0->ZA0_CTAPRO
					Else
						_cdeb := '101020252901        '           
					Endif
					DbSelectArea("ZA0")
					DbSetOrder(2)
					DbSeek(xfilial("ZA0")+alltrim(aDados[nI,9]))
					If Found()
						_ccre := ZA0->ZA0_CTAPRO
					Else
						_ccre := '101020252901        '           
					Endif
					If SubStr(_cdeb,1,1) $ "3|4"
						DbSelectArea("ZA6")
						DbSetOrder(2)
						DbSeek(xfilial("ZA6")+alltrim(aDados[nI,21]))
						_ccd  := ZA6->ZA6_PROTHE
					Endif
					if len(aDados[nI]) <= 13
					   msgstop("Aten��o erro na linha  "+strzero(nI,6) )
					Endif   
					If SubStr(_ccre,1,1) $ "3|4"
						DbSelectArea("ZA6")
						DbSetOrder(2)
						DbSeek(xfilial("ZA6")+alltrim(aDados[nI,21]))
						_ccc  := ZA6->ZA6_PROTHE
					Endif
				Endif

				RecLock("CT2",.T.)
					Replace CT2_FILIAL		with xfilial("CT2")			
					Replace CT2_DATA		with _data 
					Replace CT2_LOTE		with "008800"
					Replace CT2_SBLOTE		with "001"
					Replace CT2_DOC			with STRZERO(Val(aDados[nI,2]),6)
				    Replace CT2_LINHA	  	with _xitem
				    Replace CT2_MOEDLC  	with '01'   			
				    Replace CT2_DC   		with _dc   				
				    Replace CT2_DEBITO  	with _cdeb			 	
				    Replace CT2_CREDIT  	with _ccre			 	
				    Replace CT2_CCD	  		with _ccd			 	
				    Replace CT2_CCC  		with _ccc			 	
				    Replace CT2_VALOR  		with val(aDados[nI,11])
				    Replace CT2_HIST   		with aDados[nI,13]
				    Replace CT2_EMPORI		with "01"
				    Replace CT2_FILORI		with "0101"
				    Replace CT2_TPSALD		with "1"
				    Replace CT2_MANUAL		with "1"
				    Replace CT2_ROTINA		with "CTBA102"
				    Replace CT2_AGLUT		with "2"
				    Replace CT2_SEQHIS		with "001"
				    Replace CT2_SEQLAN		with _xitem
				MsUnLock()
				_xitem := soma1(_xitem)
/*
				aAdd(aItens,{  {'CT2_FILIAL'  	,xfilial("CT2")				,NIL},;
				               {'CT2_LINHA'  	,strzero(len(aItens)+1,3)   ,NIL},;
				               {'CT2_MOEDLC'  	,'01'   					,NIL},;
				               {'CT2_DC'   		,_dc   						,NIL},;
				               {'CT2_DEBITO'  	,_cdeb			 			,NIL},;
				               {'CT2_CREDIT'  	,_ccre			 			,NIL},;
				               {'CT2_CCD'	  	,_ccd			 			,NIL},;
				               {'CT2_CCC'  		,_ccc			 			,NIL},;
				               {'CT2_VALOR'  	,val(aDados[nI,11])			,NIL},;
				               {'CT2_HIST'   	,aDados[nI,13]				,NIL}})
*/
			Next nI          

			lMsErroAuto := .F.
//			MSExecAuto( {|X,Y,Z| CTBA102(X,Y,Z)} ,aCab ,aItens, 3)

			If lMsErroAuto
//				mostraerro()
			EndIf

        End Transaction    
Return

//				               {'CT2_ORIGEM' 	,'MSEXECAUT'				,NIL},;
//				               {'CT2_HP'   		,''   						,NIL},;
