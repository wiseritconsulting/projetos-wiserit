//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de Carteira por SKU.
Rotina para gerar todos os pedidos que possui um produto espec�fico;

@author Ronaldo Pereira
@since 10 de Abril de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL050()

	Local aRet := {}
	Local aPerg := {}
	
	
	aAdd(aPerg,{1,"Data Emiss�o de"		,dDataBase,PesqPict("SC5", "C5_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.}) // Tipo data
	aAdd(aPerg,{1,"Data Emiss�o At�" 	,dDataBase,PesqPict("SC5", "C5_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.}) // Tipo data
	aAdd(aPerg,{1,"Produto de: " 		,Space(15),""   ,""		,""	    ,""		,0	,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Produto at�: "		,Space(15),""	,""		,""		,""		,0	,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Pedido: "			,Space(06),""	,""		,""		,""		,0	,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Subcole��o: "		,Space(05),""	,""		,"ZAH"	,""		,0	,.F.}) // Tipo caractere

	
	If ParamBox(aPerg,"Par�metros...",@aRet) 

	 	Processa({|| Geradados(aRet)}, "Exportando dados")
		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Carteira '+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Dados"
    Local cTable1    	:= "Carteira por SKU" 

        
    //Dados tabela 1          
    cQry := "SELECT C6_FILIAL AS FILIAL, "
    cQry += "       CONVERT(CHAR, CAST(C5_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "
    cQry += "       C5_ORIGEM AS ORIGEM, "
    cQry += "       CASE "
    cQry += "           WHEN C5.C5_XBLQ = 'L' THEN 'LIBERADO' "
    cQry += "           ELSE 'BLOQUEADO' "
    cQry += "       END AS BLOQUEIO, "
    cQry += "       C6_NUM AS PEDIDO, "
    cQry += "       A1_COD AS COD_CLIENTE, "
    cQry += "       A1_NOME AS CLIENTE, "
    cQry += "       A1_EST AS UF, "
    cQry += "       Z1_CODIGO + ' - ' + Z1_DESC AS TIPO_PEDIDO, "
    cQry += "       Z2_CODIGO + ' - ' + Z2_DESC AS POLITICA, "
    cQry += "       ZA.ZAH_CODIGO + ' - ' + ISNULL(ZA.ZAH_DESCRI, '') AS SUBCOLECAO, "
    cQry += "       C6.C6_LOCAL AS ARMAZEM, "
    cQry += "       C6.C6_TES AS TES, "
    cQry += "       CASE "
    cQry += "           WHEN F4.F4_ESTOQUE = 'S' THEN 'SIM' "
    cQry += "           ELSE 'N�O' "
    cQry += "       END AS BAIXA_ESTOQUE, "    
    cQry += "       C6_PRODUTO AS PRODUTO, "
    cQry += "       C6_QTDVEN AS QTDE_VENDA, "
    cQry += "       C6_QTDENT AS QTDE_ENTREGUE, "
    cQry += "       ISNULL( "
    cQry += "                (SELECT SUM(R.C6_QTDVEN - R.C6_QTDENT) "
    cQry += "                 FROM "+RetSqlName("SC6")+" AS R WITH (NOLOCK) "
    cQry += "                 WHERE R.D_E_L_E_T_ = '' "
    cQry += "                   AND R.C6_NUM = C5.C5_NUM "
    cQry += "                   AND R.C6_PRODUTO = C6.C6_PRODUTO "
    cQry += "                   AND R.C6_BLQ = 'R'),0) AS QTDE_PERDA, "
    cQry += "       ((C6_QTDVEN - C6_QTDENT) - ISNULL( "
    cQry += "                                           (SELECT SUM(R.C6_QTDVEN - R.C6_QTDENT) "
    cQry += "                                            FROM "+RetSqlName("SC6")+" AS R WITH (NOLOCK) "
    cQry += "                                            WHERE R.D_E_L_E_T_ = '' "
    cQry += "                                              AND R.C6_NUM = C5.C5_NUM "
    cQry += "                                              AND R.C6_PRODUTO = C6.C6_PRODUTO "
    cQry += "                                              AND R.C6_BLQ = 'R'),0)) AS SALDO, "
    cQry += "       C6.C6_NOTA AS NOTA_FISCAL, "
    cQry += "       CASE "
    cQry += "           WHEN C6.C6_DATFAT <> '' THEN CONVERT(CHAR, CAST(C6.C6_DATFAT AS SMALLDATETIME), 103) "
    cQry += "           ELSE '' "
    cQry += "       END DATA_FAT "
    cQry += "FROM "+RetSqlName("SC6")+" C6 WITH (NOLOCK) "
    cQry += "INNER JOIN "+RetSqlName("SC5")+" C5 WITH (NOLOCK) ON (C5_FILIAL = C6_FILIAL "
    cQry += "                                       AND C5_NUM = C6_NUM "
    cQry += "                                       AND C5.D_E_L_E_T_ = '') "
    cQry += "INNER JOIN "+RetSqlName("SA1")+" A1 WITH (NOLOCK) ON (A1.A1_COD = C5.C5_CLIENTE "
    cQry += "                                       AND A1.D_E_L_E_T_ = '') "
    cQry += "INNER JOIN "+RetSqlName("SZ1")+" Z1 WITH (NOLOCK) ON (Z1_CODIGO = C5_TPPED "
    cQry += "                                       AND Z1.D_E_L_E_T_ = '') "
    cQry += "INNER JOIN "+RetSqlName("SF4")+" F4 WITH (NOLOCK) ON (F4.F4_CODIGO = C6.C6_TES "
    cQry += "                                       AND F4.D_E_L_E_T_ = '') "
    cQry += "LEFT JOIN "+RetSqlName("SZ2")+" Z2 WITH (NOLOCK) ON (Z2_CODIGO = C5_POLCOM "
    cQry += "                                      AND Z2.D_E_L_E_T_ = '') "
    cQry += "LEFT JOIN "+RetSqlName("SZ7")+" Z7 WITH (NOLOCK) ON (Z7_CODIGO = C5_PRAZO "
    cQry += "                                      AND Z2.D_E_L_E_T_ = '') "
    cQry += "LEFT JOIN "+RetSqlName("SE4")+" E4 WITH (NOLOCK) ON (E4.E4_CODIGO = C5_CONDPAG "
    cQry += "                                      AND E4.D_E_L_E_T_ = '') "
    cQry += "LEFT JOIN "+RetSqlName("SB1")+" B1 WITH (NOLOCK) ON (B1_COD = C6.C6_PRODUTO "
    cQry += "                                      AND B1.D_E_L_E_T_ = '') "
    cQry += "LEFT JOIN "+RetSqlName("ZAH")+" ZA WITH (NOLOCK) ON (ZA.ZAH_CODIGO = B1.B1_YSUBCOL "
    cQry += "                                      AND ZA.D_E_L_E_T_ = '') "
    cQry += "WHERE C6.D_E_L_E_T_ = '' AND C5.C5_FILIAL = '0101' "

	If !Empty(aRet[1])
		cQry += " AND C5.C5_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"'  " 
	Endif
  
    If !Empty(aRet[3])
    	cQry += " AND C6.C6_PRODUTO BETWEEN '"+Alltrim(aRet[3])+"' AND '"+Alltrim(aRet[4])+"' "
    EndIf
    
    If !Empty(aRet[5])
    	cQry += " AND C6.C6_NUM = '"+Alltrim(aRet[5])+"' "
    EndIf    
    
    If !Empty(aRet[6])
    	cQry += " AND ZA.ZAH_CODIGO = '"+Alltrim(aRet[6])+"' "
    EndIf    
	
  
    cQry += "GROUP BY C6.C6_FILIAL, "
    cQry += "         C5.C5_EMISSAO, "
    cQry += "         C5.C5_ORIGEM, "
    cQry += "         C5.C5_XBLQ, "
    cQry += "         C6.C6_NUM, "
    cQry += "         C6.C6_LOCAL, "
    cQry += "         C6.C6_PRODUTO, "
    cQry += "         C6.C6_QTDVEN, "
    cQry += "         C6.C6_QTDENT, "
    cQry += "         A1.A1_COD, "
    cQry += "         A1.A1_NOME, "
    cQry += "         A1.A1_EST, "
    cQry += "         Z1.Z1_CODIGO, "
    cQry += "         Z1.Z1_DESC, "
    cQry += "         ZA.ZAH_CODIGO, "
    cQry += "         ZAH_DESCRI, "
    cQry += "         Z2_CODIGO, "
    cQry += "         Z2_DESC, "
    cQry += "         C6.C6_NOTA, "
    cQry += "         C6.C6_DATFAT, "
    cQry += "         C5_NUM, "
    cQry += "         C6.C6_TES, "
    cQry += "         F4.F4_ESTOQUE "
    cQry += "ORDER BY C5.C5_EMISSAO  "

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"EMISS�O",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ORIGEM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"BLOQUEIO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PEDIDO",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_CLIENTE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CLIENTE",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"UF",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO_PEDIDO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"POL�TICA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SUBCOLE��O",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ARMAZ�M",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TES",1)                
        oFWMsExcel:AddColumn(cPlan1,cTable1,"BAIXA_ESTOQUE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PRODUTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE_VENDA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE_ENTREGUE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE_PERDA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SALDO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"NOTA_FISCAL",1)         
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DATA_FAT",1)
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                               QRY->FILIAL,;
                                               QRY->EMISSAO	,;
                                               QRY->ORIGEM,;
                                               QRY->BLOQUEIO,;
                                               QRY->PEDIDO,;
                                               QRY->COD_CLIENTE,;
                                               QRY->CLIENTE,;                                               
                                               QRY->UF,;
                                               QRY->TIPO_PEDIDO,;
                                               QRY->POLITICA,;
                                               QRY->SUBCOLECAO,;
                                               QRY->ARMAZEM,;
                                               QRY->TES,;
                                               QRY->BAIXA_ESTOQUE,;
                                               QRY->PRODUTO,;
                                               QRY->QTDE_VENDA,;
                                               QRY->QTDE_ENTREGUE,;
                                               QRY->QTDE_PERDA,;
                                               QRY->SALDO,;
                                               QRY->NOTA_FISCAL,;
                                               QRY->DATA_FAT;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return