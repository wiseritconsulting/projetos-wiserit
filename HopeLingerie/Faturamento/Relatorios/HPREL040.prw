//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//Constantes
#Define STR_PULA    Chr(13)+Chr(10)

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de Exporta��o
Rotina para gerar as informa��es do INVOICE;

@author Ronaldo Pereira
@since 18 de Dezembro de 2018
@version V.01
/*/
//_____________________________________________________________________________

user function HPREL040()
	Private cPerg	:= 'HPREL040'
	
	AjustaSX1(cPerg)
	If !Pergunte(cPerg,.T.)
    	Return
	EndIf
	
	
	lGera := .T.
    Processa( {|| lGera := GeraDados() }, "Aguarde...", "Processando...",.F.)
    
Static Function Geradados()
    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local cQuery    	:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Packing List e Invoice.xml'   
    Local cPlan1		:= "Dados"
    Local cTable1    	:= "COMMERCIAL INVOICE Nr.: " + left(MV_PAR01,6)
    Local cPlan2		:= "Produtos"
    Local cTable2	    := "PACKING LIST Nr.: " + left(MV_PAR01,6)
        
    //Dados tabela 1    
    cQry := " SELECT CONVERT(CHAR, CAST(C5.C5_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "									+ STR_PULA
    cQry += "        C5.C5_NUM AS PEDIDO, "																					+ STR_PULA
    cQry += "        A1.A1_NOME AS CLIENTE, "																				+ STR_PULA
    cQry += "        A1.A1_END AS ENDERECO, "																				+ STR_PULA
    cQry += "        A1.A1_BAIRRO AS BAIRRO, "																				+ STR_PULA
    cQry += "        A1.A1_COMPLEM AS COMPLEM, "																			+ STR_PULA
    cQry += "        YA.YA_DESCR AS PAIS, "																					+ STR_PULA
    cQry += "        'HOPE DO NORDESTE LTDA' AS REMETENTE, "																+ STR_PULA
    cQry += "        'RUA ARGEU BRAGA HERBSTER, 610' AS END_REMET, "														+ STR_PULA
    cQry += "        'MARANGUAPE - CE - CEP 61940-000' AS CEP_REMET, "														+ STR_PULA
    cQry += "        'CNPJ. 03.007.414/0001-30' AS CNPJ_REMET "																+ STR_PULA
    cQry += " FROM "+RetSqlName("SC5")+" AS C5 WITH (NOLOCK) "																+ STR_PULA
    cQry += " INNER JOIN "+RetSqlName("SA1")+" AS A1 WITH (NOLOCK) ON (A1.A1_COD = C5.C5_CLIENTE "							+ STR_PULA
    cQry += "                                           AND A1.D_E_L_E_T_ = '') "											+ STR_PULA
    cQry += " INNER JOIN "+RetSqlName("SYA")+" AS YA WITH (NOLOCK) ON (YA.YA_CODGI = A1.A1_PAIS "							+ STR_PULA
    cQry += "                                           AND YA.D_E_L_E_T_ = '') "											+ STR_PULA
    cQry += " WHERE C5.D_E_L_E_T_ = '' "																					+ STR_PULA
  
    If !Empty(MV_PAR01)
    	cQry += " AND C5.C5_NUM = '"+AllTrim(LEFT(MV_PAR01,6))+"' "   														+ STR_PULA
    EndIf
    
    TCQuery cQry New Alias "QRYPED"
           
        
    //Dados tabela 2          
    cQuery := " SELECT R.PEDIDO, "   						                                                    			+ STR_PULA    																						    																						
    cQuery += " 	   R.PREFAT, "    																						+ STR_PULA     																						
    cQuery += " 	   R.VOLUME, "    																						+ STR_PULA     																						
    cQuery += " 	   R.REFERENCIA, "    																					+ STR_PULA     																					
    cQuery += " 	   R.COR, "    																							+ STR_PULA     																							
    cQuery += " 	   R.TAMANHO, "    																						+ STR_PULA    																						
    cQuery += " 	   R.QTDE, "    																						+ STR_PULA 	    																							
    cQuery += " 	   R.PRECO, "    																						+ STR_PULA     																						
    cQuery += " 	   R.TOTAL, "    																						+ STR_PULA     																						
    cQuery += " 	   R.GRUPO, "    																						+ STR_PULA     																						
    cQuery += " 	   R.NCM, "                                                                                             + STR_PULA 
    cQuery += " 	   (R.QTDE * R.PESO_BRT) AS PESO_BRT, "																	+ STR_PULA 
    cQuery += " 	   (R.QTDE * R.PESO_LQD) AS PESO_LQD "																	+ STR_PULA 	
    cQuery += " FROM "																										+ STR_PULA 
    cQuery += " (SELECT ZP.ZP_PEDIDO AS PEDIDO, "																			+ STR_PULA 
    cQuery += "        ZP.ZP_NUMPF AS PREFAT, "																				+ STR_PULA 
    cQuery += "        ZP.ZP_VOLUME AS VOLUME, "																			+ STR_PULA 
    cQuery += "        LEFT(ZP.ZP_PRODUTO, 8) AS REFERENCIA, "																+ STR_PULA 
    cQuery += "        SUBSTRING(ZP.ZP_PRODUTO, 9, 3) AS COR, "																+ STR_PULA 
    cQuery += "        CASE "																								+ STR_PULA 
    cQuery += "            WHEN RIGHT(ZP.ZP_PRODUTO, 4) LIKE '000%' THEN REPLACE(RIGHT(ZP.ZP_PRODUTO, 4), '000', '') "		+ STR_PULA 
    cQuery += "            WHEN RIGHT(ZP.ZP_PRODUTO, 4) LIKE '00%' THEN REPLACE(RIGHT(ZP.ZP_PRODUTO, 4), '00', '') "		+ STR_PULA 
    cQuery += "            WHEN RIGHT(ZP.ZP_PRODUTO, 4) LIKE '0%' THEN SUBSTRING(RIGHT(ZP.ZP_PRODUTO, 4), 2, 4) "			+ STR_PULA 	
    cQuery += "            ELSE REPLACE(RIGHT(ZP.ZP_PRODUTO, 4), '00', '') "												+ STR_PULA 
    cQuery += "        END TAMANHO, "																						+ STR_PULA 
    cQuery += "        SUM(ZP.ZP_QUANT) AS QTDE, "																			+ STR_PULA 
    cQuery += "        C6.C6_PRCVEN AS PRECO, "																				+ STR_PULA 
    cQuery += "        (SUM(ZP.ZP_QUANT) * C6.C6_PRCVEN) AS TOTAL, "														+ STR_PULA 
    cQuery += "        BM.BM_DESC AS GRUPO, "																				+ STR_PULA 
    cQuery += "        B1.B1_POSIPI AS NCM, "																				+ STR_PULA 	

    cQuery += " 	   (SELECT  (SZP.ZP_PESO / SUM(SZP.ZP_QUANT)) AS PESO_BRT FROM SZP010 AS SZP WITH (NOLOCK) "			+ STR_PULA  
    cQuery += " 		WHERE SZP.D_E_L_E_T_ = '' AND SZP.ZP_NUMPF = ZP.ZP_NUMPF AND SZP.ZP_VOLUME = ZP.ZP_VOLUME "			+ STR_PULA 
    cQuery += " 		GROUP BY SZP.ZP_VOLUME, SZP.ZP_PESO) AS PESO_BRT, "													+ STR_PULA 

    cQuery += " 		(SELECT TOP 1 ((ZP_PESO - ZQ.ZQ_TARA)/ SUM(ZP_QUANT)) AS PESO_LQD FROM SZP010 AS SZP WITH (NOLOCK) "+ STR_PULA  
    cQuery += " 		INNER JOIN SZQ010 AS ZQ WITH (NOLOCK) ON (ZQ.ZQ_CODIGO = SZP.ZP_TPVOL AND ZQ.D_E_L_E_T_ = '') "		+ STR_PULA 
    cQuery += " 		WHERE SZP.D_E_L_E_T_ = '' AND SZP.ZP_NUMPF = ZP.ZP_NUMPF AND SZP.ZP_VOLUME = ZP.ZP_VOLUME "			+ STR_PULA 
    cQuery += " 		GROUP BY SZP.ZP_VOLUME, SZP.ZP_PESO,ZQ.ZQ_TARA) AS PESO_LQD "										+ STR_PULA 

    cQuery += " FROM SZP010 AS ZP WITH (NOLOCK) "																			+ STR_PULA
    cQuery += " INNER JOIN SC6010 AS C6 WITH (NOLOCK) ON (C6.C6_FILIAL = ZP.ZP_FILIAL "										+ STR_PULA 
    cQuery += "                                           AND C6.C6_NUM = ZP.ZP_NUMPED "									+ STR_PULA 
    cQuery += "                                           AND C6.C6_PRODUTO = ZP.ZP_PRODUTO "								+ STR_PULA 
    cQuery += "                                           AND C6.D_E_L_E_T_ = '') "											+ STR_PULA 
    cQuery += " INNER JOIN SB1010 AS B1 WITH (NOLOCK) ON (B1.B1_COD = ZP.ZP_PRODUTO "										+ STR_PULA 
    cQuery += "                                           AND B1.D_E_L_E_T_ = '') "											+ STR_PULA 
    cQuery += " INNER JOIN SBM010 AS BM WITH (NOLOCK) ON (BM.BM_GRUPO = B1.B1_GRUPO "										+ STR_PULA 
    cQuery += "                                           AND BM.D_E_L_E_T_ = '') "											+ STR_PULA 
    cQuery += " WHERE ZP.D_E_L_E_T_ = '' "																					+ STR_PULA 
	 
    If !Empty(MV_PAR01)
    	cQuery += " AND ZP.ZP_NUMPF BETWEEN '"+AllTrim(MV_PAR01)+"' AND '"+AllTrim(MV_PAR02)+"' "   						+ STR_PULA   
    EndIf
	 
    cQuery += " GROUP BY ZP.ZP_PEDIDO, "																					+ STR_PULA 
    cQuery += "          ZP.ZP_NUMPF, "																						+ STR_PULA 
    cQuery += "          ZP.ZP_VOLUME, "																					+ STR_PULA 
    cQuery += "          ZP.ZP_PRODUTO, "																					+ STR_PULA 
    cQuery += "          C6.C6_PRCVEN, "																					+ STR_PULA 
    cQuery += "          ZP.ZP_QUANT, "																						+ STR_PULA 
    cQuery += "          BM.BM_DESC, "																						+ STR_PULA 
    cQuery += "          B1.B1_POSIPI, "																					+ STR_PULA 
    cQuery += "          ZP.ZP_PESO "																						+ STR_PULA 
    cQuery += " ) R ORDER BY R.VOLUME "																						+ STR_PULA 
        
    TCQuery cQuery New Alias "QRYPRO"
    
    
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)                 //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")                //Fonte utilizada
    //oFWMsExcel:SetBgGeneralColor("#000000")    //Cor de Fundo Geral
    oFWMsExcel:SetTitleBold(.T.)               //T�tulo Negrito
    //oFWMsExcel:SetTitleFrColor("#94eaff")      //Cor da Fonte do t�tulo - Azul Claro
    //oFWMsExcel:SetLineFrColor("#d4d4d4")       //Cor da Fonte da primeira linha - Cinza Claro
    //oFWMsExcel:Set2LineFrColor("#ffffff")      //Cor da Fonte da segunda linha - Branco
     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Dados",1,1) //1 = Modo Texto
        oFWMsExcel:AddColumn(cPlan1,cTable1,"",1,1)
        //Criando as Linhas
        oFWMsExcel:AddRow(cPlan1,cTable1,{"Pedido: ", QRYPED->PEDIDO})
        oFWMsExcel:AddRow(cPlan1,cTable1,{"",""})
        oFWMsExcel:AddRow(cPlan1,cTable1,{"Remetente: ", QRYPED->REMETENTE})
        oFWMsExcel:AddRow(cPlan1,cTable1,{"", QRYPED->END_REMET})
        oFWMsExcel:AddRow(cPlan1,cTable1,{"", QRYPED->CEP_REMET})
        oFWMsExcel:AddRow(cPlan1,cTable1,{"", QRYPED->CNPJ_REMET})    
        oFWMsExcel:AddRow(cPlan1,cTable1,{"",""})
        oFWMsExcel:AddRow(cPlan1,cTable1,{"Destinatario: ", QRYPED->CLIENTE})
        oFWMsExcel:AddRow(cPlan1,cTable1,{"", QRYPED->ENDERECO})
        oFWMsExcel:AddRow(cPlan1,cTable1,{"", QRYPED->BAIRRO})
        oFWMsExcel:AddRow(cPlan1,cTable1,{"", QRYPED->PAIS}) 
        oFWMsExcel:AddRow(cPlan1,cTable1,{"", QRYPED->COMPLEM})
       
             
    //Aba 02 - Produtos
    oFWMsExcel:AddworkSheet(cPlan2)
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan2,cTable2)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Pedido",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Prefaturamento",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Volume",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Referencia",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Cor",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Tamanho",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Qtde",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Preco",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Total",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Grupo",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"NCM",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Peso Bruto",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"Peso Liquido",1)                        
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRYPRO->(EoF()))
            oFWMsExcel:AddRow(cPlan2,cTable2,{;
                                               QRYPRO->PEDIDO,;
                                               QRYPRO->PREFAT,;
                                               QRYPRO->VOLUME,;
                                               QRYPRO->REFERENCIA,;
                                               QRYPRO->COR,;
                                               QRYPRO->TAMANHO,;
                                               QRYPRO->QTDE,;
                                               QRYPRO->PRECO,;
                                               QRYPRO->TOTAL,;
                                               QRYPRO->GRUPO,;
                                               QRYPRO->NCM,;
                                               QRYPRO->PESO_BRT,;
                                               QRYPRO->PESO_LQD;
            })
            
            //Pulando Registro
            QRYPRO->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRYPED->(DbCloseArea()) 
    QRYPRO->(DbCloseArea())
    RestArea(aArea)
    
    return
    
return

Static Function AjustaSX1(cPerg)
	PutSx1(cPerg, "01","Prefaturamento de? "		,""		,""		,"mv_ch1","C",09,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Prefaturamento at�? "		,""		,""		,"mv_ch2","C",09,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
return
