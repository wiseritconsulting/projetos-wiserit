//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de Pedido
Rotina para gerar as informa��es de volumes dos pedidos;

@author Ronaldo Pereira
@since 25 de Janeiro de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL044()

	Local aRet := {}
	Local aPerg := {}

	aAdd(aPerg,{1,"Prefaturamento de: " 		,Space(15),""   ,""		,""	    ,""		,0	,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Prefaturamento at�: "		,Space(15),""	,""		,""		,""		,0	,.F.}) // Tipo caractere

	If ParamBox(aPerg,"Par�metros...",@aRet) 

	 	Processa({|| Geradados(aRet)}, "Exportando dados")
		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Relat�rio '+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Volumes"
    Local cTable1    	:= "Pedido: " + LEFT(MV_PAR01,6)

        
    //Dados tabela 1          
    cQry := "SELECT ZP_NUMPF AS NUMPF, "
    cQry += "       B1_CODBAR AS CODBAR, "
    cQry += "       ZP_PRODUTO AS PRODUTO, "
    cQry += "       ISNULL(BV_DESCRI, '-') AS COR, "
    cQry += "       ISNULL(BV_CHAVE, '-') AS COD_COR, "
    cQry += "       CASE "
    cQry += "           WHEN RIGHT(ZP_PRODUTO, 4) LIKE '000%' THEN REPLACE(RIGHT(ZP_PRODUTO, 4), '000', '') "
    cQry += "           WHEN RIGHT(ZP_PRODUTO, 4) LIKE '00%' THEN REPLACE(RIGHT(ZP_PRODUTO, 4), '00', '') "
    cQry += "           WHEN RIGHT(ZP_PRODUTO, 4) LIKE '0%' THEN SUBSTRING(RIGHT(ZP_PRODUTO, 4), 2, 4) "
    cQry += "           ELSE REPLACE(RIGHT(ZP_PRODUTO, 4), '00', '') "
    cQry += "       END TAMANHO, "
    cQry += "       SUM(ZP_QUANT) AS QTDE, "
    cQry += "       ZP_VOLUME AS VOLUME "
    cQry += "FROM "+RetSqlName("SZP")+" (NOLOCK) ZP "
    cQry += "INNER JOIN  "+RetSqlName("SB1")+" (NOLOCK) B1 ON (B1.B1_COD = ZP.ZP_PRODUTO "
    cQry += "                                  AND B1.D_E_L_E_T_ = '') "
    cQry += "LEFT JOIN "+RetSqlName("SBV")+" (NOLOCK) ON SUBSTRING(B1_COD, 9, 3) = BV_CHAVE "
    cQry += "AND BV_TABELA = 'COR' "
    cQry += "AND SBV010.D_E_L_E_T_ = '' "
    cQry += "WHERE ZP.D_E_L_E_T_ = '' "
    
    If !Empty(aRet[1])
    	cQry += "  AND ZP_NUMPF BETWEEN '"+ Alltrim(aRet[1]) +"'  AND '"+ Alltrim(aRet[2]) +"' "
    EndIf
    
    cQry += " GROUP BY ZP_NUMPF,B1.B1_CODBAR,ZP_PRODUTO,BV_DESCRI,BV_CHAVE,ZP_VOLUME " 
    cQry += "ORDER BY ZP_VOLUME, "
    cQry += "         ZP_PRODUTO "
         
                     
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Prefaturamento",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Codigo_Barras",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Produto",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Cor",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Cod_Cor",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Tamanho",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Qtde",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Volume",1) 
                               
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                               QRY->NUMPF,;
                                               QRY->CODBAR,;
                                               QRY->PRODUTO,;
                                               QRY->COR,;
                                               QRY->COD_COR,;
                                               QRY->TAMANHO,;
                                               QRY->QTDE,;
                                               QRY->VOLUME;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
                 
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return