#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL011 
Status Pr�-Faturamento
@author Weskley Silva
@since 07/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL011()

Private oReport
Private cPergCont	:= 'HPREL011' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'STA', 'STATUS PR�-FATURAMENTO ', , {|oReport| ReportPrint( oReport ), 'STATUS PR�-FATURAMENTO' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'STATUS PR�-FATURAMENTO', { 'STA', 'SZR','SZJ','SA1','SA2','SC5','SZ1'})
	
	TRCell():New( oSection1, 'EMISSAO'				,'STA', 		'EMISSAO',       					"@!"		                ,15)
	TRCell():New( oSection1, 'ENTREGA'				,'STA', 		'ENTREGA',       					"@!"		                ,15)
	TRCell():New( oSection1, 'LIBERACAO'			,'STA', 		'LIBERACAO',       					"@!"		                ,15)	
	TRCell():New( oSection1, 'TIPO_PED'	        	,'STA', 		'TIPO_PED',   						"@!"				        ,40)		
	TRCell():New( oSection1, 'PEDIDO'	    	    ,'STA', 		'PEDIDO',							"@!"                        ,10)
	TRCell():New( oSection1, 'COD_CLIENTE' 		    ,'STA', 		'COD_CLIENTE',              		"@!"						,08)
	TRCell():New( oSection1, 'CLIENTE' 		        ,'STA', 		'CLIENTE',              			"@!"						,35)
	TRCell():New( oSection1, 'UF' 		        	,'STA', 		'UF',              					"@!"						,02)	
	TRCell():New( oSection1, 'DAP'         			,'STA', 		'DAP',			           			"@!"						,10)
	TRCell():New( oSection1, 'DATA_PRE'				,'STA', 		'DATA_PRE',       					"@!"		                ,15)
	TRCell():New( oSection1, 'NUM_PRE_FATURAMENTO'	,'STA', 		'NUM_PRE_FATURAMENTO',	   			"@!"						,15)
	TRCell():New( oSection1, 'QTDE_PRE'      		,'STA', 		'QTDE_PRE',      					"@E 999,99"				    ,15)
	TRCell():New( oSection1, 'QTDE_CONF' 			,'STA', 	    'QTDE_CONF',     					"@E 999,99"		    	    ,15)
	TRCell():New( oSection1, 'A_CONFERIR' 			,'STA', 	    'A_CONFERIR',     					"@E 999,99"		    	    ,15)
	TRCell():New( oSection1, 'DESCONTO'				,'STA', 		'DESCONTO',   						"@E 999,999.99"				,15)		
	TRCell():New( oSection1, 'FRETE'				,'STA', 		'FRETE',   							"@E 999,999.99"				,15)
	TRCell():New( oSection1, 'COND_PGTO'			,'STA', 		'COND_PGTO',   						"@!"						,06)
	TRCell():New( oSection1, 'VALOR_TOTAL_PRE'		,'STA', 		'VALOR_TOTAL_PRE',   				"@E 999,999.99"				,15)
	TRCell():New( oSection1, 'VALOR_A_CONFERIR'		,'STA', 		'VALOR_A_CONFERIR',   				"@E 999,999.99"				,15)	
	TRCell():New( oSection1, 'STATUS'				,'STA', 		'STATUS',				   			"@!"						,15)
	TRCell():New( oSection1, 'CONFERENTE'			,'STA', 		'CONFERENTE',				   		"@!"						,30)
	TRCell():New( oSection1, 'DATA_CONF'			,'STA', 		'DATA_CONF',				   		"@!"						,15)
	TRCell():New( oSection1, 'NOTA_FISCAL'			,'STA', 		'NOTA_FISCAL',			   			"@!"						,15)
	TRCell():New( oSection1, 'DATA_FAT'				,'STA', 		'DATA_FAT',		   					"@!"						,15)	
	
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("STA") > 0
		STA->(dbCloseArea())
	Endif      	
	
	cQuery := " SELECT CONVERT(CHAR,CAST(R.C5_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "
	cQuery += "        CONVERT(CHAR,CAST(R.ENTREGA AS SMALLDATETIME), 103) AS ENTREGA, "
	cQuery += "        CASE WHEN R.LIBERACAO <> '' THEN CONVERT(CHAR,CAST(R.LIBERACAO AS SMALLDATETIME), 103) ELSE '' END LIBERACAO, "	
	cQuery += "        R.ZJ_PEDIDO AS PEDIDO, "
	cQuery += "        R.A1_COD AS COD_CLIENTE, "
	cQuery += "        R.TIPO_PED, "
	cQuery += "        R.A1_NOME AS CLIENTE, "
	cQuery += "        R.A1_EST AS UF, "	
	cQuery += "        R.ZJ_LOTDAP AS DAP, "
 	cQuery += "        CONVERT(CHAR,CAST(R.ZJ_DATA AS SMALLDATETIME), 103) AS DATA_PRE, "
	cQuery += "        R.ZJ_NUMPF AS NUM_PRE_FATURAMENTO, "
	cQuery += "        R.QTDE_PRE, "
	cQuery += "        R.QTDE_CONF, "
	cQuery += "        R.QTDE_PRE - R.QTDE_CONF AS A_CONFERIR, "
	cQuery += "        R.DESCONTO, "
	cQuery += "        R.COND_PGTO, "	
	cQuery += "        R.FRETE, "
	cQuery += "        ((R.VALOR_TOTAL - R.DESCONTO) + R.FRETE) AS VALOR_TOTAL_PRE, "
	cQuery += "        R.VALOR_A_CONFERIR, "
	cQuery += "        IIF(R.FALTA_ESTOQUE = 'F','Falta Estoque', "	
	cQuery += "        CASE "
	cQuery += "            WHEN R.ZJ_DOC <> '' "
	cQuery += "                 AND R.ZJ_CONF = 'S' THEN 'Faturado' "
	cQuery += "            ELSE CASE "
	cQuery += "                     WHEN R.ZJ_DOC = '' "
	cQuery += "                          AND R.ZJ_CONF = 'S' THEN 'Conferido' "
	cQuery += "                     ELSE CASE "
	cQuery += "                              WHEN R.QTDE_CONF > 0 "
	cQuery += "                                   AND R.ZJ_CONF = 'L' THEN 'Em Confer�ncia' "
	cQuery += "                              ELSE CASE "
	cQuery += "                                       WHEN R.QTDE_CONF = 0 "
	cQuery += "                                            AND R.ZJ_CONF = 'L' THEN 'Liberado para Confer�ncia' "
	cQuery += "                                       ELSE CASE "
	cQuery += "                                                WHEN R.ZJ_CONF = '' THEN 'Dap Gerado' "
	cQuery += "                                                ELSE 'Sem Status' "
	cQuery += "                                            END "
	cQuery += "                                   END "
	cQuery += "                          END "
	cQuery += "                 END "
	cQuery += "        END) AS STATUS, "
	cQuery += "        ISNULL(R.CONFERENTE,'') AS CONFERENTE, "
	cQuery += "        ISNULL(CONVERT(CHAR,CAST(R.DATA_CONF AS SMALLDATETIME), 103),'') AS DATA_CONF, "	
	cQuery += "        R.ZJ_DOC AS NOTA_FISCAL, "
	cQuery += "        CASE WHEN R.EMISSAO_NF <> '' THEN CONVERT(CHAR,CAST(R.EMISSAO_NF AS SMALLDATETIME), 103) ELSE '' END DATA_FAT "
	cQuery += " FROM "
	cQuery += "   (SELECT C5.C5_EMISSAO, "
	cQuery += "           C5.C5_FECENT AS ENTREGA, "
	cQuery += "           (SELECT MAX(ZZ1_DTLIB) FROM "+RetSqlName("ZZ1")+" (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZZ1_PEDIDO = ZJ_PEDIDO GROUP BY ZZ1_PEDIDO) AS LIBERACAO, "	
	cQuery += "           ZJ_PEDIDO, "
	cQuery += "           Z1_CODIGO + ' - ' +Z1_DESC AS TIPO_PED, "
	cQuery += "           A1.A1_COD, "
	cQuery += "           A1.A1_NOME, "
	cQuery += "           A1.A1_EST, "	
	cQuery += "           ZJ_LOTDAP, "
	cQuery += "           ZJ_DATA, "
	cQuery += "           ZJ_NUMPF, "
	cQuery += "           SUM(ZJ_QTDLIB) AS QTDE_PRE, "
	cQuery += "           SUM(ZJ_QTDSEP) AS QTDE_CONF, "

	cQuery += "           (SELECT F.ZJ_FALTA FROM SZJ010 AS F WHERE F.D_E_L_E_T_ = '' AND F.ZJ_PEDIDO = ZJ.ZJ_PEDIDO AND F.ZJ_NUMPF = ZJ.ZJ_NUMPF AND F.ZJ_FALTA = 'F' GROUP BY F.ZJ_PEDIDO,F.ZJ_NUMPF,ZJ_FALTA) AS FALTA_ESTOQUE, "	
	cQuery += "           (SELECT TOP 1 ZP.ZP_USR FROM "+RetSqlName("SZP")+" (NOLOCK) AS ZP WHERE ZP.D_E_L_E_T_ = '' AND ZP.ZP_NUMPF = ZJ_NUMPF GROUP BY ZP.ZP_NUMPF,ZP.ZP_USR) AS CONFERENTE, "
	cQuery += "           (SELECT MAX(ZP.ZP_DATA) FROM "+RetsqlName("SZP")+" (NOLOCK) AS ZP WHERE ZP.D_E_L_E_T_ = '' AND ZP.ZP_NUMPF = ZJ_NUMPF GROUP BY ZP.ZP_NUMPF) AS DATA_CONF, "
		
	cQuery += "           C5_DESCONT AS DESCONTO, "
	cQuery += "           C5_CONDPAG AS COND_PGTO, "	
	cQuery += "           C5_FRETE AS FRETE, "
	cQuery += "           SUM(ZJ_QTDLIB * C6_PRCVEN) AS VALOR_TOTAL, "
	cQuery += "           SUM((ZJ_QTDLIB - ZJ_QTDSEP) * C6_PRCVEN) AS VALOR_A_CONFERIR, "
	cQuery += "           ZJ_CONF, "
	cQuery += "           ZJ_DOC, "
	cQuery += "           ISNULL(F2_EMISSAO, '') AS EMISSAO_NF "
	cQuery += "    FROM "+RetSqlName("SZJ")+" (NOLOCK) AS ZJ "
	cQuery += "    INNER JOIN "+RetSqlName("SA1")+" (NOLOCK) AS A1 ON (A1_COD = ZJ_CLIENTE "
	cQuery += "                                         AND A1.D_E_L_E_T_ <> '*') "
	cQuery += "    LEFT JOIN "+RetSqlName("SF2")+" (NOLOCK) AS F2 ON (F2.F2_FILIAL = ZJ_FILIAL "
	cQuery += "                                        AND F2_DOC = ZJ_DOC "
	cQuery += "                                        AND F2.D_E_L_E_T_ <> '*') "
	cQuery += "    INNER JOIN "+RetSqlName("SC6")+" (NOLOCK) AS C6 ON (C6_FILIAL = ZJ_FILIAL "
	cQuery += "                                         AND C6_NUM = ZJ_PEDIDO "
	cQuery += "                                         AND C6_PRODUTO = ZJ_PRODUTO "
	cQuery += "                                         AND C6.D_E_L_E_T_ <> '*') "
	cQuery += "    INNER JOIN "+RetSqlName("SC5")+" (NOLOCK) AS C5 ON (C5_FILIAL = ZJ_FILIAL "
	cQuery += "                                         AND C5_NUM = ZJ_PEDIDO "
	cQuery += "                                         AND C5.D_E_L_E_T_ <> '*') "
	cQuery += "    LEFT JOIN "+RetSqlName("SZ1")+" (NOLOCK)AS Z1 ON (Z1_CODIGO = C5_TPPED "
	cQuery += "                                       AND Z1.D_E_L_E_T_ <> '*') "
	cQuery += " WHERE ZJ.D_E_L_E_T_ <> '*' AND ZJ_DATA BETWEEN '"+ DTOS(mv_par05) +"'  AND '"+ DTOS(mv_par06) + "'  "	
	
	IF !EMPTY(mv_par02)	
	cQuery += " AND ZJ_NUMPF BETWEEN  '"+mv_par02+"' AND '"+mv_par03+"' "
	ENDIF
		
	if !EMPTY(mv_par01)
       	cQuery += " AND ZJ_LOTDAP = '"+Alltrim(mv_par01)+"'  "
    ENDIF
    
    IF !EMPTY(mv_par04)
    	cQuery += " AND ZJ_PEDIDO = '"+Alltrim(mv_par04)+"' "
    ENDIF
    
    IF Upper(mv_par07) == "S"   
    	cQuery += " AND ZJ_DOC <> ''  "
    elseif Upper(mv_par07) == "N"   	   	
    	cQuery += " AND ZJ_DOC = '' "
    ENDIF
    
    If mv_par08 == 2 //Sim
    	cQuery += " AND ZJ_NUMPF IN (SELECT  ZJ_NUMPF FROM SZJ010 WHERE D_E_L_E_T_ = '' AND ZJ_DOC = '' AND ZJ_FALTA = 'F' GROUP BY ZJ_NUMPF) "
    EndIf
	
	cQuery += "    GROUP BY C5.C5_EMISSAO, "
	cQuery += "             C5.C5_FECENT, "
	cQuery += "             ZJ_PEDIDO, "
	cQuery += "             Z1_CODIGO, "
	cQuery += "             Z1_DESC, "
	cQuery += "             A1_NOME, "
	cQuery += "             ZJ_LOTDAP, "
	cQuery += "             ZJ_DATA, "
	cQuery += "             ZJ_NUMPF, "
	cQuery += "             ZJ_CONF, "
	cQuery += "             ZJ_DOC, "
	cQuery += "             F2_EMISSAO, "
	cQuery += "             ZJ_CONF, "
	cQuery += "             A1.A1_COD, "
	cQuery += "             C5_DESCONT, "
	cQuery += "             C5_CONDPAG,	"
	cQuery += "             A1.A1_EST, "
	cQuery += "             C5_FRETE) R	 "
	    
       	
	TCQUERY cQuery NEW ALIAS STA

	While STA->(!EOF())
	
		cNomeConf := UsrRetName(STA->CONFERENTE)
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("EMISSAO"):SetValue(STA->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")
		
		oSection1:Cell("ENTREGA"):SetValue(STA->ENTREGA)
		oSection1:Cell("ENTREGA"):SetAlign("LEFT")
		
		oSection1:Cell("LIBERACAO"):SetValue(STA->LIBERACAO)
		oSection1:Cell("LIBERACAO"):SetAlign("LEFT")		
		
		oSection1:Cell("TIPO_PED"):SetValue(STA->TIPO_PED)
		oSection1:Cell("TIPO_PED"):SetAlign("LEFT")
		
		oSection1:Cell("PEDIDO"):SetValue(STA->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_CLIENTE"):SetValue(STA->COD_CLIENTE)
		oSection1:Cell("COD_CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("CLIENTE"):SetValue(STA->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
				
		oSection1:Cell("UF"):SetValue(STA->UF)
		oSection1:Cell("UF"):SetAlign("LEFT")			
				
		oSection1:Cell("DAP"):SetValue(STA->DAP)
		oSection1:Cell("DAP"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_PRE"):SetValue(STA->DATA_PRE)
		oSection1:Cell("DATA_PRE"):SetAlign("LEFT")
		
		oSection1:Cell("NUM_PRE_FATURAMENTO"):SetValue(STA->NUM_PRE_FATURAMENTO)
		oSection1:Cell("NUM_PRE_FATURAMENTO"):SetAlign("LEFT")
				
		oSection1:Cell("QTDE_PRE"):SetValue(STA->QTDE_PRE)
		oSection1:Cell("QTDE_PRE"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_CONF"):SetValue(STA->QTDE_CONF)
		oSection1:Cell("QTDE_CONF"):SetAlign("LEFT")
				
		oSection1:Cell("A_CONFERIR"):SetValue(STA->A_CONFERIR)
		oSection1:Cell("A_CONFERIR"):SetAlign("LEFT")		
		
		oSection1:Cell("DESCONTO"):SetValue(STA->DESCONTO)
		oSection1:Cell("DESCONTO"):SetAlign("LEFT")		
		
		oSection1:Cell("FRETE"):SetValue(STA->FRETE)
		oSection1:Cell("FRETE"):SetAlign("LEFT")
		
		oSection1:Cell("COND_PGTO"):SetValue(STA->COND_PGTO)
		oSection1:Cell("COND_PGTO"):SetAlign("LEFT")		
		
		oSection1:Cell("VALOR_TOTAL_PRE"):SetValue(STA->VALOR_TOTAL_PRE)
		oSection1:Cell("VALOR_TOTAL_PRE"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR_A_CONFERIR"):SetValue(STA->VALOR_A_CONFERIR)
		oSection1:Cell("VALOR_A_CONFERIR"):SetAlign("LEFT")
		
		oSection1:Cell("STATUS"):SetValue(STA->STATUS)
		oSection1:Cell("STATUS"):SetAlign("LEFT")
		
		oSection1:Cell("CONFERENTE"):SetValue(cNomeConf)
		oSection1:Cell("CONFERENTE"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_CONF"):SetValue(STA->DATA_CONF)
		oSection1:Cell("DATA_CONF"):SetAlign("LEFT")
		
		oSection1:Cell("NOTA_FISCAL"):SetValue(STA->NOTA_FISCAL)
		oSection1:Cell("NOTA_FISCAL"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_FAT"):SetValue(STA->DATA_FAT)
		oSection1:Cell("DATA_FAT"):SetAlign("LEFT")
		
											
		oSection1:PrintLine()
		
		STA->(DBSKIP()) 
	enddo
	STA->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Lote Dap ?"		        		,""		,""		,"mv_ch1","C",06,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Pr�-Fatuamento de ?"            ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Pr�-Fatuamento ate ?"		    ,""		,""		,"mv_ch3","C",08,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Pedido ?"		                ,""		,""		,"mv_ch4","C",06,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Data Pre-Faturamento de ?"		,""		,""		,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "06","Data Pre-Faturamento at� ?"		,""		,""		,"mv_ch6","D",08,0,1,"G",""	,""	,"","","mv_par06"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "07","Faturado ?"						,""		,""		,"mv_ch7","C",02,0,1,"G",""	,""	,"","","mv_par07"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "08","Somente com falta ?"			,""		,""		,"mv_ch8","C",03,0,1,"C",""	,""	,"","","mv_par08"," ","","","","","","","","","","","","","","","")
Return