#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL010 
Pe�as bipadas
@author Weskley Silva
@since 07/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL012()

Private oReport
Private cPergCont	:= 'HPREL012' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'PEC', 'PE�AS BIPADAS', , {|oReport| ReportPrint( oReport ), 'PE�AS BIPADAS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'PE�AS BIPADAS', { 'PEC', 'SZP','SZJ','SA1','SA2','SC5','SZ1','SBM'})
			
	TRCell():New( oSection1, 'DATA_CONFERIDA'	    ,'PEC', 		'DATA_CONFERIDA',					"@!"                        ,10)
	TRCell():New( oSection1, 'HORA_CONFERIDA'	    ,'PEC', 		'HORA_CONFERIDA',  					"@!"				        ,35)
	TRCell():New( oSection1, 'CONFERENTE' 	        ,'PEC', 		'CONFERENTE',          				"@!"						,15)
	TRCell():New( oSection1, 'NOME_CONFERENTE'     	,'PEC', 		'NOME_CONFERENTE',	       			"@!"						,30)
	TRCell():New( oSection1, 'NUMERO_PEDIDO'		,'PEC', 		'NUMERO_PEDIDO',       				"@!"		                ,15)
	TRCell():New( oSection1, 'NUM_PRE_FATURAMENTO'  ,'PEC', 		'NUM_PRE_FATURAMENTO', 				"@!"				        ,15)
	TRCell():New( oSection1, 'CLIENTE'  			,'PEC', 		'CLIENTE',		    			    "@!"				        ,35)
	TRCell():New( oSection1, 'LOJA' 				,'PEC', 	    'LOJA',                      		"@!"		    	        ,15)
	TRCell():New( oSection1, 'RAZAO_SOCIAL'	        ,'PEC', 		'RAZAO_SOCIAL',      	   			"@!"						,15)
	TRCell():New( oSection1, 'PRODUTO'				,'PEC', 		'PRODUTO',				   			"@!"						,15)
	TRCell():New( oSection1, 'DESCRICAO'			,'PEC', 		'DESCRICAO',			   			"@!"						,15)
	TRCell():New( oSection1, 'QTD_CONFERIDA'		,'PEC', 		'QTD_CONFERIDA',					"@!"						,06)
	TRCell():New( oSection1, 'GRUPO_PRODUTO'		,'PEC', 		'GRUPO_PRODUTO',   					"@!"						,40)
	TRCell():New( oSection1, 'MARCA'			    ,'PEC', 		'MARCA',   					        "@!"						,40)
	TRCell():New( oSection1, 'TIPO_PEDIDO'			,'PEC', 		'TIPO_PEDIDO',   					"@!"						,40)
	TRCell():New( oSection1, 'CANAL_VENDA'			,'PEC', 		'CANAL_VENDA',   					"@!"						,40)
	
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("PEC") > 0
		PEC->(dbCloseArea())
	Endif
      
    cQuery := " SELECT CONVERT(CHAR, CAST(SZP.ZP_DATA AS SMALLDATETIME), 103) AS [DATA_CONFERIDA], " 
	cQuery += " SZP.ZP_HORA AS [HORA_CONFERIDA], " 
	cQuery += " SZP.ZP_USR AS [CONFERENTE], " 
	cQuery += " SZP.ZP_NOMEUSR AS [NOME_CONFERENTE], " 
	cQuery += " SZP.ZP_PEDIDO AS [NUMERO_PEDIDO], " 
	cQuery += " SZP.ZP_NUMPF AS [NUM_PRE_FATURAMENTO], " 
	cQuery += " CASE SC5.C5_TIPO WHEN 'B' THEN LTRIM(RTRIM(ISNULL(SA2.A2_COD, ''))) ELSE LTRIM(RTRIM(ISNULL(SA1.A1_COD, ''))) END AS CLIENTE, "
    cQuery += " CASE SC5.C5_TIPO WHEN 'B' THEN LTRIM(RTRIM(ISNULL(SA2.A2_LOJA, ''))) ELSE LTRIM(RTRIM(ISNULL(SA1.A1_LOJA, ''))) END AS LOJA, "
	cQuery += " CASE SC5.C5_TIPO WHEN 'B' THEN LTRIM(RTRIM(ISNULL(SA2.A2_NOME, ''))) ELSE LTRIM(RTRIM(ISNULL(SA1.A1_NOME, ''))) END AS [RAZAO_SOCIAL], "
	cQuery += " SZP.ZP_PRODUTO AS PRODUTO, "
	cQuery += " SB1.B1_DESC AS DESCRICAO, "
	cQuery += " SZP.ZP_QUANT AS [QTD_CONFERIDA], "
	cQuery += " SBM.BM_DESC AS GRUPO_PRODUTO, "
	cQuery += " SB1.B1_XDESCMA AS MARCA, "
	cQuery += " Z1.Z1_DESC AS TIPO_PEDIDO, "  
	cQuery += " ISNULL(ZA4.ZA4_DESCRI, '') AS [CANAL_VENDA] "

	cQuery += " FROM  "+RetSqlName('SZP')+" SZP (NOLOCK) "
	cQuery += " INNER JOIN "+RetSqlName('SB1')+"  SB1 (NOLOCK) ON SB1.B1_COD = SZP.ZP_PRODUTO AND SB1.D_E_L_E_T_ = '' "
	cQuery += " INNER JOIN "+RetSqlName('SC5')+"  SC5 (NOLOCK) ON SC5.C5_NUM = SZP.ZP_PEDIDO AND SC5.D_E_L_E_T_ = '' "
	cQuery += " LEFT OUTER JOIN "+RetSqlName('SA1')+" SA1 (NOLOCK) ON SC5.C5_CLIENTE = SA1.A1_COD AND SC5.C5_LOJAENT = SA1.A1_LOJA AND SA1.D_E_L_E_T_ = ''  "
	cQuery += " LEFT OUTER JOIN "+RetSqlName('SA2')+" SA2 (NOLOCK) ON SC5.C5_CLIENTE = SA2.A2_COD AND SC5.C5_LOJAENT = SA2.A2_LOJA AND SA2.D_E_L_E_T_ = ''  "
	cQuery += " LEFT OUTER JOIN "+RetSqlName('ZA4')+" ZA4 (NOLOCK) ON SA1.A1_XMICRRE = ZA4.ZA4_CODMAC AND ZA4.D_E_L_E_T_ = '' "
	cQuery += " LEFT OUTER JOIN "+RetSqlName('SBM')+" SBM (NOLOCK) ON (SBM.BM_GRUPO = SB1.B1_GRUPO AND SBM.D_E_L_E_T_ = '') "
	cQuery += " INNER JOIN "+RetSqlName('SZ1')+" Z1 (NOLOCK) ON Z1_CODIGO = C5_TPPED AND Z1.D_E_L_E_T_ = '' "
	cQuery += " WHERE SZP.D_E_L_E_T_ = '' "  
    
    IF !EMPTY(mv_par01)
    	 cQuery += " AND SZP.ZP_NUMPF BETWEEN  '"+mv_par02+"' AND '"+mv_par03+"' "
    endif
   
    IF !EMPTY(mv_par03)
    	cQuery += " AND SZP.ZP_NUMPED = '"+Alltrim(mv_par04)+"' "
    ENDIF
    
    if !EMPTY(mv_par04) 
    	cQuery += " AND SZP.ZP_DATA BETWEEN "+ DTOS(mv_par04)+ " AND "+ DTOS(mv_par05)+ "  "
    endif
    
    if !Empty(mv_par06)
       	cQuery += " AND LEFT(ZP_HORA,5) BETWEEN '"+Alltrim(mv_par06)+"' AND '"+Alltrim(mv_par07)+"' "
    endif
   
   	cQuery += " GROUP BY SZP.ZP_DATA, " 
	cQuery += "	 SZP.ZP_HORA, "
	cQuery += "	 SZP.ZP_USR, "
	cQuery += "	 SZP.ZP_NOMEUSR, "
	cQuery += "	 SZP.ZP_PEDIDO, "
	cQuery += "	 SZP.ZP_NUMPF, "
	cQuery += "	 SC5.C5_TIPO, "
	cQuery += "	 SA2.A2_COD, "
	cQuery += "	 SA1.A1_COD, "
	cQuery += "	 SA2.A2_LOJA, "
	cQuery += "	 SA1.A1_LOJA, "
	cQuery += "	 SA2.A2_NOME, "
	cQuery += "	 SA1.A1_NOME, "
	cQuery += "	 SZP.ZP_PRODUTO, "
	cQuery += "	 SB1.B1_DESC, "
	cQuery += "	 SZP.ZP_QUANT, "
	cQuery += "	 SBM.BM_DESC, "
	cQuery += "	 SB1.B1_XDESCMA, "
	cQuery += "	 Z1.Z1_DESC, "
	cQuery += "	 ZA4.ZA4_DESCRI  " 
	
	TCQUERY cQuery NEW ALIAS PEC

	While PEC->(!EOF())
	
	
		cNomeConf := UsrRetName(PEC->CONFERENTE)

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("DATA_CONFERIDA"):SetValue(PEC->DATA_CONFERIDA)
		oSection1:Cell("DATA_CONFERIDA"):SetAlign("LEFT")

		oSection1:Cell("HORA_CONFERIDA"):SetValue(PEC->HORA_CONFERIDA)
		oSection1:Cell("HORA_CONFERIDA"):SetAlign("LEFT")
		
		oSection1:Cell("CONFERENTE"):SetValue(PEC->CONFERENTE)
		oSection1:Cell("CONFERENTE"):SetAlign("LEFT")
		
		oSection1:Cell("NOME_CONFERENTE"):SetValue(cNomeConf)
		oSection1:Cell("NOME_CONFERENTE"):SetAlign("LEFT")
		
		oSection1:Cell("NUMERO_PEDIDO"):SetValue(PEC->NUMERO_PEDIDO)
		oSection1:Cell("NUMERO_PEDIDO"):SetAlign("LEFT")
			
		oSection1:Cell("NUM_PRE_FATURAMENTO"):SetValue(PEC->NUM_PRE_FATURAMENTO)
		oSection1:Cell("NUM_PRE_FATURAMENTO"):SetAlign("LEFT")
				
		oSection1:Cell("CLIENTE"):SetValue(PEC->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
				
		oSection1:Cell("LOJA"):SetValue(PEC->LOJA)
		oSection1:Cell("LOJA"):SetAlign("LEFT")
		
		oSection1:Cell("RAZAO_SOCIAL"):SetValue(PEC->RAZAO_SOCIAL)
		oSection1:Cell("RAZAO_SOCIAL"):SetAlign("LEFT")
				
		oSection1:Cell("PRODUTO"):SetValue(PEC->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")
						
		oSection1:Cell("DESCRICAO"):SetValue(PEC->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("QTD_CONFERIDA"):SetValue(PEC->QTD_CONFERIDA)
		oSection1:Cell("QTD_CONFERIDA"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO_PRODUTO"):SetValue(PEC->GRUPO_PRODUTO)
		oSection1:Cell("GRUPO_PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("MARCA"):SetValue(PEC->MARCA)
		oSection1:Cell("MARCA"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO_PEDIDO"):SetValue(PEC->TIPO_PEDIDO)
		oSection1:Cell("TIPO_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("CANAL_VENDA"):SetValue(PEC->CANAL_VENDA)
		oSection1:Cell("CANAL_VENDA"):SetAlign("LEFT")
											
		oSection1:PrintLine()
		
		PEC->(DBSKIP()) 
	enddo
	PEC->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	
	PutSx1(cPergCont, "01","Pr�-Fatuamento de ?"            ,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Pr�-Fatuamento ate ?"		    ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Pedido ?"		                ,""		,""		,"mv_ch3","C",06,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Data Conferencia de ?"		    ,""		,""		,"mv_ch4","D",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Data Conferencia at� ?"		    ,""		,""		,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "06","Hora de ?"		   				,""		,""		,"mv_ch6","C",08,0,1,"G",""	,""	,"","","mv_par06"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "07","Hora at� ?"					    ,""		,""		,"mv_ch7","C",08,0,1,"G",""	,""	,"","","mv_par07"," ","","","","","","","","","","","","","","","")
Return