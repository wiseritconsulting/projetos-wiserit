//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de Faturamento E-commerce.
Rotina para gerar todos os pedidos faturados de loja virtual num per�odo espec�fico;

@author Ronaldo Pereira
@since 16 de Maio de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL052()

	Local aRet := {}
	Local aPerg := {}
	
	aAdd(aPerg,{1,"Data Emiss�o de"		,dDataBase,PesqPict("SF2", "F2_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.}) 
	aAdd(aPerg,{1,"Data Emiss�o At�" 	,dDataBase,PesqPict("SF2", "F2_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.}) 
	aAdd(aPerg,{1,"Nota Fiscal: " 		,Space(15),""   ,""		,""	    ,""		,0	,.F.}) 
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| Geradados(aRet)}, "Exportando dados")		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Faturamento '+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Volumes"
    Local cTable1    	:= "Faturamento por Volumes E-commerce" 

        
    //Dados tabela 1          
    cQry := " SELECT F2.F2_FILIAL AS FILIAL, "
    cQry += " 		 CONVERT(CHAR, CAST(F2.F2_EMISSAO AS smalldatetime), 103) AS EMISSAO, "
    cQry += "        C5.C5_XDESCPO AS POLITICA, "
    cQry += "        F2.F2_DOC AS NT_FISCAL, "
    cQry += "        C5.C5_NUM AS PEDIDO, "
    cQry += "        F2_PBRUTO AS PESO_BRT, "
    cQry += "        F2_PLIQUI AS PESO_LQD, "
    cQry += "        REPLACE((F2_PBRUTO - F2_PLIQUI),',', '.') AS TARA, "

    cQry += "        ISNULL( "
    cQry += "                 (SELECT ZQ_CODIGO "
    cQry += "                  FROM "+RetSqlName("SZQ")+" (NOLOCK) "
    cQry += "                  WHERE D_E_L_E_T_ = '' "
    cQry += "                    AND ZQ_CODIGO IN ('028', '029', '030', '031', '034') "
    cQry += "                    AND ZQ_TARA = "
    cQry += "                      (SELECT REPLACE((SF2.F2_PBRUTO - SF2.F2_PLIQUI),',', '.') "
    cQry += "                       FROM "+RetSqlName("SF2")+" AS SF2 (NOLOCK) "
    cQry += "                       WHERE SF2.D_E_L_E_T_ = '' "
    cQry += "                         AND SF2.F2_FILIAL = F2.F2_FILIAL "
    cQry += "                         AND SF2.F2_CLIENTE = F2.F2_CLIENTE "
    cQry += "                         AND SF2.F2_DOC = F2.F2_DOC)),'') AS CAIXA, "

    cQry += "        ISNULL( "
    cQry += "                 (SELECT ZQ_DESCRIC "
    cQry += "                  FROM "+RetSqlName("SZQ")+" (NOLOCK) "
    cQry += "                  WHERE D_E_L_E_T_ = '' "
    cQry += "                    AND ZQ_CODIGO IN ('028', '029', '030', '031', '034') "
    cQry += "                    AND ZQ_TARA = "
    cQry += "                      (SELECT REPLACE((SF2.F2_PBRUTO - SF2.F2_PLIQUI),',', '.') "
    cQry += "                       FROM "+RetSqlName("SF2")+" AS SF2 (NOLOCK) "
    cQry += "                       WHERE SF2.D_E_L_E_T_ = '' "
    cQry += "                         AND SF2.F2_FILIAL = F2.F2_FILIAL "
    cQry += "                        AND SF2.F2_CLIENTE = F2.F2_CLIENTE "
    cQry += "                        AND SF2.F2_DOC = F2.F2_DOC)),'') AS DESCRICAO "
     
    cQry += " FROM "+RetSqlName("SF2")+" AS F2 (NOLOCK) "
    cQry += " INNER JOIN "+RetSqlName("SC5")+" AS C5 (NOLOCK) ON (C5.C5_FILIAL = F2.F2_FILIAL "
    cQry += "                                      AND C5.C5_CLIENTE = F2.F2_CLIENTE "
    cQry += "                                      AND C5.C5_NOTA = F2.F2_DOC "
    cQry += "                                      AND C5.D_E_L_E_T_ = '') "
    cQry += " WHERE F2.D_E_L_E_T_ = '' AND C5.C5_POLCOM = '018' "
	cQry += " AND F2.F2_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"' "

    If !Empty(aRet[3])
    	cQry += " AND F2_DOC = '"+Alltrim(aRet[3])+"' "
	Endif  
	 
    cQry += "ORDER BY F2.F2_EMISSAO "

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"EMISS�O",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"POLITICA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"NT_FISCAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PEDIDO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PESO_BRT",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PESO_LQD",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TARA",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CAIXA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESCRI��O",1)
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                               QRY->FILIAL,;
                                               QRY->EMISSAO	,;
                                               QRY->POLITICA,;
                                               QRY->NT_FISCAL,;
                                               QRY->PEDIDO,;
                                               QRY->PESO_BRT,;
                                               QRY->PESO_LQD,;                                               
                                               QRY->TARA,;
                                               QRY->CAIXA,;
                                               QRY->DESCRICAO;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return