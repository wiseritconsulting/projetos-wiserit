#INCLUDE "protheus.ch"

#INCLUDE "rwmake.ch"

#INCLUDE "topconn.ch"



User Function OM010TOK



	Local cEOL	     := +Chr(13)+Chr(10)

	Local _i

	xtab  := PARAMIXB[1]:AALLSUBMODELS[1]:ADATAMODEL[1][1][2]

	xcols := PARAMIXB[1]:AALLSUBMODELS[1]:ADATAMODEL[2][1][3]

	xAHeaderDA1 := PARAMIXB[1]:AALLSUBMODELS[3]:AHEADER



	DbSelectArea("DA0")

	DbSetOrder(1)

	DbSeek(xfilial("DA0")+xtab)

 		  

	If AllTrim(DA0->DA0_TABVIR) == ""



		For _i := 1 to len(xcols)

	

			_qry1 := "UPDATE "+RetSQLName("DA1")+" SET DA1_PRCVEN = "+AllTrim(str(xcols[_i,10]))+"*DA0_FATOR "+cEOL

			_qry1 += "FROM "+RetSqlName("DA1")+" DA1 "+cEOL

			_qry1 += "		INNER JOIN "+RetSqlName("DA0")+" DA0 ON DA0.D_E_L_E_T_ = '' AND DA0_CODTAB = DA1_CODTAB AND DA0_TABVIR = '"+xtab+"' "+cEOL

			_qry1 += "WHERE DA1_CODPRO = '"+xCols[_i,5]+"' "+cEOL

			TcSqlExec(_qry1)

	

			_qry2 := "SELECT DA0_CODTAB AS DA0CODTAB, DA0_DATDE AS DA0DATDE, DA0_FATOR AS DA0FATOR "+cEOL

			_qry2 += "FROM "+RetSqlName("DA0")+" DA0 WHERE D_E_L_E_T_<>'*' AND DA0_TABVIR= '"+xtab+"' "+cEOL

	

			If Select("TMPDA0") > 0

				TMPDA0->(DbCloseArea())

			EndIf

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry2),"TMPDA0",.T.,.T.)

			DbSelectArea("TMPDA0")

			TMPDA0->(DbGoTop())

			

			While !TMPDA0->(Eof())

			

				_qry3 := "UPDATE "+RetSQLName("DA1")+" SET D_E_L_E_T_='*', R_E_C_D_E_L_=R_E_C_N_O_ "+cEOL

				_qry3 += "WHERE D_E_L_E_T_<>'*' AND DA1_CODTAB='"+TMPDA0->DA0CODTAB+"' "+cEOL

				TcSqlExec(_qry3)



				_qry4 := "SELECT DA1_ITEM AS DA1ITEM  FROM "+RetSQLName("DA1") +cEOL

				_qry4 += "WHERE D_E_L_E_T_<>'*' AND DA1_CODTAB='"+TMPDA0->DA0CODTAB+"' AND DA1_CODPRO = '"+xCols[_i,5]+"' "+cEOL

	

				If Select("TMPDA1") > 0

					TMPDA1->(DbCloseArea())

				EndIf

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry4),"TMPDA1",.T.,.T.)

				DbSelectArea("TMPDA1")

				

				If AllTrim(TMPDA1->DA1ITEM)==""

				

					If !(AllTrim(xCols[_i,2]) == AllTrim(TMPDA1->DA1ITEM))

					

						RecLock("DA1",.T.)

						For _x := 1 to len(PARAMIXB[1]:AALLSUBMODELS[3]:AHEADER)

							

							_cCampo	:= AllTrim(PARAMIXB[1]:AALLSUBMODELS[3]:AHEADER[_x,2])

							

							If PARAMIXB[1]:AALLSUBMODELS[3]:AHEADER[_x,10] == "R"

								If PARAMIXB[1]:AALLSUBMODELS[3]:AHEADER[_x,2] == "DA1_PRCVEN"

									Replace &_cCampo	with xcols[_i,10] * TMPDA0->DA0FATOR

								ElseIf PARAMIXB[1]:AALLSUBMODELS[3]:AHEADER[_x,2] == "DA1_CODTAB"

									Replace &_cCampo	with TMPDA0->DA0CODTAB

								Else

									Replace &_cCampo	with xCols[_i,_x]

								End

							End

							

						Next

						Replace DA1_QTDLOT			with 999999            

						Replace DA1_INDLOT 			with '000000000999999.99'

						MsUnLock()

					End

		

				End

			

				TMPDA0->(DbSkip())

			

			EndDo

		

		Next

	

	Else

		

		aHeaderXx		:= {}

		aColsEx		:= {}

		aFieldFill		:= {}

		cTabVirt		:= DA0->DA0_TABVIR

		cTabReal		:= DA0->DA0_CODTAB

		cDtVirt		:= DA0->DA0_DATDE

		cFator			:= DA0->DA0_FATOR

		

		//Carrega campos e o Primeiro Item da Tabela de Precos

		For _nX := 1 to Len(PARAMIXB[1]:AALLSUBMODELS[3]:AHEADER)

			Aadd(aHeaderXx, {AllTrim(PARAMIXB[1]:AALLSUBMODELS[3]:AHEADER[_nX,2]), PARAMIXB[1]:AALLSUBMODELS[1]:ADATAMODEL[2,1,3,1,_nX]})

		Next



		DbSelectArea("DA1")

		DbSetOrder(1)

		DbSeek(xfilial("DA1")+cTabVirt)

		

		//Cria arrey com a tabela Real 

		While !EOF() .and. DA1->DA1_CODTAB == cTabVirt

  		

			For _i := 1 To Len(aHeaderXx)

  				

				If aHeaderXx[_i,1] == "DA1_FILIAL"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		DA1->DA1_FILIAL})

				ElseIf aHeaderXx[_i,1] == "DA1_CODTAB"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		cTabReal})

				ElseIf aHeaderXx[_i,1] == "DA1_ITEM"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		DA1->DA1_ITEM})

				ElseIf aHeaderXx[_i,1] == "DA1_CODPRO"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		DA1->DA1_CODPRO})

				ElseIf aHeaderXx[_i,1] == "DA1_PRCVEN"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		DA1->DA1_PRCVEN*cFator})

				ElseIf aHeaderXx[_i,1] == "DA1_ATIVO"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		DA1->DA1_ATIVO})

				ElseIf aHeaderXx[_i,1] == "DA1_MOEDA"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		DA1->DA1_MOEDA})

				ElseIf aHeaderXx[_i,1] == "DA1_TPOPER"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		DA1->DA1_TPOPER})

				ElseIf aHeaderXx[_i,1] == "DA1_DATVIG"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		cDtVirt})

				ElseIf aHeaderXx[_i,1] == "DA1_QTDLOT"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		DA1->DA1_QTDLOT})

				ElseIf aHeaderXx[_i,1] == "DA1_INDLOT"

					Aadd(aFieldFill, {aHeaderXx[_i,1],		DA1->DA1_INDLOT})

				Else

					Aadd(aFieldFill, {aHeaderXx[_i,1],		aHeaderXx[_i,2]})

				End

			

			Next

			

			Aadd(aFieldFill, .F.)

			Aadd(aColsEx, aFieldFill)

			aFieldFill :={}





			DbSkip()

		

		EndDo



		//Limpa a Tabela Virtual			

		PARAMIXB[1]:AALLSUBMODELS[2]:ACOLS	:= {}

		

		_qry1 := "UPDATE "+RetSQLName("DA1")+" SET D_E_L_E_T_='*', R_E_C_D_E_L_=R_E_C_N_O_ "+cEOL

		_qry1 += "WHERE D_E_L_E_T_<>'*' AND DA1_CODTAB='"+cTabReal+"' "+cEOL

		TcSqlExec(_qry1)

		

		aFieldFill := {}

		//Grava a Tabela Virtual

		For _x := 1 To Len(aColsEx)



			If aColsEx[_x,2,2] <> "000001"

				RecLock("DA1",.T.)

			End

			

			For _y := 1 To Len(aColsEx[1])-1



				If aColsEx[_x,2,2] == "000001"

					Aadd(aFieldFill, aColsEx[_x,_y,2] )

				Else

					If aColsEx[_x,_y,1] == "DA1_FILIAL"

						Replace DA1_FILIAL			with aColsEx[_x,_y,2]

					ElseIf aColsEx[_x,_y,1] == "DA1_CODTAB"

						Replace DA1_CODTAB			with aColsEx[_x,_y,2]

					ElseIf aColsEx[_x,_y,1] == "DA1_ITEM"

						Replace DA1_ITEM				with aColsEx[_x,_y,2]

					ElseIf aColsEx[_x,_y,1] == "DA1_CODPRO"

						Replace DA1_CODPRO			with aColsEx[_x,_y,2]

					ElseIf aColsEx[_x,_y,1] == "DA1_PRCVEN"

						Replace DA1_PRCVEN			with aColsEx[_x,_y,2]

					ElseIf aColsEx[_x,_y,1] == "DA1_ATIVO"

						Replace DA1_ATIVO				with aColsEx[_x,_y,2]

					ElseIf aColsEx[_x,_y,1] == "DA1_MOEDA"

						Replace DA1_MOEDA				with aColsEx[_x,_y,2]

					ElseIf aColsEx[_x,_y,1] == "DA1_TPOPER"

						Replace DA1_TPOPER			with aColsEx[_x,_y,2]

					ElseIf aColsEx[_x,_y,1] == "DA1_DATVIG"

						Replace DA1_DATVIG			with aColsEx[_x,_y,2]

					ElseIf aColsEx[_x,_y,1] == "DA1_QTDLOT"

						Replace DA1_QTDLOT			with 999999

					End



				end



			Next _y

			

			If aColsEx[_x,2,2] <> "000001"

				MsUnLock()

			End



		Next _x



		Aadd(aFieldFill, .F.)

		Aadd(PARAMIXB[1]:AALLSUBMODELS[2]:ACOLS, aFieldFill)

		aFieldFill :={}

	

	Endif

	

Return (.T.)