#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "PROTHEUS.CH"

User Function M461VTot()

	Local lRet			:= .T.
	Local aArea		:= GetArea()
	Local nValTot		:= PARAMIXB[1] // Valor total da nota
	Local nRecnoE4	:= PARAMIXB[2] // RECNO da condi��o de pagamento
	Local nPedido		:= SC9->C9_PEDIDO
	Local cNovaCond	:= ""

	DbSelectArea("SC5")
	_areaSC5 := GetArea()
	DbSetOrder(1)
	DbSeek(xfilial("SC5")+nPedido)

	If !(SC5->C5_POLCOM $ GetMV("MV_XPOL300"))

		dbSelectArea("SE4")
		SE4->(dbGoTo(nRecnoE4))
		cCondAtu	:= SE4->E4_CODIGO
	
		aParc := Condicao(nValTot,cCondAtu,,dDataBase)
	
		If Len(aParc) > 1

			While aParc[1,2] < Getmv("MV_XVALMIN") .AND. Len(aParc) > 1
				
				Alert("Valor m�nimo da Parcela n�o permitido. Favor corrigir a Condi��o de Pagamento. Parcela m�nima permitida: R$ "+AllTrim(Str(Getmv("MV_XVALMIN"))))
				lRet := .F.
		
				If Pergunte("HFATP01",.T.)
			
					cNovaCond	:= MV_PAR01
					aParc := Condicao(nValTot,cNovaCond,,dDataBase)
				
				Else
					
					Return lRet
					
				End
		
			EndDo
		End
		
		If cNovaCond <> ""
		
			SC5->(Reclock("SC5",.F.))
				Replace SC5->C5_CONDPAG with cNovaCond
				Replace SC5->C5_XCONDOR with cCondAtu
			SC5->(MsUnLock())
			
			lRet := .T.
			
			RestArea(_areaSC5)
	
		End
	
	End 

	RestArea(aArea)

Return lRet