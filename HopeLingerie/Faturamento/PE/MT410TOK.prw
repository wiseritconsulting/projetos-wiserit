#Include "PROTHEUS.CH"
#include "TbiConn.ch"
#include "TOTVS.ch"
#include "topconn.ch"

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MT410TOK  �Autor  �Bruno Parreira      � Data �  23/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Valida pedido de venda.                                     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/

User Function MT410TOK()
Local l_Ret      := .T.  // Conteudo de retorno
Local nOpc      := PARAMIXB[1]		// Opcao de manutencao
Local aRecTiAdt := PARAMIXB[2]		// Array com registros de adiantamento
Local nx        := 0
Local _aAreaOLD := GetArea()                            
Local a_SC9		:= SC9->(GetArea())
Local a_SC6		:= SC6->(GetArea())
Local a_SC5		:= SC5->(GetArea()) 
Local nPosAmz   := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_LOCAL'}) 
Local cAmzReFat := AllTrim(SuperGetMV("MV_XAMZREF",.F.,"XX")) //Armazem padrao Re-Faturamento
Local _xAltPed  := ""
Local _xobs	  	:= "N" 
Local nPosPrd   := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRODUTO'})
Local nPosItem  := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_ITEM'})
Local nPosQtd   := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_QTDVEN'})
Local nCFOP     := aScan(aHeader,{|x| Alltrim(x[2]) == 'C6_CF'})
Local nPosDes    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_DESCRI'})
Local nPosPrc    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRCVEN'})
Local nPosPUn    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRUNIT'})
Local nPosVal    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_VALOR'})
Local nPosOpe    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_OPER'})
Local nPosTES    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_TES'})
Local nPosUM     := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_UM'})
Local nPosDtEnt  := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_ENTREG'})
Local nLocal  	 := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_LOCAL'})
Local nSitClass  := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_CLASFIS'})
Local nEndere    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_LOCALIZ'})
Local nCom1      := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_COMIS1'})
Local nCom2      := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_COMIS2'})
Local nCom3      := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_COMIS3'})
Local nCom4      := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_COMIS4'})
Local nCom5      := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_COMIS5'})

Private nSecQatu
Private lQbrPed  

lVer := .F.

lConf:= .F.
If nOpc == 4  .and. SC5->C5_TIPO = "N"
	_blqdap := SuperGetMV("MV_HBLQDAP",.F.,"") 
	
	If _blqdap = "S"
		DbSelectArea("SC9")
		DbSetOrder(1)
		DbSeek(xfilial("SC9")+SC5->C5_NUM)
		
			IF Select("TEMP") > 0
				TEMP->(dbCloseArea())
			Endif
		
		cQuery := " SELECT C9_NFISCAL, C9_PEDIDO FROM "+RetSqlName('SC9')+" WITH(NOLOCK)  WHERE C9_PEDIDO = '"+SC5->C5_NUM+"' AND D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_FILIAL = '0101' "
		
	    TCQUERY cQuery NEW ALIAS TEMP
		
		IF Empty(TEMP->C9_NFISCAL) .AND. !EMPTY(TEMP->C9_PEDIDO)
			l_Ret := .F.
			 MsgAlert("Pedido j� liberado para faturamento, n�o pode ser alterado!", "A T E N � � O")
		ENDIF
	Endif
Endif

IF l_Ret
	If nOpc == 4  .and. SC5->C5_TIPO = "N"
		DbSelectArea("SZJ")
		DbSetOrder(2) 
		If DbSeek(xFilial("SZJ")+SC5->C5_NUM)
			While SZJ->(!EOF()) .And. SZJ->ZJ_PEDIDO = SC5->C5_NUM
				If Empty(SZJ->ZJ_DOC)
					IF SZJ->ZJ_CONF = "S"
						lConf := .T.
					Endif
					lVer := .T.
				EndIf	
				SZJ->(DbSkip())
			EndDo
			lConf := .F.
			If lConf
				MsgAlert("Pedido j� efetivado! N�o poder� ser alterado!","A T E N � � O")
				l_Ret := .F.
			Else
				If lVer
					DbSelectArea("SZJ")
					DbSetOrder(2)
					DbGoTop() 
					DbSeek(xFilial("SZJ")+SC5->C5_NUM)
						
					If M->C5_CLIENTE <> SZJ->ZJ_CLIENTE
						MsgAlert("Cliente foi alterado! Pedido n�o poder� ter saldo!","A T E N � � O")
						l_Ret := .F.
					Else
						For nX:= 1 to len(acols)
							cGrade	 := acols[nX][35]
							If cGrade == 'S'
		//						If acols[nX][len(acols[nX])] = .F.
								For _y := 1 to len(oGrade:aColsGrade[nX])
									cProduto  := alltrim(acols[nX][2])
									cCor      := SubStr(OGRADE:ACOLSGRADE[nX][_y][1],2,3)
									aTamanho  := u_bTam1(cProduto,cCor)
									For _z := 2 to len(oGrade:aColsGrade[nX][_y])
										If oGrade:aColsGrade[nX][_y][_z][1] > 0
											If Len(aTamanho) >= len(oGrade:aColsGrade[nX][_y])-1
												DbSelectArea("SZJ")
												DbSetOrder(2)
												//DbSeek(xfilial("SZJ")+SC5->C5_NUM+aCols[nX,nPosItem]+cProduto+cCor+aTamanho[_z-1])
												If !Empty(aTamanho[_z-1][1])
													DbSeek(xfilial("SZJ")+SC5->C5_NUM+aCols[nX,nPosItem]+cProduto+cCor+aTamanho[_z-1][1])
				
													If acols[nX][len(aHeader)+1] = .F.
														If Found()
															If SZJ->ZJ_QTDLIB > oGrade:aColsGrade[nX][_y][_z][1] 
													   			//MsgAlert("O Produto "+cProduto+cCor+aTamanho[_z-1]+" sofreu alteracao. O Pedido nao podera ser salvo!","Atencao") 
													   			MsgAlert("O Produto "+cProduto+cCor+aTamanho[_z-1][1]+" sofreu alteracao. O Pedido nao podera ser salvo!","Atencao")
													   			l_Ret := .F.
													   		Endif
														Endif
													Else
														If Found()
												   			//MsgAlert("O Produto "+cProduto+cCor+aTamanho[_z-1]+" sofreu alteracao. O Pedido nao podera ser salvo!","Atencao") 
												   			MsgAlert("O Produto "+cProduto+cCor+aTamanho[_z-1][1]+" sofreu alteracao. O Pedido nao podera ser salvo!","Atencao")
												   			l_Ret := .F.
														Endif
													Endif
												EndIf
											Endif
										Endif
									Next _z
								Next _y
		//					End
							Else
								DbSelectArea("SZJ")
								DbSetOrder(2)
								DbSeek(xfilial("SZJ")+SC5->C5_NUM+aCols[nX,nPosItem]+aCols[nX,nPosPrd])
								
								If acols[nX][len(aHeader)+1] = .F.
									If Found() 
										If SZJ->ZJ_QTDLIB > aCols[nX,nPosQtd]
									   		MsgAlert("O Produto "+aCols[nX,nPosPrd]+" sofreu alteracao. O Pedido nao podera ser salvo!","Atencao") 
									   		l_Ret := .F.
									   	Endif
								   	Endif
							   	Else
							   		If Found()
								   		MsgAlert("O Produto "+aCols[nX,nPosPrd]+" sofreu alteracao. O Pedido nao podera ser salvo!","Atencao") 
								   		l_Ret := .F.
							   		Endif
								Endif
							EndIf
						Next nx
					Endif
				Endif
			EndIf				
		EndIf
	EndIf
	
	
If nOpc == 3 .Or. nOpc == 4  //2-Visualizacao, 3-Inclusao, 4-Alteracao
		If M->C5_TIPO = "N"
			If M->C5_XREFAT = 'S'
				For nx := 1 To Len(aCols)
				   	If aCols[nX][nPosAmz] <> cAmzReFat
				   		MsgAlert("Pedido de Re-Faturamento devem ter o armazem "+cAmzReFat,"Atencao") 
				   		l_Ret := .F.
					EndIf
				Next
			EndIf
		EndIf
	EndIf
	
//TODO Validacao para inclusao do CFOP [Weskley Silva 01/08/2018]	
If nOpc == 3 .Or. nOpc == 4  //2-Visualizacao, 3-Inclusao, 4-Alteracao
	For nx := 1 To Len(aCols)
	   	If Empty(aCols[nX][nCFOP])
	   		//MsgAlert("Produto sem CFOP, Favor preencher! ","Atencao") 
	   		l_Ret := .F.
		EndIf
	Next
	iF l_Ret = .F.
		MsgAlert("Produto sem CFOP, Favor preencher! ","Atencao") 
	ENDIF
EndIf

// TODO Valida��o para o Green [Weskley Silva 21/02/2019]
if nOpc == 3 .or. nOpc == 4 
	If SC5->C5_TIPO = "N"
	For nx := 1 to Len(aCols)
		cProd := (aCols[nX][nPosPrd]) 
		nQtdPrd:= (aCols[nX][nPosQtd])
		
		nProc:= 1
		lHcopNf:= .f.

		While !Empty( ProcName( nProc ) ) .and. !lHcopNf
			Conout( AllTrim( ProcName( nProc++ ) ) )
			If ProcName(nProc) == "HCOPNF"
				lHcopNf:= .t.
			Endif
		EndDo
		
		//Valida��o para identificar se a rotina foi chamada atrav�s do bot�o de c�pia de NF
		If lHcopNf
			aChkEstoq:= { .F., cProd, nQtdPrd, "", 0, .T. } 
		Else
			//Chama rotina que faz a troca do produto Nacional para Importada
			aChkEstoq:= fTradeNacImp(cProd, nQtdPrd)
		Endif
		
		// TODO N�O CHAMA A REGRA GREEN PARA CLIENTE CONTIDO NO PARAMETRO OU TIPO DE PEDIDO 016
		IF M->C5_CLIENTE $ GETMV("HP_CLIGRN") .OR. M->C5_TPPED $ GETMV("HP_TIPGRN") .OR. M->C5_TPPED = '097' .OR. M->C5_TPPED = '100'
			aChkEstoq := { .F., cProd, nQtdPrd, "", 0, .T. }  
		else
			aChkEstoq:= fTradeNacImp(cProd, nQtdPrd)
		endif
		 
		If lQbrPed:= aChkEstoq[1]
			cProd:= aChkEstoq[2]
			nQtdPrd:= aChkEstoq[3]
			cProd2:= aChkEstoq[4]
			nQtd2Prd:= aChkEstoq[5]
			
			lSit:= aChkEstoq[6]
			
			//cProd  := cProd2
			//nQtdPrd:= nQtd2Prd
			
			aCols[nX][nPosPrd] := aChkEstoq[2]
			aCols[nX][nPosQtd] := aChkEstoq[3]
		Else
			if lSit:= aChkEstoq[6]
			aCols[nX][nPosPrd] := aChkEstoq[2]
			nQtdPrd:= aChkEstoq[3]
			endif
		endif	
			
	
			nQtPr:= Iif(lQbrPed,2,1)
			
			for nXX := 1 to nQtPr
		
			if nXX > 1
			
				If Len(acols) >= 99 
					If Len(acols) >= 999
						_cItem := STRZERO(Len(acols), 4, 0)
					Else
						_cItem := STRZERO(Len(acols), 3, 0)
					Endif	
				Else
					_cItem := STRZERO(Len(acols[nx]), 2, 0)
				Endif

				If Empty(aCols[Len(acols),nPosPrd])
					//Caso seja inclusao e seja a primeira linha do acols em branco
					If Len(aCols[nx]) == 1
						_cItem := "00"
					Endif
					ADEL(aCols,1)
					ASIZE(aCols,Len(aCols)-1)
				EndIf

				AADD(aCols,Array(Len(aHeader)+1))

				For nn:=1 to Len(aHeader)
					If IsHeadRec(Alltrim(aHeader[nn,2]))
						aCols[Len(aCols)][nn] := 0
					ElseIf IsHeadAlias(Alltrim(aHeader[nn,2]))
						aCols[Len(aCols)][nn] := "SC6"
					Else
						aCols[Len(aCols)][nn] := CriaVar(Alltrim(aHeader[nn,2]),.F.)
					Endif
				Next nn
				
				
				aCOLS[Len(aCols)][Len(aHeader)+1] := .F.
			
				cProd  := cProd2
				nQtdPrd:= nQtd2Prd
				
				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+cProd)
				
				cOrigem := SB1->B1_ORIGEM
				
				_cItem := Soma1(aCols[nX,nPosItem])
				aCols[Len(aCols),1] := _cItem
				aCols[Len(acols),nPosPrd] := cProd
				aCols[Len(acols),nPosDes] := SB1->B1_DESC
				aCols[Len(acols),nPosUM]  := "UN"
				aCols[Len(acols),nPosQtd] := aChkEstoq[5]
				
				DbSelectArea("SZ1")
				DbSetOrder(1)
				DbSeek(xfilial("SZ1")+M->C5_TPPED)

				_oper := SZ1->Z1_TPOPER

				If alltrim(_oper) = ""
					_oper := "01"
				Endif

				DbSelectArea("DA1")
				DbSetOrder(1)
				DbSeek(xfilial("DA1")+M->C5_TABELA+cProd)

				cTES  := MaTesInt(2,_oper,M->C5_CLIENTE,M->C5_LOJAENT,"C",cProd,)
				
				DbSelectArea("SF4")
				DbSetOrder(1)
				DbSeek(xFilial("SF4")+cTES)
				
				cSitClass := cOrigem+SF4->F4_SITTRIB

				aCols[Len(acols),nPosPrc] 	:= DA1->DA1_PRCVEN
				aCols[Len(acols),nPosPUn] 	:= DA1->DA1_PRCVEN
				aCols[Len(acols),nPosVal] 	:= Round(DA1->DA1_PRCVEN*nQtdPrd,2)
				aCols[Len(acols),nPosOpe] 	:= _oper
				aCols[Len(acols),nPosTES] 	:= cTES
				aCols[Len(acols),nPosDtEnt] := M->C5_FECENT
				if M->C5_TPPED = '097' .OR. M->C5_TPPED = '100'
					aCols[Len(aCols),nLocal]    := GETMV("HP_ARMQL")
				ELSEIF M->C5_TPPED = '027'
					aCols[Len(aCols),nLocal]    := GETMV("HP_LOCREF")
					aCols[Len(aCols),nEndere]   := "DEV"
				ELSE	
					aCols[Len(aCols),nLocal]    := GETMV("HP_LOCPRIV")
				ENDIF
				aCols[Len(aCols),nSitClass]    := cSitClass

			endif
		//TODO Comentado devido o erro de variavel oGetDad inexistente - 15/03/2019 - Robson	
		//oGetDad:Refresh(.T.)	
		next nXX
	
	if lSit
		l_Ret := .T.
	else 
	MsgAlert(" Quantidade do "+cProd+" indisponivel no estoque! Saldo disponivel "+cValtochar(nSecQatu)+" "," Green HOPE ")
	l_Ret := .F.
	endif
			
next nx
	
	If lQbrPed
	SysRefresh()
	GETDREFRESH()
	//SetFocus(oGetDad:oBrowse:hWnd) // Atualizacao por linha
	oGetDad:Refresh(.T.)
	oGetDad:oBrowse:Refresh(.T.)
	A410LinOk(oGetDad)
	//a410Recalc()
	oGrade:aHeadGrade := {}
	endif
ENDIF	
endif

If nOpc == 4
//Grava informacoes do pedido anterior a alteracao 
  DbSelectArea("SC5")
  DbSetOrder(1)
  DbSeek(xfilial("SC5")+M->C5_NUM)
		
  If AllTrim(SC5->C5_XOBSINT) <> AllTrim(M->C5_XOBSINT)
	_xobs	  := "S"
  End
		 
 _xAltPed  := SC5->C5_CLIENTE+'/'+SC5->C5_POLCOM+'/'+SC5->C5_CONDPAG+'/'+SC5->C5_TABELA+'/'+SC5->C5_TPPED+'/'+_xobs
		
//RecLock("SC5",.F.)
//Replace C5_XALTPED	with _xAltPed
//MsUnLock()
		
	_qry1 := "UPDATE SC5010 SET C5_XALTPED='"+_xAltPed+"' WHERE D_E_L_E_T_ <> '*' AND C5_FILIAL ='"+xfilial("SC5")+"' AND C5_NUM ='"+M->C5_NUM+"' "
	TcSqlExec(_qry1)		
	EndIf
Endif	

// TODO Recalculo do pedido de venda [Weskley Silva 04.10.2018]
a410Recalc()
If l_Ret
	//Chama a rotina de validacao de Tipo de Pedido x Politica Comercia [Robson Melo 21.01.2019]
	l_Ret:= U_HAMAT001()
Endif
// In�cio. Limpeza da comiss�o para Tipos de Pedidos que n�o devem calcular comiss�o [Actual Trend 27.02.2019]


if nOpc == 3
	IF !(M->C5_TPPED = '097' .OR. M->C5_TPPED = '100')
		For nx := 1 To Len(aCols)
	   		cBlqDem:= POSICIONE("SB1",1,xFilial("SB1")+aCols[nX][nPosPrd],"B1_YBLQDEM")
	   		lBlqDem:= Iif(Alltrim(cBlqDem)== "S",.T.,.F.) 
	   		If lBlqDem
	   			l_Ret := .F.
	   			MsgAlert("Produto "+aCols[nX][nPosPrd]+" com bloqueio de demanda! ","Atencao") 
			EndIf
		Next
	endif
ENDIF


/*If l_Ret
	If M->C5_TIPO = "N"
		DbSelectArea("SZ1")
		DbSetOrder(1)
		DbSeek(xfilial("SZ1")+M->C5_TPPED)
		
		If found() .and. SZ1->Z1_GERACOM = "N"
			C5_COMIS1 	:= 0
			C5_COMIS2	:= 0
			C5_COMIS3	:= 0
			C5_COMIS4	:= 0
			C5_COMIS5	:= 0

			For nX:= 1 to len(acols)
				If acols[nX][len(acols[nX])] = .F.
					aCols[nX,nCom1] := 0
					aCols[nX,nCom2] := 0
					aCols[nX,nCom3] := 0
					aCols[nX,nCom4] := 0
					aCols[nX,nCom5] := 0
				Endif
			Next
		Endif
	Endif
Endif
// Fim [Actual Trend 27.02.2019]
*/


RestArea(a_SC9)
RestArea(a_SC6)
RestArea(a_SC5)
RestArea(_aAreaOLD)

Return(l_Ret)

user function buscaCond()

Local oDlgSu9														// Tela
Local nPosLbx  := 0                                                 // Posicao do List
Local nPos     := 0                                                 // Posicao no array
Local cAssunto := ""                                                // Descricao do Assunto
Local lRet     := .F.                                               // Retorno da funcao
Local cFil	   := Space(15)
Local oFiltro
Private oLbx1  
Private aItems   := {}                                                // Array com os itens

CursorWait()

u_XCond()

CursorArrow()
	     
If Len(aItems) <= 0
	Alert("Condicao nao encontrada")
   Return(lRet)
Endif	
	
DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Condicao de Pagamento" PIXEL  

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
			"Descric�o","C�digo";	//"Descricao"
			SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo" 
	oLbx1:SetArray(aItems)
    oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
					 aItems[oLbx1:nAt,2]}}

    oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
//    DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (u_cadsB1("V",aItems[oLbx1:nAt][2]))
    //@ 88, 005 MSGET oTipo VAR CFIL SIZE 035, 010 OF oDlgSu9 VALID u_XCond() COLORS 0, 16777215 PIXEL
	@ 88, 005 SAY oSay43 PROMPT "Filtro" SIZE 055, 007 OF oDlgSu9 COLORS 0, 16777215 PIXEL
    @ 88, 025 MSGET oFiltro VAR CFIL SIZE 100, 010 OF oDlgSu9 VALID u_XCond(cFil) COLORS 0, 16777215 PIXEL
    DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
    DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

ACTIVATE MSDIALOG oDlgSu9 CENTERED

If lRet
   DbSelectarea("SE4")
   DbSetorder(1)
   DbSeek(xFilial("SE4")+aItems[nPos][2])
Endif

Return(lRet) 

User Function XCond(_Filtro)

	aItems := {}
	If __cUserId $ GetMV("MV_XUSRCOM")
		_qry := "Select E4_CODIGO as CODIGO, E4_DESCRI as DESCRICAO from "+RetSqlName("SE4")+" SE4 "
		_qry += "where SE4.D_E_L_E_T_ = '' "
		If alltrim(_filtro) <> ""
			_qry += "and (E4_CODIGO like '%"+alltrim(_filtro)+"%' or E4_DESCRI like '%"+alltrim(_filtro)+"%') "
		Endif
		_qry += "Order By E4_CODIGO " 
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	Else
		_qry := "Select E4_CODIGO as CODIGO, E4_DESCRI as DESCRICAO from "+RetSqlName("SE4")+" SE4 "
		_qry += "inner join "+RetSqlName("SZ3")+" SZ3 on SZ3.D_E_L_E_T_ = '' and Z3_COND = E4_CODIGO "
		_qry += "where SE4.D_E_L_E_T_ = '' and Z3_POLITIC = '"+M->C5_POLCOM+"' "
		If alltrim(_filtro) <> ""
			_qry += "and (E4_CODIGO like '%"+alltrim(_filtro)+"%' or E4_DESCRI like '%"+alltrim(_filtro)+"%') "
		Endif
		_qry += "Order By E4_CODIGO " 
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	Endif
	
	DbSelectArea("XXX")
	DbGoTop()
	
	If !EOF()
		While !Eof() 
			Aadd(aItems,{XXX->DESCRICAO,XXX->CODIGO})
			DbSkip()
		End
	Else
		Aadd(aItems,{"   ","   "})
	Endif       
	DbSelectArea("XXX")
	DbCloseArea()

If alltrim(_filtro) <> ""
	oLbx1:aArray := aItems
	oLbx1:Refresh()
Endif

Return

user function buscTab()

Local oDlgSu9														// Tela
Local nPosLbx  := 0                                                 // Posicao do List
Local nPos     := 0                                                 // Posicao no array
Local cAssunto := ""                                                // Descricao do Assunto
Local cFil	   := Space(15)
Local lRet     := .F.                                               // Retorno da funcao
Private oLbx1  
Private aItems   := {}                                                // Array com os itens

CursorWait()

u_xtab()

CursorArrow()
	     
If Len(aItems) <= 0
	Alert("Tabela n�o encontrada")
   Return(lRet)
Endif	
	
DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Tabela de Pre�os" PIXEL  

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
			"Descri��o","C�digo";	//"Descricao"
			SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo" 
	oLbx1:SetArray(aItems)
    oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
					 aItems[oLbx1:nAt,2]}}

    oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
//    DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (u_cadsB1("V",aItems[oLbx1:nAt][2]))
	@ 88, 005 SAY oSay43 PROMPT "Filtro" SIZE 055, 007 OF oDlgSu9 COLORS 0, 16777215 PIXEL
    @ 88, 025 MSGET oFiltro VAR CFIL SIZE 100, 010 OF oDlgSu9 VALID u_XTab(cFil) COLORS 0, 16777215 PIXEL

    DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
    DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

ACTIVATE MSDIALOG oDlgSu9 CENTERED

If lRet
   DbSelectarea("DA0")
   DbSetorder(1)
   DbSeek(xFilial("DA0")+aItems[nPos][2])
Endif

Return(lRet) 

User Function xTab(_Filtro)

	If alltrim(M->C5_POLCOM)<>""
		cPolitica	:= M->C5_POLCOM
	Else
		cPolitica	:= SC5->C5_POLCOM
	End
	
	aItems := {} 
	If __cUserId $ GetMV("MV_XUSRCOM")
		_qry := "Select DA0_CODTAB as CODIGO, DA0_DESCRI as DESCRICAO from "+RetSqlName("DA0")+" DA0 "
		_qry += "where DA0.D_E_L_E_T_ = '' "
		If alltrim(_filtro) <> ""
			_qry += "and (DA0_CODTAB like '%"+alltrim(_filtro)+"%' or DA0_DESCRI like '%"+alltrim(_filtro)+"%') "
		Endif

		_qry += "Order By DA0_CODTAB " 
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	Else
		_qry := "Select DA0_CODTAB as CODIGO, DA0_DESCRI as DESCRICAO from "+RetSqlName("DA0")+" DA0 " 
		_qry += "inner join "+RetSqlName("SZ5")+" SZ5 on SZ5.D_E_L_E_T_ = '' and Z5_TABELA = DA0_CODTAB "
		_qry += "where DA0.D_E_L_E_T_ = '' and Z5_POLITIC = '"+cPolitica+"' " 
		If alltrim(_filtro) <> ""
			_qry += "and (DA0_CODTAB like '%"+alltrim(_filtro)+"%' or DA0_DESCRI like '%"+alltrim(_filtro)+"%') "
		Endif

		_qry += "Order By DA0_CODTAB "
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	Endif
	
	DbSelectArea("XXX")
	DbGoTop()

	If !EOF()
		While !Eof() 
			Aadd(aItems,{XXX->DESCRICAO,XXX->CODIGO})
			DbSkip()
		End
	Else
		Aadd(aItems,{XXX->DESCRICAO,XXX->CODIGO})
	Endif       
	DbSelectArea("XXX")
	DbCloseArea()

If alltrim(_filtro) <> ""
	oLbx1:aArray := aItems
	oLbx1:Refresh()
Endif

Return


User Function bTam1(cProduto,cCor) //Funcao Busca Tamanho

	Local nX, _i
	Local aTamanhos := {}
	Local aAlterFields := {}

	If Select("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf

	cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM "+RetSQLName("SBV")+" SBV "
	cQuery  += CRLF + "WHERE D_E_L_E_T_ = '' "
	cQuery  += CRLF + "	AND BV_TABELA = (SELECT B4_COLUNA FROM "+RetSQLName("SB4")+" SB4 WHERE D_E_L_E_T_<>'*' AND B4_COD='"+cProduto+"') "
	cQuery  += CRLF + "ORDER BY R_E_C_N_O_"

	MemoWrite("MT410TOK_SBV.txt",cQuery)

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
	  
	DbSelectArea("TMPSBV")

	cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" SZD WHERE D_E_L_E_T_ = ' ' AND ZD_FILIAL = '"+xFilial("SZD")+"' AND ZD_COR ='"+cCor+"' AND ZD_PRODUTO='"+cProduto+"'"
	
	MemoWrite("MT410TOK_SZD.txt",cQuery2)
	
	If Select("TMPSZD") > 0
		TMPSZD->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)

	DbSelectArea("TMPSZD")

	If TMPSZD->(!EOF())
		While TMPSBV->(!EOF())		
			For _i := 1  to 200
				_campo		:= "ZH_TAM"+StrZero(_i,3)
				//cZdVerif	:= 0
		
				//TMPSZD->(DbGoTop())
		
				//While TMPSZD->(!EOF())
				//If TMPSZD->(!EOF())
					If &("TMPSZD->ZD_TAM"+StrZero(_i,3))=="X"
						//cZdVerif += 1
						DbSelectArea("SX3")
						SX3->(DbSetOrder(2))
						If SX3->(DbSeek(_campo)) //.and. cZdVerif==1
							//Aadd(aTamanhos, {alltrim(TMPSBV->BV_CHAVE),SX3->X3_CAMPO,"@!",12,0,"",SX3->X3_USADO,"C","","R","",""}[1])
							Aadd(aTamanhos,{alltrim(TMPSBV->BV_CHAVE),_i})
						Endif
//					Else
//						Aadd(aTamanhos,{"",_i})
					Endif
			 		
					//TMPSZD->(DbSkip())
				//EndIf	
				//EndDo
		  	
				//DbSelectArea("TMPSBV")
				
				TMPSBV->(DbSkip())
				
			Next _i
		EndDo
	EndIf

	TMPSZD->(DbClosearea())
	TMPSBV->(DbCloseArea())
	
Return (aTamanhos)

Static Function fTradeNacImp(cProd, nQtdPrd)
	Local cPrd1   := cProd
	Local nQtdOrig   := nQtdPrd
	Local cPrd2   := ""
	Local nQtd2   := 0
	Local aRet    := {}
	Local lBlqDem := .f.
	Local cPV	  := ""
	Local lConEst := .t. //Consulta Estoque
	Local aAreaSB1:= SB1->(GetArea())
	Local aAreaSB2:= SB2->(GetArea())
	Local aAreaZB5:= ZB5->(GetArea())
	Local cCodSeq := ''
	Local cCodPrio:= ''
	Local cPriori := ''
	Local cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
	Local cAmzPul := AllTrim(SuperGetMV("MV_XAMZPUL",.F.,"E1")) //Armazem padrao pulmao

	dbSelectArea("ZB5")
	dbSetOrder(1)
	ZB5->(dbGoTop())
	//Verifica se existe priorizacao de produtos, caso nao exista o pedido segue igual.
	If ZB5->(DbSeek(xFilial("ZB5")+cPrd1))
		cPriori:= ZB5->ZB5_PROIRI
		cCodPrio:= Iif(cPriori=="N",ZB5->ZB5_PRDNAC,ZB5->ZB5_PRDIMP)
		//Inverte os codigos para saber qual e o produto secundario para ser contado estoque caso falte no prd1
		cCodSeq := Iif(cPriori=="N",ZB5->ZB5_PRDIMP,ZB5->ZB5_PRDNAC)
	Else
		dbSelectArea("ZB5")
		dbSetOrder(2)//ZB5_FILIAL + ZB5_PRDIMP
		ZB5->(dbGoTop())
		If ZB5->(DbSeek(xFilial("ZB5")+cPrd1))
			cPriori:= ZB5->ZB5_PROIRI
			cCodPrio:= Iif(cPriori=="N",ZB5->ZB5_PRDNAC,ZB5->ZB5_PRDIMP)
			//Inverte os codigos para saber qual e o produto secundario para ser contado estoque caso falte no prd1
			cCodSeq := Iif(cPriori=="N",ZB5->ZB5_PRDIMP,ZB5->ZB5_PRDNAC)
			//Retorna pedido original pois nao existe priorizacao cadastrada na ZB5
		Else
			aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2 , .t.}
			RestArea(aAreaSB1)
			RestArea(aAreaSB2)
			Return (aRet) 
		Endif
	Endif

	//Verifica aqui o Bloqueio por Demanda
	cBlqDem:= POSICIONE("SB1",1,xFilial("SB1")+cCodPrio,"B1_YBLQDEM")
	lBlqDem:= Iif(Alltrim(cBlqDem)== "S",.T.,.F.) 

	If !lBlqDem
		//Verifica aqui a Situacao do Produto onde:
		//PV|PNV - Nao consulta estoque, venda liberada
		//NPV|PVR|NPVO - Consulta estoque, venda so acontece com disponibilidade do produto
		 
		cPVSec:= POSICIONE("SB1",1,xFilial("SB1")+cCodSeq,"B1_YSITUAC")
		cPV:= POSICIONE("SB1",1,xFilial("SB1")+cCodPrio,"B1_YSITUAC")
		
		lConEst:= Iif(Alltrim(cPV)== "PV" .OR. Alltrim(cPV) == "PNV" .or. Alltrim(cPV) == "PVFO"  ,.F.,.T.)

		If lConEst
			dbSelectArea("SB2")
			dbSetOrder(1)
			SB2->(dbGoTop())

			nSldPik := HFASALDO(cCodPrio,cAmzPik)
			nSldPul := HFASALDO(cCodPrio,cAmzPul)

			nSldSecPik := HFASALDO(cCodSeq,cAmzPik)
			nSldSecPul := HFASALDO(cCodSeq,cAmzPul)

			If nQtdOrig<= nSldPik+nSldPul
				aRet:= { .F., cCodPrio, nQtdOrig, cPrd2, nQtd2 ,.T.}
			Else
				nPriQatu:= nSldPik + nSldPul
				nSecQatu:= nSldSecPik + nSldSecPul
				nSldAtu := nQtdOrig - nPriQatu
				
			IF Alltrim(cPVSec) == "PV" .OR. Alltrim(cPVSec) == "PNV" .OR. Alltrim(cPVSec) == "PVFO"
				aRet:= { .T., cCodPrio, nPriQatu, cCodSeq, nSldAtu, .T. }
			ELSE			  	
				If nSecQatu <= nSldAtu  
					//nQtd2:= nSecQatu - nSldAtu
					aRet:= { .T., cCodPrio, nPriQatu, cCodSeq, nSldAtu, .F. }
				Else
					cPvSeq := POSICIONE("SB1",1,xFilial("SB1")+cCodSeq,"B1_YSITUAC") 
					if Alltrim(cPvSeq) == "PV" .OR.  Alltrim(cPvSeq) == "PNV" .OR. Alltrim(cPvSeq) == "PVFO" 
						aRet:= { .T., cCodPrio, nPriQatu, cCodSeq, nSldAtu, .T. } 
					else	 
						aRet:= { .F., cCodPrio, nPriQatu, cCodSeq, nSldAtu, .T. } 
					endif	
					RestArea(aAreaSB1)
					RestArea(aAreaSB2)
					RestArea(aAreaZB5)
					Return(aRet)
					//Produtos (Nac+Imp) juntos nao contem a quantidade total necessaria do pedido.
				Endif	
			ENDIF	
			Endif
		Else
			//Retornar aqui quantidade original + produto priorizado 
			//if !empty(cCodSeq)
			//	aRet:= { .T., cCodPrio, nPriQatu, cCodSeq, nSldAtu, .T. }
			//else
				aRet:= { .F., cCodPrio, nQtdOrig, cPrd2, nQtd2 ,.T.}
			//endif
		Endif
	Else
		Conout('Produto: '+cCodPrio+' com bloqueio de demanda.')
		MsgAlert("Produto "+cCodPrio+" com bloqueio de demanda","GREEN")
		lRet := .F.
		aRet:= { .F., cPrd1, nQtdOrig, cPrd2, nQtd2, .T. } 
		//Retornar aqui pedido original - o pedido nao podera ser feito por conta do bloqueio por demanda
	Endif

	RestArea(aAreaSB1)
	RestArea(aAreaSB2)
	RestArea(aAreaZB5)

Return (aRet) 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATSALDO �Autor  �R.Melo - Totalit � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Busca saldo na SB2  										 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function HFASALDO(cProduto,cArmazem)
	Local nRet := 0

	DbSelectArea("SB2")
	DbSetOrder(1)
	If SB2->(DbSeek(xFilial("SB2")+cProduto+cArmazem))
		nRet := SB2->B2_QATU-SB2->B2_RESERVA-SB2->B2_QEMP-SB2->B2_XRESERV //SaldoSb2()
	EndIf

Return nRet