/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �M410ALOK  �Autor  �Bruno Parreira      � Data �  09/02/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Valida a alteracao do pedido de venda                       ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/

User Function M410ALOK()

Local _aAreaOLD := GetArea()
Local a_SC9		:= SC9->(GetArea())
Local a_SC6		:= SC6->(GetArea())
Local a_SC5		:= SC5->(GetArea())
Local l_Ret     := .T.
Local cCodUsr   := AllTrim(SuperGetMV("MV_XUSRALT",.F.,"000000")) //Usuarios que poderao alterar pedido independente de ja ter gerado DAP.                                                    
Local lVer      := .F.

If SC5->C5_TIPO = "N"
	DbSelectArea("SZJ")
	DbSetOrder(2) 
	If DbSeek(xFilial("SZJ")+SC5->C5_NUM)
		While SZJ->(!EOF()) .And. SZJ->ZJ_PEDIDO = SC5->C5_NUM
			If Empty(SZJ->ZJ_DOC)
				lVer := .T.
			EndIf	
			SZJ->(DbSkip())
		EndDo
		If lVer
			If !(__cUserId $ cCodUsr)
				//MsgAlert("Esse pedido possui DAP gerado. N�o poder� ser alterado!", "A T E N � � O")
				If !MsgYesNo("Este pedido j� foi liberado pelo DAP. Confirma Altera��o?"+chr(13)+"N�o poder�o ser alterados produtos, quantidades e clientes!","A T E N � � O")
					l_Ret := .F.
				Endif
			Else
				If !MsgYesNo("Este pedido j� foi liberado pelo DAP. Confirma Altera��o?","A T E N � � O")
					l_Ret := .F.
				EndIf
			EndIf				
		EndIf			
	EndIf
EndIf
RestArea(a_SC9)
RestArea(a_SC6)
RestArea(a_SC5)
RestArea(_aAreaOLD)

Return l_Ret