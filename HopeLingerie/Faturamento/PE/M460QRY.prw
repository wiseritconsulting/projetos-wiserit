#include "rwmake.ch" 
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"


User Function M460QRY()

Local cQuery  := paramixb[1] 
_area         := GetArea()

aadd(aRotina,{"Vis.Pedido","U_HVISPED", 0 , 2,0,.F.})

//AJUSTE DO STATUS DE PEDIDO
//_qry := "update "+RetSqlName("SC9")+" set C9_XBLQ = C5_XBLQ from "+RetSqlName("SC9")+" SC9 "
//_qry += "inner join "+RetSqlName("SC5")+" SC5 on SC5.D_E_L_E_T_ = '' and C5_NUM = C9_PEDIDO "
//_qry += "where SC9.D_E_L_E_T_ = '' and C9_NFISCAL = '' and C5_XBLQ <> C9_XBLQ "
//_qry += "and C9_PEDIDO >= '"+MV_PAR05+"' and C9_PEDIDO <= '"+MV_PAR06+"' "

//TcSqlExec(_qry)

cQuery   += " AND ( SELECT " 
cQuery   += " COUNT(C5_NUM) AS C5_COUNT "
cQuery   += "    FROM " 
cQuery   += "  "+RetSqlName("SC5")+" SC5  " 
cQuery   += "   WHERE " 
cQuery   += "  SC5.C5_FILIAL = SC9.C9_FILIAL "
cQuery   += "  AND SC5.C5_NUM = SC9.C9_PEDIDO " 
cQuery   += "  AND SC5.C5_XBLQ = 'L' "
cQuery   += "  AND SC5.D_E_L_E_T_ = ' ') > 0 "
cQuery   += "  AND SC9.D_E_L_E_T_ = '' " 

RestArea(_area)

Return(cQuery)

/*User Function M460FIL()

Local cQuery := paramixb[1]

_area    := GetArea()

/*
_qry := "update "+RetSqlName("SC9")+" set C9_XBLQ = C5_XBLQ from "+RetSqlName("SC9")+" SC9 "
_qry += "inner join "+RetSqlName("SC5")+" SC5 on SC5.D_E_L_E_T_ = '' and C5_NUM = C9_PEDIDO "
_qry += "where SC9.D_E_L_E_T_ = '' and C9_NFISCAL = '' and C5_XBLQ <> C9_XBLQ "

//TcSqlExec(_qry)
*/

//cQuery   := " C9_XBLQ = 'L' "


/*cQuery   += " AND ( SELECT " 
cQuery   += " COUNT(C5_NUM) AS C5_COUNT "
cQuery   += "    FROM " 
cQuery   += "  "+RetSqlName("SC5")+" SC5  " 
cQuery   += "   WHERE " 
cQuery   += "  SC5.C5_FILIAL = SC9.C9_FILIAL "
cQuery   += "  AND SC5.C5_NUM = SC9.C9_PEDIDO " 
cQuery   += "  AND SC5.C5_XBLQ = 'L' "
cQuery   += "  AND SC5.D_E_L_E_T_ = ' ') > 0 "
cQuery   += "  AND SC9.D_E_L_E_T_ = '' " 
*/

//RestArea(_area)

//Return(cQuery)


User Function HVISPED()

	Private Inclui    := .F.
	Private Altera    := .F.
	Private nOpca     := 1   
	Private cCadastro := "Pedido de Vendas"  
	Private aRotina   := {}

	_area:= GetArea()
		        
	DbSelectArea("SC5")
	DbSetOrder(1)
	DbSeek(xfilial("SC5")+SC9->C9_PEDIDO)

	Altera   := .F.
	Inclui   := .F.
    MatA410(Nil, Nil, Nil, Nil, "A410Visual")

	RestArea(_area)
Return