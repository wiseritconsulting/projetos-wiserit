#include "rwmake.ch" 
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"


User Function M460MARK()


Private oFontN    := TFont():New("Arial",,20,,.t.,,,,,.f.)
Private oFont2    := TFont():New("Arial",,12,,.t.,,,,,.f.)
lret := .T.
_nPedido         	:= SC9->C9_PEDIDO
_nFilial         	:= SC9->C9_FILIAL
_cQtdNF				:= QtdPed(_nFilial,_nPedido,1) 
_cVlrNF				:= QtdPed(_nFilial,_nPedido,2)
_cQtdCF				:= QtdPed(_nFilial,_nPedido,3)

@ 156,367 To 400,934 Dialog mkwdlg Title OemToAnsi("Confer�ncia da Quantidade do Pedido")
n_linha	:= 015
n_latu		:= 013

@ n_latu  ,018	Say OemToAnsi("Pedido:") Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
@ n_latu  ,063	say SC9->C9_PEDIDO Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
n_latu +=  n_linha

@ n_latu  ,018	Say OemToAnsi("Cliente:") Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
@ n_latu  ,063	Say SC9->C9_CLIENTE Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
n_latu +=  n_linha //+010

DbSelectArea("SA1")
DbSetOrder(1)
DbSeek(xfilial("SA1")+SC9->C9_CLIENTE+SC9->C9_LOJA)

@ n_latu  ,018	Say OemToAnsi("Nome:") Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
@ n_latu  ,063	Say SA1->A1_NOME Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
n_latu +=  n_linha //+010

@ n_latu+8,018	Say OemToAnsi("Total de Itens Liberados: "+ cValToChar(_cQtdNF) +" pe�as") Size 200,120 COLOR CLR_BLACK FONT oFontN PIXEL OF mkwdlg
n_latu +=  n_linha+010

@ n_latu+8,018	Say OemToAnsi("Vr Total de Itens Liberados: R$ "+ cValToChar(_cVlrNF) +" ") Size 200,120 COLOR CLR_BLACK FONT oFontN PIXEL OF mkwdlg
n_latu +=  n_linha+010

@ n_latu,035 BUTTON oButton2  PROMPT "_Cancelar"    SIZE 036, 016 OF mkwdlg ACTION (lRet:=.F.,mkwdlg:End()) PIXEL MESSAGE "Cancelar"
@ n_latu,075 BUTTON oButton2  PROMPT "C_ontinuar"    SIZE 036, 016 OF mkwdlg ACTION (lRet:=.T.,mkwdlg:End()) PIXEL MESSAGE "Continuar"

Activate Dialog mkwdlg CENTERED 

IF !EMPTY(SC9->C9_XNUMPF)
If _cQtdNF <> _cQtdCF
	MsgAlert("Este pedido possui diferen�a entre as quantidades aptas a faturar e a confer�ncia do DAP. O mesmo n�o poder� ser faturado!","Aten��o")	
	lRet := .F.
Endif
ENDIF

Return(lRet)

Static Function QtdPed(nFilial,nPedido,_tp)

	If _tp = 1
		_qry := " SELECT ISNULL(SUM(C9_QTDLIB),0) AS QTDNF FROM "+RetSQLName("SC9")+" SC9 (NOLOCK) "
		_qry += " WHERE D_E_L_E_T_<>'*' AND C9_FILIAL='"+nFilial+"' AND C9_PEDIDO='"+nPedido+"' and C9_NFISCAL = '' and C9_XBLQ = 'L' and C9_BLEST = '' "
	ElseIf _tp = 2
		_qry := " SELECT ISNULL(SUM(C9_QTDLIB*C9_PRCVEN),0) AS QTDNF FROM "+RetSQLName("SC9")+" SC9 (NOLOCK) "
		_qry += " WHERE D_E_L_E_T_<>'*' AND C9_FILIAL='"+nFilial+"' AND C9_PEDIDO='"+nPedido+"' and C9_NFISCAL = '' and C9_XBLQ = 'L' and C9_BLEST = '' "
	Else
		_qry := " Select Sum(ZJ_QTDSEP) as QTDNF from "+RetSQLName("SZJ")+" SZJ (NOLOCK) where SZJ.D_E_L_E_T_ = '' and ZJ_CONF = 'S' and ZJ_DOC = '' and ZJ_NUMPF in ( "
		_qry += " SELECT C9_XNUMPF FROM "+RetSQLName("SC9")+" SC9 (NOLOCK) "
		_qry += " WHERE D_E_L_E_T_<>'*' AND C9_FILIAL='"+nFilial+"' AND C9_PEDIDO='"+nPedido+"' and C9_NFISCAL = '' and C9_XBLQ = 'L' "
		_qry += " group by C9_XNUMPF) "
	Endif

	If Select("TMPSFT") > 0
		TMPSFT->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSFT",.T.,.T.)
	DbSelectArea("TMPSFT")
	DbGoTop()
	
	cQtdNF	:= TMPSFT->QTDNF

Return(cQtdNF)
