#include "protheus.ch"
#include "topconn.ch"
#include "tbiconn.ch"
#include "shell.ch"
#Include "RwMake.ch"
#include "rwmake.ch"
#include "ap5mail.ch"
#include "xmlxfun.ch"       
#include "TOTVS.CH"
#include 'parmtype.ch'

user function HPDEV001()  
Local aLegenda 		:= {{"ZAX_STATUS == '01'","BR_VERMELHO" },;
						{"ZAX_STATUS == '02'","BR_CINZA" 	},;
						{"ZAX_STATUS == '03'","BR_AMARELO"	},;
						{"ZAX_STATUS == '04'","BR_AZUL"		},;
						{"ZAX_STATUS == '05'","BR_VERDE"	},;
						{"ZAX_STATUS == '06'","BR_PRETO"	},;
						{"ZAX_STATUS == '07'","BR_CANCEL"	}}
						  
	Private cCadastro := "Registros de Devolu��es De Produtos Acabado (PA) "
	Private aRotina := {}
	Private oCliente
	Private oTotal
	Private cCliente := ""
	Private nTotal := 0
	Private bCampo := {|nField| FieldName(nField) }
	Private aSize := {}
	Private aInfo := {}
	Private aObj := {}
	Private aPObj := {}
	Private aPGet := {}
    Private cGet1 := Space(25)
    Private cGet2 := Space(25)
    Private  aUserFl := {}
    Private IdUsuario  := ""
    Private NomeUsuario  := ""
    Private EmailUsuario := ""
    Private GrupoUsuario := ""
    Private cIdSup   := ""
    Private cccSup   := "" 
    Private aRetSup   := {}
    Private IdSuperior  := ""
    Private NomeSuperior := ""
    Private EmailSuperior := ""
    
    PswOrder(2)
    lAchou   := PSWSeek(Substr(cUsuario,7,15))
    aUserFl   := PswRet(1)
    IdUsuario  := aUserFl[1][1]     // codigo do usuario
    NomeUsuario  := aUserFl[1][4]     // nome do usuario
    EmailUsuario := aUserFl[1][14]     // Email
    PswOrder(1)
    cccSup   := PswSeek(cIdSup,.t.)
    aRetSup   := PswRet(1)
    IdSuperior  := aRetSup[1][1]   // codigo do Superior
    NomeSuperior := aRetSup[1][4]   // nome do Superior
    EmailSuperior := aRetSup[1][14]   // Email do Superior


	// Retorna a �rea �til das janelas Protheus
	aSize := MsAdvSize()
	// Ser� utilizado tr�s �reas na janela
	// 1� - Enchoice, sendo 80 pontos pixel
	// 2� - MsGetDados, o que sobrar em pontos pixel � para este objeto
	// 3� - Rodap� que � a pr�pria janela, sendo 15 pontos pixel
	AADD( aObj, { 100, 080, .T., .F. })
	AADD( aObj, { 100, 100, .T., .T. })
	AADD( aObj, { 100, 015, .T., .F. })
	// C�lculo autom�tico da dimens�es dos objetos (altura/largura) em pixel
	aInfo := { aSize[1], aSize[2], aSize[3], aSize[4], 3, 3 }
	aPObj := MsObjSize( aInfo, aObj )
	// C�lculo autom�tico de dimens�es dos objetos MSGET
	aPGet := MsObjGetPos( (aSize[3] - aSize[1]), 315, { {004, 024, 240, 270} } )
	AADD( aRotina, {"Pesquisar"  ,"AxPesqui" ,0,1})
	AADD( aRotina, {"Visualizar" ,'U_Mod3Mnt',0,2})
	AADD( aRotina, {"Incluir"    ,'U_Mod3Inc',0,3})
    AADD( aRotina, {"Alterar"    ,'U_Mod3Mnt',0,4})
 	AADD( aRotina, {"Liberar"    ,'U_Mod3Lib',0,5})
 	AADD( aRotina, {"Relat�rio"  ,'U_Mod3Rel',0,5})
	AADD( aRotina, {"Atualizar"  ,'U_Mod3Atu',0,3})
	AADD( aRotina, {"Legenda"	 ,'U_DEV01LEG',0,5}) 
	
	dbSelectArea("ZAX")
	dbSetOrder(1)
	dbGoTop()
//	MBrowse(,,,,"ZAX")
	mBrowse(6,1,22,75,"ZAX",,,,,,aLegenda)

return


User Function DEV01LEG()

BrwLegenda("Devolu��es de Vendas Recebidas","Legenda",{{"BR_VERMELHO","N�o Processado"	},;
										{"BR_CINZA"	     ,"Em Processo Fiscal    " },;
										{"BR_AMARELO"	 ,"Em Processo Qualidade " },;
										{"BR_AZUL"		 ,"Em Processo Comercial " },;
								        {"BR_VERDE"		 ,"Em Processo Financeiro" },;
								        {"BR_PRETO"      ,"Cr�dito Concedido     " },;
								        {"BR_CANCEL"     ,"Reprovado             " }} )
Return
User Function Mod3Atu()

IF MsgYesno("Confirma Atualiza��o das Devolu��es? " )
	Processa({|| u_ATUAZAX()})
Endif
Return

USER FUNCTION ATUAZAX()
Local aSE1      := {}
Local aCrateio  := {}
Local aErros    := {}
Local lSim      := .T.
Local nlin      := 0
Local aRet      := {} //
Local cTexto    := ""
Local cxTexto   := ""
Local NPARC     := 0
Local aArea  := Getarea()
Local aRegs  := {}
Local cPerg	 := "HPFINDEV1A"
Local cMensa := "" 
Local cPedido:= "" 
Local dData  := ctod(space(08))
Local cQuery := ''
PRIVATE lMsHelpAuto := .F.
PRIVATE lMsErroAuto := .F.

AjustaSX1(cPerg,1)

If !Pergunte(cPerg,.T.)
    Return
EndIf
    
cQuery := " SELECT F1_DTDIGIT AS DATA ,F1_VOLUME1, E1_PREFIXO AS PREFIXO , E1_EMISSAO AS EMISSAO ,E1_NUM AS NOTA, "   
cQuery += " E1_CLIENTE AS CODCLI, E1_LOJA    AS LOJA,  E1_NOMCLI  AS NOMCLI ,F1_FRETE , F1_TRANSP, E1_VALOR, F1_TRANSP, F1_SERIE "
cQuery += " FROM SE1010 E1 WITH (NOLOCK), SF1010 F1  WITH (NOLOCK) " 
cQuery += " WHERE E1_TIPO = 'NCC' AND E1_EMISSAO  BETWEEN '"+DtoS(MV_PAR05)+"' AND '"+DtoS(MV_PAR06)+"' AND E1_CLIENTE  BETWEEN '"+MV_PAR01+"' "  
cQuery += "      AND '"+MV_PAR02+"' AND E1_NUM BETWEEN '"+MV_PAR03+"'  AND '"+MV_PAR04+"' AND E1.D_E_L_E_T_ = '' "
cQuery += "      AND F1_DOC = E1_NUM AND F1_SERIE = E1_PREFIXO AND F1_TIPO = 'D'   AND F1_FORNECE = E1_CLIENTE AND F1_LOJA = E1_LOJA AND  F1.D_E_L_E_T_ = ''"   
cQuery += "      ORDER BY F1_DTDIGIT+E1_NUM ""

IF Select("XTRB") > 0
   XTRB->(dbCloseArea())
Endif      
TCQUERY cQuery NEW ALIAS xTRB 
XTRB->(DBGOTOP())
While XTRB->(!EOF())  
  DbSelectArea('SA1')
  SA1->(Dbgotop())
  SA1->(DbSetOrder(1)) 
  IF SA1->(DbSeek(xFilial("SA1")+XTRB->CODCLI+xTRB->LOJA,.T.))
     cCodMil := SA1->A1_XCOD //Alltrim(Posicione("SA1",1,xFilial("SA1")+Substr(xTRB->CODCLI,1,6)+xTRB->LOJA,"A1_XCOD"))
     cCgc    := SA1->A1_CGC  //Alltrim(Posicione("SA1",1,xFilial("SA1")+Substr(xTRB->CODCLI,1,6)+xTRB->LOJA,"A1_CGC"))
     cNome   := SA1->A1_NOME
     cUf     := SA1->A1_EST
     cMregiao:= SA1->A1_XMICRDE
     cRepres := SA1->A1_VEND    
  ELSE
      cCodMil := "N/Encontrato"
      cCgc    := ""
  ENDIF
  dbSelectArea("ZAX")
  ZAX->(DbSetOrder(1))  //ZAX_FILIAL, ZAX_NFENTR, ZAX_SERIE, ZAX_CODCLI, ZAX_LOJCLI, R_E_C_N_O_, D_E_L_E_T_
  ZAX->(Dbgotop())
  If !ZAX->(DbSeek(xFilial("ZAX")+padr(xTRB->NOTA,9)+padr(xTRB->PREFIXO,3)+padr(xTRB->CODCLI,6)+padr(xTRB->LOJA,4)))
     //ZAX->(Reclock("ZAX",.F.))
  //ELSE
     ZAX->(Reclock("ZAX",.T.))
     ZAX->ZAX_FILIAL := xFilial("ZAX") 
  //Endif
  ZAX->ZAX_CLINFE := cNome
  ZAX->ZAX_DTAREG := CTOD(SUBSTR(xTRB->DATA,7,2)+'/'+SUBSTR(xTRB->DATA,5,2)+'/'+SUBSTR(xTRB->DATA,3,2))
  ZAX->ZAX_NFENTR := xTRB->NOTA
  ZAX->ZAX_DTEMIS := CTOD(SUBSTR(xTRB->EMISSAO,7,2)+'/'+SUBSTR(xTRB->EMISSAO,5,2)+'/'+SUBSTR(xTRB->EMISSAO,3,2))
  ZAX->ZAX_ASSIST := ""
  ZAX->ZAX_CODCLI := XTRB->CODCLI
  ZAX->ZAX_LOJCLI := xTRB->LOJA
  ZAX->ZAX_UF     := cUf
  ZAX->ZAX_MREGIA := cMregiao 
  ZAX->ZAX_REPRES := cRepres
  ZAX->ZAX_VOLUME := XTRB->F1_VOLUME1 
  ZAX->ZAX_VALOR  := round(XTRB->E1_VALOR,2)
  ZAX->ZAX_TRANSP := XTRB->F1_TRANSP
  ZAX->ZAX_FRETE  := ''//XTRB->F1_FRETE
  ZAX->ZAX_SERIE  := XTRB->F1_SERIE
  ZAX->ZAX_STATUS := '04'
  cQry  := "SELECT D1_PEDIDO,D1_NFORI,D1_SERIORI, D1_EMISSAO, D1_ITEM, D1_COD, D1_QUANT, D1_VUNIT, D1_TOTAL, D1_FORNECE, D1_LOJA "  
  cQry  += "FROM SD1010 D1 WITH (NOLOCK)  WHERE  D1_DOC = '"+xTRB->NOTA+"' AND D1_FORNECE = '"+XTRB->CODCLI+"'  AND D1.D_E_L_E_T_ = '' "
  IF Select("YTRB") > 0
     YTRB->(dbCloseArea())
  Endif      
  TCQUERY cQry NEW ALIAS YTRB 
  YTRB->(DBGOTOP())
  If YTRB->(!EOF())
     ZAX->ZAX_NFORIG := YTRB->D1_NFORI
     ZAX->ZAX_EMIORI := CTOD(SUBSTR(YTRB->D1_EMISSAO,7,2)+'/'+SUBSTR(YTRB->D1_EMISSAO,5,2)+'/'+SUBSTR(YTRB->D1_EMISSAO,3,2))
      Pedido         := YTRB->D1_PEDIDO
     //ZAX->ZAX_NFORIG := YTRB->D1_NFORI  
     //ZAX->ZAX_EMIORI := YTRB->D1_EMISSAO
  Else
     cPedido         := ""
  Endif     
  aItens:={}
  YTRB->(DBGOTOP())
  WHILE YTRB->(!EOF())
    AADD(aItens,{YTRB->D1_NFORI,YTRB->D1_SERIORI, YTRB->D1_EMISSAO, YTRB->D1_ITEM, YTRB->D1_COD, YTRB->D1_QUANT, round(YTRB->D1_VUNIT,2), round(YTRB->D1_TOTAL,2),YTRB->D1_FORNECE, YTRB->D1_LOJA })  
    YTRB->(dbskip()) 
  Enddo  
    
  cQry  := "SELECT D2_PEDIDO FROM SD2010 D2 WITH (NOLOCK)  WHERE  D2_DOC = '"+YTRB->D1_NFORI+"' AND D2_SERIE = '"+YTRB->D1_SERIORI+"' AND D2_CLIENTE =  '"+YTRB->D1_FORNECE+"' AND  D2_LOJA = '"+YTRB->D1_LOJA+"'   AND D2.D_E_L_E_T_ = '' "  
  IF Select("YTRB") > 0
     YTRB->(dbCloseArea())
  Endif      
  TCQUERY cQry NEW ALIAS YTRB 
  YTRB->(DBGOTOP())
  If YTRB->(!EOF())
     cPedido  := YTRB->D2_PEDIDO
  Endif      
  
  cQry  := "SELECT C5_MENNOTA, C5_EMISSAO FROM SC5010 WHERE C5_NUM = '"+alltrim(cPedido)+"' AND D_E_L_E_T_ = ''"
  IF Select("YTRB") > 0
     YTRB->(dbCloseArea())
  Endif      
  TCQUERY cQry NEW ALIAS YTRB 
  YTRB->(DBGOTOP())
  If YTRB->(!EOF())
       cMensa := YTRB->C5_MENNOTA
     nPos:= at('/',cMensa)
     cPedido := substr(cMensa,nPos-17,17 ) //  : IKE-25411520002 /
     dData   := CTOD(SUBSTR(YTRB->C5_EMISSAO,7,2)+'/'+SUBSTR(YTRB->C5_EMISSAO,5,2)+'/'+SUBSTR(YTRB->C5_EMISSAO,3,2))
  Endif     
  ZAX->ZAX_PEDIDO := cPedido //xTRB->PEDIDO
  ZAX->(MsUnlock())
  if len(aItens) <> 0 
       dbSelectArea("ZAZ")
       ZAZ->(DbSetOrder(1))  //ZAZ_FILIAL, ZAZ_NFENTR, ZAZ_SERIE, ZAZ_CODCLI, ZAZ_LOJCLI, R_E_C_N_O_, D_E_L_E_T_
       FOR NI := 1 TO LEN(aItens)
           ZAZ->(Dbgotop())
           If ZAZ->(DbSeek(xFilial("ZAZ")+padr(aItens[NI][1],9)+aItens[NI][2]+aItens[NI][9]+aItens[NI][10]+aItens[NI][4],.t. ))
              ZAZ->(Reclock("ZAZ",.F.))
              ZAZ->ZAZ_FILIAL := xFilial("ZAZ")       
           ELSE
              ZAZ->(Reclock("ZAZ",.T.))
           Endif
           ZAZ->ZAZ_ITEM    := aItens[NI][4]    
           ZAZ->ZAZ_SKU     := aItens[NI][5] 
           ZAZ->ZAZ_NFENTR  := aItens[NI][1]
           ZAZ->ZAZ_SERIE   := aItens[NI][2]
           ZAZ->ZAZ_QUANT   := aItens[NI][6]
           ZAZ->ZAZ_VLUNIT  := aItens[NI][7]
           ZAZ->ZAZ_VLTOT   := aItens[NI][8]
           ZAZ->ZAZ_CODCLI  := aItens[NI][9]
           ZAZ->ZAZ_LOJCLI  := aItens[NI][10]
           CODCOR := SUBSTR(ALLTRIM(aItens[NI][5]),9,3)
           CODTAM := SUBSTR(ALLTRIM(aItens[NI][5]),12,4)
           ZAZ->ZAZ_DESCR   := Posicione("SB1",1,xFilial("SB1")+aItens[NI][5],"B1_DESC")  
           ZAZ->ZAZ_COR     := Posicione("SBV",1,xFilial("SBV")+'COR'+CODCOR,"BV_DESCRI")
           ZAZ->ZAZ_TAMANH  := Posicione("SBV",1,xFilial("SBV")+'TAM'+CODTAM,"BV_DESCRI") 
           ZAZ->ZAZ_REFER   := SUBSTR(ALLTRIM(aItens[NI][5]),1,8)
          ZAZ->(MsUnlock()) 
       NEXT NI
       
  Endif
  Endif      
  dbselectarea("XTRB")
  XTRB->(dbSKIP())
  
ENDDO
RestArea(aArea) 
Return

/*
User Function Mod3Inc( cAlias, nReg, nOpc )
	Local oDlg
	Local oGet
	Local nX := 0
	Local nOpcA := 0
	Private aHeader := {}
	Private aCOLS := {}
	Private aGets := {}
	Private aTela := {}
	dbSelectArea( cAlias )
	dbSetOrder(1)
	For nX := 1 To FCount()
		M->&( Eval( bCampo, nX ) ) := CriaVar( FieldName( nX ), .T. )
	Next nX
	Mod3aHeader()
	Mod3aCOLS( nOpc )
	DEFINE MSDIALOG oDlg TITLE cCadastro FROM ;
	aSize[7],aSize[1] TO aSize[6],aSize[5] OF oMainWnd PIXEL
	EnChoice( cAlias, nReg, nOpc, , , , , aPObj[1])
	// Atualiza��o do nome do cliente
	@ aPObj[3,1],aPGet[1,1] SAY "Cliente: " SIZE 70,7 OF oDlg PIXEL
	@ aPObj[3,1],aPGet[1,2] SAY oCliente VAR cCliente SIZE 98,7 OF oDlg PIXEL
	// Atualiza��o do total
	//@ aPObj[3,1],aPGet[1,3] SAY "Valor Total: " SIZE 70,7 OF oDlg PIXEL
	//@ aPObj[3,1],aPGet[1,4] SAY oTotal VAR nTotal PICT "@E 9,999,999,999.99" SIZE 70,7 OF oDlg PIXEL
	oGet := MSGetDados():New(aPObj[2,1],aPObj[2,2],aPObj[2,3],aPObj[2,4],nOpc,"U_Mod3LOk()",".T.","+ZAZ_ITEM",.T.)
	ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,{|| IIF( Mod3TOk().And.Obrigatorio( aGets, aTela ), ( nOpcA := 1, oDlg:End() ), NIL) },{|| oDlg:End() })
	If nOpcA == 1 .And. nOpc == 3
		Mod3Grv( nOpc )
	//	ConfirmSXE()
	Endif
	
Return */

User Function Mod3Mnt( cAlias, nReg, nOpc )
	Local oDlg
	Local oGet
	Local nX := 0
	Local nOpcA := 0
	Private aHeader := {}
	Private aCOLS := {}
	Private aGets := {}
	Private aTela := {}
	Private aREG := {}
	dbSelectArea( cAlias )
	dbSetOrder(1)
	For nX := 1 To FCount()
		M->&( Eval( bCampo, nX ) ) := FieldGet( nX )
	Next nX
	Mod3aHeader()
	Mod3aCOLS( nOpc )
	DEFINE MSDIALOG oDlg TITLE cCadastro FROM aSize[7],aSize[1] TO aSize[6],aSize[5] OF oMainWnd PIXEL
	EnChoice( cAlias, nReg, nOpc, , , , , aPObj[1])
	// Atualiza��o do nome do cliente
//	@ aPObj[3,1],aPGet[1,1] SAY "Cliente: " SIZE 70,7 OF oDlg PIXEL
//	@ aPObj[3,1],aPGet[1,2] SAY oCliente VAR cCliente SIZE 98,7 OF oDlg PIXEL
	// Atualiza��o do total
	@ aPObj[3,1],aPGet[1,3] SAY "Valor Total: " SIZE 70,7 OF oDlg PIXEL
	@ aPObj[3,1],aPGet[1,4] SAY oTotal VAR nTotal PICTURE "@E 9,999,999,999.99" SIZE 70,7 OF oDlg PIXEL
//	U_Mod3Cli()
	oGet := MSGetDados():New(aPObj[2,1],aPObj[2,2],aPObj[2,3], aPObj[2,4],nOpc,"U_Mod3LOk()",".T.","+ZAZ_ITEM",.T.) 
	ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,;
	{|| IIF( Mod3TOk().And.Obrigatorio( aGets, aTela ), ( nOpcA := 1, oDlg:End() ), NIL ) },;
	{|| oDlg:End() })
	If nOpcA == 1 .And. ( nOpc == 4 .Or. nOpc == 5 )
	   Mod3Grv( nOpc, aREG )
	Endif
Return

Static Function Mod3aHeader()
	Local aArea := GetArea()
	dbSelectArea("SX3")
	dbSetOrder(1)
	dbSeek("ZAZ")
	While !EOF() .And. X3_ARQUIVO == "ZAZ"
		If X3Uso(X3_USADO) .And. cNivel >= X3_NIVEL
			AADD( aHeader, { Trim( X3Titulo() ),;
			X3_CAMPO,;
			X3_PICTURE,;
			X3_TAMANHO,;
			X3_DECIMAL,;
			X3_DECIMAL,;
			X3_VALID,;
			X3_USADO,;
			X3_TIPO,;
			X3_ARQUIVO,;
			X3_ARQUIVO,;
			X3_ARQUIVO,;
			X3_ARQUIVO,;
			X3_CONTEXT})
		Endif
		dbSkip()
	End
	RestArea(aArea)
Return

Static Function Mod3aCOLS( nOpc )
	Local aArea := GetArea()
	Local cChave := ""
	Local cAlias := "ZAZ"
	Local nI := 0
	If nOpc <> 3
		cChave := ZAX->ZAX_NFENTR+ZAX->ZAX_SERIE
		dbSelectArea( cAlias )
		dbSetOrder(1)
		dbSeek( xFilial( cAlias ) + cChave )
		While !EOF() .And. ZAZ->( ZAZ_FILIAL +ZAZ_NFENTR+ZAZ_SERIE) == xFilial(cAlias)+ cChave
			AADD( aREG, ZAZ->( RecNo() ) )
			AADD( aCOLS, Array( Len( aHeader ) + 1 ) )
			For nI := 1 To Len( aHeader )
				If aHeader[nI,10] == "V"
					aCOLS[Len(aCOLS),nI] := CriaVar(aHeader[nI,2],.T.)
				Else
					aCOLS[Len(aCOLS),nI] := FieldGet(FieldPos(aHeader[nI,2]))
				Endif
			Next nI
			aCOLS[Len(aCOLS),Len(aHeader)+1] := .F.
			dbSkip()
		EndDo
	Else
		AADD( aCOLS, Array( Len( aHeader ) + 1 ) )
		For nI := 1 To Len( aHeader )
			aCOLS[1, nI] := CriaVar( aHeader[nI, 2], .T. )
		Next nI
		aCOLS[1, GdFieldPos("ZAZ_ITEM")] := "001"
		aCOLS[1, Len( aHeader )+1 ] := .F.
	Endif
	Restarea( aArea )
Return

User Function Mod3Cli()
	cCliente := Posicione("SA1",1,xFilial("SA1")+ M->(ZAX_CLIENT+ZAX_LOJA),;
	"A1_NREDUZ" )
	oCliente:Refresh()
Return (.T.)

User Function Mod3LOk()
	Local nI := 0
	nTotal := 0
	For nI := 1 To Len( aCOLS )
		If aCOLS[nI,Len(aHeader)+1]
			Loop
		Endif
		nTotal+=Round(aCOLS[nI,GdFieldPos("ZAZ_QUANT")]*aCOLS[nI,GdFieldPos("ZAZ_VLUNIT")],2)
               
	Next nI
	oTotal:Refresh()
Return(.T.)


Static Function Mod3TOk()
Return(.t.)

Static Function Mod3Grv( nOpc, aAltera)
	Local nX := 0
	Local nI := 0
	// Se for inclus�o
	If nOpc == 3
		// Grava os itens
		dbSelectArea("ZAZ")
		dbSetOrder(1)
		For nX := 1 To Len( aCOLS )
			If !aCOLS[ nX, Len( aCOLS ) + 1 ]
				RecLock( "ZAZ", .T. )
				For nI := 1 To Len( aHeader )
					FieldPut( FieldPos( Trim( aHeader[nI, 2] ) ), aCOLS[nX,nI] )
				Next nI
				ZAZ->ZAZ_FILIAL := xFilial("ZAZ")
				ZAZ->ZAZ_NFENTR := M->ZAX_NFENTR
				ZAZ->ZAZ_SERIE  := M->ZAX_SERIE
				ZAZ->ZAZ_CODCLI := M->ZAX_CODCLI
				ZAZ->ZAZ_LOJCLI := M->ZAX_LOJCLI
				MsUnLock()
			Endif
		Next nX
		// Grava o Cabe�alho
		dbSelectArea( "ZAX" )
		RecLock( "ZAX", .T. )
		For nX := 1 To FCount()
			If "FILIAL" $ FieldName( nX )
				FieldPut( nX, xFilial( "ZAX" ) )
			Else
				FieldPut( nX, M->&( Eval( bCampo, nX ) ) )
			Endif
		Next nX
		MsUnLock()
	Endif
	// Se for altera��o
	If nOpc == 4
		// Grava os itens conforme as altera��es
		dbSelectArea("ZAZ")
		dbSetOrder(1)
		For nX := 1 To Len( aCOLS )
			If nX <= Len( aREG )
				dbGoto( aREG[nX] )
				RecLock("ZAZ",.F.)
				If aCOLS[ nX, Len( aHeader ) + 1 ]
					dbDelete()
				Endif
			Else
				If !aCOLS[ nX, Len( aHeader ) + 1 ]
					RecLock( "ZAZ", .T. )
				Endif
			Endif
			If !aCOLS[ nX, Len(aHeader)+1 ]
				For nI := 1 To Len( aHeader )
					FieldPut( FieldPos( Trim( aHeader[ nI, 2] ) ),;
					aCOLS[ nX, nI ] )
				Next nI
				ZAZ->ZAZ_FILIAL := xFilial("ZAZ")
				ZAZ->ZAZ_NFENTR := M->ZAX_NFENTR
			Endif
			MsUnLock()
		Next nX
		// Grava o Cabe�alho
		dbSelectArea("ZAX")
		RecLock( "ZAX", .F. )
		For nX := 1 To FCount()
			If "FILIAL" $ FieldName( nX )
				FieldPut( nX, xFilial("ZAX"))
			Else
				FieldPut( nX, M->&( Eval( bCampo, nX ) ) )
			Endif
		Next
		MsUnLock()
	Endif
	// Se for exclus�o
	If nOpc == 5
		// Deleta os Itens
		dbSelectArea("ZAZ")
		dbSetOrder(1) //  ZAZ_FILIAL, ZAZ_NFENTR, ZAZ_SERIE, ZAZ_CODCLI, ZAZ_LOJCLI, ZAZ_ITEM, 
		dbSeek(xFilial("ZAZ")+M->ZAX_NFENTR+M->ZAX_SERIE+M->ZAX_CODCLI+M->ZAX_LOJCLI) 
		While !EOF() .And. ZAZ->(ZAZ_FILIAL+ZAZ_NFENTR+ZAZ_SERIE+ZAZ_CODCLI+ZAZ_LOJCLI) == xFilial("ZAX")+M->ZAX_NFENTR+M->ZAX_SERIE+M->ZAX_CODCLI+M->ZAX_LOJCLI
			RecLock("ZAZ")
			dbDelete()
			MsUnLock()
			dbSkip()
		End
		// Deleta o Cabe�alho
		dbSelectArea("ZAX")
		RecLock("ZAX",.F.)
		dbDelete()
		MsUnLock()
	Endif
Return

/*/
����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � CriaSX1  � Autor � Microsiga             � Data � 10.05.08 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Cria e analisa grupo de perguntas                          ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Parametros� cPerg                                                      ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Generico                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������
*/

Static Function AjustaSX1( cPerg,nop )
Local i,j
Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )
Local aTamSX3	:= {}
Local aRegs     := {}
cPerg:= padr(cPerg,10)
//�����������������������������������������������������������������������������������������Ŀ
//� Grava as perguntas no arquivo SX1                                                       �
//�������������������������������������������������������������������������������������������
if nop == 1
aTamSX3	:= TAMSX3( "E1_FILIAL" )
AADD(aRegs,{cPerg,	"01","Da Filial  ?" ,"Da Filial  ?","Da Filial   ?","mv_ch1", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR01","","","",""    ,"","","","","","","","","","","","","","","","","","","","","","S","","","","" })
AADD(aRegs,{cPerg,	"02","Ate Filial ?" ,"Ate Filial ?","Ate Filial  ?","mv_ch2", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR02","","","","ZZZZ","","","","","","","","","","","","","","","","","","","","","","S","","","","" })

aTamSX3	:= TAMSX3( "E1_EMISSAO" )
AADD(aRegs,{cPerg,	"03","Da Emissao ?" ,"Da Emissao ?","Da Emissao ?" ,"mv_ch3", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR03","","","","","","","","","","","","","","","","","","","","","","","","","","S","","","","" })
AADD(aRegs,{cPerg,	"04","Ate Emissao?" ,"Ate Emissao?","Ate Emissao?" ,"mv_ch4", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR04","","","","","","","","","","","","","","","","","","","","","","","","","","S","","","","" })
Endif
if nop == 2
aTamSX3	:= TAMSX3( "ZAX_CLINFE" )
AADD(aRegs,{cPerg,	"01","Do Cliente  ?" ,"Do Cliente  ?","Do Cliente  ?","mv_ch1", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR01","","","",""    ,"","","","","","","","","","","","","","","","","","","","","","S","","","","" })
AADD(aRegs,{cPerg,	"02","Ate Cliente ?" ,"Ate Cliente ?","Ate Cliente ?","mv_ch2", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR02","","","","ZZZZ","","","","","","","","","","","","","","","","","","","","","","S","","","","" })

aTamSX3	:= TAMSX3( "ZAX_DTAREG" )
AADD(aRegs,{cPerg,	"03","Da  Dta. Registro?" ,"Da  Dta. Registro?","Da  Dta. Registro?","mv_ch3", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR03","","","","","","","","","","","","","","","","","","","","","","","","","","S","","","","" })
AADD(aRegs,{cPerg,	"04","At� Dta. Registro?" ,"At� Dta. Registro?","At� Dta. Registro?","mv_ch4", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR04","","","","","","","","","","","","","","","","","","","","","","","","","","S","","","","" })

aTamSX3	:= TAMSX3( "ZAX_MREGIA" )
AADD(aRegs,{cPerg,	"05","Da  Regi�o ?" ,"Da  Regi�o ?","Da  Regi�o ?","mv_ch5", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR05","","","",""    ,"","","","","","","","","","","","","","","","","","","","","","S","","","","" })
AADD(aRegs,{cPerg,	"06","Ate Regi�o ?" ,"Ate Regi�o ?","Ate Regi�o ?","mv_ch6", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR06","","","","ZZZZZZZZ","","","","","","","","","","","","","","","","","","","","","","S","","","","" })

TamSX3	:= TAMSX3( "ZAX_NFENTR" )
AADD(aRegs,{cPerg,	"07","Da  Nota Dev ?" ,"Da  Nota Dev ?","Da  Nota Dev ?","mv_ch7", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR07","","","",""    ,"","","","","","","","","","","","","","","","","","","","","","S","","","","" })
AADD(aRegs,{cPerg,	"08","Ate Nota Dev ?" ,"Ate Nota Dev ?","Ate Nota Dev ?","mv_ch8", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR08","","","","ZZZZZZZZZ","","","","","","","","","","","","","","","","","","","","","","S","","","","" })

TamSX3	:= TAMSX3( "ZAX_CODCLI" )
AADD(aRegs,{cPerg,	"09","Do  C�d Cliente?" ,"Do  C�d Cliente?","Do  C�d Cliente?","mv_ch9", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR09","","","",""    ,"","","","","","","","","","","","","","","","","","","","","","S","","","","" })
AADD(aRegs,{cPerg,	"10","Ate C�d Cliente?" ,"Ate C�d Cliente?","Ate C�d Cliente?","mv_ch0", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR10","","","","ZZZZZZZZZ","","","","","","","","","","","","","","","","","","","","","","S","","","","" })
AADD(aRegs,{cPerg,	"11"," Na Ordem Ordem " ," Na Ordem Ordem "," Na Ordem Ordem ","mv_cha", "N","1","0",0,"C" ,"","MV_PAR11","","","",""    ,"","","","","","","","","","","","","","","","","","","","","","S","","","","" })


endif
DbSelectArea("SX1")
SX1->(DbSetOrder(1))

For I := 1 To Len(aRegs)
	If 	!dbSeek(cPerg+aRegs[i,2])		
		RecLock("SX1",.T.)
		For j:=1 to FCount()
			IF j <= Len(aRegs[i])
				FieldPut(j,aRegs[i,j])
			EndIf
		Next

		MsUnLock()
	EndIf
Next

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return()


User Function Mod3Lib()
Local aAreaLib	:= GetArea()
Local lRet      := .f. 
Local cCodUsr   := space(6)
Local NomUser   :=cUsername
Private lRetg   := .f.
Private nTipo   := 0
lRetg := U_HPdev01A()
If lRetg
   If nTipo = 1
      MsgStop("Devolu��es Aprovados ")
   Else
      MsgStop("Devolu��es Recusadas ")
   Endif   
Else
   MsgStop("Nada foi Aprovado/Recusadas ")
Endif   
RestArea( aAreaLib )
Return()


Static Function Libera(cMarca,nOpc)
Local aAreaLib	:= GetArea()
Local lRetx := .f.
	If(nOpc == 5) 
        Aviso('Libera��o !','Libera��o cancelada pelo Usu�rio!',{'OK'})	
		oDlgT:End()
	ElseIf (nOpc == 1) .or. (nOpc == 2)    
	    If U_VerSenha()	
		   Processa( {|| HPDEV01H(cMarca,nOpc) }, "Aten��o", "Iniciando a Libera��o", .F.)
		   lRetx := .t.
		   oDlgT:End()
        eLSE
           Aviso("Libera��o","Nenhuma Libera��o executada!" ,{'OK'})
           oDlgT:End()
        Endif   
	EndIf
	nTipo := nOpc
RestArea( aAreaLib )
Return(lRetx)

Static Function HPDEV01H(cMarca,xopc)
	Local aAreaSE1:= SE1->(GetArea())
	Private nSaldoComp:= 0

	
	dbSelectArea("XTRB")
	XTRB->(dbGoTop())
	While XTRB->(!Eof())
		If Alltrim(XTRB->ZAX_OK) <> ""
			Aadd(aRecZAX,{XTRB->ZAX_NFENTR,XTRB->ZAX_SERIE,XTRB->ZAX_CODCLI,XTRB->ZAX_LOJCLI} )
            DbSelectArea("SE1")
            //SE1->(DbSetOrder(1)) Antigo - Comentado R.Melo
            SE1->(DbSetOrder(2))//Inserido R.Melo
            SE1->(dbGoTop())
            c_status := '04'
            //If SE1->(DbSeek(xFilial("SE1")+XTRB->ZAX_SERIE+Padr(XTRB->ZAX_NFENTR,9)+'A  '+'NCC')) Comentado R.Melo
            If SE1->(DbSeek(xFilial("SE1")+XTRB->ZAX_CODCLI+XTRB->ZAX_LOJCLI+XTRB->ZAX_SERIE+Padr(XTRB->ZAX_NFENTR,9)))
               SE1->(RecLock( "SE1", .F. ))
               If xOpc == 2
                  SE1->E1_XSTATUS := 'RECUSADO'
                  SE1->E1_MSBLQL := '1'
                  c_status := '07' 
               Else
                  SE1->E1_XSTATUS := 'Proc.Finan' 
                  SE1->E1_MSBLQL := '2'
                  c_status := '05'
               Endif   
               SE1->(MsUnLock())
               EnvLib(SE1->E1_NUM,cGet1,xOpc )
               SE1->(dBsKip())
               DbSelectArea("ZAX")
               ZAX->(DbGoto(XTRB->ZAX_REGIST))
               If alltrim(ZAX->ZAX_NFENTR+ZAX->ZAX_SERIE+substr(ZAX->ZAX_CODCLI,1,6)+ZAX->ZAX_LOJCLI)==alltrim(XTRB->ZAX_NFENTR+XTRB->ZAX_SERIE+XTRB->ZAX_CODCLI+XTRB->ZAX_LOJCLI)
                  ZAX->(RecLock( "ZAX", .F. ))
                  ZAX->ZAX_STATUS := c_Status
                  ZAX->ZAX_DTALIB := dDatabase 
                  ZAX->ZAX_AUTORI := cGet1
                  ZAX->(MsUnLock())
                  //MsgStop("Gravei  ZAX ")    
               Else
                  //MsgStop("N�o Achei ZAX ")   
               endif
            Else
             //If SE1->(DbSeek(xFilial("SE1")+XTRB->ZAX_SERIE+Padr(XTRB->ZAX_NFENTR,9)+'   '+'NCC'))
               DbSelectArea("SE1")
               SE1->(DbSetOrder(2))
               SE1->(dbGoTop())
               If SE1->(DbSeek(xFilial("SE1")+XTRB->ZAX_CODCLI+XTRB->ZAX_LOJCLI+XTRB->ZAX_SERIE+Padr(XTRB->ZAX_NFENTR,9)))
			   SE1->(RecLock( "SE1", .F. ))
			   If xOpc == 2
			      SE1->E1_XSTATUS := 'RECUSADO'
			      SE1->E1_MSBLQL := '1'
			      c_status := '07' 
			   Else
			      SE1->E1_XSTATUS := 'Proc.Finan' 
			      SE1->E1_MSBLQL := '2'
			      c_status := '05'
			   Endif   
               SE1->(MsUnLock())
               EnvLib(SE1->E1_NUM,cGet1,xOpc )
               SE1->(dBsKip())
               DbSelectArea("ZAX")
               ZAX->(DbGoto(XTRB->ZAX_REGIST))
               If alltrim(ZAX->ZAX_NFENTR+ZAX->ZAX_SERIE+substr(ZAX->ZAX_CODCLI,1,6)+ZAX->ZAX_LOJCLI)==alltrim(XTRB->ZAX_NFENTR+XTRB->ZAX_SERIE+XTRB->ZAX_CODCLI+XTRB->ZAX_LOJCLI)
                  ZAX->(RecLock( "ZAX", .F. ))
                  ZAX->ZAX_STATUS := c_Status
                  ZAX->ZAX_DTALIB := dDatabase 
                  ZAX->ZAX_AUTORI := cGet1
                  ZAX->(MsUnLock())
                  //MsgStop("Gravei  ZAX ")    
               Else
                  //MsgStop("N�o Achei ZAX ")   
               endif
             Endif  
               lRetg := .t.
            Endif
		Endif    
		XTRB->(DBSKIP()) 
	End 
	
	RestArea(aAreaSE1)
	
	onVlrMar:Refresh()
	XTRB->(dbGoTop())	 
//	lRet := U_COMPCR(nSaldoComp)
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return(Nil)



//--------------------------------------------------------------
/*/{Protheus.doc} MyFunction
Description

@param xParam Parameter Description
@return xRet Return Description
@author Daniel Pereira de Souza - daniel.souza@terra.com.br
@since 26/02/2018
/*/
//--------------------------------------------------------------
User Function VerSenha()
Local aArea := GetArea() 
Local lRet := .f.
Local oButton1
Local oButton2
Local oGet1
Local oGet2
Local oSay1
Local oSay2
Local oSay3
Local oSay4
Local cCodUsr   := space(6)
Local NomUser   :=cUsername  
Static oDlg
cGet1  := space(20)
cGet2  := space(20)
criamv("HP_USUADEV")
cCodUsr := getmv("HP_USUADEV")
//If (__cUserId $ cCodUsr)


  DEFINE MSDIALOG oDlg TITLE "New Dialog" FROM 000, 000  TO 300, 500 COLORS 0, 16777215 PIXEL
    @ 006, 055 SAY oSay4 PROMPT "Tela de autoriza��o de libera��o de devolu��o" SIZE 185, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 030, 045 SAY oSay1 PROMPT "Autorizado por:" SIZE 068, 014 OF oDlg  COLORS 0, 16777215 PIXEL
    @ 046, 089 MSGET oGet1 VAR cGet1 SIZE 108, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 072, 089 MSGET oGet2 VAR cGet2 SIZE 108, 009 OF oDlg COLORS 0, 16777215 PASSWORD PIXEL
    @ 046, 046 SAY oSay2 PROMPT "Usu�rio .....:" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 073, 046 SAY oSay3 PROMPT "Senha .............:" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 097, 145 BUTTON oButton1 PROMPT "Confirmar" SIZE 050, 025 OF oDlg ACTION (IIF(lRet:=Confirma(1),Odlg:End(),) ) PIXEL
    @ 097, 065 BUTTON oButton2 PROMPT "Cancelar"  SIZE 050, 025 OF oDlg ACTION (lRet:=Confirma(2),Odlg:End()) PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED

RestArea(aArea)
Return(lRet)

Static Function Confirma(nPar)
Local lRet := .f.
Local cCodUsr := space(20)
criamv("HP_USUADEV")
cCodUsr := alltrim(getmv("HP_USUADEV"))

If Npar == 1
   
   //_cUserNam - variavel carregada com o nome do usuario
   //cSenhaV - variavel carregada com a senha do usuario
   //_cUsers - variavel carregada com id dos usuarios que est�o autorizados a realizar determinada rotina.

   If !Empty(cGet1)
      PswOrder(2)
      If PswSeek(cGet1,.T.)
          _aUsr      := PswRet(1)
          vbResp     := _aUsr[1][1] $ cCodUsr
          
          If !PSWNAME(Alltrim(cGet2))
               Aviso('Usu�rio!','Senha Inv�lida! Informe o Usu�rio e Senha',{'OK'})
               Return(lRet) //Return( _aRetorno )
          EndIF
          
      Else
          Aviso('Senha Inv�lida!','Senha Inv�lida! Informe o Usu�rio e Senha!',{'OK'})
          Return(lRet) //Return( _aRetorno )
      EndIf
     
   EndIf 

   If !vbResp
      Aviso('Inv�lido!','Usu�rio '+cGet1+' n�o autorizado a efetuar esta libera��o!',{'OK'})
      lRet := .f.
   Else
     //_aRetorno[1] := .T.                   //Resultado da Autoriza��o
     //_aRetorno[2] := AllTrim(_aUsr[1][1]) //C�digo do Usu�rio Autorizante
     //_aRetorno[3] := AllTrim(_aUsr[1][2]) //Nome do Usu�rio Autorizante 
     //cUserLibCp   := AllTrim(_aUsr[1][2])
     lRet := .t. 
   EndIf

ELse
     lRet := .f.
Endif     
Return(lRet )
    
 User Function HPdev01A()
	Local _astru   := {}
	Local _afields := {}     
	Local _carq    := ""         
	Local cQuery   := ""
	Local xFornec := ""
	Local xLoja   := ""
	Local xVecto  := ctod(space(8))
	Local nValor  := 0
	Local xItens  := ""
	Local xRazao  := ""
	Local cFiltraSE1
	Local ca097User 	:= RetCodUsr()
	Local aSize   := MsAdvSize()  
	Local oFont1  := TFont():New("Verdana",,012,,.T.,,,,,.F.,.F.)
	Local oFont2  := TFont():New("Verdana",,012,,.F.,,,,,.F.,.F.)
	Local aColunas := {	{"ZAX_OK"     		,,""       		},;
	{"ZAX_CLINFE" ,,"Cliente"       ,"@!"		},;
	{"ZAX_DTAREG" ,,"Dta Registro"  ,"@D"		},;
	{"ZAX_VALOR"  ,,"Valor"         ,"@E 999,999.99"  },; 
	{"ZAX_VOLUME" ,,"Volume"        ,"@E 999,999"  },;
	{"ZAX_UF"     ,,"UF"            ,"@!"		},;
	{"ZAX_CODCLI" ,,"Codigo Cliente","@!"		},;
	{"ZAX_LOJCLI" ,,"Loja Cliente"  ,"@!"		},;
	{"ZAX_MREGIA" ,,"Macro Regi�o"  ,"@!"		},;
	{"ZAX_NFENTR" ,,"Nota Entrada"  ,"@!"		},;
	{"ZAX_DTEMIS" ,,"Emiss�o"       ,"@D"		},;
	{"ZAX_OBSERV" ,,"Observa��es"   ,"@!"		},;
	{"ZAX_REPRES" ,,"Representante" ,"@!"		},;
	{"ZAX_MOTIVO" ,,"Motivo"        ,"@!"		},;
	{"ZAX_ASSIST" ,,"Assistente"    ,"@!"		},;
	{"ZAX_NFORIG" ,,"Nf Original"   ,"@!"		},;
	{"ZAX_EMIORI" ,,"Emiss�o Nf"    ,"@D"		},;
	{"ZAX_QUALID" ,,"Dept.Qualidade","@!"		},;
	{"ZAX_DTALIB" ,,"Data Libera��o","@!"		},;
	{"ZAX_SERIE"  ,,"Serie"         ,"@!"		},;
	{"ZAX_REGIST" ,,"No. Registro"  ,"@E 9999999" }}
	
	
/*
ZAX_CODCLI ZAX_LOJCLI ZAX_TPDEVO ZAX_AUTORI ZAX_TRANSP ZAX_FRETE ZAX_FLUXOD ZAX_DIATRA ZAX_QTDPEC ZAX_CONFER ZAX_1 ZAX_CONSER ZAX_CONSE2 ZAX_CONSE3 ZAX_FALTA 
ZAX_SOBRA  ZAX_PCUSAD ZAX_DTAREV ZAX_DIFERE ZAX_DTRECE ZAX_DTAQPA ZAX_ACAOCO ZAX_POSTAG ZAX_CODOBJ ZAX_PEDIDO ZAX_NFDVCO
*/
	
	
	Private cliSE1    := ""
	Private lojSE1    := ""
	Private preSE1    := ""
	Private numSE1    := ""
	Private parSE1    := ""
	Private tipSE1    := ""
	Private cli_RA    := ""
	Private loj_RA    := ""
	Private pre_RA    := ""
	Private num_RA    := ""
	Private par_RA    := ""
	Private tip_RA    := ""
	Private lMarcar   := .F.
	Private oMark
	Private cPerg     := "HPCOMP0002"
	Private aRotina   := {} 
	Private lContinua := .F.
	Private atuar     := ""
	Private cMark     := GetMark() 
	Private lInverte  := .F.    
	Private oChk      
	Private lChkSel   := .F.  
	Private lRefresh  := .T.  
	Private oDlgT   
	Private onVlrCom
	Private onVlrSld
	Private onVlrMar
	Private onVlrfIL
	Private onVlrSob
	Private nVlrCom   := 0
	Private nVlrSld   := 0
	Private nVlrMar   := 0
	Private nVlrfIL   := 0
	Private nVlrSob   := 0
	Private aRecZAX   := {}
	Private nRecnoRA  := RECNO()
	cMensa := "Confirma Libera��o de Devolu��o ao Financeiro " 
	//If MsgYesNo(cMensa,"Confirma��o")     
    cPerg:="HP_LIBDEV1"
	AjustaSX1(cPerg,2)
	If  Pergunte(cPerg, .T.)
			If Select("XTRB") > 0
				XTRB->(DbCloseArea())
			EndIf

			//Estrutura da tabela temporaria
			AADD(_astru,{"ZAX_OK"     ,"C",02,0})
			AADD(_astru,{"ZAX_CLINFE" ,"C",35,0})
			AADD(_astru,{"ZAX_DTAREG" ,"D",08,0})
			AADD(_astru,{"ZAX_VALOR"  ,"N",16,2}) 
			AADD(_astru,{"ZAX_VOLUME" ,"N",10,0})
			AADD(_astru,{"ZAX_UF"     ,"C",02,0})
			AADD(_astru,{"ZAX_CODCLI" ,"C",06,0})
			AADD(_astru,{"ZAX_LOJCLI" ,"C",04,0})
			AADD(_astru,{"ZAX_MREGIA" ,"C",06,0})
			AADD(_astru,{"ZAX_NFENTR" ,"C",09,0})
			AADD(_astru,{"ZAX_DTEMIS" ,"D",08,0})
			AADD(_astru,{"ZAX_OBSERV" ,"C",60,0})
			AADD(_astru,{"ZAX_MOTIVO" ,"C",20,0})
    		AADD(_astru,{"ZAX_ASSIST" ,"C",10,0})
			AADD(_astru,{"ZAX_REPRES" ,"C",20,0})
			AADD(_astru,{"ZAX_NFORIG" ,"C",09,0})
			AADD(_astru,{"ZAX_EMIORI" ,"D",08,0})
			AADD(_astru,{"ZAX_QUALID" ,"C",20,0})
			AADD(_astru,{"ZAX_DTALIB" ,"D",08,0})
			AADD(_astru,{"ZAX_SERIE"  ,"C",03,0})
			AADD(_astru,{"ZAX_REGIST" ,"N",10,0})
						
			cArqTrab  := CriaTrab(_astru)
			dbUseArea( .T.,, cArqTrab, "XTRB", .F., .F. )

			//Atribui a tabela temporaria ao alias TRB

			//-------------------------------------------------------------------
			// Verifica se o usuario possui direito de liberacao.           
			//-------------------------------------------------------------------
			
            cQuery := "SELECT ''  as ZAX_OK, ZAX_CLINFE,ZAX_DTAREG,ZAX_ASSIST,ZAX_UF,ZAX_MREGIA,ZAX_REPRES,ZAX_NFENTR,ZAX_DTEMIS,ZAX_VOLUME,ZAX_VALOR,ZAX_MOTIVO, "
            cQuery += " ZAX_NFORIG,ZAX_EMIORI,ZAX_QUALID,ZAX_DTALIB,ZAX_OBSERV,ZAX_SERIE,ZAX_CODCLI,ZAX_LOJCLI ,R_E_C_N_O_ AS ZAX_REGIST  "
            cQuery += " FROM "+RetSqlName("ZAX")+" AX WITH (NOLOCK) WHERE ZAX_CLINFE BETWEEN '"+MV_PAR01+"' AND '"+MV_PAR02+"' AND " 
            cQuery += " ZAX_DTAREG BETWEEN '"+DTOS(MV_PAR03)+"' AND '"+DTOS(MV_PAR04)+"' AND ZAX_MREGIA BETWEEN '"+MV_PAR05+"' AND '"+MV_PAR06+"' "
            cQuery += " AND ZAX_NFENTR BETWEEN '"+MV_PAR07+"' AND '"+MV_PAR08+"' AND ZAX_CODCLI  BETWEEN '"+MV_PAR09+"' AND '"+MV_PAR10+"' "
            cQuery += " AND ZAX_STATUS = '04' AND D_E_L_E_T_ = '' "  
            DO CASE  
               CASE MV_PAR11 == 1
                  cQuery += " ORDER BY ZAX_CLINFE "  
               CASE MV_PAR11 == 2
                  cQuery += " ORDER BY ZAX_DTAREG+ZAX_CLINFE " 
               CASE MV_PAR11 == 3
                  cQuery += " ORDER BY ZAX_CODCLI  "
               CASE MV_PAR11 == 4
                  cQuery += " ORDER BY ZAX_MREGIA+ZAX_CLINFE  "
            Endcase     
  
          	If Select("YTRB") > 0
				YTRB->(DbCloseArea())
			EndIf
			//cQuery := ChangeQuery(cQuery)
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"YTRB",.T.,.T.)

			DbSelectArea("YTRB")
			YTRB->(DbGotop())   
			While YTRB->(!EOF())
				DbSelectArea("XTRB")        
				XTRB->(RecLock("XTRB",.T.))

				XTRB->ZAX_OK     := YTRB->ZAX_OK     
				XTRB->ZAX_CLINFE := YTRB->ZAX_CLINFE
				XTRB->ZAX_DTAREG := CTOD(SUBSTR(YTRB->ZAX_DTAREG,7,2)+'/'+SUBSTR(YTRB->ZAX_DTAREG,5,2)+'/'+SUBSTR(YTRB->ZAX_DTAREG,3,2)) 
				XTRB->ZAX_ASSIST := YTRB->ZAX_ASSIST
				XTRB->ZAX_UF     := YTRB->ZAX_UF
				XTRB->ZAX_MREGIA := YTRB->ZAX_MREGIA
				XTRB->ZAX_REPRES := YTRB->ZAX_REPRES
				XTRB->ZAX_NFENTR := YTRB->ZAX_NFENTR
				XTRB->ZAX_DTEMIS := CTOD(SUBSTR(YTRB->ZAX_DTEMIS,7,2)+'/'+SUBSTR(YTRB->ZAX_DTEMIS,5,2)+'/'+SUBSTR(YTRB->ZAX_DTEMIS,3,2))
				XTRB->ZAX_VOLUME := YTRB->ZAX_VOLUME
				XTRB->ZAX_VALOR  := YTRB->ZAX_VALOR
				XTRB->ZAX_MOTIVO := YTRB->ZAX_MOTIVO
				XTRB->ZAX_NFORIG := YTRB->ZAX_NFORIG
				XTRB->ZAX_EMIORI := CTOD(SUBSTR(YTRB->ZAX_EMIORI,7,2)+'/'+SUBSTR(YTRB->ZAX_EMIORI,5,2)+'/'+SUBSTR(YTRB->ZAX_EMIORI,3,2))
				XTRB->ZAX_QUALID := YTRB->ZAX_QUALID
				XTRB->ZAX_DTALIB := CTOD(SUBSTR(YTRB->ZAX_DTALIB,7,2)+'/'+SUBSTR(YTRB->ZAX_DTALIB,5,2)+'/'+SUBSTR(YTRB->ZAX_DTALIB,3,2))
				XTRB->ZAX_OBSERV := YTRB->ZAX_OBSERV
				XTRB->ZAX_SERIE  := YTRB->ZAX_SERIE
				XTRB->ZAX_CODCLI := YTRB->ZAX_CODCLI
			    XTRB->ZAX_LOJCLI := YTRB->ZAX_LOJCLI 
			    XTRB->ZAX_REGIST := YTRB->ZAX_REGIST
				XTRB->(MsUnlock())        
				YTRB->(DbSkip())
			Enddo
			DbSelectArea("XTRB")
			XTRB->(DbGotop())   
			DEFINE MSDIALOG oDlgT TITLE "HOPE - Libera��o de Devolu��o " FROM aSize[7],100 To aSize[6],aSize[5] COLORS 0, 16777215 PIXEL Style DS_MODALFRAME
			cMarca    := GetMark()
			@ 005, 005 SAY oSay2 PROMPT "Lista das devolu��es a serem Aprovadas/Reprovadas ." SIZE 242, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
			oMark := MsSelect():New("XTRB","ZAX_OK",,aColunas,lInverte,cMarca,{ 015, 003, 242, 550}) 
			oMark:bAval:= {||(HPDEV01E(cMarca),oMark:oBrowse:Refresh())}
			oMark:oBrowse:Refresh(.F.)	   	
			oMark:oBrowse:lHasMark    := .T.
			oMark:oBrowse:lCanAllMark := .T.
			oMark:oBrowse:bAllMark := {|| U_HPDEV01F(cMarca),oMark:oBrowse:Refresh()} 
			@ 250, 005 BUTTON oButton1 PROMPT "Aprovar" 	SIZE 032, 013 OF oDlgT ACTION lretG:= Libera(cMarca,1) PIXEL
			@ 250, 040 BUTTON oButton1 PROMPT "Reprovar" 	SIZE 032, 013 OF oDlgT ACTION lretG:= Libera(cMarca,2) PIXEL
			@ 250, 075 BUTTON oButton1 PROMPT "Fechar" 	  	SIZE 035, 013 OF oDlgT ACTION LretG:= Libera(cMarca,5) PIXEL
			@ 250, 190 Say oSay7 prompt " Valor Total Marcado para Aprovar/Reprovar:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
			@ 250,315 MSGET onVlrMar VAR nVlrMar When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"

			ACTIVATE MSDIALOG oDlgT CENTERED

			if !Empty('XTRB')
				XTRB->(DbCloseArea())
			Endif
		Endif  
	
Return(lRetG)  
    
    
Static Function HPDEV01E(cMarca)            
	If Alltrim(XTRB->ZAX_OK) == ""
	   XTRB->(RecLock( "XTRB", .F. ))          
	   XTRB->ZAX_OK   := cMarca 	
	   XTRB->(msUnlock())  
	   nVlrMar :=	nVlrMar+XTRB->ZAX_VALOR
			//msgStop("Diminuindo saldo Pois foi Marcado Aqui")
	Else
		nVlrSob := 0
		XTRB->(RecLock( "XTRB", .F. ))          
		XTRB->ZAX_OK   := Space(2)
		XTRB->(msUnlock())  
		nVlrMar :=	nVlrMar-XTRB->ZAX_VALOR
		//msgStop("Somando saldo Pois foi Marcado Aqui")
	EndIf
	onVlrMar:Refresh()
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return

User Function HPDEV01F(cMarca) 
//	dbSelectArea("XTRB")
	XTRB->(dbGoTop())
	While XTRB->(!Eof())
		If Alltrim(XTRB->ZAX_OK) == ""
			RecLock( "XTRB", .F. )          
			XTRB->ZAX_OK   := cMarca 	
			XTRB->(msUnlock())
			nVlrMar :=	nVlrMar+XTRB->ZAX_VALOR
			//msgStop("Diminuindo saldo Pois foi Marcado")
		Else
			XTRB->(RecLock( "XTRB", .F. ))          
			XTRB->ZAX_OK   := Space(2)
			XTRB->(msUnlock())
			nVlrMar :=	nVlrMar-XTRB->ZAX_VALOR
			//msgStop("Somonado o saldo Pois foi desMarcado")
		EndIf

		XTRB->(DBSKIP()) 
	End 
	onVlrMar:Refresh()
	OnVlrSob:Refresh()
	XTRB->(dbGoTop())	 
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return(Nil)

    
Static function EnvLib(cNumero,cNlib,xOpc) 
Local  aArea   := GetArea()  
Local cServer  := Rtrim(SuperGetMv("MV_RELSERV"))
Local cConta   := GetMV("MV_RELACNT") //ALLTRIM(cAccount)				// Conta de acesso
Local cSenhaTK := GetMV("MV_RELPSW") //ALLTRIM(cPassword)	        // Senha de acesso
LocaL cUsuMail := "" 
Local cFrom    := cConta
Local nPos1    := 0
Local cUsu_Mail:= "" 
Local _cEmail  := GetMV("HP_MAILFIN")
Local _ccEmai  := GetMV("HP_CMAILFI")
_dEmiss := ''
_cNome       :=  "Depto Financeiro"
criamv("HP_EMEILRE")
cUsuMail := GetMV("HP_EMEILRE")   //'000365';'000363''
nPos1:= 1
cTexto := "<html>"              
cTexto += "<head>"
cTexto += " <title></title>"
cTexto += "</head>"
cTexto += "<body>"
cTexto += " <br>"
cTexto += '<font color="#000000" face="Arial">'
cTexto += " <br>"
cTexto += " <br>"
cTexto += "  Ao Departamento Financeiro   <br><br>"
cTexto += " <br>"
cTexto += " <br>"
cTexto += "             A/C   "+_cNome+ " "
cTexto += " <br>"
cTexto += " <br>"
cTexto += " <br>"
cTexto += " <br>"
if xOpc == 2
   cTexto += "       A Nota de Cr�dito ao  Cliente de n�mero "+SE1->E1_NUM+", foi RECUSADA conforme abaixo: <br><br>"
Else   
   cTexto += "       A Nota de Cr�dito ao  Cliente de n�mero "+SE1->E1_NUM+", foi liberada conforme abaixo: <br><br>"
Endif   
cTexto += " <br>"
cTexto += " <br>"
cTexto += " <br>"
cTexto += "           C�digo Cliente  : "+SE1->E1_CLIENTE+" - Loja  "+SE1->E1_LOJA+" "
cTexto += " <br>"
cTexto += "           Raz�o Social/Nome    : "+SE1->E1_NOMCLI+" "
cTexto += " <br>"
cTexto += "           N�mero do Documento  : "+SE1->E1_NUM+" "
cTexto += " <br>"                       
cTexto += "           Emiss�o do Documento : "+dTOc(SE1->E1_EMISSAO)+" "
cTexto += " <br>"                       
cTexto += "           Valor do Documento   : "+Transform(SE1->E1_VALOR,'@e 999,999,999.99')+"  "
cTexto += " <br>"
cTexto += " <br>"
if xOpc == 2
   cTexto += "                       RECUSADO por : "+cGet1+ "  "
eLSE
   cTexto += "                       liberado por : "+cGet1+ "  " 
eNDIF
cTexto += '<font color="#FF0000" face="Arial">'     
cTexto += '<font color="#000000" face="Arial">'
cTexto += "       <br><br>"
cTexto += "Atenciosamente,<br><br>"
cTexto += "HOPE DO Nordeste Ltda.  <br>"
cTexto += "    Atendimento - Comercial  <br>"
cTexto += "           "+cGet1+"  <br>"
cTexto += "Fone : (11) 2169-2200 /  <br>"
cTexto += "</body>"
cTexto += "</html>"


		cAnexos := ""
		cProc:=""
		nErro:= 0
		//   aqui daniel para n�o enviar e-mails  
		
		cTo := _cEmail // "daniel.souza@terra.com.br"
		oMail := TMailManager():New()
		oMail:SetUseSSL(.F.) //.t.            
		x_smtp:= substr(Alltrim(GetMv("MV_WFSMTP")),1,len(alltrim(GetMv("MV_WFSMTP"))) ) 
		oMail:Init( '', x_smtp,Alltrim(GetMv("MV_RELACNT")),Alltrim(GetMv("MV_RELPSW")), 0, 587 )   //465
		oMail:SetSmtpTimeOut( 120 )
		nErro := oMail:SmtpConnect()
		If nErro <> 0
			conout( "ERROR: Conectando - " + oMail:GetErrorString( nErro ) )
			oMail:SMTPDisconnect()
			Iif(len(cProc)<> 0,cProc:= "Sem Conexao SMTP  ",cProc:=cProc )
		Endif
		nErro := oMail:SmtpAuth(Alltrim(GetMv("MV_RELACNT")) ,Alltrim(GetMv("MV_RELPSW")))
		If nErro <> 0
			conout( "ERROR:2 autenticando - " + oMail:GetErrorString( nErro ) )
			oMail:SMTPDisconnect()
			Iif(len(cProc)<> 0,cProc:= "Sem Autenticacao no E-mail  " ,cProc:=cProc )
		Endif
		oMessage := TMailMessage():New()
		oMessage:Clear()
		oMessage:cFrom 		:= Alltrim(GetMv("MV_RELACNT"))
		oMessage:cTo	 		:= cTo
		oMessage:cCc 			:= _ccEmai //"daniel.souza@terra.com.br"
		oMessage:cSubject    	:=  "Libera��o de Devolu��o "
		oMessage:cBody 	    	:= cTexto
		oMessage:MsgBodyType( "text/html" )
		If oMessage:AttachFile(cAnexos ) < 0
			Conout( "Erro ao atachar o arquivo" )
			Iif(len(cProc)<> 0,cProc:= "Sem anexos  " ,cProc:=cProc )
		Else
			oMessage:AddAtthTag( 'Content-Disposition: attachment; filename=' + cNomNovArq)
		End If
		nErro := oMessage:Send( oMail )
		oMail:SMTPDisconnect()
		
		//oPrint:ResetPrinter()
		//oPrint:= TMSPrinter():New( "Protocolo " )
		
		If nErro == 0
		    
			Conout( " E-mail ENVIADO COM SUCESSO" )
		Else
			Conout( "Erro ao enviar e-mail" )
		Endif
   //	else
//		Conout( "Arquivo PDF,n�o encontrado Erro ao atachar o arquivo" )
  //	Endif
	

RestArea(aArea)
Return Nil
    
Static Function CriaMV(_pcParametro)
dbSelectArea("SX6")
dbSetOrder(1)
_pcParametro:=padr(_pcParametro,10)
dbSeek(xfilial("SX6")+_pcParametro)
If (Eof() .Or. _pcParametro <> SX6->X6_VAR)
	If _pcParametro=="HP_USUADEV"
		RecLock("SX6",.T.)
		SX6->X6_FIL 	:= '  '
		SX6->X6_VAR 	:= _pcParametro
		SX6->X6_TIPO 	:= "C" // N D
		SX6->X6_DESCRIC := "USUARIOS COM AUTORIZA��O DE LIBERA��O DE DEVOLU��O"
		SX6->X6_DESC1   := "USUARIOS COM AUTORIZA��O DE LIBERA��O DE DEVOLU��O"
		SX6->X6_DESC2   := "USUARIOS COM AUTORIZA��O DE LIBERA��O DE DEVOLU��O"
		SX6->X6_CONTEUD := "000000;000093"
		SX6->X6_CONTENG := "000000;000093"
		SX6->X6_CONTSPA := "000000;000093"
		SX6->X6_PROPRI  := "U"
		SX6->X6_PYME    := "N"
		SX6->X6_VALID   := ""
		SX6->X6_INIT    := ""
		SX6->X6_DEFPOR  := ""
		SX6->X6_DEFSPA  := ""
		MsUnLock()
	ElseIf _pcParametro=="HP_USCOMER"
		RecLock("SX6",.T.)
		SX6->X6_FIL 	:= '  '
		SX6->X6_VAR 	:= _pcParametro
		SX6->X6_TIPO 	:= "C" // N D
		SX6->X6_DESCRIC := "USUARIOS COM AUTORIZA��O DE ALTERA��O COMERCIAL "
		SX6->X6_DESC1   := "USUARIOS COM AUTORIZA��O DE ALTERA��O COMERCIAL "
		SX6->X6_DESC2   := "USUARIOS COM AUTORIZA��O DE ALTERA��O COMERCIAL "
		SX6->X6_CONTEUD := "000000;000036;000165"
		SX6->X6_CONTENG := ""
		SX6->X6_CONTSPA := ""
		SX6->X6_PROPRI  := "U"
		SX6->X6_PYME    := "N"
		SX6->X6_VALID   := ""
		SX6->X6_INIT    := ""
		SX6->X6_DEFPOR  := ""
		SX6->X6_DEFSPA  := ""
		MsUnLock()
    ElseIf _pcParametro=="HP_USQUALID"
		RecLock("SX6",.T.)
		SX6->X6_FIL 	:= '  '
		SX6->X6_VAR 	:= _pcParametro
		SX6->X6_TIPO 	:= "C" // N D
		SX6->X6_DESCRIC := "USUARIOS COM AUTORIZA��O DE ALTERA��O QUALIDADE "
		SX6->X6_DESC1   := "USUARIOS COM AUTORIZA��O DE ALTERA��O QUALIDADE "
		SX6->X6_DESC2   := "USUARIOS COM AUTORIZA��O DE ALTERA��O QUALIDADE "
		SX6->X6_CONTEUD := "000000"
		SX6->X6_CONTENG := ""
		SX6->X6_CONTSPA := ""
		SX6->X6_PROPRI  := "U"
		SX6->X6_PYME    := "N"
		SX6->X6_VALID   := ""
		SX6->X6_INIT    := ""
		SX6->X6_DEFPOR  := ""
		SX6->X6_DEFSPA  := ""
		MsUnLock()
    ElseIf _pcParametro=="HP_USRECEB"
		RecLock("SX6",.T.)
		SX6->X6_FIL 	:= '  '
		SX6->X6_VAR 	:= _pcParametro
		SX6->X6_TIPO 	:= "C" // N D
		SX6->X6_DESCRIC := "USUARIOS COM AUTORIZA��O DE ALTERA��O COMERCIAL "
		SX6->X6_DESC1   := "USUARIOS COM AUTORIZA��O DE ALTERA��O COMERCIAL "
		SX6->X6_DESC2   := "USUARIOS COM AUTORIZA��O DE ALTERA��O COMERCIAL "
		SX6->X6_CONTEUD := "000000;000036;000165"
		SX6->X6_CONTENG := ""
		SX6->X6_CONTSPA := ""
		SX6->X6_PROPRI  := "U"
		SX6->X6_PYME    := "N"
		SX6->X6_VALID   := ""
		SX6->X6_INIT    := ""
		SX6->X6_DEFPOR  := ""
		SX6->X6_DEFSPA  := ""
		MsUnLock()
    ElseIf _pcParametro=="HP_EMEILRE"
		RecLock("SX6",.T.)
		SX6->X6_FIL 	:= '  '
		SX6->X6_VAR 	:= _pcParametro
		SX6->X6_TIPO 	:= "C" // N D
		SX6->X6_DESCRIC := "USUARIOS PARA RECEBER MENSAGEM DE LIBERA��O/EXCLUSAO "
		SX6->X6_DESC1   := "USUARIOS PARA RECEBER MENSAGEM DE LIBERA��O/EXCLUSAO "
		SX6->X6_DESC2   := "USUARIOS PARA RECEBER MENSAGEM DE LIBERA��O/EXCLUSAO "
		SX6->X6_CONTEUD := "000363;000365"
		SX6->X6_CONTENG := ""
		SX6->X6_CONTSPA := ""
		SX6->X6_PROPRI  := "U"
		SX6->X6_PYME    := "N"
		SX6->X6_VALID   := ""
		SX6->X6_INIT    := ""
		SX6->X6_DEFPOR  := ""
		SX6->X6_DEFSPA  := ""
		MsUnLock()

    Endif	

EndIf
Return


USER FUNCTION ModSB1()
	Local cQuery   := ""
    
    
    cQuery := " SELECT B1_POSIPI, B4_POSIPI, B1.R_E_C_N_O_ AS REGSB1 FROM SB1010 B1 , SB4010 B4 WITH (NOLOCK) "
    cQuery += " WHERE SUBSTRING(B1_COD,1,8)  = B4_COD  AND B4_POSIPI <> '' AND B1_POSIPI <> B4_POSIPI "
    cQuery += " AND B1.D_E_L_E_T_ = '' AND  B4.D_E_L_E_T_ = ''  " 
        
IF Select("XTRB") > 0
   XTRB->(dbCloseArea())
Endif      
TCQUERY cQuery NEW ALIAS xTRB 
XTRB->(DBGOTOP())
While XTRB->(!EOF())  
     DbSelectArea('SB1')
     SB1->(Dbgoto(XTRB->REGSB1))
     SB1->(RecLock("SB1",.F.))
     SB1->B1_POSIPI = XTRB->B4_POSIPI
	 SB1->(MsUnLock())
     XTRB->(dbskip()) 	 
Enddo
Return()

  //ZAX->ZAX_TPDEVO := ''
  //ZAX->ZAX_MOTIVO := ''
 // ZAX->ZAX_AUTORI := ''
 // ZAX->ZAX_FLUXOD := ''
  //ZAX->ZAX_NFORIG := ''  
 // ZAX->ZAX_EMIORI := ''
 // ZAX->ZAX_DIATRA := 0
/*
ZAX_QUALID 
ZAX_QTDPEC 
ZAX_CONFER
ZAX_1
ZAX_CONSER
ZAX_CONSE2
ZAX_CONSE3
ZAX_FALTA
ZAX_SOBRA
ZAX_PCUSAD
ZAX_DTAREV
ZAX_DIFERE
ZAX_DTRECE
ZAX_DTAQPA
ZAX_ACAOCO
ZAX_POSTAG
ZAX_CODOBJ
ZAX_PEDIDO
ZAX_NFDVCO
ZAX_DTALIB
ZAX_OBSERV
*/
    