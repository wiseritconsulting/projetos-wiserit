#include "topconn.ch"
#INCLUDE "PROTHEUS.CH"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HESTR001  �Autor  �Bruno Parreira      � Data �  13/02/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Relatorio de ordem de abastecimento.                        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HESTR001(cDAP)                    

//��������������������������������������������������������������Ŀ
//� Define Variaveis                                             �
//����������������������������������������������������������������
Local cDesc1 	:= "Este relatorio tem o objetivo de informar para os usu�rios do"
Local cDesc2 	:= "estoque, quais itens dever�o ser transferidos do pulmao"
Local cDesc3 	:= "para o picking."
Local cString	:= "SZM"
Local cPerg     := "HESTR001"

Private tamanho	:= "P"
Private cbCont,cabec1,cabec2,cbtxt
Private aOrd	:= {}//{"Endere�o"}
Private aReturn := {"Zebrado",1,"Administracao", 2, 2, 1, "",0 }
Private nomeprog:= "HESTR001",nLastKey := 0
Private li		:= 80, limite:=132, lRodape:=.F.
Private wnrel  	:= "HESTR001"
Private titulo 	:= "Ordem de abastecimento"
Private cSerIni := ""
Private cSerFin := ""
Private nLin 	:= 100
Private nCol 	:= 60
Private nPula 	:= 60
Private	nfim    := 2400
Private imprp	:= .F.
Private a_cols 	:= {50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000}

Private	oFont14	 := TFont():New("Arial",,14,,.f.,,,,,.f.)
Private oFont14n := TFont():New("Arial",,14,,.t.,,,,,.f.)
Private oFont09  := TFont():New("Arial",,10,,.f.,,,,,.f.)
Private oFont09n := TFont():New("Arial",,10,,.t.,,,,,.f.)
Private oFont10  := TFont():New("Arial",,10,,.f.,,,,,.f.)
Private oFont10n := TFont():New("Arial",,10,,.t.,,,,,.f.)
Private oFont12  := TFont():New("Arial",,12,,.f.,,,,,.f.)
Private oFont12n := TFont():New("Arial",,12,,.t.,,,,,.f.)

cbtxt   := SPACE(10)
cbcont  := 0
Li      := 80
m_pag   := 1
cabec2  := ""
cabec1  := ""

If Empty(cDap)
	AjustaSX1(cPerg)
	
	If !Pergunte(cPerg,.T.)
		Return
	EndIf
Else
	mv_par01 := cDap
	mv_par02 := cDap
	mv_par03 := 2	
EndIf

wnrel:=SetPrint(cString,wnrel,cPerg,@Titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.F.,Tamanho)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
	Return
Endif

oPrn 	 := TMSPrinter():New()

Processa({||ImpRel()}, titulo, "Imprimindo, aguarde...")

If	( aReturn[5] == 1 ) //1-Disco, 2-Impressora
	oPrn:Preview()
Else
	oPrn:Setup()
	oPrn:Print()
EndIf

QRY->(DbCloseArea())

Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � ImpRel   � Autor � Bruno Parreira        � Data � 07/02/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Chamada do Relatorio                                       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � HESTR001	                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpRel()
Local cQuery := ""

cQuery := "select ZM_LOTDAP,ZM_AMZORIG,ZM_AMZDEST,ZM_PRODUTO,B1_DESC,B1_UM,ZM_ENDORIG,ZM_ENDDEST,ZM_NUMLOTE,ZM_DATA,ZM_HORA,ZM_EMPILHA,SUM(ZM_QUANT) AS ZM_QUANT "
cQuery += CRLF + "from "+RetSqlName("SZM")+" (NOLOCK) SZM "
//cQuery += CRLF + "inner join "+RetSqlName("SZJ")+" SZJ "
//cQuery += CRLF + "on ZJ_NUMPF = ZM_NUMPF "
//cQuery += CRLF + "and ZJ_ITEM = ZM_ITEM "
//cQuery += CRLF + "and SZJ.D_E_L_E_T_ = '' "
cQuery += CRLF + "inner join "+RetSqlName("SB1")+" (NOLOCK) SB1 "
cQuery += CRLF + "on B1_COD = ZM_PRODUTO "
cQuery += CRLF + "and SB1.D_E_L_E_T_ = '' "
cQuery += CRLF + "where ZM_LOTDAP between '"+mv_par01+"' and '"+mv_par02+"' "
If mv_par03 = 2
	cQuery += CRLF + "and ZM_QTDTRAN = 0 "
EndIf
cQuery += CRLF + "group by ZM_LOTDAP,ZM_AMZORIG,ZM_AMZDEST,ZM_PRODUTO,B1_DESC,B1_UM,ZM_ENDORIG,ZM_ENDDEST,ZM_NUMLOTE,ZM_DATA,ZM_HORA,ZM_EMPILHA "
cQuery += CRLF + "order by ZM_LOTDAP,ZM_EMPILHA,ZM_AMZORIG,ZM_ENDORIG,ZM_NUMLOTE "

memowrite("HESTR001.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"QRY", .F., .T.)

DbSelectArea("QRY")
DbGoTop()

n_cont := 0

If QRY->(!EOF())
	While QRY->(!EOF())
		n_cont++
		QRY->(DbSkip())
	EndDo
	DbGoTop()
	ProcRegua(n_cont)
	NPAG := 1
Else
	MsgAlert("N�o h� dados a serem impressos.","Aten��o")
	Return	
EndIf

nTotQtd := 0
cLOTDAP  := ""
cEmpilha := ""
aColItens := {080,320,1500,1800,2100}

While QRY->(!Eof())
	
	IncProc()
	
	If cLOTDAP <> QRY->ZM_LOTDAP .Or. Li > 30 .Or. QRY->ZM_EMPILHA <> cEmpilha   
		CabecSep()
		Li := 0
	EndIf

	cEndOrig := QRY->ZM_ENDORIG
	cEndDest := QRY->ZM_ENDDEST
	cProduto := QRY->ZM_PRODUTO+" - "+SubStr(QRY->B1_DESC,1,35)
	cRecip   := QRY->ZM_NUMLOTE
	nQuant   := QRY->ZM_QUANT
	
	oPrn:Say(nLin,aColItens[1],cEndOrig,oFont09,100)
	oPrn:Say(nLin,aColItens[2],cProduto,oFont09,100)
	oPrn:Say(nLin,aColItens[3],cEndDest,oFont09,100)
	oPrn:Say(nLin,aColItens[4],cRecip,oFont09,100)
	oPrn:Say(nLin,aColItens[5],Transform(nQuant,"@E 9,999,999")+" "+QRY->B1_UM,oFont09,100)
	
	oPrn:Box(nLin,2360,nLin+40,nfim)
	
	nLin += nPula
	Li++
	
	cLOTDAP  := QRY->ZM_LOTDAP
	cEmpilha := QRY->ZM_EMPILHA
	nTotQtd  += QRY->ZM_QUANT
	
	QRY->(DbSkip())
	
	If cLOTDAP <> QRY->ZM_LOTDAP .Or. Li > 30 .Or. QRY->ZM_EMPILHA <> cEmpilha 
		oPrn:Say(nLin,aColItens[5]-150,"Total:",oFont09n,100)
		oPrn:Say(nLin,aColItens[5],Transform(nTotQtd,"@E 9,999,999"),oFont09n,100)
		oPrn:Box(nLin-10,50,nLin+50,nfim)
		nTotQtd := 0
	EndIf
	
EndDo

MS_FLUSH()

Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �CabecSep  � Autor � Bruno Parreira        � Data � 07/02/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Imprime o cabecalho do relatorio                           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � HESTR001	                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CabecSep()
Local aArea := GetArea()
Local nx    := 0
Local cEmp  := ""
nLin	:= 100
nCol	:= 60
nPula	:= 60
nEsp    := 350
nPosTit := 60

aColCab := {80,1000,2050}
aTit    := {"End. Origem","Produto","End. Destino","Recipiente","Quantidade"}
aColTit := AClone(aColItens)

If QRY->ZM_EMPILHA = "01"
	cEmp := "Empilhador 01"
ElseIf QRY->ZM_EMPILHA = "02"
	cEmp := "Empilhador 02"
ElseIf QRY->ZM_EMPILHA = "03"
	cEmp := "Abastecedor"
EndIf		

oPrn:EndPage()

oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)
nLin += nPula

oPrn:Say(nLin,aColCab[3]-50,dtoc(date())+" - "+Time(),oFont10,100)

nLin += nPula*2

oPrn:Say(nLin,aColCab[1],"C�digo:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->ZM_LOTDAP,oFont10,100)
oPrn:Say(nLin,aColCab[2],"Empilhador:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,cEmp,oFont10,100)
MSBAR('CODE128',2.3,16.5,QRY->ZM_LOTDAP,oPrn,.F.,,.T.,0.040,1.1,,,,.F.)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Data de Abertura:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,DtoC(StoD(QRY->ZM_DATA))+" - "+QRY->ZM_HORA,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Usu�rio:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,__cUserID+" - "+UsrRetName(__cUserID),oFont10,100)
oPrn:Say(nLin,aColCab[3],QRY->ZM_LOTDAP,oFont14n,100)

nLin += nPula
nLin += nPula

oPrn:Box(nLin-10,50,nLin+50,nfim)

For nx := 1 to Len(aTit)
	oPrn:Say(nLin,aColTit[nx],aTit[nx],oFont10n,100)
Next

nLin += nPula
imprp	:= .T.

RestArea(aArea)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data �  05/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria perguntas da rotina.                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Lote DAP de          ","","","Mv_ch1","C",6,0,0,"G","",   "","","N","Mv_par01",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o lote DAP de.",""},{""},{""},"")
PutSx1(cPerg,"02","Lote DAP ate         ","","","Mv_ch2","C",6,0,0,"G","",   "","","N","Mv_par02",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o lote DAP ate.",""},{""},{""},"")
PutSx1(cPerg,"03","Imprime transferidos?","","","Mv_ch3","N",1,0,2,"C","",   "","","N","Mv_par03",       "Sim","","","",      "Nao","","",     "","","","","","","","","","","","","","","","",{"Imprime itens j� transferidos?",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)