#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

User Function HEX_PXMLPR()
	Local cEoL			:= +Chr(13)+Chr(10)
	Local qry1			:= ""
	Local qry2			:= ""
	Local qry3			:= ""
	Local qry4			:= ""
	Local lAtualiza1	:= .T.
	
	Prepare Environment Empresa "01" Filial "0101"

Local cIdEnt   	 := ""
Local aPerg   	 := {}
Local aParam  	 := {Space(If (TamSx3("F2_SERIE")[1] == 14,Len(SF2->F2_SDOC),Len(SF2->F2_SERIE))),Space(Len(SF2->F2_DOC)),Space(Len(SF2->F2_DOC)),Space(60),CToD(""),CToD(""),Space(14),Space(14)}
Local cParNfeExp := SM0->M0_CODIGO+SM0->M0_CODFIL+"SPEDNFEEXP"
Local lObrigat   := .F. 
Local lUsaColab  := .F.
Local lCTe       := (FunName()$"SPEDCTE,TMSA500,TMSA200,TMSAE70,TMSA050")
Local lSdoc     := TamSx3("F2_SERIE")[1] == 14

DEFAULT nTipo  := 1
lUsaColab := UsaColaboracao( IIF(lCte,"2","1") )

lObrigat:=If(nTipo == 2,lObrigat:=.F.,.T.)

aadd(aPerg,{1,STR0010,aParam[01],"",".T.","",".T.",30,.F.}) //"Serie da Nota Fiscal"
aadd(aPerg,{1,STR0011,aParam[02],"",".T.","",".T.",30,lObrigat}) //"Nota fiscal inicial"
aadd(aPerg,{1,STR0012,aParam[03],"",".T.","",".T.",30,lObrigat}) //"Nota fiscal final"
aadd(aPerg,{6,STR0119,aParam[04],"",".T.","!Empty(mv_par04)",80,.T.,"Arquivos XML |*.XML","",GETF_RETDIRECTORY+GETF_LOCALHARD,.T.}) //"Diretório de destino"
aadd(aPerg,{1,STR0141,aParam[05],"",".T.","",".T.",50,lObrigat}) //"Data Inicial"
aadd(aPerg,{1,STR0142,aParam[06],"",".T.","",".T.",50,lObrigat}) //"Data Final"

If nTipo == 1 .Or. nTipo == 3
	aadd(aPerg,{1,STR0143,aParam[07],"",".T.","",".T.",50,.F.}) //"CNPJ Inicial"
	aadd(aPerg,{1,STR0144,aParam[08],"",".T.","",".T.",50,.F.}) //"CNPJ final"
EndIF

aParam[01] := ParamLoad(cParNfeExp,aPerg,1,aParam[01])
aParam[02] := ParamLoad(cParNfeExp,aPerg,2,aParam[02])
aParam[03] := ParamLoad(cParNfeExp,aPerg,3,aParam[03])
aParam[04] := ParamLoad(cParNfeExp,aPerg,4,aParam[04])

aParam[05] := ParamLoad(cParNfeExp,aPerg,5,aParam[05])
aParam[06] := ParamLoad(cParNfeExp,aPerg,6,aParam[06])

If nTipo == 1 .Or. nTipo == 3
	aParam[07] := ParamLoad(cParNfeExp,aPerg,7,aParam[07])
	aParam[08] := ParamLoad(cParNfeExp,aPerg,8,aParam[08])
EndIF

 SpedPExp(cIdEnt,aParam[01],aParam[02],aParam[03],aParam[04],lEnd,aParam[05],IIF(Empty(aParam[06]),dDataBase,aParam[06]),aParam[07],aParam[08],nTipo,,If(lSdoc,aParam[Len(aParam)],aParam[1]))

	
	Reset Environment
	
Return