#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HWSRK001  �Autor  �Bruno Parreira      � Data �  01/12/16   ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATP010()

	//public cPedRak  := ""
	// TODO Mudan�a da variavel cPedRak para cPedWeb, por medidas de padroniza��o. [WESKLEY SILVA. 22/05/18]
	public cPedWeb  := ""
		
	Processa({|| U_HFAT10A(cPedWeb) },"Processando...","Processando Pedidos ITMobile...")

Return

User Function HFAT10A()

	Local aWsRet   	:= {}
	Local nx,ny    	:= 0
	Local cNroPed  	:= ""
	Local cCGC    	:= ""
	Local cCodCli  	:= ""
	Local cLojCli  	:= ""
	Local cNovCli  	:= ""
	Local cTipPes  	:= ""
	Local cNomCli  	:= ""
	Local nPosP    	:= 0
	Local cProd    	:= ""
	Local cRef     	:= ""
	Local cCor     	:= ""
	Local cTam     	:= ""
	Local nErro    	:= 0
	Local nGerados 	:= 0
	Local cCanal   	:= ""
	Local cPlB2BV  	:= ""
	Local cTpB2BV  	:= ""
	Local cPlB2BF  	:= ""
	Local cTpB2BF  	:= ""
	Local cPlB2BH  	:= ""
	Local cTpB2BH  	:= ""
	Local cPlB2BL  	:= ""
	Local cTpB2BL  	:= ""
	Local cPlB2B1  	:= ""
	Local cTpB2B1  	:= ""
	Local cNature  	:= ""
	Local cPolitc  	:= ""
	Local cTipPed  	:= ""
	Local cCndPag  	:= ""
	Local cCdEnd   	:= ""
	Local cCdMun   	:= ""
	Local cCdEnt   	:= ""
	Local cCdHop   	:= ""
	Local cCdGer   	:= ""
	Local cCdTip   	:= ""
	Local cCdGrp   	:= ""
	Local cCdDiv   	:= ""
	Local cCdReg   	:= ""
	Local cCdMac   	:= ""
	Local cNtB2C   	:= ""
	Local cNmCan   	:= ""
	Local cNmTip   	:= ""
	Local cNmGrp   	:= ""
	Local cNmDiv   	:= ""
	Local cNmReg   	:= ""
	Local cNmMac   	:= ""
	Local cEndere  	:= ""
	Local cComple  	:= ""
	Local cBairro  	:= ""
	Local cEstado  	:= ""
	Local cCEP     	:= ""
	Local cMunici  	:= ""
	Local cEmail   	:= ""
	Local cDDDTel  	:= ""
	Local cNumTel  	:= ""
	Local cDDDCel  	:= ""
	Local cNumCel  	:= ""
	Local cTime    	:= ""
	Local cDtTime  	:= ""
	Local cTpOper  	:= "01"
	Local cTES     	:= ""
	Local cCodPed  	:= ""
	Local nParce   	:= 0
	Local cCdPrz   	:= ""
	Local cBloqSLD
	Local cBloqSTA
	Local lBloqDEM
    Local dDatEntr  	:= ctod(space(08))
	Private cChvA1		:= ""
	Private cChvA2		:= ""
	Private cLog		:= ""
	Private cAutVdSLD	:= .F.
	Private cAutVdSTA	:= .F.
	Private cErroSLD	:= .F.
	Private cErroSTA	:= .F.
	Private lMsErroAuto	:= .F.

	Prepare Environment Empresa "01" Filial "0101"
	

	While .T.

		aWsRet   	:= {}
		nx			:= 0
		ny    		:= 0
		cNroPed  	:= ""
		cCGC    	:= ""
		cCodCli  	:= ""
		cLojCli  	:= ""
		cNovCli  	:= ""
		cTipPes  	:= ""
		cNomCli  	:= ""
		nPosP    	:= 0
		cProd    	:= ""
		cRef     	:= ""
		cCor     	:= ""
		cTam     	:= ""
		nErro    	:= 0
		nGerados 	:= 0
		cCanal   	:= ""
		cPlB2BV  	:= ""
		cTpB2BV  	:= ""
		cPlB2BF  	:= ""
		cTpB2BF  	:= ""
		cPlB2BH  	:= ""
		cTpB2BH  	:= ""
		cPlB2BL  	:= ""
		cTpB2BL  	:= ""
		cPlB2B1  	:= ""
		cTpB2B1  	:= ""
		cNature  	:= ""
		cPolitc  	:= ""
		cTipPed  	:= ""
		cCndPag  	:= ""
		cCdEnd   	:= ""
		cCdMun   	:= ""
		cCdEnt   	:= ""
		cCdHop   	:= ""
		cCdGer   	:= ""
		cCdTip   	:= ""
		cCdGrp   	:= ""
		cCdDiv   	:= ""
		cCdReg   	:= ""
		cCdMac   	:= ""
		cNtB2C   	:= ""
		cNmCan   	:= ""
		cNmTip   	:= ""
		cNmGrp   	:= ""
		cNmDiv   	:= ""
		cNmReg   	:= ""
		cNmMac   	:= ""
		cEndere  	:= ""
		cComple  	:= ""
		cBairro  	:= ""
		cEstado  	:= ""
		cCEP     	:= ""
		cMunici  	:= ""
		cEmail   	:= ""
		cDDDTel  	:= ""
		cNumTel  	:= ""
		cDDDCel  	:= ""
		cNumCel  	:= ""
		cTime    	:= ""
		cDtTime  	:= ""
		cTpOper  	:= "01"
		cTES     	:= ""
		cCodPed  	:= ""
		Parce   	:= 0
		cCdPrz   	:= ""
		dDatEntr  	:= ctod(space(08))
		cChvA1		:= ""
		cChvA2		:= ""
		cLog		:= ""
		cAutVdSLD	:= .F.
		cAutVdSTA	:= .F.
		cErroSLD	:= .F.
		cErroSTA	:= .F.
		lMsErroAuto	:= .F.

		cBloqSLD	:= SuperGetMV("MV_HBLQSLD",.F.,".F.")    //Bloqueia itens com Saldo inferior ao item solicitado
		cBloqSTA	:= SuperGetMV("MV_HBLQSTA",.F.,".F.")    //Bloqueia itens por Status
		lBloqDEM	:= SuperGetMV("MV_HBLQDEM",.F.,".T.")    //Bloqueia itens por Demanda
		
		cTime   	:= Time()
		//cDtTime 	:= DTOS(ddatabase) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
		cDtTime 	:= DTOS(date()) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
	
		GravaLog("In�cio do processamento...","INICIO")
	
		_qry := "SELECT DT_EMISSAO as EMISS, DT_EMISSAO_PED as DT_PED, * FROM [MOBILE].[dbo].[PEDIDO_WEB] where PEDIDO_SIGA = '' and COMPLETO = 'T' AND ERRO='' order by NUM_PED "
	
		If Select("TMPPED") > 0
			TMPPED->(DbCloseArea())
		Endif
	
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPPED",.T.,.T.)
	
		cNroPed := GetSX8Num("SC5","C5_NUM",,1)
		DbSelectArea("SC5")
		DbSetOrder(1)
	
		While DbSeek(xfilial("SC5")+cNroPed)
			ConfirmSX8()
			cNroPed := SOMA1(cNroPed)
		End
	
		DbSelectArea("TMPPED")
		DbGoTop()
		ProcRegua(RecCount())
	
		While !EOF()
	
			IncProc()
	
			//cPedRak := StrZero(TMPPED->NUM_PED,6)
			
			cPedWeb := StrZero(TMPPED->NUM_PED,6)
	
			GravaLog("Iniciando a importa��o do pedido: "+cPedWeb)
	
			DbSelectArea("SA1")
			DbSetOrder(1)
			DbSeek(xFilial("SA1")+TMPPED->COD_CLIENTE+TMPPED->LOJA_CLI)
			cCodCli := SA1->A1_COD
			cLojCli := SA1->A1_LOJA
	
			GravaLog("Cliente "+cCodCli+"-"+cLojCli+" encontrado no Protheus. Pedido: "+cPedWeb)
	
			cNature := ""
	
			If !Empty(SA1->A1_NATUREZ)
				cNature := SA1->A1_NATUREZ
			EndIf
	
			cCanal 		:= AllTrim(SA1->A1_XCANAL)
			cCndPag 	:= TMPPED->COD_COND_PAGTO
			nDesconto	:= POSICIONE("SE4",1,xFilial("ZE4")+cCndPag,"E4_DESCFIN")
			cCdPrz 		:= TMPPED->COD_PRAZO
			dDtEntrega	:= TMPPED->EMISS  //dtos(dDataBase+ POSICIONE("SZ7",1,xFilial("SZ7")+cCdPrz,"Z7_QTDDIA"))
			cPolitc		:= TMPPED->COD_POLITICA
			cTiposDesc  := ""
			cPercRepre	:= POSICIONE("SA3",1,xFilial("ZA3")+TMPPED->COD_REPRES,"A3_COMIS")
			cGerente	:= POSICIONE("ZA4",1,xFilial("ZA4")+SA1->A1_XMICRRE,"ZA4_GEREN")
			cPercGer	:= POSICIONE("SA3",1,xFilial("ZA3")+cGerente,"A3_COMIS2")
			cSuperv		:= POSICIONE("ZA4",1,xFilial("ZA4")+SA1->A1_XMICRRE,"ZA4_SUPERV")
			cPercSuper	:= POSICIONE("SA3",1,xFilial("ZA3")+cSuperv,"A3_COMIS3")
			cTpPed		:= TMPPED->TIPO_PEDIDO
			cTpOper		:= Posicione("SZ1",1,xFilial("SZ1")+cTpPed,"Z1_TPOPER")
	
			If cTpOper == ""
				cTpOper := "01"
			End
	
			For nx := 1 to len(TMPPED->INSTRUCAO) step 3
				cAux2 := SubStr(TMPPED->INSTRUCAO,nx,2)
				If alltrim(cAux2) <> ""
					cTiposDesc += Alltrim(POSICIONE("SX5",1,xFilial("SX5")+'Z4'+cAux2,"X5_DESCRI"))+"|"
				Endif
			Next
			cTiposDesc	:= SubStr(cTiposDesc,1,Len(cTiposDesc)-1)
	
			_qry := "SELECT SUM(C6_VALOR) AS TOTAL FROM [MOBILE].[dbo].[PEDIDO_ITEM_WEB] where NUM_PED = "+alltrim(str(val(cPedWeb)))
	
			If Select("TMPVTOT") > 0
				TMPVTOT->(DbCloseArea())
			Endif
	
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPVTOT",.T.,.T.)
	
			DbSelectArea("TMPVTOT")
	
			//nPercDesc := nDesconto //Round(((nDesconto / TMPVTOT->TOTAL) * 100),2)
			nPercDesc 	:= u_pegadesc(cCodCli,cLojCli,nDesconto)
	
			aCabec := {}
	
			aadd(aCabec,{"C5_NUM"    	, cNroPed,Nil})
			aadd(aCabec,{"C5_TIPO"   	, "N",Nil})
			aadd(aCabec,{"C5_CLIENTE"	, cCodCli,Nil})
			aadd(aCabec,{"C5_LOJACLI"	, cLojCli,Nil})
			aadd(aCabec,{"C5_CLIENT" 	, cCodCli,Nil})
			aadd(aCabec,{"C5_LOJAENT"	, cLojCli,Nil})
//			aadd(aCabec,{"C5_EMISSAO"	, date(),Nil})	//aadd(aCabec,{"C5_EMISSAO"	, dDatabase,Nil}) // stod(TMPPED->DT_EMISSAO),Nil}) 
			aadd(aCabec,{"C5_EMISSAO"	, stod(TMPPED->DT_PED),Nil})
			aadd(aCabec,{"C5_POLCOM" 	, cPolitc,Nil})
			aadd(aCabec,{"C5_TPPED"  	, cTpPed,Nil})
			aadd(aCabec,{"C5_TABELA" 	, TMPPED->COD_TAB_PRECO,Nil})
			aadd(aCabec,{"C5_CONDPAG"	, cCndPag,Nil})
			aadd(aCabec,{"C5_NATUREZ"	, cNature,Nil})
			//TODO Altera��o do numero de pedido da IT Mobile para o campo C5_XPEDWEB [WESKLEY SILVA 22/05] 
			//aadd(aCabec,{"C5_XPEDRAK"	, cPedRak,Nil})
			aadd(aCabec,{"C5_XPEDWEB"	, cPedWeb,Nil})
			aadd(aCabec,{"C5_DESC1"		, nPercDesc,Nil})	//aadd(aCabec,{"C5_DESC1"  ,TMPPED->DESCONTO,Nil})
			aadd(aCabec,{"C5_XOBSINT"	, TMPPED->OBSERVACAO,Nil})
			aadd(aCabec,{"C5_XCANAL" 	, SA1->A1_XCANAL,Nil})
			aadd(aCabec,{"C5_XCANALD"	, SA1->A1_XCANALD,Nil})
			aadd(aCabec,{"C5_XCODREG"	, SA1->A1_XCODREG,Nil})
			aadd(aCabec,{"C5_XDESREG"	, SA1->A1_XDESREG,Nil})
			aadd(aCabec,{"C5_XMICRRE"	, SA1->A1_XMICRRE,Nil})
			aadd(aCabec,{"C5_XMICRDE"	, SA1->A1_XMICRDE,Nil})
			aadd(aCabec,{"C5_PRAZO"  	, cCdPrz,Nil})
			aadd(aCabec,{"C5_VEND1"  	, TMPPED->COD_REPRES,Nil})
			aadd(aCabec,{"C5_COMIS1" 	, cPercRepre,Nil})
			aadd(aCabec,{"C5_VEND2"  	, cGerente,Nil})
			aadd(aCabec,{"C5_COMIS2" 	, cPercGer,Nil})
			aadd(aCabec,{"C5_VEND3"  	, cSuperv,Nil})
			aadd(aCabec,{"C5_COMIS3" 	, cPercSuper,Nil})
	//		aadd(aCabec,{"C5_NOMVEND"	, POSICIONE("SA3",1,xFilial("SA3")+TMPPED->COD_REPRES,"A3_NOME"),Nil})
			aadd(aCabec,{"C5_FECENT" 	, stod(dDtEntrega),Nil})
			aadd(aCabec,{"C5_ORIGEM" 	, 'ITMOBILE',Nil})
			aadd(aCabec,{"C5_XPEDMIL"	, AllTrim(TMPPED->PEDIDO_MILLENNIUM),Nil})
			aadd(aCabec,{"C5_XPEDCLI"	, AllTrim(TMPPED->PEDIDO_MILLENNIUM),Nil})
			aadd(aCabec,{"C5_XCDINST"	, AllTrim(TMPPED->INSTRUCAO),Nil})
			aadd(aCabec,{"C5_XINSTRU"	, SubStr(cTiposDesc,1,250),Nil})
	
			Conout(aCabec[5,2])
	        dDatEntr := stod(TMPPED->EMISS)  
	        If dDatEntr < date()  //If dDatEntr < dDatabase
	           dDatEntr := date()	//dDatEntr := dDatabase
	        Endif   
			/**************************************************************/
			/* Verifica Saldo e Status dos Itens do Produto               */
			/**************************************************************/
	
			_qry := "SELECT * FROM [MOBILE].[dbo].[PEDIDO_ITEM_WEB] where NUM_PED = "+alltrim(str(val(cPedWeb)))+" order by SUBSTRING(PRODUTO,1,8),PRECO_VENDA,PRODUTO "
	
			cQuery := ChangeQuery(_qry)
	
			If Select("TMPITEM1") > 0
				TMPITEM1->(dbclosearea())
			EndIf
	
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPITEM1",.T.,.T.)
	
			DbSelectArea("TMPITEM1")
			DbGoTop()
	
			cErroSLD	:= .F.
			cErroSTA	:= .F.
			lErroDEM    := .F.
			cMsgDem := ""
	
			While TMPITEM1->(!EOF())
	
				cItem2	:= ITEMCOD
				cProd	:= TMPITEM1->PRODUTO
				cQtd	:= TMPITEM1->QUANTIDADE
				cBlqDem := AllTrim(GetAdvFVal("SB1","B1_YBLQDEM",xFilial("SB1")+TMPITEM1->PRODUTO,1,""))
	
				_qry := "SELECT SALDO,SITUACAO FROM [MOBILE]..[VIEW_SALDOS] WHERE COD_PRODUTO='"+cProd+"' "
	
				cQuery := ChangeQuery(_qry)
	
				If Select("TMPSLD") > 0
					TMPSLD->(dbclosearea())
				EndIf
	
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cquery),"TMPSLD",.T.,.T.)
				DbSelectarea("TMPSLD")
	
				cAutVdSLD	:= .F.
				cAutVdSTA	:= .F.
				lAutVdDEM	:= .F.
					
				If cBloqSLD
					If TMPSLD->SALDO < cQtd
						cAutVdSLD	:= .T.
						cErroSLD	:= .T.
						_qry := "UPDATE [MOBILE].[dbo].[PEDIDO_ITEM_WEB] SET ERRO = 'SALDO' WHERE NUM_PED = "+alltrim(str(val(cPedWeb)))+" AND ITEMCOD = "+alltrim(str(cItem2))
						TcSqlExec(_qry)
					EndIf
				EndIf
	
				If cBloqSTA
					If AllTrim(TMPSLD->SITUACAO) == 'PNV'
						cAutVdSTA	:= .T.
						cErroSTA	:= .T.
						_qry := "UPDATE [MOBILE].[dbo].[PEDIDO_ITEM_WEB] SET ERRO = 'STAT/PNV' WHERE NUM_PED = "+alltrim(str(val(cPedWeb)))+" AND ITEMCOD = "+alltrim(str(cItem2))
						TcSqlExec(_qry)
					ElseIf AllTrim(TMPSLD->SITUACAO) == 'NPNV'
						cAutVdSTA	:= .T.
						cErroSTA	:= .T.
						_qry := "UPDATE [MOBILE].[dbo].[PEDIDO_ITEM_WEB] SET ERRO = 'STAT/NPNV' WHERE NUM_PED = "+alltrim(str(val(cPedWeb)))+" AND ITEMCOD = "+alltrim(str(cItem2))
						TcSqlExec(_qry)
					ElseIf AllTrim(TMPSLD->SITUACAO) == 'PDSV'
						cAutVdSTA	:= .T.
						cErroSTA	:= .T.
						_qry := "UPDATE [MOBILE].[dbo].[PEDIDO_ITEM_WEB] SET ERRO = 'STAT/PDSV' WHERE NUM_PED = "+alltrim(str(val(cPedWeb)))+" AND ITEMCOD = "+alltrim(str(cItem2))
						TcSqlExec(_qry)
					EndIf
				EndIf
				
				If lBloqDEM
					If cBlqDem = "S"
						lAutVdDEM := .T.
						lErroDEM  := .T.
						cMsgDem += CRLF + TMPITEM1->PRODUTO
						_qry := "UPDATE [MOBILE].[dbo].[PEDIDO_ITEM_WEB] SET ERRO = 'DEMANDA' WHERE NUM_PED = "+alltrim(str(val(cPedWeb)))+" AND ITEMCOD = "+alltrim(str(cItem2))
						TcSqlExec(_qry)
					EndIf
				EndIf
	
				If !cAutVdSTA .and. !cAutVdSLD .and. !lAutVdDEM
					_qry := "UPDATE [MOBILE].[dbo].[PEDIDO_ITEM_WEB] SET ERRO = '' WHERE NUM_PED = "+alltrim(str(val(cPedWeb)))+" AND ITEMCOD = "+alltrim(str(cItem2))
					TcSqlExec(_qry)
				EndIf
				
				
	
				TMPITEM1->(DbSkip())
	
			EndDo
	
	
			/**************************************************************/
			/* Grava o Status de erro no Pedido ou limpa se Foi Corrigido */
			/**************************************************************/
	
			If  cErroSLD .and. cErroSTA
	
				GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedWeb+" - Saldo e Status Indisponivel")
				_qry := "update [MOBILE].[dbo].[PEDIDO_WEB] set ERRO = 'SALDO/STATUS' where PEDIDO_SIGA = '' and NUM_PED = "+str(val(cPedWeb))+" "
				TcSqlExec(_qry)
	
			ElseIf cErroSLD
	
				GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedWeb+" - Saldo Indisponivel")
				_qry := "update [MOBILE].[dbo].[PEDIDO_WEB] set ERRO = 'SALDO' where PEDIDO_SIGA = '' and NUM_PED = "+str(val(cPedWeb))+" "
				TcSqlExec(_qry)
	
			ElseIf cErroSTA
	
				GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedWeb+" - Status Indisponivel")
				_qry := "update [MOBILE].[dbo].[PEDIDO_WEB] set ERRO = 'STATUS' where PEDIDO_SIGA = '' and NUM_PED = "+str(val(cPedWeb))+" "
				TcSqlExec(_qry)
			
			ElseIf lErroDEM
	
				GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedWeb+" - Bloqueado por demanda. Produtos:"+cMsgDem)
				_qry := "update [MOBILE].[dbo].[PEDIDO_WEB] set ERRO = 'DEMANDA' where PEDIDO_SIGA = '' and NUM_PED = "+str(val(cPedWeb))+" "
				TcSqlExec(_qry)
				
			Else
	
				_qry := "update [MOBILE].[dbo].[PEDIDO_WEB] set ERRO = '' where PEDIDO_SIGA = '' and NUM_PED = "+str(val(cPedWeb))+" "
				TcSqlExec(_qry)
	
			End
	
	
			/**************************************************************/
			/* Inclus�o dos itens no Acols de Produtos para importa��o    */
			/**************************************************************/
	
			_qry := " SELECT ITEM.ITEMCOD, ITEM.NUM_PED, ITEM.ITEM, ITEM.PRODUTO, ITEM.UNIDADE, ITEM.QUANTIDADE, ITEM.PRECO_VENDA, "
			_qry += " 			ITEM.C6_VALOR, ITEM.SKU, ITEM.ERRO "
			_qry += " FROM [MOBILE]..[PEDIDO_ITEM_WEB] AS ITEM "
			_qry += " 		INNER JOIN [MOBILE]..[PEDIDO_WEB] as PED ON PED.NUM_PED=ITEM.NUM_PED "
			_qry += " WHERE ITEM.NUM_PED = "+alltrim(str(val(cPedWeb)))+" AND PED.ERRO='' "
			_qry += " ORDER BY SUBSTRING(ITEM.PRODUTO,1,8),ITEM.PRECO_VENDA,ITEM.PRODUTO "
	
			cQuery := ChangeQuery(_qry)
	
			If Select("TMPITEM") > 0
				TMPITEM->(DbCloseArea())
			Endif
	
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPITEM",.T.,.T.)
	
			DbSelectArea("TMPITEM")
			DbGoTop()
			aItens		:= {}
			cItem		:= "01"
	
			While !EOF()
				
				cProd := TMPITEM->PRODUTO
				nQtdPrd:= TMPITEM->QUANTIDADE
				//Chama rotina que faz a troca do produto Nacional para Importado
				aChkEstoq:= fTradeNacImp(cProd, nQtdPrd)
				
				If lQbrPed:= aChkEstoq[1]
					//Caso tenha sido feita a quebra de quantidade em produtos diferentes por falta de estoque
					cProd:= aChkEstoq[2]
					nQtdPrd:= aChkEstoq[3]
					cProd2:= aChkEstoq[4]
					nQtd2Prd:= aChkEstoq[5]
				Else
					cProd:= aChkEstoq[2]
					nQtdPrd:= aChkEstoq[3]
				Endif
				
				nQtPr:= Iif(lQbrPed,2,1)
				
				For nXX:= 1 to nQtPr
				
					If nXX > 1 
						//Informa a quantidade e codigo do item 2 que foi dividido por falta de estoque do item 1
						cProd  := cProd2
						nQtdPrd:= nQtd2Prd
					Endif
				
					DbSelectArea("SB1")
					DbSetOrder(1)
					DbSeek(xFilial("SB1")+cProd)
		
					cDescri := GetAdvFVal("SB4","B4_DESC",xFilial("SB4")+SubStr(SB1->B1_COD,1,8),1,"")
		
					cTES  := MaTesInt(2,cTpOper,cCodCli,cLojCli,"C",SB1->B1_COD,)
					If alltrim(cTES) = ""
						cTES := "501"
					Endif
		
		//			nPrecVend  := Round(TMPITEM->PRECO_VENDA*(1-(nDesconto/100)),4)
		
					nPrUnit	 := TMPITEM->PRECO_VENDA
					nPrecVend:= TMPITEM->PRECO_VENDA
		
		//			If !Empty(cTab)
					If !Empty(TMPPED->COD_TAB_PRECO)
						DbSelectArea("DA1")
						DbSetOrder(1)
						If DbSeek(xFilial("DA1")+TMPPED->COD_TAB_PRECO + AllTrim(SB1->B1_COD))
							nPrUnit := DA1->DA1_PRCVEN
						EndIf
					EndIf
		
					If nPercDesc > 0
						nPrecVend := Round(TMPITEM->PRECO_VENDA - ((nPercDesc/100)*TMPITEM->PRECO_VENDA),2)
					EndIf
		
					aLinha := {}
					aadd(aLinha,{"C6_ITEM"		, cItem,Nil})
					aadd(aLinha,{"C6_PRODUTO"	, AllTrim(SB1->B1_COD),Nil})
					//aadd(aLinha,{"C6_QTDVEN"	, If (AllTrim(SB1->B1_COD)<>'',TMPITEM->QUANTIDADE,0),Nil})
					aadd(aLinha,{"C6_QTDVEN"	, If (AllTrim(SB1->B1_COD)<>'',nQtdPrd,0),Nil})
		//			aadd(aLinha,{"C6_PRCVEN"	, Round(TMPITEM->PRECO_VENDA*(1-(nDesconto/100)),2),Nil})			//aadd(aLinha,{"C6_PRCVEN",Round(TMPITEM->PRECO_VENDA*(1-(TMPPED->DESCONTO/100)),6),Nil})
					aadd(aLinha,{"C6_PRCVEN"	, nPrecVend,Nil})
					aadd(aLinha,{"C6_PRUNIT"	, nPrUnit,Nil})
		//			aadd(aLinha,{"C6_VALOR"		, Round(TMPITEM->PRECO_VENDA*(1-(TMPPED->DESCONTO/100)),6)*TMPITEM->QUANTIDADE,Nil})
					aadd(aLinha,{"C6_VALOR"		, Round((nPrecVend*(1-(TMPPED->DESCONTO/100))*nQtdPrd),2),Nil}) //A410Arred(Round(nPrecVend*((1-(TMPPED->DESCONTO/100))*TMPITEM->QUANTIDADE),2),"C6_VALOR")})   //Round((nPrecVend*(1-(TMPPED->DESCONTO/100))*TMPITEM->QUANTIDADE),2),Nil})
					//aadd(aLinha,{"C6_VALOR"		, Round((nPrecVend*(1-(TMPPED->DESCONTO/100))*TMPITEM->QUANTIDADE),2),Nil}) //A410Arred(Round(nPrecVend*((1-(TMPPED->DESCONTO/100))*TMPITEM->QUANTIDADE),2),"C6_VALOR")})   //Round((nPrecVend*(1-(TMPPED->DESCONTO/100))*TMPITEM->QUANTIDADE),2),Nil})
					aadd(aLinha,{"C6_TES"		, cTES,Nil})
					aadd(aLinha,{"C6_ENTREG"    , dDatEntr ,Nil})
		//			aadd(aLinha,{"C6_NUM"		, cNroPed,Nil})
		//			aadd(aLinha,{"C6_LOCAL"		, SB1->B1_LOCPAD,Nil})
					aadd(aLinha,{"C6_LOCAL"		, "E0",Nil})
					aadd(aLinha,{"C6_GRADE"		, "S" ,Nil})   //"S"
					aadd(aLinha,{"C6_ITEMGRD"	, strzero(val(cItem),3),Nil})
					If !Empty(cDescri)                                   //Grade: campo da descricao C6_DESCRI
						aadd(aLinha,{"C6_DESCRI"	, If (AllTrim(SB1->B1_COD)<>'',cDescri,"< -- Invalido"),Nil})
					EndIf
					aadd(aLinha,{"C6_OPER"		, cTpOper,Nil})
		
					aadd(aItens,aLinha)
		
					cItem := SOMA1(cItem)
				Next nXX
				
				DbSelectArea("TMPITEM")
				DbSkip()
			EndDo
	
			DbCloseArea()
	
	
			/**************************************************************/
			/* Verifica valor total do Produto na grade                   */
			/**************************************************************/
	
			For _i := 1 to len(aItens)
				If _i <> 1
					If SubStr(aitens[_i][2][2],1,8)+AllTrim(Str(aitens[_i][5][2]*100)) = SubStr(aitens[_i-1][2][2],1,8)+AllTrim(Str(aitens[_i-1][5][2]*100)) //Grade: Alterado If para quebrar por referencia + Valor
						aItens[_i][1][2] := aItens[_i-1][1][2]
						aItens[_i][11][2] := SOMA1(aItens[_i-1][11][2])
					Else
						aItens[_i][1][2] := SOMA1(aItens[_i-1][1][2])
						aItens[_i][11][2] := "001"
					Endif
				Endif
			Next
	
	
			/**************************************************************/
			/* Grava o Pedido no Siga                                     */
			/**************************************************************/
	
			BEGIN TRANSACTION
	
				lMsErroAuto := .F.
	
				If Len(aCabec) > 0 .and. Len(aItens) > 0
	
					MATA410(aCabec,aItens,3)
	
					If lMsErroAuto
	//					Mostraerro()
						GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedWeb)
						MostraErro("\log_rakuten\MOBILE\",cDtTime+alltrim(str(nErro))+"_PED.log")
						nErro++
						lMsErroAuto := .T.
						_qry := "update [MOBILE].[dbo].[PEDIDO_WEB] set ERRO = 'ERRO' where PEDIDO_SIGA = '' and NUM_PED = "+str(val(cPedWeb))+" "
						TcSqlExec(_qry)

					Else
						GravaLog("Pedido Protheus n� "+cNroPed+" foi gerado com sucesso. Pedido: "+cPedWeb)
						nGerados++
	
						_qry := "Update "+RetSqlName("SC5")+" set C5_FECENT = '"+dDtEntrega+"' where C5_NUM = '"+cNroPed+"'"
						TcSqlExec(_qry)
						_qry := "Update "+RetSqlName("SC5")+" set C5_XINSTRU = '"+SubStr(cTiposDesc,1,250)+"' where C5_NUM = '"+cNroPed+"'"
						TcSqlExec(_qry)
	
						_qry := "update [MOBILE].[dbo].[PEDIDO_WEB] set PEDIDO_SIGA = '"+cNroPed+"' where PEDIDO_SIGA = '' and NUM_PED = "+str(val(cPedWeb))+" "
						TcSqlExec(_qry)
	
						u_AjusDesc(cNroPed,0,nPercDesc) //Executa a funcao 2x
						u_AjusDesc(cNroPed,0,nPercDesc)
	
						ConfirmSX8()
						//cNroPed := SOMA1(cNroPed)
						cNroPed := GetSX8Num("SC5","C5_NUM",,1)
						DbSelectArea("SC5")
						DbSetOrder(1)
						While DbSeek(xfilial("SC5")+cNroPed)
							ConfirmSX8()
							cNroPed := SOMA1(cNroPed)
						End
						SC5->(DbCloseArea())
					EndIf
	
				Else
	               If Len(aCabec) == 0
				    	//GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedWeb+" Sem Cabe�alho")
				    	MemoWrite("\log_rakuten\MOBILE\ERRO"+cDtTime+"-"+cPedWeb+"_IMP.log","Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedWeb+" Sem Cabe�alho")
						_qry := "update [MOBILE].[dbo].[PEDIDO_WEB] set ERRO = 'ERRO' where PEDIDO_SIGA = '' and NUM_PED = "+str(val(cPedWeb))+" "
						TcSqlExec(_qry)
				    	
		           Elseif Len(aItens)  ==  0
		            	//GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedWeb+" Sem Itens ")
		            	MemoWrite("\log_rakuten\MOBILE\ERRO"+cDtTime+"-"+cPedWeb+"_IMP.log","Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedWeb+" Sem Itens")
						_qry := "update [MOBILE].[dbo].[PEDIDO_WEB] set ERRO = 'ERRO' where PEDIDO_SIGA = '' and NUM_PED = "+str(val(cPedWeb))+" "
						TcSqlExec(_qry)
		            	
		           Else
		            	//GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedWeb)
		            	MemoWrite("\log_rakuten\MOBILE\ERRO"+cDtTime+"-"+cPedWeb+"_IMP.log","Erro na gera��o do Protheus n�: "+cNroPed+". Pedido: "+cPedWeb+" DESCONHECIDO")
						_qry := "update [MOBILE].[dbo].[PEDIDO_WEB] set ERRO = 'ERRO' where PEDIDO_SIGA = '' and NUM_PED = "+str(val(cPedWeb))+" "
						TcSqlExec(_qry)
		            	
		           Endif
				Endif
	
			END TRANSACTION
	
			DbSelectArea("TMPPED")
			DbSkip()
	
		End
	
		//GravaLog("Fim do processamento.")
	
		//MemoWrite("\log_rakuten\MOBILE\"+cDtTime+"_IMP.log",cLog)
		//alert("importado")
	
		DbCloseArea()
	End
	
	Reset Environment

Return


Static Function GravaLog(cMsg,cNome)

	Local cHora   := ""
	Local cDtHora := ""

	cHora   := Time()
	//cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)
	cDtHora := DTOS(date()) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)
	
	Conout(cMsg)
	cLog += CRLF+cDtHora+": HFATP010 - "+cMsg

Return


User Function AjusDesc(cPedVen,nDescont,nDesc1)

	Local aArea  := GetArea()
	Local cQuery := ""
	
	IF SELECT("TMP") > 0
		TMP->(DbCloseArea())
	ENDIF

	cQuery := "select C6_NUM,C6_ITEM,C6_PRUNIT,C6_PRCVEN,SUM(C6_QTDVEN) AS C6_QTDVEN,SUM(C6_VALOR) AS C6_VALOR  "
	cQuery += CRLF + "from "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) "
	cQuery += CRLF + "where D_E_L_E_T_ = '' "
	cQuery += CRLF + "and C6_NUM = '"+cPedVen+"' "
	cQuery += CRLF + "group by C6_NUM,C6_ITEM,C6_PRUNIT,C6_PRCVEN "
	cQuery += CRLF + "order by C6_NUM,C6_ITEM "

	MemoWrite("HWSRK002_AjusDesc.txt",cQuery)
	
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)

	DbSelectArea("TMP")

	If TMP->(!EOF())
		_nvalped := 0
		_nvaldes := 0
		While TMP->(!EOF())
			nPven    := TMP->C6_PRUNIT
			nPVdesc  := Round(nPven*(1-(nDesc1/100)),2)
			nQtdPV   := TMP->C6_QTDVEN
			_nValped += nQtdPV*nPven
			_nvaldes += (nPven-TMP->C6_PRCVEN)*nQtdPV//(nPven-nPVdesc)*nQtdPV
			TMP->(DbSkip())
		EndDo

		If nDescont > 0
			nDescAtu := nDescont
		Else
			nDescAtu := Round(_nValped * (nDesc1/100),2) //Usando como referencia o valor do desconto que vem da RAKUTEN.
		EndIf
		nDif     := Round(nDescAtu - _nvaldes,2)

		If nDif <> 0
			TMP->(DbGoTop())
			While TMP->(!EOF())
				If nDif / TMP->C6_QTDVEN = 0 .and. nDif <> 0
					nValItem := TMP->C6_VALOR - nDif
					nValUnit := Round((nValItem / TMP->C6_QTDVEN),2)
					DbSelectArea("SC6")
					DbSetOrder(1)
					If DbSeek(xFilial("SC6")+TMP->C6_NUM+TMP->C6_ITEM)
						While SC6->(!EOF()) .And. SC6->C6_NUM+SC6->C6_ITEM = TMP->C6_NUM+TMP->C6_ITEM
							RecLock("SC6",.F.)
							SC6->C6_PRCVEN := nValUnit
							SC6->C6_VALOR  := noround((SC6->C6_QTDVEN*round(nValUnit,2)),2)
							MsUnlock()
							SC6->(!DbSkip())
						EndDo
					EndIf
					nDif := 0
					Exit
				Endif
				TMP->(DbSkip())
			EndDo
			ndif1 := ndif
			If nDif <> 0
				cChv := ""
				nC6Vlr := 0
				nC6Qtd := 0
				_dif := 99999
				TMP->(DbGoTop())
				While TMP->(!EOF())
					nValItem := TMP->C6_VALOR - nDif
					nValUnit := Round((nValItem / TMP->C6_QTDVEN),2)
					If _dif <> 0
						If nDif > 0
							If nValUnit - TMP->C6_PRCVEN <> 0 .and. abs(ndif - ((nValUnit*TMP->C6_QTDVEN) - TMP->C6_VALOR)) < abs(_dif)
								_dif := ndif - ((nValUnit*TMP->C6_QTDVEN) - TMP->C6_VALOR)
								cChv := TMP->C6_NUM+TMP->C6_ITEM
								nC6Vlr := TMP->C6_VALOR
								nC6Qtd := TMP->C6_QTDVEN
							Endif
						Else
							If nValUnit - TMP->C6_PRCVEN <> 0 .and. abs(ndif - (TMP->C6_VALOR - (nValUnit*TMP->C6_QTDVEN))) < abs(_dif)
								_dif := (TMP->C6_PRCVEN - nValUnit)
								_dif := ndif - (TMP->C6_VALOR - (nValUnit*TMP->C6_QTDVEN))
								cChv := TMP->C6_NUM+TMP->C6_ITEM
								nC6Vlr := TMP->C6_VALOR
								nC6Qtd := TMP->C6_QTDVEN
							Endif
						Endif
					EndIf
					TMP->(DbSkip())
				EndDo
				IF !Empty(cChv)
					nValItem := nC6Vlr - nDif
					nValUnit := Round((nValItem / nC6Qtd),2)
					nDif := nDif - (nC6Vlr - noround((nC6Qtd*round(nValUnit,2)),2))
					DbSelectArea("SC6")
					DbSetOrder(1)
					If DbSeek(xFilial("SC6")+cChv)
						While SC6->(!EOF()) .And. SC6->C6_NUM+SC6->C6_ITEM = cChv
							RecLock("SC6",.F.)
							SC6->C6_PRCVEN := nValUnit
							SC6->C6_VALOR  := noround((SC6->C6_QTDVEN*round(nValUnit,2)),2)
							MsUnlock()
							SC6->(DbSkip())
						EndDo
					EndIf
				//nDif := nDif - nDif1
				Endif
			Endif

		EndIf
	EndIf

	TMP->(DbCloseArea())

	RestArea(aArea)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �fTradeNacImp �Autor  �R.Melo - Totalit � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Verifica se existe estoque para o produto e caso nao haja   ���
���          �o programa pega do mesmo produto porem da origem nac ou imp ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function fTradeNacImp(cProd, nQtdPrd)
Local cPrd1   := cProd
Local nQtdOrig   := nQtdPrd
Local cPrd2   := ""
Local nQtd2   := 0
Local aRet    := {}
Local lBlqDem := .f.
Local cPV	  := ""
Local lConEst := .t. //Consulta Estoque
Local aAreaSB1:= SB1->(GetArea())
Local aAreaSB2:= SB2->(GetArea())
Local cCodSeq := ''
Local cCodPrio:= ''
Local cPriori := ''
Local cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local cAmzPul := AllTrim(SuperGetMV("MV_XAMZPUL",.F.,"E1")) //Armazem padrao pulmao

dbSelectArea("ZB5")
dbSetOrder(1)
ZB5->(dbGoTop())
//Verifica se existe priorizacao de produtos, caso nao exista o pedido segue igual.
If ZB5->(DbSeek(xFilial("ZB5")+cPrd1))
	cPriori:= ZB5->ZB5_PROIRI
	cCodPrio:= Iif(cPriori=="N",ZB5->ZB5_PRDNAC,ZB5->ZB5_PRDIMP)
	//Inverte os codigos para saber qual e o produto secundario para ser contado estoque caso falte no prd1
	cCodSeq := Iif(cPriori=="N",ZB5->ZB5_PRDIMP,ZB5->ZB5_PRDNAC)
Else
	dbSelectArea("ZB5")
	dbSetOrder(2)
	ZB5->(dbGoTop())
	If ZB5->(DbSeek(xFilial("ZB5")+cPrd1))
		cPriori:= ZB5->ZB5_PROIRI
		cCodPrio:= Iif(cPriori=="N",ZB5->ZB5_PRDNAC,ZB5->ZB5_PRDIMP)
		//Inverte os codigos para saber qual e o produto secundario para ser contado estoque caso falte no prd1
		cCodSeq := Iif(cPriori=="N",ZB5->ZB5_PRDIMP,ZB5->ZB5_PRDNAC)
		//Retorna pedido original pois nao existe priorizacao cadastrada na ZB5
	Else
		aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2 , .t.}
		RestArea(aAreaSB1)
		RestArea(aAreaSB2)
		Return (aRet) 
	Endif
Endif

//Verifica aqui o Bloqueio por Demanda
cBlqDem:= POSICIONE("SB1",1,xFilial("SB1")+cCodPrio,"B1_YBLQDEM")
lBlqDem:= Iif(Alltrim(cBlqDem)== "S",.T.,.F.) 

If !lBlqDem
	//Verifica aqui a Situacao do Produto onde:
	//PV - Nao consulta estoque, venda liberada
	//NPV - Consulta estoque, venda so acontece com disponibilidade do produto
	cPV:= POSICIONE("SB1",1,xFilial("SB1")+cCodPrio,"B1_YSITUAC")
	lConEst:= Iif(Alltrim(cPV)== "PV",.f.,.t.)

	If lConEst
		dbSelectArea("SB2")
		dbSetOrder(1)
		SB2->(dbGoTop())

		nSldPik := HFATSALDO(cCodPrio,cAmzPik)
		nSldPul := HFATSALDO(cCodPrio,cAmzPul)

		nSldSecPik := HFATSALDO(cCodSeq,cAmzPik)
		nSldSecPul := HFATSALDO(cCodSeq,cAmzPul)

		//If SB2->(DbSeek(xFilial("SB2")+cCodPrio))
		If nQtdOrig<= nSldPik+nSldPul//SB2->B2_QATU
			aRet:= { .f., cCodPrio, nQtdOrig, cPrd2, nQtd2 ,.t.}
		Else
			nPriQatu:= nSldPik + nSldPul
			nSecQatu:= nSldSecPik + nSldSecPul
			nSldAtu := nQtdOrig - nPriQatu

			If nSecQatu<= nSldAtu
				nQtd2:= nSldAtu - nSecQatu
				aRet:= { .f., cCodPrio, nQtdOrig, cCodSeq, nQtd2, .t. }
			Else
				Conout('Produtos: '+cCodPrio+' e '+cCodSeq+' nao possuem estoque para contemplar pedido.')
				aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2 ,.f.}
				RestArea(aAreaSB1)
				RestArea(aAreaSB2)
				Return (aRet)
				//Produtos (Nac+Imp) juntos nao contem a quantidade total necessaria do pedido.
			Endif	
		Endif
	Else
		//Retornar aqui quantidade original + produto priorizado 
		aRet:= { .f., cCodPrio, nQtdOrig, cPrd2, nQtd2 , .t.} 
	Endif
Else
	Conout('Produto: '+cCodPrio+' com bloqueio de demanda.')
	aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2, .t. } 
	//Retornar aqui pedido original - o pedido nao podera ser feito por conta do bloqueio por demanda
Endif

RestArea(aAreaSB1)
RestArea(aAreaSB2)

Return (aRet) 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATSALDO �Autor  �R.Melo - Totalit � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Busca saldo na SB2  										 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function HFATSALDO(cProduto,cArmazem)
Local nRet := 0

DbSelectArea("SB2")
DbSetOrder(1)
If SB2->(DbSeek(xFilial("SB2")+cProduto+cArmazem))
	nRet := SB2->B2_QATU-SB2->B2_RESERVA-SB2->B2_QEMP-SB2->B2_XRESERV //SaldoSb2()
EndIf

Return nRet