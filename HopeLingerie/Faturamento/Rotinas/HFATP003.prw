// #########################################################################################
// Projeto: Hope
// Modulo : Faturamento
// Fonte  : hFATP003
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 09/10/16 | Actual Trend      | Cadastro de Prazos de Entrega 
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

user function HFATP003
	local cVldAlt := ".T." 
	local cVldExc := ".T." 
	
	local cAlias
	
	cAlias := "SZ7"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	axCadastro(cAlias, "Cadastro de Prazo de Entrega", cVldExc, cVldAlt)
	
return
