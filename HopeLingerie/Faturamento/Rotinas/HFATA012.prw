#Include 'Protheus.ch'
#include "topconn.ch"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} HFATA012
Faturamento em JOB
@author Weskley Silva
@since 25/09/2018
@version P12
@obs ZZZ_STATUS
P-Pendente Faturamento
Q-Pendente Cancelamento
F-Faturado
C-Cancelado
EF-Erro Faturamento
EC-Erro Cancelamento
/*/


user function HFATA012()

	Local cQuery
	Local lErro		:= .F.
	Local oError
	Local cMsgErro
	Private cSASStaErr	:= "E"
	Sleep(2000)
	//If !LockByName("HFATA012-1",.F.,.F.)
	ConOut("Processso ja inicializado!")
	//Return
	//EndIf
	RpcSetEnv("01","0101",,,"FAT","U_HFATA012",{})
	cQuery	:= " SELECT TOP 1 R_E_C_N_O_ AS REGZZZ FROM "+RETSQLNAME("ZZZ")+" ZZZ WHERE ZZZ_STATUS IN ('P') AND ZZZ_FILIAL = '0101' AND ZZZ_SERIE = '' AND ZZZ_NOTA = ''   ORDER BY R_E_C_N_O_ "
	// Seta que RecLock, em caso de falha, nao vai fazer retry automatico
	SetLoopLock(.F.)
	While !killapp()
		//Monitor("Faturamento|Aguardando tarefas...")
		TCQuery cQuery new alias TMPZZZ
		If TMPZZZ->(EOF())
			TMPZZZ->(DbCloseArea())
			return
			Sleep(10000)
			Loop
		EndIF
		lErro		:= .F.
		cMsgErro	:= ""
		ZZZ->(DbGoTo(TMPZZZ->REGZZZ))
		RecLock("ZZZ",.F.)
		oError := ErrorBlock({|e| lErro := .T.,ErroLog(e,cSASStaErr) } )
		dDatabase := date()
		If ALLTRIM(ZZZ->ZZZ_STATUS)=="P"	//Pendente Faturamento
			Faturar(@lErro,@cMsgErro)
		EndIf
		Logs(Replicate("=",30))
		ZZZ->(MsUnLock())
		ErrorBlock(oError)
		TMPZZZ->(DbCloseArea())
		RETURN
	EndDo

Return

Static Function Faturar(lErro,cMsgErro)
	Local cCodUsr	:= "000000"
	Local aDocFat	:= {}
	cSASStaErr		:= "E"
	//Monitor("Faturamento:Processando Pedido :"+ZZZ->ZZZ_PEDIDO+" do Cliente :"+ZZZ->ZZZ_CLIENT)	//Atualiza mensagem no monitor
	Logs("Pedido:"+ZZZ->ZZZ_PEDIDO)
	Logs("Processando Pedido:"+ZZZ->ZZZ_PEDIDO)
	Begin Sequence
		If !lErro .AND. Empty(ZZZ->ZZZ_NOTA) .AND. !Empty(ZZZ->ZZZ_PEDIDO)															
			Monitor("Faturamento:Processando Pedido:"+ZZZ->ZZZ_PEDIDO+" do Cliente:"+ZZZ->ZZZ_CLIENT)	//Atualiza mensagem no monitor
			Logs("Faturamento:Pedido:"+ZZZ->ZZZ_PEDIDO+" do Cliente:"+ZZZ->ZZZ_CLIENT)
			Begin Transaction
				If FGeraFat(ZZZ->ZZZ_FILIAL,ZZZ->ZZZ_PEDIDO,@aDocFat,@cMsgErro)
					RecLock("ZZZ",.F.)
					ZZZ->ZZZ_NOTA	:= aDocFat[1]
					ZZZ->ZZZ_SERIE	:= aDocFat[2]
					ZZZ->ZZZ_LOG	:= ""
					ZZZ->ZZZ_STATUS := "F"
					MsUnLock()

					IF EMPTY(ZZZ->ZZZ_NUMPF)
						DbSelectarea("ZZ9")
						DbSetOrder(1)
						DbSeek(xFilial("ZZ9")+ZZZ->ZZZ_PEDIDO+ZZZ->ZZZ_CLIENT+ZZZ->ZZZ_LOJA)
						RecLock("ZZ9",.F.)
						ZZ9->ZZ9_STATUS := 'F'
						ZZ9->ZZ9_NOTA := aDocFat[1]
						MSUNLOCK()

					ELSE

						DbSelectarea("ZZ9")
						DbSetOrder(2)
						DbSeek(xFilial("ZZ9")+ZZZ->ZZZ_NUMPF+ZZZ->ZZZ_CLIENT+ZZZ->ZZZ_LOJA)
						RecLock("ZZ9",.F.)
						ZZ9->ZZ9_STATUS := 'F'
						ZZ9->ZZ9_NOTA := aDocFat[1]
						MSUNLOCK()

					ENDIF

				Else

					IF EMPTY(ZZZ->ZZZ_NUMPF)
						DbSelectarea("ZZ9")
						DbSetOrder(1)
						DbSeek(xFilial("ZZ9")+ZZZ->ZZZ_PEDIDO+ZZZ->ZZZ_CLIENT+ZZZ->ZZZ_LOJA)
						RecLock("ZZ9",.F.)
						ZZ9->ZZ9_STATUS := 'E'
						MSUNLOCK()

					ELSE

						DbSelectarea("ZZ9")
						DbSetOrder(2)
						DbSeek(xFilial("ZZ9")+ZZZ->ZZZ_NUMPF+ZZZ->ZZZ_CLIENT+ZZZ->ZZZ_LOJA)
						RecLock("ZZ9",.F.)
						ZZ9->ZZ9_STATUS := 'E'

						MSUNLOCK()
					ENDIF

					DisarmTransaction()
					Logs("Pedido:"+cMsgErro,.T.)
					lErro		:= .T.

					IF lErro
						RecLock("ZZZ",.F.)
						ZZZ_STATUS := 'E' 
						MsUnLock()
					ENDIF

					BREAK
				EndIF
			End Transaction
		EndIf

	End Sequence
	//MsUnLockAll()
	//GETMV("MV_NUMITEN",.T.)
	//SX6->(MsRUnLock())
	If lErro
		RecLock("ZZZ",.F.)
		ZZZ_STATUS := cSASStaErr
		MsUnLock()
	EndIf
Return


Static Function ErroLog(oErro,cStatus)
	DisarmTransaction()
	Logs(oErro:Description+CRLF+oErro:ErrorStack+CRLF+oErro:ErrorEnv,.T.)
	RecLock("ZZZ",.F.)
	ZZZ_STATUS=cStatus	//Altera o status para erro
	MsUnLock()
	StartJob("U_HFATA012",getenvserver(),.F.)	//Inicia um nova thread
	__Quit()									//Finaliza a thread atual
Return


/*/{Protheus.doc} FGeraFat
Fatura o Pedido de venda
@author Weskley Silva
@since 25/09/2018
@version 1.0
@param cEmpFat, String, Filial onde esta o pedido de venda para faturar
@param cPedFat, String, Numero do pedido de venda para faturar
@param aDocFat, Array, Array que recebe por referencia para gravar o Numero da NF e Serie
@param cMsgErro, String, String que recebe por referencia para gravar a msg de erro
/*/
Static Function FGeraFat(cEmpFat,cPedFat,aDocFat,cMsgErro)
	Local aArea			:= GetArea()
	Local aPvlNfs		:= {}
	Local cSerie		:= "2" 
	Local cFilBkp		:= cFilAnt
	Local cNFSaida		:= ""
	Local aNota         := {}
	Local lRet			:= .T.
	Local nX
	Local nVezes
	//Local nItemNf   	:= GETMV("MV_NUMITEN")
	Local  nItemNf   := a460NumIt(cSerie)
	Default aDocFat		:= {}
	Default cMsgErro	:= ""
	cFilAnt := cEmpFat

	//ProcRegua(4)

	Logs("Gerando Nota Fiscal de Sa�da")
	ProcessMessage()

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	aTesdig = GETAREA()

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	//cTesdig = SC6->C6_TES


	RESTAREA(aTesdig)

	dbSelectArea("SC9")
	SC9->(dbSetOrder(1))

	If SC9->(DbSeek(cEmpFat+cPedFat))
		While SC9->(!Eof()) .AND. SC9->C9_FILIAL == cEmpFat .AND. SC9->C9_PEDIDO == cPedFat 
			cProd = SC9->C9_PRODUTO
			cQuant = CVALTOCHAR(SC9->C9_QTDLIB)
			if EMPTY(SC9->C9_NFISCAL)
				If !Empty(SC9->C9_BLEST) .and. !Empty(SC9->C9_BLCRED) 
					cSASStaErr	:= "BE"
					cMsgErro	:= cEmpFat +"|"+cPedFat +"| Pedido com Bloqueio | Bloqueio de estoque/Credito "
					Return .F.
				ELSEIF !Empty(SC9->C9_BLEST) 
					cSASStaErr	:= "BE"
					cMsgErro	:= cEmpFat +"|"+cPedFat +" | "+ cProd +" | "+ cQuant +" | Pedido com Bloqueio | Bloqueio de estoque "
					Return .F.
				ELSEIF  !Empty(SC9->C9_BLCRED) 
					cSASStaErr	:= "BE"
					cMsgErro	:= cEmpFat +"|"+cPedFat +"| Pedido com Bloqueio | Bloqueio de Credito "
					Return .F.
				ENDIF

				SC6->(DbSeek(xFilial("SC6")+SC9->C9_PEDIDO+SC9->C9_ITEM+SC9->C9_PRODUTO))

				SE4->(DbSeek(xFilial("SE4")+SC5->C5_CONDPAG))
				SB1->(DbSeek(xFilial("SB1")+SC9->C9_PRODUTO))
				SB2->(DbSeek(xFilial("SB2")+SC9->C9_PRODUTO+SC9->C9_LOCAL))
				SF4->(DbSeek(xFilial("SF4")+SC6->C6_TES))

				aAdd(aPvlNfs,{;
				SC9->C9_PEDIDO,;
				SC9->C9_ITEM,;
				SC9->C9_SEQUEN,;
				SC9->C9_QTDLIB,;
				SC9->C9_PRCVEN,;
				SC9->C9_PRODUTO,;
				.F.,;
				SC9->(RECNO()),;
				SC5->(RECNO()),;
				SC6->(RECNO()),;
				SE4->(RECNO()),;
				SB1->(RECNO()),;
				SB2->(RECNO()),;
				SF4->(RECNO());
				})
				
		If ( Len(aPvlNfs) >= nItemNf )
		
		dbSelectArea("SX5")
		dbSetOrder(1)

		If SX5->(MsSeek(cEmpFat+"01"+cSerie))
			nVezes := 0
			While ( !SX5->(MsRLock()) )
				nVezes ++
				Logs("Aguardando libera��o de numera��o da Nota Fiscal de Sa�da ... "+cValtochar(nVezes))
				If ( nVezes > 5 )
					Exit
				EndIf
				Sleep(1000)
		    EndDo
		 Endif
			
			Pergunte("MT460A",.F.)
			cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .F. , .F. , .F. , .F., 0, 0, .F., .F.)
			aPvlNfs := {}
						
		dbSelectArea("SX5")
		If !SX5->(MsRLock()) .and. SX5->(!EOF())
			SX5->(MsUnlock())
		Endif	
			
		EndIf
	
		ENDIF
			SC9->(DbSkip())
		EndDo
	EndIf
	
	dbSelectArea("SX5")
	dbSetOrder(1)

	If SX5->(MsSeek(cEmpFat+"01"+cSerie))
		nVezes := 0
		While ( !SX5->(MsRLock()) )
			nVezes ++
			Logs("Aguardando libera��o de numera��o da Nota Fiscal de Sa�da ... "+cValtochar(nVezes))
			If ( nVezes > 5 )
				Exit
			EndIf
			Sleep(1000)
		EndDo
	Endif
	
	
	If Len(aPvlNfs) > 0
		Logs("Gerando Nota Fiscal de Sa�da")
		Monitor("Gerando Nota Fiscal de Sa�da")
		//Gera documento de saida
		Pergunte("MT460A",.F.)
		cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .F. , .F. , .F. , .F., 0, 0, .F., .F.)
		//AAdd( aNotas, { cSerie, cNota } )
	else
		cMsgErro := "Pedido "+cPedFat+" n�o liberado"+ CHR(10) + CHR(13)
		RestArea(aArea)
		cFilAnt:= cFilBkp
		lRet	:= .F.
		Return lRet
		
    EndIf

	/*If Len(aPvlNfs) == 0
		cMsgErro := "Pedido "+cPedFat+" n�o liberado"+ CHR(10) + CHR(13)
		RestArea(aArea)
		cFilAnt:= cFilBkp
		lRet	:= .F.
		Return lRet
	endif
	*/ 
	
	//Verifica se a numera��o da NF est� em uso:
	/*dbSelectArea("SX5")
	dbSetOrder(1)

	If SX5->(MsSeek(cEmpFat+"01"+cSerie))
		nVezes := 0
		While ( !SX5->(MsRLock()) )
			nVezes ++
			Logs("Aguardando libera��o de numera��o da Nota Fiscal de Sa�da ... "+cValtochar(nVezes))
			If ( nVezes > 5 )
				Exit
			EndIf
			Sleep(1000)
		EndDo
	Endif
	*/
	

	//Pergunte("MT460A",.F.)
	//For nX := 1 To Len(aPvlNfs)
	//	If Len(aPvlNfs[Len(aPvlNfs)])>=nItemNf
	//			aadd(aNotas,{})
	//	EndIf
	//		aadd(aNotas[Len(aNotas)],aClone(aPvlNfs[nX]))
	//Next nX
	// Gera as notas de acordo com a quebra
	//For nX := 1 To Len(aNotas)
	//	cNFSaida := MaPvlNfs(aNotas[nX], cSerie, .F. , .F. , .F. , .F. , .F., 0, 0, .F., .F.)
	//Next nX

	//Gera documento de saida
	//Pergunte("MT460A",.F.)
	//cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .F. , .F. , .F. , .F., 0, 0, .F., .F.)
	//cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .T. , .T. , .F. , .F., 0, 0, .F., .F.)  -- Contabiliza��o online

	If Empty(cNFSaida)
		cMsgErro := "N�o foi poss�vel gerar a Nota fiscal de Sa�da da Filial "+cFilAnt+ CHR(10) + CHR(13)
	Else
		aDocFat	:= {cNFSaida,cSerie}
		Logs("Nota Fiscal de Sa�da gerada")
		Monitor("Nota Fiscal de Sa�da gerada")
	Endif

	dbSelectArea("SX5")
	If !SX5->(MsRLock()) .and. SX5->(!EOF())
		SX5->(MsUnlock())
	Endif

	RestArea(aArea)
	cFilAnt:= cFilBkp
Return lRet


/*/{Protheus.doc} Logs
Grava log em arquivo texto no servidor
@author Weskley Silva
@since 25/09/2018
@version 1.0
@param cTexto, character, Texto do log
/*/
Static Function Logs(cTexto,lErro)
	Local cCodLog	:= SubStr(DTOS(Date()),1,6)
	Local cPasta	:= "\log\HFATA012"
	Local cTexLog
	Default lErro	:= .F.
	FWMakeDir(cPasta)
	If File(cPasta+"\"+cCodLog+".txt")
		nHdl := fopen(cPasta+"\"+cCodLog+".txt" , 2 + 64 )
	Else
		nHdl := FCREATE(cPasta+"\"+cCodLog+".txt", FC_NORMAL)
	EndIf
	FSeek(nHdl, 0, 2)
	FWrite(nHdl, DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+CRLF )
	fclose(nHdl)
	If lErro
		RecLock("ZZZ",.F.)
		cTexLog			:= Alltrim(ZZZ->ZZZ_LOG)
		ZZZ->ZZZ_LOG	:= DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+chr(10)+cTexLog
		MsUnLock()
		lErroJob	:= .T.

		/*DbSelectarea("ZZ9")
		DbSetOrder(1)
		DbSeek(xFilial("ZZ9")+ZZZ->ZZZ_PEDIDO+ZZZ->ZZZ_CLIENT+ZZZ->ZZZ_LOJA)
		RecLock("ZZ9",.F.)
		ZZ9->ZZ9_STATUS = 'E'
		MSUNLOCK()
		*/
	EndIf
Return

Static Function Monitor(cMsg)

	cMsg1 := DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cMsg+chr(10)

	tcsqlexec("INSERT INTO YJOB (JOBLOG) VALUES ('"+cMsg1+"')")
	PutGlbValue("HFATA012",DTOC(date(),"dd/mm/yy")+"|"+time()+cMsg)
	PtInternal(1,DTOC(date(),"dd/mm/yy")+"|"+time()+"|"+cMsg)	
Return
