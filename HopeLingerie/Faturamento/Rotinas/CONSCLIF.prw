#include "topconn.ch"            
#include "colors.ch"                  
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "COLORS.CH"
#INCLUDE "FONT.CH"  
#INCLUDE "FINC010.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "parmtype.ch"


#DEFINE QTDETITULOS	1
#DEFINE MOEDATIT	2
#DEFINE VALORTIT	3
#DEFINE VALORREAIS	4


/*/
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
����������������������������������������������������������������������������ͻ��
���Programa   �CONSCLIF  � Autor � New Bridge                �Data�21.04.2003���
����������������������������������������������������������������������������͹��
���Descricao  �Consulta Clientes em Rela��o ao Financeiro             		 ��� 
����������������������������������������������������������������������������͹��
���Sintaxe    �U_CONSCLIF()                                                  ���
����������������������������������������������������������������������������͹��
���Parametros �                                                              ��� 
����������������������������������������������������������������������������͹��
���Uso        �                                                              ���
����������������������������������������������������������������������������͹��
���Solicitante�                                              �Data�          ���
����������������������������������������������������������������������������͹��
���               ALTERACOES EFETUADAS APOS CONSTRUCAO INICIAL               ���
����������������������������������������������������������������������������͹��
���Analista   �                                              �Data�          ���
����������������������������������������������������������������������������͹��
���Descricao  �                                                              ���
����������������������������������������������������������������������������͹��
���Autor      �                                              �Data�          ���
����������������������������������������������������������������������������ͼ��
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
/*/
User Function CONSCLIF(nChamada,cCLiente)

	Local oListBox1
	Local cListBox1
	Local aBotao    := {}
	Local bCancel   := {|| oDlg:End()}
	Local bOk       := {|| RFATC01c(aList1,oListBox1:nAt,lRet,nChamada),oDlg:End()}
	Local nOpc		:= 0
	Local aTitulo1	:= { " ","Codigo  ", "Nome                        .", "Cnpj / CPF            .", "Tel       .", "Contato       .", 	"E-Mail                         .","Endere�o                       .","Macro Regi�o   ."," Regi�o    .",	"****" } 
	Local aAlias	:= {}
	Local cTitulo	:= "Consulta Clientes"
	Local cNomeCli 	:= CriaVar("A1_NOME")  
	Local cNomeRed	:= CriaVar("A1_NREDUZ")
	Local cCnpj     := CriaVar("A1_CGC")
	Local cTel      := CriaVar("A1_TEL")
	Local cContato  := CriaVar("A1_CONTATO")
	Local nLc       := CriaVar("A1_LC")
	Local nSaldUP   := CriaVar("A1_SALDUP")
	Local nSaldlim  := CriaVar("A1_SALDUP")
	Local nMcompra  := CriaVar("A1_SALDUP")
//Inicio + Adicionado por Robson (Solicitacao da Karla - Especificacao Posicao de Cliente)
	Local dDtCad  := CriaVar("A1_DTCAD")   //Data do Cadastro
	Local cEndCli := CriaVar("A1_END ")    //Endere�o
	Local cBairro := CriaVar("A1_BAIRRO")  //Bairro
	Local cCity   := CriaVar("A1_MUN")  //Cidade
	Local cState  := CriaVar("A1_EST")	   //Estado
	Local cCEP    := CriaVar("A1_CEP ")    //CEP
	Local cMail   := CriaVar("A1_EMAIL ")  //Email
	Local cCodReg := CriaVar("A1_XCODREG") //Cod Regi�o 
	Local cDscReg := CriaVar("A1_XDESREG") //Regi�o 
	Local cMacReg := CriaVar("A1_XMICRRE") //C�digo Macro 
	Local cDscMReg:= CriaVar("A1_XMICRDE") //Macro Regi�o 
	Local cObserv := CriaVar("A1_XOBS")    //Observa��o 
//Fim - Adicionado por Robson (Solicitacao da Karla - Especificacao Posicao de Cliente)

	Local lRet		:= .T.
	Local cReadVar	:= AllTrim(ReadVar())
	Local lConsulta	:= .T.
	Local nPrcBas 	:= nPrcMin := nPerDes := 0
	Local aObjects := {}
	Local aInfo    := {}
	Local aPosObj  := {}
	Local aSize    := MsAdvSize(.F.)
	Local lFc010Bol:= .f.
	Local lSigaGE  := .f.
	Local aParam := {}
	Local aGet      := {"","","",""}
	Private aList1	:= {}   // daniel  
	Private aList2	:= {} 
	Private aList3	:= {}
	Private aList4	:= {}
	Private aList5	:= {}
	Private aList6	:= {}
	Private cCadastro := ""//"Cadastro Cli x NF"
	Private aRotina  := {}             //Rotina da MBrowse
	Private cCodigo  := ""
	Private cNomered := ""
	Private cNome    := ""
	Private cCnpj    := ""
	Private cTel     := ""
	Private Contato  := ""
	Private nLc      := 0
	Private nSaldup  := 0 
	Private nSallim  := 0
	Private nMcompra := 0
	Private dPcompra := ctod(space(08)) 
	Private dUcompra := ctod(space(08)) 
	Private cEmail   := ""
	Private cEnder   := ""
	Private cMRegi   := ""
	Private cRegia   := ""
	
	//Incluido por Daniel em 27/12/2017
	//@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra
	//@cEmail,@cEnder,@cMRegi,@cRegia,
	
	
	Private AtmxFil  := {}
	Private cCodCli	:= CriaVar("A1_COD")
    Private cLoja	:= CriaVar("A1_LOJA")
	aadd(aParam,ctod('01/01/2011')) //MV_PAR01)  EMISSAO 
	aadd(aParam,ctod('01/01/2051')) //MV_PAR02)   EMISSAO
	aadd(aParam,ctod('01/01/2011')) //MV_PAR03)  VENC
	aadd(aParam,ctod('01/01/2051')) //MV_PAR04) VENC
	aadd(aParam,1) //MV_PAR05) CONSIDER PROV
	aadd(aParam,'' ) //MV_PAR06) PREFIX
	aadd(aParam,'ZZZ') //MV_PAR07) PREFIX
	aadd(aParam,1) //MV_PAR08) FATURADOS S
	aadd(aParam,1) //MV_PAR09) LIQUIDADOS S
	aadd(aParam,1) //MV_PAR10) PEDIDOS COM ITENS BLO
	aadd(aParam,1) //MV_PAR11) TIT POR LIQUID
	aadd(aParam,1) //MV_PAR12) CONSIDERA SALDO
	aadd(aParam,2) //MV_PAR13) CONSIDERA LOJA
	aadd(aParam,1) //MV_PAR14) TES GERA DUPL
	aadd(aParam,1) //MV_PAR15) RA
	aadd(aParam,1) //MV_PAR16) EXIB DIAS A VENCER 
	aadd(aParam,2) //MV_PAR17)  SELECUNA FILIAIS

	Default nChamada:= ""              
	Default cCliente:= ""                      
	
	// 1o. Elemento = Codigo		-> A1_COD
	// 2o. Elemento = Nome Reduzido	-> A1_NREDUZ
	// 3o. Elemento = Nome		    -> A1_NOME
	// 4o. Elemento = Cnpj 		    -> A1_CNPJ
	// 5o. Elemento = Telefone      -> A1_TEL
	// 6o. Elemento = Contato    	-> A1_CONTATO
	// 7o. Elemento = Limit.Cr�dito	-> A1_LC
	// 8o. Elemento = Saldo Dupl 	-> A1_SALDUP
	// 9o. Elemento = Saldo Cr�dito  	-> A1_CONTATO
	// 10o. Elemento = Data 1a. Compra	-> A1_LC
	// 11o. Elemento = Data Ult.Compra	-> A1_SALDUP

   
	aAdd(aList1,{ "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	aAdd(aList6,{ "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""}) 
	cCodCli	 := Space(Len(cCodCli)) 
	cNomeCli := Space(Len(cNomeCli))
	cNomeRed := Space(Len(cNomeRed))
	cCnpj    := Space(Len(cCnpj))
	cTel     := Space(Len(cTel))
	cContato := Space(Len(cContato))
	nLc      := 0
	nSaldup  := 0
	nSallim  := 0

	cPicture := ""

	cCEME2:= ''
	cCEME1 := ''
    cCodCli := cCliente //'162880' //
	SetKey( 12 	,{||U_RFATC01LE(cCliente)})//Legenda - <Ctrl-L>
	//SetKey( 22	,{||AxVisual("SA1",SA1->(RECNO()),2)})//Visualizar - <Ctrl-V>
	//SetKey( 16	,{||RFATC01A(cDescPro,cDescGrp,cCodGrp,cCodPro,cDescCeme,@aList1,@oListBox1,@nChamada)}) //Pesquisar - <Ctrl-P>

	Do Case
		Case "DESCRI" $ cReadVar
		cDescPro	:= &(ReadVar())
		lConsulta	:= .f.
		nChamada	:=	1
		Case "Codigo" $ cReadVar
		dbSelectArea("SA1")
		dbSetOrder(1)
		MsSeek(xFilial("SA1")+&(cReadVar))
		cNomeCli	:= Iif(!Empty(&(ReadVar())),SA1->A1_NOME,cNomeCli)
		//		lConsulta	:= .F.
		nChamada	:=	1
	EndCase

	//While .T.
   /*/
	Aadd(aObjects, {100, 010, .T., .F.})
	Aadd(aObjects, {200, 200, .T., .T.})
	Aadd(aObjects, {100, 100, .T., .T.})

	aInfo   := {aSize[1], aSize[2], aSize[3], aSize[4], 3, 2}
	aPosObj := MsObjSize(aInfo, aObjects)
	/*/

	aSize := MsAdvSize()       

	aObjects := {}
	AAdd( aObjects, { 100, 100, .T., .T. } )
	//aInfo    := { aSize[1], aSize[2], aSize[3], aSize[4], 2, 2 } 
    aInfo    := { aSize[1], aSize[2]+200, aSize[3], aSize[4]+200, 2, 2 }
	aPosObj := MsObjSize(aInfo,aObjects,.T.)

	//	DEFINE MSDIALOG oDlg TITLE cTitulo From 000,000 To 530,935 OF oMainWnd PIXEL
	DEFINE MSDIALOG oDlg TITLE cTitulo FROM aSize[7],0 TO aSize[6],aSize[5] OF oMainWnd PIXEL
	//	DEFINE MSDIALOG oDlg TITLE cTitulo From 000,000 To 530,735 OF oMainWnd PIXEL
    
	@ 005,004 SAY "Codigo:"		SIZE 030,08 OF oDlg PIXEL COLOR CLR_HBLUE
	@ 012,004 MSGET oDescCod	VAR cCodCli	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 040,08 OF oDlg PIXEL  F3 "SA1"
	@ 005,060 SAY "Nome Reduz:"		SIZE 060,08 OF oDlg PIXEL
	@ 012,060 MSGET oNomered	VAR cNomered	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) When lConsulta	SIZE 130,08 OF oDlg PIXEL 
	@ 005,210 SAY "Maior Compra: " SIZE 060,08 OF oDlg PIXEL
	@ 012,210 MSGET oMCompra	VAR nMcompra Picture "@E 999,999,999.99" Valid SIZE 130,08 OF oDlg PIXEL 
	@ 005,360 SAY "Limite de Cr�dito: " SIZE 060,08 OF oDlg PIXEL
	@ 012,360 MSGET oLimite 	VAR nLc	Picture "@E 999,999,999.99" Valid SIZE 130,08 OF oDlg PIXEL 
	@ 027,004 SAY "Nome:"	SIZE 060,08 OF oDlg PIXEL
	@ 034,004 MSGET oNome  	VAR cNomeCli		Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) When lConsulta	SIZE 190,08 OF oDlg PIXEL 
	@ 027,210 SAY "Cnpj /Cpf"	 		SIZE 060,08 OF oDlg PIXEL
	@ 034,210 MSGET oCnpj 	VAR cCnpj Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) When lConsulta	SIZE 070,08 OF oDlg PIXEL //Picture IIF(LEN(cCnpj)>11,"@E 99.999.999/9999-99","@E 999.999.999-99")  //F3 "SA1"
	@ 027,360 SAY "Saldo Em Dupl: " SIZE 060,08 OF oDlg PIXEL
	@ 034,360 MSGET oSaldUP	VAR nSaldup	Picture "@E 999,999,999.99" Valid SIZE 130,08 OF oDlg PIXEL 
	@ 049,004 SAY "Telefone:"	SIZE 060,08 OF oDlg PIXEL
	@ 056,004 MSGET oTel		VAR cTel 		Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 042,08 OF oDlg PIXEL
	@ 049,060 Say  "Contato:"   SIZE 060,08 OF oDlg PIXEL//	@ 210,004  TO 244, 366 LABEL "Desc.Cientifica:" OF oDlg PIXEL COLOR CLR_HBLUE
	@ 056,060 MSGET oContato	VAR cContato	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 049,130 Say  "1a.Compra:"   SIZE 060,08 OF oDlg PIXEL//	@ 210,004  TO 244, 366 LABEL "Desc.Cientifica:" OF oDlg PIXEL COLOR CLR_HBLUE
	@ 056,130 MSGET oPcompra	VAR dPcompra	Picture "99/99/99" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 049,210 Say  "Ult.Compra:"   SIZE 060,08 OF oDlg PIXEL//	@ 210,004  TO 244, 366 LABEL "Desc.Cientifica:" OF oDlg PIXEL COLOR CLR_HBLUE
	@ 056,210 MSGET oUComprao	VAR dUcompra	Picture "99/99/99" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 049,360 SAY "Saldo Limite: " SIZE 060,08 OF oDlg PIXEL
	@ 056,360 MSGET oSalLim	VAR nSalLim	Picture "@E 999,999,999.99" Valid SIZE 130,08 OF oDlg PIXEL 
	@ 071,004 SAY "CEP: " SIZE 060,08 OF oDlg PIXEL
	@ 078,004 MSGET oCEP	VAR cCEP	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 071,080 SAY "Endere�o: " SIZE 060,08 OF oDlg PIXEL
	@ 078,080 MSGET oEndCli	VAR cEndCli	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 120,08 OF oDlg PIXEL
	@ 071,210 SAY "Bairro: " SIZE 060,08 OF oDlg PIXEL
	@ 078,210 MSGET oBairro	VAR cBairro	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 071,360 SAY "Cidade: " SIZE 060,08 OF oDlg PIXEL
	@ 078,360 MSGET oCity	VAR cCity	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL	
	@ 093,004 SAY "Estado: " SIZE 060,08 OF oDlg PIXEL
	@ 100,004 MSGET oState	VAR cState	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 093,080 SAY "E-mail: " SIZE 060,08 OF oDlg PIXEL
	@ 100,080 MSGET oMail	VAR cMail	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 120,08 OF oDlg PIXEL
	@ 093,210 SAY "Data de Cadastro: " SIZE 060,08 OF oDlg PIXEL
	@ 100,210 MSGET oDtCad	VAR dDtCad	Picture "99/99/99" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 093,360 SAY "Cod Regi�o : " SIZE 060,08 OF oDlg PIXEL
	@ 100,360 MSGET oCodReg	VAR cCodReg	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 115,004 SAY "Regi�o: " SIZE 060,08 OF oDlg PIXEL
	@ 122,004 MSGET oDscReg	VAR cDscReg	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 115,080 SAY "Codigo Macro: " SIZE 060,08 OF oDlg PIXEL
	@ 122,080 MSGET oMacReg	VAR cMacReg	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 115,210 SAY "Macro Regi�o: " SIZE 060,08 OF oDlg PIXEL
	@ 122,210 MSGET oDscMReg	VAR cDscMReg	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 060,08 OF oDlg PIXEL
	@ 115,360 SAY "Observa��o: " SIZE 060,08 OF oDlg PIXEL
	@ 122,360 MSGET oObserv	VAR cObserv	Picture "@!" Valid RFATC01A(@cCodcli,@cNomered,@cNomeCli,@cCnpj,@cTel,@cContato,@nLc,@nSaldup,@nsallim,@dPcompra,@dUcompra,@nmCompra,@cEmail,@cEnder,@cMRegi,@cRegia,@aList1,@oListBox1,@nChamada,@dDtCad,@cEndCli,@cBairro,@cCity,@cState,@cCEP,@cMail,@cCodReg ,@cDscReg ,@cMacReg ,@cDscMReg,@cObserv) SIZE 130,08 OF oDlg PIXEL

	@ 150,004 LISTBOX oListBox1 VAR cListBox1 Fields ;
	HEADER OEMTOANSI(aTitulo1[1]),OEMTOANSI(aTitulo1[3]),OEMTOANSI(aTitulo1[7]),OEMTOANSI(aTitulo1[8]),OEMTOANSI(aTitulo1[9]),OEMTOANSI(aTitulo1[10], OEMTOANSI(aTitulo1[7]),OEMTOANSI(aTitulo1[8]),OEMTOANSI(aTitulo1[9]),OEMTOANSI(aTitulo1[10]),OEMTOANSI(aTitulo1[11]));   
	FIELDSIZES 5, GetTextWidth(0,"BBBBBB"), GetTextWidth(0,"BBBBBBBBBBBBBBBBB"),GetTextWidth(0,"BBBBBBBBBBBBBBB"), GetTextWidth(0,"BBBBBBBBBB"), GetTextWidth(0,"BBBBBBBBBBB"), GetTextWidth(0,"BBBBBBBBBB"), GetTextWidth(0,"BBBBBBBBBB"), GetTextWidth(0,"BBBBBBBBBBB"), GetTextWidth(0,"BBBBBBBBBBB"), ; 
    GetTextWidth(0,"BBBBBBBBBBBBBBBBBBBB"), GetTextWidth(0,"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB") SIZE 645,150 SCROLL PIXEL
    
    //GetTextWidth(0,"BBBBBBBBBBBBBBBBBBBB"), GetTextWidth(0,"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB") SIZE 645,150 SCROLL PIXEL
	oListBox1:SetArray(aList2) //oListBox1:SetArray(aList6)
	oListBox1:bLine 		:= {|| RCFATLIST(@oListBox1)}
	//oListBox1:bChange 		:= {|| RFATC01B(aList1,oListBox1:nAt),oListBox1:Refresh()}
	//oListBox1:blDblClick 	:= {|| RFATC01C(aList1,oListBox1:nAt,lRet,nChamada)}
    oListBox1:blDblClick 	:= {|| xPosFin(aList3,oListBox1:nAt,lRet,nChamada,cCodCli,cLoja)}    

	//@ 005,500 BUTTON Iif(lSigaGE,STR0105,STR0025) SIZE 60,12 FONT oDlg:oFont ACTION If( lFc010Bol, ExecBlock( "FC010BOL", .F., .F., {1,aVetAlias,aParam,.T.,aGet} ), _Fc010Brow(1,@aAlias,aParam,.T.,aGet))  OF oDlg PIXEL //"Boleto Aberto" / "Tit Aberto"
	@ 010,515 BUTTON "T�tulos em Aberto "  SIZE 070,10 ACTION RTITAX(aList1,oListBox1:nAt,.T.,nChamada)    OF oDlg PIXEL
	@ 025,515 BUTTON "T�tulos Baixados  "  SIZE 070,10 ACTION RTITBX(aList1,oListBox1:nAt,.T.,nChamada)    OF oDlg PIXEL
	@ 040,515 BUTTON "     Pedidos      "  SIZE 070,10 ACTION RFATC01PD(aList1,oListBox1:nAt,.T.,nChamada) OF oDlg PIXEL
	@ 055,515 BUTTON "   Faturamento    "  SIZE 070,10 ACTION RFATC01FT(aList1,oListBox1:nAt,.T.,nChamada) OF oDlg PIXEL
	@ 070,515 BUTTON "     Cliente      "  SIZE 070,10 ACTION xPosCli(aList3,oListBox1:nAt,lRet,nChamada,cCodCli,cLoja) OF oDlg PIXEL
	@ 090,515 BUTTON "     Cancela      "  SIZE 070,10 ACTION oDlg:End() Of oDlg Pixel      
	//@ 085,515 BUTTON "  Aprovar Pedido  "  SIZE 070,10 ACTION U__PolCo3(2)    OF oDlg PIXEL       
	//Define SButton  From 100,522 Type 1 Action oDlg:End()Enable Of oDlg Pixel //Apaga
	//Define SButton  From 100,552 Type 2 Action oDlg:End()Enable Of oDlg Pixel  

	//	ACTIVATE MSDIALOG oDlg Centered ON INIT EnchoiceBar(oDlg,bOk,bcancel,,aBotao)
	ACTIVATE MSDIALOG oDlg Centered //ON INIT EnchoiceBar(oDlg,bOK,bcancel,,aBotao)

	//	If nOpc == 0
	//		Exit
	//	EndIf

	//EndDo

	SetKey(12,NIL)
	SetKey(16,NIL)
	SetKey(22,NIL)

Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RFATC01A  �Autor  �New Bridge          � Data �  21.04.03   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RFATC01A(cCodigo,cNomered,cNome,cCnpj,cTel,Contato,nLc,nSaldup,nSallim,dPcompra,dUcompra,nMcompra,cEmail,cEnder,cMRegi,cRegia,aList1,oListBox1,nChamada,dDtCad,cEndCli,cBairro,cCity,cState,cCEP,cMail,cCodReg ,cDscReg ,cMacReg ,cDscMReg,cObserv)
	Local cQry	:= ""
	Local cOrd
	Local aLocal:= {}
	aList1	:= {}
	If ! oListBox1:nAt	== Nil 
		oListBox1:nAt:=	1
	EndIf
	
	//If Empty(Alltrim(cDescPro)+Alltrim(cDescGrp)+Alltrim(cCodGrp)+Alltrim(cCodPro)+Alltrim(cDescC))  
	If Empty(Alltrim(cCodigo)+Alltrim(cNomered)+Alltrim(cNome)+Alltrim(cCnpj)+Alltrim(cTel)+Alltrim(Contato))
		aAdd(aList1,{ "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
		//aAdd(aList6,{ "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""}) 
	Return(.t.)
	EndIf
	// Carrega o vetor aList com os clientes que satisfacao a condicao do filtro
	//cQry := "SELECT SA1.A1_COD, RTRIM(LTRIM(SA1.A1_NREDUZ)), RTRIM(LTRIM(SA1.A1_NOME)), SA1.A1_CGC, SA1.A1_TEL, SA1.A1_CONTATO,SA1.A1_LC,SA1.A1_SALDUP,(SA1.A1_LC-SA1.A1_SALDUP) AS A1_SALLIM ,A1_PRICOM,A1_ULTCOM,A1_MCOMPRA, SA1.R_E_C_N_O_ A1_RECNO ,A1_LOJA,"
	cQry := "SELECT SA1.A1_COD,  A1_NREDUZ,SA1.A1_NOME, SA1.A1_CGC, SA1.A1_TEL, SA1.A1_CONTATO,SA1.A1_LC,SA1.A1_SALDUP,(SA1.A1_LC-SA1.A1_SALDUP) AS A1_SALLIM ,A1_PRICOM,A1_ULTCOM,A1_MCOMPRA, SA1.R_E_C_N_O_ A1_RECNO ,A1_LOJA,"
    cQry += " RTRIM(LTRIM(A1_EMAIL))AS A1_EMAIL, RTRIM(LTRIM(A1_END))+'-'+RTRIM(LTRIM(A1_BAIRRO))+'-'+RTRIM(LTRIM(A1_EST)) AS A1_ENDT,  A1_XCODREG, A1_XDESREG, A1_XMICRRE, A1_XMICRDE, A1_DTCAD, A1_END, A1_BAIRRO, A1_MUN, "
	cQry += " 	 A1_EST, A1_CEP, A1_EMAIL, A1_XCODREG, A1_XDESREG, A1_XMICRRE, A1_XMICRDE, ISNULL(CONVERT(VARCHAR(2047), CONVERT(VARCHAR(2047), A1_XOBS)),'')  A1_XOBS " 
	cQry += " FROM "+RetSqlName("SA1")+" SA1  "
	cQry += " WHERE SA1.A1_FILIAL = '"+xFilial("SA1")+"'" 
	cQry += " AND SA1.A1_MSBLQL <> '1'"	 
	cQry += " AND SA1.D_E_L_E_T_ <> '*'"


//ISNULL(CONVERT(VARCHAR(1024),CONVERT(VARBINARY(1024),RA_XHSTBLQ)),'')
	If !Empty(Alltrim(cCodigo))
		cQry += " AND SA1.A1_COD LIKE '"+Trim(StrTran(cCodigo,"'"," "))+"%'"
	EndIf
	If !Empty(Alltrim(cNomered))
		cQry += " AND SA1.A1_NREDUZ LIKE '%"+Trim(StrTran(cNomered,"'"," "))+"%'"
	EndIf
	If !Empty(Alltrim(cNome))
		cQry += " AND SA1.A1_NOME LIKE '"+Trim(StrTran(cNome,"'"," "))+"%'"
	EndIf
	If !Empty(Alltrim(cCnpj))
		cQry += " AND SA1.A1_CGC LIKE '"+Trim(StrTran(cCnpj,"'"," "))+"%'"
	EndIf
	If !Empty(Alltrim(cTel))
		cQry += " AND SA1.A1_TEL LIKE '"+Trim(StrTran(cTel,"'"," "))+"%'"
	EndIf
	If !Empty(Alltrim(Contato))
		cQry += " AND SA1.A1_CONTATO LIKE '"+Trim(StrTran(Contato,"'"," "))+"%'"
	EndIf	     
    cOrd := " SA1.A1_NOME+ SA1.A1_COD"
	cQry += " ORDER BY" + cOrd

		MEMOWRIT("C:\TEMP\RFATC01.SQL",cQry)
	cQry := ChangeQuery(cQry)
	CursorWait() // Cursor em Processamento
	
	If Select("XTMP") > 0
		XTMP->(DbCloseArea())
	EndIf

	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQry),"XTMP", .F., .T.)

	dbSelectArea("xTMP")
	dbGoTop()
	aList1	:= {}
	aList6	:= {}

	// 1o. Elemento = Codigo		  -> A1_COD
	// 2o. Elemento = Nome		      -> A1_NOME
	// 3o. Elemento = Cnpj 		      -> A1_CNPJ
	// 4o. Elemento = Telefone        -> B1_TEL
	// 5o. Elemento = Contato    	  -> B1_CONTATO
	// 6o. Elemento = Limit.Cr�dito	  -> A1_LC
	// 7o. Elemento = Saldo Cr�dito	  -> A1_SALDUP
    // 8o. Elemento =   Endere�o      -> A1_ENDE, A1_END+'-'+A1_BAIRRO+'-'+A1_EST AS A1_ENDT
    // 9o. Elemento =   email        -> A1_EMAIL 
    // 10o. Elemento =   macro regiao -> A1_XMICRDE
    // 11o. Elemento =   regiao       -> A1_XDESREG
       
      
	While xTMP->(!Eof())	
	//	aAdd(aList1,{"",Padr(TMP->A1_COD,10),Padr(TMP->A1_NREDUZ,15),Padr(TMP->A1_NOME,30),Padr(TMP->A1_CGC,19),Padr(TMP->A1_TEL,11),Padr(TMP->A1_CONTATO,15),;
	//	TMP->A1_LC,TMP->A1_SALDUP,TMP->A1_SALLIM ,TMP->A1_PRICOM,TMP->A1_ULTCOM,TMP->A1_MCOMPRA,TMP->A1_LOJA ,TMP->A1_EMAIL, TMP->A1_ENDT,TMP->A1_XMICRDE,TMP->A1_XDESREG, space(4)}) 
	//	//ENDERE�O EMAIL MACRO REGIAO E REGIAO 
	aAdd(aList1,{"",Padr(xTMP->A1_COD,10),Padr(xTMP->A1_NREDUZ,15),Padr(xTMP->A1_NOME,30),Padr(xTMP->A1_CGC,19),Padr(xTMP->A1_TEL,11),Padr(xTMP->A1_CONTATO,15), xTMP->A1_EMAIL, xTMP->A1_ENDT,xTMP->A1_XMICRDE,xTMP->A1_XDESREG,;
		xTMP->A1_LC,xTMP->A1_SALDUP,xTMP->A1_SALLIM ,xTMP->A1_PRICOM,xTMP->A1_ULTCOM,xTMP->A1_MCOMPRA,xTMP->A1_LOJA , space(4) , xTMP->A1_DTCAD, xTMP->A1_END, xTMP->A1_BAIRRO, xTMP->A1_MUN, xTMP->A1_EST, xTMP->A1_CEP, xTMP->A1_EMAIL, xTMP->A1_XCODREG, xTMP->A1_XDESREG, xTMP->A1_XMICRRE, xTMP->A1_XMICRDE, xTMP->A1_XOBS } )  
	aAdd(aList6,{"",Padr(xTMP->A1_NOME,30),xTMP->A1_EMAIL, xTMP->A1_ENDT,xTMP->A1_XMICRDE,xTMP->A1_XDESREG,,space(4) } )
//  		aAdd(aList1,{"",Padr(TMP->A1_COD,10),Padr(TMP->A1_NREDUZ,15),Padr(TMP->A1_NOME,30),Padr(TMP->A1_CGC,19),Padr(TMP->A1_TEL,11),Padr(TMP->A1_CONTATO,15)})
		
		dbSelectArea("xTMP")
		xTMP->(dbSkip())
	EndDo

	//Caso nao encontre nenhum produto
	If Len(aList1) == 0
		aAdd(aList1,{ "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
		aAdd(aList6,{ "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""}) 
	EndIf

	dbSelectArea("xTMP")
	DbCloseArea()

	oListBox1:SetArray(aList1)  
	//oListBox1:bLine 		:= {|| {LoadBitmap(GetResources(), RFATC01D(oListBox1:aArray,oListBox1:nAt) ),aList1[oListBox1:nAt,2],aList1[oListBox1:nAt,3],aList1[oListBox1:nAt,4],;
	//								aList1[oListBox1:nAt,5],aList1[oListBox1:nAt,6],aList1[oListBox1:nAt,7]}}

	oListBox1:bLine 		:= {|| RCFATLIST(@oListBox1)}


	If Len(aList1) == 0
		aAdd(aList1,{ "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
		aAdd(aList6,{ "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""}) 
	Else
		//RFATC01b(aList1,1)	
	EndIf

	oListBox1:Refresh()     
	cCodigo  := aList1[1][2]
	cNome    := aList1[1][4]
	cCnpj    := aList1[1][5]
	cTel     := aList1[1][6]
	Contato  := aList1[1][7]
	cE_mail  := aList1[1][8]
	cEndere  := aList1[1][9] 
	cMicror  := aList1[1][10] 
	cRegiao  := aList1[1][11]
	nLc      := aList1[1][12]
	nSaldup  := aList1[1][13]  
	nSallim  := aList1[1][14]         //20170111
	dPcompra := ctod(substr(aList1[1][15],7,2)+'/'+substr(aList1[1][15],5,2)+'/'+substr(aList1[1][15],3,2) )
	dUcompra := ctod(substr(aList1[1][16],7,2)+'/'+substr(aList1[1][16],5,2)+'/'+substr(aList1[1][16],3,2) )
	nMcompra := aList1[1][17]
	cLoja    := aList1[1][18]
	cNomeRed := aList1[1][3] //Adicionado por Robson (nao preenchia nome do cliente na tela)


	//Inicio + Adicionado por Robson (Solicitacao da Karla - Especificacao Posicao de Cliente)
	dDtCad  := stod(aList1[1][20]) //Data do Cadastro
	cEndCli := aList1[1][21] //Endere�o
	cBairro := aList1[1][22] //Bairro
	cCity   := aList1[1][23] //Cidade
	cState  := aList1[1][24] //Estado
	cCEP    := aList1[1][25] //CEP
	cMail   := aList1[1][26] //Email
	cCodReg := aList1[1][27] //Cod Regi�o 
	cDscReg := aList1[1][28] //Regi�o 
	cMacReg := aList1[1][29] //C�digo Macro 
	cDscMReg:= aList1[1][30] //Macro Regi�o 
	cObserv := aList1[1][31] //Observa��o 
	//Fim - Adicionado por Robson(Solicitacao da Karla - Especificacao Posicao de Cliente)


	//nLc     := TMP->A1_LC
	//nSaldup := TMP->A1_SALDUP 
	// 1o. Elemento = Codigo		-> A1_COD
	// 2o. Elemento = Nome Reduzido	-> A1_NREDUZ
	// 3o. Elemento = Nome		    -> A1_NOME
	// 4o. Elemento = Cnpj 		    -> A1_CNPJ
	// 5o. Elemento = Telefone      -> B1_TEL
	// 6o. Elemento = Contato    	-> B1_CONTATO


	CursorArrow() // Cursor Normal                                                                                      

	//RFATC01c(aList1,oListBox1:nAt,.T.,nChamada)
	oDlg:Refresh()

Return(Nil)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RFATC01D  �Autor  �Deny B. de Mendonca � Data �  01.05.03   ���
�������������������������������������������������������������������������͹��
���Desc.     �Verifica a cor do led de acordo o status do funcionario	  ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function RFATC01D(aListAchou,nCol)

	Local cRet,cFlag

	If Len(aListAchou)>0
		cFlag := aListAchou[nCol,1]
		Do Case
			Case cFlag == 'S'
			cRet := "BR_VERDE"
			Case cFlag == 'N'
			cRet := "BR_VERMELHO"
			OtherWise
			cRet := "BR_PRETO"
		EndCase
	Else
		cRet := "BR_PRETO"
	EndIf	
Return cRet

/*
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
����������������������������������������������������������������������������ͻ��
���Programa   �RFATC01LE � Autor �New Bridge                 �Data�01.05.2003���
����������������������������������������������������������������������������͹��
���Descricao  �Legenda                                                       ��� 
����������������������������������������������������������������������������͹��
���Observacao �                                                              ���
����������������������������������������������������������������������������ͼ��
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
*/

User Function RFATC01LE()

	BrwLegenda( cCadastro, "Legenda", {{'BR_VERDE'		, 'Ativo'			},;
	{'BR_VERMELHO'	, 'Inativo' 		},;	
	{'BR_PRETO'		, 'Sem Classificacao'}})
Return(.t.)

Static Function RCFATLIST(oListBox1)
	Local aRetorno:={}
	If Len(aList6) == 0
		aList2 := aClone(oListBox1:aArray)
	EndIf

	AADD(aRetorno,LoadBitmap(GetResources(), RFATC01D(oListBox1:aArray,oListBox1:nAt) ))
	AADD(aRetorno,aList6[oListBox1:nAt,2])
	AADD(aRetorno,aList6[oListBox1:nAt,3])
	AADD(aRetorno,aList6[oListBox1:nAt,4])
	AADD(aRetorno,aList6[oListBox1:nAt,5])
	AADD(aRetorno,aList6[oListBox1:nAt,6])
    AADD(aRetorno,aList6[oListBox1:nAt,7])
	//AADD(aRetorno,aList2[oListBox1:nAt,8])
	//AADD(aRetorno,aList2[oListBox1:nAt,9])
	//AADD(aRetorno,aList2[oListBox1:nAt,10])
	//AADD(aRetorno,aList2[oListBox1:nAt,11])
	//AADD(aRetorno,aList2[oListBox1:nAt,12])
	//AADD(aRetorno,aList2[oListBox1:nAt,13])
	//AADD(aRetorno,aList2[oListBox1:nAt,14])
	
Return aRetorno       


Static Function RCFATLIS1(oLtBx2)
	Local aRetorno:={}
	If Len(aList2) == 0
		aList2 := aClone(oLtBx2:aArray)
	EndIf

	AADD(aRetorno,LoadBitmap(GetResources(), RFATC01D(oLtBx2:aArray,oLtBx2:nAt) ))
	AADD(aRetorno,aList2[oLtBx2:nAt,2])
	AADD(aRetorno,aList2[oLtBx2:nAt,3])
	AADD(aRetorno,aList2[oLtBx2:nAt,4])
	AADD(aRetorno,aList2[oLtBx2:nAt,5])
	AADD(aRetorno,aList2[oLtBx2:nAt,6])
	AADD(aRetorno,aList2[oLtBx2:nAt,7])
	AADD(aRetorno,aList2[oLtBx2:nAt,8])
	AADD(aRetorno,aList2[oLtBx2:nAt,9])
	AADD(aRetorno,aList2[oLtBx2:nAt,10])
	AADD(aRetorno,aList2[oLtBx2:nAt,11])
	AADD(aRetorno,aList2[oLtBx2:nAt,12])
	AADD(aRetorno,aList2[oLtBx2:nAt,13])
	AADD(aRetorno,aList2[oLtBx2:nAt,14])

Return aRetorno       


Static Function RCFATLIS3(oLtBx3)
	Local aRetorno:={}
	If Len(aList3) == 0
		aList3 := aClone(oLtBx3:aArray)
	EndIf

	AADD(aRetorno,LoadBitmap(GetResources(), RFATC01D(oLtBx3:aArray,oLtBx3:nAt) ))
	AADD(aRetorno,aList3[oLtBx3:nAt,2])
	AADD(aRetorno,aList3[oLtBx3:nAt,3])
	AADD(aRetorno,aList3[oLtBx3:nAt,4])
	AADD(aRetorno,aList3[oLtBx3:nAt,5])
	AADD(aRetorno,aList3[oLtBx3:nAt,6])
	AADD(aRetorno,aList3[oLtBx3:nAt,7])
	AADD(aRetorno,aList3[oLtBx3:nAt,8])
	AADD(aRetorno,aList3[oLtBx3:nAt,9])
	AADD(aRetorno,aList3[oLtBx3:nAt,10])
	AADD(aRetorno,aList3[oLtBx3:nAt,11])
	AADD(aRetorno,aList3[oLtBx3:nAt,12])
	AADD(aRetorno,aList3[oLtBx3:nAt,13])
	AADD(aRetorno,aList3[oLtBx3:nAt,14])

Return aRetorno    

Static Function RCFATLIS4(oLtBx4)
	Local aRetorno:={}
	If Len(aList4) == 0
		aList4 := aClone(oLtBx4:aArray)
	EndIf

	AADD(aRetorno,LoadBitmap(GetResources(), RFATC01D(oLtBx4:aArray,oLtBx4:nAt) ))
	AADD(aRetorno,aList4[oLtBx4:nAt,2])
	AADD(aRetorno,aList4[oLtBx4:nAt,3])
	AADD(aRetorno,aList4[oLtBx4:nAt,4])
	AADD(aRetorno,aList4[oLtBx4:nAt,5])
	AADD(aRetorno,aList4[oLtBx4:nAt,6])
	AADD(aRetorno,aList4[oLtBx4:nAt,7])
	AADD(aRetorno,aList4[oLtBx4:nAt,8])
	AADD(aRetorno,aList4[oLtBx4:nAt,9])
	AADD(aRetorno,aList4[oLtBx4:nAt,10])
	//AADD(aRetorno,aList4[oLtBx4:nAt,11])
	//AADD(aRetorno,aList4[oLtBx4:nAt,12])
	//AADD(aRetorno,aList4[oLtBx4:nAt,13])
	//AADD(aRetorno,aList4[oLtBx4:nAt,14])

Return aRetorno       
   
Static Function RCFATLIS5(oLtBx5)
	Local aRetorno:={}
	If Len(aList5) == 0
		aList5 := aClone(oLtBx5:aArray)
	EndIf

	AADD(aRetorno,LoadBitmap(GetResources(), RFATC01D(oLtBx5:aArray,oLtBx5:nAt) ))
	AADD(aRetorno,aList5[oLtBx5:nAt,2])
	AADD(aRetorno,aList5[oLtBx5:nAt,3])
	AADD(aRetorno,aList5[oLtBx5:nAt,4])
	AADD(aRetorno,aList5[oLtBx5:nAt,5])
	AADD(aRetorno,aList5[oLtBx5:nAt,6])
	AADD(aRetorno,aList5[oLtBx5:nAt,7])
	AADD(aRetorno,aList5[oLtBx5:nAt,8])
	AADD(aRetorno,aList5[oLtBx5:nAt,9])
	AADD(aRetorno,aList5[oLtBx5:nAt,10])
	AADD(aRetorno,aList5[oLtBx5:nAt,11])
	AADD(aRetorno,aList5[oLtBx5:nAt,12])
	AADD(aRetorno,aList5[oLtBx5:nAt,13])
	AADD(aRetorno,aList5[oLtBx5:nAt,14])

Return aRetorno       

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RFATC01C �Autor  �Microsiga           � Data �  08/29/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RTITAX(aList1,nLinha,lRet,nChamada)

	Local aArea  := GetArea()
	Local aList2 := {}
	Local oLtBx2
	Local cListBox2                
	Local cTitulo   := "Consulta Titulos em Abertos "
	Local aTitulo2	:=  {"","Filial","Prefixo","N�mero","Parcela","Tipo","Emiss�o","Baixa","Vencimento","Vecto Real","Valor","Atraso","Situa��o","Hist�rico"}  
	Local cQuery    := ""               
	Local aBotao    := {}
	Local bOk       := {|| _RFATC013(aList6,oLtBx2:nAt,lRet,nChamada),oDl2g:End()}
	Local nOpc		:= 0
	Local aObjects := {}
	Local aInfo    := {}
	Local aPosObj  := {}
	Local aSize    := MsAdvSize(.F.)
	Local NVLTOT   := 0
	Local nvlTot   := 0                             
    Local QtdDup   := 0
	Private cCodcli   := aList1[nLinha][2]
	Private cNomecl   := aList1[nLinha][4]       
	Private cCnpj     := aList1[nLinha][5] 
	Private nVallim   := Posicione("SA1",3,xFilial("SA1")+ALLTRIM(aList1[nLinha][5]),"A1_LC")
	Private dDtaLim   := SA1->A1_VENCLC 
	Private cLoja     := SA1->A1_LOJA

	//AADD(aBotao, {"Impress�o"    	, { ||U_RFATC01LE()	}, "Legenda - <Ctrl-L>"})
	//AADD(aBotao, {"Visualiza" 		, { ||AxVisual("SA1",SA1->(RECNO()),2)	}, "Visualizar - <Ctrl-V>"})
	//AADD(aBotao, {"Sit.Financeira" 	, { ||xPosFin(aList2,nLinha,lRet,nChamada,cCodcli,cLoja)  }, "Situa��o Financeira - <Ctrl-P>"})
//	SetKey(VK_F8, { || IIF(lKey.AND.aCfgBtn[14][1],(	lKey:= .F.,Tk271SituaC(),lKey:= .T.), "") })

	dbSelectArea("SM0")
	cFilAnt:=SM0->M0_CODFIL
	cEmpAnt:=SM0->M0_CODIGO
	SM0->(dbGotop())
    nQtdAtr:= 0
	nQtdDup:= 0 
	nValor := 0  
	nvlTot := 0
	aList2 := {}
    nQtdVen:= 0
    nqtdAve:= 0
    nVlrVen:= 0
    nVlrAve:= 0
    dPrVenc:= Ctod(space(8))
    dUlVenc:= Ctod(space(8))
    nPrVenc:= 0
    nUlVenc:= 0
     
	If Select("XTRB") > 0 
		DbSelectArea("XTRB")
		XTRB->(DbCloseArea())
	EndIf

	cQuery := "SELECT E1_FILIAL,E1_PREFIXO,E1_NUM ,E1_PARCELA,E1_TIPO,E1_EMISSAO,E1_BAIXA,E1_VENCTO,E1_VENCREA,E1_VALOR, DATEDIFF(DAY,E1_VENCTO,'"+DTOS(dDatabase)+"') AS E1_ATRASO,E1_SITUACA,E1_HIST" 
	cQuery += "FROM "+RetSqlName("SE1")+"  WHERE E1_CLIENTE = '"+cCodcli+"' AND E1_BAIXA = ''  AND D_E_L_E_T_ = ''  order by  E1_VENCTO   "
	MemoWrite("HPCONSCLIF_2.txt",cQuery)
	cQuery := ChangeQuery(cQuery)
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRB", .F., .T.)       
	XTRB->(dbgotop())
	If XTRB->(Eof()) 
		MsgStop("Nenhum Titulo em aberto para este cliente" , "Verifique") 
		Return()
	Endif   	
    XTRB->(dbgotop())   
    dPrVenc := ctod(Substr(XTRB->E1_VENCREA,7,2)+'/'+Substr(XTRB->E1_VENCREA,5,2)+'/'+Substr(XTRB->E1_VENCREA,3,2))
    nPrVenc := iif((XTRB->E1_TIPO == 'NCC' .or. XTRB->E1_TIPO == 'RA ')  ,XTRB->E1_VALOR *-1,XTRB->E1_VALOR) 

	While XTRB->(!EOF())
        dVenNor := ctod(Substr(XTRB->E1_VENCTO,7,2)+'/'+Substr(XTRB->E1_VENCTO,5,2)+'/'+Substr(XTRB->E1_VENCTO,3,2)) 
	    dVencim := ctod(Substr(XTRB->E1_VENCREA,7,2)+'/'+Substr(XTRB->E1_VENCREA,5,2)+'/'+Substr(XTRB->E1_VENCREA,3,2))
	    dBaixan := ctod(Substr(XTRB->E1_BAIXA,7,2)+'/'+Substr(XTRB->E1_BAIXA,5,2)+'/'+Substr(XTRB->E1_BAIXA,3,2))
	    If dVencim >= dDataBase
	       nqtdAve++
           nVlrAve:= iif((XTRB->E1_TIPO == 'NCC' .or. XTRB->E1_TIPO == 'RA '),XTRB->E1_VALOR *-1,XTRB->E1_VALOR)+nVlrAve
           If dPrVenc < dDataBase 
              dPrVenc := dVencim 
              nPrVenc := iif((XTRB->E1_TIPO == 'NCC' .or. XTRB->E1_TIPO == 'RA '),XTRB->E1_VALOR *-1,XTRB->E1_VALOR)
           Endif
           nQtdAtr :=0 
	    Else
	       nqtdVen++
	       nVlrVen:= iif((XTRB->E1_TIPO == 'NCC' .or. XTRB->E1_TIPO == 'RA '),XTRB->E1_VALOR *-1,XTRB->E1_VALOR)+nVlrVen
	       nQtdAtr:= dDataBase - dVencim
 	    Endif   
		if XTRB->E1_SITUACA == '0'
			cSituc := "Carteira"
		Else 
			cSituc := "Em Banco" 
		Endif
		nValor := 0
		if (XTRB->E1_TIPO == 'NCC' .or. XTRB->E1_TIPO == 'RA ')
		   nValor := XTRB->E1_VALOR *-1
		Else
		   nValor := XTRB->E1_VALOR
		Endif   
		dEmissao :=  ctod(Substr(XTRB->E1_EMISSAO,7,2)+'/'+Substr(XTRB->E1_EMISSAO,5,2)+'/'+Substr(XTRB->E1_EMISSAO,3,2))
		AADD(aList2,{"",XTRB->E1_FILIAL,XTRB->E1_PREFIXO,XTRB->E1_NUM,XTRB->E1_PARCELA,XTRB->E1_TIPO,dEmissao,dBaixan,dVenNor,dVencim,transfor(nValor,"@e 999999999,999,999.99"), nQtdAtr,cSituc,XTRB->E1_HIST}) 
		nvlTot:=nvlTot+nValor
		nQtdDup++
		dUlVenc := dVencim //XTRB->E1_VENCREA
        nUlVenc := iif((XTRB->E1_TIPO == 'NCC' .or. XTRB->E1_TIPO == 'RA '),XTRB->E1_VALOR *-1,XTRB->E1_VALOR) 
		XTRB->(dbSKIP())

	Enddo
	If len(aList2)== 0
		aAdd(aList2,{ "","","","","","","","","","","","",""})
	Endif		
	//While .T.

	Aadd(aObjects, {100, 010, .T., .F.})
	//	Aadd(aObjects, {200, 200, .T., .T.})
	Aadd(aObjects, {100, 100, .T., .T.})

	aInfo   := {aSize[1], aSize[2], aSize[3], aSize[4], 3, 2}
	aPosObj := MsObjSize(aInfo, aObjects)


	DEFINE MSDIALOG oDl2g TITLE cTitulo From 000,000 To 530,935 OF oMainWnd PIXEL
	@ 035,004 LISTBOX oLtBx2 VAR cListBox2 Fields HEADER OEMTOANSI(aTitulo2[1]),OEMTOANSI(aTitulo2[2]),OEMTOANSI(aTitulo2[3]),OEMTOANSI(aTitulo2[4]),OEMTOANSI(aTitulo2[5]),OEMTOANSI(aTitulo2[6]),OEMTOANSI(aTitulo2[7]),OEMTOANSI(aTitulo2[8]),OEMTOANSI(aTitulo2[9]),OEMTOANSI(aTitulo2[10]),OEMTOANSI(aTitulo2[11]),OEMTOANSI(aTitulo2[12]),OEMTOANSI(aTitulo2[13]),OEMTOANSI(aTitulo2[14]) FIELDSIZES 14, GetTextWidth(0,"BB"), GetTextWidth(0,"BBBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBBBBBBBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBBBBBBBB"), GetTextWidth(0,"BBBBBBBB"), GetTextWidth(0,"BBBBBBBB"), GetTextWidth(0,"BBBBBBB"), GetTextWidth(0,"BBBBBBB"),GetTextWidth(0,"BBBBBBB"),	GetTextWidth(0,"BBBBBBB"), GetTextWidth(0,"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB") SIZE 460,100 SCROLL PIXEL 
    @ 150,020 Say "Qtd Vencidas "      SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED
    @ 150,105 MSGET onQtdVen VAR nQtdVen When .f. SIZE 060,08 OF oDl2g PIXEL Picture "@E 999,999"
    @ 150,220 Say "Vlr Vencidas "      SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED
    @ 150,305 MSGET onVlrVen VAR nVlrVen When .f. SIZE 060,08 OF oDl2g PIXEL Picture "@E 999,999,999.99"
    @ 170,020 Say "Qtd A Vencer   "      SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED
    @ 170,105 MSGET onQtdAve VAR nQtdAve When .f. SIZE 060,08 OF oDl2g PIXEL Picture "@E 999,9999"
    @ 170,220 Say "Vlr A Vencer   "      SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED
    @ 170,305 MSGET onVlrAve VAR nVlrAve When .f. SIZE 060,08 OF oDl2g PIXEL Picture "@E 999,999,999.99"
    @ 190,020 Say "Pr�ximo  Vencimento"  SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED
    @ 190,105 MSGET odPrVenc VAR dPrVenc When .f. SIZE 060,08 OF oDl2g PIXEL  //Picture "@E 999,999,999.99"
    @ 190,220 Say "Vlr Prox.Vencimento"  SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED
    @ 190,305 MSGET onPrVenc VAR nPrVenc When .f. SIZE 060,08 OF oDl2g PIXEL Picture "@E 999,999,999.99"
    @ 210,020 Say "Ultimo   Vencimento"  SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED
    @ 210,105 MSGET odUlVenc VAR dUlVenc When .f. SIZE 060,08 OF oDl2g PIXEL //Picture "@E 999,999,999.99"
    @ 210,220 Say "Vlr Ult. Vencimento"  SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED
    @ 210,305 MSGET onUlVenc VAR nUlVenc When .f. SIZE 060,08 OF oDl2g PIXEL Picture "@E 999,999,999.99"
	@ 230,020 SAY "Qtd Em aberto: " SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED	 
	@ 230,105 MSGET onQtdDup VAR nQtdDup  When .f. SIZE 060,08 OF oDl2g PIXEL Picture "@E 999,999"
	@ 230,220 SAY "Total em Aberto: " SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED	 
	@ 230,305 MSGET onvlTot VAR nvlTot  When .f. SIZE 060,08 OF oDl2g PIXEL Picture "@E 999,999,999.99"
	
	oLtBx2:SetArray(aList2)
    Define SButton  From 245,400 Type 1 Action oDl2g:End() Enable Of oDl2g Pixel //Apaga
	oLtBx2:bLine 		:= {|| RCFATLIS1(@oLtBx2)}
	oLtBx2:blDblClick 	:= {|| xPosFin(aList2,oLtBx2:nAt,lRet,nChamada,SA1->A1_COD,SA1->A1_LOJA)}   
	ACTIVATE MSDIALOG oDl2g Centered //ON INIT EnchoiceBar(oDl2g,bOK,,,aBotao)
	SetKey(12,NIL)
	SetKey(16,NIL)
	SetKey(22,NIL)
	RestArea(aArea)
Return(lRet)



/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RFATC01C �Autor  �Microsiga           � Data �  08/29/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RTITBX(aList1,nLinha,lRet,nChamada)

	Local aArea  := GetArea()
	Local aList3 := {}
	Local oLtBx3
	Local cListBox3                
	Local cTitulo   := "Consulta Titulos Baixados "
	Local aTitulo3	:=  {"","Filial","Prefixo","N�mero","Parcela","Tipo","Emiss�o","Baixa","Vencimento","Vecto Real","Valor","Atraso","Situa��o","Hist�rico"}  
	Local cQuery    := ""               
	Local aBotao    := {}
	Local bOk       := {|| _RFATC013(aList2,oLtBx2:nAt,lRet,nChamada),oDl2g:End()}
	Local nOpc		:= 0
	Local aObjects := {}
	Local aInfo    := {}
	Local aPosObj  := {}
	Local aSize    := MsAdvSize(.F.)
	Local NVLTOT   := 0
	Local nvlTot   := 0                             
    Local QtdDup   := 0
	Private cCodcli   := aList1[nLinha][2]
	Private cNomecl   := aList1[nLinha][4]      
	Private cCnpj     := aList1[nLinha][5] 
	Private nVallim   := Posicione("SA1",3,xFilial("SA1")+ALLTRIM(aList1[nLinha][5]),"A1_LC")
	Private dDtaLim   := SA1->A1_VENCLC 
	Private cLoja     := SA1->A1_LOJA
    nQtdPDia:= 0
    nVlrPDia:= 0
    nQtdPAtr:= 0
    nVlrPAtr:= 0
    nMairAtr:= 0
    nValor  := 0          
	nvlTot  := 0           
    nqtdTit := 0  
    nQtdAtr := 0
    dUlPgato:= ctod(space(08))
    nUlPagto:= 0 
    NQTDDUP := 0
	If Select("XTRB") > 0 
		DbSelectArea("XTRB")
		XTRB->(DbCloseArea())
	EndIf
	
	If Select("XNCCTRB") > 0 
		DbSelectArea("XNCCTRB")
		XNCCTRB->(DbCloseArea())
	EndIf
              
	//Manutencao DATEDIFF 30/01/2019
	cQuery := "SELECT E1_FILIAL,E1_PREFIXO,E1_NUM ,E1_PARCELA,E1_TIPO,E1_EMISSAO,E1_BAIXA,E1_VENCTO,E1_VENCREA,E1_VALOR, DATEDIFF(DAY,E1_VENCTO,E1_BAIXA) AS E1_ATRASO,E1_SITUACA,E1_HIST " 
	cQuery += "FROM "+RetSqlName("SE1")+"  WHERE E1_CLIENTE = '"+cCodcli+"' AND E1_BAIXA <> ''  AND D_E_L_E_T_ = ''  order by E1_VENCREA DESC "
	MemoWrite("HPCONSCLIF_2.txt",cQuery)
	cQuery := ChangeQuery(cQuery)
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRB", .F., .T.)       
	XTRB->(dbgotop())
	If XTRB->(Eof()) 
		MsgStop("Nenhum Titulo Baixado para este cliente" , "Verifique") 
		Return()
	Endif   	
    XTRB->(dbgotop())   
	While XTRB->(!EOF())
        dVenNor := ctod(Substr(XTRB->E1_VENCTO,7,2)+'/'+Substr(XTRB->E1_VENCTO,5,2)+'/'+Substr(XTRB->E1_VENCTO,3,2)) 
	    dVencim := ctod(Substr(XTRB->E1_VENCREA,7,2)+'/'+Substr(XTRB->E1_VENCREA,5,2)+'/'+Substr(XTRB->E1_VENCREA,3,2))
	    dBaixan := ctod(Substr(XTRB->E1_BAIXA,7,2)+'/'+Substr(XTRB->E1_BAIXA,5,2)+'/'+Substr(XTRB->E1_BAIXA,3,2))
	    If dBaixan <= dVencim
           nQtdPDia++ 
           nVlrPDia := nVlrPDia +iif(XTRB->E1_TIPO == 'NCC',XTRB->E1_VALOR *-1,XTRB->E1_VALOR)
           nQtdAtr  := 0
        Else
           nQtdPAtr++ 
           nVlrPAtr:= nVlrPAtr +iif(XTRB->E1_TIPO == 'NCC',XTRB->E1_VALOR *-1,XTRB->E1_VALOR)
           nQtdAtr := dBaixan - dVencim 
           If nQtdAtr >= nMairAtr 
              nMairAtr := nQtdAtr 
           Endif   
        Endif
		if XTRB->E1_SITUACA == '0'
			cSituc := "Carteira"
		Else 
			cSituc := "Em Banco" 
		Endif
		nValor := 0
		if XTRB->E1_TIPO == 'NCC'
		   //nValor := XTRB->E1_VALOR *-1
		Else
		   nValor := XTRB->E1_VALOR
		Endif   
		dEmissao :=  ctod(Substr(XTRB->E1_EMISSAO,7,2)+'/'+Substr(XTRB->E1_EMISSAO,5,2)+'/'+Substr(XTRB->E1_EMISSAO,3,2))
	//	AADD(aList2,{"",XTRB->E1_FILIAL,XTRB->E1_PREFIXO,XTRB->E1_NUM,XTRB->E1_PARCELA,XTRB->E1_TIPO,dEmissao,dBaixan,dVenNor,dVencim,transfor(nValor,"@e 999999999,999,999.99"), nQtdAtr,cSituc,XTRB->E1_HIST}) 
		//RBM inclusao de validacao de ncc para nao mostrar na tela os valores negativos somados acima
		If XTRB->E1_TIPO <> 'NCC
			AADD(aList3,{"",XTRB->E1_FILIAL,XTRB->E1_PREFIXO,XTRB->E1_NUM,XTRB->E1_PARCELA,XTRB->E1_TIPO,dEmissao,dBaixan,dVenNor,dVencim,transfor(nValor,"@e 999999999,999,999.99"), nQtdAtr,cSituc,XTRB->E1_HIST}) 
		Endif
		nvlTot:=nvlTot+nValor
		nQtdDup++
	    dUlPgato:= dBaixan 
        nUlPagto:= iif(XTRB->E1_TIPO == 'NCC',XTRB->E1_VALOR *-1,XTRB->E1_VALOR)
		XTRB->(dbSKIP())
	Enddo
	
	
	/*cQuery := "SELECT E1_FILIAL,E1_PREFIXO,E1_NUM ,E1_PARCELA,E1_TIPO,E1_EMISSAO,E1_BAIXA,E1_VENCTO,E1_VENCREA,E1_VALOR, 0  as E1_ATRASO,E1_SITUACA,E1_HIST " 
	cQuery += "FROM "+RetSqlName("SE1")+"  WHERE E1_CLIENTE = '"+cCodcli+"'AND E1_TIPO = 'NCC' AND E1_BAIXA <> ''  AND D_E_L_E_T_ = ''  order by  E1_BAIXA desc "
	cQuery := ChangeQuery(cQuery)
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XNCCTRB", .F., .T.)       
	XTRB->(dbgotop()) 	
	
	nValNcc:= 0
	
	While XNCCTRB->(!EOF())
		//nValNcc+= XNCCTRB->E1_VALOR
		XNCCTRB->(dbSKIP())
	Enddo*/
	
	// Incluido os valores de NCC no total para bater com o FINC010 padrao. Rbm
	//nvlTot+= nValNcc
	
	If len(aList3)== 0
		aAdd(aList3,{ "","","","","","","","","","","","",""})
	Endif		
	//While .T.

	Aadd(aObjects, {100, 010, .T., .F.})
	//	Aadd(aObjects, {200, 200, .T., .T.})
	Aadd(aObjects, {100, 100, .T., .T.})

	aInfo   := {aSize[1], aSize[2], aSize[3], aSize[4], 3, 2}
	aPosObj := MsObjSize(aInfo, aObjects)
	
	

	DEFINE MSDIALOG oDl3g TITLE cTitulo From 000,000 To 530,935 OF oMainWnd PIXEL
//	@ 030,004 SAY "Codigo:"		SIZE 030,08 OF oDl2g PIXEL COLOR CLR_HBLUE
//	@ 012,004 MSGET oDesc1  	VAR cCodCli	Picture "@!"  When .f. SIZE 045,08 OF oDl2g PIXEL 
//	@ 060,004 SAY "Nome:"	SIZE 060,08 OF oDl2g PIXEL
//	@ 034,004 MSGET oNome  	VAR cNomecl	Picture "@!"  When .f.	SIZE 190,08 OF oDl2g PIXEL 
//	@ 027,200 SAY "Cnpj / Cpf:"	 		SIZE 076,08 OF oDl2g PIXEL

//	@ 034,200 MSGET oCnpj 	VAR cCnpj When .f. SIZE 070,08 OF oDl2g PIXEL //Picture IIF(LEN(cCnpj)>11,"@E 99.999.999/9999-99","@E 999.999.999-99")

//	@ 049,004 SAY "Limite Credito :"	SIZE 060,08 OF oDl2g PIXEL
//	@ 057,004 MSGET oLC		VAR nVallim  When .f. SIZE 060,08 OF oDl2g PIXEL Picture "@E 999,999,999.99"
//	@ 049,200 Say  "Data Limite  :"   SIZE 060,08 OF oDl2g PIXEL  COLOR CLR_HBLUE
//	@ 057,200 MSGET oDLC 	VAR dDtaLim	  When .f.	 SIZE 060,08 OF oDl2g PIXEL Picture "@D 99/99/99"

	@ 035,004 LISTBOX oLtBx3 VAR cListBox3 Fields HEADER OEMTOANSI(aTitulo3[1]),OEMTOANSI(aTitulo3[2]),OEMTOANSI(aTitulo3[3]),OEMTOANSI(aTitulo3[4]),OEMTOANSI(aTitulo3[5]),OEMTOANSI(aTitulo3[6]),OEMTOANSI(aTitulo3[7]),OEMTOANSI(aTitulo3[8]),OEMTOANSI(aTitulo3[9]),OEMTOANSI(aTitulo3[10]),OEMTOANSI(aTitulo3[11]),OEMTOANSI(aTitulo3[12]),OEMTOANSI(aTitulo3[13]),OEMTOANSI(aTitulo3[14]) FIELDSIZES 14, GetTextWidth(0,"BB"), GetTextWidth(0,"BBBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBBBBBBBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBBBBBBBB"), GetTextWidth(0,"BBBBBBBB"), GetTextWidth(0,"BBBBBBBB"), GetTextWidth(0,"BBBBBBB"), GetTextWidth(0,"BBBBBBB"),GetTextWidth(0,"BBBBBBB"),	GetTextWidth(0,"BBBBBBB"), GetTextWidth(0,"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB") SIZE 460,100 SCROLL PIXEL 
    @ 150,020 Say "Qtd No Prazo  "      SIZE 060,12 OF oDl3g PIXEL  COLOR CLR_RED
    @ 150,105 MSGET onQtdPDia VAR nQtdPDia When .f. SIZE 060,08 OF oDl3g PIXEL Picture "@E 999,999"
    @ 150,220 Say "Vlr no Prazo  "      SIZE 060,12 OF oDl3g PIXEL  COLOR CLR_RED
    @ 150,305 MSGET onVlrPDia VAR nVlrPDia When .f. SIZE 060,08 OF oDl3g PIXEL Picture "@E 999,999,999.99"
    @ 170,020 Say "Qtd Em Atraso "      SIZE 060,12 OF oDl3g PIXEL  COLOR CLR_RED
    @ 170,105 MSGET onQtdPAtr VAR nQtdPAtr When .f. SIZE 060,08 OF oDl3g PIXEL Picture "@E 999,9999"
    @ 170,220 Say "Vlr Em Atraso "      SIZE 060,12 OF oDl3g PIXEL  COLOR CLR_RED
    @ 170,305 MSGET onVlrPAtr VAR nVlrPAtr When .f. SIZE 060,08 OF oDl3g PIXEL Picture "@E 999,999,999.99"
    @ 190,020 Say "Ultimo Pagto Em "  SIZE 060,12 OF oDl3g PIXEL  COLOR CLR_RED
    @ 190,105 MSGET odUlPgato VAR dUlPgato When .f. SIZE 060,08 OF oDl3g PIXEL  //Picture "@E 999,999,999.99"
    @ 190,220 Say "Vlr Ultimo Pgto."  SIZE 060,12 OF oDl3g PIXEL  COLOR CLR_RED
    @ 190,305 MSGET onUlPagto VAR nUlPagto When .f. SIZE 060,08 OF oDl3g PIXEL Picture "@E 999,999,999.99"
    @ 210,020 Say "Maior Atraso/Dias "  SIZE 090,12 OF oDl3g PIXEL  COLOR CLR_RED
    @ 210,105 MSGET onMairAtr VAR nMairAtr When .f. SIZE 060,08 OF oDl3g PIXEL Picture "@E 999,999"
//    @ 210,220 Say "Vlr Ult. Vencimento"  SIZE 060,12 OF oDl2g PIXEL  COLOR CLR_RED
 //   @ 210,305 MSGET onUlVenc VAR nUlVenc When .f. SIZE 060,08 OF oDl2g PIXEL Picture "@E 999,999,999.99"
	
	/*RBM 
	 @ 210,220 Say "Total - NCC"  SIZE 060,12 OF oDl3g PIXEL  COLOR CLR_RED
	 @ 210,305 MSGET onMairAtr VAR nMairAtr When .f. SIZE 060,08 OF oDl3g PIXEL Picture "@E 999,999"
   */
   
	@ 230,020 SAY "Qtd. T�tulos Pagos:  " SIZE 060,12 OF oDl3g PIXEL  COLOR CLR_RED	 
	@ 230,105 MSGET onQtdDup VAR nQtdDup  When .f. SIZE 060,08 OF oDl3g PIXEL Picture "@E 999,999"
	@ 230,220 SAY "Total T�tulos Pagos: " SIZE 060,12 OF oDl3g PIXEL  COLOR CLR_RED	 
	@ 230,305 MSGET onvlTot VAR nvlTot  When .f. SIZE 060,08 OF oDl3g PIXEL Picture "@E 999,999,999.99"
	
	oLtBx3:SetArray(aList3)
    Define SButton  From 245,400 Type 1 Action oDl3g:End() Enable Of oDl3g Pixel //Apaga
	oLtBx3:bLine 		:= {|| RCFATLIS3(@oLtBx3)}
	oLtBx3:blDblClick 	:= {|| xPosFin(aList3,oLtBx3:nAt,lRet,nChamada,SA1->A1_COD,SA1->A1_LOJA)}   
	ACTIVATE MSDIALOG oDl3g Centered //ON INIT EnchoiceBar(oDl2g,bOK,,,aBotao)
	SetKey(12,NIL)
	SetKey(16,NIL)
	SetKey(22,NIL)
	RestArea(aArea)
Return(lRet)


Static Function xPosFin(aList2,nLinha,lRet,nChamada,cCliente,cLoja)
	Local aArea      := GetArea()
	Local nMCusto    := 0
	Local cFilAnt    := SM0->M0_CODFIL
	Local cEmpAnt    := SM0->M0_CODIGO
	Private aCpos := {"A1_COD","A1_LOJA","A1_NOME","A1_END", "A1_EST"}//aCpos  := {"A1_COD","A1_NOME"}
	Private nOpc     := 0  
	Private nCayas   := 2 //SuperGetMV("MV_CENT")			//Conteudo do paramentro
	Private Inclui   := .F.						//Se inclui registro
	Private Altera   := .T.						//Se altera registro 

	//Private cMoeda   := " "+Pad(SuperGetMV("MV_SIMB"+ALLTRIM(STR(nMCusto))),4)


	DbSelectArea("SA1")
	SA1->(DbSetorder(1))
//	if MsgYesNo("Confirma Pesquizar "+cCliente +"/"+ cLoja)
	If SA1->(DbSeek(xFilial("SA1") + Alltrim(cCliente) + alltrim(cLoja)))
		//��������������������������������������������������������������Ŀ
		//�Caso o Pergunte seja confirmado executa a consulta financeira.�
		//����������������������������������������������������������������
//		msgStop('Em desenvolvimento... achei  '+cCliente+'/'+cLoja )
		//AxAltera("SA1",SA1->(Recno()),2,,aCpos)
		//AxVisual("SA1",SA1->(Recno()),2,,aCpos)
		SA1->(AxVisual("SA1",SA1->(Recno()),2))
		//A030Altera()
		//MBrowseAuto(4,aRotAuto,"SA1")
		
		
	Else
		msgStop('Cliente N�o encontrado!')
	Endif    
//	Endif	
	RestArea(aArea)
Return 

Static Function XFc010Con(cAlias,nRecno,nOpcx)   // (n�o esta chamando esta fun��o)
	Local lPanelFin := If (FindFunction("IsPanelFin"),IsPanelFin(),.F.)
	Local aParam := {}
	Local lPergunte:= .F.
	Private Inclui := .F.
	Private Altera := .F.

	If lPanelFin
		lPergunte := PergInPanel("FIC010",.T.)
	Else
		lPergunte := Pergunte("FIC010",FunName()=="FINC010")		
	Endif

	If lPergunte .Or. FunName()<>"FINC010"

		aadd(aParam,MV_PAR01)
		aadd(aParam,MV_PAR02)
		aadd(aParam,MV_PAR03)
		aadd(aParam,MV_PAR04)
		aadd(aParam,MV_PAR05)
		aadd(aParam,MV_PAR06)
		aadd(aParam,MV_PAR07)
		aadd(aParam,MV_PAR08)
		aadd(aParam,MV_PAR09)
		aadd(aParam,MV_PAR10)
		aadd(aParam,MV_PAR11)
		aadd(aParam,MV_PAR12)
		aadd(aParam,MV_PAR13)
		aadd(aParam,MV_PAR14)
		aadd(aParam,MV_PAR15)
		aadd(aParam,MV_PAR16)

		If VerSenha(104) //Permite consulta a Posicao de Cliente
			xFc010Cli(aParam)
		Else
			Help(" ",1,"SEMPERM")
		EndIf
	EndIf
Return(Nil)

Static Function xFc010Cli(aParam)

	Local aArea 	:= GetArea()
	Local aAlias	:= {}
	Local oDlg
	Local cCadastro := "Consulta Posi��o Clientes"
	Local cCgc		:= RetTitle("A1_CGC")
	Local cMoeda    := ""
	Local nMcusto   := Iif(SA1->A1_MOEDALC > 0, SA1->A1_MOEDALC, Val(GetMv("MV_MCUSTO")))         
	Local aSavAhead := If(Type("aHeader")=="A",aHeader,{})
	Local aSavAcol  := If(Type("aCols")=="A",aCols,{})
	Local nSavN     := If(Type("N")=="N",n,0)
	Local nConLin   := 0
	Local aCols     :={}
	Local aHeader   :={}
	Local cSalFin	:=""
	Local cLcFin	:=""
	Local aGet      := {"","","",""}
	Local cTelefone := Alltrim(SA1->A1_DDI+" "+SA1->A1_DDD+" "+SA1->A1_TEL)
	Local lFc010Bol := ExistBlock( "FC010BOL" )
	Local lSigaGE   := GetMv("MV_ACATIVO") //Integracao Gestao Educacional
	Local dPRICOM   := CRIAVAR("A1_PRICOM",.F.) 
	Local dULTCOM   := CRIAVAR("A1_ULTCOM",.F.) 
	Local dDTULCHQ  := CRIAVAR("A1_DTULCHQ",.F.)
	Local dDTULTIT  := CRIAVAR("A1_DTULTIT",.F.)
	Local lhabili   := .F.          
	Local cRISCO    := ""
	Local nLC       := 0
	Local nSALDUP   := 0
	Local nSALDUPM  := 0
	Local nLCFIN    := 0
	Local nMATR     := 0
	Local nSALFIN   := 0
	Local nSALFINM  := 0
	Local nMETR     := 0
	Local nMCOMPRA  := 0
	Local nMSALDO   := 0          
	Local nCHQDEVO  := 0
	Local nTITPROT  := 0
	Local lFC010LIST := ExistBlock("FC010LIST")
	Local lNoChqTran := .T.		//Controla processo se n�o for somente cheque transitorio 
	lFc010ConT      := .f. //If(lFc010ConT==Nil,ExistTemplate("xFC010CON"),lxFc010ConT)
	lxFc010Con       := .f. //If(lFc010Con==Nil,ExistBlock("xFC010CON"),lxFc010Con) 

	SX3->(DbSetOrder(2))
	cLcFin	:=	If(SX3->(DbSeek("A1_LCFIN")) ,X3Titulo(),STR0076)
	cSalFin  := If(SX3->(DbSeek("A1_SALFIN")),X3Titulo(),STR0075)
	cMoeda  		:= " "+Pad(Getmv("MV_SIMB"+Alltrim(STR(nMCusto))),4)
	lFc010ConT      := If(lFc010ConT==Nil,ExistTemplate("xFC010CON"),lFc010ConT)
	lxFc010Con       := If(lxFc010Con==Nil,ExistBlock("xFC010CON"),lxFc010Con) 
	Private nCayas :=  2 //GetMv("MV_CENT")

	If aParam[13] == 1
		nLC      := SA1->A1_LC
		dPRICOM  := SA1->A1_PRICOM
		nSALDUP  := SA1->A1_SALDUP
		nSALDUPM := SA1->A1_SALDUPM
		dULTCOM  := SA1->A1_ULTCOM       
		nLCFIN   := SA1->A1_LCFIN       
		nMATR    := SA1->A1_MATR                 
		nSALFIN  := SA1->A1_SALFIN  
		nSALFINM := SA1->A1_SALFINM              
		nMETR    := SA1->A1_METR 
		nMCOMPRA := SA1->A1_MCOMPRA        
		cRISCO   := SA1->A1_RISCO
		nMSALDO  := SA1->A1_MSALDO
		nCHQDEVO := SA1->A1_CHQDEVO
		dDTULCHQ := SA1->A1_DTULCHQ
		nTITPROT := SA1->A1_TITPROT
		dDTULTIT := SA1->A1_DTULTIT
	Else
		Fc010Loja(@nLC,@dPRICOM,@nSALDUP,@nSALDUPM,@dULTCOM,@nLCFIN,@nMATR,@nSALFIN,@nSALFINM,@nMETR,@nMCOMPRA,@cRISCO,@nMSALDO,@nCHQDEVO,@dDTULCHQ,@nTITPROT,@dDTULTIT)
	Endif

	aHeader	:= {STR0077,STR0078,STR0069+AllTrim(cMoeda)," ",STR0077,STR0005}  
	//LIMITE DE CREDITO # PRIMEIRA COMPRA
	Aadd(aCols,{STR0014,TRansform(Round(Noround(xMoeda(nLC, nMcusto, 1,dDataBase,MsDecimais(1)+1),2),MsDecimais(1)),PesqPict("SA1","A1_LC",14,1)),;
	TRansform(nLC,PesqPict("SA1","A1_LC",14,nMCusto)),;
	" ",if(lSigaGE,STR0111,STR0015),SPACE(07)+DtoC(dPRICOM)}) // LIMITE DE CREDITO # Primeira Parcela / Primeira Compra 
	//SALDO # ULTIMA COMPRA
	Aadd(aCols,{if(lSigaGE,STR0109,STR0010),TRansform(nSALDUP,PesqPict("SA1","A1_SALDUP",14,1) ),;
	TRansform(nSALDUPM,PesqPict("SA1","A1_SALDUPM",14,nMcusto)),;
	" ",if(lSigaGE,STR0112,STR0016),SPACE(07)+DtoC(dULTCOM)}) // Valor Parcelas / Saldo  / Ultima Parcela / Ultima Compra
	//Limite de credito secundario # MAIOR ATRASO
	Aadd(aCols,{cLcFin,TRansform(Round(Noround(xMoeda(nLCFIN,nMcusto,1,dDatabase,MsDecimais(1)+1),2),MsDecimais(1)),PesqPict("SA1","A1_LCFIN",14,1)),;     
	TRansform(nLCFIN,PesqPict("SA1","A1_LCFIN",14,nMcusto)),;
	" ",STR0017,Transform(nMATR,PesqPict("SA1","A1_MATR",14))}) // Limite sec / Maior Atraso    
	//SAldo do limite de credito secundario $ media de Atraso
	Aadd(aCols,{cSalFin,TRansform(nSALFIN,PesqPict("SA1","A1_SALFIN",14,1)),;
	TRansform(nSALFINM,PesqPict("SA1","A1_SALFINM",14,nMcusto)),;
	" ",STR0018,PADC(STR(nMETR,7,2),22)}) // Saldo em Cheque / Media de Atraso
	//Maior Compra # Grau de risco
	Aadd(aCols,{if(lSigaGE,STR0110,STR0011),;
	TRansform(Round(Noround(xMoeda(nMCOMPRA, nMcusto ,1, dDataBase,MsDecimais(1)+1),2),MsDecimais(1)),PesqPict("SA1","A1_MCOMPRA",14,1) ) ,;
	TRansform(nMCOMPRA,PesqPict("SA1","A1_MCOMPRA",14,nMcusto)),;
	" ",STR0019,SPACE(25)+cRISCO}) // Maior Compra / Grau de Risco
	//MAior Saldo
	Aadd(aCols,{STR0012,;
	TRansform(Round(Noround(xMoeda(nMSALDO, nMcusto ,1, dDataBase,MsDecimais(1)+1 ),2),MsDecimais(1)),PesqPict("SA1","A1_MSALDO",14,1)),;
	TRansform(nMSALDO,PesqPict("SA1","A1_MSALDO",14,nMcusto)),;
	" "," ",""}) //Maior saldo


	IF lFC010LIST

		aCols := ExecBlock ("FC010LIST",.F.,.F.,aCols) //Leandro

	EndIF

	DEFINE MSDIALOG oDlg FROM	09,0 TO 30,85 TITLE cCadastro OF oMainWnd

	@ 001,002 TO 043, 267 OF oDlg	PIXEL
	@ 130,002 TO 154, 114 OF oDlg	PIXEL
	@ 130,121 TO 154, 267 OF oDlg	PIXEL

	If lSigaGE

		DbSelectArea("JA2")
		DbSetOrder( 5 )
		dbSeek(xFilial("JA2")+SA1->A1_COD+SA1->A1_LOJA)

		@ 004,005 SAY STR0108 SIZE 050,07          OF oDlg PIXEL	// Registro Academico
		@ 012,004 GET JA2->JA2_NUMRA 		SIZE 075,09 WHEN .F. OF oDlg PIXEL

	Else

		@ 004,005 SAY STR0006 SIZE 025,07          OF oDlg PIXEL //"Codigo"
		@ 012,004 MSGET SA1->A1_COD      SIZE 070,09 WHEN .F. OF oDlg PIXEL

		If aParam[13] == 1  //Considera loja		
			@ 004,077 SAY STR0007 SIZE 020,07          OF oDlg PIXEL //"Loja"
			@ 012,077 MSGET SA1->A1_LOJA     SIZE 021,09 WHEN .F. OF oDlg PIXEL
		Endif

	EndIf

	@ 004,100 SAY STR0008 SIZE 025,07 OF oDlg PIXEL //"Nome"
	@ 012,100 MSGET SA1->A1_NOME     SIZE 150,09 WHEN .F. OF oDlg PIXEL

	@ 023,005 SAY cCGC    SIZE 025,07 OF oDlg PIXEL
	If cPaisLoc=="BRA"
		@ 030,004 MSGET SA1->A1_CGC      SIZE 070,09 PICTURE StrTran(PicPes(SA1->A1_PESSOA),"%C","") WHEN .F. OF oDlg PIXEL
	Else
		@ 030,004 MSGET SA1->A1_CGC      SIZE 070,09 PICTURE PesqPict("SA1","A1_CGC") WHEN .F. OF oDlg PIXEL

	EndIf
	@ 023,077 SAY STR0009 SIZE 025,07 OF oDlg PIXEL //"Telefone"
	@ 030,077 MSGET cTelefone	       SIZE 060,09 WHEN .F. OF oDlg PIXEL

	@ 023,141 SAY RetTitle("A1_VENCLC")  SIZE 035,07 OF oDlg PIXEL
	@ 030,141 MSGET SA1->A1_VENCLC       SIZE 060,09 WHEN .F. OF oDlg PIXEL

	If ! lSigaGE

		@ 023,206 SAY STR0057 SIZE 035,07 OF oDlg PIXEL //"Vendedor"
		@ 030,206 MSGET SA1->A1_VEND  	 SIZE 053,09 WHEN .F. OF oDlg PIXEL

	EndIf

	oLbx := RDListBox(3.5, .42, 264, 70, aCols, aHeader,{38,51,51,11,50,63})   

	@ 124,002 SAY STR0020 SIZE 061,07 OF oDlg PIXEL //"Cheques Devolvidos"
	@ 124,121 SAY STR0021 SIZE 061,07 OF oDlg PIXEL //"Titulos Protestados"

	@ 133,005 SAY STR0022 SIZE 034,07 OF oDlg PIXEL //"Quantidade"
	@ 133,045 SAY STR0023 SIZE 066,07 OF oDlg PIXEL //"Ultimo Devolvido"
	@ 133,126 SAY STR0022 SIZE 034,07 OF oDlg PIXEL //"Quantidade"
	@ 133,163 SAY STR0024 SIZE 076,07 OF oDlg PIXEL //"Ultimo Protesto"

	@ 141,006 MSGET nCHQDEVO SIZE 024,08 WHEN .F. OF oDlg PIXEL
	@ 141,045 MSGET dDTULCHQ SIZE 050,08 WHEN .F. OF oDlg PIXEL
	@ 141,126 MSGET nTITPROT SIZE 024,08 WHEN .F. OF oDlg PIXEL
	@ 141,163 MSGET dDTULTIT SIZE 050,08 WHEN .F. OF oDlg PIXEL

	@ 001,272 BUTTON Iif(lSigaGE,STR0105,STR0025) SIZE 60,12 FONT oDlg:oFont ACTION If( lFc010Bol, ExecBlock( "FC010BOL", .F., .F., {1,@aAlias,aParam,.T.,aGet} ), xFc010Bro(1,@aAlias,aParam,.T.,aGet))  OF oDlg PIXEL //"Boleto Aberto" / "Tit Aberto"
	@ 015,272 BUTTON Iif(lSigaGE,"Boleto Pago","Tit Recebidos") SIZE 60,12 FONT oDlg:oFont ACTION If( lFc010Bol, ExecBlock( "FC010BOL", .F., .F., {2,@aAlias,aParam,.T.,aGet} ), xFC010Bro(2,@aAlias,aParam,.T.,aGet))  OF oDlg PIXEL //"Boleto Pago" / "Tit Recebidos"

	If ! lSigaGE
		@ 029,272 BUTTON STR0027 SIZE 60,12 FONT oDlg:oFont ACTION xFc010Bro(3,@aAlias,aParam,.T.,aGet)	OF oDlg PIXEL //"Pedidos"
		@ 043,272 BUTTON STR0028 SIZE 60,12 FONT oDlg:oFont ACTION xFc010Bro(4,@aAlias,aParam,.T.,aGet)	OF oDlg PIXEL //"Faturamento"
		nConLin+=14
	Else
		@ 029,272 BUTTON STR0107 SIZE 60,12 ACTION ACAR590(JA2->JA2_NUMRA) OF oDlg PIXEL  // "Extrato"
	EndIf

	If ( cPaisLoc<>"BRA" )
		@ 043+nConLin,272 BUTTON STR0029 SIZE 60,12 FONT oDlg:oFont ACTION xFc010Bro(5,@aAlias,aParam,.T.,aGet)	OF oDlg PIXEL //"Cheques/Trans"
		nConLin+=14
	EndIf
	If ( lxFC010ConT )
		@ 043+nConLin,272 BUTTON STR0030 SIZE 60,12 FONT oDlg:oFont ACTION ExecTemplate("xFC010CON",.F.,.F.) OF oDlg  PIXEL //"Cons.Especif."
		nConLin+=14
	EndIf
	If ( lxFC010Con )
		@ 043+nConLin,272 BUTTON STR0030 SIZE 60,12 FONT oDlg:oFont ACTION ExecBlock("xFC010CON",.F.,.F.) OF oDlg  PIXEL //"Cons.Especif."
		nConLin+=14
	EndIf
	If Trim(GetMV("MV_VEICULO")) == "S"
		@ 043+nConLin,272 BUTTON STR0058 SIZE 60,12 FONT oDlg:oFont ACTION FG_SALOSV(SA1->A1_COD,SA1->A1_LOJA,," ","T") OF oDlg PIXEL  //"OSs em Aberto"
		nConLin+=14
	EndIf   
	If aParam[13] == 1  //Considera loja
		lhabili := .T.
	Endif
	@ 043+nConLin,272 BUTTON STR0059 SIZE 60,12 FONT oDlg:oFont ACTION Mata030Ref("SA1",SA1->(Recno()),2) WHEN lhabili OF oDlg PIXEL //"Referencias"
	nConLin+=14

	//��������������������������������������������������������������������������������������������������������Ŀ
	//�O modulo de call center n�o esta disponivel em Pyme, portanto nao exibe o botao do historico de cobranca�
	//����������������������������������������������������������������������������������������������������������
	If !__lPyme 
		@ 043+nConLin,272 BUTTON STR0095 SIZE 60,12 FONT oDlg:oFont ACTION TmkC020() WHEN lhabili OF oDlg PIXEL //Historico de Cobranca
		nConLin += 14
	Endif	

	//��������������������������������������������������������������������������������������������������������Ŀ
	//�O Ecossistema nao esta disponivel em Pyme, portanto nao exibe o botao de consulta a credito             �
	//����������������������������������������������������������������������������������������������������������
	If !__lPyme 
		@ 043 + nConLin ,272 BUTTON STR0114 SIZE 60,12 FONT oDlg:oFont ACTION Fc010Eco() MESSAGE STR0115 WHEN lhabili OF oDlg PIXEL // "Credito", "Ecossistema - Consulta a credito" 
	Endif	

	//���������������������������������������������������������������Ŀ
	//�Ponto de entrada para inclus�o de um bot�o customizado na tela �
	//�����������������������������������������������������������������
	If ExistBlock("FC010BTN")	
		If !__lPyme
			nConLin += 14 
			@ 043+nConLin,272 BUTTON ExecBlock("FC010BTN",.F.,.F.,{1}) SIZE 60,12 FONT oDlg:oFont ACTION ExecBlock("FC010BTN",.F.,.F.,{3}) MESSAGE ExecBlock("FC010BTN",.F.,.F.,{2})  OF oDlg PIXEL // "Botao habilitado pelo cliente mediante ponto de entrada" 
		Endif		
	EndIf 

	@ 143,272 BUTTON STR0031 SIZE 60,12 FONT oDlg:oFont ACTION oDlg:End() 	OF oDlg PIXEL //"Sair"
	ACTIVATE MSDIALOG oDlg CENTERED


	//������������������������������������������������������������������������Ŀ
	//�Restaura a Integridade dos Dados                                        �
	//��������������������������������������������������������������������������
	aHeader := aSavAHead
	aCols   := aSavaCol
	N       := nSavN
	aEval(aAlias,{|x| (x[1])->(dbCloseArea()),Ferase(x[2]+GetDBExtension()),Ferase(x[2]+OrdBagExt())})
	dbSelectArea("SA1")
	RestArea(aArea)

Return(.T.)

Static Function xFc010Bro(nBrowse,aAlias,aParam,lExibe,aGet,lRelat)
	Local aArea		:= GetArea()
	Local aAreaSC5	:= SC5->(GetArea())
	Local aAreaSC6	:= SC6->(GetArea())
	Local aAreaSC9	:= SC9->(GetArea())
	Local aAreaSF4	:= SF4->(GetArea())
	Local aStru		:= {}
	Local aQuery	:= {}
	Local aSay		:= {"","","","","","","",""}
	Local oGetDb
	Local oScrPanel
	Local oBold
	Local oDlg
	Local oBtn
	Local bVisual
	Local bWhile
	Local bFiltro
	Local cAlias	:= ""
	Local cArquivo	:= ""
	Local cCadastro	:= ""
	#IFDEF TOP
	Local cQuery	:= ""
	Local cDbMs
	#ENDIF	
	Local cQry		:= ""
	Local cChave	:= ""
	Local lQuery	:= .F.
	Local nCntFor	:= 0
	Local nSalped	:= 0
	Local nSalpedl	:= 0
	Local nSalpedb	:= 0
	Local nQtdPed	:= 0
	Local nTotAbat	:= 0
	Local cAnterior	:= ""
	Local nTaxaM	:= 0	
	Local nMoeda
	Local oTipo
	Local nTipo		:= 1
	Local bTipo
	Local oCheq
	Local aTotRec	:= {{0,1,0,0}} // Totalizador de titulos a receber por por moeda
	Local aTotPag	:= {{0,1,0,0}} // Totalizador de titulos recebidos por por moeda
	Local nAscan
	Local nTotalRec	:=0
	Local aSize		:= MsAdvSize( .F. )
	Local aPosObj1	:= {}                 
	Local aObjects	:= {}                       
	Local aCpos		:= {}
	Local cCheques	:=	IIF(Type('MVCHEQUES')=='C',MVCHEQUES,MVCHEQUE)
	Local nI
	Local lPosClFt	:= (SuperGetMv("MV_POSCLFT",.F.,"N") == "S")
	Local bCond
	Local cOrdem	:= ""
	Local cNumAnt	:= ""
	Local cTpDocAnt	:= ""
	Local cParcAnt	:= ""
	Local lFC010Head := ExistBlock("FC010HEAD")
	Local cSaldo := ""
	Local lFC010Pedi := ExistBlock("FC010Pedi")
	Local aRetAux	:= {}
	Local lFC0101FAT	:= ExistBlock("FC0101FAT")
	Local lFC0102FAT	:= ExistBlock("FC0102FAT")
	Local lFC0103FAT	:= ExistBlock("FC0103FAT")
	Local aAuxCpo		:= {}
	Local aHeader1		:=	{}
	Local nA				:=	0
	Local nMulta		:= 0                            //Valor da Multa
	Local cMvJurTipo 	:= SuperGetMv("MV_JURTIPO",,"") //Tipo de Calculo de Juros do Financeiro	
	Local lLojxRMul  	:= FindFunction("LojxRMul")        //Funcao que calcula a Multa do Financeiro
	Local lMvLjIntFS   := SuperGetMv("MV_LJINTFS", ,.F.) //Habilita Integra��o com o Financial Services
	Private aHeader	:= {}
	DEFAULT lRelat	:= .F.          


	aGet := {"","","","","","","",""}

	Do Case
		Case ( nBrowse == 1 )	
		cCadastro := STR0025
		cAlias    := "FC010QRY01"
		aSay[1]   := STR0032 //"Qtd.Tit."
		aSay[2]   := STR0033 //"Principal"
		aSay[3]   := "Saldo a Receber" //STR0120 
		aSay[4]   := STR0046 //"Juros"
		aSay[5]   := STR0091 //"Acresc."
		aSay[6]   := STR0092 //"Decresc."
		aSay[7]   := STR0086 //"Abatimentos"
		aSay[8]   := STR0104 //"Tot.Geral"	
		bVisual   := {|| _Fc010Visua((cAlias)->XX_RECNO,nBrowse) }
		Case ( nBrowse == 2 )
		cCadastro := 'STR0122'
		cAlias    := "FC010QRY02"
		aSay[1]   := STR0036 //"Qtd.Pag."
		aSay[2]   := STR0037 //"Principal"
		aSay[3]   := STR0038 //"Vlr.Pagto"
		bVisual   := {|| _Fc010Visua((cAlias)->XX_RECNO,nBrowse) }
		Case ( nBrowse == 3 )
		cCadastro := STR0027
		cAlias    := "FC010QRY03"
		aSay[1]   := STR0039 //"Qtd.Ped."
		aSay[2]   := STR0040 //"Tot.Pedido"
		aSay[3]   := STR0041 //"Tot.Liber."
		aSay[4]   := STR0042 //"Sld.Pedido"
		bVisual   := {|| _Fc010Visua((cAlias)->XX_RECNO,nBrowse) }
		Case ( nBrowse == 4 )
		cCadastro := STR0028
		cAlias    := "FC010QRY04"
		aSay[1]   := STR0043 //"Qtd.Notas"
		aSay[2]   := STR0044 //"Tot.Fatur."
		bVisual   := {|| _Fc010Visua((cAlias)->XX_RECNO,nBrowse) }
		Case ( nBrowse == 5 )
		cCadastro := STR0061 //"Cartera de cheques"
		cAlias    := "FC010QRY05"
		aSay[1]   := STR0062 //"Pendiente"
		If cPaisLoc$"URU|BOL"
			aSay[2]   := STR0088 //"Rechazado"
		Else
			aSay[2]   := STR0063 //"Negociado"
		Endif   
		aSay[3]   := STR0064 //"Cobrado"
		If cPaisLoc$"ARG|COS"
			aSay[4]   := "Em Transito" // STR0134
		Endif
		bVisual   := {|| _Fc010Visua((cAlias)->XX_RECNO,If( (cAlias)->XX_ESTADO == "TRAN",5,1) ) }

	EndCase

	Do Case
		//������������������������������������������������������������������������Ŀ
		//�Titulo em Aberto                                                        �
		//��������������������������������������������������������������������������
		Case ( nBrowse == 1 )
		If !lRelat
			Aadd(aHeader,{"",	"XX_LEGEND","@BMP",10,0,"","","C","",""})
			Aadd(aStru,{"XX_LEGEND","C",12,0})
		Endif	
		dbSelectArea("SX3")
		dbSetOrder(2)
		dbSeek("E1_LOJA")
		If aParam[13] == 2  //Considera loja		
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		Endif
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_PREFIXO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_NUM")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_PARCELA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_TIPO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_CLIENTE")
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_EMISSAO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_VENCTO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_BAIXA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_VENCREA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		If !lRelat
			dbSeek("E1_MOEDA")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )

			dbSeek("E1_VALOR")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		Endif	

		dbSeek("E1_VLCRUZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aadd(aHeader,{STR0086,"E1_ABT","@E 999,999,999.99",14,2,"","","N","","V" } ) //"Abatimentos"
		aadd(aStru ,{"E1_ABT","N",14,2})

		dbSeek("E1_SDACRES")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_SDDECRE")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_VALJUR")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		//�������������������������������������������������������������Ŀ
		//� 11/Ago/2005 Rafael E. Rodrigues                             �
		//� Quando o modulo Gestao Educacional estiver em uso, inclui a �
		//� coluna com a provisao de multa de titulos em atraso.        �
		//���������������������������������������������������������������
		If GetNewPar( "MV_ACATIVO", .F. )
			dbSeek("E1_VLMULTA")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL}) 

			dbSeek("E1_MULTA")		
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Else
			//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Inicio
			/*BEGINDOC
			//�����������������������������������������������������Ŀ
			//�Calculo de juros e multas, segundo o controle de loja�
			//�������������������������������������������������������
			ENDDOC*/
			If (cMVJurTipo == "L" .OR. lMvLjIntFS).AND. lLojxRMul
				dbSeek("E1_MULTA")		
				aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
				aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
				aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			EndIf  
			//Calculo de Juros e Multas: SIGALOJA x SIGAFIN - Final
		Endif   

		//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Inicio	
		/*BEGINDOC
		//�����������������������������������������������������Ŀ
		//�Calculo de juros e multas, segundo o controle de loja�
		//�������������������������������������������������������
		ENDDOC*/
		If (cMVJurTipo == "L" .OR. lMvLjIntFs) .and. lLojxRMul
			dbSeek("E1_ACRES")		
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

			dbSeek("E1_JUROS")		
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		EndIf 
		//Calculo de Juros e Multas: SIGALOJA x SIGAFIN - Final

		dbSeek("E1_SALDO")
		aadd(aHeader,{"Saldo a Receber" ,SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )	//"Saldo a Receber"
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aadd(aHeader,{ STR0103,"E1_SALDO2",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } ) // "Saldo na moeda tit"
		aadd(aStru ,{"E1_SALDO2",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_NATUREZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_PORTADO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})		

		dbSeek("E1_NUMBCO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		If !lRelat
			dbSeek("E1_NUMLIQ")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif

		dbSeek("E1_HIST")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aadd(aHeader,{STR0035,"E1_ATR","9999999999",10,0,"","","N","","V" } ) //"Atraso"
		aadd(aStru ,{"E1_ATR","N",10,0})

		If !lRelat
			dbSeek("E1_CHQDEV")
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif

		#ifdef SPANISH
		Aadd(aStru,{"X5_DESCSPA","C",25,0})
		#else
		#ifdef ENGLISH
		Aadd(aStru,{"X5_DESCENG","C",25,0})			
		#else
		Aadd(aStru,{"X5_DESCRI","C",25,0})
		#endif
		#endif

		aadd(aStru,{"XX_RECNO","N",12,0})
		aadd(aStru,{"E1_MOEDA","N",02,0})

		aadd(aQuery,{"E1_PORCJUR","N",12,4})
		aadd(aQuery,{"E1_MOEDA","N",02,0})
		aadd(aQuery,{"E1_VALOR","N",16,2})

		If cPaisLoc == "BRA"
			aadd(aQuery,{"E1_TXMOEDA","N",17,4})
		Endif	

		#ifdef SPANISH
		Aadd(aHeader,{STR0045,"X5_DESCSPA","@!",25,0,"","","C","SX5","" } ) //"Situacao"					
		#else
		#ifdef ENGLISH
		Aadd(aHeader,{STR0045,"X5_DESCENG","@!",25,0,"","","C","SX5","" } ) //"Situacao"								
		#else
		Aadd(aHeader,{STR0045,"X5_DESCRI","@!",25,0,"","","C","SX5","" } ) //"Situacao"
		#endif
		#endif             

		IF lFC010Head
			aHeader1 :=	aHeader			
			aHeader 	:=	ExecBlock("FC010HEAD",.F.,.F.,aHeader)							

			For nA:=1 to Len(aHeader)
				If Ascan(aHeader1,{|e| e[2] = aHeader[nA,2]}) = 0//Campo nao estava no header incluir tambem nestes vetores.         
					aadd(aStru ,{aHeader[nA,2],aHeader[nA,8],aHeader[nA,4],aHeader[nA,5]})
					aadd(aQuery,{aHeader[nA,2],aHeader[nA,8],aHeader[nA,4],aHeader[nA,5]})												
				Endif					
			Next										
		EndIf

		SX3->(dbSetOrder(1))

		If ( Select(cAlias) ==	0 )
			cArquivo := CriaTrab(,.F.)			
			aadd(aAlias,{ cAlias , cArquivo })
			aadd(aStru,{"FLAG","L",01,0})
			dbCreate(cArquivo,aStru)
			dbUseArea(.T.,,cArquivo,cAlias,.F.,.F.)
			IndRegua(cAlias,cArquivo,"E1_CLIENTE+E1_LOJA+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO")

			#IFDEF TOP
			If ( TcSrvType()!="AS/400" )
				lQuery := .T.
				cQuery := ""
				aEval(aQuery,{|x| cQuery += ","+AllTrim(x[1])})
				cQuery := "SELECT "+SubStr(cQuery,2)
				cQuery +=         ",SE1.R_E_C_N_O_ SE1RECNO"

				#ifdef SPANISH
				cQuery += ",SX5.X5_DESCSPA "
				#else
				#ifdef ENGLISH
				cQuery += ",SX5.X5_DESCENG "				
				#else
				cQuery += ",SX5.X5_DESCRI "								
				#endif
				#endif

				cQuery += "FROM "+RetSqlName("SE1")+" SE1,"
				cQuery +=         RetSqlName("SX5")+" SX5 "
				cQuery += "WHERE SE1.E1_FILIAL='"+xFilial("SE1")+"' AND "
				cQuery +=       "SE1.E1_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=       "SE1.E1_LOJA='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=       "SE1.E1_EMISSAO>='"+Dtos(aParam[1])+"' AND "
				cQuery +=       "SE1.E1_EMISSAO<='"+Dtos(aParam[2])+"' AND "
				cQuery +=       "SE1.E1_VENCREA>='"+Dtos(aParam[3])+"' AND "
				cQuery +=       "SE1.E1_VENCREA<='"+Dtos(aParam[4])+"' AND "
				If ( aParam[5] == 2 )
					cQuery +=   "SE1.E1_TIPO<>'PR ' AND "
				EndIf					
				If ( aParam[15] == 2 )
					cQuery +=   "SE1.E1_TIPO<>'RA ' AND "	
				Endif
				cQuery += "SE1.E1_PREFIXO>='"+aParam[6]+"' AND "
				cQuery += "SE1.E1_PREFIXO<='"+aParam[7]+"' AND " 
				If cPaisLoc != "BRA"
					cQuery += "SE1.E1_TIPO NOT IN" + FormatIn(cCheques,"|") + " AND "
				Endif	
				cQuery += "SE1.E1_SALDO > 0 AND "

				If aParam[11] == 2 // Se nao considera titulos gerados pela liquidacao
					If aParam[09] == 1 
						cQuery += "SE1.E1_NUMLIQ ='"+Space(Len(SE1->E1_NUMLIQ))+"' AND "
					Else  
						cQuery += "SE1.E1_TIPOLIQ='"+Space(Len(SE1->E1_TIPOLIQ))+"' AND "						
						cQuery += "SE1.E1_NUMLIQ ='"+Space(Len(SE1->E1_NUMLIQ))+"' AND "
					Endif	
				Else
					If aParam[09] == 2
						cQuery += "SE1.E1_TIPOLIQ='"+Space(Len(SE1->E1_TIPOLIQ))+"' AND "						
					Endif	
				Endif

				//�����������������������������������������������������Ŀ
				//� Ponto de entrada para filtrar pelo MSFIL em caso de �
				//� arquivo compartilhado.  Titulos em aberto           �
				//�������������������������������������������������������

				If (ExistBlock("FO10FILT"))    
					cQuery += ExecBlock("FO10FILT",.F.,.F.)
				Endif                    

				cQuery +=		"SE1.D_E_L_E_T_<>'*' AND "
				cQuery +=      "SX5.X5_FILIAL='"+xFilial("SX5")+"' AND "
				cQuery +=		"SX5.X5_TABELA='07' AND "
				cQuery +=		"SX5.X5_CHAVE=SE1.E1_SITUACA AND "
				cQuery +=		"SX5.D_E_L_E_T_<>'*' "

				cQuery += "AND SE1.E1_TIPO NOT LIKE '__-' UNION ALL "+cQuery
				cQuery += "AND SE1.E1_TIPO LIKE '__-'"

				If UPPER(TcGetDb()) != "INFORMIX"
					cQuery   += " ORDER BY  " + SqlOrder("E1_CLIENTE+E1_LOJA+E1_PREFIXO+E1_NUM+E1_PARCELA+SE1RECNO")
				Endif	

				//�������������������������������������������������������������������������Ŀ
				//� Ponto de entrada para alteracao da cQuery na consulta Titulos em Aberto �
				//���������������������������������������������������������������������������
				If (ExistBlock("F010CQTA"))    
					cQuery := ExecBlock("F010CQTA",.F.,.F.,{cQuery})
				Endif  

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"A"

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)

				aEval(aQuery,{|x| If(x[2]!="C",TcSetField(cQry,x[1],x[2],x[3],x[4]),Nil)})
			Else
				#ENDIF
				cQry := "SE1"
				#IFDEF TOP
			EndIf
			#ENDIF
			dbSelectArea(cQry)
			If ( !lQuery )
				dbSetOrder(2)
				If aParam[13] == 1 //Considera loja
					dbSeek(xFilial("SE1")+SA1->A1_COD+SA1->A1_LOJA)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE .And.;
					SA1->A1_LOJA   == SE1->E1_LOJA }
				Else
					dbSeek(xFilial("SE1")+SA1->A1_COD)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE }
				Endif

				bFiltro:= {|| !(SE1->E1_TIPO $ MVABATIM) .And.;
				SE1->E1_EMISSAO >= aParam[1] .And.;
				SE1->E1_EMISSAO <= aParam[2] .And.;
				SE1->E1_VENCREA >= aParam[3] .And.;
				SE1->E1_VENCREA <= aParam[4] .And.;
				If(aParam[5]==2,SE1->E1_TIPO!="PR ",.T.) .And.;
				If(aParam[15]==2,!SE1->E1_TIPO$MVRECANT,.T.) .And.;
				SE1->E1_PREFIXO >= aParam[6] .And.;
				SE1->E1_PREFIXO <= aParam[7] .And.;
				SE1->E1_SALDO   > 0 .And.;
				IIf(cPaisLoc == "BRA",.T.,!(SE1->E1_TIPO$cCheques)) .And.;
				IIF(aParam[11] == 2, Empty(SE1->E1_NUMLIQ) .And. Empty(SE1->E1_TIPOLIQ),.T.)}
			Else
				bWhile := {|| !Eof() }
				bFiltro:= {|| .T. }
			EndIf			
			While ( Eval(bWhile) )				
				If ( Eval(bFiltro) )
					If ( !lQuery )
						dbSelectArea("SX5")
						dbSetOrder(1)
						MsSeek(xFilial("SX5")+"07"+SE1->E1_SITUACA)
					EndIf
					dbSelectArea(cAlias)
					dbSetOrder(1)
					cChave := (cQry)->(E1_CLIENTE)+(cQry)->(E1_LOJA) +;
					(cQry)->(E1_PREFIXO)+(cQry)->(E1_NUM)+;
					(cQry)->(E1_PARCELA)
					cChave += If((cQry)->(E1_TIPO)	$ MVABATIM, "",;
					(cQry)->(E1_TIPO))
					If ( !dbSeek(cChave) )
						RecLock(cAlias,.T.)						
					Else
						RecLock(cAlias,.F.)
					EndIf
					DbSetOrder(1)
					nTotAbat := 0

					//Calculo de Juros e Multas: SIGALOJA x SIGAFIN - Inicio 
					nMulta := 0 
					If (cMVJurTipo == "L" .OR. lMvLjIntFS) .AND. lLojxRMul .And. aParam[12] == 2
						nMulta := LojxRMul( , , ,(cQry)->E1_SALDO, (cQry)->E1_ACRESC, (cQry)->E1_VENCREA,  , , (cQry)->E1_MULTA, ,;
						(cQry)->E1_PREFIXO, (cQry)->E1_NUM, (cQry)->E1_PARCELA, (cQry)->E1_TIPO, (cQry)->E1_CLIENTE, (cQry)->E1_LOJA,  ) 
					EndIf
					//Calculo de Juros e Multas: SIGALOJA x SIGAFIN - Final

					For nCntFor := 1 To Len(aStru)
						Do Case

							#ifdef SPANISH
							Case ( AllTrim(aStru[nCntFor][1])=="X5_DESCSPA" )
							If !( (cQry)->(E1_TIPO)	$ MVABATIM )
								If ( lQuery )
									(cAlias)->X5_DESCSPA := (cQry)->X5_DESCSPA
								Else
									(cAlias)->X5_DESCSPA := SX5->X5_DESCSPA
								EndIf
							Endif	
							#else
							#ifdef ENGLISH
							Case ( AllTrim(aStru[nCntFor][1])=="X5_DESCENG" )
							If !( (cQry)->(E1_TIPO)	$ MVABATIM )
								If ( lQuery )
									(cAlias)->X5_DESCENG := (cQry)->X5_DESCENG
								Else
									(cAlias)->X5_DESCENG := SX5->X5_DESCENG
								EndIf
							Endif	
							#else
							Case ( AllTrim(aStru[nCntFor][1])=="X5_DESCRI" )
							If !( (cQry)->(E1_TIPO)	$ MVABATIM )
								If ( lQuery )
									(cAlias)->X5_DESCRI := (cQry)->X5_DESCRI
								Else
									(cAlias)->X5_DESCRI := SX5->X5_DESCRI
								EndIf
							Endif	
							#endif
							#endif

							Case ( AllTrim(aStru[nCntFor][1])=="E1_VALJUR" )
							Case ( AllTrim(aStru[nCntFor][1])=="E1_ABT" )
							If cPaisLoc == "BRA"
								nTaxaM := (cQry)->E1_TXMOEDA
							Else
								nTaxaM:=round((cQry)->E1_VLCRUZ / (cQry)->E1_VALOR,4)  // Pegar a taxa da moeda usada qdo da inclus�o do titulo
							Endif
							If ( (cQry)->(E1_TIPO)	$ MVABATIM )
								(cAlias)->E1_ABT += (nTotAbat := xMoeda((cQry)->(E1_SALDO),(cQry)->(E1_MOEDA),1,(cQry)->(E1_EMISSAO),,nTaxaM))
							Endif
							If ( !lQuery )
								(cAlias)->E1_ABT := (nTotAbat := SomaAbat(SE1->E1_PREFIXO,SE1->E1_NUM,SE1->E1_PARCELA,"R",1,,SE1->E1_CLIENTE,SE1->E1_LOJA))
							Endif

							Case ( AllTrim(aStru[nCntFor][1])=="E1_SALDO" )
							If cPaisLoc == "BRA"
								nTaxaM := (cQry)->E1_TXMOEDA
							Else
								nTaxaM:=round((cQry)->E1_VLCRUZ / (cQry)->E1_VALOR,4)  // Pegar a taxa da moeda usada qdo da inclus�o do titulo
							Endif	
							If ( (cQry)->(E1_TIPO)	$ MVABATIM )
								If aParam[12] == 2	 // mv_par12 = 2 : Considera juros e taxa de pernamencia na visualizacao de titulos em aberto.
									(cAlias)->E1_SALDO -= nTotAbat
								Endif
							Else
								(cAlias)->E1_SALDO += xMoeda((cQry)->(E1_SALDO),(cQry)->(E1_MOEDA),1,dDataBase,,ntaxaM)
								If aParam[12] == 2   // mv_par12 = 2 : Considera juros e taxa de pernamencia na visualizacao de titulos em aberto.
									(cAlias)->E1_SALDO += xMoeda((cQry)->(E1_SDACRES) - (cQry)->(E1_SDDECRE),(cQry)->(E1_MOEDA),1,dDataBase,,ntaxaM)
									(cAlias)->E1_SALDO += xMoeda(FaJuros((cQry)->E1_VALOR,(cQry)->E1_SALDO,(cQry)->E1_VENCTO,(cQry)->E1_VALJUR,(cQry)->E1_PORCJUR,(cQry)->E1_MOEDA,(cQry)->E1_EMISSAO,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0),(cQry)->E1_BAIXA,(cQry)->E1_VENCREA,,(cQry)->E1_PREFIXO, (cQry)->E1_NUM, (cQry)->E1_PARCELA,(cQry)->E1_TIPO),(cQry)->E1_MOEDA,1,,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0)) 	//REQ020-Calculo de Juros e Multas: SIGALOJA x SIGAFIN 
									//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Inicil
									(cAlias)->E1_SALDO += xMoeda(nMulta,(cQry)->E1_MOEDA,1,,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0))
									//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Final


									//�������������������������������������������������������������Ŀ
									//� 11/Ago/2005 Rafael E. Rodrigues                             �
									//� Quando o modulo Gestao Educacional estiver em uso, inclui a �
									//� provisao de multa no saldo de titulos em atraso.            �
									//���������������������������������������������������������������
									If GetNewPar( "MV_ACATIVO", .F. )									
										(cAlias)->E1_SALDO += xMoeda(If(Empty((cQry)->(E1_BAIXA)) .and. dDataBase > (cQry)->(E1_VENCREA), (cQry)->(E1_VLMULTA), (cQry)->(E1_MULTA)),(cQry)->(E1_MOEDA),1,dDataBase,,ntaxaM)
									Endif
								Endif
							EndIf
							If ( !lQuery )
								If aParam[12] == 2   // mv_par12 = 2 : Considera juros e taxa de pernamencia na visualizacao de titulos em aberto.	
									(cAlias)->E1_SALDO -= nTotAbat
								Endif
							EndIf
							Case ( AllTrim(aStru[nCntFor][1])=="E1_SALDO2" )
							If ( (cQry)->(E1_TIPO)	$ MVABATIM )
								If aParam[12] == 2   // mv_par12 = 2 : Considera juros e taxa de pernamencia na visualizacao de titulos em aberto.	
									(cAlias)->E1_SALDO2 -= nTotAbat
								Endif
							Else
								(cAlias)->E1_SALDO2 += (cQry)->(E1_SALDO)
								//Calculo de Juros e Multas: SIGALOJA x SIGAFIN   -Inicio
								(cAlias)->E1_VALJUR := xMoeda(FaJuros((cQry)->E1_VALOR,(cAlias)->E1_SALDO2,(cQry)->E1_VENCTO,(cQry)->E1_VALJUR,(cQry)->E1_PORCJUR,(cQry)->E1_MOEDA,(cQry)->E1_EMISSAO,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0),(cQry)->E1_BAIXA,(cQry)->E1_VENCREA,,(cQry)->E1_PREFIXO, (cQry)->E1_NUM, (cQry)->E1_PARCELA,(cQry)->E1_TIPO),(cQry)->E1_MOEDA,1,,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0))										    
								/*BEGINDOC
								//���������������������������������������������������������D�
								//�Se o calculo de juros e multa for do controle de lojas, �
								//�considera os juros e multas no saldo                    �
								//���������������������������������������������������������D�
								ENDDOC*/
								If (cMVJurTipo == "L" .OR. lMvLjIntFs)  .AND. lLojxRMul							    
									(cAlias)->E1_MULTA := nMulta 
								EndIf 
								//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Final							   								
								If aParam[12] == 2   // mv_par12 = 2 : Considera juros e taxa de pernamencia na visualizacao de titulos em aberto.	
									(cAlias)->E1_SALDO2 += (cAlias)->E1_SDACRES - (cAlias)->E1_SDDECRE
									(cAlias)->E1_SALDO2 += xMoeda((cAlias)->E1_VALJUR,1,(cQry)->(E1_MOEDA),dDataBase,,ntaxaM) 
									If (cMVJurTipo == "L" .OR. lMvLjIntFS) .and. lLojxRMul
										(cAlias)->E1_SALDO2 += xMoeda((cAlias)->E1_MULTA,1,(cQry)->(E1_MOEDA),dDataBase,,ntaxaM) 
									EndIf

									//�������������������������������������������������������������Ŀ
									//� 11/Ago/2005 Rafael E. Rodrigues                             �
									//� Quando o modulo Gestao Educacional estiver em uso, inclui a �
									//� provisao de multa no saldo de titulos em atraso.            �
									//���������������������������������������������������������������
									If GetNewPar( "MV_ACATIVO", .F. )									
										(cAlias)->E1_SALDO2 += xMoeda(If(Empty((cQry)->(E1_BAIXA)) .and. dDataBase > (cQry)->(E1_VENCREA), (cQry)->(E1_VLMULTA), (cQry)->(E1_MULTA)),(cQry)->(E1_MOEDA),1,dDataBase,,ntaxaM)
									Endif								
								Endif
							EndIf
							If ( !lQuery )
								If aParam[12] == 2   // mv_par12 = 2 : Considera juros e taxa de pernamencia na visualizacao de titulos em aberto.	
									(cAlias)->E1_SALDO2 -= nTotAbat
								Endif
							EndIf		
							Case ( AllTrim(aStru[nCntFor][1])=="XX_RECNO" )
							If !( (cQry)->(E1_TIPO)	$ MVABATIM )
								If ( lQuery )
									(cAlias)->XX_RECNO := (cQry)->SE1RECNO
								Else
									(cAlias)->XX_RECNO := SE1->(RecNo())
								EndIf
							Endif
							Case ( !lRelat .And. AllTrim(aStru[nCntFor][1])=="XX_LEGEND" )
							If (cQry)->E1_CHQDEV == "1"
								(cAlias)->XX_LEGEND := 	"BR_AMARELO"
							Else
								If !((cQry)->E1_TIPO $ MVABATIM)
									(cAlias)->XX_LEGEND := If(ROUND((cQry)->E1_SALDO,2) != ROUND((cQry)->E1_VALOR,2),"BR_AZUL","BR_VERDE")
								EndIf
							Endif
							Case ( AllTrim(aStru[nCntFor][1])=="E1_TIPO" )
							If ( Empty((cAlias)->E1_TIPO) )
								(cAlias)->E1_TIPO := (cQry)->E1_TIPO
							EndIf
							Case ( AllTrim(aStru[nCntFor][1])=="E1_ATR" )
							//Se o t�tulo estiver atrasado, faz o calculo dos dias de atraso
							If dDataBase > (cQry)->E1_VENCREA
								If (((cAlias)->E1_TIPO) $ MVRECANT+"/"+MV_CRNEG)
									(cAlias)->E1_ATR := 0
								Else	
									(cAlias)->E1_ATR := dDataBase - (cAlias)->E1_VENCREA
								EndIf	
							Else 
								If MV_PAR16 == 2 //Se o t�tulo N�O estiver atrasado, ent�o tem ATRASO = 0
									(cAlias)->E1_ATR := 0
								Else
									(cAlias)->E1_ATR := dDataBase - DataValida((cAlias)->E1_VENCREA,.T.)
								EndIf
							Endif
							Case ( AllTrim(aStru[nCntFor][1])=="FLAG" )

							Case ( AllTrim(aStru[nCntFor][1])=="E1_VLCRUZ" )
							If !((cQry)->(E1_TIPO)	$ MVABATIM)
								(cAlias)->E1_VLCRUZ := xMoeda((cQry)->(E1_VALOR),(cQry)->(E1_MOEDA),1,dDataBase,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0))
							Endif
							Case ( AllTrim(aStru[nCntFor][1])=="E1_VLMULTA" )
							//��������������������������������������������������������������Ŀ
							//� 11/Ago/2005 Rafael E. Rodrigues                              �
							//� Quando o modulo Gestao Educacional estiver em uso eh exibida �
							//� a provisao de multa no saldo de titulos em atraso.           �
							//����������������������������������������������������������������
							(cAlias)->E1_VLMULTA := xMoeda(If(Empty((cQry)->(E1_BAIXA)) .and. dDataBase > (cQry)->(E1_VENCREA), (cQry)->(E1_VLMULTA), (cQry)->(E1_MULTA)),(cQry)->(E1_MOEDA),1,dDataBase,,ntaxaM)						
							OtherWise							
							If !( (cQry)->(E1_TIPO)	$ MVABATIM )
								(cAlias)->(FieldPut(nCntFor,(cQry)->(FieldGet(FieldPos(aStru[nCntFor][1])))))
							Endif	
						EndCase
					Next nCntFor
					dbSelectArea(cAlias)
					If nTotAbat = 0
						If ( (cAlias)->E1_SALDO <= 0 )
							dbDelete()
						EndIf
					Endif						
					MsUnLock()
				EndIf
				dbSelectArea(cQry)
				dbSkip()				
			EndDo
			If ( lQuery )
				dbSelectArea(cQry)
				dbCloseArea()
			EndIf
			cOrdem := "DTOS(E1_VENCREA)"

			If (ExistBlock("F010ORD1"))    
				//Retornar chave no formato "E1_CLIENTE+E1_LOJA+E1_TIPO+E1_PREFIXO+E1_NUM+E1_PARCELA+SE1RECNO"
				cOrdem := ExecBlock("F010ORD1",.F.,.F.)
			Endif                    

			dbSelectArea(cAlias)
			IndRegua(cAlias,cArquivo,cOrdem)
		EndIf
		//������������������������������������������������������������������������Ŀ
		//�Totais da Consulta                                                      �
		//��������������������������������������������������������������������������
		aGet[1] := 0
		aGet[2] := 0
		aGet[3] := 0
		aGet[4] := 0
		aGet[5] := 0
		aGet[6] := 0
		aGet[7] := 0
		aGet[8] := 0
		aTotRec := {{0,1,0,0}} // Totalizador de titulos a receber por moeda
		dbSelectArea(cAlias)
		dbGotop()
		While !EOF()		 			 	
			aGet[1]++
			If !lRelat
				SE1->(DbGoto((cAlias)->XX_RECNO))	// Posiciona no arquivo original para obter os valores
				// em outras moedas e em R$
				//	nAscan := Ascan(aTotRec,{|e| e[MOEDATIT] == E1_MOEDA})
			Endif

			//Calcular o abatimento para visualiza��o em tela
			If (cAlias)->E1_ABT > 0
				//(cAlias)->E1_SALDO -= (cAlias)->E1_ABT		 	
				(cAlias)->E1_SALDO2 := xMoeda((cAlias)->E1_SALDO,E1_MOEDA,1,dDataBase,,ntaxaM)
			Endif		 	

			If E1_TIPO $ "RA #"+MV_CRNEG
				aGet[2] -= E1_VLCRUZ
				aGet[3] -= E1_SALDO
				aGet[4] -= E1_VALJUR

				nAcresc := nDecres := 0
				If !lRelat
					nAcresc := xMoeda(E1_SDACRES,E1_MOEDA,1,dDataBase,,ntaxaM)
					nDecres := xMoeda(E1_SDDECRE,E1_MOEDA,1,dDataBase,,ntaxaM)
					aGet[5] -= nAcresc
					aGet[6] -= nDecres
					If nAscan = 0
						Aadd(aTotRec,{1,E1_MOEDA,SE1->E1_SALDO*(-1),If(E1_MOEDA>1,xMoeda(SE1->E1_SALDO,E1_MOEDA,1,,,If(cPaisLoc=="BRA",SE1->E1_TXMOEDA,0)),SE1->E1_SALDO)*(-1)})
					Else
						aTotRec[nAscan][QTDETITULOS]--
						aTotRec[nAscan][VALORTIT]		-= SE1->E1_SALDO
						aTotRec[nAscan][VALORREAIS]	-= If(E1_MOEDA>1,xMoeda(SE1->E1_SALDO,E1_MOEDA,1,,,If(cPaisLoc=="BRA",SE1->E1_TXMOEDA,0)),SE1->E1_SALDO)
					Endif
				Endif	
				If aParam[12] == 1 //Saldo sem correcao
					aGet[8] -= E1_SALDO-E1_ABT+E1_VALJUR+nAcresc-nDecres
				Else
					aGet[8] -= E1_SALDO
				Endif
			Else	
				aGet[2] += E1_VLCRUZ
				aGet[3] += E1_SALDO
				aGet[4] += E1_VALJUR
				aGet[7] += E1_ABT
				nAcresc := nDecres := 0
				If !lRelat
					nAcresc := xMoeda(E1_SDACRES,E1_MOEDA,1,dDataBase,,ntaxaM)
					nDecres := xMoeda(E1_SDDECRE,E1_MOEDA,1,dDataBase,,ntaxaM)
					aGet[5] += nAcresc
					aGet[6] += nDecres
					If nAscan = 0
						Aadd(aTotRec,{1,E1_MOEDA,SE1->E1_SALDO,If(E1_MOEDA>1,xMoeda(SE1->E1_SALDO,E1_MOEDA,1,,,If(cPaisLoc=="BRA",SE1->E1_TXMOEDA,0)),SE1->E1_SALDO)})
					Else
						aTotRec[nAscan][QTDETITULOS]++  
						aTotRec[nAscan][VALORTIT]		+= SE1->E1_SALDO     
						aTotRec[nAscan][VALORREAIS]	+= If(E1_MOEDA>1,xMoeda(SE1->E1_SALDO,E1_MOEDA,1,,,If(cPaisLoc=="BRA",SE1->E1_TXMOEDA,0)),SE1->E1_SALDO)
					Endif
				Endif
				If aParam[12] == 1 //Saldo sem correcao
					aGet[8] += E1_SALDO-E1_ABT+E1_VALJUR+nAcresc-nDecres
				Else
					aGet[8] += E1_SALDO
				Endif
			Endif
			dbSkip()
		Enddo
		If !lRelat
			nTotalRec:=0
			aEval(aTotRec,{|e| nTotalRec+=e[VALORREAIS]})
			Aadd(aTotRec,{"","",STR0096,nTotalRec}) //"Total ====>>"
			// Formata as colunas
			aEval(aTotRec,{|e|	If(ValType(e[VALORTIT]) == "N"	, e[VALORTIT]		:= Transform(e[VALORTIT],Tm(e[VALORTIT],16,nCayas)),Nil),;
			If(ValType(e[VALORREAIS]) == "N"	, e[VALORREAIS]	:= Transform(e[VALORREAIS],Tm(e[VALORREAIS],16,nCayas)),Nil)})
		Endif										

		aGet[1] := TransForm(aGet[1],Tm(aGet[1],16,0))
		aGet[2] := TransForm(aGet[2],Tm(aGet[2],16,nCayas))
		aGet[3] := TransForm(aGet[3],Tm(aGet[3],16,nCayas))
		aGet[4] := TransForm(aGet[4],Tm(aGet[4],16,nCayas))
		aGet[5] := TransForm(aGet[5],Tm(aGet[5],16,nCayas))
		aGet[6] := TransForm(aGet[6],Tm(aGet[6],16,nCayas))
		aGet[7] := TransForm(aGet[7],Tm(aGet[7],16,nCayas))		
		aGet[8] := TransForm(aGet[8],Tm(aGet[8],16,nCayas))		
		//������������������������������������������������������������������������Ŀ
		//�Titulos Recebidos                                                       �
		//��������������������������������������������������������������������������
		Case ( nBrowse == 2 )
		dbSelectArea("SX3")
		dbSetOrder(2)
		If aParam[13] == 2  //Considera loja
			dbSeek("E1_LOJA")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif
		dbSeek("E1_PREFIXO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_NUM")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_PARCELA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_TIPO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		IF !lRelat
			dbSeek("E1_MOEDA")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif

		dbSeek("E1_EMISSAO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_VENCTO")
		If !lRelat
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_VENCREA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_DATA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_DTDISPO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		IF !lRelat
			dbSeek("E1_VALOR")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif	

		dbSeek("E1_VLCRUZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		For nCntFor := 1 To 4
			dbSeek(	If(nCntFor==1,	"E5_VLJUROS",;
			If(nCntFor==2,	"E5_VLMULTA",;
			If(nCntFor==3,	"E5_VLCORRE",;
			"E5_VLDESCO"))))
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Next	

		dbSeek("E5_VALOR")
		aadd(aHeader,{STR0047,"E1_PAGO",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,"V" } ) //"Pago"
		aadd(aStru ,{"E1_PAGO",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		IF !lRelat
			dbSeek("E1_VALOR")
			aadd(aHeader,{ STR0093,"E1_VLMOED2",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } ) //"Vlr pago  moeda tit."
			aadd(aStru ,{"E1_VLMOED2",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

			dbSeek("E5_VLMOED2")
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

			If cPaisLoc == "BRA"
				dbSeek("E5_TXMOEDA")
				aadd(aHeader,{AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
				aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
				aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			Endif
		Endif	

		dbSeek("E1_NATUREZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		If !lRelat
			dbSeek("E1_NUMLIQ")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif

		dbSeek("E5_BANCO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_AGENCIA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_CONTA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_HISTOR")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_MOTBX")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		If !lRelat
			dbSeek("E5_CNABOC")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif

		dbSeek("E5_TIPODOC")
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})		


		aadd(aHeader,{STR0035,"E1_ATR","9999999999",10,0,"","","N","","V" } ) //"Atraso"
		aadd(aStru ,{"E1_ATR","N",10,0})

		dbSeek("E1_VALJUR")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		//�������������������������������������������������������������Ŀ
		//� 11/Ago/2005 Rafael E. Rodrigues                             �
		//� Inclui a coluna com informacao de multa paga por paramento  �
		//� em atraso.                                                  �
		//���������������������������������������������������������������
		dbSeek("E1_MULTA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aadd(aStru,{"XX_RECNO","N",12,0})

		If cPaisLoc == "BRA"
			aadd(aQuery,{"E1_TXMOEDA","N",17,4})
		Endif	

		SX3->(DbSetOrder(1))

		If ( Select(cAlias) ==	0 )
			cArquivo := CriaTrab(,.F.)			
			aadd(aAlias,{ cAlias , cArquivo })
			aadd(aStru,{"FLAG","L",01,0})
			dbCreate(cArquivo,aStru)
			dbUseArea(.T.,,cArquivo,cAlias,.F.,.F.)
			IndRegua(cAlias,cArquivo,"E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO")

			#IFDEF TOP
			If ( TcSrvType()!="AS/400" )
				cQuery := ""
				aEval(aQuery,{|x| cQuery += ","+AllTrim(x[1])})
				cQuery := "SELECT "+SubStr(cQuery,2)+",E1_ORIGEM,SE5.R_E_C_N_O_ SE5RECNO , SE5.E5_DOCUMEN E5_DOCUMEN "
				cQuery += "FROM "+RetSqlName("SE1")+" SE1,"
				cQuery +=         RetSqlName("SE5")+" SE5 "
				cQuery += "WHERE SE1.E1_FILIAL='"+xFilial("SE1")+"' AND "
				cQuery +=       "SE1.E1_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=       "SE1.E1_LOJA='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=       "SE1.E1_EMISSAO>='"+Dtos(aParam[1])+"' AND "
				cQuery +=       "SE1.E1_EMISSAO<='"+Dtos(aParam[2])+"' AND "
				cQuery +=       "SE1.E1_VENCREA>='"+Dtos(aParam[3])+"' AND "
				cQuery +=       "SE1.E1_VENCREA<='"+Dtos(aParam[4])+"' AND "
				If ( aParam[5] == 2 )
					cQuery +=   "SE1.E1_TIPO<>'PR ' AND "
				EndIf					
				If ( aParam[15] == 2 )
					cQuery +=   "SE1.E1_TIPO<>'RA ' AND "
				EndIf					
				cQuery +=       "SE1.E1_PREFIXO>='"+aParam[6]+"' AND "
				cQuery +=       "SE1.E1_PREFIXO<='"+aParam[7]+"' AND "

				//�����������������������������������������������������Ŀ
				//� Ponto de entrada para filtrar pelo MSFIL em caso de �
				//� arquivo compartilhado. Titulos Recebidos            �
				//�������������������������������������������������������

				If (ExistBlock("FO10FLTR"))    
					cQuery += ExecBlock("FO10FLTR",.F.,.F.)
				Endif                    

				If cPaisLoc != "BRA"
					cQuery += "SE1.E1_TIPO NOT IN" + FormatIn(cCheques,"|") + " AND "
				Else
					cQuery += "SE1.E1_ORIGEM <> 'FINA087A' AND "
				Endif	
				cQuery +=		"SE1.E1_TIPO NOT LIKE '__-' AND "
				cQuery +=		"SE1.E1_TIPO NOT IN ('RA ','PA ','"+MV_CRNEG+"','"+MV_CPNEG+"') AND "
				cQuery +=		"SE1.D_E_L_E_T_<>'*' AND "
				cQuery +=       "SE5.E5_FILIAL='"+xFilial("SE5")+"' AND "
				cQuery +=		"SE5.E5_NATUREZ=SE1.E1_NATUREZ AND "
				cQuery +=		"SE5.E5_PREFIXO=SE1.E1_PREFIXO AND "
				cQuery +=		"SE5.E5_NUMERO=SE1.E1_NUM AND "
				cQuery +=		"SE5.E5_PARCELA=SE1.E1_PARCELA AND "
				cQuery +=		"SE5.E5_TIPO=SE1.E1_TIPO AND "
				cQuery +=		"SE5.E5_CLIFOR=SE1.E1_CLIENTE AND "
				cQuery +=		"SE5.E5_LOJA=SE1.E1_LOJA AND "
				cQuery +=		"SE5.E5_RECPAG='R' AND "
				cQuery +=		"SE5.E5_SITUACA<>'C' AND "
				If aParam[8] == 2
					//Titulos baixados por geracao de fatura
					cQuery += " SE5.E5_MOTBX <> 'FAT' AND "
				Endif
				If aParam[09] == 2
					//Titulos baixados por liquidacao
					cQuery += " SE5.E5_MOTBX <> 'LIQ' AND "
				Endif
				cQuery +=		"SE5.D_E_L_E_T_<>'*' AND NOT EXISTS ("
				cQuery += "SELECT A.E5_NUMERO "
				cQuery += "FROM "+RetSqlName("SE5")+" A "
				cQuery += "WHERE A.E5_FILIAL='"+xFilial("SE5")+"' AND "
				cQuery +=		"A.E5_NATUREZ=SE5.E5_NATUREZ AND "
				cQuery +=		"A.E5_PREFIXO=SE5.E5_PREFIXO AND "
				cQuery +=		"A.E5_NUMERO=SE5.E5_NUMERO AND "
				cQuery +=		"A.E5_PARCELA=SE5.E5_PARCELA AND "
				cQuery +=		"A.E5_TIPO=SE5.E5_TIPO AND "
				cQuery +=		"A.E5_CLIFOR=SE5.E5_CLIFOR AND "
				cQuery +=		"A.E5_LOJA=SE5.E5_LOJA AND "
				cQuery +=		"A.E5_SEQ=SE5.E5_SEQ AND "
				cQuery +=		"A.E5_TIPODOC='ES' AND "
				cQuery +=		"A.E5_RECPAG<>'R' AND "
				cQuery +=		"A.D_E_L_E_T_<>'*')"

				//�������������������������������������������������������������������������Ŀ
				//� Ponto de entrada para alteracao da cQuery na consulta Titulos Recebidos �
				//���������������������������������������������������������������������������
				If (ExistBlock("F010CQTR"))    
					cQuery := ExecBlock("F010CQTR",.F.,.F.,{cQuery})
				Endif

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"A"

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)

				aEval(aQuery,{|x| If(x[2]!="C",TcSetField(cQry,x[1],x[2],x[3],x[4]),Nil)})

				dbSelectArea(cQry)
				bWhile := {|| !Eof() }
				bFiltro:= {|| (	(cQry)->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" .AND.;
				IIf( UPPER(SUBSTR((cQry)->E5_HISTOR,1,4)) <> "LOJ-", .T.,;
				( ( (cQry)->E1_NUM == cNumAnt .AND. (cQry)->E5_TIPODOC == cTpDocAnt ) .OR.;
				( (cQry)->E1_NUM == cNumAnt .AND. (cQry)->E1_PARCELA <> cParcAnt  ) .OR.;
				(cQry)->E1_NUM <> cNumAnt ) ) ) }
				cAnterior := ""
				While ( Eval(bWhile) )				
					If ( Eval(bFiltro) )
						cNumAnt := (cQry)->E1_NUM
						cTpDocAnt := (cQry)->E5_TIPODOC
						cParcAnt := (cQry)->E1_PARCELA
						dbSelectArea(cAlias)
						dbSetOrder(1)
						RecLock(cAlias,.T.)
						For nCntFor := 1 To Len(aStru)
							Do Case
								Case ( AllTrim(aStru[nCntFor][1])=="E1_PAGO" )
								If ( (cQry)->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" )
									(cAlias)->E1_PAGO += (cQry)->E5_VALOR
								EndIf
								Case ( AllTrim(aStru[nCntFor][1])=="E1_VLMOED2" ) .And. !lRelat
								If ( (cQry)->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" ) .And. (cQry)->E1_MOEDA > 1
									(cAlias)->E1_VLMOED2 := If((cQry)->E1_MOEDA > 1, (cQry)->E5_VLMOED2, (cQry)->E5_VALOR)
								EndIf	
								Case ( AllTrim(aStru[nCntFor][1])=="E1_SALDO2" )
								If ( (cQry)->(E1_TIPO)	$ MVABATIM )
									(cAlias)->E1_SALDO2 -= nTotAbat
								Else
									(cAlias)->E1_SALDO2 += (cQry)->(E1_SALDO)
									(cAlias)->E1_SALDO2 += (cAlias)->E1_SDACRES - (cAlias)->E1_SDDECRE
								EndIf
								If ( !lQuery )
									(cAlias)->E1_SALDO2 -= nTotAbat
								EndIf	
								Case ( AllTrim(aStru[nCntFor][1])=="E1_VLCRUZ" )
								If ( (cQry)->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" )
									If cAnterior != (cQry)->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
										(cAlias)->E1_VLCRUZ := (cQry)->E1_VLCRUZ
										cAnterior := (cQry)->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
									Endif
								EndIf										
								Case ( AllTrim(aStru[nCntFor][1])=="E1_ATR" )
								If (cQry)->E5_DATA > DataValida((cQry)->E1_VENCREA,.T.)
									(cAlias)->E1_ATR := (cQry)->E5_DATA - (cQry)->E1_VENCREA
								Else
									(cAlias)->E1_ATR := (cQry)->E5_DATA - DataValida((cQry)->E1_VENCREA,.T.)
								Endif
								Case cPaisLoc=="BRA" .And. ( AllTrim(aStru[nCntFor][1])=="E5_TXMOEDA" )  .And. !lRelat
								If (cQry)->E1_MOEDA == 1
									(cAlias)->E5_TXMOEDA := 1
								Else
									If (cQry)->E5_TXMOEDA == 0 
										(cAlias)->E5_TXMOEDA := ((cQry)->E5_VALOR /(cQry)->E5_VLMOED2)
									Else
										(cAlias)->E5_TXMOEDA := (cQry)->E5_TXMOEDA
									Endif
								Endif
								Case ( !lRelat .And. AllTrim(aStru[nCntFor][1])=="E1_NUMLIQ" )
								If (cQry)->E5_MOTBX == "LIQ"
									If Empty( (cQry)->E1_NUMLIQ )
										(cAlias)->E1_NUMLIQ := SUBSTR((cQry)->E5_DOCUMEN,1,TamSx3("E1_NUMLIQ")[1])
									Else
										(cAlias)->(FieldPut(nCntFor,(cQry)->(FieldGet(FieldPos(aStru[nCntFor][1])))))			
									Endif		
								Endif
								Case ( AllTrim(aStru[nCntFor][1])=="XX_RECNO" )
								(cAlias)->XX_RECNO := (cQry)->SE5RECNO

								Case ( AllTrim(aStru[nCntFor][1])=="FLAG" )

								OtherWise
								(cAlias)->(FieldPut(nCntFor,(cQry)->(FieldGet(FieldPos(aStru[nCntFor][1])))))
							EndCase
						Next nCntFor
						(cAlias)->(MsUnLock())
					EndIf
					dbSelectArea(cQry)
					dbSkip()				
				EndDo
				dbSelectArea(cQry)
				dbCloseArea()
			Else
				#ENDIF
				dbSelectArea("SE1")
				dbSetOrder(2)
				If aParam[13] == 1  //Considera loja
					dbSeek(xFilial("SE1")+SA1->A1_COD+SA1->A1_LOJA)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE .And.;
					SA1->A1_LOJA   == SE1->E1_LOJA }
				Else
					dbSeek(xFilial("SE1")+SA1->A1_COD)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE }
				Endif

				bFiltro:= {|| 	SubStr(SE1->E1_TIPO,3,1)!="-" .And.;
				SE1->E1_EMISSAO >= aParam[1] .And.;
				SE1->E1_EMISSAO <= aParam[2] .And.;
				SE1->E1_VENCREA >= aParam[3] .And.;
				SE1->E1_VENCREA <= aParam[4] .And.;
				If(aParam[5]==2,SE1->E1_TIPO!="PR ",.T.) .And.;
				If(aParam[15]==2,!SE1->E1_TIPO$MVRECANT,.T.) .And.;									
				SE1->E1_PREFIXO >= aParam[6] .And.;
				SE1->E1_PREFIXO <= aParam[7] .And.;
				IIf(cPaisLoc == "BRA",(SE1->E1_ORIGEM<>"FINA087A"),!(SE1->E1_TIPO$cCheques)) }

				While ( Eval(bWhile) )
					If ( Eval(bFiltro) )
						dbSelectArea("SE5")
						dbSetOrder(7)
						dbSeek(xFilial("SE5")+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+SE1->E1_TIPO+SE1->E1_CLIENTE+SE1->E1_LOJA)

						While ( !Eof() .And. xFilial("SE5") == SE5->E5_FILIAL .And.;
						SE1->E1_PREFIXO == SE5->E5_PREFIXO .And.;
						SE1->E1_NUM == SE5->E5_NUMERO      .And.;
						SE1->E1_PARCELA == SE5->E5_PARCELA .And.;
						SE1->E1_TIPO == SE5->E5_TIPO       .And.;
						SE1->E1_CLIENTE == SE5->E5_CLIFOR	.And.;
						SE1->E1_LOJA == SE5->E5_LOJA )

							// Baixas efetuadas por liquidacao ou por fatura, se nao devem ser consideradas despreza
							If (aParam[08] == 2 .And. SE5->E5_MOTBX == "FAT") .Or.;
							(aParam[09] == 2 .And. SE5->E5_MOTBX == "LIQ")
								dbSelectArea("SE5")
								dbSkip()	
								Loop
							Endif

							If ((!SE5->E5_TIPO $ "RA /PA /"+MV_CRNEG+"/"+MV_CPNEG) .And. !TemBxCanc() .and. ;
							SE5->E5_SITUACA <> "C" .And. SE5->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$")
								RecLock(cAlias,.T.)
								For nCntFor := 1 To Len(aStru)
									Do Case
										Case ( AllTrim(aStru[nCntFor][1])=="E1_PAGO" )
										If ( SE5->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" )
											(cAlias)->E1_PAGO += SE5->E5_VALOR
										EndIf
										Case ( AllTrim(aStru[nCntFor][1])=="E1_VLMOED2" ) .And. !lRelat
										If ( SE5->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" ) .And. SE1->E1_MOEDA > 1
											(cAlias)->E1_VLMOED2 := If(SE1->E1_MOEDA > 1, SE5->E5_VLMOED2, SE5->E5_VALOR)
										EndIf		
										Case ( AllTrim(aStru[nCntFor][1])=="E1_VLCRUZ" )
										If ( SE5->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" )
											If cAnterior != SE1->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
												(cAlias)->E1_VLCRUZ := SE1->E1_VLCRUZ
												cAnterior := SE1->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
											Endif
										Endif	
										Case ( AllTrim(aStru[nCntFor][1])=="E1_ATR" )
										If SE5->E5_DATA > DataValida(SE1->E1_VENCREA,.T.)
											(cAlias)->E1_ATR := SE5->E5_DATA - SE1->E1_VENCREA
										Else
											(cAlias)->E1_ATR := SE5->E5_DATA - DataValida(SE1->E1_VENCREA,.T.)
										Endif
										Case cPaisLoc=="BRA" .And. ( AllTrim(aStru[nCntFor][1])=="E5_TXMOEDA" ) .And. !lRelat
										If SE1->E1_MOEDA == 1
											(cAlias)->E5_TXMOEDA := 1
										Else
											If SE5->E5_TXMOEDA == 0 
												(cAlias)->E5_TXMOEDA := (SE5->E5_VALOR /SE5->E5_VLMOED2)
											Else
												(cAlias)->E5_TXMOEDA := SE5->E5_TXMOEDA
											Endif
										Endif
										Case ( AllTrim(aStru[nCntFor][1])=="E1_NUMLIQ" )
										If SE5->E5_MOTBX == "LIQ"
											If Empty(SE1->E1_NUMLIQ)
												(cAlias)->E1_NUMLIQ := SUBSTR(SE5->E5_DOCUMEN,1,TamSx3("E1_NUMLIQ")[1])
											Else
												(cAlias)->(FieldPut(nCntFor,SE1->(FieldGet(FieldPos(aStru[nCntFor][1])))))
											Endif
										Endif
										Case ( AllTrim(aStru[nCntFor][1])=="XX_RECNO" )
										(cAlias)->XX_RECNO := SE5->(RecNo())
										Case ( "E5_"$AllTrim(aStru[nCntFor][1]) )
										(cAlias)->(FieldPut(nCntFor,SE5->(FieldGet(FieldPos(aStru[nCntFor][1])))))
										Case ( AllTrim(aStru[nCntFor][1])=="FLAG" )
										OtherWise
										(cAlias)->(FieldPut(nCntFor,SE1->(FieldGet(FieldPos(aStru[nCntFor][1])))))
									EndCase
								Next nCntFor
								(cAlias)->(MsUnLock())
							EndIf
							dbSelectArea("SE5")
							dbSkip()
						EndDo										
					EndIf
					dbSelectArea("SE1")
					dbSkip()					
				EndDo
				#IFDEF TOP
			EndIf					
			#ENDIF
			dbSelectArea(cAlias)
			IndRegua(cAlias,cArquivo,"DTOS(E1_VENCREA)")
		EndIf
		//������������������������������������������������������������������������Ŀ
		//�Totais da Consulta                                                      �
		//��������������������������������������������������������������������������
		aGet[1] := 0
		aGet[2] := 0
		aGet[3] := 0
		aTotPag := {{0,1,0,0}} // Totalizador de titulos recebidos por por moeda
		dbSelectArea(cAlias)
		dbGotop()
		_FC010TotRc(aGet,cAlias,aTotPag)  // Totais de Baixas
		If !lRelat
			nTotalRec:=0
			aEval(aTotPag,{|e| nTotalRec+=e[VALORREAIS]})
			Aadd(aTotPag,{"","",STR0094,nTotalRec}) //"Total ====>>"
			// Formata as colunas
			aEval(aTotPag,{|e|	If(ValType(e[VALORTIT]) == "N"	, e[VALORTIT]		:= Transform(e[VALORTIT],Tm(e[VALORTIT],16,nCayas)),Nil),;
			If(ValType(e[VALORREAIS]) == "N"	, e[VALORREAIS]	:= Transform(e[VALORREAIS],Tm(e[VALORREAIS],16,nCayas)),Nil)})
		Endif
		aGet[1] := TransForm(aGet[1],Tm(aGet[1],16,0))
		aGet[2] := TransForm(aGet[2],Tm(aGet[2],16,nCayas))
		aGet[3] := TransForm(aGet[3],Tm(aGet[3],16,nCayas))
		//������������������������������������������������������������������������Ŀ
		//�Pedidos                                                                 �
		//��������������������������������������������������������������������������
		Case ( nBrowse == 3 )
		dbSelectArea("SX3")
		dbSetOrder(2)                                   	
		If aParam[13] == 1  //Considera loja
			dbSeek("C5_LOJACLI")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif
		dbSeek("C5_NUM")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("C5_EMISSAO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("C6_VALOR")
		aadd(aHeader,{STR0040,"XX_SLDTOT",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } ) //"Tot.Pedido"
		aadd(aStru ,{"XX_SLDTOT",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aHeader,{STR0041,"XX_SLDLIB",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } ) //"Sld.Liberado"
		aadd(aStru ,{"XX_SLDLIB",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aHeader,{STR0042,"XX_SLDPED",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } ) //"Sld.Pedido"
		aadd(aStru ,{"XX_SLDPED",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aStru,{"XX_RECNO","N",12,0})

		IF lFC010Pedi
			aRetAux := aClone(ExecBlock("FC010PEDI",.F.,.F.,{aClone(aHeader),aClone(aStru)}))
			If ValType(aRetAux) <> "A" .or. Len(aRetAux) <> 2
				aRetAux := {{},{}}
			Else
				aEval(aRetAux[1] , {|x| aAdd(aHeader , x ) })
				aEval(aRetAux[2] , {|x| aAdd(aStru , x ) })
			EndIf
		EndIf

		SX3->(dbSetOrder(1))

		If ( Select(cAlias) ==	0 )
			cArquivo := CriaTrab(,.F.)			
			aadd(aAlias,{ cAlias , cArquivo })
			aadd(aStru,{"FLAG","L",01,0})
			dbCreate(cArquivo,aStru)
			dbUseArea(.T.,,cArquivo,cAlias,.F.,.F.)
			IndRegua(cAlias,cArquivo,"C5_NUM")

			#IFDEF TOP
			If ( TcSrvType()!="AS/400" )
				lQuery := .T.
				cQuery := "SELECT SC5.C5_NUM PEDIDO,"
				cQuery += 		  "SC5.C5_EMISSAO EMISSAO," 
				//*************************************
				// Monta campos do ponro de entrada   * 
				// FC010PEDI na tabela temporaria.    *
				//*************************************
				IF lFC010Pedi
					aEval(aRetAux[2] , {|x| cQuery += x[1] + "," })
				EndIf
				cQuery +=  		  "SC5.C5_MOEDA MOEDA,"    
				cQuery +=  		  "SC5.C5_FRETE FRETE,"
				cQuery += 		  "SC5.R_E_C_N_O_ SC5RECNO,"
				cQuery += 		  "(C6_QTDVEN-C6_QTDEMP-C6_QTDENT) QTDVEN,"
				cQuery +=		  "C6_PRCVEN PRCVEN,"
				cQuery +=         "1 TIPO,"
				cQuery +=         "C5_EMISSAO DATALIB," //NAO RETIRAR - POSTGRES
				cQuery +=         "C6_BLQ BLCRED "		//NAO RETIRAR - POSTGRES
				cQuery += "FROM "+RetSqlName("SC5")+" SC5,"
				cQuery +=         RetSqlName("SC6")+" SC6,"
				cQuery +=         RetSqlName("SF4")+" SF4 "
				cQuery += "WHERE SC5.C5_FILIAL='"+xFilial("SC5")+"' AND "
				cQuery += 		"SC5.C5_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=		"SC5.C5_LOJACLI='"+SA1->A1_LOJA+"' AND "
				Endif

				//�����������������������������������������������������Ŀ
				//� Ponto de entrada para filtrar pelo MSFIL em caso de �
				//� arquivo compartilhado.  Pedidos                     �
				//�������������������������������������������������������

				If (ExistBlock("F010FLPD"))    
					cQuery += ExecBlock("F010FLPD",.F.,.F.)
				Endif                    

				cQuery +=		"SC5.C5_TIPO NOT IN('D','B') AND "
				cQuery +=		"SC5.C5_EMISSAO >='"+Dtos(aParam[1])+"' AND "
				cQuery +=		"SC5.C5_EMISSAO <='"+Dtos(aParam[2])+"' AND "	
				cQuery +=		"SC5.D_E_L_E_T_<>'*' AND "
				cQuery +=		"SC6.C6_FILIAL='"+xFilial("SC6")+"' AND "
				cQuery +=		"SC6.C6_NUM=SC5.C5_NUM AND "
				cQuery +=		"SC6.C6_BLQ NOT IN('R ') AND "
				If aParam[10] == 2 // nao considera pedidos com bloqueio
					cQuery +=		"SC6.C6_BLQ NOT IN('S ') AND "
				Endif
				cQuery +=		"(SC6.C6_QTDVEN-SC6.C6_QTDEMP-SC6.C6_QTDENT)>0 AND "
				cQuery +=		"SC6.D_E_L_E_T_<>'*' AND "
				cQuery +=		"SF4.F4_FILIAL='"+xFilial("SF4")+"' AND "
				cQuery +=		"SF4.F4_CODIGO=SC6.C6_TES AND "
				//����������������������������������������������������������������Ŀ
				//� Considera sim, n�o ou ambos os itens com TES gerando duplicata �
				//������������������������������������������������������������������
				If aParam[14] == 2
					cQuery +=		"SF4.F4_DUPLIC='S' AND "
				ElseIf aParam[14] == 3                   
					cQuery +=		"SF4.F4_DUPLIC='N' AND "
				Endif
				cQuery +=		"SF4.D_E_L_E_T_<>'*' "
				cQuery += "UNION ALL "
				cQuery += "SELECT C5_NUM PEDIDO,"
				cQuery += 		  "C5_EMISSAO EMISSAO,"
				//*************************************
				// Monta campos do ponro de entrada   * 
				// FC010PEDI na tabela temporaria.    *
				//*************************************
				IF lFC010Pedi
					aEval(aRetAux[2] , {|x| cQuery += x[1] + "," })
				EndIf
				cQuery += 		  "C5_MOEDA MOEDA,"
				cQuery +=  		  "C5_FRETE FRETE,"
				cQuery += 		  "SC5.R_E_C_N_O_ SC5RECNO,"					
				cQuery += 		  "C9_QTDLIB QTDVEN,"
				cQuery +=		  "C9_PRCVEN PRCVEN, "
				cQuery +=         "2 TIPO,"										
				cQuery +=		  "C9_DATALIB DATALIB, "
				cQuery +=		  "C9_BLCRED BLCRED "
				cQuery += "FROM "+RetSqlName("SC5")+" SC5,"+RetSqlName("SC6")+" SC6,"
				cQuery +=         RetSqlName("SF4")+" SF4,"+RetSqlName("SC9")+" SC9 "
				cQuery += "WHERE SC5.C5_FILIAL='"+xFilial("SC5")+"' AND "
				cQuery += 		"SC5.C5_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=		"SC5.C5_LOJACLI='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=		"SC5.C5_TIPO NOT IN('D','B') AND "
				cQuery +=		"SC5.C5_EMISSAO >='"+Dtos(aParam[1])+"' AND "
				cQuery +=		"SC5.C5_EMISSAO <='"+Dtos(aParam[2])+"' AND "						
				cQuery +=		"SC5.D_E_L_E_T_<>'*' AND "
				cQuery +=		"SC6.C6_FILIAL='"+xFilial("SC6")+"' AND "
				cQuery +=		"SC6.C6_NUM=SC5.C5_NUM AND "
				cQuery +=		"SC6.D_E_L_E_T_<>'*' AND "
				cQuery +=		"SC6.C6_BLQ NOT IN('R ') AND "
				If aParam[10] == 2 // nao considera pedidos com bloqueio
					cQuery +=		"SC6.C6_BLQ NOT IN('S ') AND "
				Endif
				cQuery +=		"SF4.F4_FILIAL='"+xFilial("SF4")+"' AND "
				cQuery +=		"SF4.F4_CODIGO=SC6.C6_TES AND "
				//����������������������������������������������������������������Ŀ
				//� Considera sim, n�o ou ambos os itens com TES gerando duplicata �
				//������������������������������������������������������������������
				If aParam[14] == 2
					cQuery +=		"SF4.F4_DUPLIC='S' AND "
				ElseIf aParam[14] == 3                   
					cQuery +=		"SF4.F4_DUPLIC='N' AND "
				Endif
				cQuery +=		"SF4.D_E_L_E_T_<>'*' AND "
				cQuery += 		"SC9.C9_FILIAL='"+xFilial("SC9")+"' AND "
				cQuery +=		"SC9.C9_PEDIDO=SC5.C5_NUM AND "
				cQuery +=		"SC9.C9_ITEM=SC6.C6_ITEM AND "
				cQuery +=		"SC9.C9_PRODUTO=SC6.C6_PRODUTO AND "		
				cQuery +=		"SC9.C9_NFISCAL='"+Space(Len(SC9->C9_NFISCAL))+"' AND "
				cQuery +=		"SC9.D_E_L_E_T_<>'*'"

				//������������������������������������������������������������������Ŀ
				//� Ponto de entrada para alteracao da cQuery na consulta de Pedidos �
				//��������������������������������������������������������������������
				If (ExistBlock("F010CQPE"))    
					cQuery := ExecBlock("F010CQPE",.F.,.F.,{cQuery})
				Endif 

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"A"

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)

				TcSetField(cQry,"EMISSAO","D")
				TcSetField(cQry,"DATALIB","D")
				TcSetField(cQry,"TIPO","N",1)
				TcSetField(cQry,"SC5RECNO","N",12,0) 
				TcSetField(cQry,"QTDVEN","N",TamSx3("C6_QTDVEN")[1],TamSx3("C6_QTDVEN")[2])
				TcSetField(cQry,"PRCVEN","N",TamSx3("C6_PRCVEN")[1],TamSx3("C9_PRCVEN")[2]) 

				//*************************************
				// Monta campos do ponro de entrada   * 
				// FC010PEDI na tabela temporaria.    *
				//*************************************
				IF lFC010Pedi
					aEval(aRetAux[2] , {|x| TcSetField(cQry,x[1],x[2],x[3],x[4]) })
				EndIf

				dbSelectArea(cQry)
				bWhile := {|| !Eof() }
				bFiltro:= {|| .T. }
				While ( Eval(bWhile) )				
					If ( Eval(bFiltro) )
						dbSelectArea(cAlias)
						dbSetOrder(1)
						cChave := (cQry)->(PEDIDO)
						If ( !dbSeek(cChave) )
							RecLock(cAlias,.T.)
						Else
							RecLock(cAlias,.F.)
						EndIf
						If ( (cQry)->TIPO == 1 )
							(cAlias)->C5_NUM     := (cQry)->PEDIDO
							(cAlias)->C5_EMISSAO := (cQry)->EMISSAO
							(cAlias)->XX_SLDTOT  += xMoeda((cQry)->QTDVEN*(cQry)->PRCVEN,(cQry)->MOEDA,1,(cQry)->EMISSAO)
							IF EMPTY(cSaldo) .or. cSaldo <> cChave
								(cAlias)->XX_SLDTOT  += xMoeda((cQry)->FRETE,(cQry)->MOEDA,1,(cQry)->EMISSAO)	// Adiciona frete ao total
							ENDIF	
							(cAlias)->XX_SLDLIB  := 0
							(cAlias)->XX_SLDPED  := (cAlias)->XX_SLDTOT
						Else
							(cAlias)->C5_NUM     := (cQry)->PEDIDO
							(cAlias)->C5_EMISSAO := (cQry)->EMISSAO						
							If ( Empty((cQry)->BLCRED) )
								(cAlias)->XX_SLDLIB  += xMoeda((cQry)->QTDVEN*(cQry)->PRCVEN,(cQry)->MOEDA,1,(cQry)->DATALIB)
								IF EMPTY(cSaldo) .or. cSaldo <> cChave
									(cAlias)->XX_SLDLIB  += xMoeda((cQry)->FRETE,(cQry)->MOEDA,1,(cQry)->EMISSAO)  // Adiciona frete ao total
								ENDIF
								(cAlias)->XX_SLDTOT  += xMoeda((cQry)->QTDVEN*(cQry)->PRCVEN,(cQry)->MOEDA,1,(cQry)->DATALIB)
								IF EMPTY(cSaldo) .or. cSaldo <> cChave
									(cAlias)->XX_SLDTOT  += xMoeda((cQry)->FRETE,(cQry)->MOEDA,1,(cQry)->EMISSAO)  // Adiciona frete ao total
								ENDIF
							Else
								(cAlias)->XX_SLDTOT  += xMoeda((cQry)->QTDVEN*(cQry)->PRCVEN,(cQry)->MOEDA,1,(cQry)->DATALIB)
								IF EMPTY(cSaldo) .or. cSaldo <> cChave
									(cAlias)->XX_SLDTOT  += xMoeda((cQry)->FRETE,(cQry)->MOEDA,1,(cQry)->EMISSAO)  // Adiciona frete ao total
								ENDIF	
								(cAlias)->XX_SLDPED  += xMoeda((cQry)->QTDVEN*(cQry)->PRCVEN,(cQry)->MOEDA,1,(cQry)->DATALIB)           
								IF Empty (cSaldo) .or. cSaldo <> cChave          
									(cAlias)->XX_SLDPED  += xMoeda((cQry)->FRETE,(cQry)->MOEDA,1,(cQry)->EMISSAO)  // Adiciona frete ao total
								EndIf
							EndIf
						EndIf
						(cAlias)->XX_RECNO := (cQry)->SC5RECNO
						//*************************************
						// Monta campos do ponro de entrada   * 
						// FC010PEDI na tabela temporaria.    *
						//*************************************
						IF lFC010Pedi
							aEval(aRetAux[2] , {|x| (cAlias)->&(x[1]) := (cQry)->&(x[1])  })
						EndIf
						(cAlias)->(MsUnLock())
						cSaldo := cChave
					EndIf
					dbSelectArea(cQry)
					dbSkip()				
				EndDo
				dbSelectArea(cQry)
				dbCloseArea()			
			Else
				#ENDIF
				dbSelectArea("SC5")
				dbSetOrder(3)
				dbSeek(xFilial("SC5")+SA1->A1_COD)
				While ( !Eof() .And. SC5->C5_FILIAL==xFilial("SC5") .And.;
				SC5->C5_CLIENTE == SA1->A1_COD )
					nSalPed := 0
					nSalPedb:= 0
					nSalPedL:= 0
					nQtdPed := 0											
					If ( If(aParam[13] == 1,SC5->C5_LOJACLI == SA1->A1_LOJA,.T.) .And. !(SC5->C5_TIPO $ "DB") .And. SC5->C5_EMISSAO >= aParam[1] .And. C5_EMISSAO <= aParam[2] )
						dbSelectArea("SC6")
						dbSetOrder(1)
						dbSeek(xFilial("SC6")+SC5->C5_NUM)
						While ( !Eof() .And. SC6->C6_FILIAL == xFilial('SC5') .And.;
						SC6->C6_NUM == SC5->C5_NUM )
							If ( !AllTrim(SC6->C6_BLQ) $ "R"+If(aParam[10]==2,"#S",""))
								dbSelectArea("SF4")
								dbSetOrder(1)
								dbSeek(cFilial+SC6->C6_TES)
								//�����������������������������������������������������������Ŀ
								//� Buscar Qtde no arquivo SC9 (itens liberados) p/ A1_SALPEDL�
								//�������������������������������������������������������������
								dbSelectArea("SC9")
								dbSetOrder(2)
								dbSeek(xFilial("SC9")+SC6->C6_CLI+SC6->C6_LOJA+SC6->C6_NUM+SC6->C6_ITEM)

								//����������������������������������������������������������������Ŀ
								//� Considera sim, n�o ou ambos os itens com TES gerando duplicata �
								//������������������������������������������������������������������
								If aParam[14] == 2
									bCond := { || SF4->F4_DUPLIC == "S" }
								ElseIf aParam[14] == 3
									bCond := { || SF4->F4_DUPLIC == "N" }
								Else
									bCond := { || .T.}
								Endif

								If Eval(bCond)
									While ( !Eof() .And. xFilial("SC9") == SC9->C9_FILIAL .And.;
									SC6->C6_CLI == SC9->C9_CLIENTE .And.;
									SC6->C6_LOJA == SC9->C9_LOJA .And.;
									SC6->C6_NUM == SC9->C9_PEDIDO .And.;
									SC6->C6_ITEM == SC9->C9_ITEM )
										If ( Empty(C9_NFISCAL) .And. SC6->C6_PRODUTO==SC9->C9_PRODUTO )
											If ( Empty(SC9->C9_BLCRED) )
												nSalpedl += xMoeda( SC9->C9_QTDLIB * SC9->C9_PRCVEN , SC5->C5_MOEDA , 1 , SC9->C9_DATALIB )
												nSalpedl += xMoeda( SC5->C5_FRETE , SC5->C5_MOEDA , 1 , SC9->C9_DATALIB )
											Else
												nSalpedb += xMoeda( SC9->C9_QTDLIB * SC9->C9_PRCVEN , SC5->C5_MOEDA , 1 , SC9->C9_DATALIB )
												nSalpedb += xMoeda( SC5->C5_FRETE , SC5->C5_MOEDA , 1 , SC9->C9_DATALIB )
											EndIf
										EndIf
										dbSelectArea("SC9")
										dbSkip()
									EndDo
								Endif
								If Eval(bCond)
									nQtdPed := SC6->C6_QTDVEN - SC6->C6_QTDEMP - SC6->C6_QTDENT
									nQtdPed := IIf( nQtdPed < 0 , 0 , nQtdPed )
									nSalped += xMoeda( nQtdPed * SC6->C6_PRCVEN , SC5->C5_MOEDA , 1 , SC5->C5_EMISSAO )
									nSalped += xMoeda( SC5->C5_FRETE , SC5->C5_MOEDA , 1 , SC5->C5_EMISSAO )
								EndIf
							EndIf
							dbSelectArea("SC6")
							dbSkip()
						EndDo
					EndIf
					If ( nSalped+nSalpedl+nSalpedb > 0 )
						RecLock(cAlias,.T.)
						(cAlias)->C5_NUM     := SC5->C5_NUM
						(cAlias)->C5_EMISSAO := SC5->C5_EMISSAO
						(cAlias)->XX_SLDTOT  := nSalPed+nSalPedL+nSalPedb
						(cAlias)->XX_SLDLIB  := nSalPedL
						(cAlias)->XX_SLDPED  := nSalPed+nSalPedb
						(cAlias)->XX_RECNO    := SC5->(RecNo())
						MsUnlock()
					EndIf
					dbSelectArea("SC5")
					dbSkip()
				EndDo
				#IFDEF TOP
			EndIf
			#ENDIF
		EndIf
		dbSelectArea(cAlias)
		dbGotop()
		aGet[1] := 0
		aGet[2] := 0
		aGet[3] := 0
		aGet[4] := 0
		dbEval({|| 	aGet[1]++,;
		aGet[2]+=XX_SLDTOT,;
		aGet[3]+=XX_SLDLIB,;
		aGet[4]+=XX_SLDPED})

		aGet[1] := TransForm(aGet[1],Tm(aGet[1],16,0))
		aGet[2] := TransForm(aGet[2],Tm(aGet[2],16,nCayas))
		aGet[3] := TransForm(aGet[3],Tm(aGet[3],16,nCayas))
		aGet[4] := TransForm(aGet[4],Tm(aGet[3],16,nCayas))	
		//������������������������������������������������������������������������Ŀ
		//�Notas Fiscais                                                           �
		//��������������������������������������������������������������������������
		Case ( nBrowse == 4 )
		aCpos:={"F2_SERIE","F2_DOC","F2_EMISSAO","F2_DUPL","F2_VALFAT","F2_FRETE",;
		"F2_HORA","F2_TRANSP","A4_NREDUZ"}
		If cPaisLoc != "BRA"
			AAdd(aCpos,"F2_MOEDA") 
			AAdd(aCpos,"F2_TXMOEDA")
		EndIf

		If lFC0101FAT
			aAuxCpo := aClone(ExecBlock("FC0101FAT",.F.,.F.,{aCpos}))
			If ValType(aAuxCpo) == "A"
				aCpos := aClone(aAuxCpo)			
			EndIf
		EndIf

		dbSelectArea("SX3")
		dbSetOrder(2)
		For nI := 1 To Len(aCpos)
			dbSeek(aCpos[nI])
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Next nI

		aadd(aStru,{"XX_RECNO","N",12,0})

		SX3->(dbSetOrder(1))

		If ( Select(cAlias) ==	0 )
			cArquivo := CriaTrab(,.F.)			
			aadd(aAlias,{ cAlias , cArquivo })
			aadd(aStru,{"FLAG","L",01,0})
			dbCreate(cArquivo,aStru)
			dbUseArea(.T.,,cArquivo,cAlias,.F.,.F.)
			IndRegua(cAlias,cArquivo,"DTOS(F2_EMISSAO)+F2_SERIE+F2_DOC")

			#IFDEF TOP
			cDbMs	 := UPPER(TcGetDb())
			If ( TcSrvType()!="AS/400" .and. cDbMs!="POSTGRES")
				lQuery := .T.

				cQuery := "SELECT SF2.F2_DOC F2_DOC,"
				cQuery += 		"  SF2.F2_SERIE F2_SERIE,"
				cQuery += 		"  SF2.F2_EMISSAO F2_EMISSAO,"
				cQuery +=		"  SF2.F2_DUPL F2_DUPL,"
				cQuery += 		"  SF2.F2_VALFAT F2_VALFAT, "
				cQuery += 		"  SF2.F2_FRETE F2_FRETE, "
				cQuery += 		"  SF2.F2_HORA F2_HORA, "
				cQuery += 		"  SF2.F2_TRANSP F2_TRANSP, "
				If cPaisLoc <> "BRA"                        
					cQuery +=	"  SF2.F2_MOEDA F2_MOEDA, "
					cQuery +=	"  SF2.F2_TXMOEDA F2_TXMOEDA, "
				EndIf
				cQuery += 		"  SA4.A4_COD A4_COD, "
				cQuery += 		"  SA4.A4_NREDUZ A4_NREDUZ, "
				cQuery += 		"  SF2.R_E_C_N_O_ SF2RECNO "

				If lFC0102FAT
					cQuery += ", " + ExecBlock("FC0102FAT",.F.,.F.) + " "					
				EndIf

				cQuery += "FROM "+RetSqlName("SF2")+" SF2 "
				If	cDbMs $ "DB2/MYSQL" .or. "MSSQL"    $ cDbMs .or.;
				cDbMs $ "DB2/400" .or. "INFORMIX" $ cDbMs
					cQuery += " LEFT OUTER JOIN "+ RetSqlName("SA4") + " SA4 "
					cQuery += " ON ( SF2.F2_TRANSP = SA4.A4_COD ) "
					cQuery += " AND SA4.A4_FILIAL = '"+xFilial("SA4")+"' "
					cQuery += " AND SA4.D_E_L_E_T_ = ' '"	
				Else
					cQuery += ", "+ RetSqlName("SA4") + " SA4 "
				Endif
				cQuery += " WHERE SF2.F2_FILIAL='"+xFilial("SF2")+"' AND "
				cQuery += 		" SF2.F2_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=		" SF2.F2_LOJA='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=		" SF2.F2_TIPO NOT IN('D','B') AND "
				cQuery +=		" SF2.F2_EMISSAO>='"+DTOS(aParam[1])+"' AND "
				cQuery +=		" SF2.F2_EMISSAO<='"+DTOS(aParam[2])+"' AND "

				//�����������������������������������������������������Ŀ
				//� Ponto de entrada para filtrar pelo MSFIL em caso de �
				//� arquivo compartilhado.                              �
				//�������������������������������������������������������

				If (ExistBlock("FO10FLFT"))    
					cQuery += ExecBlock("FO10FLFT",.F.,.F.)
				Endif                    

				If aParam[14] == 3 // TES Duplic = N
					cQuery +=		" SF2.F2_VALFAT = 0 AND "					
				ElseIf aParam[14] == 2  // TES Duplic = S
					cQuery +=		" SF2.F2_VALFAT > 0 AND "					
				Endif												

				cQuery += " SF2.D_E_L_E_T_<>'*'"

				If cDbMs == "ORACLE"
					cQuery += " AND SF2.F2_TRANSP = SA4.A4_COD(+) "
					cQuery += " AND SA4.A4_FILIAL(+) = '"+xFilial("SA4")+"' "
					cQuery += " AND SA4.D_E_L_E_T_(+) = ' '"	

				ElseIf	!(cDbMs $ "DB2/MYSQL") .and. !("MSSQL"    $ cDbMs) .and.;
				!(cDbMs $ "DB2/400") .and. !("INFORMIX" $ cDbMs)
					cQuery += " AND SF2.F2_TRANSP = SA4.A4_COD "
					cQuery += " AND SA4.A4_FILIAL = '"+xFilial("SA4")+"' "
					cQuery += " AND SA4.D_E_L_E_T_ = ' '"
				Endif

				//�������������������������������������������������������������������Ŀ
				//� Ponto de entrada para alteracao da cQuery na consulta Faturamento �
				//���������������������������������������������������������������������
				If (ExistBlock("F010CQFT"))
					cQuery := ExecBlock("F010CQFT",.F.,.F.,{cQuery})
				Endif

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"A"

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)

				TcSetField(cQry,"F2_EMISSAO","D")
				TcSetField(cQry,"F2_VALFAT","N",TamSx3("F2_VALFAT")[1],TamSx3("F2_VALFAT")[2])
				TcSetField(cQry,"F2_FRETE","N",TamSx3("F2_FRETE")[1],TamSx3("F2_FRETE")[2])
				TcSetField(cQry,"SF2RECNO","N",12,0)
				If cPaisLoc <> "BRA"
					TcSetField(cQry,"F2_MOEDA","N",TamSx3("F2_MOEDA")[1],TamSx3("F2_MOEDA")[2])
					TcSetField(cQry,"F2_TXMOEDA","N",TamSx3("F2_TXMOEDA")[1],TamSx3("F2_TXMOEDA")[2])
				EndIf                                                                                
			Else
				#ENDIF
				cQry := "SF2"

				#IFDEF TOP
			EndIf
			#ENDIF
			aGet[1] := 0
			aGet[2] := 0
			dbSelectArea(cQry)
			If ( !lQuery )
				dbSetOrder(2)
				If aParam[13] == 1  //Considera loja
					dbSeek(xFilial("SF2")+SA1->A1_COD+SA1->A1_LOJA)
					bWhile := {|| !Eof() .And. xFilial("SF2") == SF2->F2_FILIAL .And.;
					SA1->A1_COD == SF2->F2_CLIENTE .And.;
					SA1->A1_LOJA == SF2->F2_LOJA }
				Else                                
					dbSeek(xFilial("SF2")+SA1->A1_COD)
					bWhile := {|| !Eof() .And. xFilial("SF2") == SF2->F2_FILIAL .And.;
					SA1->A1_COD == SF2->F2_CLIENTE}
				Endif
				If aParam[14] == 3 // TES Duplic = N
					bFiltro:= {|| !SF2->F2_TIPO$"DB" .And.;
					SF2->F2_EMISSAO >= aParam[1]     .And.;
					SF2->F2_EMISSAO <= aParam[2]	 .And.;
					SF2->F2_VALFAT  =  0}
				ElseIf aParam[14] == 2  // TES Duplic = S
					bFiltro:= {|| !SF2->F2_TIPO$"DB" .And.;
					SF2->F2_EMISSAO >= aParam[1] .And.;
					SF2->F2_EMISSAO <= aParam[2] .And.;
					SF2->F2_VALFAT  >  0}
				Else // TES Duplic = Todas
					bFiltro:= {|| !SF2->F2_TIPO$"DB" .And.;
					SF2->F2_EMISSAO >= aParam[1] .And.;
					SF2->F2_EMISSAO <= aParam[2]}				
				Endif											
			Else
				bWhile := {|| !Eof() }
				bFiltro:= {|| .T. }
			EndIf			
			While ( Eval(bWhile) )				
				If ( Eval(bFiltro) )
					If !lQuery
						// Se nao for query, posiciona SA4 para obter o nome da transportadora
						SA4->(MsSeek(xFilial("SA4")+SF2->F2_TRANSP))
					Endif
					RecLock(cAlias,.T.)
					(cAlias)->F2_SERIE   := (cQry)->F2_SERIE
					(cAlias)->F2_DOC     := (cQry)->F2_DOC
					(cAlias)->F2_EMISSAO := (cQry)->F2_EMISSAO
					(cAlias)->F2_DUPL    := (cQry)->F2_DUPL
					(cAlias)->F2_VALFAT  := (cQry)->F2_VALFAT
					(cAlias)->F2_FRETE   := (cQry)->F2_FRETE
					(cAlias)->F2_HORA    := (cQry)->F2_HORA
					(cAlias)->F2_TRANSP  := (cQry)->F2_TRANSP
					If cPaisLoc != "BRA"                      
						(cAlias)->F2_MOEDA    := (cQry)->F2_MOEDA
						(cAlias)->F2_TXMOEDA  := (cQry)->F2_TXMOEDA
					EndIf
					(cAlias)->A4_NREDUZ  := If(lQuery,(cQry)->A4_NREDUZ,SA4->A4_NREDUZ)
					(cAlias)->XX_RECNO   := If(lQuery,(cQry)->SF2RECNO,SF2->(RecNo()))
					(cAlias)->(MsUnLock())
					If lFC0103FAT
						ExecBlock("FC0103FAT",.F.,.F.,{cAlias,cQry})					
					EndIf
				EndIf
				dbSelectArea(cQry)
				dbSkip()				
			EndDo
			If ( lQuery )
				dbSelectArea(cQry)
				dbCloseArea()
			EndIf
		EndIf		
		aGet[1] := 0
		aGet[2] := 0
		dbSelectArea(cAlias)
		dbGotop()
		If cPaisLoc == "BRA"
			dbEval({|| aGet[1]++,aGet[2]+=F2_VALFAT})
		Else 
			dbEval({|| aGet[1]++,aGet[2]+=Iif(F2_MOEDA == 1,F2_VALFAT,xMoeda(F2_VALFAT,F2_MOEDA,1,F2_EMISSAO,MsDecimais(1),F2_TXMOEDA))})			
		EndIf
		aGet[1] := TransForm(aGet[1],Tm(aGet[1],16,0))
		aGet[2] := TransForm(aGet[2],Tm(aGet[2],16,nCayas))			

		//CHEQUES
		Case (nBrowse == 5)
		DEFINE MSDIALOG oCheq FROM   15,1 TO 170,272 TITLE STR0072 PIXEL //"Seleccion de parametros"
		@ 6 , 12 TO 65, 93 OF oCheq   LABEL STR0073 PIXEL //"Tipos de cheques a exhibir"

		If cPaisLoc$"ARG|COS"
			@ 13, 13 RADIO oTipo VAR nTipo ;
			PROMPT STR0062,STR0064,If(cPaisLoc="ARG",STR0088,STR0063),"Transito",STR0074 ;  //"Pendientes"###"Cobrados"###"Negociados"###"Transito"###"Todos"
			OF oCheq PIXEL SIZE 75,12
		Else
			@ 13, 13 RADIO oTipo VAR nTipo ;
			PROMPT STR0062,STR0064,If(cPaisLoc$"URU|BOL",STR0088,STR0063),STR0074 ;  //"Pendientes"###"Cobrados"###"Negociados"###"Todos"
			OF oCheq PIXEL SIZE 75,12
		Endif

		DEFINE SBUTTON FROM 45, 100 TYPE 1 ACTION oCheq:End() ENABLE OF oCheq //11,132
		ACTIVATE MSDIALOG oCheq CENTERED

		Do case
			Case nTipo 	== 	1
			bTipo	:=	{ || (cQry)->E1_SALDO > 0 }
			Case nTipo	==	2
			bTipo	:=	{ || !((cQry)->E1_SITUACA $ " 0FG") .And. (cQry)->E1_SALDO == 0 }
			Case nTipo	==	3
			bTipo	:=	{ || (cQry)->E1_STATUS == "R" .And. (cQry)->E1_SITUACA $ " 0FG" .And. (cQry)->E1_SALDO == 0}
			Case nTipo	==	4
			If cPaisLoc $ "ARG/COS"			
				bTipo	:=	{ || (cQry)->EL_TIPODOC $ cCheques .And. (cQry)->EL_TRANSIT == '1' .And. (cQry)->EL_DTENTRA == CTOD("//")}
			Else
				bTipo	:=	{ || .T. }
			Endif
			Case nTipo == 5	//Apenas ARG e COS
			bTipo	:=	{ || .T. }

		EndCase

		nMoeda := 1
		dbSelectArea("SX3")
		dbSetOrder(2)
		dbSeek("E1_STATUS")
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aadd(aHeader,{AllTrim(X3TITULO()),"XX_ESTADO","@!",04,0,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru,{"XX_ESTADO","C",04,0})

		dbSeek("E1_PREFIXO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_NUM")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_PARCELA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_NUMNOTA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_EMISSAO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_VALOR")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_SALDO")
		aadd(aHeader,{ AllTrim(X3TITULO()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_MOEDA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_VLCRUZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_VENCREA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_NATUREZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_PORTADO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_BCOCHQ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_HIST")
		aadd(aHeader,{ AllTrim(X3TITULO()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_SITUACA")
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aHeader,{STR0045,"X5_DESCRI","@!",25,0,"","","C","SX5","" } ) //"Situacao"
		aadd(aStru,{"X5_DESCRI","C",25,0})

		aadd(aStru,{"XX_RECNO" ,"N",12,0})
		aadd(aStru,{"XX_VALOR" ,"N",18,0})


		SX3->(dbSetOrder(1))

		cArquivo := CriaTrab(,.F.)			
		If !lExibe
			aadd(aAlias,{ cAlias , cArquivo })
		Endif
		aadd(aStru,{"FLAG","L",01,0})
		dbCreate(cArquivo,aStru)
		dbUseArea(.T.,,cArquivo,cAlias,.F.,.F.)
		IndRegua(cAlias,cArquivo,"E1_PREFIXO+E1_NUM+E1_PARCELA")

		lNoChqTran := IIF(cPaisLoc $ "ARG|COS", (nTipo != 4) , .T.)
		If lnoChqTran

			#IFDEF TOP
			If ( TcSrvType()!="AS/400" )
				lQuery := .T.
				cQuery := ""
				aEval(aQuery,{|x| cQuery += ","+AllTrim(x[1])})
				cQuery := "SELECT "+SubStr(cQuery,2)
				cQuery +=         ",SE1.R_E_C_N_O_ SE1RECNO"
				cQuery +=         ",SX5.X5_DESCRI "
				cQuery += "FROM "+RetSqlName("SE1")+" SE1,"
				cQuery +=         RetSqlName("SX5")+" SX5 "
				cQuery += "WHERE SE1.E1_FILIAL='"+xFilial("SE1")+"' AND "
				cQuery +=      "SE1.E1_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=      "SE1.E1_LOJA='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=      "SE1.E1_EMISSAO>='"+Dtos(aParam[1])+"' AND "
				cQuery +=      "SE1.E1_EMISSAO<='"+Dtos(aParam[2])+"' AND "
				cQuery +=      "SE1.E1_VENCREA>='"+Dtos(aParam[3])+"' AND "
				cQuery +=		"SE1.E1_VENCREA<='"+Dtos(aParam[4])+"' AND "
				cQuery +=		"SE1.E1_TIPO IN" + FormatIn(cCheques,"|") + " AND "
				cQuery +=      "SE1.E1_PREFIXO>='"+aParam[6]+"' AND "
				cQuery +=      "SE1.E1_PREFIXO<='"+aParam[7]+"' AND "
				cQuery +=		"SE1.D_E_L_E_T_<>'*' AND "
				cQuery +=      "SX5.X5_FILIAL='"+xFilial("SX5")+"' AND "
				cQuery +=		"SX5.X5_TABELA='07' AND "
				cQuery +=		"SX5.X5_CHAVE=SE1.E1_SITUACA AND "
				cQuery +=		"SX5.D_E_L_E_T_<>'*' "   
				cQuery +=		" ORDER BY SE1.E1_EMISSAO  "                                

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"A"

				MsAguarde({ || dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)},STR0065) //"Seleccionado registros en el servidor"

				aEval(aQuery,{|x| If(x[2]!="C",TcSetField(cQry,x[1],x[2],x[3],x[4]),Nil)})
			Else
				#ENDIF
				cQry := "SE1"
				#IFDEF TOP
			EndIf
			#ENDIF

			dbSelectArea(cQry)
			If ( !lQuery )
				dbSetOrder(2)

				If aParam[13] == 1  //Considera loja
					dbSeek(xFilial("SE1")+SA1->A1_COD+SA1->A1_LOJA)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE .And.;
					SA1->A1_LOJA   == SE1->E1_LOJA }
				Else
					dbSeek(xFilial("SE1")+SA1->A1_COD)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE}			
				Endif
				bFiltro:= {|| 	SE1->E1_EMISSAO >= aParam[1] .And.;
				SE1->E1_EMISSAO <= aParam[2] .And.;
				SE1->E1_VENCREA >= aParam[3] .And.;
				SE1->E1_VENCREA <= aParam[4] .And.;
				SE1->E1_TIPO	 $ cCheques .And.;
				SE1->E1_PREFIXO >= aParam[6] .And.;
				SE1->E1_PREFIXO <= aParam[7]}
			Else
				bWhile := {|| !Eof() }
				bFiltro:= {|| .T. }
			EndIf			

			While ( Eval(bWhile) )				
				If ( Eval(bFiltro) ) .And. Eval(bTipo)
					If ( !lQuery )
						dbSelectArea("SX5")
						dbSetOrder(1)
						MsSeek(xFilial("SX5")+"07"+SE1->E1_SITUACA)
					EndIf
					dbSelectArea(cAlias)
					dbSetOrder(1)
					cChave := (cQry)->(E1_PREFIXO)+(cQry)->(E1_NUM)+(cQry)->(E1_PARCELA)
					If ( !dbSeek(cChave) )
						RecLock(cAlias,.T.)						
					Else
						RecLock(cAlias,.F.)
					EndIf
					For nCntFor := 1 To Len(aStru)
						Do Case
							Case ( AllTrim(aStru[nCntFor][1])=="X5_DESCRI" )
							If ( lQuery )
								(cAlias)->X5_DESCRI := (cQry)->X5_DESCRI
							Else
								(cAlias)->X5_DESCRI := SX5->X5_DESCRI
							EndIf
							Case ( AllTrim(aStru[nCntFor][1])=="XX_ESTADO" )
							If lQuery
								If (cQry)->E1_SALDO > 0
									(cAlias)->XX_ESTADO := STR0066 //"PEND"
								ElseIf (cQry)->E1_STATUS == "R" .And. (cQry)->E1_SITUACA $ "0FG"
									(cAlias)->XX_ESTADO := If(cPaisLoc$"URU|BOL|ARG",STR0089,STR0067) //"NEGO"
								Else									
									(cAlias)->XX_ESTADO := STR0068 //"COBR"
								Endif
							Else
								If SE1->E1_SALDO > 0
									(cAlias)->XX_ESTADO := STR0066 //"PEND"
								ElseIf SE1->E1_STATUS == "R" .And. SE1->E1_SITUACA $ "0FG"
									(cAlias)->XX_ESTADO := IIf(cPaisLoc$"URU|BOL|ARG",STR0089,STR0067) //"NEGO"
								Else									
									(cAlias)->XX_ESTADO := STR0068 //"COBR"
								Endif
							Endif
							Case ( AllTrim(aStru[nCntFor][1])=="XX_RECNO" )
							If ( lQuery )
								(cAlias)->XX_RECNO := (cQry)->SE1RECNO
							Else
								(cAlias)->XX_RECNO := SE1->(RecNo())
							EndIf
							Case ( AllTrim(aStru[nCntFor][1])=="XX_VALOR" )
							If ( lQuery )
								(cAlias)->XX_VALOR := xMoeda((cQry)->E1_VALOR,(cQry)->E1_MOEDA,nMoeda,,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0))
							Else
								(cAlias)->XX_VALOR := xMoeda(SE1->E1_VALOR,SE1->E1_MOEDA,nMoeda,,,If(cPaisLoc=="BRA",SE1->E1_TXMOEDA,0))
							EndIf
							Case ( AllTrim(aStru[nCntFor][1])=="FLAG" )

							OtherWise							
							(cAlias)->(FieldPut(nCntFor,(cQry)->(FieldGet(FieldPos(aStru[nCntFor][1])))))
						EndCase
					Next nCntFor
					dbSelectArea(cAlias)
					MsUnLock()
				EndIf
				dbSelectArea(cQry)
				dbSkip()				
			EndDo
		Endif


		//Cheques em transito
		If cPaisLoc $ "ARG/COS" .and. (nTipo == 4 .or. nTipo == 5)

			#IFDEF TOP
			If ( TcSrvType()!="AS/400" )

				aQuery := SEL->(dbStruct())				

				lQuery := .T.
				cQuery := "SELECT "
				cQuery += "EL_PREFIXO,EL_NUMERO,EL_PARCELA,EL_EMISSAO,EL_VALOR,EL_VALOR,EL_MOEDA,EL_VLMOED1,"
				cQuery += "EL_DTVCTO,EL_NATUREZ,EL_BANCO,EL_BCOCHQ,El_TXMOEDA,EL_TIPODOC,EL_TRANSIT,EL_DTENTRA,"
				cQuery += "SEL.R_E_C_N_O_ SELRECNO "
				cQuery += "FROM "+RetSqlName("SEL")+" SEL "
				cQuery += "WHERE SEL.EL_FILIAL='"+xFilial("SEL")+"' AND "
				cQuery += "SEL.EL_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=      "SEL.EL_LOJA='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=  "SEL.EL_EMISSAO >= '"+Dtos(aParam[1])+"' AND "
				cQuery +=  "SEL.EL_EMISSAO <= '"+Dtos(aParam[2])+"' AND "
				cQuery +=  "SEL.EL_DTVCTO  >= '"+Dtos(aParam[3])+"' AND "
				cQuery +=  "SEL.EL_DTVCTO  <= '"+Dtos(aParam[4])+"' AND "
				cQuery +=  "SEL.EL_TIPODOC IN " + FormatIn(cCheques,"|") + " AND "
				cQuery +=  "SEL.EL_PREFIXO >= '"+aParam[6]+"' AND "
				cQuery +=  "SEL.EL_PREFIXO <= '"+aParam[7]+"' AND "
				cQuery +=  "SEL.D_E_L_E_T_ = ' ' AND "
				cQuery +=  "SEL.EL_TRANSIT = '1' AND "
				cQuery +=  "SEL.EL_DTENTRA = ' ' "	
				cQuery +=  "ORDER BY SEL.EL_EMISSAO "			

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"B"

				MsAguarde({ || dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)},STR0065) //"Seleccionado registros en el servidor"

				aEval(aQuery,{|x| If(x[2]!="C",TcSetField(cQry,x[1],x[2],x[3],x[4]),Nil)})
			Else
				#ENDIF
				cQry := "SEL"
				#IFDEF TOP
			EndIf
			#ENDIF

			dbSelectArea(cQry)

			If ( !lQuery )
				SEL->(dbSetOrder(7))

				If aParam[13] == 1  //Considera loja
					dbSeek(xFilial("SEL")+SA1->A1_COD+SA1->A1_LOJA)
					bWhile := {|| !Eof() .And. xFilial("SEL") == SEL->EL_FILIAL .And.;
					SA1->A1_COD    == SEL->EL_CLIENTE .And.;
					SA1->A1_LOJA   == SEL->EL_LOJA }
				Else
					dbSeek(xFilial("SEL")+SA1->A1_COD)
					bWhile := {|| !Eof() .And. xFilial("SEL") == SEL->EL_FILIAL .And.;
					SA1->A1_COD    == SE1->EL_CLIENTE}			
				Endif
				bFiltro:= {|| 	SEL->EL_EMISSAO >= aParam[1] .And.;
				SEL->EL_EMISSAO <= aParam[2] .And.;
				SEL->EL_DTVCTO  >= aParam[3] .And.;
				SEL->EL_DTVCTO <= aParam[4] .And.;
				SEL->EL_TIPODOC	 $ cCheques .And.;
				SEL->EL_PREFIXO >= aParam[6] .And.;
				SEL->EL_PREFIXO <= aParam[7] .And.;
				SEL->EL_TRANSIT == '1' }

			Else
				bWhile := {|| !Eof() }
				bFiltro:= {|| .T. }
			EndIf			

			While ( Eval(bWhile) )				
				If ( Eval(bFiltro) ) .And. Eval(bTipo)
					dbSelectArea(cAlias)
					dbSetOrder(1)

					RecLock(cAlias,.T.)						
					(cAlias)->XX_ESTADO  := "TRAN" 
					(cAlias)->E1_PREFIXO := (cQry)->EL_PREFIXO
					(cAlias)->E1_NUM 	 := (cQry)->EL_NUMERO
					(cAlias)->E1_PARCELA := (cQry)->EL_PARCELA
					(cAlias)->E1_NUMNOTA := ""
					(cAlias)->E1_EMISSAO := (cQry)->EL_EMISSAO
					(cAlias)->E1_VALOR   := (cQry)->EL_VALOR
					(cAlias)->E1_SALDO   := (cQry)->EL_VALOR
					(cAlias)->E1_MOEDA   := Val((cQry)->EL_MOEDA)
					(cAlias)->E1_VLCRUZ  := (cQry)->EL_VLMOED1
					(cAlias)->E1_VENCREA := (cQry)->EL_DTVCTO
					(cAlias)->E1_NATUREZ := (cQry)->EL_NATUREZ
					(cAlias)->E1_PORTADO := (cQry)->EL_BANCO
					(cAlias)->E1_BCOCHQ  := (cQry)->EL_BCOCHQ
					(cAlias)->E1_HIST    := "CHQ EM TRANSITO" 

					If ( lQuery )
						(cAlias)->XX_RECNO   := (cQry)->SELRECNO
						(cAlias)->XX_VALOR   := xMoeda((cQry)->EL_VALOR,VAL((cQry)->EL_MOEDA),nMoeda )
					Else
						(cAlias)->XX_RECNO := SEL->(RecNo())
						(cAlias)->XX_VALOR := xMoeda(SEL->EL_VALOR,Val(SEL->EL_MOEDA),nMoeda)
					Endif

					(cAlias)->(MsUnLock())

				EndIf
				dbSelectArea(cQry)
				dbSkip()				
			EndDo
		Endif	


		If ( lQuery )
			dbSelectArea(cQry)
			dbCloseArea()
		EndIf                                              
		//������������������������������������������������������������������������Ŀ
		//�Totais da Consulta                                                      �
		//��������������������������������������������������������������������������
		dbSelectArea(cAlias)
		dbGotop()
		aGet	:=	{0,0,0,0,0,0,0,0}
		While !EOF()
			DO CASE
				CASE XX_ESTADO	==	STR0066 //"PEND"
				aGet[1]	+=	(cAlias)->E1_SALDO
				aGet[4]++
				CASE XX_ESTADO	==	If(cPaisLoc$"URU|BOL|ARG",STR0089,STR0067) //"NEGO"
				aGet[2]	+=	(cAlias)->XX_VALOR
				aGet[5]++
				CASE XX_ESTADO	==	STR0068 //"COBR"
				aGet[3]	+=	(cAlias)->XX_VALOR
				aGet[6]++
				CASE XX_ESTADO	==	"TRAN" //STR0135 //
				aGet[7]	+=	(cAlias)->XX_VALOR
				aGet[8]++

			EndCase				 	
			dbSkip()
		Enddo
		If lExibe
			aGet[1] := TransForm(aGet[1],Tm(aGet[1],16,MsDecimais(nMoeda))) + " (" +Alltrim(STR(aGet[4]))+")"
			aGet[2] := TransForm(aGet[2],Tm(aGet[2],16,MsDecimais(nMoeda))) + " (" +Alltrim(STR(aGet[5]))+")"
			aGet[3] := TransForm(aGet[3],Tm(aGet[3],16,MsDecimais(nMoeda))) + " (" +Alltrim(STR(aGet[6]))+")"
			aGet[4] := TransForm(aGet[7],Tm(aGet[7],16,MsDecimais(nMoeda))) + " (" +Alltrim(STR(aGet[8]))+")"
			aGet[5] := STR0069+GetMv("MV_MOEDA1") //"Valores en "  
			aGet[6] := ""
		EndIf
		Otherwise
		Alert(STR0060)		//N�o Implementado
		lExibe := .f.
	EndCase	
	//������������������������������������������������������������������������Ŀ
	//�Exibe os dados Gerados                                                  �
	//��������������������������������������������������������������������������
	If ( lExibe )
		dbSelectArea(cAlias)
		dbGotop()
		If ( !Eof() )

			aObjects := {} 
			AAdd( aObjects, { 100, 35,  .t., .f., .t. } )
			AAdd( aObjects, { 100, 100 , .t., .t. } )
			AAdd( aObjects, { 100, 50 , .t., .f. } )

			aInfo    := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 3, 3 } 
			aPosObj1 := MsObjSize( aInfo, aObjects) 

			DEFINE FONT oBold    NAME "Arial" SIZE 0, -12 BOLD

			DEFINE MSDIALOG oDlg FROM	aSize[7],0 TO aSize[6],aSize[5] TITLE cCadastro OF oMainWnd PIXEL
			@ aPosObj1[1,1], aPosObj1[1,2] MSPANEL oScrPanel PROMPT "" SIZE aPosObj1[1,3],aPosObj1[1,4] OF oDlg LOWERED

			@ 04,004 SAY OemToAnsi(STR0006) SIZE 025,07          OF oScrPanel PIXEL //"Codigo"
			@ 12,004 SAY SA1->A1_COD  SIZE 060,09  OF oScrPanel PIXEL FONT oBold

			If aParam[13] == 1  //Considera loja		
				@ 04,067 SAY OemToAnsi(STR0007) SIZE 020,07          OF oScrPanel PIXEL //"Loja"
				@ 12,067 SAY SA1->A1_LOJA SIZE 021,09 OF oScrPanel PIXEL FONT oBold
			Endif

			@ 04,090 SAY OemToAnsi(STR0008) SIZE 025,07 OF oScrPanel PIXEL //"Nome"
			@ 12,090 SAY SA1->A1_NOME SIZE 165,09 OF oScrPanel PIXEL FONT oBold

			oGetDb:=MsGetDB():New(aPosObj1[2,1],aPosObj1[2,2],aPosObj1[2,3],aPosObj1[2,4],2,"",,,.F.,,,.F.,,cAlias,,,,,,.T.)
			oGetdb:lDeleta:=NIL
			dbSelectArea(cAlias)
			dbGotop()

			@ aPosObj1[3,1]+04,005 SAY aSay[1] SIZE 045,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+04,175 SAY aSay[2] SIZE 045,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+15,005 SAY aSay[3] SIZE 045,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+15,175 SAY aSay[4] SIZE 045,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+26,005 SAY aSay[5] SIZE 045,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+26,175 SAY aSay[6] SIZE 045,07 OF oDlg PIXEL

			@ aPosObj1[3,1]+04,060 SAY aGet[1] SIZE 060,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+04,215 SAY aGet[2] SIZE 060,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+15,060 SAY aGet[3] SIZE 060,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+15,215 SAY aGet[4] SIZE 060,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+26,060 SAY aGet[5] SIZE 060,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+26,215 SAY aGet[6] SIZE 060,07 OF oDlg PIXEL

			If ( nBrowse == 1 ) // Para titulos em aberto, mostra legenda.
				Fc010Legenda(oDlg,aPosObj1,aSay,aGet)
			Endif

			DEFINE SBUTTON 		FROM 04,aPosObj1[1,3]-If(nBrowse <= 2,60,30) TYPE  1  ENABLE OF oScrPanel ACTION ( oDlg:End() )
			// Exibe o botao de distribuicao por moedas, apenas na consulta de titulos
			// em aberto e recebidos
			If nBrowse <= 2
				SButton():New(04, aPosObj1[1,3]-30, 18,{||Fc010Moeda(If(nBrowse==1,aTotRec,aTotPag),oScrPanel)},oScrPanel,.T.,STR0097) //"Consulta distribui��o por moedas"
				TButton():New(19, aPosObj1[1,3]-60,'Excel',oScrPanel,{||Fc010Excel(cAlias,aHeader,aParam,nBrowse)},26,10,,,,.T.) 			
			Endif	

			DEFINE SBUTTON oBtn 	FROM 19,aPosObj1[1,3]-30 TYPE 15 ENABLE OF oScrPanel

			oBtn:lAutDisable := .F.
			If ( bVisual != Nil )
				oBtn:bAction := bVisual
			Else
				oBtn:SetDisable(.T.)
			EndIf
			ACTIVATE MSDIALOG oDlg
		Else
			Help(" ",1,"REGNOIS")	
		EndIf
		If nBrowse == 5
			(cAlias)->(dbCloseArea()) ;Ferase(cArquivo+GetDBExtension());Ferase(cArquivo+OrdBagExt())
		Endif
	EndIf
	RestArea(aAreaSC5)
	RestArea(aAreaSC6)
	RestArea(aAreaSC9)
	RestArea(aAreaSF4)
	RestArea(aArea)
Return(aHeader)


Static Function _RFATC013(aLista,nLinha,lRet,nChamada)
	Local lRet := .t.
	If Empty(nChamada)
		oDlg:End()
		Return(Nil)
	EndIf

	oDlg:End()
Return(lRet)

User Function _CONSESP() 
	Local campo1:=SA1->A1_COD
	Local campo2:=SA1->A1_LOJA
	Local campo3:=SA1->A1_NOME

	Return()

	****************************************************************************************************************************************************************************

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �Fc010Brow � Autor � Eduardo Riera         � Data �31.12.1999���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Consulta a Posicao de Clientes                              ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpN1 : nOpcao                                             ���
���          �         [1] Titulos em Aberto                              ���
���          �         [2] Titulos Recebidos                              ���
���          �         [3] Pedidos                                        ���
���          �         [4] Faturamento                                    ���
���          � ExpA2 : Alias a Serem Fechados.                            ���
���          � ExpA3 : [1] Data de Emissao Inicial                        ���
���          �         [2] Data de Emissao Final                          ���
���          �         [3] Vencimento Inicial                             ���
���          �         [4] Vencimento Final                               ���
���          �         [5] Considera Provisorios (1) Sim (2) Nao          ���
���          �         [6] Prefixo Inicial                                ���
���          �         [7] Prefixo Final                                  ���
���          � ExpL4 : Indica se os dados devem ser exibidos              ���
���          � ExpL5 : Indica se os dados serao montados para o relatorio ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao Efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function _Fc010Brow(nBrowse,aAlias,aParam,lExibe,aGet,lRelat)

	Local aArea		:= GetArea()
	Local aAreaSC5	:= SC5->(GetArea())
	Local aAreaSC6	:= SC6->(GetArea())
	Local aAreaSC9	:= SC9->(GetArea())
	Local aAreaSF4	:= SF4->(GetArea())
	Local aStru		:= {}
	Local aQuery	:= {}
	Local aSay		:= {"","","","","","","",""}
	Local oGetDb
	Local oScrPanel
	Local oBold
	Local oDlg
	Local oBtn
	Local bVisual
	Local bWhile
	Local bFiltro
	Local cAlias	:= ""
	Local cArquivo	:= ""
	Local cCadastro	:= ""
	#IFDEF TOP
	Local cQuery	:= ""
	Local cDbMs
	#ENDIF	
	Local cQry		:= ""
	Local cChave	:= ""
	Local lQuery	:= .F.
	Local nCntFor	:= 0
	Local nSalped	:= 0
	Local nSalpedl	:= 0
	Local nSalpedb	:= 0
	Local nQtdPed	:= 0
	Local nTotAbat	:= 0
	Local cAnterior	:= ""
	Local nTaxaM	:= 0	
	Local nMoeda
	Local oTipo
	Local nTipo		:= 1
	Local bTipo
	Local oCheq
	Local aTotRec	:= {{0,1,0,0}} // Totalizador de titulos a receber por por moeda
	Local aTotPag	:= {{0,1,0,0}} // Totalizador de titulos recebidos por por moeda
	Local nAscan
	Local nTotalRec	:=0
	Local aSize		:= MsAdvSize( .F. )
	Local aPosObj1	:= {}                 
	Local aObjects	:= {}                       
	Local aCpos		:= {}
	Local aCpoVlr	:= {}
	Local cCheques	:=	IIF(Type('MVCHEQUES')=='C',MVCHEQUES,MVCHEQUE)
	Local nI
	Local lPosClFt	:= (SuperGetMv("MV_POSCLFT",.F.,"N") == "S")
	Local bCond
	Local cOrdem	:= ""
	Local cNumAnt	:= ""
	Local cTpDocAnt	:= ""
	Local cParcAnt	:= ""
	Local lFC010Head := ExistBlock("FC010HEAD")
	Local cSaldo := ""
	Local lFC010Pedi := ExistBlock("FC010Pedi")
	Local aRetAux	:= {}
	Local lFC0101FAT	:= ExistBlock("FC0101FAT")
	Local lFC0102FAT	:= ExistBlock("FC0102FAT")
	Local lFC0103FAT	:= ExistBlock("FC0103FAT")
	Local aAuxCpo		:= {}
	Local aHeader1		:=	{}
	Local nA				:=	0
	Local nMulta		:= 0                            //Valor da Multa
	Local cMvJurTipo 	:= SuperGetMv("MV_JURTIPO",,"") //Tipo de Calculo de Juros do Financeiro	
	Local lLojxRMul  	:= .T.        //Funcao que calcula a Multa do Financeiro
	Local lMvLjIntFS   := SuperGetMv("MV_LJINTFS", ,.F.) //Habilita Integra��o com o Financial Services
	Local lFC010bxhe := ExistBlock("FC010bxhe")//Ponto de entrada para incluir campo no Head dos Tit. Baixados
	Local lFilF4		:= .F.
	Local lFilF4C6 := .F.

	/*
	GESTAO - inicio */
	Local nPosAlias	:= 0
	Local cCompSC5	:= ""
	/* GESTAO - fim
	*/
	Private nCayas  := 2
	Private aHeader	:= {}
	Private AtmxFil := {}
	DEFAULT lRelat	:= .F.

	aGet := {"","","","","","","",""}

	Posicione("SA1",3,xFilial("SA1")+ALLTRIM(cCodCli),"A1_LOJA")
	msgStop("Procurando pelo cliente "+SA1->A1_COD+ ' - '+SA1->A1_NOME +cCodCli )
	Do Case
		Case ( nBrowse == 1 )	
		cCadastro := STR0025
		cAlias    := "FC010QRY01"
		aSay[1]   := STR0032 //"Qtd.Tit."
		aSay[2]   := STR0033 //"Principal"
		aSay[3]   := "Saldo a Receber" //"Saldo a Receber"  STR0120
		aSay[4]   := STR0046 //"Juros"
		aSay[5]   := STR0091 //"Acresc."
		aSay[6]   := STR0092 //"Decresc."
		aSay[7]   := STR0086 //"Abatimentos"
		aSay[8]   := STR0104 //"Tot.Geral"	
		bVisual   := {|| _Fc010Visua((cAlias)->XX_RECNO,nBrowse) }
		Case ( nBrowse == 2 )
		cCadastro := 'STR0122'
		cAlias    := "FC010QRY02"
		aSay[1]   := STR0036 //"Qtd.Pag."
		aSay[2]   := STR0037 //"Principal"
		aSay[3]   := STR0038 //"Vlr.Pagto"
		bVisual   := {|| _Fc010Visua((cAlias)->XX_RECNO,nBrowse) }
		Case ( nBrowse == 3 )
		cCadastro := STR0027
		cAlias    := "FC010QRY03"
		aSay[1]   := STR0039 //"Qtd.Ped."
		aSay[2]   := STR0040 //"Tot.Pedido"
		aSay[3]   := STR0041 //"Tot.Liber."
		aSay[4]   := STR0042 //"Sld.Pedido"
		bVisual   := {|| _Fc010Visua((cAlias)->XX_RECNO,nBrowse) }
		Case ( nBrowse == 4 )
		cCadastro := STR0028
		cAlias    := "FC010QRY04"
		aSay[1]   := STR0043 //"Qtd.Notas"
		aSay[2]   := STR0044 //"Tot.Fatur."
		bVisual   := {|| _Fc010Visua((cAlias)->XX_RECNO,nBrowse) }
		Case ( nBrowse == 5 )
		cCadastro := STR0061 //"Cartera de cheques"
		cAlias    := "FC010QRY05"
		aSay[1]   := STR0062 //"Pendiente"
		If cPaisLoc$"URU|BOL"
			aSay[2]   := STR0088 //"Rechazado"
		Else
			aSay[2]   := STR0063 //"Negociado"
		Endif   
		aSay[3]   := STR0064 //"Cobrado"
		If cPaisLoc$"ARG|COS"
			aSay[4]   := "Em Transito" //STR0134 //"Em Transito"
		Endif
		bVisual   := {|| _Fc010Visua((cAlias)->XX_RECNO,If( (cAlias)->XX_ESTADO == "TRAN",5,1) ) }

	EndCase

	Do Case
		//������������������������������������������������������������������������Ŀ
		//�Titulo em Aberto                                                        �
		//��������������������������������������������������������������������������
		Case ( nBrowse == 1 )
		If cPaisLoc == "BRA" .And. !lRelat
			Aadd(aHeader,{"",	"XX_LEGEND","@BMP",10,0,"","","C","",""})
			Aadd(aStru,{"XX_LEGEND","C",12,0})
		Endif	
		dbSelectArea("SX3")
		dbSetOrder(2)
		dbSeek("E1_LOJA")
		If aParam[13] == 2  //Considera loja		
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		Endif
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		/* 
		GESTAO - inicio */
		dbSeek("E1_FILORIG")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		/* GESTAO - fim
		*/
		dbSeek("E1_PREFIXO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_NUM")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_PARCELA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_TIPO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		//dbSeek("E1_NFELETR")
		//aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		//aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		//aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})


		dbSeek("E1_CLIENTE")
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_EMISSAO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_VENCTO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_BAIXA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_VENCREA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		If !lRelat
			dbSeek("E1_MOEDA")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )

			dbSeek("E1_VALOR")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		Endif	

		dbSeek("E1_VLCRUZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aadd(aHeader,{STR0086,"E1_ABT","@E 999,999,999.99",14,2,"","","N","","V" } ) //"Abatimentos"
		aadd(aStru ,{"E1_ABT","N",14,2})

		dbSeek("E1_SDACRES")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_SDDECRE")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_VALJUR")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		//�������������������������������������������������������������Ŀ
		//� 11/Ago/2005 Rafael E. Rodrigues                             �
		//� Quando o modulo Gestao Educacional estiver em uso, inclui a �
		//� coluna com a provisao de multa de titulos em atraso.        �
		//���������������������������������������������������������������
		If GetNewPar( "MV_ACATIVO", .F. )
			dbSeek("E1_VLMULTA")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL}) 

			dbSeek("E1_MULTA")		
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Else
			//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Inicio
			/*BEGINDOC
			//�����������������������������������������������������Ŀ
			//�Calculo de juros e multas, segundo o controle de loja�
			//�������������������������������������������������������
			ENDDOC*/
			If (cMVJurTipo == "L" .OR. lMvLjIntFS).AND. lLojxRMul
				dbSeek("E1_MULTA")		
				aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
				aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
				aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			EndIf  
			//Calculo de Juros e Multas: SIGALOJA x SIGAFIN - Final
		Endif   

		//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Inicio	
		/*BEGINDOC
		//�����������������������������������������������������Ŀ
		//�Calculo de juros e multas, segundo o controle de loja�
		//�������������������������������������������������������
		ENDDOC*/
		If (cMVJurTipo == "L" .OR. lMvLjIntFs) .and. lLojxRMul
			dbSeek("E1_ACRES")		
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

			dbSeek("E1_JUROS")		
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		EndIf 
		//Calculo de Juros e Multas: SIGALOJA x SIGAFIN - Final

		dbSeek("E1_SALDO")
		aadd(aHeader,{"Saldo a Receber",SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )	//"Saldo a Receber"
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aadd(aHeader,{ STR0103,"E1_SALDO2",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } ) // "Saldo na moeda tit"
		aadd(aStru ,{"E1_SALDO2",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_NATUREZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_PORTADO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})		

		dbSeek("E1_NUMBCO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		If !lRelat
			dbSeek("E1_NUMLIQ")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif

		dbSeek("E1_HIST")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aadd(aHeader,{STR0035,"E1_ATR","9999999999",10,0,"","","N","","V" } ) //"Atraso"
		aadd(aStru ,{"E1_ATR","N",10,0})

		If !lRelat
			dbSeek("E1_CHQDEV")
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif
		//sitcob
		dbSeek("E1_SITUACA")
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Aadd(aStru,{"XX_STATUS","C",40,0})
		Aadd(aStru,{"FRV_DESCRI","C",40,0})
		aadd(aStru,{"XX_RECNO","N",12,0})
		aadd(aStru,{"E1_MOEDA","N",02,0})

		aadd(aQuery,{"E1_PORCJUR","N",12,4})
		aadd(aQuery,{"E1_MOEDA","N",02,0})
		aadd(aQuery,{"E1_VALOR","N",16,2})

		If cPaisLoc == "BRA"
			aadd(aQuery,{"E1_TXMOEDA","N",17,4})
		Endif	

		If !lRelat
			Aadd(aHeader,{"Status Serasa","XX_STATUS","@!",40,0,"","","C","","V" } ) //"Status Serasa"
			//sitcob
			Aadd(aHeader,{STR0045,"FRV_DESCRI","@!",40,0,"","","C","FRV","" } ) //"Situacao"
		EndIf

		IF lFC010Head
			aHeader1 :=	aHeader			
			aHeader 	:=	ExecBlock("FC010HEAD",.F.,.F.,aHeader)							

			For nA:=1 to Len(aHeader)
				If Ascan(aHeader1,{|e| e[2] = aHeader[nA,2]}) = 0//Campo nao estava no header incluir tambem nestes vetores.         
					aadd(aStru ,{aHeader[nA,2],aHeader[nA,8],aHeader[nA,4],aHeader[nA,5]})
					aadd(aQuery,{aHeader[nA,2],aHeader[nA,8],aHeader[nA,4],aHeader[nA,5]})												
				Endif					
			Next										
		EndIf

		SX3->(dbSetOrder(1))

		If ( Select(cAlias) ==	0 )
			cArquivo := CriaTrab(,.F.)			
			aadd(aAlias,{ cAlias , cArquivo })
			aadd(aStru,{"FLAG","L",01,0})
			dbCreate(cArquivo,aStru)
			dbUseArea(.T.,,cArquivo,cAlias,.F.,.F.)
			IndRegua(cAlias,cArquivo,"E1_FILORIG+E1_CLIENTE+E1_LOJA+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO")

			#IFDEF TOP
			If ( TcSrvType()!="AS/400" )
				lQuery := .T.
				cQuery := ""
				aEval(aQuery,{|x| cQuery += ","+AllTrim(x[1])})
				cQuery := "SELECT "+SubStr(cQuery,2)
				cQuery +=         ",SE1.R_E_C_N_O_ SE1RECNO"
				cQuery +=         ",FRV.FRV_DESCRI "
				cQuery += "FROM "+RetSqlName("SE1")+" SE1,"
				cQuery +=         RetSqlName("FRV")+" FRV "
				/*
				GESTAO - inicio */
				nPosAlias := _FC010QFil(1,"SE1")
				cQuery += "WHERE SE1.E1_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				/* GESTAO - fim
				*/
				cQuery +=       "SE1.E1_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=       "SE1.E1_LOJA='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=       "SE1.E1_EMISSAO>='"+Dtos(aParam[1])+"' AND "
				cQuery +=       "SE1.E1_EMISSAO<='"+Dtos(aParam[2])+"' AND "
				cQuery +=       "SE1.E1_VENCREA>='"+Dtos(aParam[3])+"' AND "
				cQuery +=       "SE1.E1_VENCREA<='"+Dtos(aParam[4])+"' AND "
				If ( aParam[5] == 2 )
					cQuery +=   "SE1.E1_TIPO<>'PR ' AND "
				EndIf					
				If ( aParam[15] == 2 )
					cQuery +=   "SE1.E1_TIPO<>'RA ' AND "	
				Endif
				cQuery += "SE1.E1_PREFIXO>='"+aParam[6]+"' AND "
				cQuery += "SE1.E1_PREFIXO<='"+aParam[7]+"' AND " 
				If cPaisLoc != "BRA"
					cQuery += "SE1.E1_TIPO NOT IN" + FormatIn(cCheques,"|") + " AND "
				Endif	
				cQuery += "SE1.E1_SALDO > 0 AND "

				If aParam[11] == 2 // Se nao considera titulos gerados pela liquidacao
					If aParam[09] == 1 
						cQuery += "SE1.E1_NUMLIQ ='"+Space(Len(SE1->E1_NUMLIQ))+"' AND "
					Else  
						cQuery += "SE1.E1_TIPOLIQ='"+Space(Len(SE1->E1_TIPOLIQ))+"' AND "						
						cQuery += "SE1.E1_NUMLIQ ='"+Space(Len(SE1->E1_NUMLIQ))+"' AND "
					Endif	
				Else
					If aParam[09] == 2
						cQuery += "SE1.E1_TIPOLIQ='"+Space(Len(SE1->E1_TIPOLIQ))+"' AND "						
					Endif	
				Endif

				//�����������������������������������������������������Ŀ
				//� Ponto de entrada para filtrar pelo MSFIL em caso de �
				//� arquivo compartilhado.  Titulos em aberto           �
				//�������������������������������������������������������
				If (ExistBlock("FO10FILT"))    
					cQuery += ExecBlock("FO10FILT",.F.,.F.)
				Endif                    

				//SITCOB
				cQuery +=		"SE1.D_E_L_E_T_<>'*' AND "
				cQuery +=    	"FRV.FRV_FILIAL = '"+xFilial("FRV")+"' AND "
				cQuery +=		"FRV.FRV_CODIGO = SE1.E1_SITUACA AND "
				cQuery +=		"FRV.D_E_L_E_T_<>'*' "

				cQuery += "AND SE1.E1_TIPO NOT LIKE '__-' UNION ALL "+cQuery
				cQuery += "AND SE1.E1_TIPO LIKE '__-'"

				If UPPER(TcGetDb()) != "INFORMIX"
					cQuery   += " ORDER BY  " + SqlOrder("E1_CLIENTE+E1_LOJA+E1_PREFIXO+E1_NUM+E1_PARCELA+SE1RECNO")
				Endif	

				//�������������������������������������������������������������������������Ŀ
				//� Ponto de entrada para alteracao da cQuery na consulta Titulos em Aberto �
				//���������������������������������������������������������������������������
				If (ExistBlock("F010CQTA"))    
					cQuery := ExecBlock("F010CQTA",.F.,.F.,{cQuery})
				Endif  

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"A"

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)

				aEval(aQuery,{|x| If(x[2]!="C",TcSetField(cQry,x[1],x[2],x[3],x[4]),Nil)})
			Else
				#ENDIF
				cQry := "SE1"
				#IFDEF TOP
			EndIf
			#ENDIF
			dbSelectArea(cQry)
			If ( !lQuery )
				dbSetOrder(2)
				If aParam[13] == 1 //Considera loja
					dbSeek(xFilial("SE1")+SA1->A1_COD+SA1->A1_LOJA)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE .And.;
					SA1->A1_LOJA   == SE1->E1_LOJA }
				Else
					dbSeek(xFilial("SE1")+SA1->A1_COD)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE }
				Endif

				bFiltro:= {|| !(SE1->E1_TIPO $ MVABATIM) .And.;
				SE1->E1_EMISSAO >= aParam[1] .And.;
				SE1->E1_EMISSAO <= aParam[2] .And.;
				SE1->E1_VENCREA >= aParam[3] .And.;
				SE1->E1_VENCREA <= aParam[4] .And.;
				If(aParam[5]==2,SE1->E1_TIPO!="PR ",.T.) .And.;
				If(aParam[15]==2,!SE1->E1_TIPO$MVRECANT,.T.) .And.;
				SE1->E1_PREFIXO >= aParam[6] .And.;
				SE1->E1_PREFIXO <= aParam[7] .And.;
				SE1->E1_SALDO   > 0 .And.;
				IIf(cPaisLoc == "BRA",.T.,!(SE1->E1_TIPO$cCheques)) .And.;
				IIF(aParam[11] == 2, Empty(SE1->E1_NUMLIQ) .And. Empty(SE1->E1_TIPOLIQ),.T.)}
			Else
				bWhile := {|| !Eof() }
				bFiltro:= {|| .T. }
			EndIf			
			While ( Eval(bWhile) )				
				If ( Eval(bFiltro) )
					dbSelectArea(cAlias)
					dbSetOrder(1)
					cChave := (cQry)->E1_FILORIG+(cQry)->(E1_CLIENTE)+(cQry)->(E1_LOJA) +;
					(cQry)->(E1_PREFIXO)+(cQry)->(E1_NUM)+;
					(cQry)->(E1_PARCELA)
					cChave += If((cQry)->(E1_TIPO)	$ MVABATIM, "",;
					(cQry)->(E1_TIPO))
					If ( !dbSeek(cChave) )
						RecLock(cAlias,.T.)						
					Else
						RecLock(cAlias,.F.)
					EndIf
					DbSetOrder(1)
					nTotAbat := 0

					//Calculo de Juros e Multas: SIGALOJA x SIGAFIN - Inicio 
					nMulta := 0 
					If !( (cQry)->E1_TIPO $ MVRECANT + "|" + MV_CRNEG ) .And. (cMVJurTipo == "L" .OR. lMvLjIntFS) .AND. lLojxRMul .And. aParam[12] == 2
						nMulta := LojxRMul( , , ,(cQry)->E1_SALDO, (cQry)->E1_ACRESC, (cQry)->E1_VENCREA,  , , (cQry)->E1_MULTA, ,;
						(cQry)->E1_PREFIXO, (cQry)->E1_NUM, (cQry)->E1_PARCELA, (cQry)->E1_TIPO, (cQry)->E1_CLIENTE, (cQry)->E1_LOJA,, .T.  ) 
					EndIf
					//Calculo de Juros e Multas: SIGALOJA x SIGAFIN - Final

					For nCntFor := 1 To Len(aStru)
						Do Case

							Case ( AllTrim(aStru[nCntFor][1])=="FRV_DESCRI" )
							If !( (cQry)->(E1_TIPO)	$ MVABATIM )
								If ( lQuery )
									(cAlias)->FRV_DESCRI := FN022SITCB((cQry)->E1_SITUACA)[9]	
								Else
									(cAlias)->FRV_DESCRI := FN022SITCB(SE1->E1_SITUACA)[9]	
								EndIf
							Endif	
							Case ( AllTrim( aStru[nCntFor][1] ) == "XX_STATUS" )
							If !( (cQry)->(E1_TIPO) $ MVABATIM )
								If ( lQuery )
									cChaveTit	:=	(cQry)->(	E1_FILORIG	+"|"+;
									E1_PREFIXO	+"|"+;
									E1_NUM		+"|"+;
									E1_PARCELA	+"|"+;
									E1_TIPO	+"|"+;
									E1_CLIENTE	+"|"+;
									E1_LOJA)									
									cIdDoc	:= FINGRVFK7("SE1", cChaveTit)
									(cAlias)->XX_STATUS := GetStatus(.F., cIdDoc)									
								EndIf
							Endif

							Case ( AllTrim(aStru[nCntFor][1])=="E1_VALJUR" )
							Case ( AllTrim(aStru[nCntFor][1])=="E1_ABT" )
							If cPaisLoc == "BRA"
								nTaxaM := (cQry)->E1_TXMOEDA
							Else
								nTaxaM:=round((cQry)->E1_VLCRUZ / (cQry)->E1_VALOR,4)  // Pegar a taxa da moeda usada qdo da inclus�o do titulo
							Endif
							If ( (cQry)->(E1_TIPO)	$ MVABATIM )
								(cAlias)->E1_ABT += (nTotAbat := xMoeda((cQry)->(E1_SALDO),(cQry)->(E1_MOEDA),1,(cQry)->(E1_EMISSAO),,nTaxaM))
							Endif
							If ( !lQuery )
								(cAlias)->E1_ABT := (nTotAbat := SomaAbat(SE1->E1_PREFIXO,SE1->E1_NUM,SE1->E1_PARCELA,"R",1,,SE1->E1_CLIENTE,SE1->E1_LOJA))
							Endif

							Case ( AllTrim(aStru[nCntFor][1])=="E1_SALDO" )
							If cPaisLoc == "BRA"
								nTaxaM := (cQry)->E1_TXMOEDA
							Else
								nTaxaM:=round((cQry)->E1_VLCRUZ / (cQry)->E1_VALOR,4)  // Pegar a taxa da moeda usada qdo da inclus�o do titulo
							Endif	
							If ( (cQry)->(E1_TIPO)	$ MVABATIM )
								(cAlias)->E1_SALDO -= nTotAbat
							Else
								(cAlias)->E1_SALDO += xMoeda((cQry)->(E1_SALDO),(cQry)->(E1_MOEDA),1,dDataBase,,ntaxaM)
								If aParam[12] == 2 // mv_par12 = 2 : Considera juros e taxa de pernamencia na visualizacao de titulos em aberto.
									(cAlias)->E1_SALDO += xMoeda((cQry)->(E1_SDACRES) - (cQry)->(E1_SDDECRE),(cQry)->(E1_MOEDA),1,dDataBase,,ntaxaM)
									If !( (cAlias)->E1_TIPO $ MVRECANT + "|" + MV_CRNEG )
										(cAlias)->E1_SALDO += xMoeda(FaJuros((cQry)->E1_VALOR,(cQry)->E1_SALDO,(cQry)->E1_VENCTO,(cQry)->E1_VALJUR,(cQry)->E1_PORCJUR,(cQry)->E1_MOEDA,(cQry)->E1_EMISSAO,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0),(cQry)->E1_BAIXA,(cQry)->E1_VENCREA,,(cQry)->E1_PREFIXO, (cQry)->E1_NUM, (cQry)->E1_PARCELA,(cQry)->E1_TIPO),(cQry)->E1_MOEDA,1,,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0)) 	//REQ020-Calculo de Juros e Multas: SIGALOJA x SIGAFIN 
										//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Inicil
										(cAlias)->E1_SALDO += xMoeda(nMulta,(cQry)->E1_MOEDA,1,,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0))
										//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Final


										//�������������������������������������������������������������Ŀ
										//� 11/Ago/2005 Rafael E. Rodrigues                             �
										//� Quando o modulo Gestao Educacional estiver em uso, inclui a �
										//� provisao de multa no saldo de titulos em atraso.            �
										//���������������������������������������������������������������
										If GetNewPar( "MV_ACATIVO", .F. )									
											(cAlias)->E1_SALDO += xMoeda(If(Empty((cQry)->(E1_BAIXA)) .and. dDataBase > (cQry)->(E1_VENCREA), (cQry)->(E1_VLMULTA), (cQry)->(E1_MULTA)),(cQry)->(E1_MOEDA),1,dDataBase,,ntaxaM)
										Endif
									EndIf
								Endif
							EndIf
							If ( !lQuery )
								If aParam[12] == 2   // mv_par12 = 2 : Considera juros e taxa de pernamencia na visualizacao de titulos em aberto.	
									(cAlias)->E1_SALDO -= nTotAbat
								Endif
							EndIf
							Case ( AllTrim(aStru[nCntFor][1])=="E1_SALDO2" )
							If ( (cQry)->(E1_TIPO)	$ MVABATIM )	
								(cAlias)->E1_SALDO2 -= nTotAbat
							Else
								(cAlias)->E1_SALDO2 += (cQry)->(E1_SALDO)
								If !( (cAlias)->E1_TIPO $ MVRECANT + "|" + MV_CRNEG )
									//Calculo de Juros e Multas: SIGALOJA x SIGAFIN   -Inicio
									(cAlias)->E1_VALJUR := xMoeda(FaJuros((cQry)->E1_VALOR,(cAlias)->E1_SALDO2,(cQry)->E1_VENCTO,(cQry)->E1_VALJUR,(cQry)->E1_PORCJUR,(cQry)->E1_MOEDA,(cQry)->E1_EMISSAO,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0),(cQry)->E1_BAIXA,(cQry)->E1_VENCREA,,(cQry)->E1_PREFIXO, (cQry)->E1_NUM, (cQry)->E1_PARCELA,(cQry)->E1_TIPO),(cQry)->E1_MOEDA,1,,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0))										    
									/*BEGINDOC
									//���������������������������������������������������������D�
									//�Se o calculo de juros e multa for do controle de lojas, �
									//�considera os juros e multas no saldo                    �
									//���������������������������������������������������������D�
									ENDDOC*/
									If (cMVJurTipo == "L" .OR. lMvLjIntFs)  .AND. lLojxRMul							    
										(cAlias)->E1_MULTA := nMulta 
									EndIf
								EndIf 
								//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Final							   								
								If aParam[12] == 2   // mv_par12 = 2 : Considera juros e taxa de pernamencia na visualizacao de titulos em aberto.	
									(cAlias)->E1_SALDO2 += (cAlias)->E1_SDACRES - (cAlias)->E1_SDDECRE
									If !( (cAlias)->E1_TIPO $ MVRECANT + "|" + MV_CRNEG )
										(cAlias)->E1_SALDO2 += xMoeda((cAlias)->E1_VALJUR,1,(cQry)->(E1_MOEDA),dDataBase,,ntaxaM) 
										If (cMVJurTipo == "L" .OR. lMvLjIntFS) .and. lLojxRMul
											(cAlias)->E1_SALDO2 += xMoeda((cAlias)->E1_MULTA,1,(cQry)->(E1_MOEDA),dDataBase,,ntaxaM) 
										EndIf

										//�������������������������������������������������������������Ŀ
										//� 11/Ago/2005 Rafael E. Rodrigues                             �
										//� Quando o modulo Gestao Educacional estiver em uso, inclui a �
										//� provisao de multa no saldo de titulos em atraso.            �
										//���������������������������������������������������������������
										If GetNewPar( "MV_ACATIVO", .F. )									
											(cAlias)->E1_SALDO2 += xMoeda(If(Empty((cQry)->(E1_BAIXA)) .and. dDataBase > (cQry)->(E1_VENCREA), (cQry)->(E1_VLMULTA), (cQry)->(E1_MULTA)),(cQry)->(E1_MOEDA),1,dDataBase,,ntaxaM)
										Endif
									EndIf								
								Endif
							EndIf
							If ( !lQuery )
								If aParam[12] == 2   // mv_par12 = 2 : Considera juros e taxa de pernamencia na visualizacao de titulos em aberto.	
									(cAlias)->E1_SALDO2 -= nTotAbat
								Endif
							EndIf		
							Case ( AllTrim(aStru[nCntFor][1])=="XX_RECNO" )
							If !( (cQry)->(E1_TIPO)	$ MVABATIM )
								If ( lQuery )
									(cAlias)->XX_RECNO := (cQry)->SE1RECNO
								Else
									(cAlias)->XX_RECNO := SE1->(RecNo())
								EndIf
							Endif
							//Tabela FWA - Situa��o de t�tulo Serasa, n�o existe em pa�ses localizados	
							Case cPaisLoc == "BRA" .And.( !lRelat .And. AllTrim(aStru[nCntFor][1])=="XX_LEGEND" )//Rodrigo

							DbSelectArea("FWA")
							FWA->( DbSetOrder(3) )

							If 	FWA->(DbSeek((cQry)->(E1_FILORIG+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO+E1_CLIENTE+E1_LOJA))) .AND. FWA->(FWA_STATUS) == "3"
								(cAlias)->XX_LEGEND := 	"DISABLE"
							ElseIf (cQry)->E1_CHQDEV == "1"
								(cAlias)->XX_LEGEND := 	"BR_AMARELO"
							Else
								If !((cQry)->E1_TIPO $ MVABATIM)
									(cAlias)->XX_LEGEND := If(ROUND((cQry)->E1_SALDO,2) != ROUND((cQry)->E1_VALOR,2),"BR_AZUL","BR_VERDE")
								EndIf
							Endif
							DbCloseArea("FWA")
							Case ( AllTrim(aStru[nCntFor][1])=="E1_TIPO" )
							If ( Empty((cAlias)->E1_TIPO) )
								(cAlias)->E1_TIPO := (cQry)->E1_TIPO
							EndIf
							Case ( AllTrim(aStru[nCntFor][1])=="E1_ATR" )
							//Se o t�tulo estiver atrasado, faz o calculo dos dias de atraso
							If dDataBase > (cQry)->E1_VENCREA
								If (((cAlias)->E1_TIPO) $ MVRECANT+"/"+MV_CRNEG)
									(cAlias)->E1_ATR := 0
								Else	
									(cAlias)->E1_ATR := dDataBase - (cAlias)->E1_VENCREA
								EndIf	
							Else 
								If MV_PAR16 == 2 //Se o t�tulo N�O estiver atrasado, ent�o tem ATRASO = 0
									(cAlias)->E1_ATR := 0
								Else
									(cAlias)->E1_ATR := dDataBase - DataValida((cAlias)->E1_VENCREA,.T.)
								EndIf
							Endif
							Case ( AllTrim(aStru[nCntFor][1])=="FLAG" )

							Case ( AllTrim(aStru[nCntFor][1])=="E1_VLCRUZ" )
							If !((cQry)->(E1_TIPO)	$ MVABATIM)
								(cAlias)->E1_VLCRUZ := xMoeda((cQry)->(E1_VALOR),(cQry)->(E1_MOEDA),1,dDataBase,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0))
							Endif
							Case ( AllTrim(aStru[nCntFor][1])=="E1_VLMULTA" )
							//��������������������������������������������������������������Ŀ
							//� 11/Ago/2005 Rafael E. Rodrigues                              �
							//� Quando o modulo Gestao Educacional estiver em uso eh exibida �
							//� a provisao de multa no saldo de titulos em atraso.           �
							//����������������������������������������������������������������
							(cAlias)->E1_VLMULTA := xMoeda(If(Empty((cQry)->(E1_BAIXA)) .and. dDataBase > (cQry)->(E1_VENCREA), (cQry)->(E1_VLMULTA), (cQry)->(E1_MULTA)),(cQry)->(E1_MOEDA),1,dDataBase,,ntaxaM)						
							OtherWise							
							If !( (cQry)->(E1_TIPO)	$ MVABATIM )
								(cAlias)->(FieldPut(nCntFor,(cQry)->(FieldGet(FieldPos(aStru[nCntFor][1])))))
							Endif	
						EndCase
					Next nCntFor
					dbSelectArea(cAlias)
					If nTotAbat = 0
						If ( (cAlias)->E1_SALDO <= 0 )
							dbDelete()
						EndIf
					Endif						
					MsUnLock()
				EndIf
				dbSelectArea(cQry)
				dbSkip()				
			EndDo
			If ( lQuery )
				dbSelectArea(cQry)
				dbCloseArea()
			EndIf
			cOrdem := "DTOS(E1_VENCREA)"

			If (ExistBlock("F010ORD1"))    
				//Retornar chave no formato "E1_CLIENTE+E1_LOJA+E1_TIPO+E1_PREFIXO+E1_NUM+E1_PARCELA+SE1RECNO"
				cOrdem := ExecBlock("F010ORD1",.F.,.F.)
			Endif                    

			dbSelectArea(cAlias)
			IndRegua(cAlias,cArquivo,cOrdem)
		EndIf
		//������������������������������������������������������������������������Ŀ
		//�Totais da Consulta                                                      �
		//��������������������������������������������������������������������������
		aGet[1] := 0
		aGet[2] := 0
		aGet[3] := 0
		aGet[4] := 0
		aGet[5] := 0
		aGet[6] := 0
		aGet[7] := 0
		aGet[8] := 0
		aTotRec := {{0,1,0,0}} // Totalizador de titulos a receber por moeda
		dbSelectArea(cAlias)
		dbGotop()
		While !EOF()		 			 	
			aGet[1]++
			If !lRelat
				SE1->(DbGoto((cAlias)->XX_RECNO))	// Posiciona no arquivo original para obter os valores
				// em outras moedas e em R$
				nAscan := Ascan(aTotRec,{|e| e[MOEDATIT] == E1_MOEDA})
			Endif

			//Calcular o abatimento para visualiza��o em tela
			If (cAlias)->E1_ABT > 0
				//(cAlias)->E1_SALDO -= (cAlias)->E1_ABT		 	
				(cAlias)->E1_SALDO2 := xMoeda((cAlias)->E1_SALDO,E1_MOEDA,1,dDataBase,,ntaxaM)
			Endif		 	

			If E1_TIPO $ "RA #"+MV_CRNEG
				aGet[2] -= E1_VLCRUZ
				aGet[3] -= E1_SALDO
				aGet[4] -= E1_VALJUR

				nAcresc := nDecres := 0
				If !lRelat
					nAcresc := xMoeda(E1_SDACRES,E1_MOEDA,1,dDataBase,,ntaxaM)
					nDecres := xMoeda(E1_SDDECRE,E1_MOEDA,1,dDataBase,,ntaxaM)
					aGet[5] -= nAcresc
					aGet[6] -= nDecres
					If nAscan = 0
						Aadd(aTotRec,{1,E1_MOEDA,SE1->E1_SALDO*(-1),If(E1_MOEDA>1,xMoeda(SE1->E1_SALDO,E1_MOEDA,1,,,If(cPaisLoc=="BRA",SE1->E1_TXMOEDA,0)),SE1->E1_SALDO)*(-1)})
					Else
						aTotRec[nAscan][QTDETITULOS]--
						aTotRec[nAscan][VALORTIT]		-= SE1->E1_SALDO
						aTotRec[nAscan][VALORREAIS]	-= If(E1_MOEDA>1,xMoeda(SE1->E1_SALDO,E1_MOEDA,1,,,If(cPaisLoc=="BRA",SE1->E1_TXMOEDA,0)),SE1->E1_SALDO)
					Endif
				Endif	
				If aParam[12] == 1 //Saldo sem correcao
					aGet[8] -= E1_SALDO-E1_ABT+E1_VALJUR+nAcresc-nDecres
				Else
					aGet[8] -= E1_SALDO
				Endif
			Else	
				aGet[2] += E1_VLCRUZ
				aGet[3] += E1_SALDO
				aGet[4] += E1_VALJUR
				aGet[7] += E1_ABT
				nAcresc := nDecres := 0
				If !lRelat
					nAcresc := xMoeda(E1_SDACRES,E1_MOEDA,1,dDataBase,,ntaxaM)
					nDecres := xMoeda(E1_SDDECRE,E1_MOEDA,1,dDataBase,,ntaxaM)
					aGet[5] += nAcresc
					aGet[6] += nDecres
					If nAscan = 0
						Aadd(aTotRec,{1,E1_MOEDA,SE1->E1_SALDO,If(E1_MOEDA>1,xMoeda(SE1->E1_SALDO,E1_MOEDA,1,,,If(cPaisLoc=="BRA",SE1->E1_TXMOEDA,0)),SE1->E1_SALDO)})
					Else
						aTotRec[nAscan][QTDETITULOS]++
						aTotRec[nAscan][VALORTIT]		+= SE1->E1_SALDO
						aTotRec[nAscan][VALORREAIS]	+= If(E1_MOEDA>1,xMoeda(SE1->E1_SALDO,E1_MOEDA,1,,,If(cPaisLoc=="BRA",SE1->E1_TXMOEDA,0)),SE1->E1_SALDO)
					Endif
				Endif
				If aParam[12] == 1 //Saldo sem correcao
					aGet[8] += E1_SALDO-E1_ABT+E1_VALJUR+nAcresc-nDecres
				Else
					aGet[8] += E1_SALDO
				Endif
			Endif
			dbSkip()
		Enddo
		If !lRelat
			nTotalRec:=0
			aEval(aTotRec,{|e| nTotalRec+=e[VALORREAIS]})
			Aadd(aTotRec,{"","",STR0096,nTotalRec}) //"Total ====>>"
			// Formata as colunas
			aEval(aTotRec,{|e|	If(ValType(e[VALORTIT]) == "N"	, e[VALORTIT]		:= Transform(e[VALORTIT],Tm(e[VALORTIT],16,nCayas)),Nil),;
			If(ValType(e[VALORREAIS]) == "N"	, e[VALORREAIS]	:= Transform(e[VALORREAIS],Tm(e[VALORREAIS],16,nCayas)),Nil)})
		Endif										

		aGet[1] := TransForm(aGet[1],Tm(aGet[1],16,0))
		aGet[2] := TransForm(aGet[2],Tm(aGet[2],16,nCayas))
		aGet[3] := TransForm(aGet[3],Tm(aGet[3],16,nCayas))
		aGet[4] := TransForm(aGet[4],Tm(aGet[4],16,nCayas))
		aGet[5] := TransForm(aGet[5],Tm(aGet[5],16,nCayas))
		aGet[6] := TransForm(aGet[6],Tm(aGet[6],16,nCayas))
		aGet[7] := TransForm(aGet[7],Tm(aGet[7],16,nCayas))		
		aGet[8] := TransForm(aGet[8],Tm(aGet[8],16,nCayas))		
		//������������������������������������������������������������������������Ŀ
		//�Titulos Recebidos                                                       �
		//��������������������������������������������������������������������������
		Case ( nBrowse == 2 )
		dbSelectArea("SX3")
		dbSetOrder(2)
		/* 
		GESTAO - inicio */
		dbSeek("E1_FILORIG")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		/* GESTAO - fim
		*/
		If aParam[13] == 2  //Considera loja
			dbSeek("E1_LOJA")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif
		dbSeek("E1_PREFIXO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_NUM")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_PARCELA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_TIPO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		//dbSeek("E1_NFELETR")
		//aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		//aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		//aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})


		IF !lRelat
			dbSeek("E1_MOEDA")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif

		dbSeek("E1_EMISSAO")
		aadd(aHeader,{ substr(AllTrim(X3Titulo()),1,8),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO-2,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_VENCTO")
		If !lRelat
			aadd(aHeader,{ substr(AllTrim(X3Titulo()),1,8),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO-2,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E1_VENCREA")
		aadd(aHeader,{substr(AllTrim(X3Titulo()),1,8),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO-2,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_DATA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO-2,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_DTDISPO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		IF !lRelat
			dbSeek("E1_VALOR")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif	

		dbSeek("E1_VLCRUZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aCpoVlr := { "E5_VLJUROS", "E5_VLMULTA", "E5_VLCORRE", "E5_VLDESCO", "E5_VLACRES", "E5_VLDECRE" }
		For nCntFor := 1 To Len(aCpoVlr)
			dbSeek(aCpoVlr[nCntFor])
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Next

		dbSeek("E5_VALOR")
		aadd(aHeader,{STR0047,"E1_PAGO",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,"V" } ) //"Pago"
		aadd(aStru ,{"E1_PAGO",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		IF !lRelat
			dbSeek("E1_VALOR")
			aadd(aHeader,{ STR0093,"E1_VLMOED2",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } ) //"Vlr pago  moeda tit."
			aadd(aStru ,{"E1_VLMOED2",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

			dbSeek("E5_VLMOED2")
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

			If cPaisLoc == "BRA"
				dbSeek("E5_TXMOEDA")
				aadd(aHeader,{AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
				aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
				aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			Endif
		Endif	

		dbSeek("E1_NATUREZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		If !lRelat
			dbSeek("E1_NUMLIQ")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif

		dbSeek("E5_BANCO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_AGENCIA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_CONTA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_HISTOR")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		dbSeek("E5_MOTBX")
		aadd(aHeader,{ "MOTBX",SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		If !lRelat
			dbSeek("E5_CNABOC")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif

		dbSeek("E5_TIPODOC")
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})		


		aadd(aHeader,{STR0035,"E1_ATR","99999",5,0,"","","N","","V" } ) //"Atraso"
		aadd(aStru ,{"E1_ATR","N",5,0})

		dbSeek("E1_VALJUR")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		//�������������������������������������������������������������Ŀ
		//� 11/Ago/2005 Rafael E. Rodrigues                             �
		//� Inclui a coluna com informacao de multa paga por paramento  �
		//� em atraso.                                                  �
		//���������������������������������������������������������������
		dbSeek("E1_MULTA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aadd(aStru,{"XX_RECNO","N",12,0})

		If cPaisLoc == "BRA"
			aadd(aQuery,{"E1_TXMOEDA","N",17,4})
		Endif	

		IF lFC010bxhe
			aHeader1 :=	aClone(aHeader)			
			aHeader 	:=	ExecBlock("FC010bxhe",.F.,.F.,aHeader)							

			For nA:=1 to Len(aHeader)
				If Ascan(aHeader1,{|e| e[2] = aHeader[nA,2]}) = 0//Campo nao estava no header incluir tambem nestes vetores.         
					aadd(aStru ,{aHeader[nA,2],aHeader[nA,8],aHeader[nA,4],aHeader[nA,5]})
					aadd(aQuery,{aHeader[nA,2],aHeader[nA,8],aHeader[nA,4],aHeader[nA,5]})												
				Endif					
			Next										
		EndIf


		SX3->(DbSetOrder(1))

		If ( Select(cAlias) ==	0 )
			cArquivo := CriaTrab(,.F.)			
			aadd(aAlias,{ cAlias , cArquivo })
			aadd(aStru,{"FLAG","L",01,0})
			dbCreate(cArquivo,aStru)
			dbUseArea(.T.,,cArquivo,cAlias,.F.,.F.)
			IndRegua(cAlias,cArquivo,"E1_FILORIG+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO")

			#IFDEF TOP
			If ( TcSrvType()!="AS/400" )
				cQuery := ""
				aEval(aQuery,{|x| cQuery += ","+AllTrim(x[1])})
				cQuery := "SELECT "+SubStr(cQuery,2)+",E1_ORIGEM,SE5.R_E_C_N_O_ SE5RECNO , SE5.E5_DOCUMEN E5_DOCUMEN "
				cQuery += "FROM "+RetSqlName("SE1")+" SE1,"
				cQuery +=         RetSqlName("SE5")+" SE5 "
				/*
				GESTAO - inicio */
				nPosAlias := _FC010QFil(1,"SE1")
				cQuery += "WHERE SE1.E1_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				/* GESTAO - fim
				*/
				cQuery +=       "SE1.E1_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=       "SE1.E1_LOJA='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=       "SE1.E1_EMISSAO>='"+Dtos(aParam[1])+"' AND "
				cQuery +=       "SE1.E1_EMISSAO<='"+Dtos(aParam[2])+"' AND "
				cQuery +=       "SE1.E1_VENCREA>='"+Dtos(aParam[3])+"' AND "
				cQuery +=       "SE1.E1_VENCREA<='"+Dtos(aParam[4])+"' AND "
				If ( aParam[5] == 2 )
					cQuery +=   "SE1.E1_TIPO<>'PR ' AND "
				EndIf					
				If ( aParam[15] == 2 )
					cQuery +=   "SE1.E1_TIPO<>'RA ' AND "
				EndIf					
				cQuery +=       "SE1.E1_PREFIXO>='"+aParam[6]+"' AND "
				cQuery +=       "SE1.E1_PREFIXO<='"+aParam[7]+"' AND "

				//�����������������������������������������������������Ŀ
				//� Ponto de entrada para filtrar pelo MSFIL em caso de �
				//� arquivo compartilhado. Titulos Recebidos            �
				//�������������������������������������������������������

				If (ExistBlock("FO10FLTR"))    
					cQuery += ExecBlock("FO10FLTR",.F.,.F.)
				Endif                    

				If cPaisLoc != "BRA"
					cQuery += "SE1.E1_TIPO NOT IN" + FormatIn(cCheques,"|") + " AND "
				Else
					cQuery += "SE1.E1_ORIGEM <> 'FINA087A' AND "
				Endif	
				cQuery +=		"SE1.E1_TIPO NOT LIKE '__-' AND "
				cQuery +=		"SE1.E1_TIPO NOT IN ('RA ','PA ','"+MV_CRNEG+"','"+MV_CPNEG+"') AND "
				cQuery +=		"SE1.D_E_L_E_T_ = ' ' AND "
				/*
				GESTAO - inicio */
				nPosAlias := _FC010QFil(1,"SE5")
				cQuery += "SE5.E5_FILIAL = SE1.E1_FILIAL AND "
				/* GESTAO - fim
				*/
				cQuery +=		"SE5.E5_NATUREZ=SE1.E1_NATUREZ AND "
				cQuery +=		"SE5.E5_PREFIXO=SE1.E1_PREFIXO AND "
				cQuery +=		"SE5.E5_NUMERO=SE1.E1_NUM AND "
				cQuery +=		"SE5.E5_PARCELA=SE1.E1_PARCELA AND "
				cQuery +=		"SE5.E5_TIPO=SE1.E1_TIPO AND "
				cQuery +=		"SE5.E5_CLIFOR=SE1.E1_CLIENTE AND "
				cQuery +=		"SE5.E5_LOJA=SE1.E1_LOJA AND "
				cQuery +=		"SE5.E5_RECPAG='R' AND "
				cQuery +=		"SE5.E5_SITUACA<>'C' AND "
				If aParam[8] == 2
					//Titulos baixados por geracao de fatura
					cQuery += " SE5.E5_MOTBX <> 'FAT' AND "
				Endif
				If aParam[09] == 2
					//Titulos baixados por liquidacao
					cQuery += " SE5.E5_MOTBX <> 'LIQ' AND "
				Endif
				cQuery +=		"SE5.D_E_L_E_T_ = ' ' AND NOT EXISTS ("
				cQuery += "SELECT A.E5_NUMERO "
				cQuery += "FROM "+RetSqlName("SE5")+" A "
				/*
				GESTAO - inicio */
				nPosAlias := _FC010QFil(1,"SE5")
				cQuery += "WHERE A.E5_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				/* GESTAO - fim
				*/
				cQuery +=		"A.E5_NATUREZ=SE5.E5_NATUREZ AND "
				cQuery +=		"A.E5_PREFIXO=SE5.E5_PREFIXO AND "
				cQuery +=		"A.E5_NUMERO=SE5.E5_NUMERO AND "
				cQuery +=		"A.E5_PARCELA=SE5.E5_PARCELA AND "
				cQuery +=		"A.E5_TIPO=SE5.E5_TIPO AND "
				cQuery +=		"A.E5_CLIFOR=SE5.E5_CLIFOR AND "
				cQuery +=		"A.E5_LOJA=SE5.E5_LOJA AND "
				cQuery +=		"A.E5_SEQ=SE5.E5_SEQ AND "
				cQuery +=		"A.E5_TIPODOC='ES' AND "
				cQuery +=		"A.E5_RECPAG<>'R' AND "
				cQuery +=		"A.D_E_L_E_T_= ' ')"

				//�������������������������������������������������������������������������Ŀ
				//� Ponto de entrada para alteracao da cQuery na consulta Titulos Recebidos �
				//���������������������������������������������������������������������������
				If (ExistBlock("F010CQTR"))    
					cQuery := ExecBlock("F010CQTR",.F.,.F.,{cQuery})
				Endif

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"A"

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)

				aEval(aQuery,{|x| If(x[2]!="C",TcSetField(cQry,x[1],x[2],x[3],x[4]),Nil)})

				dbSelectArea(cQry)
				bWhile := {|| !Eof() }
				bFiltro:= {|| (	(cQry)->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" .AND.;
				IIf( UPPER(SUBSTR((cQry)->E5_HISTOR,1,4)) <> "LOJ-", .T.,;
				( ( (cQry)->E1_NUM == cNumAnt .AND. (cQry)->E5_TIPODOC == cTpDocAnt ) .OR.;
				( (cQry)->E1_NUM == cNumAnt .AND. (cQry)->E1_PARCELA <> cParcAnt  ) .OR.;
				(cQry)->E1_NUM <> cNumAnt ) ) ) }
				cAnterior := ""
				While ( Eval(bWhile) )				
					If ( Eval(bFiltro) )
						cNumAnt := (cQry)->E1_NUM
						cTpDocAnt := (cQry)->E5_TIPODOC
						cParcAnt := (cQry)->E1_PARCELA
						dbSelectArea(cAlias)
						dbSetOrder(1)
						RecLock(cAlias,.T.)
						For nCntFor := 1 To Len(aStru)
							Do Case
								Case ( AllTrim(aStru[nCntFor][1])=="E1_PAGO" )
								If ( (cQry)->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" )
									(cAlias)->E1_PAGO += (cQry)->E5_VALOR
								EndIf
								Case ( AllTrim(aStru[nCntFor][1])=="E1_VLMOED2" ) .And. !lRelat
								If ( (cQry)->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" ) .And. (cQry)->E1_MOEDA > 1
									(cAlias)->E1_VLMOED2 := If((cQry)->E1_MOEDA > 1, (cQry)->E5_VLMOED2, (cQry)->E5_VALOR)
								EndIf	
								Case ( AllTrim(aStru[nCntFor][1])=="E1_SALDO2" )
								If ( (cQry)->(E1_TIPO)	$ MVABATIM )
									(cAlias)->E1_SALDO2 -= nTotAbat
								Else
									(cAlias)->E1_SALDO2 += (cQry)->(E1_SALDO)
									(cAlias)->E1_SALDO2 += (cAlias)->E1_SDACRES - (cAlias)->E1_SDDECRE
								EndIf
								If ( !lQuery )
									(cAlias)->E1_SALDO2 -= nTotAbat
								EndIf	
								Case ( AllTrim(aStru[nCntFor][1])=="E5_VLJUROS" ) .AND. !lRelat
								(cAlias)->E5_VLJUROS := (cQry)->E5_VLJUROS - (cQry)->E5_VLACRES
								Case ( AllTrim(aStru[nCntFor][1])=="E5_VLDESCO" ) .AND. !lRelat
								(cAlias)->E5_VLDESCO := (cQry)->E5_VLDESCO - (cQry)->E5_VLDECRE
								Case ( AllTrim(aStru[nCntFor][1])=="E1_VLCRUZ" )
								If ( (cQry)->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" )
									If cAnterior != (cQry)->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
										(cAlias)->E1_VLCRUZ := (cQry)->E1_VLCRUZ
										cAnterior := (cQry)->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
									Endif
								EndIf										
								Case ( AllTrim(aStru[nCntFor][1])=="E1_ATR" )
								If (cQry)->E5_DATA > DataValida((cQry)->E1_VENCREA,.T.)
									(cAlias)->E1_ATR := (cQry)->E5_DATA - (cQry)->E1_VENCREA
								Else
									(cAlias)->E1_ATR := (cQry)->E5_DATA - DataValida((cQry)->E1_VENCREA,.T.)
								Endif
								Case cPaisLoc=="BRA" .And. ( AllTrim(aStru[nCntFor][1])=="E5_TXMOEDA" )  .And. !lRelat
								If (cQry)->E1_MOEDA == 1
									(cAlias)->E5_TXMOEDA := 1
								Else
									If (cQry)->E5_TXMOEDA == 0 
										(cAlias)->E5_TXMOEDA := ((cQry)->E5_VALOR /(cQry)->E5_VLMOED2)
									Else
										(cAlias)->E5_TXMOEDA := (cQry)->E5_TXMOEDA
									Endif
								Endif
								Case ( !lRelat .And. AllTrim(aStru[nCntFor][1])=="E1_NUMLIQ" )
								If (cQry)->E5_MOTBX == "LIQ"
									If Empty( (cQry)->E1_NUMLIQ )
										(cAlias)->E1_NUMLIQ := SUBSTR((cQry)->E5_DOCUMEN,1,TamSx3("E1_NUMLIQ")[1])
									Else
										(cAlias)->(FieldPut(nCntFor,(cQry)->(FieldGet(FieldPos(aStru[nCntFor][1])))))			
									Endif		
								Endif
								Case ( AllTrim(aStru[nCntFor][1])=="XX_RECNO" )
								(cAlias)->XX_RECNO := (cQry)->SE5RECNO

								Case ( AllTrim(aStru[nCntFor][1])=="FLAG" )

								OtherWise
								(cAlias)->(FieldPut(nCntFor,(cQry)->(FieldGet(FieldPos(aStru[nCntFor][1])))))
							EndCase
						Next nCntFor
						(cAlias)->(MsUnLock())
					EndIf
					dbSelectArea(cQry)
					dbSkip()				
				EndDo
				dbSelectArea(cQry)
				dbCloseArea()
			Else
				#ENDIF
				dbSelectArea("SE1")
				dbSetOrder(2)
				If aParam[13] == 1  //Considera loja
					dbSeek(xFilial("SE1")+SA1->A1_COD+SA1->A1_LOJA)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE .And.;
					SA1->A1_LOJA   == SE1->E1_LOJA }
				Else
					dbSeek(xFilial("SE1")+SA1->A1_COD)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE }
				Endif

				bFiltro:= {|| 	SubStr(SE1->E1_TIPO,3,1)!="-" .And.;
				SE1->E1_EMISSAO >= aParam[1] .And.;
				SE1->E1_EMISSAO <= aParam[2] .And.;
				SE1->E1_VENCREA >= aParam[3] .And.;
				SE1->E1_VENCREA <= aParam[4] .And.;
				If(aParam[5]==2,SE1->E1_TIPO!="PR ",.T.) .And.;
				If(aParam[15]==2,!SE1->E1_TIPO$MVRECANT,.T.) .And.;									
				SE1->E1_PREFIXO >= aParam[6] .And.;
				SE1->E1_PREFIXO <= aParam[7] .And.;
				IIf(cPaisLoc == "BRA",(SE1->E1_ORIGEM<>"FINA087A"),!(SE1->E1_TIPO$cCheques)) }

				While ( Eval(bWhile) )
					If ( Eval(bFiltro) )
						dbSelectArea("SE5")
						dbSetOrder(7)
						dbSeek(xFilial("SE5")+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+SE1->E1_TIPO+SE1->E1_CLIENTE+SE1->E1_LOJA)

						While ( !Eof() .And. xFilial("SE5") == SE5->E5_FILIAL .And.;
						SE1->E1_PREFIXO == SE5->E5_PREFIXO .And.;
						SE1->E1_NUM == SE5->E5_NUMERO      .And.;
						SE1->E1_PARCELA == SE5->E5_PARCELA .And.;
						SE1->E1_TIPO == SE5->E5_TIPO       .And.;
						SE1->E1_CLIENTE == SE5->E5_CLIFOR	.And.;
						SE1->E1_LOJA == SE5->E5_LOJA )

							// Baixas efetuadas por liquidacao ou por fatura, se nao devem ser consideradas despreza
							If (aParam[08] == 2 .And. SE5->E5_MOTBX == "FAT") .Or.;
							(aParam[09] == 2 .And. SE5->E5_MOTBX == "LIQ")
								dbSelectArea("SE5")
								dbSkip()	
								Loop
							Endif

							If ((!SE5->E5_TIPO $ "RA /PA /"+MV_CRNEG+"/"+MV_CPNEG) .And. !TemBxCanc() .and. ;
							SE5->E5_SITUACA <> "C" .And. SE5->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$")
								RecLock(cAlias,.T.)
								For nCntFor := 1 To Len(aStru)
									Do Case
										Case ( AllTrim(aStru[nCntFor][1])=="E1_PAGO" )
										If ( SE5->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" )
											(cAlias)->E1_PAGO += SE5->E5_VALOR
										EndIf
										Case ( AllTrim(aStru[nCntFor][1])=="E1_VLMOED2" ) .And. !lRelat
										If ( SE5->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" ) .And. SE1->E1_MOEDA > 1
											(cAlias)->E1_VLMOED2 := If(SE1->E1_MOEDA > 1, SE5->E5_VLMOED2, SE5->E5_VALOR)
										EndIf		
										Case ( AllTrim(aStru[nCntFor][1])=="E1_VLCRUZ" )
										If ( SE5->E5_TIPODOC $ "VL/BA/V2/CP/LJ/R$" )
											If cAnterior != SE1->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
												(cAlias)->E1_VLCRUZ := SE1->E1_VLCRUZ
												cAnterior := SE1->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
											Endif
										Endif	
										Case ( AllTrim(aStru[nCntFor][1])=="E1_ATR" )
										If SE5->E5_DATA > DataValida(SE1->E1_VENCREA,.T.)
											(cAlias)->E1_ATR := SE5->E5_DATA - SE1->E1_VENCREA
										Else
											(cAlias)->E1_ATR := SE5->E5_DATA - DataValida(SE1->E1_VENCREA,.T.)
										Endif
										Case cPaisLoc=="BRA" .And. ( AllTrim(aStru[nCntFor][1])=="E5_TXMOEDA" ) .And. !lRelat
										If SE1->E1_MOEDA == 1
											(cAlias)->E5_TXMOEDA := 1
										Else
											If SE5->E5_TXMOEDA == 0 
												(cAlias)->E5_TXMOEDA := (SE5->E5_VALOR /SE5->E5_VLMOED2)
											Else
												(cAlias)->E5_TXMOEDA := SE5->E5_TXMOEDA
											Endif
										Endif
										Case ( AllTrim(aStru[nCntFor][1])=="E1_NUMLIQ" )
										If SE5->E5_MOTBX == "LIQ"
											If Empty(SE1->E1_NUMLIQ)
												(cAlias)->E1_NUMLIQ := SUBSTR(SE5->E5_DOCUMEN,1,TamSx3("E1_NUMLIQ")[1])
											Else
												(cAlias)->(FieldPut(nCntFor,SE1->(FieldGet(FieldPos(aStru[nCntFor][1])))))
											Endif
										Endif
										Case ( AllTrim(aStru[nCntFor][1])=="XX_RECNO" )
										(cAlias)->XX_RECNO := SE5->(RecNo())
										Case ( "E5_"$AllTrim(aStru[nCntFor][1]) )
										(cAlias)->(FieldPut(nCntFor,SE5->(FieldGet(FieldPos(aStru[nCntFor][1])))))
										Case ( AllTrim(aStru[nCntFor][1])=="FLAG" )
										OtherWise
										(cAlias)->(FieldPut(nCntFor,SE1->(FieldGet(FieldPos(aStru[nCntFor][1])))))
									EndCase
								Next nCntFor
								(cAlias)->(MsUnLock())
							EndIf
							dbSelectArea("SE5")
							dbSkip()
						EndDo										
					EndIf
					dbSelectArea("SE1")
					dbSkip()					
				EndDo
				#IFDEF TOP
			EndIf					
			#ENDIF
			dbSelectArea(cAlias)
			IndRegua(cAlias,cArquivo,"DTOS(E5_DATA)") 
		EndIf
		//������������������������������������������������������������������������Ŀ
		//�Totais da Consulta                                                      �
		//��������������������������������������������������������������������������
		aGet[1] := 0
		aGet[2] := 0
		aGet[3] := 0
		aTotPag := {{0,1,0,0}} // Totalizador de titulos recebidos por por moeda
		dbSelectArea(cAlias)
		dbGotop()
		_FC010TotRc(aGet,cAlias,aTotPag,lRelat)  // Totais de Baixas
		If !lRelat
			nTotalRec:=0
			aEval(aTotPag,{|e| nTotalRec+=e[VALORREAIS]})
			Aadd(aTotPag,{"","",STR0094,nTotalRec}) //"Total ====>>"
			// Formata as colunas
			aEval(aTotPag,{|e|	If(ValType(e[VALORTIT]) == "N"	, e[VALORTIT]		:= Transform(e[VALORTIT],Tm(e[VALORTIT],16,nCayas)),Nil),;
			If(ValType(e[VALORREAIS]) == "N"	, e[VALORREAIS]	:= Transform(e[VALORREAIS],Tm(e[VALORREAIS],16,nCayas)),Nil)})
		Endif
		aGet[1] := TransForm(aGet[1],Tm(aGet[1],16,0))
		aGet[2] := TransForm(aGet[2],Tm(aGet[2],16,nCayas))
		aGet[3] := TransForm(aGet[3],Tm(aGet[3],16,nCayas))
		//������������������������������������������������������������������������Ŀ
		//�Pedidos                                                                 �
		//��������������������������������������������������������������������������
		Case ( nBrowse == 3 )
		dbSelectArea("SX3")
		dbSetOrder(2)                                   	
		If aParam[13] == 1  //Considera loja
			dbSeek("C5_LOJACLI")
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
			aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Endif
		/*
		GESTAO - inicio */
		dbSeek("C5_FILIAL")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		/* GESTAO - fim
		*/
		dbSeek("C5_NUM")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("C5_EMISSAO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("C6_VALOR")
		aadd(aHeader,{STR0040,"XX_SLDTOT",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } ) //"Tot.Pedido"
		aadd(aStru ,{"XX_SLDTOT",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aHeader,{STR0041,"XX_SLDLIB",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } ) //"Sld.Liberado"
		aadd(aStru ,{"XX_SLDLIB",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aHeader,{STR0042,"XX_SLDPED",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } ) //"Sld.Pedido"
		aadd(aStru ,{"XX_SLDPED",SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aStru,{"XX_RECNO","N",12,0})

		IF lFC010Pedi
			aRetAux := aClone(ExecBlock("FC010PEDI",.F.,.F.,{aClone(aHeader),aClone(aStru)}))
			If ValType(aRetAux) <> "A" .or. Len(aRetAux) <> 2
				aRetAux := {{},{}}
			Else
				aEval(aRetAux[1] , {|x| aAdd(aHeader , x ) })
				aEval(aRetAux[2] , {|x| aAdd(aStru , x ) })
			EndIf
		EndIf

		SX3->(dbSetOrder(1))

		If ( Select(cAlias) ==	0 )
			cArquivo := CriaTrab(,.F.)			
			aadd(aAlias,{ cAlias , cArquivo })
			aadd(aStru,{"FLAG","L",01,0})
			dbCreate(cArquivo,aStru)
			dbUseArea(.T.,,cArquivo,cAlias,.F.,.F.)
			IndRegua(cAlias,cArquivo,"C5_FILIAL+C5_NUM")

			#IFDEF TOP
			If ( TcSrvType()!="AS/400" )
				cCompSC5 := FWModeAccess("SC5",1) + FWModeAccess("SC5",2) + FWModeAccess("SC5",3)

				//Caso de compartilhamentos id�nticos
				If FWModeAccess("SC6",3) == FWModeAccess("SF4",3) .And. FWModeAccess("SC6",2) ==  FWModeAccess("SF4",2) .And. FWModeAccess("SC6",1) ==  FWModeAccess("SF4",1)
					lFilF4C6 := .T.
				EndIf

				IF FWModeAccess("SF4",1) == "C"
					lFilF4 := .T.
				ElseIf FWModeAccess("SC6",3) == "E"
					lFilF4 := .T.
				ElseIf FWModeAccess("SC6",1) == "C" 
					lFilF4 := .F.
				ElseIf FWModeAccess("SF4",2) == "E" .AND. !(FWModeAccess("SF4",3) == "C" .AND. FWModeAccess("SC6",2) == "E")
					lFilF4 := .F.
				Else
					lFilF4 := .T.
				EndIf

				lQuery := .T.
				cQuery := "SELECT SC5.C5_FILIAL,SC5.C5_NUM PEDIDO,"
				cQuery += 		  "SC5.C5_EMISSAO EMISSAO," 
				//*************************************
				// Monta campos do ponro de entrada   * 
				// FC010PEDI na tabela temporaria.    *
				//*************************************
				IF lFC010Pedi
					aEval(aRetAux[2] , {|x| cQuery += x[1] + "," })
				EndIf
				cQuery +=  		  "SC5.C5_MOEDA MOEDA,"    
				cQuery +=  		  "SC5.C5_FRETE FRETE,"
				cQuery += 		  "SC5.R_E_C_N_O_ SC5RECNO,"
				cQuery += 		  "(C6_QTDVEN-C6_QTDEMP-C6_QTDENT) QTDVEN,"
				cQuery +=		  "C6_PRCVEN PRCVEN,"
				cQuery +=         "1 TIPO,"
				cQuery +=         "C5_EMISSAO DATALIB," //NAO RETIRAR - POSTGRES
				cQuery +=         "C6_BLQ BLCRED "		//NAO RETIRAR - POSTGRES
				cQuery += "FROM "+RetSqlName("SC5")+" SC5,"
				cQuery +=         RetSqlName("SC6")+" SC6,"
				cQuery +=         RetSqlName("SF4")+" SF4 "
				/*
				GESTAO - inicio */
				nPosAlias := _FC010QFil(1,"SC5")
				cQuery += "WHERE SC5.C5_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				/* GESTAO - fim
				*/
				cQuery += 		"SC5.C5_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=		"SC5.C5_LOJACLI='"+SA1->A1_LOJA+"' AND "
				Endif

				//�����������������������������������������������������Ŀ
				//� Ponto de entrada para filtrar pelo MSFIL em caso de �
				//� arquivo compartilhado.  Pedidos                     �
				//�������������������������������������������������������

				If (ExistBlock("F010FLPD"))    
					cQuery += ExecBlock("F010FLPD",.F.,.F.)
				Endif                    

				cQuery +=		"SC5.C5_TIPO NOT IN('D','B') AND "
				cQuery +=		"SC5.C5_EMISSAO >='"+Dtos(aParam[1])+"' AND "
				cQuery +=		"SC5.C5_EMISSAO <='"+Dtos(aParam[2])+"' AND "	
				cQuery +=		"SC5.D_E_L_E_T_ = ' ' AND "
				/*
				GESTAO - inicio */
				If (FWModeAccess("SC6",1) + FWModeAccess("SC6",2) + FWModeAccess("SC6",3)) == cCompSC5
					cQuery += "SC6.C6_FILIAL = SC5.C5_FILIAL AND "
				Else
					nPosAlias := _FC010QFil(1,"SC6")
					cQuery += "SC6.C6_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				Endif
				/* GESTAO - fim
				*/
				cQuery +=		"SC6.C6_NUM=SC5.C5_NUM AND "
				cQuery +=		"SC6.C6_BLQ NOT IN('R ') AND "
				If aParam[10] == 2 // nao considera pedidos com bloqueio
					cQuery +=		"SC6.C6_BLQ NOT IN('S ') AND "
				Endif
				cQuery +=		"(SC6.C6_QTDVEN-SC6.C6_QTDEMP-SC6.C6_QTDENT)>0 AND "
				cQuery +=		"SC6.D_E_L_E_T_ = ' ' AND "

				If lFilF4C6
					cQuery		+=		"SF4.F4_FILIAL=SC6.C6_FILIAL AND "
				Else
					If !lFilF4
						cQuery +=		"SF4.F4_FILIAL='"+xFilial("SF4")+"' AND "
					Else
						cQuery +=		"SF4.F4_FILIAL='"+xFilial("SF4",SC6->C6_FILIAL)+"' AND "
					EndIf
				EndIf

				cQuery +=		"SF4.F4_CODIGO=SC6.C6_TES AND "
				//����������������������������������������������������������������Ŀ
				//� Considera sim, n�o ou ambos os itens com TES gerando duplicata �
				//������������������������������������������������������������������
				If aParam[14] == 2
					cQuery +=		"SF4.F4_DUPLIC='S' AND "
				ElseIf aParam[14] == 3                   
					cQuery +=		"SF4.F4_DUPLIC='N' AND "
				Endif
				cQuery +=		"SF4.D_E_L_E_T_ = ' ' "
				cQuery += "UNION ALL "
				cQuery += "SELECT C5_FILIAL,C5_NUM PEDIDO,"
				cQuery += 		  "C5_EMISSAO EMISSAO,"
				//*************************************
				// Monta campos do ponro de entrada   * 
				// FC010PEDI na tabela temporaria.    *
				//*************************************
				IF lFC010Pedi
					aEval(aRetAux[2] , {|x| cQuery += x[1] + "," })
				EndIf
				cQuery += 		  "C5_MOEDA MOEDA,"
				cQuery +=  		  "C5_FRETE FRETE,"
				cQuery += 		  "SC5.R_E_C_N_O_ SC5RECNO,"					
				cQuery += 		  "C9_QTDLIB QTDVEN,"
				cQuery +=		  "C9_PRCVEN PRCVEN, "
				cQuery +=         "2 TIPO,"										
				cQuery +=		  "C9_DATALIB DATALIB, "
				cQuery +=		  "C9_BLCRED BLCRED "
				cQuery += "FROM "+RetSqlName("SC5")+" SC5,"+RetSqlName("SC6")+" SC6,"
				cQuery +=         RetSqlName("SF4")+" SF4,"+RetSqlName("SC9")+" SC9 "
				/*
				GESTAO - inicio */
				nPosAlias := _FC010QFil(1,"SC5")
				cQuery += "WHERE SC5.C5_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				/* GESTAO - fim
				*/
				cQuery += 		"SC5.C5_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=		"SC5.C5_LOJACLI='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=		"SC5.C5_TIPO NOT IN('D','B') AND "
				cQuery +=		"SC5.C5_EMISSAO >='"+Dtos(aParam[1])+"' AND "
				cQuery +=		"SC5.C5_EMISSAO <='"+Dtos(aParam[2])+"' AND "						
				cQuery +=		"SC5.D_E_L_E_T_ =  ' ' AND "
				/*
				GESTAO - inicio */
				If (FWModeAccess("SC6",1) + FWModeAccess("SC6",2) + FWModeAccess("SC6",3)) == cCompSC5
					cQuery += "SC6.C6_FILIAL = SC5.C5_FILIAL AND "
				Else
					nPosAlias := _FC010QFil(1,"SC6")
					cQuery += "SC6.C6_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				Endif
				/* GESTAO - fim
				*/
				cQuery +=		"SC6.C6_NUM=SC5.C5_NUM AND "
				cQuery +=		"SC6.D_E_L_E_T_ = ' ' AND "
				cQuery +=		"SC6.C6_BLQ NOT IN('R ') AND "
				If aParam[10] == 2 // nao considera pedidos com bloqueio
					cQuery +=		"SC6.C6_BLQ NOT IN('S ') AND "
				Endif
				/*
				GESTAO - inicio */
				nPosAlias := _FC010QFil(1,"SF4")
				cQuery += "SF4.F4_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				/* GESTAO - fim
				*/
				cQuery +=		"SF4.F4_CODIGO=SC6.C6_TES AND "
				//����������������������������������������������������������������Ŀ
				//� Considera sim, n�o ou ambos os itens com TES gerando duplicata �
				//������������������������������������������������������������������
				If aParam[14] == 2
					cQuery +=		"SF4.F4_DUPLIC='S' AND "
				ElseIf aParam[14] == 3                   
					cQuery +=		"SF4.F4_DUPLIC='N' AND "
				Endif
				cQuery +=		"SF4.D_E_L_E_T_ = ' ' AND "
				/*
				GESTAO - inicio */
				If (FWModeAccess("SC9",1) + FWModeAccess("SC9",2) + FWModeAccess("SC9",3)) == cCompSC5
					cQuery += "SC9.C9_FILIAL = SC5.C5_FILIAL AND "
				Else
					nPosAlias := _FC010QFil(1,"SC9")
					cQuery += "SC9.C9_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				Endif
				/* GESTAO - fim
				*/
				cQuery +=		"SC9.C9_PEDIDO=SC5.C5_NUM AND "
				cQuery +=		"SC9.C9_ITEM=SC6.C6_ITEM AND "
				cQuery +=		"SC9.C9_PRODUTO=SC6.C6_PRODUTO AND "		
				cQuery +=		"SC9.C9_NFISCAL='"+Space(Len(SC9->C9_NFISCAL))+"' AND "
				cQuery +=		"SC9.D_E_L_E_T_ = ' '"

				//������������������������������������������������������������������Ŀ
				//� Ponto de entrada para alteracao da cQuery na consulta de Pedidos �
				//��������������������������������������������������������������������
				If (ExistBlock("F010CQPE"))    
					cQuery := ExecBlock("F010CQPE",.F.,.F.,{cQuery})
				Endif 

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"A"

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)

				TcSetField(cQry,"EMISSAO","D")
				TcSetField(cQry,"DATALIB","D")
				TcSetField(cQry,"TIPO","N",1)
				TcSetField(cQry,"SC5RECNO","N",12,0) 
				TcSetField(cQry,"QTDVEN","N",TamSx3("C6_QTDVEN")[1],TamSx3("C6_QTDVEN")[2])
				TcSetField(cQry,"PRCVEN","N",TamSx3("C6_PRCVEN")[1],TamSx3("C9_PRCVEN")[2]) 

				//*************************************
				// Monta campos do ponro de entrada   * 
				// FC010PEDI na tabela temporaria.    *
				//*************************************
				IF lFC010Pedi
					aEval(aRetAux[2] , {|x| TcSetField(cQry,x[1],x[2],x[3],x[4]) })
				EndIf

				dbSelectArea(cQry)
				bWhile := {|| !Eof() }
				bFiltro:= {|| .T. }
				While ( Eval(bWhile) )				
					If ( Eval(bFiltro) )
						dbSelectArea(cAlias)
						dbSetOrder(1)
						cChave := (cQry)->(C5_FILIAL + PEDIDO)
						If ( !dbSeek(cChave) )
							RecLock(cAlias,.T.)
						Else
							RecLock(cAlias,.F.)
						EndIf
						If ( (cQry)->TIPO == 1 )
							(cAlias)->C5_FILIAL  := (cQry)->C5_FILIAL
							(cAlias)->C5_NUM     := (cQry)->PEDIDO
							(cAlias)->C5_EMISSAO := (cQry)->EMISSAO
							(cAlias)->XX_SLDTOT  += xMoeda((cQry)->QTDVEN*(cQry)->PRCVEN,(cQry)->MOEDA,1,(cQry)->EMISSAO)
							IF EMPTY(cSaldo) .or. cSaldo <> cChave
								(cAlias)->XX_SLDTOT  += xMoeda((cQry)->FRETE,(cQry)->MOEDA,1,(cQry)->EMISSAO)	// Adiciona frete ao total
							ENDIF	
							(cAlias)->XX_SLDLIB  := 0
							(cAlias)->XX_SLDPED  := (cAlias)->XX_SLDTOT
						Else
							(cAlias)->C5_FILIAL  := (cQry)->C5_FILIAL
							(cAlias)->C5_NUM     := (cQry)->PEDIDO
							(cAlias)->C5_EMISSAO := (cQry)->EMISSAO						
							If ( Empty((cQry)->BLCRED) )
								(cAlias)->XX_SLDLIB  += xMoeda((cQry)->QTDVEN*(cQry)->PRCVEN,(cQry)->MOEDA,1,(cQry)->DATALIB)
								IF EMPTY(cSaldo) .or. cSaldo <> cChave
									(cAlias)->XX_SLDLIB  += xMoeda((cQry)->FRETE,(cQry)->MOEDA,1,(cQry)->EMISSAO)  // Adiciona frete ao total
								ENDIF
								(cAlias)->XX_SLDTOT  += xMoeda((cQry)->QTDVEN*(cQry)->PRCVEN,(cQry)->MOEDA,1,(cQry)->DATALIB)
								IF EMPTY(cSaldo) .or. cSaldo <> cChave
									(cAlias)->XX_SLDTOT  += xMoeda((cQry)->FRETE,(cQry)->MOEDA,1,(cQry)->EMISSAO)  // Adiciona frete ao total
								ENDIF
							Else
								(cAlias)->XX_SLDTOT  += xMoeda((cQry)->QTDVEN*(cQry)->PRCVEN,(cQry)->MOEDA,1,(cQry)->DATALIB)
								IF EMPTY(cSaldo) .or. cSaldo <> cChave
									(cAlias)->XX_SLDTOT  += xMoeda((cQry)->FRETE,(cQry)->MOEDA,1,(cQry)->EMISSAO)  // Adiciona frete ao total
								ENDIF	
								(cAlias)->XX_SLDPED  += xMoeda((cQry)->QTDVEN*(cQry)->PRCVEN,(cQry)->MOEDA,1,(cQry)->DATALIB)           
								IF Empty (cSaldo) .or. cSaldo <> cChave          
									(cAlias)->XX_SLDPED  += xMoeda((cQry)->FRETE,(cQry)->MOEDA,1,(cQry)->EMISSAO)  // Adiciona frete ao total
								EndIf
							EndIf
						EndIf
						(cAlias)->XX_RECNO := (cQry)->SC5RECNO
						//*************************************
						// Monta campos do ponro de entrada   * 
						// FC010PEDI na tabela temporaria.    *
						//*************************************
						IF lFC010Pedi
							aEval(aRetAux[2] , {|x| (cAlias)->&(x[1]) := (cQry)->&(x[1])  })
						EndIf
						(cAlias)->(MsUnLock())
						cSaldo := cChave
					EndIf
					dbSelectArea(cQry)
					dbSkip()				
				EndDo
				dbSelectArea(cQry)
				dbCloseArea()			
			Else
				#ENDIF
				dbSelectArea("SC5")
				dbSetOrder(3)
				dbSeek(xFilial("SC5")+SA1->A1_COD)
				While ( !Eof() .And. SC5->C5_FILIAL==xFilial("SC5") .And.;
				SC5->C5_CLIENTE == SA1->A1_COD )
					nSalPed := 0
					nSalPedb:= 0
					nSalPedL:= 0
					nQtdPed := 0											
					If ( If(aParam[13] == 1,SC5->C5_LOJACLI == SA1->A1_LOJA,.T.) .And. !(SC5->C5_TIPO $ "DB") .And. SC5->C5_EMISSAO >= aParam[1] .And. C5_EMISSAO <= aParam[2] )
						dbSelectArea("SC6")
						dbSetOrder(1)
						dbSeek(xFilial("SC6")+SC5->C5_NUM)
						While ( !Eof() .And. SC6->C6_FILIAL == xFilial('SC5') .And.;
						SC6->C6_NUM == SC5->C5_NUM )
							If ( !AllTrim(SC6->C6_BLQ) $ "R"+If(aParam[10]==2,"#S",""))
								dbSelectArea("SF4")
								dbSetOrder(1)
								dbSeek(cFilial+SC6->C6_TES)
								//�����������������������������������������������������������Ŀ
								//� Buscar Qtde no arquivo SC9 (itens liberados) p/ A1_SALPEDL�
								//�������������������������������������������������������������
								dbSelectArea("SC9")
								dbSetOrder(2)
								dbSeek(xFilial("SC9")+SC6->C6_CLI+SC6->C6_LOJA+SC6->C6_NUM+SC6->C6_ITEM)

								//����������������������������������������������������������������Ŀ
								//� Considera sim, n�o ou ambos os itens com TES gerando duplicata �
								//������������������������������������������������������������������
								If aParam[14] == 2
									bCond := { || SF4->F4_DUPLIC == "S" }
								ElseIf aParam[14] == 3
									bCond := { || SF4->F4_DUPLIC == "N" }
								Else
									bCond := { || .T.}
								Endif

								If Eval(bCond)
									While ( !Eof() .And. xFilial("SC9") == SC9->C9_FILIAL .And.;
									SC6->C6_CLI == SC9->C9_CLIENTE .And.;
									SC6->C6_LOJA == SC9->C9_LOJA .And.;
									SC6->C6_NUM == SC9->C9_PEDIDO .And.;
									SC6->C6_ITEM == SC9->C9_ITEM )
										If ( Empty(C9_NFISCAL) .And. SC6->C6_PRODUTO==SC9->C9_PRODUTO )
											If ( Empty(SC9->C9_BLCRED) )
												nSalpedl += xMoeda( SC9->C9_QTDLIB * SC9->C9_PRCVEN , SC5->C5_MOEDA , 1 , SC9->C9_DATALIB )
												nSalpedl += xMoeda( SC5->C5_FRETE , SC5->C5_MOEDA , 1 , SC9->C9_DATALIB )
											Else
												nSalpedb += xMoeda( SC9->C9_QTDLIB * SC9->C9_PRCVEN , SC5->C5_MOEDA , 1 , SC9->C9_DATALIB )
												nSalpedb += xMoeda( SC5->C5_FRETE , SC5->C5_MOEDA , 1 , SC9->C9_DATALIB )
											EndIf
										EndIf
										dbSelectArea("SC9")
										dbSkip()
									EndDo
								Endif
								If Eval(bCond)
									nQtdPed := SC6->C6_QTDVEN - SC6->C6_QTDEMP - SC6->C6_QTDENT
									nQtdPed := IIf( nQtdPed < 0 , 0 , nQtdPed )
									nSalped += xMoeda( nQtdPed * SC6->C6_PRCVEN , SC5->C5_MOEDA , 1 , SC5->C5_EMISSAO )
									nSalped += xMoeda( SC5->C5_FRETE , SC5->C5_MOEDA , 1 , SC5->C5_EMISSAO )
								EndIf
							EndIf
							dbSelectArea("SC6")
							dbSkip()
						EndDo
					EndIf
					If ( nSalped+nSalpedl+nSalpedb > 0 )
						RecLock(cAlias,.T.)
						(cAlias)->C5_NUM     := SC5->C5_NUM
						(cAlias)->C5_EMISSAO := SC5->C5_EMISSAO
						(cAlias)->XX_SLDTOT  := nSalPed+nSalPedL+nSalPedb
						(cAlias)->XX_SLDLIB  := nSalPedL
						(cAlias)->XX_SLDPED  := nSalPed+nSalPedb
						(cAlias)->XX_RECNO    := SC5->(RecNo())
						MsUnlock()
					EndIf
					dbSelectArea("SC5")
					dbSkip()
				EndDo
				#IFDEF TOP
			EndIf
			#ENDIF
		EndIf
		dbSelectArea(cAlias)
		dbGotop()
		aGet[1] := 0
		aGet[2] := 0
		aGet[3] := 0
		aGet[4] := 0
		dbEval({|| 	aGet[1]++,;
		aGet[2]+=XX_SLDTOT,;
		aGet[3]+=XX_SLDLIB,;
		aGet[4]+=XX_SLDPED})

		aGet[1] := TransForm(aGet[1],Tm(aGet[1],16,0))
		aGet[2] := TransForm(aGet[2],Tm(aGet[2],16,nCayas))
		aGet[3] := TransForm(aGet[3],Tm(aGet[3],16,nCayas))
		aGet[4] := TransForm(aGet[4],Tm(aGet[3],16,nCayas))	
		//������������������������������������������������������������������������Ŀ
		//�Notas Fiscais                                                           �
		//��������������������������������������������������������������������������
		Case ( nBrowse == 4 )
		If cPaisLoc == "BRA"
			aCpos:={"F2_FILIAL","F2_SERIE","F2_DOC","F2_NFELETR","F2_EMISSAO","F2_DUPL","F2_VALFAT","F2_FRETE",;
			"F2_HORA","F2_TRANSP","A4_NREDUZ"}
		Else
			aCpos:={"F2_FILIAL","F2_SERIE","F2_DOC","F2_EMISSAO","F2_DUPL","F2_VALFAT","F2_FRETE",;
			"F2_HORA","F2_TRANSP","A4_NREDUZ","F2_MOEDA","F2_TXMOEDA"}
		EndIf

		If lFC0101FAT
			aAuxCpo := aClone(ExecBlock("FC0101FAT",.F.,.F.,{aCpos}))
			If ValType(aAuxCpo) == "A"
				aCpos := aClone(aAuxCpo)			
			EndIf
		EndIf

		dbSelectArea("SX3")
		dbSetOrder(2)
		For nI := 1 To Len(aCpos)
			dbSeek(aCpos[nI])
			aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
			aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		Next nI

		aadd(aStru,{"XX_RECNO","N",12,0})

		SX3->(dbSetOrder(1))

		If ( Select(cAlias) ==	0 )
			cArquivo := CriaTrab(,.F.)			
			aadd(aAlias,{ cAlias , cArquivo })
			aadd(aStru,{"FLAG","L",01,0})
			dbCreate(cArquivo,aStru)
			dbUseArea(.T.,,cArquivo,cAlias,.F.,.F.)
			IndRegua(cAlias,cArquivo,"F2_FILIAL+DTOS(F2_EMISSAO)+F2_SERIE+F2_DOC")

			#IFDEF TOP
			cDbMs	 := UPPER(TcGetDb())
			If ( TcSrvType()!="AS/400" .and. cDbMs!="POSTGRES")
				lQuery := .T.

				cQuery := "SELECT SF2.F2_FILIAL,SF2.F2_DOC F2_DOC,"
				cQuery += 		"  SF2.F2_SERIE F2_SERIE,"
				cQuery += 		"  SF2.F2_EMISSAO F2_EMISSAO,"
				cQuery +=		"  SF2.F2_DUPL F2_DUPL,"
				If cPaisLoc == "BRA"
					cQuery +=		"  SF2.F2_NFELETR F2_NFELETR,"	
				EndIf				
				cQuery += 		"  SF2.F2_VALFAT F2_VALFAT, "
				cQuery += 		"  SF2.F2_FRETE F2_FRETE, "
				cQuery += 		"  SF2.F2_HORA F2_HORA, "
				cQuery += 		"  SF2.F2_TRANSP F2_TRANSP, "
				If cPaisLoc <> "BRA"                        
					cQuery +=	"  SF2.F2_MOEDA F2_MOEDA, "
					cQuery +=	"  SF2.F2_TXMOEDA F2_TXMOEDA, "
				EndIf
				/*
				GESTAO - inicio */
				cQuery += 		"  SF2.R_E_C_N_O_ SF2RECNO "
				/* GESTAO - fim
				*/
				If lFC0102FAT
					cQuery += ", " + ExecBlock("FC0102FAT",.F.,.F.) + " "					
				EndIf

				cQuery += "FROM "+RetSqlName("SF2")+" SF2 "
				/*
				GESTAO - inicio */
				nPosAlias := _FC010QFil(1,"SF2")
				cQuery += " WHERE SF2.F2_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				/* GESTAO - fim
				*/
				cQuery += 		" SF2.F2_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=		" SF2.F2_LOJA='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=		" SF2.F2_TIPO NOT IN('D','B') AND "
				cQuery +=		" SF2.F2_EMISSAO>='"+DTOS(aParam[1])+"' AND "
				cQuery +=		" SF2.F2_EMISSAO<='"+DTOS(aParam[2])+"' AND "

				//�����������������������������������������������������Ŀ
				//� Ponto de entrada para filtrar pelo MSFIL em caso de �
				//� arquivo compartilhado.                              �
				//�������������������������������������������������������

				If (ExistBlock("FO10FLFT"))    
					cQuery += ExecBlock("FO10FLFT",.F.,.F.)
				Endif                    

				If aParam[14] == 3 // TES Duplic = N
					cQuery +=		" SF2.F2_VALFAT = 0 AND "					
				ElseIf aParam[14] == 2  // TES Duplic = S
					cQuery +=		" SF2.F2_VALFAT > 0 AND "					
				Endif												

				cQuery += " SF2.D_E_L_E_T_ = ' '"
				/*
				GESTAO
				*/
				//�������������������������������������������������������������������Ŀ
				//� Ponto de entrada para alteracao da cQuery na consulta Faturamento �
				//���������������������������������������������������������������������
				If (ExistBlock("F010CQFT"))
					cQuery := ExecBlock("F010CQFT",.F.,.F.,{cQuery})
				Endif

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"A"

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)

				TcSetField(cQry,"F2_EMISSAO","D")
				TcSetField(cQry,"F2_VALFAT","N",TamSx3("F2_VALFAT")[1],TamSx3("F2_VALFAT")[2])
				TcSetField(cQry,"F2_FRETE","N",TamSx3("F2_FRETE")[1],TamSx3("F2_FRETE")[2])
				TcSetField(cQry,"SF2RECNO","N",12,0)
				If cPaisLoc <> "BRA"
					TcSetField(cQry,"F2_MOEDA","N",TamSx3("F2_MOEDA")[1],TamSx3("F2_MOEDA")[2])
					TcSetField(cQry,"F2_TXMOEDA","N",TamSx3("F2_TXMOEDA")[1],TamSx3("F2_TXMOEDA")[2])
				EndIf                                                                                
			Else
				#ENDIF
				cQry := "SF2"

				#IFDEF TOP
			EndIf
			#ENDIF
			aGet[1] := 0
			aGet[2] := 0
			dbSelectArea(cQry)
			If ( !lQuery )
				dbSetOrder(2)
				If aParam[13] == 1  //Considera loja
					dbSeek(xFilial("SF2")+SA1->A1_COD+SA1->A1_LOJA)
					bWhile := {|| !Eof() .And. xFilial("SF2") == SF2->F2_FILIAL .And.;
					SA1->A1_COD == SF2->F2_CLIENTE .And.;
					SA1->A1_LOJA == SF2->F2_LOJA }
				Else                                
					dbSeek(xFilial("SF2")+SA1->A1_COD)
					bWhile := {|| !Eof() .And. xFilial("SF2") == SF2->F2_FILIAL .And.;
					SA1->A1_COD == SF2->F2_CLIENTE}
				Endif
				If aParam[14] == 3 // TES Duplic = N
					bFiltro:= {|| !SF2->F2_TIPO$"DB" .And.;
					SF2->F2_EMISSAO >= aParam[1]     .And.;
					SF2->F2_EMISSAO <= aParam[2]	 .And.;
					SF2->F2_VALFAT  =  0}
				ElseIf aParam[14] == 2  // TES Duplic = S
					bFiltro:= {|| !SF2->F2_TIPO$"DB" .And.;
					SF2->F2_EMISSAO >= aParam[1] .And.;
					SF2->F2_EMISSAO <= aParam[2] .And.;
					SF2->F2_VALFAT  >  0}
				Else // TES Duplic = Todas
					bFiltro:= {|| !SF2->F2_TIPO$"DB" .And.;
					SF2->F2_EMISSAO >= aParam[1] .And.;
					SF2->F2_EMISSAO <= aParam[2]}				
				Endif											
			Else
				bWhile := {|| !Eof() }
				bFiltro:= {|| .T. }
			EndIf			
			While ( Eval(bWhile) )				
				If ( Eval(bFiltro) )
					If !lQuery
						// Se nao for query, posiciona SA4 para obter o nome da transportadora
						SA4->(MsSeek(xFilial("SA4")+SF2->F2_TRANSP))
						Else		/* GESTAO */
						SA4->(MsSeek(xFilial("SA4",(cQry)->F2_FILIAL) + (cQry)->F2_TRANSP))
					Endif
					RecLock(cAlias,.T.)
					(cAlias)->F2_FILIAL  := (cQry)->F2_FILIAL
					(cAlias)->F2_SERIE   := (cQry)->F2_SERIE
					(cAlias)->F2_DOC     := (cQry)->F2_DOC
					(cAlias)->F2_EMISSAO := (cQry)->F2_EMISSAO
					(cAlias)->F2_DUPL    := (cQry)->F2_DUPL
					If cPaisLoc == "BRA"
						(cAlias)->F2_NFELETR := (cQry)->F2_NFELETR
					EndIf
					(cAlias)->F2_VALFAT  := (cQry)->F2_VALFAT
					(cAlias)->F2_FRETE   := (cQry)->F2_FRETE
					(cAlias)->F2_HORA    := (cQry)->F2_HORA
					(cAlias)->F2_TRANSP  := (cQry)->F2_TRANSP
					If cPaisLoc != "BRA"                      
						(cAlias)->F2_MOEDA    := (cQry)->F2_MOEDA
						(cAlias)->F2_TXMOEDA  := (cQry)->F2_TXMOEDA
					EndIf
					(cAlias)->A4_NREDUZ  := SA4->A4_NREDUZ
					(cAlias)->XX_RECNO   := If(lQuery,(cQry)->SF2RECNO,SF2->(RecNo()))
					(cAlias)->(MsUnLock())
					If lFC0103FAT
						ExecBlock("FC0103FAT",.F.,.F.,{cAlias,cQry})					
					EndIf
				EndIf
				dbSelectArea(cQry)
				dbSkip()				
			EndDo
			If ( lQuery )
				dbSelectArea(cQry)
				dbCloseArea()
			EndIf
		EndIf		
		aGet[1] := 0
		aGet[2] := 0
		dbSelectArea(cAlias)
		dbGotop()
		If cPaisLoc == "BRA"
			dbEval({|| aGet[1]++,aGet[2]+=F2_VALFAT})
		Else 
			dbEval({|| aGet[1]++,aGet[2]+=Iif(F2_MOEDA == 1,F2_VALFAT,xMoeda(F2_VALFAT,F2_MOEDA,1,F2_EMISSAO,MsDecimais(1),F2_TXMOEDA))})			
		EndIf
		aGet[1] := TransForm(aGet[1],Tm(aGet[1],16,0))
		aGet[2] := TransForm(aGet[2],Tm(aGet[2],16,nCayas))			

		//CHEQUES
		Case (nBrowse == 5)
		DEFINE MSDIALOG oCheq FROM   15,1 TO 170,272 TITLE STR0072 PIXEL //"Seleccion de parametros"
		@ 6 , 12 TO 65, 93 OF oCheq   LABEL STR0073 PIXEL //"Tipos de cheques a exhibir"

		If cPaisLoc$"ARG|COS"
			@ 13, 13 RADIO oTipo VAR nTipo ;
			PROMPT STR0062,STR0064,If(cPaisLoc="ARG",STR0088,STR0063),"Transito",STR0074 ;  //"Pendientes"###"Cobrados"###"Negociados"###"Transito"###"Todos"
			OF oCheq PIXEL SIZE 75,12
		Else
			@ 13, 13 RADIO oTipo VAR nTipo ;
			PROMPT STR0062,STR0064,If(cPaisLoc$"URU|BOL",STR0088,STR0063),STR0074 ;  //"Pendientes"###"Cobrados"###"Negociados"###"Todos"
			OF oCheq PIXEL SIZE 75,12
		Endif

		DEFINE SBUTTON FROM 45, 100 TYPE 1 ACTION oCheq:End() ENABLE OF oCheq //11,132
		ACTIVATE MSDIALOG oCheq CENTERED

		Do case
			Case nTipo 	== 	1
			bTipo	:=	{ || (cQry)->E1_SALDO > 0 }
			Case nTipo	==	2
			bTipo	:=	{ || !(FN022SITCB((cQry)->E1_SITUACA)[1]) .And. (cQry)->E1_SALDO == 0 }
			Case nTipo	==	3
			bTipo	:=	{ || (cQry)->E1_STATUS == "R" .And. FN022SITCB((cQry)->E1_SITUACA)[1] .And. (cQry)->E1_SALDO == 0}
			Case nTipo	==	4
			If cPaisLoc $ "ARG/COS"			
				bTipo	:=	{ || (cQry)->EL_TIPODOC $ cCheques .And. (cQry)->EL_TRANSIT == '1' .And. (cQry)->EL_DTENTRA == CTOD("//")}
			Else
				bTipo	:=	{ || .T. }
			Endif
			Case nTipo == 5	//Apenas ARG e COS
			bTipo	:=	{ || .T. }

		EndCase

		nMoeda := 1
		dbSelectArea("SX3")
		dbSetOrder(2)
		dbSeek("E1_STATUS")
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		aadd(aHeader,{AllTrim(X3TITULO()),"XX_ESTADO","@!",04,0,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru,{"XX_ESTADO","C",04,0})
		/*
		GESTAO - inicio */
		dbSeek("E1_FILORIG")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		/* GESTAO - fim
		*/
		dbSeek("E1_PREFIXO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_NUM")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_PARCELA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_NUMNOTA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_EMISSAO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_VALOR")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_SALDO")
		aadd(aHeader,{ AllTrim(X3TITULO()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_MOEDA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_VLCRUZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_VENCREA")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_NATUREZ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_PORTADO")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_BCOCHQ")
		aadd(aHeader,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		dbSeek("E1_HIST")
		aadd(aHeader,{ AllTrim(X3TITULO()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aStru ,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})

		//sitcob
		dbSeek("E1_SITUACA")
		aadd(aQuery,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		aadd(aHeader,{STR0045,"FRV_DESCRI","@!",40,0,"","","C","FRV","" } ) //"Situacao"
		aadd(aStru,{"FRV_DESCRI","C",40,0})

		aadd(aStru,{"XX_RECNO" ,"N",12,0})
		aadd(aStru,{"XX_VALOR" ,"N",18,0})


		SX3->(dbSetOrder(1))

		cArquivo := CriaTrab(,.F.)			
		If !lExibe
			aadd(aAlias,{ cAlias , cArquivo })
		Endif
		aadd(aStru,{"FLAG","L",01,0})
		dbCreate(cArquivo,aStru)
		dbUseArea(.T.,,cArquivo,cAlias,.F.,.F.)
		IndRegua(cAlias,cArquivo,"E1_PREFIXO+E1_NUM+E1_PARCELA")

		lNoChqTran := IIF(cPaisLoc $ "ARG|COS", (nTipo != 4) , .T.)
		If lnoChqTran

			#IFDEF TOP
			If ( TcSrvType()!="AS/400" )
				lQuery := .T.
				cQuery := ""
				aEval(aQuery,{|x| cQuery += ","+AllTrim(x[1])})
				cQuery := "SELECT "+SubStr(cQuery,2)
				cQuery +=         ",SE1.R_E_C_N_O_ SE1RECNO"
				cQuery +=         ",FRV.FRV_DESCRI "
				cQuery += "FROM "+RetSqlName("SE1")+" SE1,"
				cQuery +=         RetSqlName("FRV")+" FRV, "
				cQuery +=         RetSqlName("SX5")+" SX5 "
				/*
				GESTAO - inicio */
				nPosAlias := _FC010QFil(1,"SE1")
				cQuery += " WHERE SE1.E1_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				/* GESTAO - fim
				*/
				cQuery +=      "SE1.E1_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=      "SE1.E1_LOJA='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=		"SE1.E1_EMISSAO>='"+Dtos(aParam[1])+"' AND "
				cQuery +=		"SE1.E1_EMISSAO<='"+Dtos(aParam[2])+"' AND "
				cQuery +=		"SE1.E1_VENCREA>='"+Dtos(aParam[3])+"' AND "
				cQuery +=		"SE1.E1_VENCREA<='"+Dtos(aParam[4])+"' AND "
				cQuery +=		"SE1.E1_TIPO IN" + FormatIn(cCheques,"|") + " AND "
				cQuery +=		"SE1.E1_PREFIXO>='"+aParam[6]+"' AND "
				cQuery +=		"SE1.E1_PREFIXO<='"+aParam[7]+"' AND "
				cQuery +=		"SE1.D_E_L_E_T_ = ' ' AND "
				cQuery +=		"FRV.FRV_FILIAL = '"+xFilial("FRV")+"' AND "		//SITCOB
				cQuery +=		"FRV.FRV_CODIGO = SE1.E1_SITUACA AND "
				cQuery +=		"FRV.D_E_L_E_T_ = ' '"            

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"A"

				MsAguarde({ || dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)},STR0065) //"Seleccionado registros en el servidor"

				aEval(aQuery,{|x| If(x[2]!="C",TcSetField(cQry,x[1],x[2],x[3],x[4]),Nil)})
			Else
				#ENDIF
				cQry := "SE1"
				#IFDEF TOP
			EndIf
			#ENDIF

			dbSelectArea(cQry)
			If ( !lQuery )
				dbSetOrder(2)

				If aParam[13] == 1  //Considera loja
					dbSeek(xFilial("SE1")+SA1->A1_COD+SA1->A1_LOJA)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE .And.;
					SA1->A1_LOJA   == SE1->E1_LOJA }
				Else
					dbSeek(xFilial("SE1")+SA1->A1_COD)
					bWhile := {|| !Eof() .And. xFilial("SE1") == SE1->E1_FILIAL .And.;
					SA1->A1_COD    == SE1->E1_CLIENTE}			
				Endif
				bFiltro:= {|| 	SE1->E1_EMISSAO >= aParam[1] .And.;
				SE1->E1_EMISSAO <= aParam[2] .And.;
				SE1->E1_VENCREA >= aParam[3] .And.;
				SE1->E1_VENCREA <= aParam[4] .And.;
				SE1->E1_TIPO	 $ cCheques .And.;
				SE1->E1_PREFIXO >= aParam[6] .And.;
				SE1->E1_PREFIXO <= aParam[7]}
			Else
				bWhile := {|| !Eof() }
				bFiltro:= {|| .T. }
			EndIf			

			While ( Eval(bWhile) )				
				If ( Eval(bFiltro) ) .And. Eval(bTipo)
					dbSelectArea(cAlias)
					dbSetOrder(1)
					cChave := (cQry)->(E1_PREFIXO)+(cQry)->(E1_NUM)+(cQry)->(E1_PARCELA)
					If ( !dbSeek(cChave) )
						RecLock(cAlias,.T.)						
					Else
						RecLock(cAlias,.F.)
					EndIf
					For nCntFor := 1 To Len(aStru)
						Do Case
							//SITCOB
							Case ( AllTrim(aStru[nCntFor][1])=="FRV_DESCRI" )
							If ( lQuery )
								(cAlias)->FRV_DESCRI := FN022SITCB((cQry)->E1_SITUACA)[9]	
							Else
								(cAlias)->FRV_DESCRI := FN022SITCB(SE1->E1_SITUACA)[9]	
							EndIf

							Case ( AllTrim(aStru[nCntFor][1])=="XX_ESTADO" )
							If lQuery
								If (cQry)->E1_SALDO > 0
									(cAlias)->XX_ESTADO := STR0066 //"PEND"
								ElseIf (cQry)->E1_STATUS == "R" .And. FN022SITCB((cQry)->E1_SITUACA)[1]		//(cQry)->E1_SITUACA $ "0FG"
									(cAlias)->XX_ESTADO := If(cPaisLoc$"URU|BOL|ARG",STR0089,STR0067) //"NEGO"
								Else									
									(cAlias)->XX_ESTADO := STR0068 //"COBR"
								Endif
							Else
								If SE1->E1_SALDO > 0
									(cAlias)->XX_ESTADO := STR0066 //"PEND"
								ElseIf SE1->E1_STATUS == "R" .And. FN022SITCB((cQry)->E1_SITUACA)[1]		//SE1->E1_SITUACA $ "0FG"
									(cAlias)->XX_ESTADO := IIf(cPaisLoc$"URU|BOL|ARG",STR0089,STR0067) //"NEGO"
								Else									
									(cAlias)->XX_ESTADO := STR0068 //"COBR"
								Endif
							Endif
							Case ( AllTrim(aStru[nCntFor][1])=="XX_RECNO" )
							If ( lQuery )
								(cAlias)->XX_RECNO := (cQry)->SE1RECNO
							Else
								(cAlias)->XX_RECNO := SE1->(RecNo())
							EndIf
							Case ( AllTrim(aStru[nCntFor][1])=="XX_VALOR" )
							If ( lQuery )
								(cAlias)->XX_VALOR := xMoeda((cQry)->E1_VALOR,(cQry)->E1_MOEDA,nMoeda,,,If(cPaisLoc=="BRA",(cQry)->E1_TXMOEDA,0))
							Else
								(cAlias)->XX_VALOR := xMoeda(SE1->E1_VALOR,SE1->E1_MOEDA,nMoeda,,,If(cPaisLoc=="BRA",SE1->E1_TXMOEDA,0))
							EndIf
							Case ( AllTrim(aStru[nCntFor][1])=="FLAG" )

							OtherWise							
							(cAlias)->(FieldPut(nCntFor,(cQry)->(FieldGet(FieldPos(aStru[nCntFor][1])))))
						EndCase
					Next nCntFor
					dbSelectArea(cAlias)
					MsUnLock()
				EndIf
				dbSelectArea(cQry)
				dbSkip()				
			EndDo
		Endif


		//Cheques em transito
		If cPaisLoc $ "ARG/COS" .and. (nTipo == 4 .or. nTipo == 5)

			#IFDEF TOP
			If ( TcSrvType()!="AS/400" )

				aQuery := SEL->(dbStruct())				

				lQuery := .T.
				cQuery := "SELECT "
				cQuery += "EL_FILIAL,EL_PREFIXO,EL_NUMERO,EL_PARCELA,EL_EMISSAO,EL_VALOR,EL_VALOR,EL_MOEDA,EL_VLMOED1,"
				cQuery += "EL_DTVCTO,EL_NATUREZ,EL_BANCO,EL_BCOCHQ,El_TXMOEDA,EL_TIPODOC,EL_TRANSIT,EL_DTENTRA,"
				cQuery += "SEL.R_E_C_N_O_ SELRECNO "
				cQuery += "FROM "+RetSqlName("SEL")+" SEL "
				/*
				GESTAO - inicio */
				nPosAlias := _FC010QFil(1,"SEL")
				cQuery += " WHERE SEL.EL_FILIAL " + AtmxFil[nPosAlias,2] + " AND "
				/* GESTAO - fim
				*/
				cQuery += "SEL.EL_CLIENTE='"+SA1->A1_COD+"' AND "
				If aParam[13] == 1  //Considera loja
					cQuery +=      "SEL.EL_LOJA='"+SA1->A1_LOJA+"' AND "
				Endif
				cQuery +=  "SEL.EL_EMISSAO >= '"+Dtos(aParam[1])+"' AND "
				cQuery +=  "SEL.EL_EMISSAO <= '"+Dtos(aParam[2])+"' AND "
				cQuery +=  "SEL.EL_DTVCTO  >= '"+Dtos(aParam[3])+"' AND "
				cQuery +=  "SEL.EL_DTVCTO  <= '"+Dtos(aParam[4])+"' AND "
				cQuery +=  "SEL.EL_TIPODOC IN " + FormatIn(cCheques,"|") + " AND "
				cQuery +=  "SEL.EL_PREFIXO >= '"+aParam[6]+"' AND "
				cQuery +=  "SEL.EL_PREFIXO <= '"+aParam[7]+"' AND "
				cQuery +=  "SEL.D_E_L_E_T_ = ' ' AND "
				cQuery +=  "SEL.EL_TRANSIT = '1' AND "
				cQuery +=  "SEL.EL_DTENTRA = ' ' "				

				cQuery := ChangeQuery(cQuery)
				cQry   := cArquivo+"B"

				MsAguarde({ || dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cQry,.T.,.T.)},STR0065) //"Seleccionado registros en el servidor"

				aEval(aQuery,{|x| If(x[2]!="C",TcSetField(cQry,x[1],x[2],x[3],x[4]),Nil)})
			Else
				#ENDIF
				cQry := "SEL"
				#IFDEF TOP
			EndIf
			#ENDIF

			dbSelectArea(cQry)

			If ( !lQuery )
				SEL->(dbSetOrder(7))

				If aParam[13] == 1  //Considera loja
					dbSeek(xFilial("SEL")+SA1->A1_COD+SA1->A1_LOJA)
					bWhile := {|| !Eof() .And. xFilial("SEL") == SEL->EL_FILIAL .And.;
					SA1->A1_COD    == SEL->EL_CLIENTE .And.;
					SA1->A1_LOJA   == SEL->EL_LOJA }
				Else
					dbSeek(xFilial("SEL")+SA1->A1_COD)
					bWhile := {|| !Eof() .And. xFilial("SEL") == SEL->EL_FILIAL .And.;
					SA1->A1_COD    == SE1->EL_CLIENTE}			
				Endif
				bFiltro:= {|| 	SEL->EL_EMISSAO >= aParam[1] .And.;
				SEL->EL_EMISSAO <= aParam[2] .And.;
				SEL->EL_DTVCTO  >= aParam[3] .And.;
				SEL->EL_DTVCTO <= aParam[4] .And.;
				SEL->EL_TIPODOC	 $ cCheques .And.;
				SEL->EL_PREFIXO >= aParam[6] .And.;
				SEL->EL_PREFIXO <= aParam[7] .And.;
				SEL->EL_TRANSIT == '1' }

			Else
				bWhile := {|| !Eof() }
				bFiltro:= {|| .T. }
			EndIf			

			While ( Eval(bWhile) )				
				If ( Eval(bFiltro) ) .And. Eval(bTipo)
					dbSelectArea(cAlias)
					dbSetOrder(1)

					RecLock(cAlias,.T.)						
					(cAlias)->XX_ESTADO  := "TRAN" 
					(cAlias)->E1_FILORIG := (cQry)->EL_FILIAL 
					(cAlias)->E1_PREFIXO := (cQry)->EL_PREFIXO
					(cAlias)->E1_NUM 	 := (cQry)->EL_NUMERO
					(cAlias)->E1_PARCELA := (cQry)->EL_PARCELA
					(cAlias)->E1_NUMNOTA := ""
					(cAlias)->E1_EMISSAO := (cQry)->EL_EMISSAO
					(cAlias)->E1_VALOR   := (cQry)->EL_VALOR
					(cAlias)->E1_SALDO   := (cQry)->EL_VALOR
					(cAlias)->E1_MOEDA   := Val((cQry)->EL_MOEDA)
					(cAlias)->E1_VLCRUZ  := (cQry)->EL_VLMOED1
					(cAlias)->E1_VENCREA := (cQry)->EL_DTVCTO
					(cAlias)->E1_NATUREZ := (cQry)->EL_NATUREZ
					(cAlias)->E1_PORTADO := (cQry)->EL_BANCO
					(cAlias)->E1_BCOCHQ  := (cQry)->EL_BCOCHQ
					(cAlias)->E1_HIST    := "CHQ EM TRANSITO" 

					If ( lQuery )
						(cAlias)->XX_RECNO   := (cQry)->SELRECNO
						(cAlias)->XX_VALOR   := xMoeda((cQry)->EL_VALOR,VAL((cQry)->EL_MOEDA),nMoeda )
					Else
						(cAlias)->XX_RECNO := SEL->(RecNo())
						(cAlias)->XX_VALOR := xMoeda(SEL->EL_VALOR,Val(SEL->EL_MOEDA),nMoeda)
					Endif

					(cAlias)->(MsUnLock())

				EndIf
				dbSelectArea(cQry)
				dbSkip()				
			EndDo
		Endif	


		If ( lQuery )
			dbSelectArea(cQry)
			dbCloseArea()
		EndIf                                              
		//������������������������������������������������������������������������Ŀ
		//�Totais da Consulta                                                      �
		//��������������������������������������������������������������������������
		dbSelectArea(cAlias)
		dbGotop()
		aGet	:=	{0,0,0,0,0,0,0,0}
		While !EOF()
			DO CASE
				CASE XX_ESTADO	==	STR0066 //"PEND"
				aGet[1]	+=	(cAlias)->E1_SALDO
				aGet[4]++
				CASE XX_ESTADO	==	If(cPaisLoc$"URU|BOL|ARG",STR0089,STR0067) //"NEGO"
				aGet[2]	+=	(cAlias)->XX_VALOR
				aGet[5]++
				CASE XX_ESTADO	==	STR0068 //"COBR"
				aGet[3]	+=	(cAlias)->XX_VALOR
				aGet[6]++
				CASE XX_ESTADO	==	"TRAN" //STR0135 //"TRAN"
				aGet[7]	+=	(cAlias)->XX_VALOR
				aGet[8]++

			EndCase				 	
			dbSkip()
		Enddo
		If lExibe
			aGet[1] := TransForm(aGet[1],Tm(aGet[1],16,MsDecimais(nMoeda))) + " (" +Alltrim(STR(aGet[4]))+")"
			aGet[2] := TransForm(aGet[2],Tm(aGet[2],16,MsDecimais(nMoeda))) + " (" +Alltrim(STR(aGet[5]))+")"
			aGet[3] := TransForm(aGet[3],Tm(aGet[3],16,MsDecimais(nMoeda))) + " (" +Alltrim(STR(aGet[6]))+")"
			aGet[4] := TransForm(aGet[7],Tm(aGet[7],16,MsDecimais(nMoeda))) + " (" +Alltrim(STR(aGet[8]))+")"
			aGet[5] := STR0069+GetMv("MV_MOEDA1") //"Valores en "  
			aGet[6] := ""
		EndIf
		Otherwise
		Alert(STR0060)		//N�o Implementado
		lExibe := .f.
	EndCase	
	//������������������������������������������������������������������������Ŀ
	//�Exibe os dados Gerados                                                  �
	//��������������������������������������������������������������������������
	If ( lExibe )
		dbSelectArea(cAlias)
		dbGotop()
		If ( !Eof() )

			aObjects := {} 
			AAdd( aObjects, { 100, 35,  .t., .f., .t. } )
			AAdd( aObjects, { 100, 100 , .t., .t. } )
			AAdd( aObjects, { 100, 50 , .t., .f. } )

			aInfo    := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 3, 3 } 
			aPosObj1 := MsObjSize( aInfo, aObjects) 

			DEFINE FONT oBold    NAME "Arial" SIZE 0, -12 BOLD

			DEFINE MSDIALOG oDlg FROM	aSize[7],0 TO aSize[6],aSize[5] TITLE cCadastro OF oMainWnd PIXEL
			@ aPosObj1[1,1], aPosObj1[1,2] MSPANEL oScrPanel PROMPT "" SIZE aPosObj1[1,3],aPosObj1[1,4] OF oDlg LOWERED

			@ 04,004 SAY OemToAnsi(STR0006) SIZE 025,07          OF oScrPanel PIXEL //"Codigo"
			@ 12,004 SAY SA1->A1_COD  SIZE 060,09  OF oScrPanel PIXEL FONT oBold

			If aParam[13] == 1  //Considera loja		
				@ 04,067 SAY OemToAnsi(STR0007) SIZE 020,07          OF oScrPanel PIXEL //"Loja"
				@ 12,067 SAY SA1->A1_LOJA SIZE 021,09 OF oScrPanel PIXEL FONT oBold
			Endif

			@ 04,090 SAY OemToAnsi(STR0008) SIZE 025,07 OF oScrPanel PIXEL //"Nome"
			@ 12,090 SAY SA1->A1_NOME SIZE 165,09 OF oScrPanel PIXEL FONT oBold

			oGetDb:=MsGetDB():New(aPosObj1[2,1],aPosObj1[2,2],aPosObj1[2,3],aPosObj1[2,4],2,"",,,.F.,,,.F.,,cAlias,,,,,,.T.)
			oGetdb:lDeleta:=NIL
			dbSelectArea(cAlias)
			dbGotop()

			@ aPosObj1[3,1]+04,005 SAY aSay[1] SIZE 045,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+04,175 SAY aSay[2] SIZE 045,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+15,005 SAY aSay[3] SIZE 045,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+15,175 SAY aSay[4] SIZE 045,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+26,005 SAY aSay[5] SIZE 045,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+26,175 SAY aSay[6] SIZE 045,07 OF oDlg PIXEL

			@ aPosObj1[3,1]+04,060 SAY aGet[1] SIZE 060,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+04,215 SAY aGet[2] SIZE 060,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+15,060 SAY aGet[3] SIZE 060,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+15,215 SAY aGet[4] SIZE 060,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+26,060 SAY aGet[5] SIZE 060,07 OF oDlg PIXEL
			@ aPosObj1[3,1]+26,215 SAY aGet[6] SIZE 060,07 OF oDlg PIXEL

			If ( nBrowse == 1 ) // Para titulos em aberto, mostra legenda.
				Fc010Legenda(oDlg,aPosObj1,aSay,aGet)
			Endif

			DEFINE SBUTTON 		FROM 04,aPosObj1[1,3]-If(nBrowse <= 2,60,30) TYPE  1  ENABLE OF oScrPanel ACTION ( oDlg:End() )
			// Exibe o botao de distribuicao por moedas, apenas na consulta de titulos
			// em aberto e recebidos
			If nBrowse <= 2
				SButton():New(04, aPosObj1[1,3]-30, 18,{||Fc010Moeda(If(nBrowse==1,aTotRec,aTotPag),oScrPanel)},oScrPanel,.T.,STR0097) //"Consulta distribui��o por moedas"
				TButton():New(19, aPosObj1[1,3]-60,'Excel',oScrPanel,{||Fc010Excel(cAlias,aHeader,aParam,nBrowse)},26,10,,,,.T.) 			
			Endif	

			DEFINE SBUTTON oBtn 	FROM 19,aPosObj1[1,3]-30 TYPE 15 ENABLE OF oScrPanel

			oBtn:lAutDisable := .F.
			If ( bVisual != Nil )
				oBtn:bAction := bVisual
			Else
				oBtn:SetDisable(.T.)
			EndIf
			ACTIVATE MSDIALOG oDlg
		Else
			Help(" ",1,"REGNOIS")	
		EndIf
		If nBrowse == 5
			(cAlias)->(dbCloseArea()) ;Ferase(cArquivo+GetDBExtension());Ferase(cArquivo+OrdBagExt())
		Endif
	EndIf
	RestArea(aAreaSC5)
	RestArea(aAreaSC6)
	RestArea(aAreaSC9)
	RestArea(aAreaSF4)
	RestArea(aArea)
Return(aHeader)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o	 �fc010Visua� Autor �Eduardo Riera  		� Data �04/01/2000���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Funcao de Visualizacao dos Itens individuais da Posicao de  ���
���          �Cliente.                                                    ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe	 �Fc010Visua()        						     			  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpN1		: Recno do Arquivo principal                      ���
���          �ExpN2		: nBrowse                                         ���
�������������������������������������������������������������������������Ĵ��
��� Uso		 � FINC010													 				  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function _Fc010Visua(nRecno,nBrowse)

	Local aArea := GetArea()
	Local aAreaSE1 := SE1->(GetArea())
	Local aAreaSE5 := SE5->(GetArea())
	Local aAreaSC5 := SC5->(GetArea())
	Local aSavAhead:= If(Type("aHeader")=="A",aHeader,{})
	Local aSavAcol := If(Type("aCols")=="A",aCols,{})
	Local nSavN    := If(Type("N")=="N",N,0)
	Local cFilBkp  := cFilAnt 

	Do Case
		Case ( nBrowse == 1 )
		SE1->(MsGoto(nRecno))
		// Modifica a filial para visualizacao atraves da funcao AxVisual
		cFilAnt := IIf(cFilAnt <> SE1->E1_FILIAL .And. !Empty(cFilAnt),SE1->E1_FILIAL,cFilAnt)	
		SE1->(AxVisual("SE1",nRecNo,2))

		Case ( nBrowse == 2 )
		SE5->(MsGoto(nRecno))
		// Modifica a filial para visualizacao atraves da funcao AxVisual
		cFilAnt := IIf(cFilAnt <> SE5->E5_FILIAL .And. !Empty(cFilAnt),SE5->E5_FILIAL,cFilAnt)
		SE5->(AxVisual("SE5",nRecNo,2))

		Case ( nBrowse == 3 )
		SC5->(MsGoto(nRecno))		
		// Modifica a filial para visualizacao atraves da funcao AxVisual
		cFilAnt := IIf(cFilAnt <> SC5->C5_FILIAL .And. !Empty(cFilAnt),SC5->C5_FILIAL,cFilAnt)
		SC5->(a410Visual("SC5",nRecNo,2))

		Case ( nBrowse == 4 )
		SF2->(MsGoto(nRecno))
		// Modifica a filial para visualizacao atraves da funcao AxVisual
		cFilAnt := IIf(cFilAnt <> SF2->F2_FILIAL .And. !Empty(cFilAnt),SF2->F2_FILIAL,cFilAnt)
		SF2->(Mc090Visual("SF2",nRecNo,2))
		Case ( nBrowse == 5 )
		SEL->(MsGoto(nRecno))
		// Modifica a filial para visualizacao atraves da funcao AxVisual
		cFilAnt := IIf(cFilAnt <> SEL->EL_FILIAL .And. !Empty(cFilAnt),SEL->EL_FILIAL,cFilAnt)	
		SEL->(AxVisual("SEL",nRecNo,2))
	EndCase
	//������������������������������������������������������������������������Ŀ
	//�Restaura a Integridade dos Dados                                        �
	//��������������������������������������������������������������������������
	cFilAnt := cFilBkp
	aHeader := aSavAHead
	aCols   := aSavACol
	N       := nSavN

	RestArea(aAreaSC5)
	RestArea(aAreaSE5)
	RestArea(aAreaSE1)
	RestArea(aArea)
Return(.T.)	

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Program   � ImpDet   � Autor � Eduardo Riera         � Data �02.07.1998���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Controle de Fluxo do Relatorio.                             ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Nenhum                                                      ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Nenhum                                                      ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function _ImpDet(lEnd,wnrel,cString,cNomeprog,cTitulo)

	Local li      := 100 // Contador de Linhas
	Local lImp    := .F. // Indica se algo foi impresso
	Local cbCont  := 0   // Numero de Registros Processados
	Local cbText  := ""  // Mensagem do Rodape
	Local cHist	  := ""
	Local cMoeda  := ""
	Local aCol    := {}
	Local aHeader := {}
	Local nCntFor := 0 
	Local nTamHis := 0	// Tamanho do campo historico a ser impresso
	//
	//                          1         2         3         4         5         6         7         8         9        10        11        12        13        14        15        16        17        18        19        20        21        22
	//                01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
	Local cCabec1 := ""
	Local cCabec2 := ""
	Local aAlias  := {}
	Local aParam  := {}
	Local nMCusto := If(SA1->A1_MOEDALC > 0,SA1->A1_MOEDALC,Val(GetMv("MV_MCUSTO")))
	Local cSalFin :=""
	Local cLcFin  :=""
	Local aGet      := {"","","",""}
	Local lInverte
	Local nSizeCol := 0

	SX3->(DbSetOrder(2))
	cLcFin	:=	If(SX3->(DbSeek("A1_LCFIN")) ,X3Titulo(),STR0076)
	cSalFin  := If(SX3->(DbSeek("A1_SALFIN")),X3Titulo(),STR0075)

	aadd(aParam,MV_PAR01)
	aadd(aParam,MV_PAR02)
	aadd(aParam,MV_PAR03)
	aadd(aParam,MV_PAR04)
	aadd(aParam,MV_PAR05)
	aadd(aParam,MV_PAR06)
	aadd(aParam,MV_PAR07)
	aadd(aParam,MV_PAR08)
	aadd(aParam,MV_PAR09)
	aadd(aParam,MV_PAR10)
	aadd(aParam,MV_PAR11)
	aadd(aParam,MV_PAR12)
	aadd(aParam,MV_PAR13)
	aadd(aParam,MV_PAR14)
	aadd(aParam,MV_PAR15)

	cMoeda	:= AllTrim(STR(nMCusto))
	cMoeda	:= SubStr(Getmv("MV_SIMB"+cMoeda)+Space(4),1,4)

	dbSelectArea(cString)
	SetRegua(LastRec())
	dbSetOrder(1)

	Li := cabec(cTitulo,cCabec1,cCabec2,cNomeProg,Tamanho)
	@li,000 PSAY Replicate("*",220)
	Li++
	@li,001 PSAY RetTitle("A1_COD")+": "+SA1->A1_COD+" "+RetTitle("A1_NOME")+": "+SA1->A1_NOME+" "+RetTitle("A1_TEL")+": "+;
	Alltrim(SA1->A1_DDI+" "+SA1->A1_DDD+" "+SA1->A1_TEL)
	Li++
	@li,001 PSAY RetTitle("A1_CGC")+": "+SA1->A1_CGC
	Li++
	@Li,001 PSAY RetTitle("A1_PRICOM")+": "+DTOC(SA1->A1_PRICOM)
	Li++
	@Li,001 PSAY RetTitle("A1_ULTCOM")+": "+DTOC(SA1->A1_ULTCOM)
	Li++
	@Li,001 PSAY RetTitle("A1_MATR")+": "+TransForm(SA1->A1_MATR,PesqPict("SA1","A1_MATR"))
	Li++
	@Li,001 PSAY RetTitle("A1_METR")+": "+TransForm(SA1->A1_METR,PesqPict("SA1","A1_METR")) 
	Li++
	@Li,001 PSAY RetTitle("A1_CHQDEVO")+": "+TransForm(SA1->A1_CHQDEVO,PesqPict("SA1","A1_CHQDEVO",4))
	Li++
	@Li,001 PSAY RetTitle("A1_DTULCHQ")+": "+Dtoc(SA1->A1_DTULCHQ)
	Li++
	@Li,001 PSAY RetTitle("A1_TITPROT")+": "+TransForm(SA1->A1_TITPROT,PesqPict("SA1","A1_TITPROT",4))
	Li++
	@Li,001 PSAY RetTitle("A1_DTULTIT")+": "+DtoC(SA1->A1_DTULTIT)
	Li++
	@Li,001 PSAY RetTitle("A1_RISCO")+": "+SA1->A1_RISCO
	Li++
	@Li,001 PSAY RetTitle("A1_VENCLC")+": "+Dtoc(SA1->A1_VENCLC)
	Li++
	@Li,001 PSAY STR0077+Space(31-Len(STR0077))+STR0078+;
	Space(11-Len(STR0078))+STR0069+cMoeda
	Li++
	@Li,001 PSAY RetTitle("A1_SALDUP")+": "+Space(20-Len(RetTitle("A1_SALDUP")))+TransForm(SA1->A1_SALDUP,PesqPict("SA1","A1_SALDUP",14,1))+;
	SPACE(6)+TransForm(SA1->A1_SALDUPM,PesqPict("SA1","A1_SALDUPM",14,nMcusto))
	Li++
	@Li,001 PSAY RetTitle("A1_MCOMPRA")+": "+Space(20-Len(RetTitle("A1_MCOMPRA")))+TransForm(xMoeda(SA1->A1_MCOMPRA,nMcusto,1,dDataBase,MsDecimais(1)),;
	PesqPict("SA1","A1_MCOMPRA",14,1))+SPACE(6)+TransForm(SA1->A1_MCOMPRA,PesqPict("SA1","A1_MCOMPRA",14,nMcusto))
	Li++
	@Li,001 PSAY RetTitle("A1_MSALDO")+": "+Space(20-Len(RetTitle("A1_MSALDO")))+TransForm(xMoeda(SA1->A1_MSALDO,nMcusto,1,dDataBase,MsDecimais(1)),;
	PesqPict("SA1","A1_MCOMPRA",14,1))+SPACE(6)+TransForm(SA1->A1_MSALDO,PesqPict("SA1","A1_MCOMPRA",14,nMCusto))
	Li++

	@Li,001 PSAY cSalFin+": "+Space(20-Len(cSalFin))+;
	TRansform(SA1->A1_SALFIN,PesqPict("SA1","A1_SALFIN",14,1))+SPACE(6)+;
	TRansform(SA1->A1_SALFINM,PesqPict("SA1","A1_SALFINM",14,nMcusto))
	Li++

	@Li,001 PSAY cLcFin+": "+Space(20-Len(cLcFin))+;
	TRansform(xMoeda(SA1->A1_LCFIN,nMcusto,1,dDatabase,MsDecimais(1)),PesqPict("SA1","A1_LCFIN",14,1))+;
	SPACE(6)+TRansform(SA1->A1_LCFIN,PesqPict("SA1","A1_LCFIN",14,nMcusto))

	Li++

	@Li,001 PSAY RetTitle("A1_LC")+": "+Space(20-Len(RetTitle("A1_LC")))+TransForm(xMoeda(SA1->A1_LC,nMcusto,1,dDataBase,MsDecimais(nMCusto)),PesqPict("SA1","A1_LC",14,nMCusto))+SPACE(6)+;
	TransForm(SA1->A1_LC,PesqPict("SA1","A1_LC",14,1))

	Li+=3
	@Li,001 PSAY PadC(STR0053,Limite) //"TITULOS EM ABERTO"
	Li++
	aHeader := Fc010Brow(1,@aAlias,aParam,.F.,aGet,.T.)
	IncRegua(1)
	cCabec1 := ""
	aCol    := {}
	dbSelectArea(aAlias[Len(aAlias)][1])
	dbGotop()
	aadd(aCol,1)

	aHeader := FC010COLIMP(aHeader)

	//Atribui m�scara de acordo com o dimensionamento da coluna
	AEVAL(aHeader,{|e,i| aHeader[i][3] := IF(aHeader[i][5] > 0 .AND. aHeader[i][8] == 'N','@E 9,999,999.99',aHeader[i][3])})

	For nCntFor := 1 To Len(aHeader)

		nSizeCol := aHeader[nCntFor][4]+If(aHeader[nCntFor][5]>2,aHeader[nCntFor][5]+1,0)
		nSizeCol += If(aHeader[nCntFor][8]=='D',2,0)

		If (nSizeCol > 3) .and. (LEN(aHeader[nCntFor][1]) > nSizeCol)
			aHeader[nCntFor][1] := SUBSTR(aHeader[nCntFor][1],1,nSizeCol)
		EndIf 

		If nModulo != 49    //Gestao Educacional
			If !(Alltrim(aHeader[nCntFor][2]) $ "E1_SDACRES#E1_SDDECRE#E1_HIST") .and. !(Alltrim(aHeader[nCntFor][2])== "E1_SALDO2")				
				cCabec1 += PadR(aHeader[nCntFor][1],Max(Len(TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3]))),Len(AllTrim(aHeader[nCntFor][1]))))+Space(2)

			ElseIf Alltrim(aHeader[nCntFor][2]) $ "E1_HIST"  //Limita Historico
				cHist	:= PadR(aHeader[nCntFor][1],12)+Space(2)
				cCabec1 += cHist
				nTamHis := Len(cHist) - 1
			Endif
		Else
			If !(Alltrim(aHeader[nCntFor][2]) $ "E1_SDACRES#E1_SDDECRE#E1_HIST") .and. !(Alltrim(aHeader[nCntFor][2])== "E1_SALDO2")
				cCabec1 += PadR(aHeader[nCntFor][1],Max(Len(TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3]))),Len(AllTrim(aHeader[nCntFor][1]))))+Space(2)
			Endif
		EndIf		

		aadd(aCol,Len(cCabec1)+2)

	Next nCntFor
	@ Li,001 PSAY cCabec1
	Li++
	While ( !Eof() )
		lImp := .T.
		If lEnd
			@ Prow()+1,001 PSAY STR0070 //"CANCELADO PELO OPERADOR"
			Exit
		EndIf
		If ( Li > 56 )
			li := cabec(cTitulo,cCabec1,cCabec2,cNomeprog,Tamanho)
			li++
		Endif
		For nCntFor := 1 To Len(aHeader)
			If nModulo != 49    //Gestao Educacional
				If !(Alltrim(aHeader[nCntFor][2]) $ "E1_SDACRES#E1_SDDECRE") .and. !(Alltrim(aHeader[nCntFor][2])== "E1_SALDO2")
					lInverte	:= E1_TIPO $ MVRECANT+","+MV_CRNEG .And. Alltrim(aHeader[nCntFor][2]) $ "E1_VLCRUZ#E1_SALDO"
					If (Alltrim(aHeader[nCntFor][2]) <> "E1_HIST")
						@ Li,aCol[nCntFor]-If(lInverte,1,0) PSAY If(lInverte,"(","")+TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3])) + If(lInverte,")","")
					Else
						@ Li,aCol[nCntFor] PSAY SubStr(TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3])),1,nTamHis)
					EndIf
				Endif		
			Else
				If !(Alltrim(aHeader[nCntFor][2]) $ "E1_SDACRES#E1_SDDECRE#E1_HIST") .and. !(Alltrim(aHeader[nCntFor][2])== "E1_SALDO2")
					lInverte	:= E1_TIPO $ MVRECANT+","+MV_CRNEG .And. Alltrim(aHeader[nCntFor][2]) $ "E1_VLCRUZ#E1_SALDO"
					@ Li,aCol[nCntFor]-If(lInverte,1,0) PSAY If(lInverte,"(","")+TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3])) + If(lInverte,")","")
				Endif		
			EndIf
		Next nCntFor
		Li++
		dbSkip()
		cbCont++
	EndDo
	Li++
	@Li,001 PSAY STR0087 + RIGHT(aGet[1],5)  //"Total : "
	@Li,077 PSAY aGet[2]
	@Li,130 PSAY aGet[3]

	Li+=3
	If ( Li > 55 )
		li := cabec(cTitulo,"","",cNomeprog,Tamanho)
		li++
	Endif

	@Li,001 PSAY PadC(STR0054,Limite) //"TITULOS RECEBIDOS"
	Li++
	aHeader := Fc010Brow(2,@aAlias,aParam,.F.,aGet,.T.)
	cCabec1 := ""
	aCol    := {}
	IncRegua(2)

	dbSelectArea(aAlias[Len(aAlias)][1])
	dbGotop()
	aadd(aCol,1)
	aHeader := FC010COLIMP(aHeader)

	//Atribui m�scara de acordo com o dimensionamento da coluna
	AEVAL(aHeader,{|e,i| aHeader[i][3] := IF(aHeader[i][5] > 0 .AND. aHeader[i][8] == 'N','@E 9,999,999.99',aHeader[i][3])})

	For nCntFor := 1 To Len(aHeader)

		nSizeCol := aHeader[nCntFor][4]+If(aHeader[nCntFor][5]>2,aHeader[nCntFor][5]+1,0)
		nSizeCol += If(aHeader[nCntFor][8]=='D',2,0)

		If (nSizeCol > 3) .and. (LEN(aHeader[nCntFor][1]) > nSizeCol)
			aHeader[nCntFor][1] := SUBSTR(aHeader[nCntFor][1],1,nSizeCol)
		EndIf 

		IF	'@E' $ aHeader[nCntFor][3]
			AHEADER[NCNTFOR][3] := "@E 9999999.99"
			AHEADER[NCNTFOR][4] := 10
		ENDIF

		If nSizeCol < 4
			aHeader[nCntFor][1] := SUBSTR(aHeader[nCntFor][1],1,4)
		endif

		If !Alltrim(aHeader[nCntFor][2]) $ "E5_VLJUROS#E5_VLMULTA#E5_VLCORRE#E5_VLDESCO#E5_HISTOR"
			cCabec1 += PadR(aHeader[nCntFor][1],Max(Len(TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3]))),Len(AllTrim(aHeader[nCntFor][1]))))+Space(1)	
			aadd(aCol,Len(cCabec1)+1)
		ElseIf Alltrim(aHeader[nCntFor][2]) $ "E5_HISTOR"		//Limita Historico a 35 posicoes
			cCabec1 += PadR(aHeader[nCntFor][1],12)+Space(2)
			aadd(aCol,Len(cCabec1)+1)
		Else
			aadd(aCol,aCol[Len(aCol)])		// Deixar aCol do mesmo tamanho de aHeader para que na impressao nao
			// ocorram problemas.
		Endif	
	Next nCntFor
	@ Li,001 PSAY cCabec1
	Li++
	While ( !Eof() )
		lImp := .T.
		If lEnd
			@ Prow()+1,001 PSAY STR0070 //"CANCELADO PELO OPERADOR"
			Exit
		EndIf
		If ( Li > 56 )
			li := cabec(cTitulo,cCabec1,cCabec2,cNomeprog,Tamanho)
			li++
		Endif
		For nCntFor := 1 To Len(aHeader)
			If !Alltrim(aHeader[nCntFor][2]) $ "E5_VLJUROS#E5_VLMULTA#E5_VLCORRE#E5_VLDESCO#E5_HISTOR" .and. aHeader[nCntFor][8] != "D"
				@ Li,aCol[nCntFor] PSAY TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3]))
			ElseIf Alltrim(aHeader[nCntFor][2]) $ "E5_HISTOR"		//Limita Historico a 35 posicoes
				@ Li,aCol[nCntFor] PSAY Substr(FieldGet(FieldPos(aHeader[nCntFor][2])),1,12)+Space(2)
			ELSEIF aHeader[nCntFor][8] = "D"		//altera para data abreviada
				@ Li,aCol[nCntFor] PSAY TransForm(gravadata(FieldGet(FieldPos(aHeader[nCntFor][2])),.f.,1),"@R 99/99/99")
			Endif	
		Next nCntFor
		Li++
		dbSkip()
		cbCont++
	EndDo
	Li++
	@Li,001 PSAY STR0087 + RIGHT(aGet[1],5)   //"Total : "
	@Li,054 PSAY aGet[3]

	Li+=3
	If ( Li > 55 )
		li := cabec(cTitulo,"","",cNomeprog,Tamanho)
		li++
	Endif

	@Li,001 PSAY PadC(STR0055,Limite) //"PEDIDOS"
	Li++
	aHeader := Fc010Brow(3,@aAlias,aParam,.F.,aGet,.T.)
	cCabec1 := ""
	aCol    := {}
	IncRegua(3)

	dbSelectArea(aAlias[Len(aAlias)][1])
	dbGotop()
	aadd(aCol,1)
	For nCntFor := 1 To Len(aHeader)
		cCabec1 += PadR(aHeader[nCntFor][1],Max(Len(TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3]))),Len(AllTrim(aHeader[nCntFor][1]))))+Space(1)	
		aadd(aCol,Len(cCabec1)+1)
	Next nCntFor
	@ Li,001 PSAY cCabec1
	Li++
	While ( !Eof() )
		lImp := .T.
		If lEnd
			@ Prow()+1,001 PSAY STR0070 //"CANCELADO PELO OPERADOR"
			Exit
		EndIf
		If ( Li > 56 )
			li := cabec(cTitulo,cCabec1,cCabec2,cNomeprog,Tamanho)
			li++
		Endif
		For nCntFor := 1 To Len(aHeader)
			@ Li,aCol[nCntFor] PSAY TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3]))
		Next nCntFor
		Li++
		dbSkip()
		cbCont++
	EndDo
	Li+=2

	@Li,001 PSAY STR0087 + RIGHT(aGet[1],5)   //"Total : "
	@Li,019 PSAY Transform(Val(StrTran(StrTran(aGet[2],".",""),",",".")),"@E 999,999,999.99")
	@Li,034 PSAY Transform(Val(StrTran(StrTran(aGet[3],".",""),",",".")),"@E 999,999,999.99")
	@Li,049 PSAY Transform(Val(StrTran(StrTran(aGet[4],".",""),",",".")),"@E 999,999,999.99")

	Li+=3

	If ( Li > 55 )
		li := cabec(cTitulo,"","",cNomeprog,Tamanho)
		li++
	Endif

	@Li,001 PSAY PadC(STR0056,Limite) //"FATURAMENTO"
	Li++
	aHeader := Fc010Brow(4,@aAlias,aParam,.F.,aGet,.T.)
	cCabec1 := ""
	aCol    := {}
	IncRegua(4)

	dbSelectArea(aAlias[Len(aAlias)][1])
	dbGotop()
	aadd(aCol,1)
	For nCntFor := 1 To Len(aHeader)
		cCabec1 += PadR(aHeader[nCntFor][1],Max(Len(TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3]))),Len(AllTrim(aHeader[nCntFor][1]))))+Space(1)	
		aadd(aCol,Len(cCabec1)+1)
	Next nCntFor
	@ Li,001 PSAY cCabec1
	Li++
	While ( !Eof() )
		lImp := .T.
		If lEnd
			@ Prow()+1,001 PSAY STR0070 //"CANCELADO PELO OPERADOR"
			Exit
		EndIf
		If ( Li > 56 )
			li := cabec(cTitulo,cCabec1,cCabec2,cNomeprog,Tamanho)
			li++
		Endif
		For nCntFor := 1 To Len(aHeader)
			@ Li,aCol[nCntFor] PSAY TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3]))
		Next nCntFor
		Li++
		dbSkip()
		cbCont++
	EndDo
	Li++
	@Li,001 PSAY STR0087 + RIGHT(aGet[1],5)   //"Total : "
	@Li,045 PSAY aGet[2]
	Li++

	If cPaisLoc	<> "BRA"
		Li++
		@Li,001 PSAY PadC(STR0061,Limite) //"Cartera de Cheques"
		Li++

		aHeader := Fc010Brow(5,@aAlias,aParam,.F.,aGet,.T.)

		IncRegua(5)

		cCabec1 := ""
		aCol    := {}

		dbSelectArea(aAlias[Len(aAlias)][1])
		dbGotop()

		aadd(aCol,1)

		For nCntFor := 1 To Len(aHeader)
			//������������������������������������������������������������������������Ŀ
			//� Verifica se e um campo numero, para alinhar a direita                  �
			//��������������������������������������������������������������������������

			If aHeader[nCntFor,8] == "N"
				cCabec1 += PadL(aHeader[nCntFor,1],Max(Len(TransForm(FieldGet(FieldPos(aHeader[nCntFor,2])),Trim(aHeader[nCntFor,3]))),Len(AllTrim(aHeader[nCntFor,1]))))+Space(1)
			Else
				cCabec1 += PadR(aHeader[nCntFor,1],Max(Len(TransForm(FieldGet(FieldPos(aHeader[nCntFor,2])),Trim(aHeader[nCntFor,3]))),Len(AllTrim(aHeader[nCntFor,1]))))+Space(1)
			EndIf

			aadd(aCol,Len(cCabec1)+1)
		Next nCntFor

		@ Li,001 PSAY alltrim(cCabec1)

		Li++

		//����������������������������������������������������Ŀ
		//� Variaveis que controlam os totalizadores           �
		//������������������������������������������������������

		aVals	:=	{0,0,0,0,0,0}	
		nPosEst	:=	Ascan(aHeader,{|x| Trim(x[2]) == "XX_ESTADO"})
		nPosVal	:=	Ascan(aHeader,{|x| Trim(x[2]) == "E1_VLCRUZ"})
		dbSelectArea(aAlias[Len(aAlias)][1])
		dbGotop()

		While ( !Eof() )
			lImp := .T.

			If lEnd
				@ Prow()+1,001 PSAY STR0070 //"CANCELADO PELO OPERADOR"
				Exit
			EndIf

			If ( Li > 56 )
				li := cabec(cTitulo,cCabec1,cCabec2,cNomeprog,Tamanho)
				li++
			Endif


			For nCntFor := 1 To Len(aHeader)
				@ Li,aCol[nCntFor] PSAY TransForm(FieldGet(FieldPos(aHeader[nCntFor][2])),Trim(aHeader[nCntFor][3]))
			Next

			Li++
			cbCont++
			Do Case
				CASE FieldGet(FieldPos(aHeader[nPosEst,2]))	==	STR0066 //"PEND"
				aVals[1]	+=	FieldGet(FieldPos(aHeader[nPosVal,2]))
				aVals[4]++
				CASE FieldGet(FieldPos(aHeader[nPosEst,2]))	==	If(cPaisLoc$"URU|BOL|ARG",STR0089,STR0067) //"NEGO"
				aVals[2]	+=	FieldGet(FieldPos(aHeader[nPosVal,2]))
				aVals[5]++
				CASE FieldGet(FieldPos(aHeader[nPosEst,2]))	==	STR0068 //"COBR"
				aVals[3]	+=	FieldGet(FieldPos(aHeader[nPosVal,2]))
				aVals[6]++
			EndCase				 	

			dbSkip()
		EndDo

		Li++

		@Li,000  PSAY STR0071 //"Totales --> "
		@Li,020 	PSAY STR0062+ " : " + Transform(aVals[1],Tm(aVals[1],16,MsDecimais(1))) + " (" + Alltrim(STR(aVals[4])) + ")" //"Pendientes"
		If cPaisLoc$"URU|BOL"
			@Li,060	PSAY STR0088+ " : " + Transform(aVals[2],Tm(aVals[2],16,MsDecimais(1)))	+ " (" + Alltrim(STR(aVals[5])) + ")" //"Negociados"
		Else
			@Li,060	PSAY STR0063+ " : " + Transform(aVals[2],Tm(aVals[2],16,MsDecimais(1)))	+ " (" + Alltrim(STR(aVals[5])) + ")" //"Negociados"
		Endif
		@Li,100 	PSAY STR0064+ " : " + Transform(aVals[3],Tm(aVals[3],16,MsDecimais(1)))	+ " (" + Alltrim(STR(aVals[6])) + ")" //"Cobrados  "

		Li++
	Endif
	If ( lImp )
		Roda(cbCont,cbText,Tamanho)
	EndIf
	If Trim(GetMV("MV_VEICULO")) == "S"
		FG_SALOSV(SA1->A1_COD,SA1->A1_LOJA,," ","I")
	EndIf   

	aEval(aAlias,{|x| (x[1])->(dbCloseArea()),Ferase(x[2]+GetDBExtension()),Ferase(x[2]+OrdBagExt())})
	dbSelectArea("SA1")
	Set Device To Screen
	Set Printer To


	If ( aReturn[5] = 1 )
		dbCommitAll()
		OurSpool(wnrel)
	Endif
	MS_FLUSH()
Return(.T.)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Program   �FC010TotRc� Autor � Mauricio Pequim Jr    � Data �30.08.2001���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Somat�ria dos titulos recebidos                             ���
�������������������������������������������������������������������������Ĵ��
���Parametros�aGet - Array que guardar� o nro de titulos, valores princi- ���
���          �       pais e valor da baixa                                ���
���          �cAlias - Alias do arquivo tempor�rio                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function _FC010TotRc(aGet,cAlias,aTotPag, lRelat)
	Local cTitAnt := ""
	Local nAscan

	Default lRelat:= .F.

	While !Eof()
		aGet[1]++
		// Impressao do relatorio nao existe E1_MOEDA no temporario
		If !lRelat
			nAscan := Ascan(aTotPag,{|e| e[MOEDATIT] == E1_MOEDA})
			If nAscan = 0
				Aadd(aTotPag,{1,E1_MOEDA,If(E1_MOEDA>1,E1_VLMOED2,E1_PAGO),E1_PAGO})
			Else
				aTotPag[nAscan][QTDETITULOS]++
				aTotPag[nAscan][VALORTIT]+= If(E1_MOEDA>1,E1_VLMOED2,E1_PAGO)
				aTotPag[nAscan][VALORREAIS]	+= E1_PAGO
			Endif

		Endif
		//����������������������������������������������������������������Ŀ
		//� Somo o valor do titulo apenas uma vez em caso de baixa parcial �
		//������������������������������������������������������������������
		If (cAlias)->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO) != cTitAnt
			aGet[2]+= (cAlias)->E1_VLCRUZ
			cTitAnt := (cAlias)->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
		Endif
		aGet[3] += E1_PAGO
		dbSkip()
	Enddo
Return (aGet)	

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Program   �Fc010Moeda� Autor � Claudio Donizete Souza� Data �13.11.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Exibe os totais a pagar a recebidos por moeda               ���
�������������������������������������������������������������������������Ĵ��
���Parametros�aTotais- Matriz de totais por moeda								  ���
���          �oDlg   - Objeto dialog que sera exibida a tela da consulta  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function _Fc010Moeda(aTotais,oDlg)
	Local	aCab := { STR0098	,; //"Qtde. Titulos"
	STR0099	,; //"Moeda"
	STR0100	,; //"Valor na moeda"
	STR0101} 	//"Valor em R$"
	Local oLbx					 
	DEFINE DIALOG oDlg FROM 0,0 TO 20,70 TITLE STR0102 OF oDlg //"Distribui��o por moeda"
	oLbx := RDListBox(.5, .4, 270, 130, aTotais, aCab)
	oLbx:LNOHSCROLL := .T.
	oLbx:LHSCROLL := .F.
	ACTIVATE MSDIALOG oDlg CENTERED
Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Program   �Fc010Legen� Autor � Claudio Donizete Souza� Data �13.11.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Exibe legenda de titulos baixados parcial ou totalmente     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
static Function _Fc010Legenda(oDlg,aPosObj1,aSay,aGet)
	@ aPosObj1[3,1]+37,005 SAY aSay[7] SIZE 035,07 OF oDlg PIXEL  //Total Geral
	@ aPosObj1[3,1]+37,060 SAY aGet[7] SIZE 060,07 OF oDlg PIXEL

	@ aPosObj1[3,1]+4, 300 BITMAP oBmp RESNAME "BR_AZUL" SIZE 16,16 NOBORDER OF oDlg PIXEL
	@ aPosObj1[3,1]+4, 310 SAY "Baixado parcial"  OF oDlg PIXEL // "Baixado parcial" //STR0121

	@ aPosObj1[3,1]+20.5, 300 BITMAP oBmp1 RESNAME "BR_VERDE" SIZE 16,16 NOBORDER OF oDlg PIXEL
	@ aPosObj1[3,1]+20.5, 310 SAY STR0094 OF oDlg PIXEL // "Sem baixas"

	@ aPosObj1[3,1]+37, 300 BITMAP oBmp RESNAME "BR_AMARELO" SIZE 16,16 NOBORDER OF oDlg PIXEL
	@ aPosObj1[3,1]+37, 310 SAY STR0113 OF oDlg PIXEL   //"Titulo c/ Cheque Devolvido"

Return Nil


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Fc010Loja �Autor  �Microsiga           � Data �  12/30/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Carrega as variaveis com todos os valores da empresa        ���
���          �selecionada                                                 ���
�������������������������������������������������������������������������͹��
���Uso       � Finc010                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function _Fc010Loja(nLC,dPRICOM,nSALDUP,nSALDUPM,dULTCOM,nLCFIN,nMATR,nSALFIN,nSALFINM,nMETR,nMCOMPRA,cRISCO,nMSALDO,nCHQDEVO,dDTULCHQ,nTITPROT,dDTULTIT)

	Local nRecSA1 := SA1->(Recno())
	Local cCod := SA1->A1_COD
	Local cChave := xFilial("SA1") + cCod         
	Local aAreaSA1 := SA1->(GetArea())

	SA1->(DbSetOrder(1))
	SA1->(DbSeek(cChave))
	While cChave == (xFilial("SA1") + SA1->A1_COD)
		nLC      += SA1->A1_LC
		nSALDUP  += SA1->A1_SALDUP
		nSALDUPM += SA1->A1_SALDUPM
		nLCFIN   += SA1->A1_LCFIN       
		nSALFIN  += SA1->A1_SALFIN  
		nSALFINM += SA1->A1_SALFINM              
		If nMCOMPRA < SA1->A1_MCOMPRA
			nMCOMPRA := SA1->A1_MCOMPRA        
		Endif
		If nMSALDO < SA1->A1_MSALDO
			nMSALDO := SA1->A1_MSALDO
		Endif       
		If cRISCO < SA1->A1_RISCO
			cRISCO := SA1->A1_RISCO
		Endif
		If nMETR < SA1->A1_METR 
			nMETR := SA1->A1_METR 
		Endif
		If nMATR < SA1->A1_MATR
			nMATR := SA1->A1_MATR
		Endif                 
		If dULTCOM < SA1->A1_ULTCOM       
			dULTCOM := SA1->A1_ULTCOM       
		Endif
		If Empty(dPRICOM) .Or. dPRICOM > SA1->A1_PRICOM
			dPRICOM :=SA1->A1_PRICOM
		Endif
		nCHQDEVO += SA1->A1_CHQDEVO
		nTITPROT += SA1->A1_TITPROT
		If dDTULCHQ < SA1->A1_DTULCHQ
			dDTULCHQ := SA1->A1_DTULCHQ
		Endif
		If dDTULTIT < SA1->A1_DTULTIT
			dDTULTIT := SA1->A1_DTULTIT
		Endif
		SA1->(DbSkip())
	EndDo

	RestArea(aAreaSA1)
	SA1->(dbGoTo(nRecSA1))

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �Fc010Eco  � Autor � Sergio Silveira       � Data �25/07/2005���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Efetua a chamada das consultas de credito do ecossistema    ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao Efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function _Fc010Eco()         

	Local aArea := GetArea() 

	If !Empty( Aviso( STR0117, STR0116, { STR0118 }, 2 ) ) // "Atencao", "Este recurso efetua consulta aos servicos de parceiros via ecossistema. Deste modo, os parametros de filtro nao serao aplicados.","OK"
		fConsWizard({ SA1->A1_NOME, SA1->A1_PESSOA, SA1->A1_NREDUZ, SA1->A1_END, SA1->A1_CEP, SA1->A1_DDD, SA1->A1_TEL, SA1->A1_CGC, SA1->A1_RG, SA1->A1_DTNASC})
	EndIf                      

	RestArea( aArea ) 

Return

/*/
���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �MenuDef   � Autor � Ana Paula N. Silva     � Data �28/11/06 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Utilizacao de menu Funcional                               ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Array com opcoes da rotina.                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Parametros do array a Rotina:                               ���
���          �1. Nome a aparecer no cabecalho                             ���
���          �2. Nome da Rotina associada                                 ���
���          �3. Reservado                                                ���
���          �4. Tipo de Transa��o a ser efetuada:                        ���
���          �		1 - Pesquisa e Posiciona em um Banco de Dados			  ���
���          �    2 - Simplesmente Mostra os Campos                       ���
���          �    3 - Inclui registros no Bancos de Dados                 ���
���          �    4 - Altera o registro corrente                          ���
���          �    5 - Remove o registro corrente do Banco de Dados        ���
���          �5. Nivel de acesso                                          ���
���          �6. Habilita Menu Funcional                                  ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()
	Private aRotina	:=	{{STR0001, "AxPesqui" , 0 , 1},; //"Pesquisar"
	{STR0003, "xFC010CON" , 0 , 2},;  //"Consultar"
	{STR0002, "AxVisual" , 0 , 2},;  //"Visualizar"
	{STR0004, "FC010IMP" , 0 , 2}}   //"Impressao"
Return(aRotina)



/*/
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FC010COLIMP� Autor � Marcelo Celi Marques � Data � 26.09.08 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Altera o tamanho da coluna de impressao mediante tam picture���
�������������������������������������������������������������������������Ĵ��
��� Uso      � FINC010                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function _FC010COLIMP(aHeader)
	Local nCntFor := 0
	For nCntFor := 1 To Len(aHeader)					

		// Mudan�a das Pictures
		If Alltrim(aHeader[nCntFor][2]) $ "E1_ATR"
			aHeader[nCntFor][3] := "99999"					
		Endif      
		If Alltrim(aHeader[nCntFor][2]) $ "E1_VLCRUZ#E1_SDACRES#E1_SDDECRE#E1_SALDO#E1_SALDO2#E1_MULTA"
			aHeader[nCntFor][3] := "@E 999,999,999.99"					
		Endif      
		If Alltrim(aHeader[nCntFor][2]) $ "E1_VALJUR"
			aHeader[nCntFor][3] := "@E 9,999,999.99"					
		Endif

		// Mudan�a das STR
		If Alltrim(aHeader[nCntFor][2]) $ "E1_PREFIXO"
			aHeader[nCntFor][1] := 'PREFIXO'//STR0124
		Endif      
		If Alltrim(aHeader[nCntFor][2]) $ "E1_PARCELA"
			aHeader[nCntFor][1] := 'PARCELA' //STR0125
		Endif      
		If Alltrim(aHeader[nCntFor][2]) $ "E1_VALJUR"
			aHeader[nCntFor][1] := 'VAL. JUROS' //STR0126
		Endif      
		If Alltrim(aHeader[nCntFor][2]) $ "E1_PORTADO"
			aHeader[nCntFor][1] := 'PORTADOR' //STR0127
		Endif

	Next
Return aHeader


/*/
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FinC010T   � Autor � Marcelo Celi Marques � Data � 31.03.08 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Chamada semi-automatica utilizado pelo gestor financeiro   ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � FINC010                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function _FinC010T(aParam)	
	cRotinaExec := "FINC010"
	ReCreateBrow("SA1",FinWindow)      	
	FinC010(aParam[1])
	ReCreateBrow("SA1",FinWindow)      		

	dbSelectArea("SA1")

	INCLUI := .F.
	ALTERA := .F.

Return .T.

/*/
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �Fc010Excel � Autor � Mauricio Pequim Jr   � Data � 31.03.08 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Rotina para exportatacao de dados para Excel               ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 : Alias                                              ���
���          � ExpA2 : Array com as Descricoes do Cabecalho               ���
���          � ExpA3 : Array com os parametros (perguntes) da rotina      ���
���          � ExpN4 : Opcao executada                                    ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � FINC010                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function _Fc010Excel(cAlias,aHeader,aParam,nBrowse)

	LOCAL aItem			:= {}
	LOCAL aItenXCel	:= {}
	Local aStruD		:= {}
	Local aStruX		:= {}
	Local aArea			:= (cAlias)->(GetArea())
	Local lLoja			:= .T.
	Local nDel			:= 0
	Local nX				:= 0
	Local cTexto		:= ""

	DEFAULT cAlias	:= "SE1"
	DEFAULT aHeader:= {}
	DEFAULT aParam	:= {}
	DEFAULT nBrowse 	:= 0

	If Len(aHeader) > 0 .and. Len(aParam) > 0

		//aParam[13] == 1 -> considera lojas
		If aParam[13] == 1
			cCliente := SA1->A1_COD + " - " + SA1->A1_LOJA + " - " + SA1->A1_NREDUZ
		Else
			cCliente := SA1->A1_COD + " - " + SA1->A1_NREDUZ
			lLoja := .F.
		Endif

		If ! ApOleClient( 'MsExcel' ) 
			MsgAlert('MsExcel nao instalado') // STR0133  //'MsExcel nao instalado'
		Else
			DbSelectArea(cAlias)
			DbGotop()

			//Montagem dos dados a serem exportados para Excel
			For nX := 1 to Len(aHeader)
				//------------------------------------
				//aHeader
				//01 AllTrim(X3Titulo())
				//02 SX3->X3_CAMPO
				//03 SX3->X3_PICTURE
				//04 SX3->X3_TAMANHO
				//05 SX3->X3_DECIMAL
				//06 SX3->X3_VALID
				//07 SX3->X3_USADO
				//08 SX3->X3_TIPO
				//09 SX3->X3_ARQUIVO
				//10 SX3->X3_CONTEXT
				//------------------------------------

				If !("XX_" $ aHeader[nX][2]) .and. !(If(lLoja,("_LOJA" $ aHeader[nX][2]),.F.))
					aADD(aStruD,{aHeader[nX][1],aHeader[nX][8],aHeader[nX][4],aHeader[nX][5]})
					aADD(aStruX,{aHeader[nX][2],aHeader[nX][8],aHeader[nX][4],aHeader[nX][5]})
				Endif
			Next

			AADD(aStruD,{"","C",1,0})
			AADD(aStruX,{"","C",1,0})


			WHILE (cAlias)->(!EOF())

				aItem := Array(Len(aStruX))
				For nX := 1 to Len(aStruX)
					IF nX == Len(aStruX)  // Coluna de compatibilizacao com a abertura no EXCEL
						aItem[nX] := CHR(160)
					ELSEIF aStruX[nX][2] == "C"
						aItem[nX] := CHR(160)+(cAlias)->&(aStruX[nX][1])
					ELSE
						aItem[nX] := (cAlias)->&(aStruX[nX][1])
					ENDIF
				Next nX 
				AADD(aItenXcel,aItem)
				aItem := {}
				(cAlias)->(dbSkip())
			Enddo

			If nBrowse == 1
				cTexto := "Consulta Titulos em Aberto do Cliente - " +cCLiente //"Consulta Titulos em Aberto do Cliente - "
			Else		
				cTexto :="Consulta Titulos Baixados do Cliente - "+cCLiente //"Consulta Titulos Baixados do Cliente - "
			Endif

			MsgRun("Favor Aguardar.....", "Exportando os Registros para o Excel",{||DlgToExcel({{"GETDADOS",cTexto,aStruD,aItenXcel}})}) //"Favor Aguardar....."###"Exportando os Registros para o Excel"

		EndIf

	Endif

	RestArea(aArea)

Return

/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FC010QFil� Autor �                      � Data �06/12/2013���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Cria um complemento para uso em queries para os casos em   ���
���          � se necessisita verificar em uma relacao de filiais.        ���
���          � Utiliza as funcoes do ctb para controle da quatidade de    ���
���          � filiais.                                                   ���
�������������������������������������������������������������������������Ĵ��
���Paramatros� nAcao: 1 cria o complemento para query.                    ���
���          �        2 apaga as tabelas temporaria, se criadas.          ���
���          � cAliasFil: tabela onde sera comparada a filial.            ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � FINC010                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function _FC010QFil(nAcao,cAliasFil)
	Local nPosAlias		:= 0
	Local cTmpFil		:= ""
	//Private AtmxFil       := {}
	Private aSexFil   	:= {}
	Default cAliasFil	:= ""
	Default nAcao		:= 2

	If nAcao == 1
		If !Empty(cAliasFil)
			nPosAlias := Ascan(AtmxFil,{|carq| carq[1] == cAliasFil})
			If nPosAlias == 0
				Aadd(AtmxFil,{"","",""})
				nPosAlias := Len(AtmxFil)
				AtmxFil[nPosAlias,1] := cAliasFil
				MsgRun("Favor Aguardar.....",STR0005 ,{|| AtmxFil[nPosAlias,2] := GetRngFil(aSexFil,cAliasFil,.T.,@cTmpFil)}) //"Favor Aguardar....."###"Consulta Posi��o Clientes"
				AtmxFil[nPosAlias,3] := cTmpFil
			Endif
		Endif
	Else
		If nAcao == 2
			If !Empty(AtmxFil) 
				MsgRun("Favor Aguardar.....",STR0005 ,{|| AEval(AtmxFil,{|tmpfil| CtbTmpErase(tmpFil[3])})}) //"Favor Aguardar....."###"Consulta Posi��o Clientes"
				nPosAlias := Len(AtmxFil)
				AtmxFil := {}
				aSexFil := {}
			Endif
		Endif
	Endif
Return(nPosAlias)

//-------------------------------------------------------------------
/*/{Protheus.doc} GetStatus
Fun��o para retorno da descri��o do status do titulo no Serasa.
@author	lucas.oliveira
@since		01/07/2015

/*/
//-------------------------------------------------------------------
Static Function _GetStatus(lLegenda, cIdDoc)
	Local aArea		:= GetArea()
	Local cStatus		:= ""
	Local cLote		:= F770Lote(cIdDoc)
	Local cNumSeq		:= ""
	Default cIdDoc	:= ""
	Default lLegenda	:= .F.

	//Detalhes Lote Serasa, n�o existe FW9/FWB em pa�ses localizados 
	If cPaisLoc == "BRA"
		DbSelectArea("FW9")
		FW9->(DbSetOrder(1))

		If FW9->(DbSeek(xFilial("FW9")+cLote+cIdDoc))

			cNumSeq := F770NumSeq(cIdDoc, 2)

			DbSelectArea("FWB")
			FWB->(DbSetOrder(1))
			If FWB->(DbSeek(xFilial("FWB")+cIdDoc+cNumSeq))
				cStatus := FWB->FWB_DESCR
			Endif
			FWB->(DbCloseArea())

		ElseIf !(lLegenda)

			cStatus := "Sem restri��es" //STR0136// "Sem restri��es"

		EndIf

		FW9->(DbCloseArea())
		RestArea(aArea)
	EndIf

Return cStatus


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RFATC01C �Autor  �Microsiga           � Data �  08/29/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RFATC01PD(aList1,nLinha,lRet,nChamada)

	Local aArea  := GetArea()
	Local aList4 := {}
	Local oLtBx4
	Local cListBox4                
	Local cTitulo   := "Consulta Pedidos Emitidos "
	//Local aTitulo2	:=  {"","Pedido","Emissao","Entrega","Status","Quantidade","Vlr.TOTAL","Endere�o Entrega","Cidade","Estado","Representante","Transportadora","Tabela","Cond.Pagto"}
	Local aTitulo2	:=  {"","Pedido","Emissao","Entrega","Status","Quantidade","Vlr.TOTAL","Representante","Tabela","Cond.Pagto"}     	
   Local cQuery    := ""               
	Local aBotao    := {}
	Local bOk       := {|| _RFATC013(aList4,oLtBx4:nAt,lRet,nChamada),oDl4g:End()}
	Local nOpc		:= 0
	Local aObjects := {}
	Local aInfo    := {}
	Local aPosObj  := {}
	Local aSize    := MsAdvSize(.F.)
	Local NVLTOT   := 0
	Local nvlTot   := 0                             
    Local QtdDup   := 0
	Private cCodcli   := aList1[nLinha][2]
	Private cNomecl   := aList1[nLinha][4]      
	Private cCnpj     := aList1[nLinha][5] 
	Private nVallim   := Posicione("SA1",3,xFilial("SA1")+ALLTRIM(aList1[nLinha][5]),"A1_LC")
	Private dDtaLim   := SA1->A1_VENCLC 
	Private cLoja     := SA1->A1_LOJA

	//AADD(aBotao, {"Impress�o"    	, { ||U_RFATC01LE()	}, "Legenda - <Ctrl-L>"})
	//AADD(aBotao, {"Visualiza" 		, { ||AxVisual("SA1",SA1->(RECNO()),2)	}, "Visualizar - <Ctrl-V>"})
	//AADD(aBotao, {"Sit.Financeira" 	, { ||xPosFin(aList2,nLinha,lRet,nChamada,cCodcli,cLoja)  }, "Situa��o Financeira - <Ctrl-P>"})
//	SetKey(VK_F8, { || IIF(lKey.AND.aCfgBtn[14][1],(	lKey:= .F.,Tk271SituaC(),lKey:= .T.), "") })

	nQtdAtr:= 0
	nQtdDup:= 0 
	nValor := 0  
	nvlTot := 0
	aList2 := {}
    nQtdVen:= 0
    nqtdAve:= 0
    nVlrVen:= 0
    nVlrAve:= 0
    dPrVenc:= Ctod(space(8))
    dUlVenc:= Ctod(space(8))
    nPrVenc:= 0
    nUlVenc:= 0
     
	If Select("XTRB") > 0 
		DbSelectArea("XTRB")
		XTRB->(DbCloseArea())
	EndIf

    cQuery := "SELECT C6_NUM, C5_EMISSAO, C6_ENTREG, C5_TRANSP, C5_TABELA, C5_PRAZO, C5_CLIENTE,C5_LOJAENT,C5_VEND1,C6_QTDVEN,C6_QTDENT,C6_VALOR "
	cQuery += "FROM "+RetSqlName("SC6")+" C6,  "+RetSqlName("SC5")+" C5 WHERE C6_CLI = '"+cCodcli+"' AND C6.D_E_L_E_T_ = '' "
	cQuery += "AND C5_NUM = C6_NUM AND C5_CLIENTE = C6_CLI ORDER BY C6_NUM+C6_ITEM+C6_PRODUTO DESC "
	MemoWrite("HPCONSCLIF_2.txt",cQuery)
	cQuery := ChangeQuery(cQuery)
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRB", .F., .T.)       
	XTRB->(dbgotop())
	If XTRB->(Eof()) 
		MsgStop("Nenhum Pedido encontrado para este cliente" , "Verifique") 
		Return()
	Endif   	
    XTRB->(dbgotop())   
    
	While XTRB->(!EOF())
        cStatus := ''
        nQtd    := 0
        nValor  := 0
        cNumero := XTRB->C6_NUM 
        cCliente:= XTRB->C5_CLIENTE
        cLoja   := XTRB->C5_LOJAENT
        cEndEnt := ""
        cCidade := ""
        cEstado := ""
        cRepres := ""
        cTabela := ""
        cCondpg := "" 
        cTransp := ""
        WHILE  XTRB->(!EOF()) .AND. XTRB->C6_NUM ==  cNumero
            dEmissao := ctod(Substr(XTRB->C5_EMISSAO,7,2)+'/'+Substr(XTRB->C5_EMISSAO,5,2)+'/'+Substr(XTRB->C5_EMISSAO,3,2))                
            dEntrega := ctod(Substr(XTRB->C6_ENTREG,7,2)+'/'+Substr(XTRB->C6_ENTREG,5,2)+'/'+Substr(XTRB->C6_ENTREG,3,2)) 
            If XTRB->C6_QTDVEN - XTRB->C6_QTDENT == 0
               If cStatus ==  'Faturado Total ' .or. cStatus ==''  
                  cStatus :=  'Faturado Total '
               Endif   
            ElseIf  XTRB->C6_QTDENT <> 0  
               cStatus := 'Faturado Parcial'
            Else   
               cStatus := 'A Faturar'
            Endif   
            nQtd    := nQtd+ C6_QTDVEN
            nValor  := nValor+C6_VALOR
            cEndEnt := Alltrim(Posicione("SA1",1,xFilial("SA1")+cCliente+cLoja,"A1_END"))
            cCidade := Alltrim(Posicione("SA1",1,xFilial("SA1")+cCliente+cLoja,"A1_MUN"))
            cEstado := Alltrim(Posicione("SA1",1,xFilial("SA1")+cCliente+cLoja,"A1_EST"))
            cRepres := AllTrim(Posicione("SA3",1,xFilial("SA3")+PADR(XTRB->C5_VEND1,6),"A3_NOME"))
            cTabela := XTRB->C5_TABELA+'-'+Posicione("DA0",1,xFilial("DA0")+PADR(XTRB->C5_TABELA,3),"DA0_DESCRI")
            cCondpg := Posicione("SE4",1,xFilial("SE4")+PADR(XTRB->C5_PRAZO,3),"E4_DESCRI")
            cTransp := alltrim(XTRB->C5_TRANSP)+'-'+Posicione("SA4",1,xFilial("SA4")+PADR(XTRB->C5_TRANSP,6),"A4_NOME") 
		    XTRB->(dbSKIP())
        Enddo  
        AADD(aList4,{"",cNumero,dEmissao,dEntrega,cStatus,nQtd,nValor,cRepres,cTabela,cCondpg}) 
		nvlTot:=nvlTot+nValor
		nQtdDup++
	Enddo
	If len(aList4)== 0
		aAdd(aList4,{ "","","","","","","","","","","","",""})
	Endif		
	//While .T.

	Aadd(aObjects, {100, 010, .T., .F.})
	//	Aadd(aObjects, {200, 200, .T., .T.})
	Aadd(aObjects, {100, 100, .T., .T.})

	aInfo   := {aSize[1], aSize[2], aSize[3], aSize[4], 3, 2}
	aPosObj := MsObjSize(aInfo, aObjects)


	DEFINE MSDIALOG oDl4g TITLE cTitulo From 000,000 To 530,935 OF oMainWnd PIXEL

	@ 025,004 LISTBOX oLtBx4 VAR cListBox4 Fields HEADER OEMTOANSI(aTitulo2[1]),OEMTOANSI(aTitulo2[2]),OEMTOANSI(aTitulo2[3]),OEMTOANSI(aTitulo2[4]),OEMTOANSI(aTitulo2[5]),OEMTOANSI(aTitulo2[6]),OEMTOANSI(aTitulo2[7]),OEMTOANSI(aTitulo2[8]),OEMTOANSI(aTitulo2[9]),OEMTOANSI(aTitulo2[10]) FIELDSIZES 10, GetTextWidth(0,"BB"), GetTextWidth(0,"BBBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBBBBBBBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBBBBBBBB"), GetTextWidth(0,"BBBBBBBB"), GetTextWidth(0,"BBBBBBBB"), GetTextWidth(0,"BBBBBBB") SIZE 460,200 SCROLL PIXEL 
    @ 230,020 SAY "Qtd Pedidos: " SIZE 060,12 OF oDl4g PIXEL  COLOR CLR_RED	 
	@ 230,105 MSGET onQtdDup VAR nQtdDup  When .f. SIZE 060,08 OF oDl4g PIXEL Picture "@E 999,999"
	@ 230,220 SAY "Total em Valor: " SIZE 060,12 OF oDl4g PIXEL  COLOR CLR_RED	 
	@ 230,305 MSGET onvlTot VAR nvlTot  When .f. SIZE 060,08 OF oDl4g PIXEL Picture "@E 999,999,999.99"
	
	oLtBx4:SetArray(aList4)
    Define SButton  From 245,400 Type 1 Action oDl4g:End() Enable Of oDl4g Pixel //Apaga
	oLtBx4:bLine 		:= {|| RCFATLIS4(@oLtBx4)}
	oLtBx4:blDblClick 	:= {|| xPosFin(aList4,oLtBx4:nAt,lRet,nChamada,SA1->A1_COD,SA1->A1_LOJA)}   
	ACTIVATE MSDIALOG oDl4g Centered //ON INIT EnchoiceBar(oDl2g,bOK,,,aBotao)
	SetKey(12,NIL)
	SetKey(16,NIL)
	SetKey(22,NIL)
	RestArea(aArea)
Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RFATC01C �Autor  �Microsiga           � Data �  08/29/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RFATC01FT(aList1,nLinha,lRet,nChamada)

	Local aArea  := GetArea()
	Local aList5 := {}
	Local oLtBx5
	Local cListBox5                
	Local cTitulo   := "Consulta Faturamento  "
	Local aTitulo2	:=  {"","DATA","Filial ","Nota Fiscal","Transportadora","Valor Total","Qtde.Pe�as","$M�dio","Vendedor","Tabela","Cond.Pagto","Pedidos","Pedido Cliente","Hist�rico"}  
	Local cQuery    := ""               
	Local aBotao    := {}
	Local bOk       := {|| _RFATC013(aList5,oLtBx5:nAt,lRet,nChamada),oDl5g:End()}
	Local nOpc		:= 0
	Local aObjects := {}
	Local aInfo    := {}
	Local aPosObj  := {}
	Local aSize    := MsAdvSize(.F.)
	Local NVLTOT   := 0
	Local nvlTot   := 0                             
    Local QtdDup   := 0
	Private cCodcli   := aList1[nLinha][2]
	Private cNomecl   := aList1[nLinha][4]      
	Private cCnpj     := aList1[nLinha][5] 
	Private nVallim   := Posicione("SA1",3,xFilial("SA1")+ALLTRIM(aList1[nLinha][5]),"A1_LC")
	Private dDtaLim   := SA1->A1_VENCLC 
	Private cLoja     := SA1->A1_LOJA

	//AADD(aBotao, {"Impress�o"    	, { ||U_RFATC01LE()	}, "Legenda - <Ctrl-L>"})
	//AADD(aBotao, {"Visualiza" 		, { ||AxVisual("SA1",SA1->(RECNO()),2)	}, "Visualizar - <Ctrl-V>"})
	//AADD(aBotao, {"Sit.Financeira" 	, { ||xPosFin(aList2,nLinha,lRet,nChamada,cCodcli,cLoja)  }, "Situa��o Financeira - <Ctrl-P>"})
//	SetKey(VK_F8, { || IIF(lKey.AND.aCfgBtn[14][1],(	lKey:= .F.,Tk271SituaC(),lKey:= .T.), "") })

    nQtdAtr:= 0
	nQtdDup:= 0 
	nValor := 0  
	nvlTot := 0
	aList2 := {}
    nQtdVen:= 0
    nqtdAve:= 0
    nVlrVen:= 0
    nVlrAve:= 0
    dPrVenc:= Ctod(space(8))
    dUlVenc:= Ctod(space(8))
    nPrVenc:= 0
    nUlVenc:= 0
    nQtdNf := 0

	If Pergunte("HPPERFAT")
		cFiltro:= "" 
	Endif
	
	If Select("XTRB") > 0 
		DbSelectArea("XTRB")
		XTRB->(DbCloseArea())
	EndIf
	cQuery := "SELECT F2_EMISSAO,F2_FILIAL,F2_DOC,F2_TRANSP,F2_VALBRUT, F2_VEND1,F2_COND,D2_PEDIDO " + CRLF
    cQuery += "  FROM "+RetSqlName("SF2")+" F2, "+RetSqlName("SD2")+"  D2 "+ CRLF
	cQuery += " WHERE F2_CLIENTE = '"+cCodcli+"' "+ CRLF
	cQuery += "   AND D2_DOC     = F2_DOC"        + CRLF
    cQuery += "   AND D2_SERIE   = F2_SERIE"      + CRLF
	cQuery += "   AND D2_ITEM    = '01' "         + CRLF
	cQuery += "   AND F2_EMISSAO BETWEEN '"+DTOS(MV_PAR01)+"'  AND '"+DTOS(MV_PAR02)+"' "  + CRLF
	cQuery += "   AND F2.D_E_L_E_T_ = '' "        + CRLF
	cQuery += "   AND D2.D_E_L_E_T_ = '' "        + CRLF
	cQuery += " ORDER BY F2_EMISSAO+F2_DOC DESC"
	MemoWrite("HPCONSCLIF_2.txt",cQuery)
	cQuery := ChangeQuery(cQuery)
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRB", .F., .T.)       
	XTRB->(dbgotop()) 
	If XTRB->(Eof()) 
		MsgStop("Nenhuma Nota Fiscal encontrada para este cliente" , "Verifique") 
		Return()
	Endif   	
    XTRB->(dbgotop())   
    While XTRB->(!EOF())
        dEmissao := ctod(Substr(XTRB->F2_EMISSAO,7,2)+'/'+Substr(XTRB->F2_EMISSAO,5,2)+'/'+Substr(XTRB->F2_EMISSAO,3,2)) 
        aAreaSM0:= SM0->(GetArea())
        dbselectArea("SM0")
        SM0->(DbGoTop())
        While !SM0->(Eof())
	  	   If Alltrim(SM0->M0_CODFIL) == Alltrim(XTRB->F2_FILIAL) 
		      cNomFil  :=  M0_NOMECOM
		   Endif    
   	       SM0->(DbSkip())
        EndDo
        RestArea(aAreaSM0)
        If Select("XTMP") > 0 
		   DbSelectArea("XTMP")
		   XTMP->(DbCloseArea())
	    EndIf
        cNomTrans:= alltrim(XTRB->F2_TRANSP)+'-'+Posicione("SA4",1,xFilial("SA4")+PADR(XTRB->F2_TRANSP,6),"A4_NOME")
        cQuery   := "SELECT SUM(D2_QUANT) AS QTDE FROM "+RetSqlName("SD2")+" D2 WHERE D2_DOC = '"+XTRB->F2_DOC+"' AND D2_CLIENTE = '"+cCodcli+"' AND D2.D_E_L_E_T_ = '' "  
        cQuery := ChangeQuery(cQuery)
        DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTMP", .F., .T.)       
        //Estrutura da tabela temporaria
        Dbselectarea("XTMP")
        XTMP->(DbGoTop())
        If XTMP->(!EOF())
           NQtPeca  := XTMP->QTDE
        Else
           NQtPeca  := 0  
        Endif
        If NQtPeca > 0
           nMedia := XTRB->F2_VALBRUT/NQtPeca
        Else
           nMedia := 0
        Endif     
        Vendedor := Posicione("SA3",1,xFilial("SA3")+PADR(XTRB->F2_VEND1,6),"A3_NOME")
        cCodTab  := Posicione("SC5",1,xFilial("SC5")+PADR(XTRB->D2_PEDIDO,6),"C5_TABELA")
        cTabela  := cCodTab+'-'+Posicione("DA0",1,xFilial("DA0")+PADR(cCodTab,3),"DA0_DESCRI")
        cCondPgto:= Posicione("SE4",1,xFilial("SE4")+PADR(XTRB->F2_COND,3),"E4_DESCRI")
        nQtdNf++ 
        nvlTot:= nvlTot+XTRB->F2_VALBRUT
		AADD(aList5,{"", dEmissao,XTRB->F2_FILIAL+" - "+cNomFil,XTRB->F2_DOC,cNomTrans,XTRB->F2_VALBRUT,NQtPeca,nMedia,Vendedor,cTabela,cCondPgto,XTRB->D2_PEDIDO,"",""}) 
		XTRB->(dbSKIP())

	Enddo
	If len(aList5)== 0
		aAdd(aList5,{ "","","","","","","","","","","","","",""})
	Endif		
	//While .T.

	Aadd(aObjects, {100, 010, .T., .F.})
	//	Aadd(aObjects, {200, 200, .T., .T.})
	Aadd(aObjects, {100, 100, .T., .T.})

	aInfo   := {aSize[1], aSize[2], aSize[3], aSize[4], 3, 2}
	aPosObj := MsObjSize(aInfo, aObjects)


	DEFINE MSDIALOG oDl5g TITLE cTitulo From 000,000 To 530,935 OF oMainWnd PIXEL
 	@ 025,004 LISTBOX oLtBx5 VAR cListBox5 Fields HEADER OEMTOANSI(aTitulo2[1]),OEMTOANSI(aTitulo2[2]),OEMTOANSI(aTitulo2[3]),OEMTOANSI(aTitulo2[4]),OEMTOANSI(aTitulo2[5]),OEMTOANSI(aTitulo2[6]),OEMTOANSI(aTitulo2[7]),OEMTOANSI(aTitulo2[8]),OEMTOANSI(aTitulo2[9]),OEMTOANSI(aTitulo2[10]),OEMTOANSI(aTitulo2[11]),OEMTOANSI(aTitulo2[12]),OEMTOANSI(aTitulo2[13]),OEMTOANSI(aTitulo2[14]) FIELDSIZES 14, GetTextWidth(0,"BB"), GetTextWidth(0,"BBBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBBBBBBBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBB"), GetTextWidth(0,"BBBBBBBBB"), GetTextWidth(0,"BBBBBBBB"), GetTextWidth(0,"BBBBBBBB"), GetTextWidth(0,"BBBBBBB"), GetTextWidth(0,"BBBBBBB"),GetTextWidth(0,"BBBBBBB"),	GetTextWidth(0,"BBBBBBB"), GetTextWidth(0,"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB") SIZE 460,200 SCROLL PIXEL 
	@ 230,020 SAY "Total de Notas Fiscais : " SIZE 060,12 OF oDl5g PIXEL  COLOR CLR_RED	 
	@ 230,105 MSGET onQtdNf VAR nQtdNf  When .f. SIZE 060,08 OF oDl5g PIXEL Picture "@E 999,999"
	@ 230,220 SAY "Total Faturado: " SIZE 060,12 OF oDl5g PIXEL  COLOR CLR_RED	 
	@ 230,305 MSGET onvlTot VAR nvlTot  When .f. SIZE 060,08 OF oDl5g PIXEL Picture "@E 999,999,999.99"
	
	oLtBx5:SetArray(aList5)
    Define SButton  From 245,400 Type 1 Action oDl5g:End() Enable Of oDl5g Pixel //Apaga
	oLtBx5:bLine 		:= {|| RCFATLIS5(@oLtBx5)}
	oLtBx5:blDblClick 	:= {|| xPosFat(aList5,oLtBx5:nAt,lRet,nChamada,SA1->A1_COD,SA1->A1_LOJA)}   
	ACTIVATE MSDIALOG oDl5g Centered //ON INIT EnchoiceBar(oDl2g,bOK,,,aBotao)
	SetKey(12,NIL)
	SetKey(16,NIL)
	SetKey(22,NIL)
	RestArea(aArea)
Return(lRet)


Static Function RFATC01C() 
MSGsTOP("mOSTRAR Dados do Cliente - Espec�fico")
Return()


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �XPOSFAT�Autor  �Robson Melo            � Data �  05/02/19   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina responsavel por abrir NF posicionada na tela chamada���
��           � a partir do duplo click									  ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function xPosFat(aList5,nLinha,lRet,nChamada,cCliente,cLoja)
	Local aArea      := GetArea()
	Local nMCusto    := 0
	Local cFilAnt    := SM0->M0_CODFIL
	Local cEmpAnt    := SM0->M0_CODIGO
	Local cFilSf2	 := SubStr( Alltrim(aList5[nLinha,3]),1,4)
	Private nOpc     := 0  
	Private Inclui   := .F.						//Se inclui registro
	Private Altera   := .F.						//Se altera registro 
	

	DbSelectArea("SF2")
	SF2->(DbSetorder(1))
//	if MsgYesNo("Confirma Pesquizar "+cCliente +"/"+ cLoja)
	If SF2->(DbSeek(cFilSf2 + Alltrim(aList5[nLinha,4])))

		//AxVisual("SF2",SF2->(Recno()),2)
		Mc090Visual("SF2",SF2->(Recno()),2)
		
	Else
		msgStop('NF N�o encontrada!  ' )
	Endif    
//	Endif	
	RestArea(aArea)
Return 


Static Function xPosCli(aList3,nLinha,lRet,nChamada,cCliente,cLoja) 

Local cString := "SA1" 
Local cFilter := "" 
LOCAL aCores := {} 
Private cCadastro := "Cadastro de Cliente" 
Private aRotina   := {} 

AADD(aRotina,{"Visualizar","AxVisual", 0, 2 }) 
AADD(aRotina,{"Alterar"   ,"axAltera", 0, 4 }) 

cFilter := "A1_COD = '"+cCodCli+"' " 

dbSelectArea("SA1") 
dbSetOrder(1) 

If SA1->(DbSeek(xFilial("SA1") + Alltrim(cCliente) + alltrim(cLoja)))
	mBrowse( 6,1,22,75,cString,,,,,,,,,,,,,,cFilter) 
Else
	msgStop('Cliente N�o encontrado!  ' )
Endif

Return 