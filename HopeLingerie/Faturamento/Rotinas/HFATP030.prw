#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"

#define cEOL    chr(13) + chr(10)

User Function HFATP030()
	
	Local aSays 		:= {}
	Local aButtons 		:= {}
	Local nOpcA			:= 0
	Local cCadastro 	:= "Importa��o EDI Americanas"
	Private oProcesso 	:= Nil
	Private cTXTFile 	:= ""
	Private cPedido 	:= ""
	Private cAlertTab	:= ""
	
	AADD(aSays,OemToAnsi("Esta rotina tem por objetivo receber o arquivo de "))
	AADD(aSays,OemToAnsi(".TXT de EDI das Lojas Americanas.                 "))
	
	AADD(aButtons, { 19,.T.,{|o| nOpcA:= 1, o:oWnd:End() }} )//-- Botao Avancar
	AADD(aButtons, {  2,.T.,{|o| nOpcA:= 0, o:oWnd:End() }} )//-- Botao Cancelar
	
	FormBatch( cCadastro, aSays, aButtons,,200,405 )
	
	If nOpcA == 1 //Confirmacao de processamento
		oProcesso := MsNewProcess():New({|lEnd| doIt() },"Lendo Arquivo(s)...")//Funcao de leitura do arquivo que transforma o conteudo lido em Array
		oProcesso:Activate()
	EndIf

Return nil


Static Function doIt()

	Local alinha:= {}

	oProcesso:SetRegua1( 2 )
	alinha:= doLerTxt() //Leitura do arquivo(TXT)

	If cErro1<>'1'
		If Len(alinha) > 0
			oProcesso:IncRegua1("Gravado Informacoes: ")
			lRet	:= doProcess(@alinha)
			If !lRet
				cTitErr			:= "Pedido incluido n� "+cPedido
				If cAlertTab == ""
					cAlertTabComp	:= cTitErr +" , incluido com �xito !"+cEOL
				Else
					cAlertTabComp	:= cTitErr +cEOL
					cAlertTabComp	+= +cEOL
					cAlertTabComp	+= "Os itens abaixo n�o existem na tabela de Pre�os !"+cEOL
					cAlertTabComp	+= "Pre�os recuperados do Pedido de Compra do Cliente."+cEOL
					cAlertTabComp	+= cEOL
					cAlertTabComp	+= cAlertTab
				End
				
				DEFINE MSDIALOG oAlert FROM 0,0 TO 330,420 PIXEL TITLE cTitErr
				@ 005,005 GET oMemo VAR cAlertTabComp MEMO SIZE 200,135 PIXEL OF oAlert
				@ 150,005 BUTTON "Finalizar" SIZE 55,10 ACTION (oAlert:end())
				ACTIVATE MSDIALOG oAlert CENTER
				
			Endif
		Else
			MsgSTop("O Arquivo informado esta inv�lido !","EDI")
			Return nil
		EndIf
	EndIf

return nil


Static Function doLerTxt()

	Local nHdlLog 	:= 0
	Local alinha	:= {}
	Local nLast		:= 0
	Local nlinha	:= 0
	Local cErro		:= ""
	Public cErro1	:= ""

	cTXTFile := UFDIR()

	If Empty(cTXTFile)
		Return alinha
	EndIf

	oProcesso:IncRegua1("Lendo Arquivo: "+cTXTFile ) // Atualiza a barra de processamento

	If File(cTXTFile)
		nHdlLog := FOpen(cTXTFile, 2) 	//Abertura do arquivo
		If nHdlLog == -1 				//verifica se abriu com sucesso
			cErro += "N�o foi pross�vel abrir o Arquivo selecionado ["+Alltrim(cTXTFile)+"] !" +cEOL
			Return nil
		Else
			FT_FUSE(cTXTFile)
			FT_FGOTOP()
			nLast := FT_FLastRec()		// Retorna o numero de linhas do arquivo
			oProcesso:SetRegua2(nLast)  //Define o limite da regua

			While !FT_FEOF()

				cLineCpos := AllTrim(FT_FREADLN())
				oProcesso:IncRegua2("Lendo "+alltrim(str(nlinha))+" de "+ alltrim(str( nLast )))
					
				If SubStr(cLineCpos,1,2) $ "01/02"
					Aadd(alinha, cLineCpos)
				End

				nlinha++
				FT_FSKIP()
			Enddo
		EndIf
	Else
		cErro += "Arquivo selecionado ["+Alltrim(cTXTFile)+"] n�o encontrado !" +cEOL
		Return nil
	EndIf

	If !Empty(cErro)
		EECView(cErro)
	EndIf

Return alinha


static function doProcess(alinhTxt)
	
Local nx	:= 0
Local _i	:= 0
Local ntam	:= len(alinhTxt)

oProcesso:SetRegua2(ntam) //Define o limite da regua

//For nx:=1 to ntam

	oProcesso:IncRegua2("Gravando item: "+alltrim(str( 1 )) +" de "+ alltrim(str( ntam )))

	cNroPed := GetSX8Num("SC5","C5_NUM",,1)
	DbSelectArea("SC5")
	DbSetOrder(1)
	
	While DbSeek(xfilial("SC5")+cNroPed)
		ConfirmSX8()
		cNroPed := SOMA1(cNroPed)
	End
	
	cPedRak 	:= SubStr(alinhTxt[1],6,10)
	dEmissao	:= SubStr(alinhTxt[1],16,8)
	
	//GravaLog("Iniciando a importa��o do pedido: "+cPedRak)
	
 	//query para capturar o Codigo do Cliente 
	_Qry := " SELECT A1_COD+A1_LOJA AS CODFOR FROM SA1010 (NOLOCK) WHERE D_E_L_E_T_ <> '*' AND A1_XEANFOR = '"+SubStr(alinhTxt[1],53,13)+"' "+cEOL
	If Select("TMPSA1") > 0
		TMPSA1->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSA1",.T.,.T.)
	
	If AllTrim(TMPSA1->CODFOR) <> ""

		DbSelectArea("SA1")
		DbSetOrder(1)
		DbSeek(xFilial("SA1")+TMPSA1->CODFOR)
		cCodCli 	:= SA1->A1_COD
		cLojCli 	:= SA1->A1_LOJA
		
		cNature		:= ""
		
		If !Empty(SA1->A1_NATUREZ)
			cNature := SA1->A1_NATUREZ
		EndIf
		
		cCndPag 	:= "285"
		nDesconto	:= 0
//		cDiaPVecto	:= SubStr(alinhTxt[1],96,3)
//		dVcto		:= If(SubStr(alinhTxt[1],99,8)<>"00000000",SubStr(alinhTxt[1],99,8),DtoS(Stod(dEmissao)+Val(cDiaPVecto)))
		cCdPrz 		:= "001"
		dDtEntrega	:= SubStr(alinhTxt[1],79,8)
		cPolitc		:= SUPERGETMV("HP_POLAME", .T., "011")//"011"			//Politica Magazine
		cTabPrz		:= SUPERGETMV("HP_TABAME", .T., "614")//"417"			//Tabela LASA 2018
		cTpPed		:= SUPERGETMV("HP_TPPAME", .T., "018")//"018"			//Tipo de pedido Magazine
		cTiposDesc  := ""
		cRepresent	:= POSICIONE("ZA4",1,xFilial("ZA4")+SA1->A1_XMICRRE,"ZA4_REPRE")
		cPercRepre	:= POSICIONE("SA3",1,xFilial("ZA3")+cRepresent,"A3_COMIS")
		cGerente	:= POSICIONE("ZA4",1,xFilial("ZA4")+SA1->A1_XMICRRE,"ZA4_GEREN")
		cPercGer	:= POSICIONE("SA3",1,xFilial("ZA3")+cGerente,"A3_COMIS2")
		cSuperv		:= POSICIONE("ZA4",1,xFilial("ZA4")+SA1->A1_XMICRRE,"ZA4_SUPERV")
		cPercSuper	:= POSICIONE("SA3",1,xFilial("ZA3")+cSuperv,"A3_COMIS3") 
		cTpOper		:= Posicione("SZ1",1,xFilial("SZ1")+cTpPed,"Z1_TPOPER")
	

		dDtFimVig:=  POSICIONE("DA0",1,xFilial("DA0")+cTabPrz,"DA0_DATATE")

		If dDtFimVig < dDataBase
			MsgAlert("Tabela de pre�o fora da vig�ncia!")
			Return
		Endif

		If dDtFimVig
		Endif

		If AllTrim(cTpOper) == ""
			cTpOper := "01"
		End
		
		nPercDesc 	:= u_pegadesc(cCodCli,cLojCli,nDesconto)
		
		aCabec := {}
		
		aadd(aCabec,{"C5_NUM"    	, cNroPed					,Nil})
		aadd(aCabec,{"C5_TIPO"   	, "N"						,Nil})
		aadd(aCabec,{"C5_TABELA" 	, cTabPrz					,Nil})
		aadd(aCabec,{"C5_FECENT" 	, stod(dDtEntrega)			,Nil})
		aadd(aCabec,{"C5_CLIENTE"	, cCodCli					,Nil})
		aadd(aCabec,{"C5_LOJACLI"	, cLojCli					,Nil})
		aadd(aCabec,{"C5_CLIENT" 	, cCodCli					,Nil})
		aadd(aCabec,{"C5_LOJAENT"	, cLojCli					,Nil})
		aadd(aCabec,{"C5_POLCOM" 	, cPolitc					,Nil})
		aadd(aCabec,{"C5_TPPED"  	, cTpPed					,Nil})
		aadd(aCabec,{"C5_CONDPAG"	, cCndPag					,Nil})
		aadd(aCabec,{"C5_NATUREZ"	, cNature					,Nil})
		aadd(aCabec,{"C5_XPEDRAK"	, cPedRak					,Nil})
		aadd(aCabec,{"C5_DESC1"		, nPercDesc					,Nil})
		aadd(aCabec,{"C5_PRAZO"  	, cCdPrz					,Nil})
		aadd(aCabec,{"C5_EMISSAO"	, stod(dEmissao)			,Nil})
		aadd(aCabec,{"C5_ORIGEM" 	, 'AMERICANA'				,Nil})
		aadd(aCabec,{"C5_VEND1"  	, cRepresent				,Nil})
		aadd(aCabec,{"C5_COMIS1" 	, cPercRepre				,Nil})
		aadd(aCabec,{"C5_VEND2"  	, cGerente					,Nil})
		aadd(aCabec,{"C5_COMIS2" 	, cPercGer					,Nil})
		aadd(aCabec,{"C5_VEND3"  	, cSuperv					,Nil})
		aadd(aCabec,{"C5_COMIS3" 	, cPercSuper				,Nil})
		aadd(aCabec,{"C5_XPEDCLI"	, cPedRak					,Nil})
		aadd(aCabec,{"C5_XCANAL" 	, SA1->A1_XCANAL			,Nil})
		aadd(aCabec,{"C5_XCANALD"	, SA1->A1_XCANALD			,Nil})
		aadd(aCabec,{"C5_XCODREG"	, SA1->A1_XCODREG			,Nil})
		aadd(aCabec,{"C5_XDESREG"	, SA1->A1_XDESREG			,Nil})
		aadd(aCabec,{"C5_XMICRRE"	, SA1->A1_XMICRRE			,Nil})
		aadd(aCabec,{"C5_XMICRDE"	, SA1->A1_XMICRDE			,Nil})
		aadd(aCabec,{"C5_XINSTRU"	, SubStr(cTiposDesc,1,250)	,Nil})
		
		aItens		:= {}
		cItem		:= "01"
	
		For nx:=2 to ntam
		
			oProcesso:IncRegua2("Gravando item: "+alltrim(str( nx )) +" de "+ alltrim(str( ntam )))
			
			If SubStr(alinhTxt[nx],1,2) =="02"
			
				cEAN := Alltrim(str(val(SubStr(alinhTxt[nx],3,14))))
			
				DbSelectArea("SB1")
				DbSetOrder(5)	//Codigo de Barras
				DbSeek(xFilial("SB1")+cEAN)
				
				cProd 	:= SB1->B1_COD
				cDescri := GetAdvFVal("SB4","B4_DESC",xFilial("SB4")+SubStr(SB1->B1_COD,1,8),1,"")
				cQuant	:= val(Substr(alinhTxt[nx],17,10)+'.'+Substr(alinhTxt[nx],27,3))
				cTES  	:= MaTesInt(2,cTpOper,cCodCli,cLojCli,"C",SB1->B1_COD,)
				If alltrim(cTES) = ""
					cTES := "501"
				Endif
		
				If !Empty(cTabPrz)
					DbSelectArea("DA1")
					DbSetOrder(1)
					If DbSeek(xFilial("DA1")+ cTabPrz + cProd )
						nPrUnit := DA1->DA1_PRCVEN
					Else 
						nPrUnit := 0
					EndIf
				EndIf
			
				If nPercDesc > 0
					nPrecVend	:= Round(nPrUnit - ((nPercDesc/100) * nPrUnit),2)
					nPvenTot	:= Round((nPrecVend * (1-(nPercDesc/100))* cQuant ),2)
				Else
					nPrecVend	:= Round(nPrUnit,2)
					nPvenTot	:= Round((nPrecVend * cQuant),2)
				EndIf
				
				If nPrecVend == 0
					nPrUnit		:= val(Substr(alinhTxt[nx],159,13)+'.'+Substr(alinhTxt[nx],172,4)) 
					nPrecVend	:= Round(nPrUnit,2)
					nPvenTot	:= Round((nPrecVend * cQuant),2)
					If cAlertTab == ""
						cAlertTab	:= "Item: "+cItem+" - Produto: "+AllTrim(SB1->B1_COD)+cEOL 
					Else
						cAlertTab	+= "Item: "+cItem+" - Produto: "+AllTrim(SB1->B1_COD)+cEOL
					EndIf
				EndIf
				
				aLinha := {}
				aadd(aLinha,{"C6_ITEM"		, cItem										,Nil})
				aadd(aLinha,{"C6_PRODUTO"	, AllTrim(SB1->B1_COD)						,Nil})
				aadd(aLinha,{"C6_QTDVEN"	, If (AllTrim(SB1->B1_COD)<>'',cQuant,0)	,Nil})
				aadd(aLinha,{"C6_PRCVEN"	, nPrecVend									,Nil})
				aadd(aLinha,{"C6_PRUNIT"	, nPrUnit									,Nil})
				aadd(aLinha,{"C6_VALOR"		, nPvenTot									,Nil})
				aadd(aLinha,{"C6_TES"		, cTES										,Nil})
				aadd(aLinha,{"C6_ENTREG"    , stod(dDtEntrega)							,Nil})
				aadd(aLinha,{"C6_LOCAL"		, "E0"										,Nil})
				aadd(aLinha,{"C6_GRADE"		, "S"										,Nil})
				aadd(aLinha,{"C6_ITEMGRD"	, strzero(val(cItem),3)						,Nil})
				If !Empty(cDescri)                                   //Grade: campo da descricao C6_DESCRI
					aadd(aLinha,{"C6_DESCRI"	, If (AllTrim(SB1->B1_COD)<>'',cDescri,"< -- Invalido")			,Nil})
				EndIf
				aadd(aLinha,{"C6_OPER"		, cTpOper									,Nil})
			
				aadd(aItens,aLinha)
			
				cItem := SOMA1(cItem)
				
			EndIf
	
		Next nx
	
		For _i := 1 to len(aItens)
			If _i <> 1
				If SubStr(aitens[_i][2][2],1,8)+AllTrim(Str(aitens[_i][5][2]*100)) = SubStr(aitens[_i-1][2][2],1,8)+AllTrim(Str(aitens[_i-1][5][2]*100)) //Grade: Alterado If para quebrar por referencia + Valor
					aItens[_i][1][2] := aItens[_i-1][1][2]
					aItens[_i][11][2] := SOMA1(aItens[_i-1][11][2])
				Else
					aItens[_i][1][2] := SOMA1(aItens[_i-1][1][2])
					aItens[_i][11][2] := "001"
				Endif
			Endif
		Next
	
		BEGIN TRANSACTION
		
			lMsErroAuto := .F.
		
			If Len(aCabec) > 0 .and. Len(aItens) > 0
		
				MATA410(aCabec,aItens,3)
		
				If lMsErroAuto
					MostraErro()
					//GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedRak)
					lMsErroAuto := .T.
				Else
					
					DbSelectArea("SC5")
					DbSetOrder(1)
					If DbSeek(xFilial("SC5")+cNroPed)
						If Empty(SC5->C5_TABELA)
							RecLock("SC5",.F.)
							Replace C5_TABELA	with cTabPrz
							Replace C5_FECENT	with stod(dDtEntrega)
							MsUnlock()
						EndIf
					EndIf	
					
					//GravaLog("Pedido Protheus n� "+cNroPed+" foi gerado com sucesso. Pedido: "+cPedRak)
					u_AjusDesc(cNroPed,0,nPercDesc) //Executa a funcao 2x
					u_AjusDesc(cNroPed,0,nPercDesc)
				
					ConfirmSX8()
				EndIf
			Endif
			
		END TRANSACTION
	Else
		Alert("Codigo do Fornecedor (EAN Fornecedor "+SubStr(alinhTxt[1],40,13)+"), n�o encontrado")
	EndIf
	
	cPedido	:= cNroPed
	
return (lMsErroAuto)


Static Function UFDIR()

	Local cArq		:= ""
	Local aDir		:= {}
	Local cMask		:= "Selecione o arquivo (*.ord) |*.ord*"		//(Caracter) -> M�scara para filtro (Ex: 'Informes Protheus (*.JPG) | *.JPG')
	Local cTitle	:= "Selecione arquivo"				//(Caracter) -> T�tulo da Janela
	Local nMask		:= 1								//(Num�rico) -> N�mero da m�scara padr�o ( Ex: 1 p/ *.JPG ) (** 0 = Mais de uma mascara**)
	Local cDir		:= "C:\Dir\"						//(Caracter) -> Diret�rio inicial
	Local lSave		:= .T.								//(L�gico)   -> .T. para mostrar o bot�o como 'Abrir' e .F. para botao 'Salvar'
	Local lServer	:= .T.								//(L�gico)   -> .T. exibe diret�rio do servidor

	//������������������������������������������������������������������������������������Ŀ
	//� Comando para selecionar um arquivo.                                                �
	//�		GETF_NOCHANGEDIR    // Impede que o diretorio definido seja mudado             �
	//�		GETF_LOCALFLOPPY    // Mostra arquivos do drive de Disquete                    �
	//�		GETF_LOCALHARD      // Mostra arquivos dos Drives locais como HD e CD/DVD      �
	//�		GETF_NETWORKDRIVE   // Mostra pastas compartilhadas da rede                    �
	//�		GETF_RETDIRECTORY   // Retorna apenas o diret�rio e n�o o nome do arquivo      �
	//��������������������������������������������������������������������������������������

	//Seleciona o arquivo
	cArq 	:= cGetFile(cMask,cTitle,nMask,cDir,lSave,GETF_LOCALHARD+GETF_NETWORKDRIVE,lServer)

	aDir	:= { { cArq } }

	mvRet := UPPER(cArq)

Return (mvRet)