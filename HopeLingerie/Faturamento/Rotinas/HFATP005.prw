#include "rwmake.ch"

// #########################################################################################
// Projeto: Hope
// Modulo : Faturamento
// Fonte  : hFATP005
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 09/10/16 | Actual Trend      | Cadastro de Macro Regi�o 
// ---------+-------------------+-----------------------------------------------------------

User Function HFATP005()

	Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
	Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.
	Private cPerg   := "ZA4"

	Private cString := "ZA4"

	dbSelectArea("ZA4")
	dbSetOrder(1)

	cPerg   := "ZA4"

	Pergunte(cPerg,.F.)

	SetKey(123,{|| Pergunte(cPerg,.T.)}) // Seta a tecla F12 para acionamento dos parametros

	AxCadastro(cString,"Cadastro de Macro Regi�o", cVldExc, cVldAlt)

	Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros

Return
