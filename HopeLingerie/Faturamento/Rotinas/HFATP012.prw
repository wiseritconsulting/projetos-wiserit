#Include "PROTHEUS.CH"

User Function HFATP012()
	local cVldAlt := ".T."
	local cVldExc := ".T."
	local cAlias
	
	cAlias := "SZU"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	private cCadastro := "Regra de libera��o de Pedido de Venda"
	aRotina := {;
		{ "Pesquisar" , "AxPesqui", 0, 1},;
		{ "Visualizar", "u_PolCo2(4)", 0, 2},;
		{ "Incluir"   , "u_PolCo2(1)", 0, 3};
		}
//		{ "Copiar"   	, "u_PolCo2(2)", 0, 4};

	dbSelectArea(cAlias)
	mBrowse( 6, 1, 22, 75, cAlias)
	
return

User Function PolCo2(_tp)

	Local oButton1
	Local oButton2
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oGet6
	Local oGet7
	Local oGet8
	Local oCheckBo01
	Local oCheckBo02
	Local oCheckBo03
	Local oCheckBo04
	Local oCheckBo05
	Local oCheckBo06
	Local oCheckBo07
	Local oCheckBo08
	Local oCheckBo09
	Local oCheckBo10
	Local oCheckBo11
	Local oCheckBo12
	Local oCheckBo13
	Local oCheckBo14
	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local oSay6
	Local oGet7
	Private oFolder1
	Private cGet1			:= CodSZU()
	Private cGet2			:= ddatabase
	Private cGet3			:= cUserName
	Private cGet4			:= 0
	Private cGet5			:= 0
	Private cGet6			:= 0
	Private cGet7			:= 0
	Private cGet8			:= 0
	Private lCheckBo01 	:= .F.
	Private lCheckBo02 	:= .F.
	Private lCheckBo03 	:= .F.
	Private lCheckBo04 	:= .F.
	Private lCheckBo05 	:= .F.
	Private lCheckBo06 	:= .F.
	Private lCheckBo07 	:= .F.
	Private lCheckBo08 	:= .F.
	Private lCheckBo09 	:= .F.
	Private lCheckBo10 	:= .F.
	Private lCheckBo11 	:= .F.
//Private lCheckBo12 	:= .F.
	Private lCheckBo13 	:= .F.
	Private lCheckBo14 	:= .F.

	Static oDlg

	If _tp <> 1
		cGet1	:= SZU->ZU_CODIGO			//Codigo Regra
		cGet2	:= SZU->ZU_DATA			//Data
		cGet3	:= SZU->ZU_USUARIO		//Usu�rio
		cGet4	:= SZU->ZU_VENCDIA		//Em Abertos Vencidos (Dias)
		cGet5	:= SZU->ZU_ATRAZO			//Pag. c/ atrazo maior (Dias)
		cGet6	:= SZU->ZU_INATIVO		//Dias Inativo
		cGet7	:= SZU->ZU_LIMITE			//Percentual de limite
		cGet8	:= SZU->ZU_VLMINPD		//Valor Minimo Pedido
		lCheckBo01  := If(SZU->ZU_TITPROT="S",.T.,.F.)	//T�tulos Protesto
		lCheckBo02  := If(SZU->ZU_CHEQDEV="S",.T.,.F.)	//Cheque Devolvido
		lCheckBo03  := If(SZU->ZU_PERDAS="S",.T.,.F.)		//Perdas (Conta)
		lCheckBo04  := If(SZU->ZU_NOTDEV="S",.T.,.F.)		//Nota D�bito
		lCheckBo05  := If(SZU->ZU_TITATRA="S",.T.,.F.)	//T�tulos Atrasado
		lCheckBo06  := If(SZU->ZU_CLIINAT="S",.T.,.F.)	//Clientes Inativos
		lCheckBo07  := If(SZU->ZU_CLINOVO="S",.T.,.F.)	//Clientes Novos
		lCheckBo08  := If(SZU->ZU_CLIINCO="S",.T.,.F.)	//Dados Cliente Incompleto
		lCheckBo09  := If(SZU->ZU_CLILIMT="S",.T.,.F.)	//Limite Ultrapassado
		lCheckBo10  := If(SZU->ZU_VALPED="S",.T.,.F.)		//Valor Pedido
		lCheckBo11  := If(SZU->ZU_OBSERV="S",.T.,.F.)		//Observacao
//	lCheckBo12  := If(SZU->ZU_PRZADIC="S",.T.,.F.)	//Prazo Adicional
		lCheckBo13  := If(SZU->ZU_PRODINA="S",.T.,.F.)	//Produto Inativo
		lCheckBo14  := If(SZU->ZU_PEDB2B="S",.T.,.F.)		//Pedidos B2B
	Endif

	DEFINE MSDIALOG oDlg TITLE "Regra de Libera��o de Pedidos" FROM 000, 000  TO 600, 1200 COLORS 0, 16777215 PIXEL

	@ 012, 015 SAY oSay1 PROMPT "Codigo"		 			SIZE 025, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 010, 036 MSGET oGet1 VAR cGet1						SIZE 030, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL

	@ 012, 089 SAY oSay2 PROMPT "Data" 					SIZE 025, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 010, 107 MSGET oGet2 VAR cGet2 						SIZE 050, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
	@ 012, 185 SAY oSay3 PROMPT "Usu�rio" 				SIZE 025, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 010, 209 MSGET oGet3 VAR cGet3 						SIZE 378, 010 OF oDlg Picture "@!" 	COLORS 0, 16777215 READONLY PIXEL
	@ 030, 015 CHECKBOX oCheckBo01 VAR lCheckBo01 		PROMPT "T�tulos Protesto" 	SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 030, 090 CHECKBOX oCheckBo02 VAR lCheckBo02 		PROMPT "Cheque Devolvido" 	SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 030, 165 CHECKBOX oCheckBo03 VAR lCheckBo03 		PROMPT "Perdas (Conta)" 		SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 030, 240 CHECKBOX oCheckBo04 VAR lCheckBo04 		PROMPT "Nota D�bito" 		SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 030, 315 CHECKBOX oCheckBo05 VAR lCheckBo05 		PROMPT "T�tulos Atrasado"	SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 030, 390 SAY oSay4 PROMPT "Em Abertos Vencidos (Dias)" 							SIZE 070, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 028, 463 MSGET oGet4 VAR cGet4						SIZE 011, 010 OF oDlg Picture "@E 99" 	COLORS 0, 16777215 PIXEL
	@ 030, 495 SAY oSay5 PROMPT "Pag. c/ atrazo maior (Dias)" 							SIZE 070, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 028, 565 MSGET oGet5 VAR cGet5						SIZE 022, 010 OF oDlg Picture "@E 999" 	COLORS 0, 16777215 PIXEL
	@ 050, 015 CHECKBOX oCheckBo06 VAR lCheckBo06 		PROMPT "Clientes Inativos" 	SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 050, 090 SAY oSay6 PROMPT "Dias Inativo" 			SIZE 050, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 048, 125 MSGET oGet6 VAR cGet6						SIZE 011, 010 OF oDlg Picture "@E 999" 	COLORS 0, 16777215 PIXEL
	@ 050, 165 CHECKBOX oCheckBo07 VAR lCheckBo07 		PROMPT "Clientes Novos" 		SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 050, 240 CHECKBOX oCheckBo08 VAR lCheckBo08 		PROMPT "Dados Cliente Incompleto"	SIZE 070, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 050, 315 CHECKBOX oCheckBo11 VAR lCheckBo11 		PROMPT "Observacao" 			SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 050, 390 CHECKBOX oCheckBo13 VAR lCheckBo13 		PROMPT "Produto Inativo"		SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 070, 015 CHECKBOX oCheckBo09 VAR lCheckBo09 		PROMPT "Limite Ultrapassado" SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 070, 077 SAY oSay7 PROMPT "% Limite Exedido"		SIZE 090, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 068, 125 MSGET oGet7 VAR cGet7						SIZE 015, 010 OF oDlg Picture "@E 999" 	COLORS 0, 16777215 PIXEL
	@ 070, 165 CHECKBOX oCheckBo10 VAR lCheckBo10 		PROMPT "Valor Pedido" 		SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 068, 210 MSGET oGet8 VAR cGet8						SIZE 070, 010 OF oDlg Picture "@E 9,999,999,999,999.99"	COLORS 0, 16777215 PIXEL
	@ 070, 315 CHECKBOX oCheckBo14 VAR lCheckBo14 		PROMPT "Pedidos B2B" 			SIZE 060, 008 OF oDlg COLORS 0, 16777215 PIXEL

	@ 90, 015 FOLDER oFolder1 								SIZE 571, 176 OF oDlg ITEMS "Tab.Pre�o","Cond.Pagto","Tp.Pedido","Cliente" COLORS 0, 16777215 PIXEL
	fMSNewGe1(_tp)
	fMSNewGe2(_tp)
	fMSNewGe3(_tp)
	fMSNewGe4(_tp)
	If _tp == 1 .OR. _tp == 2
		@ 275, 495 BUTTON oButton2 PROMPT "Cancelar" 		SIZE 040, 012 ACTION oDlg:End()  				OF oDlg PIXEL
		@ 275, 546 BUTTON oButton1 PROMPT "Salvar" 		SIZE 040, 012 ACTION (Save(_tp),oDlg:End()) 	OF oDlg PIXEL
	Else
		@ 275, 546 BUTTON oButton2 PROMPT "Cancelar" 		SIZE 040, 012 ACTION oDlg:End()  				OF oDlg PIXEL
	End

	ACTIVATE MSDIALOG oDlg CENTERED

Return

//------------------------------------------------ 
Static Function fMSNewGe1(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"ZV_CODTAB","ZV_DESCRIC"}
	Local aAlterFields := {"ZV_CODTAB"}
	Private aHeader1 := {}
	Private aCols1 := {}

//TABELA DE PRE�OS

	Static oMSNewGe1

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek("ZV_CODTAB")) .AND. aFields[nX]=="ZV_CODTAB"
			Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_VldFMS(1,M->ZV_CODTAB)",;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		ElseIf SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	If _tp <> 1
		DbSelectArea("SZV")
		DbSetOrder(1)
		DbSeek(xfilial("SZV")+cGet1+dtos(SZU->ZU_DATA))

		If Found()
			While !EOF() .and. SZV->ZV_DATA == SZU->ZU_DATA
				Aadd(aCols1,{SZV->ZV_CODTAB,SZV->ZV_DESCRIC,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols1, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols1, aFieldFill)
	Endif
  
	oMSNewGe1 := MsNewGetDados():New( 001, 001, 156, 569, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[1], aHeader1, aCols1)

Return

//------------------------------------------------ 
Static Function fMSNewGe2(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"ZW_CONDPAG","ZW_DESCRIC"}
	Local aAlterFields := {"ZW_CONDPAG"}
	Private aHeader2 := {}
	Private aCols2 := {}
	Static oMSNewGe2

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek("ZW_CONDPAG")) .AND. aFields[nX]=="ZW_CONDPAG"
			Aadd(aHeader2, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_VldFMS(2,M->ZW_CONDPAG)",;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		ElseIf SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader2, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	If _tp <> 1
		DbSelectArea("SZW")
		DbSetOrder(1)
		DbSeek(xfilial("SZW")+cGet1+dtos(SZU->ZU_DATA))
  	
		IF Found()
			While !EOF() .and. SZW->ZW_DATA == SZU->ZU_DATA
				Aadd(aCols2,{SZW->ZW_CONDPAG,SZW->ZW_DESCRIC,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols2, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols2, aFieldFill)
	Endif

	oMSNewGe2 := MsNewGetDados():New( 001, 001, 156, 569, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[2], aHeader2, aCols2)

Return

//------------------------------------------------ 
Static Function fMSNewGe3(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"ZX_TPPED","ZX_DESCRIC"}
	Local aAlterFields := {"ZX_TPPED"}
	Private aHeader3 := {}
	Private aCols3 := {}
	Static oMSNewGe3

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek("ZX_TPPED")) .AND. aFields[nX]=="ZX_TPPED"
			Aadd(aHeader3, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_VldFMS(3,M->ZX_TPPED)",;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		ElseIf SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader3, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	If _tp <> 1
		DbSelectArea("SZX")
		DbSetOrder(1)
		DbSeek(xfilial("SZX")+cGet1+dTos(SZU->ZU_DATA))
  	
		If Found()
			While !EOF() .and. SZX->ZX_DATA == SZU->ZU_DATA
				Aadd(aCols3,{SZX->ZX_TPPED,SZX->ZX_DESCRIC,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols3, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols3, aFieldFill)
	Endif
  
	oMSNewGe3 := MsNewGetDados():New( 001, 001, 156, 569, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[3], aHeader3, aCols3)

Return

//------------------------------------------------ 
Static Function fMSNewGe4(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"ZZ2_CODCLI","ZZ2_LJCLI","ZZ2_NOMCLI"}
	Local aAlterFields := {"ZZ2_CODCLI","ZZ2_LJCLI"}
	Private aHeader4 := {}
	Private aCols4 := {}
	Static oMSNewGe4

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek("ZZ2_CODCLI")) .AND. aFields[nX]=="ZZ2_CODCLI"
			Aadd(aHeader4, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_VldFMS(4,M->ZZ2_CODCLI)",;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		ElseIf SX3->(DbSeek("ZZ2_LJCLI")) .AND. aFields[nX]=="ZZ2_LJCLI"
			Aadd(aHeader4, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_VldFMS(5,ZZ2_LJCLI)",;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		ElseIf SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader4, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX
  
  // Define field values
	If _tp <> 1
		DbSelectArea("ZZ2")
		DbSetOrder(1)
		DbSeek(xfilial("ZZ2")+cGet1+dtos(SZU->ZU_DATA))
  	
		IF Found()
			While !EOF() .and. ZZ2->ZZ2_DATA == SZU->ZU_DATA
				Aadd(aCols4,{ZZ2->ZZ2_CODCLI,ZZ2->ZZ2_LJCLI,ZZ2->ZZ2_NOMCLI,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols4, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols4, aFieldFill)
	Endif

	oMSNewGe4 := MsNewGetDados():New( 001, 001, 156, 569, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[4], aHeader4, aCols4)

Return


Static Function Save(_tp)

	Local I

	DbSelectArea("SZU")
	DbSetOrder(1)
	DbSeek(xfilial("SZU")+cGet1+DTOS(cGet2))

	If !Found()
		RecLock("SZU",.T.)
		Replace ZU_FILIAL			with xfilial("SZU")
		Replace ZU_CODIGO			with cGet1
		Replace ZU_DATA			with cGet2
		Replace ZU_USUARIO		with cGet3
	Else
		RecLock("SZU",.F.)
	Endif
	Replace ZU_VENCDIA		with cGet4
	Replace ZU_ATRAZO			with cGet5
	Replace ZU_INATIVO		with cGet6
	Replace ZU_LIMITE			with cGet7
	Replace ZU_VLMINPD		with cGet8
	Replace ZU_TITPROT		with If(lCheckBo01=.T.,"S","N")
	Replace ZU_CHEQDEV		with If(lCheckBo02=.T.,"S","N")
	Replace ZU_PERDAS			with If(lCheckBo03=.T.,"S","N")
	Replace ZU_NOTDEV			with If(lCheckBo04=.T.,"S","N")
	Replace ZU_TITATRA		with If(lCheckBo05=.T.,"S","N")
	Replace ZU_CLIINAT		with If(lCheckBo06=.T.,"S","N")
	Replace ZU_CLINOVO		with If(lCheckBo07=.T.,"S","N")
	Replace ZU_CLIINCO		with If(lCheckBo08=.T.,"S","N")
	Replace ZU_CLILIMT		with If(lCheckBo09=.T.,"S","N")
	Replace ZU_VALPED			with If(lCheckBo10=.T.,"S","N")
	Replace ZU_OBSERV			with If(lCheckBo11=.T.,"S","N")
//	Replace ZU_PRZADIC		with If(lCheckBo12=.T.,"S","N")
	Replace ZU_PRODINA		with If(lCheckBo13=.T.,"S","N")
	Replace ZU_PEDB2B		with If(lCheckBo14=.T.,"S","N")
	MsUnLock()

	If _tp == 1 .or. _tp == 2
		For i := 1 to Len(OMSNEWGE1:ACOLS)
			If OMSNEWGE1:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE1:ACOLS[i,1]) <> ""
				RecLock("SZV",.T.)
				Replace ZV_FILIAL		with xfilial("SZV")
				Replace ZV_CODSZU		with cGet1
				Replace ZV_DATA		with cGet2
				Replace ZV_CODTAB		with OMSNEWGE1:ACOLS[i,1]
				Replace ZV_DESCRIC	with OMSNEWGE1:ACOLS[i,2]
				MsUnLock()
			Endif
		Next

		For i := 1 to Len(OMSNEWGE2:ACOLS)
			If OMSNEWGE2:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE2:ACOLS[i,1]) <> ""
				RecLock("SZW",.T.)
				Replace ZW_FILIAL		with xfilial("SZW")
				Replace ZW_CODSZU		with cGet1
				Replace ZW_DATA		with cGet2
				Replace ZW_CONDPAG	with OMSNEWGE2:ACOLS[i,1]
				Replace ZW_DESCRIC	with OMSNEWGE2:ACOLS[i,2]
				MsUnLock()
			Endif
		Next

		For i := 1 to Len(OMSNEWGE3:ACOLS)
			If OMSNEWGE3:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE3:ACOLS[i,1]) <> ""
				RecLock("SZX",.T.)
				Replace ZX_FILIAL		with xfilial("SZX")
				Replace ZX_CODSZU		with cGet1
				Replace ZX_DATA		with cGet2
				Replace ZX_TPPED		with OMSNEWGE3:ACOLS[i,1]
				Replace ZX_DESCRIC	with OMSNEWGE3:ACOLS[i,2]
				MsUnLock()
			Endif
		Next
	
		For i := 1 to Len(OMSNEWGE4:ACOLS)
			If OMSNEWGE4:ACOLS[i,4] == .F. .and. alltrim(OMSNEWGE4:ACOLS[i,1]) <> ""
				RecLock("ZZ2",.T.)
				Replace ZZ2_FILIAL	with xfilial("ZZ2")
				Replace ZZ2_CODSZU	with cGet1
				Replace ZZ2_DATA		with cGet2
				Replace ZZ2_CODCLI	with OMSNEWGE4:ACOLS[i,1]
				Replace ZZ2_LJCLI		with OMSNEWGE4:ACOLS[i,2]
				Replace ZZ2_NOMCLI	with OMSNEWGE4:ACOLS[i,3]
				MsUnLock()
			Endif
		Next
	
	Endif

Return

Static Function CodSZU()

	If Select("VLDDB") > 0
		VLDDB->(DbCloseArea())
	EndIf

	cQuery  := " SELECT ISNULL(MAX(ZU_CODIGO),0)+1 AS CODIGO FROM "+RetSQLName("SZU")+" WHERE D_E_L_E_T_<>'*' "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
	lRet := StrZero(VLDDB->CODIGO,4)
		
	VLDDB->(DbCloseArea())
	
Return(lRet)

User Function VldFMS(_nItem,_Cod)

	Local lRet := .T.
	Local _i

	If _nItem == 1

		For _i:=1 to len(OMSNEWGE1:aCols)
			If alltrim(_Cod) == alltrim(OMSNEWGE1:aCols[_i,1]) .and. _i <> OMSNEWGE1:nAT
				Alert("A Tabela de Pre�os j� foi incluida, selecione outra Tabela.")
				lRet := .F.
				Return(lRet)
			ElseIf alltrim(_Cod) == ""
				Alert("N�o poder� incluir uma linha em branco.")
				lRet := .F.
				Return(lRet)
			Else
				If Select("VLDDB") > 0
					VLDDB->(DbCloseArea())
				EndIf
		 	
				cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("DA0")
				cQuery  += " WHERE D_E_L_E_T_<>'*' AND DA0_CODTAB='"+_Cod+"'"
			
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
				If VLDDB->COUNT==0 .and. _Cod<>""
					Alert("A Tabela de Pre�os digitada n�o existe no cadastro.")
					lRet := .F.
					VLDDB->(DbCloseArea())
					Return(lRet)
				End
			
				VLDDB->(DbCloseArea())
			Endif
		Next
	
	ElseIf _nItem == 2
		For _i:=1 to len(OMSNEWGE2:aCols)
			If alltrim(_Cod) == alltrim(OMSNEWGE2:aCols[_i,1]) .and. _i <> OMSNEWGE2:nAT
				Alert("A Condi�ao de Pagamento j� foi incluida, selecione outra Condi��o.")
				lRet := .F.
				Return(lRet)
			ElseIf alltrim(_Cod) == ""
				Alert("N�o poder� incluir uma linha em branco.")
				lRet := .F.
				Return(lRet)
			Else
				If Select("VLDDB") > 0
					VLDDB->(DbCloseArea())
				EndIf
		 	
				cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SE4")
				cQuery  += " WHERE D_E_L_E_T_<>'*' AND E4_CODIGO='"+_Cod+"'"
			
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
				If VLDDB->COUNT==0 .and. _Cod<>""
					Alert("A Condi�ao de Pagamento digitada n�o existe no cadastro.")
					lRet := .F.
					VLDDB->(DbCloseArea())
					Return(lRet)
				End
			
				VLDDB->(DbCloseArea())
			Endif
		Next
	ElseIf _nItem == 3
		For _i:=1 to len(OMSNEWGE3:aCols)
			If alltrim(_Cod) == alltrim(OMSNEWGE3:aCols[_i,1]) .and. _i <> OMSNEWGE3:nAT
				Alert("O Tipo de Pedido j� foi incluido, selecione outra Tipo.")
				lRet := .F.
				Return(lRet)
			ElseIf alltrim(_Cod) == ""
				Alert("N�o poder� incluir uma linha em branco.")
				lRet := .F.
				Return(lRet)
			Else
				If Select("VLDDB") > 0
					VLDDB->(DbCloseArea())
				EndIf
		 	
				cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SZ1")
				cQuery  += " WHERE D_E_L_E_T_<>'*' AND Z1_CODIGO='"+_Cod+"'"
			
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
				If VLDDB->COUNT==0 .and. _Cod<>""
					Alert("A Tipo de Pedido digitado n�o existe no cadastro.")
					lRet := .F.
					VLDDB->(DbCloseArea())
					Return(lRet)
				End
			
				VLDDB->(DbCloseArea())
			Endif
		Next
	ElseIf _nItem == 4
		For _i:=1 to len(OMSNEWGE4:aCols)
			If alltrim(_Cod)<>""
				If Select("VLDDB") > 0
					VLDDB->(DbCloseArea())
				EndIf

				cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SA1")
				cQuery  += " WHERE D_E_L_E_T_<>'*' AND A1_COD='"+_Cod+"' "
						
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
				If VLDDB->COUNT==0 .and. _Cod<>""
					Alert("o Codigo de Cliente digitado n�o existe no cadastro.")
					lRet := .F.
					VLDDB->(DbCloseArea())
					Return(lRet)
				End
			
				VLDDB->(DbCloseArea())
			Endif
		Next
	ElseIf _nItem == 5
		OMSNEWGE4:aCols[OMSNEWGE4:nAT,3] := Posicione( "SA1", 1, xFilial("SA1")+alltrim(alltrim(OMSNEWGE4:aCols[OMSNEWGE4:nAT,1]))+alltrim(_Cod) ,"A1_NOME")
		For _i:=1 to len(OMSNEWGE4:aCols)
			If alltrim(alltrim(OMSNEWGE4:aCols[OMSNEWGE4:nAT,1]))+alltrim(_Cod) == alltrim(OMSNEWGE4:aCols[_i,1])+alltrim(OMSNEWGE4:aCols[_i,2]) .and. _i <> OMSNEWGE4:nAT
				Alert("O Cliente j� foi incluido, selecione outra Cliente.")
				lRet := .F.
				Return(lRet)
			ElseIf alltrim(alltrim(OMSNEWGE4:aCols[OMSNEWGE4:nAT,1])) == ""
				Alert("N�o poder� incluir uma linha com o Codigo em branco.")
				lRet := .F.
				Return(lRet)
			ElseIf alltrim(OMSNEWGE4:aCols[_i,2])<>""
				If Select("VLDDB") > 0
					VLDDB->(DbCloseArea())
				EndIf

				cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SA1")
				cQuery  += " WHERE D_E_L_E_T_<>'*' AND A1_COD='"+alltrim(OMSNEWGE4:aCols[_i,1])+"' AND A1_LOJA='"+_Cod+"'"
						
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
				If VLDDB->COUNT==0 .and. _Cod<>""
					Alert("O Cliente digitado n�o existe no cadastro.")
					lRet := .F.
					VLDDB->(DbCloseArea())
					Return(lRet)
				End
			
				VLDDB->(DbCloseArea())
			Endif
		Next
	End

Return(lRet)