#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"
#include "fileio.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA009  �Autor  �Bruno Parreira      � Data �  04/05/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Exportacao de Cliente B2B                                   ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATA009(cCliente,cLoja)
Local lRet := .T.
Local cExistCli := ""
Local aWsRet    := {}
Local lConta    := .F.
Local lEnd      := .F.
Local lUsr      := .F.

Private cChvA1  := ""
Private cChvA2  := ""
Private cDtTime := ""

Static oWsCli   := NIL

cDtTime := DTOS(DDATABASE) + SubStr(Time(),1,2) + SubStr(Time(),4,2) + SubStr(Time(),7,2)

oWsCli := WSCliente():New()

cChvA1 := AllTrim(SuperGetMV("MV_XRA1B2B",.F.,"")) //Chave A1 para validar acesso ao webservice
cChvA2 := AllTrim(SuperGetMV("MV_XRA2B2B",.F.,"")) //Chave A2 para validar acesso ao webservice

DbSelectArea("SA1")
DbSetOrder(1)
If DbSeek(xFilial("SA1")+cCliente+cLoja)
	cCNPJ := SA1->A1_CGC
	If Len(cCNPJ) = 14 //Permite apenas CNPJ
		MemoWrite("\log_rakuten\B2B\"+cDtTime+"_EXP-Cliente_"+cCNPJ+".txt","In�cio do processamento...")

		nHandle := fopen("\log_rakuten\B2B\"+cDtTime+"_EXP-Cliente_"+cCNPJ+".txt",FO_READWRITE+FO_SHARED)
		
		GravaLog("In�cio do processamento...")
		
		GravaLog("Executando fun��o Listar para o CNPJ: "+cCNPJ)
		
		If oWsCli:Listar(1,"","",cCNPJ,cChvA1,cChvA2)
			cWsRet := oWsCli:oWSListarResult:cDescricao
			
			If "SUCESSO" $ UPPER(cWsRet) 
				
				GravaLog("CNPJ "+cCNPJ+" encontrado. Cliente ser� alterado.")
				//aWsRet := aClone(oWsCli:oWSListarResult:oWSLista:oWSClsUsuario)
				MsgAlert("CNPJ ja cadastrado. Por enquanto somente permitido inclus�o.")
				lRet := .F.
			Else
				GravaLog("CNPJ "+cCNPJ+" n�o encontrado. Cliente ser� inclu�do.")
				
				cCodigo   := SA1->A1_COD
				cNome     := AllTrim(SA1->A1_NREDUZ)
				cSobrenom := AllTrim(SA1->A1_NREDUZ)
				cRazao    := AllTrim(SA1->A1_NOME)
				cFantasia := AllTrim(SA1->A1_NREDUZ)
				cCNPJ     := AllTrim(SA1->A1_CGC)
				cIE       := AllTrim(SA1->A1_INSCR)
				cIM       := AllTrim(SA1->A1_INSCRM)
				cEmail    := AllTrim(SA1->A1_EMAIL)
				cEndereco := AllTrim(SA1->A1_END)
				cNumero   := SubStr(SA1->A1_END,At(",",SA1->A1_END)+2,15)
				cNumero   := SubStr(cNumero,1,At(" ",cNumero)-1)
				cNumero   := AllTrim(cNumero)
				cCompl    := AllTrim(SA1->A1_COMPLEM)
				cBairro   := AllTrim(SA1->A1_BAIRRO)
				cCidade   := AllTrim(SA1->A1_MUN)
				cEstado   := AllTrim(SA1->A1_EST)
				cPais     := AllTrim(SA1->A1_PAIS)
				cCEP      := AllTrim(SA1->A1_CEP)
				cDDD1     := AllTrim(SA1->A1_DDD)
				cTel1     := AllTrim(SA1->A1_TEL)
				cDDD2     := AllTrim(SA1->A1_DDDC)
				cTel2     := AllTrim(SA1->A1_TELC)
				cDDD3     := AllTrim(SA1->A1_DDDE)
				cTel3     := AllTrim(SA1->A1_TELE)
				cLogOn    := cCNPJ
				cSenha    := "123456"
				
				//nLojaCodigo,cContaCodigoInterno,nParceiroCodigo,nTipo,cNome,cSobrenome,cRazaoSocial,cNomeFantasia,cCPF,cCNPJ,cRG,cIE,cIM,cDataNascimento,nContaStatus,cTexto1,cTexto2,cTexto3,nNumero1,nNumero2,nNumero3,cA1,cA2
				GravaLog("Parametros oWsVli:SalvarConta: 1,"+cCodigo+",0,2,"+cNome+","+cSobrenom+","+cRazao+","+cFantasia+",'',"+cCNPJ+",'',"+cIE+","+cIM+","+DtoS(DDATABASE)+",1,'','','',0,0,0")
				If oWsCli:SalvarConta(1,cCodigo,0,2,cNome,cSobrenom,cRazao,cFantasia,"",cCNPJ,"",cIE,cIM,DDATABASE,1,"","","",0,0,0,cChvA1,cChvA2)
				
					cWsRet := oWsCli:oWSSalvarContaResult:cDescricao
					
					GravaLog("Retorno SalvarConta: "+cWsRet+" nCodigo: "+AllTrim(Str(oWsCli:oWSSalvarContaResult:nCodigo)))
					
					If "SUCESSO" $ UPPER(cWsRet) 
						lConta := .T.
						GravaLog("Conta salva com sucesso.")
						GravaLog("Par�metros SalvarEndere�o: 1,"+cCodigo+",0,0,"+cNome+","+cEmail+",2,1,1,"+cEndereco+","+cNumero+","+cCompl+","+cBairro+","+cCidade+","+cEstado+","+cPais+","+cCEP+","+cDDD1+","+cTel1+",'',"+cDDD2+","+cTel2+",'',"+cDDD3+","+cTel3+",'','','','','','',1,'','','',0,0,0")
						//nLojaCodigo,cContaCodigoInterno,nAfiliadoCodigoEcommerce,nCodigoEnderecoEcommerce,cNome,cEmail,nTipo,nFinalidade,nTipoLogradouro,cLogradouro,cNumero,cComplemento,cBairro,cCidade,cEstado,cPais,cCEP,cDDD1,cTelefone1,cRamal1,cDDD2,cTelefone2,cRamal2,cDDD3,cTelefone3,cRamal3,cDDDCelular,cCelular,cDDDFax,cFax,cReferencia,nStatusEndereco,cTexto1,cTexto2,cTexto3,nNumero1,nNumero2,nNumero3
						If oWsCli:SalvarEndereco(1,cCodigo,0,0,cNome,cEmail,2,1,1,cEndereco,cNumero,cCompl,cBairro,cCidade,cEstado,cPais,cCEP,cDDD1,cTel1,"",cDDD2,cTel2,"",cDDD3,cTel3,"","","","","","",1,"","","",0,0,0,cChvA1,cChvA2)
							cWsRet := oWsCli:oWSSalvarEnderecoResult:cDescricao
							
							GravaLog("Retorno SalvarEndereco: "+cWsRet)
							
							If "SUCESSO" $ UPPER(cWsRet) 
								lEnd := .T.
								GravaLog("Endere�o salvo com sucesso.")
							Else
								GravaLog("N�o foi poss�vel cadastrar o endere�o.")
								lEnd := .F.
							EndIf
						Else
							GravaLog("Erro na funcao SalvarEndereco do Webservice Cliente: "+GetWSCError())
							MsgAlert("Ocorreu erro ao salvar o endere�o do cliente na Rakuten.","Expota��o para Rakuten")
							lRet := .F.
						EndIf
						
						GravaLog("Par�metros SalvarUsuario: 1,"+cCodigo+","+cCodigo+","+cLogOn+","+cSenha+","+cNome+","+cSobrenom+","+cEmail+",1,0,1,'','','',0,0,0")
						//nLojaCodigo,cUsuarioCodigoInterno,cContaCodigoInterno,cLogOn,cSenha,cNome,cSobrenome,cEmail,nNewsletter,nSexo,nUsuarioStatus,cTexto1,cTexto2,cTexto3,nNumero1,nNumero2,nNumero3
						If oWsCli:SalvarUsuario(1,cCodigo,cCodigo,cLogOn,cSenha,cNome,cSobrenom,cEmail,1,1,1,"","","",0,0,0,cChvA1,cChvA2)
							cWsRet := oWsCli:oWSSalvarUsuarioResult:cDescricao
							
							GravaLog("Retorno SalvarUsuario: "+cWsRet)
							
							If "SUCESSO" $ UPPER(cWsRet) 
								lUsr := .T.
								GravaLog("Usu�rio salvo com sucesso.")
							Else
								GravaLog("N�o foi poss�vel cadastrar o usu�rio.")
								lUsr := .F.
							EndIf
						Else
							GravaLog("Erro na funcao SalvarUsuario do Webservice Cliente: "+GetWSCError())
							MsgAlert("Ocorreu erro ao salvar o usuario do cliente na Rakuten.","Expota��o para Rakuten")
							lRet := .F.
						EndIf
					Else
						GravaLog("N�o foi poss�vel cadastrar a Conta.")
						lConta := .F.
					EndIf
				Else
					GravaLog("Erro na funcao SalvarConta do Webservice Cliente: "+GetWSCError())
					MsgAlert("Ocorreu erro ao salvar a conta do cliente na Rakuten.","Expota��o para Rakuten")
					lRet := .F.
				EndIf
			EndIf
		Else
			GravaLog("Erro na funcao Listar do Webservice Cliente: "+GetWSCError())
			MsgAlert("Ocorreu erro ao listar o cliente na Rakuten.","Expota��o para Rakuten")
			lRet := .F.
		EndIf
		
		/*cExistCli := EXISTCLI(SA1->A1_CGC) 
		Do Case
			Case cExistCli = "I"
				INCLUICLI()
			Case !Empty(cExistCli)
				ALTERACLI()
			Otherwise
				MsgAlert("Ocorreu erro ao listar o cliente na Rakuten.","Expota��o para Rakuten")
				lRet := .F.
		EndCase*/
		
	Else
		MsgAlert("Somente CNPJ � aceito para exporta��o.","Expota��o para Rakuten")
		lRet := .F.
	EndIf
Else
	MsgAlert("Cliente n�o encontrado!","Expota��o para Rakuten")
	lRet := .F.
EndIf

If lConta .And. lEnd .And. lUsr
	MsgAlert("Cliente exportado com sucesso para a Rakuten!","Expota��o para Rakuten")
	GravaLog("Cliente exportado com sucesso para a Rakuten!")
EndIf

Return lRet

//--- Log do processamento
Static Function GravaLog(cMsg)
Local cDtHora := ""

cDtHora := DTOS(DDATABASE) + SubStr(Time(),1,2) + SubStr(Time(),4,2) + SubStr(Time(),7,2)

Conout(cDtHora+": HFATA009 - "+cMsg)
FWRITE(nHandle,CRLF+cDtHora+": HFATA009 - "+cMsg)

Return
