#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"
#include "fileio.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA008  �Autor  �Bruno Parreira      � Data �  02/08/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Exportacao de Estoque B2B                                   ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATA008(cFlag,cAut) 

Local oWs     	:= NIL
Local oWsEstq	:= NIL
Local oWsPrdFil := NIL
Local oWsPrdPai := NIL
Local aWsRet  	:= {}
Local cWsRet	:= ""
Local cQryEstq	:= ""
Local nQtEstq	:= 0
Local nQtdMin	:= 5 //Criar par�metro para retornar quantidade m�nima de produto
Local nx        := 0

Private cChvA1   := ""
Private cChvA2   := ""
Private cLog     := ""
Private cPerg    := "HFATA008"
Private cHoraIni := Time()
Private cHoraFim := ""
Private cMinEst  := ""
Private nMinEst  := 0
Private lShowroom:= .F.

Default cAut     := "N"
Default cFlag    := ""

lMenu := .T.

If Select("SX2") = 0
	Prepare Environment Empresa "01" Filial "0101"
	lMenu := .F.
Endif

If cAut = "S"
	lMenu := .F.
Endif

cMinEst  := AllTrim(SuperGetMV("MV_XSLDMI2",.F.,"2"))
nMinimo  := Val(cMinEst)

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

MemoWrite("\log_rakuten\B2B\"+cDtTime+"_EXP-Estoque_.txt","In�cio do processamento...")

nHandle := fopen("\log_rakuten\B2B\"+cDtTime+"_EXP-Estoque_.txt",FO_READWRITE+FO_SHARED)

GravaLog("In�cio do processamento...")

oWsEstq	  := WSEstoque():New()
oWsPrdFil := WSSKU():New()
oWsPrdPai := WSProduto():New()

cChvA1 := AllTrim(SuperGetMV("MV_XRA1B2B",.F.,"")) //Chave A1 para validar acesso ao webservice
cChvA2 := AllTrim(SuperGetMV("MV_XRA2B2B",.F.,"")) //Chave A2 para validar acesso ao webservice

If !lMenu
	GravaLog("Executando funcao atualizar estoque automatica")
	xFiltros("",cFlag)
Else
	GravaLog("Executando via menu.")
	AjustaSX1(cPerg)
	If !Pergunte(cPerg)
		GravaLog("Processo cancelado pelo usu�rio.")
		Return
	Else
		//If Empty(mv_par01)
		//	MsgAlert("N�o foi digitado nenhum produto.","Aten��o")
		//	Return	
		//EndIf
		cFlag := mv_par02

		xFiltros(AllTrim(mv_par01),cFlag)	
	EndIf
	
EndIf

nPrd := 0	
cRef := ""
lInativ := .F.

//-----------------------------//
//--- Exporta��o do Estoque ---//
DbSelectArea("TRB")
TRB->(dbGoTop())
If TRB->(!Eof())
	While TRB->(!Eof())
		If !Empty(TRB->CODRAK)
			cProdPrt	:= AllTrim(TRB->CODPROT)
			cPartNbr	:= AllTrim(TRB->CODRAK)
		Else
			cProdPrt	:= AllTrim(TRB->CODPROT)
			cPartNbr	:= AllTrim(TRB->CODPROT)
		EndIf
		
		nEstoque := IF(TRB->TOTAL_E0E1<=0,0,TRB->TOTAL_E0E1)
		//nEstoque := TRB->TOTAL_E0E1
		
		cRef := SubStr(cProdPrt,1,8)
		
		/*For nx := 1 to 1//2
			
			If nx = 2
				cProdPrt := "OUT_"+cProdPrt
				cPartNbr := "OUT_"+cPartNbr
			EndIf*/
			
			DbSelectArea("ZA3")
			DbSetOrder(1)
			If DbSeek(xFilial("ZA3")+"002") //CANAL SHOW ROOM
				dDatLib := ZA3->ZA3_DTINI
			Else
				dDatLib := DDATABASE
			EndIf
			
			lMinEst := .F.
			
			lShowRoom := .F.
			
			If !Empty(TRB->B1_YDATLIB)
				If StoD(TRB->B1_YDATLIB) >= dDatLib .And. AllTrim(TRB->B1_YSITUAC) = "PNV"
					nMinEst := -1
					lMinEst := .T.
					//If AllTrim(TRB->B1_YCICLO) <> '00001'
						nEstoque := 99999
						lShowroom:= .T.
					//EndIf
				EndIf
			EndIf
			
			If TRB->ZA8_DWEB2B <> 'T' .And. !lShowRoom
				TRB->(DbSkip())
				Loop
			EndIf
			
			If !lMinEst .And. Alltrim(TRB->B1_YCICLO) = '00001' .And. AllTrim(TRB->B1_YSITUAC) = "PV"
				nMinEst := -1
				lMinEst := .T.
			EndIf
			
			If !lMinEst
				nMinEst := nMinimo
			EndIf
			
			/*If nEstoque > nMinEst
				nStatus := 1
			Else
				nStatus := 2
			EndIf*/
							
			If oWsEstq:Salvar(0,cPartNbr,cPartNbr,nEstoque,nMinEst,3,cChvA1,cChvA2)			
				cWsRet := oWsEstq:oWSSalvarResult:cDescricao
				GravaLog("Produto: "+TRB->CodProt+" - "+cWsRet)
				/*If "SUCESSO" $ UPPER(cWsRet)
					DbSelectArea("ZZX")
					DbSetOrder(2)
					If DbSeek(xFilial("ZZX")+"B2B"+cProdPrt)
						RecLock("ZZX",.F.)
						ZZX->ZZX_QTDE := IF(TRB->TOTAL_E0E1<=0,0,TRB->TOTAL_E0E1)
						ZZX->ZZX_DATA := DDATABASE
						ZZX->ZZX_HORA := Time()
						ZZX->ZZX_ESTMIN := 0
						ZZX->ZZX_STATUS := "N" //Nao Exportado para Rakuten. Por isso nao precisara realizar exportacao na proxima carga.
						MsUnlock()
					Else
						RecLock("ZZX",.T.)
						ZZX->ZZX_COD    := cProdPrt
						ZZX->ZZX_QTDE   := IF(TRB->TOTAL_E0E1<=0,0,TRB->TOTAL_E0E1)
						ZZX->ZZX_DATA   := DDATABASE
						ZZX->ZZX_HORA   := Time()
						ZZX->ZZX_ORIGEM := "B2B"
						ZZX->ZZX_ESTMIN := 0
						ZZX->ZZX_STATUS := "N" //Nao Exportado
						MsUnlock()
					EndIf
				EndIf*/
			Else			
				GravaLog("Erro na funcao Salvar o Estoque: "+GetWSCError())
				Loop		
			EndIf
			
			If nEstoque <= 0 .Or. TRB->B1_YSITUAC = 'NPNV' .Or. TRB->PRECO <= 0 .Or. TRB->B1_YBLQDEM = 'S' 
				oWsPrdFil:AlterarStatus( 0 , cPartNbr , 2 ,cChvA1,cChvA2)
				cWsRet := oWsPrdFil:oWSAlterarStatusResult:cDescricao
				If !Empty(cWsRet)
					GravaLog("Resultado Inativa Produto: oWsPrdFil:AlterarStatus: " + cPartNbr + " - " + cWsRet)
				EndIf
			EndIf
			
			If nEstoque > nMinEst .And. TRB->B1_YSITUAC <> 'NPNV' .And. TRB->PRECO > 0
				oWsPrdFil:AlterarStatus( 0 , cPartNbr , 1 ,cChvA1,cChvA2)
				cWsRet := oWsPrdFil:oWSAlterarStatusResult:cDescricao
				If !Empty(cWsRet)
					GravaLog("Resultado Ativa Produto: oWsPrdFil:AlterarStatus: "+cPartNbr + " - " + cWsRet)
				EndIf
			//EndIf
			EndIf
			//If TRB->TOTAL_E0E1 <= nMinEst
		//Next
		
		TRB->(dbSkip())
		
		/*If cRef <> SubStr(TRB->TOTAL_E0E1,1,8)
			lInativ := SLDFILHOS(cRef)
			If lInativ
				oWsPrdPai:AlterarStatus(0,cRef,2,cChvA1,cChvA2)
				cWsRet := oWsPrdPai:oWSAlterarStatusResult:cDescricao
				GravaLog("Resultado Inativa Produto: oWsPrdPai:AlterarStatus: "+cRef+" - "+cWsRet)
			Else
				oWsPrdPai:AlterarStatus(0,cRef,1,cChvA1,cChvA2)
				cWsRet := oWsPrdPai:oWSAlterarStatusResult:cDescricao
				GravaLog("Resultado Ativa Produto: oWsPrdPai:AlterarStatus: "+cRef+" - "+cWsRet)
			EndIf
		EndIf
		
		If nPrd > 50
			cTime   := Time()
			cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
			MemoWrite("\log_rakuten\B2C\"+cDtTime+"_EXP-ESTQ.log",cLog)
			cLog := "_"
			nPrd := 0
		EndIf*/
		
		/*nPrd++
			
		If nPrd > 200
			cTime   := Time()
			cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
			MemoWrite("\log_rakuten\B2B\"+cDtTime+"_EXP-ESTQ.log",cLog)
			cLog := "_"
			nPrd := 0
		EndIf*/
	EndDo
Else
	GravaLog("Produto n�o encontrado.")
EndIf

GravaLog("Fim do processamento.")

//MemoWrite("\log_rakuten\B2C\"+cDtTime+"_EXP-ESTQ.log",cLog)

cHoraFim := Time()

If lMenu
	MsgInfo("Extra��o de estoque finalizada."+cHoraIni+" - "+cHoraFim,"Aviso")
EndIf

TRB->(DbCloseArea())

Return

//--- Log do processamento
Static Function GravaLog(cMsg)
Local cHora   := ""
Local cDtHora := ""

cHora   := Time()
cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

Conout(cDtHora+": HFATA008 - "+cMsg)
FWRITE(nHandle,CRLF+cDtHora+": HFATA008 - "+cMsg)
//cLog += CRLF+cDtHora+": HFATA008 - "+cMsg

Return

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Produto?","Produto?","Produto?","Mv_ch1","C",15,0,0,"G","","","","N","Mv_par01",""   ,""   ,""   ,"",""   ,""   ,""   ,"","","","","","","","","",{"Atualiza qual Produto? (Vazio atualiza todos)",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)

//---------------------------------//
//--- Regras Exporta��o Estoque ---//
Static Function xFiltros(cProduto,cFlg)

Local cQuery   := ""//GetNextAlias()
Local cWhere   := ""
Local cArmazem := AllTrim(SuperGetMV("MV_XARMB2C",.F.,"'E0','E1'"))
Local cTabB2B  := SuperGetMV("MV_XTABB2B",.F.,"'140'")
Default cFlg   := ""

/*If !Empty(cProduto)
	//cWhere := "SB1.B1_YDWEB = 'S' "
	If Len(cProduto) <= 8 
		cWhere += "AND SUBSTRING(SB1.B1_COD,1,8) = '"+AllTrim(cProduto)+"' "
	Else
		cWhere += "AND SB1.B1_COD = '"+AllTrim(cProduto)+"' "
	EndIf	
Else
	//cWhere := "SB1.B1_YDWEB = 'S' "
EndIf*/

/*SELECT SB1.B1_COD as CODPROT, SB1.B1_YFORMAT as CODRAK,B1_YDATLIB,B1_YCICLO,B1_YSITUAC,SB1.B1_YDWEB2B,ISNULL(ZA8_DWEB2B,'F') AS ZA8_DWEB2B, 
ISNULL(DA1_PRCVEN,0) as PRECO, SUM(ISNULL(SB2.B2_QATU,0))-ISNULL(MAX(CARTEIRA_TOTAL),0) as TOTAL_E0E1 
FROM SB1010 SB1 
LEFT JOIN SB2010 SB2   		
		ON SB1.B1_COD = SB2.B2_COD 
		AND SB2.B2_FILIAL = '0101'		
		AND SB2.D_E_L_E_T_ = '' 
       	AND SB2.B2_LOCAL IN ('E0','E1') 
 LEFT JOIN ZA8010 ZA8 
 ON ZA8_CODIGO = B1_YSITUAC 
 AND ZA8_FILIAL = '    ' 
 AND ZA8.D_E_L_E_T_ = '' 
 LEFT JOIN DA1010 DA1 
 ON DA1.DA1_CODPRO = B2_COD 
 AND DA1.D_E_L_E_T_ = '' 
 AND DA1.DA1_CODTAB IN ('140') 
 LEFT JOIN CART_ATUAL CART ON CART.B1_COD = SB2.B2_COD 
WHERE SB1.D_E_L_E_T_ = '' 
AND SB1.B1_FILIAL = '    ' 
		 AND SB1.B1_COD = '00023840HVI0042' 
		AND SB1.B1_YCOLECA IN	( SELECT ZAA.ZAA_CODIGO  
								  FROM ZAA010 ZAA 
								  WHERE ZAA.ZAA_FILIAL = '    ' 
								  AND ZAA.D_E_L_E_T_ = '' 
								  AND ZAA.ZAA_DWEB = 'S' 
								) 
GROUP BY SB1.B1_COD, SB1.B1_YFORMAT,B1_YDATLIB,B1_YCICLO,B1_YSITUAC,SB1.B1_YDWEB2B,ZA8_DWEB2B,DA1_PRCVEN 
ORDER BY SB1.B1_COD */

cQuery := "SELECT SB1.B1_COD as CODPROT, SB1.B1_YFORMAT as CODRAK,B1_YDATLIB,B1_YBLQDEM,B1_YCICLO,B1_YSITUAC,SB1.B1_YDWEB2B,ISNULL(ZA8_DWEB2B,'F') AS ZA8_DWEB2B, "
cQuery += CRLF + "ISNULL(DA1_PRCVEN,0) as PRECO, SUM(ISNULL(SB2.B2_QATU,0))-ISNULL(MAX(CARTEIRA_TOTAL),0) as TOTAL_E0E1 "
cQuery += CRLF + "FROM "+RetSqlName("SB1")+" SB1 "
cQuery += CRLF + "LEFT JOIN "+RetSqlName("SB2")+" SB2  " 
cQuery += CRLF + "		ON SB2.B2_COD = SB1.B1_COD "
cQuery += CRLF + "		AND SB2.B2_FILIAL = '"+xFilial("SB2")+"' "
cQuery += CRLF + "		AND SB2.B2_LOCAL IN ("+cArmazem+") "
cQuery += CRLF + "		AND SB2.D_E_L_E_T_ = '' "

cQuery += CRLF + "LEFT JOIN "+RetSqlName("ZA8")+" ZA8 "
cQuery += CRLF + "		ON ZA8_CODIGO = B1_YSITUAC "
cQuery += CRLF + "		AND ZA8_FILIAL = '"+xFilial("ZA8")+"' "
cQuery += CRLF + "		AND ZA8.D_E_L_E_T_ = '' "

cQuery += CRLF + "LEFT JOIN "+RetSqlName("DA1")+" DA1 "
cQuery += CRLF + "		ON DA1.DA1_CODPRO = B2_COD "
cQuery += CRLF + "		AND DA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "		AND DA1.DA1_CODTAB IN ('"+cTabB2B+"') "

cQuery += CRLF + "LEFT JOIN CART_ATUAL CART "
cQuery += CRLF + "		ON CART.B1_COD = SB2.B2_COD "

cQuery += CRLF + "WHERE SB1.D_E_L_E_T_ = '' "
If !Empty(cProduto) //.And. !Empty(cWhere)
	If Len(cProduto) <= 8 
		cQuery += CRLF + "		AND SUBSTRING(SB1.B1_COD,1,8) = '"+AllTrim(cProduto)+"' "
	Else
		cQuery += CRLF + "		AND SB1.B1_COD = '"+AllTrim(cProduto)+"' "
	EndIf
Else
	If !Empty(cFlg)
		If cFlg = "9" //Passar 9 para pegar os peordutos em com Flag em Branco
			cQuery += CRLF + "		AND SB1.B1_XFLAGRK = '' "
		Else
			cQuery += CRLF + "		AND SB1.B1_XFLAGRK = '"+cFlg+"' "
		EndIf
	EndIf	
EndIf

cQuery += CRLF + "		AND SB1.B1_YCOLECA IN	( SELECT ZAA.ZAA_CODIGO  " 
cQuery += CRLF + "								  FROM "+RetSqlName("ZAA")+" ZAA "
cQuery += CRLF + "								  WHERE ZAA.ZAA_FILIAL = '"+xFilial("ZAA")+"' "
cQuery += CRLF + "								  AND ZAA.D_E_L_E_T_ = '' "
cQuery += CRLF + "								  AND ZAA.ZAA_DWEB = 'S' "
cQuery += CRLF + "								) "
cQuery += CRLF + "		AND SB1.B1_FILIAL = '"+xFilial("SB1")+"' "


cQuery += CRLF + "GROUP BY SB1.B1_COD, SB1.B1_YFORMAT,B1_YDATLIB,B1_YBLQDEM,B1_YCICLO,B1_YSITUAC,SB1.B1_YDWEB2B,ZA8_DWEB2B,DA1_PRCVEN "

cQuery += CRLF + "ORDER BY SB1.B1_COD "

MemoWrite("HFATA008.txt",cQuery)
	    
cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)
						
//EndSql

Return cQuery

/*Static Function SLDFILHOS(cPai)
Local lRet     := .T.
Local cQuery   := ""
Local cArmazem := AllTrim(SuperGetMV("MV_XARMB2C",.F.,"'E0','E1'"))

cQuery := "SELECT B2_COD,SUM(SB2.B2_QATU)-SUM(SB2.B2_QEMP)-SUM(SB2.B2_RESERVA)-SUM(SB2.B2_XRESERV) as TOTAL_E0E1 "
cQuery += CRLF + "FROM "+RetSqlName("SB2")+" SB2 "
cQuery += CRLF + "WHERE 	SB2.B2_FILIAL = '"+xFilial("SB2")+"' "
cQuery += CRLF + "		AND SB2.B2_COD like '"+AllTrim(cPai)+"%' "
cQuery += CRLF + "		AND SB2.D_E_L_E_T_ = '' "
cQuery += CRLF + "		AND SB2.B2_LOCAL IN ("+cArmazem+") "
cQuery += CRLF + "GROUP BY SB2.B2_COD

MemoWrite("HFATA004_SLD.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"SLD", .F., .T.)

DbSelectArea("SLD")

If SLD->(!EOF())
	While SLD->(!EOF())
		If SLD->TOTAL_E0E1 > nMinEst 
			lRet := .F.
			Exit
		EndIf
		SLD->(DbSkip())
	EndDo	
EndIf

SLD->(DbCloseArea())

Return lRet*/