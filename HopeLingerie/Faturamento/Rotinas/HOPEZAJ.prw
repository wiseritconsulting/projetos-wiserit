#INCLUDE "rwmake.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HOPEZAJ   � Autor � Roberto Le�o       � Data �  08/12/16   ���
�������������������������������������������������������������������������͹��
���Descricao � Cadastro de Ciclo de Produto.                              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE LINGERIE                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function HOPEZAJ()
Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.

//���������������������������������������������������������������������Ŀ
//� Declaracao de Variaveis                                             �
//�����������������������������������������������������������������������

Private cPerg   := "ZAJ"
Private cString := "ZAJ"

dbSelectArea("ZAJ")
dbSetOrder(1)

cPerg   := "ZAJ"

Pergunte(cPerg,.F.)
SetKey(123,{|| Pergunte(cPerg,.T.)}) // Seta a tecla F12 para acionamento dos parametros

AxCadastro(cString,"Ciclo de Produto",cVldExc,cVldAlt)

Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros


Return
