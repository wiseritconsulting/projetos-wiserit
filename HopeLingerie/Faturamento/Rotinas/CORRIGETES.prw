#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

User Function CORRIGETES()
Local cQuery := ""
Local nCont := 0

cQuery := "select C6_NUM,C6_ITEM,C6_PRODUTO,C6_CLI,C6_LOJA "
cQuery += CRLF + "from "+RetSqlName("SC6")+" SC6 "
cQuery += CRLF + "where C6_NUM like 'I%' "
cQuery += CRLF + "and SC6.D_E_L_E_T_ = '' "
cQuery += CRLF + "and C6_FILIAL = '"+xFilial("SC6")+"' "
//cQuery += CRLF + "and C6_QTDENT < C6_QTDVEN "
cQuery += CRLF + "and C6_TES = '999' "
cQuery += CRLF + "order by C6_NUM,C6_ITEM "

MemoWrite("CORRIGETES.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)

DbSelectArea("TRB")
DbGoTop()

While TRB->(!EOF())
	nCont++
	TRB->(DbSkip())
EndDo	

Processa( {|| PROCTES(nCont) }, "Aguarde...", "Processando...",.F.)
	
Return

Static Function PROCTES(nCont)
Local cTES  := ""
Local nOk   := 0
Local nErro := 0
Local cLog  := "Pedidos com TES n�o encontrada: PEDIDO - ITEM - PRODUTO - CLIENTE - LOJA"

DbSelectArea("TRB")
DbGoTop()

ProcRegua(nCont)

While TRB->(!EOF())
	IncProc()
	DbSelectArea("SC6")
	DbSetOrder(1)
	If DbSeek(xFilial("SC6")+TRB->C6_NUM+TRB->C6_ITEM+TRB->C6_PRODUTO)
		cTES  := MaTesInt(2,"01",SC6->C6_CLI,SC6->C6_LOJA,"C",SC6->C6_PRODUTO,)
    	If !Empty(cTES)
    		cUF := GetAdvFVal("SA1","A1_EST",xFilial("SA1")+SC6->C6_CLI+SC6->C6_LOJA,1,"CE")
    		cCFOP := "9999"
    		If cUF = "CE"
    			cDig := "5"
    		ElseIf cUF = "EX"
    			cDig := "7"
    		Else
    			cDig := "6"
    		EndIf	
    		DbSelectArea("SF4")
    		DbSetOrder(1)
    		If DbSeek(xFilial("SF4")+cTES)
    			cCFOP := cDig + SubStr(SF4->F4_CF,2,3)
    		EndIf
    		RecLock("SC6",.F.)
    		SC6->C6_TES := cTES
    		SC6->C6_CF  := cCFOP
    		MsUnlock()
    		nOk++
    	Else
    		cLog += CRLF + SC6->C6_NUM + " - " + SC6->C6_ITEM + " - " + SC6->C6_PRODUTO + " - " + SC6->C6_CLI + " - " + SC6->C6_LOJA 
    		nErro++
    	EndIf
	EndIf
	If nErro > 1000
		MemoWrite("CORRIGETES"+StrTran(Time(),":","")+".log",cLog)
		nErro := 0
		cLog  := "Pedidos com TES n�o encontrada: PEDIDO - ITEM - PRODUTO - CLIENTE - LOJA"
	EndIf	
	TRB->(DbSkip())
EndDo

MemoWrite("CORRIGETES"+StrTran(Time(),":","")+".log",cLog)

Return