--FopParamDataRmPortal
 SELECT CODCOLIGADA, IDWORKFLOW, TIPOGATILHO, NOME, OBJETO
                           FROM GGATILHOWORKFLOW  WITH(NOLOCK/*AZURE*/)
                           WHERE TIPOGATILHO <> 0 
                            AND UPPER(OBJETO) = 'FOPPARAMDATARMPORTAL'   AND NOME IN ('ReadViewPre','ReadViewPost','ReadRecordPre','ReadRecordPost','BeforeUpdatePre','BeforeUpdatePost','SaveRecordPre','SaveRecordPost','AfterUpdatePre','AfterUpdatePost','ReadLookupViewPre','ReadLookupViewPost','ReadLookupValuesPre','ReadLookupValuesPost','GetDataSetPre','GetDataSetPost') 
                           ORDER BY PRIORIDADE DESC
go
--FopRelatorioGeradorLookupData
 SELECT CODCOLIGADA, IDWORKFLOW, TIPOGATILHO, NOME, OBJETO
                           FROM GGATILHOWORKFLOW  WITH(NOLOCK/*AZURE*/)
                           WHERE TIPOGATILHO <> 0 
                            AND UPPER(OBJETO) = 'FOPRELATORIOGERADORLOOKUPDATA'   AND NOME IN ('ReadViewPre','ReadViewPost','ReadRecordPre','ReadRecordPost','BeforeUpdatePre','BeforeUpdatePost','SaveRecordPre','SaveRecordPost','AfterUpdatePre','AfterUpdatePost','ReadLookupViewPre','ReadLookupViewPost','ReadLookupValuesPre','ReadLookupValuesPost','GetDataSetPre','GetDataSetPost') 
                           ORDER BY PRIORIDADE DESC
go
--FopParamQuiosqueData
SELECT CODCLIENTE, 
		                        CODINTERNO, 
		                        DESCRICAO 
                      FROM PTPANOTACAO  WITH(NOLOCK/*AZURE*/)
go
