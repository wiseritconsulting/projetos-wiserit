

SELECT ZZ3_FILIAL,
       ZZ3_NUMERO,
       ZZ3_MEDICA,
       ZZ3_CLIENT,
       ZZ3_LOJA,
       ZZ2_PV03FL,
       ZZ2_CLNF03,
       ZZ2_PV03NF,
       ZZ2_SER03,
	   ZZ2_DTGER,
       SUM(PRECOPEDIDO)ValNF,
       sum(CALCULO) VALOK,
       SUM(PRECOPEDIDO - CALCULO) DIFERENCA
FROM
  (SELECT *
   FROM
     (SELECT ZZ2_MEDICA,
             ZZ2_DTGER,
             ZZ2_HRGER,
             ZZ3_PRODUT KIT,
             C6.C6_PRODUTO PRODUTO,
             ZZ3_VLUNIT,

        (SELECT COUNT(*)
         FROM ZZ7010 Z71
         WHERE Z71.D_E_L_E_T_ =' '
           AND Z71.ZZ7_CODPAI = Z7.ZZ7_CODPAI
           AND Z71.ZZ7_FILIAL = Z7.ZZ7_FILIAL
           AND C6_YCODKIT = Z71.ZZ7_CODPAI
           AND Z71.ZZ7_CATEGO IN ('A',
                                  'C',
                                  'D',
                                  'V')
           AND Z71.ZZ7_SUPLEM = 'N'
           AND Z71.ZZ7_TIPO = 'A'
           AND Z71.ZZ7_PERDSC = '0') QTD,
             C6_PRCVEN PRECOPEDIDO,
             ROUND((ZZ3_VLUNIT -
                      (SELECT COUNT(*)
                       FROM ZZ7010 Z72
                       WHERE Z72.D_E_L_E_T_ =' '
                         AND Z72.ZZ7_CODPAI = Z7.ZZ7_CODPAI
                         AND Z72.ZZ7_FILIAL = Z7.ZZ7_FILIAL
                         AND C6_YCODKIT = Z72.ZZ7_CODPAI
                         AND Z72.ZZ7_CATEGO IN ('A','C','D','V')
                         AND Z72.ZZ7_SUPLEM <> 'N'
                         AND Z72.ZZ7_TIPO = 'A'
                         AND Z72.ZZ7_PERDSC <> '0')*10) /
                     (SELECT COUNT(*)
                      FROM ZZ7010 Z71
                      WHERE Z71.D_E_L_E_T_ =' '
                        AND Z71.ZZ7_CODPAI = Z7.ZZ7_CODPAI
                        AND Z71.ZZ7_FILIAL = Z7.ZZ7_FILIAL
                        AND C6_YCODKIT = Z71.ZZ7_CODPAI
                        AND Z71.ZZ7_CATEGO IN ('A','C','D','V')
                        AND Z71.ZZ7_SUPLEM = 'N'
                        AND Z71.ZZ7_TIPO = 'A'
                        AND Z71.ZZ7_PERDSC = '0'),2) CALCULO,
             Z7.ZZ7_CODPAI,
             C6.C6_YCODKIT,
             C6.C6_YPAIKIT,
             Z7.ZZ7_TIPO,
             Z7.ZZ7_PERDSC,
             Z7.ZZ7_SUPLEM,
             Z7.ZZ7_CATEGO,
             C6.C6_DATFAT,
             C6.C6_FILIAL,
             C6.C6_NUM,
             C6.C6_ITEM,
             Z3.ZZ3_FILIAL,
             Z3.ZZ3_NUMERO,
             C6.C6_YCONTRA,
             Z3.ZZ3_MEDICA,
             Z3.ZZ3_CLIENT,
             Z3.ZZ3_LOJA,
             C6.C6_YMEDICA,
             Z3.ZZ3_ITEM,
             C6.C6_YITCONT,
             C6.C6_NOTA,
             Z3.ZZ3_PRODUT,
             ZZ2_PV03FL,
             ZZ2_CLNF03,
             ZZ2_PV03NF,
             ZZ2_SER03
      FROM SC6010 C6 (NOLOCK)
      JOIN ZZ2010 Z2 (NOLOCK) ON Z2.ZZ2_PV03FL = C6.C6_FILIAL
      AND Z2.ZZ2_PV03 = C6.C6_NUM
      AND Z2.ZZ2_CLNF03 = C6.C6_CLI
      AND Z2.ZZ2_LJNF03 = C6.C6_LOJA
      AND Z2.ZZ2_PV03NF = C6.C6_NOTA
      AND Z2.ZZ2_SER03 = C6.C6_SERIE
      AND Z2.D_E_L_E_T_ <> '*'
      JOIN ZZ3010 Z3 (NOLOCK) ON Z3.ZZ3_FILIAL = Z2.ZZ2_FILIAL
      AND Z3.ZZ3_NUMERO = Z2.ZZ2_NUMERO
      AND Z3.ZZ3_MEDICA = Z2.ZZ2_MEDICA
      AND Z3.D_E_L_E_T_ <> '*'
      JOIN ZZ7010 Z7 (NOLOCK) ON Z7.ZZ7_PRODUT = C6.C6_PRODUTO
      AND Z7.ZZ7_CODPAI = Z3.ZZ3_PRODUT
      AND Z7.D_E_L_E_T_ <> '*'
      JOIN ZZ7010 Z72 (NOLOCK) ON Z72.ZZ7_CODIGO = Z7.ZZ7_CODPAI
      AND Z72.D_E_L_E_T_ <> '*'
      WHERE C6.D_E_L_E_T_ <> '*'
        AND C6.C6_QTDVEN = ZZ3_QUANT
        AND C6.C6_NOTA <> ''
        AND ZZ3_ITEM = C6_YITCONT
        AND ISNULL(C6.C6_YCONTRA, '') <> ''
        AND Z7.ZZ7_CODPAI <> ' '
        AND Z7.ZZ7_CATEGO IN ('A',
                              'C',
                              'D',
                              'V')
        AND Z7.ZZ7_SUPLEM = 'N'
        AND Z7.ZZ7_TIPO = 'A'
        AND Z7.ZZ7_PERDSC = '0') AS A
   WHERE CALCULO <> PRECOPEDIDO
     AND (PRECOPEDIDO - CALCULO > 1
          OR CALCULO - PRECOPEDIDO > 1)
     --AND ZZ3_FILIAL = '020101'
     --AND ZZ3_NUMERO = '000084'
     --AND ZZ3_MEDICA = '000001'
     --AND ZZ3_CLIENT = '305344'
     --AND ZZ3_LOJA = '01'
	 ) F
GROUP BY ZZ3_FILIAL,
         ZZ3_NUMERO,
         ZZ3_MEDICA,
         ZZ3_CLIENT,
         ZZ3_LOJA,
         ZZ2_PV03FL,
         ZZ2_CLNF03,
         ZZ2_PV03NF,
         ZZ2_SER03,
		 ZZ2_DTGER

		 ORDER BY ZZ2_DTGER 