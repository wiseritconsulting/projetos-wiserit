  
alter PROCEDURE GERA_TXT_APROP_HORAS    
AS    

--dlara as vari�veis necess�rias 
    
DECLARE @DATAINI datetime    
DECLARE @DATAFIM datetime      
DECLARE @QTD_AULAS INT    
DECLARE @TOT_HORAS NUMERIC(10,2)    
DECLARE @NUM_FUNC VARCHAR(10)    
DECLARE @NUM_FUNC_FORTES VARCHAR(10)    
DECLARE @FACUDADE VARCHAR(50)    
DECLARE @FACUDADE_ANT VARCHAR(50)    
DECLARE @TOT_HORAS_MESANT NUMERIC(10,2)    
DECLARE @TOT_HORAS_MESANT_ANT NUMERIC(10,2)    
DECLARE @COUNT INT    
DECLARE @VINCULO VARCHAR(100)    
DECLARE @EVENTO VARCHAR(50)    
DECLARE @VALOR MONEY    
DECLARE @FACULDADE_FORTES VARCHAR(10)    
DECLARE @FACULDADE_FORTES_ANT VARCHAR(10)    
  
--alimenta as variaveis caso necess�rio  
set @DATAINI =  CAST(   '16'     
      +    
      '/' +    
      CASE WHEN CAST(DATEPART(MM,GETDATE())-1 AS VARCHAR(2)) = 0    
        THEN '12'    
        ELSE RIGHT(REPLICATE('0',2) + CAST(DATEPART(MM,GETDATE()) -1 AS VARCHAR(2)),2)    
      END +    
      '/'+    
      CAST(YEAR(GETDATE()) AS VARCHAR(4)) AS char(10))-- PRIMEIRO DIA APOS FECHAMENTO DA FOLHA    
          
        
set @DATAFIM = CAST(    
        '15'+    
        '/' +    
        RIGHT(REPLICATE('0',2) + CAST(DATEPART(MM,GETDATE()) AS VARCHAR(2)),2)+            
        '/'+    
        CAST(YEAR(GETDATE()) AS VARCHAR(4)) AS char(10))         
      
    
--variaveis para usar na migracao de dados para txt.    
DECLARE @arqTexto VARCHAR(8000),    
  @caminho  VARCHAR(255),    
  @retEcho  INT       ,    --variavel para verificar se o comando foi executado com exito ou ocorreu alguma falha    
        @ttt VARCHAR(8000),    
        @head VARCHAR(8000),    
        @head_ant VARCHAR(8000)    
set @head_ant = 1     
    
--declara cursor    
DECLARE C_TXT CURSOR FOR    


--consulta para passar dados para o cursor    
SELECT DISTINCT    
 --QTD_AULAS,    
 A.TOT_HORAS,    
 CAST(A.NUM_FUNC AS VARCHAR(15)),    
 CAST(A.NUM_FUNC_FORT AS VARCHAR(15)),    
 --FACUDADE,    
 A.VINCULO,    
 D.CODIGO_EVENTO_FORTES,    
 A.VALOR,    
 A.FACULDADE_FORTES    
FROM APROP_HORAS A INNER JOIN DOCENTE_FORTES_EVENTOS D ON A.EVENTO = D.EVENTO   
  WHERE A.VINCULO = 'CLT'    
  AND A.DATAINI = @DATAINI   
  AND A.DATAFIM = @DATAFIM  
ORDER BY A.FACULDADE_FORTES    
 
--abre cursor 
OPEN C_TXT    
    
FETCH NEXT FROM C_TXT INTO @TOT_HORAS,@NUM_FUNC,@NUM_FUNC_FORTES,@VINCULO,@EVENTO,@VALOR,@FACULDADE_FORTES    
    
WHILE @@FETCH_STATUS = 0    
BEGIN    
    
 SET @FACULDADE_FORTES_ANT = @FACULDADE_FORTES     
    
-- A PARTIR DAQUI GERA DADOS PARA ARQUIVO TXT    
-- NESTE PRIMEIRO MOMENTO IREMOS LAN�AR O ARQUIVO "APROPRIA_HORAS.TXT" NO C:\ SENDO DEFINIDO O LOCAL PARA ALOCA��O DESTE ARQUIVO    
    
--HABILITA SERVIDOR TEMPORARIAMENTE PARA GERA��O DE TXT    
EXEC sp_configure 'allow updates', 0    
RECONFIGURE    
    
EXEC sp_configure 'show advanced options', 1    
RECONFIGURE    
    
EXEC sp_configure 'xp_cmdshell', 1    
RECONFIGURE    
          
    
WHILE @FACULDADE_FORTES = @FACULDADE_FORTES_ANT AND @@FETCH_STATUS = 0    
BEGIN    
    
	
	--seta o caminho o ser� gerado o arquivo txt
   SET @caminho = 'C:\APROPRIA_HORAS'+'_'+CAST(@FACULDADE_FORTES AS VARCHAR(10))+'.'+'txt'    
   
   --alimenta a linha de cabe�alo(header).
   SET @head = (    
    SELECT     
     CAST(0 AS VARCHAR(1))+    
     CAST(2 AS VARCHAR(2))+    
     'ACXXXXXXXX'+    
     'LYCEUMXXXX'+    
     RIGHT(REPLICATE('0',4)+CAST(@FACULDADE_FORTES AS VARCHAR(4)),4)+    
     REPLICATE('X',40)      
    )    
  --alimenta linha a ser impressa no txt com os devisdos dados      
   SET @ttt = (    
      SELECT     
     CAST(1 AS VARCHAR(1))+    
     RIGHT('000000'+CAST(@NUM_FUNC_FORTES AS VARCHAR(6)),6)+    
     RIGHT('000'+CONVERT(VARCHAR(3),@EVENTO),3)+    
     RIGHT(REPLICATE('0',10)+CAST(@TOT_HORAS AS VARCHAR(10)),10)+     
     RIGHT(REPLICATE('0',15)+CAST((@TOT_HORAS * @VALOR) AS VARCHAR(15)),15)    
      )    
 
--trata para nao repetir a linha de cabe�alho 
IF  @head <> @head_ant    
BEGIN    
--cria o arquivo txt
   SET @arqTexto = 'echo ' + @head +  ' >> ' + @caminho    
   EXEC @retEcho = MASTER..XP_CMDSHELL @arqTexto, NO_OUTPUT    
 set @head_ant = @head    
 END    
     
   SET @arqTexto = 'echo ' + @ttt +  ' >> ' + @caminho    
   --remove caracteres n�o aceitos pelo comando ECHO, veja no final do post coment�rio    
   --select @arqTexto = dbo.FU_CONVERTE_TXT(@arqTexto)    
   --escreve no arquivo txt, se ele naum existir ser� criado, se existir ser� adicionado (incrementado) a partir da �ltima linha    
   EXEC @retEcho = MASTER..XP_CMDSHELL @arqTexto, NO_OUTPUT --NO_OUTPUT indica que o comando n�o ir� retornar alguma poss�vel mensagem na sua execu��o    
   IF (@retEcho <> 0)    
   BEGIN    
    --caso ocorrer algum erro podemos retornar uma mensagem    
    SELECT 'ocorreu erro, verifique se o diret�rio existe' as Falha, @retEcho as retEcho    
    
 END    
   SET @FACULDADE_FORTES_ANT = @FACULDADE_FORTES     
    
SET @COUNT = @COUNT +1    
    
FETCH NEXT FROM C_TXT INTO @TOT_HORAS,@NUM_FUNC,@NUM_FUNC_FORTES,@VINCULO,@EVENTO,@VALOR,@FACULDADE_FORTES    
    
END    
    
--DESABILITA SERVIDOR PARA GERA��O DE TXT    
EXEC sp_configure 'show advanced options', 1    
RECONFIGURE    
    
EXEC sp_configure 'xp_cmdshell', 0    
RECONFIGURE    
    
END    
--4. FECHA E DESALOCA O CURSOR    
CLOSE C_TXT    
DEALLOCATE C_TXT    
  