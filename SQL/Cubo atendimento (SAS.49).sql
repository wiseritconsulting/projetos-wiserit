
SELECT MONTH(X.DATAEMISSAO) AS 'MES',X.USUARIOCRIACAO,CAST(COUNT(CAST(X.NUMEROMOV AS DECIMAL(10,5)))AS DECIMAL(10,2)) AS 'QTDE. DE OS'
      ,COUNT(Y.NUMEROMOV) AS 'QTDE. DE OS CANCELADAS'
      ,CAST((COUNT(Y.NUMEROMOV)/CAST(COUNT(CAST(X.NUMEROMOV AS DECIMAL(10,2)))AS DECIMAL(10,2)))*100  AS DECIMAL(10,2))AS '%'
      ,Y.HISTORICOCURTO AS MOTIVO
   
FROM 
	(
		SELECT 
			T.NUMEROMOV,TH.HISTORICOCURTO,T.USUARIOCRIACAO,T.CODCOLIGADA,T.IDMOV,T.DATAEMISSAO
		FROM TMOV T 
			JOIN	TMOVHISTORICO TH ON(T.CODCOLIGADA = TH.CODCOLIGADA AND T.IDMOV = TH.IDMOV)  
		WHERE 	T.CODCOLIGADA = 3 		
		AND		T.CODTMV = '2.1.04'
		AND		T.DATAEMISSAO LIKE '%2014%'
	 )X
	 
	 LEFT JOIN
	 	(
		SELECT 
			T.NUMEROMOV,TH.HISTORICOCURTO,T.USUARIOCRIACAO,T.CODCOLIGADA,T.IDMOV,T.DATAEMISSAO
		FROM TMOV T 
			JOIN	TMOVHISTORICO TH ON(T.CODCOLIGADA = TH.CODCOLIGADA AND T.IDMOV = TH.IDMOV)  
		WHERE T.STATUS = 'C'
		AND		T.CODCOLIGADA = 3 
		AND		TH.HISTORICOCURTO LIKE '%Altera��o de materia%'
		OR		TH.HISTORICOCURTO LIKE '%Altera��o de pedido%'
	    OR		TH.HISTORICOCURTO LIKE '%Altera��o de quantidade - Escola%'
	    OR		TH.HISTORICOCURTO LIKE '%Desist�ncia - Escola%'
	    OR		TH.HISTORICOCURTO LIKE '%Diverg�ncia de informa��o pela conveniada%'
	    OR		TH.HISTORICOCURTO LIKE '%Falta de material no CD%'
	    OR		TH.HISTORICOCURTO LIKE '%Material j� enviado%'
	    OR		TH.HISTORICOCURTO LIKE '%Planilha incorreta - Ari de S�%'
	    OR		TH.HISTORICOCURTO LIKE '%Solicita��o incorreta de outro setor%'
	    OR		TH.HISTORICOCURTO LIKE '%Quantidade incorreta%'
	    OR		TH.HISTORICOCURTO LIKE '%Tipo de movimento incorreto%'
	    OR		TH.HISTORICOCURTO LIKE '%C�digo incorreto%'
	    OR		TH.HISTORICOCURTO LIKE '%Aplica��o fluxo incorreto%'
	    OR		TH.HISTORICOCURTO LIKE '%Falta de observa��o%'
	    OR		TH.HISTORICOCURTO LIKE '%Cliente incorreto%'
	    OR		TH.HISTORICOCURTO LIKE '%Ano de compet�ncia incorreto%'    
		AND		T.CODTMV = '2.1.04'
		AND		T.DATAEMISSAO LIKE '%2014%'
	 )Y
	 ON(X.CODCOLIGADA = Y.CODCOLIGADA AND X.IDMOV = Y.IDMOV)
	 GROUP BY X.USUARIOCRIACAO,MONTH(X.DATAEMISSAO),Y.HISTORICOCURTO
	 ORDER BY MONTH(X.DATAEMISSAO)
	 
	 

--SELECT ISNULL(HISTORICOCURTO,'-'),* FROM TMOVHISTORICO 
--WHERE CODCOLIGADA = 3 