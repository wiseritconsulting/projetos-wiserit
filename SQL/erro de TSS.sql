Treinamento SQL

RESPONDERRESPONDER A TODOSENCAMINHAR
marcar como lida

Heliomar Junior
sex 04/04/2014 17:18
Para:
Weskley Benedito da Silva;

Foi identificado que a mensagem ocorre quando o evento esta sendo rejeitado pela SEFAZ. 
Como solu��o paliativa, deve ser verificado no banco de dados do TSS (sped150), 
qual o status, caso seje status 5, dever� ser alterado o status da NF-e na tabela TNFEESTADUAL para "A" e na tabela TNFEESTADUALEVENT para "R". 
Feito isso, verifique o motivo da rejei��o no campo CMOTEVENT, realize as devidas configura��es e reenvie o evento.
Gentileza atualizar o TSS para vers�o 2.28 que est� disponivel no Portal para download.
--- 1o Passo --
use Corpore
go
-- Qdo, h� erro de TSS, n�o autorizando:
--A consulta n�o pode ser realizada devido alguns erros. Total de NF-e: 5.

--NF-e n�o foi autorizada. Erro: 999 - 104/Lote processado. Retorno inexperado, entre em contato com a Totvs.
Filial: 5, Identificador do Movimento: 163424, Chave de Acesso da NFe: 23140406267630000537550010000343671968365765
SELECT * FROM TNFEESTADUAL WHERE IDMOV in(163424,163432,163433,163435,163436)

---- K

UPDATE TNFEESTADUAL SET STATUS='R' WHERE IDMOV in(163424,163432,163433,163435,163436)


--- 2o Passo --_
SELECT * FROM  TNFEESTADUALEVENT  WHERE IDMOV in(163424,163432,163433,163435,163436)





UPDATE  TNFEESTADUALEVENT SET STATUS='R' WHERE IDMOV=160902

Edson verifique no banco se a nota esta gravada na tabela SPED050, pegue no xml na tag (<cNF>numero</cNF>) e consulte nesta tabela o numero que apace nesta tag � o nfe_id. 

-- Consulta --

---excluir
use TOTVSSped
go

--P/ cancelar o envio do cancelamento e refazer, excluir a linha e vai no movimento e cancela a nf-e,2- TNFEESTADUAL SET STATUS='A', 3- TNFEESTADUALEVENT SET STATUS='R'
SELECT *  FROM SPED050 WHERE NFE_ID='96836576'
SELECT *  FROM SPED054 WHERE NFE_ID='96836576'


SELECT *  FROM SPED150 WHERE NFE_CHV='23140406267630000537550010000343671968365765'

---Ver motivo ---
SELECT * FROM SPED154 WHERE NFE_CHV='23140406267630000537550010000343671968365765'