#Include 'Protheus.ch'

/*/{Protheus.doc} MT106SC1
Gera��o SC1 por SA
@author Diogo
@since 26/06/2015
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/User Function MT106SC1()

dbSelectArea("SC1")
SC1->(dbGoto(PARAMIXB[4]))
	SC1->(RecLock("SC1",.F.))
		SC1->C1_OBS	:= Strtran(SC1->C1_OBS,'SC gerada por SA','SC gerada por SA '+SCP->CP_NUM)
		SC1->C1_CC	:= SCP->CP_CC
		SC1->C1_YCO := SCP->CP_YCO
	SC1->(MsUnlock())
	//public cCCusto := GetMv("MV_YCCEST")
	//U_MT110GRV() //Obrigar chamada do PE respons�vel pela al�ada na Solicita��o de Compras
Return