#INCLUDE "Rwmake.ch"
#INCLUDE "Protheus.ch"
#INCLUDE "Topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � MT120FIM    �Autor  � Rafael Xavier   � Data �  28/12/15   ���
�������������������������������������������������������������������������͹��
���Desc.     � Ponto de Entrada que trata que unifica PC                  ���
�������������������������������������������������������������������������͹��
���Uso       �                                                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������

*/

User Function MT160WF()

	Local cPedido	
	Local lInclui
	Local nCount	:= 0

	Private cAlias	:= getNextAlias()
	Private cAliasInc := getNextAlias()
	Private aDados	:= {}

	DbSelectArea("SC7")
	dbSetOrder(1)
	IF dbSeek(xFilial("SC7")+SC8->C8_NUMPED+SC8->C8_ITEMPED)

		cPedido:= SC7->C7_NUM

		BeginSql Alias cAlias
		SELECT 
		COUNT(CTT.CTT_YGPRES) as Qtd
		FROM %table:SC7% SC7
		LEFT JOIN %table:CTT% CTT 
		ON CTT.CTT_FILIAL			= %xFilial:CTT%
		AND CTT.CTT_CUSTO		= SC7.C7_CC
		AND CTT.%notDel%
		WHERE SC7.C7_FILIAL			= %xFilial:SC7%
		and SC7.C7_NUM 			= %Exp:cPedido%
		and SC7.%notDel% 
		GROUP BY
		CTT.CTT_YGPRES
		
		EndSql

		While !(cAlias)->(Eof())
			nCount := nCount+1
			(cAlias)->(dbSkip())
		EndDo

		(cAlias)->(dbCloseArea())
		BeginTran()
		If nCount >= 1
			CriaDados(cPedido)	//Cria informa��es para dividir o pedido
			If ExclSC7(cPedido)		//Exclui o pedido original
				CriaSC7(cPedido)		//Cria novos pedidos com base no original
			EndIf
		EndIf
		EndTran()
	EndIf

Return

Static Function CriaDados(cPedido)
	Local aCab		:= {}
	Local aItem		:= {}
	Local aItens	:= {}
	Local cGrupo	:= ""
	
	BeginSql Alias cAliasInc

	SELECT 
	SC7.*,
	CTT.CTT_YGPRES
	FROM %table:SC7% SC7
	LEFT JOIN %table:CTT% CTT 
	ON
	CTT.CTT_FILIAL = %xFilial:CTT%  
	AND CTT.CTT_CUSTO = SC7.C7_CC
	AND CTT.%notDel%
	WHERE 
	C7_FILIAL = %xFilial:SC7% 
	AND C7_NUM = %Exp:cPedido%
	AND SC7.%notDel%
	ORDER BY
	SC7.C7_FILIAL,
	SC7.C7_NUM,
	CTT.CTT_YGPRES,
	SC7.C7_ITEM
	
	EndSql

	While !(cAliasInc)->(Eof())


		aCab := {	{"C7_NUM"	,CriaVar('C7_NUM', .T.)		,Nil},;
		{"C7_FORNECE"	,(cAliasInc)->C7_FORNECE		,Nil},;
		{"C7_LOJA"		,(cAliasInc)->C7_LOJA			,Nil},;
		{"C7_EMISSAO"	,STOD((cAliasInc)->C7_EMISSAO)	,Nil},;
		{"C7_COND"		,(cAliasInc)->C7_COND			,Nil},;
		{"C7_CONTATO"	,(cAliasInc)->C7_CONTATO		,Nil},;
		{"C7_FILENT"	,(cAliasInc)->C7_FILENT			,Nil},;
		{"C7_MOEDA"	,(cAliasInc)->C7_MOEDA			,Nil},;
		{"C7_TXMOEDA"	,(cAliasInc)->C7_TXMOEDA		,Nil}}
		
		cGrupo	:= (cAliasInc)->CTT_YGPRES 	 

		aUPDSC8:={}

		While !(cAliasInc)->(Eof()) .AND. cGrupo == (cAliasInc)->CTT_YGPRES

			aItem	:= {}

			aAdd(aItem,{"C7_PRODUTO"	,(cAliasInc)->C7_PRODUTO		,Nil})
			aAdd(aItem,{"C7_UM"		    ,(cAliasInc)->C7_UM				    ,Nil})
			aAdd(aItem,{"C7_CODTAB"	    ,(cAliasInc)->C7_CODTAB			    ,Nil})
			aAdd(aItem,{"C7_QUANT"		,(cAliasInc)->C7_QUANT			,Nil})
			aAdd(aItem,{"C7_PRECO"		,(cAliasInc)->C7_PRECO			,Nil})
			aAdd(aItem,{"C7_TOTAL"		,(cAliasInc)->C7_TOTAL			,Nil})
			aAdd(aItem,{"C7_DATPRF"	     ,STOD((cAliasInc)->C7_DATPRF)	    ,Nil})
			aAdd(aItem,{"C7_LOCAL"		,(cAliasInc)->C7_LOCAL			,Nil})
			aAdd(aItem,{"C7_OBS"		,(cAliasInc)->C7_OBS			,Nil})
			aAdd(aItem,{"C7_CC"		    ,(cAliasInc)->C7_CC				    ,Nil})
			aAdd(aItem,{"C7_CONTA"		,(cAliasInc)->C7_CONTA			,Nil})
			aAdd(aItem,{"C7_ITEMCTA"	,(cAliasInc)->C7_ITEMCTA		,Nil})
			aAdd(aItem,{"C7_RESIDUO"	,(cAliasInc)->C7_RESIDUO		,Nil})
			aAdd(aItem,{"C7_VLDESC"	    ,(cAliasInc)->C7_VLDESC			    ,Nil})
			aAdd(aItem,{"C7_APROV"		,(cGrupo)						,Nil})
			aAdd(aItem,{"C7_TES"		,(cAliasInc)->C7_TES			,Nil})
			aAdd(aItem,{"C7_DESC"		,(cAliasInc)->C7_DESC			,Nil})
			aAdd(aItem,{"C7_FILCEN"	    ,(cAliasInc)->C7_FILCEN			    ,Nil})
			aAdd(aItem,{"C7_QUJE"		,(cAliasInc)->C7_QUJE			,Nil})
			aAdd(aItem,{"C7_REAJUST"	,EhEmpty((cAliasInc)->C7_REAJUST,CriaVar("C7_ACCPROC"))		,Nil})
			aAdd(aItem,{"C7_NUMPR"		,(cAliasInc)->C7_NUMPR			,Nil})
			aAdd(aItem,{"C7_NUMSC"		,(cAliasInc)->C7_NUMSC			,Nil})
			aAdd(aItem,{"C7_ITEMSC"	    ,(cAliasInc)->C7_ITEMSC			    ,Nil})
			aAdd(aItem,{"C7_NUMCOT"	    ,(cAliasInc)->C7_NUMCOT			    ,Nil})
			aAdd(aItem,{"C7_YCO"	    ,POSICIONE("SC1",1,xFilial('SC1') + SC7->C7_NUMSC + SC7->C7_ITEMSC, "SC1->C1_YCO"),Nil})
			aAdd(aItem,{"C7_YCLAORC"	,POSICIONE("SC1",1,xFilial('SC1') + SC7->C7_NUMSC + SC7->C7_ITEMSC, "SC1->C1_YCLAORC"),Nil})
			AAdd(aItens,aItem)

			(cAliasInc)->(dbSkip())

		EndDo

		AADD(aDados,{aCab,aItens})//,aUPDSC8

	EndDO
	/*TRATAMENTO PARA AMARRA O PEDIDO A COTA��O J� EXISTENTE
	For nCont1:=1 to Len(aDados)
	aUPDSC8:= aDados[nCont1][3]
	For nCont:=1 to Len(aUPDSC8)
	If !Empty(aUPDSC8[nCont][3])
	If TCSQLExec("UPDATE "+RETSQLNAME("SC7")+" SET C7_NUMCOT='"+SPACE(GetSx3Cache("C7_NUMCOT","X3_TAMANHO"))+"' WHERE C7_FILIAL='"+xFilial("SC7")+"' AND C7_NUM='"+aUPDSC8[nCont][1]+"' AND D_E_L_E_T_=' '")<0
	Alert("Erro na atualiza��o do pedido de compra")
	EndIf
	//If TCSQLExec("UPDATE "+RETSQLNAME("SC1")+" SET C1_PEDIDO='"+SPACE(GetSx3Cache("C1_PEDIDO","X3_TAMANHO"))+"', C1_ITEMPED='"+SPACE(GetSx3Cache("C1_ITEMPED","X3_TAMANHO"))+"' WHERE C1_FILIAL='"+xFilial("SC1")+"' AND C1_PEDIDO='"+aUPDSC8[nCont][1]+"' AND D_E_L_E_T_=' '")<0
	//	Alert("Erro na atualiza��o da solicita��o de compra")
	//EndIf
	EndIf
	Next
	Next
	*/

	(cAliasInc)->(dbCloseArea())
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � CriaSC7     �Autor  � FSW-CE          � Data �  13/07/12   ���
�������������������������������������������������������������������������͹��
���Desc.     � Cria Pedido de Compra                                      ���
�������������������������������������������������������������������������͹��
���Uso       �            : MT120GOK                                      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function CriaSC7(cPedido)

	Local aCab		:= {}
	Local aItem		:= {}
	Local cGrupo	:= ""
	//Local aUPDSC8	:= {}
	Local nCont

	Private lMsHelpAuto		:= .T.
	Private lAutoErrNoFile	:= .F.
	Private lMsErroAuto		:= .F.



	For nCont1:=1 to Len(aDados)
		aCab	:= aDados[nCont1][1]
		aItem	:= aDados[nCont1][2]
		
		MATA120(1,aCab,aItem,3)

		If lMsErroAuto
			Mostraerro()
		
		endif
				
	Next
	

Return



/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � CriaSC7     �Autor  � FSW-CE          � Data �  13/07/12   ���
�������������������������������������������������������������������������͹��
���Desc.     � Exclui Pedido de Compra                                    ���
�������������������������������������������������������������������������͹��
���Uso       �            : MT120GOK                                      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/



Static Function ExclSC7(cPedido)

	Local aCab	:= {}
	Local aItem	:= {}
	Local lret	:= .T.

	Private lMsHelpAuto		:= .T.
	Private lAutoErrNoFile	:= .F.
	Private lMsErroAuto		:= .F.

	aCab := { {"C7_FILIAL"	,xFilial("SC7")	,Nil},;
	{"C7_NUM"		,cPedido			,Nil}}

	MATA120(1,aCab,aItem,5)

	If lMsErroAuto
		lErro := .T.
		lret	:= .F.
		DisarmTransaction()
		MostraErro()
	EndIf	

Return lret

Static Function EhEmpty(cValor1,cValor2)


Return If(Empty(cValor1),cValor2,cValor1)


