#include 'protheus.ch'
#include 'parmtype.ch'
#include 'topconn.ch'


user function sasp128()

Local cNum := ""
Local cQuery := ""
Local TEMP   := ""


cQuery := " Select max(ZZO_COD) as SeqMax from " + RetSqlName("ZZO") + " " + " Where D_E_L_E_T_ ='' AND ZZO_COD <>99 " 

tcquery cQuery new alias TEMP

//dbselectArea(TEMP)

If TEMP->(EOF())  
	cNum := strzero (1,2)
	
Else

	cNum := SOMA1(TEMP->SeqMax)
	
Endif

If Select (TEMP) > 0

	dbcloseArea(TEMP)
	
EndIf


return cNum