#INCLUDE "rwmake.ch"
#INCLUDE "TOPCONN.CH"

User Function SASG003()

Local aArea := GetArea()
Local cCod := "" 
Local cQuery := "SELECT MAX(A2_COD) A2_COD FROM "+RetSqlName("SA2")+" WHERE A2_COD <> 'UNIAO' AND D_E_L_E_T_ = ''"

TcQuery cQuery New Alias T01

If T01->(!Eof())
	cCod := SOMA1(T01->A2_COD)
Else
	cCod := "000001"
EndIf
dbSelectArea("T01")
dbCloseArea("T01")
RestArea(aArea)

Return cCod