#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "rwmake.ch"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"

user function SASG004()

	Local aArea := GetArea()
	Local cQuery := "SELECT MAX(A1_COD) A1_COD FROM "+RetSqlName("SA1")+" WHERE D_E_L_E_T_ = ''"
	Local cCod := ""
	
	TcQuery cQuery New Alias T01
	
	cProx := SOMA1(SA1->A1_COD)
	
	If T01->(!Eof())
		cCod := SOMA1(T01->A1_COD)
	Else
		cCod := "000001"
	EndIf
	
return cCod