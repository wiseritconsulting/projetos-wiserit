#Include 'Protheus.ch'
#include "topconn.ch"
#INCLUDE "FILEIO.CH"

user function SASP058()

	If !LockByName("SASP058-1",.F.,.F.)
		ConOut("Processso ja inicializado - CONTROLE DE JOB!")
		Return
	EndIf
	
	//tcsqlexec("INSERT INTO YJOB (JOBLOG) VALUES ('JOB INICIADO')")
	
	  
	While !KillApp()

		U_SASP050()
		Monitor("Alterando Processos")
		Sleep(3000)
		U_SASP152()
		Monitor("Alterando Processos")
		Sleep(3000)
		U_SASP050()
		Sleep(3000)
		Monitor("Alterando Processos")
		Sleep(3000)
		U_SASP152()
		Sleep(3000)
		Monitor("Alterando Processos")
		U_SASP129()
		Sleep(3000)
		Monitor("Alterando Processos")
	    U_SASP056()
		Sleep(3000)
	Enddo
	
	UnLockByName("SASP058-4",.F.,.F.)

return