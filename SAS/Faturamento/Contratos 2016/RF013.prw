#Include "TopConn.Ch"
#Include "Protheus.Ch"
#Include "Totvs.Ch"
#Include "Rwmake.Ch"
#Include "TbiConn.Ch"
#Include "TbiCode.Ch"

/*/{Protheus.doc} RF013 
Tela Controle de Titularidade de OS
@author �lvaro Bessa
@since 28/10/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User Function RF013()

	Private aDados     := {} //Array com os campos do MsBrGetDBase.
	Private aDados2    := {} //Array com os campos para inclus�o tabela ZZQ
	Static oDlg

	PREPARE ENVIRONMENT EMPRESA '01' Filial '050101' //USER 'totvs' PASSWORD 'totvs' TABLES ""

	FRF013()

Return

Static Function FRF013()

	__cInterNet		  := nil
	Private oLista
	Private cMvLibFat := GetMv("SA_ETAPA")  //Par�metro para identifica��o da fase para faturamento
	Private cMvFetapa := GetMv("SA_FETAPA") //Par�metro para identifica��o da fase final do processo
	Private cCodEtp   := ""
	Private _nOpc     := 0
	Private cOrdem    := Space(20)
	Private cContrato := Space(GetSx3Cache("ZZR_CONTRA","X3_TAMANHO"))
	Private cMedicao  := Space(GetSx3Cache("ZZR_MEDICA","X3_TAMANHO"))
	Private cCodCli   := Space(GetSx3Cache("ZZR_CODCLI","X3_TAMANHO"))
	Private cCodLoja  := Space(GetSx3Cache("ZZQ_LOJA","X3_TAMANHO"))
	Private cCliente  := Space(GetSx3Cache("ZZR_NOMECL","X3_TAMANHO"))
	Private cCodUse   := Space(15)
	Private cNomeUse  := Space(GetSx3Cache("ZZR_NOMUSE","X3_TAMANHO"))
	Private cCodEtp   := Space(GetSx3Cache("ZZR_ETAPA","X3_TAMANHO"))
	Private cEtapa    := Space(GetSx3Cache("ZZR_NOMETP","X3_TAMANHO"))
	Private cCodPetp  := Space(GetSx3Cache("ZZR_ETAPA","X3_TAMANHO"))
	Private cPetapa   := Space(GetSx3Cache("ZZR_NOMETP","X3_TAMANHO"))
	Private dEmissao  := Space(GetSx3Cache("ZZR_EMISSA","X3_TAMANHO"))
	Private cObs      := Space(GetSx3Cache("ZZR_OBS","X3_TAMANHO"))
	Private cDesSer   := ""


	oFont  := TFont():New( "Arial",0,18,,.T.,0,,700,.F.,.F.,,,,,, )
	oFont2 := TFont():New( "Arial",0,22,,.T.,0,,700,.F.,.F.,,,,,, )
	DEFINE MSDIALOG oDlg TITLE "Controle de Titularidade de OS" FROM 000, 000  TO 700, 900 COLORS 0, 16777215 PIXEL
	oDlg:SetFont(oFont)

	oSayOrdem	:= TSay():New( 020,020,{||"Medi��o"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,032,008)
	oGet1	    := TGet():New( 032,020,{|u|If(PCount()>0,cOrdem:=u,cOrdem)},,100,015,'@!',{||FBuscaMed(cOrdem),ItensBrow(),.T.},,,oFont2,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cOrdem",,)

	oSayEmi		:= TSay():New( 020,125,{||"Emiss�o"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,040,008)
	oGet2		:= TGet():New( 032,125,{|u| If(PCount()>0,dEmissao:=u,dEmissao)},,080,015,'@!',{||},,,oFont2,,,.T.,"",,{||.F.},.F.,.F.,,.F.,.F.,"","dEmissao",,)

	oSayFAtu	:= TSay():New( 020,210,{||"Fase Atual"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,052,008)
	oGet3		:= TGet():New( 032,210,{|u| If(PCount()>0,cEtapa:=u,cEtapa)},,200,015,'@!',{|| .T. },,,oFont2,,,.T.,"",,{||.F.},.F.,.F.,,.F.,.F.,"","cEtapa",,)

	oSayFatu	:= TSay():New( 055,020,{||"Cliente"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,052,008)
	oGet4		:= TGet():New( 065,020,{|u| If(PCount()>0,cCliente:=u,cCliente)},,300,015,'@!',{|| .T. },,,oFont2,,,.T.,"",,{||.F.},.F.,.F.,,.F.,.F.,"","cCliente",,)

	oSaycResp	:= TSay():New( 090,020,{||"C�digo Resp."},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,080,008)
	oGet5		:= TGet():New( 100,020,{|u| If(PCount()>0,cCodUse:=u,cCodUse)},,050,015,'@!',{||FBuscaUser()},,,oFont2,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cCodUse",,)

	oSaycResp	:= TSay():New( 090,080,{||"Respons�vel Atual"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,080,008)
	oGet6		:= TGet():New( 100,080,{|u| If(PCount()>0,cNomeUse:=u,cNomeUse)},,300,015,'@!',{||},,,oFont2,,,.T.,"",,{||.F.},.F.,.F.,,.F.,.F.,"","cNomeUse",,)

	oSayFAtu2	:= TSay():New( 125,020,{||"Pr�xima Fase"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,080,008)
	oGet7		:= TGet():New( 135,020,{|u| If(PCount()>0,cPetapa:=u,cPetapa)},,200,015,'@!',{|| .T. },,,oFont2,,,.T.,"",,{||.F.},.F.,.F.,,.F.,.F.,"","cPetapa",,)

	oSayObs		:= TSay():New( 155,020,{||"Observa��o"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,080,008)
	oGet8		:= TGet():New( 165,020,{|u| If(PCount()>0,cObs:=u,cObs)},,300,015,'@!',{|| .T. },,,oFont2,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cObs",,)

	oSBtnConf   := TButton():New( 300,020,"Confimar(F5)",oDlg,{|| IIf(FBtOk(cOrdem),_nOpc:= 1,_nOpc:= 0) },0100,030,,oFont,,.T.,,"Confirmar",,,,.F. )
	oSBtnConf:SetCss(CssbtnImg("ok.png","#006386"))

	oSBtnCanc   := TButton():New( 300,150,"Cancelar(F7)",,{|| oDlg:End(),_nOpc:= 2 },0100,030,,oFont,,.T.,,"Cancelar",,,,.F. )
	oSBtnCanc:SetCss(CssbtnImg("cancel.png","#006386"))

	oDlg:lMaximized := .T.

	//Confirmar
	SET KEY VK_F5 TO
	SETKEY(VK_F5,{||IIf(FBtOk(cOrdem),_nOpc:= 1,_nOpc:= 0)})

	//Cancelar
	SET KEY VK_F7 TO
	SETKEY(VK_F7,{||oDlg:End()})


	ACTIVATE MSDIALOG oDlg CENTERED

	If _nOpc == 1 //Confirma��o
		FRF013()
	Endif

Return

Static Function FBuscaMed(cOrdem)

	Local cCodEtapa := ""

	If Select("T01") <> 0
		T01->(DbCloseArea())
	EndIf

	cCodEtapa := POSICIONE("ZZ2",1,SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6) + SubStr(cOrdem,13,6), "ZZ2->ZZ2_ETAPA")

	If cCodEtapa >= GetMv("SA_ZZ2CONF")

		//1 - Filial + Contrato + Medi��o + Cliente + Data+ Etapa
		cQuery := " SELECT ZZ2_FILIAL,ZZ2_NUMERO,ZZ2_MEDICA,ZZ2_CLIENT,ZZ2_LOJA,ZZ2_NOME,ZZ2_DTGER,ZZ2_ETAPA "
		cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2  "
		cQuery += " INNER JOIN "+RetSqlName("ZZ0")+" ZZ0(NOLOCK) "
		cQuery += " ON ZZ0_FILIAL = ZZ2_FILIAL AND ZZ0_NUMERO = ZZ2_NUMERO AND ZZ0_CLIENT = ZZ2_CLIENT AND ZZ0_LOJA = ZZ2_LOJA AND ZZ0_ANOCOM = '2016' AND ZZ0.D_E_L_E_T_ ='' "
		cQuery += " WHERE  ZZ2.D_E_L_E_T_ = ''  "
		cQuery += " AND ZZ2_FILIAL ='" + SubStr(cOrdem,1,6) + "' "
		cQuery += " AND ZZ2_NUMERO ='" + SubStr(cOrdem,7,6) + "'"
		cQuery += " AND ZZ2_MEDICA ='" + SubStr(cOrdem,13,6)+ "'"
		//cQuery += " AND ZZ2_CLIENT ='"+ cCodCli+ "'"
		//cQuery += " AND ZZ2_LOJA = '"+ cCodLoja +"'"
		cQuery += " ORDER BY ZZ2_FILIAL ,ZZ2_MEDICA  "
	
		cQuery := ChangeQuery(cQuery)

		DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T01")

		cContrato:= T01->ZZ2_NUMERO
		cMedicao := T01->ZZ2_MEDICA
		cCodCli  := T01->ZZ2_CLIENT
		cCodLoja := T01->ZZ2_LOJA
		cCliente := T01->ZZ2_NOME
		dEmissao := sToD(T01->ZZ2_DTGER)
		cCodEtp  := T01->ZZ2_ETAPA
		cEtapa   := POSICIONE("SX5",1,xFilial("SX5")+"ZA"+T01->ZZ2_ETAPA,"X5_DESCRI")

		T01->(dbclosearea())

		If Select("T02") <> 0
			T02->(DbCloseArea())
		EndIf

		//2 - Fases do processo
		cQuery := " SELECT TOP	1 X5_TABELA,X5_CHAVE,X5_DESCRI "
		cQuery += " FROM "+RetSqlName("SX5")+" SX5  "
		cQuery += " WHERE  SX5.D_E_L_E_T_ = ''  "
		cQuery += " AND X5_TABELA ='ZA'  "
		cQuery += " AND X5_CHAVE >'" + cCodEtp + "'"
		cQuery += " ORDER BY X5_CHAVE "

		cQuery := ChangeQuery(cQuery)

		DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T02")


		If !Empty(cCodEtp)
			cCodPetp := T02->X5_CHAVE
			cPetapa  := T02->X5_DESCRI
		EndIF

		T02->(dbclosearea())

	Else

		Alert("OS n�o confirmada no PCE. Confirme-a e tente novamente.")

	EndIf


Return .T.

Static Function FBuscaUser()

	Local cColigada := ""
	Local cChapa := ""
	Local cCodUse2 := cCodUse

	cColigada := Substring(cCodUse,1,2)
	cChapa := Substring(cCodUse,3,5)

	If Substring(cColigada,1,1) == '0' 

		cColigada := Substring(cColigada,2,1)

	EndIf


	If Select("T03") <> 0
		T03->(DbCloseArea())
	EndIf

	//3 - C�digo Respons�vel
	cQuery := " SELECT CODCOLIGADA,CHAPA,NOME "
	cQuery += " FROM "+RetSqlName("VW_USUARIOS_RM")+"VW_USUARIOS_RM  "
	cQuery += " WHERE CRACHA_SAS = '"+ cCodUse + "' "
	//cQuery += " WHERE CODCOLIGADA = '"+ Alltrim(cColigada) + "' " //SubStr(cOrdem,1,6) cracha_sas	
	//cQuery += " AND CHAPA ='" + Alltrim(cChapa) + "'"
	cQuery += " ORDER BY CODCOLIGADA,CHAPA"

	cQuery := ChangeQuery(cQuery)

	DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T03")

	cCodUse  := T03->CHAPA
	cNomeUse := T03->NOME

	T03->(dbclosearea())

	cCodUse := cCodUse2

Return

Static Function ItensBrow()

	Local cDescri := ""

	If cCodEtp == cMvLibFat .AND. !Empty(cOrdem) //Valida��o do c�digo da etapa com par�metro SA_ETAPA para cria��o do grid
		aDados:= {}
		Aadd(aDados,{"","","","",0})

		oLista := TSay():New( 185,020,{||"Volume"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,090,008)
		oLista  := MsBrGetDBase():New( 195 ,020,400,100,,,, oDlg,,,,,,,,,,,, .F., "", .T.,, .F.,,{"5"}, )
		oLista:SetArray(aDados)

		If type("oLista:nAt") <> "U"
			oLista:AddColumn(TCColumn():New("Filial ",{ || aDados[oLista:nAt,1] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("N�mero ",{ || aDados[oLista:nAt,2] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("Medi��o ",{ || aDados[oLista:nAt,3] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("Descri��o                                 ",{ || aDados[oLista:nAt,4] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("Volume ",{ || aDados[oLista:nAt,5] },GetSx3Cache("ZZ3_QUANT ","X3_PICTURE"),,,"RIGHT",,.F.,.F.,,,,.F.,))
			//oLista:blDblClick	:=	{||lEditCell(aDados,oLista,"@E 999,999,999.99",5) }
			oLista:blDblClick	:=	{||FInfoVol() }		
		Endif
		//TODO Tipo de pedido da Medi��o
		// Weskley Silva 19.09.2016
		cTipoMed := POSICIONE("ZZ2",1,SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6) + SubStr(cOrdem,13,6), "ZZ2->ZZ2_TIPO")
		
		If Select("T04") <> 0
			T04->(DbCloseArea())
		EndIf
		 
		//T04 - Dados  Filial + Contrato + Medi��o + Serie + Envio+ Grupo + Categoria + Descri��o para montagem do grid
		
		// TODO Se o tipo de pedido for diferente da campanha de Matricula(000024), ser� agrupado por serie e envio, sen�o ser� agrupado por serie
		// Weskley Silva 19.09.2016
		IF !cTipoMed == "000024"
		
		cQuery := " SELECT ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA,ZZ7_SERIE,ZZ7_ENVIO,ZZ7_GRUPO,ZZ7_CATEGO AS TCATEGO, 'K' AS TIPO,ZZ3_CLIENT AS CLIENTE,ZZ3_LOJA,ZZ2_TIPO AS TPMED , " // ZZ7_CODIGO AS B1_COD, ZZ7_DESCR AS B1_DESC, "
		cQuery += " CASE ZZ7_CATEGO WHEN 'A' THEN 'Aluno' "
		cQuery += " WHEN 'P' THEN 'Professor' "
		cQuery += " WHEN 'C' THEN 'Coordenacao' "
		cQuery += " WHEN 'O' THEN 'Outros' end ZZ7_CATEGOB "
		cQuery += " FROM "+RetSqlName("ZZ3")+" ZZ3  "
		cQuery += " INNER JOIN " +RetSqlName("ZZ7")+" ZZ7  "
		//cQuery += "		ON ZZ3.ZZ3_PRODUT  = ZZ7.ZZ7_CODPAI
		cQuery += "		ON ZZ3.ZZ3_PRODUT  = ZZ7.ZZ7_CODIGO "
		cQuery += " INNER JOIN ZZ2010 ZZ2 "
	    cQuery += " ON ZZ2_FILIAL = ZZ3_FILIAL AND ZZ2_NUMERO = ZZ3_NUMERO AND ZZ2_MEDICA = ZZ3_MEDICA AND ZZ2_CLIENT = ZZ3_CLIENT AND ZZ2_LOJA = ZZ3_LOJA "
		cQuery += " WHERE ZZ3.D_E_L_E_T_ = ''  "
		cQuery += " AND ZZ7.D_E_L_E_T_ = ''  "
		cQuery += " AND ZZ3_FILIAL ='" + SubStr(cOrdem,1,6) + "'"
		cQuery += " AND ZZ3_NUMERO ='" + SubStr(cOrdem,7,6) + "'"
		cQuery += " AND ZZ3_MEDICA ='" + SubStr(cOrdem,13,6) + "'"
		cQuery += " AND ZZ3_CLIENT = '"+cCodCli+"' "
		cQuery += " AND ZZ3_LOJA = '"+cCodLoja+"' " 
		cQuery += " AND ZZ3_TIPO = 'K' "
		cQuery += " GROUP BY ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA,ZZ7_SERIE,ZZ7_ENVIO,ZZ7_GRUPO, ZZ7_CATEGO, ZZ3_CLIENT,ZZ3_LOJA, ZZ2_TIPO "

		cQuery += " UNION "

		cQuery += " SELECT ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA,'20' AS ZZ7_SERIE ,'-' AS ZZ7_ENVIO ,'-' AS ZZ7_GRUPO,'-' AS TCATEGO, 'P' AS TIPO, "//, B1_COD AS B1_COD, B1_DESC AS B1_DESC, "
		cQuery += " '-' AS ZZ7_CATEGOB,ZZ3_CLIENT AS CLIENTE,ZZ3_LOJA, '-' AS TPMED "
		cQuery += " FROM "+RetSqlName("ZZ3")+" ZZ3  "
		cQuery += " INNER JOIN " +RetSqlName("SB1")+" SB1  "
		cQuery += "		ON ZZ3.ZZ3_PRODUT  = SB1.B1_COD "
		cQuery += " WHERE ZZ3.D_E_L_E_T_ = ''  "
		cQuery += " AND SB1.D_E_L_E_T_ = ''  "
		cQuery += " AND ZZ3_FILIAL ='" + SubStr(cOrdem,1,6) + "'"
		cQuery += " AND ZZ3_NUMERO ='" + SubStr(cOrdem,7,6) + "'"
		cQuery += " AND ZZ3_MEDICA ='" + SubStr(cOrdem,13,6) + "'"
		cQuery += " AND ZZ3_CLIENT = '"+cCodCli+"' "
		cQuery += " AND ZZ3_LOJA = '"+cCodLoja+"' " 
		cQuery += " AND ZZ3_TIPO = 'P' "
		cQuery += " GROUP BY ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA,ZZ3_CLIENT,ZZ3_LOJA"//,B1_COD,B1_DESC
		cQuery += " ORDER BY ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA "
		
		ELSE 	
		
		cQuery := " SELECT ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA,ZZ7_SERIE,'-' AS ZZ7_ENVIO,ZZ7_GRUPO,ZZ7_CATEGO AS TCATEGO, 'K' AS TIPO,ZZ3_CLIENT AS CLIENTE,ZZ3_LOJA,ZZ2_TIPO AS TPMED , " // ZZ7_CODIGO AS B1_COD, ZZ7_DESCR AS B1_DESC, "
		cQuery += " CASE ZZ7_CATEGO WHEN 'A' THEN 'Aluno' "
		cQuery += " WHEN 'P' THEN 'Professor' "
		cQuery += " WHEN 'C' THEN 'Coordenacao' "
		cQuery += " WHEN 'O' THEN 'Outros' end ZZ7_CATEGOB "
		cQuery += " FROM "+RetSqlName("ZZ3")+" ZZ3  "
		cQuery += " INNER JOIN " +RetSqlName("ZZ7")+" ZZ7  "
		//cQuery += "		ON ZZ3.ZZ3_PRODUT  = ZZ7.ZZ7_CODPAI
		cQuery += "		ON ZZ3.ZZ3_PRODUT  = ZZ7.ZZ7_CODIGO "
		cQuery += " INNER JOIN ZZ2010 ZZ2 "
	    cQuery += " ON ZZ2_FILIAL = ZZ3_FILIAL AND ZZ2_NUMERO = ZZ3_NUMERO AND ZZ2_MEDICA = ZZ3_MEDICA AND ZZ2_CLIENT = ZZ3_CLIENT AND ZZ2_LOJA = ZZ3_LOJA "
		cQuery += " WHERE ZZ3.D_E_L_E_T_ = ''  "
		cQuery += " AND ZZ7.D_E_L_E_T_ = ''  "
		cQuery += " AND ZZ3_FILIAL ='" + SubStr(cOrdem,1,6) + "'"
		cQuery += " AND ZZ3_NUMERO ='" + SubStr(cOrdem,7,6) + "'"
		cQuery += " AND ZZ3_MEDICA ='" + SubStr(cOrdem,13,6) + "'"
		cQuery += " AND ZZ3_CLIENT = '"+cCodCli+"' "
		cQuery += " AND ZZ3_LOJA = '"+cCodLoja+"' " 
		cQuery += " AND ZZ3_TIPO = 'K' "
		cQuery += " GROUP BY ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA,ZZ7_SERIE,ZZ7_GRUPO, ZZ7_CATEGO, ZZ3_CLIENT,ZZ3_LOJA, ZZ2_TIPO "

		cQuery += " UNION "

		cQuery += " SELECT ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA,'20' AS ZZ7_SERIE ,'-' AS ZZ7_ENVIO ,'-' AS ZZ7_GRUPO,'-' AS TCATEGO, 'P' AS TIPO, "//, B1_COD AS B1_COD, B1_DESC AS B1_DESC, "
		cQuery += " '-' AS ZZ7_CATEGOB,ZZ3_CLIENT AS CLIENTE,ZZ3_LOJA, '-' AS TPMED "
		cQuery += " FROM "+RetSqlName("ZZ3")+" ZZ3  "
		cQuery += " INNER JOIN " +RetSqlName("SB1")+" SB1  "
		cQuery += "		ON ZZ3.ZZ3_PRODUT  = SB1.B1_COD "
		cQuery += " WHERE ZZ3.D_E_L_E_T_ = ''  "
		cQuery += " AND SB1.D_E_L_E_T_ = ''  "
		cQuery += " AND ZZ3_FILIAL ='" + SubStr(cOrdem,1,6) + "'"
		cQuery += " AND ZZ3_NUMERO ='" + SubStr(cOrdem,7,6) + "'"
		cQuery += " AND ZZ3_MEDICA ='" + SubStr(cOrdem,13,6) + "'"
		cQuery += " AND ZZ3_CLIENT = '"+cCodCli+"' "
		cQuery += " AND ZZ3_LOJA = '"+cCodLoja+"' " 
		cQuery += " AND ZZ3_TIPO = 'P' "
		cQuery += " GROUP BY ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA,ZZ3_CLIENT,ZZ3_LOJA"//,B1_COD,B1_DESC
		cQuery += " ORDER BY ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA "
			
		
		ENDIF
		
		cQuery := ChangeQuery(cQuery)

		DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T04")
	
		cContrat := SubStr(cOrdem,7,6)
		cFili    := SubStr(cOrdem,1,6)
		cCliet   := ALLTRIM(T04->CLIENTE)
		cTipoMed := ALLTRIM(T04->TPMED)
				
		//TODO Contrato Prospec��o: Consulta para retornar os contratos de prospec��o 
		// Weskley Silva 05.09.2016 
		cQuery := " SELECT ZZ0_TIPCTR AS TIPO FROM " +RetSqlName("ZZ0") + " ZZ0 "
		cQuery += " WHERE ZZ0.ZZ0_FILIAL ='" + cFili + "' "
		cQuery += " AND ZZ0.ZZ0_NUMERO ='" + cContrat + "' "
		cQuery += " AND ZZ0.ZZ0_CLIENT = '" + cCliet + "' "
		cQuery += " AND ZZ0.ZZ0_LOJA = '"+cCodLoja+"' " 
		cQuery += " AND ZZ0.D_E_L_E_T_ = '' "
		
		cQuery := ChangeQuery(cQuery)

		DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T00")
		
		cTipCtr := ALLTRIM(T00->TIPO)
		
		cCodSer := IIF(ALLTRIM(T04->TIPO) == "P" .OR. cTipCtr == "P" /*.AND. _AVULSO*/,"99",ALLTRIM(T04->ZZ7_SERIE))

		If T04->(EOF())
			Aadd(aDados,{"","","","",0})
		Else
			aDados:= {}
		EndIf
				
		aDados2 := {}
		aadd(aDados2,{ALLTRIM(T04->ZZ3_FILIAL),;
		ALLTRIM(T04->ZZ3_NUMERO),;
		ALLTRIM(T04->ZZ3_MEDICA),;
		ALLTRIM(T04->ZZ7_SERIE),;
		ALLTRIM(T04->ZZ7_ENVIO),;
		ALLTRIM(T04->ZZ7_GRUPO),;
		ALLTRIM(T04->TCATEGO),;
		ALLTRIM(T04->CLIENTE),;
		ALLTRIM(T04->ZZ3_LOJA)})

		//_AVULSO := .T. 
		While !T04->(EOF())			
		
		IF cTipCtr == "P"
			cDescri := "PRODUTO AVULSO"
			
		ELSEIF ALLTRIM(T04->TIPO) == "K"	
			cDescri := Alltrim(SUBSTR(ALLTRIM(POSICIONE("ZZO",1,ALLTRIM(T04->ZZ7_SERIE),"ZZO->ZZO_DESC" )) + " - " + ;
			Alltrim(T04->ZZ7_CATEGOB)+" - " + ;
			Alltrim(POSICIONE("ZZP",1,XFILIAL("ZZP")+T04->ZZ7_GRUPO,"ZZP_DESCRI")) +" - " + ;
			T04->ZZ7_ENVIO+" Envio"        ,0,60))

		ELSE 
		cDescri := "PRODUTO AVULSO"

		ENDIF			
			IF T04->TIPO == "K" .OR. (T04->TIPO == "P" )//.AND. _AVULSO	)	
				aadd(aDados,{ALLTRIM(T04->ZZ3_FILIAL),;
				ALLTRIM(T04->ZZ3_NUMERO),;
				ALLTRIM(T04->ZZ3_MEDICA),;
				ALLTRIM(cDescri),;
				POSICIONE("ZZQ",1,ALLTRIM(T04->ZZ3_FILIAL)+ T04->ZZ3_NUMERO + T04->ZZ3_MEDICA +  T04->ZZ7_SERIE + T04->TCATEGO + T04->ZZ7_GRUPO + T04->ZZ7_ENVIO ,"ZZQ->ZZQ_QUANT"),;
				IIF(ALLTRIM(T04->TIPO) == "P" .OR. cTipCtr == "P" /*.AND. _AVULSO*/,"99",ALLTRIM(T04->ZZ7_SERIE)),;
				ALLTRIM(T04->ZZ7_ENVIO),;
				ALLTRIM(T04->ZZ7_GRUPO),;
				ALLTRIM(T04->TCATEGO)})

			END			

			T04->(dbSkip())
		EndDo
		If Select("T05") <> 0
			T05->(DbCloseArea())
		EndIf

		//5- Consulta para retornar descri��o da serie, tabela ZZO.
		cQuery := " SELECT ZZO_DESC "
		cQuery += " FROM "+RetSqlName("ZZO") + "  ZZO  "
		cQuery += " WHERE ZZO.D_E_L_E_T_  = ''
		cQuery += " AND ZZO_COD ='" + cCodSer + "' "

		cQuery := ChangeQuery(cQuery)

		DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T05") 
				
		cDesSer := IIF(ALLTRIM(cCodSer) == "99", "PRODUTO AVULSO", T05->ZZO_DESC)
		

		IF(ALLTRIM(cCodSer) == "99" .OR. cTipCtr == 'P')
				aadd(aDados[1],0)
				aDados = {aDados[1]}

		ENDIF
		
		T05->(dbclosearea())
		
		T00->(DbCloseArea())

		T04->(dbclosearea())

		oLista:SetArray(aDados)
		oLista:refresh()
		oLista:Refresh()
		getDRefresh()

	Else
		Aadd(aDados,{"","","","",0})

		oLista := TSay():New( 185,020,{||"Volume"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,090,008)
		oLista  := MsBrGetDBase():New( 195 ,020,400,100,,,, oDlg,,,,,,,,,,,, .F., "", .T.,, .F.,,{"5"}, )
		oLista:SetArray(aDados)

	EndIf

Return .T.

Static Function FBtOk(cOrdem)

	Local lRet		:= .T.
	Local nVolume	:= 0
	Local cQuery	:= ""
	Local lLib		:= .F.
	
	//IF SELECT("ZA0") > 0
	 //  ZA0->(DBCLOSEAREA()) 
	//ENDIF
	
	//DbSelectArea("ZA0")
	//ZA0->(DBSETORDER(1))
	
	//If(GetMV("SA_BLQBIP"))
		//If(ZA0->(DBSEEK(xFilial("ZA0") + AllTrim(cCodUse) + cCodEtp)))
			lLib := .T.
		//EndIf
		lLib := .T.
	//Endif
	//INICIOOK
	If (lLib)
		If Empty(cOrdem)
			Alert("N�o � possivel confirmar um item vazio")
			Return lRet
		EndIf
	
		//Valida��o se informou o volume
		If cCodEtp == cMvLibFat
			For nX:=1 To len(aDados)
				If aDados[nX,5] <= 0
					Alert("N�o poder� ser gravado opera��o devido a n�o ter sido informado o volume")
					Return .F.
				Endif
			Next
		Endif
	
		//Valida se foi informado a Medi��o corretamente
		dbSelectArea("ZZ2")
		ZZ2->(dbSetOrder(1))
		If !(ZZ2->(dbSeek(SubStr(cOrdem,1,6) + Alltrim(SubStr(cOrdem,7,6))+ Alltrim(SubStr(cOrdem,13,6)))))
			Alert("N�o poder� ser gravado opera��o devido a n�o ter sido informado a medi��o corretamente")
			Return .F.
		Endif
	
		//Valida cod etapa na medi��o
		If Empty(cCodEtp)
			Alert("Campo C�d. Etapa n�o preenchido na medi��o. Verificar conteudo do campo.")
			Return .F.
		EndIf
	
		//VERIFICAR NA ETAPA DE FATURAMENTO SE JA FOI FATURADA, PARA PERMITIR IR PARA A EXPEDI�AO
		If POSICIONE("ZZ2",1,SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6) + SubStr(cOrdem,13,6), "ZZ2->ZZ2_ETAPA") == alltrim(getmv("SA_ETPFAT"))		
			IF ALLTRIM(POSICIONE("ZZ2",1,SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6) + SubStr(cOrdem,13,6), "ZZ2->ZZ2_PV01NF")) == ""
				Alert("Pedidos n�o faturados. Verifique os pedidos da medi��o")
				Return .F.
			ENDIF
		EndIf
	
		//VERIFICAR SE A MEDI��O FOI BLOQUEADA DEPOIS DE FATURADA 
		If POSICIONE("ZZ2",1,SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6) + SubStr(cOrdem,13,6), "ZZ2->ZZ2_ETAPA") == alltrim(getmv("SA_ETPFAT")) .AND.	alltrim(POSICIONE("ZZ2",1,SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6) + SubStr(cOrdem,13,6), "ZZ2->ZZ2_STATOS")) == "B"		
			Alert("Medi��o bloqueada ap�s faturamento, verificar com setor do PCE! ")
			Return .F.
		EndIf
	
		//Valida se informou o respons�vel
		If Empty(cNomeUse)
			Alert("Respons�vel n�o informado")
			Return .F.
		Endif
	
		If lRet
			DbSelectArea("ZZR")
			dbSetOrder(2)
			If dbSeek(SubStr(cOrdem,1,6) + Alltrim(cContrato)+ Alltrim(cMedicao) + cCodCli + cCodLoja + GETMV("SA_ETPFAT"))
				alert("Registro j� cadastrado")
				Return lRet
			Else
			

				RecLock("ZZR", .T.)
				ZZR->ZZR_FILIAL := SubStr(cOrdem,1,6)
				ZZR->ZZR_CONTRA := cContrato
				ZZR->ZZR_MEDICA := cMedicao
				ZZR->ZZR_EMISSA := dEmissao
				ZZR->ZZR_CODCLI := cCodCli      //Atualiza��o tabela ZZR
				ZZR->ZZR_LOJA	:= cCodLoja
				ZZR->ZZR_NOMECL := cCliente
				ZZR->ZZR_CODUSE := cCodUse
				ZZR->ZZR_NOMUSE := cNomeUse
				ZZR->ZZR_ETAPA  := cCodEtp
				ZZR->ZZR_NOMETP := cEtapa
				ZZR->ZZR_OBS    := cObs
				ZZR->ZZR_DATA   := Date()
				ZZR->ZZR_HORA   := Time()
				MsUnLock()
			EndIf
		Endif
	
		If cCodEtp == cMvLibFat	
	
			For nX:=1 To len(aDados)			
	
				DBSELECTAREA("ZZQ")
				DBSETORDER(3)
				DBSEEK(aDados2[1,1] + aDados2[1,2] + aDados2[1,3] + cCodCli + cCodLoja + aDados[nX,6] + aDados[nX,9] + PadR( aDados[nX,8], TamSX3( "ZZQ_GRUPO")[ 1 ] )  +  PadR( aDados[nX,7], TamSX3( "ZZQ_ENVIO")[ 1 ] ) )
	
				If Found()
	
					RecLock("ZZQ", .F.)
					ZZQ->ZZQ_FILIAL := aDados2[1,1]
					ZZQ->ZZQ_NUMERO := aDados2[1,2]
					ZZQ->ZZQ_MEDICA := aDados2[1,3]	//Atualiza��o tabela ZZQ
					ZZQ->ZZQ_CODSER := aDados[nX,6]
					ZZQ->ZZQ_CLIENT := cCodCli
					ZZQ->ZZQ_LOJA	:= cCodLoja
					ZZQ->ZZQ_DESCSE := aDados[nX,4]//Alltrim(cDesSer)
					ZZQ->ZZQ_ENVIO  := aDados[nX,7]
					ZZQ->ZZQ_GRUPO  := aDados[nX,8]
					ZZQ->ZZQ_CATEGO := aDados[nX,9]
					ZZQ->ZZQ_DESGRU := POSICIONE("ZZP",1,xFilial("ZZP", SubStr(cOrdem,1,6))+ aDados[nX,8] ,"ZZP_DESCRI")
					ZZQ->ZZQ_QUANT  := aDados[nX,5]
					MsUnLock()		
	
				Else
	
					RecLock("ZZQ", .T.)
					ZZQ->ZZQ_FILIAL := aDados2[1,1]
					ZZQ->ZZQ_NUMERO := aDados2[1,2]
					ZZQ->ZZQ_MEDICA := aDados2[1,3]	//Atualiza��o tabela ZZQ
					ZZQ->ZZQ_CODSER := aDados[nX,6]
					ZZQ->ZZQ_DESCSE := aDados[nX,4]//Alltrim(cDesSer)
					ZZQ->ZZQ_CLIENT := cCodCli
					ZZQ->ZZQ_LOJA	:= cCodLoja
					ZZQ->ZZQ_ENVIO  := aDados[nX,7]
					ZZQ->ZZQ_GRUPO  := aDados[nX,8]
					ZZQ->ZZQ_CATEGO := aDados[nX,9]
					ZZQ->ZZQ_DESGRU := POSICIONE("ZZP",1,xFilial("ZZP", SubStr(cOrdem,1,6))+ aDados[nX,8],"ZZP_DESCRI")
					ZZQ->ZZQ_QUANT  := aDados[nX,5]
					MsUnLock()
	
				EndIf
	
				nVolume += aDados[nX,5]
	
			Next
	
			cQuery := ""
			cQuery += " SELECT C5_FILIAL,C5_NUM "
			cQuery += "   FROM " + RETSQLNAME("SC5")  + " SC5 "
			cQuery += "  WHERE SC5.D_E_L_E_T_ = '' "
			//cQuery += "    AND SC5.C5_FILIAL = '" +  aDados2[1,1] + "' "  
			cQuery += "    AND SC5.C5_YCONTRA = '" + aDados2[1,2] + "' "
			cQuery += "    AND SC5.C5_YMEDICA = '" + aDados2[1,3] + "' "
	
			cQuery := ChangeQuery(cQuery)
	
			DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"XC5")
	
			While XC5->(!eof()) //SC5->C5_FILIAL+SC5->C5_NUM == aDados2[1][1] +  XC5->C5_NUM		
	
				DBSELECTAREA("SC5")
				DBSETORDER(1)
				DBSEEK(XC5->C5_FILIAL + XC5->C5_NUM)
	
				RECLOCK("SC5", .F.)
	
				SC5->C5_VOLUME1 := nVolume
				SC5->C5_ESPECI1 := "CX" 			
	
				MSUNLOCK()	
				SC5->(DBSKIP())
	
				dbselectArea("XC5")
				XC5->(dbSkip())
	
			EndDo
	
			XC5->(DBCLOSEAREA())
	
	
			aDados:= {}
			Aadd(aDados,{"","","","",0})
			oLista:SetArray(aDados)
			oLista:Refresh()
		Endif
		
		IF cCodEtp == '30' 
			DBSELECTAREA("ZZ2")
			DBSETORDER(1)
			DBSEEK(XFILIAL("ZZ2",aDados2[1,1])+ aDados2[1,2] + aDados2[1,3] + cCodCli + cCodLoja)
			U_SASR031(aDados2[1,3],cCodCli,cCodLoja)	
       ENDIF
		If cCodEtp < cMvFetapa
			RecLock("ZZ2",.F.)
			ZZ2->ZZ2_ETAPA := cCodPetp
			MsUnLock()
		Endif
	
		cOrdem    := Space(20)
		cCodCli   := Space(GetSx3Cache("ZZR_CODCLI","X3_TAMANHO"))
		cCliente  := Space(GetSx3Cache("ZZR_NOMECL","X3_TAMANHO"))
		cCodUse   := Space(GetSx3Cache("ZZR_CODUSE","X3_TAMANHO"))
		cNomeUse  := Space(GetSx3Cache("ZZR_NOMUSE","X3_TAMANHO"))
		cCodEtp   := Space(GetSx3Cache("ZZR_ETAPA","X3_TAMANHO"))
		cEtapa    := Space(GetSx3Cache("ZZR_NOMETP","X3_TAMANHO"))
		cPetapa   := Space(GetSx3Cache("ZZR_NOMETP","X3_TAMANHO"))
		dEmissao  := Space(GetSx3Cache("ZZR_EMISSA","X3_TAMANHO"))
		cObs      := Space(GetSx3Cache("ZZR_OBS","X3_TAMANHO"))
	
		oGet1:BUFFER := Space(20)
	
		oGet1:ReFresh()
		oGet2:ReFresh()
		oGet3:ReFresh()
		oGet4:ReFresh()
		oGet5:ReFresh()
		oGet6:ReFresh()
		oGet7:ReFresh()
		oGet8:ReFresh()
	
		oGet1:SetFocus()
		GetdRefresh()
	
		If lRet
			msGInfo("Registro inclu�do com sucesso")
			//If POSICIONE("ZZ2",1,SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6) + SubStr(cOrdem,13,6), "ZZ2->ZZ2_ETAPA") == alltrim(getmv("SA_ETPFAT"))
	
			//Endif
			oDlg:End()
		Endif
	Else
		Alert("Seu usu�rio n�o tem permiss�o para confirmar nesta etapa.")
		Return lRet
	Endif
//FIMOK
Return lRet

Static Function FInfoVol
	Local aPergs	:= {}
	Local aRetOpc	:= {}
	Local nVol		:= 0

	If Type("oLista:nAt") <> "U"
		nVol:= aDados[oLista:nAt,5]
	Else
		Return
	Endif

	aAdd( aPergs ,{1,"Volume",	nVol	,GetSx3Cache("ZZ3_QUANT","X3_PICTURE") ,'.T.',"" ,'.T.',80 ,.T.})

	If ParamBox(aPergs,"Informe Volume",aRetOpc,,,,,,,"_RF13",.F.,.F.)
		aDados[oLista:nAt,5]:=	aRetOpc[1]
		oLista:Refresh()

		DBSELECTAREA("ZZQ")
		DBSETORDER(3)
		//       Filial         Contrato        Medicao      Cliente    Loja          Serie                   Categoria                 Grupo                   Envio
		DBSEEK(aDados2[1,1] + aDados2[1,2] + aDados2[1,3] + cCodCli + cCodLoja +  aDados[oLista:nAt,6] + aDados[oLista:nAt,9] + aDados[oLista:nAt,8] +  aDados[oLista:nAt,7])
                                                                 
		
		If Found()

			RecLock("ZZQ", .F.)
				ZZQ->ZZQ_FILIAL := aDados2[1,1]
				ZZQ->ZZQ_NUMERO := aDados2[1,2]
				ZZQ->ZZQ_MEDICA := aDados2[1,3]	//Atualiza��o tabela ZZQ
				ZZQ->ZZQ_CODSER := aDados[oLista:nAt,6]
				ZZQ->ZZQ_DESCSE := aDados[oLista:nAt,4]
				ZZQ->ZZQ_CLIENT := cCodCli
				ZZQ->ZZQ_LOJA	:= cCodLoja
				ZZQ->ZZQ_ENVIO  := aDados[oLista:nAt,7]
				ZZQ->ZZQ_GRUPO  := aDados[oLista:nAt,8]
				ZZQ->ZZQ_CATEGO := aDados[oLista:nAt,9]
				ZZQ->ZZQ_DESGRU := POSICIONE("ZZP",1,xFilial("ZZP", SubStr(cOrdem,1,6))+ aDados[oLista:nAt,8] ,"ZZP_DESCRI")
				ZZQ->ZZQ_QUANT  := aDados[oLista:nAt,5]
			MsUnLock()	
			
			U_SASR032(aDados2[1,1] + aDados2[1,2] + aDados2[1,3],aDados[oLista:nAt,7],aDados[oLista:nAt,6],aDados[oLista:nAt,9],cCodCli,cCodLoja)
			
		Else

			RecLock("ZZQ", .T.)
			ZZQ->ZZQ_FILIAL := aDados2[1,1]
			ZZQ->ZZQ_NUMERO := aDados2[1,2]
			ZZQ->ZZQ_MEDICA := aDados2[1,3]	//Atualiza��o tabela ZZQ
			ZZQ->ZZQ_CODSER := aDados[oLista:nAt,6]
			ZZQ->ZZQ_DESCSE := aDados[oLista:nAt,4]
			ZZQ->ZZQ_CLIENT := cCodCli
			ZZQ->ZZQ_LOJA	:= cCodLoja
			ZZQ->ZZQ_ENVIO  := aDados[oLista:nAt,7]
			ZZQ->ZZQ_GRUPO  := aDados[oLista:nAt,8]
			ZZQ->ZZQ_CATEGO := aDados[oLista:nAt,9]
			ZZQ->ZZQ_DESGRU := POSICIONE("ZZP",1,xFilial("ZZP", SubStr(cOrdem,1,6))+ aDados[oLista:nAt,8],"ZZP_DESCRI")
			ZZQ->ZZQ_QUANT  := aDados[oLista:nAt,5]
			MsUnLock()

			U_SASR032(ALLTRIM(cOrdem),aDados[oLista:nAt,7],aDados[oLista:nAt,6],aDados[oLista:nAt,9],cCodCli,cCodLoja)
			
			
		EndIf

	Endif
	/*
				DBSELECTAREA("ZZ2")
				DBSETORDER(1)
				DBSEEK(XFILIAL("ZZ2",aDados2[1,1])+ aDados2[1,2] + aDados2[1,3])
				U_SASR031(aDados2[1,3])	
				*/
Return

Static Function FInfoVolCx
	Local aPergs	:= {}
	Local aRetOpc	:= {}
	Local nVol		:= 0
	Local nCx		:= 0

	If Type("oLista:nAt") <> "U"
		nVol:= aDados[oLista:nAt,5]
		nCx:= aDados[oLista:nAt,10]
	Else
		Return
	Endif

	aAdd( aPergs ,{1,"Volume",	nVol	,GetSx3Cache("ZZ3_QUANT","X3_PICTURE") ,'.T.',"" ,'.T.',80 ,.T.})
	aAdd( aPergs ,{1,"Qtd. Caixas",	nCx	,GetSx3Cache("ZZ3_QUANT","X3_PICTURE") ,'.T.',"" ,'.T.',80 ,.T.})
	If ParamBox(aPergs,"Informe Volume",aRetOpc,,,,,,,"_RF13",.F.,.F.)
		aDados[oLista:nAt,5]:=	aRetOpc[1]
		aDados[oLista:nAt,10]:= aRetOpc[2]
		oLista:Refresh()

		DBSELECTAREA("ZZQ")
		DBSETORDER(3)
		//       Filial         Contrato        Medicao        Serie                   Categoria                 Grupo                   Envio
			DBSEEK(aDados2[1,1] + aDados2[1,2] + aDados2[1,3] + cCodCli + cCodLoja +  aDados[oLista:nAt,6] + aDados[oLista:nAt,9] + aDados[oLista:nAt,8] +  aDados[oLista:nAt,7])

		If Found()

			RecLock("ZZQ", .F.)
				ZZQ->ZZQ_FILIAL := aDados2[1,1]
				ZZQ->ZZQ_NUMERO := aDados2[1,2]
				ZZQ->ZZQ_MEDICA := aDados2[1,3]	//Atualiza��o tabela ZZQ
				ZZQ->ZZQ_CODSER := aDados[oLista:nAt,6]
				ZZQ->ZZQ_DESCSE := aDados[oLista:nAt,4]
				ZZQ->ZZQ_CLIENT := cCodCli
				ZZQ->ZZQ_LOJA	:= cCodLoja
				ZZQ->ZZQ_ENVIO  := aDados[oLista:nAt,7]
				ZZQ->ZZQ_GRUPO  := aDados[oLista:nAt,8]
				ZZQ->ZZQ_CATEGO := aDados[oLista:nAt,9]
				ZZQ->ZZQ_DESGRU := POSICIONE("ZZP",1,xFilial("ZZP", SubStr(cOrdem,1,6))+ aDados[oLista:nAt,8] ,"ZZP_DESCRI")
				ZZQ->ZZQ_QUANT  := aDados[oLista:nAt,5]
			MsUnLock()	
			
			U_SASR032(aDados2[1,1] + aDados2[1,2] + aDados2[1,3],aDados[oLista:nAt,7],aDados[oLista:nAt,6],aDados[oLista:nAt,9],cCodCli,cCodLoja)	

		Else

			RecLock("ZZQ", .T.)
			ZZQ->ZZQ_FILIAL := aDados2[1,1]
			ZZQ->ZZQ_NUMERO := aDados2[1,2]
			ZZQ->ZZQ_MEDICA := aDados2[1,3]	//Atualiza��o tabela ZZQ
			ZZQ->ZZQ_CODSER := aDados[oLista:nAt,6]
			ZZQ->ZZQ_DESCSE := aDados[oLista:nAt,4]
			ZZQ->ZZQ_CLIENT := cCodCli
			ZZQ->ZZQ_LOJA	:= cCodLoja
			ZZQ->ZZQ_ENVIO  := aDados[oLista:nAt,7]
			ZZQ->ZZQ_GRUPO  := aDados[oLista:nAt,8]
			ZZQ->ZZQ_CATEGO := aDados[oLista:nAt,9]
			ZZQ->ZZQ_DESGRU := POSICIONE("ZZP",1,xFilial("ZZP", SubStr(cOrdem,1,6))+ aDados[oLista:nAt,8],"ZZP_DESCRI")
			ZZQ->ZZQ_QUANT  := aDados[oLista:nAt,5]
			MsUnLock()

			U_SASR032(ALLTRIM(cOrdem),aDados[oLista:nAt,7],aDados[oLista:nAt,6],aDados[oLista:nAt,9],cCodCli,cCodLoja)	
			
		EndIf
		
	Endif		

Return

Static Function CssbtnImg(cImg,cCor)
	Local cRet := " QPushButton{ background-image: url(rpo:"+cImg+")"+;
	" ;border-style:solid"+;
	" ;border-width:5px"+;
	" ;background-repeat: none; margin: 1px "+;
	" ;background-color: "+cCor+""+;
	" ;border-radius: 6px"+;
	" ;font-weight: bold;font-size: 12px"+;
	" ;color: rgb(255,255,255) "+;
	"}"+;
	"QPushButton:hover { "+;
	" ;background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop:0 "+cCor+", stop:0.5 "+cCor+", stop:1 white)"+;
	"}"
Return cRet