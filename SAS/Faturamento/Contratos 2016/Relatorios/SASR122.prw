#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} RF013 
Romaneio de carga por transportadora (Livrarias)
@author Weskley Silva
@since 19/09/2016
@version 1.0
@example
(examples)
@see (links_or_references)
/*/


user function SASR122()

	Private oReport
	Private cPergCont	:= 'SASR122' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 19 de Setembro de 2016
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportDef()
	Local nOrd	:= 1
	Local oReport
	Local oSection1
	Local oSection2
	Local oBreak
	Local oFunctio
	Local aOrdem:={}

	oReport := TReport():New( 'ROM', 'ROMANEIO DE ENTREGA POR TRANSPORTADORA ', cPergCont, {|oReport| ReportPrint( oReport ), 'ROMANEIO DE ENTREGA POR TRANSPORTADORA' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'ROMANEIO DE ENTREGA POR TRANSPORTADORA', { 'ROM', 'SF2','SA1','SD2','SA4'})

	TRCell():New( oSection1, 'PEDIDO'		    ,'ROM', 'PEDIDO',				"@!"                        ,18)
	TRCell():New( oSection1, 'NF'		        ,'ROM', 'NF',				    "@!"						,15)
	TRCell():New( oSection1, 'COD_CLI'       	,'ROM', 'COD_CLI',		        "@!"				        ,06)
	TRCell():New( oSection1, 'CLIENTE'          ,'ROM', 'CLIENTE',              "@!"						,48)
	TRCell():New( oSection1, 'UF'	   	        ,'ROM', 'UF' ,		     	    "@!"		                ,03)
	TRCell():New( oSection1, 'MUNICIPIO'	    ,'ROM', 'MUNICIPIO' ,	        "@!"		                ,22)
	TRCell():New( oSection1, 'VOL'	            ,'ROM', 'VOL' ,	                "@E 999999"	                ,04)
	TRCell():New( oSection1, 'PESO'	            ,'ROM', 'PESO' ,	            "@E 999,999.99"		        ,08)
	TRCell():New( oSection1, 'STATUS'	        ,'ROM', 'STATUS' ,	            "@!"	                    ,08)
	TRCell():New( oSection1, 'TRANSPORTADORA'	,'ROM', 'TRANSPORTADORA' ,	    "@!"	                    ,19)
	
	oBreakT := TRBreak():New( oSection1, { || }, 'PESO' )
	TRFunction():New(oSection1:Cell('PESO'),' ','SUM',oBreakT,,"@E 999,999.99",    ,.F.,.F.)

	oBreakT := TRBreak():New( oSection1, { || }, 'VOL' )
	TRFunction():New(oSection1:Cell('VOL'),' ','SUM',oBreakT,,"@E 999999",    ,.F.,.F.)

	oBreakT := TRBreak():New( oSection1, { || }, 'NF' )
	TRFunction():New(oSection1:Cell('NF'),' ','COUNT',oBreakT,,"@E 999,999.99",    ,.F.,.F.)

	//oSection1:SetTotalInLine( .F. )


	oSection2 := TRSection():New( oReport, 'ROMANEIO DE ENTREGA POR TRANSPORTADORA', { 'ROM'},,, )

	TrCell():New(oSection2, 'PESSOA',"",'',,75)
	TrCell():New(oSection2, 'TRANSP',"",'',,75)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author Weskley Silva
@since 19 de Setembro de 2016
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("ROM") > 0
		ROM->(dbCloseArea())
	Endif

	cQuery := " SELECT D2_FILIAL+D2_PEDIDO AS PEDIDO,F2_DOC AS NF,F2_CLIENTE AS COD_CLI,SUBSTRING(A1_NOME,1,38) AS CLIENTE, "   
	cQuery += " CASE WHEN (A1_ESTE ='' OR A1_ESTE = NULL OR LTRIM(RTRIM(A1_ESTE)) = 'VAZIO' OR LTRIM(RTRIM(A1_ESTE)) = 'VA') THEN A1_EST ELSE A1_ESTE END AS UF, " 
	cQuery += " CASE WHEN (A1_MUNE = '' OR A1_MUNE = NULL OR LTRIM(RTRIM(A1_MUNE)) = 'VAZIO') THEN A1_MUN ELSE A1_MUNE END AS MUNICIPIO, "
	cQuery += " F2_VOLUME1 AS VOL,SUM(D2_PESO) AS PESO, SUBSTRING(A4_NOME,1,19) AS TRANSPORTADORA, '___________' AS STATUS "   
	cQuery += " FROM SF2010 JOIN  SA1010 ON(F2_LOJA = A1_LOJA AND F2_CLIENTE = A1_COD)"
	cQuery += " JOIN SD2010 ON(D2_FILIAL = F2_FILIAL AND D2_DOC = F2_DOC)"   
	cQuery += " LEFT JOIN SA4010 ON(A4_COD = F2_TRANSP) "           
	cQuery += " WHERE  F2_VOLUME1 >=1 AND A4_COD = '"+ mv_par01 +"' AND F2_EMISSAO  BETWEEN '"+ DTOS(mv_par02) +"' AND '"+ DTOS(mv_par03) +"' "
	cQuery += " AND CASE WHEN (A1_ESTE ='' OR A1_ESTE = NULL OR LTRIM(RTRIM(A1_ESTE)) = 'VAZIO' OR LTRIM(RTRIM(A1_ESTE)) = 'VA') THEN A1_EST ELSE A1_ESTE END BETWEEN '"+mv_par04+"' AND '"+mv_par05+"' "
	cQuery += " AND SF2010.D_E_L_E_T_ ='' AND SD2010.D_E_L_E_T_ ='' AND SA1010.D_E_L_E_T_ ='' AND SA4010.D_E_L_E_T_ = ''"
	cQuery += " AND SF2010.F2_CLIENTE NOT IN('300056','051373','300057','051373','103031','300497','101075','056041','304295')"
	cQuery += " AND D2_TES NOT IN('013','014','015','016','503','504','512')"
	cQuery += " GROUP BY F2_DOC,F2_FILIAL,F2_CLIENTE,A1_NOME,A1_ESTE,A1_EST,A1_MUNE, A1_MUN, "
	cQuery += " F2_VOLUME1,F2_PLIQUI,F2_EMISSAO,A4_COD,A4_NOME,F2_YFILIAL,F2_YCONTRA,F2_YMEDICA,D2_FILIAL+D2_PEDIDO "

	TCQUERY cQuery NEW ALIAS ROM
	//ROM->(DBGOTOP())

	While ROM->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("PEDIDO"):SetValue(ROM->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")

		oSection1:Cell("NF"):SetValue(ROM->NF)
		oSection1:Cell("NF"):SetAlign("LEFT")

		oSection1:Cell("COD_CLI"):SetValue(ROM->COD_CLI)
		oSection1:Cell("COD_CLI"):SetAlign("LEFT")

		oSection1:Cell("CLIENTE"):SetValue(ROM->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")	

		oSection1:Cell("UF"):SetValue(ROM->UF)
		oSection1:Cell("UF"):SetAlign("LEFT")

		oSection1:Cell("MUNICIPIO"):SetValue(ROM->MUNICIPIO)
		oSection1:Cell("MUNICIPIO"):SetAlign("LEFT")

		oSection1:Cell("VOL"):SetValue(ROM->VOL)
		oSection1:Cell("VOL"):SetAlign("LEFT")

		oSection1:Cell("PESO"):SetValue(ROM->PESO)
		oSection1:Cell("PESO"):SetAlign("LEFT")

		oSection1:Cell("STATUS"):SetValue("_______________")
		oSection1:Cell("STATUS"):SetAlign("LEFT")

		oSection1:Cell("TRANSPORTADORA"):SetValue(ROM->TRANSPORTADORA)
		oSection1:Cell("TRANSPORTADORA"):SetAlign("LEFT")

		oSection1:PrintLine()

		ROM->(DBSKIP())

	enddo
	oSection1:Finish()
	
	oSection2:Init()
	oSection2:SetHeaderSection(.T.)

	oSection2:Cell("PESSOA"):SetValue("")
	oSection2:Cell("TRANSP"):SetValue("")
	oSection2:PrintLine()

	oSection2:Cell("PESSOA"):SetValue("")
	oSection2:Cell("TRANSP"):SetValue("")
	oSection2:PrintLine()

	oSection2:Cell("PESSOA"):SetValue("Conferente: ____________________________________________________")
	oSection2:Cell("PESSOA"):SetAlign("LEFT")

	oSection2:Cell("TRANSP"):SetValue("Transportadora: _________________________________________________")
	oSection2:Cell("TRANSP"):SetAlign("LEFT")

	oSection2:PrintLine()

	oSection2:Finish()
	ROM->(DBCLOSEAREA())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 19 de Setembro de 2016
@version P11
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Transportadora"			,""		,""		,"mv_ch01","C",06,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Dt Inicial"		        ,""		,""		,"mv_ch02","D",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Dt Final"			    ,""		,""		,"mv_ch03","D",08,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","UF de"			        ,""		,""		,"mv_ch04","C",02,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","UF Ate"				    ,""		,""		,"mv_ch05","C",02,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
Return

