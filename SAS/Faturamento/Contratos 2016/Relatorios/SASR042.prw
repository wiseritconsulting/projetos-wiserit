// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : SASR042
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Rog�rio J�come            | Descricao Relat�rio Medi��es x NF (Wesley)
// ---------+-------------------+-----------------------------------------------------------
// 21/03/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"
#INCLUDE 'TOPCONN.CH'
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo
Montagem da tela de processamento

@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     21/03/2016
/*/
//------------------------------------------------------------------------------------------
user function SASR042()
	//--< vari�veis >---------------------------------------------------------------------------
	Local oReport
	private cPerg := "SASR042"

	AjustaSX1(cPerg)


	oReport := ReportDef()
	oReport:PrintDialog()
	
	
Return
 
 
Static Function ReportDef()
	Local oReport
	Local oSection
	Local oSection2
	Local aOrdem:={}


	oReport := TReport():New(cPerg,"RELAT�RIO NOTAS FISCAIS " ,cPerg,{|oReport| ReportPrint(oReport)},"RELAT�RIO NOTAS FISCAIS ")
	oReport:SetLandscape()

	oSection1 := TRSection():New(oReport,OemToAnsi("RELAT�RIO NOTAS FISCAIS"),{"SF2"},aOrdem)
	
	TRCell():New(oSection1,"ZZ2_FILIAL","","C�digo Filial","@!",TAMSX3("RA_NOME")[1])
	//TRCell():New(oSection1,"FILIAL","","Nome","@!",23)
	
	oSection2:= TRSection():New(oReport, "Medi��es", {"ZZ2"}, NIL, .F., .T.)
	TRCell():New(oSection2,"ZZ2_TIPCTR"  ,"TRBNCM","Tipo de Contrato"		,"@!",20)
	TRCell():New(oSection2,"ZZ3_TES"  ,"TRBNCM","Tes"	,"@!",TAMSX3("ZZ3_TES")[1])
	TRCell():New(oSection2,"FT_CFOP"  ,"TRBNCM","CFOP"	,"@!",TAMSX3("E2_NUMBOR")[1])
	TRCell():New(oSection2,"ZZ3_FILIAL"	,"TRBNCM","Filial"	,"@!",TAMSX3("ZZ3_FILIAL")[1])
	TRCell():New(oSection2,"F2_DOC"	,"TRBNCM","Numero NF"	,"@!",TAMSX3("F2_DOC")[1])
	TRCell():New(oSection2,"F2_EMISSAO"	,"TRBNCM","Emiss�o NF"			,"@!",TAMSX3("F2_EMISSAO")[1])
	TRCell():New(oSection2,"F2_VALBRUT"  	,"TRBNCM","Valor Bruto"	,"@E 999,999.99",TAMSX3("F2_VALBRUT")[1])
	TRCell():New(oSection2,"F2_DESCONT"	,"TRBNCM","Desconto"			,"@E 999,999.99",TAMSX3("F2_DESCONT")[1])
	TRCell():New(oSection2,"F2_FRETE"	,"TRBNCM","Valor Frete"	,"@E 999,999.99",TAMSX3("F2_FRETE")[1])
	TRCell():New(oSection2,"F2_TPFRETE"	,"TRBNCM","Respons�vel pelo Frete"	,"@!",TAMSX3("F2_FRETE")[1])//F2_TPFRETE
	TRCell():New(oSection2,"F2_MSG"	,"TRBNCM","Msg NT"	,"@!",TAMSX3("F2_YMSGNF")[1])
	TRCell():New(oSection2,"ZZ2_CLIENT"	,"TRBNCM","Cliente"			,"@!",TAMSX3("ZZ2_CLIENT")[1])
	TRCell():New(oSection2,"ZZ2_LOJA"	,"TRBNCM","Loja"			,"@!",TAMSX3("ZZ2_LOJA")[1])
	TRCell():New(oSection2,"ZZ2_NOME"	,"TRBNCM","Nome"			,"@!",TAMSX3("ZZ2_NOME")[1])
	TRCell():New(oSection2,"ZZ2_LOCAL"	,"TRBNCM","Armazem"			,"@!",10)
	TRCell():New(oSection2,"F2_CHVNFE"	,"TRBNCM","Chave NF"		,"@!",TAMSX3("F2_CHVNFE")[1])//F2_DAUTNFE
	TRCell():New(oSection2,"F2_DAUTNFE"	,"TRBNCM","DT Autorizacao NF"		,"@!",TAMSX3("F2_CHVNFE")[1])//F2_DAUTNFE	
	
	
	oSection1:SetPageBreak(.T.)
	oSection1:SetTotalText(" ")
	
RETURN (oReport)

Static Function ReportPrint(oReport)

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Pergunte(cPerg,.F.)
	
																														   "
cQuery := " SELECT 																									   "
cQuery += "		PV1.ZZ2_FILIAL AS FILIAL_MEDICAO,																	   "
cQuery += "		PV1.ZZ2_TIPCTR AS TIPO,																				   "
cQuery += "		PV1.ZZ2_PV01NF AS NF01,																				   "
cQuery += "		PV1.ZZ2_SER01 AS SERIE1,																			   "
cQuery += "		PV1.D2_CF AS CFOP1,																					   "
cQuery += "		PV1.F2_EMISSAO AS DTNF1,																			   "
cQuery += "		PV1.F2_VALBRUT AS VALOR_BRUTO,																		   "
cQuery += "		PV1.F2_FRETE AS FRETE_NF1,																			   "
cQuery += "		PV1.F2_DESCONT AS DESCONT_NF1,																		   "
cQuery += "		PV1.ZZ2_CLNF01 AS CLIENT_NF1,																		   "
cQuery += "		PV1.A1_NOME AS NOME_CLI1,																			   "
cQuery += "		PV1.ZZ2_LJNF01 AS LOJA_NF1,																			   "
cQuery += "		PV1.F2_CHVNFE AS CHAVE_NF1,																			   "
cQuery += "		PV1.MSG_NF1,																						   "
cQuery += "		PV1.F2_TPFRETE AS TPFRETE_NF1,																		   "
cQuery += "		PV1.F2_DAUTNFE AS DTA_NF1,																			   "
cQuery += "		PV2.ZZ2_PV02FL AS FILIAL_NF2,																		   "
cQuery += "		PV2.ZZ2_PV02NF AS NF02,																				   "
cQuery += "		PV2.ZZ2_SER02 AS SERIE2,																			   "
cQuery += "		PV2.D2_CF AS CFOP2,																					   "
cQuery += "		PV2.F2_EMISSAO AS DTNF12,																			   "
cQuery += "		PV2.F2_VALBRUT AS VALOR_BRUTO,																		   "
cQuery += "		PV2.F2_FRETE AS FRETE_NF2,																			   "
cQuery += "     PV2.F2_DESCONT AS DESCONT_NF2,																		   "
cQuery += "	    PV2.ZZ2_CLNF02 AS CLIENT_NF2,																		   "
cQuery += "		PV2.A1_NOME AS NOME_CLI2,																			   "
cQuery += "		PV2.ZZ2_LJNF02 AS LOJA_NF2,																			   "
cQuery += "		PV2.F2_CHVNFE AS CHAVE_NF2,																			   "
cQuery += "		PV2.MSG_NF2,																						   "
cQuery += "		PV2.F2_TPFRETE AS TPFRETE_NF2,																		   "
cQuery += "		PV2.F2_DAUTNFE AS DTA_NF2,																			   "
cQuery += "		PV3.ZZ2_PV03FL AS FILIAL_NF3,																		   "
cQuery += "		PV3.ZZ2_PV03NF AS NF03,																				   "
cQuery += "		PV3.ZZ2_SER03 AS SERIE3,																			   "
cQuery += "		PV3.D2_CF AS CFOP3,																					   "
cQuery += "		PV3.F2_EMISSAO AS DTNF3,																			   "
cQuery += "		PV3.F2_VALBRUT AS VALOR_BRUTO,																		   "
cQuery += "		PV3.F2_FRETE AS FRETE_NF3,																			   "
cQuery += "		PV3.F2_DESCONT AS DESCONT_NF3,																		   "
cQuery += "		PV3.ZZ2_CLNF03 AS CLIENT_NF3,																		   "
cQuery += "		PV3.A1_NOME AS NOME_CLI3,																			   "
cQuery += "		PV3.ZZ2_LJNF03 AS LOJA_NF3,																			   "
cQuery += "		PV3.F2_CHVNFE AS CHAVE_NF3,																			   "
cQuery += "		PV3.MSG_NF3,																						   "
cQuery += "		PV3.F2_TPFRETE AS TPFRETE_NF3,																		   "
cQuery += "		PV3.F2_DAUTNFE AS DTA_NF3																			   "
cQuery += "																											   "
cQuery += "	FROM																									   "
cQuery += "	(																										   "
cQuery += "																											   "
cQuery += " SELECT ZZ2.ZZ2_PV01FL,																					   "
cQuery += "       CASE ZZ2_TIPCTR																					   "
cQuery += "           WHEN 1 THEN 'Direta'																			   "
cQuery += "           WHEN 2 THEN 'Triangular'																		   "
cQuery += "           ELSE 'Ambas'																					   "
cQuery += "       END ZZ2_TIPCTR,																					   "
cQuery += "       D2.D2_TES,																						   "
cQuery += "       F2.F2_DOC,																						   "
cQuery += "       ZZ2.ZZ2_PV01NF,																					   "
cQuery += "       ZZ2.ZZ2_SER01,																					   "
cQuery += "       D2.D2_CF,																							   "
cQuery += "       F2.F2_EMISSAO,																					   "
cQuery += "       F2.F2_VALBRUT,																					   "
cQuery += "       F2.F2_FRETE,																						   "
cQuery += "       F2.F2_DESCONT,																					   "
cQuery += "       ZZ0.ZZ0_FRTCLI,																					   "
cQuery += "       ZZ0.ZZ0_FRTEMP,																					   "
cQuery += "       ZZ2.ZZ2_NUMERO,																					   "
cQuery += "       ZZ2.ZZ2_MEDICA,																					   "
cQuery += "       ZZ2.ZZ2_CLNF01,																					   "
cQuery += "       A1.A1_NOME,																						   "
cQuery += "       F2.F2_CHVNFE,																						   "
cQuery += "       ZZ2.ZZ2_LJNF01,																					   "
cQuery += "       ISNULL(CAST(CAST([F2].[F2_YMSGNF] AS VARBINARY(1024)) AS VARCHAR(1024)),'') AS [MSG_NF1],			   "
cQuery += "       CASE F2.F2_TPFRETE																				   "
cQuery += "           WHEN 'C' THEN 'CIF/REMETENTE'																	   "
cQuery += "           WHEN 'F' THEN 'FOB/EMITENTE'																	   "
cQuery += "           WHEN 'T' THEN 'TERCEIROS'																		   "
cQuery += "           ELSE 'SEM FRETE'																				   "
cQuery += "       END AS F2_TPFRETE,																				   "
cQuery += "       F2.F2_DAUTNFE,																					   "
cQuery += "	   ZZ2.ZZ2_FILIAL																						   "
cQuery += "																											   "
cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2	"																		
cQuery += " JOIN "+RetSqlName("SF2")+" F2 "
cQuery += "	ON ZZ2.ZZ2_PV01FL = F2.F2_FILIAL																		   "
cQuery += "	AND ZZ2.ZZ2_CLNF01 = F2.F2_CLIENT																		   "
cQuery += "	AND ZZ2.ZZ2_LJNF01 = F2.F2_LOJA																			   "
cQuery += "	AND ZZ2.ZZ2_PV01NF = F2.F2_DOC																			   "
cQuery += "	AND F2.D_E_L_E_T_ = ''																					   "
cQuery += " JOIN "+RetSqlName("SD2")+" D2 " 
cQuery += "	ON D2.D2_DOC = F2.F2_DOC 																				   "
cQuery += "	AND D2.D2_SERIE = F2.F2_SERIE 																			   "
cQuery += "	AND D2.D2_FILIAL = F2.F2_FILIAL																			   "
cQuery += "	AND D2.D2_CLIENTE = F2.F2_CLIENT																		   "
cQuery += "	AND D2.D_E_L_E_T_ = '' 																					   "
cQuery += " JOIN "+RetSqlName("ZZ0")+" ZZ0 "
cQuery += "	ON ZZ0.ZZ0_FILIAL = ZZ2.ZZ2_FILIAL																		   "
cQuery += "	AND ZZ0.ZZ0_NUMERO = ZZ2.ZZ2_NUMERO																		   "
cQuery += "	AND ZZ0.ZZ0_CLIENT = ZZ2.ZZ2_CLIENT																		   "
cQuery += "	AND ZZ0.ZZ0_LOJA = ZZ2.ZZ2_LOJA																			   "
cQuery += "	AND ZZ0.D_E_L_E_T_ = ''																					   "
cQuery += " JOIN "+RetSqlName("SA1")+" A1 "
cQuery += "	ON A1.A1_COD = ZZ2.ZZ2_CLNF01																			   "
cQuery += "	AND A1.A1_LOJA = ZZ2.ZZ2_LJNF01																			   "
cQuery += "	AND A1.D_E_L_E_T_ = ''																					   "
cQuery += " WHERE ZZ2.D_E_L_E_T_ = ''																				   "
cQuery += "  AND F2_EMISSAO BETWEEN '"+DTOS(mv_par01)+"' AND '"+DTOS(mv_par02)+"' "
cQuery += "  AND F2_DOC BETWEEN '"+mv_par05+"' AND '"+mv_par06+"' "
cQuery += "  AND F2_SERIE BETWEEN '"+mv_par07+"' AND '"+mv_par08+"' "
cQuery += "  AND D2.D2_TES BETWEEN '"+mv_par09+"' AND '"+mv_par10+"' "
cQuery += "																											   "
cQuery += " GROUP BY ZZ2.ZZ2_PV01FL,																				   "
cQuery += "         ZZ2.ZZ2_TIPCTR,																					   "
cQuery += "         D2.D2_TES,																						   "
cQuery += "         ZZ2.ZZ2_PV01NF,																					   "
cQuery += "         ZZ2.ZZ2_SER01,																					   "
cQuery += "         F2.F2_EMISSAO,																					   "
cQuery += "         F2.F2_VALBRUT,																					   "
cQuery += "         F2.F2_FRETE,																					   "
cQuery += "         F2.F2_DESCONT,																					   "
cQuery += "         ZZ0.ZZ0_FRTCLI,																					   "
cQuery += "         ZZ0.ZZ0_FRTEMP,																					   "
cQuery += "         ZZ2.ZZ2_NUMERO,																					   "
cQuery += "         ZZ2.ZZ2_MEDICA,																					   "
cQuery += "         ZZ2.ZZ2_CLNF01,																					   "
cQuery += "         A1.A1_NOME,																						   "
cQuery += "         F2.F2_CHVNFE,																					   "
cQuery += "         ZZ2.ZZ2_LJNF01,																					   "
cQuery += "         CAST([F2_YMSGNF] AS VARBINARY(1024)),															   "
cQuery += "         F2.F2_TPFRETE,																					   "
cQuery += "         F2.F2_DAUTNFE,																					   "
cQuery += "		 D2.D2_CF,																							   "
cQuery += "		 F2.F2_DOC,																							   "
cQuery += "		 ZZ2.ZZ2_FILIAL																						   "
cQuery += "																											   "
cQuery += " ) PV1 																									   "
cQuery += " JOIN																									   "
cQuery += " (																										   "
cQuery += " SELECT ZZ2.ZZ2_PV02FL,																					   "
cQuery += "       CASE ZZ2_TIPCTR																					   "
cQuery += "           WHEN 1 THEN 'Direta'																			   "
cQuery += "           WHEN 2 THEN 'Triangular'																		   "
cQuery += "           ELSE 'Ambas'																					   "
cQuery += "       END ZZ2_TIPCTR,																					   "
cQuery += "       D2.D2_TES,																						   "
cQuery += "       F2.F2_DOC,																						   "
cQuery += "       ZZ2.ZZ2_PV02NF,																					   "
cQuery += "       ZZ2.ZZ2_SER02,																					   "
cQuery += "       D2.D2_CF,																							   "
cQuery += "       F2.F2_EMISSAO,																					   "
cQuery += "       F2.F2_VALBRUT,																					   "
cQuery += "       F2.F2_FRETE,																						   "
cQuery += "       F2.F2_DESCONT,																					   "
cQuery += "       ZZ0.ZZ0_FRTCLI,																					   "
cQuery += "       ZZ0.ZZ0_FRTEMP,																					   "
cQuery += "       ZZ2.ZZ2_NUMERO,																					   "
cQuery += "       ZZ2.ZZ2_MEDICA,																					   "
cQuery += "       ZZ2.ZZ2_CLNF02,																					   "
cQuery += "       A1.A1_NOME,																						   "
cQuery += "       F2.F2_CHVNFE,																						   "
cQuery += "       ZZ2.ZZ2_LJNF02,																					   "
cQuery += "       ISNULL(CAST(CAST([F2].[F2_YMSGNF] AS VARBINARY(1024)) AS VARCHAR(1024)),'') AS [MSG_NF2],			   "
cQuery += "       CASE F2.F2_TPFRETE																				   "
cQuery += "           WHEN 'C' THEN 'CIF/REMETENTE'																	   "
cQuery += "           WHEN 'F' THEN 'FOB/EMITENTE'																	   "
cQuery += "           WHEN 'T' THEN 'TERCEIROS'																		   "
cQuery += "           ELSE 'SEM FRETE'																				   "
cQuery += "       END AS F2_TPFRETE,																				   "
cQuery += "       F2.F2_DAUTNFE,																					   "
cQuery += "	   ZZ2.ZZ2_FILIAL																						   "
cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2	"
cQuery += " JOIN "+RetSqlName("SF2")+" F2 "
cQuery += "	ON ZZ2.ZZ2_PV02FL = F2.F2_FILIAL																		   "
cQuery += "	AND ZZ2.ZZ2_CLNF02 = F2.F2_CLIENT																		   "
cQuery += "	AND ZZ2.ZZ2_LJNF02 = F2.F2_LOJA																			   "
cQuery += "	AND ZZ2.ZZ2_PV02NF = F2.F2_DOC																			   "
cQuery += "	AND F2.D_E_L_E_T_ = ''																					   "
cQuery += " JOIN "+RetSqlName("SD2")+" D2 " 
cQuery += "	ON D2.D2_DOC = F2.F2_DOC 																				   "
cQuery += "	AND D2.D2_SERIE = F2.F2_SERIE 																			   "
cQuery += "	AND D2.D2_FILIAL = F2.F2_FILIAL																			   "
cQuery += "	AND D2.D2_CLIENTE = F2.F2_CLIENT																		   "
cQuery += "	AND D2.D_E_L_E_T_ = '' 																					   "
cQuery += " JOIN "+RetSqlName("ZZ0")+" ZZ0 "
cQuery += "	ON ZZ0.ZZ0_FILIAL = ZZ2.ZZ2_FILIAL																		   "
cQuery += "	AND ZZ0.ZZ0_NUMERO = ZZ2.ZZ2_NUMERO																		   "
cQuery += "	AND ZZ0.ZZ0_CLIENT = ZZ2.ZZ2_CLIENT																		   "
cQuery += "	AND ZZ0.ZZ0_LOJA = ZZ2.ZZ2_LOJA																			   "
cQuery += "	AND ZZ0.D_E_L_E_T_ = ''																					   "
cQuery += " JOIN "+RetSqlName("SA1")+" A1 "
cQuery += "	ON A1.A1_COD = ZZ2.ZZ2_CLNF02																			   "
cQuery += "	AND A1.A1_LOJA = ZZ2.ZZ2_LJNF02																			   "
cQuery += "	AND A1.D_E_L_E_T_ = ''																					   "
cQuery += "																											   "
cQuery += " WHERE ZZ2.D_E_L_E_T_ = ''																				   "
cQuery += "  AND F2_EMISSAO BETWEEN '"+DTOS(mv_par01)+"' AND '"+DTOS(mv_par02)+"' "
cQuery += "  AND F2_DOC BETWEEN '"+mv_par05+"' AND '"+mv_par06+"' "
cQuery += "  AND F2_SERIE BETWEEN '"+mv_par07+"' AND '"+mv_par08+"' "
cQuery += "  AND D2.D2_TES BETWEEN '"+mv_par09+"' AND '"+mv_par10+"' "
cQuery += "																											   "
cQuery += " GROUP BY ZZ2.ZZ2_PV02FL,																				   "
cQuery += "         ZZ2.ZZ2_TIPCTR,																					   "
cQuery += "         D2.D2_TES,																						   "
cQuery += "         ZZ2.ZZ2_PV02NF,																					   "
cQuery += "         ZZ2.ZZ2_SER02,																					   "
cQuery += "         F2.F2_EMISSAO,																					   "
cQuery += "         F2.F2_VALBRUT,																					   "
cQuery += "         F2.F2_FRETE,																					   "
cQuery += "         F2.F2_DESCONT,																					   "
cQuery += "         ZZ0.ZZ0_FRTCLI,																					   "
cQuery += "         ZZ0.ZZ0_FRTEMP,																					   "
cQuery += "         ZZ2.ZZ2_NUMERO,																					   "
cQuery += "         ZZ2.ZZ2_MEDICA,																					   "
cQuery += "         ZZ2.ZZ2_CLNF02,																					   "
cQuery += "         A1.A1_NOME,																						   "
cQuery += "         F2.F2_CHVNFE,																					   "
cQuery += "         ZZ2.ZZ2_LJNF02,																					   "
cQuery += "         CAST([F2_YMSGNF] AS VARBINARY(1024)),															   "
cQuery += "         F2.F2_TPFRETE,																					   "
cQuery += "         F2.F2_DAUTNFE,																					   "
cQuery += "		 D2.D2_CF,																							   "
cQuery += "		 F2.F2_DOC,																							   "
cQuery += "		 ZZ2.ZZ2_FILIAL																						   "
cQuery += " 																										   "
cQuery += " ) PV2																									   "
cQuery += " ON(PV1.ZZ2_NUMERO = PV2.ZZ2_NUMERO AND PV1.ZZ2_MEDICA = PV2.ZZ2_MEDICA AND PV1.ZZ2_FILIAL = PV2.ZZ2_FILIAL) "
cQuery += " JOIN 																									   "
cQuery += " (																										   "
cQuery += " 																										   "
cQuery += " SELECT ZZ2.ZZ2_PV03FL,																					   "
cQuery += "       CASE ZZ2_TIPCTR																					   "
cQuery += "           WHEN 1 THEN 'Direta'																			   "
cQuery += "           WHEN 2 THEN 'Triangular'																		   "
cQuery += "           ELSE 'Ambas'																					   "
cQuery += "       END ZZ2_TIPCTR,																					   "
cQuery += "       D2.D2_TES,																						   "
cQuery += "       F2.F2_DOC,																						   "
cQuery += "       ZZ2.ZZ2_PV03NF,																					   "
cQuery += "       ZZ2.ZZ2_SER03,																					   "
cQuery += "       D2.D2_CF,																							   "
cQuery += "       F2.F2_EMISSAO,																					   "
cQuery += "       F2.F2_VALBRUT,																					   "
cQuery += "       F2.F2_FRETE,																						   "
cQuery += "       F2.F2_DESCONT,																					   "
cQuery += "       ZZ0.ZZ0_FRTCLI,																					   "
cQuery += "       ZZ0.ZZ0_FRTEMP,																					   "
cQuery += "       ZZ2.ZZ2_NUMERO,																					   "
cQuery += "       ZZ2.ZZ2_MEDICA,																					   "
cQuery += "       ZZ2.ZZ2_CLNF03,																					   "
cQuery += "       A1.A1_NOME,																						   "
cQuery += "       F2.F2_CHVNFE,																						   "
cQuery += "       ZZ2.ZZ2_LJNF03,																					   "
cQuery += "       ISNULL(CAST(CAST([F2].[F2_YMSGNF] AS VARBINARY(1024)) AS VARCHAR(1024)),'') AS [MSG_NF3],			   "
cQuery += "       CASE F2.F2_TPFRETE																				   "
cQuery += "           WHEN 'C' THEN 'CIF/REMETENTE'																	   "
cQuery += "           WHEN 'F' THEN 'FOB/EMITENTE'																	   "
cQuery += "           WHEN 'T' THEN 'TERCEIROS'																		   "
cQuery += "           ELSE 'SEM FRETE'																				   "
cQuery += "       END AS F2_TPFRETE,																				   "
cQuery += "       F2.F2_DAUTNFE,																					   "
cQuery += "	   ZZ2.ZZ2_FILIAL																						   "
cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2	"
cQuery += " JOIN "+RetSqlName("SF2")+" F2 "
cQuery += "	ON ZZ2.ZZ2_PV03FL = F2.F2_FILIAL																		   "
cQuery += "	AND ZZ2.ZZ2_CLNF03 = F2.F2_CLIENT																		   "
cQuery += "	AND ZZ2.ZZ2_LJNF03 = F2.F2_LOJA																			   "
cQuery += "	AND ZZ2.ZZ2_PV03NF = F2.F2_DOC																			   "
cQuery += "	AND F2.D_E_L_E_T_ = ''																					   "
cQuery += " JOIN "+RetSqlName("SD2")+" D2 " 
cQuery += "	ON D2.D2_DOC = F2.F2_DOC 																				   "
cQuery += "	AND D2.D2_SERIE = F2.F2_SERIE 																			   "
cQuery += "	AND D2.D2_FILIAL = F2.F2_FILIAL																			   "
cQuery += "	AND D2.D2_CLIENTE = F2.F2_CLIENT																		   "
cQuery += "	AND D2.D_E_L_E_T_ = '' 																					   "
cQuery += " JOIN "+RetSqlName("ZZ0")+" ZZ0 "
cQuery += "	ON ZZ0.ZZ0_FILIAL = ZZ2.ZZ2_FILIAL																		   "
cQuery += "	AND ZZ0.ZZ0_NUMERO = ZZ2.ZZ2_NUMERO																		   "
cQuery += "	AND ZZ0.ZZ0_CLIENT = ZZ2.ZZ2_CLIENT																		   "
cQuery += "	AND ZZ0.ZZ0_LOJA = ZZ2.ZZ2_LOJA																			   "
cQuery += "	AND ZZ0.D_E_L_E_T_ = ''																					   "
cQuery += " JOIN "+RetSqlName("SA1")+" A1 "
cQuery += "	ON A1.A1_COD = ZZ2.ZZ2_CLNF03																			   "
cQuery += "	AND A1.A1_LOJA = ZZ2.ZZ2_LJNF03																			   "
cQuery += "	AND A1.D_E_L_E_T_ = ''																					   "
cQuery += " WHERE ZZ2.D_E_L_E_T_ = ''																				   "
cQuery += "  AND F2_EMISSAO BETWEEN '"+DTOS(mv_par01)+"' AND '"+DTOS(mv_par02)+"' "
cQuery += "  AND F2_DOC BETWEEN '"+mv_par05+"' AND '"+mv_par06+"' "
cQuery += "  AND F2_SERIE BETWEEN '"+mv_par07+"' AND '"+mv_par08+"' "
cQuery += "  AND D2.D2_TES BETWEEN '"+mv_par09+"' AND '"+mv_par10+"' "
cQuery += " GROUP BY ZZ2.ZZ2_PV03FL,																				   "
cQuery += "         ZZ2.ZZ2_TIPCTR,																					   "
cQuery += "         D2.D2_TES,																						   "
cQuery += "         ZZ2.ZZ2_PV03NF,																					   "
cQuery += "         ZZ2.ZZ2_SER03,																					   "
cQuery += "         F2.F2_EMISSAO,																					   "
cQuery += "         F2.F2_VALBRUT,																					   "
cQuery += "         F2.F2_FRETE,																					   "
cQuery += "         F2.F2_DESCONT,																					   "
cQuery += "         ZZ0.ZZ0_FRTCLI,																					   "
cQuery += "         ZZ0.ZZ0_FRTEMP,																					   "
cQuery += "         ZZ2.ZZ2_NUMERO,																					   "
cQuery += "         ZZ2.ZZ2_MEDICA,																					   "
cQuery += "         ZZ2.ZZ2_CLNF03,																					   "
cQuery += "         A1.A1_NOME,																						   "
cQuery += "         F2.F2_CHVNFE,																					   "
cQuery += "         ZZ2.ZZ2_LJNF03,																					   "
cQuery += "         CAST([F2_YMSGNF] AS VARBINARY(1024)),															   "
cQuery += "         F2.F2_TPFRETE,																					   "
cQuery += "         F2.F2_DAUTNFE,																					   "
cQuery += "		 D2.D2_CF,																							   "
cQuery += "		 F2.F2_DOC,																							   "
cQuery += "		 ZZ2.ZZ2_FILIAL																						   "
cQuery += " ) PV3 																									   "
cQuery += " ON(PV2.ZZ2_NUMERO = PV3.ZZ2_NUMERO AND PV2.ZZ2_MEDICA = PV3.ZZ2_MEDICA AND PV2.ZZ2_FILIAL = PV3.ZZ2_FILIAL)"



	TcQuery cQuery New Alias T01
  
  
	oReport:SetMeter(T01->(LastRec()))
  
  
	while !T01->(EOF())
		if oReport:Cancel()
			exit
		endif
	
	
		oSection1:Init()

		oReport:IncMeter()
			
		cFil 	:= T01->ZZ2_FILIAL
			//imprimo a primeira se��o	
		oSection1:Cell("ZZ2_FILIAL"):SetValue(T01->ZZ2_FILIAL)
		//oSection1:Cell("Filial"):SetValue(T01->E5_FILIAL)
		oSection1:Printline()
		
		//inicializo a segunda se��o
		oSection2:init()
		
		
		While T01->ZZ2_FILIAL == cFil
			oReport:IncMeter()
		
			oSection2:Cell( "ZZ2_TIPCTR"):SetValue(T01->ZZ2_TIPCTR)
			oSection2:Cell( "ZZ2_TIPCTR"):Setalign( 'center')//E2_NUMBOR
			oSection2:Cell( "ZZ3_TES"):SetValue( T01->ZZ3_TES)
			oSection2:Cell( "ZZ3_TES"):Setalign( 'center')
			oSection2:Cell( "FT_CFOP"):SetValue( T01->FT_CFOP )
			oSection2:Cell( "FT_CFOP"):Setalign( 'center')
			oSection2:Cell( "ZZ3_FILIAL"):SetValue(T01->ZZ2_FILIAL)
			oSection2:Cell( "ZZ3_FILIAL"):Setalign( 'center')
			oSection2:Cell( "F2_DOC"):SetValue(T01->F2_DOC)
			oSection2:Cell( "F2_DOC"):Setalign( 'center')
			oSection2:Cell( "F2_EMISSAO"):SetValue( STOD(T01->F2_EMISSAO))
			oSection2:Cell( "F2_EMISSAO"):Setalign( 'center')
			oSection2:Cell( "F2_VALBRUT"):SetValue( T01->F2_VALBRUT	)
			oSection2:Cell( "F2_VALBRUT"):Setalign( 'center')
			oSection2:Cell( "F2_FRETE"):SetValue(T01->F2_FRETE)
			oSection2:Cell( "F2_FRETE"):Setalign( 'center')//F2_TPFRETE
			oSection2:Cell( "F2_TPFRETE"):SetValue(T01->F2_TPFRETE)
			oSection2:Cell( "F2_TPFRETE"):Setalign( 'center')
			oSection2:Cell( "F2_DESCONT"):SetValue(T01->F2_DESCONT)
			oSection2:Cell( "F2_DESCONT"):Setalign( 'center')
			oSection2:Cell( "F2_MSG"):SetValue( T01->F2_YMSGNF)
			oSection2:Cell( "F2_MSG"):Setalign( 'center')
			oSection2:Cell( "ZZ2_CLIENT"):SetValue(T01->ZZ2_CLIENT)
			oSection2:Cell( "ZZ2_CLIENT"):Setalign( 'center')
			oSection2:Cell( "ZZ2_LOJA"):SetValue( T01->ZZ2_LOJA)
			oSection2:Cell( "ZZ2_LOJA"):Setalign( 'center')
			oSection2:Cell( "ZZ2_NOME"):SetValue(T01->A1_NOME)
			oSection2:Cell( "ZZ2_NOME"):Setalign( 'center')
			oSection2:Cell( "ZZ2_LOCAL"):SetValue( POSICIONE("SD2", 3, T01->ZZ2_FILIAL + T01->F2_DOC + T01->F2_SERIE + T01->ZZ2_CLIENT + T01->ZZ2_LOJA, "D2_LOCAL")  )
			oSection2:Cell( "ZZ2_LOCAL"):Setalign( 'center')
			oSection2:Cell( "F2_CHVNFE"):SetValue( { || T01->F2_CHVNFE	})
			oSection2:Cell( "F2_CHVNFE"):Setalign( 'center')
			oSection2:Cell( "F2_DAUTNFE"):SetValue( { || STOD(T01->F2_DAUTNFE)	})
			oSection2:Cell( "F2_DAUTNFE"):Setalign( 'center')
		
			oSection2:Printline()
				
		
		//oSection:PrintLine()
			T01->(DBSKIP())
		
		EndDo
	
		oSection2:Finish()
 		//imprimo uma linha para separar uma NCM de outra
		oReport:ThinLine()
 		//finalizo a primeira se��o
		oSection1:Finish()
	
	EndDo

RETURN


Static Function AjustaSX1(cPerg)

	PutSx1(cPerg, "01","Data De"	,						  "","","mv_ch01","D",08,0,0,"G","","","","","mv_par01","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Data Ate",						  "","","mv_ch02","D",08,0,0,"G","","","","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","Armazem de:",				  		  "","","mv_ch03","C",02,0,0,"G","","","","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "04","Armazem ate:",				      "","","mv_ch04","C",02,0,0,"G","","","","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "05","NF de:",						  "","","mv_ch05","C",09,0,0,"G","","SF2","","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "06","NF ate:",					  "","","mv_ch06","C",09,0,0,"G","","SF2","","","mv_par06"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "07","Serie de:",						  "","","mv_ch07","C",03,0,0,"G","","","","","mv_par07"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "08","Serie ate:",					  "","","mv_ch08","C",03,0,0,"G","","","","","mv_par08"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "09","Tes de:",						  "","","mv_ch09","C",03,0,0,"G","","SF4","","","mv_par09"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "10","Tes ate:",					  "","","mv_ch10","C",03,0,0,"G","","SF4","","","mv_par10"," ","","","","","","","","","","","","","","","")
	
return
//--< fim de arquivo >----------------------------------------------------------------------
