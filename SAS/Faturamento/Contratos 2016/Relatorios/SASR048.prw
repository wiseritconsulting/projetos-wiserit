#include 'protheus.ch'
#include 'parmtype.ch'

user function SASR048()

    Private oReport
	//Private cPergCont	:= 'SASR048' 

	************************
	*Monta pergunte do Log *
	************************
	//AjustaSX1(cPergCont)

	//If Pergunte( cPergCont, .T. )    
		Processa( { || ProcCont() }, 'Processando....' )
	//EndIf
    
	
Return(Nil)

Static Function ProcCont()
	Local cQuery	:= ""
	Local aPagto    := {}

	BeginSql Alias "TMP1"

	SELECT B1_COD AS CODIGO,
		   B1_DESC AS DESCRICAO,
		   DATA
    FROM AUDIT_PRODUTO

	EndSql

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()

	TMP1->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 08 de Abril de 2016
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportDef()
	Local oFirst
	Local nOrd	:= 1


	oReport := TReport():New( 'TMP1', 'AUDITORIA DE PRODUTOS', , {|oReport| ReportPrint( oReport ), 'AUDITORIA DE PRODUTOS' } )
	oReport:SetLandScape()
	oReport:lParamReadOnly := .T.

	oReport:SetTotalInLine( .F. )

	oFirst := TRSection():New( oReport, 'AUDITORIA DE PRODUTOS', { 'TMP1', 'SB1'},,, )

	oFirst:SetTotalInLine( .F. )


	TRCell():New( oFirst, 'CODIGO'		    ,'TMP1', 'CODIGO',				"@!"						    ,15,,							{ || TMP1->CODIGO 			                  } )
	TRCell():New( oFirst, 'DESCRICAO'	    ,'TMP1', 'DESCRICAO',			"@!"						    ,50,,							{ || TMP1->DESCRICAO		     	          } )
	TRCell():New( oFirst, 'DATA'	   	    ,'TMP1', 'DATA' ,		     	"@!"		                    ,15,,							{ || TMP1->DATA 	   		                  } )

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author Weskley Silva
@since 08 de Abril de 2016
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	//MakeAdvplExpr( cPergCont )
	oReport:Section(1):Enable()

	dbSelectArea( 'TMP1')
	oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 08 de Abril de 2016
@version P11
/*/
//_____________________________________________________________________________

/*Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Cliente de"			,""		,""		,"mv_ch1","C",06,0,0,"G",""	,"SA1"	,"","","MV_PAR01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Cliente ate"		,""		,""		,"mv_ch2","C",06,0,0,"G",""	,"SA1"	,"","","MV_PAR02"," ","","","","","","","","","","","","","","","")
	
Return
*/