#Include "Protheus.ch"
#Include "TopConn.ch"

/*/{Protheus.doc} SASR041

Relat�rio diverg�ncia pedido

@author Diogo Costa
@since 17/03/2016
@version 1.0
@example
(examples)
@see (links_or_references)

/*/

User Function SASR041()
	Local oReport

	oReport:=ReportDef()
	oReport:PrintDialog()

Return

/*/{Protheus.doc} ReportDef
Defini��o das Colunas do Relat�rio
@author Diogo
@since 17/03/2016
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Static Function ReportDef()

	Local cHelp  := "Relatorio Kits"
	Local aOrdem := {}
	Local cPerg  := "SASR041"
	Local oReport
	Local oSection1
	Local oSection2

	AjustaSX1(cPerg)
	Pergunte(cPerg,.T.)


	oReport := TReport():New("SASR041","Relat�rio Kits ",cPerg,{|oReport| ReportPrint(oReport,aOrdem)},cHelp)
	oReport:nfontbody:=9
	oReport:SetLandscape()

	oSection1:= TRSection():New(oReport,OemToAnsi("Pedido"),{"SC6"})
	TrCell():New(oSection1,"C6_ITEM"   	," ",GetSX3Cache("C6_ITEM","X3_TITULO")		,,GetSX3Cache("C6_ITEM","X3_TAMANHO"))
	TrCell():New(oSection1,"C6_PRODUTO" ," ",GetSX3Cache("C6_PRODUTO","X3_TITULO")	,,GetSX3Cache("C6_PRODUTO","X3_TAMANHO"))
	TrCell():New(oSection1,"C6_DESCRI" 	," ",GetSX3Cache("C6_DESCRI","X3_TITULO")	,,GetSX3Cache("C6_DESCRI","X3_TAMANHO"))
	TrCell():New(oSection1,"C6_YITCONT" ," ",GetSX3Cache("C6_YITCONT","X3_TITULO")	,,GetSX3Cache("C6_YITCONT","X3_TAMANHO"))
	TrCell():New(oSection1,"C6_YCODKIT" ," ",GetSX3Cache("C6_YCODKIT","X3_TITULO")	,,GetSX3Cache("C6_YCODKIT","X3_TAMANHO")+6)
	TrCell():New(oSection1,"C6_YPAIKIT"	," ",GetSX3Cache("C6_YPAIKIT","X3_TITULO")	,,GetSX3Cache("C6_YPAIKIT","X3_TAMANHO")+6)
	TrCell():New(oSection1,"C6_QTDVEN" 	," ",GetSX3Cache("C6_QTDVEN","X3_TITULO")  	,GetSX3Cache("C6_QTDVEN","X3_PICTURE")	,GetSX3Cache("C6_QTDVEN","X3_TAMANHO"))
	TrCell():New(oSection1,"C6_PRCVEN" 	," ",GetSX3Cache("C6_PRCVEN","X3_TITULO")  	,GetSX3Cache("C6_PRCVEN","X3_PICTURE") 	,GetSX3Cache("C6_PRCVEN","X3_TAMANHO"))
	TrCell():New(oSection1,"C6_VALOR" 	," ",GetSX3Cache("C6_VALOR","X3_TITULO")	,GetSX3Cache("C6_VALOR","X3_PICTURE")	,GetSX3Cache("C6_VALOR","X3_TAMANHO") )
	
	oSection1:SetEdit(.F.)

	oSection2:= TRSection():New(oReport,OemToAnsi("Kits"),{"ZZ7"})

	TrCell():New(oSection2,"ZZ7_CODPAI"," ",GetSX3Cache("ZZ7_CODPAI","X3_TITULO"),,GetSX3Cache("ZZ7_CODPAI","X3_TAMANHO"))
	TrCell():New(oSection2,"ZZ7_DESCR"," ",GetSX3Cache("ZZ7_DESCR","X3_TITULO"),,GetSX3Cache("ZZ7_DESCR","X3_TAMANHO"))

Return(oReport)


Static Function ReportPrint(oReport,aOrdem)

	Local oSection1  := oReport:Section(1)
	Local oSection2  := oReport:Section(2)
	Local nOrdem     := oSection1:GetOrder()
	Local cQuery     := ''
	Local cMotaju    := ''
	Local cNome      := ''

	oReport:SetTitle(oReport:Title())

	oSection1:BeginQuery() 

	//Seleciona dados do Pedido conforme par�metros
	cQuery := " SELECT  C6_ITEM,C6_PRODUTO,C6_DESCRI,C6_YCODKIT,C6_YITCONT,"
	cQuery += " C6_YPAIKIT,C6_QTDVEN,C6_PRCVEN,C6_VALOR,C6_YCONTRA,C6_YMEDICA " 
	cQuery += " FROM "+RetSqlName("SC6")+" SC6 	"
	cQuery += " WHERE C6_NUM = '"+mv_par02+"'	"
	cQuery += "  AND C6_FILIAL = '"+mv_par01+"'	"
	cQuery += " AND SC6.D_E_L_E_T_ = ' '		"
	cQuery += " ORDER BY C6_PRODUTO				"
	

	If SELECT("T1")>0
		T1->(dbCloseArea())
	Endif
	
	TCQUERY cQuery NEW ALIAS T1

	If T1->(Eof())
		Alert("Nenhum Registro localizado!!")
		T1->(DbCloseArea())
		Return
	EndIf

	oSection1:EndQuery()
	oReport:SetMeter(T1->(Reccount()))
	
	aDados	:=	FDadosKits()
	
	
	
	DbSelectArea("T1")

	While T1->(!Eof())

		If oReport:Cancel()
			Exit
		EndIf
		
		oSection1:Init()
		
		oSection1:Cell("C6_ITEM"):SetValue(T1->C6_ITEM)
		oSection1:Cell("C6_PRODUTO"):SetValue(T1->C6_PRODUTO)
		oSection1:Cell("C6_DESCRI"):SetValue(T1->C6_DESCRI)
		oSection1:Cell("C6_YITCONT"):SetValue(T1->C6_YITCONT)
		oSection1:Cell("C6_YCODKIT"):SetValue(T1->C6_YCODKIT)
		oSection1:Cell("C6_YPAIKIT"):SetValue(T1->C6_YPAIKIT)
		oSection1:Cell("C6_QTDVEN"):SetValue(T1->C6_QTDVEN)
		oSection1:Cell("C6_PRCVEN"):SetValue(T1->C6_PRCVEN)
		oSection1:Cell("C6_VALOR"):SetValue(T1->C6_VALOR)
		
		oSection1:PrintLine()
			
		oSection2:Init()
		For i:=1 To Len(aDados)
			If aLLTRIM(aDados[i][3]) == aLLTRIM(T1->C6_PRODUTO)
				oSection2:Cell("ZZ7_CODPAI"):SetValue(aDados[i][1])
				oSection2:Cell("ZZ7_DESCR"):SetValue(aDados[i][2])
				oSection2:PrintLine()
			Endif
		Next
		oSection2:Finish()
		
		oSection1:Finish()
		
		oReport:SkipLine()
		oReport:SkipLine()
		oReport:SkipLine()
		oReport:SkipLine()
				
		T1->(Dbskip())
	EndDo
	
	
Return


//|----------------------------------------------------------|//
//|Funcao: Criacao das Perguntas							 |//
//|----------------------------------------------------------|//
Static Function AjustaSX1(cPerg)

	PutSx1(cPerg, "01","Filial "		 ,"Filial "		    ,"Filial "		    ,"mv_ch01","C",GetSx3Cache("C6_FILIAL","X3_TAMANHO"),0,0,"G",""		,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Pedido "		 ,"Pedido "		    ,"Pedido "		    ,"mv_ch02","C",GetSx3Cache("C5_NUM","X3_TAMANHO")	,0,0,"G",""		,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")

Return()

/*/{Protheus.doc} FDadosKits
Retorna array com os kits/produtos da Medi��o
@author Diogo
@since 17/03/2016
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Static Function FDadosKits
	Local aRet:= {}
	
	//Seleciona a medi��o referente ao Pedido
	cQuery := " SELECT ZZ3_PRODUT  "
	cQuery += " FROM "+RetSqlName("ZZ3")+" ZZ3		"
	cQuery += " WHERE ZZ3_NUMERO = '"+T1->C6_YCONTRA+"'	"
	cQuery += "  AND ZZ3_MEDICA = '"+T1->C6_YMEDICA+"'	"
	cQuery += "  AND ZZ3_FILIAL = '"+mv_par01+"'	"
	cQuery += " AND ZZ3.D_E_L_E_T_ = ' '			"
	cQuery += " AND ZZ3_TIPO = 'K' 					"

	TCQUERY cQuery NEW ALIAS T2
	
	While T2->(!eof())
		
		//Busca itens do Kit referente a medi��o
		cQuery := " SELECT ZZ7_CODPAI,ZZ7_DESCR,ZZ7_PRODUT "
		cQuery += " FROM "+RetSqlName("ZZ7")+" ZZ7 "
		cQuery += " WHERE ZZ7_CODPAI = '"+T2->ZZ3_PRODUT+"' "
		cQuery += " AND ZZ7.D_E_L_E_T_ =' '  "
		cQuery += " AND ZZ7_TIPO = 'A' "
		
		TCQUERY cQuery NEW ALIAS T3
		
			while T3->(!eof())
				
				aadd(aRet,{ T3->ZZ7_CODPAI 	,;
							T3->ZZ7_DESCR 	,;
							T3->ZZ7_PRODUT 	;
							};
					)
				
				T3->(dbSkip())
			enddo
		
		T3->(dbCloseArea())
		
		dbSelectArea("T2")
		T2->(dbSkip())
	Enddo
	
	T2->(dbCloseArea())

Return aRet