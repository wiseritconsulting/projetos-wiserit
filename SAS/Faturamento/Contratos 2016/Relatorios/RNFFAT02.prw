#include 'protheus.ch'
#include 'parmtype.ch'
#include 'TOPCONN.ch'

user function RNFFAT02()

	// AUTO :> JOAO FILHO  (TOTVS)
	// DATA :> 11/03/16
	// A��O :> O RELAT�RIO VAI LISTAR CONFORME PARAMETROS DOS USU�RIOS AS NOTAS QUE APRESENTAM DIVERGENCIA 
	//         DE VALOR, PODENDO ASSIM FAZER UM MONITORAMENTO DAS MESMAS 

	Local oReport

	oReport:=ReportDef()
	oReport:PrintDialog()

RETURN

Static Function ReportDef()
	Local oReport
	Local oSection
	Local cPerg     :="RNFFAT02"

	AjustaSX1(cPerg)
	Pergunte(cPerg,.F.)

	oReport := TReport():New("RNFFAT02","Relat�rio Ajustes de Notas Fiscais","RNFFAT02", {|oReport| ReportPrint(oReport)},"Relat�rio Ajustes de Notas Fiscais")
	// oReport:SetLandscape() //Paisagem

	//������������������������������������������������������������������������Ŀ
	//� Section 1			                                                  �
	//��������������������������������������������������������������������������
	oSection:= TRSection():New(oReport,OemToAnsi("Rela��o de Medi��es"),{"ZZ2","ZZ3","SC6"})
	TRCell():New(oSection,"ZZ2_FILIAL"		," ","Filial"              					      ,,06)
	TRCell():New(oSection,"ZZ2_NUMERO"		," ","Contrato"              				      ,,06)
	TRCell():New(oSection,"ZZ2_MEDICA"		," ","Medi��o"              				      ,,06)
	TRCell():New(oSection,"ZZ2_CLIENT"		," ","Cli Med"                       		      ,,06)
	TRCell():New(oSection,"ValMed"		    ," ","Valor Medi��o "               		      ,,14)

	if mv_par03 = 1
		TRCell():New(oSection,"ZZ2_PV01FL"		," ","Fil NF1 "                  		      ,,06)
		TRCell():New(oSection,"ZZ2_CLNF01"		," ","Cliente NF1 "                  	  	  ,,06)
		TRCell():New(oSection,"ZZ2_PV01NF"		," ","NF1 "                  	        	  ,,09)
		TRCell():New(oSection,"ZZ2_SER01"		," ","Serie NF1 "              	        	  ,,02)
		TRCell():New(oSection,"ValNF1E"		    ," ","Val NF1 "              	        	  ,,14)
		TRCell():New(oSection,"ValNF1C"		    ," ","Val Ok "               	        	  ,,14)
		TRCell():New(oSection,"Diverg1"		    ," ","Diferen�a "           	        	  ,,14)

		TRCell():New(oSection,"ZZ2_PV03FL"		," ","Fil NF3 "                  		  ,,06)
		TRCell():New(oSection,"ZZ2_CLNF03"		," ","Cliente NF3 "                  		  ,,06)
		TRCell():New(oSection,"ZZ2_PV03NF"		," ","NF3 "                  	        	  ,,09)
		TRCell():New(oSection,"ZZ2_SER03"		," ","Serie NF3 "              	        	  ,,02)
		TRCell():New(oSection,"ValNF3E"		    ," ","Val NF3 "              	        	  ,,14)
		TRCell():New(oSection,"ValNF3C"		    ," ","Val OK "              	        	  ,,14)
		TRCell():New(oSection,"Diverg3"		    ," ","Diferen�a "           	        	  ,,14)

	else
		TRCell():New(oSection,"ZZ2_PV01FL"		," ","Fil Ped 01 "                  		  ,,06)
		TRCell():New(oSection,"ZZ2_CLNF01"		," ","Cliente Ped 01 "                 		  ,,06)
		TRCell():New(oSection,"ZZ2_PV01"		," ","Pedido 01"              	        	  ,,06)
		TRCell():New(oSection,"ValPD1E"		    ," ","Valor Ped 01 "              	          ,,14)
		TRCell():New(oSection,"ValPD1C"		    ," ","Valor OK "      	                      ,,14)
		TRCell():New(oSection,"Diverg1"		    ," ","Diferen�a "           	        	  ,,14)

		TRCell():New(oSection,"ZZ2_PV03FL"		," ","Fil Ped 03 "                  		  ,,06)
		TRCell():New(oSection,"ZZ2_CLNF03"		," ","Cliente Ped 03 "                  	  ,,06)
		TRCell():New(oSection,"ZZ2_PV03"		," ","Pedido 03"              	        	  ,,06)
		TRCell():New(oSection,"ValPD3E"	    	," ","Val Ped 03 "              	          ,,14)
		TRCell():New(oSection,"ValPD3C"		    ," ","Val Ped 03 OK "           	          ,,14)
		TRCell():New(oSection,"Diverg3"		    ," ","Diferen�a "           	        	  ,,14)
	endif


return(oReport)

Static Function ReportPrint(oReport)

	Local oSection   := oReport:Section(1)
	Local nValMed    
	Local cFilMed 
	Local cNumMed 
	Local cMedica  
	Local cCLIENT  
	Local cLojCli  
	Local cPV01FL  
	Local cCLNF01  
	Local cPV01NF  
	Local cSER01   
	Local nValNf1  
	Local nValOK1  
	Local nDiverg1 
	Local cPV03FL  
	Local cCLNF03  
	Local cPV03NF  
	Local cSER03   
	Local nValNf3  
	Local nValOK3  
	Local nDiverg3 
	Local aPrint   := {}

	oReport:SetTitle( "Relat�rio Ajustes de Notas Fiscais.")

	cQuery:=" SELECT  DISTINCT ZZ3_FILIAL , ZZ3_NUMERO , ZZ3_MEDICA , ZZ3_CLIENT , ZZ3_LOJA  FROM ( "
	cQuery+=" SELECT  "
	cQuery+=" ZZ2_MEDICA, "
	cQuery+=" ZZ2_DTGER, "
	cQuery+=" ZZ2_HRGER, "
	cQuery+=" ZZ3_PRODUT KIT, "
	cQuery+=" C6.C6_PRODUTO PRODUTO, "
	cQuery+=" ZZ3_VLUNIT, "
	cQuery+=" (SELECT COUNT(*) FROM "+ RetSqlName("ZZ7") + " Z71 " 
	cQuery+=" WHERE Z71.D_E_L_E_T_ =' ' "
	cQuery+=" AND Z71.ZZ7_CODPAI = Z7.ZZ7_CODPAI "
	cQuery+=" AND Z71.ZZ7_FILIAL = Z7.ZZ7_FILIAL "
	cQuery+=" AND C6_YCODKIT = Z71.ZZ7_CODPAI "
	cQuery+=" AND Z71.ZZ7_CATEGO IN ('A','C','D','V') "		
	cQuery+=" AND Z71.ZZ7_SUPLEM = 'N' "
	cQuery+=" AND Z71.ZZ7_TIPO = 'A' "
	cQuery+=" AND Z71.ZZ7_PERDSC = '0' "
	cQuery+=" ) QTD, "

	cQuery+=" C6_PRCVEN PRECOPEDIDO, "
	cQuery+=" ROUND( "
	cQuery+=" (ZZ3_VLUNIT - ( "
	cQuery+=" SELECT COUNT(*) FROM "+ RetSqlName("ZZ7") + " Z72 "
	cQuery+=" WHERE Z72.D_E_L_E_T_ =' ' "
	cQuery+=" AND Z72.ZZ7_CODPAI = Z7.ZZ7_CODPAI "
	cQuery+=" AND Z72.ZZ7_FILIAL = Z7.ZZ7_FILIAL "
	cQuery+=" AND C6_YCODKIT = Z72.ZZ7_CODPAI "
	cQuery+=" AND Z72.ZZ7_CATEGO IN ('A','C','D','V') "		
	cQuery+=" AND Z72.ZZ7_SUPLEM <> 'N' "
	cQuery+=" AND Z72.ZZ7_TIPO = 'A' "
	cQuery+=" AND Z72.ZZ7_PERDSC <> '0' " 
	cQuery+=" )*10) " 	
	cQuery+=" / "  
	cQuery+=" (SELECT COUNT(*) FROM "+ RetSqlName("ZZ7") + " Z71 " 
	cQuery+=" WHERE Z71.D_E_L_E_T_ =' ' "
	cQuery+=" AND Z71.ZZ7_CODPAI = Z7.ZZ7_CODPAI "
	cQuery+=" AND Z71.ZZ7_FILIAL = Z7.ZZ7_FILIAL "
	cQuery+=" AND C6_YCODKIT = Z71.ZZ7_CODPAI "
	cQuery+=" AND Z71.ZZ7_CATEGO IN ('A','C','D','V') "		
	cQuery+=" AND Z71.ZZ7_SUPLEM = 'N' "
	cQuery+=" AND Z71.ZZ7_TIPO = 'A' "
	cQuery+=" AND Z71.ZZ7_PERDSC = '0' "
	cQuery+=" ),2)  "
	cQuery+=" CALCULO, Z7.ZZ7_CODPAI, C6.C6_YCODKIT, C6.C6_YPAIKIT,  Z7.ZZ7_TIPO, Z7.ZZ7_PERDSC, Z7.ZZ7_SUPLEM, Z7.ZZ7_CATEGO, C6.C6_DATFAT, "
	cQuery+=" C6.C6_FILIAL, C6.C6_NUM, C6.C6_ITEM, Z3.ZZ3_FILIAL, Z3.ZZ3_NUMERO, C6.C6_YCONTRA,  Z3.ZZ3_MEDICA, Z3.ZZ3_CLIENT, Z3.ZZ3_LOJA, "
	cQuery+=" C6.C6_YMEDICA,  Z3.ZZ3_ITEM, C6.C6_YITCONT,  C6.C6_NOTA, Z3.ZZ3_PRODUT ,ZZ2_PV01FL ,ZZ2_CLNF01 ,ZZ2_PV01NF , ZZ2_SER01"
	cQuery+=" FROM "+ RetSqlName("SC6") + " C6 (NOLOCK) "
	cQuery+=" JOIN "+ RetSqlName("ZZ2") + " Z2 (NOLOCK) "
	cQuery+=" ON  Z2.ZZ2_PV01FL = C6.C6_FILIAL "
	cQuery+=" AND Z2.ZZ2_PV01 = C6.C6_NUM "
	cQuery+=" AND Z2.ZZ2_CLNF01 = C6.C6_CLI "
	cQuery+=" AND Z2.ZZ2_LJNF01 = C6.C6_LOJA "
	cQuery+=" AND Z2.ZZ2_PV01NF = C6.C6_NOTA "
	cQuery+=" AND Z2.ZZ2_SER01 = C6.C6_SERIE "
	cQuery+=" AND Z2.D_E_L_E_T_ <> '*' "
	cQuery+=" JOIN "+ RetSqlName("ZZ3") + " Z3 (NOLOCK) "
	cQuery+=" ON Z3.ZZ3_FILIAL = Z2.ZZ2_FILIAL "
	cQuery+=" AND Z3.ZZ3_NUMERO = Z2.ZZ2_NUMERO "
	cQuery+=" AND Z3.ZZ3_MEDICA = Z2.ZZ2_MEDICA "
	cQuery+=" AND Z3.D_E_L_E_T_ <> '*' "
	cQuery+=" JOIN "+ RetSqlName("ZZ7") + " Z7 (NOLOCK) "
	cQuery+=" ON Z7.ZZ7_PRODUT = C6.C6_PRODUTO "
	cQuery+=" AND Z7.ZZ7_CODPAI = Z3.ZZ3_PRODUT "
	cQuery+=" AND Z7.D_E_L_E_T_ <> '*' "
	cQuery+=" JOIN "+ RetSqlName("ZZ7") + " Z72 (NOLOCK) "
	cQuery+=" ON Z72.ZZ7_CODIGO = Z7.ZZ7_CODPAI "
	cQuery+=" AND Z72.D_E_L_E_T_ <> '*' "
	cQuery+=" WHERE C6.D_E_L_E_T_ <> '*' "
	cQuery+=" AND C6.C6_QTDVEN = ZZ3_QUANT "
	if mv_par03 = 1
		cQuery+=" AND C6.C6_NOTA <> ''  "
	elseif mv_par03 = 2
		cQuery+=" AND C6.C6_NOTA = ''  "
	endif
	cQuery+=" AND ZZ3_ITEM = C6_YITCONT "
	cQuery+=" AND ISNULL(C6.C6_YCONTRA, '') <> '' "
	cQuery+=" AND Z7.ZZ7_CODPAI <> ' ' "
	cQuery+=" AND Z7.ZZ7_CATEGO IN ('A','C','D','V') "		  
	cQuery+=" AND Z7.ZZ7_SUPLEM = 'N' "
	cQuery+=" AND Z7.ZZ7_TIPO = 'A' "
	cQuery+=" AND Z7.ZZ7_PERDSC = '0' " 
	cQuery+=" )  "
	cQuery+=" AS A    "
	cQuery+=" WHERE CALCULO <> PRECOPEDIDO "
	cQuery+=" AND (PRECOPEDIDO - CALCULO  > 1 OR CALCULO - PRECOPEDIDO > 1) "
	cQuery+=" AND ZZ2_DTGER  BETWEEN '"+DTOS(MV_PAR01)+"' AND '"+DTOS(MV_PAR02)+"' "
	cQuery+=" order by ZZ3_FILIAL , ZZ3_MEDICA "
	TCQUERY cQuery NEW ALIAS T1

	WHILE !T1->(EOF())

		//PV01

		if mv_par03 = 1
			cQuery:=" Select ZZ3_FILIAL , ZZ3_NUMERO , ZZ3_MEDICA , ZZ3_CLIENT , ZZ3_LOJA , ZZ2_PV01FL ,ZZ2_CLNF01 ,ZZ2_PV01NF , ZZ2_SER01 ,SUM(PRECOPEDIDO)ValNF , sum(CALCULO) VALOK , SUM(PRECOPEDIDO - CALCULO) DIFERENCA "
		elseif mv_par03 = 2
			cQuery:=" Select ZZ3_FILIAL , ZZ3_NUMERO , ZZ3_MEDICA , ZZ3_CLIENT , ZZ3_LOJA , ZZ2_PV01FL ,ZZ2_CLNF01 ,ZZ2_PV01  ,SUM(PRECOPEDIDO)ValNF , sum(CALCULO) VALOK , SUM(PRECOPEDIDO - CALCULO) DIFERENCA "
		endif
		cQuery+=" From ( "
		cQuery+=" SELECT  *  FROM ( "
		cQuery+=" SELECT  "
		cQuery+=" ZZ2_MEDICA, "
		cQuery+=" ZZ2_DTGER, "
		cQuery+=" ZZ2_HRGER, "
		cQuery+=" ZZ3_PRODUT KIT, "
		cQuery+=" C6.C6_PRODUTO PRODUTO, "
		cQuery+=" ZZ3_VLUNIT, "
		cQuery+=" (SELECT COUNT(*) FROM "+ RetSqlName("ZZ7") + " Z71 " 
		cQuery+=" WHERE Z71.D_E_L_E_T_ =' ' "
		cQuery+=" AND Z71.ZZ7_CODPAI = Z7.ZZ7_CODPAI "
		cQuery+=" AND Z71.ZZ7_FILIAL = Z7.ZZ7_FILIAL "
		cQuery+=" AND C6_YCODKIT = Z71.ZZ7_CODPAI "
		cQuery+=" AND Z71.ZZ7_CATEGO IN ('A','C','D','V') "		
		cQuery+=" AND Z71.ZZ7_SUPLEM = 'N' "
		cQuery+=" AND Z71.ZZ7_TIPO = 'A' "
		cQuery+=" AND Z71.ZZ7_PERDSC = '0' "
		cQuery+=" ) QTD, "

		cQuery+=" C6_PRCVEN PRECOPEDIDO, "
		cQuery+=" ROUND( "
		cQuery+=" (ZZ3_VLUNIT - ( "
		cQuery+=" SELECT COUNT(*) FROM "+ RetSqlName("ZZ7") + " Z72 "
		cQuery+=" WHERE Z72.D_E_L_E_T_ =' ' "
		cQuery+=" AND Z72.ZZ7_CODPAI = Z7.ZZ7_CODPAI "
		cQuery+=" AND Z72.ZZ7_FILIAL = Z7.ZZ7_FILIAL "
		cQuery+=" AND C6_YCODKIT = Z72.ZZ7_CODPAI "
		cQuery+=" AND Z72.ZZ7_CATEGO IN ('A','C','D','V') "		
		cQuery+=" AND Z72.ZZ7_SUPLEM <> 'N' "
		cQuery+=" AND Z72.ZZ7_TIPO = 'A' "
		cQuery+=" AND Z72.ZZ7_PERDSC <> '0' " 
		cQuery+=" )*10) " 	
		cQuery+=" / "  
		cQuery+=" (SELECT COUNT(*) FROM "+ RetSqlName("ZZ7") + " Z71 " 
		cQuery+=" WHERE Z71.D_E_L_E_T_ =' ' "
		cQuery+=" AND Z71.ZZ7_CODPAI = Z7.ZZ7_CODPAI "
		cQuery+=" AND Z71.ZZ7_FILIAL = Z7.ZZ7_FILIAL "
		cQuery+=" AND C6_YCODKIT = Z71.ZZ7_CODPAI "
		cQuery+=" AND Z71.ZZ7_CATEGO IN ('A','C','D','V') "		
		cQuery+=" AND Z71.ZZ7_SUPLEM = 'N' "
		cQuery+=" AND Z71.ZZ7_TIPO = 'A' "
		cQuery+=" AND Z71.ZZ7_PERDSC = '0' "
		cQuery+=" ),2)  "
		cQuery+=" CALCULO, Z7.ZZ7_CODPAI, C6.C6_YCODKIT, C6.C6_YPAIKIT,  Z7.ZZ7_TIPO, Z7.ZZ7_PERDSC, Z7.ZZ7_SUPLEM, Z7.ZZ7_CATEGO, C6.C6_DATFAT, "
		cQuery+=" C6.C6_FILIAL, C6.C6_NUM, C6.C6_ITEM, Z3.ZZ3_FILIAL, Z3.ZZ3_NUMERO, C6.C6_YCONTRA,  Z3.ZZ3_MEDICA, Z3.ZZ3_CLIENT, Z3.ZZ3_LOJA, "
		cQuery+=" C6.C6_YMEDICA,  Z3.ZZ3_ITEM, C6.C6_YITCONT,  C6.C6_NOTA, Z3.ZZ3_PRODUT ,ZZ2_PV01FL ,ZZ2_CLNF01 ,ZZ2_PV01NF , ZZ2_SER01"
		cQuery+=" FROM "+ RetSqlName("SC6") + " C6 (NOLOCK) "
		cQuery+=" JOIN "+ RetSqlName("ZZ2") + " Z2 (NOLOCK) "
		cQuery+=" ON  Z2.ZZ2_PV01FL = C6.C6_FILIAL "
		cQuery+=" AND Z2.ZZ2_PV01 = C6.C6_NUM "
		cQuery+=" AND Z2.ZZ2_CLNF01 = C6.C6_CLI "
		cQuery+=" AND Z2.ZZ2_LJNF01 = C6.C6_LOJA "
		cQuery+=" AND Z2.ZZ2_PV01NF = C6.C6_NOTA "
		cQuery+=" AND Z2.ZZ2_SER01 = C6.C6_SERIE "
		cQuery+=" AND Z2.D_E_L_E_T_ <> '*' "
		cQuery+=" JOIN "+ RetSqlName("ZZ3") + " Z3 (NOLOCK) "
		cQuery+=" ON Z3.ZZ3_FILIAL = Z2.ZZ2_FILIAL "
		cQuery+=" AND Z3.ZZ3_NUMERO = Z2.ZZ2_NUMERO "
		cQuery+=" AND Z3.ZZ3_MEDICA = Z2.ZZ2_MEDICA "
		cQuery+=" AND Z3.D_E_L_E_T_ <> '*' "
		cQuery+=" JOIN "+ RetSqlName("ZZ7") + " Z7 (NOLOCK) "
		cQuery+=" ON Z7.ZZ7_PRODUT = C6.C6_PRODUTO "
		cQuery+=" AND Z7.ZZ7_CODPAI = Z3.ZZ3_PRODUT "
		cQuery+=" AND Z7.D_E_L_E_T_ <> '*' "
		cQuery+=" JOIN "+ RetSqlName("ZZ7") + " Z72 (NOLOCK) "
		cQuery+=" ON Z72.ZZ7_CODIGO = Z7.ZZ7_CODPAI "
		cQuery+=" AND Z72.D_E_L_E_T_ <> '*' "
		cQuery+=" WHERE C6.D_E_L_E_T_ <> '*' "
		cQuery+=" AND C6.C6_QTDVEN = ZZ3_QUANT "
		if mv_par03 = 1
			cQuery+=" AND C6.C6_NOTA <> ''  "
		elseif mv_par03 = 2
			cQuery+=" AND C6.C6_NOTA = ''  "
		endif
		cQuery+=" AND ZZ3_ITEM = C6_YITCONT "
		cQuery+=" AND ISNULL(C6.C6_YCONTRA, '') <> '' "
		cQuery+=" AND Z7.ZZ7_CODPAI <> ' ' "
		cQuery+=" AND Z7.ZZ7_CATEGO IN ('A','C','D','V') "		  
		cQuery+=" AND Z7.ZZ7_SUPLEM = 'N' "
		cQuery+=" AND Z7.ZZ7_TIPO = 'A' "
		cQuery+=" AND Z7.ZZ7_PERDSC = '0' " 
		cQuery+=" )  "
		cQuery+=" AS A    "
		cQuery+=" WHERE CALCULO <> PRECOPEDIDO "
		cQuery+=" AND (PRECOPEDIDO - CALCULO  > 1 OR CALCULO - PRECOPEDIDO > 1) "
		cQuery+=" AND ZZ3_FILIAL = '"+T1->ZZ3_FILIAL+"' "
		cQuery+=" AND ZZ3_NUMERO = '"+T1->ZZ3_NUMERO+"' "
		cQuery+=" AND ZZ3_MEDICA = '"+T1->ZZ3_MEDICA+"' "
		cQuery+=" AND ZZ3_CLIENT = '"+T1->ZZ3_CLIENT+"' "
		cQuery+=" AND ZZ3_LOJA   = '"+T1->ZZ3_LOJA+"' "
		cQuery+=" ) F "
		if mv_par03 = 1
			cQuery+=" GROUP BY ZZ3_FILIAL , ZZ3_NUMERO , ZZ3_MEDICA , ZZ3_CLIENT , ZZ3_LOJA , ZZ2_PV01FL , "
			cQuery+=" ZZ2_CLNF01 ,ZZ2_PV01NF , ZZ2_SER01 "
		elseif mv_par03 = 2
			cQuery+=" GROUP BY ZZ3_FILIAL , ZZ3_NUMERO , ZZ3_MEDICA , ZZ3_CLIENT , ZZ3_LOJA , ZZ2_PV01FL , "
			cQuery+=" ZZ2_CLNF01 ,ZZ2_PV01  "
		endif

		TCQUERY cQuery NEW ALIAS T2

		cFilMed  := T2->ZZ3_FILIAL
		cNumMed  := T2->ZZ3_NUMERO
		cMedica  := T2->ZZ3_MEDICA
		cCLIENT  := T2->ZZ3_CLIENT
		cLojCli  := T2->ZZ3_LOJA
		cPV01FL  := T2->ZZ2_PV01FL 
		cCLNF01  := T2->ZZ2_CLNF01
		cPV01NF  := IF(mv_par03 == 1 ,T2->ZZ2_PV01NF, T2->ZZ2_PV01)
		cSER01   := IF(mv_par03 == 1 ,T2->ZZ2_SER01, "")
		nValNf1  := T2->ValNf
		nValOK1  := T2->VALOK
		nDiverg1 := T2->DIFERENCA

		T2->(DbCloseArea())

		//nValMed := T1->TOTAL
		// Valor total da medi��o 
		cQuery:=" Select sum(ZZ3_VTOTAL)  TOTALMED "
		cQuery+=" From  "+ RetSqlName("ZZ3") + " ZZ3  "
		cQuery+=" WHERE ZZ3_FILIAL = '"+T1->ZZ3_FILIAL+"' "
		cQuery+=" AND ZZ3_NUMERO = '"+T1->ZZ3_NUMERO+"' "
		cQuery+=" AND ZZ3_MEDICA = '"+T1->ZZ3_MEDICA+"' "		  
		cQuery+=" AND ZZ3_CLIENT = '"+T1->ZZ3_CLIENT+"' "
		cQuery+=" AND ZZ3_LOJA = '"+T1->ZZ3_LOJA+"' "
		cQuery+=" AND D_E_L_E_T_ = '' "
		TCQUERY cQuery NEW ALIAS T3

		nValMed := T3->TOTALMED

		T3->(DbCloseArea())

		
		//PV03
		// ESPELHO DO PEDIDO 01
		
		if mv_par03 = 1
			cQuery:=" Select ZZ3_FILIAL , ZZ3_NUMERO , ZZ3_MEDICA , ZZ3_CLIENT , ZZ3_LOJA , ZZ2_PV03FL ,ZZ2_CLNF03 ,ZZ2_PV03NF , ZZ2_SER03 ,SUM(PRECOPEDIDO)ValNF , sum(CALCULO) VALOK , SUM(PRECOPEDIDO - CALCULO) DIFERENCA "
		elseif mv_par03 = 2
			cQuery:=" Select ZZ3_FILIAL , ZZ3_NUMERO , ZZ3_MEDICA , ZZ3_CLIENT , ZZ3_LOJA , ZZ2_PV03FL ,ZZ2_CLNF03 ,ZZ2_PV03  ,SUM(PRECOPEDIDO)ValNF , sum(CALCULO) VALOK , SUM(PRECOPEDIDO - CALCULO) DIFERENCA "
		endif
		cQuery+=" From ( "
		cQuery+=" SELECT  *  FROM ( "
		cQuery+=" SELECT  "
		cQuery+=" ZZ2_MEDICA, "
		cQuery+=" ZZ2_DTGER, "
		cQuery+=" ZZ2_HRGER, "
		cQuery+=" ZZ3_PRODUT KIT, "
		cQuery+=" C6.C6_PRODUTO PRODUTO, "
		cQuery+=" ZZ3_VLUNIT, "
		cQuery+=" (SELECT COUNT(*) FROM "+ RetSqlName("ZZ7") + " Z71 " 
		cQuery+=" WHERE Z71.D_E_L_E_T_ =' ' "
		cQuery+=" AND Z71.ZZ7_CODPAI = Z7.ZZ7_CODPAI "
		cQuery+=" AND Z71.ZZ7_FILIAL = Z7.ZZ7_FILIAL "
		cQuery+=" AND C6_YCODKIT = Z71.ZZ7_CODPAI "
		cQuery+=" AND Z71.ZZ7_CATEGO IN ('A','C','D','V') "		
		cQuery+=" AND Z71.ZZ7_SUPLEM = 'N' "
		cQuery+=" AND Z71.ZZ7_TIPO = 'A' "
		cQuery+=" AND Z71.ZZ7_PERDSC = '0' "
		cQuery+=" ) QTD, "

		cQuery+=" C6_PRCVEN PRECOPEDIDO, "
		cQuery+=" ROUND( "
		cQuery+=" (ZZ3_VLUNIT - ( "
		cQuery+=" SELECT COUNT(*) FROM "+ RetSqlName("ZZ7") + " Z72 "
		cQuery+=" WHERE Z72.D_E_L_E_T_ =' ' "
		cQuery+=" AND Z72.ZZ7_CODPAI = Z7.ZZ7_CODPAI "
		cQuery+=" AND Z72.ZZ7_FILIAL = Z7.ZZ7_FILIAL "
		cQuery+=" AND C6_YCODKIT = Z72.ZZ7_CODPAI "
		cQuery+=" AND Z72.ZZ7_CATEGO IN ('A','C','D','V') "		
		cQuery+=" AND Z72.ZZ7_SUPLEM <> 'N' "
		cQuery+=" AND Z72.ZZ7_TIPO = 'A' "
		cQuery+=" AND Z72.ZZ7_PERDSC <> '0' " 
		cQuery+=" )*10) " 	
		cQuery+=" / "  
		cQuery+=" (SELECT COUNT(*) FROM "+ RetSqlName("ZZ7") + " Z71 " 
		cQuery+=" WHERE Z71.D_E_L_E_T_ =' ' "
		cQuery+=" AND Z71.ZZ7_CODPAI = Z7.ZZ7_CODPAI "
		cQuery+=" AND Z71.ZZ7_FILIAL = Z7.ZZ7_FILIAL "
		cQuery+=" AND C6_YCODKIT = Z71.ZZ7_CODPAI "
		cQuery+=" AND Z71.ZZ7_CATEGO IN ('A','C','D','V') "		
		cQuery+=" AND Z71.ZZ7_SUPLEM = 'N' "
		cQuery+=" AND Z71.ZZ7_TIPO = 'A' "
		cQuery+=" AND Z71.ZZ7_PERDSC = '0' "
		cQuery+=" ),2)  "
		cQuery+=" CALCULO, Z7.ZZ7_CODPAI, C6.C6_YCODKIT, C6.C6_YPAIKIT,  Z7.ZZ7_TIPO, Z7.ZZ7_PERDSC, Z7.ZZ7_SUPLEM, Z7.ZZ7_CATEGO, C6.C6_DATFAT, "
		cQuery+=" C6.C6_FILIAL, C6.C6_NUM, C6.C6_ITEM, Z3.ZZ3_FILIAL, Z3.ZZ3_NUMERO, C6.C6_YCONTRA,  Z3.ZZ3_MEDICA, Z3.ZZ3_CLIENT, Z3.ZZ3_LOJA, "
		cQuery+=" C6.C6_YMEDICA,  Z3.ZZ3_ITEM, C6.C6_YITCONT,  C6.C6_NOTA, Z3.ZZ3_PRODUT ,ZZ2_PV03FL ,ZZ2_CLNF03 ,ZZ2_PV03NF , ZZ2_SER03"
		cQuery+=" FROM "+ RetSqlName("SC6") + " C6 (NOLOCK) "
		cQuery+=" JOIN "+ RetSqlName("ZZ2") + " Z2 (NOLOCK) "
		cQuery+=" ON  Z2.ZZ2_PV03FL = C6.C6_FILIAL "
		cQuery+=" AND Z2.ZZ2_PV03 = C6.C6_NUM "
		cQuery+=" AND Z2.ZZ2_CLNF03 = C6.C6_CLI "
		cQuery+=" AND Z2.ZZ2_LJNF03 = C6.C6_LOJA "
		cQuery+=" AND Z2.ZZ2_PV03NF = C6.C6_NOTA "
		cQuery+=" AND Z2.ZZ2_SER03 = C6.C6_SERIE "
		cQuery+=" AND Z2.D_E_L_E_T_ <> '*' "
		cQuery+=" JOIN "+ RetSqlName("ZZ3") + " Z3 (NOLOCK) "
		cQuery+=" ON Z3.ZZ3_FILIAL = Z2.ZZ2_FILIAL "
		cQuery+=" AND Z3.ZZ3_NUMERO = Z2.ZZ2_NUMERO "
		cQuery+=" AND Z3.ZZ3_MEDICA = Z2.ZZ2_MEDICA "
		cQuery+=" AND Z3.D_E_L_E_T_ <> '*' "
		cQuery+=" JOIN "+ RetSqlName("ZZ7") + " Z7 (NOLOCK) "
		cQuery+=" ON Z7.ZZ7_PRODUT = C6.C6_PRODUTO "
		cQuery+=" AND Z7.ZZ7_CODPAI = Z3.ZZ3_PRODUT "
		cQuery+=" AND Z7.D_E_L_E_T_ <> '*' "
		cQuery+=" JOIN "+ RetSqlName("ZZ7") + " Z72 (NOLOCK) "
		cQuery+=" ON Z72.ZZ7_CODIGO = Z7.ZZ7_CODPAI "
		cQuery+=" AND Z72.D_E_L_E_T_ <> '*' "
		cQuery+=" WHERE C6.D_E_L_E_T_ <> '*' "
		cQuery+=" AND C6.C6_QTDVEN = ZZ3_QUANT "
		if mv_par03 = 1
			cQuery+=" AND C6.C6_NOTA <> ''  "
		elseif mv_par03 = 2
			cQuery+=" AND C6.C6_NOTA = ''  "
		endif
		cQuery+=" AND ZZ3_ITEM = C6_YITCONT "
		cQuery+=" AND ISNULL(C6.C6_YCONTRA, '') <> '' "
		cQuery+=" AND Z7.ZZ7_CODPAI <> ' ' "
		cQuery+=" AND Z7.ZZ7_CATEGO IN ('A','C','D','V') "		  
		cQuery+=" AND Z7.ZZ7_SUPLEM = 'N' "
		cQuery+=" AND Z7.ZZ7_TIPO = 'A' "
		cQuery+=" AND Z7.ZZ7_PERDSC = '0' " 
		cQuery+=" )  "
		cQuery+=" AS A    "
		cQuery+=" WHERE CALCULO <> PRECOPEDIDO "
		cQuery+=" AND (PRECOPEDIDO - CALCULO  > 1 OR CALCULO - PRECOPEDIDO > 1) "
		cQuery+=" AND ZZ3_FILIAL = '"+T1->ZZ3_FILIAL+"' "
		cQuery+=" AND ZZ3_NUMERO = '"+T1->ZZ3_NUMERO+"' "
		cQuery+=" AND ZZ3_MEDICA = '"+T1->ZZ3_MEDICA+"' "
		cQuery+=" AND ZZ3_CLIENT = '"+T1->ZZ3_CLIENT+"' "
		cQuery+=" AND ZZ3_LOJA   = '"+T1->ZZ3_LOJA+"' "
		cQuery+=" ) F "

		if mv_par03 = 1
			cQuery+=" GROUP BY ZZ3_FILIAL , ZZ3_NUMERO , ZZ3_MEDICA , ZZ3_CLIENT , ZZ3_LOJA , ZZ2_PV03FL , "
			cQuery+=" ZZ2_CLNF03 ,ZZ2_PV03NF , ZZ2_SER03 "
		elseif mv_par03 = 2
			cQuery+=" GROUP BY ZZ3_FILIAL , ZZ3_NUMERO , ZZ3_MEDICA , ZZ3_CLIENT , ZZ3_LOJA , ZZ2_PV03FL , "
			cQuery+=" ZZ2_CLNF03 ,ZZ2_PV03  "
		endif

		TCQUERY cQuery NEW ALIAS T2
		
		cPV03FL  := T2->ZZ2_PV03FL 
		cCLNF03  := T2->ZZ2_CLNF03
		cPV03NF  := IF(mv_par03 == 1 ,T2->ZZ2_PV03NF, T2->ZZ2_PV03) 
		cSER03   := IF(mv_par03 == 1 ,T2->ZZ2_SER03, "")
		// devido ao problema nos carimbos dos kit's alguns produtos n�o eram identificados no pv3 , 
		// como o pv3 � espelho do pv01 estou retonando os memos valores do pv01 no pv03 , que foram 
		// validados  
		nValNf3  := if(alltrim(cPV03NF)<>"",nValNf1,"")
		nValOK3  := if(alltrim(cPV03NF)<>"",nValOK1,"")
		nDiverg3 := if(alltrim(cPV03NF)<>"",nDiverg1,"")

		T2->(DbCloseArea())

		IF mv_par03 == 1
			AADD(aPrint,{cFilMed,cNumMed,cMedica,cCLIENT,nValMed,cPV01FL,cCLNF01,cPV01NF,cSER01,nValNf1,nValOK1,nDiverg1,cPV03FL,cCLNF03,cPV03NF,cSER03,nValNf3,nValOK3,nDiverg3})
		ELSEIF mv_par03 == 2
			AADD(aPrint,{cFilMed,cNumMed,cMedica,cCLIENT,nValMed,cPV01FL,cCLNF01,cPV01NF,nValNf1,nValOK1,nDiverg1,cPV03FL,cCLNF03,cPV03NF,nValNf3,nValOK3,nDiverg3})
		ENDIF
		T1->(DbSkip())
	ENDDO
	T1->(DbCloseArea())

	oSection:Init()
	FOR nAx := 1 to (len(aPrint))

		IF mv_par03 == 1 
			oSection:Cell("ZZ2_FILIAL" ):SetValue(aPrint[nAx][1])
			oSection:Cell("ZZ2_NUMERO" ):SetValue(aPrint[nAx][2])
			oSection:Cell("ZZ2_MEDICA"):SetValue(aPrint[nAx][3])
			oSection:Cell("ZZ2_CLIENT"):SetValue(aPrint[nAx][4])
			oSection:Cell("ValMed"):SetValue(aPrint[nAx][5])
			oSection:Cell("ZZ2_PV01FL"):SetValue(aPrint[nAx][6])
			oSection:Cell("ZZ2_CLNF01"):SetValue(aPrint[nAx][7])
			oSection:Cell("ZZ2_PV01NF"):SetValue(aPrint[nAx][8])
			oSection:Cell("ZZ2_SER01"):SetValue(aPrint[nAx][9])
			oSection:Cell("ValNF1E"):SetValue(aPrint[nAx][10])
			oSection:Cell("ValNF1C"):SetValue(aPrint[nAx][11])
			oSection:Cell("Diverg1"):SetValue(aPrint[nAx][12])
			oSection:Cell("ZZ2_PV03FL"):SetValue(aPrint[nAx][13])
			oSection:Cell("ZZ2_CLNF03"):SetValue(aPrint[nAx][14])
			oSection:Cell("ZZ2_PV03NF"):SetValue(aPrint[nAx][15])
			oSection:Cell("ZZ2_SER03"):SetValue(aPrint[nAx][16])
			oSection:Cell("ValNF3E"):SetValue(aPrint[nAx][17])
			oSection:Cell("ValNF3C"):SetValue(aPrint[nAx][18])
			oSection:Cell("Diverg3"):SetValue(aPrint[nAx][19])
			oSection:PrintLine()
		ELSEIF mv_par03 == 2
			oSection:Cell("ZZ2_FILIAL" ):SetValue(aPrint[nAx][1])
			oSection:Cell("ZZ2_NUMERO" ):SetValue(aPrint[nAx][2])
			oSection:Cell("ZZ2_MEDICA"):SetValue(aPrint[nAx][3])
			oSection:Cell("ZZ2_CLIENT"):SetValue(aPrint[nAx][4])
			oSection:Cell("ValMed"):SetValue(aPrint[nAx][5])
			oSection:Cell("ZZ2_PV01FL"):SetValue(aPrint[nAx][6])
			oSection:Cell("ZZ2_CLNF01"):SetValue(aPrint[nAx][7])
			oSection:Cell("ZZ2_PV01"):SetValue(aPrint[nAx][8])
			oSection:Cell("ValPD1E"):SetValue(aPrint[nAx][9])
			oSection:Cell("ValPD1C"):SetValue(aPrint[nAx][10])
			oSection:Cell("Diverg1"):SetValue(aPrint[nAx][11])
			oSection:Cell("ZZ2_PV03FL"):SetValue(aPrint[nAx][12])
			oSection:Cell("ZZ2_CLNF03"):SetValue(aPrint[nAx][13])
			oSection:Cell("ZZ2_PV03"):SetValue(aPrint[nAx][14])
			oSection:Cell("ValPD3E"):SetValue(aPrint[nAx][15])
			oSection:Cell("ValPD3C"):SetValue(aPrint[nAx][16])
			oSection:Cell("Diverg3"):SetValue(aPrint[nAx][17])
			oSection2:PrintLine()
		ENDIF
	next
	aPrint :={}
	oSection:Finish()
return

//|----------------------------------------------------------|//
//|Fun��o: Cria��o das Perguntas							 |//
//|----------------------------------------------------------|//
Static Function AjustaSX1(cPerg)

	PutSx1(cPerg, "01","Data De"         ,".",".","mv_ch01","D",08,0,1,"G","",""   ,"","","mv_par01","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Data Ate"        ,".",".","mv_ch02","D",08,0,1,"G","",""   ,"","","mv_par02","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","Tipo"            ,".",".","mv_ch03","N",01,0,2,"C","",""   ,"","","mv_par03","Nota Fiscal" ,"Nota Fiscal","Nota Fiscal","","Pedidos","Pedidos","Pedidos","","","","","","","","","")


Return()