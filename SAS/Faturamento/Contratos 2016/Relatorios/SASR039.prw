#include 'protheus.ch'
#include 'parmtype.ch'
#include 'topconn.ch'

user function SASR039()

	Local oReport
	Local cQuery

	Private cPerg	:= "SASR039"


	AjustaSX1(cPerg)

	oReport := ReportDef()
	oReport:PrintDialog()

Return


Static Function ReportDef()

	Local oReport
	Local oSection


	Local oBreak
	Local aOrdem 	:= {}


	oReport := TReport():New(cPerg,"Relatorio de Auditoria",cPerg,{|oReport| PrintReport(oReport)},"Relatorio de Auditoria")
	oReport:SetLandscape()
	oReport:SetTotalInLine(.F.)


	oSection := TRSection():New(oReport,"Relatorio de Auditoria",{},)

	TRCell():New(oSection, "FILAL"				, "SF2", 'Filial'			,,TamSX3("F2_FILIAL")[1]+1,,)
	TRCell():New(oSection, "CONTRATO"				, "", 'Contrato'			,,6)
	TRCell():New(oSection, "CLIENTE"				, "", 'Codigo Cliente'			,,6)
	TRCell():New(oSection, "LOJA"					, "", 'Loja'			,,2)
	TRCell():New(oSection, "NOME"				, "", 'Nome Cliente'			,,60)
	TRCell():New(oSection, "TIPO"				, "", 'Tipo'			,,20)
	TRCell():New(oSection, "PV01"				, "ZZ2", 'PV01'			,,TamSX3("ZZ2_PV01")[1]+1,,)
	TRCell():New(oSection, "NF01"				, "ZZ2", 'NF01'			,,TamSX3("ZZ2_PV01NF")[1]+1,,)
	//TRCell():New(oSection, "DATA1"				, "SF2", 'Data1'			,,TamSX3("F2_EMISSAO")[1]+1,,)
	TRCell():New(oSection, "TES01"				, "SD2", 'TES 02'			,,TamSX3("D2_TES")[1]+1,,)
	TRCell():New(oSection, "CFOP01"				, "SD2", 'CFOP 02'			,,TamSX3("D2_CF")[1]+1,,)
	//TRCell():New(oSection, "CODTAB1"				, "", 'Codigo Tabela1'			,,6)
	TRCell():New(oSection, "VALOR1"				,"", 'Valor1'			,"@E 999,999,999.99",12)
	TRCell():New(oSection, "DATA1"				, "", 'Data Transmiss�o NF01'			,,TamSX3("F2_DAUTNFE")[1]+1,,)
	TRCell():New(oSection, "DATAEM1"				, "", 'Data Emiss�o NF01'			,,TamSX3("F2_DAUTNFE")[1]+1,,)
	TRCell():New(oSection, "CH1"				, "", 'Chave NF01'			,,TamSX3("F2_CHVNFE")[1]+1,,)
	TRCell():New(oSection, "MSG1"				, "", 'Mensagem NF01'			,,TamSX3("F2_YMSGNF")[1]+1,,)
	TRCell():New(oSection, "LOCAL1"				, "", 'Armazem NF01'			,,TamSX3("D2_LOCAL")[1]+1,,)
	TRCell():New(oSection, "FRETE1"				, "", 'FRETE NF01'			,,TamSX3("F2_FRETE")[1]+1,,)
	TRCell():New(oSection, "DESC1"				, "", 'DESCONTO NF01'			,,TamSX3("F2_DESCONT")[1]+1,,)
	
	
	TRCell():New(oSection, "PV02"				, "ZZ2", 'PV02'			,,TamSX3("ZZ2_PV02")[1]+1,,)
	TRCell():New(oSection, "NF02"				, "ZZ2", 'NF02'			,,TamSX3("ZZ2_PV02NF")[1]+1,,)
	//TRCell():New(oSection, "DATA2"				, "SF2", 'Data'			,,TamSX3("F2_EMISSAO")[1]+1,,)
	TRCell():New(oSection, "TES02"				, "SD2", 'TES 02'			,,TamSX3("D2_TES")[1]+1,,)
	TRCell():New(oSection, "CFOP02"				, "SD2", 'CFOP 02'			,,TamSX3("D2_CF")[1]+1,,)
	//TRCell():New(oSection, "CODTAB2"				, "", 'Codigo Tabela2'			,,6)
	TRCell():New(oSection, "VALOR2"				,"", 'Valor2'			,"@E 999,999,999.99",12)
	TRCell():New(oSection, "DATA2"				, "", 'Data Transmiss�o NF02'			,,TamSX3("F2_DAUTNFE")[1]+1,,)
	TRCell():New(oSection, "DATAEM2"				, "", 'Data Emiss�o NF02'			,,TamSX3("F2_DAUTNFE")[1]+1,,)
	TRCell():New(oSection, "CH2"				, "", 'Chave NF02'			,,TamSX3("F2_CHVNFE")[1]+1,,)
	TRCell():New(oSection, "MSG2"				, "", 'Mensagem NF02'			,,TamSX3("F2_YMSGNF")[1]+1,,)
	TRCell():New(oSection, "LOCAL2"				, "", 'Armazem NF02'			,,TamSX3("D2_LOCAL")[1]+1,,)
	TRCell():New(oSection, "FRETE2"				, "", 'FRETE NF02'			,,TamSX3("F2_FRETE")[1]+1,,)
	TRCell():New(oSection, "DESC2"				, "", 'DESCONTO NF02'			,,TamSX3("F2_DESCONT")[1]+1,,)
	
	
	TRCell():New(oSection, "PV03"				, "ZZ2", 'PV03'			,,TamSX3("ZZ2_PV03")[1]+1,,)
	TRCell():New(oSection, "NF03"				, "ZZ2", 'NF03'			,,TamSX3("ZZ2_PV03NF")[1]+1,,)
	//TRCell():New(oSection, "DATA3"				, "SF2", 'Data3'			,,TamSX3("F2_EMISSAO")[1]+1,,)
	TRCell():New(oSection, "TES03"				, "SD2", 'TES 03'			,,TamSX3("D2_TES")[1]+1,,)
	TRCell():New(oSection, "CFOP03"				, "SD2", 'CFOP 03'			,,TamSX3("D2_CF")[1]+1,,)
	//TRCell():New(oSection, "CODTAB3"				, "", 'Codigo Tabela3'			,,6)
	TRCell():New(oSection, "VALOR3"				,"", 'Valor3'			,"@E 999,999,999.99",12)
	TRCell():New(oSection, "DATA3"				, "", 'Data Transmiss�o NF03'			,,TamSX3("F2_DAUTNFE")[1]+1,,)
	TRCell():New(oSection, "DATAEM3"				, "", 'Data Emiss�o NF03'			,,TamSX3("F2_DAUTNFE")[1]+1,,)
	TRCell():New(oSection, "MSG3"				, "", 'Mensagem NF03'			,,TamSX3("F2_YMSGNF")[1]+1,,)
	TRCell():New(oSection, "CH3"				, "", 'Chave NF03'			,,TamSX3("F2_CHVNFE")[1]+1,,)
	TRCell():New(oSection, "LOCAL3"				, "", 'Armazem NF03'			,,TamSX3("D2_LOCAL")[1]+1,,)
	TRCell():New(oSection, "FRETE3"				, "", 'FRETE NF03'			,,TamSX3("F2_FRETE")[1]+1,,)
	TRCell():New(oSection, "DESC3"				, "", 'DESCONTO NF03'			,,TamSX3("F2_DESCONT")[1]+1,,)
	
	
	TRCell():New(oSection, "PC01"				, "ZZ2", 'PC01'			,,TamSX3("ZZ2_PC01")[1]+1,,)
	TRCell():New(oSection, "NFE01"				, "ZZ2", 'NFE01'			,,TamSX3("ZZ2_NFE1")[1]+1,,)
	//TRCell():New(oSection, "DATAPC"				, "SF1", 'Data PC'			,,TamSX3("F1_EMISSAO")[1]+1,,)
	TRCell():New(oSection, "TESPC"				, "SD1", 'TES PC'			,,TamSX3("D1_TES")[1]+1,,)
	TRCell():New(oSection, "CFOPPC"				, "SD1", 'CFOP PC'			,,TamSX3("D1_CF")[1]+1,,)
	TRCell():New(oSection, "VALORPC"				,"", 'Valor PC'			,"@E 999,999,999.99",12)
	TRCell():New(oSection, "OBS"				, "", 'OBS'			,,50)

Return oReport

/*Inicia Logica Print Report */

Static Function PrintReport(oReport)

	Local oSection := oReport:Section(1)
	//Local oSection2 := oReport:Section(2)
	Local oBreak
	Local cPart
	Local cCase
	Local cOBS := ""

	Pergunte(cPerg,.F.)


	cQuery := "SELECT DISTINCT ZZ2_FILIAL, ZZ2_NUMERO AS CONTRATO, ZZ2_MEDICA, ZZ2_CLIENT, ZZ2_LOJA, F21.F2_FILIAL FILIAL, F21.F2_CLIENTE CLIENTE, F21.F2_LOJA LOJA, "
	cQuery += " ZZ2_NOME NOME, CASE ZZ2_TIPCTR WHEN '1' THEN 'DIRETO' WHEN '2' THEN 'TRIANGULAR' END AS TIPO, ZZ2_PV01 PV01, ZZ2_PV01NF NF01, D21.D2_TES TES01, D21.D2_CF CFOP01, F21.F2_VALBRUT VLNF01, F21.F2_CHVNFE CHV01,F21.F2_DAUTNFE,  "
	cQuery += " ISNULL(CAST(CAST([F21].[F2_YMSGNF] AS VARBINARY(1024)) AS VARCHAR(1024)),'') AS [MSG_NF1], F2_EMISSAO, F2_CHVNFE, F2_FRETE, F2_DESCONT, D2_LOCAL "
	cQuery += "FROM "+RETSQLNAME("ZZ2")+" ZZ2 "
	cQuery += " INNER JOIN SF2010 F21 ON F21.F2_FILIAL = ZZ2_PV01FL AND ZZ2_PV01NF = F21.F2_DOC AND ZZ2_SER01 = F21.F2_SERIE "
	cQuery += " AND ZZ2_CLNF01 = F21.F2_CLIENTE AND ZZ2_LJNF01 = F21.F2_LOJA "
	cQuery += "  INNER JOIN SD2010 D21 ON D21.D2_FILIAL = ZZ2_PV01FL AND ZZ2_PV01NF = D21.D2_DOC AND ZZ2_SER01 = D21.D2_SERIE "
	cQuery += " AND ZZ2_CLNF01 = D21.D2_CLIENTE AND ZZ2_LJNF01 = D21.D2_LOJA "
	cQuery += " WHERE ZZ2_PV01FL BETWEEN '"+mv_par01+"' AND  '"+mv_par02+"' AND F21.F2_EMISSAO BETWEEN '"+DTOS(mv_par03)+"' AND  '"+DTOS(mv_par04)+"' "
	cQuery += " AND ZZ2.D_E_L_E_T_ = '' AND F21.D_E_L_E_T_ = '' AND D21.D_E_L_E_T_ = ''  "
	cQuery += " ORDER BY TIPO,F2_FILIAL, F2_CLIENTE, F2_LOJA "

	if Select("T01") > 0
		dbSelectArea("T01")
		dbCloseArea()
	endif

	TcQuery cQuery New Alias T01


	oSection:Init()

	DO While !oReport:Cancel() .AND. !T01->(EOF())

		oReport:IncMeter()

		cOBS := ""

		oSection:Cell("FILIAL"):SetValue(T01->FILIAL)
		oSection:Cell("CONTRATO"):SetValue(T01->CONTRATO)
		oSection:Cell("CLIENTE"):SetValue(T01->CLIENTE)
		oSection:Cell("LOJA"):SetValue(T01->LOJA)
		oSection:Cell("NOME"):SetValue(T01->NOME)
		oSection:Cell("TIPO"):SetValue(T01->TIPO)
		oSection:Cell("PV01"):SetValue(T01->PV01)
		oSection:Cell("NF01"):SetValue(T01->NF01)
		oSection:Cell("TES01"):SetValue(T01->TES01)
		oSection:Cell("CFOP01"):SetValue(T01->CFOP01)
		//oSection:Cell("CODTAB1"):SetValue("")
		oSection:Cell("VALOR1"):SetValue(T01->VLNF01)
		oSection:Cell("DATA1"):SetValue(STOD(T01->F2_DAUTNFE))
		
		
		
		oSection:Cell("DATAEM1"):SetValue(STOD(T01->F2_EMISSAO))
		oSection:Cell("MSG1"):SetValue(T01->MSG_NF1)
		oSection:Cell("CH1"):SetValue(T01->F2_CHVNFE)
		oSection:Cell("LOCAL1"):SetValue(T01->D2_LOCAL)
		oSection:Cell("FRETE1"):SetValue(T01->F2_FRETE)
		oSection:Cell("DESC1"):SetValue(T01->F2_DESCONT)
		
		
		cOBS += IIF(ALLTRIM(T01->CHV01) == "", "NF01 N TRANSMITIDA ", "")

		cQuery := "SELECT DISTINCT ZZ2_PV02 PV02, ZZ2_PV02NF NF02, D2.D2_TES TES02, D2.D2_CF CFOP02, F2.F2_VALBRUT VLNF02, F2.F2_CHVNFE CHV02,F2.F2_DAUTNFE,  "
		cQuery += " ISNULL(CAST(CAST([F2].[F2_YMSGNF] AS VARBINARY(1024)) AS VARCHAR(1024)),'') AS [MSG_NF2], F2_EMISSAO, F2_CHVNFE, F2_FRETE, F2_DESCONT, D2_LOCAL "
		cQuery += "FROM "+RETSQLNAME("ZZ2")+" ZZ2 "
		cQuery += " INNER JOIN SF2010 F2 ON F2.F2_FILIAL = ZZ2_PV02FL AND ZZ2_PV02NF = F2.F2_DOC AND  "
		cQuery += " ZZ2_SER02 = F2.F2_SERIE AND ZZ2_CLNF02 = F2.F2_CLIENTE AND ZZ2_LJNF02 = F2.F2_LOJA "
		cQuery += "  INNER JOIN SD2010 D2 ON D2.D2_FILIAL = ZZ2_PV02FL AND ZZ2_PV02NF = D2.D2_DOC AND "
		cQuery += "  ZZ2_SER02 = D2.D2_SERIE AND ZZ2_CLNF02 = D2.D2_CLIENTE AND ZZ2_LJNF02 = D2.D2_LOJA "
		cQuery += " WHERE ZZ2_FILIAL = '"+T01->ZZ2_FILIAL+"' AND ZZ2_NUMERO = '"+T01->CONTRATO+"' AND
		cQuery += " ZZ2_MEDICA = '"+T01->ZZ2_MEDICA+"' AND ZZ2_CLIENT = '"+T01->ZZ2_CLIENT+"' AND  "
		cQuery += "  ZZ2_LOJA = '"+T01->ZZ2_LOJA+"' AND ZZ2.D_E_L_E_T_ = '' AND F2.D_E_L_E_T_ = '' AND D2.D_E_L_E_T_ = '' "

		if Select("T02") > 0
			dbSelectArea("T02")
			dbCloseArea()
		endif

		TcQuery cQuery New Alias T02

		IF !T02->(EOF())

			oSection:Cell("PV02"):SetValue(T02->PV02)
			oSection:Cell("NF02"):SetValue(T02->NF02)
			oSection:Cell("TES02"):SetValue(T02->TES02)
			oSection:Cell("CFOP02"):SetValue(T02->CFOP02)
			//oSection:Cell("CODTAB2"):SetValue("")
			oSection:Cell("VALOR2"):SetValue(T02->VLNF02)
			oSection:Cell("DATA2"):SetValue(STOD(T02->F2_DAUTNFE))
			
			oSection:Cell("DATAEM2"):SetValue(STOD(T02->F2_EMISSAO))
			oSection:Cell("MSG2"):SetValue(T02->MSG_NF2)
			oSection:Cell("CH2"):SetValue(T02->F2_CHVNFE)
			oSection:Cell("LOCAL2"):SetValue(T02->D2_LOCAL)
			oSection:Cell("FRETE2"):SetValue(T02->F2_FRETE)
			oSection:Cell("DESC2"):SetValue(T02->F2_DESCONT)
		
			cOBS+= IIF(ALLTRIM(T02->CHV02) == "", "/NF02 N TRANSMITIDA ", "")

		ENDIF

		T02->(DbCloseArea())

		cQuery := "SELECT DISTINCT ZZ2_PV03 PV03, ZZ2_PV03NF NF03, D2.D2_TES TES03, D2.D2_CF CFOP03, F2.F2_VALBRUT VLNF03, F2.F2_CHVNFE CHV03,F2.F2_DAUTNFE,  "
		cQuery += " ISNULL(CAST(CAST([F2].[F2_YMSGNF] AS VARBINARY(1024)) AS VARCHAR(1024)),'') AS [MSG_NF3], F2_EMISSAO, F2_CHVNFE, F2_FRETE, F2_DESCONT, D2_LOCAL "
		cQuery += "FROM "+RETSQLNAME("ZZ2")+" ZZ2 "
		cQuery += " INNER JOIN SF2010 F2 ON F2.F2_FILIAL = ZZ2_PV03FL AND ZZ2_PV03NF = F2.F2_DOC AND  "
		cQuery += " ZZ2_SER03 = F2.F2_SERIE AND ZZ2_CLNF03 = F2.F2_CLIENTE AND ZZ2_LJNF03 = F2.F2_LOJA "
		cQuery += "  INNER JOIN SD2010 D2 ON D2.D2_FILIAL = ZZ2_PV03FL AND ZZ2_PV03NF = D2.D2_DOC AND "
		cQuery += "  ZZ2_SER03 = D2.D2_SERIE AND ZZ2_CLNF03 = D2.D2_CLIENTE AND ZZ2_LJNF03 = D2.D2_LOJA "
		cQuery += " WHERE ZZ2_FILIAL = '"+T01->ZZ2_FILIAL+"' AND ZZ2_NUMERO = '"+T01->CONTRATO+"' AND
		cQuery += " ZZ2_MEDICA = '"+T01->ZZ2_MEDICA+"' AND ZZ2_CLIENT = '"+T01->ZZ2_CLIENT+"' AND  "
		cQuery += "  ZZ2_LOJA = '"+T01->ZZ2_LOJA+"' AND ZZ2.D_E_L_E_T_ = '' AND F2.D_E_L_E_T_ = '' AND D2.D_E_L_E_T_ = '' "

		if Select("T03") > 0
			dbSelectArea("T03")
			dbCloseArea()
		endif

		TcQuery cQuery New Alias T03

		IF !T03->(EOF())

			oSection:Cell("PV03"):SetValue(T03->PV03)
			oSection:Cell("NF03"):SetValue(T03->NF03)
			oSection:Cell("TES03"):SetValue(T03->TES03)
			oSection:Cell("CFOP03"):SetValue(T03->CFOP03)
			//oSection:Cell("CODTAB3"):SetValue("")
			oSection:Cell("VALOR3"):SetValue(T03->VLNF03)
			oSection:Cell("DATA3"):SetValue(STOD(T03->F2_DAUTNFE))
			
			
			oSection:Cell("DATAEM3"):SetValue(STOD(T03->F2_EMISSAO))
			oSection:Cell("MSG3"):SetValue(T03->MSG_NF3)
			oSection:Cell("CH3"):SetValue(T03->F2_CHVNFE)
			oSection:Cell("LOCAL3"):SetValue(T03->D2_LOCAL)
			oSection:Cell("FRETE3"):SetValue(T03->F2_FRETE)
			oSection:Cell("DESC3"):SetValue(T03->F2_DESCONT)
		
		
			cOBS+= IIF(ALLTRIM(T03->CHV03) == "", "/NF03 N TRANSMITIDA", "")

		ENDIF

		T03->(DbCloseArea())

		cQuery := "SELECT DISTINCT ZZ2_PC01 PC01, ZZ2_NFE1 NFE1, D1.D1_TES TESPC, D1.D1_CF CFOPPC, F1.F1_VALBRUT VLNFE  "
		cQuery += "FROM "+RETSQLNAME("ZZ2")+" ZZ2 "
		cQuery += " INNER JOIN SF1010 F1 ON F1.F1_FILIAL = ZZ2_PCFIL AND ZZ2_NFE1 = F1.F1_DOC AND  "
		cQuery += " ZZ2_SER1 = F1.F1_SERIE AND ZZ2_FORNEC = F1.F1_FORNECE AND ZZ2_LOJFOR = F1.F1_LOJA "
		cQuery += " INNER JOIN SD1010 D1 ON D1.D1_FILIAL = ZZ2_PCFIL AND ZZ2_NFE1 = D1.D1_DOC AND "
		cQuery += " ZZ2_SER1 = D1.D1_SERIE AND ZZ2_FORNEC = D1.D1_FORNECE AND ZZ2_LOJFOR = D1.D1_LOJA "
		cQuery += " WHERE ZZ2_FILIAL = '"+T01->ZZ2_FILIAL+"' AND ZZ2_NUMERO = '"+T01->CONTRATO+"' AND
		cQuery += " ZZ2_MEDICA = '"+T01->ZZ2_MEDICA+"' AND ZZ2_CLIENT = '"+T01->ZZ2_CLIENT+"' AND  "
		cQuery += " ZZ2_LOJA = '"+T01->ZZ2_LOJA+"' AND ZZ2.D_E_L_E_T_ = '' AND F1.D_E_L_E_T_ = '' AND D1.D_E_L_E_T_ = '' "

		if Select("T04") > 0
			dbSelectArea("T04")
			dbCloseArea()
		endif

		TcQuery cQuery New Alias T04

		IF !T04->(EOF())

			oSection:Cell("PC01"):SetValue(T04->PC01)
			oSection:Cell("NFE01"):SetValue(T04->NFE1)
			oSection:Cell("TESPC"):SetValue(T04->TESPC)
			oSection:Cell("CFOPPC"):SetValue(T04->CFOPPC)
			oSection:Cell("VALORPC"):SetValue(T04->VLNFE)

		ENDIF

		T04->(DbCloseArea())

		oSection:Cell("OBS"):SetValue(cOBS)

		oSection:PrintLine()

		T01->(DbSkip())

	ENDDO

	T01->(DbCloseArea())


	oSection:Finish()

Return

static function AjustaSX1(cPerg)

	aHelpPor	:= {}
	aHelpEng	:= {}
	aHelpSpa	:= {}

	PutSx1(cPerg, "01","Da Filial?","Da Filial?","Da Filial?","mv_ch1","G",6,0,0,"G","","SM0","","",;
		"mv_par01","","","","","","","","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)

	PutSx1(cPerg, "02","At� Filial?","At� Filial?","At� Filial?","mv_ch2","G",6,0,0,"G","","SM0","","",;
		"mv_par02","","","","","","","","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)

	PutSx1(cPerg, "03","Da Data?","Da Data?","Da Data?","mv_ch3","D",10,0,0,"G","","","","",;
		"mv_par03","","","","","","","","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)

	PutSx1(cPerg, "04","At� Data?","At� Data?","At� Data?","mv_ch4","D",10,0,0,"G","","","","",;
		"mv_par04","","","","","","","","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)

Return