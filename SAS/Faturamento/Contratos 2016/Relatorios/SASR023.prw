#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"
#Include "RWMAKE.CH"
#include 'parmtype.ch'
#INCLUDE "TBICONN.CH"
#INCLUDE "TBICODE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

User Function RF023()

	Local aArea		 := GetArea()
	Local aCoors	 := FwGetDialogSize( oMainWnd )
	Local oFWLayer,oPanelTop1,oView
	Local cOk        := "B"
	Local aFiltros   := {}
	Local bBlockClique
	Private oBrowse
	Private aMedicao := {}
	Static nFiliala  := 1
	Static nMedia    := 2
	Static nCodClia  := 3
	Static nljClia   := 4
	Static nNomClia  := 5
	Static nDtGera   := 6
	Static nDtMaxa   := 7
	Static nStatusa  := 8
	Static nobsa     := 9
	Static nNumeroa  := 10
	Static nLocExa   := 11
	Static nOk       := 12
	Static nEtapa    := 13
	Static nImpres   := 14
	Static nTipo     := 15
	Static nDescTipo := 16
	Static nEspecie  := 17
	Static nDescEsp  := 18
	Static nTransp   := 19
	Static nUF       := 20
	Static nPV       := 21
	Static nFilPV    := 22
	Static nNFPV     := 23
	Static nStatus   := 24
	Static nUserAlt  := 25
	Static nNomeAlt  := 26
	Static nDataAlt  := 27
	Static nHora     := 28
	Static nDTSolic  := 29
	Static nDTConf   := 30
	Static nDtpret	 := 31
	Static nMun		 := 32	

	Private oDlg
	Private aRotina	:= {}
	//Prepare Environment Empresa "01" Filial "0101" TABLES  "ZZ2","SA1"
	bBlockClique := {|| Clique2() }

	aMedicao := U_RF23iTEM(aMedicao)

	//If TYPE("_aArmario")<>"A"
	//	Public _aArmario	:= {}
	//EndIf

	oDlg		:= MSDialog():New( aCoors[1],aCoors[2],aCoors[3],aCoors[4],"Planejamento e Controle de Expedi��o",,,.F.,,,,,,.T.,,,.T. )
	//oDlg		:= MSDialog():New( 0,0,500,700,"Planejamento e Controle de Expedi��o",,,.F.,,,,,,.T.,,,.T. )
	oFWLayer	:= FWLayer():New()
	oFWLayer:Init( oDlg, .F., .T. )
	oFWLayer:AddLine( 'TOP1', 100, .F. )
	oPanelTop1 := oFWLayer:getLinePanel( 'TOP1' )

	oBrowse	    := FWFormBrowse():New()
	oMarkBrow	:= oBrowse:FWBrowse()
	oMarkBrow:SetDataArray()
	oMarkBrow:SetArray(@aMedicao)
	oMarkBrow:SetDescription('Medi��es')
	oMarkBrow:SetOwner( oDlg )
	oMarkBrow:SetProfileID("4")
	oMarkBrow:SetMenuDef("")
	oMarkBrow:SetDoubleClick({|| nil })
	//oMarkBrow:AddButton('Incluir', 'U_RVENX04B()', , 4, 0)
	//oMarkBrow:AddButton('Excluir', 'U_RVENX04A()', , 4, 0)
	//oMarkBrow:SetDelete(.T.,{||U_RVENX04A()})
	bBlockClique	:= {|| U_CLICK23() }
	//bBlockClique	:= {|| IF(Empty(aMedicao[oMarkBrow:At()][nOk]),aMedicao[oMarkBrow:At()][nOk]:= 1,aMedicao[oMarkBrow:At()][nOk] := nil) }

	// bot�es

	oMarkBrow:AddButton("Atualizar",{||YATUALIZ(aMedicao,oMarkBrow)},,,)
	oMarkBrow:AddButton("Bloquear",{||YBLOQMED(aMedicao,oMarkBrow,1)},,,)
	oMarkBrow:AddButton("Desbloquear",{||YDSBLQMED(aMedicao,oMarkBrow,1)},,,)
	oMarkBrow:AddButton("Transportadora",{||YTRANSP(aMedicao,oMarkBrow,1)},,,)
	oMarkBrow:AddButton("Remover Transp.",{||YRMVTRANSP(aMedicao,oMarkBrow,1)},,,)
	oMarkBrow:AddButton("Confirmar ",{||YCONFIRMA(aMedicao,oMarkBrow,1)},,,)
	oMarkBrow:AddButton("Cancelar Confirm. ",{||YDESCONFIRMA(aMedicao,oMarkBrow,1)},,,)
	oMarkBrow:AddButton("Pedido 01 ",{||YPED01(aMedicao,oMarkBrow)},,,)
	oMarkBrow:AddButton("Pedido 02 ",{||YPED02(aMedicao,oMarkBrow)},,,)
	oMarkBrow:AddButton("Pedido 03 ",{||YPED03(aMedicao,oMarkBrow)},,,)
	oMarkBrow:AddButton("Alterar Medi��o ",{||YALTMED(aMedicao,oMarkBrow)},,,)
	oMarkBrow:AddButton("Impressao ",{||YIMPRIMIR(aMedicao,oMarkBrow)},,,)
	oMarkBrow:AddButton("Historico ",{||YHISTOR(aMedicao,oMarkBrow)},,,)
	oMarkBrow:AddButton("Placa de embarque ",{||YPLACA(aMedicao,oMarkBrow)},,,)
	oMarkBrow:AddButton("Ajusta Volume ",{||AjTOTAL()},,,)
	oMarkBrow:AddButton("Nota PDV ",{||NotaPDV()},,,)

	// FILTROS
	oBrowse:SetUseFilter()

	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nFiliala)+"]",GetSx3Cache("ZZ2_FILIAL","X3_TITULO"),"C",GetSx3Cache("ZZ2_FILIAL","X3_TAMANHO"),0,GetSx3Cache("ZZ2_FILIAL","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nMedia)+"]",  GetSx3Cache("ZZ2_MEDICAO","X3_TITULO"),"C",GetSx3Cache("ZZ2_MEDICAO","X3_TAMANHO"),0,GetSx3Cache("ZZ2_MEDICAO","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nCodClia)+"]",GetSx3Cache("ZZ2_CLIENT","X3_TITULO"),"C",GetSx3Cache("ZZ2_CLIENT","X3_TAMANHO"),0,GetSx3Cache("ZZ2_CLIENT","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nljClia)+"]", GetSx3Cache("ZZ2_LOJA","X3_TITULO"),"C",GetSx3Cache("ZZ2_LOJA","X3_TAMANHO"),0,GetSx3Cache("ZZ2_LOJA","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nDtGera)+"]", GetSx3Cache("ZZ2_DTGER","X3_TITULO"),"D",GetSx3Cache("ZZ2_DTGER","X3_TAMANHO"),0,GetSx3Cache("ZZ2_DTGER","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nDtMaxa)+"]", GetSx3Cache("ZZ2_DTMAX","X3_TITULO"),"D",GetSx3Cache("ZZ2_DTMAX","X3_TAMANHO"),0,GetSx3Cache("ZZ2_DTMAX","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nStatusa)+"]",GetSx3Cache("ZZ2_STATOS","X3_TITULO"),"C",GetSx3Cache("ZZ2_STATOS","X3_TAMANHO"),0,GetSx3Cache("ZZ2_STATOS","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nobsa)+"]",   GetSx3Cache("ZZ2_OBS","X3_TITULO"),"C",GetSx3Cache("ZZ2_OBS","X3_TAMANHO"),0,GetSx3Cache("ZZ2_OBS","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nNumeroa)+"]",GetSx3Cache("ZZ2_NUMERO","X3_TITULO"),"C",GetSx3Cache("ZZ2_NUMERO","X3_TAMANHO"),0,GetSx3Cache("ZZ2_NUMERO","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nLocExa)+"]", GetSx3Cache("ZZ2_FATEXP","X3_TITULO"),"C",GetSx3Cache("ZZ2_FATEXP","X3_TAMANHO"),0,GetSx3Cache("ZZ2_FATEXP","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nEtapa)+"]",  GetSx3Cache("ZZ2_ETAPA","X3_TITULO"),"C",GetSx3Cache("ZZ2_ETAPA","X3_TAMANHO"),0,GetSx3Cache("ZZ2_ETAPA","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nImpres)+"]", GetSx3Cache("ZZ2_IMPRES","X3_TITULO"),"C",GetSx3Cache("ZZ2_IMPRES","X3_TAMANHO"),0,GetSx3Cache("ZZ2_IMPRES","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nTransp)+"]", GetSx3Cache("ZZ2_TRANSP","X3_TITULO"),"C",GetSx3Cache("ZZ2_TRANSP","X3_TAMANHO"),0,GetSx3Cache("ZZ2_TRANSP","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nTipo)+"]", GetSx3Cache("ZZ2_TIPO","X3_TITULO"),"C",GetSx3Cache("ZZ2_TIPO","X3_TAMANHO"),0,GetSx3Cache("ZZ2_TIPO","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nEspecie)+"]", GetSx3Cache("ZZG_ESPCTR","X3_TITULO"),"C",GetSx3Cache("ZZG_ESPCTR","X3_TAMANHO"),0,GetSx3Cache("ZZG_ESPCTR","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nUF)+"]", GetSx3Cache("A1_EST","X3_TITULO"),"C",GetSx3Cache("A1_EST","X3_TAMANHO"),0,GetSx3Cache("A1_EST","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nStatus)+"]", GetSx3Cache("ZZ2_STATUS","X3_TITULO"),"C",GetSx3Cache("ZZ2_STATUS","X3_TAMANHO"),0,GetSx3Cache("ZZ2_STATUS","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nDTSolic)+"]", GetSx3Cache("ZZ2_DTSOLI","X3_TITULO"),"D",GetSx3Cache("ZZ2_DTSOLI","X3_TAMANHO"),0,GetSx3Cache("ZZ2_DTSOLI","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nDTConf)+"]", GetSx3Cache("ZZ2_DTCONF","X3_TITULO"),"D",GetSx3Cache("ZZ2_DTCONF","X3_TAMANHO"),0,GetSx3Cache("ZZ2_DTCONF","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nLocExa)+"]", GetSx3Cache("ZZ2_LOCEX","X3_TITULO"),"C",GetSx3Cache("ZZ2_LOCEX","X3_TAMANHO"),0,GetSx3Cache("ZZ2_LOCEX","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nMun)+"]", GetSx3Cache("A1_MUN","X3_TITULO"),"C",GetSx3Cache("A1_MUN","X3_TAMANHO"),0,GetSx3Cache("A1_MUN","X3_PICTURE")} )
	
	oBrowse:SetFieldFilter(aFiltros)

	//oMarkBrow: AddFilter  ( "Local de Expedicao",aMedicao[oMarkBrow:At()][nFiliala]=='020101', .T.,,,.T.,,)

	// legenda
	oMarkBrow:AddLegend({||aMedicao[oMarkBrow:At()][nStatusa]=="CB"}  ,"BR_PRETO",     "Cliente Bloqueado")
	oMarkBrow:AddLegend({||aMedicao[oMarkBrow:At()][nStatusa]=="B"}   ,"BR_VERMELHO",  "Bloqueada Medi��o ")
	oMarkBrow:AddLegend({||aMedicao[oMarkBrow:At()][nStatusa]=="I"}   ,"BR_PINK",      "Indisponivel no Estoque ")
	oMarkBrow:AddLegend({||aMedicao[oMarkBrow:At()][nStatusa]=="OK"}  ,"BR_VERDE",     "OS Confirmada")
	oMarkBrow:AddLegend({||aMedicao[oMarkBrow:At()][nStatusa]=="SE"}  ,"BR_AMARELO",   "Sem Expedi�ao")
	oMarkBrow:AddLegend({||aMedicao[oMarkBrow:At()][nStatusa]=="AC"}   ,"BR_BRANCO",   "A Confirmar")


	// BOX
	oMarkBrow:AddMarkColumns(;
	{||If(aMedicao[oMarkBrow:At()][nOk]=="","LBNO","LBTICK")};	//Imagem da marca
	,bBlockClique/*{|| YINVERTE(oMarkBrow)}*/;
	,{|| YINVERTE(oMarkBrow)})


	//filial
	oMarkBrow:SetColumns({{;
	"Filial",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nFiliala],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_FILIAL","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_FILIAL","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	//Medicao
	oMarkBrow:SetColumns({{;
	"Medi�ao",;  					   				// T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nMedia],"")},;		// Code-Block de carga dos dados
	"C",;                       			// Tipo de dados
	"@!",;                       			// M�scara
	1,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_MEDICAO","X3_TAMANHO"),;                       			// Tamanho
	GetSx3Cache("ZZ2_MEDICAO","X3_DECIMAL"),;                       			// Decimal
	.F.,;                                       			// Indica se permite a edi��o
	{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       			// Indica se exibe imagem
	bBlockClique,;                     			// Code-Block de execu��o do duplo clique
	NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			// Code-Block de execu��o do clique no header
	.F.,;                                       			// Indica se a coluna est� deletada
	.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	//cod cliente
	oMarkBrow:SetColumns({{;
	"Cod Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nCodClia],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_CLIENT","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_CLIENT","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	//Loja Cliente
	oMarkBrow:SetColumns({{;
	"Loja Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nLjClia],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_LOJA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_LOJA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	// NOME CLIENTE
	oMarkBrow:SetColumns({{;
	"Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nNomClia],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A1_NOME","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("A1_NOME","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Estado do cliente
	oMarkBrow:SetColumns({{;
	"Estado",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),posicione("SA1",1,xFilial("SA1") + aMedicao[oMarkBrow:At()][nCodClia] + aMedicao[oMarkBrow:At()][nLjClia],"A1_EST"),"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A1_EST","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("A1_EST","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Municipio do cliente
	oMarkBrow:SetColumns({{;
	"Municipio",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),posicione("SA1",1,xFilial("SA1") + aMedicao[oMarkBrow:At()][nCodClia] + aMedicao[oMarkBrow:At()][nLjClia],"A1_MUN"),"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A1_MUN","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("A1_MUN","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//DATA GERA��O DA MEDICAO
	oMarkBrow:SetColumns({{;
	"Data Ger.Medi��o",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nDtGera],"")},;  // Code-Block de carga dos dados
	"D",;                                                        	   // Tipo de dados
	"",;                       			                               // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_DTGER","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_DTGER","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	// DATA MAXIMA
	oMarkBrow:SetColumns({{;
	"Data M�xima",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nDtMaxa],"")},;  // Code-Block de carga dos dados
	"D",;                                                        	   // Tipo de dados
	"",;                       			                               // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_DTMAX","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_DTMAX","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	//Status
	oMarkBrow:SetColumns({{;
	"Status OS",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nStatusa],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_STATOS","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_STATOS","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)	
	
	// Medi��o transferida
	oMarkBrow:SetColumns({{;
	"Medi��o Transf.",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),IIF(Empty(aMedicao[oMarkBrow:At()][nDtpret]),"N�o", "Sim"),"")},;  							// Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	3,;                       	    									// Tamanho
	0,;        									                	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// TIPO
	oMarkBrow:SetColumns({{;
	"Tipo ",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nTipo],"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_TIPO","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_TIPO","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// DESCRI��O TIPO
	oMarkBrow:SetColumns({{;
	"Desc. Tipo",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nDescTipo],"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZG_DESCRI","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZG_DESCRI","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//ESPECIE
	oMarkBrow:SetColumns({{;
	"Cod.Especie",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nEspecie],"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZG_ESPCTR","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZG_ESPCTR","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//DESC. ESPECIE
	oMarkBrow:SetColumns({{;
	"Tipo Medicao",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nDescEsp],"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZL_DESCR","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZL_DESCR","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//Transportadora
	oMarkBrow:SetColumns({{;
	"Transportadora",;  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nTransp] +" "+ U_YDESTRA(aMedicao[oMarkBrow:At()][nTransp]) ,"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A4_NOME","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("A4_NOME","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// obs
	oMarkBrow:SetColumns({{;
	"Obs",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nobsa],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_OBS","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_OBS","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	// Numero do Contrato
	oMarkBrow:SetColumns({{;
	"Numero",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nNumeroa],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_NUMERO","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_NUMERO","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	// Local Expedicao
	oMarkBrow:SetColumns({{;
	"Local Expedicao",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao), aMedicao[oMarkBrow:At()][nLocExa]+" - "+POSICIONE("SX5",1,XFILIAL("SX5")+"X2"+aMedicao[oMarkBrow:At()][nLocExa],"X5_DESCRI") ,"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_LOCEX","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_LOCEX","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// ETAPA
	oMarkBrow:SetColumns({{;
	"Etapa ",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nEtapa],"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_ETAPA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_ETAPA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Impressa ?
	oMarkBrow:SetColumns({{;
	"Impressa ",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),iif(aMedicao[oMarkBrow:At()][nImpres]=="1","Sim","Nao"),"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_ETAPA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_ETAPA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Peso Total
	oMarkBrow:SetColumns({{;
	"Peso Liquido",;			  					   			               // T�tulo da coluna
	{|| POSICIONE("SC5",1,XFILIAL("SC5",aMedicao[oMarkBrow:At()][nFilPV] ) + aMedicao[oMarkBrow:At()][nPV] , "SC5->C5_PESOL") },;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@E 999,999.9999",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_PESOL","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C5_PESOL","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Valor total
	oMarkBrow:SetColumns({{;
	"Valor Total",;			  					   			               // T�tulo da coluna
	{||ROUND( GetValor( aMedicao[oMarkBrow:At()][nFilPV], aMedicao[oMarkBrow:At()][nPV] ),2) },;   // Code-Block de carga dos dados
	"N",;                                                        	   // Tipo de dados
	"@E 999,999,999.99",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C6_VALOR","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C6_VALOR","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Status
	oMarkBrow:SetColumns({{;
	"Status",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nStatus],"") },;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_ETAPA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_ETAPA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// USER ALTERA�AO
	oMarkBrow:SetColumns({{;
	"Usuario Alt",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nUserAlt],"") },;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_USERAL","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_USERAL","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Nome Usuario
	oMarkBrow:SetColumns({{;
	"Nome Usuario",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nNomeAlt],"") },;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_NOMEAL","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_NOMEAL","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Data Altera�ao
	oMarkBrow:SetColumns({{;
	"Data Alt",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nDataAlt],"") },;   // Code-Block de carga dos dados
	"D",;                                                        	   // Tipo de dados
	GetSx3Cache("ZZ2_DTALTE","X3_PICTURE"),;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_DTALTE","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_DTALTE","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Hora Altera�ao
	oMarkBrow:SetColumns({{;
	"Hora Alt",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nHora],"") },;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	GetSx3Cache("ZZ2_HORAAL","X3_PICTURE"),;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_HORAAL","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_HORAAL","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)


	// Data Solicitacao
	oMarkBrow:SetColumns({{;
	"Dt Solic",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nDTSolic],"") },;   // Code-Block de carga dos dados
	"D",;                                                        	   // Tipo de dados
	GetSx3Cache("ZZ2_DTSOLI","X3_PICTURE"),; 									                      	   // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_DTSOLI","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_DTSOLI","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	// Data Confirma��o
	oMarkBrow:SetColumns({{;
	"Dt Conf",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nDTConf],"") },;   // Code-Block de carga dos dados
	"D",;                                                        	   // Tipo de dados
	GetSx3Cache("ZZ2_DTCONF","X3_PICTURE"),; 									                      	   // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_DTCONF","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_DTCONF","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	

	oMarkBrow:Activate()
	oDlg:Activate(,,,.T.)
	//U_RVENX006()	//ATUALIZA DADOS DO SALDO CART�O
	RestArea(aArea)

	//Reset Environment
	aMedicao := {}
Return

User Function RF23ITEM(aMedicao)

	Local cArea   := GetArea()
	Local cStatus := ""
	Local cFilAcesso := ""
	Local cNomeUser
	Local aRetUser := {}
	Local cDepto
	Local cEspecie := ""
	Local cNomeUser := ""

	cNomeUser := substr(cUsuario,7,15)

	// Defino a ordem
	PswOrder(2) // Ordem de nome

	// Efetuo a pesquisa, definindo se pesquiso usu�rio ou grupo
	If PswSeek(cNomeUser,.T.)

		// Obtenho o resultado conforme vetor
		aRetUser := PswRet(2)

		cDepto   := upper(alltrim(aRetUser[1,12]))

	EndIf
	IF LEN(aRetUser) > 0
		For nAx := 1 to len(aRetUser[1][6])
			if nAx == 1
				cFilAcesso += "'"+substr(aRetUser[1][6][nAx],3,6)+"'"
			else
				cFilAcesso +=" , '"+substr(aRetUser[1][6][nAx],3,6)+"'"
			endif

		Next
	ENDIF


//	cQuery := " SELECT ZZ2_FILIAL ,ZZ2_MEDICA ,ZZ2_CLIENT, ZZ2_LOJA , ZZ2_DTGER , ZZ2_DTMAX , ZZ2_STATOS , ZZ2_OBS , ZZ2_NUMERO , ZZ2_LOCEX ,ZZ2_FATEXP, ZZ2_ETAPA,ZZ2_IMPRES, NULL AS OK , "
//	cQuery += " ZZ2_TRANSP, ZZ2_TIPO, ZZ2_PV01, ZZ2_PV01FL, ZZ2_PV01NF, ZZ2_STATUS , ZZ2_USERAL , ZZ2_NOMEAL , ZZ2_DTALTE , ZZ2_HORAAL, ZZ2_DTSOLI "
//	cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2  "
//	cQuery += " WHERE  ZZ2.D_E_L_E_T_ = ''  "
//	
	
cQuery := " SELECT ZZ2.ZZ2_FILIAL, ZZ2.ZZ2_MEDICA, ZZ2.ZZ2_CLIENT, ZZ2.ZZ2_LOJA, ZZ2.ZZ2_DTGER, ZZ2.ZZ2_DTMAX, ZZ2.ZZ2_STATOS, ZZ2.ZZ2_OBS, ZZ2.ZZ2_NUMERO, ZZ2.ZZ2_LOCEX, ZZ2.ZZ2_FATEXP, ZZ2.ZZ2_ETAPA, ZZ2.ZZ2_IMPRES, NULL AS OK, ZZ2.ZZ2_TRANSP,ZZ2.ZZ2_TIPO, "
cQuery += " ZZ2.ZZ2_PV01, ZZ2.ZZ2_PV01FL, ZZ2.ZZ2_PV01NF, ZZ2.ZZ2_STATUS, ZZ2.ZZ2_USERAL, ZZ2.ZZ2_NOMEAL, ZZ2.ZZ2_DTALTE, ZZ2.ZZ2_HORAAL, ZZ2.ZZ2_DTSOLI, ZZ2.ZZ2_DTCONF, ZZ2.ZZ2_DTPRET, ZZG.ZZG_ESPCTR, ZZG.ZZG_DESCRI, ZZL.ZZL_DESCR, SA1.A1_NOME, SA1.A1_EST, SA1.A1_MUN "
cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2 LEFT JOIN ZZG010 ZZG ON ZZG.ZZG_COD = ZZ2.ZZ2_TIPO AND ZZG.D_E_L_E_T_='' LEFT JOIN SA1010 SA1 ON SA1.A1_COD = ZZ2.ZZ2_CLIENT AND SA1.A1_LOJA = ZZ2.ZZ2_LOJA AND SA1.D_E_L_E_T_=''  "
cQuery += " LEFT JOIN ZZL010 ZZL ON ZZL.ZZL_COD = ZZG.ZZG_ESPCTR AND ZZL.D_E_L_E_T_='' WHERE ZZ2.D_E_L_E_T_ = '' "
	
	
	if cFilAcesso <> ""
		cQuery += " AND    ZZ2_FILIAL IN ("+cFilAcesso+")  "
	endif
	cQuery += " ORDER BY ZZ2.ZZ2_FILIAL, ZZ2.ZZ2_NUMERO, ZZ2.ZZ2_MEDICA	  "
	TcQuery cQuery New Alias T01

	WHILE !T01->(EOF())
		cStatus := ""
		// VIRIFICA��O DO BLOQUEIO DE CLIENTE

		if ALLTRIM(T01->ZZ2_STATuS) == "F"
			cStatus := "OK"
		elseif Clien23(T01->ZZ2_FILIAL,T01->ZZ2_MEDICA,T01->ZZ2_CLIENT,T01->ZZ2_LOJA,T01->ZZ2_NUMERO)
			cStatus := "CB"
		elseif 	alltrim(T01->ZZ2_STATOS) == "B"
			cStatus := "B"
		elseif T01->ZZ2_FATEXP == "1"
			cStatus := "SE"
		elseif T01->ZZ2_STATOS == "OK"
			cStatus := "OK"
		else
			cStatus := "AC"
		endif

		cEspecie := T01->ZZG_ESPCTR //POSICIONE("ZZG",1,XFILIAL("ZZG") + T01->ZZ2_TIPO, "ZZG->ZZG_ESPCTR" )

		aadd(aMedicao,{ALLTRIM(T01->ZZ2_FILIAL),;
		ALLTRIM(T01->ZZ2_MEDICA),;
		ALLTRIM(T01->ZZ2_CLIENT),;
		ALLTRIM(T01->ZZ2_LOJA),;
		ALLTRIM(T01->A1_NOME ) ,;//POSICIONE("SA1",1,XFILIAL("SA1")+T01->ZZ2_CLIENT+T01->ZZ2_LOJA ,"A1_NOME")
		STOD(ALLTRIM(T01->ZZ2_DTGER)) ,;
		STOD(ALLTRIM(T01->ZZ2_DTMAX)) ,;
		ALLTRIM(cStatus) ,;
		ALLTRIM(T01->ZZ2_OBS) ,;
		ALLTRIM(T01->ZZ2_NUMERO) ,;
		ALLTRIM(T01->ZZ2_LOCEX)  ,;
		"",;
		ALLTRIM(T01->ZZ2_ETAPA),;
		ALLTRIM(T01->ZZ2_IMPRES),;
		ALLTRIM(T01->ZZ2_TIPO),;//Tipo
		ALLTRIM(T01->ZZG_DESCRI),;//Descri��o Tipo - POSICIONE("ZZG",1,XFILIAL("ZZG") + T01->ZZ2_TIPO, "ZZG->ZZG_DESCRI" )
		ALLTRIM(cEspecie),;//Especie
		ALLTRIM(T01->ZZL_DESCR ),;//Descri��o Especie - POSICIONE("ZZL",1,XFILIAL("ZZL") + cEspecie, "ZZL->ZZL_DESCR" )
		ALLTRIM(T01->ZZ2_TRANSP),;
		ALLTRIM(T01->A1_EST),;//posicione("SA1",1,xFilial("SA1") + T01->ZZ2_CLIENT + T01->ZZ2_LOJA,"A1_EST")
		T01->ZZ2_PV01,;
		T01->ZZ2_PV01FL,;
		T01->ZZ2_PV01NF,;
		T01->ZZ2_STATUS,;
		T01->ZZ2_USERAL ,;
		T01->ZZ2_NOMEAL ,;
		T01->ZZ2_DTALTE ,;
		T01->ZZ2_HORAAL ,;
		STOD(ALLTRIM(T01->ZZ2_DTSOLI)) ,;
		STOD(ALLTRIM(T01->ZZ2_DTCONF)),;
		T01->ZZ2_DTPRET ,;
		T01->A1_MUN})
		T01->(dbSkip())

	ENDDO

	T01-> (dbclosearea())

Return aMedicao

USER FUNCTION CLICK23()

	IF(aMedicao[oMarkBrow:At()][nOk]=="",aMedicao[oMarkBrow:At()][nOk]:= "1",aMedicao[oMarkBrow:At()][nOk] := "")

RETURN

STATIC FUNCTION YATUALIZ(aMedicao,oMarkBrow)

	/*Local cQuery := ""
	Local cFilAcesso := ""

	aMedicao := {}

	cNomeUser := substr(cUsuario,7,15)

	// Defino a ordem
	PswOrder(2) // Ordem de nome

	// Efetuo a pesquisa, definindo se pesquiso usu�rio ou grupo
	If PswSeek(cNomeUser,.T.)

	// Obtenho o resultado conforme vetor
	aRetUser := PswRet(2)

	cDepto   := upper(alltrim(aRetUser[1,12]))

	EndIf
	IF LEN(aRetUser) > 0
	For nAx := 1 to len(aRetUser[1][6])
	if nAx == 1
	cFilAcesso += "'"+substr(aRetUser[1][6][nAx],3,6)+"'"
	else
	cFilAcesso +=" , '"+substr(aRetUser[1][6][nAx],3,6)+"'"
	endif

	Next
	ENDIF

	cQuery := " SELECT ZZ2_FILIAL ,ZZ2_MEDICA ,ZZ2_CLIENT, ZZ2_LOJA , ZZ2_DTGER , ZZ2_DTMAX , ZZ2_STATOS , ZZ2_OBS , ZZ2_NUMERO , ZZ2_LOCEX ,ZZ2_FATEXP, ZZ2_ETAPA,ZZ2_IMPRES, NULL AS OK , "
	cQuery += " ZZ2_TRANSP, ZZ2_TIPO, ZZ2_PV01, ZZ2_PV01FL, ZZ2_PV01NF, ZZ2_STATUS , ZZ2_USERAL , ZZ2_NOMEAL , ZZ2_DTALTE , ZZ2_HORAAL "
	cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2  "
	cQuery += " WHERE  ZZ2.D_E_L_E_T_ = ''  "
	if cFilAcesso <> ""
	cQuery += " AND    ZZ2_FILIAL IN ("+cFilAcesso+")  "
	endif
	cQuery += " ORDER BY ZZ2_FILIAL , ZZ2_NUMERO , ZZ2_MEDICA  "
	TcQuery cQuery New Alias T01

	WHILE !T01->(EOF())
	cStatus := ""
	// VIRIFICA��O DO BLOQUEIO DE CLIENTE

	if Clien23(T01->ZZ2_FILIAL,T01->ZZ2_MEDICA,T01->ZZ2_CLIENT,T01->ZZ2_LOJA,T01->ZZ2_NUMERO)
	cStatus := "CB"
	elseif 	alltrim(T01->ZZ2_STATOS) == "B"
	cStatus := "B"
	elseif T01->ZZ2_FATEXP == "1"
	cStatus := "SE"
	elseif T01->ZZ2_STATOS == "OK"
	cStatus := "OK"
	else
	cStatus := "AC"
	endif

	cEspecie := POSICIONE("ZZG",1,XFILIAL("ZZG") + T01->ZZ2_TIPO, "ZZG->ZZG_ESPCTR" )

	aadd(aMedicao,{ALLTRIM(T01->ZZ2_FILIAL),;
	ALLTRIM(T01->ZZ2_MEDICA),;
	ALLTRIM(T01->ZZ2_CLIENT),;
	ALLTRIM(T01->ZZ2_LOJA),;
	ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+T01->ZZ2_CLIENT+T01->ZZ2_LOJA ,"A1_NOME")) ,;
	STOD(ALLTRIM(T01->ZZ2_DTGER)) ,;
	STOD(ALLTRIM(T01->ZZ2_DTMAX)) ,;
	ALLTRIM(cStatus) ,;
	ALLTRIM(T01->ZZ2_OBS) ,;
	ALLTRIM(T01->ZZ2_NUMERO) ,;
	ALLTRIM(T01->ZZ2_LOCEX)  ,;
	"",;
	ALLTRIM(T01->ZZ2_ETAPA),;
	ALLTRIM(T01->ZZ2_IMPRES),;
	ALLTRIM(T01->ZZ2_TIPO),;//Tipo
	ALLTRIM(POSICIONE("ZZG",1,XFILIAL("ZZG") + T01->ZZ2_TIPO, "ZZG->ZZG_DESCRI" ) ),;//Descri��o Tipo
	ALLTRIM(cEspecie),;//Especie
	ALLTRIM(POSICIONE("ZZL",1,XFILIAL("ZZL") + cEspecie, "ZZL->ZZL_DESCR" ) ),;//Descri��o Especie
	ALLTRIM(T01->ZZ2_TRANSP),;
	ALLTRIM(posicione("SA1",1,xFilial("SA1") + T01->ZZ2_CLIENT + T01->ZZ2_LOJA,"A1_EST")),;
	T01->ZZ2_PV01,;
	T01->ZZ2_PV01FL,;
	T01->ZZ2_PV01NF,;
	T01->ZZ2_STATUS,;
	T01->ZZ2_USERAL ,;
	T01->ZZ2_NOMEAL ,;
	T01->ZZ2_DTALTE ,;
	T01->ZZ2_HORAAL })
	//ALLTRIM(T01->ZZ2_STATOS) ,;
	T01->(dbSkip())
	ENDDO

	T01-> (dbclosearea())

	oMarkBrow:SetArray(@aMedicao)
	oMarkBrow:refresh()
	oBrowse:refresh()
	oMarkBrow:goto(1,.T.)*/

	aMedicao:= {}
	U_RF23ITEM(aMedicao)
	oMarkBrow:SetArray(@aMedicao)
	oMarkBrow:refresh()
	oBrowse:refresh()
	oMarkBrow:goto(1,.T.)


Return


// DESBLOQUEIO
STATIC FUNCTION YDSBLQMED(aMedicao,oMarkBrow,posicao)

	//Local oModel:= FWModelActive()
	Local aDesBloqueio := {}
	Local cMedBloq  := ""
	Local nAx := 1
	Local cArea := GetArea()
	Local cStatus := ""

	FOR nAx := 1 to LEN(aMedicao)
		if aMedicao[nAx][nOk] == "1"

			IF aMedicao[nAx][nStatusa] <> "B"
				ALERT("O DESBLOQUEIO � apenas para medi��es BLOQUEADAS !")
				YDESMARCA()
				oMarkBrow:refresh()
				RETURN

			ELSE
				aadd(aDesBloqueio,{aMedicao[nAx][nFiliala],;
				aMedicao[nAx][nMedia],;
				aMedicao[nAx][nCodClia],;
				aMedicao[nAx][nljClia],;
				aMedicao[nAx][nNomClia],;
				aMedicao[nAx][nDtGera],;
				aMedicao[nAx][nDtMaxa],;
				aMedicao[nAx][nStatusa],;
				aMedicao[nAx][nobsa],;
				aMedicao[nAx][nNumeroa],;
				aMedicao[nAx][nLocExa],;
				aMedicao[nAx][nOk],;
				aMedicao[nAx][nEtapa],;
				nAx})

				if cMedBloq == ""
					cMedBloq += "'"+aMedicao[nAx][nMedia]+"'"
				else
					cMedBloq += ",'"+aMedicao[nAx][nMedia]+"'"
				endif
			ENDIF
		endif
	NEXT

	IF LEN(aDesBloqueio) == 0
		RETURN
	ENDIF

	for nAx:= 1 to len(aDesBloqueio)

		cquery  := " SELECT * "
		cquery  += " FROM "+ RETSQLNAME("ZZ2") +" ZZ2 "
		cquery += " WHERE ZZ2_FILIAL  = '"+aDesBloqueio[nAx][nFiliala]+"' "
		cquery += " AND ZZ2_MEDICA = '" +aDesBloqueio[nAx][nMedia]+ "' "
		cquery += " AND ZZ2_CLIENT = '" +aDesBloqueio[nAx][nCodClia]+  "' "
		cquery += " AND ZZ2_LOJA = '" +aDesBloqueio[nAx][nljClia]+  "' "
		cquery += " AND ZZ2_NUMERO = '" +aDesBloqueio[nAx][nNumeroa]+  "' "
		cquery += " AND ZZ2_STATOS <> 'B' "
		cquery += " AND D_E_L_E_T_ = '' "
		TCQuery cquery new alias T02

		IF !T02->(EOF())
			alert('Medi��o n�o est� bloqueada : '+T02->ZZ2_MEDICA)
			T02->(DbCloseArea())
			Return
		ELSEIF T02->(EOF())

			if aDesBloqueio[nAx][nLocExa] == "1"
				cStatus := "OK"
				aMedicao[aDesBloqueio[nAx][14]][nStatusa] := "OK"
			else
				cStatus := "D"
				aMedicao[aDesBloqueio[nAx][14]][nStatusa] := "D"

				dbSelectArea("ZZ2")
				dbSetOrder(1)      // ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICAO+ZZ2_CLIENT+ZZ2_LOJA
				dbSeek(aDesBloqueio[nAx][nFiliala] + aDesBloqueio[nAx][nNumeroa] + aDesBloqueio[nAx][nMedia] + aDesBloqueio[nAx][nCodClia] + aDesBloqueio[nAx][nljClia] )     // Busca exata

				IF FOUND()    // Avalia o retorno da pesquisa realizada
					RECLOCK("ZZ2", .F.)

					ZZ2_STATOS := ZZ2-> ZZ2_STSANT

					MSUNLOCK()     // Destrava o registro
				ENDIF

			endif

		ENDIF
		T02->(DbCloseArea())
	next

	// GRAVANDO NA TABELA DE HITORICO

	FOR nAx := 1 to len(aDesBloqueio)
		dbSelectArea("ZZR")
		dbSetOrder(1)      // ZZR_FILIAL+ZZR_CONTRA+ZZR_MEDICA+ZZRETAPA


		IF ! dbSeek(aDesBloqueio[nAx][nFiliala] + aDesBloqueio[nAx][nNumeroa] + aDesBloqueio[nAx][nMedia] + aDesBloqueio[nAx][nEtapa] )     // Busca exata
			RECLOCK("ZZR", .T.)

			ZZR_FILIAL  := aDesBloqueio[nAx][nFiliala]
			ZZR_CONTRA  := aDesBloqueio[nAx][nNumeroa]
			ZZR_MEDICA  := aDesBloqueio[nAx][nMedia]
			ZZR_EMISSA  := POSICIONE("ZZ2",1,aDesBloqueio[nAx][nFiliala]+ aDesBloqueio[nAx][nNumeroa] + aDesBloqueio[nAx][nMedia] + aDesBloqueio[nAx][nCodClia] + aDesBloqueio[nAx][nljClia],"ZZ2_DTGER")
			ZZR_CODCLI  := aDesBloqueio[nAx][nCodClia]
			ZZR_NOMECL  := POSICIONE("SA1",1,xFilial("SA1")+aDesBloqueio[nAx][nCodClia]+aDesBloqueio[nAx][nljClia],"A1_NOME")
			ZZR_CODUSE  := RetCodUsr()
			ZZR_NOMUSE  := UsrFullName(RetCodUsr())
			ZZR_ETAPA   := aDesBloqueio[nAx][nEtapa]
			ZZR_NOMETP  := POSICIONE("SX5",1,XFILIAL("SX5")+'ZA'+aDesBloqueio[nAx][nEtapa],"X5_DESCRI")
			ZZR_DATA    := date()
			ZZR_HORA    := time()
			ZZR_OBS     := "Medi��o Desbloqueada "

			MSUNLOCK()     // Destrava o registro
		ENDIF
	NEXT
	//----------------------------------------

	//YATUEST(aMedicao,oMarkBrow)
	RestArea( cArea )
	YDESMARCA()
	YATUALIZ(aMedicao,oMarkBrow)
	oMarkBrow:goto(posicao,.T.)


RETURN

// fun�ao para bloqueio de OS

STATIC FUNCTION YBLOQMED(aMedicao,oMarkBrow,posicao)

	//Local oModel:= FWModelActive()
	Local aBloqueio := {}
	Local cMedBloq  := ""
	Local nAx := 1
	Local cArea := GetArea()

	FOR nAx := 1 to LEN(aMedicao)
		if aMedicao[nAx][nOk] == "1"

			IF aMedicao[nAx][nStatusa] == "B" .OR. aMedicao[nAx][nStatusa] == "CB" //.OR. aMedicao[nAx][nStatusa] == "OK"
				ALERT("O Bloqueio � permitido apenas para, medi��es com status Indisponivel no Estoque ,Disponivel no Estoque e Sem Expedi��o !")
				YDESMARCA()
				oMarkBrow:refresh()
				RETURN

			ELSE
				aadd(aBloqueio,{aMedicao[nAx][nFiliala],;
				aMedicao[nAx][nMedia],;
				aMedicao[nAx][nCodClia],;
				aMedicao[nAx][nljClia],;
				aMedicao[nAx][nNomClia],;
				aMedicao[nAx][nDtGera],;
				aMedicao[nAx][nDtMaxa],;
				aMedicao[nAx][nStatusa],;
				aMedicao[nAx][nobsa],;
				aMedicao[nAx][nNumeroa],;
				aMedicao[nAx][nLocExa],;
				aMedicao[nAx][nOk],;
				aMedicao[nAx][nEtapa],;
				nAx})

				if cMedBloq == ""
					cMedBloq += "'"+aMedicao[nAx][nMedia]+"'"
				else
					cMedBloq += ",'"+aMedicao[nAx][nMedia]+"'"
				endif
			ENDIF
		endif
	NEXT

	IF LEN(aBloqueio) == 0
		RETURN
	ENDIF

	for nAx:= 1 to len(aBloqueio)
		// verificar se ja existe pedido faturado para essa medi��o , caso exita impede a altera��o!
		cquery  := " SELECT * "
		cquery  += " FROM "+ RETSQLNAME("ZZ2") +" ZZ2 "
		cquery += " WHERE ZZ2_FILIAL  = '"+aBloqueio[nAx][nFiliala]+"' "
		cquery += " AND ZZ2_MEDICA = '" +aBloqueio[nAx][nMedia]+ "' "
		cquery += " AND ZZ2_CLIENT = '" +aBloqueio[nAx][nCodClia]+  "' "
		cquery += " AND ZZ2_LOJA = '" +aBloqueio[nAx][nljClia]+  "' "
		cquery += " AND ZZ2_NUMERO = '" +aBloqueio[nAx][nNumeroa]+  "' "
		cquery += " AND ZZ2_STATOS = 'B' "
		cquery += " AND D_E_L_E_T_ = '' "
		TCQuery cquery new alias T02

		IF !T02->(EOF())
			alert('Medi��o j� bloqueada : '+T02->ZZ2_MEDICA)
			T02->(DbCloseArea())
			Return
		ELSEIF T02->(EOF())

			dbSelectArea("ZZ2")
			dbSetOrder(1)      // ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICAO+ZZ2_CLIENT+ZZ2_LOJA
			dbSeek(aBloqueio[nAx][nFiliala] + aBloqueio[nAx][nNumeroa] + aBloqueio[nAx][nMedia] + aBloqueio[nAx][nCodClia] + aBloqueio[nAx][nljClia] )     // Busca exata

			IF FOUND()    // Avalia o retorno da pesquisa realizada
				RECLOCK("ZZ2", .F.)

				ZZ2_STSANT := ZZ2-> ZZ2_STATOS
				ZZ2_STATOS := "B"

				MSUNLOCK()     // Destrava o registro
			ENDIF

			for nAx := 1 to len(aBloqueio)

				if aMedicao[aBloqueio[nAx][14]][nStatusa] <> "CB"
					aMedicao[aBloqueio[nAx][14]][nStatusa] := "B"
				endif
			next
		ENDIF
		T02->(DbCloseArea())
	next

	// GRAVANDO NA TABELA DE HITORICO

	FOR nAx := 1 to len(aBloqueio)
		dbSelectArea("ZZR")
		dbSetOrder(1)      // ZZR_FILIAL+ZZR_CONTRA+ZZR_MEDICA+ZZRETAPA


		IF ! dbSeek(aBloqueio[nAx][nFiliala] + aBloqueio[nAx][nNumeroa] + aBloqueio[nAx][nMedia] + aBloqueio[nAx][nEtapa] )     // Busca exata
			RECLOCK("ZZR", .T.)

			ZZR_FILIAL  := aBloqueio[nAx][nFiliala]
			ZZR_CONTRA  := aBloqueio[nAx][nNumeroa]
			ZZR_MEDICA  := aBloqueio[nAx][nMedia]
			ZZR_CODCLI  := aBloqueio[nAx][nCodClia]
			ZZR_NOMECL  := POSICIONE("SA1",1,xFilial("SA1")+aBloqueio[nAx][nCodClia]+aBloqueio[nAx][nljClia],"A1_NOME")
			ZZR_CODUSE  := RetCodUsr()
			ZZR_NOMUSE  := UsrFullName(RetCodUsr())
			ZZR_ETAPA   := aBloqueio[nAx][nEtapa]
			ZZR_NOMETP  := POSICIONE("SX5",1,XFILIAL("SX5")+'ZA'+aBloqueio[nAx][nEtapa],"X5_DESCRI")
			ZZR_DATA    := date()
			ZZR_HORA    := time()
			ZZR_OBS     := "MEDICAO BLOQUEADA "

			MSUNLOCK()     // Destrava o registro
		ENDIF
	NEXT
	//----------------------------------------


	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
	oMarkBrow:goto(posicao,.T.)


RETURN


// FUN��O PARA ATUALIZAR AS TRANSPORTADORAS

STATIC FUNCTION YTRANSP(aMedicao,oMarkBrow)

	Local lOk	:= .F.
	Local cArea := GetArea()
	Local cTransportadora	:= Space(GetSx3Cache("A4_COD","X3_TAMANHO"))
	Local cModal	:= Space(GetSx3Cache("ZZ2_MODAL","X3_TAMANHO"))
	Local aModal    := {'AQUAVIARIO','RODOVIARIO','FERROVIARIO ','AEREO'}
	Local cTabFret	:= Space(GetSx3Cache("ZZ2_TABFRT","X3_TAMANHO"))
	Local aTabFret	:= {'EXPRESSO','CONVENCIONAL'}
	Local aParam := {}
	Local aRetParm	:= {}
	Local aTransp := {}
	Local dDataPre := ctod("")
	Local dDataMax := ctod("")
	Local cModal := ""
	Local cTab := ""

	aAdd(aParam,{1,"Transportadora"		,cTransportadora	,GetSx3Cache("A4_COD","X3_PICTURE"),"existCpo('SA4')","SA4",".T.",80,.F.})
	aAdd(aParam,{2,"Modal"      		,cModal	,aModal,80,,.F.,})
	aAdd(aParam,{1,"Valor cotado"  		,0	,"@E 999,999,999.99 ",'.T.',,".T.",80,.F.})
	aAdd(aParam,{2,"Tabela Frete" 		,cTabFret	,aTabFret,80,,.F.,})
	aAdd(aParam ,{1,"Data Previs�o",dDataPre,"",'.T.',"",'.T.',60,.F.})
	//aAdd(aParam ,{1,"Data M�xima",dDataMax,X3Picture("E1_EMISSAO"),'.T.',"",'.T.',60,.F.})

	If ParamBox(aParam,"Escolha da transportadora",@aRetParm,{||.T.},,,,,,"U_RF023",.T.,.T.)
		lOk	:= .T.
	EndIf

	if lOk

		IF aRetParm[2] == "AQUAVIARIO"
			cModal  := "AQ"
		ELSEIF	aRetParm[2] == "RODOVIARIO"
			cModal  := "RD"
		ELSEIF	aRetParm[2] == "FERROVIARIO"
			cModal  := "FR"
		ELSEIF	aRetParm[2] == "AEREO"
			cModal  := "AE"
		ENDIF

		IF aRetParm[4] == "EXPRESSO"
			cTab  := "1"
		ELSEIF	aRetParm[4] == "CONVENCIONAL"
			cTab  := "2"
		ENDIF

		FOR nAx := 1 to LEN(aMedicao)

			if aMedicao[nAx][nOk] == "1"

				DBSELECTAREA("ZZ2")
				DBSETORDER(1)
				DBSEEK(aMedicao[nAx][nFiliala] + aMedicao[nAx][nNumeroa] + aMedicao[nAx][nMedia] + aMedicao[nAx][nCodClia] + aMedicao[nAx][nljClia] )

				If Found()

					RECLOCK("ZZ2", .F.)

					ZZ2->ZZ2_DTPREV := aRetParm[5]
					//ZZ2->ZZ2_DTMAX := aRetParm[6]

					MSUNLOCK()

				EndIf

				cQuery := " SELECT ZZ2_PV01FL , ZZ2_PV01 , ZZ2_PV01NF , ZZ2_PV02FL , ZZ2_PV02 , ZZ2_PV03FL , ZZ2_PV03 "
				cQuery += " FROM "+RETSQLNAME("ZZ2")+" ZZ2 "
				cQuery += " WHERE D_E_L_E_T_=''  "
				cQuery += " AND  ZZ2_FILIAL = '"+aMedicao[nAx][nFiliala]+"' "
				cQuery += " AND  ZZ2_NUMERO = '"+aMedicao[nAx][nNumeroa]+"' "
				cQuery += " AND  ZZ2_MEDICA = '"+aMedicao[nAx][nMedia]+"' "
				cQuery += " AND  ZZ2_CLIENT = '"+aMedicao[nAx][nCodClia]+"' "
				cQuery += " AND  ZZ2_LOJA   = '"+aMedicao[nAx][nljClia]+"' "
				//TCQuery cquery new alias T08

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"T08",.F.,.T.)

				IF !T08->(EOF())

					IF ALLTRIM(T08->ZZ2_PV01NF) <> ""
						ALERT("Medi��o j� faturada, n�o pode ter transportadora alterada")
						T08->(DBCLOSEAREA())
						RestArea( cArea )
						YDESMARCA()
						oMarkBrow:refresh()
						RETURN
					ENDIF

					dbSelectArea("ZZ2")
					dbSetOrder(1)      // ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICAO+ZZ2_CLIENT+ZZ2_LOJA
					dbSeek(aMedicao[nAx][nFiliala] + aMedicao[nAx][nNumeroa] + aMedicao[nAx][nMedia] + aMedicao[nAx][nCodClia] + aMedicao[nAx][nljClia] )     // Busca exata

					IF FOUND()    // Avalia o retorno da pesquisa realizada
						RECLOCK("ZZ2", .F.)

						ZZ2->ZZ2_TRANSP := aRetParm[1]
						ZZ2->ZZ2_DESTRA := U_YDESTRA(aRetParm[1])
						ZZ2->ZZ2_MODAL  := cModal
						ZZ2->ZZ2_VALFRT := aRetParm[3]
						ZZ2->ZZ2_TABFRT := cTab

						MSUNLOCK()     // Destrava o registro
					ENDIF
					ZZ2->(DBCLOSEAREA())

					//  PEDIDO 1
					dbSelectArea("SC5")
					dbSetOrder(1)      // SC5_FILIAL + SC5_NUM
					dbSeek(T08->ZZ2_PV01FL + T08->ZZ2_PV01  )     // Busca exata

					IF FOUND()    // Avalia o retorno da pesquisa realizada
						RECLOCK("SC5", .F.)

						C5_TRANSP := aRetParm[1]

						MSUNLOCK()     // Destrava o registro
					ENDIF

					// PEDIDO 2
					dbSelectArea("SC5")
					dbSetOrder(1)      // SC5_FILIAL + SC5_NUM
					dbSeek(T08->ZZ2_PV02FL + T08->ZZ2_PV02  )     // Busca exata

					IF FOUND()    // Avalia o retorno da pesquisa realizada
						RECLOCK("SC5", .F.)

						C5_TRANSP := aRetParm[1]

						MSUNLOCK()     // Destrava o registro
					ENDIF

					// PEDIDO 3
					dbSelectArea("SC5")
					dbSetOrder(1)      // SC5_FILIAL + SC5_NUM
					dbSeek(T08->ZZ2_PV03FL + T08->ZZ2_PV03  )     // Busca exata

					IF FOUND()    // Avalia o retorno da pesquisa realizada
						RECLOCK("SC5", .F.)

						C5_TRANSP := aRetParm[1]

						MSUNLOCK()     // Destrava o registro
					ENDIF

				ENDIF

				T08->(DBCLOSEAREA())
				//STATUS DA GRID
				aMedicao[nAx][nTransp] := aRetParm[1]

			endif

		NEXT

		aRetParm :={}  //limpando array
		aParam   :={}  //limpando array

		RestArea( cArea )
	endif
	YDESMARCA()
	oMarkBrow:refresh()

RETURN

// FUN��O PARA REMOVER AS TRANSPORTADORA

STATIC FUNCTION YRMVTRANSP(aMedicao,oMarkBrow)
	Local cArea := GetArea()
	
	Local lRet := MsgBox("Deseja remover a transportadora da medi��o selecionada?", "Remover transportadora", "YESNO")
	
	If lRet
		FOR nAx := 1 to LEN(aMedicao)
			
				if aMedicao[nAx][nOk] == "1"
					
					DBSELECTAREA("ZZ2")
					DBSETORDER(1)
					DBSEEK(aMedicao[nAx][nFiliala] + aMedicao[nAx][nNumeroa] + aMedicao[nAx][nMedia] + aMedicao[nAx][nCodClia] + aMedicao[nAx][nljClia] )
	
					If Found()
	
						RECLOCK("ZZ2", .F.)
	
						ZZ2->ZZ2_DTPREV := Nil
	
						MSUNLOCK()
	
					EndIf
	
					cQuery := " SELECT ZZ2_PV01FL , ZZ2_PV01 , ZZ2_PV01NF , ZZ2_PV02FL , ZZ2_PV02 , ZZ2_PV03FL , ZZ2_PV03 "
					cQuery += " FROM "+RETSQLNAME("ZZ2")+" ZZ2 "
					cQuery += " WHERE D_E_L_E_T_=''  "
					cQuery += " AND  ZZ2_FILIAL = '"+aMedicao[nAx][nFiliala]+"' "
					cQuery += " AND  ZZ2_NUMERO = '"+aMedicao[nAx][nNumeroa]+"' "
					cQuery += " AND  ZZ2_MEDICA = '"+aMedicao[nAx][nMedia]+"' "
					cQuery += " AND  ZZ2_CLIENT = '"+aMedicao[nAx][nCodClia]+"' "
					cQuery += " AND  ZZ2_LOJA   = '"+aMedicao[nAx][nljClia]+"' "
					
	
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"T08",.F.,.T.)
	
					IF !T08->(EOF())
	
						IF ALLTRIM(T08->ZZ2_PV01NF) <> ""
							ALERT("Medi��o j� faturada, n�o pode ter transportadora alterada")
							T08->(DBCLOSEAREA())
							RestArea( cArea )
							YDESMARCA()
							oMarkBrow:refresh()
							RETURN
						ENDIF
	
						dbSelectArea("ZZ2")
						dbSetOrder(1)      // ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICAO+ZZ2_CLIENT+ZZ2_LOJA
						dbSeek(aMedicao[nAx][nFiliala] + aMedicao[nAx][nNumeroa] + aMedicao[nAx][nMedia] + aMedicao[nAx][nCodClia] + aMedicao[nAx][nljClia] )     // Busca exata
	
						IF FOUND()    // Avalia o retorno da pesquisa realizada
							RECLOCK("ZZ2", .F.)
	
							ZZ2->ZZ2_TRANSP := ""
							ZZ2->ZZ2_DESTRA := ""
							ZZ2->ZZ2_MODAL  := ""
							ZZ2->ZZ2_VALFRT := 0
							ZZ2->ZZ2_TABFRT := ""
	
							MSUNLOCK()     // Destrava o registro
						ENDIF
						ZZ2->(DBCLOSEAREA())
	
						//  PEDIDO 1
						dbSelectArea("SC5")
						dbSetOrder(1)      // SC5_FILIAL + SC5_NUM
						dbSeek(T08->ZZ2_PV01FL + T08->ZZ2_PV01  )     // Busca exata
	
						IF FOUND()    // Avalia o retorno da pesquisa realizada
							RECLOCK("SC5", .F.)
	
							C5_TRANSP := ""
	
							MSUNLOCK()     // Destrava o registro
						ENDIF
	
						// PEDIDO 2
						dbSelectArea("SC5")
						dbSetOrder(1)      // SC5_FILIAL + SC5_NUM
						dbSeek(T08->ZZ2_PV02FL + T08->ZZ2_PV02  )     // Busca exata
	
						IF FOUND()    // Avalia o retorno da pesquisa realizada
							RECLOCK("SC5", .F.)
	
							C5_TRANSP := ""
	
							MSUNLOCK()     // Destrava o registro
						ENDIF
	
						// PEDIDO 3
						dbSelectArea("SC5")
						dbSetOrder(1)      // SC5_FILIAL + SC5_NUM
						dbSeek(T08->ZZ2_PV03FL + T08->ZZ2_PV03  )     // Busca exata
	
						IF FOUND()    // Avalia o retorno da pesquisa realizada
							RECLOCK("SC5", .F.)
	
							C5_TRANSP := ""
	
							MSUNLOCK()     // Destrava o registro
						ENDIF
	
					ENDIF
	
					T08->(DBCLOSEAREA())				
	
				endif
	
			NEXT
	EndIf
	
	RestArea( cArea )
	
	YDESMARCA()
	oMarkBrow:refresh()
	

RETURN

// Fun��o para confirmar as medi��es selecionadas
//  - medi��es bloqueadas n�o podaram ser confirmadas
//  - medi��es com cliente bloqueado n�o pode ser confirmada
//  - medi��es com estoque indispon�vel n�o pode ser confirmadas
STATIC FUNCTION YCONFIRMA(aMedicao,oMarkBrow,cnum)

	Local aConfirma := {}
	Local nAx := 1
	Local cArea := GetArea()
	Local aPergs	:= {}
	Local dDataExp := ctod("")
	Local lOk := .F.
	Local aRetParm	:= {}

	aAdd( aPergs ,{1,"Data Expedi��o     : " ,dDataExp	,""					,'.T.',""			,'.T.',50	,.F.})

	If ParamBox(aPergs,"Expedi��o",@aRetParm,{||.T.},,,,,,"U_RF023",.T.,.T.)
		lOk	:= .T.
	EndIf

	FOR nAx := 1 to LEN(aMedicao)
		if aMedicao[nAx][nOk] == "1"

			// verificar se est� bloqueada
			if aMedicao[nAx][nStatusa] == "B" .OR. aMedicao[nAx][nStatusa] == "CB" .OR. aMedicao[nAx][nStatusa] == "I" .OR. aMedicao[nAx][nStatusa] == "OK"

				ALERT("A confirma��o � somente permitida para medi��es A Confirmar ou Sem Expedi��o!")
				YDESMARCA()
				oMarkBrow:refresh()
				RETURN

			ELSE
				aadd(aConfirma,{aMedicao[nAx][nFiliala],;
				aMedicao[nAx][nMedia]  ,;
				aMedicao[nAx][nCodClia],;
				aMedicao[nAx][nljClia] ,;
				aMedicao[nAx][nNomClia],;
				aMedicao[nAx][nDtGera] ,;
				aMedicao[nAx][nDtMaxa] ,;
				aMedicao[nAx][nStatusa],;
				aMedicao[nAx][nobsa]   ,;
				aMedicao[nAx][nNumeroa],;
				aMedicao[nAx][nLocExa] ,;
				aMedicao[nAx][nOk]     ,;
				aMedicao[nAx][nEtapa]  ,;
				nAx})

			endif
		endif
	NEXT

	FOR nAx := 1 to len(aConfirma)

		//atualizar array do grid
		aMedicao[aConfirma[nAx][14]][nStatusa] := "OK"

		//ATUALIZANDO O CADASTRO DE MEDI��O
		dbSelectArea("ZZ2")
		dbSetOrder(1)      // ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICAO+ZZ2_CLIENT+ZZ2_LOJA
		dbSeek(aConfirma[nAx][nFiliala] + aConfirma[nAx][nNumeroa] + aConfirma[nAx][nMedia] + aConfirma[nAx][nCodClia] + aConfirma[nAx][nljClia] )     // Busca exata

		IF FOUND()    // Avalia o retorno da pesquisa realizada
			RECLOCK("ZZ2", .F.)

			If lOk
				ZZ2->ZZ2_DTEXP := aRetParm[1]
			Else
				ZZ2->ZZ2_DTEXP := date()
			EndIf

			ZZ2_STATOS := "OK"
			ZZ2_DTCONF := date()
			
			if aConfirma[nAx][nStatusa] == "SE"
				ZZ2_ETAPA  := GETMV("SA_ETPFIM")
			else
				ZZ2_ETAPA  := GETMV("SA_ZZ2CONF")
			endif

			MSUNLOCK()     // Destrava o registro
		ENDIF

		//ATUALIZANDO O CADASTRO DE HIST�RICO
		dbSelectArea("ZZR")
		dbSetOrder(1)      // ZZR_FILIAL+ZZR_CONTRA+ZZR_MEDICA+ZZRETAPA


		
		RECLOCK("ZZR", .T.)

		ZZR_FILIAL := aConfirma[nAx][nFiliala]
		ZZR_CONTRA := aConfirma[nAx][nNumeroa]
		ZZR_MEDICA := aConfirma[nAx][nMedia]
		ZZR_EMISSA := POSICIONE("ZZ2",1,aConfirma[nAx][nFiliala] + aConfirma[nAx][nNumeroa] + aConfirma[nAx][nMedia] + aConfirma[nAx][nCodClia] + aConfirma[nAx][nljClia],"ZZ2_DTGER")
		ZZR_CODCLI := aConfirma[nAx][nCodClia]
		ZZR_NOMECL := POSICIONE("SA1",1,xFilial("SA1")+aConfirma[nAx][nCodClia]+aConfirma[nAx][nljClia],"A1_NOME")
		ZZR_CODUSE := RetCodUsr()
		ZZR_NOMUSE := UsrFullName(RetCodUsr())
		ZZR_ETAPA  := GETMV("SA_ETPMED")
		ZZR_NOMETP := POSICIONE("SX5",1,XFILIAL("SX5")+"ZA"+GETMV("SA_ZZ2CONF"),"X5_DESCRI")
		ZZR_DATA   := date()
		ZZR_HORA   := time()
		ZZR_OBS    := "MEDICAO CONFIRMADA "

		MSUNLOCK()     // Destrava o registro
	next

	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
RETURN

// Fun��o para desconfirmar as medi��es selecionadas
//  - Somente medi��es confirmadas e sem transfer�ncias podem ser desconfirmadas
STATIC FUNCTION YDESCONFIRMA(aMedicao,oMarkBrow,cnum)
	Local aConfirma := {}
	Local nAx := 1
	Local cArea := GetArea()
	Local aPergs	:= {}
	Local dDataExp := ctod("")
	Local lOk := .F.
	Local aRetParm	:= {}
	
	
	
	FOR nAx := 1 to LEN(aMedicao)
		if aMedicao[nAx][nOk] == "1"

			// verificar se est� confirmada
			if aMedicao[nAx][nStatusa] != "OK"

				ALERT("O cancelamento da confirma��o somente � permitido para medi��es confirmadas!")
				YDESMARCA()
				oMarkBrow:refresh()
				RETURN

			ELSE				
				if(!Empty(aMedicao[nAx][nDtpret]))
					ALERT("O cancelamento da confirma��o somente � permitido para medi��es n�o transferidas!")
					YDESMARCA()
					oMarkBrow:refresh()
					RETURN
				else
					aadd(aConfirma,{aMedicao[nAx][nFiliala],;
					aMedicao[nAx][nMedia]  ,;
					aMedicao[nAx][nCodClia],;
					aMedicao[nAx][nljClia] ,;
					aMedicao[nAx][nNomClia],;
					aMedicao[nAx][nDtGera] ,;
					aMedicao[nAx][nDtMaxa] ,;
					aMedicao[nAx][nStatusa],;
					aMedicao[nAx][nobsa]   ,;
					aMedicao[nAx][nNumeroa],;
					aMedicao[nAx][nLocExa] ,;
					aMedicao[nAx][nOk]     ,;
					aMedicao[nAx][nEtapa]  ,;
					nAx})
				endif
			endif
		endif
	NEXT

	FOR nAx := 1 to len(aConfirma)

		//atualizar array do grid
		aMedicao[aConfirma[nAx][14]][nStatusa] := "AC"

		//ATUALIZANDO O CADASTRO DE MEDI��O
		dbSelectArea("ZZ2")
		dbSetOrder(1)      // ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICAO+ZZ2_CLIENT+ZZ2_LOJA
		dbSeek(aConfirma[nAx][nFiliala] + aConfirma[nAx][nNumeroa] + aConfirma[nAx][nMedia] + aConfirma[nAx][nCodClia] + aConfirma[nAx][nljClia] )     // Busca exata

		IF FOUND()    // Avalia o retorno da pesquisa realizada
			RECLOCK("ZZ2", .F.)

			
			ZZ2->ZZ2_DTEXP := nil			
			ZZ2_STATOS := ""
			ZZ2_DTCONF := nil
			
			if aConfirma[nAx][nEtapa] == GETMV("SA_ETPFIM") .OR. aConfirma[nAx][nEtapa] == GETMV("SA_ZZ2CONF")
				ZZ2_ETAPA := GETMV("SA_ETPMED")
			endif

			MSUNLOCK()     // Destrava o registro
		ENDIF

		//ATUALIZANDO O CADASTRO DE HIST�RICO
		dbSelectArea("ZZR")
		dbSetOrder(1)      // ZZR_FILIAL+ZZR_CONTRA+ZZR_MEDICA+ZZRETAPA


		
		RECLOCK("ZZR", .T.)

		ZZR_FILIAL := aConfirma[nAx][nFiliala]
		ZZR_CONTRA := aConfirma[nAx][nNumeroa]
		ZZR_MEDICA := aConfirma[nAx][nMedia]
		ZZR_EMISSA := POSICIONE("ZZ2",1,aConfirma[nAx][nFiliala] + aConfirma[nAx][nNumeroa] + aConfirma[nAx][nMedia] + aConfirma[nAx][nCodClia] + aConfirma[nAx][nljClia],"ZZ2_DTGER")
		ZZR_CODCLI := aConfirma[nAx][nCodClia]
		ZZR_NOMECL := POSICIONE("SA1",1,xFilial("SA1")+aConfirma[nAx][nCodClia]+aConfirma[nAx][nljClia],"A1_NOME")
		ZZR_CODUSE := RetCodUsr()
		ZZR_NOMUSE := UsrFullName(RetCodUsr())
		ZZR_ETAPA  := aConfirma[nAx][nEtapa]
		ZZR_NOMETP := POSICIONE("SX5",1,XFILIAL("SX5")+"ZA"+"01","X5_DESCRI")
		ZZR_DATA   := date()
		ZZR_HORA   := time()
		ZZR_OBS    := "CANCELADA A CONFIRMA��O "

		MSUNLOCK()     // Destrava o registro
		
	next

	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
	
RETURN


// fun��o para validar o cliente e atualizar a legenda

STATIC function Clien23(cFil,cMedicao,cClien,cLoja,cNum)

	Local lRet := .F.

	cQuery := " select ZZ3_TES "
	cQuery += " FROM " + RETSQLNAME("ZZ3")+" ZZ3 "
	cQuery += " WHERE ZZ3.D_E_L_E_T_ =''  "
	cQuery += " AND   ZZ3.ZZ3_TES    IN ("+GETMV("SA_TESFIN")+") "
	cQuery += " AND   ZZ3.ZZ3_FILIAL = '"+cFil+"' "
	cQuery += " AND   ZZ3.ZZ3_MEDICA = '"+cMedicao+"' "
	cQuery += " AND   ZZ3.ZZ3_NUMERO = '"+cNum+"' "
	cQuery += " AND   ZZ3.ZZ3_CLIENT = '"+cClien+"' "
	cQuery += " AND   ZZ3.ZZ3_LOJA   = '"+cLoja+"' "
	//cQuery += " AND   ZZ3.ZZ3_TIPO   = 'K' "
	cQuery += " GROUP BY ZZ3.ZZ3_TES "
	TCQuery cquery new alias T04

	if !T04->(eof())
		if POSICIONE("SA1",1,XFILIAL("SA1")+cClien+cLoja,"A1_YBLOQ") == "S"
			lRet := .T.
		endif
	endif

	T04->(DbCloseArea())
RETURN lRet
//-----------------------------------------------------



// FUN��O PARA ABRIR O PEDIDO 01

STATIC FUNCTION YPED01(aMedicao,oMarkBrow)
	Local aPed01 := {}
	Local cfilatu := cFilant
	Local nCont  := 0
	Local cArea := GetArea()
	Private aRotina    := {}
	Private INCLUI      := .F.
	Private ALTERA      := .F.
	Private nTipoPed    := 1
	Private cCadastro   := ''
	Private l120Auto    := .F.
	Private LINTGC

	//--Monta o aRotina para compatibilizacao
	AAdd( aRotina, { '' , '' , 0, 1 } )
	AAdd( aRotina, { '' , '' , 0, 2 } )
	AAdd( aRotina, { '' , '' , 0, 3 } )
	AAdd( aRotina, { '' , '' , 0, 4 } )
	AAdd( aRotina, { '' , '' , 0, 5 } )


	FOR nAx := 1 to LEN(aMedicao)

		if aMedicao[nAx][nOk] == "1"
			nCont ++
			aadd(aPed01,{aMedicao[nAx][nFiliala],;
			aMedicao[nAx][nMedia]  ,;
			aMedicao[nAx][nCodClia],;
			aMedicao[nAx][nljClia] ,;
			aMedicao[nAx][nNomClia],;
			aMedicao[nAx][nDtGera] ,;
			aMedicao[nAx][nDtMaxa] ,;
			aMedicao[nAx][nStatusa],;
			aMedicao[nAx][nobsa]   ,;
			aMedicao[nAx][nNumeroa],;
			aMedicao[nAx][nLocExa] ,;
			aMedicao[nAx][nOk]     ,;
			aMedicao[nAx][nEtapa]  ,;
			nAx})
		endif
		if nCont > 1
			alert("Selecione apena uma medi��o para visualizar o Pedido 01")
			YDESMARCA()
			oMarkBrow:refresh()
			RETURN
		endif

	next

	if nCont == 0
		alert("Selecione uma medi��o para visualizar o Pedido 01")
		YDESMARCA()
		oMarkBrow:refresh()
		RETURN
	endif

	cquery := " SELECT C5_FILIAL,C5_NUM,SC5.R_E_C_N_O_ AS C5RECNO "
	cquery += " FROM "+ RETSQLNAME("ZZ2") +" ZZ2 ,"+ RETSQLNAME("SC5") +" SC5 "
	cquery += " WHERE ZZ2_FILIAL  = '"+aPed01[1][nFiliala]+"' "
	cquery += " AND ZZ2_MEDICA = '" +aPed01[1][nMedia]+ "' "
	cquery += " AND ZZ2_CLIENT = '" +aPed01[1][nCodClia]+  "' "
	cquery += " AND ZZ2_LOJA = '" +aPed01[1][nljClia]+  "' "
	cquery += " AND ZZ2_NUMERO = '" +aPed01[1][nNumeroa]+  "' "
	cquery += " AND ZZ2.D_E_L_E_T_ = '' AND SC5.D_E_L_E_T_='' "
	cquery += " AND C5_FILIAL=ZZ2_PV01FL "
	cquery += " AND C5_NUM = ZZ2_PV01 "

	TCQuery cquery new alias T01

	IF ! T01->(EOF())

		cFilant := T01->C5_FILIAL

		dbSelectArea("SC5")
		dbSetOrder(1)
		dbSeek(T01->C5_FILIAL+T01->C5_NUM)

		SC5->(a410Visual("SC5",RecNo(),2))

		//A410Visual ( "SC5", T01->C5RECNO, 2 )
	ELSE
		ALERT("Pedido 01 da medi��o "+aPed01[1][nMedia]+" do contrato "+aPed01[1][nNumeroa]+" n�o encontrado! ")
	ENDIF

	T01->(dbclosearea())
	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
RETURN



// FUN��O PARA ABRIR O PEDIDO 02

STATIC FUNCTION YPED02(aMedicao,oMarkBrow)
	Local aPed02 := {}
	Local cfilatu := cFilant
	Local nCont  := 0
	Local cArea := GetArea()
	Private aRotina    := {}
	Private INCLUI      := .F.
	Private ALTERA      := .F.
	Private nTipoPed    := 1
	Private cCadastro   := ''
	Private l120Auto    := .F.
	Private LINTGC

	//--Monta o aRotina para compatibilizacao
	AAdd( aRotina, { '' , '' , 0, 1 } )
	AAdd( aRotina, { '' , '' , 0, 2 } )
	AAdd( aRotina, { '' , '' , 0, 3 } )
	AAdd( aRotina, { '' , '' , 0, 4 } )
	AAdd( aRotina, { '' , '' , 0, 5 } )


	FOR nAx := 1 to LEN(aMedicao)

		if aMedicao[nAx][nOk] == "1"
			nCont ++
			aadd(aPed02,{aMedicao[nAx][nFiliala],;
			aMedicao[nAx][nMedia]  ,;
			aMedicao[nAx][nCodClia],;
			aMedicao[nAx][nljClia] ,;
			aMedicao[nAx][nNomClia],;
			aMedicao[nAx][nDtGera] ,;
			aMedicao[nAx][nDtMaxa] ,;
			aMedicao[nAx][nStatusa],;
			aMedicao[nAx][nobsa]   ,;
			aMedicao[nAx][nNumeroa],;
			aMedicao[nAx][nLocExa] ,;
			aMedicao[nAx][nOk]     ,;
			aMedicao[nAx][nEtapa]  ,;
			nAx})
		endif
		if nCont > 1
			alert("Selecione apena uma medi��o para visualizar o Pedido 02")
			YDESMARCA()
			oMarkBrow:refresh()
			RETURN
		endif

	next

	if nCont == 0
		alert("Selecione uma medi��o para visualizar o Pedido 02")
		YDESMARCA()
		oMarkBrow:refresh()
		RETURN
	endif

	cquery := " SELECT C5_FILIAL,C5_NUM,SC5.R_E_C_N_O_ AS C5RECNO "
	cquery += " FROM "+ RETSQLNAME("ZZ2") +" ZZ2 ,"+ RETSQLNAME("SC5") +" SC5 "
	cquery += " WHERE ZZ2_FILIAL  = '"+aPed02[1][nFiliala]+"' "
	cquery += " AND ZZ2_MEDICA = '" +aPed02[1][nMedia]+ "' "
	cquery += " AND ZZ2_CLIENT = '" +aPed02[1][nCodClia]+  "' "
	cquery += " AND ZZ2_LOJA = '" +aPed02[1][nljClia]+  "' "
	cquery += " AND ZZ2_NUMERO = '" +aPed02[1][nNumeroa]+  "' "
	cquery += " AND ZZ2.D_E_L_E_T_ = '' AND SC5.D_E_L_E_T_='' "
	cquery += " AND C5_FILIAL=ZZ2_PV02FL "
	cquery += " AND C5_NUM = ZZ2_PV02 "

	TCQuery cquery new alias T01

	IF ! T01->(EOF())

		cFilant := T01->C5_FILIAL

		dbSelectArea("SC5")
		dbSetOrder(1)
		dbSeek(T01->C5_FILIAL+T01->C5_NUM)

		SC5->(a410Visual("SC5",RecNo(),2))

		//A410Visual ( "SC5", T01->C5RECNO, 2 )
	ELSE
		ALERT("Pedido 02 da medi��o "+aPed02[1][nMedia]+" do contrato "+aPed02[1][nNumeroa]+" n�o encontrado! ")
	ENDIF

	T01->(dbclosearea())
	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
RETURN



// FUN��O PARA ABRIR O PEDIDO 03

STATIC FUNCTION YPED03(aMedicao,oMarkBrow)

	Local aPed03 := {}
	Local cfilatu := cFilant
	Local nCont  := 0
	Local cArea := GetArea()
	Private aRotina    := {}
	Private INCLUI      := .F.
	Private ALTERA      := .F.
	Private nTipoPed    := 1
	Private cCadastro   := ''
	Private l120Auto    := .F.
	Private LINTGC

	//--Monta o aRotina para compatibilizacao
	AAdd( aRotina, { '' , '' , 0, 1 } )
	AAdd( aRotina, { '' , '' , 0, 2 } )
	AAdd( aRotina, { '' , '' , 0, 3 } )
	AAdd( aRotina, { '' , '' , 0, 4 } )
	AAdd( aRotina, { '' , '' , 0, 5 } )


	FOR nAx := 1 to LEN(aMedicao)

		if aMedicao[nAx][nOk] == "1"
			nCont ++
			aadd(aPed03,{aMedicao[nAx][nFiliala],;
			aMedicao[nAx][nMedia]  ,;
			aMedicao[nAx][nCodClia],;
			aMedicao[nAx][nljClia] ,;
			aMedicao[nAx][nNomClia],;
			aMedicao[nAx][nDtGera] ,;
			aMedicao[nAx][nDtMaxa] ,;
			aMedicao[nAx][nStatusa],;
			aMedicao[nAx][nobsa]   ,;
			aMedicao[nAx][nNumeroa],;
			aMedicao[nAx][nLocExa] ,;
			aMedicao[nAx][nOk]     ,;
			aMedicao[nAx][nEtapa]  ,;
			nAx})
		endif
		if nCont > 1
			alert("Selecione apena uma medi��o para visualizar o Pedido 03")
			YDESMARCA()
			oMarkBrow:refresh()
			RETURN
		endif

	next

	if nCont == 0
		alert("Selecione uma medi��o para visualizar o Pedido 03")
		YDESMARCA()
		oMarkBrow:refresh()
		RETURN
	endif

	cquery := " SELECT C5_FILIAL,C5_NUM,SC5.R_E_C_N_O_ AS C5RECNO "
	cquery += " FROM "+ RETSQLNAME("ZZ2") +" ZZ2 ,"+ RETSQLNAME("SC5") +" SC5 "
	cquery += " WHERE ZZ2_FILIAL  = '"+aPed03[1][nFiliala]+"' "
	cquery += " AND ZZ2_MEDICA = '" +aPed03[1][nMedia]+ "' "
	cquery += " AND ZZ2_CLIENT = '" +aPed03[1][nCodClia]+  "' "
	cquery += " AND ZZ2_LOJA = '" +aPed03[1][nljClia]+  "' "
	cquery += " AND ZZ2_NUMERO = '" +aPed03[1][nNumeroa]+  "' "
	cquery += " AND ZZ2.D_E_L_E_T_ = '' AND SC5.D_E_L_E_T_='' "
	cquery += " AND C5_FILIAL=ZZ2_PV03FL "
	cquery += " AND C5_NUM = ZZ2_PV03 "

	TCQuery cquery new alias T01

	IF ! T01->(EOF())

		cFilant := T01->C5_FILIAL

		dbSelectArea("SC5")
		dbSetOrder(1)
		dbSeek(T01->C5_FILIAL+T01->C5_NUM)

		SC5->(a410Visual("SC5",RecNo(),2))

		//A410Visual ( "SC5", T01->C5RECNO, 2 )
	ELSE
		ALERT("Pedido 03 da medi��o "+aPed03[1][nMedia]+" do contrato "+aPed03[1][nNumeroa]+" n�o encontrado! ")
	ENDIF

	T01->(dbclosearea())
	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
RETURN


// FUN��O PARA ALTERA��O DO CABE�ALHO DA MEDI��O

STATIC FUNCTION YALTMED(aMedicao,oMarkBrow)

	Local aAltmed  		:= {}
	Local cfilatu  		:= cFilant
	Local nCont    		:= 0
	Local cArea         := GetArea()
	//cFlag  :=

	FOR nAx := 1 to LEN(aMedicao)

		if aMedicao[nAx][nOk] == "1"

			if aMedicao[nAx][nStatusa] == "B" .OR. aMedicao[nAx][nStatusa] == "CB"

				ALERT("A altera��o da medi��o n�o � permitida para medi�oes com status Cliente Bloqueado e Medi��o Bloqueada !")
				YDESMARCA()
				oMarkBrow:refresh()
				RETURN
			else
				nCont ++
				aadd(aAltmed,{aMedicao[nAx][nFiliala],;
				aMedicao[nAx][nMedia]  ,;
				aMedicao[nAx][nCodClia],;
				aMedicao[nAx][nljClia] ,;
				aMedicao[nAx][nNomClia],;
				aMedicao[nAx][nDtGera] ,;
				aMedicao[nAx][nDtMaxa] ,;
				aMedicao[nAx][nStatusa],;
				aMedicao[nAx][nobsa]   ,;
				aMedicao[nAx][nNumeroa],;
				aMedicao[nAx][nLocExa] ,;
				aMedicao[nAx][nOk]     ,;
				aMedicao[nAx][nEtapa]  ,;
				nAx})
			endif
			if nCont > 1
				alert("Selecione apena uma medi��o para Alterar ")
				YDESMARCA()
				oMarkBrow:refresh()
				RETURN
			endif
		endif
	next
	if nCont == 0
		alert("Selecione uma medi��o para Alterar")
		YDESMARCA()
		oMarkBrow:refresh()
		RETURN
	endif

	cFilant := aAltmed[1][nFiliala]
	dbSelectArea("ZZ2")
	dbSetOrder(1)      // ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICAO+ZZ2_CLIENT+ZZ2_LOJA
	dbSeek(aAltmed[1][nFiliala] + aAltmed[1][nNumeroa] + aAltmed[1][nMedia] + aAltmed[1][nCodClia] + aAltmed[1][nljClia] )     // Busca exata

	FWExecView ("Alterar", "SASP038", MODEL_OPERATION_UPDATE ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )

	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
RETURN


// FUN��O PARA IMPRIMIR A MEDI��O
STATIC FUNCTION YIMPRIMIR(aMedicao,oMarkBrow)

	//Local cRestri   := STRTOKARR (GETMV("SA_IMPROS"),';')
	Local cRestri   := ALLTRIM(GETMV("SA_IMPROS"))
	Local aImprime  := {}
	Local nCont     := 0
	Local cArea     := GetArea()

	FOR nAx := 1 to LEN(aMedicao)

		if aMedicao[nAx][nOk] == "1" .AND. IIF(cRestri == "" ,.T.,aMedicao[nAx][nStatusa] $ cRestri)
			nCont ++
			aadd(aImprime,{aMedicao[nAx][nFiliala] + aMedicao[nAx][nNumeroa] + aMedicao[nAx][nMedia] + aMedicao[nAx][nCodClia] + aMedicao[nAx][nljClia]})
		endif

	next

	IF nCont == 0
		U_RF012()
	ELSE
		U_RF012(aImprime)
	ENDIF

	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
RETURN
//FUNCAO PARA IMPRIMIR A PLACA DE EMBARQUE

STATIC FUNCTION YPLACA(aMedicao,oMarkBrow)

	Local aImprimi  := {}
	Local nCont     := 0
	Local cArea     := GetArea()


	FOR nAx := 1 to len(aMedicao)

		if aMedicao[nAx][nOk] == "1"
			nCont ++
			aadd(aImprimi,{aMedicao[nAx][nFiliala], aMedicao[nAx][nNumeroa] ,aMedicao[nAx][nMedia]})
			nAx := len(aMedicao)
		endif
	next nAx

	if nCont == 0

		U_SASR034(aMedicao[nAx][nFiliala] , aMedicao[nAx][nNumeroa] , aMedicao[nAx][nMedia])

	else

		U_SASR034(aImprimi)

	endif

	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()

RETURN

//FUN��O PARA HIST�RICO DE OS

Static Function YHISTOR(aMedicao,oMarkBrow)

	Local nCont := 0
	Local aHist := {}
	Local oGet1
	Local cGet1 := "Define variable value"
	Local oGet2
	Local cGet2 := "Define variable value"
	Local oSay1
	Local oSay2
	Local oSButton1
	Static oDlg

	FOR nAx := 1 to LEN(aMedicao)

		if aMedicao[nAx][nOk] == "1"
			nCont ++
			aadd(aHist,{aMedicao[nAx][nFiliala],;
			aMedicao[nAx][nMedia]  ,;
			aMedicao[nAx][nCodClia],;
			aMedicao[nAx][nljClia] ,;
			aMedicao[nAx][nNomClia],;
			aMedicao[nAx][nDtGera] ,;
			aMedicao[nAx][nDtMaxa] ,;
			aMedicao[nAx][nStatusa],;
			aMedicao[nAx][nobsa]   ,;
			aMedicao[nAx][nNumeroa],;
			aMedicao[nAx][nLocExa] ,;
			aMedicao[nAx][nOk]     ,;
			aMedicao[nAx][nEtapa]  ,;
			nAx})
		endif
		if nCont > 1
			alert("Selecione apena uma medi��o para Alterar ")
			YDESMARCA()
			oMarkBrow:refresh()
			RETURN
		endif

	next
	if nCont == 0
		alert("Selecione uma medi��o para Alterar ")
		YDESMARCA()
		oMarkBrow:refresh()
		RETURN
	endif

	cGet1 := aHist[1][nNumeroa]
	cGet2 := aHist[1][nMedia]

	DEFINE MSDIALOG oDlg TITLE "Hist�rico de Medi��o" FROM 000, 000  TO 300, 500 COLORS 0, 16777215 PIXEL

	gridhistor(aHist)
	@ 017, 009 SAY oSay1 PROMPT "CONTRATO :" SIZE 037, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 017, 045 MSGET oGet1 VAR cGet1 SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 017, 125 SAY oSay2 PROMPT "MEDI��O:" SIZE 037, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 017, 155 MSGET oGet2 VAR cGet2 SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	DEFINE SBUTTON oSButton1 FROM 132, 211 TYPE 01 OF oDlg ACTION  (oDlg:end()) ENABLE

	ACTIVATE MSDIALOG oDlg CENTERED

	YDESMARCA()
	oMarkBrow:refresh()
Return

//------------------------------------------------
Static Function gridhistor(aHist)
	//------------------------------------------------
	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {}
	Local aAlterFields := {}
	Static oMSNewGe1

	aadd (aFields,{"ZZR_FILIAL"})
	aadd (aFields,{"ZZR_CONTRA"})
	aadd (aFields,{"ZZR_MEDICA"})
	aadd (aFields,{"ZZR_EMISSA"})
	aadd (aFields,{"ZZR_CODCLI"})
	aadd (aFields,{"ZZR_NOMECL"})
	aadd (aFields,{"ZZR_CODUSE"})
	aadd (aFields,{"ZZR_NOMUSE"})
	aadd (aFields,{"ZZR_OBS"   })
	aadd (aFields,{"ZZR_DATA"  })
	aadd (aFields,{"ZZR_HORA"  })
	aadd (aFields,{"ZZR_ETAPA"})
	aadd (aFields,{"ZZR_NOMETP"})

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX][1]))
			Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

	// Define field values
	For nX := 1 to Len(aFields)
		If DbSeek(aFields[nX][1])
			Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
		Endif
	Next nX
	Aadd(aFieldFill, .F.)
	//Aadd(aColsEx, aFieldFill)

	cQuery:= " SELECT ZZR_FILIAL , ZZR_CONTRA ,ZZR_MEDICA ,ZZR_EMISSA ,ZZR_CODCLI ,ZZR_NOMECL ,ZZR_CODUSE ,ZZR_NOMUSE ,ZZR_OBS ,ZZR_DATA ,ZZR_HORA ,ZZR_ETAPA , ZZR_NOMETP"
	cQuery+= " FROM "+ RETSQLNAME("ZZR") +" ZZR "
	cQuery+= " WHERE ZZR_FILIAL = '"+aHist[1][nFiliala]+"' "
	cQuery+= " AND   ZZR_CONTRA = '"+aHist[1][nNumeroa]+"' "
	cQuery+= " AND   ZZR_MEDICA = '"+aHist[1][nMedia]+"' "
	cQuery+= " AND   D_E_L_E_T_ = '' "


	TCQuery cquery new alias T01

	WHILE !T01->(EOF())
		aadd(aColsEx,{T01->ZZR_FILIAL,;
		T01->ZZR_CONTRA,;
		T01->ZZR_MEDICA,;
		T01->ZZR_EMISSA,;
		T01->ZZR_CODCLI,;
		T01->ZZR_NOMECL,;
		T01->ZZR_CODUSE,;
		T01->ZZR_NOMUSE,;
		T01->ZZR_OBS,;
		T01->ZZR_DATA,;
		T01->ZZR_HORA,;
		T01->ZZR_ETAPA,;
		T01->ZZR_NOMETP,;
		.F.})

		T01->(dbSkip())
	ENDDO
	T01->(DBCLOSEAREA())
	oMSNewGe1 := MsNewGetDados():New( 042, 003, 114, 244, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return


// BUSCANDO A DESCRI��O DA TRANSPORTADORA

USER FUNCTION YDESTRA(cCod)
	
	Local cDesc := ""

	cQuery := " select A4_NOME "
	cQuery += " FROM " + RETSQLNAME("SA4")+"  "
	cQuery += " WHERE D_E_L_E_T_ =''  "
	cQuery += " AND   A4_FILIAL  = '"+xFilial("SA4")+"' "
	cQuery += " AND   A4_COD = '"+cCod+"' "
	TCQuery cquery new alias T05

	IF !T05->(eof())
		cDesc:= ALLTRIM(T05->A4_NOME)
	ENDIF
	T05->(dbclosearea())

Return cDesc

Static function valid23()

	Local lRet := .F.

return lRet

// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO DESMARCADO
STATIC FUNCTION YDESMARCA()

	FOR nAx := 1 to len(aMedicao)
		aMedicao[nAx][nOK] := ""
	NEXT


RETURN

// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO MARCADO
STATIC FUNCTION YMARCA()

	FOR nAx := 1 to len(aMedicao)
		aMedicao[nAx][nOK] := "1"
	NEXT


RETURN

// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO MARCADO
STATIC FUNCTION YINVERTE(oMarkBrow)

	FOR nAx := 1 to len(aMedicao)
		IF aMedicao[nAx][nOK] == "1"
			aMedicao[nAx][nOK] := ""
		ELSE
			aMedicao[nAx][nOK] := "1"
		ENDIF
	NEXT

	oMarkBrow:refresh()
RETURN

Static Function GetValor(cFilPV, cPedVen)

	Local cQuery := ""
	Local nValTotal := 0

	cQuery := ""
	cQuery += " SELECT SUM(C6_VALOR) AS TOTAL "
	cQuery += "   FROM " + RETSQLNAME("SC6") + " SC6 "
	cQuery += "  WHERE SC6.D_E_L_E_T_ = '' "
	cQuery += "    AND SC6.C6_FILIAL = '" + XFILIAL("SC6",cFilPV) + "' "
	cQuery += "    AND SC6.C6_NUM ='" + cPedVen + "' "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XC6",.F.,.T.)

	nValTotal := Round(XC6->TOTAL,2)

	nValTotal := (nValTotal + POSICIONE("SC5",1,XFILIAL("SC5",cFilPV) + cPedVen,"C5_FRETE") ) - POSICIONE("SC5",1,XFILIAL("SC5",cFilPV) + cPedVen,"C5_DESCONT")

	XC6->(DBCLOSEAREA())

Return nValTotal

Static function AjTOTAL

	Local nI
	Local oGetDB
	Local nUsado := 0
	Local aStruct := {}
	local _usuP := SUPERGETMV("SA_YAJVOL") //criar parametro
	LOCAL _usuL := RETCODUSR()
	local oDlg
	local nOpca := 1
	Local cNF1 := ""
	Local cNF2 := ""
	Private lRefresh := .T.
	Private aHeader := {}
	Private aCols := {}
	Private aRotina := {{"Pesquisar", "AxPesqui", 0, 1},;
	{"Visualizar", "AxVisual", 0, 2},;
	{"Incluir", "AxInclui", 0, 3},;
	{"Alterar", "AxAltera", 0, 4},;
	{"Excluir", "AxDeleta", 0, 5}}

	if !_usuL$_usuP
		ALERT("ATEN��O: Seu usu�rio n�o tem permiss�o para fazer essa altera��o! Verifique.")
		return
	endif 

	DbSelectArea("SX3")
	DbSetOrder(1)
	DbSeek("ZZQ")

	While !Eof() .and. SX3->X3_ARQUIVO == "ZZQ"
		If X3Uso(SX3->X3_USADO) .and. cNivel >= SX3->X3_NIVEL
			nUsado++
			IF ALLTRIM(SX3->X3_CAMPO) == "ZZQ_FILIAL"  .OR. ALLTRIM(SX3->X3_CAMPO) == "ZZQ_NUMERO"  .OR. ALLTRIM(SX3->X3_CAMPO) =="ZZQ_MEDICA" .OR. ALLTRIM(SX3->X3_CAMPO) =="ZZQ_DESCSE" .OR. ALLTRIM(SX3->X3_CAMPO) =="ZZQ_QUANT" .OR. ALLTRIM(SX3->X3_CAMPO) =="ZZQ_DESGRU"
				AADD(aHeader,{Trim(X3Titulo()),;
				SX3->X3_CAMPO,;
				SX3->X3_PICTURE,;
				SX3->X3_TAMANHO,;
				SX3->X3_DECIMAL,;
				SX3->X3_VALID,;
				"",;
				SX3->X3_TIPO,;
				"",;
				"" })
				AADD(aStruct,{SX3->X3_CAMPO,SX3->X3_TIPO,SX3->X3_TAMANHO,;
				SX3->X3_DECIMAL})
			END
		EndIf
		DbSkip()
	End
	AADD(aStruct,{"FLAG","L",1,0})
	cCriaTrab := CriaTrab(aStruct,.T.)
	DbUseArea(.T.,__LocalDriver,cCriaTrab,,.T.,.F.)
	FOR nAx := 1 to LEN(aMedicao)
		if aMedicao[nAx][nOk] == "1"

			_valid	:= POSICIONE("ZZ2",1,aMedicao[nAx][nFiliala]+aMedicao[nAx][nNumeroa]+aMedicao[nAx][nMedia]+aMedicao[nAx][nCodClia]+aMedicao[nAx][nljClia],"ZZ2_ETAPA")

			cNF1	:= POSICIONE("ZZ2",1,aMedicao[nAx][nFiliala]+aMedicao[nAx][nNumeroa]+aMedicao[nAx][nMedia]+aMedicao[nAx][nCodClia]+aMedicao[nAx][nljClia],"ZZ2_PV01NF")
			cNF2	:= POSICIONE("ZZ2",1,aMedicao[nAx][nFiliala]+aMedicao[nAx][nNumeroa]+aMedicao[nAx][nMedia]+aMedicao[nAx][nCodClia]+aMedicao[nAx][nljClia],"ZZ2_PV02NF")

			IF _VALID != "40" 
				ALERT("ATEN��O: Somente medi��es na etapa de separa��o podem ser alterados os volumes!")
				return

			ELSEIF ALLTRIM(cNF1) <> "" .OR. ALLTRIM(cNF2) <> ""

				ALERT("ATEN��O: Medicoes em processo de faturamento, n�o � possivel alterar medi�oes!")
				return

			ENDIF

			DBSELECTAREA("ZZQ")
			DBSETORDER(1)
			IF DBSEEK(aMedicao[nAx][nFiliala]+aMedicao[nAx][nNumeroa]+aMedicao[nAx][nMedia])
				nY:=0
				WHILE !EOF() .AND. aMedicao[nAx][nFiliala]+aMedicao[nAx][nNumeroa]+aMedicao[nAx][nMedia] == ZZQ->ZZQ_FILIAL+ZZQ->ZZQ_NUMERO+ZZQ->ZZQ_MEDICA
					nY++
					aadd(aCols,Array(Len(aHeader)+1))
					For nX := 1 to Len(aHeader)
						IF aHeader[nX][2] == "ZZQ_DESCSE"
							DBSELECTAREA("ZZO")
							DBSETORDER(1)
							DBSEEK(ZZQ->ZZQ_CODSER)
							DBSELECTAREA("ZZQ")
							_DESC := FieldGet(FieldPos(aHeader[nX][2]))
							aCols[nY][nX] := alltrim(ZZO->ZZO_DESC)+" - " + alltrim(_DESC)

						ELSE
							aCols[nY][nX] := FieldGet(FieldPos(aHeader[nX][2]))
						ENDIF
					NEXT
					aCols[nY][Len(aHeader)+1] := .F.	
					DBSKIP()
				ENDDO
			ENDIF
			exit
		endif

	next
	//OBS: RETIRAR O POSICIONE NO X3_CBOX
	oDlg := MSDIALOG():New(000,000,300,900, "Altera��o volume ",,,,,,,,,.T.)
	_nj:= 420
	oGet:= MSGetDados():New(05,05,145,_nj,3,,,"",,{"ZZQ_QUANT "})
	oGet:Refresh()
	//oGetDB := MsGetDB():New(05,05,145,195,3,/*"U_LINHAOK"*/, /*"U_TUDOOK"*/,/* "+A1_COD"*/, ;
	//	.F.,{"ZZQ_QUANT"},,.F.,,cCriaTrab,/*"U_FIELDOK"*/,,.F.,oDlg, .T., ,/*"U_DELOK"*/,;
	//	/*"U_SUPERDEL"*/)
	oDlg:lCentered := .T.
	_nt := 425
	DEFINE SBUTTON FROM 010, _nt TYPE 1 ACTION (nOpca:=2,oDlg:end()) ENABLE OF oDlg

	oDlg:Activate()
	DbSelectArea(cCriaTrab)
	DbCloseArea()
	IF nOpca == 2
		gravAlt()
	ENDIF

Return

static function gravAlt

	Local nVolume := 0
	Local cQuery := ""

	nPosFil := aScan(aHeader,{|x| AllTrim(x[2])=='ZZQ_FILIAL'})
	nPosNum := aScan(aHeader,{|x| AllTrim(x[2])=='ZZQ_NUMERO'})
	nPosMed := aScan(aHeader,{|x| AllTrim(x[2])=='ZZQ_MEDICA'})
	nPosQt := aScan(aHeader,{|x| AllTrim(x[2])=='ZZQ_QUANT'})


	DBSELECTAREA("ZZQ")
	DBSETORDER(1)
	IF DBSEEK(acols[1][nPosFil]+acols[1][nPosNum]+acols[1][nPosMed])
		for i:= 1 to len(acols)
			if !empty(acols[i][1]) .AND. !EOF()
				RECLOCK("ZZQ",.F.)
				REPLACE ZZQ_QUANT WITH acols[i][nPosQt]
				MSUNLOCK()
				nVolume := nVolume + acols[i][nPosQt]
				DBSKIP()		
			endif
		next

		cQuery := ""
		cQuery += " SELECT C5_FILIAL,C5_NUM "
		cQuery += "   FROM " + RETSQLNAME("SC5")  + " SC5 "
		cQuery += "  WHERE SC5.D_E_L_E_T_ = '' "
		cQuery += "    AND SC5.C5_FILIAL = '" +  acols[1][nPosFil] + "' "  
		cQuery += "    AND SC5.C5_YCONTRA = '" + acols[1][nPosNum] + "' "
		cQuery += "    AND SC5.C5_YMEDICA = '" + acols[1][nPosMed] + "' "

		cQuery := ChangeQuery(cQuery)

		DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"XC5")

		While XC5->(!eof()) //SC5->C5_FILIAL+SC5->C5_NUM == aDados2[1][1] +  XC5->C5_NUM		

			DBSELECTAREA("SC5")
			DBSETORDER(1)
			DBSEEK(XC5->C5_FILIAL + XC5->C5_NUM)

			RECLOCK("SC5", .F.)

			SC5->C5_VOLUME1 := nVolume
			SC5->C5_ESPECI1 := "CX" 			

			MSUNLOCK()	
			SC5->(DBSKIP())

			dbselectArea("XC5")
			XC5->(dbSkip())

		EndDo

		XC5->(DBCLOSEAREA())

	ELSE
		
		ALERT("Houve um problema na busca da medi��o. Atualize os dados da tela!")
		
		return
	end

	MSGINFO("Volumes alterados com sucesso!")

return

static function NotaPDV()


	Local aPergs := {}
	Local _nf := space(TAMSX3("F2_DOC")[1])
	local _serie := space(TAMSX3("F2_SERIE")[1])
	local _Emiss := "Emissao: " 
	local _client := space(TAMSX3("F2_CLIENTE")[1])
	local _loja := space(TAMSX3("F2_CLIENTE")[1])
	local _contr := space(TAMSX3("F2_YCONTRA")[1]) 
	local _envio := space(TAMSX3("ZZ7_ENVIO")[1])


	Local aRet := {}
	Local lRet 

	Msginfo("Para vincular a nota do pdv � necessario entrar na filial 030101","Aten��o")

	if !(cFilAnt $ '030101')
		Alert("Filial n�o autorizada! Favor entrar na Filial 030101.")
		Return
	else
		Alert("Voc� est� na filial: "+cFilAnt)
	endif

	//Criar CP SF2_2 e ZZ0
	aAdd( aPergs ,{1,"NF ",_nf,"@!",".t.","SF2_2",'.T.',TAMSX3("F2_DOC")[1],.T.})   
	aAdd( aPergs ,{1,"Serie ",_serie,"@!",'.T.',,'.T.',TAMSX3("F2_SERIE")[1],.T.})
	//aAdd( aPergs ,{9,_emiss,30,20,.f.})
	aAdd( aPergs ,{1,"Cliente ",_client,"@!",'.T.',"SA1",'.T.',TAMSX3("F2_SERIE")[1],.T.})
	aAdd( aPergs ,{1,"Loja ",_loja,"@!",'.T.',,'.T.',TAMSX3("F2_SERIE")[1],.T.})
	aAdd( aPergs ,{1,"Contrato ",_contr,"@!",'.T.',"ZZ0",'.T.',TAMSX3("F2_SERIE")[1],.T.})
	aAdd( aPergs ,{1,"Envio ",_envio,"@!",'.T.',,'.T.',TAMSX3("ZZ7_ENVIO")[1],.T.})


	If ParamBox(aPergs ,"Parametros ",aRet)       

		if msgyesno("Esta opera��o ir� realizar uma atualiza��o no campo de Mensagem da NF dos pedidos DESTA FILIAL("+CFILANT+"). Deseja continuar?")
			qr:="SELECT * FROM "+RETSQLNAME("SF2")+" WHERE D_E_L_E_T_='' AND F2_DOC='"+MV_PAR01+"' AND F2_SERIE='"+MV_PAR02+"' AND F2_FILIAL = '"+CFILANT+"' "
			TCQuery qr new alias T0402

			DBSELECTAREA("T0402")

			IF EOF()
				ALERT("Aten��o: NF n�o encontrada. Verifique os parametros.")
				return
			ELSE
				_EMISS:=STOD(T0402->F2_EMISSAO)
				DBCLOSEAREA() 	
			ENDIF 

			cMenNota := "Referente Remessa para Venda Fora do Estabelecimento conforme Nota Fiscal N�. "+MV_PAR01+", S�rie "+MV_PAR02+" emitida em "+DTOC(_EMISS)+" Conforme IN 29 Art. 2 de 17/06/2013.) "
			_MSGPAD := "MSG"

			_Q:= "UPDATE "+RETSQLNAME("SC5")+"  SET C5_MENNOTA ='"+cMenNota+"',C5_MENPAD = '"+_MSGPAD+"' "
			//_Q+=", C5_YFILNFM='"+CFILANT+"', C5_YNFMAE='"+MV_PAR01+"', C5_YSERMAE='"+MV_PAR02+"' "
			_Q+=" FROM "+RETSQLNAME("ZZ2")+" ZZ2, "+RETSQLNAME("ZZ3")+" ZZ3, "+RETSQLNAME("ZZ7")+" ZZ7 "
			_Q+=" WHERE  "+RETSQLNAME("SC5")+".D_E_L_E_T_=' ' AND C5_FILIAL+C5_NUM = ZZ2_PV01FL+ZZ2_PV01 AND "
			_Q+=" ZZ3.D_E_L_E_T_=' ' AND ZZ7.D_E_L_E_T_=' ' AND ZZ2.D_E_L_E_T_=' ' "
			_Q+=" AND ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICA = ZZ3_FILIAL+ZZ3_NUMERO+ZZ3_MEDICA " 
			_Q+=" AND ZZ3_TIPO = 'K' AND ZZ2_STATUS<>'F' "
			_Q+=" AND ZZ3_PRODUT = ZZ7_CODIGO AND ZZ2_CLIENT = '"+MV_PAR03+"' AND ZZ2_LOJA='"+MV_PAR04+"'"
			_Q+=" AND C5_YCONTRA='"+MV_PAR05+"' AND ZZ7_ENVIO='"+MV_PAR06+"' "		      

			IF TCSQLExec(_Q) < 0 
				ALERT("ATEN��O: Houve um problema na execu��o da atualiza��o. Verifique os parametros!")
			else
				msginfo("Processamento realizado com sucesso!")
			ENDIF 	   
		endif

	EndIf


return
