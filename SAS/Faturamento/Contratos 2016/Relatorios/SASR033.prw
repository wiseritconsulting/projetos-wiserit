#include "tbiconn.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "protheus.ch"
#include "topconn.ch"

/*/{Protheus.doc} SASR033
@type function
@author Weskley Silva
@since 13/11/2015
@version P11
/*/

User function SASR033()

	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local oPrinter
	Local cLocal          := "c:\TEMP\"
	Local cCodINt25 := "050101 000999 009999 99A"					
	Local oFont1 	:= TFont():New( "Arial",,20,,.T.,,,,,  .T. )
	Local oFont2 	:= TFont():New( "Arial",,110,,.T.,,,,,  .F. )
	Local oFont3 	:= TFont():New( "Arial",,28,,.T.,,,,,  .F. )
	Local oFont4 	:= TFont():New( "Arial",,18,,.F.,,,,,  .F. )
	Local oFont5 	:= TFont():New( "Arial",,12,,.T.,,,,,  .T. )
	Local oPrn      := FwMSPrinter():New( 'Placa de embarque' , 6 , .F. , , .T. )
	Local cFilePrint := ""
	Local nLin := 150
	Local nTmLin := 10
	Local nSalto := 20
	Local cQuery := ""
	Local aArea := GetArea()
	Local cSeq2 := ""

	cSeq2 := Seq()
	
	IF EMPTY(ZZ2->ZZ2_PV01NF)
		
		Msginfo("Favor faturar as medi�oes para imprimir a placa","Placa de Embarque")
		return
	ENDIF
	
	If Empty(ZZ2->ZZ2_PLACEX)

		RECLOCK("ZZ2", .F.)
		ZZ2_PLACEX := PADL(cSeq2,3,"0")
		MSUNLOCK()  	
	Else 

		cSeq2 := Alltrim(ZZ2->ZZ2_PLACEX)

	Endif

	cQuery :=" SELECT ZZ2_CLIENT,"
	cQuery +=" ZZ2_NOME,"
	cQuery +=" ZZ2_TRANSP,"
	cQuery +=" A4_NOME,"
	cQuery +=" ZZQ_CODSER,"
	cQuery +=" ZZQ_DESCSE,"
	cQuery +=" ZZQ_ENVIO,"
	cQuery +=" ZZ2_MEDICA,"
	cQuery +=" ZZQ_QUANT,"
	cQuery +=" ZZ2_PV01NF,"
	cQuery +=" ZZ2_SER01,"
	cQuery +=" ZZ2_PV03NF,"
	cQuery +=" ZZ2_SER03,"
	cQuery +=" SUM(ZZQ_QUANT)AS 'TOTALVOLUMES'"
	cQuery +=" FROM ZZQ010 ZZQ"
	cQuery +=" JOIN ZZ2010 ZZ2 ON(ZZQ_FILIAL = ZZ2_FILIAL"
	cQuery +=" AND ZZQ_NUMERO = ZZ2_NUMERO"
	cQuery +=" AND ZZQ_MEDICA = ZZ2_MEDICA)"
	cQuery +=" JOIN SA4010 A4 ON(A4_COD = ZZ2_TRANSP)"	
	cQuery +=" JOIN ZZO010 ZZO ON(ZZO_COD = ZZQ_CODSER)	"	
	cQuery +=" WHERE "
	cQuery +=" 	A4.D_E_L_E_T_ = ''"
	cQuery += "AND ZZQ_MEDICA = '"+ZZ2->ZZ2_MEDICA+"'
	cQuery +=" AND ZZQ_NUMERO = '"+ZZ2->ZZ2_NUMERO+"'
	cQuery +=" AND ZZQ_FILIAL = '"+ZZ2->ZZ2_FILIAL+"'
	cQuery +=" AND ZZQ.D_E_L_E_T_ = ''"
	cQuery +=" AND ZZ2.D_E_L_E_T_ = ''"
	cQuery +=" GROUP BY ZZQ_QUANT,"
	cQuery +=" ZZ2_NOME,"
	cQuery +=" A4_NOME,"
	cQuery +=" ZZQ_CODSER,"
	cQuery +=" ZZQ_ENVIO,"
	cQuery +=" ZZ2_MEDICA, "
	cQuery +=" ZZ2_CLIENT, "
	cQuery +=" ZZ2_TRANSP, "
	cQuery +=" ZZQ_DESCSE, "
	cQuery +=" ZZ2_PV01NF,"
	cQuery +=" ZZ2_SER01,"
	cQuery +=" ZZ2_PV03NF,"
	cQuery +=" ZZ2_SER03"
	
	/*IF !(cSerie=="" .OR. cEnv=="")
		cQuery +=" HAVING "
		cQuery +=" ZZQ_CODSER = '"+cSerie+"'"
		cQuery +=" AND ZZQ_ENVIO = '"+cEnv+"'"
	ENDIF
	MEMOWRITE("C:\TEMP\SQLR032.TXT", cQuery)
	*/
	
	TCQUERY cQuery NEW ALIAS T01 


	cCodINt25 := ZZ2->(ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICA)

	oPrinter := FWMSPrinter():New('emba_000000.PD_', IMP_SPOOL, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetLandscape()

	WHILE !T01->(EOF())

		oPrinter:StartPage()

		oPrinter:FWMSBAR("CODE128" /*cTypeBar*/,5/*nRow*/ ,45/*nCol*/ ,cCodINt25  /*cCode*/,oPrinter/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.04 /*nWidth*/,2/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)


		oPrinter:Say ( 120, 100, PADL(cSeq2,3,"0"),  oFont2 ) 	
		oPrinter:Say ( nLin, 570, cCodINt25,  oFont5 )	

		oPrinter:Say ( 230, 20, "CLIENTE",  oFont1 ) 
		oPrinter:Say ( 250, 30, T01->ZZ2_CLIENT + " - " + T01->ZZ2_NOME,  oFont4 )
		oPrinter:Say ( 270, 20, "TRANSPORTADORA",  oFont1 )
		oPrinter:Say ( 290, 30, IIF(!EMPTY(T01->ZZ2_TRANSP),T01->ZZ2_TRANSP + ' - ' + POSICIONE("SA4",1,xFilial("SA4")+T01->ZZ2_TRANSP,"A4_NOME")," #### TRANSPORTADORA NAO INFORMADA.###") ,  oFont4 )
		oPrinter:Say ( 310, 20, "SERIE",  oFont1 )
		oPrinter:Say ( 330, 30, T01->ZZQ_CODSER + " - " + T01->ZZQ_DESCSE,  oFont4 )
		oPrinter:Say ( 350, 20, "ENVIO",  oFont1 )
		oPrinter:Say ( 370, 30, T01->ZZQ_ENVIO,  oFont4 )
		oPrinter:Say ( 390, 20, "VOLUME",  oFont1 )
		oPrinter:Say ( 410, 30, cValToChar(T01->ZZQ_QUANT),  oFont4 )
		oPrinter:Say ( 430, 20, "TOTAL VOLUMES",  oFont1 )
		oPrinter:Say ( 450, 30, cValToChar(T01->TOTALVOLUMES),  oFont4 )
		oPrinter:Say ( 470, 20, "NOTA FISCAL 1",  oFont1 )
		oPrinter:Say ( 490, 30, T01->ZZ2_PV01NF + " / " + T01->ZZ2_SER01 ,  oFont4 )
		oPrinter:Say ( 510, 20, "NOTA FISCAL 3",  oFont1 )
		oPrinter:Say ( 530, 30, T01->ZZ2_PV03NF + " / " + T01->ZZ2_SER03,  oFont4 )

		oPrinter:EndPage()
		T01->(dbskip())

	ENDDO
	oPrinter:Preview()
	T01->(dbCloseArea())
	RestArea(aArea)
Return

Static Function Seq()


	Local cSeque := 0
	Local cFili := cfilAnt

	cfilAnt := ZZ2->ZZ2_FILIAL

	cSeque := GetMv( "SA_PLEMBAR")

	cSeque := Soma1(cvaltochar(cSeque))

	If val(cSeque) > 500
		PUTMV("SA_PLEMBAR",1)
		cSeque := 1
	else	 
		PUTMV("SA_PLEMBAR",val(cSeque)) 
	endif

	cfilAnt := cFili

Return(cSeque)
