#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"
#Include "RWMAKE.CH"
#include 'parmtype.ch'

/*/{Protheus.doc} SASR096
//TODO Relat�rio de OS - Medi��o - Confer�ncia cega
@author Diogo
@since 04/11/2016
@version undefined
@example
(examples)
@see (links_or_references)
/*/

user function SASR096(aFiltro)

	//Objetos para tamanho e tipo das fontes
	Local oFont1    := TFont():New( "Times New Roman",,10,,.T.,,,,,.F.)
	Local oFont2    := TFont():New( "Tahoma",,15,,.T.,,,,,.F.)
	Local oFont3    := TFont():New( "Arial"       ,,20,,.F.,,,,,.F.)
	Local oFont4    := TFont():New( "Times New Roman",,14,,.T.,,,,,.F.)
	Local oFont5    := TFont():New( "Tahoma",,12,,.F.,,,,,.F.)
	Local oFont6    := TFont():New( "Tahoma",,12,,.T.,,,,,.F.)
	Local oPrn      := FwMSPrinter():New( 'Ordem_de_separa��o' , 6 , .F. , , .T. )
	Local oBrush    := TBrush():New(,RGB(205,205,205))
	Local cFigura   := ""
	Local nLinha
	Local nQuant    := 0
	Local nPeso     := 0
	Local cCodINt25
	Local nPag      := 1
	Local cTexto := ""
	Local aRet := {}
	Local aParamBox := {}
	Local nTotCx := 0
	Default aFiltro := {}

	cPerg := "SASR096"

	oPrn:SetUp()              // Abre opcoes para o usuario
	oPrn:SetPortrait()        // ou SetLandscape()

	AjustaSX1(cPerg)            // cria as perguntas

	IF FUNNAME() == "RF023" .AND. LEN(aFiltro) >= 1
		cFiltro := ""
		
		for nAx := 1 to len(aFiltro)
			if nAx == 1
				cFiltro += "'"+aFiltro[nAx][1]+"'"
			else
				cFiltro += " , '"+aFiltro[nAx][1]+"'"
			endif
		next

		/*
		//--- Pergunta se imprime quantidades

		aAdd(aParamBox,{3,"Imprime Quantidade",1,{"Sim","N�o"},50,"",.F.})
		
		If ParamBox(aParamBox,"Pergunta",@aRet)
			MV_PAR07 = aRet[1]
		else
			MV_PAR07 := getmv("SA_IMPQTA")
		Endif

		//---
		*/
		MV_PAR08 := getmv("SA_IMPCLBL")

		MV_PAR09 := ""

		MV_PAR10 := ""
		
		cQuery := " SELECT ZZ2_FILIAL ,ZZ2_DTSOLI ,ZZ2_DTGER ,ZZ2_CLIENT, ZZ2_LOJA , ZZ2_NUMERO , ZZ2_MEDICA  , ZZ2_OBS ,ZZ2_LOCEX , ZZ2_TIPO , ZZ2_IMPRES  "
		cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2 "
		cQuery += " WHERE  ZZ2.D_E_L_E_T_ = '' "
		cQuery += " AND ZZ2_FILIAL+ ZZ2_NUMERO+ZZ2_MEDICA+ZZ2_CLIENT+ZZ2_LOJA IN ("+cFiltro+") "
		IF  ALLTRIM(MV_PAR09) <> ""
			cQuery += " AND ZZ2_TIPO = '"+MV_PAR09+"' "
		ENDIF
		IF  ALLTRIM(MV_PAR10) <> ""
			cQuery += " AND ZZ2_LOCEX = '"+MV_PAR10+"' "
		ENDIF
		cQuery += " ORDER BY ZZ2_MEDICA "
		TcQuery cQuery New Alias T01

	ELSE
		Pergunte(cPerg,.T.)         // abre na tela as perguntas

		cQuery := " SELECT ZZ2_FILIAL ,ZZ2_DTSOLI, ZZ2_TIPO ,ZZ2_DTGER ,ZZ2_CLIENT, ZZ2_LOJA , ZZ2_NUMERO , ZZ2_MEDICA  , ZZ2_OBS ,ZZ2_LOCEX , ZZ2_TIPO , ZZ2_IMPRES  "
		cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2 "
		cQuery += " WHERE  ZZ2.D_E_L_E_T_ = '' "
		cQuery += " AND ZZ2_MEDICA BETWEEN '"+MV_PAR01+"' AND '"+MV_PAR02+"' "
		cQuery += " AND ZZ2_DTGER BETWEEN '"+DTOS(MV_PAR03)+"' AND '"+DTOS(MV_PAR04)+"' "
		cQuery += " AND ZZ2_CLIENT BETWEEN '"+MV_PAR05+"' AND '"+MV_PAR06+"' "
		IF  ALLTRIM(MV_PAR09) <> ""
			cQuery += " AND ZZ2_TIPO = '"+MV_PAR09+"' "
		ENDIF
		IF  ALLTRIM(MV_PAR10) <> ""
			cQuery += " AND ZZ2_LOCEX = '"+MV_PAR10+"' "
		ENDIF
		cQuery += " ORDER BY ZZ2_MEDICA "
		TcQuery cQuery New Alias T01

	ENDIF

	While !T01->(EOF())

		IF MV_PAR08 == 2
			IF POSICIONE("SA1",1,XFILIAL("SA1")+T01->ZZ2_CLIENT+T01->ZZ2_LOJA ,"A1_YBLOQ") = "S"
				T01->(DBSkip())
				LOOP
			ENDIF
		ENDIF

		nPag      := 1

		oPrn:StartPage()          // Inicia uma nova p�gina
		// 1* LINHA
		//-- logo SAS
		cFigura := "\system\OS"+cEmpAnt+".BMP"
		oPrn:SayBitmap(30,10,cFigura,150,60)
		//-- TITULO
		oPrn:Say(55,150, "Ordem de Separa��o ",oFont2,100)
		IF ALLTRIM(T01->ZZ2_IMPRES) == "" .OR. ALLTRIM(T01->ZZ2_IMPRES) == "2"
			oPrn:Say(65,150, "IMPRESS�O",oFont5,100)

			cUpdate := " UPDATE " + RETSQLNAME("ZZ2") "
			cUpdate += " SET ZZ2_IMPRES = '1' "
			cUpdate += " WHERE ZZ2_FILIAL  = '"+ T01->ZZ2_FILIAL +"' "
			cUpdate += " AND ZZ2_MEDICA = '" + T01->ZZ2_MEDICA +  "' "
			cUpdate += " AND ZZ2_CLIENT = '" + T01->ZZ2_CLIENT +  "' "
			cUpdate += " AND ZZ2_LOJA = '" + T01->ZZ2_LOJA +  "' "
			cUpdate += " AND ZZ2_NUMERO = '" + T01->ZZ2_NUMERO +  "' "
			cUpdate += " AND D_E_L_E_T_ = ' ' "
			
			TCSQLExec(cUpdate)
		ELSEIF ALLTRIM(T01->ZZ2_IMPRES) == "1"
			oPrn:Say(65,150, "REIMPRESS�O",oFont5,100)
		ENDIF
		oPrn:Line(90,0,90,2500)
		//-- COD BARRA	
		cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
		oPrn:FWMSBAR("CODE128" /*cTypeBar*/,2.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

		//oPrn:Say(60,330, cCodINt25 ,oFont5,100)
		oPrn:Say(80,330, cCodINt25 ,oFont2,100)

		//2* LINHA 
		
		//--Cliente                                  
		oPrn:Say(110,30, "C�d. Cliente:",oFont4,100)
		oPrn:Say(110,110, SUBSTR(ALLTRIM(T01->ZZ2_CLIENT)+"- "+ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+T01->ZZ2_CLIENT+T01->ZZ2_LOJA ,"A1_NOME")),0,36) ,oFont5,100)
		
		//--Medicao
		oPrn:Say(110,350,"N� Pedido:  ",oFont4,100)
		oPrn:Say(110,410,cCodINt25,oFont5,100)
		
		//--NOME
		oPrn:Say(125,30, "Nome:    ",oFont4,100) // ATE 37 CARACT PARA O NOME
		oPrn:Say(125,75, SUBSTR(ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+T01->ZZ2_CLIENT+T01->ZZ2_LOJA ,"A1_NREDUZ")),0,50),oFont5,100)
		
		//--Erro ZERO()
		oPrn:Say(125,400, "ESCOLA ERRO ZERO(   )   ",oFont4,100)
		oPrn:Say(125,517, POSICIONE("SA1",1,XFILIAL("SA1")+T01->ZZ2_CLIENT+T01->ZZ2_LOJA ,"A1_YREVISA"),oFont5,100)
		
		//-- Bairro                        
		oPrn:Say(140,30, "Bairro:  ",oFont4,100)
		oPrn:Say(140,75, SUBSTR(ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+T01->ZZ2_CLIENT+T01->ZZ2_LOJA ,"A1_BAIRRO")),0,30),oFont5,100)
		
				
		//-- Estado                        
		oPrn:Say(140,200, "Estado:  ",oFont4,100)
		oPrn:Say(140,245, SUBSTR(ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+T01->ZZ2_CLIENT+T01->ZZ2_LOJA ,"A1_EST")),0,30),oFont5,100)

		//--Cidade
		oPrn:Say(140,270,"Cidade:  ",oFont4,100)
		oPrn:Say(140,315,SUBSTR(ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+T01->ZZ2_CLIENT+T01->ZZ2_LOJA ,"A1_MUN")),0,40),oFont5,100)
	
		//--Tipo de Pedido		
		oPrn:Say(155,30,"Tipo de Pedido: " + T01->ZZ2_TIPO+"("+POSICIONE("ZZG",1,XFILIAL("ZZG",T01->ZZ2_FILIAL) + T01->ZZ2_TIPO,"ZZG->ZZG_DESCRI")+")",oFont4,100)
				
		oPrn:Say(170,30, "Transportadora:" ,oFont4,100)
		
		oPrn:Say(170,200, "Seq Carregamento:" ,oFont4,100)
		
		oPrn:Say(170,400, "Placa de Expedi��o:" ,oFont4,100)
		
		oPrn:Line(175,20,175,580)
		
		nLinha := 190


		//6* LINHA
		//-- produto                          //-- descri��o                              //--quantidade //-- peso liq //--Volume 
		
		oPrn:Say(nLinha,30, "Produto  " ,oFont4,100)
		oPrn:Say(nLinha,125,"Descri��o  " ,oFont4,100)
		oPrn:Say(nLinha,350,"Caixas  " ,oFont4,100)
		oPrn:Say(nLinha,400,"Avulso" ,oFont4,100)
		oPrn:Say(nLinha,450,"Shrink  " ,oFont4,100)
		oPrn:Say(nLinha,500,"Volume  " ,oFont4,100)
		oPrn:Say(nLinha,550,"Total  " ,oFont4,100)

		nLinha += 10
		oPrn:Line(nLinha,20,nLinha,580)


		cQuery := " SELECT ZZ3_PRODUT , ZZ3_DESCRI , ZZ3_QUANT , ZZ3_TIPO "
		cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2 , "+RetSqlName("ZZ3")+" ZZ3 "
		cQuery += " WHERE  ZZ2.D_E_L_E_T_ = '' AND ZZ3.D_E_L_E_T_ = ''  AND "
		cQuery += " ZZ3_FILIAL = '"+T01->ZZ2_FILIAL+"' AND ZZ3_MEDICA='"+T01->ZZ2_MEDICA+"' AND ZZ3_NUMERO = '"+T01->ZZ2_NUMERO+"' AND ZZ3_CLIENT = '"+T01->ZZ2_CLIENT+"' "
		cQuery += " AND   ZZ2_FILIAL = ZZ3_FILIAL "
		cQuery += " AND   ZZ2_NUMERO = ZZ3_NUMERO "
		cQuery += " AND   ZZ2_MEDICA = ZZ3_MEDICA "
		cQuery += " AND   ZZ2_CLIENT = ZZ3_CLIENT "
		cQuery += " ORDER BY ZZ3_PRODUT "
		TcQuery cQuery New Alias T02
		_entr := .f.
		WHILE !T02->(EOF())
			IF nLinha >= 750
				nLinha += 10

				oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
				nPag += 1

				oPrn:EndPage()
				oPrn:StartPage()
				nLinha := 10

				//-- medicao
				oPrn:Say(nLinha+10,30,"Medi��o: " ,oFont4,100)
				oPrn:Say(nLinha+10,80, cCodINt25 ,oFont5,100)

				nLinha += 20
				
				
				//-- produto                          //-- descri��o                              //--quantidade //-- peso liq //--Volume 
				oPrn:Say(nLinha,30, "Produto  " ,oFont4,100)
				oPrn:Say(nLinha,125,"Descri��o  " ,oFont4,100)
				oPrn:Say(nLinha,350,"Caixas  " ,oFont4,100)
				oPrn:Say(nLinha,400,"Avulso" ,oFont4,100)
				oPrn:Say(nLinha,450,"Shrink  " ,oFont4,100)
				oPrn:Say(nLinha,500,"Volume  " ,oFont4,100)
				oPrn:Say(nLinha,550,"Total  " ,oFont4,100)

				nLinha += 7
				oPrn:Line(nLinha,20,nLinha,580)

			ENDIF

			nLinha += 13

			IF T02->ZZ3_TIPO='P'
				if !_entr
					
					cTexto := "PRODUTO AVULSO"
					oPrn:Say(nLinha,36,cTexto,oFont4,100)
					
	
					oPrn:Say(nLinha,380,"  " ,oFont4,100)
					oPrn:Say(nLinha,470,"  " ,oFont4,100)
					oPrn:Say(nLinha,550,"  " ,oFont4,100)
	
					nLinha += 10
					_entr :=.T.
				endif
				
				oPrn:Say(nLinha,30, T02->ZZ3_PRODUT ,oFont5,100)
				oPrn:Say(nLinha,125,SUBSTR(ALLTRIM(T02->ZZ3_DESCRI),0,45) ,oFont5,100)
				
				

				nPeso  += (POSICIONE("SB1",1,XFILIAL("SB1")+T02->ZZ3_PRODUT,"B1_PESO") * T02->ZZ3_QUANT )
			ELSEIF T02->ZZ3_TIPO='K'


				cQuery := " select ZZ7_PRODUT , ZZ7_DESCR , ZZ7_QUANT , ZZ7_VOLUME ,ZZ7_TIPO , ZZ7_SERIE , ZZ7_SRINK, ZZ7_QTDCX, "
				cQuery += " CASE ZZ7_CATEGO WHEN 'A' THEN 'Aluno' "
				cQuery += " WHEN 'P' THEN 'Professor' "
				cQuery += " WHEN 'C' THEN 'Coordenacao' "
				cQuery += " WHEN 'O' THEN 'Outros' end ZZ7_CATEGO , ZZ7_ENVIO , ZZ7_GRUPO  "
				cQuery += " from "+RetSqlName("ZZ7")+" ZZ7 "
				cQuery += " WHERE D_E_L_E_T_='' "
				cQuery += " AND   ZZ7_CODPAI='"+T02->ZZ3_PRODUT+"' "
				cQuery += " AND   ZZ7_TIPO='A' "

				cQuery += " UNION ALL "

				cQuery += " select ZZ7_PRODUT , ZZ7_DESCR , ZZ7_QUANT , ZZ7_VOLUME ,ZZ7_TIPO , ZZ7_SERIE , ZZ7_SRINK, ZZ7_QTDCX, "
				cQuery += " CASE ZZ7_CATEGO WHEN 'A' THEN 'Aluno' "
				cQuery += " WHEN 'P' THEN 'Professor' "
				cQuery += " WHEN 'C' THEN 'Coordenacao' "
				cQuery += " WHEN 'O' THEN 'Outros' end ZZ7_CATEGO , ZZ7_ENVIO , ZZ7_GRUPO  "
				cQuery += " from "+RetSqlName("ZZ7")+" ZZ7 "
				cQuery += " WHERE D_E_L_E_T_= '' "
				cQuery += " AND   ZZ7_CODIGO='"+T02->ZZ3_PRODUT+"' "
				cQuery += " ORDER BY ZZ7_TIPO DESC "

				TcQuery cQuery New Alias T03

				oPrn:Say(nLinha,30, "  " ,oFont4,100)

				cQuery1 := " SELECT ZZO_DESC "
				cQuery1 += " FROM "+RetSqlName("ZZO")+" ZZO "
				cQuery1 += " WHERE ZZO.D_E_L_E_T_='' "
				cQuery1 += " AND ZZO_COD = '"+T03->ZZ7_SERIE+"' "

				TcQuery cQuery1 New Alias T04
				
				if alltrim(cTexto) <> alltrim(SUBSTR(ALLTRIM(T04->ZZO_DESC) +" - "+ALLTRIM(T03->ZZ7_CATEGO)+" - "+ ALLTRIM(POSICIONE("ZZP",1,XFILIAL("ZZP")+T03->ZZ7_GRUPO,"ZZP_DESCRI")) +" - "+T03->ZZ7_ENVIO+" Envio"        ,0,60))
				
					cTexto := SUBSTR(ALLTRIM(T04->ZZO_DESC) +" - "+ALLTRIM(T03->ZZ7_CATEGO)+" - "+ ALLTRIM(POSICIONE("ZZP",1,XFILIAL("ZZP")+T03->ZZ7_GRUPO,"ZZP_DESCRI")) +" - "+T03->ZZ7_ENVIO+" Envio"        ,0,60)
					oPrn:Say(nLinha,36,cTexto,oFont4,100)
					
				EndIf
				T04->(DBCLOSEAREA())

				oPrn:Say(nLinha,380,"  " ,oFont4,100)
				oPrn:Say(nLinha,470,"  " ,oFont4,100)
				oPrn:Say(nLinha,550,"  " ,oFont4,100)

				nLinha 	+= 13
				nTotCx	:= 0
				if T03->ZZ7_QTDCX <> 0
								
					nTotCx := IIF( INT(ROUND(T02->ZZ3_QUANT /T03->ZZ7_QTDCX,2)) == 0 , 0 ,INT(ROUND(T02->ZZ3_QUANT /T03->ZZ7_QTDCX,2)))
				
				Endif				
				 
				nTotAvu := IIF( T02->ZZ3_QUANT - (nTotCx * T03->ZZ7_QTDCX) < 0, 0, T02->ZZ3_QUANT - (nTotCx * T03->ZZ7_QTDCX))

				//TODO CAIXAS
				oPrn:FillRect({nLinha, 042, nLinha-9, 580}, oBrush)
				oPrn:Say(nLinha,42, T02->ZZ3_PRODUT ,oFont5,100)
				oPrn:Say(nLinha,125, SUBSTR(ALLTRIM(T02->ZZ3_DESCRI),0,45) ,oFont5,100)
				
				nTotCx := 0
				nTotAvu :=  0

				WHILE !T03->(EOF())
				
					if  T03->ZZ7_TIPO = 'S'
						T03->(dbSkip())
						loop
					endif
					nLinha += 13
					
					IF nLinha >= 750
						nLinha += 10

						oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
						nPag += 1

						oPrn:EndPage()
						oPrn:StartPage()
						//-- logo SAS
						cFigura := "\system\OS"+cEmpAnt+".BMP"
						oPrn:SayBitmap(10,10,cFigura,150,60)
						//-- TITULO
						oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
						oPrn:Line(90,0,90,2500)
						//-- COD BARRA	
						cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
						oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

						oPrn:Say(60,330, cCodINt25 ,oFont5,100)
						nLinha := 110
					ENDIF
					
					
					If T03->ZZ7_SRINK <> '1'
						oPrn:FillRect({nLinha, 042, nLinha-9, 580}, oBrush)
					Endif 
					
					oPrn:Say(nLinha,42, T03->ZZ7_PRODUT ,oFont5,100)
					oPrn:Say(nLinha,125,SUBSTR(ALLTRIM(T03->ZZ7_DESCR),0,50) ,oFont5,100)
					oPrn:Say(nLinha,350," ",oFont5,100) //Caixas
					oPrn:Say(nLinha,460, IIF(T03->ZZ7_SRINK == '1',"Sim","N�o") ,oFont5,100)

					nPeso  += ((POSICIONE("SB1",1,XFILIAL("SB1")+T03->ZZ7_PRODUT,"B1_PESO") * T03->ZZ7_QUANT) * T02->ZZ3_QUANT)
					T03->(dbSkip())
				ENDDO
				T03->(DBCLOSEAREA())
			ENDIF

			T02->(dbSkip())
		ENDDO
		T02->(DBCLOSEAREA())

		nLinha += 7
		oPrn:Line(nLinha,20,nLinha,580)
		nLinha += 13

		//7* LINHA

		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF

		nQuant := 0
		nPeso  := 0

		nLinha += 7
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		oPrn:Line(nLinha,20,nLinha,580)
		nLinha += 30

		//8* LINHA
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		//Separado por                   conferido por 
		oPrn:Say(nLinha,30, "Separado por: _____________________________" ,oFont4,100)
		oPrn:Say(nLinha,320,"Conferido por: _____________________________" ,oFont4,100)
		nLinha += 20
		
		nLinha += 7

		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		oPrn:Line(nLinha,20,nLinha,580)
		nLinha += 10

		//9* LINHA
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		//observa��o 	 	
		oPrn:Say(nLinha,30, "Observa��o:" ,oFont4,100)
		oPrn:Say(nLinha,125,SUBSTR(ALLTRIM(T01->ZZ2_OBS),0,60) ,oFont5,100)
		nLinha += 10
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		oPrn:Say(nLinha,125,SUBSTR(ALLTRIM(T01->ZZ2_OBS),60,120) ,oFont5,100)
		nLinha += 10
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		oPrn:Say(nLinha,125,SUBSTR(ALLTRIM(T01->ZZ2_OBS),121,180) ,oFont5,100)
		nLinha += 10
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		oPrn:Say(nLinha,125,SUBSTR(ALLTRIM(T01->ZZ2_OBS),181,200) ,oFont5,100)
		nLinha += 10

		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		//expedi��o                          //() material revisado 	 	
		oPrn:Say(nLinha,30, "Local Expedi��o:" ,oFont4,100)
		oPrn:Say(nLinha,125, T01->ZZ2_LOCEX+" - "+POSICIONE("SX5",1,XFILIAL("SX5")+"X2"+T01->ZZ2_LOCEX,"X5_DESCRI") ,oFont5,100)
				
		nLinha += 15
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF

		oPrn:Say(nLinha,30, "Data de Solicita��o:"  ,oFont4,100)
		oPrn:Say(nLinha,135, DTOC(stod(T01->ZZ2_DTSOLI)) ,oFont5,100)
		
		nLinha += 15
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		
		oPrn:Say(nLinha,30, "Data de Emiss�o:" ,oFont4,100)
		oPrn:Say(nLinha,125, dtoc(stod(T01->ZZ2_DTGER)) ,oFont5,100)

		//oPrn:Line(nLinha,20,nLinha,580)
		nLinha += 10

		//10* LINHA
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		//transportadora 	 	
		
		nLinha += 20
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1

			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF

		nLinha += 20
		//Placa Expedi��o
		IF nLinha >= 750
			nLinha += 10

			oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
			nPag += 1
			oPrn:EndPage()
			oPrn:StartPage()
			//-- logo SAS
			cFigura := "\system\OS"+cEmpAnt+".BMP"
			oPrn:SayBitmap(10,10,cFigura,150,60)
			//-- TITULO
			oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
			oPrn:Line(90,0,90,2500)
			//-- COD BARRA	
			cCodINt25 := T01->ZZ2_FILIAL+T01->ZZ2_NUMERO+T01->ZZ2_MEDICA
			oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

			oPrn:Say(60,330, cCodINt25 ,oFont5,100)
			nLinha := 110
		ENDIF
		

		nLinha += 10

		oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)

		//oPrn:Say(800,30, "FIM:" ,oFont4,100)	
		//finaliza a pagina
		oPrn:EndPage()

		T01->(DBSkip())

	END DO
	oPrn:Print()

	T01->(DBCloseArea())

	oPrn:SetViewPDF ( .T. )

Return


Static Function AjustaSX1(cPerg)

	PutSX1(cPerg, "01","Medi��o de ?",                           "","","mv_ch01","C",GetSx3Cache( "ZZ2_MEDICA" , "X3_TAMANHO" ),0,0,"G","","ZZ2","","","MV_PAR01","","","","","","","","","","","")
	PutSX1(cPerg, "02","Medi�ao at� ?",                          "","","mv_ch02","C",GetSx3Cache( "ZZ2_MEDICA" , "X3_TAMANHO" ),0,0,"G","","ZZ2","","","MV_PAR02","","","","","","","","","","","")
	PutSX1(cPerg, "03","Emiss�o de ?",                           "","","mv_ch03","D",10,0,0,"G","","","","","MV_PAR03","","","","","","",,,,"","")
	PutSX1(cPerg, "04","Emiss�o at� ?",                          "","","mv_ch04","D",10,0,0,"G","","","","","MV_PAR04","","","","","","",,,,"","")
	PutSX1(cPerg, "05","Cliente de ?",                           "","","mv_ch05","C",GetSx3Cache( "A1_COD" , "X3_TAMANHO" ),0,0,"G","","SA1","","","MV_PAR05","","","","","","",,,,"","")
	PutSX1(cPerg, "06","Cliente at� ?",                          "","","mv_ch06","C",GetSx3Cache( "A1_COD" , "X3_TAMANHO" ),0,0,"G","","SA1","","","MV_PAR06","","","","","","",,,,"","")
	PutSX1(cPerg, "07","Imprime quantidade ?",                   "","","mv_ch07","C",04,0,0,"C","","","","","MV_PAR07","SIM","","","","N�O","",,,,"","")
	PutSX1(cPerg, "08","Cliente Bloqueado ?",                    "","","mv_ch08","C",04,0,0,"C","","","","","MV_PAR08","SIM","","","","N�O","",,,,"","")
	PutSX1(cPerg, "09","Tipo de Pedido ?",                       "","","mv_ch09","C",GetSx3Cache( "ZZL_COD" , "X3_TAMANHO" ),0,0,"G","","ZZL","","","MV_PAR09","","","","","","",,,,"","")
	PutSX1(cPerg, "10","Local de Expedi��o ?",                   "","","mv_ch010","C",GetSx3Cache( "ZZ2_LOCEX" , "X3_TAMANHO" ),0,0,"G","","X2","","","MV_PAR010","","","","","","",,,,"","")

return