#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"
#Include "RWMAKE.CH"
#include 'parmtype.ch'

/*/{Protheus.doc} SASR099
Relat�rio de SEPARA�AO baseado no Pedido de Vendas.
@type function
@author TOTVS
@since 28/10/2016
@version 1.0
@param cFilPed, character, (C�digo da filial do PV)
@param cNum, character, (C�digo do PV)
@param cCli, character,(Cliente do PV )
@param cLoja, character, (Loja do Cliente do PV)
@example
(examples)
@see (links_or_references)
/*/
User function SASR099(cFilPed, cNum , cCli, cLoja)

	//Objetos para tamanho e tipo das fontes
	
	Local oBrush    := TBrush():New(,RGB(205,205,205))
	Local cFigura   := ""	
	Local nQuant    := 0
	Local nPeso     := 0
	Local cCodINt25	
	Local cTexto := ""
	Local aRet := {}
	Local aParamBox := {}
	Private oPrn      := FwMSPrinter():New( 'Ordem_de_separa��o' , 6 , .F. , , .T. )
	Private nLinha := 0
	Private nPag      := 1
	Private oFont1    := TFont():New( "Times New Roman",,10,,.T.,,,,,.F.)
	Private oFont2    := TFont():New( "Tahoma",,15,,.T.,,,,,.F.)
	Private oFont3    := TFont():New( "Arial"       ,,20,,.F.,,,,,.F.)
	Private oFont4    := TFont():New( "Times New Roman",,14,,.T.,,,,,.F.)
	Private oFont5    := TFont():New( "Tahoma",,12,,.F.,,,,,.F.)
	
	/*select na SC6*/
	cQuery := " SELECT "
	cQuery += "   SC5.C5_MENNOTA , "
	cQuery += "   SC5.C5_YTPVEN , "
	cQuery += "   SC5.C5_TRANSP , "
	cQuery += "   SC5.C5_YLOCEX, "
	cQuery += "   SC5.C5_EMISSAO, "
	cQuery += "   SC5.C5_YDTSOL, "
	cQuery += "   SC6.C6_PRODUTO, "
	cQuery += "   SC6.C6_DESCRI, "
	cQuery += "   SC6.C6_QTDVEN, "
	cQuery += "   SC6.C6_CLI, "
	cQuery += "   SC6.C6_LOJA, "
	cQuery += "   SC6.C6_YCODKIT "
	cQuery += "   FROM " + RETSQLNAME("SC6") + " SC6 "
	cQuery += "   JOIN " + RETSQLNAME("SC5") + " SC5 "
	cQuery += "   ON C5_FILIAL = C6_FILIAL AND "
	cQuery += "     C5_NUM = C6_NUM "
	cQuery += "  WHERE SC6.D_E_L_E_T_ = ' ' "
	cQuery += "    AND SC5.D_E_L_E_T_ = ' ' "
	cQuery += "    AND SC6.C6_NUM = '" + cNum + "' "
	cQuery += "    AND SC6.C6_FILIAL = '" + cFilPed + "'  "
	cQuery += "    AND SC6.C6_CLI = '" + cCli + "' "
	cQuery += "    AND SC6.C6_LOJA = '" + cLoja + "' "
	cQuery += "    ORDER BY C6_YCODKIT "
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XSC6",.F.,.T.)
	
	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	If SC5->(dbSeek(cFilPed+cNum))
		SC5->(RecLock("SC5",.F.))
			SC5->C5_YIMPRES	:=	'S'
		SC5->(MsUnlock())
	Endif
	cKits	:= ""
	cLocEX	:= ""
	cEmiss	:= ""
	cDtSol	:= ""
	cOBS	:= ""
	NovaPagina(cFilPed, cNum,1)
	
	WHILE XSC6->(!EOF())
		
		If !empty(XSC6->C6_YCODKIT) .and. !(XSC6->C6_YCODKIT $ cKits)
			cKits+= XSC6->C6_YCODKIT
			

				cQuery := " select ZZ7_CODIGO,ZZ7_PRODUT , ZZ7_DESCR , ZZ7_QUANT , ZZ7_VOLUME ,ZZ7_TIPO , ZZ7_SERIE , ZZ7_SRINK,ZZ7_QTDCX , "
				cQuery += " CASE ZZ7_CATEGO WHEN 'A' THEN 'Aluno' " 
				cQuery += " WHEN 'P' THEN 'Professor' "
				cQuery += " WHEN 'C' THEN 'Coordenacao' " 
				cQuery += " WHEN 'O' THEN 'Outros' end ZZ7_CATEGO , ZZ7_ENVIO , ZZ7_GRUPO  "
				cQuery += " from "+RetSqlName("ZZ7")+" ZZ7 "
				cQuery += " WHERE D_E_L_E_T_= '' "
				cQuery += " AND   ZZ7_CODIGO='"+XSC6->C6_YCODKIT+"' "
				cQuery += " ORDER BY ZZ7_TIPO DESC "

				TcQuery cQuery New Alias T03

				oPrn:Say(nLinha,30, "  " ,oFont4,100)	

				cQuery1 := " SELECT ZZO_DESC "
				cQuery1 += " FROM "+RetSqlName("ZZO")+" ZZO "
				cQuery1 += " WHERE ZZO.D_E_L_E_T_='' "
				cQuery1 += " AND ZZO_COD = '"+T03->ZZ7_SERIE+"' "

				TcQuery cQuery1 New Alias T04
				
				cTexto := SUBSTR(ALLTRIM(T04->ZZO_DESC) +" - "+ALLTRIM(T03->ZZ7_CATEGO)+" - "+;
				 			ALLTRIM(POSICIONE("ZZP",1,XFILIAL("ZZP")+T03->ZZ7_GRUPO,"ZZP_DESCRI")) +;
				 			" - "+T03->ZZ7_ENVIO+" Envio",0,60)
				 			
				oPrn:Say(nLinha,30,cTexto,oFont4,100)
				nLinha += 10	

				nTotCx	:= 0
				nTotAvu	:= 0

				if T03->ZZ7_QTDCX <> 0
									
					nTotCx := IIF( INT(ROUND(XSC6->C6_QTDVEN /T03->ZZ7_QTDCX,2)) == 0 , 0 ,INT(ROUND(XSC6->C6_QTDVEN /T03->ZZ7_QTDCX,2)))
					
				Endif				
					 
				nTotAvu := IIF( XSC6->C6_QTDVEN - (nTotCx * T03->ZZ7_QTDCX) < 0, 0, XSC6->C6_QTDVEN - (nTotCx * T03->ZZ7_QTDCX))


				oPrn:FillRect({nLinha, 030, nLinha-9, 580}, oBrush)
				oPrn:Say(nLinha,30, T03->ZZ7_CODIGO ,oFont5,100)	
				oPrn:Say(nLinha,125, SUBSTR(ALLTRIM(T03->ZZ7_DESCR),0,50) ,oFont5,100)
				oPrn:Say(nLinha,360, PADL(CVALTOCHAR(nTotCx),10,) ,oFont5,100)
				oPrn:Say(nLinha,393, PADL(CVALTOCHAR(nTotAvu),10,) ,oFont5,100)
			
				nLinha += 10	
				T03->(DBCLOSEAREA())
				T04->(DBCLOSEAREA())
			
		Endif

		cSRINK:= ' ' 

		If !empty(XSC6->C6_YCODKIT)

			cQuery := " SELECT ZZ7_SRINK,ZZ7_QTDCX,ZZ7_QUANT "
			cQuery += " from "+RetSqlName("ZZ7")+" ZZ7 "
			cQuery += " WHERE D_E_L_E_T_= '' "
			cQuery += " AND   ZZ7_CODPAI='"+XSC6->C6_YCODKIT+"' "
			cQuery += " AND   ZZ7_PRODUT='"+XSC6->C6_PRODUTO+"' "
			cQuery += " ORDER BY ZZ7_TIPO DESC "

			TcQuery cQuery New Alias T03

			cSRINK	:= iif(T03->ZZ7_SRINK = '1',"Sim","N�o ")
			If T03->ZZ7_SRINK <> '1'
				oPrn:FillRect({nLinha, 030, nLinha-9, 580}, oBrush)
			Endif 
		
			T03->(DBCLOSEAREA())
		Endif
			
		oPrn:Say(nLinha,30, XSC6->C6_PRODUTO ,oFont5,100)
		oPrn:Say(nLinha,125,SUBSTR(ALLTRIM(XSC6->C6_DESCRI),0,45) ,oFont5,100)
		
		//If !empty(XSC6->C6_YCODKIT) .and. cSRINK = 'N�o' //Kit
		//oPrn:Say(nLinha,395, PADL(CVALTOCHAR(XSC6->C6_QTDVEN),10,) ,oFont5,100)
		
		//Elseif empty(XSC6->C6_YCODKIT) //Produto
		//Endif 
		
		oPrn:Say(nLinha,470,cSRINK,oFont5,100)
		oPrn:Say(nLinha,500," ",oFont5,100)
		oPrn:Say(nLinha,500, PADL(CVALTOCHAR(XSC6->C6_QTDVEN),10,) ,oFont5,100)
		
		nPeso  += (POSICIONE("SB1",1,XFILIAL("SB1")+XSC6->C6_PRODUTO,"B1_PESO") *  XSC6->C6_QTDVEN )	
		
		nLinha += 13
		
		IF nLinha >= 750
		
			oPrn:EndPage()
			
			NovaPagina(cFilPed, cNum,2)
		
		EndIf
		cLocEX	:= XSC6->C5_YLOCEX
		cEmiss	:= XSC6->C5_EMISSAO
		cDtSol	:= XSC6->C5_YDTSOL
		cOBS	:= XSC6->C5_MENNOTA
		
		XSC6->(DBSKIP())
	
	ENDDO
		
	XSC6->(DBGOTOP())
			

	IF nLinha >= 750
		oPrn:EndPage()
		NovaPagina(cFilPed, cNum,2)
			
	ENDIF
		
		//-- totalizadores                         quantidade      peso    volume 
	oPrn:Say(nLinha,30, "PESO TOTAL" ,oFont4,100)
	oPrn:Say(nLinha,515, PADL(cValToChar(TRANSFORM(nPeso,"@E 999,999.99")),10,) ,oFont5,100)
		
	nLinha += 7
		
	IF nLinha >= 750
		oPrn:EndPage()
		NovaPagina(cFilPed, cNum,2)
			
	ENDIF
	oPrn:Line(nLinha,20,nLinha,580)
	nLinha += 30

		//8* LINHA
	IF nLinha >= 750
		oPrn:EndPage()
		NovaPagina(cFilPed, cNum,2)
			
	ENDIF
		//Separado por                   conferido por 
	oPrn:Say(nLinha,30, "Separado por: _____________________________" ,oFont4,100)
	oPrn:Say(nLinha,320,"Conferido por: _____________________________" ,oFont4,100)
		
	nLinha += 27

	IF nLinha >= 750
		oPrn:EndPage()
		NovaPagina(cFilPed, cNum,2)
			
	ENDIF
	oPrn:Line(nLinha,20,nLinha,580)
	nLinha += 10

		//9* LINHA
	IF nLinha >= 750
		oPrn:EndPage()
		NovaPagina(cFilPed, cNum,2)
			
	ENDIF
		//observa��o 	 	
	oPrn:Say(nLinha,30, "Observa��o:" ,oFont4,100)
	oPrn:Say(nLinha,125,SUBSTR(cOBS,1,60) ,oFont5,100)
	nLinha += 10

	If nLinha >= 750
		oPrn:EndPage()
		NovaPagina(cFilPed, cNum,2)
	Endif
	
	//expedi��o                          //() material revisado 	 	
	oPrn:Say(nLinha,30, "Local Expedi��o:" ,oFont4,100)
	oPrn:Say(nLinha,130, cLocEX+" - "+POSICIONE("SX5",1,XFILIAL("SX5")+"X2"+cLocEX,"X5_DESCRI") ,oFont5,100) ////
	
				
	nLinha += 15
	IF nLinha >= 750
		oPrn:EndPage()
		NovaPagina(cFilPed, cNum,2)
			
	ENDIF

	oPrn:Say(nLinha,30, "Data de Solicita��o:"  ,oFont4,100)
	oPrn:Say(nLinha,130,SUBSTR(cDtSol,7,2)+"/"+SUBSTR(cDtSol,5,2)+"/"+SUBSTR(cDtSol,1,4),oFont5,100) ///
		
	nLinha += 15
	IF nLinha >= 750
		oPrn:EndPage()
		NovaPagina(cFilPed, cNum,2)
	ENDIF
		
	oPrn:Say(nLinha,30, "Data de Emiss�o:" ,oFont4,100)
	oPrn:Say(nLinha,130, SUBSTR(cEmiss,7,2)+"/"+SUBSTR(cEmiss,5,2)+"/"+SUBSTR(cEmiss,1,4) ,oFont5,100)
	
	nLinha += 10
		
	oPrn:EndPage()
	XSC6->(DBCLOSEAREA())

	oPrn:Print()
	oPrn:SetViewPDF ( .T. )

Return


Static Function AjustaSX1(cPerg)

	PutSX1(cPerg, "01","Pedido de ?",                           "","","mv_ch01","C",GetSx3Cache( "C5_NUM" , "X3_TAMANHO" ),0,0,"G","","ZZ2","","","MV_PAR01","","","","","","","","","","","")
	PutSX1(cPerg, "02","Pedido at� ?",                          "","","mv_ch02","C",GetSx3Cache( "C5_NUM" , "X3_TAMANHO" ),0,0,"G","","ZZ2","","","MV_PAR02","","","","","","","","","","","")
	PutSX1(cPerg, "09","Tipo de Pedido ?",                       "","","mv_ch09","C",GetSx3Cache( "ZZL_COD" , "X3_TAMANHO" ),0,0,"G","","ZZL","","","MV_PAR09","","","","","","",,,,"","")
	PutSX1(cPerg, "10","Local de Expedi��o ?",                   "","","mv_ch010","C",GetSx3Cache( "ZZ2_LOCEX" , "X3_TAMANHO" ),0,0,"G","","X2","","","MV_PAR010","","","","","","",,,,"","")

return


Static Function NovaPagina(cFilPed, cNum, nTipo)


	Local cFigura := "\system\OS"+cEmpAnt+".BMP"
	Local cCodINt25 := ""
	
	oPrn:StartPage()

	if nTipo == 1
	
		oPrn:SayBitmap(30,10,cFigura,150,60)
	
	//-- TITULO
		oPrn:Say(55,150, "Ordem de Separa��o ",oFont2,100)
	
		oPrn:Say(65,150, "IMPRESS�O",oFont5,100)
		
		oPrn:Line(90,0,90,2500)
		
	//-- COD BARRA	
		cCodINt25 := alltrim(cFilPed)+alltrim(cNum)
		oPrn:FWMSBAR("CODE128" /*cTypeBar*/,2.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

		oPrn:Say(80,330, cCodINt25 ,oFont2,100)

	//2* LINHA 
		
	//--Cliente                                  
		oPrn:Say(110,30, "C�d. Cliente:",oFont4,100)
		oPrn:Say(110,110, SUBSTR(ALLTRIM(XSC6->C6_CLI)+"- "+ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+XSC6->C6_CLI+XSC6->C6_LOJA ,"A1_NOME")),0,36) ,oFont5,100)
		
	//--Medicao
		oPrn:Say(110,350,"N� Pedido:  ",oFont4,100)
		oPrn:Say(110,410,cCodINt25,oFont5,100)
		
	//--NOME
		oPrn:Say(125,30, "Nome:    ",oFont4,100) // ATE 37 CARACT PARA O NOME
		oPrn:Say(125,75, SUBSTR(ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+XSC6->C6_CLI+XSC6->C6_LOJA ,"A1_NREDUZ")),0,50),oFont5,100)
		
	//--Erro ZERO()
		oPrn:Say(125,400, "(  ) ESCOLA ERRO ZERO   ",oFont4,100)
		
	//-- Bairro                        
		oPrn:Say(140,30, "Bairro:  ",oFont4,100)
		oPrn:Say(140,75, SUBSTR(ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+XSC6->C6_CLI+XSC6->C6_LOJA ,"A1_BAIRRO")),0,30),oFont5,100)
					
	//-- Estado                        
		oPrn:Say(140,200, "Estado:  ",oFont4,100)
		oPrn:Say(140,245, SUBSTR(ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+XSC6->C6_CLI+XSC6->C6_LOJA ,"A1_EST")),0,30),oFont5,100)

	//--Cidade
		oPrn:Say(140,270,"Cidade:  ",oFont4,100)
		oPrn:Say(140,315,SUBSTR(ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+XSC6->C6_CLI+XSC6->C6_LOJA ,"A1_MUN")),0,40),oFont5,100)
	
	//--Tipo de Pedido		
		oPrn:Say(155,30,"Tipo de Pedido: ",oFont4,100)
		oPrn:Say(155,115,SUBSTR(ALLTRIM(POSICIONE("ZZG",1,XFILIAL("ZZG")+XSC6->C5_YTPVEN,"ZZG_DESCRI")),0,40),oFont5,100)
				
		oPrn:Say(170,30, "Transportadora:" ,oFont4,100)
		oPrn:Say(170,115,SUBSTR(ALLTRIM(POSICIONE("SA4",1,XFILIAL("SA4")+XSC6->C5_TRANSP ,"A4_NOME")),0,40),oFont5,100)
		
		//oPrn:Say(170,200, "Seq Carregamento:" ,oFont4,100)
		
		oPrn:Say(170,400, "Placa de Expedi��o:" ,oFont4,100)
		
		oPrn:Line(175,20,175,580)

		nLinha := 190

	//6* LINHA
	//-- produto                          //-- descri��o                              Caixas //--Avulso //-- Shrink //--Volume //Quantidade 	
		
		oPrn:Say(nLinha,30, "Produto  " ,oFont4,100)
		oPrn:Say(nLinha,125,"Descri��o  " ,oFont4,100)
		oPrn:Say(nLinha,395,"Caixas  " ,oFont4,100)
		oPrn:Say(nLinha,430,"Avulso" ,oFont4,100)
		oPrn:Say(nLinha,465,"Shrink  " ,oFont4,100)
		oPrn:Say(nLinha,500,"Volume  " ,oFont4,100)
		oPrn:Say(nLinha,540,"Total " ,oFont4,100)

		nLinha += 10
		oPrn:Line(nLinha,20,nLinha,580)
		
		oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
	
		nLinha += 13
		nPag++
	
	Else
	
		cFigura := "\system\OS"+cEmpAnt+".BMP"
		oPrn:SayBitmap(10,10,cFigura,150,60)
		
		//-- TITULO
		oPrn:Say(50,150, "Ordem de Separa��o ",oFont2,100)
		oPrn:Line(90,0,90,2500)
		//-- COD BARRA	
		cCodINt25 := alltrim(cFilPed)+alltrim(cNum)
		oPrn:FWMSBAR("CODE128" /*cTypeBar*/,0.5/*nRow*/ ,27/*nCol*/ ,cCodINt25  /*cCode*/,oPrn/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.02 /*nWidth*/,1/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)

		oPrn:Say(60,330, cCodINt25 ,oFont5,100)
		nLinha := 110
		
		oPrn:Line(nLinha,20,nLinha,580)
		
		oPrn:Say(830,500, CVALTOCHAR(nPag)+" Pag" ,oFont4,100)
		nPag++
	
	EndIf

Return