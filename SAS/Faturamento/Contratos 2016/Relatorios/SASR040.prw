// #########################################################################################
// Projeto: Romaneio por Transportadora
// Modulo : Faturamento
// Fonte  : SASR040
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Weskley Silva           | Descricao Romanerio por transportadora
// ---------+-------------------+-----------------------------------------------------------
// 03/08/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "tbiconn.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "protheus.ch"
#include "topconn.ch"

user function SASR040()

	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local cLocal          := "c:\TEMP\"					
	Local cFilePrint := "romaneio2016.xlsx"
	Local xArea := GetArea()	
	Local cLogo := "\system\OS01.bmp"
	Private cPerg		:= "ROMA"
	Private oPrinter
	Private oFont1 	:= TFont():New( "Arial",,11,,.T.,,,,,  .F. )
	Private oFont2 	:= TFont():New( "Arial",,10,,.F.,,,,,  .F. )
	Private oFont3 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Private nQuantNf := 0
	Private nQuantVol := 0
	Private nPesoLi := 0
	Private nCont := 0
	Private nLin   := 25
	Private nTmLin := 15
	Private nSalto := 20


	AjustaSX1(cPerg)

	If !Pergunte(cPerg, .T.)
		Return
	Endif


	oPrinter := FWMSPrinter():New(cFilePrint, IMP_SPOOL, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetPortrait()
	oPrinter:StartPage()	

	ProcRegua(0)

	oPrinter:Say ( nLin, 200, "ROMANEIO DE ENTREGA POR TRANSPORTADORA",  oFont3 )
	oPrinter:SayBitmap( nLin-010, 050, cLogo, 100, 058)
	nLin := nLin + nTmLin

	if Select("ROM") > 0
		ROM->(dbCloseArea())
	endif

	cQuery := " SELECT F2_DOC,F2_FILIAL AS Z5_EMPPV1,F2_CLIENTE,A1_NOME, "   
	cQuery += " CASE WHEN (A1_ESTE ='' OR A1_ESTE = NULL OR LTRIM(RTRIM(A1_ESTE)) = 'VAZIO' OR LTRIM(RTRIM(A1_ESTE)) = 'VA') THEN A1_EST ELSE A1_ESTE END AS UF_ENT," 
	cQuery += " CASE WHEN (A1_MUNE = '' OR A1_MUNE = NULL OR LTRIM(RTRIM(A1_MUNE)) = 'VAZIO') THEN A1_MUN ELSE A1_MUNE END AS MUNICIPIO_ENT, "
	cQuery += " F2_VOLUME1,SUM(D2_PESO) AS PESO_LIQ,CONVERT(DATE,F2_EMISSAO,101) AS 'DT_EMISSAO',"        
	cQuery += " A4_COD  AS COD_TRANSP,A4_NOME AS NOME_TRANS,'' DA3_COD,'' DA3_DESC "
	cQuery += " FROM  SF2010 JOIN  SA1010 ON(F2_LOJA = A1_LOJA AND F2_CLIENTE = A1_COD)"
	cQuery += " JOIN SD2010 ON(D2_FILIAL = F2_FILIAL AND D2_DOC = F2_DOC)"   
	cQuery += " LEFT JOIN SA4010 ON(A4_COD = F2_TRANSP) "           
	cQuery += " WHERE  F2_VOLUME1 >=1 AND A4_COD = '"+mv_par01+"' AND F2_EMISSAO  BETWEEN '"+mv_par02+"' AND '"+mv_par03+"' "
	cQuery += " AND CASE WHEN (A1_ESTE ='' OR A1_ESTE = NULL OR LTRIM(RTRIM(A1_ESTE)) = 'VAZIO' OR LTRIM(RTRIM(A1_ESTE)) = 'VA') THEN A1_EST ELSE A1_ESTE END BETWEEN '"+mv_par04+"' AND '"+mv_par05+"' "
	cQuery += " AND SF2010.D_E_L_E_T_ ='' AND SD2010.D_E_L_E_T_ ='' AND SA1010.D_E_L_E_T_ ='' AND SA4010.D_E_L_E_T_ = ''"
	cQuery += " AND SF2010.F2_CLIENTE NOT IN('300056','051373','300057','051373','103031','300497','101075','056041','304295')"
	cQuery += " AND D2_TES NOT IN('013','014','015','016','503','504','512')"
	cQuery += " GROUP BY F2_DOC,F2_FILIAL,F2_CLIENTE,A1_NOME,A1_ESTE,A1_EST,A1_MUNE, A1_MUN,"
	cQuery += " F2_VOLUME1,F2_PLIQUI,F2_EMISSAO,A4_COD,A4_NOME "

	IF SELECT("ROM") > 0
		DbSelectArea("ROM")
	endif	

	TcQuery cQuery New Alias ROM	 

	cTrans := ROM->NOME_TRANS
	cEmiss := ROM->DT_EMISSAO

	oPrinter:Say ( nLin, 200, "Transp", oFont1 )
	oPrinter:Say ( nLin, 240, cTrans, oFont1 )
	nLin := nLin + nTmLin 

	oPrinter:Say ( nLin, 200, "Remessa", oFont1 )
	oPrinter:Say ( nLin, 260,  cEmiss, oFont1 )
	nLin := nLin + nTmLin 

	nLin := nLin + 25
	oPrinter:Say ( nLin,  20, "NF",       oFont1 )
	oPrinter:Say ( nLin,  80,  "CLIENTE",  oFont1 )
	oPrinter:Say ( nLin, 300, "UF",       oFont1 )
	oPrinter:Say ( nLin, 350, "CIDADE",   oFont1 )
	oPrinter:Say ( nLin, 490, "VOL",      oFont1 )
	oPrinter:Say ( nLin, 525, "PESO LIQ", oFont1 )


	While !ROM->(EOF())

		IF nLin >= 750
			nLin   := 25
			oPrinter:EndPage()
			oPrinter:StartPage()
		ENDIF

		oPrinter:Say ( nLin+10,   20, ROM->F2_DOC ,                     oFont2 )
		oPrinter:Say ( nLin+10,   80, ROM->A1_NOME,                     oFont2 )
		oPrinter:Say ( nLin+10,  300, ROM->UF_ENT ,                     oFont2 )
		oPrinter:Say ( nLin+10,  350, ROM->MUNICIPIO_ENT ,              oFont2 )
		oPrinter:Say ( nLin+10,  490, cValToChar(ROM->F2_VOLUME1),      oFont2 )
		oPrinter:Say ( nLin+10,  525, cValToChar(ROM->PESO_LIQ) ,       oFont2 )


		nQuantVol += ROM->F2_VOLUME1
		nPesoLi   += ROM->PESO_LIQ

		ROM->(dbSkip())
		nLin := nLin + 10
		nCont++
	ENDDO
	nLin := nLin + 30
	FunRdp()

	ROM->(DbCloseArea())

	oPrinter:EndPage()

	oPrinter:Preview()
	Restarea(xArea)
return

Static Function FunRdp

	nLin := nLin + 10
	
    IF nLin >= 750
		oPrinter:EndPage()
	ENDIF
		
	oPrinter:Say ( nLin, 250, "QTD VOL",  oFont1 )
	oPrinter:Say ( nLin, 450, padl(TransForm(nQuantVol, "@E 999,999,999"),20),  oFont2 )				

	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 250, "PESO TOTAL:",  oFont1 )
	oPrinter:Say ( nLin, 450, padl(TransForm(nPesoLi, "@E 9999,999,999.99"),20),  oFont2 )				

	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 250, "TOTAL NF",  oFont1 )
	oPrinter:Say ( nLin, 450, padl(TransForm(nCont, "@E 9999,999,999.99"),20),  oFont2 )
	
	nLin := nLin + nTmLin
	oPrinter:Line( nLin, 120, nLin, 220)	
    oPrinter:Say ( nLin, 040, "Resp.Expedi��o",  oFont1 )
	oPrinter:Line( nLin, 120, nLin, 220)	
	oPrinter:Say ( nLin, 250, "Resp.Transportadora",  oFont1 )
	
				
Return

Static Function AjustaSX1(cPerg)
	PutSx1(cPerg, "01","Transportadora"			,""		,""		,"mv_ch01","C",06,0,0,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Dt Inicial"		        ,""		,""		,"mv_ch02","C",08,0,0,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","Dt Final"			    ,""		,""		,"mv_ch03","C",08,0,0,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "04","UF de"			        ,""		,""		,"mv_ch04","C",02,0,0,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "05","UF Ate"				    ,""		,""		,"mv_ch05","C",02,0,0,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
Return




