// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : SASR038
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Rog�rio J�come             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 15/03/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------


#include 'protheus.ch'
#include 'parmtype.ch'
#include 'TopConn.CH'
#include "tbiconn.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"

user function SASR038()
	
	
	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local cLocal          := "c:\"
	//Local xArea := GetArea()
	Local Vez := 0

	local _ano := space(4)
	local _filial := space(6)
	local _client := space(TAMSX3("A1_COD")[1])
	local _loja := space(TAMSX3("A1_LOJA")[1])
	local _data := space(TamSX3("D1_DTDIGIT")[1])
	Local aPergs := {}
	Local aRet := {}
	Local lRet
	Local cQuery
	
	Local oRadMenu1
	Local nRadMenu1 := 1
	Local aButtons := {}
	Local oTButton1 := Nil

	Private iX := 0
	Private oPrinter
	Private cFilePrint := "contratos.pdf"
	Private nQuant := 0
	Private qVol := 0
	Private qPesLiq := 0
	Private oFont1 	:= TFont():New( "Arial",,10,,.F.,,,,,.F.)
	Private oFont2 	:= TFont():New( "Arial",,9,,.F.,,,,,.F.)
	Private oFont3 	:= TFont():New( "Arial",,12,,.T.,,,,,.F.)
	Private oFont4 	:= TFont():New( "Arial",,10,,.T.,,,,,.F.)
	
	Private nLin   := 0
	Private nTmLin := 60
	Private nSalto := 20
	Private nPag	:= 0
	Private cContra := ""
	Private cSitua := ""
	Private oBrush
	Private cRazao := ""
	Private nTContrato := 0
	Private nTEnviados := 0
	private nTAditivos := 0
	Private nTDevolvidos := 0
	
	
	
	Private nAdiant := 0
	
	Private nTSaldo := 0
	
	//Totalizadores se��o financeiro
	Private nTFin := 0
	Private nTFJur := 0
	Private nTFMulta := 0 
	Private nTotalDesconto := 0
	Private nTFDesc := 0
	Private nTFBai := 0
	
	//Totalizadores se��o fiscal
	Private nTFis := 0
	Private nTFrete := 0
	Private nTJuros := 0
	Private nTDesconto := 0
	Private nTAdFisc := 0
	Private nTBaixaNF := 0
	
	
	Static oDlg
	
	

	aAdd( aPergs ,{1,"Ano: ",_ano,"@!",'.T.',,'.T.',4,.F.})
	aAdd( aPergs ,{1,"Filial: ",_filial,"@!",'.T.',"SM0",'.T.',6,.T.})
	aAdd( aPergs ,{1,"Cliente: ",_client,"@!",'.T.',"SA1",'.T.',TAMSX3("A1_COD")[1],.T.})
	aAdd( aPergs ,{1,"Loja: ",_loja,"@!",'.T.',,'.T.',TAMSX3("A1_LOJA")[1],.T.})
	aAdd( aPergs ,{1,"Contrato: ",_filial,"@!",'.T.',"ZZ0",'.T.',6,.F.})
	aAdd( aPergs ,{1,"Data Base: ",dDatabase ,PesqPict("SD1", "D1_DTDIGIT"),'.T.',"",'.T.', TamSX3("D1_DTDIGIT")[1], .T.})


	If ParamBox(aPergs ,"Parametros ",aRet)
	
	
		DEFINE MSDIALOG oDlg TITLE "Importa��o Tabelas" FROM 000, 000  TO 400, 800 COLORS 0, 16777215 PIXEL
	
		@ 15, 05 GROUP oGrpCon 	TO 182, 400 LABEL "Escolha o relat�rio que deseja imprimir" OF oDlg PIXEL DESIGN
	
		@ 039, 130 SAY oSay1 PROMPT "Este relat�rio � para impress�o de contratos ou para diverg�ncias de contrato." SIZE 150, 029 OF oDlg COLORS 0, 16777215 PIXEL
   // @ 070, 095 RADIO oRadMenu1 VAR nRadMenu1 ITEMS "Folha         Gerencial","F�rias         Gerencial","13�Sal�rio  Gerencial" SIZE 170, 10 OF oDlg COLOR 0, 16777215 PIXEL
    
		@ 070,160 Radio oRadMenu1 Var nRadMenu1 Items	"Relat�rio de contratos",;
			"Relat�rio ajustes de contratos";
			3D Size 170,10 Of oDlg PIXEL DESIGN //ON CHANGE ImpChgRadio(nRadioArq,@cNmAlias)
    
    
	
		@ 185, 205 BUTTON Avancar PROMPT "Imprimir..." SIZE 042, 012 OF oDlg PIXEL ACTION (nOpca := 0,oDlg:End())
		@ 185, 250 BUTTON Avancar PROMPT "Cancelar" SIZE 037, 012 OF oDlg PIXEL ACTION (nOpca := 1,oDlg:End())
	
		ACTIVATE MSDIALOG oDlg CENTERED
	 
		IF nOpca == 0
			
			IIF(nRadMenu1 == 1,	RelContratos(),RelAjustes() )
		ELSEIF nOpca == 1
	
			return
		ENDIF
	
	endif
return
	
	
Static Function RelContratos()
		
	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local cLocal          := "c:\"
	//Local xArea := GetArea()
	Local Vez := 0

	local _ano := space(4)
	local _filial := space(6)
	local _client := space(TAMSX3("A1_COD")[1])
	local _loja := space(TAMSX3("A1_LOJA")[1])
	local _data := space(TamSX3("D1_DTDIGIT")[1])
	Local aPergs := {}
	Local aRet := {}
	Local lRet
	Local cQuery
	
	Local oRadMenu1
	Local nRadMenu1 := 1
	Local aButtons := {}
	Local oTButton1 := Nil
	
	cRazao := POSICIONE("SA1",1,XFILIAL("SA1")+MV_PAR03+MV_PAR04,"A1_NOME")

	oPrinter := FWMSPrinter():New(cFilePrint, IMP_SPOOL, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetPortrait()

	cQuery := "SELECT CASE ZZ0_STATUS WHEN 'I' THEN 'INCLUIDO' WHEN 'L' THEN 'LIBERADO' WHEN 'A' THEN 'ENCERRADO' END AS 'STATUS',*  "
	cQuery += "FROM "+RETSQLNAME("ZZ0")+" ZZ0 "
	cQuery += " WHERE ZZ0_FILIAL = '"+mv_par02+"' "
	IF alltrim(MV_PAR01) <> ''
		cQuery += " AND ZZ0_ANOCOM = '"+mv_par01+"' "
	ENDIF
	IF alltrim(MV_PAR05) <> ''
		cQuery += " AND ZZ0_NUMERO = '"+mv_par05+"' "
	ENDIF
	cQuery += " AND  D_E_L_E_T_ = ' ' "
	cQuery += " AND ZZ0_CLIENT = '"+mv_par03+"' AND ZZ0_LOJA = '"+mv_par04+"' AND ZZ0_EMISSA <= '"+DTOS(mv_par06)+"' "

	if Select("T01") > 0
		dbSelectArea("T01")
		dbCloseArea()
	endif

	TcQuery cQuery New Alias T01

	While !(T01->(Eof()))

		cContra := T01->ZZ0_NUMERO
		cSitua := T01->STATUS
			
		funCbc() //Chama o cabe�alho
			
		nTEnviados := 0
		nTDevolvidos := 0
		nTAditivos := 0
		nTFrete := 0
		nTJuros := 0
		nTDesconto := 0
		nAdiant := 0
		nTBaixaNF := 0
		nTSaldo := 0
			
		DadosDoContrato()//Chama os dados do contrato
			
		PosicaoFiscal()
		
		//Aditivos()//Chama os dados dos aditivos
			
		MateriaisEnviados()//Chama os dados dos materiais enviados
			
		Devolvidos()
			
		PosicaoFinanceira()
			
			
		Totalizador()
		T01->(dbSkip())

		oPrinter:EndPage()

	ENDDO

	T01->(DbCloseArea())

	oPrinter:Preview()

	

Return



Static Function funCbc

	Local cLogo := "\system\OS01.bmp"
	nPag	:= nPag + 1
	nLin   := 80
	
	oPrinter:StartPage()
	
	oPrinter:Box( 65, 40, 130, 570, "-4")

	oPrinter:SayBitmap( nLin-010, 050, cLogo, 100, 058)

	oPrinter:Say ( nLin, 160, "FINANCEIRO POR CONTRATO",  oFont3 )
	oPrinter:Say ( nLin+10, 160, cRazao,  oFont3 )
	oPrinter:Say ( nLin, 500, "Data: "+DTOC(DDATABASE),  oFont1 )
	oPrinter:Say ( nLin+10, 500, "Hora: "+Time(),  oFont1 )
	oPrinter:Say ( nLin+20, 500, "Pag: "+cValtoChar(nPag),  oFont1 )
	oPrinter:Say ( nLin+25, 160, "Competencia: "+MV_PAR01,  oFont1 )
	oPrinter:Say ( nLin+25, 300, "Cod Cliente: "+MV_PAR03+" "+MV_PAR04+" - "+cContra,  oFont1 )
	oPrinter:Say ( nLin+40, 160, "Status Contrato: "+cSitua,  oFont1 )

	nLin := nLin + nTmLin
	

Return

Static Function DadosDoContrato()

	Local nTotal := 0
	Local espaco := space(1)
	
	
	
	oPrinter:Say ( nLin, 40, "DADOS DO CONTRATO",  oFont3 )
	oPrinter:Say ( nLin, 170, " % Frete Principal: "+cvaltochar(T01->ZZ0_FRTCLI),  oFont1 )
	oPrinter:Say ( nLin, 320, " % Frete Complementar: "+cvaltochar(T01->ZZ0_FRTEMP),  oFont1 )
			
	nLin := nLin + 10
			
	oBrush1 := TBrush():New( , CLR_HGRAY)
	oPrinter:Fillrect( {155, 40, 143, 570 }, oBrush1, "-4")
	oPrinter:Say ( nLin, 50, "Codigo",  oFont1 )
			//oPrinter:Line( 10, 10, 10, 50, 0,"-4")
	oPrinter:Say ( nLin, 140, "Descricao",  oFont1 )
	oPrinter:Say ( nLin, 360, "Quantidade",  oFont1 )
	oPrinter:Say ( nLin, 450, "Valor",  oFont1 )
	oPrinter:Say ( nLin, 500, "Total",  oFont1 )
	
	nLin := nLin + 15
	
	cQuery := "SELECT *  "
	cQuery += "FROM "+RETSQLNAME("ZZ1")+" ZZ1 "
	cQuery += " INNER JOIN ZZ0010 Z0 ON ZZ0_FILIAL = ZZ1_FILIAL AND ZZ0_NUMERO = ZZ1_NUMERO AND ZZ0_CLIENT = ZZ1_CLIENT AND ZZ0_LOJA = ZZ1_LOJA "
	cQuery += " INNER JOIN SF4010 SF4 ON SF4.F4_CODIGO = ZZ1.ZZ1_TES AND SF4.F4_FILIAL = SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) AND SF4.D_E_L_E_T_='' "
	cQuery += " WHERE ZZ1_FILIAL = '"+mv_par02+"' AND ZZ1_NUMERO = '"+T01->ZZ0_NUMERO+"' AND  ZZ1.D_E_L_E_T_ = ' ' "
	cQuery += " AND ZZ1_CLIENT = '"+mv_par03+"' AND ZZ1_LOJA = '"+mv_par04+"' AND SF4.D_E_L_E_T_ = '' AND Z0.D_E_L_E_T_ = '' "
	cQuery += " AND SF4.F4_DUPLIC='S' "
	

	if Select("T02") > 0
		dbSelectArea("T02")
		dbCloseArea()
	endif

	TcQuery cQuery New Alias T02

	While !(T02->(Eof()))
		
		IF nLin >= 750
			oPrinter:EndPage()
			funCbc()
		ENDIF
		
		oPrinter:Say ( nLin, 040, T02->ZZ1_PRODUT ,  oFont1 )
		oPrinter:Say ( nLin, 140, T02->ZZ1_DESC ,  oFont1 )
		oPrinter:Say ( nLin, 360, PadL(cValtochar(T02->ZZ1_QUANT),12) ,  oFont1 )
		oPrinter:Say ( nLin, 450, PadL(alltrim(TRANSFORM(T02->ZZ1_VLRUN - T02->ZZ1_VLRDSC,"@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 500, PadL(alltrim(TRANSFORM(T02->ZZ1_VLRLIQ, "@E 999,999,999.99")),12,) ,  oFont1 )
		
		nTContrato += T02->ZZ1_VLRTOT
		
				
				
		T02->(dbSkip())
		nLin := nLin + 10
	ENDDO
	nLin := nLin + 30
		
		
	oPrinter:Line( nLin, 040, nLin, 570)
	oPrinter:Say ( nLin+15, 360, "TOTAL DO CONTRATO:",  oFont3 )
	oPrinter:Say ( nLin+15, 500, PadL(alltrim(TRANSFORM(nTContrato, "@E 999,999,999.99")),12,) ,  oFont3 )
	
		
	nLin := nLin + 50
	nTotal := 0
	T02->(DbCloseArea())
return


Static Function MateriaisEnviados()

	Local nTotal := 0
	Local aErros := {}
	
	IF nLin >= 750
		oPrinter:EndPage()
		funCbc()
	ENDIF
	
	
	//nLin := nLin+50
	oPrinter:Say ( nLin, 40, "MATERIAIS ENVIADOS",  oFont3 )
	//oPrinter:Say ( nLin, 170, "Frete Principal: "+cvaltochar(T01->ZZ0_FRTCLI),  oFont1 )
	//oPrinter:Say ( nLin, 320, "Frete Complementar: "+cvaltochar(T01->ZZ0_FRTEMP),  oFont1 )
			
	nLin := nLin + 10
			
	oBrush1 := TBrush():New( , CLR_HGRAY)
	oPrinter:Fillrect( {nLin+5, 40, nLin-7, 570 }, oBrush1, "-4")
	oPrinter:Say ( nLin, 50, "Codigo",  oFont1 )
	oPrinter:Say ( nLin, 110, "Data",  oFont1 )
	oPrinter:Say ( nLin, 180, "Descricao",  oFont1 )
	oPrinter:Say ( nLin, 340, "Quantidade",  oFont1 )
	oPrinter:Say ( nLin, 390, "Valor",  oFont1 )
	oPrinter:Say ( nLin, 440, "Suplemento",  oFont1 )
	oPrinter:Say ( nLin, 500, "Total",  oFont1 )
	
	nLin := nLin + 15
	
	cQuery := " SELECT ZZ3_FILIAL, F2_DOC, F2_SERIE, F2_EMISSAO, ZZ3_NUMERO,ZZ3_MEDICA, ZZ3_CLIENT, ZZ3_LOJA, SUM(ZZ1_VLRDSC) ZZ1_VLRDSC, SUM(ZZ3_QUANT) ZZ3_QUANT, SUM(ZZ3_VLUNIT) ZZ3_VLUNIT, SUM(ZZ3_VTOTAL) ZZ3_VTOTAL FROM "+RetSqlName("ZZ3")+" ZZ3   "
	cQuery += " INNER JOIN "+RetSqlName("ZZ2")+" ZZ2 "
	cQuery += " ON ZZ3_FILIAL = ZZ2_FILIAL "
	cQuery += " AND ZZ3_NUMERO = ZZ2_NUMERO "
	cQuery += " AND ZZ3_CLIENT = ZZ2_CLIENT "
	cQuery += " AND ZZ3_LOJA = ZZ2_LOJA "
	cQuery += " AND ZZ3_MEDICA = ZZ2_MEDICA "
	cQuery += " LEFT JOIN "+RetSqlName("SF2")+" F2 "
	cQuery += " ON ZZ3_FILIAL = F2_YFILIAL "
	cQuery += " AND ZZ3_NUMERO = F2_YCONTRA "
	cQuery += " AND ZZ3_MEDICA = F2_YMEDICA "
	cQuery += " AND ZZ3_CLIENT = F2_CLIENT "
	cQuery += " AND ZZ3_LOJA = F2_LOJA "
	cQuery += " INNER JOIN ZZ1010 ZZ1 ON ZZ3_FILIAL = ZZ1_FILIAL AND ZZ3_NUMERO = ZZ1_NUMERO AND ZZ3_PRODUT = ZZ1_PRODUT AND ZZ3_CLIENT = ZZ1_CLIENT AND ZZ3_LOJA = ZZ1_LOJA AND ZZ3_ITEM = ZZ1_ITEM "
	cQuery += " INNER JOIN SF4010 SF4 ON SF4.F4_CODIGO = ZZ3.ZZ3_TES AND SF4.F4_FILIAL = SUBSTRING(ZZ3.ZZ3_FILIAL,1,2) AND SF4.D_E_L_E_T_='' "
	//cQuery += " INNER JOIN ZZ7010 ZZ7 ON ZZ3_PRODUT = ZZ7_CODIGO AND ZZ7_SUPLEM <> 'S' "
	cQuery += " WHERE ZZ3_FILIAL = '"+mv_par02+"' AND ZZ3_NUMERO = '"+T01->ZZ0_NUMERO+"' AND  ZZ3.D_E_L_E_T_ = ' ' "
	cQuery += " AND ZZ2.D_E_L_E_T_ = '' "
	cQuery += " AND SF4.F4_DUPLIC='S' "
	cQuery += " AND F2.D_E_L_E_T_ = '' "
	cQuery += " AND ZZ1.D_E_L_E_T_ = '' "
	IF alltrim(MV_PAR01) <> ''
		cQuery += " AND ZZ2_DTGER LIKE '"+mv_par01+"%' "
	ENDIF
	cQuery += " GROUP BY ZZ3_FILIAL, F2_DOC, F2_SERIE, F2_EMISSAO, ZZ3_NUMERO, ZZ3_CLIENT, ZZ3_LOJA, ZZ3_MEDICA "
	
	
	
	if Select("T02") > 0
		dbSelectArea("T02")
		dbCloseArea()
	endif

	TcQuery cQuery New Alias T02
	cAux := T02->F2_DOC
 
	
	While !(T02->(Eof()))
	
		cQuery2 := " SELECT F2_VALBRUT, F2_DESCONT,((F2_VALBRUT + F2_DESCONT) - F2_FRETE) VALORMEDICAO  FROM SF2010 F2 "
		cQuery2 += " WHERE F2.D_E_L_E_T_ = '' "
		cQuery2 += " AND F2_DOC = '"+T02->F2_DOC+"' "
		cQuery2 += " AND F2_SERIE = '"+T02->F2_SERIE+"' "
		cQuery2 += " AND F2_CLIENTE = '"+T02->ZZ3_CLIENT+"' "
		cQuery2 += " AND F2_LOJA = '"+T02->ZZ3_LOJA+"' "
		TcQuery cQuery2 new Alias T03
	
		While !T03->(EOF())
	
			IF ABS(T03->VALORMEDICAO - T02->ZZ3_VTOTAL) > 5
				AADD(aErros,{T02->ZZ3_FILIAL,T02->F2_DOC, T02->F2_SERIE,T02->ZZ3_CLIENT, T02->ZZ3_LOJA,T02->ZZ3_NUMERO,T02->ZZ3_VTOTAL,T03->F2_VALBRUT + T03->F2_DESCONT})
			ENDIF
			T03->(DbSkip())
		Enddo
		DbCloseArea("T03")
	
		T02->(DbSkip())
	
	endDo
		
		
	
	
	
	cQuery := " SELECT ZZ3_FILIAL,F2_DOC, F2_DESCONT, ZZ1_VLRDSC,F2_EMISSAO, ZZ3_NUMERO, ZZ3_CLIENT, ZZ3_LOJA, ZZ3_ITEM, ZZ3_MEDICA,ZZ3_PRODUT,ZZ3_PRODUT, ZZ3_DESCRI,ZZ3_QUANT,ZZ3_VLUNIT,ZZ3_VTOTAL,ZZ2_DTENTR FROM "+RetSqlName("ZZ3")+" ZZ3   "
	cQuery += " INNER JOIN "+RetSqlName("ZZ2")+" ZZ2 "
	cQuery += " ON ZZ3_FILIAL = ZZ2_FILIAL "
	cQuery += " AND ZZ3_NUMERO = ZZ2_NUMERO "
	cQuery += " AND ZZ3_CLIENT = ZZ2_CLIENT "
	cQuery += " AND ZZ3_LOJA = ZZ2_LOJA "
	cQuery += " AND ZZ3_MEDICA = ZZ2_MEDICA "
	cQuery += " LEFT JOIN "+RetSqlName("SF2")+" F2 "
	cQuery += " ON ZZ3_FILIAL = F2_FILIAL "
	cQuery += " AND ZZ3_NUMERO = F2_YCONTRA "
	cQuery += " AND ZZ3_MEDICA = F2_YMEDICA "
	cQuery += " AND ZZ3_CLIENT = F2_CLIENT "
	cQuery += " AND ZZ3_LOJA = F2_LOJA "
	cQuery += " INNER JOIN ZZ1010 ZZ1 ON ZZ3_FILIAL = ZZ1_FILIAL AND ZZ3_NUMERO = ZZ1_NUMERO AND ZZ3_PRODUT = ZZ1_PRODUT AND ZZ3_CLIENT = ZZ1_CLIENT AND ZZ3_LOJA = ZZ1_LOJA AND ZZ3_ITEM = ZZ1_ITEM "
	cQuery += " INNER JOIN SF4010 SF4 ON SF4.F4_CODIGO = ZZ3.ZZ3_TES AND SF4.F4_FILIAL = SUBSTRING(ZZ3.ZZ3_FILIAL,1,2) AND SF4.D_E_L_E_T_='' "
	//cQuery += " INNER JOIN ZZ7010 ZZ7 ON ZZ3_PRODUT = ZZ7_CODIGO AND ZZ7_SUPLEM <> 'S' "
	cQuery += " WHERE ZZ3_FILIAL = '"+mv_par02+"' AND ZZ3_NUMERO = '"+T01->ZZ0_NUMERO+"' AND  ZZ3.D_E_L_E_T_ = ' ' "
	cQuery += " AND ZZ2.D_E_L_E_T_ = '' "
	cQuery += " AND SF4.F4_DUPLIC='S' "
	cQuery += " AND F2.D_E_L_E_T_ = '' "
	cQuery += " AND ZZ1.D_E_L_E_T_ = '' "
	IF alltrim(MV_PAR01) <> ''
		cQuery += " AND ZZ2_DTGER LIKE '"+mv_par01+"%' "
	ENDIF
	cQuery += " GROUP BY ZZ3_FILIAL,F2_DOC, F2_DESCONT, ZZ1_VLRDSC,F2_EMISSAO, ZZ3_NUMERO, ZZ3_CLIENT, ZZ3_LOJA, ZZ3_ITEM, ZZ3_MEDICA,ZZ3_PRODUT,ZZ3_PRODUT, ZZ3_DESCRI,ZZ3_QUANT,ZZ3_VLUNIT,ZZ3_VTOTAL,ZZ2_DTENTR "
		
	if Select("T02") > 0
		dbSelectArea("T02")
		dbCloseArea()
	endif

	TcQuery cQuery New Alias T02

	While !(T02->(Eof()))
				
		IF nLin >= 750
			oPrinter:EndPage()
			funCbc()
		ENDIF
		
		IF ascan(aErros,{|x| x[2] == T02->F2_DOC}) <> 0
			oPrinter:Say ( nLin, 040, T02->F2_DOC + "***" ,  oFont1 )
		ELSE
			oPrinter:Say ( nLin, 040, T02->F2_DOC ,  oFont1 )
		ENDIF
		
		oPrinter:Say ( nLin, 110, DTOC(STOD(T02->ZZ2_DTENTR)) ,  oFont1 )
		oPrinter:Say ( nLin, 180, SUBSTR(T02->ZZ3_DESCRI,1,32) ,  oFont1 )
		oPrinter:Say ( nLin, 340, cValtochar(T02->ZZ3_QUANT) ,  oFont1 )
		oPrinter:Say ( nLin, 390, TRANSFORM(T02->ZZ3_VLUNIT, "@E 999,999,999.99") ,  oFont1 )
		oPrinter:Say ( nLin, 440, TRANSFORM((T02->ZZ1_VLRDSC * T02->ZZ3_QUANT), "@E 999,999,999.99") ,  oFont1 )
		oPrinter:Say ( nLin, 500, TRANSFORM(T02->ZZ3_VTOTAL - (T02->ZZ1_VLRDSC * T02->ZZ3_QUANT), "@E 999,999,999.99") ,  oFont1 )
		
		nTEnviados += (T02->ZZ3_VTOTAL - (T02->ZZ1_VLRDSC * T02->ZZ3_QUANT))
		/*
					oPrinter:Say ( nLin, 180, "Descricao",  oFont1 )
	oPrinter:Say ( nLin, 340, "Quantidade",  oFont1 )
	oPrinter:Say ( nLin, 390, "Valor",  oFont1 )
	oPrinter:Say ( nLin, 440, "Suplemento",  oFont1 )
	oPrinter:Say ( nLin, 500, "Total",  oFont1 )
		*/
				
		T02->(dbSkip())
		nLin := nLin + 10
	ENDDO
	nLin := nLin + 30
	
	
		
	oPrinter:Line( nLin, 040, nLin, 570)
	
	oPrinter:Say ( nLin+15, 360, "TOTAL DO ENVIADOS:",  oFont3 )
	oPrinter:Say ( nLin+15, 500, PadL(alltrim(TRANSFORM(nTEnviados, "@E 999,999,999.99")),12,) ,  oFont3 )
	
		
	nLin := nLin + 50
	nTotal := 0
	
	if T02->(EOF()) .AND. nLin >= 750
		oPrinter:EndPage()
		funCbc()
	endif
	T02->(DbCloseArea())
return


Static Function Aditivos()

	
	Local nTotal := 0
	
	IF nLin >= 750
		oPrinter:EndPage()
		funCbc()
	ENDIF
	oPrinter:Say ( nLin, 40, "ADITIVOS",  oFont3 )
	//oPrinter:Say ( nLin, 170, "Frete Principal: "+cvaltochar(T01->ZZ0_FRTCLI),  oFont1 )
	//oPrinter:Say ( nLin, 320, "Frete Complementar: "+cvaltochar(T01->ZZ0_FRTEMP),  oFont1 )
			
	nLin := nLin + 10
			
	oBrush1 := TBrush():New( , CLR_HGRAY)
	oPrinter:Fillrect( {nLin+5, 40, nLin-7, 570 }, oBrush1, "-4")
	oPrinter:Say ( nLin, 50, "Codigo",  oFont1 )
	oPrinter:Say ( nLin, 110, "Data",  oFont1 )
	oPrinter:Say ( nLin, 180, "Descricao",  oFont1 )
	oPrinter:Say ( nLin, 380, "Quantidade",  oFont1 )
	oPrinter:Say ( nLin, 450, "Valor",  oFont1 )
	oPrinter:Say ( nLin, 500, "Total",  oFont1 )

	nLin := nLin + 15
	
	cQuery := "SELECT *  "
	cQuery += "FROM "+RETSQLNAME("ZZ6")+" ZZ6 "
	cQuery += "INNER JOIN "+RetSqlName("ZZ7")+" ZZ7 "
	cQuery += "ON ZZ6_PRODUT = ZZ7_PRODUT "
	cQuery += "AND ZZ7_TIPO <> 'S' "
	cQuery += " WHERE ZZ6_FILIAL = '"+mv_par02+"' AND ZZ6_NUMERO = '"+T01->ZZ0_NUMERO+"' AND  ZZ7.D_E_L_E_T_ = ' ' AND  ZZ6.D_E_L_E_T_ = ' '"
	cQuery += " AND ZZ6_CLIENT = '"+mv_par03+"' AND ZZ6_LOJA = '"+mv_par04+"' "

	if Select("T02") > 0
		dbSelectArea("T02")
		dbCloseArea()
	endif

	TcQuery cQuery New Alias T02

	While !(T02->(Eof()))
				
		IF nLin >= 750
			oPrinter:EndPage()
			funCbc()
		ENDIF
		
		oPrinter:Say ( nLin, 040, T02->ZZ6_PRODUT ,  oFont1 )
		oPrinter:Say ( nLin, 140, T02->ZZ6_DESCRI ,  oFont1 )
		oPrinter:Say ( nLin, 380, cValtochar(T02->ZZ6_QUANT) ,  oFont1 )
		oPrinter:Say ( nLin, 450, PadL(alltrim(TRANSFORM(T02->ZZ6_VLRUN, "@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 500, PadL(alltrim(TRANSFORM(T02->ZZ6_VLRTOT, "@E 999,999,999.99")),12,) ,  oFont1 )
		
		nTAditivos += T02->ZZ6_VLRTOT
		
		
				
				
		T02->(dbSkip())
		nLin := nLin + 10
	ENDDO
	nLin := nLin + 30
		
		
	oPrinter:Line( nLin, 040, nLin, 570)
	
	oPrinter:Say ( nLin+15, 360, "TOTAL ADITIVOS:",  oFont3 )
	oPrinter:Say ( nLin+15, 500, PadL(alltrim(TRANSFORM(nTAditivos, "@E 999,999,999.99")),12,) ,  oFont3 )
		
	nLin := nLin + 50
	nTotal := 0
	
	
	if T02->(EOF()) .AND. nLin >= 750
		oPrinter:EndPage()
		funCbc()
	endif
	
	T02->(DbCloseArea())
	
	
return

Static Function Devolvidos()

	Local nTotal := 0
	
	IF nLin >= 750
		oPrinter:EndPage()
		funCbc()
	ENDIF
	//nLin := nLin+50
	oPrinter:Say ( nLin, 40, "MATERIAIS DEVOLVIDOS",  oFont3 )
	//oPrinter:Say ( nLin, 170, "Frete Principal: "+cvaltochar(T01->ZZ0_FRTCLI),  oFont1 )
	//oPrinter:Say ( nLin, 320, "Frete Complementar: "+cvaltochar(T01->ZZ0_FRTEMP),  oFont1 )
			
	nLin := nLin + 10
			
	oBrush1 := TBrush():New( , CLR_HGRAY)
	oPrinter:Fillrect( {nLin+5, 40, nLin-7, 570 }, oBrush1, "-4")
	oPrinter:Say ( nLin, 50, "Codigo",  oFont1 )
	oPrinter:Say ( nLin, 110, "Data",  oFont1 )
	oPrinter:Say ( nLin, 180, "Descricao",  oFont1 )
	oPrinter:Say ( nLin, 380, "Quantidade",  oFont1 )
	oPrinter:Say ( nLin, 450, "Valor",  oFont1 )
	oPrinter:Say ( nLin, 500, "Total",  oFont1 )
	
	nLin := nLin + 15
	
	cQuery := " SELECT F1_DOC, "
	cQuery += "    D1_COD, "
	cQuery += "    D1_ITEM, "
	cQuery += "    F1_EMISSAO, "
	cQuery += "    B1_DESC, "
	cQuery += "    D1_QUANT, "
	cQuery += "    D1_VUNIT "
	cQuery += " FROM ZZH010 ZZH "
	cQuery += " INNER JOIN SF1010 F1 ON ZZH_PC01FL = F1_FILIAL "
	cQuery += " AND ZZH_PC01NF = F1_DOC "
	cQuery += " AND ZZH_FONF01 = F1_FORNECE "
	cQuery += " AND ZZH_LOJA = F1_LOJA "
	cQuery += " INNER JOIN SD1010 D1 ON D1_FILIAL = F1_FILIAL "
	cQuery += " AND D1_DOC = F1_DOC "
	cQuery += " AND D1_FORNECE = F1_FORNECE "
	cQuery += " AND D1_LOJA = F1_LOJA "
	cQuery += " AND F1_SERIE = D1_SERIE "
	cQuery += " INNER JOIN SB1010 B1 ON D1_COD = B1_COD "
	cQuery += " INNER JOIN ZZV010 ZZV ON ZZV_FILIAL = ZZH_FILIAL "
	cQuery += " AND ZZV_NUMERO = ZZH_NUMERO "
	cQuery += " AND ZZV_MEDICA = ZZH_MEDICA "
	cQuery += " AND ZZV_CLIENT = ZZH_CLIENT "
	cQuery += " AND ZZV_LOJA = ZZH_LOJA "
	cQuery += " INNER JOIN ZZ7010 ZZ7 ON ZZV_PRODUT = ZZ7_CODIGO "
	cQuery += " INNER JOIN ZZ7010 Z7 ON Z7.ZZ7_PRODUT = D1.D1_COD "
	cQuery += " WHERE ZZH.D_E_L_E_T_ = '' "
	cQuery += "   AND F1.D_E_L_E_T_ = '' "
	cQuery += "   AND D1.D_E_L_E_T_ = '' "
	cQuery += " AND ZZH_FILIAL = '"+mv_par02+"' "
	cQuery += " AND ZZH_CLIENT = '"+mv_par03+"' "
	cQuery += " AND ZZH_LOJA = '"+mv_par04+"' "
	cQuery += " 	AND ZZ7.ZZ7_CODIGO = Z7.ZZ7_CODPAI "
	cQuery += "   AND Z7.ZZ7_SUPLEM <> 'S' "
	cQuery += "   AND ZZ7.ZZ7_SUPLEM <> 'S' "
	cQuery += "   AND ZZ7.D_E_L_E_T_ = '' "
	cQuery += "   AND ZZV.D_E_L_E_T_ = '' "
	cQuery += "   AND ZZH_NUMERO = '"+T01->ZZ0_NUMERO+"' "
	cQuery += "   GROUP BY "
	cQuery += " 		F1_DOC, "
	cQuery += "        D1_COD, "
	cQuery += "        D1_ITEM, "
	cQuery += "        F1_EMISSAO, "
	cQuery += "        B1_DESC, "
	cQuery += "        D1_QUANT, "
	cQuery += "        D1_VUNIT "
	cQuery += " ORDER BY 1, "
	cQuery += "          4 "
	
	
	if Select("T02") > 0
		dbSelectArea("T02")
		dbCloseArea()
	endif

	TcQuery cQuery New Alias T02

	While !(T02->(Eof()))
				
		IF nLin >= 750
			oPrinter:EndPage()
			funCbc()
		ENDIF
		
		oPrinter:Say ( nLin, 040, T02->F1_DOC ,  oFont1 )
		oPrinter:Say ( nLin, 110, DTOC(STOD(T02->F1_EMISSAO)) ,  oFont1 )
		oPrinter:Say ( nLin, 180, T02->B1_DESC ,  oFont1 )
		oPrinter:Say ( nLin, 380, cValtochar(T02->D1_QUANT) ,  oFont1 )
		oPrinter:Say ( nLin, 450, PadL(alltrim(TRANSFORM(T02->D1_VUNIT, "@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 500, PadL(alltrim(TRANSFORM(T02->D1_QUANT * T02->D1_VUNIT, "@E 999,999,999.99")),12,) ,  oFont1 )
		
		nTDevolvidos += T02->D1_QUANT * T02->D1_VUNIT
		
					
				
		T02->(dbSkip())
		nLin := nLin + 10
	ENDDO
	nLin := nLin + 30
	
	
		
	oPrinter:Line( nLin, 040, nLin, 570)
	
	oPrinter:Say ( nLin+15, 360, "TOTAL DO DEVOLVIDOS:",  oFont3 )
	oPrinter:Say ( nLin+15, 500, PadL(alltrim(TRANSFORM(nTDevolvidos, "@E 999,999,999.99")),12,) ,  oFont3 )
	
		
	nLin := nLin + 50
	nTotal := 0
	
	if T02->(EOF()) .AND. nLin >= 750
		oPrinter:EndPage()
		funCbc()
	endif
	T02->(DbCloseArea())
return


Static Function PosicaoFinanceira()


	
	Local nTotal := 0
	
	IF nLin >= 750
		oPrinter:EndPage()
		funCbc()
	ENDIF
	oPrinter:Say ( nLin, 40, "POSI��O FINANCEIRA (ADIANTAMENTOS)",  oFont3 )
	//oPrinter:Say ( nLin, 170, "Frete Principal: "+cvaltochar(T01->ZZ0_FRTCLI),  oFont1 )
	//oPrinter:Say ( nLin, 320, "Frete Complementar: "+cvaltochar(T01->ZZ0_FRTEMP),  oFont1 )
			
	nLin := nLin + 10
			
	oBrush1 := TBrush():New( , CLR_HGRAY)
	oPrinter:Fillrect( {nLin+5, 40, nLin-7, 570 }, oBrush1, "-4")
	oPrinter:Say ( nLin, 50, "Vencimento",  oFont1 )
	oPrinter:Say ( nLin, 110, "Baixa",  oFont1 )
	oPrinter:Say ( nLin, 160, "Hist�rico",  oFont1 )
	oPrinter:Say ( nLin, 310, "Valor Original",  oFont1 )
	oPrinter:Say ( nLin, 370, "Juros",  oFont1 )
	oPrinter:Say ( nLin, 430, "Desconto",  oFont1 )
	oPrinter:Say ( nLin, 480, "Valor Baixado",  oFont1 )
	
	nLin := nLin + 15
	
	cQuery := " SELECT E1_VENCREA, "
	cQuery += " E5_SEQ, "
	cQuery += " E5_DATA, "
	cQuery += " E5_TIPODOC, "
	cQuery += " E1_BAIXA, "
	cQuery += " E1_YCONTRA, "
	cQuery += " SUBSTRING(E1_HIST,13,30) E1_HIST, "
	cQuery += " E1_TIPO, "
	cQuery += " E1_PREFIXO, "
	cQuery += " E1_NUM, "
	cQuery += " E1_PARCELA, "
	cQuery += " E1_VALOR, "
	cQuery += " E5_VALOR AS 'BAIXA', "
	cQuery += " E1_JUROS, "
	cQuery += " E1_MULTA, "
	cQuery += " E5_VLDESCO, "
	cQuery += " E1_SALDO "
	cQuery += " FROM "+RetSqLName("SE1")+" E1 "
	cQuery += " INNER JOIN "+RetSqlName("ZZ3")+" ZZ3 ON ZZ3_FILIAL = E1_YFILIAL "
	cQuery += " AND ZZ3_NUMERO = E1_YCONTRA "
	cQuery += " INNER JOIN "+RetSqlName("SE5")+" E5 ON E1_FILIAL = E5_FILIAL "
	cQuery += " AND E1_NUM = E5_NUMERO "
	cQuery += " AND E1_CLIENTE = E5_CLIENTE "
	cQuery += " AND E1_LOJA = E5_LOJA "
	cQuery += " AND E1_PARCELA = E5_PARCELA "
	cQuery += " AND E1_PREFIXO = E5_PREFIXO "
	cQuery += " AND E1_TIPO = E5_TIPO "
	cQuery += " AND E5.E5_MOTBX <> 'ACD' "
	cQuery += " AND E5.E5_TIPODOC IN ('VL','ES') "
	cQuery += " INNER JOIN "+RetSqlName("SF4")+" SF4 ON SF4.F4_CODIGO = ZZ3.ZZ3_TES "
	cQuery += " AND SF4.F4_FILIAL = SUBSTRING(ZZ3.ZZ3_FILIAL,1,2) "
	cQuery += " AND SF4.D_E_L_E_T_='' "
	cQuery += " WHERE E1.D_E_L_E_T_ = '' "
	cQuery += " AND ZZ3.D_E_L_E_T_ = '' "
	cQuery += " AND E5.D_E_L_E_T_ = '' "
	cQuery += " AND E1_YCONTRA = '"+T01->ZZ0_NUMERO+"' "
	cQuery += " AND E1_YFILIAL = '"+mv_par02+"' "
	cQuery += " AND E1_CLIENTE = '"+mv_par03+"' "
	cQuery += " AND E1_LOJA = '"+mv_par04+"' "
	cQuery += " AND E1_BAIXA <> '' "
	cQuery += " AND SF4.F4_DUPLIC='S' "
	IF alltrim(MV_PAR01) <> ''
		cQuery += " AND E1_EMISSAO LIKE '"+mv_par01+"%' "
	ENDIF
	IF alltrim(MV_PAR05) <> ''
		cQuery += " AND E1_YCONTRA = '"+mv_par05+"' "
	ENDIF
	cQuery += " GROUP BY E1_VENCREA, "
	cQuery += " E5_SEQ, "
	cQuery += " E5_DATA, "
	cQuery += " E5_TIPODOC, "
	cQuery += " E1_BAIXA, "
	cQuery += " E1_YCONTRA, "
	cQuery += " E1_HIST, "
	cQuery += " E1_TIPO, "
	cQuery += " E1_PREFIXO, "
	cQuery += " E1_NUM, "
	cQuery += " E1_PARCELA, "
	cQuery += " E1_VALOR, "
	cQuery += " E5_VALOR, "
	cQuery += " E1_JUROS, "
	cQuery += " E1_MULTA, "
	cQuery += " E5_VLDESCO, "
	cQuery += " E1_SALDO "
	cQuery += " UNION ALL "
	cQuery += " SELECT E1_VENCREA, "
	cQuery += " '01' E5_SEQ, " 	
	cQuery += " E1_VENCREA E5_DATA, "
	cQuery += " 'VL' E5_TIPODOC, "  
	cQuery += " E1_BAIXA, "
	cQuery += " E1_YCONTRA, "
	cQuery += " SUBSTRING(E1_HIST,13,30) E1_HIST, "
	cQuery += " E1_TIPO, "
	cQuery += " E1_PREFIXO, "
	cQuery += " E1_NUM, "
	cQuery += " E1_PARCELA, "
	cQuery += " E1_VALOR, "
	cQuery += " ((E1_JUROS + E1_MULTA) - E1_DESCONT) + E1_VALOR - E1_SALDO AS 'BAIXA', "
	cQuery += " E1_JUROS, "
	cQuery += " E1_MULTA, "
	cQuery += " E1_DESCONT AS E5_VLDESCO, "
	cQuery += " E1_SALDO "
	cQuery += " FROM "+RetSqlName("SE1")+" E1 "
	cQuery += " INNER JOIN "+RetSqlName("ZZ3")+" ZZ3 ON ZZ3_FILIAL = E1_YFILIAL "
	cQuery += " AND ZZ3_NUMERO = E1_YCONTRA "
	cQuery += " INNER JOIN "+RetSqlName("SF4")+" SF4 ON SF4.F4_CODIGO = ZZ3.ZZ3_TES "
	cQuery += " AND SF4.F4_FILIAL = SUBSTRING(ZZ3.ZZ3_FILIAL,1,2) "
	cQuery += " AND SF4.D_E_L_E_T_='' "
	cQuery += " WHERE E1.D_E_L_E_T_ = '' "
	cQuery += " AND ZZ3.D_E_L_E_T_ = '' "
	cQuery += " AND E1.E1_BAIXA = '' "
	cQuery += " AND E1_YCONTRA = '"+T01->ZZ0_NUMERO+"' "
	cQuery += " AND E1_YFILIAL = '"+mv_par02+"' "
	cQuery += " AND E1_CLIENTE = '"+mv_par03+"' "
	cQuery += " AND E1_LOJA = '"+mv_par04+"' "
	cQuery += " AND SF4.F4_DUPLIC='S' "
	IF alltrim(MV_PAR01) <> ''
		cQuery += " AND E1_EMISSAO LIKE '"+mv_par01+"%' "
	ENDIF
	IF alltrim(MV_PAR05) <> ''
		cQuery += " AND E1_YCONTRA = '"+mv_par05+"' "
	ENDIF
	cQuery += " AND E1.E1_TIPO IN ('ADT','ACF','BL','NF') "
	cQuery += " AND E1.E1_PREFIXO NOT IN ('B01','B02') "//RETIRA OS TITULOS DE ADITIVO
	//cQuery += " AND E1.E1_PREFIXO LIKE '%A%' "
	cQuery += " GROUP BY E1_VENCREA, "
	cQuery += " E1_BAIXA, "
	cQuery += " E1_YCONTRA, "
	cQuery += " E1_HIST, "
	cQuery += " E1_TIPO, "
	cQuery += " E1_PREFIXO, "
	cQuery += " E1_NUM, "
	cQuery += " E1_PARCELA, "
	cQuery += " E1_VALOR, "
	cQuery += " E1_JUROS, "
	cQuery += " E1_MULTA, "
	cQuery += " E1_DESCONT, "
	cQuery += " E1_SALDO "

	if Select("T02") > 0
		dbSelectArea("T02")
		dbCloseArea()
	endif

	TcQuery cQuery New Alias T02

	While !(T02->(Eof()))
		
		IF IIF(alltrim(T02->E1_TIPO) == 'NF', IIF(alltrim(T02->E1_PREFIXO) == 'MAN',.T.,.F.),.T.) .AND. at("2015",T02->E1_HIST) == 0//Se o titulo for tipo NF s� aceita os titulos com prefixo MAN		
		
		IF nLin >= 750
			oPrinter:EndPage()
			funCbc()
		ENDIF
		
		oPrinter:Say ( nLin, 040, DTOC(STOD(T02->E1_VENCREA)) ,  oFont1 )
		oPrinter:Say ( nLin, 110, DTOC(STOD(IIF(empty(T02->E1_BAIXA),T02->E1_BAIXA,T02->E5_DATA))) ,  oFont1 )
		oPrinter:Say ( nLin, 160, SUBSTR(T02->E1_HIST,1,32) ,  oFont1 )
		oPrinter:Say ( nLin, 310, PadL(alltrim(TRANSFORM(T02->E1_VALOR, "@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 370, PadL(alltrim(TRANSFORM(T02->E1_JUROS, "@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 430, PadL(alltrim(TRANSFORM(T02->E5_VLDESCO, "@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 480, PadL(alltrim(TRANSFORM(IIF(Alltrim(T02->E5_TIPODOC) == 'ES', T02->BAIXA * -1,T02->BAIXA ), "@E 999,999,999.99")),12,) ,  oFont1 )
		
		
		nTFin += T02->E1_VALOR
		nTFJur += T02->E1_JUROS
		nTFMulta += T02->E1_MULTA
		nTotalDesconto += T02->E5_VLDESCO
		nTFDesc += ((T02->E1_JUROS + T02->E1_MULTA) - (((T02->E1_VALOR - E5_VLDESCO) - T02->E1_SALDO) + T02->E5_VLDESCO))
		nTFBai += IIF(Alltrim(T02->E5_TIPODOC) == 'ES', T02->BAIXA * -1,T02->BAIXA )//((T02->E1_JUROS + T02->E1_MULTA) - T02->E5_VLDESCO) + (T02->E1_VALOR - E5_VLDESCO) - T02->E1_SALDO//T02->E1_VALOR - T02->E1_SALDO
		
		
		T02->(dbSkip())
		nLin := nLin + 10
		ELSE
		T02->(dbSkip())
		ENDIF
	ENDDO
	nLin := nLin + 30
			
		
	oPrinter:Line( nLin, 040, nLin, 570)
	oPrinter:Say ( nLin+15, 260, "TOTAL: " ,  oFont3 )
	oPrinter:Say ( nLin+15, 310, PadL(alltrim(TRANSFORM(nTFin, "@E 999,999,999.99")),12,) ,  oFont3 )
	oPrinter:Say ( nLin+15, 370, PadL(alltrim(TRANSFORM(nTFJur, "@E 999,999,999.99")),12,) ,  oFont3 )
	oPrinter:Say ( nLin+15, 430, PadL(alltrim(TRANSFORM(nTotalDesconto, "@E 999,999,999.99")),12,) ,  oFont3 )
	oPrinter:Say ( nLin+15, 480, PadL(alltrim(TRANSFORM(nTFBai, "@E 999,999,999.99")),12,) ,  oFont3 )
	
		
	nLin := nLin + 50
	
	
	if T02->(EOF()) .AND. nLin >= 750
		oPrinter:EndPage()
		funCbc()
	endif
	
	T02->(DbCloseArea())

return

Static Function PosicaoFiscal()


	
	Local nTotal := 0
	
	
	IF nLin >= 750
		oPrinter:EndPage()
		funCbc()
	ENDIF
	oPrinter:Say ( nLin, 40, "NOTAS FISCAIS",  oFont3 )
	//oPrinter:Say ( nLin, 170, "Frete Principal: "+cvaltochar(T01->ZZ0_FRTCLI),  oFont1 )
	//oPrinter:Say ( nLin, 320, "Frete Complementar: "+cvaltochar(T01->ZZ0_FRTEMP),  oFont1 )
			
	nLin := nLin + 10
			
	oBrush1 := TBrush():New( , CLR_HGRAY)
	oPrinter:Fillrect( {nLin+5, 40, nLin-7, 570 }, oBrush1, "-4")
	oPrinter:Say ( nLin, 050, "Emiss�o NF",  oFont1 )
	oPrinter:Say ( nLin, 110, "N�Nfe",  oFont1 )
	oPrinter:Say ( nLin, 170, "Valor Mercadoria",  oFont1 )
	oPrinter:Say ( nLin, 280, "Desconto",  oFont1 )
	oPrinter:Say ( nLin, 360, "Frete",  oFont1 )
	oPrinter:Say ( nLin, 420, "Total NF",  oFont1 )
	oPrinter:Say ( nLin, 480, "Vlr.Baixa",  oFont1 )
	
	nLin := nLin + 15

	
	cQuery := " SELECT F2_EMISSAO,((F2_VALBRUT + F2_DESCONT) - F2_FRETE) VALORMEDICAO,  SUM(E1_VALOR) - SUM(E1_SALDO) VALOR,F2_VALBRUT,F2_FRETE,F2_DESCONT, F2_DOC FROM "+RetSqlName("SF2")+" F2 "
	cQuery += " INNER JOIN "+RetSqlName("SE1")+" E1 "
	cQuery += " ON F2_DUPL = E1_NUM  AND F2_SERIE = E1_SERIE AND F2_CLIENT = E1_CLIENTE AND F2_LOJA = E1_LOJA "
	cQuery += " INNER JOIN "+RetSqlName("SD2")+" D2
	cQuery += " ON F2_FILIAL = D2_FILIAL 
	cQuery += " AND F2_DOC = D2_DOC
	cQuery += " AND F2_SERIE = D2_SERIE
	cQuery += " AND F2_CLIENTE = D2_CLIENTE
	cQuery += " AND F2_LOJA = D2_LOJA 
	cQuery += " INNER JOIN "+RetSqlName("SF4")+" SF4 ON SF4.F4_CODIGO = D2.D2_TES
	cQuery += " AND SF4.F4_FILIAL = SUBSTRING(D2.D2_FILIAL,1,2)
	cQuery += " AND SF4.F4_DUPLIC='S'
	cQuery += " AND SF4.D_E_L_E_T_=''
	cQuery += " WHERE F2.D_E_L_E_T_ = '' "
	cQuery += " AND F2_YFILIAL = '"+mv_par02+"' "
	cQuery += " AND F2_YCONTRA = '"+T01->ZZ0_NUMERO+"' "
	cQuery += " AND F2_CLIENT = '"+mv_par03+"' "
	cQuery += " AND F2_LOJA = '"+mv_par04+"' "
	IF alltrim(MV_PAR01) <> ''
		cQuery += " AND E1_EMISSAO LIKE '"+mv_par01+"%' "
	ENDIF
	cQuery += " GROUP BY F2_EMISSAO, F2_VALBRUT, F2_FRETE,  F2_DESCONT,  F2_DOC "
	if Select("T02") > 0
		dbSelectArea("T02")
		dbCloseArea()
	endif

	TcQuery cQuery New Alias T02

	While !(T02->(Eof()))
				
		IF nLin >= 750
			oPrinter:EndPage()
			funCbc()
		ENDIF
		
		oPrinter:Say ( nLin, 050, DTOC(STOD(T02->F2_EMISSAO)) ,  oFont1 )
		oPrinter:Say ( nLin, 110, T02->F2_DOC ,  oFont1 )
		oPrinter:Say ( nLin, 170, PadL(alltrim(TRANSFORM(T02->VALORMEDICAO, "@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 280, PadL(alltrim(TRANSFORM(T02->F2_DESCONT, "@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 360, PadL(alltrim(TRANSFORM(T02->F2_FRETE, "@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 420, PadL(alltrim(TRANSFORM(T02->F2_VALBRUT, "@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 480, PadL(alltrim(TRANSFORM(T02->VALOR, "@E 999,999,999.99")),12,) ,  oFont1 )
		
		
		
		nTFis += T02->VALORMEDICAO
		nTFrete += T02->F2_FRETE
		//nTJuros += T02->E1_JUROS
		nTDesconto += T02->F2_DESCONT
		nTAdFisc += T02->F2_VALBRUT
		nTBaixaNF += T02->VALOR
						
		
		T02->(dbSkip())
		nLin := nLin + 10
	ENDDO
	nLin := nLin + 30
		
		
	oPrinter:Line( nLin, 040, nLin, 570)
	oPrinter:Say ( nLin+15, 050, "TOTAL: " ,  oFont3 )
	oPrinter:Say ( nLin+15, 170, PadL(alltrim(TRANSFORM(nTFis, "@E 999,999,999.99")),12,) ,  oFont3 )
	oPrinter:Say ( nLin+15, 280, PadL(alltrim(TRANSFORM(nTDesconto, "@E 999,999,999.99")),12,) ,  oFont3 )
	oPrinter:Say ( nLin+15, 360, PadL(alltrim(TRANSFORM(nTFrete, "@E 999,999,999.99")),12,) ,  oFont3 )
	oPrinter:Say ( nLin+15, 420, PadL(alltrim(TRANSFORM(nTAdFisc, "@E 999,999,999.99")),12,) ,  oFont3 )
	oPrinter:Say ( nLin+15, 480, PadL(alltrim(TRANSFORM(nTBaixaNF, "@E 999,999,999.99")),12,) ,  oFont3 )
	
		
	nLin := nLin + 50
	
	
	
	if T02->(EOF()) .AND. nLin >= 750
		oPrinter:EndPage()
		funCbc()
	endif
	
	T02->(DbCloseArea())

return

Static Function Totalizador()


	
	Local nTotal := 0
	//oPrinter:Say ( nLin, 40, "POSI��O FINCANEIRA(ADIANTAMENTOS)",  oFont3 )
	//oPrinter:Say ( nLin, 170, "Frete Principal: "+cvaltochar(T01->ZZ0_FRTCLI),  oFont1 )
	//oPrinter:Say ( nLin, 320, "Frete Complementar: "+cvaltochar(T01->ZZ0_FRTEMP),  oFont1 )
			
	nLin := nLin + 10
	
	IF nLin >= 700
		oPrinter:EndPage()
		funCbc()
	ENDIF
		
			
	oBrush1 := TBrush():New( , CLR_HGRAY)
	oPrinter:Fillrect( {nLin+5, 310, nLin-9, 570 }, oBrush1, "-4")
	oPrinter:Say ( nLin, 360, "QUADRO RESUMO FINANCEIRO",  oFont3 )
	oPrinter:Box( nLin+5, 310, nLin+140, 570, "-4")
	oPrinter:Say ( nLin+20, 320, "ENVIADO:",  oFont1 )
	oPrinter:Say ( nLin+20, 460, alltrim(TRANSFORM(nTEnviados, "@E 999,999,999.99")),  oFont1 )//nTEnviados
	
	oPrinter:Say ( nLin+40, 320, "DEVOLVIDO:",  oFont1 )
	oPrinter:Say ( nLin+40, 460, alltrim(TRANSFORM(nTDevolvidos, "@E 999,999,999.99")),  oFont1 )
	
//	oPrinter:Say ( nLin+40, 460, "DEVOLVIDO:",  oFont1 )
	
	oPrinter:Say ( nLin+60, 320, "FRETE:",  oFont1 )
	oPrinter:Say ( nLin+60, 460, alltrim(TRANSFORM(nTFrete, "@E 999,999,999.99")),  oFont1 )
	
	
	//oPrinter:Say ( nLin+100, 320, "PAGAMENTO",  oFont3 )
			
	
	
	oPrinter:Say ( nLin+080, 320, "PAGAMENTOS :",  oFont1 )
	oPrinter:Say ( nLin+080, 460, alltrim(TRANSFORM( (nTFBai + nTotalDesconto) - (nTFJur + nTFMulta), "@E 999,999,999.99")),  oFont1 )
	
	
	oPrinter:Say ( nLin+100, 320, "JUROS/MULTA:",  oFont1 )
	oPrinter:Say ( nLin+100, 460, alltrim(TRANSFORM(nTFJur + nTFMulta, "@E 999,999,999.99")),  oFont1 )
	
	//oPrinter:Say ( nLin+120, 320, "ADIANTAMENTO:",  oFont1 )
	//oPrinter:Say ( nLin+120, 460, alltrim(TRANSFORM(nTFin, "@E 999,999,999.99")),  oFont1 )
	
	//oPrinter:Say ( nLin+140, 320, "BAIXA :",  oFont1 )
	//oPrinter:Say ( nLin+140, 460, alltrim(TRANSFORM(nTBaixaNF, "@E 999,999,999.99")),  oFont1 )
	
	//oPrinter:Say ( nLin+140, 320, "BAIXA NF:",  oFont1 )
	//oPrinter:Say ( nLin+140, 460, alltrim(TRANSFORM(nTBaixaNF, "@E 999,999,999.99")),  oFont1 )
	
	oPrinter:Say ( nLin+120, 320, "SALDO:",  oFont1 )
	oPrinter:Say ( nLin+120, 460, alltrim(TRANSFORM((nTEnviados + nTFrete + nTFJur + nTFMulta) - ( nTDevolvidos + ((nTFBai + nTotalDesconto) - (nTFJur + nTFMulta + nTotalDesconto))) , "@E 999,999,999.99")),  oFont1 )
	
	//nTotalDesconto
	nTFis := 0
	nTFrete := 0
	nTJuros := 0
	nTDesconto := 0
	nTAdFisc := 0
	nTBaixaNF := 0
	
	
	nTFin := 0
	nTFJur := 0
	nTFDesc := 0
	nTotalDesconto := 0 
	nTFBai := 0
	
return

Static Function RelAjustes()

	Local nTotal := 0
	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local cLocal          := "c:\"
	Local cFilePrint := "Ajustes_Contratos.pdf"
	//Local xArea := GetArea()
	Local Vez := 0

	local _ano := space(4)
	local _filial := space(6)
	local _client := space(TAMSX3("A1_COD")[1])
	local _loja := space(TAMSX3("A1_LOJA")[1])
	local _data := space(TamSX3("D1_DTDIGIT")[1])
	Local aPergs := {}
	Local aRet := {}
	Local lRet
	Local cQuery
	Local cAux := ''
	Local cValid := .T.
	
	Local aErros := {}
	
	cRazao := POSICIONE("SA1",1,XFILIAL("SA1")+MV_PAR03+MV_PAR04,"A1_NOME")

	oPrinter := FWMSPrinter():New(cFilePrint, IMP_SPOOL, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetPortrait()
	
	cQuery := "SELECT CASE ZZ0_STATUS WHEN 'I' THEN 'INCLUIDO' WHEN 'L' THEN 'LIBERADO' WHEN 'A' THEN 'ENCERRADO' END AS 'STATUS',*  "
	cQuery += "FROM "+RETSQLNAME("ZZ0")+" ZZ0 "
	cQuery += " WHERE ZZ0_FILIAL = '"+mv_par02+"' "
	IF alltrim(MV_PAR01) <> ''
		cQuery += " AND ZZ0_ANOCOM = '"+mv_par01+"' "
	ENDIF
	IF alltrim(MV_PAR05) <> ''
		cQuery += " AND ZZ0_NUMERO = '"+mv_par05+"' "
	ENDIF
	cQuery += " AND  D_E_L_E_T_ = ' ' "
	cQuery += " AND ZZ0_CLIENT = '"+mv_par03+"' AND ZZ0_LOJA = '"+mv_par04+"' AND ZZ0_EMISSA <= '"+DTOS(mv_par06)+"' "

	if Select("T01") > 0
		dbSelectArea("T01")
		dbCloseArea()
	endif

	TcQuery cQuery New Alias T01

	While !T01->(Eof())
		
		cContra := T01->ZZ0_NUMERO
		cSitua := T01->STATUS
		
		funCbc()
	
		IF nLin > 750
			nLin   := 80
		ENDIF
	//nLin := nLin+50
		oPrinter:Say ( nLin, 40, "MATERIAIS ENVIADOS",  oFont3 )
	//oPrinter:Say ( nLin, 170, "Frete Principal: "+cvaltochar(T01->ZZ0_FRTCLI),  oFont1 )
	//oPrinter:Say ( nLin, 320, "Frete Complementar: "+cvaltochar(T01->ZZ0_FRTEMP),  oFont1 )
			
		nLin := nLin + 10
			
		oBrush1 := TBrush():New( , CLR_HGRAY)
		oPrinter:Fillrect( {nLin+5, 40, nLin-7, 570 }, oBrush1, "-4")
		oPrinter:Say ( nLin, 50, "N�Nfe",  oFont1 )
		oPrinter:Say ( nLin, 110, "Data",  oFont1 )
		oPrinter:Say ( nLin, 180, "Descricao",  oFont1 )
		oPrinter:Say ( nLin, 340, "Quantidade",  oFont1 )
		oPrinter:Say ( nLin, 390, "Valor",  oFont1 )
		oPrinter:Say ( nLin, 440, "Suplemento",  oFont1 )
		oPrinter:Say ( nLin, 500, "Total",  oFont1 )
	
		nLin := nLin + 15
	

	
		cQuery := " SELECT ZZ3_FILIAL, F2_DOC, F2_SERIE, F2_EMISSAO, ZZ3_NUMERO,ZZ3_MEDICA, ZZ3_CLIENT, ZZ3_LOJA, SUM(ZZ1_VLRDSC) ZZ1_VLRDSC, SUM(ZZ3_QUANT) ZZ3_QUANT, SUM(ZZ3_VLUNIT) ZZ3_VLUNIT, SUM(ZZ3_VTOTAL) ZZ3_VTOTAL FROM "+RetSqlName("ZZ3")+" ZZ3   "
		cQuery += " INNER JOIN "+RetSqlName("ZZ2")+" ZZ2 "
		cQuery += " ON ZZ3_FILIAL = ZZ2_FILIAL "
		cQuery += " AND ZZ3_NUMERO = ZZ2_NUMERO "
		cQuery += " AND ZZ3_CLIENT = ZZ2_CLIENT "
		cQuery += " AND ZZ3_LOJA = ZZ2_LOJA "
		cQuery += " AND ZZ3_MEDICA = ZZ2_MEDICA "
		cQuery += " LEFT JOIN "+RetSqlName("SF2")+" F2 "
		cQuery += " ON ZZ3_FILIAL = F2_FILIAL "
		cQuery += " AND ZZ3_NUMERO = F2_YCONTRA "
		cQuery += " AND ZZ3_MEDICA = F2_YMEDICA "
		cQuery += " AND ZZ3_CLIENT = F2_CLIENT "
		cQuery += " AND ZZ3_LOJA = F2_LOJA "
		cQuery += " INNER JOIN ZZ1010 ZZ1 ON ZZ3_FILIAL = ZZ1_FILIAL AND ZZ3_NUMERO = ZZ1_NUMERO AND ZZ3_PRODUT = ZZ1_PRODUT AND ZZ3_CLIENT = ZZ1_CLIENT AND ZZ3_LOJA = ZZ1_LOJA AND ZZ3_ITEM = ZZ1_ITEM "
		cQuery += " INNER JOIN SF4010 SF4 ON SF4.F4_CODIGO = ZZ3.ZZ3_TES AND SF4.F4_FILIAL = SUBSTRING(ZZ3.ZZ3_FILIAL,1,2) AND SF4.D_E_L_E_T_='' "
		cQuery += " INNER JOIN ZZ7010 ZZ7 ON ZZ3_PRODUT = ZZ7_CODIGO AND ZZ7_SUPLEM <> 'S' "
		cQuery += " WHERE ZZ3_FILIAL = '"+mv_par02+"' AND ZZ3_NUMERO = '"+T01->ZZ0_NUMERO+"' AND  ZZ3.D_E_L_E_T_ = ' ' "
		cQuery += " AND ZZ2.D_E_L_E_T_ = '' "
		cQuery += " AND SF4.F4_DUPLIC='S' "
		cQuery += " AND F2.D_E_L_E_T_ = '' "
		cQuery += " AND ZZ1.D_E_L_E_T_ = '' "
		IF alltrim(MV_PAR01) <> ''
			cQuery += " AND ZZ2_DTGER LIKE '"+mv_par01+"%' "
		ENDIF
		cQuery += " GROUP BY ZZ3_FILIAL, F2_DOC, F2_SERIE, F2_EMISSAO, ZZ3_NUMERO, ZZ3_CLIENT, ZZ3_LOJA, ZZ3_MEDICA "
	
	
	
		if Select("T02") > 0
			dbSelectArea("T02")
			dbCloseArea()
		endif

		TcQuery cQuery New Alias T02
		cAux := T02->F2_DOC
 
	
		While !T02->(Eof())
			
			cValid := .T.
			cQuery2 := " SELECT F2_VALBRUT, F2_DESCONT  FROM SF2010 F2 "
			cQuery2 += " WHERE F2.D_E_L_E_T_ = '' "
			cQuery2 += " AND F2_DOC = '"+T02->F2_DOC+"' "
			cQuery2 += " AND F2_SERIE = '"+T02->F2_SERIE+"' "
			cQuery2 += " AND F2_CLIENTE = '"+T02->ZZ3_CLIENT+"' "
			cQuery2 += " AND F2_LOJA = '"+T02->ZZ3_LOJA+"' "
			TcQuery cQuery2 new Alias T03
	
			While !T03->(EOF())
	
				IF ABS((T03->F2_VALBRUT + T03->F2_DESCONT) - T02->ZZ3_VTOTAL) > 5
					AADD(aErros,{T02->ZZ3_FILIAL,T02->F2_DOC, T02->F2_SERIE,T02->ZZ3_CLIENT, T02->ZZ3_LOJA,T02->ZZ3_NUMERO,T02->ZZ3_VTOTAL,T03->F2_VALBRUT + T03->F2_DESCONT})
				ENDIF
				T03->(DbSkip())
			Enddo
			DbCloseArea("T03")
	
			T02->(DbSkip())
	
		endDo
		
		T02->(DbCloseArea())
		
		
		IF cValid
		
			for x := 1 to len(aErros)
			
			
			
				cQuery2 := " SELECT D2_DOC, D2_CLIENTE, D2_SERIE, D2_ITEM, D2_EMISSAO, B1_DESC, D2_QUANT,D2_PRUNIT, D2_DESCON, ((D2_QUANT * D2_PRUNIT) - D2_DESCON) TOTAL FROM SD2010 D2 "
				cQuery2 += " INNER JOIN SB1010 B1 ON B1_COD = D2_COD "
				cQuery2 += " WHERE D2.D_E_L_E_T_ = '' "
				cQuery2 += " AND B1.D_E_L_E_T_ = '' "
				cQuery2 += " AND D2_DOC = '"+aErros[x][2]+"' "
				cQuery2 += " AND D2_SERIE = '"+aErros[x][3]+"' "
				cQuery2 += " AND D2_CLIENTE = '"+aErros[x][4]+"' "
				cQuery2 += " AND D2_LOJA = '"+aErros[x][5]+"' "
				cQuery2 += " GROUP BY D2_DOC, D2_CLIENTE,D2_SERIE, D2_ITEM, D2_EMISSAO, B1_DESC, D2_QUANT,D2_PRUNIT, D2_DESCON	 "
				TcQuery cQuery2 new Alias T04
			
				WHILE !T04->(EOF())
			
					IF nLin >= 750
						oPrinter:EndPage()
						funCbc()
					ENDIF
			
					oPrinter:Say ( nLin, 040, T04->D2_DOC ,  oFont1 )
					oPrinter:Say ( nLin, 110, DTOC(STOD(T04->D2_EMISSAO)) ,  oFont1 )
					oPrinter:Say ( nLin, 180, SUBSTR(T04->D2_ITEM + T04->B1_DESC,1,32) ,  oFont1 )
					oPrinter:Say ( nLin, 340, cValtochar(T04->D2_QUANT) ,  oFont1 )
					oPrinter:Say ( nLin, 390, alltrim(TRANSFORM(T04->D2_PRUNIT, "@E 999,999,999.99")) ,  oFont1 )
					oPrinter:Say ( nLin, 440, alltrim(TRANSFORM(T04->D2_DESCON, "@E 999,999,999.99")) ,  oFont1 )
					oPrinter:Say ( nLin, 500, alltrim(TRANSFORM(T04->TOTAL, "@E 999,999,999.99")) ,  oFont1 )
		
					nTEnviados += T04->TOTAL
		
				
					T04->(dbSkip())
					nLin := nLin + 10
			
				EndDo
			
				T04->(DbCloseArea())
			next
		
		ENDIF
		nLin := nLin + 30
	
	
		
		oPrinter:Line( nLin, 040, nLin, 570)
	
		oPrinter:Say ( nLin+15, 360, "TOTAL DO ENVIADOS:",  oFont3 )
		oPrinter:Say ( nLin+15, 500, PadR(alltrim(TRANSFORM(nTEnviados, "@E 999,999,999.99")),12,) ,  oFont3 )
	
		
		nLin := nLin + 50
		nTotal := 0
	
		/*if nLin >= 750
			oPrinter:EndPage()
			funCbc()
		endif*/
		
		
		
		T04->(DbCloseArea())
		
		cValid := .F.
		T01->(dbSkip())
	
		oPrinter:EndPage()
	
	EnDDo
	
	T01->(DbCloseArea())
	
	oPrinter:Preview()
return