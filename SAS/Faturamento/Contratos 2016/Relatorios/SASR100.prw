#include "tbiconn.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "protheus.ch"
#include "topconn.ch"

User Function SASR100(Qry, cAlias)

	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local oPrinter
	Local cLocal          := "c:\TEMP\"					
	Local oFont1 	:= TFont():New( "Arial",,11,,.F.,,,,,  .F. )
	Local oFont2 	:= TFont():New( "Arial",,9,,.F.,,,,,  .F. )
	Local oFont3 	:= TFont():New( "Arial",,14,,.F.,,,,,  .F. )
	Local cFilePrint := "romaneio.pdf"
	Local xArea := GetArea()
	Local nLin   := 25
	Local nTmLin := 15
	Local nSalto := 20	
	Local nQuant := 0
	Local cChave := ""
	
	oPrinter := FWMSPrinter():New(cFilePrint, IMP_SPOOL, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetPortrait()
	
	While (cAlias)->(!EOF())
		nLin   := 25
		nQuant := 0
		SC5->(DbGoTo((cAlias)->REGSC5))
		cChave := SC5->C5_FILIAL + SC5->C5_NUM
		oPrinter:StartPage()
	
		/*oPrinter:Box( 10, 10, 780, 600, "-4")
		oPrinter:Box( 10, 10, 140, 600, "-4")
		oPrinter:Box( 140, 10, 155, 600, "-4")
		oPrinter:Box( 680, 10, 780, 600, "-4")*/
	
		oPrinter:Say ( nLin, 200, "ROMANEIO DE ENTREGA DO CLIENTE",  oFont3 )
		nLin := nLin + nTmLin
		oPrinter:Say ( nLin, 250, "N�O � DOCUMENTO FISCAL",  oFont2 )
	
	
		nLin := nLin + nTmLin + 10 
		oPrinter:Say ( nLin, 20, "CLIENTE: " + SC5->C5_CLIENT +'/'+ SC5->C5_LOJACLI + ' - ' +;
		POSICIONE("SA1",1,XFILIAL("SA1")+SC5->C5_CLIENT+SC5->C5_LOJACLI,"A1_NOME"),  oFont1 )
	
		/*
		cquery := "SELECT ZZL.ZZL_DESCR FROM "
		cquery += "	ZZG010 ZZG "
		cquery += "	LEFT JOIN ZZL010 ZZL "
		cquery += "		ON ZZL.ZZL_COD = ZZG.ZZG_ESPCTR AND ZZL.D_E_L_E_T_='' "
		cquery += "WHERE "
		cquery += "	ZZG.D_E_L_E_T_='' AND ZZG.ZZG_COD='"+ZZ2->ZZ2_TIPO+"'"				
		*/
		cquery := "SELECT ZZG_DESCRI FROM "
		cquery += "	ZZG010 ZZG "
		cquery += "WHERE "
		cquery += "	ZZG.D_E_L_E_T_='' AND ZZG.ZZG_COD='"+SC5->C5_YTPVEN+"'"
	
		TCQuery cquery new alias T0402
		cTipoPed :=  rtrim(T0402->ZZG_DESCRI)
		T0402->(DBCLOSEAREA())	
	
	
		nLin := nLin + nTmLin
		oPrinter:Say ( nLin, 20, "TIPO DE PEDIDO: " + cTipoPed,  oFont1 )
	
		nLin := nLin + nTmLin
		oPrinter:Say ( nLin, 20, "NOTA FISCAL DE VENDA/BONIFICADO: " + SC5->C5_NOTA,  oFont1 )
	
		//nLin := nLin + nTmLin
		//oPrinter:Say ( nLin, 20, "NOTA FISCAL DE REMESSA: " + "PERGUNTAR A RAISSA",  oFont1 )
		
		nLin := nLin + nTmLin
		oPrinter:Say ( nLin, 20, "PEDIDO: " + cChave,  oFont1 )
	
		nLin := nLin + nTmLin
		//oPrinter:Say ( nLin, 20, "DATA EMISSAO:",  oFont1 )
	
	
		nLin := nLin + 25
		oPrinter:Say ( nLin,  20, "TIPO",  oFont1 )
		//oPrinter:Say ( nLin, 120, "GRUPO",  oFont1 )
		oPrinter:Say ( nLin, 330, "SERIE",  oFont1 )
		oPrinter:Say ( nLin, 490, "ENVIO",  oFont1 )
		oPrinter:Say ( nLin, 550, "VOLUMES",  oFont1 )
	
		if Select("QRON") > 0
			QRON->(dbCloseArea())
		endif
	
	
		nValorPed := GetValor(SC5->C5_FILIAL, SC5->C5_NUM)
		nValDesc := SC5->C5_DESCONT
		nValFret := SC5->C5_FRETE
	
		cQuery := " SELECT * FROM "+RetSqlName("SC6")+" C6(NOLOCK) "
		cQuery += " INNER JOIN "+RetSqlName("ZZ7")+" ZZ7(NOLOCK) ON C6_YCODKIT = ZZ7_CODIGO AND ZZ7.D_E_L_E_T_ = '' "
		cQuery += " WHERE C6.D_E_L_E_T_ = '' "
		cQuery += " AND C6_FILIAL = '"+SC5->C5_FILIAL+"' "
		cQuery += " AND C6_NUM = '"+SC5->C5_NUM+"' "
		TcQuery cQuery new Alias QRON
		
		While !QRON->(Eof())
			
			If	nLin >= 650
				nLin   := 25
				oPrinter:EndPage()
				oPrinter:StartPage()
			Endif
			
			
			DO CASE
				CASE RTRIM(QRON->ZZ7_CATEGO) = 'A'
				cCatego := 'ALUNO'
				CASE RTRIM(QRON->ZZ7_CATEGO) = 'C'
				cCatego := 'COORDENADOR'
				CASE RTRIM(QRON->ZZ7_CATEGO) = 'P'
				cCatego := 'PROFESSOR'
				OTHERWISE
				cCatego := ''
			ENDCASE	
	
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin,  20, cCatego,  oFont1 )
	
	
			//GRUPO
			cquery := "SELECT ZZP_DESCRI FROM ZZP010 WHERE ZZP_CODIGO='"+QRON->ZZ7_GRUPO+"'"		
			TCQuery cquery new alias T0402
			cGrupo :=  T0402->ZZP_DESCRI
			T0402->(DBCLOSEAREA())	
	
	
			//oPrinter:Say ( nLin, 120, cGrupo,  oFont1 )
	
			cquery := "SELECT ZZO_DESC FROM ZZO010 WHERE ZZO_COD='"+QRON->ZZ7_SERIE+"'"		
			TCQuery cquery new alias T0402
			cSerie :=  IIF(ALLTRIM(QRON->ZZ7_SERIE) == "99", "PRODUTO AVULSO", T0402->ZZO_DESC)
			T0402->(DBCLOSEAREA())
	
			oPrinter:Say ( nLin, 330, cSerie,  oFont1 ) //serie
			oPrinter:Say ( nLin, 495, QRON->ZZQ_ENVIO,  oFont1 )  //envio
			oPrinter:Say ( nLin, 555, PADL(cValtoChar(QRON->ZZQ_QUANT),5),  oFont1 ) //volumes
	
			nQuant += QRON->ZZQ_QUANT
	
			
	
			QRON->(dbSkip())
	
		Enddo
	
		QRON->(dbCloseArea())
	
		nLin := 705
		oPrinter:Say ( nLin, 250, "TOTAL DE VOLUMES",  oFont1 )
		oPrinter:Say ( nLin, 450, padl(TransForm(nQuant, "@E 999,999,999"),20),  oFont1 )				
	
		nLin := nLin + nTmLin				
		oPrinter:Say ( nLin, 250, "VALOR TOTAL DOS PRODUTOS",  oFont1 )
		oPrinter:Say ( nLin, 450, padl(TransForm(nValorPed, "@E 9999,999,999.99"),20),  oFont1 )				
	
		nLin := nLin + nTmLin				
		oPrinter:Say ( nLin, 250, "FRETE DESTACADO",  oFont1 )
		oPrinter:Say ( nLin, 450, padl(TransForm(nValFret, "@E 9999,999,999.99"),20),  oFont1 )				
	
		nLin := nLin + nTmLin				
		oPrinter:Say ( nLin, 250, "DESCONTO",  oFont1 )
		oPrinter:Say ( nLin, 450, padl(TransForm(nValDesc, "@E 9999,999,999.99"),20),  oFont1 )	
	
		nLin := nLin + nTmLin				
		oPrinter:Say ( nLin, 250, "VALOR TOTAL DA NOTA",  oFont1 )
		oPrinter:Say ( nLin, 450, padl(TransForm((nValorPed+nValFret)-nValDesc, "@E 9999,999,999.99"),20),  oFont1 )
	
	
		oPrinter:EndPage()
			
		(cAlias)->(DbSkip())
	EndDO
	oPrinter:Preview()

Return

Static Function GetValor(cFilPV, cPedVen)

	Local cQuery := ""
	Local nValTotal := 0

	cQuery := ""
	cQuery += " SELECT SUM(C6_VALOR) AS TOTAL "
	cQuery += "   FROM " + RETSQLNAME("SC6") + " SC6 "
	cQuery += "  WHERE SC6.D_E_L_E_T_ = '' "
	cQuery += "    AND SC6.C6_FILIAL = '" + XFILIAL("SC6",cFilPV) + "' "
	cQuery += "    AND SC6.C6_NUM ='" + cPedVen + "' "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XC6",.F.,.T.)

	nValTotal := Round(XC6->TOTAL,2)

	XC6->(DBCLOSEAREA())	

Return nValTotal 



