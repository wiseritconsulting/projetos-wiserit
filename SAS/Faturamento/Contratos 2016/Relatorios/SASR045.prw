// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : SASR045
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Rog�rio J�come            | Descricao Pr� Notas (J�ssica)
// ---------+-------------------+-----------------------------------------------------------
// 21/03/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"
#INCLUDE 'TOPCONN.CH'
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo
Montagem da tela de processamento

@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     21/03/2016
/*/
//------------------------------------------------------------------------------------------
user function SASR045()
	//--< vari�veis >---------------------------------------------------------------------------
	Local oReport
	private cPerg := "SASR045"

	AjustaSX1(cPerg)


	oReport := ReportDef()
	oReport:PrintDialog()
	
	
Return
 
 
Static Function ReportDef()
	Local oReport
	Local oSection
	Local oSection1
	Local aOrdem:={}


	oReport := TReport():New(cPerg,"PR� NOTAS" ,cPerg,{|oReport| ReportPrint(oReport)},"PR� NOTAS")
	oReport:SetLandscape()

	oSection1 := TRSection():New(oReport,OemToAnsi("RELAT�RIO MEDI��ES X NF"),{"ZZH"},aOrdem)
	TRCell():New(oSection1,"ZZH_CLIENT","","CLIENTE","@!",TAMSX3("ZZH_CLIENT")[1])
	TRCell():New(oSection1,"ZZH_MEDICA","","MEDICAO","@!",TAMSX3("ZZH_MEDICA")[1])
	TRCell():New(oSection1,"A1_NOME","","RAZ�O SOCIAL","@!",TAMSX3("A1_NOME")[1])
	TRCell():New(oSection1,"ZZH_PC01NF","","NF1","@!",TAMSX3("ZZH_PC01NF")[1])
	TRCell():New(oSection1,"ZZH_NF02","","NF2","@!",TAMSX3("ZZH_NF02")[1])
	TRCell():New(oSection1,"ZZH_NF03","","NF3","@!",TAMSX3("ZZH_NF03")[1])
	TRCell():New(oSection1,"F1_EMISSAO","","DATA EMISS�O","@!",TAMSX3("F1_EMISSAO")[1])
	TRCell():New(oSection1,"F1_VOLUME1","","VOLUMES","@!",TAMSX3("F1_VOLUME1")[1])
	TRCell():New(oSection1,"ZZH_TRANSP","","TRANSPORTADORA","@!",TAMSX3("ZZH_TRANSP")[1])
	TRCell():New(oSection1,"F1_YFILIAL","","FILIAL","@!",TAMSX3("ZZH_FILIAL")[1])
	TRCell():New(oSection1,"F1_DAUTNFE","","DATA FATURAMENTO","@!",TAMSX3("F1_DAUTNFE")[1])
	TRCell():New(oSection1,"MUNICIPIO","","MUNICIPIO","@!",TAMSX3("RA_NOME")[1])
	TRCell():New(oSection1,"F1_EST","","ESTADO","@!",TAMSX3("F1_EST")[1])
	TRCell():New(oSection1,"F1_VALBRUT","","VALOR TOTAL","@E 999,999.99",TAMSX3("F1_VALBRUT")[1])
	TRCell():New(oSection1,"F1_PBRUTO","","PESO BRUTO","@!",TAMSX3("F1_PBRUTO")[1])
	TRCell():New(oSection1,"ZZH_PEDDEV","","NUMERO DO PEDIDO","@!",TAMSX3("ZZH_PEDDEV")[1])
	
	//TRCell():New(oSection1,"FILIAL","","Nome","@!",23)
	
	//oSection1:= TRSection():New(oReport, "PR� NOTAS", {"ZZH"}, NIL, .F., .T.)
	TRCell():New(oSection1,"D1_COD"  ,"TRBNCM","PRODUTO"		,"@!",20)
	TRCell():New(oSection1,"B1_DESC"  ,"TRBNCM","DESCRICAO"	,"@!",TAMSX3("B1_DESC")[1])
	TRCell():New(oSection1,"D1_QUANT"  ,"TRBNCM","QUANTIDADE"	,"@!",TAMSX3("D1_QUANT")[1])
	TRCell():New(oSection1,"D1_VUNIT"	,"TRBNCM","VALOR UNIT"	,"@E 999,999.99",TAMSX3("D1_VUNIT")[1])
	TRCell():New(oSection1,"D1_CF"	,"TRBNCM","CFOP"	,"@!",TAMSX3("D1_CF")[1])
	TRCell():New(oSection1,"D1_TES"	,"TRBNCM","TES"			,"@!",TAMSX3("D1_TES")[1])
	TRCell():New(oSection1,"D1_PEDIDO"  	,"TRBNCM","PEDIDO"	,"@!",TAMSX3("D1_PEDIDO")[1])
	TRCell():New(oSection1,"D1_PESO"	,"TRBNCM","PESO ITEM"			,"@!",TAMSX3("D1_PESO")[1])
	
		
	oSection1:SetPageBreak(.T.)
	oSection1:SetTotalText(" ")
	
RETURN (oReport)

Static Function ReportPrint(oReport)

	Local oSection1 := oReport:Section(1)
	//Local oSection1 := oReport:Section(2)
	Pergunte(cPerg,.F.)

	cQuery := " SELECT F1_YFILIAL,F1_DOC, F1_SERIE,D1_ITEM,F1_EMISSAO,ZZH_PC01NF, ZZH_PC02NF,ZZH_NF02,ZZH_NF03, ZZH_NUMERO, ZZH_MEDICA, ZZH_CLIENT,ZZH_FONF01,ZZH_FONF02,ZZH_FONF03, "
	cQuery += " A1_NOME,A1_MUN, ZZH_PV01NF, ZZH_NF02, ZZH_NF03, ZZH_TRANSP, ZZH_NOMTRA, ZZH_PEDDEV, "
	cQuery += " ZZH_QUATVL, "
	cQuery += " F1_DAUTNFE, F1_EST, F1_PBRUTO,F1_VALBRUT, ZZH_PEDDEV, D1_COD,D1_QUANT, D1_VUNIT,D1_CF, D1_TES, D1_PEDIDO,D1_PESO, B1_DESC,B1_PESO "
	cQuery += " FROM ZZH010 ZZH "
	cQuery += " JOIN SF1010 F1 (NOLOCK) "
	cQuery += " ON F1.F1_YFILIAL = ZZH_FILIAL "
	cQuery += " AND (F1.F1_FORNECE = ZZH_FONF01 OR F1.F1_FORNECE = ZZH_FONF03) "
	cQuery += " AND (F1.F1_LOJA = ZZH_LJNF01 OR F1.F1_LOJA = ZZH_LJNF03) "
	cQuery += " AND (F1_DOC = ZZH_PC01NF OR F1_DOC = ZZH_NF03) "
	//cQuery += " AND F1.F1_YMEDICA = ZZH_MEDICA "
	//cQuery += " AND F1.F1_YCONTRA = ZZH_NUMERO "
	cQuery += " INNER JOIN SD1010 D1 "
	cQuery += " ON D1_FILIAL = F1_YFILIAL "
	cQuery += " AND (D1_DOC = ZZH_PC01NF OR D1_DOC = ZZH_NF03) "
	cQuery += " AND (F1.F1_FORNECE = D1_FORNECE) "
	cQuery += " INNER JOIN SA1010 A1 "
	cQuery += " ON A1_COD = ZZH_CLIENT AND A1_LOJA = ZZH_LOJA "
	cQuery += " INNER JOIN SB1010 B1 "
	cQuery += " ON B1_COD = D1_COD "
	cQuery += " WHERE ZZH.D_E_L_E_T_ = '' "
	cQuery += " AND F1.D_E_L_E_T_ = '' "
	cQuery += " AND A1.D_E_L_E_T_ = '' AND B1.D_E_L_E_T_ = '' AND D1.D_E_L_E_T_ = ''  "
	cQuery += " AND F1_EMISSAO BETWEEN '"+DTOS(mv_par01)+"' AND '"+DTOS(mv_par02)+"'  "
	cQuery += " AND D1_LOCAL BETWEEN '"+mv_par03+"' AND '"+mv_par04+"'  "
	cQuery += " AND F1_DOC BETWEEN '"+mv_par05+"' AND '"+mv_par06+"' "
	cQuery += " AND F1_SERIE BETWEEN '"+mv_par07+"' AND '"+mv_par08+"' "
	cQuery += " AND D1_TES BETWEEN '"+mv_par09+"' AND '"+mv_par10+"' "
	cQuery += " GROUP BY F1_YFILIAL,F1_DOC, F1_SERIE,D1_ITEM,F1_EMISSAO,ZZH_PC01NF, ZZH_PC02NF,ZZH_NF02,ZZH_NF03, ZZH_NUMERO, ZZH_MEDICA, ZZH_CLIENT,ZZH_FONF01,ZZH_FONF02,ZZH_FONF03, "
	cQuery += " A1_NOME,A1_MUN, ZZH_PV01NF, ZZH_NF02, ZZH_NF03, ZZH_TRANSP, ZZH_NOMTRA, ZZH_PEDDEV, "
	cQuery += " ZZH_QUATVL, "
	cQuery += " F1_DAUTNFE, F1_EST, F1_PBRUTO,F1_VALBRUT, ZZH_PEDDEV, D1_COD,D1_QUANT,D1_VUNIT,D1_CF,D1_TES,D1_PEDIDO, D1_PESO, B1_DESC, B1_PESO "
	cQuery += " ORDER BY F1_YFILIAL, F1_DOC, D1_ITEM "
	TcQuery cQuery New Alias T01

	oReport:SetMeter(T01->(LastRec()))
  
  	oSection1:Init()
  
	while !T01->(EOF())
		if oReport:Cancel()
			exit
		endif
	
	
	
		oReport:IncMeter()
			
		//cFil 	:= T01->F1_YFILIAL
		//cDoc	:= T01->F1_DOC
		//cSerie	:= T01->F1_SERIE
			//imprimo a primeira se��o	
		oSection1:Cell("ZZH_CLIENT"):SetValue(T01->ZZH_CLIENT)
		oSection1:Cell("A1_NOME"):SetValue(T01->A1_NOME)
		oSection1:Cell("ZZH_MEDICA"):SetValue(T01->ZZH_MEDICA)
		oSection1:Cell("ZZH_PC01NF"):SetValue(T01->ZZH_PC01NF)
		oSection1:Cell("ZZH_NF02"):SetValue(T01->ZZH_NF02)
		oSection1:Cell("ZZH_NF03"):SetValue(T01->ZZH_NF03)
		oSection1:Cell("F1_EMISSAO"):SetValue(STOD(T01->F1_EMISSAO))
		oSection1:Cell("F1_VOLUME1"):SetValue(T01->ZZH_QUATVL)
		oSection1:Cell("ZZH_TRANSP"):SetValue(T01->ZZH_NOMTRA)
		oSection1:Cell("F1_YFILIAL"):SetValue(T01->F1_YFILIAL)
		oSection1:Cell("F1_DAUTNFE"):SetValue(STOD(T01->F1_DAUTNFE))
		oSection1:Cell("MUNICIPIO"):SetValue(T01->A1_MUN)
		oSection1:Cell("F1_EST"):SetValue(T01->F1_EST)
		oSection1:Cell("F1_VALBRUT"):SetValue(T01->F1_VALBRUT)
		oSection1:Cell("F1_PBRUTO"):SetValue(T01->F1_PBRUTO)
		oSection1:Cell("ZZH_PEDDEV"):SetValue(T01->ZZH_PEDDEV)
		//oSection1:Cell("Filial"):SetValue(T01->E5_FILIAL)
		//oSection1:Printline()
	
		//inicializo a segunda se��o
	//	oSection1:init()
		
		
		//While T01->F1_YFILIAL == cFil .AND. T01->F1_DOC == cDoc .AND. T01->F1_SERIE == cSerie
		//	oReport:IncMeter()
			
	
		
			oSection1:Cell( "D1_COD"):SetValue(T01->D1_COD)
			oSection1:Cell( "D1_COD"):Setalign( 'center')//E2_NUMBOR
			oSection1:Cell( "B1_DESC"):SetValue( T01->B1_DESC)
			oSection1:Cell( "B1_DESC"):Setalign( 'center')
			oSection1:Cell( "D1_QUANT"):SetValue( T01->D1_QUANT )
			oSection1:Cell( "D1_QUANT"):Setalign( 'center')
			oSection1:Cell( "D1_VUNIT"):SetValue(T01->D1_VUNIT)
			oSection1:Cell( "D1_VUNIT"):Setalign( 'center')
			oSection1:Cell( "D1_CF"):SetValue(T01->D1_CF)
			oSection1:Cell( "D1_CF"):Setalign( 'center')
			oSection1:Cell( "D1_TES"):SetValue( T01->D1_TES)
			oSection1:Cell( "D1_TES"):Setalign( 'center')
			oSection1:Cell( "D1_PEDIDO"):SetValue( T01->D1_PEDIDO	)
			oSection1:Cell( "D1_PEDIDO"):Setalign( 'center')
			oSection1:Cell( "D1_PESO"):SetValue(cvaltochar(T01->B1_PESO))
			oSection1:Cell( "D1_PESO"):Setalign( 'center')//F2_TPFRETE
		
			oSection1:Printline()
				
		
		//oSection:PrintLine()
			T01->(DBSKIP())
		
		EndDo
	
		oSection1:Finish()
 		//imprimo uma linha para separar uma NCM de outra
		oReport:ThinLine()
 		//finalizo a primeira se��o
		oSection1:Finish()
	DbCloseArea("T01")
//	EndDo

RETURN


Static Function AjustaSX1(cPerg)

	PutSx1(cPerg, "01","Data De"	,						  "","","mv_ch01","D",08,0,0,"G","","","","","mv_par01","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Data Ate",						  "","","mv_ch02","D",08,0,0,"G","","","","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","Armazem de:",				  		  "","","mv_ch03","C",02,0,0,"G","","","","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "04","Armazem ate:",				      "","","mv_ch04","C",02,0,0,"G","","","","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "05","NF de:",						  "","","mv_ch05","C",09,0,0,"G","","SF2","","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "06","NF ate:",					  "","","mv_ch06","C",09,0,0,"G","","SF2","","","mv_par06"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "07","Serie de:",						  "","","mv_ch07","C",03,0,0,"G","","","","","mv_par07"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "08","Serie ate:",					  "","","mv_ch08","C",03,0,0,"G","","","","","mv_par08"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "09","Tes de:",						  "","","mv_ch09","C",03,0,0,"G","","SF4","","","mv_par09"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "10","Tes ate:",					  "","","mv_ch10","C",03,0,0,"G","","SF4","","","mv_par10"," ","","","","","","","","","","","","","","","")
	
return
//--< fim de arquivo >----------------------------------------------------------------------
