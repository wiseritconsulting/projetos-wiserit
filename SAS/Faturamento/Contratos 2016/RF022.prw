#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"
#Include "RWMAKE.CH"
#include 'parmtype.ch'
#INCLUDE "TBICONN.CH" 
#INCLUDE "TBICODE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

User Function RF022()

	Local aArea		 := GetArea()
	Local aCoors	 := FwGetDialogSize( oMainWnd )
	Local oFWLayer,oPanelTop1,oView
	Local bBlockClique
	Local aFiltros   := {}
	Private oBrowse
	Private aHistorico := {}
	Static nFilial   := 1
	Static nContra   := 2
	Static nMedia    := 3
	Static nEmissa   := 4
	Static nCodCli   := 5
	Static nNomCli   := 6
	Static nCodUse   := 7
	Static nNomUse   := 8
	Static nobs      := 9
	Static nData     := 10
	Static nHora     := 11
	Static nEtapa    := 12
	Static nNomtp    := 13
	
	Private oDlg
	Private aRotina	:= {}


	aHistorico := U_RF22ITEM(aHistorico)


	oDlg		:= MSDialog():New( aCoors[1],aCoors[2],aCoors[3],aCoors[4],"Hist�rico de Medi��o",,,.F.,,,,,,.T.,,,.T. )
	//oDlg		:= MSDialog():New( 0,0,500,700,"Planejamento e Controle de Expedi��o",,,.F.,,,,,,.T.,,,.T. )
	oFWLayer	:= FWLayer():New()
	oFWLayer:Init( oDlg, .F., .T. )
	oFWLayer:AddLine( 'TOP1', 100, .F. )
	oPanelTop1 := oFWLayer:getLinePanel( 'TOP1' )

	oBrowse	    := FWFormBrowse():New()
	oMarkBrow	:= oBrowse:FWBrowse()
	oMarkBrow:SetDataArray()
	oMarkBrow:SetArray(@aHistorico)
	oMarkBrow:SetDescription('Hist�rico')
	oMarkBrow:SetOwner( oDlg )
	oMarkBrow:SetProfileID("4")
	oMarkBrow:SetMenuDef("")
	oMarkBrow:SetDoubleClick({|| nil })


	Static nFilial   := 1
	Static nContra   := 2
	Static nMedia    := 3
	Static nEmissa   := 4
	Static nCodCli   := 5
	Static nNomCli   := 6
	Static nCodUse   := 7
	Static nNomUse   := 8
	Static nobs      := 9
	Static nData     := 10
	Static nHora     := 11
	Static nEtapa    := 12
	Static nNomtp    := 13

	// FILTROS 
	oBrowse:SetUseFilter()

	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nFilial)+"]",GetSx3Cache("ZZR_FILIAL","X3_TITULO"),"C",GetSx3Cache("ZZR_FILIAL","X3_TAMANHO"),0,GetSx3Cache("ZZR_FILIAL","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nContra)+"]",GetSx3Cache("ZZR_CONTRA","X3_TITULO"),"C",GetSx3Cache("ZZR_CONTRA","X3_TAMANHO"),0,GetSx3Cache("ZZR_CONTRA","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nMedia)+"]" ,GetSx3Cache("ZZR_MEDICA","X3_TITULO"),"C",GetSx3Cache("ZZR_MEDICA","X3_TAMANHO"),0,GetSx3Cache("ZZR_MEDICA","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nEmissa)+"]",GetSx3Cache("ZZR_EMISSA","X3_TITULO"),"D",GetSx3Cache("ZZR_EMISSA","X3_TAMANHO"),0,GetSx3Cache("ZZR_EMISSA","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nCodCli)+"]",GetSx3Cache("ZZR_CODCLI","X3_TITULO"),"C",GetSx3Cache("ZZR_CODCLI","X3_TAMANHO"),0,GetSx3Cache("ZZR_CODCLI","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nNomCli)+"]",GetSx3Cache("ZZR_NOMECL","X3_TITULO"),"C",GetSx3Cache("ZZR_NOMECL","X3_TAMANHO"),0,GetSx3Cache("ZZR_NOMECL","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nCodUse)+"]",GetSx3Cache("ZZR_CODUSE","X3_TITULO"),"C",GetSx3Cache("ZZR_CODUSE","X3_TAMANHO"),0,GetSx3Cache("ZZR_CODUSE","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nNomUse)+"]",GetSx3Cache("ZZR_NOMEUSE","X3_TITULO"),"C",GetSx3Cache("ZZR_NOMEUSE","X3_TAMANHO"),0,GetSx3Cache("ZZR_NOMEUSE","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nobs)+"]"   ,GetSx3Cache("ZZR_OBS","X3_TITULO"),"C",GetSx3Cache("ZZR_OBS","X3_TAMANHO"),0,GetSx3Cache("ZZR_OBS","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nData)+"]"  ,GetSx3Cache("ZZR_DATA","X3_TITULO"),"D",GetSx3Cache("ZZR_DATA","X3_TAMANHO"),0,GetSx3Cache("ZZR_DATA","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nHora)+"]"  ,GetSx3Cache("ZZR_HORA","X3_TITULO"),"C",GetSx3Cache("ZZR_HORA","X3_TAMANHO"),0,GetSx3Cache("ZZR_HORA","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nEtapa)+"]" ,GetSx3Cache("ZZR_ETAPA","X3_TITULO"),"C",GetSx3Cache("ZZR_ETAPA","X3_TAMANHO"),0,GetSx3Cache("ZZR_ETAPA","X3_PICTURE")} )
	Aadd( aFiltros, { "aHistorico[oBrowse:At()]["+cvaltochar(nNomtp)+"]" ,GetSx3Cache("ZZR_NOMETP","X3_TITULO"),"C",GetSx3Cache("ZZR_NOMETP","X3_TAMANHO"),0,GetSx3Cache("ZZR_NOMETP","X3_PICTURE")} )
	
	oBrowse:SetFieldFilter(aFiltros)
	
	//oMarkBrow: AddFilter ( "Local de Expedicao",aHistorico[oMarkBrow:At()][nFiliala]=='020101', .T.,,,.T.,,) 


	//filial	
	oMarkBrow:SetColumns({{;
	"Filial",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nFilial],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_FILIAL","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_FILIAL","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	// Contrato	
	oMarkBrow:SetColumns({{;
	"Contrato",;  					   				                      // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nContra],"")},;  // Code-Block de carga dos dados
	"C",;						                               			  // Tipo de dados
	"@!",;                       										  // M�scara
	1,;					   												  // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_CONTRA","X3_TAMANHO"),;                       		  // Tamanho
	GetSx3Cache("ZZR_CONTRA","X3_DECIMAL"),;                       		  // Decimal
	.F.,;                                       						  // Indica se permite a edi��o
	{||.T.},;                                   						  // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       			                              // Indica se exibe imagem
	bBlockClique,;                     			                          // Code-Block de execu��o do duplo clique
	NIL,;                                       			              // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			              // Code-Block de execu��o do clique no header
	.F.,;                                       			              // Indica se a coluna est� deletada
	.T.,;                     				   				              // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                                  // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	//Medi��o
	oMarkBrow:SetColumns({{;
	"Medi��o",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nMedia],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_MEDICA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_MEDICA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)	
	
	//Emiss�o
	oMarkBrow:SetColumns({{;
	"Emiss�o",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nEmissa],"")},;  // Code-Block de carga dos dados
	"D",;                                                        	      // Tipo de dados
	"",;                       			                                  // M�scara
	1,;					   				                                  // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_EMISSA","X3_TAMANHO"),;                        	  // Tamanho
	GetSx3Cache("ZZR_EMISSA","X3_DECIMAL"),;                       	      // Decimal
	.F.,;                                       			              // Indica se permite a edi��o
	{||.T.},;                                   			              // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                              // Indica se exibe imagem
	bBlockClique,;                     			                          // Code-Block de execu��o do duplo clique
	NIL,;                                       			              // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			              // Code-Block de execu��o do clique no header
	.F.,;                                       			              // Indica se a coluna est� deletada
	.T.,;                     				   				              // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                                  // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	// Cod CLIENTE
	oMarkBrow:SetColumns({{;
	"Cod Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nCodCli],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_CODCLI","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_CODCLI","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Nome Cliente 
	oMarkBrow:SetColumns({{;
	"Nome Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nNomCli],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_NOMECL","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_NOMECL","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Cod User
	oMarkBrow:SetColumns({{;
	"Cod Usu�rio",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nCODUSE],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_CODUSE","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_CODUSE","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Nome Usu�rio
	oMarkBrow:SetColumns({{;
	"Nome Usu�rio",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nNomUse],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                               // M�scara 
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_NOMUSE","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_NOMUSE","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	// OBS
	oMarkBrow:SetColumns({{;
	"Observa��o",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nobs],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                               // M�scara 
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_OBS","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_OBS","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	// Data
	oMarkBrow:SetColumns({{;
	"Data",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nData],"")},;  // Code-Block de carga dos dados
	"D",;                                                        	   // Tipo de dados
	"",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_DATA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_DATA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Hora 
	oMarkBrow:SetColumns({{;
	"Hora",;  					   			               // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nHora] +" "+ U_YDESTRA(aHistorico[oMarkBrow:At()][nTransp]) ,"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_HORA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_HORA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// obs
	oMarkBrow:SetColumns({{;
	"Obs",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nobsa],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_OBS","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_OBS","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	// Numero do Contrato
	oMarkBrow:SetColumns({{;
	"Numero",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nNumeroa],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_NUMERO","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_NUMERO","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	// Local Expedicao
	oMarkBrow:SetColumns({{;
	"Local Expedicao",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aHistorico), aHistorico[oMarkBrow:At()][nLocExa]+" - "+POSICIONE("SX5",1,XFILIAL("SX5")+"X2"+aHistorico[oMarkBrow:At()][nLocExa],"X5_DESCRI") ,"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_LOCEX","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_LOCEX","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// ETAPA
	oMarkBrow:SetColumns({{;
	"Etapa ",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aHistorico),aHistorico[oMarkBrow:At()][nEtapa],"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_ETAPA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_ETAPA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Nome Etapa
	oMarkBrow:SetColumns({{;
	"Desc Etapa ",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aHistorico),iif(aHistorico[oMarkBrow:At()][nNomtp]=="1","Sim","Nao"),"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZR_NOMETP","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZR_NOMETP","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	oMarkBrow:Activate()
	oDlg:Activate(,,,.T.)
	//U_RVENX006()	//ATUALIZA DADOS DO SALDO CART�O
	RestArea(aArea)

	//Reset Environment
	aHistorico := {}
Return

User Function RF22ITEM(aHistorico)

	Local cStatus := ""

	cQuery:= " SELECT ZZR_FILIAL , ZZR_CONTRA ,ZZR_MEDICA ,ZZR_EMISSA ,ZZR_CODCLI ,ZZR_NOMECL ,ZZR_CODUSE ,ZZR_NOMUSE ,ZZR_OBS ,ZZR_DATA ,ZZR_HORA ,ZZR_ETAPA , ZZR_NOMETP "
	cQuery+= " FROM "+ RETSQLNAME("ZZR") +" ZZR "
	cQuery+= " WHERE ZZR_FILIAL = '"+cFilant+"' "
	cQuery+= " AND   D_E_L_E_T_ = '' "
	cQuery+= " ORDER BY ZZR_FILIAL , ZZR_CONTRA ,ZZR_MEDICA , ZZR_ETAPA"
	TCQuery cquery new alias T01

	WHILE !T01->(EOF())
		cStatus := ""

		aadd(aHistorico,;	
		{ALLTRIM(T01->ZZR_FILIAL),;
		ALLTRIM(T01->ZZR_CONTRA),;
		ALLTRIM(T01->ZZR_MEDICA),;
		STOD(ALLTRIM(T01->ZZR_EMISSA)) ,;
		ALLTRIM(T01->ZZR_CODCLI),;
		ALLTRIM(T01->ZZR_NOMECL),;
		ALLTRIM(T01->ZZR_CODUSE),;
		ALLTRIM(T01->ZZR_NOMUSE),;
		ALLTRIM(T01->ZZR_OBS),;
		STOD(ALLTRIM(T01->ZZR_DATA)) ,;
		ALLTRIM(T01->ZZR_HORA),;
		ALLTRIM(T01->ZZR_ETAPA),;
		ALLTRIM(T01->ZZR_NOMETP)})
		//ALLTRIM(T01->ZZ2_STATOS) ,;
		T01->(dbSkip())
	ENDDO 

	T01-> (dbclosearea())

Return aHistorico