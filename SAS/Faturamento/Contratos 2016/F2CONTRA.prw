#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWMBROWSE.CH"
#include "dbtree.ch"
#include "topconn.ch"

user function F2CONTRA(cFIL,cNUM,cMEDICA,cCLT,cLJ,cFLPV,cPV,cNota,cSer,cNf)

	Local _nota := ""
	Local _serie := ""
	Local cCodKit := ""
	Local cCodPai := ""
	Local cQuery := ""
	Local cEmpres := ""
	Local cEndereco := ""
	Local cFilBkp	:= cFilAnt
	cFilAnt	:= cFLPV
	_nota := cNota
	_serie := cSer
	cEndereco := ""
	cMsgNF := ""

	
	cMsgNF:= "" //Mensagem para a Nota Fiscal

	//NF01
	cquery1 := " SELECT * "
	cquery1 += " from " + retsqlname('ZZ2') + " ZZ2 "
	cquery1 += " where  "
	cquery1 += " D_E_L_E_T_ <> '*' "
	cquery1 += " and ZZ2_FILIAL  = '"+ ALLTRIM(cFIL) +"' "
	cquery1 += " and ZZ2_NUMERO = '"+ ALLTRIM(cNUM) +"' "
	cquery1 += " and ZZ2_MEDICA = '"+ ALLTRIM(cMEDICA) +"' "
	cquery1 += " and ZZ2_CLIENT = '"+ ALLTRIM(cCLT) +"' "
	cquery1 += " and ZZ2_LOJA   = '"+ ALLTRIM(cLJ) +"' "
	cquery1 += " and ZZ2_PV01FL = '"+ ALLTRIM(cFLPV) +"' "
	cquery1 += " and ZZ2_PV01NF = '"+ ALLTRIM(cNota) +"' "
	cquery1 += " and ZZ2_SER01  = '"+ ALLTRIM(cSer) +"' "

	TCQuery cquery1 new alias T03


	IF ! T03->(EOF())  .and. Alltrim(T03->ZZ2_PV01NF) ==  ALLTRIM(cNota) .and. cNf == "NF1"
		CONOUT("NF01")
		dbSelectArea("SA1")
		SA1->(dbSetOrder(1))
		SA1->(dbSeek(xFilial("SA1")+T03->ZZ2_CLNF01+T03->ZZ2_LJNF01))

		cUFCLI:= SUBSTR(SuperGetMv( "MV_ESTADO",,, T03->ZZ2_PV01FL ),1,6)//SA1->A1_EST
		
		IF ALLTRIM(SA1->A1_ENDENT) != ""
			cEndereco := "End. Entrega: " + SA1->A1_ENDENT + " "
		ENDIF 
		
		//CNPJ(Holding), IE(Holding), (Raz�o social Holding), (N�mero da NF1), (Data da emiss�o)

		dbSelectArea("ZZZ")
		ZZZ->(dbSetOrder(1))
		If ZZZ->(dbSeek(xFilial("ZZZ")+cUFCLI))

			If Alltrim(T03->ZZ2_PV01NF) == Alltrim(cNota) .AND. (T03->ZZ2_TIPCTR) == "1" //Mensagem Nota contrato direto
				cMsgNF:= ZZZ->ZZZ_MSGPAD
			ElseIf Alltrim(T03->ZZ2_PV01NF) == Alltrim(cNota) .AND. (T03->ZZ2_TIPCTR) == "2"//Mensagem Nota 01
				cMsgNF:= ZZZ->ZZZ_MSGPAD+ZZZ->ZZZ_MSGNF1
			Elseif Alltrim(T03->ZZ2_PV02NF) == Alltrim(cNota) //Mensagem Nota 02
				cMsgNF:= ZZZ->ZZZ_MSGNF2
			Elseif Alltrim(T03->ZZ2_PV03NF) == Alltrim(cNota) //Mensagem Nota 03
				cMsgNF:= ZZZ->ZZZ_MSGNF3
			Endif
		Endif

		//nPosEmp:= at("#EMPRESA", cMsgNF) //#EMPRESA010101#

		//If nPosEmp > 0

			cEmpres := T03->ZZ2_PV03FL//substr(cMsgNF,nPosEmp+8,6)
			cCgcSA1	:= FWArrFilAtu(cEmpAnt, cEmpres)[18]
			SA1->(dbSetOrder(3))
			If SA1->(dbSeek(xFilial("SA1")+cCgcSA1))
				cMsgNF:= strtran(cMsgNF,"#CNPJ#",Alltrim(SA1->A1_CGC))
				cMsgNF:= strtran(cMsgNF,"#RAZAO#",Alltrim(SA1->A1_NOME))
				cMsgNF:= strtran(cMsgNF,"#IE#",Alltrim(SA1->A1_INSCR))
			Endif

		/*Else
			cMsgNF:= strtran(cMsgNF,"#CNPJ#","")
			cMsgNF:= strtran(cMsgNF,"#RAZAO#","")
			cMsgNF:= strtran(cMsgNF,"#IE#","")

		Endif

		cMsgNF:= strtran(cMsgNF,"#EMPRESA"+cEmpres+"#","")*/

		cMsgNF:= strtran(cMsgNF,"#NF1#",Alltrim(cNota))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO1#",DTOC(POSICIONE("SF2",1,T03->ZZ2_PV01FL + T03->ZZ2_PV01NF + T03->ZZ2_SER01 + T03->ZZ2_CLNF01 + T03->ZZ2_LJNF01  ,"F2_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF2#",Alltrim(T03->ZZ2_PV02NF))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO2#",DTOC(POSICIONE("SF2",1,T03->ZZ2_PV02FL + T03->ZZ2_PV02NF + T03->ZZ2_SER02 + T03->ZZ2_CLNF02 + T03->ZZ2_LJNF02 ,"F2_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF3#",Alltrim(T03->ZZ2_PV03NF))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO3#",DTOC(POSICIONE("SF2",1,T03->ZZ2_PV03FL + T03->ZZ2_PV03NF + T03->ZZ2_SER03 + T03->ZZ2_CLNF03 + T03->ZZ2_LJNF03 ,"F2_EMISSAO")))

		DBSELECTAREA("SF2")
		DBSETORDER(1)
		DBSEEK(cFLPV + _nota + _serie)

		If Found()

			RECLOCK("SF2",.F.)

			SF2->F2_YFILIAL := T03->ZZ2_PV01FL
			SF2->F2_YCONTRA := T03->ZZ2_NUMERO
			SF2->F2_YMEDICA := T03->ZZ2_MEDICA
			SF2->F2_YMSGNF 	:= cEndereco +  cMsgNF

			MSUNLOCK()

		EndIf
	ENDIF

	T03->(DBCLOSEAREA())

	//NF02
	cquery1 := " SELECT * "
	cquery1 += " from " + retsqlname('ZZ2') + " ZZ2 "
	cquery1 += " where  "
	cquery1 += " D_E_L_E_T_ <> '*' "
	cquery1 += " and ZZ2_FILIAL  = '"+ ALLTRIM(cFIL) +"' "
	cquery1 += " and ZZ2_NUMERO = '"+ ALLTRIM(cNUM) +"' "
	cquery1 += " and ZZ2_MEDICA = '"+ ALLTRIM(cMEDICA) +"' "
	cquery1 += " and ZZ2_CLIENT = '"+ ALLTRIM(cCLT) +"' "
	cquery1 += " and ZZ2_LOJA   = '"+ ALLTRIM(cLJ) +"' "
	cquery1 += " and ZZ2_PV02FL = '"+ ALLTRIM(cFLPV) +"' "
	cquery1 += " and ZZ2_PV02NF = '"+ ALLTRIM(cNota) +"' "
	cquery1 += " and ZZ2_SER02  = '"+ ALLTRIM(cSer) +"' "

	TCQuery cquery1 new alias T03


	IF ! T03->(EOF()) .AND. Alltrim(T03->ZZ2_PV02NF) ==  ALLTRIM(cNota)  .and. cNf == "NF2"  .AND. T03->ZZ2_TIPCTR = "2"
		CONOUT("NF02")
		dbSelectArea("SA1")
		SA1->(dbSetOrder(1))
		SA1->(dbSeek(xFilial("SA1")+T03->ZZ2_CLNF02+T03->ZZ2_LJNF02))

		cUFCLI:= SUBSTR(SuperGetMv( "MV_ESTADO",,, T03->ZZ2_PV02FL ),1,6)//SA1->A1_EST

		//CNPJ(Holding), IE(Holding), (Raz�o social Holding), (N�mero da NF1), (Data da emiss�o)

		dbSelectArea("ZZZ")
		ZZZ->(dbSetOrder(1))
		If ZZZ->(dbSeek(xFilial("ZZZ")+cUFCLI))

			If Alltrim(T03->ZZ2_PV01NF) == Alltrim(cNota) //Mensagem Nota 01
				cMsgNF:= ZZZ->ZZZ_MSGPAD+ZZZ->ZZZ_MSGNF1
			Elseif Alltrim(T03->ZZ2_PV02NF) == Alltrim(cNota) //Mensagem Nota 02
				cMsgNF:= ZZZ->ZZZ_MSGNF2
			Elseif Alltrim(T03->ZZ2_PV03NF) == Alltrim(cNota) //Mensagem Nota 03
				cMsgNF:= ZZZ->ZZZ_MSGNF3
			Endif
		Endif

		//nPosEmp:= at("#EMPRESA", cMsgNF) //#EMPRESA010101#

		//If nPosEmp > 0

			cEmpres:= T03->ZZ2_PV03FL//substr(cMsgNF,nPosEmp+8,6)
			cCgcSA1	:= FWArrFilAtu(cEmpAnt, cEmpres)[18]
			SA1->(dbSetOrder(3))
			If SA1->(dbSeek(xFilial("SA1")+cCgcSA1))
				cMsgNF:= strtran(cMsgNF,"#CNPJ#",Alltrim(SA1->A1_CGC))
				cMsgNF:= strtran(cMsgNF,"#RAZAO#",Alltrim(SA1->A1_NOME))
				cMsgNF:= strtran(cMsgNF,"#IE#",Alltrim(SA1->A1_INSCR))
			Endif
/*
		Else
			cMsgNF:= strtran(cMsgNF,"#CNPJ#","")
			cMsgNF:= strtran(cMsgNF,"#RAZAO#","")
			cMsgNF:= strtran(cMsgNF,"#IE#","")

		Endif

		cMsgNF:= strtran(cMsgNF,"#EMPRESA"+cEmpres+"#","")*/

		cMsgNF:= strtran(cMsgNF,"#NF1#",Alltrim(T03->ZZ2_PV01NF))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO1#",DTOC(POSICIONE("SF2",1,T03->ZZ2_PV01FL + T03->ZZ2_PV01NF + T03->ZZ2_SER01 + T03->ZZ2_CLNF01 + T03->ZZ2_LJNF01  ,"F2_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF2#",Alltrim(T03->ZZ2_PV02NF))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO2#",DTOC(POSICIONE("SF2",1,T03->ZZ2_PV02FL + T03->ZZ2_PV02NF + T03->ZZ2_SER02 + T03->ZZ2_CLNF02 + T03->ZZ2_LJNF02 ,"F2_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF3#",Alltrim(T03->ZZ2_PV03NF))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO3#",DTOC(POSICIONE("SF2",1,T03->ZZ2_PV03FL + T03->ZZ2_PV03NF + T03->ZZ2_SER03 + T03->ZZ2_CLNF03 + T03->ZZ2_LJNF03 ,"F2_EMISSAO")))



		DBSELECTAREA("SF2")
		DBSETORDER(1)
		DBSEEK(cFLPV + _nota + _serie)

		If Found()

			RECLOCK("SF2",.F.)

			SF2->F2_YFILIAL := T03->ZZ2_PV02FL
			SF2->F2_YCONTRA := T03->ZZ2_NUMERO
			SF2->F2_YMEDICA := T03->ZZ2_MEDICA
			SF2->F2_YMSGNF 	:= cMsgNF

			MSUNLOCK()

		EndIf
	ENDIF

	T03->(DBCLOSEAREA())

	//NF03
	cquery1 := " SELECT * "
	cquery1 += " from " + retsqlname('ZZ2') + " ZZ2 "
	cquery1 += " where  "
	cquery1 += " D_E_L_E_T_ <> '*' "
	cquery1 += " and ZZ2_FILIAL  = '"+ ALLTRIM(cFIL) +"' "
	cquery1 += " and ZZ2_NUMERO = '"+ ALLTRIM(cNUM) +"' "
	cquery1 += " and ZZ2_MEDICA = '"+ ALLTRIM(cMEDICA) +"' "
	cquery1 += " and ZZ2_CLIENT = '"+ ALLTRIM(cCLT) +"' "
	cquery1 += " and ZZ2_LOJA   = '"+ ALLTRIM(cLJ) +"' "
	cquery1 += " and ZZ2_PV03FL = '"+ ALLTRIM(cFLPV) +"' "
	cquery1 += " and ZZ2_PV03NF = '"+ ALLTRIM(cNota) +"' "
	cquery1 += " and ZZ2_SER03  = '"+ ALLTRIM(cSer) +"' "

	TCQuery cquery1 new alias T03


	IF ! T03->(EOF()) .AND.  alltrim(T03->ZZ2_PV03NF) ==  ALLTRIM(cNota) .and. cNf == "NF3" .AND. T03->ZZ2_TIPCTR = "2"
		CONOUT("NF03")
		dbSelectArea("SA1")
		SA1->(dbSetOrder(1))
		SA1->(dbSeek(xFilial("SA1")+T03->ZZ2_CLNF03+T03->ZZ2_LJNF03))

		cUFCLI:= SUBSTR(SuperGetMv( "MV_ESTADO",,, T03->ZZ2_PV03FL ),1,6)//SA1->A1_EST
		cEndereco := "End. Entrega: "  +  SA1->A1_ENDENT + " "
		//CNPJ(Holding), IE(Holding), (Raz�o social Holding), (N�mero da NF1), (Data da emiss�o)

		dbSelectArea("ZZZ")
		ZZZ->(dbSetOrder(1))
		If ZZZ->(dbSeek(xFilial("ZZZ")+cUFCLI))

			If Alltrim(T03->ZZ2_PV01NF) == Alltrim(cNota) //Mensagem Nota 01
				cMsgNF:= ZZZ->ZZZ_MSGPAD+ZZZ->ZZZ_MSGNF1
			Elseif Alltrim(T03->ZZ2_PV02NF) == Alltrim(cNota) //Mensagem Nota 02
				cMsgNF:= ZZZ->ZZZ_MSGNF2
			Elseif Alltrim(T03->ZZ2_PV03NF) == Alltrim(cNota) //Mensagem Nota 03
				cMsgNF:= ZZZ->ZZZ_MSGNF3
			Endif
		Endif

		//nPosEmp:= at("#EMPRESA", cMsgNF) //#EMPRESA010101#

		//If nPosEmp > 0
			cEmpres:= T03->ZZ2_PV01FL//substr(cMsgNF,nPosEmp+8,6)
			cCgcSA1	:= FWArrFilAtu(cEmpAnt, cEmpres)[18]
			SA1->(dbSetOrder(3))
			If SA1->(dbSeek(xFilial("SA1")+cCgcSA1))
				cMsgNF:= strtran(cMsgNF,"#CNPJ#",Alltrim(SA1->A1_CGC))
				cMsgNF:= strtran(cMsgNF,"#RAZAO#",Alltrim(SA1->A1_NOME))
				cMsgNF:= strtran(cMsgNF,"#IE#",Alltrim(SA1->A1_INSCR))
			Endif
/*
		Else
			cMsgNF:= strtran(cMsgNF,"#CNPJ#","")
			cMsgNF:= strtran(cMsgNF,"#RAZAO#","")
			cMsgNF:= strtran(cMsgNF,"#IE#","")

		Endif

		cMsgNF:= strtran(cMsgNF,"#EMPRESA"+cEmpres+"#","")*/

		cMsgNF:= strtran(cMsgNF,"#NF1#",Alltrim(T03->ZZ2_PV01NF))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO1#",DTOC(POSICIONE("SF2",1,T03->ZZ2_PV01FL + T03->ZZ2_PV01NF + T03->ZZ2_SER01 + T03->ZZ2_CLNF01 + T03->ZZ2_LJNF01  ,"F2_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF2#",Alltrim(T03->ZZ2_PV02NF))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO2#",DTOC(POSICIONE("SF2",1,T03->ZZ2_PV02FL + T03->ZZ2_PV02NF + T03->ZZ2_SER02 + T03->ZZ2_CLNF02 + T03->ZZ2_LJNF02 ,"F2_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF3#",Alltrim(T03->ZZ2_PV03NF))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO3#",DTOC(POSICIONE("SF2",1,T03->ZZ2_PV03FL + T03->ZZ2_PV03NF + T03->ZZ2_SER03 + T03->ZZ2_CLNF03 + T03->ZZ2_LJNF03 ,"F2_EMISSAO")))

		//POSICIONE("SF2",1,T03->ZZ2_PV01FL + T03->ZZ2_PV01NF + T03->ZZ2_SER01+ T03->ZZ2_CLNF01 + T03->ZZ2_LJNF01  ,"F2_EMISSAO")


		DBSELECTAREA("SF2")
		DBSETORDER(1)
		DBSEEK(cFLPV + _nota + _serie)

		If Found()

			RECLOCK("SF2",.F.)

			SF2->F2_YFILIAL := T03->ZZ2_PV03FL
			SF2->F2_YCONTRA := T03->ZZ2_NUMERO
			SF2->F2_YMEDICA := T03->ZZ2_MEDICA
			SF2->F2_YMSGNF 	:= cEndereco + cMsgNF

			MSUNLOCK()

		EndIf
	ENDIF

	T03->(DBCLOSEAREA())

	cFilAnt	:= cFilBkp
REturn
