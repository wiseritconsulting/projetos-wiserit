#include 'protheus.ch'
#include "topconn.ch"
#include 'parmtype.ch'

/*/{Protheus.doc} SASP040
Fun��o chamada para verifica��o da sequencia no cadastro de Kits
@author Diogo
@since 16/09/2015
@version version
@example
(examples)
@see (links_or_references)
/*/
User function SASP040(cCod,cTipo)

	Local aArea:= getArea()
	Local cSecatual := ZZ7->ZZ7_SEQ
	Local lInclui := Type("INCLUI")<>"U".and.INCLUI

	if ! lInclui	
		RestArea(aArea)
		Return cSecatual
	endif
	
	//cQuery := " SELECT COUNT(*)+1 AS SEQ "
	cQuery := " SELECT TOP 1 substring(ZZ7_CODIGO,len(ltrim(ZZ7_CODPAI))+1,15)+1 AS SEQ ,CAST(substring(ZZ7_CODIGO,len(ltrim(ZZ7_CODPAI))+1,15) AS INT) AS COD  "
	cQuery += " FROM   "+RetSqlName( 'ZZ7' ) + " ZZ7 " "
	cQuery += " WHERE  ZZ7.D_E_L_E_T_='' "
	cQuery += " AND    ZZ7_CODPAI = '"+cCod+"' "
	cQuery += " AND    ZZ7_TIPO = '"+cTipo+"' "
	cQuery += " order by COD DESC "

	TCQuery cQuery new alias TZZ7

	IF !TZZ7->(EOF())
		M->ZZ7_SEQ := cValtochar(TZZ7->SEQ)
		IF len(ALLTRIM(M->ZZ7_SEQ)) == 1
			M->ZZ7_SEQ := "0"+ALLTRIM(M->ZZ7_SEQ)
		ENDIF
	ELSE
		M->ZZ7_SEQ := "01"
	ENDIF	

	M->ZZ7_CODIGO := alltrim(M->ZZ7_CODPAI)+alltrim(M->ZZ7_SEQ)

	TZZ7->(DBCLOSEAREA())

	RestArea(aArea)

Return M->ZZ7_SEQ