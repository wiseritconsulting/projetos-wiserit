#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"
#INCLUDE "RWMAKE.CH"
#include "dbtree.ch"

USER FUNCTION SASP032()

	LOCAL cAlias := "ZZ7"
	//LOCAL cAreaAnt := GETAREA()
	PRIVATE cCadastro := "CADASTRO KITS"
	PRIVATE aRotina     := { }
	Public  lSair := .F.

	//aCores :=   {{'ZP3_STATUS == "1"' , 'BR_VERMELHO' },;
		//            {'ZP3_STATUS <> "1"' , 'BR_VERDE'      }}


	AADD(aRotina, { "Pesquisar" , "AxPesqui"  , 0, 1 })
	AADD(aRotina, { "Visualizar", "U_SASP034()"  , 0, 2 })
	//AADD(aRotina, { "Legenda" , "U_Leadleg()" , 0, 3 })
	AADD(aRotina, { "Alterar"   , "U_SASP034()"  , 0, 4 })
	AADD(aRotina, { "incluir"   , "U_SASP034()"  , 0, 3 })
	AADD(aRotina, { "Excluir"   , "U_SASP034()"  , 0, 5 })


	dbSelectArea(cAlias)

	dbSetOrder(1)

	U_SASP034()

	IF lSair
		RETURN  NIL
	ENDIF

	//mBrowse(6,1,22,75,cAlias,,,,,6,aCores)
	//mBrowse(6,1,22,75,cAlias,,,,,6,)

RETURN NIL



