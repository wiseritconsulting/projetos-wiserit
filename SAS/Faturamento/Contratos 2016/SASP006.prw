//Bibliotecas
#Include 'Protheus.ch'
#Include 'FWMVCDef.ch'
#include "topconn.ch"

User Function SASP006(Filial,Item,Numero,Cliente,Loja,Quantidade)

	Local aArea := getarea()
	Local nNumero := 0
	Local oBrowse
	Local aCoors	 := FwGetDialogSize( oMainWnd )
	Local oFWLayer,oPanelTop1,oView
	Local bBlockClique
	Local nE :=1
	
	private cFil		:= Filial
	private cItem		:= Item
	private cNumero		:= Numero
	private cCli		:= Cliente
	private cLoja		:= Loja
	
	Private aTitulos := {}
	Private oDlg
	Private nTotal := 0
	Private oGet2 := ""
	Static nFilial   := 1
	Static nItem   := 2
	Static nNum    := 3
	Static nCliente   := 4
	Static nLoja   := 5
	Static nTipo   := 6
	Static nTipoA	:= 7
	Static nQuant   := 8
	Static nVlrADT   := 9
	Static nValor   := 10
	Static nTitulo   := 11
	Static nTipoTi   := 12
	Static nUser   := 13
	Static nDTHora   := 14
	Static nok := 8

	aTitulos := RTITULB(aTitulos)

	cQuery := " SELECT SUM(CASE Z10_TIPO WHEN 'A' THEN Z10_QUANT ELSE Z10_QUANT * - 1 END) QUANTIDADE FROM Z10010 (NOLOCK) "
	cQuery += " WHERE D_E_L_E_T_ = ''  "
	cQuery += " AND Z10_FILIAL = '"+cFil+"' "
	cQuery += " AND Z10_NUMERO = '"+cNumero+"' "
	cQuery += " AND Z10_CLIENT = '"+cCli+"' "
	cQuery += " AND Z10_LOJA = '"+cLoja+"' "
	cQuery += " AND Z10_ITEMZ6 = '"+cItem+"' "
	TcQuery cQuery new Alias T54
	
	nNumero := quantidade + T54->QUANTIDADE
	
	oDlg		:= MSDialog():New( aCoors[1],aCoors[2],aCoors[3],aCoors[4],"Aglutinar titulos",,,.F.,,,,,,.T.,,,.T. )
	//oDlg		:= MSDialog():New( 0,0,500,700,"Planejamento e Controle de Expedicao",,,.F.,,,,,,.T.,,,.T. )/

	@275,10 SAY oSay1 PROMPT "Qnt Original:" SIZE 50,30 OF oDlg PIXEL
	@270,40 GET oGet2 VAR quantidade PICTURE GetSx3Cache("E1_VALOR","X3_PICTURE") SIZE 80, 12 WHEN .F. OF oDlg  PIXEL
	
	@275,120 SAY oSay1 PROMPT "Qnt Aditivada:" SIZE 50,30 OF oDlg PIXEL
	@270,160 GET oGet2 VAR T54->QUANTIDADE PICTURE GetSx3Cache("E1_VALOR","X3_PICTURE") SIZE 80, 12 WHEN .F. OF oDlg  PIXEL
	
	@275,250 SAY oSay1 PROMPT "Qnt Atual :" SIZE 50,30 OF oDlg PIXEL
	@270,290 GET oGet2 VAR nNumero PICTURE GetSx3Cache("E1_VALOR","X3_PICTURE") SIZE 80, 12 WHEN .F. OF oDlg  PIXEL

	oFWLayer	:= FWLayer():New()
	oFWLayer:Init( oDlg, .F., .T. )
	oFWLayer:AddLine( 'TOP1', 90, .F. )
	oPanelTop1 := oFWLayer:getLinePanel( 'TOP1' )

	oBrowse	    := FWFormBrowse():New()
	oMarkBrow	:= oBrowse:FWBrowse()
	oMarkBrow:SetDataArray()
	oMarkBrow:lAdjust := .F.
	oMarkBrow:SetArray(@aTitulos)
	oMarkBrow:SetDescription('Aglutinacao')
	oMarkBrow:SetOwner( oPanelTop1 )
	oMarkBrow:SetProfileID("4")
	oMarkBrow:SetMenuDef("")
	oMarkBrow:SetDoubleClick({|| nil })

	bBlockClique := {|| CLICKDB() }


	oMarkBrow:AddButton("Sair",{||oDlg:End()},,,)//aButtons

	// legenda
	//oMarkBrow:AddLegend({||aTitulos[oMarkBrow:At()][nNF01]  == "" }  ,"BR_PRETO",     "Medicao nao Faturada")
	//oMarkBrow:AddLegend({||aTitulos[oMarkBrow:At()][nNF01]  <> "" }  ,"BR_VERDE",     "Medicao Faturada")
	//----------------------------------------------------------------------------------------------------------------



	//Filial		
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_FILIAL","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nFilial],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_FILIAL","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_FILIAL","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_FILIAL","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_FILIAL","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};
	)

	//Prefixo		
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_ITEM","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nItem],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_ITEM","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_ITEM","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_ITEM","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_ITEM","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};
	)

	//Numero		
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_NUMERO","X3_TITULO"),;  					   			                       // Tiiulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nNum],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_NUMERO","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_NUMERO","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_NUMERO","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_NUMERO","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};
	)

	//Parcela		
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_CLIENT","X3_TITULO"),;  					   			                       // Tiiulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nCliente],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_CLIENT","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_CLIENT","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_CLIENT","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_CLIENT","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcoes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)

	//Tipo		
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_LOJA","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nLoja],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_LOJA","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_LOJA","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_LOJA","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_LOJA","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcoes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)

	//Vencto	
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_TIPO","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| IIF(alltrim(aTitulos[oMarkBrow:At()][nTipo]) == 'A',"ACRESCIMO",IIF(alltrim(aTitulos[oMarkBrow:At()][nTipo]) == 'D',"DECRESCIMO","NOVO PRODUTO"))},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_TIPO","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_TIPO","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_TIPO","X3_TAMANHO") + 20,;                       	                      // Tamanho
	GetSx3Cache("Z10_TIPO","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcoes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)
	
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_TPADT","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| IIF(alltrim(aTitulos[oMarkBrow:At()][nTipoA]) == 'Q',"QUANTIADDE",IIF(alltrim(aTitulos[oMarkBrow:At()][nTipoA]) == 'V',"VALOR","QUANTIDADE E VALOR"))},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_TPADT","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_TPADT","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_TPADT","X3_TAMANHO") + 20,;                       	                      // Tamanho
	GetSx3Cache("Z10_TPADT","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcoes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)
	

	//Valor
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_QUANT","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nQuant],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_QUANT","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_QUANT","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_QUANT","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_QUANT","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execcao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcaes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)
	
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_VLRADT","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nVlrADT],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_VLRADT","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_VLRADT","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_VLRADT","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_VLRADT","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execcao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcaes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)
	
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_VALOR","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nValor],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_VALOR","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_VALOR","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_VALOR","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_VALOR","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execcao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcaes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_TITULO","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nTitulo],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_TITULO","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_TITULO","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_TITULO","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_TITULO","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execcao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcaes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_TIPOTI","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nTipoTi],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_TIPOTI","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_TIPOTI","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_TIPOTI","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_TIPOTI","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execcao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcaes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_DTHORA","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nUser],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_DTHORA","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_DTHORA","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_DTHORA","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_DTHORA","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execcao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcaes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)
	oMarkBrow:SetColumns({{;
	GetSx3Cache("Z10_USUARI","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nDTHora],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("Z10_USUARI","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("Z10_USUARI","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("Z10_USUARI","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("Z10_USUARI","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execcao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcaes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)



	oMarkBrow:SetMenuDef( 'DEV03' )	 
	oMarkBrow:Activate()	
	oDlg:Activate(,,,.T.)	

	RestArea(aArea)

	//Reset Environment
	aTitulos := {}
	T54->(DbCloseArea())
Return

//Inclusao dos dados nos vetor
Static function RTITULB(aTitulos)

	Local aTit2 := {}
	Local cQuery := ""

	cQuery := " SELECT * FROM Z10010 (NOLOCK) "
	cQuery += " WHERE D_E_L_E_T_ = ''  "
	cQuery += " AND Z10_FILIAL = '"+cFil+"' "
	cQuery += " AND Z10_NUMERO = '"+cNumero+"' "
	cQuery += " AND Z10_CLIENT = '"+cCli+"' "
	cQuery += " AND Z10_LOJA = '"+cLoja+"' "
	cQuery += " AND Z10_ITEMZ6 = '"+cItem+"' "
	TcQuery cQuery New Alias XE1

	While XE1->(!Eof())

		aTit2:= {}

		aADD(aTit2,XE1->Z10_FILIAL)
		aADD(aTit2,XE1->Z10_ITEM)
		aADD(aTit2,XE1->Z10_NUMERO)
		aADD(aTit2,XE1->Z10_CLIENT)
		aADD(aTit2,XE1->Z10_LOJA)
		aADD(aTit2,XE1->Z10_TIPO)
		aADD(aTit2,XE1->Z10_TPADT)
		aADD(aTit2,XE1->Z10_QUANT)
		aADD(aTit2,XE1->Z10_VLRADT)
		aADD(aTit2,XE1->Z10_VALOR)
		aADD(aTit2,XE1->Z10_TITULO)
		aADD(aTit2,XE1->Z10_TIPOTI)
		aADD(aTit2,XE1->Z10_DTHORA)
		aADD(aTit2,XE1->Z10_USUARI)
		AaDD(aTit2,"")

		aAdd(aTitulos,aTit2)
		XE1->(DBSKIP())
	EndDo

	XE1->(DBCLOSEAREA())

return aTitulos

Static Function CLICKDB()

	IF aTitulos[oMarkBrow:At()][nOk]==""

		aTitulos[oMarkBrow:At()][nOk]:= "1"
		nTotal := nTotal + aTitulos[oMarkBrow:At()][nValor]

	Else
		aTitulos[oMarkBrow:At()][nOk] := ""
		nTotal := nTotal - aTitulos[oMarkBrow:At()][nValor]
	EndIf

Return

Static Function YATUALIZ(aTitulos,oMarkBrow)


	aTitulos:= {}
	RTITULB(aTitulos)
	oMarkBrow:SetArray(@aTitulos)
	oMarkBrow:refresh()
	oBrowse:refresh()
	oMarkBrow:goto(1,.T.)

Return

// FUNcaO PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO MARCADO
Static Function YINVERTE(oMarkBrow)

	For nAx := 1 to len(aTitulos)

		If aTitulos[nAx][nOK] == "1"
			aTitulos[nAx][nOK] := ""
			nTotal := nTotal - aTitulos[nAx][nValor]
		Else
			aTitulos[nAx][nOK] := "1"
			nTotal := nTotal + aTitulos[nAx][nValor]
		EndIf
	Next

	oGet2:Refresh()
	oMarkBrow:refresh()
	oDlg:Refresh()

Return

Static Function CriaVetor(cAliasT)

	Local _cAlias := cAliasT
	Local nUsado  := 0
	Local aCpoVis	:= {}
	Local aHeadT := {}
	Local nPos := 1

	aHeadT	:= {}

	DbSelectArea(_cAlias)
	RegToMemory(_cAlias, .T., .F. )

	//
	// Montagem do aHeadTer                                  
	//
	dbSelectArea("SX3")
	SX3->(dbSetOrder(1))
	MsSeek(_cAlias)
	Do While ( SX3->(!Eof()) .And. (SX3->X3_ARQUIVO == _cAlias) )
		If ( X3USO(SX3->X3_USADO) .And.	cNivel >= SX3->X3_NIVEL )
			nUsado++
			Aadd(aHeadT,{ TRIM(X3Titulo()),;
			SX3->X3_CAMPO,;
			SX3->X3_PICTURE,;
			SX3->X3_TAMANHO,;
			SX3->X3_DECIMAL,;
			"",;
			SX3->X3_USADO,;
			SX3->X3_TIPO,;
			SX3->X3_F3,;
			SX3->X3_CONTEXT,;
			nPos} )

			nPos++
		EndIf

		SX3->(dbSkip())
	EndDo

Return aHeadT