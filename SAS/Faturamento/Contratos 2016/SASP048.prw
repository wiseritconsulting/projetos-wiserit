#include 'protheus.ch'
#include 'parmtype.ch'

user function SASP048()
	
Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.

Private cString := "ZZM"

dbSelectArea("ZZM")
dbSetOrder(1)

AxCadastro(cString,"Serie x Envio x Data",cVldExc,cVldAlt)

Return