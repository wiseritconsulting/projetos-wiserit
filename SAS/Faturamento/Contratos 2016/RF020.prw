#Include 'Protheus.ch'
#Include 'Topconn.ch'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} RF020
Tela de Acompanhamento de Entrega
@type function
@author Diogo 
@since 23/10/2015
@version 1.0
/*/

User Function RF020()
	Local aColumns
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Private aDados	:= {}
	Private oBrowse := {}
	Private aCampSX3	:= {{"ZZ2_FILIAL"};
		,{"ZZ2_PV01NF"};
		,{"ZZ2_SER01"};
		,{"F2_EMISSAO","POSICIONE('SF2',1,XFILIAL('SF2',ZZ2->ZZ2_PV01FL)+ZZ2->ZZ2_PV01NF + ZZ2->ZZ2_SER01,'F2_EMISSAO')"};//TODO INCLUIR DATA DA EMISSAO
		,{"ZZ2_PV03NF"};
		,{"ZZ2_SER03"};
		,{"C5_FRETE","POSICIONE('SC5',1,xFilial('SC5',ZZ2->ZZ2_PV03FL)+ZZ2->ZZ2_PV03,'C5_FRETE')"};
		,{"F2_EMISSAO","POSICIONE('SF2',1,XFILIAL('SF2',ZZ2->ZZ2_PV03FL)+ZZ2->ZZ2_PV03NF + ZZ2->ZZ2_SER03,'F2_EMISSAO')"};//TODO INCLUIR DATA DA EMISSAO
		,{"ZZ2_TIPO"};
		,{"ZZG_DESCRI","POSICIONE('ZZG',1,XFILIAL('ZZG') + ZZ2->ZZ2_TIPO, 'ZZG->ZZG_DESCRI' )"};
		,{"A1_END","POSICIONE('SA1',1,xFilial('SA1',ZZ2->ZZ2_FILIAL)+ZZ2->(ZZ2_CLIENT+ZZ2_LOJA),'A1_END')"};
		,{"ZZ3_TES","POSICIONE('ZZ3',1,xFilial('ZZ3',ZZ2->ZZ2_FILIAL)+ZZ2->(ZZ2_NUMERO+ZZ2_MEDICA),'ZZ3_TES')"};
		,{"F4_TEXTO","POSICIONE('SF4',1,xFilial('SF4',ZZ2->ZZ2_FILIAL)+ZZ3->ZZ3_TES,'F4_TEXTO')"};
		,{"ZZ2_ETAPA","POSICIONE('SX5',1,xFilial('SX5',ZZ2->ZZ2_FILIAL)+'ZA'+ZZ2->ZZ2_ETAPA,'X5_DESCRI')"};
		,{"ZZ2_STACOM","POSICIONE('SX5',1,xFilial('SX5',ZZ2->ZZ2_FILIAL)+'ZB'+ZZ2->ZZ2_STACOM,'X5_DESCRI')"};		
		,{"ZZ2_OBSACO"};
		,{"C5_VOLUME1","U_VZZQTOT()"};
		,{"ZZ3_VTOTAL","U_VZZ2TOT()"}; 
		,{"ZZ2_DTPREV"};
		,{"ZZ2_STATUS"};
		,{"ZZ2_MEDICA"};
		,{"ZZ2_DTGER"};
		,{"ZZ2_CLIENT"};
		,{"ZZ2_LOJA"};
		,{"ZZ2_NOME"};
		,{"ZZ2_DTSOLIC"};
		,{"ZZ2_DTMAX"};
		,{"ZZ2_DTEXP"};
		,{"NDIAS","ZZ2->ZZ2_DTMAX-DDATABASE","Dias","N",3,0,""};
		,{"ZZ2_TRANSP"};
		,{"A4_NOME","POSICIONE('SA4',1,xFilial('SA4')+ZZ2->ZZ2_TRANSP,'SA4->A4_NOME')"};
		,{"ZZ2_VALFRT"};
		,{"ZZ2_TABFRT","IIF(ZZ2->ZZ2_TABFRT == '1','EXPRESSO','CONVENCIONAL')"};
		,{"ZZ2_NUMERO"};
		,{"A1_MUN","POSICIONE('SA1',1,xFilial('SA1',ZZ2->ZZ2_FILIAL)+ZZ2->(ZZ2_CLIENT+ZZ2_LOJA),'A1_MUN')"};
		,{"A1_EST","POSICIONE('SA1',1,xFilial('SA1')+ZZ2->(ZZ2_CLIENT+ZZ2_LOJA),'A1_EST')"};
		,{"ZZ2_DTENTR"};
		,{"ZZ2_MODAL"};
		,{"ZZ2_LOCEX","ZZ2->ZZ2_LOCEX+'-'+POSICIONE('SX5',1,xFilial('SX5',ZZ2->ZZ2_FILIAL)+'X2'+ZZ2->ZZ2_LOCEX,'X5_DESCRI')",,,20}}
		
	oDlg := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Tela de Acompanhamento de Entrega',,,,nOr(WS_VISIBLE,WS_POPUP),,,,,.T.,,,,)
	oBrowForm	:= FWFormBrowse():New()
	oBrowse := oBrowForm:FWBrowse()
	oBrowse:SetDataTable(.T.)
	oBrowse:SetAlias("ZZ2")
	oBrowse:SetSeek()
	oBrowse:SetDescription('Tela de Acompanhamento de Entrega')
	oBrowse:DisableDetails()
	oBrowse:SetUseFilter(.T.)
	oBrowse:SetDBFFilter(.T.)
	
	bAtualiza	:= {|| oBrowse:Refresh(),oBrowse:GoTop() }
	nTempo:= SuperGetMv("SA_TMPREF",.F.,30)*1000 
	oTimer:= TTimer():New(nTempo,bAtualiza,oDlg)
	oTimer:Activate()
	
	
	oBrowse:SetMenuDef( 'RF020' )
	
	oBrowse:AddButton('Hist�rico OS', 'U_HISTRF020()', , 4, 0)	
	oBrowse:AddButton("Alterar Previs�o",{||ALTPREVISAO()},,,)
	
	oBrowse:AddLegend( " ZZ2->ZZ2_DTMAX-DDATABASE >=  0 " , "BR_VERDE", 'NO PRAZO' )
	
	//Legenda
	dbSelectArea("ZZS")
	ZZS->(dbGotop())
	ZZS->(dbSetOrder(1))
	while ZZS->(!eof())
		
		oBrowse:AddLegend( 	"ZZ2->ZZ2_DTMAX-DDATABASE <  0 .AND. "+;
							"abs(ZZ2->ZZ2_DTMAX-DDATABASE) >= "+cValtochar(ZZS->ZZS_ATRDE)+" .AND."+;
							"abs(ZZ2->ZZ2_DTMAX-DDATABASE) <= "+cValtochar(ZZS->ZZS_ATRATE),+;
							fLegZZS(ZZS->ZZS_LEGEND), ZZS->ZZS_DESCR  )

		ZZS->(dbSkip())
	enddo

	oBrowse:AddFilter("ATRASADOS "," ZZ2->ZZ2_DTMAX < DDATABASE " )

	aColumns	:= DefCampos()
	oBrowse:SetColumns(aColumns)

	oBrowse:SetFilterDefault( "ZZ2_PV01NF <>' ' " ) //Somente Faturados	
	
	oBrowForm:Activate(oDlg)
	oDlg:Activate(,,,.T.)

Return

Static Function DefCampos()
	Local aColumns	:= {}
	Local aColumn
	SX3->(DbSetOrder(2))	//X3_CAMPO
	For nCont:=1 to Len(aCampSX3)
		If SX3->(DbSeek(PADR(aCampSX3[nCont][1],10)))
			aColumn := {;
				GetCamDef(aCampSX3[nCont],3,,GetSx3Cache(aCampSX3[nCont][1],"X3_TITULO"));				//[n][01] T�tulo da coluna
				,&("{|| "+GetCamDef(aCampSX3[nCont],2,,GetTabDados(aCampSX3[nCont]))+"}");				//[n][02] Code-Block de carga dos dados
				,GetSx3Cache(aCampSX3[nCont][1],"X3_TIPO");					//[n][03] Tipo de dados
				,GetSx3Cache(aCampSX3[nCont][1],"X3_PICTURE");				//[n][04] M�scara
				,If(GetSx3Cache(aCampSX3[nCont][1],"X3_TIPO")=="N",2,1);	//[n][05] Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				,GetCamDef(aCampSX3[nCont],5,,GetSx3Cache(aCampSX3[nCont][1],"X3_TAMANHO"));				//[n][06] Tamanho
				,GetCamDef(aCampSX3[nCont],6,,GetSx3Cache(aCampSX3[nCont][1],"X3_DECIMAL"));				//[n][07] Decimal
				,.F.;						//[n][08] Indica se permite a edi��o
				,{||.T.};					//[n][09] Code-Block de valida��o da coluna ap�s a edi��o
				,.F.;						//[n][10] Indica se exibe imagem
				,{||nil};					//[n][11] Code-Block de execu��o do duplo clique
				,"";						//[n][12] Vari�vel a ser utilizada na edi��o (ReadVar)
				,{||nil};					//[n][13] Code-Block de execu��o do clique no header
				,.F.;						//[n][14] Indica se a coluna est� deletada
				,.F.;						//[n][15] Indica se a coluna ser� exibida nos detalhes do Browse
				,{};						//[n][16] Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			}
		Else
			aColumn := {;
				GetCamDef(aCampSX3[nCont],3,1);				//[n][01] T�tulo da coluna
				,&("{|| "+GetTabDados(aCampSX3[nCont])+"}");		//[n][02] Code-Block de carga dos dados
				,GetCamDef(aCampSX3[nCont],4,,"C");					//[n][03] Tipo de dados
				,GetCamDef(aCampSX3[nCont],7,,"");				//[n][04] M�scara
				,If(GetCamDef(aCampSX3[nCont],4,,"C")=="N",2,1);	//[n][05] Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				,GetCamDef(aCampSX3[nCont],5,,10);				//[n][06] Tamanho
				,GetCamDef(aCampSX3[nCont],6,,0);					//[n][07] Decimal
				,.F.;						//[n][08] Indica se permite a edi��o
				,{||.T.};					//[n][09] Code-Block de valida��o da coluna ap�s a edi��o
				,.F.;						//[n][10] Indica se exibe imagem
				,{||nil};					//[n][11] Code-Block de execu��o do duplo clique
				,"";						//[n][12] Vari�vel a ser utilizada na edi��o (ReadVar)
				,{||nil};					//[n][13] Code-Block de execu��o do clique no header
				,.F.;						//[n][14] Indica se a coluna est� deletada
				,.F.;						//[n][15] Indica se a coluna ser� exibida nos detalhes do Browse
				,{};						//[n][16] Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			}
		EndIf
		Aadd(aColumns, aColumn )
	Next
Return aColumns

Static Function GetCamDef(aCampo,nIndice,nIndDef,cDefault)
	Default cDefault	:= ""
	If Len(aCampo)>=nIndice .and. ValType(aCampo[nIndice])<>"U"
		Return aCampo[nIndice]
	ElseIf ValType(nIndDef)=="N" .and. nIndDef>0
		Return aCampo[nIndDef]
	Else
		Return cDefault
	EndIf
Return cDefault

Static Function GetTabDados(aCampo)
	Local cRet	:= ""
	If SubStr(aCampo[1],1,4)=="ZZ2_"
		cRet	:= "ZZ2->"+aCampo[1]
	Else
		If Len(aCampo)>=2
			cRet	:= aCampo[2]
		Else
			cRet	:= "''"
		EndIf
	EndIf
Return cRet

Static Function fLegZZS(cCOR)

	Local cRet:=""
	Local nAt
	aValores:= Separa(GetSx3Cache("ZZS_LEGEND","X3_CBOX"),";",.T.) 
	nPos			:= aScan(aValores,{|x| Left(x,Len(cCOR))==cCOR})
	If nPos>0
		nAt		:= At("=",aValores[nPos])
		cRet	:= SubStr(aValores[nPos],nAt+1)
	EndIf

Return cRet

//Calcula total dos Itens
User Function VZZ2TOT
	cQuery:=" SELECT SUM(ZZ3_VTOTAL) ZZ3_VTOTAL FROM  "+RetSqlName("ZZ3")+" ZZ3 "
	cQuery+=" WHERE ZZ3.D_E_L_E_T_=' ' AND "
	cQuery+=" ZZ3_FILIAL+ZZ3_NUMERO+ZZ3_MEDICA = '"+xFilial('ZZ3',ZZ2->ZZ2_FILIAL)+ZZ2->(ZZ2_NUMERO+ZZ2_MEDICA)+"' "

	If SELECT("Qa020")>0
		Qa020->(dbCloseArea())
	Endif
	TCQUERY cQuery NEW ALIAS Qa020
	
	nRet:= Qa020->ZZ3_VTOTAL
	
	Qa020->(dbCloseArea())	
	
Return nRet


//Calcula total dos Itens
User Function VZZQTOT
	cQuery:=" SELECT SUM(ZZQ_QUANT) ZZQ_QUANT FROM  "+RetSqlName("ZZQ")+" ZZQ "
	cQuery+=" WHERE ZZQ.D_E_L_E_T_=' ' AND "
	cQuery+=" ZZQ_FILIAL+ZZQ_NUMERO+ZZQ_MEDICA = '"+ZZ2->(ZZ2->ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICA)+"' "

	If SELECT("Qa020")>0
		Qa020->(dbCloseArea())
	Endif
	TCQUERY cQuery NEW ALIAS Qa020
	
	nRet:= Qa020->ZZQ_QUANT
	
	Qa020->(dbCloseArea())	
	
Return nRet



//Hist�rico de Status de Acompanhamento
User Function HISTRF020
	Local aDad20		:= {}
	Private cFilContr	:=	ZZ2->ZZ2_FILIAL	
	Private cContra		:=	ZZ2->ZZ2_NUMERO
	Private cMedic		:=	ZZ2->ZZ2_MEDICA	
	Private cClient     :=  ZZ2->ZZ2_CLIENT
	Private cLoja       :=  ZZ2->ZZ2_LOJA
	 
	
	cQuery := " SELECT ZZT_FILIAL,ZZT_CONTRA,"
	cQuery += " ZZT_MEDICA,ZZT_CODIGO,"
	cQuery += " ZZT_USUARI,ZZT_DTINC,"
	cQuery += " ZZT_HORA,ZZT_STATUS,X5_DESCRI, ZZT_OBS " 
	cQuery += " FROM "+RetSqlName("ZZT")+" ZZT "
	cQuery += " JOIN "+RetSqlName("SX5")+" SX5 "
	cQuery += " ON ZZT_STATUS = X5_CHAVE "
	cQuery += " AND X5_TABELA = 'ZB' "
	cQuery += " WHERE ZZT.D_E_L_E_T_ =' ' "
	cQuery += " AND SX5.D_E_L_E_T_ =' ' "
	cQuery += " AND ZZT_FILIAL= '"+xFilial("ZZT",cFilContr)+"' "
	cQuery += " AND ZZT_CONTRA = '"+cContra+"' "
	cQuery += " AND ZZT_MEDICA = '"+cMedic+"' "
	cQuery += " ORDER BY ZZT_CODIGO "	
	
	TCQUERY cQuery NEW ALIAS "TQRY020"
	
	If TQRY020->(eof())
		Aadd(aDad20,{"","","","","","","","","","",""})
	Endif	
	
	while TQRY020->(!eof())
		aadd(aDad20, {	TQRY020->ZZT_FILIAL,;
						TQRY020->ZZT_CONTRA,;
						TQRY020->ZZT_MEDICA,;
						TQRY020->ZZT_CODIGO,;
						TQRY020->ZZT_USUARI,;
						Upper(Alltrim(UsrRetName(TQRY020->ZZT_USUARI))),;
						dtoc(stod(TQRY020->ZZT_DTINC)),;
						TQRY020->ZZT_HORA,;
						TQRY020->ZZT_STATUS,;
						TQRY020->X5_DESCRI,;
						TQRY020->ZZT_OBS;
						})
		TQRY020->(dbSkip())
	enddo
		
	TQRY020->(dbCloseArea())
	aCoors		:= FwGetDialogSize( oMainWnd )
 	DEFINE DIALOG oDlgHist TITLE "Hist�rico Status / Contrato: "+Alltrim(cContra)+"- Medi��o: "+Alltrim(cMedic) FROM aCoors[1],aCoors[2] TO aCoors[3],aCoors[4] PIXEL

    // Cria browse
    oBrowHist := MsBrGetDBase():New( 0, 0, aCoors[3]+87, 260 ,,,, oDlgHist,,,,,,,,,,,, .F., "", .T.,, .F.,,, ) //COLUNA e LINHA
    // Define vetor para a browse
    oBrowHist:SetArray(aDad20)
    
    oFont  := TFont():New( "Arial",0,-11,,.T.,0,,700,.F.,.F.,,,,,, )
    TButton():New( 265	,006,"Incluir",,{|| IncZZT(@aDad20),oBrowHist:Refresh()  },048,016,,oFont,,.T.,,"Incluir",,,,.F. )
    
		
    // Cria colunas do browse
    oBrowHist:AddColumn(TCColumn():New("Filial",{ || aDad20[oBrowHist:nAt,1] }; 
        ,GetSx3Cache("ZZT_FILIAL","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 
    
    oBrowHist:AddColumn(TCColumn():New("Contrato",{ || aDad20[oBrowHist:nAt,2] }; 
        ,GetSx3Cache("ZZT_CONTRA","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 
    
    oBrowHist:AddColumn(TCColumn():New("Medi��o",{ || aDad20[oBrowHist:nAt,3] }; 
        ,GetSx3Cache("ZZT_MEDICA","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 
    
    oBrowHist:AddColumn(TCColumn():New("C�digo",{ || aDad20[oBrowHist:nAt,4] }; 
        ,GetSx3Cache("ZZT_CODIGO","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Usu�rio",{ || aDad20[oBrowHist:nAt,5] }; 
        ,GetSx3Cache("ZZT_USUARI","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Nome Usu�rio",{ || aDad20[oBrowHist:nAt,6] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Data Inclus�o",{ || aDad20[oBrowHist:nAt,7] }; 
        ,GetSx3Cache("ZZT_DTINC","X3_PICTURE"),,,"CENTER",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Hora",{ || aDad20[oBrowHist:nAt,8] }; 
        ,GetSx3Cache("ZZT_HORA","X3_PICTURE"),,,"CENTER",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Status",{ || aDad20[oBrowHist:nAt,9] }; 
        ,GetSx3Cache("ZZT_STATUS","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Desc. Status",{ || aDad20[oBrowHist:nAt,10] }; 
        ,GetSx3Cache("ZZT_STATUS","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Observa��o",{ || aDad20[oBrowHist:nAt,11] }; 
        ,GetSx3Cache("ZZT_OBS","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:Refresh()
  ACTIVATE DIALOG oDlgHist CENTERED 
  
Return

//Inclus�o ZZT
Static Function IncZZT(aDad20)
	Local aPergs	:= {}
	Local aRetOpc	:= {}
	Local cStat		:= space(3)
	Local cOBS		:= space(GetSx3Cache("ZZT_OBS","X3_TAMANHO"))
	Local lOk		:= .F.
	Local dDataE 	:= dDatabase

	aAdd( aPergs ,{1,"Status " 	,cStat	,"@!" ,'.T.',"ZB" ,'.T.',80 ,.T.})
	aAdd( aPergs ,{1,"Observa��o" ,cOBS	,"@!" ,'.T.',""   ,'.T.',80 ,.F.})
	aAdd( aPergs ,{1,"Data Entrega:" ,dDataE	,"@!" ,'.T.',""   ,'.T.',80 ,.F.})
	
	If ParamBox(aPergs,"Modifica��o de Status",aRetOpc,,,,,,,"_RF20",.F.,.F.)
		
		Begin Transaction
		
		cMaxZZT:= FMaxZZT()
		RecLock('ZZT',.T.)
			ZZT->ZZT_STATUS	:=	aRetOpc[1]
			ZZT->ZZT_OBS	:=	aRetOpc[2]
			ZZT->ZZT_FILIAL	:=	xFilial("ZZT",cFilContr)
			ZZT->ZZT_CONTRA	:=	cContra
			ZZT->ZZT_MEDICA	:=	cMedic
			ZZT->ZZT_CODIGO	:=	cMaxZZT
			ZZT->ZZT_USUARI	:=	RetCodUsr()
			ZZT->ZZT_DTINC	:=	Date()
			ZZT->ZZT_HORA	:=	substr(Time(),1,8) 
		MsUnlock()

		aadd(aDad20, {	xFilial("ZZT",cFilContr),;
						cContra,;
						cMedic,;
						cMaxZZT,;
						RetCodUsr(),;
						Upper(Alltrim(UsrRetName(RetCodUsr()))),;
						dtoc(Date()),;
						substr(Time(),1,8),;
						aRetOpc[1],;
						Posicione("SX5",1,xFilial("SX5")+"ZB"+aRetOpc[1],"X5_DESCRI"),;
						aRetOpc[2];
						})
		oBrowHist:SetArray(aDad20)
		
		End Transaction
		
		//Gravar �ltimo status na ZZ2
		dbSelectArea("ZZ2")
		ZZ2->(dbSetOrder(1))
		If ZZ2->(dbSeek(xFilial("ZZ2",cFilContr)+cContra+cMedic+cClient+cLoja) )
			RecLock("ZZ2",.F.)
				ZZ2->ZZ2_STACOM	:=	aRetOpc[1]
				ZZ2->ZZ2_OBSACO	:=	aRetOpc[2]
				ZZ2->ZZ2_DTENTR   := aRetOpc[3]
			MsUnlock()
		Endif
		
		
		MsgInfo("Hist�rico gravado com sucesso!")
		
	Endif

Return

//Calcula o sequencial da ZZT
Static Function FMaxZZT

	Local nRetSeq:= 0
	
	cQuery:= "SELECT ISNULL(MAX(ZZT_CODIGO),0) NMAX FROM "+RetSqlName("ZZT")+ ""
	cQuery+= " WHERE D_E_L_E_T_ =' ' AND "
	cQuery+= " ZZT_FILIAL = '"+xFilial("ZZT",cFilContr)+"' AND "
	cQuery+= " ZZT_CONTRA = '"+cContra+"' AND "
	cQuery+= " ZZT_MEDICA = '"+cMedic+"'  "
	
	TcQuery cQuery NEW Alias "QMAXZZT"
	
	nRetSeq:= soma1(QMAXZZT->NMAX,GetSx3Cache("ZZT_CODIGO","X3_TAMANHO"))
	
	QMAXZZT->(dbCloseArea())

Return nRetSeq

//Retorna o posi��o do campo no array de dados Ex.: GetSeqCam("ZZ2_FILIAL) retorna 1
Static function GetSeqCam(cCampo)
	Local nSeq := nil;
	
	For x:=1 to len(aCampSX3)
		if aCampSX3[x][1] == cCampo
			Return x	
		endif
	Next
	
Return

//Altera previs�o de entrega ap�s o faturamento
Static function ALTPREVISAO()
	Local cArea := GetArea()
	Local aPergs	:= {}
	Local dDataPrev := ctod("")
	Local lOk := .F.
	Local aRetParm	:= {}	
	
	If Empty(ZZ2->ZZ2_DTENTR)
		aAdd( aPergs ,{1,"Previs�o de Entrega  : ",dDataPrev,"", '.T.',"", '.T.', 50, .F.})
	
		If ParamBox(aPergs,"Alterar Previs�o",@aRetParm,{||.T.},,,,,,"U_RF040",.T.,.T.)
			lOk	:= .T.
		EndIf	
		
		If lOk
			//ATUALIZANDO O CADASTRO DE MEDI��O
			dbSelectArea("ZZ2")
			dbSetOrder(1)      // ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_MEDICAO+ZZ2_CLIENT+ZZ2_LOJA
			dbSeek(ZZ2->ZZ2_FILIAL + ZZ2->ZZ2_NUMERO + ZZ2->ZZ2_MEDICA + ZZ2->ZZ2_CLIENT + ZZ2->ZZ2_LOJA)     // Busca exata	
			
			IF FOUND()    // Avalia o retorno da pesquisa realizada
				RECLOCK("ZZ2", .F.)			
					ZZ2->ZZ2_DTPREV := aRetParm[1]				
				MSUNLOCK()     // Destrava o registro
			ENDIF
			
			("ZZ2")->(DbCloseArea())
			
			oBrowse:Refresh()
		Endif
	else
		MsgAlert("N�o � poss�vel alterar a previs�o para meteriais entregues."+CHR(13)+"Data de entrega: "+DToC(ZZ2->ZZ2_DTENTR),"Alterar Previs�o de Entrega")
	endif
Return