/*/{Protheus.doc} SASP105 
Bloqueio de usu�rio Controle de Titularidade de OS
@since 24/06/2016
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function SASP105()	
	

	local cVldAlt := ".T." // Operacao: ALTERACAO
	local cVldExc := ".T." // Operacao: EXCLUSAO
	

	local cAlias
	

	cAlias := "ZA0"
	chkFile(cAlias)
	dbSelectArea(cAlias)

	dbSetOrder(1)
	axCadastro(cAlias, "Trava do Controle de Titularidade", cVldExc, cVldAlt)
	
return

