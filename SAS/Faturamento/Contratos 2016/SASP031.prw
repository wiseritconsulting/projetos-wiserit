#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"

//-------------------------------------------------------------------
/*/{Protheus.doc} CADSZ1
Cadastro Contratos

@author Erenilson
@since 20/08/2015
/*/
//-------------------------------------------------------------------
Static aCacheTES	:= {}

USER FUNCTION SASP031()

	Local oBrowse
	Local oBrowse2

	Private cFilDest:= ""

	aCacheTES	:= {}	//Limpa o Cache das TES
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('ZZ0')
	oBrowse:SetDescription('Cadastro de Contrato')
	oBrowse:DisableDetails()

	oBrowse:AddLegend( "Empty(ZZ0_APROV) .AND. ZZ0_STATUS == 'A'", "YELLOW", "Aguardando aprova��o"  )
	oBrowse:AddLegend( "!Empty(ZZ0_APROV) .AND. ZZ0_STATUS=='L' ", "GREEN", "Aprovado"  )	
	oBrowse:AddLegend( "ZZ0_STATUS=='E'", "RED", "Encerrado"  )
	oBrowse:AddLegend( "ZZ0_STATUS=='B'", "BR_MARRON", "Bloqueado"  )
	oBrowse:AddLegend( "ZZ0_STATUS=='I'", "BLACK", "Inativo"  )
	oBrowse:SetMenuDef( 'SASP031' ) 
	oBrowse:Activate()

	oBrowse2 := FWMBrowse():New()
	oBrowse2:SetAlias('ZZ1')
	oBrowse2:SetDescription('Cadastro de Contrato')
	oBrowse2:DisableDetails()

	oBrowse2:AddLegend( "!Empty(ZZ1_NUMERO)", "YELLOW", "Incluido"  )
	oBrowse2:SetMenuDef( 'SASP031B' ) 

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional
@obs aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
9 - C�pia
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional
/*/
//-------------------------------------------------------------------

Static Function MenuDef()

	Local aRotina := {}

	Public cFlag := ""
	Public aKit := {}


	ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PesqBrw"			OPERATION 1 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.SASP031"	OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.SASP031"	OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.SASP031"	OPERATION 4 ACCESS 143
	ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.SASP031"	OPERATION 5 ACCESS 144
	ADD OPTION aRotina TITLE "Imprimir"   ACTION "VIEWDEF.SASP031"	OPERATION 8 ACCESS 0

	If (Alltrim(RetCodUsr()) $ Alltrim(GetMv("SA_USUAPRO")))

		ADD OPTION aRotina TITLE "Aprova��o"  ACTION "U_YZZ0001()"		OPERATION 10 ACCESS 0 DISABLE MENU

	EndIf
	If (Alltrim(RetCodUsr()) $ Alltrim(GetMv("SA_USUDESA")))

		ADD OPTION aRotina TITLE "Desaprova��o"  ACTION "U_YZZ0099()"		OPERATION 10 ACCESS 0 DISABLE MENU

	EndIf
	ADD OPTION aRotina TITLE "Medi��o"    				ACTION "U_YZZ0002()"	OPERATION 4 ACCESS 0 DISABLE MENU
	//ADD OPTION aRotina TITLE "Medi��o Automatica"   	ACTION "U_SASP035()"	OPERATION 4 ACCESS 0 DISABLE MENU
	//ADD OPTION aRotina TITLE "Encerramento"    			ACTION "U_YZZ0009()"	OPERATION 4 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Aditivo"    				ACTION "U_YZZ0003()"	OPERATION 4 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Ativar/Inativar Itens"    ACTION "U_SASP033()"	OPERATION 4 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Ver Adiantamentos"    	ACTION "U_YZZ0008()"	OPERATION 2 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Recalculo"    	        ACTION "U_Recalc()"	OPERATION 4 ACCESS 0 DISABLE MENU
	If (Alltrim(RetCodUsr()) $ Alltrim(GetMv("SA_USUAPRO")))
		ADD OPTION aRotina TITLE "Bloquear Contrato"    	ACTION "U_YZZ0006()"	OPERATION 4 ACCESS 0 DISABLE MENU
		ADD OPTION aRotina TITLE "Desbloquear Contrato"    	ACTION "U_YZZ0007()"	OPERATION 4 ACCESS 0 DISABLE MENU
	Endif	

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	Local oStructZZ0 := Nil
	Local oStructZZ1 := Nil
	Local oModel     := Nil
	Local nI
	//-----------------------------------------
	//Monta a estrutura do formul�rio com base no dicion�rio de dados
	//-----------------------------------------
	oStructZZ0 := FWFormStruct(1,"ZZ0")
	oStructZZ1 := FWFormStruct(1,"ZZ1")

	oStructZZ0:AddTrigger( ;
	"ZZ0_CLIENT"		,;										//[01] Id do campo de origem
	"ZZ0_NOME"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SA1",1,xFilial("SA1")+oModel:GetValue("ZZ0_CLIENT"),"SA1->A1_NOME") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ1:AddTrigger( ;
	"ZZ1_QUANT"			,;										//[01] Id do campo de origem
	"ZZ1_VLRTOT"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ1_QUANT") * oModel:GetValue("ZZ1_VLRUN") }  )	// [04] Bloco de codigo de execu��o do gatilho

	//Atualizar o saldo conforme as medi��es.	
	oStructZZ1:AddTrigger( ;
	"ZZ1_QUANT"			,;										//[01] Id do campo de origem
	"ZZ1_SALDO"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ1_QUANT") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ1:AddTrigger( ;
	"ZZ1_VLRUN"		,;										//[01] Id do campo de origem
	"ZZ1_VLRTOT"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ1_QUANT") * oModel:GetValue("ZZ1_VLRUN") }  )	// [04] Bloco de codigo de execu��o do gatilho

	/*
	oStructZZ1:AddTrigger( ;
	"ZZ1_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ1_DESC"			,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("SB1",oModel:GetValue('ZZ1_PRODUT'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SB1",1,xFilial("SB1")+oModel:GetValue("ZZ1_PRODUT"),"B1_DESC") }  )	// [04] Bloco de codigo de execu��o do gatilho
	*/


	oStructZZ1:AddTrigger( ;
	"ZZ1_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ1_VLRDSC"		,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("ZZ7",oModel:GetValue('ZZ1_PRODUT'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_RETDSCZZ0(@oModel) }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ0:AddTrigger( ;
	"ZZ1_CLIENT"		,;										//[01] Id do campo de origem
	"ZZ1_NOME"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SA1",1,xFilial("SA1")+oModel:GetValue('ZZ0_CLIENT')+oModel:GetValue('ZZ0_LOJA'),"A1_NOME") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ0:AddTrigger( ;
	"ZZ1_LOJA"		,;											//[01] Id do campo de origem
	"ZZ1_NOME"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SA1",1,xFilial("SA1")+oModel:GetValue('ZZ0_CLIENTE')+oModel:GetValue('ZZ0_LOJA'),"A1_NOME") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ1:AddTrigger( ;
	"ZZ1_VLRUN"		,;										//[01] Id do campo de origem
	"ZZ1_VLRLIQ"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ1_QUANT") * oModel:GetValue("ZZ1_VLRUN") - (oModel:GetValue("ZZ1_VLRDSC") * oModel:GetValue("ZZ1_QUANT")) }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ1:AddTrigger( ;
	"ZZ1_QUANT"			,;										//[01] Id do campo de origem
	"ZZ1_VLRLIQ"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ1_QUANT") * oModel:GetValue("ZZ1_VLRUN") - (oModel:GetValue("ZZ1_VLRDSC") * oModel:GetValue("ZZ1_QUANT")) }  )	// [04] Bloco de codigo de execu��o do gatilho


	//-----------------------------------------
	//Monta o modelo do formul�rio
	//-----------------------------------------

	oModel:= MPFormModel():New("YCADZZ0",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)

	oModel:AddFields("ZZ0MASTER",/*cOwner*/, oStructZZ0 ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)		

	oModel:AddGrid("ZZ1DETAIL", "ZZ0MASTER"/*cOwner*/,oStructZZ1, ,/*bLinePost*/,/*bPre*/,/*bPost*/,/*Carga*/)

	//Quando for inclus�o
	IF empty(cFlag) .OR. Alltrim(cFlag) ==  "SASP036"


		//Quando for encerramento do tipo TRANSFERENCIA - gera��o de novo contrato.	
	Elseif alltrim(cFlag) != 'YZZ0003'

		oModel:SetActivate({|oModel| LoadZZ0(oModel) })

	EndIf

	oModel:SetPrimaryKey({"ZZ0_FILIAL","ZZ0_NUMERO"})
	oModel:GetModel("ZZ1DETAIL"):GetStruct():SetProperty("ZZ1_PRODUT",MODEL_FIELD_VALID,{|oModel|ZZ0ValProd(oModel) })
	oModel:GetModel("ZZ1DETAIL"):GetStruct():SetProperty("ZZ1_TES",MODEL_FIELD_VALID,{|oModel|ZZ0ValTES(oModel) })

	/*
	If cFlag == "SASP031" .AND. ALLTRIM(cFilDest) <> ""

	oModel:GetModel("ZZ1DETAIL"):GetStruct():SetProperty("ZZ1_DESC",MODEL_FIELD_INIT,{|oModel|Posicione("SB1",1,xFilial("SB1")+;
	oModel:GetValue("ZZ1_PRODUT"),"B1_DESC") })

	ENDIF
	*/

	// Adiciona ao modelo uma estrutura de formul�rio de campos calculados
	oModel:AddCalc( 'YCADZZ0CALC1', 'ZZ0MASTER', 'ZZ1DETAIL', 'ZZ1_QUANT', 'ZZ1__TOT01', 'SUM',,,'Total Itens',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"QTDMED") }*/ )
	oModel:AddCalc( 'YCADZZ0CALC1', 'ZZ0MASTER', 'ZZ1DETAIL', 'ZZ1_SALDO', 'ZZ1__TOT02', 'SUM',,,'Saldo dos Itens',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)
	oModel:AddCalc( 'YCADZZ0CALC1', 'ZZ0MASTER', 'ZZ1DETAIL', 'ZZ1_VLRTOT', 'ZZ1__TOT03', 'SUM',,,'Valor Total(R$)',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)
	oModel:AddCalc( 'YCADZZ0CALC1', 'ZZ0MASTER', 'ZZ1DETAIL', 'ZZ1_VLRLIQ', 'ZZ1__TOT04', 'SUM',,,'Valor Total Liquido(R$)',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)

	oModel:GetModel( 'ZZ1DETAIL' ):SetUniqueLine( { 'ZZ1_PRODUT','ZZ1_TES' } )

	oModel:GetModel("ZZ0MASTER"):SetDescription("Contrato")
	oModel:GetModel("ZZ1DETAIL"):SetDescription("Itens Contrato")
	oModel:SetRelation("ZZ1DETAIL",{{"ZZ1_FILIAL",'xFilial("ZZ1")'},{"ZZ1_NUMERO","ZZ0_NUMERO"},{"ZZ1_CLIENT","ZZ0_CLIENT"},{"ZZ1_LOJA","ZZ0_LOJA"}},ZZ1->(IndexKey(1)))
	oModel:SetVldActive( { |oModel| ZZ0ValAct( oModel ) } )
	oModel:SetCommit({|oModel| SZ1Commit(oModel) },.F.)
	oModel:SetCommit({|oModel| FWFormCommit(oModel) },.T.)

	oModel:AddRules( 'ZZ1DETAIL', 'ZZ1_QUANT'	,'ZZ1DETAIL'	,'ZZ1_PRODUT'	,1 )
	oModel:AddRules( 'ZZ1DETAIL', 'ZZ1_VLRUN'	,'ZZ1DETAIL'	,'ZZ1_PRODUT'	,1 )
	oModel:AddRules( 'ZZ1DETAIL', 'ZZ1_VLRTOT'	,'ZZ1DETAIL'	,'ZZ1_PRODUT'	,1 )
	oModel:AddRules( 'ZZ1DETAIL', 'ZZ1_TES'		,'ZZ1DETAIL'	,'ZZ1_PRODUT'	,1 )

Return(oModel)

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
	Local oStructZZ0	:= FWFormStruct( 2, 'ZZ0' )
	Local oStructZZ1	:= FWFormStruct(2,"ZZ1",{|cCampo| !AllTrim(cCampo)+"|" $ "ZZ1_FILIAL|ZZ1_LOJA|ZZ1_CLIENT|ZZ1_NOME|ZZ1_LOJA|"})
	Local oModel		:= FWLoadModel( 'SASP031' )	
	Local oCalc1	

	Local oView
	oStructZZ1:RemoveField( 'ZZ1_NUMERO' )	

	// Cria o objeto de Estrutura
	oCalc1	:= FWCalcStruct( oModel:GetModel( 'YCADZZ0CALC1') )
	oView	:= FWFormView():New()

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField( "ZZ0MASTER",oStructZZ0)
	oView:AddGrid("ZZ1DETAIL",oStructZZ1)
	oView:AddField( 'VIEW_CALC', oCalc1, 'YCADZZ0CALC1' )

	oView:CreateHorizontalBox("CABEC",30)
	oView:CreateHorizontalBox("GRID",60)
	oView:CreateHorizontalBox( 'RODAPE',10)

	oView:SetOwnerView( "ZZ0MASTER","CABEC")
	oView:SetOwnerView( "ZZ1DETAIL","GRID")
	oView:SetOwnerView( 'VIEW_CALC', 'RODAPE' )

	oView:SetProgressBar(.T.)
	oView:AddIncrementField("ZZ1DETAIL","ZZ1_ITEM")
	oView:EnableTitleView('ZZ1DETAIL','Itens Contrato')

	oView:AddUserButton( 'Procurar Kit', 'CLIPS', { |oView| ProcKit(oView) }, , VK_F4,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE,MODEL_OPERATION_VIEW} )
	oView:AddUserButton( 'Visualizar Kit', 'CLIPS', { |oView| VISKit(oView) }, , VK_F7,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE,MODEL_OPERATION_VIEW} )
	oView:AddUserButton( 'Visualizar Aditivos', 'CLIPS', { |oView| Aditivos(oView) }, , VK_F8,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE,MODEL_OPERATION_VIEW}) 

Return oView

//Carga na ZZ0 quando for um contrato gerado a partir do Encerramento - Transferencia
Static Function LoadZZ0(oModel)

	Local cQuery := ""
	Local oModelZZ1 := oModel:GetModel( 'ZZ1DETAIL' )
	Local nOperation := oModel:GetOperation()
	Local cNum		  := "000001"
	Local cFilAn	:= ZZ0->ZZ0_FILIAL
	If nOperation <> 2  

		IF ALLTRIM(cFilDest) <> ""

			cQuery := "" 
			cQuery += " SELECT " 
			cQuery += "    MAX(ZZ0_NUMERO) AS NUM "
			cQuery += "   FROM " + RETSQLNAME("ZZ0")
			cQuery += "  WHERE D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ0_FILIAL = '" + cFilDest + "' "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XZZ0",.F.,.T.)

			IF XZZ0->(!Eof())

				cNum 	  := SOMA1(XZZ0->NUM)             

			Else

				cNum 	  := "000001"

			Endif

			XZZ0->(DBCLOSEAREA())

			oModel:SetValue("ZZ0MASTER", "ZZ0_FILIAL", cFilDest)	
			oModel:SetValue("ZZ0MASTER", "ZZ0_NUMCRM", ZZ0->ZZ0_NUMCRM)
			oModel:SetValue("ZZ0MASTER", "ZZ0_NUMERO", cNum)
			oModel:SetValue("ZZ0MASTER", "ZZ0_CLIENT", ZZ0->ZZ0_CLIENT)
			oModel:SetValue("ZZ0MASTER", "ZZ0_LOJA", ZZ0->ZZ0_LOJA)
			oModel:SetValue("ZZ0MASTER", "ZZ0_NOME", ZZ0->ZZ0_NOME)
			oModel:SetValue("ZZ0MASTER", "ZZ0_NATURE", ZZ0->ZZ0_NATURE)
			oModel:SetValue("ZZ0MASTER", "ZZ0_ANOCOM", ZZ0->ZZ0_ANOCOM)
			oModel:SetValue("ZZ0MASTER", "ZZ0_CODCRM", ZZ0->ZZ0_CODCRM)
			oModel:SetValue("ZZ0MASTER", "ZZ0_CCUSTO", ZZ0->ZZ0_CCUSTO)
			oModel:SetValue("ZZ0MASTER", "ZZ0_CONORI", ZZ0->ZZ0_NUMERO)
			oModel:SetValue("ZZ0MASTER", "ZZ0_CLIORI", ZZ0->ZZ0_CLIENT)
			oModel:SetValue("ZZ0MASTER", "ZZ0_LOJORI", ZZ0->ZZ0_LOJA)
			oModel:SetValue("ZZ0MASTER", "ZZ0_ADIANT", ZZ0->ZZ0_ADIANT)
			oModel:SetValue("ZZ0MASTER", "ZZ0_STATUS", "A")
			oModel:SetValue("ZZ0MASTER", "ZZ0_APROV", "")

			cQuery := "" 
			cQuery += " SELECT " 
			cQuery += "    ZZ1_PRODUT, "
			cQuery += "    ZZ1_DESC, " 
			cQuery += "    ZZ1_QUANT, "
			cQuery += "    ZZ1_SALDO, "
			cQuery += "    ZZ1_TES, " 
			cQuery += "    ZZ1_VLRUN, "
			cQuery += "    ZZ1_STATUS "
			cQuery += "   FROM " + RETSQLNAME("ZZ1")
			cQuery += "  WHERE D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ1_FILIAL = '" + cFilAn + "' "
			cQuery += "    AND ZZ1_NUMERO = '" + ZZ0->ZZ0_NUMERO + "' "
			//cQuery += "    AND ZZ1_SALDO > 0 "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XZZ1",.F.,.T.)

			While XZZ1->(!EOF())

				oModelZZ1:AddLine()

				oModel:SetValue("ZZ1DETAIL", "ZZ1_FILIAL", cFilDest)
				oModel:SetValue("ZZ1DETAIL", "ZZ1_PRODUT", XZZ1->ZZ1_PRODUT)
				oModel:SetValue("ZZ1DETAIL", "ZZ1_DESC", XZZ1->ZZ1_DESC)
				oModel:SetValue("ZZ1DETAIL", "ZZ1_QUANT", XZZ1->ZZ1_QUANT)
				oModel:SetValue("ZZ1DETAIL", "ZZ1_SALDO", XZZ1->ZZ1_SALDO)	
				oModel:SetValue("ZZ1DETAIL", "ZZ1_TES", XZZ1->ZZ1_TES)
				oModel:SetValue("ZZ1DETAIL", "ZZ1_VLRUN", XZZ1->ZZ1_VLRUN)			
				oModel:SetValue("ZZ1DETAIL", "ZZ1_STATUS", XZZ1->ZZ1_STATUS)		

				XZZ1->(DBSKIP())	

			EndDo

			XZZ1->(DBCLOSEAREA())

		ENDIF

	ENDIF

Return

Static Function ZZ0ValAct(oModel)
	Local lRet:= .T.
	Local cContrato
	If oModel:getOperation() == MODEL_OPERATION_INSERT
		/*If cFilAnt=="010105"
		oModel:SetErrorMessage('ZZ0MASTER',,,,"ATEN��O", 'Impossivel realizar contratos na empresa Matriz',"Escolha outra filial para criar o contrato" )
		Return .F.
		EndIf
		*/
	ElseIf oModel:getOperation() == MODEL_OPERATION_DELETE
		cContrato	:= ZZ0->ZZ0_NUMERO
		SE1->(DbSetOrder(1))	//E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO

		If	!Empty(ZZ0->ZZ0_APROV) .and. empty(cFlag)

			oModel:SetErrorMessage('ZZ0MASTER',,,,"ATEN��O", 'Contrato j� liberado, n�o pode ser excluido!', '')
			Return .F.

		elseIf SE1->(DbSeek(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+padl(cContrato,9,'0'))) .AND. !Empty(SE1->E1_BAIXA)
			oModel:SetErrorMessage('ZZ0MASTER',,,,"ATEN��O", 'O titulo relacionando a esse contrato j� possui movimenta��es!', 'Para excluir � necessario excluir os movimentos primeiro!')
			Return .F.
		EndIf
	ElseIf oModel:getOperation() == MODEL_OPERATION_UPDATE

		//Verifica se h� aprovador e se a variavel cFlag est� vazia. A cFlag � como uma flag para informar se n�o � uma medi��o.
		If	!Empty(ZZ0->ZZ0_APROV) .and. empty(cFlag)

			oModel:SetErrorMessage('ZZ0MASTER',,,,"ATEN��O", 'Contrato j� liberado, n�o pode ser alterado!', 'Para altera��o realize a adi��o do contrato!')
			Return .F.

		ElseIf Alltrim(cFlag) == 'YZZ0003'

			Return .T.

		EndIf
	EndIf
Return lRet

Static Function SZ1Commit(oModel)

	Local cContrato	:= oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_NUMERO')
	Local cClient :=  oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_CLIENT')
	Local cLoja :=  oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_LOJA')
	Local nSaldo		:= 0
	Local nVlAdiant	:= 0
	Local nQtd			:= 0
	Local lRet			:= .T.
	Local nCont
	Local aDados		:= {}
	Local cUpdate := ""
	Local nAdiant := 0
	Local nEstoque := 0
	Local dDataPgto := ''
	Local cMsg := ""	
	Local oView := FWViewActive()

	Private lMsErroAuto	:= .F.	


	If	(oModel:getOperation() == MODEL_OPERATION_UPDATE) .OR. 	(oModel:getOperation() == MODEL_OPERATION_INSERT)

		oModelZZ1	:= oModel:GetModel( 'ZZ1DETAIL' )
		For nCont := 1 To oModelZZ1:Length()

			oModelZZ1:GoLine( nCont )
			If oModelZZ1:IsDeleted()
				Loop
			EndIf

			If SF4->(DbSeek(xFilial("SF4")+oModelZZ1:GetValue('ZZ1_TES')))  .AND. (SF4->F4_DUPLIC=="S" .OR.  oModelZZ1:GetValue('ZZ1_TES') $ GETMV("SA_TESEXCE")) 	//Gera Financeiro
				nAdiant	+= oModelZZ1:GetValue('ZZ1_VLRLIQ') 
			EndIf

			//incluido por rafael pinheiro - 02/11/2015
			// verifica se tes movimenta estoque
			If SF4->(DbSeek(xFilial("SF4")+oModelZZ1:GetValue('ZZ1_TES')))  .AND. SF4->F4_ESTOQUE=="N"	//Gera Financeiro
				nEstoque	+= oModelZZ1:GetValue('ZZ1_VLRLIQ') 
			EndIf
			//fim - incluido por rafael pinheiro - 02/11/2015

		Next
	EndIf

	//VALOR DO FRETE SER� CONSIDERADO SOMENTE O QUE GERA ESTOQUE.
	nAdiant	+=  (nAdiant-nEstoque) * (oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_FRTCLI') / 100) // modificado por rafael pinheiro 02/11/2015
	dDataPgto :=  oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_DTCOND')

	//oModel:LoadValue("ZZ0MASTER","ZZ0_ADIANT",nAdiant) 
	// Rotina inclusa por Weskley Silva 19/10/2015
	if oModel:getOperation() == 3 .OR. oModel:getOperation() == 4

		oModel:LoadValue("ZZ0MASTER","ZZ0_ADIANT",nAdiant) 

	EndIf	
	// -----------------------------------------------------
	oView:REFRESH()

	If nAdiant > 0 .AND. Empty(StrTran(dtoc(dDataPgto),'/',''))	

		oModel:SetErrorMessage('ZZ0MASTER',,,,"ATEN��O", "O valor de adiantamento � maior que zero, o campo de data de pagamento deve ser preenchido para que seja gerado titulo no contas a receber.", "Verifique o campo de data para pagamento(ZZ0_DTCOND).")

		Return .F.			
	EndIf	

	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
	If	(oModel:getOperation() == MODEL_OPERATION_UPDATE) .OR. 	(oModel:getOperation() == MODEL_OPERATION_INSERT)

		oModelZZ1	:= oModel:GetModel( 'ZZ1DETAIL' )
		For nCont := 1 To oModelZZ1:Length()
			oModelZZ1:GoLine( nCont )
			If oModelZZ1:IsDeleted()
				Loop
			EndIf

			If SF4->(DbSeek(xFilial("SF4")+oModelZZ1:GetValue('ZZ1_TES')))  .AND. (SF4->F4_DUPLIC=="S" .OR.  oModelZZ1:GetValue('ZZ1_TES') $ GETMV("SA_TESEXCE"))	//Gera Financeiro
				//nVlAdiant	+= oModelZZ1:GetValue('Z2_VLRLIQ')
			EndIf
		Next

		//Se variavel cFlag estiver preenchida, sinaliza que � uma transfer�ncia.
		If !empty(cFlag) .AND. ZZ0->ZZ0_NUMERO <> cContrato

			cUpdate := ""
			cUpdate := " UPDATE " + RETSQLNAME("ZZ1")
			cUpdate += "    SET ZZ1_SALDO = 0 "
			cUpdate += "  WHERE D_E_L_E_T_ = '' "
			cUpdate += "    AND ZZ1_FILIAL = '" + ZZ0->ZZ0_FILIAL + "' "
			cUpdate += "    AND ZZ1_NUMERO = '" + ZZ0->ZZ0_NUMERO + "' "

			TCSQLEXEC(cUpdate)

			RECLOCK("ZZ0",.F.)

			ZZ0->ZZ0_OBSERV := ZZ0->ZZ0_OBSERV + Mv_Par02 + "Contrato " + ZZ0->ZZ0_NUMERO + " foi transferido para o contrato " + cContrato +". " 
			ZZ0->ZZ0_STATUS := 'I'

			MSUNLOCK()

		EndIf

		cMsg := ""

		If	(oModel:getOperation() == MODEL_OPERATION_UPDATE) .OR. ALLTRIM(cFilDest) <> ""
			cMsg += "Alterado contrato: " + CHR(13) + CHR(10)
		ELSE
			cMsg += "Incluido contrato: " + CHR(13) + CHR(10)
		EndIf

		cMsg += "Filial: " + cFilDest + CHR(13) + CHR(10)
		cMsg += "Contrato: " + cContrato  + CHR(13) + CHR(10)
		cMsg += "Cliente: " +  cClient + "-" + POSICIONE("SA1",1,XFILIAL("SA1") + cClient+cLoja,"SA1->A1_NREDUZ")
		cMsg += "Loja: " + cLoja + CHR(13) + CHR(10)

		MessageBox(cMsg, "TOTVS", 64)

	ElseIf oModel:getOperation() == MODEL_OPERATION_DELETE
		SE1->(DbSetOrder(1))	//E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
		Begin Transaction
			While SE1->(DbSeek(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+PadL(cContrato,GetSx3Cache("E1_NUM","X3_TAMANHO"))))
				AADD(aDados,{"E1_FILIAL"		,xFilial("SE1")		,"#"})
				AADD(aDados,{"E1_PREFIXO"	,SE1->E1_PREFIXO		,"#"})
				AADD(aDados,{"E1_NUM"		,SE1->E1_NUM			,"#"})
				/*AADD(aDados,{"E1_PARCELA"	,SE1->E1_PARCELA		,"#"})
				AADD(aDados,{"E1_TIPO"		,SE1->E1_TIPO			,"#"})
				AADD(aDados,{"INDEX"			,1						,"#"})*/
				MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 5)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
				If lMsErroAuto
					DisarmTransaction()
					AutoGrLog("Erro ao excluir o titulo!")
					AutoGrLog("Opera��o cancelada!")
					MostraErro()
					oModel:SetErrorMessage('ZZ0MASTER',,,,"ERRO", 'Erro ao excluir o titulo, opera��o cancelada!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				Endif
			EndDo
		End Transaction
	EndIf

Return lRet

Static Function Total02(oModel,cTipo)
	Local nCont		:= 0
	Local nTotal		:= 0
	Local oModelZZ1	:= oModel:GetModel( 'ZZ1DETAIL' )
	Local aSaveLines := FWSaveRows()
	Local cDuplic
	Default cTipo		:= "TOTAL"

	For nCont := 1 To oModelZZ1:Length()
		oModelZZ1:GoLine( nCont )
		If oModelZZ1:IsDeleted()
			Loop
		EndIf
		If cTipo == "TOTAL"
			nTotal	+= oModelZZ1:GetValue('Z2_VLRLIQ')
		ElseIf cTipo=="QTDMED"
			nTotal	+= oModelZZ1:GetValue('Z2_QTDMED')
		ElseIf cTipo=="VENDA"
			If (GetCacheSF4(oModelZZ1:GetValue('ZZ1_TES'),"F4_DUPLIC",@cDuplic) .and. cDuplic=="S") .OR. oModelZZ1:GetValue('ZZ1_TES') $ GETMV("SA_TESEXCE")	//Gera Financeiro
				nTotal	+= oModelZZ1:GetValue('Z2_VLRLIQ')
			EndIf
		EndIf
	Next
	FWRestRows( aSaveLines )
Return nTotal

User Function YZZ0099()

	Local aDados	:= {}
	Local aCondicao:= {}
	Local nValFin := 0
	Local nDesc := 0
	Local nCount := 0
	Local cMsg := ""
	Local cQuery := ""
	Local cQuery2 := ""
	Local nFrete := 0
	Local cUpdate	:= ""
	Private lMsErroAuto	:= .F.	
	
	IF MsgYesNo("Deseja desaprovar o contrato?")

	/*cQuery := " SELECT ZZ2_FILIAL, ZZ2_NUMERO, ZZ2_CLIENT, ZZ2_LOJA FROM "+RetSqlName("ZZ2")+" WHERE D_E_L_E_T_ = '' AND ZZ2_FILIAL = '"+ZZ0->ZZ0_FILIAL+"' "
	cQuery += " AND ZZ2_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' AND ZZ2_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' AND ZZ2_LOJA = '"+ZZ0->ZZ0_LOJA+"' "
	TcQuery cQuery new Alias T12

	IF !T12->(EOF())	
		MsgAlert("Existem medi��es geradas para este contrato, exclua a medi��o para depois fazer a desaprova��o!","Aten��o!!!")
		return
	ENDIF
	T12->(DbCloseArea())*/

	cQuery := " SELECT * FROM "+RetSqlName("SE1")+" E1(NOLOCK) "
	cQuery += " WHERE D_E_L_E_T_ = '' "
	cQuery += " AND E1_YCONTRA = '"+ZZ0->ZZ0_NUMERO+"' "
	cQuery += " AND E1_YFILIAL = '"+ZZ0->ZZ0_FILIAL+"'"
	cQuery += " AND E1_CLIENTE = '"+ZZ0->ZZ0_CLIENT+"' "
	cQuery += " AND E1_LOJA = '"+ZZ0->ZZ0_LOJA+"' "
	TcQuery cQuery new Alias T13

	WHILE !T13->(EOF())

		AADD(aDados,{"E1_PREFIXO"	,T13->E1_PREFIXO,nil})
		AADD(aDados,{"E1_NUM"		,T13->E1_NUM		,nil})
		AADD(aDados,{"E1_TIPO"		,T13->E1_TIPO					,nil})
		AADD(aDados,{"E1_PARCELA"	,T13->E1_PARCELA,nil})
		AADD(aDados,{"E1_NATUREZ"	,T13->E1_NATUREZ		,nil})
		AADD(aDados,{"E1_CLIENTE"	,T13->E1_CLIENTE		,nil})
		AADD(aDados,{"E1_LOJA"		,T13->E1_LOJA			,nil})
		AADD(aDados,{"E1_HIST"		,T13->E1_HIST,nil})
		AADD(aDados,{"E1_CCC"		,T13->E1_CCC		,nil})
		AADD(aDados,{"E1_VENCTO"		,T13->E1_VENCTO	,nil})
		AADD(aDados,{"E1_YCONTRA"		,T13->E1_YCONTRA,nil})
		AADD(aDados,{"E1_YFILIAL"		,T13->E1_YFILIAL,nil})					
		AADD(aDados,{"E1_VALOR"		,T13->E1_VALOR	,nil})
		AADD(aDados,{"E1_YVLFRET"		, T13->E1_YVLFRET 	,nil})
		AADD(aDados,{"E1_YCONTRA",T13->E1_YCONTRA	,nil})
		lMsErroAuto	:= .F.
		MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 5)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
		If lMsErroAuto
			DisarmTransaction()
			AutoGrLog("Erro ao excluir o titulo!")
			AutoGrLog("Opera��o cancelada!")
			MostraErro()
			
		Else
		aDados := {}
			T13->(DbSkip())					
		Endif
		

	ENDDO
	RecLock("ZZ0",.F.)
			ZZ0->ZZ0_APROV	:= "" //cNomeUsr
			ZZ0->ZZ0_HRAPRO	:= ""
			ZZ0->ZZ0_DTAPROV := STOD("")
			ZZ0->ZZ0_STATUS := "A"		
	ZZ0->(MsUnLock())
	MsgInfo("Titulos exclu�dos do financeiro com sucesso!")
	T13->(DbCloseArea())
	
	ENDIF
return

/*/{Protheus.doc} YZZ0001
Aprova��o do documento
@author Erenilson
@since 10/08/2015
@version 1.0
@return lRet, Logico se opera��o realizada com sucesso
/*/
User Function YZZ0001(lMsg,cNomeUsr,cTime)
	Local aDados	:= {}
	Local aCondicao:= {}
	Local nValFin := 0
	Local nDesc := 0
	Local nCount := 0
	Local cMsg := ""
	Local cQuery := ""
	Local cQuery2 := ""
	Local nFrete := 0
	Local cUpdate	:= ""
	Default cNomeUsr	:= UsrFullName(RetCodUsr())
	Default cTime		:= Time()
	Default lMsg		:= .T.
	Private lMsErroAuto	:= .F.	

	xArea := GetArea()

	If !Empty(ZZ0->ZZ0_APROV)
		If lMsg
			MsgAlert("Documento j� aprovado!")
		EndIf
		Return .F.
	Elseif lMsg .AND. !MsgYesNo("Deseja aprovar o documento?")
		Return .F.
	EndIf

	DBSELECTARE("ZZ0")
	dbSetOrder(1)  
	IF dbSeek(ZZ0->ZZ0_FILIAL+ZZ0->ZZ0_NUMERO+ZZ0->ZZ0_CLIENT+ZZ0->ZZ0_LOJA)
		//incluido por rafael pinheiro - 02/11/2015
		U_RF041(ZZ0->ZZ0_FILIAL,ZZ0->ZZ0_NUMERO,ZZ0->ZZ0_CLIENT,ZZ0->ZZ0_LOJA)

		IF alltrim(ZZ0->ZZ0_TIPCTR) <> 'V' .OR.  ZZ0->ZZ0_CONFNF == '2'//Se o contrato n�o for de venda n�o gera o financeiro

			RecLock("ZZ0",.F.)
			ZZ0->ZZ0_APROV	:= __cUserID //cNomeUsr
			ZZ0->ZZ0_HRAPRO	:= cTime
			ZZ0->ZZ0_DTAPROV := DATE()
			ZZ0->ZZ0_STATUS := "L"		
			ZZ0->(MsUnLock())

			IF alltrim(ZZ0->ZZ0_TIPCTR) <> 'V'
				MsgInfo("Contrato de Prospec��o/Bonifica��o aprovado com sucesso!")
			ELSEIF ZZ0->ZZ0_CONFNF == '2'
				MsgInfo("Contrato conforme nota aprovado com sucesso!")		
			ENDIF
			return .T.


		ENDIF
		//fim - incluido por rafael pinheiro - 02/11/2015


		If ZZ0->ZZ0_ADIANT >0 .AND. ZZ0->ZZ0_CONFNF == '1'

			nValFin	:= ZZ0->ZZ0_ADIANT //Verifica se j� teve item adicionado
			nDesc	:= FDescZ1(ZZ0->ZZ0_NUMERO,ZZ0->ZZ0_CLIENT,ZZ0->ZZ0_LOJA) //Busca desconto referente a produto kit
			nValDsc	:= 0 //Total do desconto por parcela
		ENDIF
		SE1->(dbSetOrder(1))
		Begin Transaction
			aCondicao	:= Condicao(nValFin,ZZ0->ZZ0_CPFIN,,ZZ0->ZZ0_DTCOND)
			If Empty(aCondicao)
				DisarmTransaction()
				AutoGrLog("Erro ao gerar parcelas do titulo!")
				AutoGrLog("Verifique a condi��o de pagamento e o valor total!")
				MostraErro()
				Return .F.
			EndIf

			//Verifica se j� existe T�tulo para o contrato
			cParc:= ""
			nTotD:= 0
			If SE1->(dbSeek(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+PADL(ZZ1->ZZ1_NUMERO,9,'0'))) //E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
				While SE1->(!eof()) .and. Alltrim(SE1->(E1_FILIAL+E1_PREFIXO+E1_NUM)) == Alltrim(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+PADL(ZZ1->ZZ1_NUMERO,9,'0'))
					cParc:= SE1->E1_PARCELA
					nTotD+=	SE1->E1_DECRESC
					SE1->(dbSkip())
				Enddo
			Endif
			If !empty(cParc)
				cParc:= soma1(cParc)
			Endif

			if nDesc > 0 //Existe produto Kit com desconto
				nValDsc:= Round(((nDesc-nTotD) / (len(aCondicao))),2) //Rateia desconto
			endif

			cQuery := ""
			cQuery += " SELECT SUM(ZZ1_VLRLIQ) AS ADIANT "
			cQuery += "   FROM " + RETSQLNAME("ZZ1") + " ZZ1 "
			cQuery += "  INNER JOIN " + RETSQLNAME("SF4") + " SF4 "
			cQuery += "     ON SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) = SF4.F4_FILIAL "
			cQuery += "    AND ZZ1.ZZ1_TES = SF4.F4_CODIGO "
			cQuery += "   WHERE ZZ1.ZZ1_FILIAL = '" + ZZ0->ZZ0_FILIAL + "' "
			cQuery += "     AND ZZ1.ZZ1_NUMERO = '" + ZZ0->ZZ0_NUMERO + "' "
			cQuery += "   	AND ZZ1.ZZ1_CLIENT = '" + ZZ0->ZZ0_CLIENT + "' "
			cQuery += "     AND ZZ1.ZZ1_LOJA = '" + ZZ0->ZZ0_LOJA + "' "
			cQuery += "     AND ZZ1.D_E_L_E_T_ = '' "
			cQuery += "     AND SF4.D_E_L_E_T_ = '' " 
			cQuery += "     AND (SF4.F4_DUPLIC = 'S' "

			for nY := 1 to len(StrTokArr(GETMV("SA_TESEXCE"),"|"))
				cQuery += "  OR ZZ1.ZZ1_TES = '"+StrTokArr(GETMV("SA_TESEXCE"),"|")[nY]+"' "
			next
			cQuery += " )"

			cQuery := ChangeQuery(cQuery)

			DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"XZZ1A")		

			cQuery2 := ""
			cQuery2 += " SELECT SUM(ZZ1_VLRLIQ) AS DECRE "
			cQuery2 += "   FROM " + RETSQLNAME("ZZ1") + " ZZ1 "
			cQuery2 += "  INNER JOIN " + RETSQLNAME("SF4") + " SF4 "
			cQuery2 += "     ON SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) = SF4.F4_FILIAL "
			cQuery2 += "    AND ZZ1.ZZ1_TES = SF4.F4_CODIGO "
			cQuery2 += "   WHERE ZZ1.ZZ1_FILIAL = '" + ZZ0->ZZ0_FILIAL + "' "
			cQuery2 += "     AND ZZ1.ZZ1_NUMERO = '" + ZZ0->ZZ0_NUMERO + "' "
			cQuery2 += "   	AND ZZ1.ZZ1_CLIENT = '" + ZZ0->ZZ0_CLIENT + "' "
			cQuery2 += "     AND ZZ1.ZZ1_LOJA = '" + ZZ0->ZZ0_LOJA + "' "
			cQuery2 += "     AND ZZ1.D_E_L_E_T_ = '' "
			cQuery2 += "     AND SF4.D_E_L_E_T_ = '' " 
			cQuery2 += "     AND SF4.F4_ESTOQUE = 'N' "

			cQuery2 := ChangeQuery(cQuery2)

			DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery2),"XZZ1B")

			nFrete := NOROUND( (XZZ1A->ADIANT - XZZ1B->DECRE) * (ZZ0->ZZ0_FRTCLI / 100 ) ,2)

			XZZ1A->(DBCLOSEAREA())
			XZZ1B->(DBCLOSEAREA())

			For nCont:=1 to Len(aCondicao)
				aDados	:= {}
				AADD(aDados,{"E1_PREFIXO"	,"A"+SUBSTR(CFILANT,5,2),nil})
				AADD(aDados,{"E1_NUM"		,PADL(ZZ0->ZZ0_NUMERO+SubStr(ZZ0->ZZ0_ANOCOMP,2),9,'0')		,nil})
				AADD(aDados,{"E1_TIPO"		,"ADT"					,nil})
				AADD(aDados,{"E1_PARCELA"	,RetAsc(nCont,GetSx3Cache("E1_PARCELA","X3_TAMANHO"),.T.),nil})
				AADD(aDados,{"E1_NATUREZ"	,ZZ0->ZZ0_NATUREZ		,nil})
				AADD(aDados,{"E1_CLIENTE"	,ZZ0->ZZ0_CLIENT		,nil})
				AADD(aDados,{"E1_LOJA"		,ZZ0->ZZ0_LOJA			,nil})
				AADD(aDados,{"E1_HIST"		,'CONTRATO: '+ZZ0->ZZ0_NUMERO + "Tipo do Titulo: Adiantamento",nil})
				AADD(aDados,{"E1_CCC"		,GetMv("SA_CCC")		,nil})
				AADD(aDados,{"E1_VENCTO"		,aCondicao[nCont][1]	,nil})
				AADD(aDados,{"E1_YCONTRA"		,ZZ0->ZZ0_NUMERO,nil})
				AADD(aDados,{"E1_YFILIAL"		,ZZ0->ZZ0_FILIAL,nil})					
				AADD(aDados,{"E1_VALOR"		,aCondicao[nCont][2]	,nil})
				AADD(aDados,{"E1_YVLFRET"		, NOROUND(nFrete / Len(aCondicao),2) 	,nil})
				AADD(aDados,{"E1_YCONTRA",ZZ0->ZZ0_NUMERO	,nil})
				lMsErroAuto	:= .F.
				MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
				If lMsErroAuto
					DisarmTransaction()
					AutoGrLog("Erro ao incluir o titulo!")
					AutoGrLog("Opera��o cancelada!")
					MostraErro()
					Return .F.
				Else
					nCount++					
				Endif
			Next
		End Transaction

	EndIf

	If nCount > 0

		cMsg := ""
		cMsg += "Titulo gerado no contas a receber: " + CHR(13) + CHR(10)
		cMsg += "Filial: " + XFILIAL("SE1") + CHR(13) + CHR(10)
		cMsg += "No. Titulo: " + PADL(ZZ0->ZZ0_NUMERO+ZZ0->ZZ0_ANOCOMP,9,'0') + CHR(13) + CHR(10)
		cMsg += "Cliente: " +  ZZ0->ZZ0_CLIENT + "-" + POSICIONE("SA1",1,XFILIAL("SA1") + ZZ0->ZZ0_CLIENT,"SA1->A1_NREDUZ")

		MessageBox(cMsg, "TOTVS", 64)

	EndIf

	RestArea(xArea)

	RecLock("ZZ0",.F.)
	ZZ0->ZZ0_APROV	:= __cUserID //cNomeUsr
	ZZ0->ZZ0_HRAPRO	:= cTime
	ZZ0->ZZ0_DTAPROV := DATE()
	ZZ0->ZZ0_STATUS := "L"		
	ZZ0->(MsUnLock())


	cUpdate := " UPDATE "+RetSqlName("ZZ1")+" " //Update da quantidade inicial
	cUpdate += " SET ZZ1_QNTINI = ZZ1_QUANT "
	cUpdate += " FROM "+RetSqlName("ZZ1")+" (NOLOCK) "
	cUpdate += " WHERE ZZ1_FILIAL = '"+ZZ0->ZZ0_FILIAL+"' "
	cUpdate += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' "
	cUpdate += " AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' "
	cUpdate += " AND ZZ1_LOJA = '"+ZZ0->ZZ0_LOJA+"' "
	TCSQLExec(cUpdate) 


Return .T.



// rafael pinheiro - 28/10/2015
User Function YZZ0035()
	Local cContrato	:= ZZ0->ZZ0_NUMERO
	Local aDados		:= {}

	cFlag := "SASP035"

	If ZZ0->ZZ0_STATUS = 'I'
		Alert("Contrato inativado! N�o poder� ser realizada medi��o.")
	ElseIf ZZ0->ZZ0_STATUS = 'B'
		Alert("Contrato bloqueado! N�o poder� ser realizada medi��o.")
	ElseIf !empty(ZZ0->ZZ0_APROV)
		FWExecView ("Medi��o Automatica", "SASP035", MODEL_OPERATION_UPDATE ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
	Else
		Alert("Contrato ainda n�o foi aprovado. Aprove-o e em seguida execute a medi��o.")
	EndIf		

Return
// fim - rafael pinheiro - 28/10/2015

User Function YZZ0002()
	Local cContrato	:= ZZ0->ZZ0_NUMERO
	Local aDados		:= {}

	cFlag := "SASP036"

	If ZZ0->ZZ0_STATUS = 'I'
		Alert("Contrato inativado! N�o poder� ser realizada medi��o.")
	ElseIf ZZ0->ZZ0_STATUS = 'B'
		Alert("Contrato bloqueado! N�o poder� ser realizada medi��o.")
	ElseIf !empty(ZZ0->ZZ0_APROV)
		FWExecView ("Medi��o", "SASP036", MODEL_OPERATION_UPDATE ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
	Else
		Alert("Contrato ainda n�o foi aprovado. Aprove-o e em seguida execute a medi��o.")
	EndIf		

Return

//Aditivo de Contrato
User Function YZZ0003()
	Local cContrato	:= ZZ0->ZZ0_NUMERO
	Local aDados		:= {}	

	//Local aParam1	:= {}
	//Local aRetParm1 := {}
	//Local lOk1		:= .F.

	Public lOk		:= .F.
	Public aParam	:= {}
	Public aRetParm	:= {}	

	Public cProdIni	:= Space(GetSx3Cache("B1_COD","X3_TAMANHO"))
	Public cProdFim	:= Space(GetSx3Cache("B1_COD","X3_TAMANHO"))
	Public cTes		:= Space(GetSx3Cache("ZZ1_TES","X3_TAMANHO"))
	Public cSerie	:= Space(GetSx3Cache("B1_YSERIE","X3_TAMANHO"))
	Public cVolume	:= Space(GetSx3Cache("B1_YVOL","X3_TAMANHO"))
	Public aTipoAdd	:= {'Quantidade / Financeiro','Produto'}



	aAdd(aParam,{1,"Itens De"		,cProdIni	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{1,"Itens Ate"	,cProdFim	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{1,"Tes"			,cTes		,GetSx3Cache("SF4_CODIGO","X3_PICTURE"),".T.","SF4",".T.",80,.F.})
	aAdd(aParam,{1,"Serie"				,cSerie	,GetSx3Cache("B1_YSERIE","X3_PICTURE"),".T.","ZZO",".T.",80,.F.})
	aAdd(aParam,{1,"Envio"				,cVolume ,GetSx3Cache("B1_YVOL","X3_PICTURE"),".T.","Z3",".T.",80,.F.})
	aAdd(aParam,{2,"Tipo de Aditivo" ,,aTipoAdd,80,,.T.})

	cFlag := 'YZZ0003'

	If ZZ0->ZZ0_STATUS = 'I'
		Alert("Contrato inativado! N�o poder� ser realizada medi��o.")
	ElseIf ZZ0->ZZ0_STATUS = 'B'
		Alert("Contrato bloqueado! N�o poder� ser realizada medi��o.")
	ElseIf !empty(ZZ0->ZZ0_APROV)
		If ParamBox(aParam,"Filtrar Aditivo",@aRetParm,{||.T.},,,,,,"U_SASP042",.T.,.T.)
			FWExecView ("Aditivo", "SASP042", MODEL_OPERATION_UPDATE ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
		EndIf	
	Else
		Alert("Contrato ainda n�o foi aprovado. Aprove-o e em seguida execute a adi��o.")
	EndIf	


Return

//Desbloqueio de Contrato
User Function YZZ0007()

	if ZZ0_STATUS== 'B'
		If MsgYesNo("Deseja desbloquear o contrato "+ZZ0->ZZ0_NUMERO+" ?")
			RecLock('ZZ0',.F.)
			ZZ0->ZZ0_STATUS	:=	'L'
			MsUnlock()
			Aviso("Aten��o","Contrato "+ZZ0->ZZ0_NUMERO+" foi desbloqueado.",{"Ok"})
		Endif
	ELSE
		Alert("Somente contrato bloqueado pode ser desbloqueado.")
	EndIf		

	Return

	//Encerramento de Contrato
	/*
	User Function YZZ0009()

	Local cPerg 	:= "YZZ9C"
	Local cUpdate
	Local nRet
	Local cFilAn


	//Verifica se tem medi��o em aberta para o contrato
	cSelec:= "SELECT COUNT(*) QTD FROM "+RetSqlName("ZZ2")+" ZZ2"
	cSelec+= " WHERE ZZ2.D_E_L_E_T_ =' ' AND "
	cSelec+= " ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_CLIENT+ZZ2_LOJA = '"+ZZ0->(ZZ0_FILIAL+ZZ0_NUMERO+ZZ0_CLIENT+ZZ0_LOJA)+"' AND "
	cSelec+= " ZZ2_PV01 <> ' ' AND "
	cSelec+= " ZZ2_PV01NF = ' '  "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cSelec),"XQ031A",.F.,.T.)

	If XQ031A->QTD > 0
	XQ031A->(dbCloseArea())
	Aviso("Aten��o","Contrato "+ZZ0->ZZ0_NUMERO+" tem medi��es em aberto," +chr(13)+chr(10)+;
	" n�o podendo ser inativado ou transferido",{"Ok"})
	Return
	Endif

	XQ031A->(dbCloseArea())

	AjustaSX1(cPerg)

	If Pergunte(cPerg,.T.)

	cFilDest:= mv_par03

	cQuery := "SELECT *  "
	cQuery += "FROM "+RETSQLNAME("ZZ1")+" AS ZZ1 "
	cQuery += " WHERE ZZ1_FILIAL = '"+ZZ0->ZZ0_FILIAL+"' AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' AND  D_E_L_E_T_ = '' "
	cQuery += " AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' AND ZZ1_LOJA = '"+ZZ0->ZZ0_LOJA+"' "
	cQuery += " AND ZZ1_SALDO > 0"


	if Select("T01") > 0
	dbSelectArea("T01")
	dbCloseArea()
	endif

	TcQuery cQuery New Alias T01
	T01->(dbCloseArea())

	/*If mv_par01 == 1 //Inativa��o

	RecLock("ZZ0",.F.)
	ZZ0->ZZ0_STATUS 	:= 'I'					
	ZZ0->ZZ0_OBSERV 	:= ZZ0->ZZ0_OBSERV + " " + Mv_Par02				
	ZZ0->(MsUnLock())				


	Aviso("Aten��o","Contrato "+ZZ0->ZZ0_NUMERO+" inativado!!!",{"Ok"})
	*/
	/*
	If mv_par01 == 2 //Transfer�ncia

	cFlag := "SASP031"
	nRet  := FWExecView ("Medi��o", "SASP031", MODEL_OPERATION_INSERT,/*oDlg*///, {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )

	/*
	If nRet == 0

	cFilAn := ZZ0->ZZ0_FILIAL

	DBSELECTARE("ZZ0")
	dbSetOrder(1)  
	IF dbSeek(ZZ0->ZZ0_FILIAL+ZZ0->ZZ0_NUMERO+ZZ0->ZZ0_CLIENT+ZZ0->ZZ0_LOJA)
	RecLock("ZZ0",.F.)
	ZZ0->ZZ0_FILIAL 	:= cFilDest									
	ZZ0->(MsUnLock())

	ENDIF

	cUpdate := ""
	cUpdate := " UPDATE " + RETSQLNAME("ZZ1")
	cUpdate += "    SET ZZ1_FILIAL = '"+cFilDest+"' "
	cUpdate += "  WHERE D_E_L_E_T_ = '' "
	cUpdate += "    AND ZZ1_FILIAL = '" + cFilAn + "' "
	cUpdate += "    AND ZZ1_NUMERO = '" + ZZ0->ZZ0_NUMERO + "' "

	TCSQLEXEC(cUpdate)

	ENDIF

	EndIf
	endif
	*/	
Return


Static Function MedicaoAut(cEmpCon,cCliIni,cLjIni,cCliFim,cLjFim,cProdIni,cProdFim,nQuant)
	Local oModel,nCont
	Local cTmp	:= GetNextAlias()
	Local cQuery
	Default nQuant	:= 0
	ProcRegua(10)
	ProcessMessage() //ATUALIZA CLIENTE

	cQuery := ""
	cQuery += "SELECT R_E_C_N_O_ AS REGISTRO FROM "+RetSqlName( 'ZZ0' ) + " ZZ0 "
	cQuery += " WHERE ZZ0_FILIAL='"+ cEmpCon + "'"
	cQuery += " AND ZZ0_APROV<>'"+SPACE(GetSx3Cache("ZZ0_APROV","X3_TAMANHO"))+"' "
	//cQuery += " AND Z1_SALDO>0 "
	cQuery += " AND ZZ0_CLIENT>='"+cCliIni+"' AND ZZ0_CLIENT0<='"+cCliFim+"'
	cQuery += " AND ZZ0_LOJA>='"+cLjIni+"' AND ZZ0_LOJA<='"+cLjFim+"'"
	cQuery += " AND ZZ0.D_E_L_E_T_=' '"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.F.,.T.)
	ZZ0->(DbSetOrder(1))	//Z1_FILIAL+Z1_CONTRAT
	While (cTmp)->(!EOF())
		ZZ0->(DBGOTO((cTmp)->REGISTRO))
		IncProc("Contrato: "+ZZ0->Z0_CONTRAT)
		ProcessMessage() //ATUALIZA CLIENTE
		lEdit			:= .F.
		oModel 		:= FWLoadModel( 'CADZZ2' )
		oModel:SetOperation( MODEL_OPERATION_UPDATE )
		lRet := oModel:Activate()
		oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')
		If lRet
			For nCont := 1 To oModelZZ3:Length()
				oModelZZ3:GoLine(nCont)
				If oModelZZ3:IsDeleted()
					Loop
				EndIf
				cCodPro	:= Posicione("SB1",1,xFilial("SB1")+oModelZZ2:GetValue("ZZ3_PRODUT"),"B1_COD")
				If !(cCodPro>=cProdIni .AND. cCodPro<=cProdFim)	//Filtro de Grupo de,ate
					Loop
				EndIf
				If !oModelSZ3:SetValue("ZZ3_QUANT", oModelSZ3:GetValue("ZZ3_SALDO") )
					Exit
				Else
					lEdit	:= .T.
				EndIf
			Next
			If !lEdit
				oModel:DeActivate()
				(cTmp)->(DbSkip())
				Loop
			EndIf
		EndIf
		If lRet .and. ( lRet := oModel:VldData() )
			oModel:CommitData()
		EndIf
		If !lRet
			aErro   := oModel:GetErrorMessage()
			AutoGrLog( "Id do formul�rio de origem:" + ' [' + AllToChar( aErro[1]  ) + ']' )
			AutoGrLog( "Id do campo de origem:     " + ' [' + AllToChar( aErro[2]  ) + ']' )
			AutoGrLog( "Id do formul�rio de erro:  " + ' [' + AllToChar( aErro[3]  ) + ']' )
			AutoGrLog( "Id do campo de erro:       " + ' [' + AllToChar( aErro[4]  ) + ']' )
			AutoGrLog( "Id do erro:                " + ' [' + AllToChar( aErro[5]  ) + ']' )
			AutoGrLog( "Mensagem do erro:          " + ' [' + AllToChar( aErro[6]  ) + ']' )
			AutoGrLog( "Mensagem da solu��o:       " + ' [' + AllToChar( aErro[7]  ) + ']' )
			AutoGrLog( "Valor atribu�do:           " + ' [' + AllToChar( aErro[8]  ) + ']' )
			AutoGrLog( "Valor anterior:            " + ' [' + AllToChar( aErro[9]  ) + ']' )
		Else
			nQuant++
		EndIf
		oModel:DeActivate()
		If !lRet
			MostraErro()
			Return .F.
		EndIf
		(cTmp)->(DbSkip())
	EndDo
	(cTmp)->(DbCloseArea())
Return .T.

Static Function PrecoTab(cProduto)
	Local cTabPad		:= SuperGetMV("MV_TABPAD")
	Local oModel		:= FWModelActive()
	Local lKit			:= ExistCpo("ZZ7",cProduto,1,,.F.)
	Local nValorTab	:= 0
	SA1->(dbSetOrder(1))	//A1_FILIAL+A1_COD
	SA1->(dbSeek(xFilial("SA1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')))
	cTabPad	:= SA1->A1_TABELA

	If lKit

		If SA1->(dbSeek(xFilial("SA1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))) .AND. !Empty(SA1->A1_Tabela)
			cTabPad	:= SA1->A1_TABELA
		EndIf

		ZZ7->(DbSetOrder(1))
		ZZ7->(DbSeek(xFilial("ZZ7")+cProduto))
		While ZZ7->(!EOF()) .AND. xFilial("ZZ7")+cProduto==ZZ7->(ZZ7_FILIAL+ZZ7_CODIGO)
			nValorTab += ZZ7->ZZ7_QUANT*MaTabPrVen(cTabPad;
			,ZZ7->ZZ7_CODIGO;
			,1;
			,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT');
			,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))
			ZZ7->(DbSkip())
		EndDo
	Else
		nValorTab	:= MaTabPrVen(cTabPad;
		,cProduto;
		,1;
		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT');
		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))
	EndIf
Return nValorTab

Static Function ZZ0ValProd(oModelZZ1)
	Local lRet := ExistCpo("ZZ7",oModelZZ1:GetValue('ZZ1_PRODUT'),1,,.F.).OR.ExistCpo("SB1",oModelZZ1:GetValue('ZZ1_PRODUT'),1,,.F.)
	Local oModel := FWModelActive()
	If !lRet
		oModel:SetErrorMessage('ZZ1DETAIL',"ZZ1_PRODUT",,,"ATEN��O", 'Produto/Kit n�o encontrado', 'Verifique se o Produto ou Kit esta cadastrado!')
	Else
		oModel:GetModel('ZZ1DETAIL'):LoadValue('ZZ1_QUANT',0)
		oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_VLRUN',PrecoTab(oModelZZ1:GetValue('ZZ1_PRODUT')))
		oModel:GetModel('ZZ1DETAIL'):LoadValue('ZZ1_VLRTOT',0)
		oModel:GetModel('ZZ1DETAIL'):LoadValue('ZZ1_TES',Space(GetSx3Cache("ZZ1_TES","X3_TAMANHO")))
	EndIf
Return lRet

//Fun��o SASP034 - Fun��o para retornar o kit - Arvore.
Static Function ProcKit(oView)	

	U_SASP034('KIT')

Return

//Visualiza��o pelo F7 do KIT
Static Function VISKit

	Local oModel:= FWModelActive()

	U_SASP034(''," ( (ZZ7_CODIGO = '"+oModel:GetModel('ZZ1DETAIL'):GetValue('ZZ1_PRODUT')+"') OR (ZZ7_CODPAI = '"+oModel:GetModel('ZZ1DETAIL'):GetValue('ZZ1_PRODUT')+"'))")

Return

//A vari�vel LPrimeiro, � um "flag" para verificar se � a primeira chamada da fun��o recursiva iniciada no fonte SASP034.
User function YZZ0005(cCodKit2, aRet,lPrimeiro)

	Local oModel := FWModelActive()
	Local cCodKit	:= cCodKit2
	Local cTipo :=  ""
	Local cDesc := ""
	Local cQuery := ""
	Local cQuery9 := ""
	Local oModelZZ1 := oModel:GetModel( 'ZZ1DETAIL' )
	Local nRegistros := 0
	Local cAliasT := GetNextAlias()	
	Local nRateio := 0
	Local nPerDes := 0
	Local cQuery8 := ""
	Local nValorDes := 0
	Local oView := FWViewActive()

	//Avo - Sintetico
	//   Pai - Sintetico
	//     Filho - Sintetico
	//     Filho - Analitico	
	//Retorna somente o filho sint�tico.

	If !Empty(cCodKit)

		cTipo := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_TIPO")

		If cTipo == 'S' 	

			cQuery := ""
			cQuery += " SELECT ZZ7_CODIGO, ZZ7_DESCR "
			cQuery += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7 "
			cQuery += "  WHERE D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7_CODPAI = '" + cCodKit +  "' "
			cQuery += "    AND ZZ7_TIPO = 'S' "
			cQuery += "    AND ZZ7_CODIGO <> '" + cCodKit + "' "   

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasT,.F.,.T.)

			Count To nRegistros

			(cAliasT)->(DBGOTOP())

			if nRegistros > 0

				While (cAliasT)->(!EOF())												

					U_YZZ0005((cAliasT)->ZZ7_CODIGO, 0,.F.)								

					(cAliasT)->(DBSKIP())				

				EndDo

				(cAliasT)->(DBCLOSEAREA())
			Else
				//TODO kits!

				cQuery8 := ""
				cQuery8 += " SELECT COUNT(*) AS QTD "
				cQuery8 += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7"
				cQuery8 += "  WHERE D_E_L_E_T_ = '' "
				cQuery8 += "    AND ZZ7.ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
				cQuery8 += "    AND ZZ7.ZZ7_CODPAI = '" + cCodKit + "' "
				cQuery8 += "    AND ZZ7.ZZ7_PERDSC = 100 "  

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery8),"PERCDES",.F.,.T.)

				cDesc := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_DESCR" )
				bProf := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_CATEGO" )

				If PERCDES->QTD > 0					

					nPerDes := PERCDES->QTD		

				Else

					nPerDes := 0

				EndIf

				PERCDES->(DBCLOSEAREA())

				//Codigo do kit, Descri��o do item, Quantidade, TES, Valor unitario, Tipo(k = Kit), Percentual Desconto
				AADD(aKit, {cCodKit,cDesc,1,"",1,'K',nPerDes})

				(cAliasT)->(DBCLOSEAREA())

			EndIf

		Else		
			Alert("Produto selecionado n�o � um kit. O tipo do produto deve ser SINT�TICO.")
		EndIf

	EndIf

	//Se nValor for booleano == .F. , o usu�rio deve ter clicado em Cancelar no ParamBox.	
	if ValType(aRet) == 'L'

		nValor := 1

	EndIf

	If lPrimeiro == .T. 

		For nX := 1 to len(aKit)				

			cQuery := ""
			cQuery += " SELECT ZZK_PORCEN "
			cQuery += "   FROM " + RETSQLNAME("ZZK") + " ZZK "
			cQuery += "  INNER JOIN  " + RETSQLNAME("ZZ7") + " ZZ7 "
			cQuery += "     ON ZZK.ZZK_FILIAL = ZZ7.ZZ7_FILIAL "
			cQuery += "    AND ZZK.ZZK_SERIE = ZZ7.ZZ7_SERIE "
			cQuery += "    AND ZZK.ZZK_ENVIO = ZZ7.ZZ7_ENVIO "
			cQuery += "  WHERE ZZK.D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7.D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7.ZZ7_CODIGO = '" + aKit[nX][1] + "' "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"PERCRAT",.F.,.T.)

			If PERCRAT->(!EOF())

				nRateio := ROUND(aRet[1] * (PERCRAT->ZZK_PORCEN / 100),2)

			Else

				Alert("O kit " + aKit[nX][1] + " n�o possui porcentagem de rateio definido, valor do produto ser� R$ 1. Verifique o cadastro do kit e tente novamente." )
				nRateio := 1

			EndIf

			PERCRAT->(DBCLOSEAREA())						

			If aKit[nX][7] > 0
				nValorDes := aKit[nX][7] * GETMV("SA_PRECDSC")

			Else

				nValorDes := 0

			EndIf

			// TODO KIT PROFESSOR
			// Inicio - Rafael Pinheiro 
			// Data: 05/10/2015
			// Objetivo: Kit do professor valor fixo de R$ 10,00

			if Select("QSASPrf") > 0
				dbCloseArea("QSASPrf")
			endif

			nProf = 0

			cQrySAS := ""
			cQrySAS += " SELECT COUNT(*) AS QTD "
			cQrySAS += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7"
			cQrySAS += "  WHERE D_E_L_E_T_ = '' "
			cQrySAS += "    AND ZZ7.ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
			cQrySAS += "    AND ZZ7.ZZ7_CODPAI = '" + aKit[nX][1] + "' "
			cQrySAS += "    AND ZZ7.ZZ7_CATEGO = 'P' "  

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySAS),"QSASPrf",.F.,.T.)

			nProf := QSASPrf->QTD

			if nProf > 0
				nRateio := nProf * 10
				nValorDes := 0
			endif

			dbCloseArea("QSASPrf")

			// FIM - Rafael Pinheiro

			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_PRODUT',aKit[nX][1])
			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_DESC',aKit[nX][2])
			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_QUANT',aKit[nX][3])
			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_TES',aRet[2])
			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_VLRDSC',nValorDes)


			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_VLRUN',nRateio + nValorDes)	


			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_TIPO',aKit[nX][6])
			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_VLRTOT',nRateio + nValorDes)				

			If nX < len(aKit) 
				oModelZZ1:AddLine()
				oView:Refresh()
			EndIf

			GETDREFRESH()
			nDifer := 0

		next nX

		aKit := {}

	EndIf


Return

Static Function ZZ0ValTES(oModelZZ1)
	Local lRet := .T.
	Local oModel := FWModelActive()
	Local oModelZZ0 := oModel:GETMODEL("ZZ0MASTER")
	If !ExistCpo("SF4",oModelZZ1:GetValue('ZZ1_TES'),1,,.F.)	//N�o existi TES
		lRet	:= .F.
		oModel:SetErrorMessage('ZZ1DETAIL',"ZZ1_TES",,,"ATEN��O", 'Tes n�o encontrado', 'Verifique se a TES est� cadastrada!')
	ElseIf oModelZZ1:GetValue('ZZ1_TES') <= "500"
		lRet	:= .F.
		oModel:SetErrorMessage('ZZ1DETAIL',"ZZ1_TES",,,"ATEN��O", 'A Tes informada n�o � uma valida para saida', 'Verifique se a TES � de saida!')
	EndIf

	IF oModelZZ0:GetValue('ZZ0_CONFNF') == '1' .AND. oModelZZ1:GetValue('ZZ1_TES') == '502'

		MsgAlert("Para clientes conforme nota N�O, utilizar a TES 581! TES 501=582 e 502=581","ATEN��O!!!")
		lRet	:= .F.
	ELSEIF oModelZZ0:GetValue('ZZ0_CONFNF') == '1' .AND. oModelZZ1:GetValue('ZZ1_TES') == '501'

		MsgAlert("Para clientes conforme nota N�O, utilizar a TES 582! TES 501=582 e 502=581","ATEN��O!!!")
		lRet	:= .F.

	ENDIF
Return lRet


//Desconto do kit
Static Function FDescZ1(cContr,cCli,cLoj)
	Local nRet		:= 0
	Local cTabPad	:= SuperGetMV("MV_TABPAD")

	If SA1->(dbSeek(xFilial("SA1")+cCli+cLoj)) .AND. !Empty(SA1->A1_Tabela)
		cTabPad	:= SA1->A1_TABELA
	EndIf

	dbSelectArea("ZZ1")
	ZZ1->(dbSetOrder(1))
	ZZ1->(dbSeek(xFilial("ZZ1")+cContr))

	while ZZ1->(!eof()) .and. Alltrim(xFilial("ZZ1")+cContr) == Alltrim(ZZ1->ZZ1_FILIAL+ZZ1->ZZ1_NUMERO)

		lKit:= ExistCpo("ZZ7",ZZ7->ZZ7_CODIGO,1,,.F.)

		If lKit
			ZZ7->(DbSetOrder(1))
			ZZ7->(DbSeek(xFilial("ZZ7")+Alltrim(ZZ1->ZZ1_PRODUT)))
			While ZZ7->(!EOF()) .AND. Alltrim(xFilial("ZZ7")+ZZ1->ZZ1_PRODUT)==Alltrim(ZZ7->(ZZ7_FILIAL+ZZ7_CODIGO))
				nVlrItem	:= MaTabPrVen(cTabPad;
				,ZZ7->ZZ7_PRODUT;
				,1;
				,cCli;
				,cLoj)

				//Verifica se tem desconto
				If ZZ7->ZZ7_PERDSC > 0
					nRet+= (( nVlrItem*ZZ7->ZZ7_QUANT*ZZ1->ZZ1_QUANT)*(ZZ7->ZZ7_PERDSC/100))
				Endif
				ZZ7->(dbSkip())
			Enddo
		Endif
		ZZ1->(dbSkip())
	Enddo

Return nRet

/*/{Protheus.doc} RETDSCZZ0
Retorna Desconto conforme Kit de Produtos
@author Diogo
@since 22/09/2015
@version 1.0
/*/
User Function RETDSCZZ0(oModelZZ1)
	Local nRet:= 0
	Local cTabPad	:= SuperGetMV("MV_TABPAD")
	Local aAreaZ1	:= GetArea()
	Local oModel	:= FWModelActive()

	if Empty(oModel:GetValue("ZZ0MASTER","ZZ0_CLIENT"))
		Return nRet
	endif

	lKit:= ExistCpo("ZZ7",oModelZZ1:GetValue("ZZ1_PRODUT"),1,,.F.)
	If SA1->(dbSeek(xFilial("SA1")+oModel:GetValue("ZZ0MASTER","ZZ0_CLIENT")+oModel:GetValue("ZZ0MASTER","ZZ0_LOJA"))) .AND. !Empty(SA1->A1_Tabela)
		cTabPad	:= SA1->A1_TABELA
	EndIf

	If lKit
		ZZ7->(DbSetOrder(1))
		ZZ7->(DbSeek(xFilial("ZZ7")+Alltrim(oModelZZ1:GetValue("ZZ1_PRODUT"))))
		While ZZ7->(!EOF()) .AND. Alltrim(xFilial("ZZ7")+oModelZZ1:GetValue("ZZ1_PRODUT"))==Alltrim(ZZ7->(ZZ7_FILIAL+ZZ7_CODIGO	))
			nVlrItem	:= MaTabPrVen(cTabPad;
			,ZZ7->ZZ7_PRODUT;
			,1;
			,oModel:GetValue("ZZ0MASTER","ZZ0_CLIENT");
			,oModel:GetValue("ZZ0MASTER","ZZ0_LOJA"))

			If ZZ7->ZZ7_PERDSC > 0
				nRet+= (( nVlrItem*ZZ7->ZZ7_QUANT*oModelZZ1:GetValue("ZZ1_QUANT"))*(ZZ7->ZZ7_PERDSC/100))
			Endif
			ZZ7->(dbSkip())
		Enddo
	Endif

	RestArea(aAreaZ1)

Return nRet

Static Function GetCacheSF4(cCodTES,cCampo,xRet)
	Local lRet	:= .F.
	Local nPos	:= aScan(aCacheTES,{|x| x[1]==cCodTES .and. x[2]==cCampo})
	If nPos>0
		xRet	:=	aCacheTES[nPos][3]
		Return .T.
	EndIf
	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
	If SF4->(DbSeek(xFilial("SF4")+cCodTES))
		AADD(aCacheTES,{cCodTES,cCampo,SF4->(&cCampo)})
		lRet	:= .T.
		xRet	:= SF4->(&cCampo)
	EndIf
Return lRet

User Function YZZ0008()
	Local oBrowse
	Local aLegenda	:= {}
	Local nCont
	Private cCadastro	:= "Adiantamentos"
	Private aRotina := {}

	//rafael pinheiro - 28/10/2015

	If (ZZ0->ZZ0_STATUS == 'A')
		Alert("Contrato ainda n�o aprovado.")
		Return
	EndIf		

	// fim - rafael pinheiro - 28/10/2015


	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('SE1')
	oBrowse:SetDescription('Adiantamentos')
	oBrowse:DisableDetails()
	oBrowse:SetMenuDef( '' )
	//oBrowse:SetFilterDefault("E1_FILIAL=='"+xFilial("SE1", ZZ0->ZZ0_FILIAL)+"' .AND. E1_NUM='"+PADL(ZZ0->ZZ0_NUMERO,GetSx3Cache("E1_NUM","X3_TAMANHO"),'0')+"' .AND. E1_TIPO $ 'ADT|NCC|'")
	oBrowse:SetFilterDefault("E1_YCONTRA == '" + ZZ0->ZZ0_NUMERO + "' .AND. E1_YFILIAL == '" + ZZ0->ZZ0_FILIAL +"' .AND. E1_TIPO $ 'ADT|NCC|'")
	oBrowse:AddButton("Legenda","Fa040Legenda",0,2,0)
	aLegenda	:= Fa040Legenda("SE1")
	For nCont:=1 to Len(aLegenda)
		oBrowse:AddLegend( aLegenda[nCont][1], aLegenda[nCont][2], "")
	Next
	oBrowse:Activate()
Return

//Bloqueio do Contrato
User Function YZZ0006()

	If ZZ0_STATUS== 'E'
		Alert("Contrato encerrado! N�o poder� ser bloqueado.")
	ELSEIF ZZ0_STATUS== 'A'
		Alert("Contrato n�o aprovado! N�o poder� ser bloqueado.")
	Else
		If MsgYesNo("Deseja bloquear o contrato "+ZZ0->ZZ0_NUMERO+" ?")
			RecLock('ZZ0',.F.)
			ZZ0->ZZ0_STATUS	:=	'B'
			MsUnlock()
			Aviso("Aten��o","Contrato "+ZZ0->ZZ0_NUMERO+" foi bloqueado.",{"Ok"})
		Endif
	EndIf		

Return


Static Function AjustaSX1(cPerg)

	Local aHelp := {"Ativa / Inativa item a partir da sele��o"}

	PutSx1(cPerg, "01","Tipo de Inativa��o?",		  		 "","","mv_ch01","C",01,0,0,"C","","   ","","","mv_par01","1-Inativa��o","1-Inativa��o","1-Inativa��o","2-Transfer�ncia","2-Transfer�ncia","2-Transfer�ncia",,,,"","","","",,"","")
	PutSx1(cPerg, "02","Observa��o?",				  		 "","","mv_ch02","C",50,0,0,"G","",,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","Filial Destino: ",				  		 "","","mv_ch03","C",6,0,0,"G","","SM0","","","mv_par03"," ","","","","","","","","","","","","","","","")

Return


User Function Data031(nAdiant, dDataPgto)

	Local lOk := .F.	

	If nAdiant > 0 .AND. Empty(dtoc(dDataPgto))

		Alert("O valor de adiantamento � maior que zero, o campo de data de pagamento deve ser preenchido para que seja gerado titulo no contas a receber.")
		lOk := .F.	
	Else

		lOk := .T.

	EndIf

Return lOk

User Function AnoCom()

	Local cAno := cValtoChar(Year(DATE())) + '=' + cValtoChar(Year(DATE())) + ';'

	For nX:= 1 To 10

		cAno += cValTochar(Year(DATE()) + nX) + '=' + cValtochar(Year(DATE())+ nX) + ';'

	Next nX

Return cAno


/* Incluido por Weskley Silva 
Data: 22/01/2016
Titulo: Recalculo do valor de desconto no Contrato
*/

User Function Recalc()

	Local cContrato := ZZ0->ZZ0_NUMERO 
	Local cFil := ZZ0->ZZ0_FILIAL
	Local cStatus := ZZ0->ZZ0_APROV
	Local nValDesc := 0
	Local nAdiant := 0
	Local nEstoque := 0
	Local cClient := ZZ0->ZZ0_CLIENT
	Local cLoja := ZZ0->ZZ0_LOJA
	Local cMsg := "Existem Item(s) com valor(es) revisado(s)."+ CHR(10) + CHR(13)
	Local bMsg := .F.
	Local oModel	:= FWModelActive()


	IF !EMPTY(cStatus) 

		MsgInfo("Contrato Aprovado n�o podera ser recalculado","SAS")
		return

	ELSE

		dbSelectArea("ZZ1")
		dbSetOrder(2)

		if ZZ1->(dbSeek(cFil+cContrato+cClient+cLoja))

			While !ZZ1->(eof()) .and. ZZ1->(ZZ1_FILIAL+ZZ1_NUMERO+ZZ1_CLIENT+ZZ1_LOJA) == cFil+cContrato+cClient+cLoja

				nValDesc := QtdSuple(ZZ1->ZZ1_PRODUT)
				nValUni := ZZ1->ZZ1_VLRUN 

				if ZZ1->ZZ1_VLRLIQ != ZZ1->ZZ1_VLRTOT - (nValDesc * ZZ1->ZZ1_QUANT) .or. nValDesc != ZZ1->ZZ1_VLRDSC

					//TODO  Incluido por Weskley Silva 22/01/2016
					IF nValDesc > 	ZZ1->ZZ1_VLRDSC 
						nValUni := ZZ1->ZZ1_VLRUN + (nValDesc - ZZ1->ZZ1_VLRDSC) 
					ELSEIF nValDesc < ZZ1->ZZ1_VLRDSC 
						nValUni := ZZ1->ZZ1_VLRUN - (ZZ1->ZZ1_VLRDSC - nValDesc)
					ENDIF

					cMsg += 'Desconto do item:'+ZZ1->ZZ1_ITEM+' foi revisado de '+cValToChar(ZZ1->ZZ1_VLRDSC)+' para '+cValToChar(nValDesc) +'.' + CHR(10) + CHR(13)
					RecLock("ZZ1",.F.)
					ZZ1->ZZ1_VLRDSC := nValDesc
					ZZ1->ZZ1_VLRUN  := nValUni
					ZZ1->ZZ1_VLRTOT := ZZ1->ZZ1_QUANT * nValUni
					ZZ1->ZZ1_VLRLIQ := ZZ1->ZZ1_VLRTOT - (nValDesc * ZZ1->ZZ1_QUANT)
					MsUnlock()	
					bMsg := .T.			 
				endif
				// FIM 

				If SF4->(DbSeek(xFilial("SF4")+ZZ1->ZZ1_TES))  .AND. (SF4->F4_DUPLIC=="S" .OR.  ZZ1->ZZ1_TES $ GETMV("SA_TESEXCE"))	//Gera Financeiro
					nAdiant	+= ZZ1->ZZ1_VLRLIQ
				EndIf

				// verifica se tes movimenta estoque
				If SF4->(DbSeek(xFilial("SF4")+ZZ1->ZZ1_TES))  .AND. SF4->F4_ESTOQUE=="N"	//Gera estoque
					nEstoque += ZZ1->ZZ1_VLRLIQ
				EndIf


				ZZ1->(dbSkip())
			End Do

			ZZ0Atua(nAdiant,nEstoque,cFil,cContrato,cClient,cLoja)


			if (bMsg)
				MsgInfo(cMsg,"Revis�o")
			endif

		Endif
	ENDIF
Return

Static Function QtdSuple(cKit)

	Local cString
	Local nRet := 0
	Local aQSupAre := GetArea()
	Local nDescont := GETMV("SA_PRECDSC")

	cString := "SELECT" 
	cString += "	COUNT(*) AS QTD" 
	cString += " FROM "
	cString += "	ZZ7010 " 
	cString += " WHERE "
	cString += "	ZZ7_CODPAI='"+cKit+"'" 
	cString += "	AND D_E_L_E_T_=''"
	cString += "	AND ZZ7_SUPLEM='S'"
	cString += "	AND ZZ7_PERDSC='100'"

	IF Select("QSUP") > 0
		QSUP->(dbCloseArea())
	ENDIF

	TCQuery cString New Alias QSUP
	nRet := QSUP->QTD * nDescont
	QSUP->(dbCloseArea())

	RestArea(aQSupAre)

Return nRet

Static Function ZZ0Atua(nAdiant,nEstoque,cFil,cContrato,cClient,cLoja)

	Local aZZ0Are := GetArea()
	Local nFrete := 0
	//Local nAdiant := 0

	IF ZZ0->(dbSeek(cFil+cContrato+cClient+cLoja)) .and. ZZ0->ZZ0_CONFNF == "1"

		nFrete := (nAdiant - nEstoque) * (ZZ0->ZZ0_FRTCLI / 100)
		RecLock("ZZ0",.F.)
		ZZ0->ZZ0_ADIANT := nAdiant + nFrete
		MsUnLock()

	EndIF

	RestArea(aZZ0Are)
Return 


Static Function Aditivos

	Local oModel:= FWModelActive()

	U_SASP006(oModel:GetModel('ZZ1DETAIL'):GetValue('ZZ1_FILIAL'),oModel:GetModel('ZZ1DETAIL'):GetValue('ZZ1_ITEM'),oModel:GetModel('ZZ1DETAIL'):GetValue('ZZ1_NUMERO'),oModel:GetModel('ZZ1DETAIL'):GetValue('ZZ1_CLIENT'),oModel:GetModel('ZZ1DETAIL'):GetValue('ZZ1_LOJA'),oModel:GetModel('ZZ1DETAIL'):GetValue('ZZ1_QNTINI'))

Return
