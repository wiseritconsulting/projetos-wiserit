#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWMBROWSE.CH"
#include "dbtree.ch"
#include "topconn.ch"

/*/{Protheus.doc} SASP042
Aditivo de Contratos
@author Jo�o
@since 22/09/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User Function SASP042()

	Local oBrowse


	oBrowse:= FWmBrowse():New()
	oBrowse:SetAlias('ZZ0')
	oBrowse:SetDescription('MVC')
	oBrowse:SETMENUDEF('SASP042')
	oBrowse:Activate()

Return //nil


Static Function MenuDef()

	Local aRotina := {}
	Public aKit := {}

	// MENU

	ADD OPTION aRotina Title 'Pesquisa'      Action 'PesqBrw'          OPERATION 1 ACCESS 0
	ADD OPTION aRotina Title 'Visualizar'    Action 'VIEWDEF.SASP036'  OPERATION 2 ACCESS 0
	ADD OPTION aRotina Title 'Incluir'       Action 'VIEWDEF.SASP036'  OPERATION 3 ACCESS 0
	ADD OPTION aRotina Title 'Alterar'       Action 'VIEWDEF.SASP036'  OPERATION 4 ACCESS 0
	ADD OPTION aRotina Title 'Excluir'       Action 'VIEWDEF.SASP036'  OPERATION 5 ACCESS 0

Return aRotina

Static Function ModelDef()


	Local oStructSZ1	:= Nil
	Local oStructZZ6 := FWFormStruct(1,'ZZ6')
	Local oModel	 := FWLoadModel( 'SASP031' )
	oModel:aModelStruct[1][4]	:= {}

	//TODO GATILHO REFERENTE A ATUALIZA��O DO GRID DO ADITIVO(SE DER PROBLEMA NAS QUANTIDADE E VALOR DE CONTRATO VERIFICAR ESSES GATILHOS)

	oStructZZ6:AddTrigger( ;
	"ZZ6_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ6_DESCRI"			,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("SB1",oModel:GetValue('ZZ6_PRODUT'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SB1",1,xFilial("SB1")+oModel:GetValue("ZZ6_PRODUT"),"SB1->B1_DESC") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ6:AddTrigger( ;
	"ZZ6_QUANT"			,;										                //[01] Id do campo de origem
	"ZZ6_VLRTOT"			,;										            //[02] Id do campo de destino
	{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| (oModel:GetValue("ZZ6_QUANT") * oModel:GetValue("ZZ6_VLRUN")) - (oModel:GetValue("ZZ6_VLRDSC") * oModel:GetValue("ZZ6_QUANT")) }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ6:AddTrigger( ;
	"ZZ6_QUANT"			,;										                //[01] Id do campo de origem
	"ZZ6_QTATU"			,;										                 //[02] Id do campo de destino
	{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| IIF(oModel:GetValue("ZZ6_TIPO") == 'A' .or. oModel:GetValue("ZZ6_TIPO") == 'N',oModel:GetValue("ZZ6_QTANT") + oModel:GetValue("ZZ6_QUANT"),IIF(oModel:GetValue("ZZ6_TIPO") == 'D',oModel:GetValue("ZZ6_QTANT") - oModel:GetValue("ZZ6_QUANT"),)) }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ6:AddTrigger( ;
	"ZZ6_QUANT"			,;										                //[01] Id do campo de origem
	"ZZ6_VLRTOT"			,;										            //[02] Id do campo de destino
	{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| (oModel:GetValue("ZZ6_QTATU") * oModel:GetValue("ZZ6_VLRUN")) - (oModel:GetValue("ZZ6_VLRDSC") * oModel:GetValue("ZZ6_QTATU")) }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ6:AddTrigger( ;
	"ZZ6_VLUATU"			,;										                //[01] Id do campo de origem
	"ZZ6_VLRUN"			,;										            //[02] Id do campo de destino
	{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel|  IIF(oModel:GetValue("ZZ6_TIPO") $ 'A-N',oModel:GetValue("ZZ6_VLUATU") + oModel:GetValue("ZZ6_VLUANT"),oModel:GetValue("ZZ6_VLUANT") - oModel:GetValue("ZZ6_VLUATU")) }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ6:AddTrigger( ;
	"ZZ6_VLRUN"			,;										                //[01] Id do campo de origem
	"ZZ6_VLRTOT"			,;										            //[02] Id do campo de destino
	{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| (oModel:GetValue("ZZ6_QTATU") * oModel:GetValue("ZZ6_VLRUN")) - (oModel:GetValue("ZZ6_VLRDSC") * oModel:GetValue("ZZ6_QTATU")) }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructZZ6:AddTrigger( ;
	"ZZ6_QUANT"			,;										                //[01] Id do campo de origem
	"ZZ6_SLATU"			,;										                 //[02] Id do campo de destino
	{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| IIF(oModel:GetValue("ZZ6_TIPO") == 'A' .or. oModel:GetValue("ZZ6_TIPO") == 'N' ,oModel:GetValue("ZZ6_SLANT") + oModel:GetValue("ZZ6_QUANT"),IIF(oModel:GetValue("ZZ6_TIPO") == 'D',oModel:GetValue("ZZ6_SLANT") - oModel:GetValue("ZZ6_QUANT"),)) }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ6:AddTrigger( ;
	"ZZ6_QUANT"			,;										                //[01] Id do campo de origem
	"ZZ6_SE1"			,;										            //[02] Id do campo de destino
	{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| IIF(oModel:GetValue("ZZ6_TIPO") == 'A' .or. oModel:GetValue("ZZ6_TIPO") == 'N' ,(oModel:GetValue("ZZ6_QUANT") * oModel:GetValue("ZZ6_VLRUN")) - (oModel:GetValue("ZZ6_VLRDSC") * oModel:GetValue("ZZ6_QUANT")),0) }  )	// [04] Bloco de codigo de execu��o do gatilho

		oStructZZ6:AddTrigger( ;
	"ZZ6_VLUATU"			,;										                //[01] Id do campo de origem
	"ZZ6_SE1"			,;										            //[02] Id do campo de destino
	{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| IIF((oModel:GetValue("ZZ6_TIPO") == 'A' .or. oModel:GetValue("ZZ6_TIPO") == 'N') .AND. oModel:GetValue("ZZ6_QUANT") == 0 ,oModel:GetValue("ZZ6_VLUATU") * oModel:GetValue("ZZ6_QTATU"),IIF((oModel:GetValue("ZZ6_TIPO") == 'A' .or. oModel:GetValue("ZZ6_TIPO") == 'N') .AND. oModel:GetValue("ZZ6_QUANT") <> 0,((oModel:GetValue("ZZ6_QUANT") * oModel:GetValue("ZZ6_VLUANT")) - (oModel:GetValue("ZZ6_VLRDSC") * oModel:GetValue("ZZ6_QUANT"))) + oModel:GetValue("ZZ6_VLUATU") * oModel:GetValue("ZZ6_QTATU"),0 )) }  )	// [04] Bloco de codigo de execu��o do gatilho
	
		oStructZZ6:AddTrigger( ;
	"ZZ6_QUANT"			,;										                //[01] Id do campo de origem
	"ZZ6_NCC"			,;										            //[02] Id do campo de destino
	{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| IIF(oModel:GetValue("ZZ6_TIPO") == 'D',(oModel:GetValue("ZZ6_QUANT") * oModel:GetValue("ZZ6_VLRUN")) - (oModel:GetValue("ZZ6_VLRDSC") * oModel:GetValue("ZZ6_QUANT")),0) }  )	// [04] Bloco de codigo de execu��o do gatilho

		oStructZZ6:AddTrigger( ;
	"ZZ6_VLUATU"			,;										                //[01] Id do campo de origem
	"ZZ6_NCC"			,;										            //[02] Id do campo de destino
	{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| IIF((oModel:GetValue("ZZ6_TIPO") == 'D') .AND. oModel:GetValue("ZZ6_QUANT") == 0 ,oModel:GetValue("ZZ6_VLUATU") * oModel:GetValue("ZZ6_QTATU"),IIF((oModel:GetValue("ZZ6_TIPO") == 'D') .AND. oModel:GetValue("ZZ6_QUANT") <> 0,((oModel:GetValue("ZZ6_QUANT") * oModel:GetValue("ZZ6_VLUANT")) - (oModel:GetValue("ZZ6_VLRDSC") * oModel:GetValue("ZZ6_QUANT"))) + oModel:GetValue("ZZ6_VLUATU") * oModel:GetValue("ZZ6_QTATU"),0 )) }  )	// [04] Bloco de codigo de execu��o do gatilho

	//oStructZZ6:AddTrigger( ;
	//"ZZ6_QUANT"			,;										                //[01] Id do campo de origem
	//"ZZ6_VLRDSC"			,;										                 //[02] Id do campo de destino
	//{ |oModel| .T. }	,;										                //[03] Bloco de codigo de valida��o da execu��o do gatilho
	//{ |oModel| oModel:GetValue("ZZ6_VLRDSC") * oModel:GetValue("ZZ6_QUANT")/*RETDSCZZ6(oModel)*/ }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ6:SetProperty("ZZ6_QUANT",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| QUANTZZ6(oModel,cId,xValor) })
	oStructZZ6:SetProperty("ZZ6_VLRUN",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| VLRUNZZ6(oModel,cId,xValor) })
	oStructZZ6:SetProperty("ZZ6_TIPO",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| TIPOZZ6(oModel,cId,xValor) })

	oModel:AddGrid("ZZ6DETAIL", "ZZ0MASTER"/*cOwner*/,oStructZZ6, ,/*bLinePost*/,{|oModelZZ6,nLinha,cOperacao,cCampo| FormLinPre(oModelZZ6,nLinha,cOperacao,cCampo) }/*bPre*/,/*bPost*/,{ |oModel, lCopia| LoadZZ1( oModel, lCopia ) }/*Carga*/)
	oModel:SetRelation( 'ZZ6DETAIL', { { 'ZZ6_FILIAL', 'xFilial( "ZZ6" )' }, { 'ZZ6_NUMERO', 'ZZ0_NUMERO' } }, ZZ6->( IndexKey( 1 ) ) )

	// ADD RODAPE , ONDE S�O INFORMADOS OS TOTALIZADORES
	oModel:AddCalc( 'YCADZZ2CALC1', 'ZZ0MASTER', 'ZZ6DETAIL', 'ZZ6_QUANT', 'ZZ6__TOT01', 'SUM',,,'Total Itens',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"QTDMED") }*/ )
	oModel:AddCalc( 'YCADZZ2CALC1', 'ZZ0MASTER', 'ZZ6DETAIL', 'ZZ6_VLRTOT', 'ZZ6__TOT03', 'SUM',,,'Valor Total(R$)',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)

	//BLOQUEIO DOS CAMPOS , PERMITINDO ALTERAR SOMENTE QUANTIDADE E VALOR UNITARIO
	oModel:GetModel("ZZ0MASTER" ):GetStruct():SetProperty("*",MODEL_FIELD_WHEN,{||.F.})

	oModel:SetOnlyQuery('ZZ0MASTER',.T.)

	oModel:SetPrimaryKey({'ZZ6_FILIAL','ZZ6_ITEM','ZZ6_PRODUT','ZZ6_TES'})
	oModel:GetModel("ZZ6DETAIL"):GetStruct():SetProperty("ZZ6_TES",MODEL_FIELD_VALID,{|oModel|ZZ6ValTES(oModel) })

	oModel:SetCommit({|oModel| ZZ6Commit(oModel) },.F.)

Return (oModel)

Static Function ViewDef()

	Local oModel := FWLoadModel('SASP042')
	Local oStructZZ0:= FWFormStruct(2,'ZZ0')
	Local oStructZZ6:= FWFormStruct(2,"ZZ6",{|cCampo| !AllTrim(cCampo)+"|" $ "ZZ6_NUMERO|ZZ6_CLIENT|ZZ6_LOJA|ZZ6_DTGER|ZZ6_HRGER|ZZ6_USER|"})
	Local oView:=FWFormView():New()

	oCalc1	:= FWCalcStruct( oModel:GetModel( 'YCADZZ2CALC1') )

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField('ZZ0MASTER',oStructZZ0)
	oView:AddGrid('ZZ6DETAIL',oStructZZ6)
	oView:AddField( 'VIEW_CALC', oCalc1, 'YCADZZ2CALC1' )

	oView:CreateHorizontalBox('SUPERIOR',40)
	oView:CreateHorizontalBox('INFERIOR',50)
	oView:CreateHorizontalBox('RODAPE',10)

	oView:SetOwnerView('ZZ0MASTER','SUPERIOR')
	oView:SetOwnerView('ZZ6DETAIL','INFERIOR')
	oView:SetOwnerView( 'VIEW_CALC', 'RODAPE' )

	oView:EnableTitleView('ZZ6DETAIL','Itens Medi��o')

	oView:SetProgressBar(.T.)
	oView:AddIncrementField("ZZ6DETAIL","ZZ6_ITEM")
	//oView:EnableTitleView('ZZ6DETAIL','Itens Contrato')

	oView:AddUserButton( 'Procurar Kit', 'CLIPS', { |oView| ProcKit(oView) }, , VK_F4,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} )
	oView:AddUserButton( 'Historico Aditivo', 'CLIPS', { |oView| HistADT(oView) }, , VK_F8,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} )
Return oView


STATIC FUNCTION LoadZZ1(oGrid,lCopia)
	Local aArea   := GetArea()
	Local aFields := {}
	Local aRet    := {}
	Local cTmp    := ''
	Local cQuery  := ''
	Local cFields := 'R_E_C_N_O_'
	Local nTam
	Local aStruct	:= oGrid:oFormModelStruct:GetFields()
	Local aCategoria:= {'ALUNO','PROFESSOR','COORDENA��O','OUTROS'}
	Local lOk		:= .F.

	oModel 		:= FWModelActive()

	// Pega campos que fazem parte da estrutura do objeto, para otimizar retorno da query
	cTmp   := GetNextAlias()
	cQuery := " SELECT * "
	cQuery += " FROM "+RetSqlName( 'ZZ1' ) + " ZZ1 "
	cQuery += " INNER JOIN ZZ7010 ZZ7(NOLOCK) ON ZZ1_PRODUT = ZZ7_CODIGO AND ZZ7.D_E_L_E_T_ = '' "
	If !Empty(aRetParm[5]) .and. aRetParm[6] <> "Produto"
		cQuery += " AND ZZ7_ENVIO='" +aRetParm[5] + "' "
	EndIf
	If !Empty(aRetParm[4]) .and. aRetParm[6] <> "Produto"
		cQuery += " AND ZZ7_SERIE='" +aRetParm[4] + "' "
	EndIf
	cQuery += " WHERE ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
	cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' "
	cQuery += " AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' "
	cQuery += " AND ZZ1_LOJA = '"+ZZ0->ZZ0_LOJA+"' "
	If !Empty(aRetParm[2]) .and. aRetParm[6] <> "Produto"
	cQuery += " AND ZZ1_ITEM BETWEEN '"+aRetParm[1]+"' AND '"+aRetParm[2]+"' "
	EndIf
	If !Empty(aRetParm[3]) .and. aRetParm[6] <> "Produto"
		cQuery += " AND ZZ1_TES='" +aRetParm[3] + "' "
	EndIf
	cQuery += " AND ZZ1.D_E_L_E_T_=' '"
	cQuery += " AND ZZ1_STATUS <> 'I' "
	cQuery += " ORDER BY ZZ1_ITEM "


	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.F.,.T.)

	While (cTmp)->(!EOF())

		If (cTmp)->ZZ1_STATUS == 'I'
			(cTmp)->(DbSkip())
			Loop
		Endif


		//Gera��o da Duplicata = 'S'
		cDuplic	:=	Posicione("SF4",1,xFilial("SF4",(cTmp)->ZZ1_FILIAL)+(cTmp)->ZZ1_TES,"F4_DUPLIC")


		//If cDuplic <> 'S'
		//	(cTmp)->(DbSkip())
		//	Loop
		//Endif


		AADD(aRet,{0,{}})
		nTam	:= Len(aRet)
		For nCont:=1 To Len(aStruct)



			AADD(aRet[nTam][2],CriaVar(aStruct[nCont][MODEL_FIELD_IDFIELD]))
			If aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_FILIAL"
				aRet[nTam][2][nCont]	:= cFilant
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_NUMERO"
				aRet[nTam][2][nCont]	:= ZZ0->ZZ0_NUMERO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_ITEM"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_ITEM
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_CLIENT"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_CLIENT
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_LOJA"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_LOJA
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_TES"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_TES
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_PRODUT"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_PRODUT
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_DESCRI"
				IF (cTmp)->ZZ1_TIPO == 'P'
					aRet[nTam][2][nCont]	:= POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM((cTmp)->ZZ1_PRODUT),"B1_DESC")
				ELSEIF (cTmp)->ZZ1_TIPO == 'K'
					aRet[nTam][2][nCont]	:= POSICIONE("ZZ7",1,XFILIAL("ZZ7")+ALLTRIM((cTmp)->ZZ1_PRODUT),"ZZ7_DESCR")
				ENDIF
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_QUANT"
				aRet[nTam][2][nCont]	:= 0//(cTmp)->ZZ1_SALDO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_QNTINI"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_QNTINI//(cTmp)->ZZ1_SALDO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_VLRUN"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_VLRUN
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_VLUANT"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_VLRUN
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_VLRTOT"
				aRet[nTam][2][nCont]	:= 0//(cTmp)->ZZ1_SALDO*(cTmp)->ZZ1_VLRUN
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_VLTANT"
				aRet[nTam][2][nCont]	:= ((cTmp)->ZZ1_VLRUN * (cTmp)->ZZ1_QUANT) - ((cTmp)->ZZ1_QUANT * (cTmp)->ZZ1_VLRDSC) 
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_SLANT"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_SALDO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_QTANT"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_QUANT
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_SLATU"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_SALDO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_QTATU"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_QUANT
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_TPPROD"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_TIPO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_STATUS"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_STATUS
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_VLRDSC"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_VLRDSC
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ6_TIPO"
				aRet[nTam][2][nCont]	:= 'A'
			EndIf

		Next
		(cTmp)->(DbSkip())
	EndDo
	(cTmp)->(dbCloseArea())
	RestArea(aArea)

Return aRet

// n�o utilizado
STATIC Function RETDSCZZ6(oModel)
	Local nRet:= 0
	Local cTabPad	:= SuperGetMV("MV_TABPAD")
	Local aAreaZ1	:= GetArea()
	Local oModel	:= FWModelActive()
	Local oModelZZ6 := oModel:GETMODEL("ZZ6DETAIL")
	Local oModelZZ0 := oModel:GETMODEL("ZZ0MASTER")

	if Empty(oModel:GetValue("ZZ0MASTER","ZZ0_CLIENT"))
		Return nRet
	endif

	lKit:= ExistCpo("ZZ7",oModelZZ6:GetValue("ZZ6_PRODUT"),1,,.F.)
	If SA1->(dbSeek(xFilial("SA1")+oModel:GetValue("ZZ0MASTER","ZZ0_CLIENT")+oModel:GetValue("ZZ0MASTER","ZZ0_LOJA"))) .AND. !Empty(SA1->A1_Tabela)
		cTabPad	:= SA1->A1_TABELA
	EndIf

	If lKit
		ZZ7->(DbSetOrder(1))
		ZZ7->(DbSeek(xFilial("ZZ7")+Alltrim(oModelZZ6:GetValue("ZZ6_PRODUT"))))
		While ZZ7->(!EOF()) .AND. Alltrim(xFilial("ZZ7")+oModelZZ6:GetValue("ZZ6_PRODUT"))==Alltrim(ZZ7->(ZZ7_FILIAL+ZZ7_CODIGO	))
			nVlrItem	:= MaTabPrVen(cTabPad;
			,ZZ7->ZZ7_PRODUT;
			,1;
			,oModel:GetValue("ZZ0MASTER","ZZ0_CLIENT");
			,oModel:GetValue("ZZ0MASTER","ZZ0_LOJA"))

			If ZZ7->ZZ7_PERDSC > 0
				nRet+= (( nVlrItem*ZZ7->ZZ7_QUANT*oModelZZ6:GetValue("ZZ6_QUANT"))*(ZZ7->ZZ7_PERDSC/100))
			Endif
			ZZ7->(dbSkip())
		Enddo
	Endif

	RestArea(aAreaZ1)

Return nRet

static function ZZ6Commit(oModel)

	Local lRet := .T.
	Local oModelZZ0 := oModel:getmodel("ZZ0MASTER")
	Local oModelZZ6 := oModel:getmodel("ZZ6DETAIL")
	Local nValDp     := 0
	Local nDesDp     := 0
	Local nValNCC    := 0
	Local nDesNCC    := 0
	Local nValNV     := 0
	Local nDesNV     := 0
	Local nValFin    := 0
	Local nDesc      := 0
	Local aCondicao  := {}
	Local cParc      := ""
	Local nTotD      := 0
	Local cNum       := ""
	Local nCount     := 0
	Local cAux 		 := "" 
	Local nQtd		 := 0
	Local lProdNovo  := .F.
	Local cFinanceiro := oModelZZ0:GETVALUE("ZZ0_CONFNF")
	Local cPdDp      := ""
	Local cPdNv		 := ""
	Local lOp		 := .T. 
	Local cQuery := ""
	Local cQuery2 := ""
	Local nFrete := 0
	Local cPrxItem := ""
	
	
	private cItem1
	private cFil1
	private cNumero1
	private  cCli1
	private  cLoja1
	private cTipo1
	private nQtd1
	private nVlr1 
	private cTipoA1
	private cTit1
	private cTipoTi1

	FOR nAx := 1 to len(oModelZZ6:acols)
		oModelZZ6:GoLine(nAx)

		IF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'A'.AND. ;//SE O ADITIVO FOR DE QUANTIDADE
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') == 0 .AND. ;
		(POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S' .OR.  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES') $ GETMV("SA_TESEXCE"))

			nValDp  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			nDesDp  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			
			cItem1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			cFil1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL')
			cNumero1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
			cCli1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT')
			cLoja1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA')
			cTipo1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO')
			nQtd1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			nVlr1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')

			Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"Q",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))	
		
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'A'.AND. ;//SE O ADITIVO FOR DE VALOR
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') == 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') <> 0 .AND. ;
		(POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S' .OR.  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES') $ GETMV("SA_TESEXCE")) .AND. ;
		POSICIONE("ZZ1",2,oModelZZ0:GETVALUE("ZZ0_FILIAL") + oModelZZ0:GETVALUE("ZZ0_NUMERO") + oModelZZ0:GETVALUE("ZZ0_CLIENT") + oModelZZ0:GETVALUE("ZZ0_LOJA") + oModelZZ6:GETVALUE("ZZ6_PRODUT") + oModelZZ6:GETVALUE("ZZ6_TES")  ,"ZZ1_VLRUN") < oModelZZ6:GETVALUE("ZZ6_VLRUN")
			
			cItem1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			cFil1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL')
			cNumero1:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
			cCli1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT')
			cLoja1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA')
			cTipo1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO')
			nQtd1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			nVlr1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
			
			nValDp  +=  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') *  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
			//nDesDp  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC')
			
			Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"V",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'A'.AND. ;//SE O ADITIVO FOR DE VALOR
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') <> 0 .AND. ;
		(POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S' .OR.  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES') $ GETMV("SA_TESEXCE")) .AND. ;
		POSICIONE("ZZ1",2,oModelZZ0:GETVALUE("ZZ0_FILIAL") + oModelZZ0:GETVALUE("ZZ0_NUMERO") + oModelZZ0:GETVALUE("ZZ0_CLIENT") + oModelZZ0:GETVALUE("ZZ0_LOJA") + oModelZZ6:GETVALUE("ZZ6_PRODUT") + oModelZZ6:GETVALUE("ZZ6_TES")  ,"ZZ1_VLRUN") < oModelZZ6:GETVALUE("ZZ6_VLRUN")
			
			cItem1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			cFil1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL')
			cNumero1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
			cCli1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT')
			cLoja1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA')
			cTipo1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO')
			nQtd1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			nVlr1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
			
			nValDp  += (oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUANT')) - (oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')) + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
			//nDesDp  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"A",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
			
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'A'.AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') == 0 .AND. ;
		(POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S' .OR.  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES') $ GETMV("SA_TESEXCE")) .AND. ;
		POSICIONE("ZZ1",2,oModelZZ0:GETVALUE("ZZ0_FILIAL") + oModelZZ0:GETVALUE("ZZ0_NUMERO") + oModelZZ0:GETVALUE("ZZ0_CLIENT") + oModelZZ0:GETVALUE("ZZ0_LOJA") + oModelZZ6:GETVALUE("ZZ6_PRODUT") + oModelZZ6:GETVALUE("ZZ6_TES")  ,"ZZ1_VLRUN") < oModelZZ6:GETVALUE("ZZ6_VLRUN")

			nValDp  += oModelZZ6:GETVALUE("ZZ6_VLRUN") - POSICIONE("ZZ1",2,oModelZZ0:GETVALUE("ZZ0_FILIAL") + oModelZZ0:GETVALUE("ZZ0_NUMERO") + oModelZZ0:GETVALUE("ZZ0_CLIENT") + oModelZZ0:GETVALUE("ZZ0_LOJA") + oModelZZ6:GETVALUE("ZZ6_PRODUT") + oModelZZ6:GETVALUE("ZZ6_TES")  ,"ZZ1_VLRUN")
			nDesDp  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC')

		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'D' .AND. ;//SE O ADITIVO FOR DECRESCIMO E DE QUANTIDADE
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') == 0 .AND. ;
		(POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S' .OR.  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES') $ GETMV("SA_TESEXCE"))
			
			cItem1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			cFil1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL')
			cNumero1:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
			cCli1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT')
			cLoja1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA')
			cTipo1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO')
			nQtd1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			nVlr1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
			
			
			nValNCC  +=  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			nDesNCC  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"Q",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'D' .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') == 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') <> 0 .AND. ;
		(POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S' .OR.  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES') $ GETMV("SA_TESEXCE")) .AND. ;
		POSICIONE("ZZ1",2,oModelZZ0:GETVALUE("ZZ0_FILIAL") + oModelZZ0:GETVALUE("ZZ0_NUMERO") + oModelZZ0:GETVALUE("ZZ0_CLIENT") + oModelZZ0:GETVALUE("ZZ0_LOJA") + oModelZZ6:GETVALUE("ZZ6_PRODUT") + oModelZZ6:GETVALUE("ZZ6_TES")  ,"ZZ1_VLRUN") > oModelZZ6:GETVALUE("ZZ6_VLRUN")

			cItem1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			cFil1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL')
			cNumero1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
			cCli1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT')
			cLoja1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA')
			cTipo1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO')
			nQtd1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			nVlr1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
			
			
			nValNCC  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') *  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
			//nDesNCC  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC')
			Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"V",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'D' .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') <> 0 .AND. ;
		(POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S' .OR.  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES') $ GETMV("SA_TESEXCE")) .AND. ;
		POSICIONE("ZZ1",2,oModelZZ0:GETVALUE("ZZ0_FILIAL") + oModelZZ0:GETVALUE("ZZ0_NUMERO") + oModelZZ0:GETVALUE("ZZ0_CLIENT") + oModelZZ0:GETVALUE("ZZ0_LOJA") + oModelZZ6:GETVALUE("ZZ6_PRODUT") + oModelZZ6:GETVALUE("ZZ6_TES")  ,"ZZ1_VLRUN") > oModelZZ6:GETVALUE("ZZ6_VLRUN")
			
			cItem1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			cFil1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL')
			cNumero1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
			cCli1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT')
			cLoja1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA')
			cTipo1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO')
			nQtd1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			nVlr1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
			
			nValNCC  += (oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUANT')) - (oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')) + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
			//nDesNCC  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"A",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'N' .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. ;
		(POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S' .OR.  oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES') $ GETMV("SA_TESEXCE"))
		
			cItem1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			cFil1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL')
			cNumero1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
			cCli1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT')
			cLoja1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA')
			cTipo1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO')
			nQtd1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			nVlr1	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')

			nValNV  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			nDesNV  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"N",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'N' .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0;

			Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"N",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		
		
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'A' .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') == 0;
		
		Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"Q",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'A' .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') == 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') <> 0
		Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"V",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		
			
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'A' .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') <> 0
		Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"A",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		
			ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'D' .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') == 0
		Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"Q",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'D' .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') == 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') <> 0
		Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"V",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		
			
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'D' .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. ;
		oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU') <> 0
		Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"A",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'))		
		
		ENDIF

		/*
		IF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'A' .AND. oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S'
		nValDp  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRTOT')
		nDesDp  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC')
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'D' .AND. oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S'
		nValNCC  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRTOT')
		nDesNCC  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC')
		ELSEIF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'N' .AND. oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT') <> 0 .AND. POSICIONE("SF4",1,XFILIAL("SF4")+oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES'),"F4_DUPLIC") == 'S'
		nValNV  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRTOT')
		nDesNV  += oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC')
		ENDIF
		*/
	NEXT

	IF nValDp > 0
		nValFin	:= nValDp
		nDesc	:= nDesDp

		nFrete :=  NOROUND( nValFin * ( oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FRTCLI')	/ 100),2)

		nValFin += NOROUND( nValFin * ( oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FRTCLI')	/ 100),2)

		cNum := ProxNum("ADT")
		//cNum := GetSXENum("SE1","E1_FILIAL"+"E1_NUM"+"E1_TIPO")
		SE1->(dbSetOrder(1))
		Begin Transaction
			aCondicao	:= Condicao(nValFin,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CPFIN'),,date())
			If Empty(aCondicao)
				DisarmTransaction()
				AutoGrLog("Erro ao gerar parcelas do titulo!")
				AutoGrLog("Verifique a condi��o de pagamento e o valor total!")
				MostraErro()
				Return .F.
			EndIf

			IF cFinanceiro == '1'
				IF GETMV("SA_FINADT")
					For nCont:=1 to Len(aCondicao)
						aDados	:= {}
						AADD(aDados,{"E1_PREFIXO"	,"B"+SUBSTR(CFILANT,5,2),nil})
						AADD(aDados,{"E1_NUM"		,PADL(Alltrim(cNum),9,'0')		    ,nil})
						AADD(aDados,{"E1_TIPO"		,"ADT"					,nil})
						AADD(aDados,{"E1_PARCELA"	,RetAsc(nCont,GetSx3Cache("E1_PARCELA","X3_TAMANHO"),.T.),nil})
						AADD(aDados,{"E1_NATUREZ"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NATURE')		,nil})
						AADD(aDados,{"E1_CLIENTE"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')		,nil})
						AADD(aDados,{"E1_LOJA"		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')			,nil})
						AADD(aDados,{"E1_HIST"		,'Aditivo contrato: '+ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')),nil})
						AADD(aDados,{"E1_CCC"		,GetMv("SA_CCC")		,nil})
						AADD(aDados,{"E1_VENCTO"	,aCondicao[nCont][1]	,nil})
						AADD(aDados,{"E1_VALOR"		,aCondicao[nCont][2]	,nil})
						AADD(aDados,{"E1_YCONTRA" ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO'))   ,nil})
						AADD(aDados,{"E1_YFILIAL"  ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FILIAL')),nil})
						IF nDesDp > 0
							AADD(aDados,{"E1_DESCONT"		,nDesDp	,nil})
							AADD(aDados,{"E1_DESCONT"		,nDesDp	,nil})
						ENDIF
						AADD(aDados,{"E1_YVLFRET"		, NOROUND(nFrete/len(aCondicao),2)	,nil})
						lMsErroAuto	:= .F.
						MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
						If lMsErroAuto
							DisarmTransaction()
							AutoGrLog("Erro ao incluir o titulo!")
							AutoGrLog("Opera��o cancelada!")
							MostraErro()
							Return .F.
						Else
							cAux := "ADT"
							lOp := .F.
							Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'),PADL(Alltrim(cNum),9,'0'),cAux)
							cPdDp:= "Pedido :"+ALLTRIM(cNum)+" Prefixo :"+"B"+SUBSTR(CFILANT,5,2)+ CRLF
						ENDIF
					NEXT
					//ALERT(cPdDp)
					cPdDp:=""

				ELSE

					aDados	:= {}
					AADD(aDados,{"E1_PREFIXO"	,"B"+SUBSTR(CFILANT,5,2),nil})
					AADD(aDados,{"E1_NUM"		,PADL(Alltrim(cNum),9,'0')		    ,nil})
					AADD(aDados,{"E1_TIPO"		,"ADT"					,nil})
					AADD(aDados,{"E1_NATUREZ"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NATURE')		,nil})
					AADD(aDados,{"E1_CLIENTE"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')		,nil})
					AADD(aDados,{"E1_LOJA"		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')			,nil})
					AADD(aDados,{"E1_HIST"		,'Aditivo contrato: '+ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')),nil})
					AADD(aDados,{"E1_CCC"		,GetMv("SA_CCC")		,nil})
					AADD(aDados,{"E1_VENCTO"	,aCondicao[1][1]		,nil})
					AADD(aDados,{"E1_VALOR"		,nValFin - nDesDp	,nil})
					AADD(aDados,{"E1_YCONTRA" ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO'))   ,nil})
					AADD(aDados,{"E1_YFILIAL"  ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FILIAL')),nil})
					IF nDesDp > 0
						AADD(aDados,{"E1_DESCONT"		,nDesDp	,nil})
					ENDIF
					AADD(aDados,{"E1_YVLFRET"		, NOROUND(nFrete,2)	,nil})
					lMsErroAuto	:= .F.
					MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
					If lMsErroAuto
						DisarmTransaction()
						AutoGrLog("Erro ao incluir o titulo!")
						AutoGrLog("Opera��o cancelada!")
						MostraErro()
						Return .F.
					Else
						cAux := "ADT"
						lOp := .F.
						//TODO comentado por Erenilson Bezerra -18/11/2016
						Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'),PADL(Alltrim(cNum),9,'0'),cAux)
						cPdDp:= "Pedido :"+ALLTRIM(cNum)+" Prefixo :"+"B"+SUBSTR(CFILANT,5,2)+ CRLF
					ENDIF
				ENDIF
			ENDIF

			/*
			dbSelectArea("ZZ2")
			dbSetOrder(1)
			IF dbSeek(XFILIAL("ZZ2")+oModelZZ0:GETVALUE("ZZ0_NUMERO")+oModelZZ0:GETVALUE("ZZ0_MEDICA")+oModelZZ0:GETVALUE("ZZ0_CLIENT")+oModelZZ0:GETVALUE("ZZ0_LOJA"),.T.)

			RECLOCK("ZZ2", .F.)
			//ZZ2->ZZ2_DTPREV	:= validTipo(alltrim(oModelZZ2:GETVALUE("ZZ2_TIPO")),oModel)
			ZZ2->ZZ2_DTMAX	:= validTipo(alltrim(oModelZZ0:GETVALUE("ZZ0_TIPO")),oModel)
			MSUNLOCK()

			ENDIF
			*/
		End Transaction

		MsgInfo("TITULO :"+cNum + Chr(13) + Chr(10)+" TIPO: ADT"+ Chr(13) + Chr(10)+"FILIAL: "+ALLTRIM(cFilant)+ Chr(13) + Chr(10),"Mensagem")

	ENDIF

	nDesc	:= 0

	IF nValNCC > 0
		nValFin	:= nValNCC
		nDesc	:= nDesNCC

		nFrete :=  NOROUND( nValFin * ( oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FRTCLI')	/ 100),2)

		nValFin += NOROUND( nValFin * ( oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FRTCLI')	/ 100),2)


		cNum := ProxNum("NCC")
		//cNum := GetSXENum("SE1","E1_FILIAL"+"E1_NUM"+"E1_TIPO")
		SE1->(dbSetOrder(1))
		Begin Transaction

			IF cFinanceiro == '1'
				IF GETMV("SA_FINADT")
					aDados	:= {}
					AADD(aDados,{"E1_PREFIXO"	,"B"+SUBSTR(CFILANT,5,2),nil})
					AADD(aDados,{"E1_NUM"		,PADL(Alltrim(cNum),9,'0')		    ,nil})
					AADD(aDados,{"E1_TIPO"		,"NCC"					,nil})
					AADD(aDados,{"E1_PARCELA"	,RetAsc(1,GetSx3Cache("E1_PARCELA","X3_TAMANHO"),.T.),nil})
					AADD(aDados,{"E1_NATUREZ"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NATURE')		,nil})
					AADD(aDados,{"E1_CLIENTE"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')		,nil})
					AADD(aDados,{"E1_LOJA"		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')			,nil})
					AADD(aDados,{"E1_HIST"		,'Aditivo contrato: '+ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')),nil})
					AADD(aDados,{"E1_CCC"		,GetMv("SA_CCC")		,nil})
					AADD(aDados,{"E1_VENCTO"	,date()             	,nil})
					AADD(aDados,{"E1_VALOR"		,nValFin            	,nil})
					AADD(aDados,{"E1_YCONTRA" ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO'))   ,nil})
					AADD(aDados,{"E1_YFILIAL"  ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FILIAL')),nil})
					IF nDesc > 0
						AADD(aDados,{"E1_DESCONT"		,nDesNCC	,nil})
					ENDIF
					AADD(aDados,{"E1_YVLFRET"		, NOROUND(nFrete/len(aCondicao),2),nil})
					lMsErroAuto	:= .F.
					MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
					If lMsErroAuto
						DisarmTransaction()
						AutoGrLog("Erro ao incluir o titulo!")
						AutoGrLog("Opera��o cancelada!")
						MostraErro()
						Return .F.
					ELSE
						cAux := "NCC"
						lOp := .F.
						Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'),PADL(Alltrim(cNum),9,'0'),cAux)							
						MsgInfo("Pedido :"+ALLTRIM(cNum)+" Prefixo :"+"B"+SUBSTR(CFILANT,5,2),"Mensagem")
					ENDIF

				ELSE

					aDados	:= {}
					AADD(aDados,{"E1_PREFIXO"	,"B"+SUBSTR(CFILANT,5,2),nil})
					AADD(aDados,{"E1_NUM"		,PADL(Alltrim(cNum),9,'0')		    ,nil})
					AADD(aDados,{"E1_TIPO"		,"NCC"					,nil})
					AADD(aDados,{"E1_NATUREZ"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NATURE')		,nil})
					AADD(aDados,{"E1_CLIENTE"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')		,nil})
					AADD(aDados,{"E1_LOJA"		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')			,nil})
					AADD(aDados,{"E1_HIST"		,'Aditivo contrato: '+ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')),nil})
					AADD(aDados,{"E1_CCC"		,GetMv("SA_CCC")		,nil})
					AADD(aDados,{"E1_VENCTO"	,date()		,nil})
					AADD(aDados,{"E1_VALOR"		,nValFin - nDesNCC	,nil})
					AADD(aDados,{"E1_YCONTRA" ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO'))   ,nil})
					AADD(aDados,{"E1_YFILIAL"  ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FILIAL')),nil})
					IF nDesDp > 0
						AADD(aDados,{"E1_DESCONT"		,nDesNCC	,nil})
					ENDIF
					AADD(aDados,{"E1_YVLFRET"		, NOROUND(nFrete,2)	,nil})
					lMsErroAuto	:= .F.
					MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
					If lMsErroAuto
						DisarmTransaction()
						AutoGrLog("Erro ao incluir o titulo!")
						AutoGrLog("Opera��o cancelada!")
						MostraErro()
						Return .F.
					Else
						cAux := "NCC"
						lOp := .F.
						Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'),PADL(Alltrim(cNum),9,'0'),cAux)	
						cPdDp:= "Pedido :"+ALLTRIM(cNum)+" Prefixo :"+"B"+SUBSTR(CFILANT,5,2)+ CRLF
					ENDIF
				ENDIF
			ENDIF


		End Transaction

		MsgInfo("TITULO :"+cNum + Chr(13) + Chr(10)+" TIPO: NCC"+ Chr(13) + Chr(10)+"FILIAL: "+ALLTRIM(cFilant)+ Chr(13) + Chr(10),"Mensagem")

	ENDIF

	//incluindo os itens novos no contrato
	IF nValNV > 0
		nValFin	:= nValNV
		nDesc	:= nDesNV

		nFrete :=  NOROUND( nValFin * ( oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FRTCLI')	/ 100),2)

		nValFin += NOROUND( nValFin * ( oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FRTCLI')	/ 100),2)

		cNum := ProxNum("DP")
		SE1->(dbSetOrder(1))
		Begin Transaction
			aCondicao	:= Condicao(nValFin,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CPFIN'),,date())

			IF cFinanceiro == '1'
				IF GETMV("SA_FINADT")
					For nCont:=1 to Len(aCondicao)
						aDados	:= {}
						AADD(aDados,{"E1_PREFIXO"	,"B"+SUBSTR(CFILANT,5,2),nil})
						AADD(aDados,{"E1_NUM"		,PADL(Alltrim(cNum),9,'0')		    ,nil})
						AADD(aDados,{"E1_TIPO"		,"ADT"					,nil})
						AADD(aDados,{"E1_PARCELA"	,RetAsc(nCont,GetSx3Cache("E1_PARCELA","X3_TAMANHO"),.T.),nil})
						AADD(aDados,{"E1_NATUREZ"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NATURE')		,nil})
						AADD(aDados,{"E1_CLIENTE"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')		,nil})
						AADD(aDados,{"E1_LOJA"		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')			,nil})
						AADD(aDados,{"E1_HIST"		,'Aditivo contrato: '+ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')),nil})
						AADD(aDados,{"E1_CCC"		,GetMv("SA_CCC")		,nil})
						AADD(aDados,{"E1_VENCTO"	,aCondicao[nCont][1]	,nil})
						AADD(aDados,{"E1_VALOR"		,aCondicao[nCont][2]	,nil})
						AADD(aDados,{"E1_YCONTRA" ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO'))   ,nil})
						AADD(aDados,{"E1_YFILIAL"  ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FILIAL')),nil})

						IF nDesDp > 0
							AADD(aDados,{"E1_DESCONT"		,nDesDp	,nil})
						ENDIF
						AADD(aDados,{"E1_YVLFRET"		, NOROUND(nFrete/len(aCondicao),2)	,nil})
						lMsErroAuto	:= .F.
						MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
						If lMsErroAuto
							DisarmTransaction()
							AutoGrLog("Erro ao incluir o titulo!")
							AutoGrLog("Opera��o cancelada!")
							MostraErro()
							Return .F.
						ELSE
							cAux := "ADT"
							lOp := .F.
							Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'),PADL(Alltrim(cNum),9,'0'),cAux)
							cPdNv := "Pedido :"+ALLTRIM(cNum)+" Prefixo :"+"B"+SUBSTR(CFILANT,5,2)+ CRLF
						ENDIF
					Next
					//alert(cPdNv)
					cPdNv:=""

				ELSE

					aDados	:= {}
					AADD(aDados,{"E1_PREFIXO"	,"B"+SUBSTR(CFILANT,5,2),nil})
					AADD(aDados,{"E1_NUM"		,PADL(Alltrim(cNum),9,'0')		    ,nil})
					AADD(aDados,{"E1_TIPO"		,"ADT"					,nil})
					AADD(aDados,{"E1_NATUREZ"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NATURE')		,nil})
					AADD(aDados,{"E1_CLIENTE"	,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')		,nil})
					AADD(aDados,{"E1_LOJA"		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')			,nil})
					AADD(aDados,{"E1_HIST"		,'Aditivo contrato: '+ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')),nil})
					AADD(aDados,{"E1_CCC"		,GetMv("SA_CCC")		,nil})
					AADD(aDados,{"E1_VENCTO"	,aCondicao[1][1]		,nil})
					AADD(aDados,{"E1_VALOR"		,nValFin	,nil})
					AADD(aDados,{"E1_YCONTRA" ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO'))   ,nil})
					AADD(aDados,{"E1_YFILIAL"  ,ALLTRIM(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FILIAL')),nil})
					IF nDesDp > 0
						AADD(aDados,{"E1_DESCONT"		,nDesDp	,nil})
					ENDIF
					AADD(aDados,{"E1_YVLFRET"		, NOROUND(nFrete,2)	,nil})
					lMsErroAuto	:= .F.
					MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
					If lMsErroAuto
						DisarmTransaction()
						AutoGrLog("Erro ao incluir o titulo!")
						AutoGrLog("Opera��o cancelada!")
						MostraErro()
						Return .F.
					Else
						cAux := "ADT"
						lOp := .F.
						Z10(lOp,oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN'),"",oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLUATU'),PADL(Alltrim(cNum),9,'0'),cAux)
						cPdDp:= "Pedido :"+ALLTRIM(cNum)+" Prefixo :"+"B"+SUBSTR(CFILANT,5,2)+ CRLF
					ENDIF
				ENDIF
			ENDIF

		End Transaction

		MsgInfo("TITULO :"+cNum + Chr(13) + Chr(10)+" TIPO: ADT"+ Chr(13) + Chr(10)+"FILIAL: "+ALLTRIM(cFilant)+ Chr(13) + Chr(10),"Mensagem")

	ENDIF

	FOR nAx := 1 to len(oModelZZ6:acols)
		oModelZZ6:GoLine(nAx)
		IF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'A'
			// INCLUINDO O ADITIVO NA TABELA ZZ6
			dbSelectArea("ZZ6")
			RECLOCK("ZZ6", .T.)

			ZZ6->ZZ6_FILIAL	:= cFilant //xFilial("ZZ3")
			ZZ6->ZZ6_NUMERO	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
			ZZ6->ZZ6_CLIENT := oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')
			ZZ6->ZZ6_LOJA   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			ZZ6->ZZ6_DTGER  := DATE()
			ZZ6->ZZ6_HRGER  := TIME()
			ZZ6->ZZ6_USER   := ALLTRIM(RetCodUsr())
			ZZ6->ZZ6_TIPO   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO')
			ZZ6->ZZ6_PRODUT  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_PRODUT')
			ZZ6->ZZ6_ITEM  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			ZZ6->ZZ6_QUANT  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			ZZ6->ZZ6_TES   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES')
			ZZ6->ZZ6_VLRUN   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
			ZZ6->ZZ6_VLRTOT   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRTOT')
			ZZ6->ZZ6_VLRDSC   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC')
			ZZ6->ZZ6_DESCRI   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_DESCRI')
			ZZ6->ZZ6_SLANT   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_SLANT')
			ZZ6->ZZ6_SLATU   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_SLATU')
			ZZ6->ZZ6_QTANT   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTANT')
			ZZ6->ZZ6_QTATU   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
			ZZ6->ZZ6_TPPROD   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TPPROD')
			ZZ6->ZZ6_STATUS   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_STATUS')
			MSUNLOCK()

			//ATUALIZANDO O CADASTRO DA CONTRATO
			dbSelectArea("ZZ1")
			ZZ1->(DbSetOrder(4)) //ZZ1_FILIAL+ZZ1_NUMERO+ZZ1_CLIENT+ZZ1_LOJA+ZZ1_PRODUT+ZZ1_TES+ZZ1_ITEM
			dbSeek(xFilial("ZZ1") + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_PRODUT') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM') )
			RECLOCK("ZZ1", .F.)
				ZZ1->ZZ1_FILIAL	:= cFilant //xFilial("ZZ3")
				ZZ1->ZZ1_NUMERO	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
				ZZ1->ZZ1_ITEM   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
				ZZ1->ZZ1_QUANT  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
				ZZ1->ZZ1_SALDO  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_SLATU')
				ZZ1->ZZ1_VLRUN  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
				ZZ1->ZZ1_VLRTOT  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
				ZZ1->ZZ1_VLRLIQ  := ZZ1->ZZ1_VLRTOT - (ZZ1->ZZ1_VLRDSC * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU'))
			MSUNLOCK()
		ENDIF

		IF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'D'
			// INCLUINDO O ADITIVO NA TABELA ZZ6
			dbSelectArea("ZZ6")
			RECLOCK("ZZ6", .T.)

			ZZ6->ZZ6_FILIAL	:= cFilant //xFilial("ZZ3")
			ZZ6->ZZ6_NUMERO	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
			ZZ6->ZZ6_CLIENT := oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')
			ZZ6->ZZ6_LOJA   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			ZZ6->ZZ6_DTGER  := DATE()
			ZZ6->ZZ6_HRGER  := TIME()
			ZZ6->ZZ6_USER   := ALLTRIM(RetCodUsr())
			ZZ6->ZZ6_TIPO   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO')
			ZZ6->ZZ6_PRODUT  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_PRODUT')
			ZZ6->ZZ6_ITEM  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			ZZ6->ZZ6_QUANT  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			ZZ6->ZZ6_TES   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES')
			ZZ6->ZZ6_VLRUN   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
			ZZ6->ZZ6_VLRTOT   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRTOT')
			ZZ6->ZZ6_VLRDSC   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC')
			ZZ6->ZZ6_DESCRI   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_DESCRI')
			ZZ6->ZZ6_SLANT   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_SLANT')
			ZZ6->ZZ6_SLATU   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_SLATU')
			ZZ6->ZZ6_QTANT   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTANT')
			ZZ6->ZZ6_QTATU   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
			ZZ6->ZZ6_TPPROD   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TPPROD')
			ZZ6->ZZ6_STATUS   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_STATUS')
			MSUNLOCK()

			//ATUALIZANDO O CADASTRO DA CONTRATO
			dbSelectArea("ZZ1")
			//ZZ1->(DbSetOrder(1))	//ZZ1_FILIAL+ZZ1_CONTRAT+ZZ1_ITEM
			//dbSeek(xFilial("ZZ1") + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'))

			ZZ1->(DbSetOrder(4)) //ZZ1_FILIAL+ZZ1_NUMERO+ZZ1_CLIENT+ZZ1_LOJA+ZZ1_PRODUT+ZZ1_TES+ZZ1_ITEM
			dbSeek(xFilial("ZZ1") + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_PRODUT') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM') )			
			
			
			RECLOCK("ZZ1", .F.)
				ZZ1->ZZ1_FILIAL	:= cFilant //xFilial("ZZ3")
				ZZ1->ZZ1_NUMERO	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
				ZZ1->ZZ1_ITEM  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
				ZZ1->ZZ1_QUANT  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
				ZZ1->ZZ1_SALDO  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_SLATU')
				ZZ1->ZZ1_VLRUN  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
				ZZ1->ZZ1_VLRTOT := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
				ZZ1->ZZ1_VLRLIQ  := ZZ1->ZZ1_VLRTOT - (ZZ1->ZZ1_VLRDSC * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU'))
				//ZZ1->ZZ1_VLRTOT := ZZ1->ZZ1_VLRTOT - oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRTOT')

			MSUNLOCK()

		ENDIF

		IF oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO') == 'N'
			// INCLUINDO O ADITIVO NA TABELA ZZ6
			dbSelectArea("ZZ6")
			RECLOCK("ZZ6", .T.)

			ZZ6->ZZ6_FILIAL	:= cFilant //xFilial("ZZ3")
			ZZ6->ZZ6_NUMERO	:= oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
			ZZ6->ZZ6_CLIENT := oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')
			ZZ6->ZZ6_LOJA   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			ZZ6->ZZ6_DTGER  := DATE()
			ZZ6->ZZ6_HRGER  := TIME()
			ZZ6->ZZ6_USER   := ALLTRIM(RetCodUsr())
			ZZ6->ZZ6_TIPO   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TIPO')
			ZZ6->ZZ6_PRODUT  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_PRODUT')
			ZZ6->ZZ6_ITEM  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
			ZZ6->ZZ6_QUANT  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
			ZZ6->ZZ6_TES   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES')
			ZZ6->ZZ6_VLRUN   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
			ZZ6->ZZ6_VLRTOT   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRTOT')
			ZZ6->ZZ6_VLRDSC   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC')
			ZZ6->ZZ6_DESCRI   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_DESCRI')
			ZZ6->ZZ6_SLANT   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_SLANT')
			ZZ6->ZZ6_SLATU   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_SLATU')
			ZZ6->ZZ6_QTANT   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTANT')
			ZZ6->ZZ6_QTATU   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU')
			ZZ6->ZZ6_TPPROD   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TPPROD')
			ZZ6->ZZ6_STATUS   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_STATUS')
			MSUNLOCK()

			//TODO INCLUIDO POR WESKLEY SILVA
			cQuery := " SELECT MAX(ZZ1_ITEM) AS ITEM FROM  "+ RETSQLNAME('ZZ1') 
			cQuery += " WHERE ZZ1_NUMERO = '"+ oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO') +"' AND ZZ1_FILIAL = '"+ xFilial("ZZ1") +"' "
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),'TY8',.F.,.T.)

			cPrxItem :=  soma1(TY8->ITEM)

			TY8->(DBCLOSEAREA())

			dbSelectArea("ZZ1")
			ZZ1->(DbSetOrder(2))	//ZZ1_FILIAL+ZZ1_CONTRAT+ZZ1_ITEM
			IF dbSeek(xFilial("ZZ1") + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO') + oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'))
				RECLOCK("ZZ1", .T.)
				ZZ1->ZZ1_FILIAL := cFilant //xFilial("ZZ3")
				ZZ1->ZZ1_NUMERO := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO')
				ZZ1->ZZ1_CLIENT := oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')
				ZZ1->ZZ1_LOJA   := oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')
				ZZ1->ZZ1_ITEM   := cPrxItem //oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM')
				ZZ1->ZZ1_PRODUT := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_PRODUT')
				ZZ1->ZZ1_DESC   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_DESCRI')
				ZZ1->ZZ1_QUANT  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
				ZZ1->ZZ1_TES    := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TES')
				ZZ1->ZZ1_SALDO  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_SLATU')
				ZZ1->ZZ1_VLRUN  := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN')
				ZZ1->ZZ1_VLRTOT := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRUN') * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QUANT')
				ZZ1->ZZ1_VLRDSC := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_VLRDSC')
				ZZ1->ZZ1_TIPO   := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_TPPROD')
				ZZ1->ZZ1_STATUS := oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_STATUS')
				ZZ1->ZZ1_VLRLIQ  := ZZ1->ZZ1_VLRTOT - (ZZ1->ZZ1_VLRDSC * oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QTATU'))

				MSUNLOCK()
			ENDIF
		ENDIF

	NEXT


return lRet

Static Function QUANTZZ6(oModel,cId,xValor)
	Local lRet := .T.
	IF xValor < 0
		lRet := .F.
	ENDIF
	IF oModel:GetModel("ZZ6DETAIL"):GETVALUE("ZZ6_TIPO") == 'D' .AND. xValor > oModel:GetModel("ZZ6DETAIL"):GETVALUE("ZZ6_SLANT")
		lRet := .F.
	ENDIF
return lRet

Static Function VLRUNZZ6(oModel,cId,xValor)
	Local lRet := .T.
	Local oModelZZ0 := oModel:getmodel("ZZ0MASTER")
	Local oModelZZ6 := oModel:getmodel("ZZ6DETAIL")

	IF POSICIONE("ZZ1",2,oModelZZ0:GETVALUE("ZZ0_FILIAL") + oModelZZ0:GETVALUE("ZZ0_NUMERO") + oModelZZ0:GETVALUE("ZZ0_CLIENT") + oModelZZ0:GETVALUE("ZZ0_LOJA") + oModelZZ6:GETVALUE("ZZ6_PRODUT") + oModelZZ6:GETVALUE("ZZ6_TES")  ,"ZZ1_VLRUN") >= oModelZZ6:GETVALUE("ZZ6_VLRUN") .AND. oModel:GetModel("ZZ6DETAIL"):GETVALUE("ZZ6_TIPO") == 'A'
		IF oModel:GetModel("ZZ6DETAIL"):GETVALUE("ZZ6_VLUANT") <> 0
		ALERT("Valor digitado menor que o valor de contrato!!")
		lRet := .F.
		ENDIF
	ENDIF

/*	IF POSICIONE("ZZ1",2,oModelZZ0:GETVALUE("ZZ0_FILIAL") + oModelZZ0:GETVALUE("ZZ0_NUMERO") + oModelZZ0:GETVALUE("ZZ0_CLIENT") + oModelZZ0:GETVALUE("ZZ0_LOJA") + oModelZZ6:GETVALUE("ZZ6_PRODUT") + oModelZZ6:GETVALUE("ZZ6_TES")  ,"ZZ1_VLRUN") <= oModelZZ6:GETVALUE("ZZ6_VLRUN") .AND. oModel:GetModel("ZZ6DETAIL"):GETVALUE("ZZ6_TIPO") == 'D'
		ALERT("Valor digitado maior que o valor de contrato!!")
		lRet := .F.
	ENDIF
*/
	IF oModelZZ6:GETVALUE("ZZ6_VLRUN") == 0 .AND. oModel:GetModel("ZZ6DETAIL"):GETVALUE("ZZ6_TIPO") == 'N'
		ALERT("Valor unitario zerado !!")
		lRet := .F.
	ENDIF

return lRet

Static Function TIPOZZ6(oModel,cId,xValor)
	Local lRet := .T.
	IF alltrim(xValor) == ""
		lRet := .F.
	ENDIF
return lRet

Static Function FormLinPre(oModelZZ6,nLinha,cOperacao,cCampo)
	Local oModel	:= FWModelActive()
	If cOperacao=="DELETE"
		If alltrim(oModelZZ6:GetValue("ZZ6_PRODUT"))<> "" .AND. ( oModelZZ6:GETVALUE("ZZ6_TIPO") <> 'N' .OR. ALLTRIM(oModelZZ6:GETVALUE("ZZ6_TIPO")) <> '' )
			oModel:SetErrorMessage('ZZ6DETAIL',,,,"DELETE", 'Essa linha n�o pode ser deletada!', '')
			Return .F.
		EndIf
	EndIf
Return .T.

Static Function validTipo(cTipo,oModel)
	Local cEspecie  := ""
	Local nDias
	Local oModelZZ0 := oModel:GETMODEL("ZZ0MASTER")
	Local oModelZZ6 := oModel:GETMODEL("ZZ6DETAIL")
	Local aSerieEnvio := {}
	Local dMinima
	Local dDataMax
	Local dSolicita := oModelZZ0:GETVALUE("ZZ0_DTSOLI")


	//MONTA ARRAY COM A SERIE E ENVIO DE CADA PRODUTO, PABA BUSCAR A DATA MAXIMA DE ENTREGA
	FOR nAx := 1 to len(oModelZZ6:acols)
		oModelZZ6:GoLine(nAx)
		IF oModelZZ6:getvalue("ZZ3_TIPO") == "P"
			aadd(aSerieEnvio,{ALLTRIM(POSICIONE("SB1",1,XFILIAL("SB1")+alltrim(oModelZZ6:getvalue("ZZ3_PRODUT")),"B1_YSRPRD")),oModelZZ6:getvalue("ZZ3_ENVIO") })
		ELSEIF oModelZZ6:getvalue("ZZ3_TIPO") == "K"
			cquery1 := " select * "
			cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 "
			cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
			cquery1 += " and D_E_L_E_T_ <> '*' "
			cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ6:GetValue('ZZ3_PRODUT')) +"' "
			cquery1 += " and ZZ7_TIPO = 'A' "
			cquery1 += " ORDER BY ZZ7_PRODUT"
			TCQuery cquery1 new alias T13
			WHILE T13->(!EOF())
				aadd(aSerieEnvio,{ALLTRIM(T13->ZZ7_SERIE),oModelZZ6:getvalue("ZZ3_ENVIO") })
				T13->(DbSkip())
			ENDDO
			T13->(DBCLOSEAREA())
		ENDIF
	NEXT
	oModelZZ6:GoLine(1)

	cEspecie := POSICIONE("ZZG",1,XFILIAL("ZZG")+cTipo,'ZZG_ESPCTR')

	IF POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_FIXO')== 'S'
		IF !alltrim(POSICIONE("ZZM",2,XFILIAL("ZZM")+cTipo,'ZZM_TIPO'))==""
			//IIF (POSICIONE("ZZM",1,XFILIAL("ZZM")+""+""+cTipo,'ZZM_DATA')<dSolicita,dMinima:=POSICIONE("ZZM",1,XFILIAL("ZZM")+""+""+cTipo,'ZZM_DATA'),)
			dMinima:=POSICIONE("ZZM",2,XFILIAL("ZZM")+cTipo,'ZZM_DATA')
		ELSE
			FOR nAx:= 1 to len(aSerieEnvio)
				IF nAx == 1
					dMinima:=POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')
				ELSE
					IF !vazio(POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')) .AND. (POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA') < dMinima)
						dMinima:=POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')
					ENDIF
				ENDIF
			NEXT
		ENDIF
	ELSEIF POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_FIXO')== 'N'
		FOR nAx:= 1 to len(aSerieEnvio)
			IF nAx == 1
				nDias := POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')
			ELSE
				IF !vazio(POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')) .AND. (POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS') < nDias)
					nDias := POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')
				ENDIF
			ENDIF
		NEXT
	ENDIF

	IF !Empty(nDias)
		dDataMax := dSolicita + nDias
	elseIF !Empty(dMinima)
		dDataMax := dMinima
	ENDIF

RETURN dDataMax

//Fun��o SASP034 - Fun��o para retornar o kit - Arvore.
Static Function ProcKit(oView)

	if aRetParm[6] == "Produto"
		U_SASP034('ADITIVO')
	elseif	aRetParm[6] <> "Produto"
		Msginfo("O filtro para a inclus�o de produtos deve conte no campo TIPO DE ADITIVO a op��o PRODUTO","Erro de Filtro")
	endif

Return

Static Function HistADT(oView)

	Local oModel:= FWModelActive()
	
	if aRetParm[6] == "Quantidade / Financeiro"
		U_SASP006(oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_FILIAL'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_ITEM'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_NUMERO'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_CLIENT'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_LOJA'),oModel:GetModel('ZZ6DETAIL'):GetValue('ZZ6_QNTINI'))
	elseif	aRetParm[6] <> "Quantidade / Financeiro"
		Msginfo("O filtro para aditivo de quantidade e financeiro deve conte no campo TIPO DE ADITIVO a op��o Quantidade / Financeiro","Erro de Filtro")
	endif

Return

//A vari�vel LPrimeiro, � um "flag" para verificar se � a primeira chamada da fun��o recursiva iniciada no fonte SASP034.
User function ADITIVOZZ6(cCodKit2, aDados1,lPrimeiro)

	Local oModel := FWModelActive()
	Local cCodKit	:= cCodKit2
	Local cTipo :=  ""
	Local cDesc := ""
	Local cQuery := ""
	Local cQuery9 := ""
	Local oModelZZ6 := oModel:GetModel('ZZ6DETAIL')
	Local nRegistros := 0
	Local cAliasT := GetNextAlias()
	Local nRateio := 0
	Local nValor := 1
	Local nValorDes := 0
	Local cQuery8 := ""
	Local oView := FWViewActive()
	Local nx

	//Avo - Sintetico
	//   Pai - Sintetico
	//     Filho - Sintetico
	//     Filho - Analitico
	//Retorna somente o filho sint�tico.

	If !Empty(cCodKit)

		cTipo := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_TIPO")

		If cTipo == 'S'

			cQuery := ""
			cQuery += " SELECT ZZ7_CODIGO, ZZ7_DESCR "
			cQuery += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7 "
			cQuery += "  WHERE D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7_CODPAI = '" + cCodKit +  "' "
			cQuery += "    AND ZZ7_TIPO = 'S' "
			cQuery += "    AND ZZ7_CODIGO <> '" + cCodKit + "' "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasT,.F.,.T.)

			Count To nRegistros

			(cAliasT)->(DBGOTOP())

			if nRegistros > 0

				While (cAliasT)->(!EOF())

					U_ADITIVOZZ6((cAliasT)->ZZ7_CODIGO, 0,.F.)

					(cAliasT)->(DBSKIP())

				EndDo

				(cAliasT)->(DBCLOSEAREA())
			Else

				cQuery8 := ""
				cQuery8 += " SELECT COUNT(*) AS QTD "
				cQuery8 += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7 "
				cQuery8 += "  WHERE D_E_L_E_T_ = '' "
				cQuery8 += "    AND ZZ7.ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
				cQuery8 += "    AND ZZ7.ZZ7_CODPAI = '" + cCodKit + "' "
				cQuery8 += "    AND ZZ7.ZZ7_PERDSC = 100 " "

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery8),"PERCDES",.F.,.T.)

				cDesc := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_DESCR" )
				bProf := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_CATEGO" )
				//Codigo do kit, Descri��o do item, Quantidade, TES, Valor unitario, Tipo(k = Kit)

				If PERCDES->QTD > 0

					nPerDes := PERCDES->QTD

				Else

					nPerDes := 0

				EndIf

				PERCDES->(DBCLOSEAREA())

				//Codigo do kit, Descri��o do item, Quantidade, TES, Valor unitario, Tipo(k = Kit), Percentual Desconto
				AADD(aKit, {cCodKit,cDesc,1,"",1,'K',nPerDes})

				(cAliasT)->(DBCLOSEAREA())

			EndIf

		Else
			Alert("Produto selecionado n�o � um kit. O tipo do produto deve ser SINT�TICO.")
		EndIf

	EndIf

	if ValType(aRet) == 'L'

		nValor := 1

	EndIf

	If lPrimeiro == .T.

		For nX := 1 to len(aKit)

			cQuery := ""
			cQuery += " SELECT ZZK_PORCEN "
			cQuery += "   FROM " + RETSQLNAME("ZZK") + " ZZK "
			cQuery += "  INNER JOIN  " + RETSQLNAME("ZZ7") + " ZZ7 "
			cQuery += "     ON ZZK.ZZK_FILIAL = ZZ7.ZZ7_FILIAL "
			cQuery += "    AND ZZK.ZZK_SERIE = ZZ7.ZZ7_SERIE "
			cQuery += "    AND ZZK.ZZK_ENVIO = ZZ7.ZZ7_ENVIO "
			cQuery += "  WHERE ZZK.D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7.D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7.ZZ7_CODIGO = '" + aKit[nX][1] + "' "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"PERCRAT",.F.,.T.)

			If PERCRAT->(!EOF())

				nRateio := ROUND(aRet[1] * (PERCRAT->ZZK_PORCEN / 100),2)

			Else

				Alert("O kit " + aKit[nX][1] + " n�o possui porcentagem de rateio definido, valor do produto ser� R$ 1. Verifique o cadastro do kit e tente novamente." )
				nRateio := 1

			EndIf

			PERCRAT->(DBCLOSEAREA())

			If aKit[nX][7] > 0

				nValorDes := aKit[nX][7] * GETMV("SA_PRECDSC")

			Else

				nValorDes := 0

			EndIf

			if Select("QSASPrf") > 0
				dbCloseArea("QSASPrf")
			endif

			nProf = 0

			cQrySAS := ""
			cQrySAS += " SELECT COUNT(*) AS QTD "
			cQrySAS += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7"
			cQrySAS += "  WHERE D_E_L_E_T_ = '' "
			cQrySAS += "    AND ZZ7.ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
			cQrySAS += "    AND ZZ7.ZZ7_CODPAI = '" + aKit[nX][1] + "' "
			cQrySAS += "    AND ZZ7.ZZ7_CATEGO = 'P' "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySAS),"QSASPrf",.F.,.T.)

			nProf := QSASPrf->QTD

			if nProf > 0
				nRateio := nProf * 10
				nValorDes := 0
			endif

			If oModelZZ6:GetLine() <= oModelZZ6:Length() .AND. lPrimeiro
				oModelZZ6:AddLine()
			EndIf


			dbCloseArea("QSASPrf")

			oModel:GetModel('ZZ6DETAIL'):SetValue('ZZ6_TIPO','N')
			oModel:GetModel('ZZ6DETAIL'):SetValue('ZZ6_TPPROD',aKit[nX][6])
			oModel:GetModel('ZZ6DETAIL'):SetValue('ZZ6_PRODUT',aKit[nX][1])
			oModel:GetModel('ZZ6DETAIL'):SetValue('ZZ6_DESCRI',aKit[nX][2])
			oModel:GetModel('ZZ6DETAIL'):SetValue('ZZ6_QUANT',aKit[nX][3])
			oModel:GetModel('ZZ6DETAIL'):SetValue('ZZ6_TES',aDados1[2])
			oModel:GetModel('ZZ6DETAIL'):SetValue('ZZ6_VLRDSC',nValorDes)
			oModel:GetModel('ZZ6DETAIL'):SetValue('ZZ6_VLRUN',nRateio + nValorDes )
			oModel:GetModel('ZZ6DETAIL'):SetValue('ZZ6_VLRTOT',nRateio)

			GETDREFRESH()

			If nX < len(aKit)
				oModelZZ6:AddLine()
				oView:Refresh()
			EndIf

			GETDREFRESH()
			nDifer := 0

		next nX

		aKit := {}

	EndIf
Return

Static Function ProxNum(cTipo)

	Local cNum := ""

	If Select("T13") > 0
		T13->(DBCLOSEAREA())
	EndIf

	cquery := " select TOP 1 substring(E1_NUM,4,len(E1_NUM))+1 as E1_NUM "
	cquery += " from " + retsqlname('SE1') + " SE1 "
	cquery += " where E1_FILIAL = '" + xfilial("SE1") + "' "
	cquery += " and D_E_L_E_T_ <> '*' "
	cquery += " and E1_PREFIXO = '"+"B"+SUBSTR(CFILANT,5,2)+"' "
	if cTipo == 'DP'
		cquery += " and E1_TIPO = 'ADT' "
	elseif cTipo == 'NCC'
		cquery += " and E1_TIPO = 'NCC' "
	endif
	cquery += " ORDER BY E1_NUM DESC"
	TCQuery cquery new alias T13

	IF T13->(EOF())
		Return "000001"
	ELSE
		cNum := STRZERO(T13->E1_NUM, 6, 0)
	ENDIF

	T13->(DBCLOSEAREA())

Return cNum

Static Function Z10(lOp,cItem,cFil,cNumero,cCli,cLoja,cTipo,nQtd,nVlr,cTipoA,VLRADT,cTit,cTipoTi)

	Local cQuery := ''
	Local cProxI := ''
	Local oModel:= FWModelActive()

	IF lOp
		
		cQuery := " SELECT MAX(Z10_ITEM) ITEM " 
		cQuery += " FROM " + retsqlname('Z10') + " Z10 " 
		cQuery += " WHERE D_E_L_E_T_ = '' "
		cQuery += " AND Z10_FILIAL = '"+cFil+"' "
		cQuery += " AND Z10_NUMERO = '"+cNumero+"' "
		cQuery += " AND Z10_CLIENT = '"+cCli+"' "
		cQuery += " AND Z10_LOJA = '"+cLoja+"' "
		cQuery += " AND Z10_ITEMZ6 = '"+cItem+"' "
		
		TcQuery cQuery new Alias T22
		
		IF empty(T22->ITEM)
		cProxI := "001"
		ELSE
		cProxI := SOMA1(T22->ITEM)
		ENDIF
		T22->(DbCloseArea())
		
		DbSelectArea("Z10")
		RecLock("Z10",lOp)
		
		Z10->Z10_FILIAL 	:= cFil
		Z10->Z10_ITEM	 	:= cProxI
		Z10->Z10_ITEMZ6		:= cItem
		Z10->Z10_NUMERO		:= cNumero
		Z10->Z10_CLIENT		:= cCli
		Z10->Z10_LOJA		:= cLoja
		Z10->Z10_TIPO		:= cTipo
		Z10->Z10_TPADT		:= cTipoA
		Z10->Z10_QUANT		:= nQtd
		Z10->Z10_VALOR		:= nVlr
		Z10->Z10_TITULO		:= cTit
		Z10->Z10_TIPOTI		:= cTipoTi
		Z10->Z10_USUARI		:= cusername
		Z10->Z10_DTHORA		:= DTOC(date())+ " - " + time()
		Z10->Z10_VLRADT		:= VLRADT
		MSUNLOCK() 
		//Z10->(DbCloseArea())

	ELSE
	
		cQuery := " SELECT MAX(Z10_ITEM) ITEM FROM Z10010 (NOLOCK) "
		cQuery += " WHERE D_E_L_E_T_ = '' "
		cQuery += " AND Z10_FILIAL = '"+cFil1+"' "
		cQuery += " AND Z10_NUMERO = '"+cNumero1+"' "
		cQuery += " AND Z10_CLIENT = '"+cCli1+"' "
		cQuery += " AND Z10_LOJA = '"+cLoja1+"' "
		cQuery += " AND Z10_ITEMZ6 = '"+cItem1+"' "
		TcQuery cQuery new Alias T22

		DbSelectArea("Z10")
		DbSetOrder(2)	
		IF DbSeek(cFil + T22->ITEM + cItem1 + cNumero1 + cCli1 + cLoja1)
			
			RecLock("Z10",lOp)
			Z10->Z10_TITULO		:= cTit
			Z10->Z10_TIPOTI		:= cTipoTi

			MSUNLOCK() 
		ENDIF
		//Z10->(DbCloseArea())
		T22->(DbCloseArea())
	ENDIF
	
	//TODO Z10
return

Static Function ZZ6ValTES(oModelZZ6)

	Local lRet 		:= .T.
	Local oModel 	:= FWModelActive()
	Local oModelZZ0 := oModel:GETMODEL("ZZ0MASTER")
	Local oModelZZ6 := oModel:GETMODEL("ZZ6DETAIL")
	
	
	IF oModelZZ0:GetValue('ZZ0_CONFNF') == '1' .AND. oModelZZ6:GetValue('ZZ6_TES') == '502'
	
	MsgAlert("Para clientes conforme nota N�O, utilizar a TES 581! TES 501=582 e 502=581","ATEN��O!!!")
	lRet	:= .F.
	ELSEIF oModelZZ0:GetValue('ZZ0_CONFNF') == '1' .AND. oModelZZ6:GetValue('ZZ6_TES') == '501'
	
	MsgAlert("Para clientes conforme nota N�O, utilizar a TES 582! TES 501=582 e 502=581","ATEN��O!!!")
	lRet	:= .F.
	
	ENDIF

return lRet