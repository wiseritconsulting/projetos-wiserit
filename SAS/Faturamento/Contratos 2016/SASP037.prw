#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"

User Function SASP037()

	Local oBrowse	
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('ZZK')
	oBrowse:SetDescription('Cadastro de Distribui��o dos pre�os de Kits')
	oBrowse:DisableDetails()

	oBrowse:SetMenuDef( 'SASP037' ) 
	oBrowse:Activate()

Return

Static Function MenuDef()

	Local aRotina := {}	
	
	ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PesqBrw"			OPERATION 1 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.SASP037"	OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.SASP037"	OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.SASP037"	OPERATION 4 ACCESS 143
	ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.SASP037"	OPERATION 5 ACCESS 144
	ADD OPTION aRotina TITLE "Imprimir"   ACTION "VIEWDEF.SASP037"	OPERATION 8 ACCESS 0
	
Return aRotina

Static Function ModelDef()
	
	Local oStructZZK := Nil
	Local oModel := ""
	
	oStructZZK := FWFormStruct(1,"ZZK")
	
	oModel:= MPFormModel():New("YCADZZK",/*Pre-Validacao*/,{|oModel| ZZKCommit(oModel) }/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
	
	oModel:AddFields("ZZKMASTER",/*cOwner*/, oStructZZK ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)
	
	oModel:SetPrimaryKey({"ZZK_SERIE","ZZK_ENVIO"})	
	
	//oModel:SetCommit({|oModel| ZZKCommit(oModel) },.Ftotvs.)
	//oModel:SetCommit({|oModel| FWFormCommit(oModel) },.F.)
	
	
Return (oModel)

Static Function ViewDef()

	Local oStructZZK	:= FWFormStruct( 2, 'ZZK' )	
	Local oModel		:= FWLoadModel( 'SASP037' )
	
	Local oViewtotvs	
		
	oView	:= FWFormView():New()

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField( "ZZKMASTER",oStructZZK)
	
Return oView

Static Function ZZKCommit(oModel)

	Local cSerie := ""
	Local cEnvio := ""
	
	if oModel:getOperation() == MODEL_OPERATION_INSERT
		
		cSerie := oModel:GetModel( 'ZZKMASTER' ):GetValue('ZZK_SERIE')
		cEnvio := oModel:GetModel( 'ZZKMASTER' ):GetValue('ZZK_ENVIO')
	
		DBSELECTAREA("ZZK")
		DBSETORDER(1)
		DBSEEK(cSerie + cEnvio) 
	
		If Found()
			
			oModel:SetErrorMessage('ZZKMASTER',,,,"ATEN��O", "O c�digo de serie e envio j� foram utilizados, n�o � permitido chaves duplicadas.", "Verifique o campo de serie(ZZK_SERIE) e envio (ZZK_ENVIO).")
			Return .F.
		
		EndIf
	EndIf
	
Return .T.
