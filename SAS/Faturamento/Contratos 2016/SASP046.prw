#include 'protheus.ch'
#include 'parmtype.ch'

user function SASP046()
	
Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.

Private cString := "ZZL"

dbSelectArea("ZZL")
dbSetOrder(1)

AxCadastro(cString,"Especie de Contrato",cVldExc,cVldAlt)

Return