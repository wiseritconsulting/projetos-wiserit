#Include 'Protheus.ch'
#Include 'Topconn.ch'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} RF014
Acompanhamento de OS
@type function
@author Diogo 
@since 23/10/2015
@version 1.0
/*/
/*
listar tudo
historico da os
hitorico de entrega (embarque, em transito...)
*/

User Function RF014()
	Local aColumns
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Private aDados	:= {}
	Private oBrwTOP := {}
	Private aCampSX3	:= {{"ZZ2_FILIAL"};
		,{"ZZ2_NUMERO"};
		,{"ZZ2_MEDICA"};
		,{"ZZ2_ETAPA","POSICIONE('SX5',1,xFilial('SX5',ZZ2->ZZ2_FILIAL)+'ZA'+ZZ2->ZZ2_ETAPA,'X5_DESCRI')"};
		,{"ZZ2_TIPO"};
		,{"Tipo","POSICIONE('ZZG',1,xFilial('ZZG',ZZ2->ZZ2_FILIAL)+ZZ2->ZZ2_TIPO,'ZZG_DESCRI')"};
		,{"ZZ2_TRANSP"};
		,{"Transportadora","POSICIONE('SA4',1,xFilial('SA4',ZZ2->ZZ2_FILIAL)+ZZ2->ZZ2_TRANSP,'A4_NOME')"};
		,{"ZZ2_PV01NF"};
		,{"ZZ2_SER01"};
		,{"ZZ2_PV03NF"};
		,{"ZZ2_SER03"};
		,{"C5_FRETE","POSICIONE('SC5',1,xFilial('SC5',ZZ2->ZZ2_PV01FL)+ZZ2->ZZ2_PV01,'C5_FRETE')"};
		,{"ZZ2_FRETNE","IIF(ZZ2->ZZ2_FRETNE == 'S', 'Sim', 'N�o')", "Frete Negociado"};
		,{"C5_PESOL","POSICIONE('SC5',1,ZZ2->ZZ2_PV01FL + ZZ2->ZZ2_PV01,'C5_PESOL')"};
		,{"A1_END","POSICIONE('SA1',1,xFilial('SA1',ZZ2->ZZ2_FILIAL)+ZZ2->(ZZ2_CLIENT+ZZ2_LOJA),'A1_END')"};
		,{"ZZ3_TES","POSICIONE('ZZ3',1,xFilial('ZZ3',ZZ2->ZZ2_FILIAL)+ZZ2->(ZZ2_NUMERO+ZZ2_MEDICA),'ZZ3_TES')"};
		,{"F4_TEXTO","POSICIONE('SF4',1,xFilial('SF4',ZZ2->ZZ2_FILIAL)+ZZ3->ZZ3_TES,'F4_TEXTO')"};
		,{"ZZ2_STACOM","POSICIONE('SX5',1,xFilial('SX5',ZZ2->ZZ2_FILIAL)+'ZB'+ZZ2->ZZ2_STACOM,'X5_DESCRI')"};		
		,{"ZZ2_OBSACO"};		//,{"C5_VOLUME1","POSICIONE('SC5',1,xFilial('SC5',ZZ2->ZZ2_FILIAL)+ZZ2->ZZ2_PV03NF,'C5_VOLUME1')"};
		,{"C5_VOLUME1","POSICIONE('SC5',1,ZZ2->ZZ2_PV01FL + ZZ2->ZZ2_PV01,'C5_VOLUME1')"};//"ALERT(cvaltochar(POSICIONE('SC5',1,ZZ2->ZZ2_PV03FL + ZZ2->ZZ2_PV03NF,'C5_VOLUME1')) +''+ ZZ2->ZZ2_PV03FL + ZZ2->ZZ2_PV03NF)"};///*
		,{"Valor de Nota","Transform(U_VZZ2TOT(),'@E 999,999,999.99')"};
		,{"ZZ2_DTGER"};
		,{"ZZ2_DTPREV"};
		,{"ZZ2_DTENTR"};
		,{"ZZ2_STATUS"};
		,{"Cliente Bloqueado","iif(POSICIONE('SA1',1,XFILIAL('SA1')+ ZZ2->ZZ2_CLIENT + ZZ2->ZZ2_LOJA,'A1_YBLOQ') == 'S','Sim','Nao')"};
		,{"ZZ2_CLIENT"};
		,{"ZZ2_LOJA"};
		,{"ZZ2_NOME"};
		,{"ZZ2_DTSOLIC"};
		,{"ZZ2_DTMAX"};
		,{"Dt. Fat. Sefaz","POSICIONE('SF2',2,xFilial('SF2',ZZ2->ZZ2_FILIAL)+ZZ2->ZZ2_CLIENT+ZZ2->ZZ2_LOJA+ZZ2->ZZ2_PV01NF+ZZ2->ZZ2_SER01,'F2_DAUTNFE')"};
		,{"NDIAS","ZZ2->ZZ2_DTMAX-DDATABASE","Dias","N",3,0,""};
		,{"A1_MUN","POSICIONE('SA1',1,xFilial('SA1',ZZ2->ZZ2_FILIAL)+ZZ2->(ZZ2_CLIENT+ZZ2_LOJA),'A1_MUN')"};
		,{"A1_EST","POSICIONE('SA1',1,xFilial('SA1')+ZZ2->(ZZ2_CLIENT+ZZ2_LOJA),'A1_EST')"};
		,{"ZZ2_LOCEX","ZZ2->ZZ2_LOCEX+'-'+POSICIONE('SX5',1,xFilial('SX5',ZZ2->ZZ2_FILIAL)+'X2'+ZZ2->ZZ2_LOCEX,'X5_DESCRI')",,,20}}
		
		
	oDlg := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Tela de Acompanhamento de OS',,,.F.,,,,,,.T.,,,.T. )

	oFWLayer := FWLayer():New()
	oFWLayer:Init( oDlg, .F., .T. )
	
	oFWLayer:addLine( '1', 60, .F. )  
	oFWLayer:addLine( '2', 50, .F. )

	oPanel1 := oFWLayer:getLinePanel( '1' )
	oPanel2 := oFWLayer:getLinePanel( '2' )
	
		
	//Browse 1
	oBrowForm1	:= FWFormBrowse():New()
	oBrwTOP := oBrowForm1:FWBrowse()
	oBrwTOP:SetDataTable(.T.)
	oBrwTOP:SetAlias("ZZ2")
	oBrwTOP:SetSeek()
	oBrwTOP:SetOwner( oPanel1 )
	oBrwTOP:SetProfileID("1")
	oBrwTOP:SetDescription('Tela de Acompanhamento de OS')
	oBrwTOP:DisableDetails()
	oBrwTOP:SetUseFilter(.T.)
	oBrwTOP:SetDBFFilter(.T.)
	oBrwTOP:SetMenuDef( 'RF014' )
	oBrwTOP:AddButton('Visualizar', 'u_FItensOS()', , 4, 0)
	oBrwTOP:AddButton("Frete Negociado ",{||U_FreteNegociado()},,,)

	oBrwTOP:SetChange({|| u_FItensOS() })
	
	oBrowForm1:AddFilter("Cliente Bloqueado","POSICIONE('SA1',1,XFILIAL('SA1')+ ZZ2->ZZ2_CLIENT + ZZ2->ZZ2_LOJA,'A1_YBLOQ') == 'S'",,,"ZZ2")
	oBrowForm1:AddFilter("Cliente Desbloqueados","POSICIONE('SA1',1,XFILIAL('SA1')+ ZZ2->ZZ2_CLIENT + ZZ2->ZZ2_LOJA,'A1_YBLOQ') <> 'S'",,,"ZZ2")
	
	//Browse 2
	_nj := aCoors[3]+((aCoors[3]*70)/595)
	_nh2:= (aCoors[4]*1000)/1218
	aTFolder := { "Produtos", "Hist�rico da OS","Hist�rico de Entrega"}
	oFolder := TFolder():new( aCoors[1],aCoors[2],aTFolder,,oPanel2,,,,.t.,,_nj,aCoors[4]-_nh2)
  //_width :=(aCoors[3]
  _heigth :=(aCoors[4]*1125)/1218
    
    //-------------Aba - Produtos--------------------------
 
    oBrowProd := MsBrGetDBase():New( aCoors[1], aCoors[2], _nj,aCoors[4]- _heigth ,,,, oFolder:aDialogs[1],,,,,,,,,,,, .F., "", .T.,, .F.,,, ) //COLUNA e LINHA

    aDadProd := {}
    aadd(aDadProd,{"","","","",space(40),0})
    
    // Define vetor para a browse
    oBrowProd:SetArray(aDadProd)
    
    // Cria colunas do browse
    oBrowProd:AddColumn(TCColumn():New("Filial",{ || aDadProd[oBrowProd:nAt,1] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 
    
    oBrowProd:AddColumn(TCColumn():New("Contrato",{ || aDadProd[oBrowProd:nAt,2] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 	

    oBrowProd:AddColumn(TCColumn():New("Medi��o",{ || aDadProd[oBrowProd:nAt,3] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 	

    oBrowProd:AddColumn(TCColumn():New("Produto",{ || aDadProd[oBrowProd:nAt,4] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 	

    oBrowProd:AddColumn(TCColumn():New("Descri��o",{ || aDadProd[oBrowProd:nAt,5] }; 
        ,"@!",,,"LEFT",110,.F.,.F.,,,,.F.,)) 	

    oBrowProd:AddColumn(TCColumn():New("Quantidade",{ || aDadProd[oBrowProd:nAt,6] }; 
        ,GetSx3Cache("ZZ3_QUANT","X3_PICTURE"),,,"RIGTH",,.F.,.F.,,,,.F.,)) 	
	

    //-------------Aba - Hist�rico da OS-------------------------------
    oBrowHist := MsBrGetDBase():New( aCoors[1], aCoors[2], _nj, aCoors[4]-_heigth ,,,, oFolder:aDialogs[2],,,,,,,,,,,, .F., "", .T.,, .F.,,, ) //COLUNA e LINHA

    aDadHist := {}
    aadd(aDadHist,{"","","","","","",""})
    
    // Define vetor para a browse
    oBrowHist:SetArray(aDadHist)
    
    // Cria colunas do browse
    oBrowHist:AddColumn(TCColumn():New("Filial",{ || aDadHist[oBrowHist:nAt,1] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 
    
    oBrowHist:AddColumn(TCColumn():New("Contrato",{ || aDadHist[oBrowHist:nAt,2] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 	

    oBrowHist:AddColumn(TCColumn():New("Medi��o",{ || aDadHist[oBrowHist:nAt,3] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 	

    oBrowHist:AddColumn(TCColumn():New("Etapa",{ || aDadHist[oBrowHist:nAt,4] }; 
        ,"@!",,,"LEFT",120,.F.,.F.,,,,.F.,)) 	

    oBrowHist:AddColumn(TCColumn():New("Data",{ || aDadHist[oBrowHist:nAt,5] }; 
        ,"@!",,,"CENTER",60,.F.,.F.,,,,.F.,)) 	

    oBrowHist:AddColumn(TCColumn():New("Hora",{ || aDadHist[oBrowHist:nAt,6] }; 
        ,"@!",,,"CENTER",,.F.,.F.,,,,.F.,)) 	

    oBrowHist:AddColumn(TCColumn():New("Usu�rio",{ || aDadHist[oBrowHist:nAt,7] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 	

		
    //-------------Aba - Hist�rico de Entrega-----------------------
    oBrowEntreg := MsBrGetDBase():New( aCoors[1], aCoors[2], _nj, aCoors[4]-_heigth ,,,, oFolder:aDialogs[3],,,,,,,,,,,, .F., "", .T.,, .F.,,, )

    aDadEntreg := {}
    Aadd(aDadEntreg,{"","","","","","","","","","",""})
    
    // Define vetor para a browse
    oBrowEntreg:SetArray(aDadEntreg)
    
    // Cria colunas do browse
    oBrowEntreg:AddColumn(TCColumn():New("Filial",{ || aDadEntreg[oBrowEntreg:nAt,1] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 
    
    oBrowEntreg:AddColumn(TCColumn():New("Contrato",{ || aDadEntreg[oBrowEntreg:nAt,2] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 	

    oBrowEntreg:AddColumn(TCColumn():New("Medi��o",{ || aDadEntreg[oBrowEntreg:nAt,3] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 	

    oBrowEntreg:AddColumn(TCColumn():New("C�digo",{ || aDadEntreg[oBrowEntreg:nAt,4] }; 
        ,GetSx3Cache("ZZT_CODIGO","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowEntreg:AddColumn(TCColumn():New("Usu�rio",{ || aDadEntreg[oBrowEntreg:nAt,5] }; 
        ,GetSx3Cache("ZZT_USUARI","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowEntreg:AddColumn(TCColumn():New("Nome Usu�rio",{ || aDadEntreg[oBrowEntreg:nAt,6] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowEntreg:AddColumn(TCColumn():New("Data Inclus�o",{ || aDadEntreg[oBrowEntreg:nAt,7] }; 
        ,GetSx3Cache("ZZT_DTINC","X3_PICTURE"),,,"CENTER",,.F.,.F.,,,,.F.,)) 

    oBrowEntreg:AddColumn(TCColumn():New("Hora",{ || aDadEntreg[oBrowEntreg:nAt,8] }; 
        ,GetSx3Cache("ZZT_HORA","X3_PICTURE"),,,"CENTER",,.F.,.F.,,,,.F.,)) 

    oBrowEntreg:AddColumn(TCColumn():New("Status",{ || aDadEntreg[oBrowEntreg:nAt,9] }; 
        ,GetSx3Cache("ZZT_STATUS","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowEntreg:AddColumn(TCColumn():New("Desc. Status",{ || aDadEntreg[oBrowEntreg:nAt,10] }; 
        ,GetSx3Cache("ZZT_STATUS","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowEntreg:AddColumn(TCColumn():New("Observa��o",{ || aDadEntreg[oBrowEntreg:nAt,11] }; 
        ,GetSx3Cache("ZZT_OBS","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 		
	
    //Temporizador
	bAtualiza	:= {|| oBrwTOP:Refresh(),oBrwTOP:GoTop() }
	nTempo:= SuperGetMv("SA_TMPREF",.F.,30)*1000 
	oTimer:= TTimer():New(nTempo,bAtualiza,oDlg)
	//oTimer:Activate()
	
	oBrwTOP:AddFilter("ATRASADOS "," ZZ2->ZZ2_DTMAX < DDATABASE " )

	aColumns	:= DefCampos()
	oBrwTOP:SetColumns(aColumns)

	
	oBrowForm1:Activate(oPanel1)
	oDlg:Activate(,,,.T.)

Return

Static Function DefCampos()
	Local aColumns	:= {}
	Local aColumn
	SX3->(DbSetOrder(2))	//X3_CAMPO
	For nCont:=1 to Len(aCampSX3)
		If SX3->(DbSeek(PADR(aCampSX3[nCont][1],10)))
			aColumn := {;
				GetCamDef(aCampSX3[nCont],3,,GetSx3Cache(aCampSX3[nCont][1],"X3_TITULO"));				//[n][01] T�tulo da coluna
				,&("{|| "+GetCamDef(aCampSX3[nCont],2,,GetTabDados(aCampSX3[nCont]))+"}");				//[n][02] Code-Block de carga dos dados
				,GetSx3Cache(aCampSX3[nCont][1],"X3_TIPO");					//[n][03] Tipo de dados
				,GetSx3Cache(aCampSX3[nCont][1],"X3_PICTURE");				//[n][04] M�scara
				,If(GetSx3Cache(aCampSX3[nCont][1],"X3_TIPO")=="N",2,1);	//[n][05] Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				,GetCamDef(aCampSX3[nCont],5,,GetSx3Cache(aCampSX3[nCont][1],"X3_TAMANHO"));				//[n][06] Tamanho
				,GetCamDef(aCampSX3[nCont],6,,GetSx3Cache(aCampSX3[nCont][1],"X3_DECIMAL"));				//[n][07] Decimal
				,.F.;						//[n][08] Indica se permite a edi��o
				,{||.T.};					//[n][09] Code-Block de valida��o da coluna ap�s a edi��o
				,.F.;						//[n][10] Indica se exibe imagem
				,{||nil};					//[n][11] Code-Block de execu��o do duplo clique
				,"";						//[n][12] Vari�vel a ser utilizada na edi��o (ReadVar)
				,{||nil};					//[n][13] Code-Block de execu��o do clique no header
				,.F.;						//[n][14] Indica se a coluna est� deletada
				,.F.;						//[n][15] Indica se a coluna ser� exibida nos detalhes do Browse
				,{};						//[n][16] Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			}
		Else
			aColumn := {;
				GetCamDef(aCampSX3[nCont],3,1);				//[n][01] T�tulo da coluna
				,&("{|| "+GetTabDados(aCampSX3[nCont])+"}");		//[n][02] Code-Block de carga dos dados
				,GetCamDef(aCampSX3[nCont],4,,"C");					//[n][03] Tipo de dados
				,GetCamDef(aCampSX3[nCont],7,,"");				//[n][04] M�scara
				,If(GetCamDef(aCampSX3[nCont],4,,"C")=="N",2,1);	//[n][05] Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				,GetCamDef(aCampSX3[nCont],5,,10);				//[n][06] Tamanho
				,GetCamDef(aCampSX3[nCont],6,,0);					//[n][07] Decimal
				,.F.;						//[n][08] Indica se permite a edi��o
				,{||.T.};					//[n][09] Code-Block de valida��o da coluna ap�s a edi��o
				,.F.;						//[n][10] Indica se exibe imagem
				,{||nil};					//[n][11] Code-Block de execu��o do duplo clique
				,"";						//[n][12] Vari�vel a ser utilizada na edi��o (ReadVar)
				,{||nil};					//[n][13] Code-Block de execu��o do clique no header
				,.F.;						//[n][14] Indica se a coluna est� deletada
				,.F.;						//[n][15] Indica se a coluna ser� exibida nos detalhes do Browse
				,{};						//[n][16] Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			}
		EndIf
		Aadd(aColumns, aColumn )
	Next
Return aColumns

Static Function GetCamDef(aCampo,nIndice,nIndDef,cDefault)
	Default cDefault	:= ""
	If Len(aCampo)>=nIndice .and. ValType(aCampo[nIndice])<>"U"
		Return aCampo[nIndice]
	ElseIf ValType(nIndDef)=="N" .and. nIndDef>0
		Return aCampo[nIndDef]
	Else
		Return cDefault
	EndIf
Return cDefault

Static Function GetTabDados(aCampo)
	Local cRet	:= ""
	If SubStr(aCampo[1],1,4)=="ZZ2_"
		cRet	:= "ZZ2->"+aCampo[1]
	Else
		If Len(aCampo)>=2
			cRet	:= aCampo[2]
		Else
			cRet	:= "''"
		EndIf
	EndIf
Return cRet

Static Function fLegZZS(cCOR)

	Local cRet:=""
	Local nAt
	aValores:= Separa(GetSx3Cache("ZZS_LEGEND","X3_CBOX"),";",.T.) 
	nPos			:= aScan(aValores,{|x| Left(x,Len(cCOR))==cCOR})
	If nPos>0
		nAt		:= At("=",aValores[nPos])
		cRet	:= SubStr(aValores[nPos],nAt+1)
	EndIf

Return cRet

//Visualiza Produtos/Hist�rico de OS
User Function FItensOS

	cQuery := " SELECT ZZ3_FILIAL,		"
	cQuery += " ZZ3_NUMERO,ZZ3_MEDICA,	"
	cQuery += " ZZ3_ITEM,ZZ3_PRODUT,	"
	cQuery += " ZZ3_DESCRI,ZZ3_QUANT	" 
	cQuery += " FROM "+RetSqlName("ZZ3")+" ZZ3 "
	cQuery += " JOIN "+RetSqlName("ZZ2")+" ZZ2 "
	cQuery += " ON ZZ3_FILIAL = ZZ2_FILIAL"
	cQuery += " AND ZZ3_NUMERO = ZZ2_NUMERO "
	cQuery += " AND ZZ3_MEDICA = ZZ2_MEDICA "
	cQuery += " AND ZZ3_CLIENT = ZZ2_CLIENT "
	cQuery += " WHERE ZZ3.D_E_L_E_T_ =' ' "
	cQuery += " AND ZZ2.D_E_L_E_T_ =' ' "
	cQuery += " AND ZZ3_FILIAL= '"+ZZ2->ZZ2_FILIAL+"' "
	cQuery += " AND ZZ3_NUMERO = '"+ZZ2->ZZ2_NUMERO+"' "
	cQuery += " AND ZZ3_CLIENT = '"+ZZ2->ZZ2_CLIENT+"' "
	cQuery += " AND ZZ3_LOJA = '"+ZZ2->ZZ2_LOJA+"' "
	cQuery += " AND ZZ3_MEDICA = '"+ZZ2->ZZ2_MEDICA+"' "
	cQuery += " ORDER BY ZZ3_FILIAL,ZZ3_NUMERO,ZZ3_MEDICA,ZZ3_ITEM "	
	
	TCQUERY cQuery NEW ALIAS "TQRY014PROD"
    
    If TQRY014PROD->(!eof())
    	aDadProd:={}
    Else
    	aDadProd:={}
    	aadd(aDadProd,{"","","","",space(40),0})
    Endif
    
    while TQRY014PROD->(!eof())
    	Aadd(aDadProd,{	TQRY014PROD->ZZ3_FILIAL,; 
    					TQRY014PROD->ZZ3_NUMERO,;
    					TQRY014PROD->ZZ3_MEDICA,;
    					TQRY014PROD->ZZ3_PRODUT,;
    					TQRY014PROD->ZZ3_DESCRI,;
    					TQRY014PROD->ZZ3_QUANT;
    					})
    	
    	TQRY014PROD->(dbSkip())
    enddo
    
    TQRY014PROD->(dbCloseArea())
    
    oBrowProd:SetArray(aDadProd)
    oBrowProd:Refresh()
    
    //Hist�rico da OS

    cQuery := " SELECT ZZR_FILIAL,ZZR_ETAPA, 	"
	cQuery += " ZZR_CONTRA,ZZR_MEDICA,			"
	cQuery += " ZZR_NOMETP,ZZR_DATA,			"
	cQuery += " ZZR_HORA,ZZR_NOMUSE				" 
	cQuery += " FROM "+RetSqlName("ZZR")+" ZZR 	"
	cQuery += " JOIN "+RetSqlName("ZZ2")+" ZZ2 	"
	cQuery += " ON ZZR_FILIAL = ZZ2_FILIAL"
	cQuery += " AND ZZR_CONTRA = ZZ2_NUMERO "
	cQuery += " AND ZZR_MEDICA = ZZ2_MEDICA "
	cQuery += " WHERE ZZR.D_E_L_E_T_ =' ' "
	cQuery += " AND ZZ2.D_E_L_E_T_ =' ' "
	cQuery += " AND ZZR_FILIAL= '"+ZZ2->ZZ2_FILIAL+"' "
	cQuery += " AND ZZR_CONTRA = '"+ZZ2->ZZ2_NUMERO+"' "
	cQuery += " AND ZZR_MEDICA = '"+ZZ2->ZZ2_MEDICA+"' "
	cQuery += " ORDER BY ZZR_FILIAL,ZZR_CONTRA,ZZR_MEDICA,ZZR_ETAPA "	
	
	TCQUERY cQuery NEW ALIAS "TQRY014HIST"
    
    If TQRY014HIST->(!eof())
    	aDadHist:={}
    Else
    	aDadHist := {}
    	aadd(aDadHist,{"","","","","","",""})    	
    Endif
    
    while TQRY014HIST->(!eof())
    	Aadd(aDadHist,{	TQRY014HIST->ZZR_FILIAL,; 
    					TQRY014HIST->ZZR_CONTRA,;
    					TQRY014HIST->ZZR_MEDICA,;
    					TQRY014HIST->ZZR_NOMETP,;
    					dToc(Stod(TQRY014HIST->ZZR_DATA)),;
    					TQRY014HIST->ZZR_HORA,;
    					TQRY014HIST->ZZR_NOMUSE;
    					})
    	
    	TQRY014HIST->(dbSkip())
    enddo
    
    TQRY014HIST->(dbCloseArea())
    
    oBrowHist:SetArray(aDadHist)
    oBrowHist:Refresh()  
    
    //Hist�rico de Entrega
	cQuery := " SELECT ZZT_FILIAL,ZZT_CONTRA,"
	cQuery += " ZZT_MEDICA,ZZT_CODIGO,"
	cQuery += " ZZT_USUARI,ZZT_DTINC,"
	cQuery += " ZZT_HORA,ZZT_STATUS,X5_DESCRI, ZZT_OBS " 
	cQuery += " FROM "+RetSqlName("ZZT")+" ZZT "
	cQuery += " JOIN "+RetSqlName("SX5")+" SX5 "
	cQuery += " ON ZZT_STATUS = X5_CHAVE "
	cQuery += " AND X5_TABELA = 'ZB' "
	cQuery += " WHERE ZZT.D_E_L_E_T_ =' ' "
	cQuery += " AND SX5.D_E_L_E_T_ =' ' "
	cQuery += " AND ZZT_FILIAL= '"+ZZ2->ZZ2_FILIAL+"' "
	cQuery += " AND ZZT_CONTRA = '"+ZZ2->ZZ2_NUMERO+"' "
	cQuery += " AND ZZT_MEDICA = '"+ZZ2->ZZ2_MEDICA+"' "
	cQuery += " ORDER BY ZZT_CODIGO "	

	TCQUERY cQuery NEW ALIAS "TQRY014ENTRE"
    
    If TQRY014ENTRE->(!eof())
    	aDadEntreg:={}
    Else
    	aDadEntreg := {}
    	Aadd(aDadEntreg,{"","","","","","","","","","",""})    	
    Endif
    
    while TQRY014ENTRE->(!eof())
    	Aadd(aDadEntreg,{	TQRY014ENTRE->ZZT_FILIAL,; 
    					TQRY014ENTRE->ZZT_CONTRA,;
    					TQRY014ENTRE->ZZT_MEDICA,;
    					TQRY014ENTRE->ZZT_CODIGO,;
    					TQRY014ENTRE->ZZT_USUARI,;
    					Upper(Alltrim(UsrRetName(TQRY014ENTRE->ZZT_USUARI))),;
    					dToc(Stod(TQRY014ENTRE->ZZT_DTINC)),;
    					TQRY014ENTRE->ZZT_HORA,;
    					TQRY014ENTRE->ZZT_STATUS,;
    					TQRY014ENTRE->X5_DESCRI,;
    					TQRY014ENTRE->ZZT_OBS})
    	
    	TQRY014ENTRE->(dbSkip())
    enddo
    
    TQRY014ENTRE->(dbCloseArea())
    
    oBrowEntreg:SetArray(aDadEntreg)
    oBrowEntreg:Refresh()       
    
Return

User Function FreteNegociado()
	Local bRet := msgyesno("Deseja marcar/desmarcar essa medi��o como Frete Negociado?","Frete Negociado")

	if (bRet)
		dbSelectArea("ZZ2")
		dbSetOrder(1)
		dbSeek(ZZ2->ZZ2_FILIAL + ZZ2->ZZ2_NUMERO + ZZ2->ZZ2_MEDICA + ZZ2->ZZ2_CLIENT + ZZ2->ZZ2_LOJA )     // Busca exata
		 
		IF FOUND()    // Avalia o retorno da pesquisa realizada
		          RECLOCK("ZZ2", .F.)		 
		            If(ZZ2->ZZ2_FRETNE != 'S')
		            	ZZ2->ZZ2_FRETNE := 'S'
		            Else
		            	ZZ2->ZZ2_FRETNE := ''
		            Endif
		          MSUNLOCK()     // Destrava o registro
		          
		          oBrwTOP:Refresh()
		ENDIF
	Endif
Return