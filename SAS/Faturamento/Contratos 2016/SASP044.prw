#include 'protheus.ch'
#include 'parmtype.ch'

user function SASP044()
	
Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.

Private cString := "ZZG"

dbSelectArea("ZZG")
dbSetOrder(1)

AxCadastro(cString,"Tipo de Contrato",cVldExc,cVldAlt)

Return