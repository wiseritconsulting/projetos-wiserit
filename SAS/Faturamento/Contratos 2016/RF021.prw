#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"

/*/{Protheus.doc} RF021
Cadastro de Legendas de Atraso
@author Diogo
@since 26/10/2015
@version version
@example
(examples)
@see (links_or_references)
/*/

User Function RF021()

	Local oBrowse	
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('ZZS')
	oBrowse:SetDescription('Cadastro Legendas Atraso')
	oBrowse:DisableDetails()

	oBrowse:SetMenuDef( 'RF021' ) 
	oBrowse:Activate()

Return

Static Function MenuDef()

	Local aRotina := {}	
	
	ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PesqBrw"			OPERATION 1 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.RF021"	OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.RF021"	OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.RF021"	OPERATION 4 ACCESS 143
	ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.RF021"	OPERATION 5 ACCESS 144
	ADD OPTION aRotina TITLE "Imprimir"   ACTION "VIEWDEF.RF021"	OPERATION 8 ACCESS 0
	
Return aRotina

Static Function ModelDef()
	
	Local oStructZZS := Nil
	Local oModel := ""
	
	oStructZZS := FWFormStruct(1,"ZZS")
	
	oModel:= MPFormModel():New("YCADZZS",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
	
	oModel:AddFields("ZZSMASTER",/*cOwner*/, oStructZZS ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)
	
	oModel:SetPrimaryKey({"ZZS_CODIGO"})	

	   
Return (oModel)

Static Function ViewDef()

	Local oStructZZS	:= FWFormStruct( 2, 'ZZS' )	
	Local oModel		:= FWLoadModel( 'RF021' )
	
	Local oView
		
	oView	:= FWFormView():New()

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField( "ZZSMASTER",oStructZZS)
	
Return oView