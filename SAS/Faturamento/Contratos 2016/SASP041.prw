#include 'protheus.ch'
#include "topconn.ch"
#include 'parmtype.ch'

/*/{Protheus.doc} SASP040
Fun��o para valida��o de produtos duplicados no cadastro de Kits (ZZ7)
@author Diogo
@since 16/09/2015
@version version
@example
(examples)
@see (links_or_references)
/*/
User function SASP041(cCod,cCodPai)

Local aArea	:= getArea()
Local lRet	:= .t.

cQuery := " SELECT ZZ7_PRODUT	" 
cQuery += " FROM   "+RetSqlName( 'ZZ7' ) + " ZZ7 " "
cQuery += " WHERE  ZZ7.D_E_L_E_T_='' "
cQuery += " AND    ZZ7_PRODUT = '"+cCod+"' "
cQuery += " AND    ZZ7_CODPAI = '"+cvaltochar(cCodPai)+"' "
cQuery += " AND    ZZ7_TIPO = 'A' "

TCQuery cQuery new alias TP041

If !TP041->(EOF())
	lRet:= .F.
	Aviso("Aten��o","Produto "+Alltrim(cCod)+" j� utilizado para o kit!!",{"Ok"})
Endif	

TP041->(DBCLOSEAREA())
 
RestArea(aArea)
	
Return lRet