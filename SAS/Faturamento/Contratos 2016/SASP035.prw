#include "Protheus.ch"
#include "dbtree.ch"
#include "topconn.ch"
#INCLUDE "RWMAKE.CH"

/*/{Protheus.doc} SASP035
Fun��o desenvolvida para a medi��o autom�tica dos contratos para o cliente Sistema Ari de S� (SAS)
@author Erenilson Bezerra
@since 24/08/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function SASP035()

	Local cPerg := "SASP035xR"

	Public  dSolicita

	Public cProdutos := ""
	Private cTesTmp		:= "900"	//Tes temporaria para pedido

	AjustaSX1(cPerg)

	If Pergunte(cPerg, .T.)

		ListaItens()

	EndIf

Return


//Tela para a sele��o dos produtos do tipo KIT
Static Function ListaItens()

	Local cQuery := ""
	Local oDlg
	Local oButton1
	Local oButton2
	Local oButton3
	Local oButton4
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Local cCodPaiAnt := ""
	Local XKIT := ""

	Private oTreeP35 := ""

	Public aDados := {}
	Public oBrowse := ""

	Aadd(aDados,{"",""})

	//Defini��o de tamanho de tela e posi��o
	DEFINE MSDIALOG oDlg TITLE "Medi��o Autom�tica" FROM 000, 000  TO 570, 1300 COLORS 0, 16777215 PIXEL

	@ 140, 150 BUTTON oButton1 PROMPT "Incluir" SIZE 064, 013 OF oDlg ACTION AddItem(oTreeP35:GETCARGO()) PIXEL
	@ 140, 234 BUTTON oButton2 PROMPT "Excluir" SIZE 064, 013 OF oDlg ACTION DelItem() PIXEL
	@ 140, 318 BUTTON oButton3 PROMPT "Confirmar" SIZE 064, 013 OF oDlg ACTION (oDlg:End(),ListaProd()) PIXEL

	@ 140, 402 BUTTON oButton4 PROMPT "Cancelar" SIZE 064, 013 OF oDlg ACTION oDlg:End() PIXEL

	//Consulta SQL para retornar os registros dos kits baseado nos parametros informados. Vide: AjustaSX1()
	cQuery := " SELECT DISTINCT  ZZ7.ZZ7_CODIGO, ZZ7.ZZ7_CODPAI , ZZ7_PRODUT  , ZZ7_DESCR , ZZ7_TIPO                                           "
	cQuery += "  FROM "+RetSqlName("ZZ1")+" ZZ1			"
	cQuery += " INNER JOIN "+RetSqlName("ZZ7")+" ZZ7	"
	cQuery += "    ON ZZ1.ZZ1_PRODUT = ZZ7.ZZ7_CODIGO	"
	cQuery += " INNER JOIN "+RetSqlName("ZZ0")+" ZZ0    "
	cQuery += "    ON ZZ1.ZZ1_FILIAL = ZZ0.ZZ0_FILIAL   "
	cQuery += "   AND ZZ1.ZZ1_NUMERO = ZZ0.ZZ0_NUMERO   "
	cQuery += "   AND ZZ1.ZZ1_CLIENT = ZZ0.ZZ0_CLIENT   "
	cQuery += "   WHERE ZZ1.D_E_L_E_T_ = ''             "
	cQuery += "   AND ZZ1.ZZ1_SALDO > 0                 "
	cQuery += "   AND ZZ1.ZZ1_STATUS = 'A'              "
	cQuery += "   AND ZZ0.D_E_L_E_T_ = ''               "
	//cQuery += "   AND ZZ0.ZZ0_STATUS IN ('A','L')       "
	cQuery += "   AND ZZ0.ZZ0_STATUS IN ('L')       "
	cQuery += "   AND ZZ1_TIPO = 'K'  "
	cQuery += "   AND ZZ7.D_E_L_E_T_ = ''"
	//Cliente de:
	If !empty(mv_par01)
		cQuery += "   AND ZZ1.ZZ1_CLIENT >= '" + mv_par01 + "' "
	EndIF

	//Cliente at�:
	If !empty(mv_par02)
		cQuery += "   AND ZZ1.ZZ1_CLIENT <= '" + mv_par02 + "' "
	EndIF

	//Contrato de:
	If !empty(mv_par03)
		cQuery += "   AND ZZ1.ZZ1_NUMERO >= '" + mv_par03 + "' "
	EndIF

	//Contrato at�:
	If !empty(mv_par04)
		cQuery += "   AND ZZ1.ZZ1_NUMERO <= '" + mv_par04 + "' "
	EndIf

	//TES:
	If !empty(mv_par08)
		cQuery += "   AND ZZ1.ZZ1_TES = '" + mv_par08 + "' "
	EndIf

	cQuery += "   UNION "
	cQuery += " SELECT DISTINCT  ZZ7.ZZ7_CODIGO, ZZ7.ZZ7_CODPAI,ZZ7_PRODUT, ZZ7_DESCR , ZZ7_TIPO	"
	cQuery += "  FROM "+RetSqlName("ZZ7")+" ZZ7														"
	cQuery += " WHERE ZZ7.ZZ7_CODIGO IN ( SELECT DISTINCT ZZ7.ZZ7_CODPAI							"
	cQuery += "                             FROM "+RetSqlName("ZZ1")+" ZZ1    						"
	cQuery += "                            INNER JOIN "+RetSqlName("ZZ7")+" ZZ7						"
	cQuery += "                               ON ZZ1.ZZ1_PRODUT = ZZ7.ZZ7_CODIGO    				"
	cQuery += "                            INNER JOIN "+RetSqlName("ZZ0")+" ZZ0                   	"
	cQuery += "                               ON ZZ1.ZZ1_FILIAL = ZZ0.ZZ0_FILIAL                    "
	cQuery += "                              AND ZZ1.ZZ1_NUMERO = ZZ0.ZZ0_NUMERO                    "
	cQuery += "                              AND ZZ1.ZZ1_CLIENT = ZZ0.ZZ0_CLIENT                    "
	cQuery += "                            WHERE ZZ1.D_E_L_E_T_ = ''                                "
	cQuery += "                              AND ZZ1.ZZ1_SALDO > 0                                  "
	cQuery += "                              AND ZZ1.ZZ1_STATUS = 'A'                               "
	cQuery += "                              AND ZZ0.D_E_L_E_T_ = ''                                "
	//cQuery += "                              AND ZZ0.ZZ0_STATUS IN ('A','L')                  		"
	cQuery += "                              AND ZZ0.ZZ0_STATUS IN ('L')                  		"
	cQuery += "                              AND ZZ1_TIPO = 'K'  									"

	//Cliente de:
	If !empty(mv_par01)
		cQuery += "   AND ZZ1.ZZ1_CLIENT >= '" + mv_par01 + "' "
	EndIF

	//Cliente at�:
	If !empty(mv_par02)
		cQuery += "   AND ZZ1.ZZ1_CLIENT <= '" + mv_par02 + "' "
	EndIF

	//Contrato de:
	If !empty(mv_par03)
		cQuery += "   AND ZZ1.ZZ1_NUMERO >= '" + mv_par03 + "' "
	EndIF

	//Contrato at�:
	If !empty(mv_par04)
		cQuery += "   AND ZZ1.ZZ1_NUMERO <= '" + mv_par04 + "' "
	EndIf

	//TES:
	If !empty(mv_par08)
		cQuery += "   AND ZZ1.ZZ1_TES = '" + mv_par08 + "' "
	EndIf

	cQuery += "		AND ZZ7.D_E_L_E_T_ = '' "
	cQuery += "		AND ZZ7_TIPO = 'S') "

	cQuery += " UNION "
	cQuery += " SELECT DISTINCT  ZZ7_CODIGO, ZZ7_CODPAI , ZZ7_PRODUT  , ZZ7_DESCR , ZZ7_TIPO "
	cQuery += " FROM "+RetSqlName("ZZ7")+" ZZ7		"
	cQuery += " WHERE ZZ7_CODIGO ='1' AND D_E_L_E_T_=''	"

	cQuery += "   ORDER BY 2,1 "

	//Instaciando objeto da classe DBTree. A arvore conter� os kits e seus "pais"
	oTreeP35 := DbTree():New(2,2,aCoors[3] * 0.24 ,aCoors[4] * 0.5 ,oDlg,,,.T.,.F.)
	//oTreeP35 := DbTree():New(2,2,((oDlg:nClientHeight)/4)-15,oDlg:nClientWidth - 600,oDlg,,,.T.,.F.)

	oTreeP35:SetScroll(1,.T.)
	oTreeP35:SetScroll(2,.T.)

	oTreeP35:BeginUpdate()


	TCQuery cQuery new alias XZ1Z7

	//Percorre a �rea criado para alimentar os dados da arvore.
	While XZ1Z7->(!EOF())

		Incproc('Gerando KITS...')
		ccargo := XZ1Z7->ZZ7_CODIGO

		IF  alltrim(cCodPaiAnt) $ alltrim(XZ1Z7->ZZ7_CODPAI)
			oTreeP35:TreeSeek(XZ1Z7->ZZ7_CODPAI)
		else
			oTreeP35:TreeSeek("1")
		ENDIF

		cCodPaiAnt := XZ1Z7->ZZ7_CODPAI

		If XZ1Z7->ZZ7_TIPO = 'A'
			oTreeP35:AddItem(Alltrim(XZ1Z7->ZZ7_PRODUT) + " - " + Posicione("SB1",1,xFilial("SB1")+XZ1Z7->ZZ7_PRODUT,"B1_DESC" ),XZ1Z7->ZZ7_CODIGO,"FOLDER12","FOLDER13",,,2)
		Else
			oTreeP35:AddItem(Alltrim(XZ1Z7->ZZ7_CODIGO) + " - " + XZ1Z7->ZZ7_DESCR,XZ1Z7->ZZ7_CODIGO,"FOLDER6","FOLDER6",,,2)
		Endif

		XZ1Z7->(DBSKIP())

	EndDo

	oTreeP35:EndUpdate()
	oTreeP35:Refresh()

	XZ1Z7->(DBCLOSEAREA())
	//2,2,aCoors[3] * 0.24 ,aCoors[4] * 0.5
	oBrowse := MsBrGetDBase():New( 160,2, aCoors[3] * 1.133 ,aCoors[4] * 0.095,,,, oDlg,,,,,,,,,,,, .F., "", .T.,, .F.,,, )
	//oBrowse := MsBrGetDBase():New( 160,2, oDlg:nClientWidth -110, 170,,,, oDlg,,,,,,,,,,,, .F., "", .T.,, .F.,,, )
	oBrowse:SetArray(aDados)

	//Cria duas colunas no grid no rodap�.
	oBrowse:AddColumn(TCColumn():New("Kit"	,{ || aDados[oBrowse:nAt,1] },,,,"LEFT",,.F.,.F.,,,,.F.,))
	oBrowse:AddColumn(TCColumn():New("Descri��o"		,{ || aDados[oBrowse:nAt,2] },,,,"LEFT",,.F.,.F.,,,,.F.,))

	Activate MsDialog oDlg CENTERED

	Release object oTreeP35

Return

//Adiciona itens ao grid a partir do botao incluir na interface.
Static Function AddItem(cCodProd )

	cQuery := " SELECT DISTINCT  ZZ7.ZZ7_CODIGO ZZ7_CODIGO , ZZ7.ZZ7_CODPAI ZZ7_CODPAI , ZZ7_PRODUT  , ZZ7_DESCR , ZZ7_TIPO                                           "
	cQuery += "  FROM "+RetSqlName("ZZ1")+" ZZ1			"
	cQuery += " INNER JOIN "+RetSqlName("ZZ7")+" ZZ7	"
	cQuery += "    ON ZZ1.ZZ1_PRODUT = ZZ7.ZZ7_CODIGO	AND (ZZ7.ZZ7_CODPAI ='"+cCodProd+"' OR ZZ7.ZZ7_CODIGO ='"+cCodProd+"') "
	cQuery += " INNER JOIN "+RetSqlName("ZZ0")+" ZZ0    "
	cQuery += "    ON ZZ1.ZZ1_FILIAL = ZZ0.ZZ0_FILIAL   "
	cQuery += "   AND ZZ1.ZZ1_NUMERO = ZZ0.ZZ0_NUMERO   "
	cQuery += "   AND ZZ1.ZZ1_CLIENT = ZZ0.ZZ0_CLIENT   "
	cQuery += "   WHERE ZZ1.D_E_L_E_T_ = ''             "
	cQuery += "   AND ZZ1.ZZ1_SALDO > 0                 "
	cQuery += "   AND ZZ1.ZZ1_STATUS = 'A'              "
	cQuery += "   AND ZZ0.D_E_L_E_T_ = ''               "
	//cQuery += "   AND ZZ0.ZZ0_STATUS IN ('A','L')       "
	cQuery += "   AND ZZ0.ZZ0_STATUS IN ('L')       "
	cQuery += "   AND ZZ1_TIPO = 'K'  "
	cQuery += "   AND ZZ7.D_E_L_E_T_ = ''"
	//Cliente de:
	If !empty(mv_par01)
		cQuery += "   AND ZZ1.ZZ1_CLIENT >= '" + mv_par01 + "' "
	EndIF

	//Cliente at�:
	If !empty(mv_par02)
		cQuery += "   AND ZZ1.ZZ1_CLIENT <= '" + mv_par02 + "' "
	EndIF

	//Contrato de:
	If !empty(mv_par03)
		cQuery += "   AND ZZ1.ZZ1_NUMERO >= '" + mv_par03 + "' "
	EndIF

	//Contrato at�:
	If !empty(mv_par04)
		cQuery += "   AND ZZ1.ZZ1_NUMERO <= '" + mv_par04 + "' "
	EndIf
	TCQuery cQuery new alias XKIT

	while	! XKIT->(EOF())
		If aScanX(aDados,{ |X| X[1] == XKIT->ZZ7_CODIGO}) == 0

			If empty(aDados[1][1])
				aDados[1][1] := XKIT->ZZ7_CODIGO
				aDados[1][2] := POSICIONE('ZZ7',1,XFILIAL("ZZ7")+XKIT->ZZ7_CODIGO, "ZZ7->ZZ7_DESCR")
			Else
				AADD(aDados,{XKIT->ZZ7_CODIGO,POSICIONE('ZZ7',1,XFILIAL("ZZ7")+XKIT->ZZ7_CODIGO, "ZZ7->ZZ7_DESCR")})
			EndIf

		Else
			MsgBox("O Kit "+ Alltrim(XKIT->ZZ7_CODIGO) + " - " + Alltrim(POSICIONE('ZZ7',1,XFILIAL("ZZ7")+XKIT->ZZ7_CODIGO, "ZZ7->ZZ7_DESCR")) + " j� foi incluido.")
		EndIf

		XKIT->(DBSKIP())
	enddo
	XKIT->(DBCLOSEAREA())
	oBrowse:CallRefresh()
	/*
	while	! XKIT->(EOF())
	If aScanX(aDados,{ |X| X[1] == cCodProd}) == 0

	If empty(aDados[1][1])
	aDados[1][1] := cCodProd
	aDados[1][2] := POSICIONE('ZZ7',1,XFILIAL("ZZ7")+cCodProd, "ZZ7->ZZ7_DESCR")
	Else
	AADD(aDados,{cCodProd,POSICIONE('ZZ7',1,XFILIAL("ZZ7")+cCodProd, "ZZ7->ZZ7_DESCR")})
	EndIf

	Else
	MsgBox("O Kit "+ Alltrim(cCodProd) + " - " + Alltrim(POSICIONE('ZZ7',1,XFILIAL("ZZ7")+cCodProd, "ZZ7->ZZ7_DESCR")) + " j� foi incluido.")
	EndIf
	enddo
	oBrowse:CallRefresh()
	*/
Return

//Remove itens ao grid a partir do botao excluir na interface.
Static Function DelItem()

	Local aDadosC := {}

	If !empty(aDados[1][1])
		For nX := 1 to len(aDados)

			If nX <> oBrowse:nAt .AND. !empty(aDados[nX][1])

				AADD(aDadosC,{aDados[nX][1], aDados[nX][2]})

			EndIf

		next nX

		if len(aDadosC) == 0
			Aadd(aDadosC,{"",""})
		EndIf

		aDados := {}
		aDados := aClone(aDadosC)

		oBrowse:SetArray(aDados)
		oBrowse:CallRefresh()
	Endif

Return

//Lista produtos da "SB1" utilizados nos contratos selecionados nos parametros.
Static Function ListaProd()

	Local cQuery := ""
	Local cStatus := ""
	Local _lRetorno := .F. //Validacao da dialog criada oDlg
	Local _nOpca := 0 //Opcao da confirmacao
	Local _aStruTrb := {} //Estrutura da tabela tempor�ria
	Local _aBrowse := {} //Array do browse para demonstrac�o dos produtos
	Local bOk := {|| oBrwTrb:bMark := {| | Disp()}	,_nOpca:=1,_lRetorno:=.T.,oDlg2:End() } //Bot�o de ok
	Local bCancel := {|| _nOpca:=0,oBrwTrb:oBrowse:Refresh(),oDlg2:End() } //Bot�o de cancelamento
	Private lInverte := .F. //Variaveis para o MsSelect
	Private cMarca := GetMark() //Variaveis para o MsSelect
	Private oBrwTrb //objeto do msselect
	Private oDlg2 //Objeto Dialog

	If Select("TRB") > 0
		DbSelectArea("TRB")
		DbCloseArea()
	EndIf

	If Select("XTABPROD") > 0
		DbSelectArea("XTABPROD")
		DbCloseArea()
	EndIf


	//Select para retornar os produtos que N�O s�o kits a partir do parametros informados inicialmente.
	cQuery := ""
	cQuery += " SELECT DISTINCT "
	cQuery += "    ZZ1.ZZ1_PRODUT, "
	cQuery += "    ZZ1.ZZ1_DESC "
	cQuery += "   FROM " + RETSQLNAME("ZZ1") + " ZZ1 "
	cQuery += "  INNER JOIN " + RETSQLNAME("ZZ0")+ " ZZ0 "
	cQuery += "     ON ZZ1.ZZ1_FILIAL = ZZ0.ZZ0_FILIAL "
	cQuery += "    AND ZZ1.ZZ1_NUMERO = ZZ0.ZZ0_NUMERO "
	cQuery += "    AND ZZ1.ZZ1_CLIENT = ZZ0.ZZ0_CLIENT "
	cQuery += "  WHERE ZZ1.D_E_L_E_T_ = '' "
	cQuery += "    AND ZZ1.ZZ1_TIPO = 'P' "
	cQuery += "    AND ZZ1.ZZ1_SALDO > 0 "
	cQuery += "    AND ZZ1.ZZ1_STATUS = 'A' "
	//cQuery += "    AND ZZ0.ZZ0_STATUS IN ('A','L') "
	cQuery += "    AND ZZ0.ZZ0_STATUS IN ('L') "
	cQuery += "    AND ZZ0.D_E_L_E_T_ = '' "

	//Cliente de:
	If !empty(mv_par01)
		cQuery += "   AND ZZ1.ZZ1_CLIENT >= '" + mv_par01 + "' "
	EndIF

	//Cliente at�:
	If !empty(mv_par02)
		cQuery += "   AND ZZ1.ZZ1_CLIENT <= '" + mv_par02 + "' "
	EndIF

	//Contrato de:
	If !empty(mv_par03)
		cQuery += "   AND ZZ1.ZZ1_NUMERO >= '" + mv_par03 + "' "
	EndIF

	//Contrato at�:
	If !empty(mv_par04)
		cQuery += "   AND ZZ1.ZZ1_NUMERO <= '" + mv_par04 + "' "
	EndIf

	cQuery += "   ORDER BY ZZ1.ZZ1_PRODUT "

	TcQuery cQuery New Alias XTABPROD

	DBSELECTAREA("XTABPROD")

	//Definindo os campos para a tabela tempor�ria
	AADD(_aStruTrb,{"OK" ,"C",2,0}) //Campo de OK
	AADD(_aStruTrb,{"CODPROD" ,"C",GetSX3Cache("B1_COD","X3_TAMANHO"),0})      //C�digo do produto
	AADD(_aStruTrb,{"DESCPROD" ,"C",GetSX3Cache("B1_DESC","X3_TAMANHO"),0})    //Descri��o do produto

	//Definindo os campos para o GRID
	AADD(_aBrowse,{"OK" ,,""})  //Campo de OK
	AADD(_aBrowse,{"CODPROD" ,,"C�digo"})      //C�digo do produto
	AADD(_aBrowse,{"DESCPROD" ,"","Descri��o"})    //Descri��o do produto

	_cArqEmp := CriaTrab(_aStruTrb,.T.)

	dbUseArea(.T.,__LocalDriver,_cArqEmp,"TRB")
	cIndex := CriaTrab(nil,.f.)
	cChave := "CODPROD"
	IndRegua("TRB",cIndex,cChave,,,"Selecionando Registros...")

	XTABPROD->(DBGOTOP())

	//Percorre a area do Select para popular a �rea tempor�ria
	While XTABPROD->(!EOF())

		RecLock("TRB",.T.)

		TRB->OK := space(2)
		TRB->CODPROD := XTABPROD->ZZ1_PRODUT
		TRB->DESCPROD := XTABPROD->ZZ1_DESC

		MsUnlock()

		XTABPROD->(DBSKIP())
	EndDo
	XTABPROD->(DBCLOSEAREA())

	@ 001,001 TO 400,700 DIALOG oDlg2 TITLE OemToAnsi("Selecione os itens do(s) contrato(s) para medi��o")

	oBrwTrb := MsSelect():New("TRB","OK","",_aBrowse,@lInverte,@cMarca,{025,001,170,350})
	oBrwTrb:oBrowse:lCanAllmark := .T.
	oBrwTrb:oBrowse:align := CONTROL_ALIGN_ALLCLIENT
	oBrwTrb:oBrowse:bAllMark := {|| MarcaTudo()}
	//oBrwTrb:oBrowse:bLDblClick :=  { || ListaCon(TRB->DESCPROD)}
	Eval(oBrwTrb:oBrowse:bGoTop)

	oBrwTrb:oBrowse:Refresh()

	oDlg2:lMaximized := .T.
	Activate MsDialog oDlg2 On Init (EnchoiceBar(oDlg2,bOk,bCancel,,)) Centered VALID _lRetorno

	If _nOpca == 1
		ListaCon()
	Endif

Return

//Tela para a sele��o de contratos a partir dos parametros informados inicialmente.
Static Function ListaCon(cNome)

	Local cQuery4 := ""
	Local cStatus := ""
	Local _lRetorno := .F. //Validacao da dialog criada oDlg
	Local _nOpca := 0 //Opcao da confirmacao
	Local _aStruTrb := {} //Estrutura da tabela tempor�ria
	Local _aBrowse := {} //Array do browse para demonstrac�o dos produtos
	Local bOk := {|| oBrwTrb3:bMark := {| | Disp()}	,_nOpca:=1,_lRetorno:=.T.,oDlg3:End() } //Bot�o de ok
	Local bCancel := {|| _nOpca:=0,oBrwTrb3:oBrowse:Refresh(),oDlg3:End() } //Bot�o de cancelamento
	Private lInverte := .F. //Variaveis para o MsSelect
	Private cMarca2 := GetMark() //Variaveis para o MsSelect
	Private oBrwTrb3 //objeto do msselect
	Private oDlg3 //Objeto Dialog

	//TODO La�o para agrupar todos os c�digos de KITS selecionados pelo usu�rio
	For nX:= 1 to len(aDados)

		cProdutos += "'" + aDados[nX][1]	+"',"

	next nX

	TRB->(DBGOTOP())

	While TRB->(!EOF())

		If !Empty(TRB->OK)

			cProdutos += "'" + TRB->CODPROD	+"',"

		EndIf

		TRB->(DBSKIP())
	EndDo

	cProdutos := SUBSTRING(cProdutos,1,len(cProdutos)-1)


	cQuery4 := " SELECT "
	cQuery4 += "   ZZ0.ZZ0_FILIAL, "
	cQuery4 += "   ZZ0.ZZ0_NUMERO, "
	cQuery4 += "   ZZ0.ZZ0_CLIENT, "
	cQuery4 += "   ZZ0.ZZ0_LOJA, "
	cQuery4 += "   ZZ0.ZZ0_NOME "
	cQuery4 += "   FROM " + RETSQLNAME("ZZ0")+ " ZZ0 "
	cQuery4 += "  INNER JOIN (SELECT ZZ1.ZZ1_FILIAL, "
	cQuery4 += "                 ZZ1.ZZ1_NUMERO, "
	cQuery4 += "                 ZZ1.ZZ1_CLIENT, "
	cQuery4 += "                 SUM(ZZ1.ZZ1_SALDO) AS ZZ1_SALDO "
	cQuery4 += "                FROM " + RETSQLNAME("ZZ1")+ " ZZ1 "
	cQuery4 += "               WHERE ZZ1.D_E_L_E_T_ = '' "
	cQuery4 += "                 AND ZZ1.ZZ1_SALDO > 0 "
	cQuery4 += "                 AND ZZ1.ZZ1_STATUS = 'A' "
	cQuery4 += "                 AND ZZ1.ZZ1_PRODUT IN( " + cProdutos + " )"
	cQuery4 += "               GROUP BY ZZ1.ZZ1_FILIAL,"
	cQuery4 += "                        ZZ1.ZZ1_NUMERO, "
	cQuery4 += "                        ZZ1.ZZ1_CLIENT) AS TZZ1 "
	cQuery4 += "     ON ZZ0.ZZ0_FILIAL = TZZ1.ZZ1_FILIAL "
	cQuery4 += "    AND ZZ0.ZZ0_NUMERO = TZZ1.ZZ1_NUMERO "
	cQuery4 += "    AND ZZ0.ZZ0_CLIENT = TZZ1.ZZ1_CLIENT "
	cQuery4 += "  WHERE ZZ0.D_E_L_E_T_ = '' "
	cQuery4 += "    AND TZZ1.ZZ1_SALDO > 0 "
	//cQuery4 += "    AND ZZ0.ZZ0_STATUS IN ('L','A') "
	cQuery4 += "    AND ZZ0.ZZ0_STATUS IN ('L') "

	//Cliente de:
	If !empty(mv_par01)
		cQuery4 += "   AND TZZ1.ZZ1_CLIENT >= '" + mv_par01 + "' "
	EndIF

	//Cliente at�:
	If !empty(mv_par02)
		cQuery4 += "   AND TZZ1.ZZ1_CLIENT <= '" + mv_par02 + "' "
	EndIF

	//Contrato de:
	If !empty(mv_par03)
		cQuery4 += "   AND TZZ1.ZZ1_NUMERO >= '" + mv_par03 + "' "
	EndIF

	//Contrato at�:
	If !empty(mv_par04)
		cQuery4 += "   AND TZZ1.ZZ1_NUMERO <= '" + mv_par04 + "' "
	EndIf

	If Select("TRB2") > 0
		DbSelectArea("TRB2")
		DbCloseArea()
	EndIf

	If Select("XCONT") > 0
		DbSelectArea("XCONT")
		DbCloseArea()
	EndIf

	TcQuery cQuery4 New Alias XCONT

	DBSELECTAREA("XCONT")

	//Definindo os campos para a tabela tempor�ria
	AADD(_aStruTrb,{"OK" ,"C",2,0}) //Campo de OK
	AADD(_aStruTrb,{"CODFILIAL" ,"C",GetSX3Cache("ZZ0_FILIAL","X3_TAMANHO"),0})      //Filial
	AADD(_aStruTrb,{"CODCONTR" ,"C",GetSX3Cache("ZZ0_NUMERO","X3_TAMANHO"),0})    //C�digo contrato
	AADD(_aStruTrb,{"CODCLIENTE" ,"C",GetSX3Cache("ZZ0_CLIENTE","X3_TAMANHO"),0})    //C�digo cliente
	AADD(_aStruTrb,{"NOMECLIENT" ,"C",GetSX3Cache("ZZ0_NOME","X3_TAMANHO"),0})    //Nome Cliente

	//Definindo os campos para o GRID
	AADD(_aBrowse,{"OK" ,,""})  //Campo de OK
	AADD(_aBrowse,{"CODFILIAL" ,,"Filial"})      //Filial
	AADD(_aBrowse,{"CODCONTR" ,"","Contrato"})    //C�digo Contrato
	AADD(_aBrowse,{"CODCLIENTE" ,"","Cliente"})    //C�digo Cliente
	AADD(_aBrowse,{"NOMECLIENT" ,"","Nome"})    //C�digo Cliente

	_cArqEmp := CriaTrab(_aStruTrb,.T.)

	dbUseArea(.T.,__LocalDriver,_cArqEmp,"TRB2")
	cIndex := CriaTrab(nil,.f.)
	cChave := "CODCONTR"
	IndRegua("TRB2",cIndex,cChave,,,"Selecionando Registros...")

	XCONT->(DBGOTOP())

	While XCONT->(!EOF())

		cCliBlq	:=	Posicione("SA1",1,xFilial("SA1")+XCONT->ZZ0_CLIENT+XCONT->ZZ0_LOJA,"A1_MSBLQL" )

		If cCliBlq == '1' //Cliente Bloqueado

			//Select para identificar se um dos itens gera financeiro, caso posivito, n�o poder� gerar medi��o!
			cQuery	:=	"SELECT COUNT(*) QTD FROM "+RetSqlName("ZZ1")+" ZZ1	"
			cQuery	+=	"	JOIN "+RetSqlName("SF4")+" SF4	"
			cQuery	+=	"	ON F4_FILIAL = '"+xFilial("SF4",XCONT->ZZ0_FILIAL)+"' "
			cQuery	+=	"		AND ZZ1_FILIAL = '"+xFilial("ZZ1",XCONT->ZZ0_FILIAL)+"' "
			cQuery	+=	"		AND ZZ1_TES = F4_CODIGO	"
			cQuery	+=	"	WHERE ZZ1_FILIAL+ZZ1_NUMERO+ZZ1_CLIENT+ZZ1_LOJA = '"+XCONT->ZZ0_FILIAL+XCONT->ZZ0_NUMERO+XCONT->ZZ0_CLIENT+XCONT->ZZ0_LOJA+"'"
			cQuery	+=	"	AND ZZ1.D_E_L_E_T_ =' ' "
			cQuery	+=	"	AND SF4.D_E_L_E_T_ =' ' 	"
			cQuery	+=	"	AND SF4.F4_DUPLIC = 'S'		" //Gera Duplicada
			cQuery 	+= 	"   AND ZZ1.ZZ1_SALDO > 0 		"
			cQuery 	+= 	"   AND ZZ1.ZZ1_STATUS = 'A'	"

			If Select("XP0351") > 0
				dbSelectArea("XP0351")
				XP0351->(dbCloseArea())
			Endif
			TCQuery cQuery new alias XP0351

			If XP0351->QTD > 0 .and. XP0351->(!eof())

				dbSelectArea("XCONT")
				XCONT->(dbSkip())
				loop

			Endif

			XP0351->(dbCloseArea())

		Endif

		RecLock("TRB2",.T.)

		TRB2->OK 			:= space(2)
		TRB2->CODFILIAL 	:= XCONT->ZZ0_FILIAL
		TRB2->CODCONTR 		:= XCONT->ZZ0_NUMERO
		TRB2->CODCLIENTE	:= XCONT->ZZ0_CLIENT
		TRB2->NOMECLIENT	:= XCONT->ZZ0_NOME

		MsUnlock()

		XCONT->(DBSKIP())
	EndDo
	XCONT->(DBCLOSEAREA())

	@ 001,001 TO 400,700 DIALOG oDlg3 TITLE OemToAnsi("Selecione os contratos para medi��o")

	oBrwTrb3 := MsSelect():New("TRB2","OK","",_aBrowse,@lInverte,@cMarca2,{025,001,170,350})
	oBrwTrb3:oBrowse:lCanAllmark := .T.
	oBrwTrb3:oBrowse:align := CONTROL_ALIGN_ALLCLIENT
	oBrwTrb3:oBrowse:bAllMark := {|| MarcaTudo2()}

	oBrwTrb3:oBrowse:bLDblClick :=  { || ShowItem(TRB2->CODFILIAL, TRB2->CODCONTR, TRB2->CODCLIENTE)}
	Eval(oBrwTrb3:oBrowse:bGoTop)
	oBrwTrb3:oBrowse:Refresh()

	oDlg3:lMaximized := .T.
	Activate MsDialog oDlg3 On Init (EnchoiceBar(oDlg3,bOk,bCancel,,)) Centered VALID _lRetorno

	If _nOpca == 1
		MedicAut()
	Endif

Return

Static Function MedicAut()

	Local cContrato	:= ""
	Local cCNPJ		:= FWArrFilAtu()[18] //Coligada
	Local nSaldo		:= 0
	Local nQtd			:= 0
	Local lRet			:= .T.
	Local nCont,nCont2
	Local cMedicao
	Local aCab			:= {}
	Local aItens		:= {}
	Local aItens2		:= {}
	Local nQtdMed		:= 0
	Local nTam
	Local aCliente	:= {}
	Local nContIT		:= 0
	Local nVlrItem	:= 0
	Local nVlrTotal	:= 0
	Local nVlrAdiant	:= 0
	Local lKit
	Local cTabPad		:= SuperGetMV("SA_TABPREC")
	Local nj
	Local aLocks		:= {}
	Local oView		:= FWViewActive()
	Local nValVenda := 1
	//TELA TIPO DE MEDI��O
	Local oButton1
	Local oButton2
	Local NCOMBOBO1
	Local oCheckBo1
	Local lCheckBo1 := .F.
	Local oCheckBo2
	Local lCheckBo2 := .F.
	Local nOpca := 0
	Local oSay1
	Local oDlg
	Local cQuery := ""
	Local cQuery2 := ""
	Local cQuery3 := ""
	Local lPergunta := .F.

	Private nFret		:= 0
	Private nDesc		:= 0
	Private lMsErroAuto	:= .F.

	TRB2->(DBGOTOP())
	//TODO inicio while - medicaoautomica
	WHILE TRB2->(!EOF())

		aItens		:= {}
		aItens2		:= {}
		aCab		:= {}

		IF !EMPTY(TRB2->OK)

			cQuery := ""
			cQuery += " SELECT ZZ0_NUMERO, "
			cQuery += "    ZZ0_NATURE, "
			cQuery += "    ZZ0_FATDIR, "
			cQuery += "    ZZ0_CLIENT, "
			cQuery += "    ZZ0_LOJA, "
			cQuery += "    ZZ0_STATUS, "
			cQuery += "    ZZ0_FILIAL, "
			cQuery += "    ZZ0_FRTCLI, "
			cQuery += "    ZZ0_CPFIN, "
			cQuery += "    ZZ0_NOME, "
			cQuery += "    ZZ0_FATDIR "
			cQuery += "   FROM " + RETSQLNAME("ZZ0")+ " ZZ0 "
			cQuery += "  WHERE ZZ0.D_E_L_E_T_ = '' "
			cQuery += "   AND ZZ0.ZZ0_NUMERO = '" + TRB2->CODCONTR + "' "
			cQuery += "   AND ZZ0.ZZ0_FILIAL = '" + TRB2->CODFILIAL + "' "
			cQuery += "   AND ZZ0.ZZ0_STATUS IN ('L') "

			//ATUALIZAR O PRODUTO NA TABELA SB9

			If GETMV("SA_SB9")

				PROCESSA( {|| ARMAZEMSB9(TRB2->CODCONTR,TRB2->CODFILIAL) } , "Verificando armazem!!",,.F.)

			EndIf

			If Select("XZZ0") > 0
				dbSelectArea("XZZ0")
				XZZ0->(dbCloseArea())
			Endif

			TCQuery cQuery new alias XZZ0

			nOpca := 0

			cContrato := XZZ0->ZZ0_NUMERO

			IF XZZ0->ZZ0_FATDIR == "3"

				IF ! lPergunta
					WHILE nOpca == 0

						DEFINE MSDIALOG oDlg TITLE "TIPO DE MEDI��O - Contrato: " + cContrato FROM 000, 000  TO 150, 300 COLORS 0, 16777215 PIXEL

						@ 012, 071 MSCOMBOBOX oComboBo1 VAR nComboBo1 ITEMS {"DIRETO","TRIANGULAR"} SIZE 072, 010 OF oDlg COLORS 0, 16777215 PIXEL
						@ 012, 005 SAY oSay1 PROMPT "Tipo de Medi��o:" SIZE 049, 010 OF oDlg COLORS 0, 16777215 PIXEL
						@ 039, 075 BUTTON oButton1 PROMPT "ok" SIZE 037, 012 OF oDlg ACTION  (nOpca:=1,oDlg:end()) PIXEL

						ACTIVATE MSDIALOG oDlg

					ENDDO
				ENDIF

				IF nOpca == 2
					Return .F.
				ENDIF
				IF  nComboBo1 == "DIRETO"
					lCheckBo1 := .T.
					lCheckBo2 := .F.
					lPergunta := .T.
				ELSEIF nComboBo1 == "TRIANGULAR"
					lCheckBo1 := .F.
					lCheckBo2 := .T.
					lPergunta := .T.
				ENDIF
			ENDIF

			SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
			If SA1->(DbSeek(xFilial("SA1")+cCNPJ))
				cCliCol	:= SA1->A1_COD
				cLjCol	:= SA1->A1_LOJA
				cTabPad2:= iif(empty(SA1->A1_TABELA),cTabPad,SA1->A1_TABELA)
			EndIf

			SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
			ZZ0->(DbSetOrder(1))	//ZZ0_FILIAL+ZZ0_NUMERO
			ZZ1->(DbSetOrder(1))	//ZZ1_FILIAL+ZZ1_CONTRAT+ZZ1_ITEM
			SA1->(dbSetOrder(1))	//A1_FILIAL+A1_COD
			SB1->(DbSetOrder(1))	//B1_FILIAL+B1_COD
			SB2->(DbSetOrder(1))	//B2_FILIAL+B2_COD+B2_LOCAL

			cQuery2 := " SELECT MAX(ZZ2_MEDICA) AS ZZ2_MEDICA "
			cQuery2 += "   FROM " + RETSQLNAME('ZZ2') + " ZZ2 "
			cQuery2 += "  WHERE ZZ2_FILIAL = '" + TRB2->CODFILIAL + "' "
			//cQuery2 += "    AND D_E_L_E_T_ <> '*' "
			cQuery2 += "    AND ZZ2_NUMERO = '" + cContrato + "' "

			TCQuery cquery2 new alias T02
			DBSELECTAREA("T02")
			IF Alltrim(T02->ZZ2_MEDICA) == ""
				cMedicao := STRZERO(1,GetSx3Cache("ZZ2_MEDICA","X3_TAMANHO"),0)
			ELSE
				cMedicao := STRZERO(VAL(T02->ZZ2_MEDICA)+1,GetSx3Cache("ZZ2_MEDICA","X3_TAMANHO"),0)
			ENDIF
			T02->(DBCLOSEAREA())

			cNaturez:= XZZ0->ZZ0_NATURE

			Begin Transaction
				//IF oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FATDIR') == '2' //1 DIRETO  - 2 TRIANGULAR

				SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
				If SA1->(DbSeek(xFilial("SA1")+cCNPJ))
					AADD(aLocks,{"SA1",SA1->(Recno())})
				EndIf

				Pergunte("SASP035xR", .F.)

				dbSelectArea("ZZ2")
				RECLOCK("ZZ2", .T.)

				ZZ2->ZZ2_FILIAL	:= TRB2->CODFILIAL //xFilial("ZZ3")
				ZZ2->ZZ2_NUMERO	:= cContrato
				ZZ2->ZZ2_CLIENT := XZZ0->ZZ0_CLIENT
				ZZ2->ZZ2_LOJA   := XZZ0->ZZ0_LOJA
				ZZ2->ZZ2_NOME   := XZZ0->ZZ0_NOME
				ZZ2->ZZ2_EST    := POSICIONE("SA1",1,xFilial("SA1") + XZZ0->ZZ0_CLIENT + XZZ0->ZZ0_LOJA,"A1_EST")
				ZZ2->ZZ2_MEDICA := cMedicao //SEQUENCIAL
				ZZ2->ZZ2_DTGER  := DATE()
				ZZ2->ZZ2_HRGER  := TIME()
				ZZ2->ZZ2_STATUS := XZZ0->ZZ0_STATUS
				ZZ2->ZZ2_LOCEX  := mv_par07  //SX5
				ZZ2->ZZ2_TIPO   := mv_par05
				ZZ2->ZZ2_DTSOLI := DATE()
				ZZ2->ZZ2_DTPREV	:= validTipo(alltrim(mv_par05),cContrato, XZZ0->ZZ0_FILIAL )
				ZZ2->ZZ2_DTMAX	:= validTipo(alltrim(mv_par05),cContrato, XZZ0->ZZ0_FILIAL )
				ZZ2->ZZ2_NATMED := cNaturez
				ZZ2->ZZ2_ETAPA  := GETMV("SA_ETPMED")
				ZZ2->ZZ2_FATEXP := "2"
				/*
				ZZ2->ZZ2_PCFIL  := ""
				ZZ2->ZZ2_PC01   := ""
				ZZ2->ZZ2_NFE1   := ""
				ZZ2->ZZ2_SER1   := ""
				ZZ2->ZZ2_FORNEC := ""
				ZZ2->ZZ2_PV01FL := ""
				ZZ2->ZZ2_PV01   := ""
				ZZ2->ZZ2_PV01NF := ""
				ZZ2->ZZ2_SER01  := ""
				ZZ2->ZZ2_CLNF01	:= ""
				ZZ2->ZZ2_LJNF01	:= ""
				ZZ2->ZZ2_PV02FL := ""
				ZZ2->ZZ2_PV02   := ""
				ZZ2->ZZ2_PV02NF := ""
				ZZ2->ZZ2_SER02  := ""
				ZZ2->ZZ2_CLNF02 := ""
				ZZ2->ZZ2_LJNF02 := ""
				ZZ2->ZZ2_PV03FL := ""
				ZZ2->ZZ2_PV03   := ""
				ZZ2->ZZ2_PV03NF := ""
				ZZ2->ZZ2_SER03	:= ""
				ZZ2->ZZ2_CLNF03	:= ""
				ZZ2->ZZ2_LJNF03 := ""
				ZZ2->ZZ2_ANOCOM := ""
				ZZ2->ZZ2_ENVIO  := ""
				*/
				MSUNLOCK()
				DbCloseArea("ZZ3")
				ZZ3->(ConfirmSX8())

				//TODO HERE!
				cQuery3 := " SELECT * "
				cQuery3 += "   FROM " + RETSQLNAME("ZZ1") + " ZZ1 "
				cQuery3 += "  WHERE ZZ1.D_E_L_E_T_ = '' "
				cQuery3 += "    AND ZZ1.ZZ1_PRODUT IN(" + cProdutos + ") "
				cQuery3 += "    AND ZZ1.ZZ1_NUMERO = '" + cContrato + "' "
				cQuery3 += "    AND ZZ1.ZZ1_FILIAL = '" + TRB2->CODFILIAL + "'"
				cQuery3 += "    AND ZZ1.ZZ1_SALDO > 0 "
				cQuery3 += "    AND ZZ1.ZZ1_STATUS = 'A' "
				cQuery3 += "    AND ZZ1.ZZ1_TES = '" + mv_par08 + "' "

				TCQuery cQuery3 new alias XZZ1B1

				WHILE XZZ1B1->(!EOF())



					DbSelectArea("ZZ3")
					RecLock("ZZ3",.T.)

					ZZ3->ZZ3_FILIAL	:= XZZ1B1->ZZ1_FILIAL
					ZZ3->ZZ3_NUMERO	:= XZZ1B1->ZZ1_NUMERO
					ZZ3->ZZ3_MEDICA := cMedicao
					ZZ3->ZZ3_CLIENT := XZZ0->ZZ0_CLIENT
					ZZ3->ZZ3_LOJA 	:= XZZ0->ZZ0_LOJA
					ZZ3->ZZ3_ITEM 	:= XZZ1B1->ZZ1_ITEM
					ZZ3->ZZ3_PRODUT := XZZ1B1->ZZ1_PRODUT
					ZZ3->ZZ3_QUANT 	:= XZZ1B1->ZZ1_SALDO
					ZZ3->ZZ3_TES 	:= XZZ1B1->ZZ1_TES
					ZZ3->ZZ3_SALDO 	:= 0
					If XZZ1B1->ZZ1_TIPO = 'K'
						ZZ3->ZZ3_ENVIO 	:= POSICIONE("ZZ7",1,XFILIAL("ZZ7")+ALLTRIM(XZZ1B1->ZZ1_PRODUT),"ZZ7_ENVIO")
						ZZ3->ZZ3_ENVPRV := POSICIONE("ZZ7",1,XFILIAL("ZZ7")+ALLTRIM(XZZ1B1->ZZ1_PRODUT),"ZZ7_ENVIO")
					Else
						ZZ3->ZZ3_ENVIO 	:= POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(XZZ1B1->ZZ1_PRODUT),"B1_YVOL")
						ZZ3->ZZ3_ENVPRV := POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(XZZ1B1->ZZ1_PRODUT),"B1_YVOL")
					Endif
					ZZ3->ZZ3_TIPO 	:= XZZ1B1->ZZ1_TIPO
					ZZ3->ZZ3_VLUNIT := XZZ1B1->ZZ1_VLRUN
					ZZ3->ZZ3_DESCRI := XZZ1B1->ZZ1_DESC
					ZZ3->ZZ3_VTOTAL := XZZ1B1->ZZ1_VLRTOT

					ZZ3->(MsUnLock())
					dbCloseArea("ZZ3")
					ZZ3->(ConfirmSX8())

					DBSELECTAREA("ZZ1")
					dbSetOrder(1)
					If ZZ1->(DbSeek(XZZ1B1->ZZ1_FILIAL+XZZ1B1->ZZ1_NUMERO+XZZ1B1->ZZ1_ITEM))

						If SF4->(DbSeek(xFilial("SF4")+ZZ1->ZZ1_TES)) .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
							nVlrAdiant	+= XZZ1B1->ZZ1_VLRUN * XZZ1B1->ZZ1_SALDO
							If XZZ1B1->ZZ1_TIPO == 'K'
								nVlrAdiant  -= DESCFRET(XZZ1B1->ZZ1_PRODUT, XZZ1B1->ZZ1_VLRUN) * XZZ1B1->ZZ1_SALDO
							endif
						EndIf

						//VERIFICA O TIPO DE ITEM A SER MEDIDO SE � KITE OU PRODUTO   (K= KIT / P= PRODUTO)
						If XZZ1B1->ZZ1_TIPO == 'K' //lKit

							cquery1 := " SELECT * "
							cquery1 += "   FROM " + RETSQLNAME('ZZ7') + " ZZ7 "
							cquery1 += "  WHERE ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
							cquery1 += "    AND D_E_L_E_T_ <> '*' "
							cquery1 += "    AND ZZ7_CODPAI = '" + XZZ1B1->ZZ1_PRODUT + "' "
							cquery1 += "    AND ZZ7_TIPO = 'A' "
							cquery1 += "  ORDER BY ZZ7_PRODUT"
							TCQuery cquery1 new alias T03

							aValVenda := ValVenda(XZZ1B1->ZZ1_PRODUT,XZZ1B1->ZZ1_VLRUN)


							While T03->(!EOF())  //ZZ7->(!EOF()) .AND. xFilial("ZZ7")+oModelZZ3:GetValue('ZZ3_PRODUT') == ZZ7->(ZZ7_FILIAL+ZZ7_PRODUT)
								//nValVenda ++
								nContIT++
								nVlrItem2	:= MaTabPrVen(cTabPad,T03->ZZ7_PRODUT,1, XZZ0->ZZ0_CLIENT,XZZ0->ZZ0_LOJA) //nVlrItem	:= MaTabPrVen(cTabPad,alltrim(T03->ZZ7_PRODUT),1,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT'),oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))
								//nVlrTotal	+= nVlrItem*T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT')
								/*
								If SF4->(DbSeek(xFilial("SF4")+ZZ1->ZZ1_TES)) .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
								nVlrAdiant	+= nVlrItem*T03->ZZ7_QUANT*XZZ1B1->ZZ1_SALDO
								EndIf
								*/
								AADD(aItens,{})
								nTam	:= Len(aItens)
								AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
								AADD(aItens[nTam],{"C6_PRODUTO"	,T03->ZZ7_PRODUT											,nil})
								AADD(aItens[nTam],{"C6_QTDVEN"	,T03->ZZ7_QUANT*XZZ1B1->ZZ1_SALDO				,nil})
								AADD(aItens[nTam],{"C6_QTDLIB"	,T03->ZZ7_QUANT*XZZ1B1->ZZ1_SALDO			,nil})

								//TODO VALOR PARA PROFESSOR : RAFAEL PINHEIRO - 04/11/2015
								// ajustado para quando for professor considerar o valor do parametro.
								IF T03->ZZ7_PERDSC == 0 .AND. T03->ZZ7_CATEGO $ 'A | C | D | V ' .AND. T03->ZZ7_SUPLEM == 'N'
									AADD(aItens[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1]  ,nil}) //nVlrItem
									//nValVenda ++
								ELSEIF T03->ZZ7_CATEGO='P' // INCLU
									AADD(aItens[nTam],{"C6_PRCVEN"	,GETMV('SA_PRECDSC') 									,nil})
								ELSEIF T03->ZZ7_PERDSC == 100 .AND. T03->ZZ7_CATEGO<>'P'
									AADD(aItens[nTam],{"C6_PRCVEN"	,GETMV('SA_PRECDSC') 								    ,nil}) //nVlrItem
									nDesc +=(( GETMV('SA_PRECDSC')*T03->ZZ7_QUANT*XZZ1B1->ZZ1_SALDO)*(T03->ZZ7_PERDSC/100))
								ELSEIF T03->ZZ7_PERDSC == 0
									IF nVlrItem2 <> 0
										AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2             			,nil}) // AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1] 									,nil}) //nVlrItem
									ELSE
										AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1]		,nil})
										nValVenda ++
									ENDIF
								ENDIF


								AADD(aItens[nTam],{"C6_YTES"	,XZZ1B1->ZZ1_TES														,nil})//ZZ1->ZZ1_TES
								AADD(aItens[nTam],{"C6_TES"		,cTesTmp														,nil})//ZZ1->ZZ1_TES
								AADD(aItens[nTam],{"C6_YCONTRA"	,cContrato													,nil})
								AADD(aItens[nTam],{"C6_YMEDICA"	,cMedicao													,nil})
								AADD(aItens[nTam],{"C6_YITCONT"	,XZZ1B1->ZZ1_ITEM							,nil})
								AADD(aItens[nTam],{"C6_YCODKIT" ,ALLTRIM(XZZ1B1->ZZ1_PRODUT)					,nil})//oModelZZ3:GetValue('000000000000003')
								AADD(aItens[nTam],{"C6_YPAIKIT" ,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(XZZ1B1->ZZ1_PRODUT),"ZZ7_CODPAI"))	,nil})
								AADD(aItens[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT') 									,nil})

								If SB1->(DbSeek(xFilial("SB1")+T03->ZZ7_PRODUT)) .AND. SB2->(DbSeek(xFilial("SB2")+T03->ZZ7_PRODUT+SB1->B1_LOCPAD))
									AADD(aLocks,{"SB2",SB2->(Recno())})
								EndIf

								//nVlrItem2	:= MaTabPrVen(cTabPad2,T03->ZZ7_PRODUT,1,cCliCol,cLjCol)

								AADD(aItens2,{})
								nTam	:= Len(aItens2)
								AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.),nil})
								AADD(aItens2[nTam],{"C6_PRODUTO",T03->ZZ7_PRODUT										,nil})
								AADD(aItens2[nTam],{"C6_QTDVEN"	,T03->ZZ7_QUANT*XZZ1B1->ZZ1_SALDO			,nil})
								AADD(aItens2[nTam],{"C6_QTDLIB"	,T03->ZZ7_QUANT*XZZ1B1->ZZ1_SALDO		,nil})

								IF T03->ZZ7_PERDSC == 0 .AND. T03->ZZ7_CATEGO $ 'A | C | D | V ' .AND. T03->ZZ7_SUPLEM == 'N' //T03->ZZ7_PERDSC == 0
									IF nVlrItem2 <> 0
										AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2             			,nil}) // AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1] 									,nil}) //nVlrItem
										nValVenda ++
									ELSE
										AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1]		,nil})
										nValVenda ++
									ENDIF
								ELSEIF T03->ZZ7_PERDSC == 100
									AADD(aItens2[nTam],{"C6_PRCVEN"	,GETMV('SA_PRECDSC') 				,nil}) //nVlrItem
								ELSEIF T03->ZZ7_PERDSC == 0
									IF nVlrItem2 <> 0
										AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2             			,nil}) // AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1] 									,nil}) //nVlrItem
									ELSE
										AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1]		,nil})
										nValVenda ++
									ENDIF
								ENDIF

								AADD(aItens2[nTam],{"C6_YTES"	,XZZ1B1->ZZ1_TES									,nil})//ZZ1->ZZ1_TES
								AADD(aItens2[nTam],{"C6_TES"	,cTesTmp									,nil})//ZZ1->ZZ1_TES
								AADD(aItens2[nTam],{"C6_YCONTRA",cContrato								,nil})
								AADD(aItens2[nTam],{"C6_YMEDICA",cMedicao								,nil})
								AADD(aItens2[nTam],{"C6_YITCONT",XZZ1B1->ZZ1_ITEM						,nil})
								AADD(aItens2[nTam],{"C6_YCODKIT",ALLTRIM(XZZ1B1->ZZ1_PRODUT)	  		,nil})//oModelZZ3:GetValue('000000000000003')
								AADD(aItens2[nTam],{"C6_YPAIKIT",Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(XZZ1B1->ZZ1_PRODUT),"ZZ7_CODPAI"))					,nil})
								AADD(aItens2[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT') 					,nil})

								T03->(DbSkip())

							EndDo

							nValVenda := 1
							T03->(DbCloseArea())

						Else

							nContIT++
							nVlrItem2	:= MaTabPrVen(cTabPad,XZZ1B1->ZZ1_PRODUT,1,XZZ0->ZZ0_CLIENT,XZZ0->ZZ0_LOJA)
							AADD(aItens,{})
							nTam	:= Len(aItens)
							nVlrTotal	+= ZZ1->ZZ1_VLRUN * XZZ1B1->ZZ1_SALDO

							//If SF4->(DbSeek(xFilial("SF4") + ZZ1->ZZ1_TES)) .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
							//	nVlrAdiant	+= ZZ1->ZZ1_VLRUN * XZZ1B1->ZZ1_SALDO
							//EndIf

							AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
							AADD(aItens[nTam],{"C6_PRODUTO"	,XZZ1B1->ZZ1_PRODUT	,nil})
							AADD(aItens[nTam],{"C6_QTDVEN"	,XZZ1B1->ZZ1_SALDO	,nil})
							AADD(aItens[nTam],{"C6_QTDLIB"	,XZZ1B1->ZZ1_SALDO	,nil})
							AADD(aItens[nTam],{"C6_PRCVEN"	,ZZ1->ZZ1_VLRUN						,nil})
							AADD(aItens[nTam],{"C6_YTES"	,XZZ1B1->ZZ1_TES            		,nil})//ZZ1->ZZ1_TES
							AADD(aItens[nTam],{"C6_TES"		,cTesTmp            		,nil})//ZZ1->ZZ1_TES
							AADD(aItens[nTam],{"C6_YCONTRA"	,cContrato							,nil})
							AADD(aItens[nTam],{"C6_YMEDICA"	,cMedicao							,nil})
							AADD(aItens[nTam],{"C6_YITCONT"	,XZZ1B1->ZZ1_ITEM		,nil})
							AADD(aItens[nTam],{"C6_YCODKIT" ,ALLTRIM(XZZ1B1->ZZ1_PRODUT)					,nil})//oModelZZ3:GetValue('000000000000003')
							AADD(aItens[nTam],{"C6_YPAIKIT" ,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(XZZ1B1->ZZ1_PRODUT),"ZZ7_CODPAI"))	,nil})
							AADD(aItens[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT') 					,nil})

							If SB1->(DbSeek(xFilial("SB1")+XZZ1B1->ZZ1_PRODUT)) .AND. SB2->(DbSeek(xFilial("SB2")+XZZ1B1->ZZ1_PRODUT+SB1->B1_LOCPAD))
								AADD(aLocks,{"SB2",SB2->(Recno())})
							EndIf

							AADD(aItens2,{})
							nTam	:= Len(aItens2)
							AADD(aItens2[nTam],{"C6_ITEM"	    ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
							AADD(aItens2[nTam],{"C6_PRODUTO"	,XZZ1B1->ZZ1_PRODUT                        ,nil})
							AADD(aItens2[nTam],{"C6_QTDVEN"	    ,XZZ1B1->ZZ1_SALDO							,nil})
							AADD(aItens2[nTam],{"C6_QTDLIB"	    ,XZZ1B1->ZZ1_SALDO						,nil})
							IF nVlrItem2 <> 0
								AADD(aItens2[nTam],{"C6_PRCVEN"	    ,nVlrItem2 												,nil})
							ELSE
								AADD(aItens2[nTam],{"C6_PRCVEN"	    ,,XZZ1B1->ZZ1_VLRUN											,nil})
							ENDIF
							AADD(aItens2[nTam],{"C6_YTES"		,XZZ1B1->ZZ1_TES							        		,nil})
							AADD(aItens2[nTam],{"C6_TES"		,cTesTmp							        		,nil})
							AADD(aItens2[nTam],{"C6_YCONTRA"	,cContrato													,nil})
							AADD(aItens2[nTam],{"C6_YMEDICA"	,cMedicao													,nil})
							AADD(aItens2[nTam],{"C6_YITCONT"	,XZZ1B1->ZZ1_ITEM									,nil})
							AADD(aItens2[nTam],{"C6_YCODKIT" ,ALLTRIM(XZZ1B1->ZZ1_PRODUT)					,nil})
							AADD(aItens2[nTam],{"C6_YPAIKIT" ,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(XZZ1B1->ZZ1_PRODUT),"ZZ7_CODPAI"))	,nil})
							AADD(aItens2[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT') 					,nil})


						EndIf

						dbSelectArea("ZZ1")
						dbSetOrder(1)
						ZZ1->(DbSeek(XZZ1B1->ZZ1_FILIAL+XZZ1B1->ZZ1_NUMERO+XZZ1B1->ZZ1_ITEM))

						RecLock("ZZ1",.F.)
						ZZ1->ZZ1_SALDO	:=	0
						ZZ1->(MsUnLock())

					Else
						DisarmTransaction()
						Alert('N�o foi encontrado o item do contrato na base de dados!')
						Return .F.
					EndIf

					XZZ1B1->(DBSKIP())
				ENDDO
				XZZ1B1->(DBCLOSEAREA())

				/*
				If !u_MultLock(aLocks)
				DisarmTransaction()
				Alert('Problema ao tentar reservar registros exclusivos!')
				Return .F.
				EndIf
				*/

				nSaldo  := 1
				nFret	:= 0
				AADD(aCab,{"C5_TIPO"		,"N"													,Nil})
				AADD(aCab,{"C5_CLIENTE"		,""														,Nil})
				AADD(aCab,{"C5_LOJACLI"		,""														,Nil})
				//AADD(aCab,{"C5_CONDPAG"		,XZZ0->ZZ0_CPFIN	,Nil}) //oModelSZ1:GetValue('ZZ0_CPFIM')
				AADD(aCab,{"C5_CONDPAG"		,"001"	,Nil})
				AADD(aCab,{"C5_NATUREZ"		,cNaturez												,Nil})
				AADD(aCab,{"C5_YTPVEN"		,"000005"												,Nil})
				AADD(aCab,{"C5_TPEDAV"		,"39"   												,Nil})
				AADD(aCab,{"C5_YCONTRA"		,XZZ0->ZZ0_NUMERO	,Nil})
				AADD(aCab,{"C5_YMEDICA"		,cMedicao  												,Nil})
				AADD(aCab,{"C5_YFILIAL"		,XZZ0->ZZ0_FILIAL, Nil})

				If XZZ0->ZZ0_FRTCLI <> 0 //=="1"
					nFret:= nVlrAdiant*XZZ0->ZZ0_FRTCLI/100
				EndIf

				aCliente	:= {XZZ0->ZZ0_CLIENT,XZZ0->ZZ0_LOJA,XZZ0->ZZ0_NOME}

				Processa({|| lRet := Pedido(aCab,aItens,aCliente,cContrato,cMedicao,nFret,aItens2,lCheckBo1,lCheckBo2,XZZ0->ZZ0_FATDIR,XZZ0->ZZ0_FILIAL) },"Criando Pedidos","Aguarde...",.F.)

				nFret := 0
				nDesc := 0

				If !lRet
					DisarmTransaction()
					Alert('N�o foi possivel adicionar o pedido de venda!')
					Return .F.
				EndIf

			End Transaction
			MsUnLockAll()

			XZZ0->(DBCLOSEAREA())

		ENDIF

		TRB2->(DBSKIP())
	ENDDO
	TRB2->(DBCLOSEAREA())

Return


Static Function Pedido(aCab,aItens,aCliente,cContrato,cMedicao,nFret,aItens2,lCheckBo1,lCheckBo2,cFatDir,cFilTemp)

	Local cFilBak			:= cFilAnt
	Local aCabCliente		:= aClone(aCab)
	Local aCabCol			:= aClone(aCab)
	Local cCNPJ			    := FWArrFilAtu()[18]					//Coligada
	Local cCNPJMatriz		:= FWArrFilAtu("01","010105")[18]	//Holding
	Local cCliCol           := cLjCol := ""
	Local nPosCliente		:= aScan(aCab,{|x| x[1]=="C5_CLIENTE"})
	Local nPosLoja		    := aScan(aCab,{|x| x[1]=="C5_LOJACLI"})
	Local nPosProd		    := aScan(aItens[1],{|x|  x[1]=="C6_PRODUTO"})
	Local nPosQuant		    := aScan(aItens[1],{|x|  x[1]=="C6_QTDVEN"})
	Local nPosVlr			:= aScan(aItens[1],{|x|  x[1]=="C6_PRCVEN"})
	Local nPosTes			:= aScan(aItens[1],{|x|  x[1]=="C6_YTES"})
	Local nPosKit           := aScan(aItens2[1],{|x| x[1]=="C6_YCODKIT"})
	Local nPosPai           := aScan(aItens2[1],{|x| x[1]=="C6_YPAIKIT"})
	Local cTesPV2			:= GetTes(SuperGetMv("SA_TESPV2",.F.,"","010105"),aItens[1][nPosTes][2])		//Matriz-Comercial	//502:503;505:505
	Local cTesPV3			:= GetTes(SuperGetMv("SA_TESPV3",.F.,"","010105"),aItens[1][nPosTes][2])		//Matriz-Cliente	//502:504;505:504
	Local cTesPC4			:= GetTes(SuperGetMv("SA_TESPC4",.F.,"",cFilAnt),aItens[1][nPosTes][2])		    //Comercial-Matriz	//502:016;505:016
	Local cPedido1,cPedido2,cPedido3
	Local aFornec			:= {,}
	Local aCabPC			:= {}
	Local aItensPC	:= {}
	Local nCont,aLinha
	Local cPedido4   := ""
	Default aItens		:= {}
	Private lMsErroAuto	:= .F.
	Private cMat120Num	:= ""

	cFilAnt := cFilTemp

	ProcRegua(4)
	cCliCol	:= SUBSTR(SuperGetMv( "SA_CLIEFIL",,, cFilAnt ),1,6)//SA1->A1_COD
	cLjCol	:= SUBSTR(SuperGetMv( "SA_CLIEFIL",,, cFilAnt ),8,9)//SA1->A1_LOJA

	aFornec[1]	:= SUBSTR(GetMv( "SA_PCFOR" ),1,6)
	aFornec[2]	:= SUBSTR(GetMv( "SA_PCFOR" ),8,9)

	aCabCliente[nPosCliente][2]	:= aCliente[1]
	aCabCliente[nPosLoja][2]	:= aCliente[2]
	aCabCol[nPosCliente][2]		:= cCliCol
	aCabCol[nPosLoja][2]		:= cLjCol
	IncProc("Criando pedido para o cliente")
	ProcessMessage()
	//Comercial - Cliente

	AADD(aCabCliente,{"C5_FILIAL",xFilial("SC5") ,Nil})
	AADD(aCabCliente,{"C5_FRETE",nFret,Nil})
	AADD(aCabCliente,{"C5_DESCONT",nDesc,Nil})
	AADD(aCabCliente,{"C5_TPFRETE","C",NIL})

	//AADD(aCabCliente,{"C5_FILIAL",xFilial("SC5"),Nil})
	//Grava��o Reclock ou Execauto
	If SuperGetMv("SA_TPGRV",,"R") == "R" //Reclock

		cRet:= u_SASP039(aCabCliente,aItens,3,"MATA410")

		If !empty(cRet[1])
			AutoGrLog("Erro ao gerar pedido de venda do Comercial para o cliente!")
			Aviso("Aten��o",cRet[1],{"Ok"})
			Return .F.
		Else
			cPedido1:= cRet[2]
		Endif

	Else //ExecAuto

		MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o

		If lMsErroAuto
			AutoGrLog("Erro ao gerar pedido de venda do Comercial para o cliente!")
			MostraErro()
			Return .F.
		Else
			cPedido1:= SC5->C5_NUM
		Endif

	Endif

	IF cFatDir =='2' .OR. (cFatDir == "3" .AND. lCheckBo2)

		aSize(aCabCliente,Len(aCabCliente)-3) //Deleta Frete/Desconto para outras filiais/filial

		IncProc("Criando pedido na Matriz para o Comercial")
		ProcessMessage()

		//Matriz - Comercial
		cFilAnt	:= "010105"	//Matriz
		lMsErroAuto	:= .F.
		For nCont:=1 to Len(aItens2)
			aItens2[nCont][nPosTes][2]	:= cTesPV2//cTesPV2
		Next

		AADD(aCabCol,{"C5_FILIAL",xFilial("SC5"),Nil})

		//Grava��o Reclock ou Execauto
		If SuperGetMv("SA_TPGRV",,"R") == "R" //Reclock
			cRet:= u_SASP039(aCabCol,aItens2,3,"MATA410")

			If !empty(cRet[1])
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Comercial!")
				Aviso("Aten��o",cRet[1],{"Ok"})
				Return .F.
			Else
				cPedido2:= cRet[2]
			Endif

		Else //ExecAuto

			MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCol,aItens2,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o

			If lMsErroAuto
				cFilAnt	:= cFilBak
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Comercial!")
				MostraErro()
				Return .F.
			Else
				cPedido2	:= SC5->C5_NUM
			Endif

		Endif

		//Matriz - Cliente
		IncProc("Criando pedido na Matriz para o Cliente")
		ProcessMessage() //ATUALIZA CLIENTE
		lMsErroAuto	:= .F.
		For nCont:=1 to Len(aItens)
			aItens[nCont][nPosTes][2]			:= cTesPV3//cTesPV3
		Next

		AADD(aCabCliente,{"C5_FILIAL",xFilial("SC5"),Nil})
		AADD(aCabCliente,{"C5_FRETE",0,Nil})
		AADD(aCabCliente,{"C5_DESCONT",nDesc,Nil})
		AADD(aCabCliente,{"C5_TPFRETE","C",NIL})

		//Grava��o Reclock ou Execauto
		If SuperGetMv("SA_TPGRV",,"R") == "R" //Reclock
			cRet:= u_SASP039(aCabCliente,aItens,3,"MATA410")

			If !empty(cRet[1])
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Cliente!")
				Aviso("Aten��o",cRet[1],{"Ok"})
				Return .F.
			Else
				cPedido3:= cRet[2]
			Endif

		Else //ExecAuto

			MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o


			If lMsErroAuto
				cFilAnt	:= cFilBak
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Cliente!")
				MostraErro()
				Return .F.
			Else
				cPedido3	:= SC5->C5_NUM
			Endif
		Endif

		aSize(aCabCliente,Len(aCabCliente)-1) //Deleta Desconto para outras filiais

		cFilAnt	:= cFilBak

		IncProc("Criando pedido de compra para a Matriz")
		cPedido4	:= CriaVar('C7_NUM', .T.)
		aadd(aCabPC,{"C7_FILIAL"	,cFilAnt})
		aadd(aCabPC,{"C7_NUM"		,cPedido4})
		aadd(aCabPC,{"C7_EMISSAO"	,dDataBase})
		aadd(aCabPC,{"C7_FORNECE"	,aFornec[1]})
		aadd(aCabPC,{"C7_LOJA"		,aFornec[2]})
		aadd(aCabPC,{"C7_COND"		,"001"})		//TODO VERIFICAR CONDI��O PAG
		aadd(aCabPC,{"C7_CONTATO"	,"AUTO"})
		aadd(aCabPC,{"C7_FILENT"	,cFilAnt})
		//aadd(aCabPC,{"C7_YCONTRA"	,cContrato})
		//aadd(aCabPC,{"C7_YMEDICA"	,cMedicao})

		For nCont:= 1 To Len(aItens2)
			aLinha := {}
			aadd(aLinha,{"C7_PRODUTO"	,aItens2[nCont][nPosProd][2]	,Nil})
			aadd(aLinha,{"C7_QUANT"		,aItens2[nCont][nPosQuant][2]	,Nil})
			aadd(aLinha,{"C7_PRECO"		,aItens2[nCont][nPosVlr][2]		,Nil})
			aadd(aLinha,{"C7_YCODKIT", aItens2[nCont][nPosKit][2], NIL})
			aadd(aLinha,{"C7_YPAIKIT", aItens2[nCont][nPosPai][2], NIL})

			aadd(aLinha,{"C7_TES"		,cTesPC4						,Nil})
			aadd(aLinha,{"C7_YCONTRA"	,cContrato 					,Nil})
			aadd(aLinha,{"C7_YMEDICA"	,cMedicao					    ,Nil})
			aadd(aLinha,{"C7_LOCAL"	,GETMV("SA_ENTLOCA")			    ,Nil})
			aadd(aItensPC,aLinha)
		Next nCont
		lMsErroAuto	:= .F.
		cMat120Num		:= ""	//Variavel preenchida na A120Grava

		//Grava��o Reclock ou Execauto
		If SuperGetMv("SA_TPGRV",,"R") == "R" //Reclock
			cRet:= u_SASP039(aCabPC,aItensPC,3,"MATA120")

			If !empty(cRet[1])
				AutoGrLog("Erro ao gerar pedido de compras !")
				Aviso("Aten��o",cRet[1],{"Ok"})
				Return .F.
			Else
				cPedido4:= cRet[2]
			Endif

		Else //ExecAuto

			MsExecAuto({|x,y,z| MATA120(1,x,y,z)}, aCabPC,aItensPC,3)

			If lMsErroAuto
				AutoGrLog("Erro ao gerar pedido de compra!")
				MostraErro()
				Return .F.
			Else
				cPedido4	:= If(!Empty(cMat120Num),cMat120Num,cPedido4)
				dbSelectArea("SC7")
				dbSetOrder(1)
				If !MsSeek(xFilial("SC7")+cPedido4)
					AutoGrLog("Erro ao localizar pedido de compra gerado!")
					MostraErro()
					Return .F.
				EndIf
			Endif
		Endif
	Endif

	dbSelectArea("ZZ2")
	dbSetOrder(1)
	If DbSeek(cFilTemp+cContrato+cMedicao)
		RecLock("ZZ2",.F.)
		ZZ2->ZZ2_FILIAL	:= cFilTemp
		ZZ2->ZZ2_NUMERO	:= cContrato
		ZZ2->ZZ2_MEDICA	:= cMedicao
		ZZ2->ZZ2_TIPCTR := IIF(cFatDir == '1','1',IIF(cFatDir == "3", IIF(lCheckBo2,'2','1'),'2'))
		ZZ2->ZZ2_CLIENT	:= aCliente[1]
		ZZ2->ZZ2_LOJA	:= aCliente[2]
		ZZ2->ZZ2_EST	:= POSICIONE("SA1",1,xFilial("SA1") + aCliente[1] + aCliente[2],"A1_EST")
		ZZ2->ZZ2_PV01FL	:= IIF(EMPTY(cPedido1),"",cFilTemp)
		ZZ2->ZZ2_PV01	:= cPedido1

		if alltrim(ZZ2->ZZ2_PV01FL) <> "" .AND. alltrim(ZZ2->ZZ2_PV01) <> ""
			U_PESOMED(alltrim(ZZ2->ZZ2_PV01FL),alltrim(ZZ2->ZZ2_PV01),.F.)
		endif

		ZZ2->ZZ2_CLNF01 := IIF(EMPTY(cPedido1),"",aCliente[1])
		ZZ2->ZZ2_LJNF01 := IIF(EMPTY(cPedido1),"",aCliente[2])
		ZZ2->ZZ2_PV02FL	:= IIF(EMPTY(cPedido2),"","010105")
		ZZ2->ZZ2_PV02	:= cPedido2

		if alltrim(ZZ2->ZZ2_PV02FL) <> "" .AND. alltrim(ZZ2->ZZ2_PV02) <> ""
			U_PESOMED(alltrim(ZZ2->ZZ2_PV02FL),alltrim(ZZ2->ZZ2_PV02),.F.)
		endif

		ZZ2->ZZ2_CLNF02 := IIF(EMPTY(cPedido2),"",cCliCol)
		ZZ2->ZZ2_LJNF02 := IIF(EMPTY(cPedido2),"",cLjCol)
		ZZ2->ZZ2_PV03FL	:= IIF(EMPTY(cPedido3),"","010105")
		ZZ2->ZZ2_PV03	:= cPedido3

		if alltrim(ZZ2->ZZ2_PV03FL) <> "" .AND. alltrim(ZZ2->ZZ2_PV03) <> ""
			U_PESOMED(alltrim(ZZ2->ZZ2_PV03FL),alltrim(ZZ2->ZZ2_PV03),.T.)
		endif

		ZZ2->ZZ2_CLNF03 := IIF(EMPTY(cPedido3),"",aCliente[1])
		ZZ2->ZZ2_LJNF03	:= IIF(EMPTY(cPedido3),"",aCliente[2])
		ZZ2->ZZ2_PCFIL	:= IIF(EMPTY(cPedido4),"",cFilAnt)
		ZZ2->ZZ2_PC01	:= cPedido4
		ZZ2->ZZ2_FORNEC := aFornec[1]
		ZZ2->ZZ2_LOJFOR := aFornec[2]
		MsUnLock()

	Endif

	dbSelectArea("ZZR")
	dbSetOrder(1)      // ZZR_FILIAL+ZZR_CONTRA+ZZR_MEDICA+ZZRETAPA

		RECLOCK("ZZR", .T.)

		ZZR_FILIAL  := ZZ2->ZZ2_FILIAL
		ZZR_CONTRA  := ZZ2->ZZ2_NUMERO
		ZZR_MEDICA  := ZZ2->ZZ2_MEDICA
		ZZR_EMISSA  := ZZ2->ZZ2_DTGER
		ZZR_CODCLI  := ZZ2->ZZ2_CLIENT
		ZZR_NOMECL  := POSICIONE("SA1",1,xFilial("SA1")+ZZ2->ZZ2_CLIENT+ZZ2->ZZ2_LOJA,"A1_NOME")
		ZZR_CODUSE  := RetCodUsr()
		ZZR_NOMUSE  := UsrFullName(RetCodUsr())
		ZZR_ETAPA   := "00"
		ZZR_NOMETP  := POSICIONE("SX5",1,XFILIAL("SX5")+'ZA'+"00","X5_DESCRI")
		ZZR_DATA    := date()
		ZZR_HORA    := time()
		ZZR_OBS     := "Medi��o Criada "

		MSUNLOCK()     // Destrava o registro


	ALERT("Realizada medi��o de n�mero : "+alltrim(cMedicao)+" para o contrato de numero: "+alltrim(cContrato)+" !")

Return .T.

Static Function GetTes(cParam,cTes)
	Local cRetTES	:= ""
	Local aTES		:= {}
	Local aTmp		:= Separa(cParam,";",.T.)
	Local nPos
	aEval(aTmp,{|x| If(":"$x,AADD(aTES,Separa(x,":",.T.)),AADD(aTES,Separa(x+":",":",.T.)))})
	nPos	:= aScan(aTES,{|x| x[1]==cTes})
	If nPos>0
		cRetTES	:= aTES[nPos][2]
	EndIf
Return cRetTES

STATIC Function validTipo(cTipo,cContr, cFilT)
	Local cEspecie  := ""
	Local nDias
	Local aSerieEnvio := {}
	Local dMinima
	Local dDataMax
	Local cQuery := ""
	Local cQuery1 := ""

	cQuery := " SELECT ZZ1_PRODUT, ZZ1_TIPO "
	cQuery += "   FROM " + RETSQLNAME("ZZ1") + " ZZ1 "
	cQuery += "  WHERE ZZ1.D_E_L_E_T_ = '' "
	cQuery += "    AND ZZ1.ZZ1_PRODUT IN(" + cProdutos + ") "
	cQuery += "    AND ZZ1.ZZ1_NUMERO = '" + cContr + "' "
	cQuery += "    AND ZZ1.ZZ1_FILIAL = '" + cFilT + "'"

	TCQuery cQuery new alias XZZ1B

	//MONTA ARRAY COM A SERIE E ENVIO DE CADA PRODUTO, PABA BUSCAR A DATA MAXIMA DE ENTREGA
	WHILE XZZ1B->(!EOF())

		IF XZZ1B->ZZ1_TIPO == "P"
			aadd(aSerieEnvio,{ALLTRIM(POSICIONE("SB1",1,XFILIAL("SB1")+alltrim(XZZ1B->ZZ1_PRODUT),"B1_YSRPRD")),1 })
		ELSEIF XZZ1B->ZZ1_TIPO == "K"

			if Select("T13") > 0
				T13->(dbCloseArea())
			endif

			cquery1 := " select * "
			cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 "
			cquery1 += " where ZZ7_FILIAL = '" + XFILIAL("ZZ7")+ "' "
			cquery1 += " and D_E_L_E_T_ <> '*' "
			cquery1 += " and ZZ7_CODPAI = '"+ XZZ1B->ZZ1_PRODUT +"' "
			cquery1 += " and ZZ7_TIPO = 'A' "
			cquery1 += " ORDER BY ZZ7_PRODUT"
			TCQuery cquery1 new alias T13
			WHILE T13->(!EOF())
				aadd(aSerieEnvio,{ALLTRIM(T13->ZZ7_SERIE),1})
				T13->(DbSkip())
			ENDDO
			T13->(DBCLOSEAREA())
		ENDIF
		XZZ1B->(DBSKIP())
	ENDDO

	XZZ1B->(DBCLOSEAREA())

	cEspecie := POSICIONE("ZZG",1,XFILIAL("ZZG")+cTipo,'ZZG_ESPCTR')

	IF POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_FIXO')== 'S'
		IF !alltrim(POSICIONE("ZZM",2,XFILIAL("ZZM")+cTipo,'ZZM_TIPO'))==""
			dMinima:=POSICIONE("ZZM",2,XFILIAL("ZZM")+cTipo,'ZZM_DATA')
		ELSE
			FOR nAx:= 1 to len(aSerieEnvio)
				IF nAx == 1
					dMinima:=POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+;
					PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')
				ELSE
					IF !vazio(POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+;
					PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')) ;
					.AND. ;
					(POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+;
					PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA') < dMinima)
						dMinima:=POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')
					ENDIF
				ENDIF
			NEXT
		ENDIF
	ELSEIF POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_FIXO')== 'N'
		FOR nAx:= 1 to len(aSerieEnvio)
			IF nAx == 1
				nDias := POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')
			ELSE
				IF !vazio(POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')) .AND. (POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS') < nDias)
					nDias := POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')
				ENDIF
			ENDIF
		NEXT
	ENDIF

	dSolicita := mv_par06

	IF !Empty(nDias)
		dDataMax := dSolicita + nDias
	elseIF !Empty(dMinima)
		dDataMax := dMinima
	ENDIF

RETURN dDataMax

//TODO Fun��o para mostrar todos os itens do contrato
Static Function ShowItem(cFilialT, cContr, cClient)

	Local oDlg3
	Local aDados2 := {}
	Local cQuery5 := ""

	Public oBrowse3 := ""

	If Empty(TRB2->OK)

		cQuery5 := ""
		cQuery5 += " SELECT ZZ1.ZZ1_PRODUT,"
		cQuery5 += "    ZZ1.ZZ1_DESC, "
		cQuery5 += "    ZZ1.ZZ1_SALDO "
		cQuery5 += "   FROM " + RETSQLNAME("ZZ1") + " ZZ1 "
		cQuery5 += "  WHERE ZZ1.D_E_L_E_T_ = '' "
		cQuery5 += "    AND ZZ1.ZZ1_NUMERO = '" + TRB2->CODCONTR + "' "
		cQuery5 += "    AND ZZ1.ZZ1_PRODUT IN( " + cProdutos + " )"
		cQuery5 += "    AND ZZ1.ZZ1_FILIAL = '" + TRB2->CODFILIAL + "'"
		cQuery5 += "    AND ZZ1.ZZ1_SALDO > 0 "
		cQuery5 += "    AND ZZ1.ZZ1_STATUS = 'A' "
		cQuery5 += "    AND ZZ1.ZZ1_TES ='" + mv_par08 + "' "

		TCQuery cQuery5 new alias XZZ1B

		WHILE XZZ1B->(!EOF())

			AADD(aDados2,{XZZ1B->ZZ1_PRODUT, XZZ1B->ZZ1_DESC, XZZ1B->ZZ1_SALDO})

			XZZ1B->(DBSKIP())

		ENDDO
		XZZ1B->(DBCLOSEAREA())

		RECLOCK("TRB2", .F.)

		TRB2->OK := cMarca2

		MSUNLOCK()

		DEFINE MSDIALOG oDlg3 TITLE "Itens para medi��o. Contrato: " + TRB2->CODCONTR FROM 000, 000  TO 570, 1300 COLORS 0, 16777215 PIXEL

		oBrowse3 := MsBrGetDBase():New( 002,002, oDlg3:nClientWidth -110, 270,,,, oDlg3,,,,,,,,,,,, .F., "", .T.,, .F.,,, )
		oBrowse3:SetArray(aDados2)

		//Cria colunas para o grid
		oBrowse3:AddColumn(TCColumn():New("Produto"	,{ || aDados2[oBrowse3:nAt,1] },,,,"LEFT",,.F.,.F.,,,,.F.,))
		oBrowse3:AddColumn(TCColumn():New("Descri��o"		,{ || aDados2[oBrowse3:nAt,2] },,,,"LEFT",,.F.,.F.,,,,.F.,))
		oBrowse3:AddColumn(TCColumn():New("Saldo"	,{ || aDados2[oBrowse3:nAt,3] },,,,"LEFT",,.F.,.F.,,,,.F.,))

		oBrowse3:Refresh()

		Activate MsDialog oDlg3 CENTERED

	Else

		RECLOCK("TRB2", .F.)

		TRB2->OK := ""

		MSUNLOCK()

	EndIf


Return

Static Function  MarcaTudo2()

	//Guarda posi��o do ponteiro atual
	Local nReg := TRB2->(RECNO())
	Local cMar := "  "

	// Inicio do Arquivo
	TRB2->(DBGOTOP())

	// Verifica controle de marcacao
	If lInverte
		cMar := cMarca2
		lInverte := .F.
	Else
		cMar := "  "
		lInverte := .T.
	EndIF

	// Percorre todo o TRB
	While !TRB2->(EOF())
		// Atualiza a marca��o
		RecLock("TRB2",.F.)
		TRB2->OK    := cMar
		MsUnlock("TRB2")
		// Pr�ximo registro
		TRB2->(DBSKIP())

	EndDo

	// Volta o cursos para a posi��o antes de marcar desmarcar tudo
	TRB2->(dbGoTo(nReg))

Return()

Static Function  MarcaTudo()

	//Guarda posi��o do ponteiro atual
	Local nReg := TRB->(RECNO())
	Local cMar := "  "

	// Inicio do Arquivo
	TRB->(DBGOTOP())

	// Verifica controle de marcacao
	If lInverte
		cMar := cMarca
		lInverte := .F.
	Else
		cMar := "  "
		lInverte := .T.
	EndIF

	// Percorre todo o TRB
	While !TRB->(EOF())
		// Atualiza a marca��o
		RecLock("TRB",.F.)
		TRB->OK    := cMar
		MsUnlock("TRB")
		// Pr�ximo registro
		TRB->(DBSKIP())

	EndDo

	// Volta o cursos para a posi��o antes de marcar desmarcar tudo
	TRB->(dbGoTo(nReg))

Return()

STATIC FUNCTION DESCFRET(cProduto, nValor)

	Local nDesc     := 0

	cquery := " Select COUNT(ZZ7_CODIGO) TOTAL "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery += " and D_E_L_E_T_ <> '*' "
	cquery += " and ZZ7_CODPAI = '"+ cProduto +"' "
	cquery += " and ZZ7_TIPO = 'A'		"
	cquery += " and ZZ7_SUPLEM = 'S' " //Considera somente que n�o tem desconto

	TCQuery cquery new alias T0402

	nDesc :=  T0402->TOTAL * GETMV('SA_PRECDSC')

	T0402->(DBCLOSEAREA())

RETURN nDesc

Static Function ValVenda(cProduto, nValor)

	Local aValVenda := {}
	Local nValkit   := nValor
	Local nDifer    := 0
	Local nValkit   := nValor
	Local nDesc     := 0

	cquery4 := " select COUNT(ZZ7_CODIGO) TOTAL "
	cquery4 += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery4 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery4 += " and D_E_L_E_T_ <> '*' "
	cquery4 += " and ZZ7_CODPAI = '"+ cProduto +"' "
	cquery4 += " and ZZ7_TIPO = 'A' "
	cquery4 += " and ZZ7_SUPLEM = 'S' "

	TCQuery cquery4 new alias T04

	nDesc :=  T04->TOTAL * GETMV('SA_PRECDSC')

	T04->(DBCLOSEAREA())

	nValkit -= nDesc

	cquery := " Select COUNT(ZZ7_CODIGO) TOTAL "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery += " and D_E_L_E_T_ <> '*' "
	cquery += " and ZZ7_CODPAI = '"+ cProduto +"' "
	cquery += " and ZZ7_TIPO = 'A'		"
	cquery += " and ZZ7_SUPLEM = 'N'	"
	cquery += " and ZZ7_CATEGO IN ('A','C','D','V')	"
	cquery += " and ZZ7_PERDSC = '0'	" //Considera somente que n�o tem desconto
	TCQuery cquery new alias T04

	FOR nX := 1 to T04->TOTAL
		aadd(aValVenda,{NoRound(nValkit / T04->TOTAL,2)})
		nDifer+= NoRound(nValkit / T04->TOTAL,2)
	NEXT

	If len(aValVenda) > 0 .and. (nValkit <> nDifer)
		aValVenda[1][1] := aValVenda[1][1] + (nValkit - nDifer)
	Endif

	T04->(dbclosearea())

	/*
	FOR nX := 1 to T04->TOTAL

	If nX <> T04->TOTAL .OR. T04->TOTAL == 1

	aadd(aValVenda,{NoRound(nValkit / T04->TOTAL,2)})

	//Se nX for igual ao tamanho do vetor, o calculo � o valor de rateio + o valor faltante dos outros itens.
	Else
	aadd(aValVenda,{NoRound(nValkit / T04->TOTAL,2)})

	nDifer := NoRound(nValkit - (NoRound(nValkit / T04->TOTAL,2) * T04->TOTAL) ,2)
	aValVenda[nX][1] := aValVenda[nX][1] + nDifer

	Endif
	NEXT

	T04->(dbclosearea())
	*/
Return aValVenda

STATIC FUNCTION ARMAZEMSB9(cCONTRA,cFIL)

	Local cFilanterior := ""
	Local cAreaAnterior := getarea()
	Local lMsErroAuto  := .F.

	procregua(100)

	cQuery := ""
	cQuery += " SELECT * "
	cQuery += "   FROM " + RETSQLNAME("ZZ1")+ " ZZ1 "
	cQuery += "   WHERE D_E_L_E_T_ = '' "
	cQuery += "   AND ZZ1_NUMERO = '" + cCONTRA + "' "
	cQuery += "   AND ZZ1_FILIAL = '" + cFIL + "' "
	TCQuery cQuery new alias T07

	while ! T07->(EOF())
		if T07->ZZ1_TIPO == "K"

			cquery1 := " SELECT ZZ7_PRODUT,ZZ7_QUANT,ZZ7_PERDSC, ZZ7_CATEGO  "
			cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 "
			cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
			cquery1 += " and D_E_L_E_T_ <> '*' "
			cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(T07->ZZ1_PRODUT) +"' "
			cquery1 += " and ZZ7_TIPO = 'A' "
			cquery1 += " ORDER BY ZZ7_PRODUT"
			TCQuery cquery1 new alias T03

			//cFilanterior := cfilant


			WHILE !T03->(EOF())

				dbSelectArea("SM0")
				dbGOTOP()                 // B9_FILIAL+B9_COD+B9_LOCAL+B9_DATA
				WHILE ! SM0->(EOF())
					//cfilant := SM0->M0_CODFIL
					IF SM0->M0_CODFIL == '100101'
						SM0->(DbSkip())
					ENDIF

					cquery1 := " SELECT *  "
					cquery1 += " from " + retsqlname('SB9') + " SB9 "
					cquery1 += " where B9_FILIAL = '" + SM0->M0_CODFIL + "' "
					cquery1 += " and D_E_L_E_T_ = '' "
					cquery1 += " and B9_COD = '"+ T03->ZZ7_PRODUT +"' "
					cquery1 += " and B9_LOCAL = '"+GETMV('SA_LOCFAT')+"' "
					TCQuery cquery1 new alias T09

					IF  T09->(eof())    // Avalia o retorno da pesquisa realizada

						cFilanterior := cfilant
						incproc("Aguarde...")
						Begin Transaction
							PARAMIXB1 := {}
							aadd(PARAMIXB1,{"B9_FILIAL",xfilial("SB9",SM0->M0_CODFIL),})
							aadd(PARAMIXB1,{"B9_COD",alltrim(T03->ZZ7_PRODUT),})
							aadd(PARAMIXB1,{"B9_LOCAL", GETMV('SA_LOCFAT'),})
							aadd(PARAMIXB1,{"B9_QINI",0,})
							//aadd(PARAMIXB1,{"B9_DATA",DDATABASE,})
							cfilant := SM0->M0_CODFIL
							MSExecAuto({|x,y| mata220(x,y)},PARAMIXB1,3)
							cfilant := cFilanterior
							If lMsErroAuto
								mostraerro()
							EndIf
						End Transaction

					ENDIF
					T09->(DBCLOSEAREA())
					SM0->(DbSkip())
					//cfilant := cFilanterior
				ENDDO
				T03->(DbSkip())
			ENDDO
			T03->(DBCLOSEAREA())

		elseif T07->ZZ1_TIPO == "P"
			incproc("Aguarde...")

			dbSelectArea("SM0")
			dbGOTOP()                 // B9_FILIAL+B9_COD+B9_LOCAL+B9_DATA
			WHILE ! SM0->(EOF())
				//cfilant := SM0->M0_CODFIL
				IF SM0->M0_CODFIL == '100101'
					SM0->(DbSkip())
				ENDIF

				cquery1 := " SELECT *  "
				cquery1 += " from " + retsqlname('SB9') + " SB9 "
				cquery1 += " where B9_FILIAL = '" + SM0->M0_CODFIL + "' "
				cquery1 += " and D_E_L_E_T_ = '' "
				cquery1 += " and B9_COD = '"+ T07->ZZ1_PRODUT +"' "
				cquery1 += " and B9_LOCAL = '"+GETMV('SA_LOCFAT')+"' "
				TCQuery cquery1 new alias T01

				IF  T01->(eof())    // Avalia o retorno da pesquisa realizada

					cFilanterior := cfilant
					incproc("Aguarde...")
					Begin Transaction
						PARAMIXB1 := {}
						aadd(PARAMIXB1,{"B9_FILIAL",xfilial("SB9",SM0->M0_CODFIL),})
						aadd(PARAMIXB1,{"B9_COD",alltrim(T07->ZZ1_PRODUT),})
						aadd(PARAMIXB1,{"B9_LOCAL", GETMV('SA_LOCFAT'),})
						aadd(PARAMIXB1,{"B9_QINI",0,})
						//aadd(PARAMIXB1,{"B9_DATA",DDATABASE,})
						cfilant := SM0->M0_CODFIL
						MSExecAuto({|x,y| mata220(x,y)},PARAMIXB1,3)
						cfilant := cFilanterior
						If lMsErroAuto
							mostraerro()
						EndIf
					End Transaction

				ENDIF
				T01->(DBCLOSEAREA())
				SM0->(DbSkip())
				//cfilant := cFilanterior
			ENDDO
		endif
		T07->(DbSkip())
	ENDDO
	T07->(DBCLOSEAREA())
	RestArea(cAreaAnterior)

RETURN

Static Function AjustaSX1(cPerg)

	PutSx1(cPerg, "01","Cliente de?",				  		 "","","mv_ch01","C",GETSX3CACHE("A1_COD","X3_TAMANHO"),0,0,"G","","SA1","","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Cliente at�?",				  		 "","","mv_ch02","C",GETSX3CACHE("A1_COD","X3_TAMANHO"),0,0,"G","","SA1","","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","Contrato de?",				  		 "","","mv_ch03","C",GETSX3CACHE("ZZ0_NUMERO","X3_TAMANHO"),0,0,"G","","","","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "04","Contrato at�?",				  	 	"","","mv_ch04","C",GETSX3CACHE("ZZ0_NUMERO","X3_TAMANHO"),0,0,"G","","","","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "05","Tipo de Medi��o?",				  	 "","","mv_ch05","C",GetSx3Cache("ZZG_COD","X3_TAMANHO"),0,0,"G","","ZZG","","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "06","Data Solicitacao?",				  	 "","","mv_ch06","D",,0,0,"G","","","","","mv_par06"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "07","Local Expedi��o?",				  	 "","","mv_ch07","C",GETSX3CACHE("ZZ2_LOCEX","X3_TAMANHO"),0,0,"G","","X2","","","mv_par07"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "08","TES?",				  	             "","","mv_ch08","C",GETSX3CACHE("F4_CODIGO","X3_TAMANHO"),0,0,"G","","SF4","","","mv_par08"," ","","","","","","","","","","","","","","","")

Return