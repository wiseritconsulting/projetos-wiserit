#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"


User Function SASP054()

	Local oBrowse	
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('ZZP')
	oBrowse:SetDescription('Cadastro Grupo de Kit')
	oBrowse:DisableDetails()

	oBrowse:SetMenuDef( 'SASP054' ) 
	oBrowse:Activate()

Return

Static Function MenuDef()

	Local aRotina := {}	
	
	ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PesqBrw"			OPERATION 1 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.SASP054"	OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.SASP054"	OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.SASP054"	OPERATION 4 ACCESS 143
	ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.SASP054"	OPERATION 5 ACCESS 144
	ADD OPTION aRotina TITLE "Imprimir"   ACTION "VIEWDEF.SASP054"	OPERATION 8 ACCESS 0
	
Return aRotina

Static Function ModelDef()
	
	Local oStructZZP := Nil
	Local oModel := ""
	
	oStructZZP := FWFormStruct(1,"ZZP")
	
	oModel:= MPFormModel():New("YCADZZP",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
	

	oModel:AddFields("ZZPMASTER",/*cOwner*/, oStructZZP ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)
	
	oModel:SetPrimaryKey({"ZZP_CODIGO"})	
	
Return (oModel)

Static Function ViewDef()

	Local oStructZZP	:= FWFormStruct( 2, 'ZZP' )	
	Local oModel		:= FWLoadModel( 'SASP054' )	
	Local oView
	
		
	oView	:= FWFormView():New()

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField( "ZZPMASTER",oStructZZP)
	
Return oView