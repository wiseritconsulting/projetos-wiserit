#Include 'Protheus.ch'
#include "topconn.ch"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} SASP050
Faturamento em JOB
@author Saulo Gomes Martins
@since 03/09/2015
@version P11
@obs	ZZ2_STATUS
P-Pendente Faturamento
Q-Pendente Cancelamento
F-Faturado
C-Cancelado
EF-Erro Faturamento
EC-Erro Cancelamento
/*/
User Function SASP050()
	Local cQuery
	Local lErro		:= .F.
	Local oError
	Local cMsgErro
	Private cSASStaErr	:= "E"
	Sleep(2000)
	//If !LockByName("SASP050-5",.F.,.F.)
		ConOut("Processso ja inicializado!")
		//Return
	//EndIf
	RpcSetEnv("01","010101",,,"FAT","U_SASP050",{})
	//__cInterNet	:= nil
	cQuery	:= "SELECT TOP 1 R_E_C_N_O_ AS REGZZ2 FROM "+RETSQLNAME("ZZ2")+" ZZ2 WHERE ZZ2_STATUS In ('P','Q')  ORDER BY ZZ2_PRIORI"
	// Seta que RecLock, em caso de falha, nao vai fazer retry automatico
	SetLoopLock(.F.)
	While !killapp()
		Monitor("Faturamento|Aguardando tarefas...")	//Atualiza mensagem no monitor
		TCQuery cQuery new alias TMPZZ2
		If TMPZZ2->(EOF())
			TMPZZ2->(DbCloseArea())
			return
			Sleep(10000)
			Loop
		EndIF
		lErro		:= .F.
		cMsgErro	:= ""
		ZZ2->(DbGoTo(TMPZZ2->REGZZ2))
		RecLock("ZZ2",.F.)
		oError := ErrorBlock({|e| lErro := .T.,ErroLog(e,cSASStaErr) } )
		dDatabase := date()
		If ALLTRIM(ZZ2->ZZ2_STATUS)=="P"	//Pendente Faturamento
			Faturar(@lErro,@cMsgErro)
		ElseIf ALLTRIM(ZZ2->ZZ2_STATUS)=="Q"	//Pendente Cancelar Faturamento
			Cancelar(@lErro,@cMsgErro)
		EndIf
		Logs(Replicate("=",30))
		ZZ2->(MsUnLock())
		ErrorBlock(oError)
		TMPZZ2->(DbCloseArea())
		RETURN
	EndDo
	//UnLockByName("SASP050-5",.F.,.F.)
	
	
Return

Static Function Faturar(lErro,cMsgErro)
	Local cCodUsr	:= "000000"
	Local aDocFat	:= {}
	cSASStaErr		:= "EF"
	Monitor("Faturamento:Processando Contrato:"+ZZ2->ZZ2_NUMERO+" medi��o:"+ZZ2->ZZ2_MEDICA)	//Atualiza mensagem no monitor
	Logs("Contrato:"+ZZ2->ZZ2_NUMERO)
	Logs("Processando Medicao:"+ZZ2->ZZ2_MEDICA)
	Begin Sequence
		If !lErro .AND. Empty(ZZ2->ZZ2_PV02NF) .AND. !Empty(ZZ2->ZZ2_PV02)															//SAIDA DA MATRIZ PARA COMERCIAL
			Monitor("Faturamento:Processando Contrato:"+ZZ2->ZZ2_NUMERO+" medi��o:"+ZZ2->ZZ2_MEDICA+"|"+"SAIDA DA MATRIZ PARA COMERCIAL Pedido:"+ZZ2->ZZ2_PV02FL+"|"+ZZ2->ZZ2_PV02)	//Atualiza mensagem no monitor
			Logs("SAIDA DA MATRIZ PARA COMERCIAL Pedido:"+ZZ2->ZZ2_PV02FL+"|"+ZZ2->ZZ2_PV02)
			Begin Transaction
				If FGeraFat(ZZ2->ZZ2_PV02FL,ZZ2->ZZ2_PV02,@aDocFat,@cMsgErro)
					RecLock("ZZ2",.F.)
					ZZ2->ZZ2_PV02NF	:= aDocFat[1]
					ZZ2->ZZ2_SER02	:= aDocFat[2]
					ZZ2->ZZ2_LOG	:= ""
					MsUnLock()

					//U_F2CONTRA(ZZ2->ZZ2_FILIAL,ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_MEDICA,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,ZZ2->ZZ2_PV02FL,ZZ2->ZZ2_PV02,aDocFat[1],aDocFat[2],"NF2")

					U_SASP023(xFilial("ZZ9"),ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,DATE(),TIME(),cCodUsr,ZZ2->ZZ2_PV02NF+ZZ2->ZZ2_SER02,"06","SAIDA DA MATRIZ PARA COMERCIAL")
				Else
					DisarmTransaction()
					Logs("PV02:"+cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		If !lErro .AND. !Empty(ZZ2->ZZ2_PV02NF) .and. Empty(ZZ2->ZZ2_NFE1)  .AND. !Empty(ZZ2->ZZ2_PC01)								//ENTRADA NO COMERCIAL
			Monitor("Faturamento:Processando Contrato:"+ZZ2->ZZ2_NUMERO+" medi��o:"+ZZ2->ZZ2_MEDICA+"|"+"ENTRADA NO COMERCIAL Pedido:"+ZZ2->ZZ2_PCFIL+"|"+ZZ2->ZZ2_PC01)
			Logs("ENTRADA NO COMERCIAL Pedido:"+ZZ2->ZZ2_PCFIL+"|"+ZZ2->ZZ2_PC01)
			Begin Transaction
				If FatuPC(ZZ2->ZZ2_PCFIL,ZZ2->ZZ2_PC01,@aDocFat,@cMsgErro,ZZ2->ZZ2_PV02NF,ZZ2->ZZ2_SER02)
					RecLock("ZZ2",.F.)
					ZZ2->ZZ2_NFE1	:= aDocFat[1]
					ZZ2->ZZ2_SER1	:= aDocFat[2]
					MsUnLock()
					U_SASP023(xFilial("ZZ9"),ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,DATE(),TIME(),cCodUsr,ZZ2->ZZ2_NFE1+ZZ2->ZZ2_SER1,"19","ENTRADA NO COMERCIAL")
				Else
					DisarmTransaction()
					Logs("PC01:"+cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf

		//EM OPERA��O DIRETA O UNICO QUE � FATURADO � O PV01
		If !lErro .AND. Empty(ZZ2->ZZ2_PV01NF) .AND. !Empty(ZZ2->ZZ2_PV01)															//SAIDA DA COMERCIAL PARA O CLIENTE
			Monitor("Faturamento:Processando Contrato:"+ZZ2->ZZ2_NUMERO+" medi��o:"+ZZ2->ZZ2_MEDICA+"|"+"SAIDA DA COMERCIAL PARA O CLIENTE Pedido:"+ZZ2->ZZ2_PV01FL+"|"+ZZ2->ZZ2_PV01)
			Logs("SAIDA DA COMERCIAL PARA O CLIENTE Pedido:"+ZZ2->ZZ2_PV01FL+"|"+ZZ2->ZZ2_PV01)
			Begin Transaction
				If FGeraFat(ZZ2->ZZ2_PV01FL,ZZ2->ZZ2_PV01,@aDocFat,@cMsgErro)
					RecLock("ZZ2",.F.)
					ZZ2->ZZ2_PV01NF	:= aDocFat[1]
					ZZ2->ZZ2_SER01	:= aDocFat[2]
					ZZ2->ZZ2_LOG	:= ""
					MsUnLock()

					//U_F2CONTRA(ZZ2->ZZ2_FILIAL,ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_MEDICA,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,ZZ2->ZZ2_PV01FL,ZZ2->ZZ2_PV01,aDocFat[1],aDocFat[2],"NF1")

					U_SASP023(xFilial("ZZ9"),ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,DATE(),TIME(),cCodUsr,ZZ2->ZZ2_PV01NF+ZZ2->ZZ2_SER01,"06","SAIDA DA COMERCIAL PARA O CLIENTE")
				Else
					DisarmTransaction()
					Logs("PV01:"+cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		If !lErro .AND. !Empty(ZZ2->ZZ2_NFE1) .and. Empty(ZZ2->ZZ2_PV03NF) .AND. !Empty(ZZ2->ZZ2_PV03)
			//		If  Empty(ZZ2->ZZ2_PV03NF) .AND. !Empty(ZZ2->ZZ2_PV03)								//SAIDA MATRIZ PARA CLIENTE
			Monitor("Faturamento:Processando Contrato:"+ZZ2->ZZ2_NUMERO+" medi��o:"+ZZ2->ZZ2_MEDICA+"|"+"SAIDA MATRIZ PARA CLIENTE Pedido:"+ZZ2->ZZ2_PV03FL+"|"+ZZ2->ZZ2_PV03)
			Logs("SAIDA MATRIZ PARA CLIENTE Pedido:"+ZZ2->ZZ2_PV03FL+"|"+ZZ2->ZZ2_PV03)
			Begin Transaction
				If FGeraFat(ZZ2->ZZ2_PV03FL,ZZ2->ZZ2_PV03,@aDocFat,@cMsgErro)
					RecLock("ZZ2",.F.)
					ZZ2->ZZ2_PV03NF	:= aDocFat[1]
					ZZ2->ZZ2_SER03	:= aDocFat[2]
					ZZ2->ZZ2_LOG	:= ""
					MsUnLock()

					//U_F2CONTRA(ZZ2->ZZ2_FILIAL,ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_MEDICA,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,ZZ2->ZZ2_PV03FL,ZZ2->ZZ2_PV03,aDocFat[1],aDocFat[2],"NF3")

					U_SASP023(xFilial("ZZ9"),ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,DATE(),TIME(),cCodUsr,ZZ2->ZZ2_PV03NF+ZZ2->ZZ2_SER03,"06","SAIDA MATRIZ PARA CLIENTE")
				Else
					DisarmTransaction()
					Logs("PV03:"+cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		If !lErro
			RecLock("ZZ2",.F.)
			ZZ2_STATUS := 'F'
			ZZ2_ETAPA  := "46"
			MsUnLock()			
			
			//Grava hist�rico			
			DbSelectArea("ZZR")
			//DbSetOrder(1)
			
			//IF ! dbSeek(ZZ2->ZZ2_FILIAL + ZZ2->ZZ2_NUMERO + ZZ2->ZZ2_MEDICA + ZZ2->ZZ2_ETAPA )			
				RECLOCK("ZZR", .T.)
	
					ZZR_FILIAL  := ZZ2->ZZ2_FILIAL
					ZZR_CONTRA  := ZZ2->ZZ2_NUMERO
					ZZR_MEDICA  := ZZ2->ZZ2_MEDICA
					ZZR_EMISSA  := ZZ2->ZZ2_DTGER
					ZZR_CODCLI  := ZZ2->ZZ2_CLIENT
					ZZR_NOMECL  := POSICIONE("SA1",1,xFilial("SA1")+ZZ2->ZZ2_CLIENT+ZZ2->ZZ2_LOJA,"A1_NOME")
					ZZR_CODUSE  := RetCodUsr()
					ZZR_NOMUSE  := UsrFullName(RetCodUsr())
					ZZR_ETAPA   := "45" //ZZ2->ZZ2_ETAPA
					ZZR_NOMETP  := POSICIONE("SX5",1,XFILIAL("SX5")+'ZA'+ZZ2->ZZ2_ETAPA,"X5_DESCRI")
					ZZR_DATA    := date()
					ZZR_HORA    := time()
					ZZR_OBS     := "Medi��o Faturada "
	
				MSUNLOCK()
			//ENDIF
			("ZZR")->(DbCloseArea()); 
			
			U_F2CONTRA(ZZ2->ZZ2_FILIAL,ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_MEDICA,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,ZZ2->ZZ2_PV01FL,ZZ2->ZZ2_PV01,ZZ2->ZZ2_PV01NF,ZZ2->ZZ2_SER01,"NF1")
			U_F2CONTRA(ZZ2->ZZ2_FILIAL,ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_MEDICA,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,ZZ2->ZZ2_PV02FL,ZZ2->ZZ2_PV02,ZZ2->ZZ2_PV02NF,ZZ2->ZZ2_SER02,"NF2")
			U_F2CONTRA(ZZ2->ZZ2_FILIAL,ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_MEDICA,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,ZZ2->ZZ2_PV03FL,ZZ2->ZZ2_PV03,ZZ2->ZZ2_PV03NF,ZZ2->ZZ2_SER03,"NF3")
			//FATURADO			
		EndIf
	End Sequence
	MsUnLockAll()
	GETMV("MV_NUMITEN",.T.)
	SX6->(MsRUnLock())
	If lErro
		RecLock("ZZ2",.F.)
		ZZ2_STATUS=cSASStaErr//'EF'
		MsUnLock()
	EndIf
Return

Static Function Cancelar(lErro,cMsgErro)
	Local cCodUsr	:= "000000"
	cSASStaErr	:= "EC"
	Monitor("Faturamento:Cancelar Faturamento Contrato:"+ZZ2->ZZ2_NUMERO+" Medi��o:"+ZZ2->ZZ2_MEDICA)	//Atualiza mensagem no monitor
	Logs("Contrato:"+ZZ2->ZZ2_NUMERO)
	Logs("Processando Medicao:"+ZZ2->ZZ2_MEDICA)
	Begin Sequence
		//EM OPERA��O DIRETA O UNICO QUE � FATURADO � O PV01
		If !lErro .AND. !Empty(ZZ2->ZZ2_PV01NF)
			Logs("CANCELANDO NF, SAIDA DA COMERCIAL PARA O CLIENTE NF:"+ZZ2->ZZ2_PV01FL+"|"+ZZ2->ZZ2_PV01NF)
			Begin Transaction
				If FCancFat(ZZ2->ZZ2_PV01FL,ZZ2->ZZ2_PV01NF,ZZ2->ZZ2_SER01,@cMsgErro)									//SAIDA DA COMERCIAL PARA O CLIENTE
					U_SASP023(xFilial("ZZ9"),ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,DATE(),TIME(),cCodUsr,ZZ2->ZZ2_PV01NF+ZZ2->ZZ2_SER01,"17","SAIDA DA COMERCIAL PARA O CLIENTE")
					RecLock("ZZ2",.F.)
					ZZ2->ZZ2_PV01NF	:= ""
					ZZ2->ZZ2_SER01	:= ""
					MsUnLock()
				Else
					DisarmTransaction()
					Logs(cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		If !lErro .AND. !Empty(ZZ2->ZZ2_PV02NF) 																						//SAIDA DA MATRIZ PARA COMERCIAL
			Logs("CANCELANDO NF, SAIDA DA MATRIZ PARA COMERCIAL NF:"+ZZ2->ZZ2_PV02FL+"|"+ZZ2->ZZ2_PV02NF)
			Begin Transaction
				If FCancFat(ZZ2->ZZ2_PV02FL,ZZ2->ZZ2_PV02NF,ZZ2->ZZ2_SER02,@cMsgErro)
					U_SASP023(xFilial("ZZ9"),ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,DATE(),TIME(),cCodUsr,ZZ2->ZZ2_PV02NF+ZZ2->ZZ2_SER02,"17","SAIDA DA MATRIZ PARA COMERCIAL")
					RecLock("ZZ2",.F.)
					ZZ2->ZZ2_PV02NF	:= ""
					ZZ2->ZZ2_SER02	:= ""
					MsUnLock()
				Else
					DisarmTransaction()
					Logs(cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		If !lErro .AND. !Empty(ZZ2->ZZ2_PV03NF) 																						//SAIDA MATRIZ PARA CLIENTE
			Logs("CANCELANDO NF, SAIDA MATRIZ PARA CLIENTE NF:"+ZZ2->ZZ2_PV03FL+"|"+ZZ2->ZZ2_PV03NF)
			Begin Transaction
				If FCancFat(ZZ2->ZZ2_PV03FL,ZZ2->ZZ2_PV03NF,ZZ2->ZZ2_SER03,@cMsgErro)
					U_SASP023(xFilial("ZZ9"),ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,DATE(),TIME(),cCodUsr,ZZ2->ZZ2_PV03NF+ZZ2->ZZ2_SER03,"17","SAIDA MATRIZ PARA CLIENTE")
					RecLock("ZZ2",.F.)
					ZZ2->ZZ2_PV03NF	:= ""
					ZZ2->ZZ2_SER03	:= ""
					MsUnLock()
				Else
					DisarmTransaction()
					Logs(cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		If !lErro .AND. !Empty(ZZ2->ZZ2_NFE1) 																					//ENTRADA NO COMERCIAL
			Logs("CANCELANDO ENTRADA NO COMERCIAL NF:"+ZZ2->ZZ2_PCFIL+"|"+ZZ2->ZZ2_NFE1)
			Begin Transaction
				If FCancNfEnt(ZZ2->ZZ2_PCFIL,ZZ2->ZZ2_NFE1,ZZ2->ZZ2_SER1,ZZ2->ZZ2_PC01,@cMsgErro)
					U_SASP023(xFilial("ZZ9"),ZZ2->ZZ2_NUMERO,ZZ2->ZZ2_CLIENT,ZZ2->ZZ2_LOJA,DATE(),TIME(),cCodUsr,ZZ2->ZZ2_NFE1+ZZ2->ZZ2_SER1,"17","ENTRADA NO COMERCIAL")
					RecLock("ZZ2",.F.)
					ZZ2->ZZ2_NFE1	:= ""
					ZZ2->ZZ2_SER1	:= ""
					MsUnLock()
				Else
					DisarmTransaction()
					Logs(cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		If !lErro
			RecLock("ZZ2",.F.)
			ZZ2_STATUS='C'
			MsUnLock()
		EndIf
	End Sequence
	MsUnLockAll()
	GETMV("MV_NUMITEN",.T.)
	SX6->(MsRUnLock())
	If lErro
		RecLock("ZZ2",.F.)
		ZZ2_STATUS=cSASStaErr//'EC'
		MsUnLock()
	EndIf
Return


Static Function ErroLog(oErro,cStatus)
	DisarmTransaction()
	Logs(oErro:Description+CRLF+oErro:ErrorStack+CRLF+oErro:ErrorEnv,.T.)
	RecLock("ZZ2",.F.)
	ZZ2_STATUS=cStatus	//Altera o status para erro
	MsUnLock()
	StartJob("U_SASP050",getenvserver(),.F.)	//Inicia um nova thread
	__Quit()									//Finaliza a thread atual
Return


/*/{Protheus.doc} FGeraFat
Fatura o Pedido de venda
@author Saulo
@since 03/09/2015
@version 1.0
@param cEmpFat, String, Filial onde esta o pedido de venda para faturar
@param cPedFat, String, Numero do pedido de venda para faturar
@param aDocFat, Array, Array que recebe por referencia para gravar o Numero da NF e Serie
@param cMsgErro, String, String que recebe por referencia para gravar a msg de erro
/*/
Static Function FGeraFat(cEmpFat,cPedFat,aDocFat,cMsgErro)
	Local aArea			:= GetArea()
	Local aPvlNfs		:= {}
	Local cSerie		:= PadR("1",GetSx3Cache("F2_SERIE","X3_TAMANHO"))
	Local cFilBkp		:= cFilAnt
	Local cNFSaida		:= ""
	Local lRet			:= .T.
	Local nVezes
	Default aDocFat		:= {}
	Default cMsgErro	:= ""
	cFilAnt := cEmpFat

	//ProcRegua(4)
	Logs("Gerando Nota Fiscal de Sa�da")
	ProcessMessage()

	U_PESOMED(cEmpFat,cPedFat)

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))
	
	

	If !(SC9->(DbSeek(cEmpFat+cPedFat))) //Caso n�o tenha sido Liberado, chama para realizar a Libera��o do Pedido
	Endif
	FLibPedSZ5(cEmpFat,cPedFat,@cMsgErro)

	aTesdig = GETAREA()

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	cTesdig = SC6->C6_TES 
	

	RESTAREA(aTesdig)

	dbSelectArea("SC9")
	SC9->(dbSetOrder(1))

	If SC9->(DbSeek(cEmpFat+cPedFat))
		While SC9->(!Eof()) .AND. SC9->C9_FILIAL == cEmpFat .AND. SC9->C9_PEDIDO == cPedFat
			cProd = SC9->C9_PRODUTO
			cQuant = CVALTOCHAR(SC9->C9_QTDLIB)
			IF GETMV("SA_BLEST")
				IF cTesdig != "527"
					If !Empty(SC9->C9_BLEST) .and. !Empty(SC9->C9_BLCRED)
							cSASStaErr	:= "BE"
							cMsgErro	:= cEmpFat +"|"+cPedFat +"| Pedido com Bloqueio | Bloqueio de estoque/Credito "
							Return .F.
					ELSEIF !Empty(SC9->C9_BLEST) 
							cSASStaErr	:= "BE"
							cMsgErro	:= cEmpFat +"|"+cPedFat +" | "+ cProd +" | "+ cQuant +" | Pedido com Bloqueio | Bloqueio de estoque "
							Return .F.
					ELSEIF  !Empty(SC9->C9_BLCRED)
							cSASStaErr	:= "BE"
							cMsgErro	:= cEmpFat +"|"+cPedFat +"| Pedido com Bloqueio | Bloqueio de Credito "
							Return .F.
					ENDIF
				ENDIF
			endif
			SC6->(DbSeek(xFilial("SC6")+SC9->C9_PEDIDO+SC9->C9_ITEM+SC9->C9_PRODUTO))

			SE4->(DbSeek(xFilial("SE4")+SC5->C5_CONDPAG))
			//SE4->(DBSEEK(XFILIAL("SE4") + '001')
			SB1->(DbSeek(xFilial("SB1")+SC9->C9_PRODUTO))
			SB2->(DbSeek(xFilial("SB2")+SC9->C9_PRODUTO+SC9->C9_LOCAL))
			SF4->(DbSeek(xFilial("SF4")+SC6->C6_TES))

			aAdd(aPvlNfs,{;
			SC9->C9_PEDIDO,;
			SC9->C9_ITEM,;
			SC9->C9_SEQUEN,;
			SC9->C9_QTDLIB,;
			SC9->C9_PRCVEN,;
			SC9->C9_PRODUTO,;
			.F.,;
			SC9->(RECNO()),;
			SC5->(RECNO()),;
			SC6->(RECNO()),;
			SE4->(RECNO()),;
			SB1->(RECNO()),;
			SB2->(RECNO()),;
			SF4->(RECNO());
			})
			SC9->(DbSkip())
		EndDo
	EndIf

	If Len(aPvlNfs) == 0
		cMsgErro := "Pedido "+cPedFat+" n�o liberado"+ CHR(10) + CHR(13)
		RestArea(aArea)
		cFilAnt:= cFilBkp
		lRet	:= .F.
		Return lRet
	endif

	//Verifica se a numera��o da NF est� em uso:
	dbSelectArea("SX5")
	dbSetOrder(1)

	If SX5->(MsSeek(cEmpFat+"01"+cSerie))
		nVezes := 0
		While ( !SX5->(MsRLock()) )
			nVezes ++
			Logs("Aguardando libera��o de numera��o da Nota Fiscal de Sa�da ... "+cValtochar(nVezes))
			If ( nVezes > 30 )
				Exit
			EndIf
			Sleep(1000)
		EndDo
	Endif

	Logs("Gerando Nota Fiscal de Sa�da")

	//Gera documento de saida
	Pergunte("MT460A",.F.)
    //cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .F. , .F. , .F. , .F., 0, 0, .F., .F.)
    cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .T. , .T. , .F. , .F., 0, 0, .F., .F.)

	If Empty(cNFSaida)
		cMsgErro := "N�o foi poss�vel gerar a Nota fiscal de Sa�da da Filial "+cFilAnt+ CHR(10) + CHR(13)
	Else
		aDocFat	:= {cNFSaida,cSerie}
		Logs("Nota Fiscal de Sa�da gerada")
	Endif

	dbSelectArea("SX5")
	//If !SX5->(MsRLock()) .and. SX5->(!EOF())
	SX5->(MsUnlock())
	//Endif

	RestArea(aArea)
	cFilAnt:= cFilBkp
Return lRet


/*/{Protheus.doc} FLibPedSZ5
Liberar Pedido de Venda
@type function
@author Saulo Gomes Martins
@since 03/09/2015
@version P11
@param cEmpFat, character, Empresa do Pedido
@param cPedFat, character, Numero do Pedido
@param cErro, character, Erro enviado por referencia
/*/
Static Function FLibPedSZ5(cEmpFat,cPedFat,cMsgErro)
	Local aArea1:= GetArea()
	Local aCab	:= {}
	Local aItem	:= {}
	Local lRet	:= .T.
	Default cMsgErro	:= ""

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	aCab:= {	{"C5_TIPO"		,SC5->C5_TIPO		,NIL};
	,{"C5_NUM"		,SC5->C5_NUM		,NIL};
	,{"C5_CLIENTE"	,SC5->C5_CLIENTE	,NIL};
	,{"C5_CLIENT"	,SC5->C5_CLIENT		,NIL};
	,{"C5_LOJACLI"	,SC5->C5_LOJACLI	,NIL};
	,{"C5_CONDPAG"	,SC5->C5_CONDPAG	,NIL};
	,{"C5_EMISSAO"  ,SC5->C5_EMISSAO	,NIL}}

	//DBSELECTAREA("ZZ0")
	//DBSETORDER(1)
	
	//TODO 
	
	//DBSEEK(SC5->C5_YFILIAL+SC5->C5_YCONTRA+SC5->C5_CLIENTE+SC5->C5_LOJACLI)
	
	// FATURAMENTO LIVRARIA 
	if Select("QLIV") > 0
		QLIV->(dbCloseArea())
	Endif
	cQryITM := "SELECT ZZ0_TIPCTR FROM ZZ0010 WHERE ZZ0_FILIAL = '"+SC5->C5_YFILIAL+"' AND ZZ0_NUMERO = '"+SC5->C5_YCONTRA+"' AND  D_E_L_E_T_=''"
	TCQUERY cQryITM NEW ALIAS "QLIV"	
	
	_TPLVR:= QLIV->ZZ0_TIPCTR  //POSICIONE("ZZ0",1,SC5->C5_YFILIAL+SC5->C5_YCONTRA+SC5->C5_CLIENTE+SC5->C5_LOJACLI,"ZZ0_TIPCTR")
	
	conout(ZZ2->ZZ2_TIPCTR)

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))



	If SC6->(DbSeek(cEmpFat+cPedFat))
		While SC6->(!Eof()) .AND. SC6->C6_FILIAL == cEmpFat .AND. SC6->C6_NUM == cPedFat
			aAdd(aItem,{ {"C6_PRODUTO"	,SC6->C6_PRODUTO,NIL};
			,{"C6_ITEM"		,SC6->C6_ITEM   ,NIL};
			,{"C6_QTDVEN"	,SC6->C6_QTDVEN ,NIL};
			,{"C6_PRCVEN"	,SC6->C6_PRCVEN ,NIL};
			,{"C6_VALOR"	,SC6->C6_VALOR	,NIL};
			,{"C6_TES"		,If(!Empty(SC6->C6_YTES),SC6->C6_YTES,SC6->C6_TES)	,NIL};
			,{"C6_YTES"		,SC6->C6_YTES	,NIL};
			,{"C6_YCONTRA"	,SC6->C6_YCONTRA ,NIL};
			,{"C6_YMEDICA"	,SC6->C6_YMEDICA,NIL};
			,{"C6_YITCONT"	,SC6->C6_YITCONT,NIL};
			,{"C6_YCODKIT"	,SC6->C6_YCODKIT,NIL};
			,{"C6_YPAIKIT"	,SC6->C6_YPAIKIT,NIL};
			,{"C6_CLI"		,SC6->C6_CLI	,NIL};
			,{"C6_QTDLIB"	,SC6->C6_QTDVEN	,NIL};
			,{"C6_LOJA"		,SC6->C6_LOJA	,NIL}; 
			,{"C6_LOCAL"	,IIF(_TPLVR == "L" .AND. ZZ2->ZZ2_FATEXP == '1',SC6->C6_LOCAL,IIF(ZZ2->ZZ2_TIPCTR == '1',SUPERGETMV("SA_ARSEXP",.F.,"01"),"12")),NIL}})
			
			SC6->(DbSkip())
		EndDo

		lMsErroAuto := .F.
		//		__cInternet	:= nil
		MSExecAuto({|x,y,z|Mata410(x,y,z)},aCab,aItem,4)
		If lMsErroAuto
			cMsgErro 	:= MostraErro()
			cMsgErro	+= "Pedido "+cPedFat+" n�o foi liberado"+ CHR(10) + CHR(13)
			lRet		:= .F.
		Endif
	Endif

	RestArea(aArea1)
Return lRet

/*/{Protheus.doc} FatuPC
Gera documento de entrada referente ao Pedido de Compra
@author Diogo/Saulo
@since 04/09/2015
@version 1.0
/*/
Static Function FatuPC(cEmpFat,cPedFat,aDocFat,cMsgErro,cDocNF,cSerieNF)
	Local aCabec	:= {}
	Local aItens	:= {}
	Local aLinha	:= {}
	Local cFilBkp	:= cFilAnt
	Local lOk		:= .T.
    Local cEntarm := ZZ2->ZZ2_TIPCTR 
	Private lMsErroAuto := .F.

	cFilAnt	:= cEmpFat
	

	//ProcRegua(4)
	Logs("Gerando Nota Fiscal de Entrada")
	ProcessMessage()

	dbSelectArea("SC7")
	SC7->(dbSetOrder(1))
	SC7->(dbSeek(xFilial("SC7",cEmpFat)+cPedFat))

	aadd(aCabec,{"F1_TIPO"   ,"N"})
	aadd(aCabec,{"F1_FORMUL" ,"N"})
	aadd(aCabec,{"F1_DOC"    ,cDocNF})
	aadd(aCabec,{"F1_SERIE"  ,cSerieNF})
	aadd(aCabec,{"F1_EMISSAO",dDataBase})
	aadd(aCabec,{"F1_FORNECE",SC7->C7_FORNECE})
	aadd(aCabec,{"F1_LOJA"   ,SC7->C7_LOJA})
	aadd(aCabec,{"F1_ESPECIE","SPED"})
	aadd(aCabec,{"F1_YFILIAL",ALLTRIM(SC7->C7_FILIAL)})
	aadd(aCabec,{"F1_YCONTRA",ALLTRIM(SC7->C7_YCONTRA)})
	aadd(aCabec,{"F1_YMEDICA",ALLTRIM(SC7->C7_YMEDICA)})
	aadd(aCabec,{"F1_NATUREZ",GetMv("SA_NATPAG")})
	NNR->(dbSetOrder(1))
	_linha :=1 
	While SC7->(!eof()) .and. SC7->C7_FILIAL+SC7->C7_NUM == xFilial("SC7",cEmpFat)+cPedFat
		If !Empty(SC7->C7_YTES)
			RecLock("SC7",.F.)
			SC7->C7_TES	:= SC7->C7_YTES
			MsUnLock()
		EndIf
		aLinha := {}
		Aadd(aLinha,{"D1_COD"     	,SC7->C7_PRODUTO		,Nil})
		//Aadd(aLinha,{"D1_ITEM"     	,SC7->C7_ITEM	    ,Nil})
		Aadd(aLinha,{"D1_ITEM"     	,strzero(_linha,4)		,Nil})
		Aadd(aLinha,{"D1_QUANT"		,SC7->C7_QUANT			,Nil})
		Aadd(aLinha,{"D1_VUNIT"		,SC7->C7_PRECO			,Nil})
		Aadd(aLinha,{"D1_TOTAL"		,SC7->C7_TOTAL			,Nil})
		Aadd(aLinha,{"D1_PEDIDO"	,SC7->C7_NUM			,Nil})
		Aadd(aLinha,{"D1_TES"		,SC7->C7_TES			,Nil})
		Aadd(aLinha,{"D1_ITEMPC"	,SC7->C7_ITEM			,Nil})
		Aadd(aLinha,{"D1_LOCAL"		,IIF(cEntarm == '1',SUPERGETMV("SA_ARSEXP",.F.,"01"),SUPERGETMV("SA_ENTLOCA",.F.,"12"))	,Nil})// Faturamento Direto (Weskley Silva 11/02/2016)
			
		_linha++
		If (!NNR->(MsSeek(xFilial("NNR")+GETMV("SA_ENTLOCA"))))//(!NNR->(MsSeek(xFilial("NNR")+SC7->C7_LOCAL)))
			AGRA045(;
			{{"NNR_CODIGO",GETMV("SA_ENTLOCA"),NIL};
			,{"NNR_DESCRI","Armazem "+GETMV("SA_ENTLOCA"),NIL};
			};
			,3,{},{})
			//AGRA045(;
			//{{"NNR_CODIGO",SC7->C7_LOCAL,NIL};
			//,{"NNR_DESCRI","Armazem "+SC7->C7_LOCAL,NIL};
			//};
			//,3,{},{})
		EndIf
		aadd(aItens,aLinha)
		SC7->(DbSkip())
	EndDo

	MSExecAuto({|x,y,z| MATA103(x,y,z) },aCabec,aItens,3)
	If !lMsErroAuto
		lOk:= .T.
		aDocFat	:= {SF1->F1_DOC,SF1->F1_SERIE}
	Else
		lOk:= .F.
		cMsgErro := Mostraerro()
		cMsgErro += "N�o foi poss�vel gerar a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
	EndIf

	cFilAnt:= cFilBkp

Return lOk

/*/{Protheus.doc} FCancFat
Faz exclus�o das Notas Fiscais de Sa�da
@author Saulo Gomes Martins
@since 11/09/2015
@version 1.0
/*/

Static Function FCancFat(cEmpFat,cDocNF,cSerNF,cMsgErro)
	Local cFilBkp	:= cFilAnt
	Local nSpedExc	:= GetNewPar("MV_SPEDEXC",24)
	Local lRet		:= .T.
	Local lMostraCTB:= .F.
	Local lAglCTB	:= .F.
	Local lContab	:= .F.
	Local lCarteira	:= .T.
	Local lRetExc
	Local aRegSD2	:= {}
	Local aRegSE1	:= {}
	Local aRegSE2	:= {}
	Local nHoras
	Default cMsgErro	:= ""
	cFilAnt:= cEmpFat

	dbSelectArea("SF2")
	SF2->(dbSetOrder(1))
	If SF2->(!dbSeek(cEmpFat+cDocNF+cSerNF))
		Logs("Registro n�o encotrado! "+cEmpFat+cDocNF+cSerNF)	//N�o dar erro, mas coloca em Log
		lRet	:= .T.
		Return lRet
	EndIF

	Logs("Excluindo Nota Fiscal de Sa�da "+cDocNF)
	//Verifica se poder� realizar a exclus�o da nota fiscal
	If !Empty(SF2->F2_DAUTNFE)
		nHoras := SubtHoras(SF2->F2_DAUTNFE,SF2->F2_HAUTNFE,Date(),SubStr(Time(),1,2)+":"+SubStr(Time(),4,2))
		If nHoras > nSpedExc
			cMsgErro+="N�o foi possivel excluir a(s) nota(s), pois o prazo para o cancelamento da(s) NF-e � de " + Alltrim(STR(nSpedExc)) +" horas "+chr(13)+chr(10)
			lRet	:= .F.
			Return lRet
		EndIf
	Endif

	cMarca := GetMark(,"SF2","F2_OK")

	RecLock("SF2",.F.)
	SF2->F2_OK := cMarca
	MSUNLOCK()

	lRetExc := MaCanDelF2("SF2",SF2->(RecNo()),@aRegSD2,@aRegSE1,@aRegSE2)

	If lRetExc
		SF2->(MaDelNFS(aRegSD2,aRegSE1,aRegSE2,lMostraCTB,lAglCTB,lContab,lCarteira))
	Else
		AutoGrLog("Erro na exclus�o do Documento de Sa�da da Filial "+cFilAnt)
		cMsgErro:=Mostraerro()
		lRet	:= .F.
	EndIf
	cFilAnt:= cFilBkp
Return lRet

/*/{Protheus.doc} FCancNfEnt
Efetua estorno do Documento de Entrada
@author Saulo Gomes Martins
@since 11/09/2015
@version P11
/*/
Static Function FCancNfEnt(cEmpFat,cDocNF,cSerNF,cNumPC,cMsgErro)
	Local aCabec 		:= {}
	Local aItens		:= {}
	Local aLinha 		:= {}
	Local cFilBkp		:= cFilAnt
	Local lRet			:= .T.
	Local aFornece
	Private lMsErroAuto := .F.
	Default cMsgErro	:= ""
	cFilAnt				:= cEmpFat
	dbSelectArea("SC7")
	SC7->(dbSetOrder(1))
	SC7->(dbSeek(xFilial("SC7",cEmpFat)+cNumPC))
	aFornece	:= {SC7->C7_FORNECE,SC7->C7_LOJA}

	dbSelectArea("SF1")
	SF1->(dbSetOrder(1))	//F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA+F1_TIPO
	If SF1->(!dbSeek(xFilial("SF1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]))
		cMsgErro := "Chave n�o encontrada: "+xFilial("SF1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]
		Return .F.
	EndIf
	Logs("Excluindo Nota Fiscal de Entrada "+cDocNF)

	aadd(aCabec,{"F1_TIPO"   ,SF1->F1_TIPO})
	aadd(aCabec,{"F1_FORMUL" ,SF1->F1_FORMUL})
	aadd(aCabec,{"F1_DOC"    ,SF1->F1_DOC})
	aadd(aCabec,{"F1_SERIE"  ,SF1->F1_SERIE})
	aadd(aCabec,{"F1_EMISSAO",SF1->F1_EMISSAO})
	aadd(aCabec,{"F1_FORNECE",SF1->F1_FORNECE})
	aadd(aCabec,{"F1_LOJA"   ,SF1->F1_LOJA})
	aadd(aCabec,{"F1_ESPECIE",SF1->F1_ESPECIE})

	dbSelectArea("SD1")
	SD1->(dbSetOrder(1))	//D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
	SD1->(dbSeek(xFilial("SD1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]))

	While SD1->(!eof()) .and. SD1->D1_FILIAL+SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA == xFilial("SD1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]
		aLinha := {}
		Aadd(aLinha,{"D1_COD"     	,SD1->D1_COD			,Nil})
		Aadd(aLinha,{"D1_ITEM"     	,SD1->D1_ITEM			,Nil})
		Aadd(aLinha,{"D1_QUANT"		,SD1->D1_QUANT			,Nil})
		Aadd(aLinha,{"D1_VUNIT"		,SD1->D1_VUNIT			,Nil})
		Aadd(aLinha,{"D1_TOTAL"		,SD1->D1_TOTAL			,Nil})
		Aadd(aLinha,{"D1_PEDIDO"	,SD1->D1_PEDIDO			,Nil})
		Aadd(aLinha,{"D1_ITEMPC"	,SD1->D1_ITEMPC			,Nil})
		Aadd(aLinha,{"D1_LOCAL"		,SD1->D1_LOCAL			,Nil})

		NNR->(dbSetOrder(1))
		If (!NNR->(MsSeek(xFilial("NNR")+SD1->D1_LOCAL)))
			AGRA045(;
			{{"NNR_CODIGO",SD1->D1_LOCAL,NIL};
			,{"NNR_DESCRI","Armazem "+SD1->D1_LOCAL,NIL};
			};
			,3,{},{})
		EndIf
		aadd(aItens,aLinha)
		SD1->(DbSkip())
	EndDo

	//	MATA103(aCabec,aItens,5)
	MSExecAuto({|x,y,z| MATA103(x,y,z) },aCabec,aItens,5)

	If !lMsErroAuto
	Else
		lRet	:= .F.
		cMsgErro += Mostraerro()
		cMsgErro := "N�o foi poss�vel excluir a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
	EndIf
	cFilAnt:= cFilBkp

Return lRet

/*/{Protheus.doc} Logs
Grava log em arquivo texto no servidor
@author Saulo Gomes Martins
@since 22/04/2015
@version 1.0
@param cTexto, character, Texto do log
/*/
Static Function Logs(cTexto,lErro)
	Local cCodLog	:= SubStr(DTOS(Date()),1,6)
	Local cPasta	:= "\log\SASP050"
	Local cTexLog
	Default lErro	:= .F.
	FWMakeDir(cPasta)
	If File(cPasta+"\"+cCodLog+".txt")
		nHdl := fopen(cPasta+"\"+cCodLog+".txt" , 2 + 64 )
	Else
		nHdl := FCREATE(cPasta+"\"+cCodLog+".txt", FC_NORMAL)
	EndIf
	FSeek(nHdl, 0, 2)
	FWrite(nHdl, DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+CRLF )
	//cErroJob	+= DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+CRLF
	fclose(nHdl)
	If lErro
		RecLock("ZZ2",.F.)
		cTexLog			:= Alltrim(ZZ2->ZZ2_LOG)
		ZZ2->ZZ2_LOG	:= DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+chr(10)+cTexLog
		MsUnLock()
		lErroJob	:= .T.
	EndIf
Return

Static Function Monitor(cMsg)

	tcsqlexec("INSERT INTO YJOB (JOBLOG) VALUES ('"+cMsg+"')")
	PutGlbValue("SASP050",DTOC(date(),"dd/mm/yy")+"|"+time()+cMsg)
	PtInternal(1,DTOC(date(),"dd/mm/yy")+"|"+time()+"|"+cMsg)	//Atualiza mensagem no monitor
Return
