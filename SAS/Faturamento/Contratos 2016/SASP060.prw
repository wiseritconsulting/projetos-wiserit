#INCLUDE "RWMAKE.CH"
#Include "Protheus.ch"
#INCLUDE "topconn.ch"
#include "tbiconn.ch"

User function SASP060()

Local cProd := ""
	Local lImpo := .F.
	Local oFont
	Local lMSErroAuto := .F.
	Local lMsHelpAuto := .T.
	Local aCab        := {}
	Local aItens      := {}
	Local aLinha      := {}
	Private oLeTxt,cRegs

	DEFINE MSDIALOG oLeTxt FROM 0, 0 TO 150, 400 TITLE "Importar PEDIDO OI" PIXEL
	@ 10, 10  SAY   "Programa realizar manuten�ao no custo de entrada, "        OF oLeTxt PIXEL
	@ 22, 10  SAY   "atraves de arquivos com extens�o *.CSV     "        OF oLeTxt PIXEL
	@ 46, 50  BUTTON oBtn Prompt "CSV" SIZE 30, 13 OF oLeTxt PIXEL Action (lImpo := .T.,Close(oLeTxt))      //Importacao CSV
	@ 46,130  BUTTON oBtn Prompt "CANCEL" SIZE 30, 13 OF oLeTxt PIXEL Action (lExec := .F.,Close(oLeTxt))   //Close(oLeTxt)

	ACTIVATE MSDIALOG oLeTxt CENTERED

	If lImpo

		Processa({|| RunImpo() },"Importando...")

	Endif

Return Nil


/*-----------------------------------------------------------------------+
| Programa  | RunImpo   | Autor | Manoel Nesio    | Data | 16/02/2016  |
+------------------------------------------------------------------------+
| Descri��o | Leitura de arquivo CSV para update de D1_CUSTO         	|
+-----------------------------------------------------------------------*/
Static Function RunImpo()

	Local aCab          := {} //Array para receber os campos dos Produtos Inventariados
	Local aItem         := {} //Array para receber os campos dos Produtos Inventariados
	Local nI			:= 1
	Local cBuffer		:= ""
	Local cTemp			:= ""
	Local cFileOpen     := ""
	Local cTitulo1		:= "Selecione o arquivo"
	Local cExtens		:= "Arquivo CSV | *.csv"
	Local XTEMP         := ""
	Local cQry          := ""
	Local nCount        := 0
	Local lFirst        := .t.

	//Variaveis do arquivo CSV
	Local cRecno		:=""
	Local cSerie		:=""
	Local cDoc      	:=""
	Local caData 		:= CTOD("  /  /  ")
	Local caQini		:= 0
	Local caLinha       :=""
	Local cFornece  	:=""
	Local cLoja		    :=""
	Local cProd		    :=""
	Local caValor		:=""
	Local nCountInv     := 0
	Local nItem         := 0
	Local lRet          := .T.
	Local lok           := .T.
	Local aNf			:={}
	Local aNotas		:={}
	Local cRecIssF		:=""
	Local cRecIssC		:=""
	Local cRecIssP		:=""
	Local cContFile		:= "0000"
	Local cItem			:= ""
	//Array para impress�o

	Private cMsgErr     := ""
	Private cMsgErr1    := ""
	Private nIndice		:= 1

	//--------------------
	Private cCadastro	:= "Ler arquivo texto"
	PRIVATE lMsErroAuto := .F.

	cFileOpen := cGetFile(cExtens,cTitulo1,,,.T.)

	If !File(cFileOpen)
		MsgAlert("Arquivo texto: "+cFileOpen+" n�o localizado", cCadastro)
		Return
	EndIF

	/**************************************************************



	***************************************************************/


	FT_FUSE(cFileOpen) 			//Abrir
	FT_FGOTOP() 				//Ponto no topo
	ProcRegua(FT_FLASTREC()) 	//Quantos registros ler
	_lfirst := .T.

	aFolsPln := {}
	aChvMat  := {}

	While !FT_FEOF() 		    //Faca enquanto nao for fim de arquivo

		IncProc()
		
		
		
		cBuffer     := FT_FREADLN() //Lendo linha

		if substr(cBuffer,1,1)=="N" .or. substr(cBuffer,1,1)=="n"
			FT_FSKIP()
			cBuffer     := FT_FREADLN() //Lendo linha
		endif
		
		//aNf			:={}
		
		cRecno       := cValToChar(Val(SubStr(cBuffer,01, At(";",cBuffer)-1)))

	//	cBuffer 	:=SubStr(cBuffer,At(";",cBuffer)+1,Len(cBuffer))
//		caValor     :=SubStr(cBuffer,01, At(";",cBuffer)-1)
	  //  caValor     :=  str(ROUND(VAL(caValor),6))
	//	caValor     :=StrTran(caValor,".","")
	//	caValor     :=StrTran(caValor,",",".")
		//AADD(aNf, {"Valor", ALLTRIM(caValor)})
		
		
 caValor  :=SubStr(cBuffer,At(";",cBuffer)+1,Len(cBuffer))

  caValor     := StrTran(caValor,".","")
  caValor     := StrTran(caValor,",",".")
		
		

		cContFile := SOMA1(cContFile)
		
		//update com recno
		cQuery := "UPDATE "+RetSqlName("SD1")+" SET D1_CUSTO ="+caValor+" "
		cQuery += "WHERE R_E_C_N_O_="+cRecno 
		IF TCSQLEXEC(cQuery ) < 0
			Alert("Linha "+cContFile+" dados n�o encontrado! Verifique!")
		Endif

		
		FT_FSKIP() //Pr�ximo registro no arquivo csv

	EndDo

	FT_FUSE() //Fecha o arquivo csv
	If !lok
		MsgInfo ("ERRO NA IMPORTA��O :"+chr(13)+chr(10)+cMsgErr )

		Return
	Else
		MsgInfo ("Leitura realizada : "+cContFile+" linhas." )
	EndIf


	//Processa({|| lRet := UpdCusto(aNf) },"Realizando a manuten��o nos custos...","Aguarde...",.F.)

	MsgInfo("Manuten�ao do custo de entrada realizada!")



Return