#include 'protheus.ch'
#include "topconn.ch"
#include 'parmtype.ch'


User function SASP052(cPai,cTipo,cCod)
Local lRet := .T.

cQuery := " SELECT * "
cQuery += " FROM   "+RetSqlName( 'ZZ7' ) + " ZZ7 " "
cQuery += " WHERE  ZZ7.D_E_L_E_T_='' "
cQuery += " AND    ZZ7_CODPAI = '"+cPai+"' "
cQuery += " AND    ZZ7_TIPO =   '"+cTipo+"' "
cQuery += " AND    ZZ7_CODIGO = '"+cCod+"' "

TCQuery cQuery new alias VALD

IF !VALD->(EOF())
	ALERT("O C�digo "+cCod+" j� cadastrado, digite outro sequencial !")
	lRet := .F.
ENDIF

VALD->(DBCLOSEAREA())
	
Return lRet