#Include 'Protheus.ch'
#INCLUDE 'FWMBROWSE.CH'
#INCLUDE 'FWMVCDEF.CH'
#include 'TopConn.CH'

/*/{Protheus.doc} SASP051
Tela para marca��o de faturamento
@type function
@author Saulo Gomes Martins
@since 18/09/2015
@version P11
/*/
User Function SASP051()
	Local oDlg
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Local cTextHead	:= "QLabel { color: rgb(39,82,102); font-size: 12px; font-weight: bold;  } "
	Local oFWLayer
	Local aOperacao	:= {" ","Faturar","Cancelar NF","Abortar Fatur./Cancel.","Alterar Prioridade", "Gerar Romaneio"}	
	Private aObjTela	:= {}
	Private cOperacao	:= aOperacao[1]
	Private oMark
	Private nPenFat		:= 0
	Private nPenCan		:= 0
	Private nPenErr		:= 0
	Private nPenTFat	:= 0
	Private nPenTCan	:= 0
	Private nPenTErr	:= 0
	Private cHoraAtu	:= ""
	Private cDataAtu	:= ""
	Private cMonitor	:= ""
	Private lFatLiv		:= .F.
	Private aCampos		:= {}

	//StartJob("U_SASP058",getenvserver(),.F.)	//Inicia um nova thread
	AtualTot()
	oDlg := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Faturamento',,,,nOr(WS_VISIBLE,WS_POPUP),,,,,.T.,,,,)
	/*oFWLayer := FWLayer():New()
	oFWLayer:Init( oDlg, .F., .T. )
	oFWLayer:AddLine('LIN1', 10, .F. )
	oFWLayer:AddLine('LIN2', 90, .F. )
	oFWLayer:AddCollumn('COL1',100, .T., 'LIN1' )
	oFWLayer:AddCollumn('COL1',100, .T., 'LIN2' )
	oPainel1 := oFWLayer:GetColPanel( 'COL1', 'LIN1' )
	oPainel2 := oFWLayer:GetColPanel( 'COL1', 'LIN2' )*/
	@00,00 MSPANEL oBar SIZE 33,33 of oDlg
	oBar:Align := CONTROL_ALIGN_TOP
	oBtn := TButton():New(01,01,"     Atualizar",oBar ,{|| AtualTot(),oMark:Refresh(.F.) },50,20,,,.F.,.T.,.F.,,.F.,,,.F. )
	oBtn:SetCss(CssbtnImg("reload.png","#006386"))
	oSay := TSay():New(001,060, {||"Pendente Faturamento Filial: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(008,060, {||"Pendente Cancelamento Filial: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(015,060, {||"Erros Filial: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(001,160, {||Transform(nPenFat,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(008,160, {||Transform(nPenCan,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(015,160, {||Transform(nPenErr,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(001,200, {||"Pendente Faturamento Total: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(008,200, {||"Pendente Cancelamento Total: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(015,200, {||"Erros Total: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(001,300, {||Transform(nPenTFat,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(008,300, {||Transform(nPenTCan,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(015,300, {||Transform(nPenTErr,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(001,350, {||"Opera��o:"},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	TComboBox():New(008,350,{|u|if(PCount()>0,cOperacao:=u,cOperacao)},;
	aOperacao,090,20,oBar,,{|| TrocaOperacao() };
	,,,,.T.,,,,,,,,,'cOperacao')
		
	oSay := TSay():New(008,550, {||"Data JOB: "+DToC(cDataAtu)},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)

	oSay := TSay():New(024,010, {||"Status JOB:"+cMonitor},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	
	oSay := TSay():New(024,550, {||"Atualizado: "+cHoraAtu},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)

	oMark 	:= FWMarkBrowse():New()
	oMark:SetAlias('ZZ2')	
	oMark:SetSemaphore(.T.)
	oMark:SetValid({|| Valid() })
	oMark:SetDescription('Faturamento')
	oMark:SetFieldMark( 'ZZ2_OK' )
	oMark:SetAllMark({|| MarcaTudo() })

	oMark:SetUseFilter(.T.)	
	oMark:AddButton("Fat Livraria ",{||FatLivra()},,,)

	oMark:AddLegend( "ZZ2_STATUS=='A '"						,"BR_MARROM"	,"Ativo")
	oMark:AddLegend( "ZZ2_STATUS=='L '"						,"BR_LARANJA"	,"Liberado")
	oMark:AddLegend( "ZZ2_STATUS=='F '"						,"BR_VERDE"		,"Faturado")
	oMark:AddLegend( "ZZ2_STATUS=='C '"						,"BR_VERMELHO"	,"Cancelado")
	oMark:AddLegend( "ZZ2_STATUS=='P '"						,"BR_AMARELO"	,"Pendente Faturamento")
	oMark:AddLegend( "ZZ2_STATUS=='Q '"						,"BR_AZUL"		,"Pendente Cancelamento")
	oMark:AddLegend( "ZZ2_STATUS=='Q '.OR.ZZ2_STATUS=='P '"	,"BR_AMARELO"	,"Pendentes")
	oMark:AddLegend( "ZZ2_STATUS=='EF'"						,"BR_PRETO"		,"Erro Faturamento")
	oMark:AddLegend( "ZZ2_STATUS=='EC'"						,"BR_CINZA"		,"Erro Cancelamento")
	oMark:AddLegend( "ZZ2_STATUS=='EF'.OR.ZZ2_STATUS=='EC'"	,"BR_PRETO"		,"Erros")
	oMark:AddLegend( "ZZ2_STATUS=='BE'"						,"BR_PINK"		,"Bloqueio Estoque")
	//oMark:SetTimer({||AtualTot()},3000)
	oMark:SetMenuDef( 'SASP051' )	
	
	oMark:Activate(oDlg)
	oTimer := TTimer():New(3000,{||AtualTot(),aEval(aObjTela,{|x| x:Refresh()})}, oDlg )
	oTimer:Activate()
	oDlg:Activate(,,,.T.)
Return

Static Function TrocaOperacao()


	Local cEtpfim := GETMV('SA_ETPFIM')

	LimparMarca()
	
	If Empty(cOperacao)
		oMark:SetFilterDefault(".T.")
	ElseIf cOperacao=="Faturar"
		oMark:SetFilterDefault("ZZ2_STATUS $ '  |C |L |EF|BE' .AND. ZZ2_ETAPA >= '" + cEtpfim + "'")
		//oMark:SetFilterDefault("ZZ2_STATUS $ '  |C |L |EF|' ")
	ElseIf cOperacao=="Cancelar NF"
		oMark:SetFilterDefault("ZZ2_STATUS $ 'F |EC|BE'")
	ElseIf cOperacao=="Abortar Fatur./Cancel."
		oMark:SetFilterDefault("ZZ2_STATUS=='P '.or.ZZ2_STATUS='Q '")
	ElseIf cOperacao=="Alterar Prioridade"
		oMark:SetFilterDefault("ZZ2_ETAPA == '" + cEtpfim + "'")
	ElseIf cOperacao == "Gerar Romaneio"		
		//TODO INCLUIR PARAMETRO PARA ETAPA FATURADO
		oMark:SetFilterDefault("ZZ2_ETAPA == '46' .AND. ZZ2_STATUS == 'F'")
	EndIf
	//oMark:ExecuteFilter()
	
	AtualTot()
	oMark:Refresh(.T.)

Return

static function FatLivra()


	Local aPergs := {}


	local _serie := space(TAMSX3("ZZO_COD")[1]) 
	local _client := space(TAMSX3("A1_COD")[1])
	local _loja := space(TAMSX3("A1_LOJA")[1])
	local _contr := space(TAMSX3("ZZ0_NUMERO")[1]) 
	local _envio := space(TAMSX3("ZZ7_ENVIO")[1])


	Local aRet := {}
	Local lRet 
	Local cQuery
	Local cFilLiv	:= ""


	//Criar CP SF2_2 e ZZ0

	//aAdd( aPergs ,{9,_emiss,30,20,.f.})
	aAdd( aPergs ,{1,"Cliente ",_client,"@!",'.T.',"SA1",'.T.',TAMSX3("A1_COD")[1],.T.})
	aAdd( aPergs ,{1,"Loja ",_loja,"@!",'.T.',,'.T.',TAMSX3("A1_LOJA")[1],.T.})
	aAdd( aPergs ,{1,"Contrato ",_contr,"@!",'.T.',"ZZ0",'.T.',TAMSX3("ZZ0_NUMERO")[1],.T.})
	aAdd( aPergs ,{1,"Envio ",_envio,"@!",'.T.',,'.T.',TAMSX3("ZZ7_ENVIO")[1],.T.})
	aAdd( aPergs ,{1,"Serie ",_serie,"@!",'.T.',"ZZO",'.T.',TAMSX3("ZZO_COD")[1],.T.})

	If ParamBox(aPergs ,"Parametros ",aRet)       


		cQuery := " SELECT COUNT(*) CONT FROM "+RETSQLNAME("ZZ2")+" ZZ2"
		cQuery+= " INNER JOIN "+RETSQLNAME("ZZ3")+" ZZ3 ON ZZ2_FILIAL = ZZ3_FILIAL AND ZZ2_NUMERO = ZZ3_NUMERO "
		cQuery+= " AND ZZ2_MEDICA = ZZ3_MEDICA AND ZZ2_CLIENT = ZZ3_CLIENT AND ZZ2_LOJA = ZZ3_LOJA"
		cQuery+= " INNER JOIN "+RETSQLNAME("ZZ0")+" ZZ0 ON ZZ0_FILIAL = ZZ2_FILIAL AND ZZ0_NUMERO = ZZ2_NUMERO AND "
		cQuery+= " ZZ0_CLIENT = ZZ2_CLIENT AND ZZ0_LOJA = ZZ2_LOJA "
		cQuery+= " INNER JOIN "+RETSQLNAME("ZZ7")+" ZZ7 ON ZZ7_CODIGO = ZZ3_PRODUT "
		cQuery+= " WHERE ZZ2_CLIENT = '"+MV_PAR01+"' AND ZZ2_LOJA = '"+MV_PAR02+"' " 
		cQuery+= " AND ZZ2_NUMERO = '"+MV_PAR03+"' AND ZZ3_ENVIO = '"+MV_PAR04+"' AND ZZ7_SERIE = '"+MV_PAR05+"' AND ZZ2_STATUS = 'L' "
		cQuery+= " AND ZZ0_TIPCTR = 'L' "
		cQuery+= " AND ZZ3.D_E_L_E_T_ = '' AND ZZ2.D_E_L_E_T_ = '' AND ZZ7.D_E_L_E_T_ = '' AND ZZ0.D_E_L_E_T_ = '' "

		if Select("T01") > 0
			dbSelectArea("T01")
			dbCloseArea()
		endif

		TcQuery cQuery New Alias T01

		IF T01->(!EOF())

			cQuery := " SELECT ZZ2_NUMERO, ZZ2_MEDICA, ZZ2_CLIENT, ZZ2_LOJA FROM "+RETSQLNAME("ZZ2")+" ZZ2"
			cQuery+= " INNER JOIN "+RETSQLNAME("ZZ3")+" ZZ3 ON ZZ2_FILIAL = ZZ3_FILIAL AND ZZ2_NUMERO = ZZ3_NUMERO "
			cQuery+= " AND ZZ2_MEDICA = ZZ3_MEDICA AND ZZ2_CLIENT = ZZ3_CLIENT AND ZZ2_LOJA = ZZ3_LOJA"
			cQuery+= " INNER JOIN "+RETSQLNAME("ZZ0")+" ZZ0 ON ZZ0_FILIAL = ZZ2_FILIAL AND ZZ0_NUMERO = ZZ2_NUMERO AND "
			cQuery+= " ZZ0_CLIENT = ZZ2_CLIENT AND ZZ0_LOJA = ZZ2_LOJA "
			cQuery+= " INNER JOIN "+RETSQLNAME("ZZ7")+" ZZ7 ON ZZ7_CODIGO = ZZ3_PRODUT "
			cQuery+= " WHERE ZZ2_CLIENT = '"+MV_PAR01+"' AND ZZ2_LOJA = '"+MV_PAR02+"' " 
			cQuery+= " AND ZZ2_NUMERO = '"+MV_PAR03+"' AND ZZ3_ENVIO = '"+MV_PAR04+"' AND ZZ7_SERIE = '"+MV_PAR05+"' AND ZZ2_STATUS = 'L'"
			cQuery+= " AND ZZ0_TIPCTR = 'L' "
			cQuery+= " AND ZZ3.D_E_L_E_T_ = '' AND ZZ2.D_E_L_E_T_ = '' AND ZZ7.D_E_L_E_T_ = '' AND ZZ0.D_E_L_E_T_ = '' "

			if Select("T02") > 0
				dbSelectArea("T02")
				dbCloseArea()
			endif

			TcQuery cQuery New Alias T02

			For nI:=1  to T01->CONT

				IF nI <> T01->CONT

					cFilLiv += "(ZZ2_MEDICA == '"+T02->ZZ2_MEDICA+"' .AND. ZZ2_NUMERO == '"+T02->ZZ2_NUMERO+"' ) .OR. "

				ELSE

					cFilLiv += "(ZZ2_MEDICA == '"+T02->ZZ2_MEDICA+"' .AND. ZZ2_NUMERO == '"+T02->ZZ2_NUMERO+"') "

				ENDIF

				T02->(DbSkip())

			nEXT

			//cFilLiv:= "ZZ2_NUMERO = '000364' AND ZZ2_FILIAL = '050101'"
			oMark:SetFilterDefault(cFilLiv)
			//oMark:AddFilter("Filtro Livraria", cFilLiv,.T. ,.T. , "ZZ3",.F.,,)


		ENDIF

	Endif

return

Static Function MenuDef()
	Local aRotina := {}
	Local aNota	:= {}
	Local aPedido	:= {}
	Local aDevol	:= {}
	ADD OPTION aRotina	TITLE 'Visualizar'	 					ACTION 'U_SASP051B' OPERATION 2 ACCESS 0
	ADD OPTION aRotina	TITLE 'Processar Marcados'	 			ACTION 'U_SASP051A()' OPERATION 4 ACCESS 0
	ADD OPTION aRotina	TITLE 'Re-Liberar'	 		            ACTION 'U_SASP051C()' OPERATION 4 ACCESS 0
	ADD OPTION aRotina	TITLE 'Placa de Embarque'	 			ACTION 'U_SASR033()' OPERATION 2 ACCESS 0
	ADD OPTION aRotina	TITLE 'Imprimir Etiqueta'	 			ACTION 'U_SASR031()' OPERATION 2 ACCESS 0	
	ADD OPTION aRotina TITLE  'Romaneio'			 			ACTION 'U_SASP051D()' OPERATION 2 ACCESS 0
	//ADD OPTION aRotina TITLE  'Romaneio'			 			ACTION 'U_SASR035()' OPERATION 2 ACCESS 0
	//ADD OPTION aRotina	TITLE 'Marcar Tudo'	 		         	ACTION 'MarcaTudo()' OPERATION 4 ACCESS 0
	//ADD OPTION aRotina	TITLE 'Cancelamento'	 			ACTION 'U_SASP051A("Faturamento","Q ")' OPERATION 4 ACCESS 0
	//ADD OPTION aRotina	TITLE 'Alterar Prioridade'	 		ACTION 'U_SASP051A("Alterar Prioridade",)' OPERATION 4 ACCESS 0
Return aRotina


Static Function Valid()
	If Empty(cOperacao)
		MsgAlert("Escolha a opera��o primeiro")
		Return .F.
	EndIf
Return .T.

// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO MARCADO
STATIC FUNCTION YINVERTE(oMark)
	if empty(ZZ2->ZZ2_OK)
		oMark:Mark()
	endif
RETURN

User Function SASP051A(cTitulo,cStatus)
	If Empty(cOperacao)
		MsgAlert("Escolha a opera��o")
		Return
	ElseIf cOperacao=="Faturar"
		cTitulo	:= cOperacao
		cStatus	:= "P "
	ElseIf cOperacao=="Cancelar NF"
		If !MsgYesNo("Confirmar cancelamento da NF?")
			Return
		EndIf
		cTitulo	:= cOperacao
		cStatus	:= "Q "
	ElseIf cOperacao=="Abortar Fatur./Cancel."
		cTitulo	:= cOperacao
		cStatus	:= "L "
	ElseIf cOperacao=="Alterar Prioridade"
		cTitulo	:= cOperacao
		cStatus	:= nil
	Else
		Return
	EndIf
	
	Processa({|lEnd| RunProc(cTitulo,cStatus,@lEnd) },cTitulo,"Aguarde...",.T.)
	
Return



Static Function RunProc(cTitulo,cStatus,lEnd)
	Local cAlias		:= GetNextAlias()
	Local aPergs		:= {}
	Local aRet			:= {}
	Local cPrior		:= "99"
	Local cAvisos		:= ""
	If !QueryMarca(cAlias) 
		MsgAlert("N�o foi marcado nenhum item!")
		(cAlias)->(DbCloseArea())
		Return .F.
	EndIF
	
	If ZZ2->ZZ2_FATEXP == "2" .AND. ALLTRIM(ZZ2->ZZ2_TRANSP) == "" 
		MsgAlert("Medicao "+ZZ2->ZZ2_MEDICA+" sem Transportadora! Verifique")
		(cAlias)->(DbCloseArea())
		Return .F.
	EndIF
	//TODO BLOQUEIO DE MEDICAO NAO TRANSFERIDA DA TRIANGULA�AO 
	If ALLTRIM(ZZ2->ZZ2_DTPRET) == "" .AND. ALLTRIM(ZZ2->ZZ2_PRETRA) == "" .AND. ZZ2->ZZ2_TIPCTR == '2'
		MsgAlert("Medicao "+ZZ2->ZZ2_MEDICA+" ainda n�o transferida! Verifique")
		(cAlias)->(DbCloseArea())
		Return .F.
	EndIF
	
	//TODO Verifica se Cliente est� bloqueado 
	// Weskley Silva 30.11.2016
	cQuery1 := " select ZZ3_TES "
	cQuery1 += " FROM " + RETSQLNAME("ZZ3")+" ZZ3 "
	cQuery1 += " WHERE ZZ3.D_E_L_E_T_ =''  "
	cQuery1 += " AND   ZZ3.ZZ3_TES    IN ("+GETMV("SA_TESFIN")+") "
	cQuery1 += " AND   ZZ3.ZZ3_FILIAL = '"+ZZ2->ZZ2_FILIAL+"' "
	cQuery1 += " AND   ZZ3.ZZ3_MEDICA = '"+ZZ2->ZZ2_MEDICA+"' "
	cQuery1 += " AND   ZZ3.ZZ3_NUMERO = '"+ZZ2->ZZ2_NUMERO+"' "
	cQuery1 += " AND   ZZ3.ZZ3_CLIENT = '"+ZZ2->ZZ2_CLIENT+"' "
	cQuery1 += " AND   ZZ3.ZZ3_LOJA   = '"+ZZ2->ZZ2_LOJA+"' "
	cQuery1 += " GROUP BY ZZ3.ZZ3_TES "
	
	IF Select("T04") > 0 
	T04->(DbCloseArea())
	ENDIF
	TCQuery cquery1 new alias T04
	
	IF !T04->(eof())
	IF POSICIONE("SA1",1,XFILIAL("SA1")+ALLTRIM(ZZ2->ZZ2_CLIENT)+ALLTRIM(ZZ2->ZZ2_LOJA),"A1_YBLOQ") == "S"
		MsgAlert("Cliente "+ZZ2->ZZ2_CLIENT+" Bloqueado! Verifique com o Financeiro ou PCL")
		(cAlias)->(DbCloseArea())
		Return .F.
	ENDIF
	T04->(DbCloseArea())
	ENDIF
	// Fim
	
	
	aAdd( aPergs ,{1,'Prioridade',cPrior,'99',"","",'.T.',60,.F.})
	If ParamBox(aPergs,cTitulo,aRet,,,,,,,"",.F.,.F.)
		While (cAlias)->(!EOF()) .and. !lEnd
			IncProc("Contrato:"+ZZ2->ZZ2_NUMERO+",Medi��o:"+ZZ2->ZZ2_MEDICA)
			ZZ2->(DbGoTo((cAlias)->REGZZ2))
			If !(ZZ2->(SimpleLock()) .and. RecLock("ZZ2",.F.))
				cAvisos			+= "N�o � possivel realizar opera��o, registro em uso. Contrato:"+ZZ2->ZZ2_NUMERO+"Medi��o:"+ZZ2->ZZ2_MEDICA+CRLF
				(cAlias)->(DbSkip())
				Loop
			EndIf
			ZZ2->(DbGoTo((cAlias)->REGZZ2))	//Atualizar registro posicionado!
			If cStatus=="  " .and. (ZZ2->ZZ2_STATUS=="F " .OR. ZZ2->ZZ2_STATUS=="C ")
				cAvisos			+= "N�o � possivel abortar, opera��o j� concluida. Contrato:"+ZZ2->ZZ2_NUMERO+"Medi��o:"+ZZ2->ZZ2_MEDICA+CRLF
				ZZ2->ZZ2_OK		:= Space(Len(ZZ2->ZZ2_OK))
				MsUnLock()
				(cAlias)->(DbSkip())
				Loop
			EndIF
			If ValType(cStatus)=="C"
				ZZ2->ZZ2_STATUS	:= cStatus
				ZZ2->ZZ2_USEFAT := UPPER(cUserName)
			EndIf
			ZZ2->ZZ2_PRIORI	:= StrZero(Val(aRet[1]),Len(ZZ2->ZZ2_STATUS))
			ZZ2->ZZ2_OK		:= Space(Len(ZZ2->ZZ2_OK))
			ZZ2->ZZ2_USEFAT := UPPER(cUserName)
			MsUnLock()
			(cAlias)->(DbSkip())
		EndDO
		oMark:Refresh(.T.)	//Atualiza e posiciona no primeiro registro
		AtualTot()
		If !Empty(cAvisos)
			MsgAlert(cAvisos)
		EndIF
		If lEnd
			MsgAlert("Altera��o cancelada!"+chr(13)+"Alguns registros podem ter sido alterado.")
		Else
			MsgInfo("Opera��o concluida!")
		EndIf
	EndIf
	(cAlias)->(DbCloseArea())
Return

Static Function QueryMarca(cAlias)
	Local cMarca	:= oMark:Mark()
	BeginSql Alias cAlias
	SELECT R_E_C_N_O_ AS REGZZ2 FROM %table:ZZ2% ZZ2 WHERE ZZ2_FILIAL=%xFilial:ZZ2% AND ZZ2.ZZ2_OK = %Exp:cMarca% and ZZ2.D_E_L_E_T_ = ' '
	EndSql
Return (cAlias)->(!EOF())


Static Function LimparMarca()
	Local cAlias	:= GetNextAlias()
	QueryMarca(cAlias)
	While (cAlias)->(!EOF())
		ZZ2->(DbGoTo((cAlias)->REGZZ2))
		RecLock("ZZ2")
		ZZ2->ZZ2_OK		:= Space(Len(ZZ2->ZZ2_OK))
		MsUnLock()
		(cAlias)->(DbSkip())
	EndDO
Return

Static Function QueryDsMr(cAlias)
	Local cMarca	:= oMark:Mark()
	BeginSql Alias cAlias
	SELECT R_E_C_N_O_ AS REGZZ2 FROM %table:ZZ2% ZZ2 WHERE ZZ2_FILIAL=%xFilial:ZZ2% AND ZZ2.ZZ2_OK <> %Exp:cMarca% and ZZ2.D_E_L_E_T_ = ' '
	EndSql
Return (cAlias)->(!EOF())

Static Function MarcaTudo()
	Alert("Marca Tudo")
	/*Local cAlias	:= GetNextAlias()
	QueryDsMr(cAlias)
	alert("cAlias")
	While (cAlias)->(!EOF())
		alert("entrou")
		ZZ2->(DbGoTo((cAlias)->REGZZ2))
		RecLock("ZZ2")
		ZZ2->ZZ2_OK		:= cMarca
		alert("cMarca")
		MsUnLock()
		(cAlias)->(DbSkip())
	EndDO*/
Return

Static Function SasMark(oMark)
	MarcaTudo()
	AtualTot()
	oMark:Refresh(.T.)
RETURN


Static Function AtualTot()
	Local cAlias	:= GetNextAlias()
	Local aMonitor	:= {}
	Local nPos
	Local cFilZZ2	:= xFilial("ZZ2",cFilAnt)
	Local aArea		:= GetArea()
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZ2% ZZ2 WHERE ZZ2_FILIAL=%exp:cFilZZ2% AND ZZ2_STATUS='P ' and ZZ2.D_E_L_E_T_ = ' '
	EndSql
	nPenFat	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZ2% ZZ2 WHERE ZZ2_FILIAL=%exp:cFilZZ2% AND ZZ2_STATUS='Q ' and ZZ2.D_E_L_E_T_ = ' '
	EndSql
	nPenCan	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZ2% ZZ2 WHERE ZZ2_FILIAL=%exp:cFilZZ2% AND (ZZ2_STATUS like 'E%' OR ZZ2_STATUS like 'BE') and ZZ2.D_E_L_E_T_ = ' '
	EndSql
	nPenErr	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	//TOTAL
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZ2% ZZ2 WHERE ZZ2_STATUS='P ' and ZZ2.D_E_L_E_T_ = ' '
	EndSql
	nPenTFat	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZ2% ZZ2 WHERE ZZ2_STATUS='Q ' and ZZ2.D_E_L_E_T_ = ' '
	EndSql
	nPenTCan	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZ2% ZZ2 WHERE (ZZ2_STATUS like 'E%' OR ZZ2_STATUS like 'BE') and ZZ2.D_E_L_E_T_ = ' '
	EndSql
	nPenTErr	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	cHoraAtu	:= TIME()
	cDataAtu	:= DATE()
	cMonitor	:= GetGlbValue("SASP050")
	If ValType(cMonitor)=="U"
		cMonitor	:= ""
	EndIf
	RestArea(aArea)
	ProcessMessage()
Return

User Function SASP051C()


	If Alltrim(ZZ2_STATUS)=='EF' .OR. Alltrim(ZZ2_STATUS)=='EC'

		DBSELECTAREA("ZZ2")
		DBSETORDER(1)
		DBSEEK(ZZ2->ZZ2_FILIAL + ZZ2->ZZ2_NUMERO + ZZ2->ZZ2_MEDICA + ZZ2->ZZ2_CLIENT + ZZ2->ZZ2_LOJA)

		If Found()

			RECLOCK("ZZ2", .F.)

			ZZ2->ZZ2_STATUS := 'L'
			ZZ2->ZZ2_LOG := MSMM("",80)

			MSUNLOCK()

		EndIf

	Else

		MessageBox("O contrato n�o est� com ERROS. Verifique e tente novamente.","TOTVS",48)

	EndIf

Return

//Romaneio por lote
User Function SASP051D()
	Local cAlias := GetNextAlias()
	Local Qry := QueryMarca(cAlias)
	
	

	U_SASR035(Qry, cAlias)
Return


Static Function CssbtnImg(cImg,cCor)
	Local cRet	:= " QPushButton{ background-image: url(rpo:"+cImg+")"+;
	" ;border-style:solid"+;
	" ;border-width:5px"+;
	" ;background-repeat: none; margin: 1px "+;
	" ;background-color: "+cCor+""+;
	" ;border-radius: 6px"+;
	" ;font-weight: bold;font-size: 12px"+;
	" ;color: rgb(255,255,255) "+;
	"}"+;
	"QPushButton:hover { "+;
	" ;background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop:0 "+cCor+", stop:0.5 "+cCor+", stop:1 white)"+;
	"}"
Return cRet


User Function SASP051B
	
	FWExecView("Medi��o", "SASP038", MODEL_OPERATION_VIEW ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )

Return

User Function YUSERLIBFAT()
	Local cRet
	if ZZ2->ZZ2_ETAPA == "40"
		cRet := POSICIONE("ZZR",1,ZZ2->ZZ2_FILIAL+ZZ2->ZZ2_NUMERO+ZZ2->ZZ2_MEDICA+ZZ2->ZZ2_ETAPA,"ZZR_NOMUSE")
	endif	
Return cRet