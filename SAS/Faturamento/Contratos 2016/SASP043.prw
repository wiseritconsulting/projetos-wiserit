#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"


User Function SASP043()

	Local oBrowse	
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('ZZO')
	oBrowse:SetDescription('Cadastro de S�rie')
	oBrowse:DisableDetails()

	oBrowse:SetMenuDef( 'SASP043' ) 
	oBrowse:Activate()

Return

Static Function MenuDef()

	Local aRotina := {}	
	
	ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PesqBrw"			OPERATION 1 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.SASP043"	OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.SASP043"	OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.SASP043"	OPERATION 4 ACCESS 143
	ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.SASP043"	OPERATION 5 ACCESS 144
	ADD OPTION aRotina TITLE "Imprimir"   ACTION "VIEWDEF.SASP043"	OPERATION 8 ACCESS 0
	
Return aRotina

Static Function ModelDef()
	
	Local oStructZZO := Nil
	Local oModel := ""
	
	oStructZZO := FWFormStruct(1,"ZZO")
	
	oModel:= MPFormModel():New("YCADZZO",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
	
	oModel:AddFields("ZZOMASTER",/*cOwner*/, oStructZZO ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)
	
	oModel:SetPrimaryKey({"ZZO_COD"})	
	
Return (oModel)

Static Function ViewDef()

	Local oStructZZO	:= FWFormStruct( 2, 'ZZO' )	
	Local oModel		:= FWLoadModel( 'SASP043' )
	
	Local oView
		
	oView	:= FWFormView():New()

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField( "ZZOMASTER",oStructZZO)
	
Return oView
