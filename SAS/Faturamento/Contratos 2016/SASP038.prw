#include 'parmtype.ch'
#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"

/*/{Protheus.doc} SASP038
Altera��o da Medi��o
@author Jo�o Filho / Diogo
@since 16/09/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User Function SASP038()

	oBrowse:= FWmBrowse():New()
	oBrowse:SetAlias('ZZ2')
	oBrowse:SetDescription('Medi��o')

	oBrowse:AddLegend( "!Empty(ZZ2_PV01NF) ", "RED", "Medi��o Faturada"  )
	oBrowse:AddLegend( "Empty(ZZ2_PV01NF) ", "GREEN", "Medi��o N�o Faturada"  )

	oBrowse:SETMENUDEF('SASP038')
	oBrowse:Activate()

Return

Static Function MenuDef()
	Local aRotina := {}
	Public aKit := {}

	// MENU

	ADD OPTION aRotina Title 'Pesquisa'      Action 'PesqBrw'          OPERATION 1 ACCESS 0
	ADD OPTION aRotina Title 'Visualizar'    Action 'VIEWDEF.SASP038'  OPERATION 2 ACCESS 0
	ADD OPTION aRotina Title 'Alterar'       Action 'VIEWDEF.SASP038'  OPERATION 4 ACCESS 0
	ADD OPTION aRotina Title 'Excluir'       Action 'VIEWDEF.SASP038'  OPERATION 5 ACCESS 0
	ADD OPTION aRotina Title 'Imprimir OS cega'       Action 'U_SASR096()'  OPERATION 5 ACCESS 0

Return aRotina

Static Function ModelDef()

	Local oStructZZ2 := FWFORMSTRUCT(1,'ZZ2')
	Local oStructZZ3 := FWFORMSTRUCT(1,'ZZ3')
	Local oModel     := MPFormModel():New('SASP038M')
	Public nSaldoZZ3 := 0


	oStructZZ3:AddTrigger( ;
	"ZZ3_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ3_TES"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_FRetTES38() }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructZZ3:AddTrigger( ;
	"ZZ3_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ3_VLUNIT"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_FRetVUNT38() }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructZZ3:AddTrigger( ;
	"ZZ3_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ3_VTOTAL"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_FRetVTOT38() }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructZZ3:AddTrigger( ;
	"ZZ3_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ3_DESCRI"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_FRetDsc38(@oModel) }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ3:AddTrigger( ;
	"ZZ3_QUANT"		,;										//[01] Id do campo de origem
	"ZZ3_SALDO"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_RETSLD38(@oModel) }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructZZ3:AddTrigger( ;
	"ZZ3_QUANT"		,;										//[01] Id do campo de origem
	"ZZ3_VTOTAL"	,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ3_QUANT") * oModel:GetValue("ZZ3_VLUNIT") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ3:AddField( ;                      // Ord. Tipo Desc.
	'Pode Deletar';								// [01]  C   Titulo do campo
	,'linha pode ser deletada';					// [02]  C   ToolTip do campo
	,'CANDELETE';								// [03]  C   Id do Field
	,'L';										// [04]  C   Tipo do campo
	,1;											// [05]  N   Tamanho do campo
	,0;											// [06]  N   Decimal do campo
	,NIL;										// [07]  B   Code-block de valida��o do campo
	,NIl;										// [08]  B   Code-block de valida��o When do campo
	,NIL;										// [09]  A   Lista de valores permitido do campo
	,NIL;										// [10]  L   Indica se o campo tem preenchimento obrigat�rio
	,{||.T.};									// [11]  B   Code-block de inicializacao do campo
	,NIL;										// [12]  L   Indica se trata-se de um campo chave
	,NIL;										// [13]  L   Indica se o campo pode receber valor em uma opera��o de update.
	,.T.;										// [14]  L   Indica se o campo � virtual
	)


	oStructZZ3:SetProperty("ZZ3_PRODUT",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| LINHAZZ3(oModelGrid,cId,xValor,oModel) })
	oStructZZ3:SetProperty("ZZ3_QUANT",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| QUANTZZ3(oModelGrid,cId,xValor) })

	oModel:AddFields('ZZ2MASTER',/*pai*/,oStructZZ2)
	oModel:AddGrid("ZZ3DETAIL", "ZZ2MASTER"/*cOwner*/,oStructZZ3, ,/*bLinePost*/,/*bPre*/,/*bPost*/,/*Carga*/)
	//

	oModel:SetRelation( 'ZZ3DETAIL', { { 'ZZ3_FILIAL', 'xFilial( "ZZ3" )' }, { 'ZZ3_NUMERO', 'ZZ2_NUMERO' },{ 'ZZ3_MEDICA', 'ZZ2_MEDICA' },{ 'ZZ3_CLIENT', 'ZZ2_CLIENT' },{ 'ZZ3_LOJA', 'ZZ2_LOJA' } }, ZZ3->( IndexKey( 1 ) ) )

	// ADD RODAPE , ONDE S�O INFORMADOS OS TOTALIZADORES
	oModel:AddCalc( 'YCADZZ2CALC1', 'ZZ2MASTER', 'ZZ3DETAIL', 'ZZ3_QUANT', 'ZZ3__TOT01', 'SUM',,,'Total Itens',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"QTDMED") }*/ )
	oModel:AddCalc( 'YCADZZ2CALC1', 'ZZ2MASTER', 'ZZ3DETAIL', 'ZZ3_SALDO', 'ZZ3__TOT02', 'SUM',,,'Saldo dos Itens',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)
	oModel:AddCalc( 'YCADZZ2CALC1', 'ZZ2MASTER', 'ZZ3DETAIL', 'ZZ3_VTOTAL', 'ZZ3__TOT03', 'SUM',,,'Valor Total(R$)',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)

	oModel:SetPrimaryKey({'ZZ2_FILIAL','ZZ2_NUMERO','ZZ2_MEDICA','ZZ2_CLIENT','ZZ2_LOJA'})

	oModel:GetModel("ZZ2MASTER" ):GetStruct():SetProperty("*",MODEL_FIELD_WHEN,{||.F.})
	oModel:GetModel("ZZ2MASTER" ):GetStruct():SetProperty("ZZ2_DTPREV",MODEL_FIELD_WHEN,{||.T.})
	oModel:GetModel("ZZ2MASTER" ):GetStruct():SetProperty("ZZ2_FATEXP",MODEL_FIELD_WHEN,{||.T.})
	oModel:GetModel("ZZ2MASTER" ):GetStruct():SetProperty("ZZ2_DTEXP",MODEL_FIELD_WHEN,{||.T.})
	oModel:GetModel("ZZ2MASTER" ):GetStruct():SetProperty("ZZ2_LOG",MODEL_FIELD_WHEN,{||.T.})
	oModel:SetOnlyQuery('ZZ2MASTER',.T.)


	oModel:GetModel( 'ZZ3DETAIL' ):SetUniqueLine( { 'ZZ3_PRODUT','ZZ3_TES' } )

	oModel:SetVldActive( { |oModel| ZZ2ValAct( oModel ) } )

	oModel:SetActivate({|oModel| LoadZZ3(oModel) })
	oModel:SetCommit({|oModel| ZZ3Commit(oModel) },.F.)


Return (oModel)

Static Function ViewDef()

	Local oModel    := FWLoadModel('SASP038')
	Local oStructZZ2:= FWFormStruct(2,'ZZ2')
	Local oStructZZ3:= FWFormStruct(2,'ZZ3')
	Local oView:=FWFormView():New()

	oCalc1	:= FWCalcStruct( oModel:GetModel( 'YCADZZ2CALC1') )

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField('ZZ2MASTER',oStructZZ2)
	oView:AddGrid('ZZ3DETAIL',oStructZZ3)
	oView:AddField( 'VIEW_CALC', oCalc1, 'YCADZZ2CALC1' )

	oView:CreateHorizontalBox('SUPERIOR',40)
	oView:CreateHorizontalBox('INFERIOR',50)
	oView:CreateHorizontalBox('RODAPE',10)

	oView:SetOwnerView('ZZ2MASTER','SUPERIOR')
	oView:SetOwnerView('ZZ3DETAIL','INFERIOR')
	oView:SetOwnerView( 'VIEW_CALC', 'RODAPE' )

	oView:EnableTitleView('ZZ3DETAIL','Itens Medi��o')

	oView:SetProgressBar(.T.)
	oView:AddIncrementField("ZZ3DETAIL","ZZ3_ITEM")

	oView:AddUserButton( 'Procurar Kit', 'CLIPS', { |oView| ProcKit(oView) }, , VK_F4,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} )

	oView:AddUserButton( 'Quantidade Item', 'CLIPS', { |oView| YSASPO36Ln(oView,oModel) }, , VK_F11,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} )
	oView:AddUserButton( 'Visualizar Kit', 'CLIPS', { |oView| VISKit(oView) }, , VK_F7,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE,MODEL_OPERATION_VIEW} )
	
	oView:AddUserButton( 'Atualizar Peso Produtos', 'CLIPS', { |oView| AtualizarP(oModel) },,)
	
	oView:AddUserButton( 'Pedido 01', 'CLIPS', { |oView| YPED01(oModel) },,)
	oView:AddUserButton( 'Pedido 02', 'CLIPS', { |oView| YPED02(oModel) },,)
	oView:AddUserButton( 'Pedido 03', 'CLIPS', { |oView| YPED03(oModel) },,)

Return oView

STATIC FUNCTION LoadZZ3(oModel)


	cQuery:= " SELECT * "
	cQuery+= " FROM  "+RetSqlName( 'ZZ3' ) + " ZZ3 "
	cQuery+= " WHERE ZZ3_FILIAL = '"+xfilial('ZZ3')+"' "
	cQuery+= " AND ZZ3.D_E_L_E_T_='' "
	cQuery+= " AND ZZ3_FILIAL ='"+oModel:GETMODEL('ZZ3DETAIL'):GetValue('ZZ3_FILIAL')+"' "
	cQuery+= " AND ZZ3_NUMERO ='"+oModel:GETMODEL('ZZ3DETAIL'):GetValue('ZZ3_NUMERO')+"' "
	cQuery+= " AND ZZ3_CLIENT ='"+oModel:GETMODEL('ZZ3DETAIL'):GetValue('ZZ3_CLIENT')+"' "
	cQuery+= " AND ZZ3_LOJA ='"+oModel:GETMODEL('ZZ3DETAIL'):GetValue('ZZ3_LOJA')+"' "
	cQuery+= " AND ZZ3_MEDICA ='"+oModel:GETMODEL('ZZ3DETAIL'):GetValue('ZZ3_MEDICA')+"' "
	TCQuery cquery new alias T10

	if oModel:GetOperation() == 4
		WHILE !T10->(EOF())
			oModel:GetModel('ZZ3DETAIL'):SetValue('CANDELETE',.F.)
			T10->(dbSkip())
		ENDDO
	ENDIF
	
	T10->(DBCLOSEAREA())
RETURN .T.

Static Function ZZ3Commit(oModel)

	Local lRet          := .T.
	Local cContrato	    := oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_NUMERO')
	Local cMedic		:= oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_MEDICA')
	Local cCNPJ		    := FWArrFilAtu()[18] //Coligada
	Local nQtd			:= 0
	Local nCont,nCont2
	Local cMedicao
	Local aCab			:= {}
	Local aItens		:= {}
	Local aItens2		:= {}
	Local nQtdMed		:= 0
	Local nTam
	Local aStruct	    := oModel:GetModel( 'ZZ3DETAIL' ):oFormModelStruct:GetFields()
	Local oModelZZ2	    := oModel:GetModel('ZZ2MASTER')
	Local oModelZZ3	    := oModel:GetModel('ZZ3DETAIL')
	Local cTipoPrincipal		:= SuperGetMV("SA_TIPOMED")
	Local cTipoComplementar     := SuperGetMV("SA_TPCOMPL")
	Local aCliente	    := {}
	Local nContIT		:= 0
	Local nVlrItem	    := 0
	Local nVlrItem2     := 0
	Local nVlrTotal	    := 0
	Local nVlrAdiant	:= 0
	Local lKit
	Local cTabPad		:= SuperGetMV("SA_TABPREC")
	Local cTab02        := SuperGetMV("SA_TABPR2")
	Local nj
	Local oView		    := FWViewActive()
	Local aValVenda     := {}
	Local nValVenda     := 1
	Local cQuery,cquery1
	Local cCliCol,cLjCol,cNaturez
	Local cFilHonld
	Private cTesTmp		:= "900"	//Tes temporaria para pedido
	Private nFret		:= 0
	Private nDesc		:= 0
	Private lMsErroAuto	:= .F.


	cFilHonld := oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV02FL')

	IF cFilHonld == "020101"
		cTabPad := cTab02
	ENDIF

	if oModel:GetOperation() == 4
		// verificar se ja existe pedido faturado para essa medi��o , caso exista impede a altera��o!
		cQuery:= " SELECT C5_NUM "
		cQuery+= " FROM "+RetSqlName("SC5")+" SC5 "
		cQuery+= " WHERE C5_YCONTRA = '"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_NUMERO')+"' "
		cQuery+= " AND SC5.D_E_L_E_T_='' "
		cQuery+= " AND C5_YMEDICA ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_MEDICA')+"' "
		cQuery+= " AND ("
		cQuery+= " C5_FILIAL ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV01FL')+"' "
		cQuery+= " AND C5_NUM ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV01')+"' "
		cQuery+= " AND C5_NOTA <> '' "
		cQuery+= " )"
		cQuery+= " OR ("
		cQuery+= " C5_FILIAL ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV02FL')+"' "
		cQuery+= " AND C5_NUM ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV02')+"' "
		cQuery+= " AND C5_NOTA <> '' "
		cQuery+= " )"
		cQuery+= " OR ("
		cQuery+= " C5_FILIAL ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV03FL')+"' "
		cQuery+= " AND C5_NUM ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV03')+"' "
		cQuery+= " AND C5_NOTA <> '' "
		cQuery+= " )"

		TCQuery cquery new alias T01

		IF !T01->(EOF())
			oModel:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", 'Pedido j� faturado, impossibitando altera��o da medi��o', "Verificar nota de n�mero: '"+T01->C5_NOTA+"'!")
			T01->(DbCloseArea())
			Return .F.
		ENDIF
		T01->(DbCloseArea())

		SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
		If SA1->(DbSeek(xFilial("SA1")+cCNPJ))
			cCliCol	:= SA1->A1_COD
			cLjCol	:= SA1->A1_LOJA
		EndIf

		SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
		ZZ2->(DbSetOrder(1))	//ZZ2_FILIAL+ZZ2_NUMERO
		ZZ3->(DbSetOrder(1))	//ZZ3_FILIAL+ZZ3_CONTRAT+ZZ3_ITEM
		SA1->(dbSetOrder(1))	//A1_FILIAL+A1_COD
		SB1->(DbSetOrder(1))	//B1_FILIAL+B1_COD

		cNaturez:= oModelZZ2:GetValue('ZZ2_NATMED')

		Begin Transaction

			For nCont := 1 To oModelZZ3:Length()
				oModelZZ3:GoLine(nCont)

				If oModelZZ3:GetValue("ZZ3_QUANT") == 0
					Loop
				EndIf

				If ! oModelZZ3:IsDeleted()
					If SF4->(DbSeek(xFilial("SF4")+oModelZZ3:GetValue("ZZ3_TES"))) .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
						nVlrAdiant	+= ((oModelZZ3:GetValue('ZZ3_VLUNIT')*oModelZZ3:GetValue('ZZ3_QUANT')) )
						if alltrim(oModelZZ3:GetValue('ZZ3_TIPO')) == 'K'
							nVlrAdiant -= DESCFRET(oModel)
						endif
					EndIf
				EndIf

				//VERIFICA O TIPO DE ITEM A SER MEDIDO SE � KIT OU PRODUTO   (K= KIT / P= PRODUTO)
				If alltrim(oModelZZ3:GetValue('ZZ3_TIPO')) == 'K' //lKit

					cquery1 := " select ZZ7_PRODUT,ZZ7_QUANT,ZZ7_PERDSC ,ZZ7_CATEGO ,ZZ7_SUPLEM "
					cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 "
					cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
					cquery1 += " and D_E_L_E_T_ <> '*' "
					cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
					cquery1 += " and ZZ7_TIPO = 'A' "
					cquery1 += " ORDER BY ZZ7_PRODUT"
					TCQuery cquery1 new alias T03

					aValVenda := ValVenda(oModel)

					While T03->(!EOF())

						//nValVenda ++
						nContIT++
						nVlrItem2	:= MaTabPrVen(cTabPad,T03->ZZ7_PRODUT,1,oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT'),;
						oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJA'))

						nVlrTotal	+= nVlrItem*T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT')

						AADD(aItens,{})
						nTam	:= Len(aItens)
						AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
						AADD(aItens[nTam],{"C6_PRODUTO"	,T03->ZZ7_PRODUT											,nil})
						AADD(aItens[nTam],{"C6_QTDVEN"	,T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT')				,nil})

						IF T03->ZZ7_PERDSC == 0 .AND. T03->ZZ7_CATEGO $ 'A | C | D | V ' .AND. T03->ZZ7_SUPLEM == 'N' 
							AADD(aItens[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1] 									,nil}) //nVlrItem
						ELSEIF T03->ZZ7_CATEGO='P' 
							AADD(aItens[nTam],{"C6_PRCVEN"	,GETMV('SA_PRECDSC') 								    ,nil}) //nVlrItem
						ELSEIF T03->ZZ7_PERDSC == 100 .AND. T03->ZZ7_CATEGO<>'P'
							AADD(aItens[nTam],{"C6_PRCVEN"	,GETMV('SA_PRECDSC') 								        ,nil}) //nVlrItem
							If ! oModelZZ3:IsDeleted()
								nDesc +=(( GETMV('SA_PRECDSC')*T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT'))*(T03->ZZ7_PERDSC/100))
							ENDIF
						ELSEIF T03->ZZ7_PERDSC == 0

							IF nVlrItem2 <> 0
								AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2             			,nil}) // AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1] 									,nil}) //nVlrItem
							ELSE
								AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1]		,nil})
								//nValVenda ++
							ENDIF

						ENDIF
						_fil:= oModelZZ2:GetValue('ZZ2_FILIAL')
						_CLI:= oModelZZ2:GetValue('ZZ2_CLIENT')
						_LOJA:= oModelZZ2:GetValue('ZZ2_LOJA')
						_TPLVR:= POSICIONE("ZZ0",1,_fil+cContrato+_CLI+_LOJA,"ZZ0_TIPCTR")

						AADD(aItens[nTam],{"C6_YTES"	,oModelZZ3:GetValue('ZZ3_TES')	                        	,nil})
						AADD(aItens[nTam],{"C6_TES"		,cTesTmp	                        	,nil})
						AADD(aItens[nTam],{"C6_YCONTRA"	,cContrato													,nil})
						AADD(aItens[nTam],{"C6_YMEDICA"	,cMedic														,nil})
						AADD(aItens[nTam],{"C6_YITCONT"	,oModelZZ3:GetValue('ZZ3_ITEM')								,nil})
						AADD(aItens[nTam],{"C6_YCODKIT" ,ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT'))						,nil})
						AADD(aItens[nTam],{"C6_YPAIKIT" ,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')),"ZZ7_CODPAI"))					,nil})
						AADD(aItens[nTam],{"C6_LOCAL"	,IIF(_TPLVR == "L",POSICIONE("SB1",1,XFILIAL("SB1")+T03->ZZ7_PRODUT,"B1_LOCPAD"),GETMV('SA_LOCFAT'))		,nil})
						If oModelZZ3:IsDeleted()
							aadd(aItens[nTam],{"AUTDELETA","S",Nil})
						ELSE
							aadd(aItens[nTam],{"AUTDELETA","N",Nil})
						EndIf


						AADD(aItens2,{})
						nTam	:= Len(aItens2)
						AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.),nil})
						AADD(aItens2[nTam],{"C6_PRODUTO",T03->ZZ7_PRODUT										,nil})
						AADD(aItens2[nTam],{"C6_QTDVEN"	,T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT')			,nil})
						//						AADD(aItens2[nTam],{"C6_QTDLIB"	,T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT')			,nil})

						IF T03->ZZ7_PERDSC == 0 .AND. T03->ZZ7_CATEGO $ 'A | C | D | V ' .AND. T03->ZZ7_SUPLEM == 'N' //T03->ZZ7_PERDSC == 0
							IF nVlrItem2 <> 0
								AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2         								,nil})
								nValVenda ++
							ELSE
								AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1]						,nil})
								nValVenda ++
							ENDIF
						ELSEIF T03->ZZ7_PERDSC == 100
							AADD(aItens2[nTam],{"C6_PRCVEN"	,GETMV('SA_PRECDSC') 								        ,nil}) //nVlrItem
						ELSEIF T03->ZZ7_PERDSC == 0
							IF nVlrItem2 <> 0
								AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2             			,nil}) // AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1] 									,nil}) //nVlrItem
							ELSE
								AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1]		,nil})
								//nValVenda ++
							ENDIF
						ENDIF

						AADD(aItens2[nTam],{"C6_YTES",oModelZZ3:GetValue('ZZ3_TES')	                	,nil})
						AADD(aItens2[nTam],{"C6_TES"	,cTesTmp	                	,nil})
						AADD(aItens2[nTam],{"C6_YCONTRA",cContrato												,nil})
						AADD(aItens2[nTam],{"C6_YMEDICA",cMedic													,nil})
						AADD(aItens2[nTam],{"C6_YITCONT",oModelZZ3:GetValue('ZZ3_ITEM')							,nil})
						AADD(aItens2[nTam],{"C6_YCODKIT",ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT'))	  				,nil})//oModelZZ3:GetValue('000000000000003')
						AADD(aItens2[nTam],{"C6_YPAIKIT",Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')),"ZZ7_CODPAI"))					,nil})
						AADD(aItens2[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT')		,nil})
						If oModelZZ3:IsDeleted()
							aadd(aItens2[nTam],{"AUTDELETA","S",Nil})
						ELSE
							aadd(aItens2[nTam],{"AUTDELETA","N",Nil})
						EndIf


						T03->(DbSkip())
					EndDo
					nValVenda := 1
					T03->(DbCloseArea())
				Else
					nContIT++
					nVlrItem2	:= MaTabPrVen(cTabPad,oModelZZ3:GetValue('ZZ3_PRODUT'),1,oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT'),oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJA'))
					AADD(aItens,{})
					nTam	:= Len(aItens)
					nVlrTotal	+= oModelZZ3:GetValue('ZZ3_VLUNIT')*oModelZZ3:GetValue('ZZ3_QUANT')

					//If SF4->(DbSeek(xFilial("SF4")+oModelZZ3:GetValue('ZZ3_TES'))) .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
					//	nVlrAdiant	+= oModelZZ3:GetValue('ZZ3_VLUNIT')*oModelZZ3:GetValue('ZZ3_QUANT')
					//EndIf

					AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
					AADD(aItens[nTam],{"C6_PRODUTO"	,oModelZZ3:GetValue('ZZ3_PRODUT')	,nil})
					AADD(aItens[nTam],{"C6_QTDVEN"	,oModelZZ3:GetValue('ZZ3_QUANT')	,nil})
					//					AADD(aItens[nTam],{"C6_QTDLIB"	,oModelZZ3:GetValue('ZZ3_QUANT')	,nil})
					AADD(aItens[nTam],{"C6_PRCVEN"	,oModelZZ3:GetValue('ZZ3_VLUNIT')	,nil})
					AADD(aItens[nTam],{"C6_YTES"	,oModelZZ3:GetValue('ZZ3_TES')		,nil})
					AADD(aItens[nTam],{"C6_TES"		,cTesTmp							,nil})
					AADD(aItens[nTam],{"C6_YITCONT"	,oModelZZ3:GetValue('ZZ3_ITEM')		,nil})
					AADD(aItens[nTam],{"C6_YCONTRA"	,cContrato							,nil})
					AADD(aItens[nTam],{"C6_YMEDICA"	,cMedic								,nil})
					If oModelZZ3:IsDeleted()
						aadd(aItens[nTam],{"AUTDELETA","S",Nil})
					ELSE
						aadd(aItens[nTam],{"AUTDELETA","N",Nil})
					EndIf

					AADD(aItens2,{})
					nTam	:= Len(aItens2)
					AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
					AADD(aItens2[nTam],{"C6_PRODUTO"	,oModelZZ3:GetValue('ZZ3_PRODUT')	                    ,nil})
					AADD(aItens2[nTam],{"C6_QTDVEN"	,oModelZZ3:GetValue('ZZ3_QUANT')							,nil})
					//					AADD(aItens2[nTam],{"C6_QTDLIB"	,oModelZZ3:GetValue('ZZ3_QUANT')							,nil})
					IF nVlrItem2 <> 0
						AADD(aItens2[nTam],{"C6_PRCVEN"	    ,nVlrItem2 											,nil})
					ELSE
						AADD(aItens2[nTam],{"C6_PRCVEN"	    ,oModelZZ3:GetValue('ZZ3_VLUNIT')					,nil})
					ENDIF
					AADD(aItens2[nTam],{"C6_YTES"	,oModelZZ3:GetValue('ZZ3_TES')							,nil})
					AADD(aItens2[nTam],{"C6_TES"		,cTesTmp												,nil})
					AADD(aItens2[nTam],{"C6_YITCONT"	,oModelZZ3:GetValue('ZZ3_ITEM')							,nil})
					AADD(aItens2[nTam],{"C6_YCONTRA"	,cContrato												,nil})
					AADD(aItens2[nTam],{"C6_YMEDICA"	,cMedic													,nil})
					AADD(aItens2[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT')		,nil})
					If oModelZZ3:IsDeleted()
						aadd(aItens2[nTam],{"AUTDELETA","S",Nil})
					ELSE
						aadd(aItens2[nTam],{"AUTDELETA","N",Nil})
					EndIf

				EndIf
				/*
				RecLock("ZZ1",.F.)
				ZZ1->ZZ1_SALDO	-= oModelZZ3:GetValue('ZZ3_QUANT')
				nQtdMed			+= oModelZZ3:GetValue('ZZ3_QUANT')
				ZZ1->(MsUnLock())
				*/
			Next

			nFret	:= 0
			AADD(aCab,{"C5_TIPO"		,"N"													,Nil})
			AADD(aCab,{"C5_CLIENTE"		,oModelZZ2:GetValue('ZZ2_CLIENT')						,Nil})
			AADD(aCab,{"C5_LOJACLI"		,oModelZZ2:GetValue('ZZ2_LOJA')							,Nil})
			AADD(aCab,{"C5_CONDPAG"		,POSICIONE("ZZ0",1,XFILIAL("ZZ0")+oModelZZ2:GetValue('ZZ2_NUMERO')+oModelZZ2:GetValue('ZZ2_CLIENT')+oModelZZ2:GetValue('ZZ2_LOJA'),"ZZ0_CPFIN")		,Nil}) //oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CPFIN')
			AADD(aCab,{"C5_NATUREZ"		,cNaturez												,Nil})
			AADD(aCab,{"C5_YTPVEN"		,oModelZZ2:GetValue('ZZ2_TIPO')							,Nil})
			AADD(aCab,{"C5_TPEDAV"		,"39"													,Nil})
			AADD(aCab,{"C5_YCONTRA"		,cContrato 												,Nil})
			AADD(aCab,{"C5_YMEDICA"		,cMedic   												,Nil})
			AADD(aCab,{"C5_TPFRETE"		,"C"   												    ,Nil})

			if ! POSICIONE("ZZG",1,XFILIAL("ZZG") + oModelZZ2:GetValue('ZZ2_TIPO'),"ZZG_ESPCTR") $ cTipoPrincipal .AND. POSICIONE("ZZG",1,XFILIAL("ZZG") + oModelZZ2:GetValue('ZZ2_TIPO'),"ZZG_ESPCTR") $ cTipoComplementar
				nFret := nVlrAdiant*( POSICIONE("ZZ0",1,oModelZZ2:GetValue('ZZ2_FILIAL')+ oModelZZ2:GetValue('ZZ2_NUMERO') + oModelZZ2:GetValue('ZZ2_CLIENT') + oModelZZ2:GetValue('ZZ2_LOJA') , "ZZ0_FRTEMP"  ) / 100)
			elseif  POSICIONE("ZZG",1,XFILIAL("ZZG") + oModelZZ2:GetValue('ZZ2_TIPO'),"ZZG_ESPCTR") $ cTipoPrincipal .AND. ! POSICIONE("ZZG",1,XFILIAL("ZZG") + oModelZZ2:GetValue('ZZ2_TIPO'),"ZZG_ESPCTR") $ cTipoComplementar
				nFret := nVlrAdiant*( POSICIONE("ZZ0",1,oModelZZ2:GetValue('ZZ2_FILIAL')+ oModelZZ2:GetValue('ZZ2_NUMERO') + oModelZZ2:GetValue('ZZ2_CLIENT') + oModelZZ2:GetValue('ZZ2_LOJA') , "ZZ0_FRTCLI"  ) / 100)
			endif

			/*
			If POSICIONE("ZZ0",1,XFILIAL("ZZ0")+oModelZZ2:GetValue('ZZ2_NUMERO')+oModelZZ2:GetValue('ZZ2_CLIENT')+oModelZZ2:GetValue('ZZ2_LOJA'),"ZZ0_FRTCLI") <> 0 //=="1"
			nFret:= nVlrAdiant*POSICIONE("ZZ0",1,XFILIAL("ZZ0")+oModelZZ2:GetValue('ZZ2_NUMERO')+oModelZZ2:GetValue('ZZ2_CLIENT')+oModelZZ2:GetValue('ZZ2_LOJA'),"ZZ0_FRTCLI")/100
			EndIf
			*/
			aCliente	:= {oModelZZ2:GetValue('ZZ2_CLIENT'),oModelZZ2:GetValue('ZZ2_LOJA'),POSICIONE("ZZ0",1,XFILIAL("ZZ0")+oModelZZ2:GetValue('ZZ2_NUMERO')+oModelZZ2:GetValue('ZZ2_CLIENT')+oModelZZ2:GetValue('ZZ2_LOJA'),"ZZ0_NOME")}


			Processa({|| lRet := Pedido(aCab,aItens,aCliente,cContrato,cMedicao,nFret,aItens2,oModel,4,cFilHonld) },"Processando","Aguarde...",.F.)
			If !lRet
				DisarmTransaction()
				oModel:SetErrorMessage('ZZ3DETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
				Return .F.
			EndIf

			dbSelectArea("ZZ2")
			dbSetOrder(1)
			IF dbSeek(XFILIAL("ZZ2")+oModelZZ2:GETVALUE("ZZ2_NUMERO")+oModelZZ2:GETVALUE("ZZ2_MEDICA")+oModelZZ2:GETVALUE("ZZ2_CLIENT")+oModelZZ2:GETVALUE("ZZ2_LOJA"),.T.)

				RECLOCK("ZZ2", .F.)
				//ZZ2->ZZ2_DTPREV	:= validTipo(alltrim(oModelZZ2:GETVALUE("ZZ2_TIPO")),oModel)
				ZZ2->ZZ2_DTMAX	:= validTipo(alltrim(oModelZZ2:GETVALUE("ZZ2_TIPO")),oModel)
				ZZ2->ZZ2_DTPREV	:= oModelZZ2:GETVALUE("ZZ2_DTPREV")
				ZZ2->ZZ2_FATEXP	:= oModelZZ2:GETVALUE("ZZ2_FATEXP")
				ZZ2->ZZ2_ETAPA  := IIF(oModelZZ2:GETVALUE("ZZ2_FATEXP") == "1",GETMV('SA_ETPFIM'),)
				ZZ2->ZZ2_DTALTE := DATE()
				ZZ2->ZZ2_HORAAL := TIME()
				ZZ2->ZZ2_NOMEAL := UsrFullName(RetCodUsr())
				ZZ2->ZZ2_USERAL := RetCodUsr()
				MSUNLOCK()

			ENDIF


			dbSelectArea("ZZR")
			dbSetOrder(1)      // ZZR_FILIAL+ZZR_CONTRA+ZZR_MEDICA+ZZRETAPA

			RECLOCK("ZZR", .T.)

			ZZR_FILIAL  := ZZ2->ZZ2_FILIAL
			ZZR_CONTRA  := ZZ2->ZZ2_NUMERO
			ZZR_MEDICA  := ZZ2->ZZ2_MEDICA
			ZZR_EMISSA  := ZZ2->ZZ2_DTGER
			ZZR_CODCLI  := ZZ2->ZZ2_CLIENT
			ZZR_NOMECL  := POSICIONE("SA1",1,xFilial("SA1")+ZZ2->ZZ2_CLIENT+ZZ2->ZZ2_LOJA,"A1_NOME")
			ZZR_CODUSE  := RetCodUsr()
			ZZR_NOMUSE  := UsrFullName(RetCodUsr())
			ZZR_ETAPA   := "01"
			ZZR_NOMETP  := POSICIONE("SX5",1,XFILIAL("SX5")+'ZA'+"01","X5_DESCRI")
			ZZR_DATA    := date()
			ZZR_HORA    := time()
			ZZR_OBS     := "Medi��o Alterada "

			MSUNLOCK()     // Destrava o registro



		End Transaction

		MsUnLockAll()

		/*

		*/

		For nCont := 1 To oModelZZ3:Length()
			oModelZZ3:GoLine(nCont)

			If oModelZZ3:IsDeleted()
				//Loop
				dbSelectArea("ZZ3")
				ZZ3->(dbSetOrder(1))
				If ZZ3->(dbSeek(oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_FILIAL')+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_NUMERO')+;
				oModelZZ2:GetValue('ZZ2_MEDICA')+oModelZZ2:GetValue('ZZ2_CLIENT')+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_LOJA')+;
				oModelZZ3:GetValue('ZZ3_PRODUT')+oModelZZ3:GetValue('ZZ3_TES') ))

					nDiff:= oModelZZ3:GetValue('ZZ3_QUANT')

					If nDiff <> 0

						dbSelectArea("ZZ1")
						ZZ1->(dbSetOrder(3))

						If ZZ1->(dbSeek(oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_FILIAL')+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_NUMERO')+;
						oModelZZ2:GetValue('ZZ2_CLIENT')+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_LOJA')+;
						oModelZZ3:GetValue('ZZ3_ITEM')+oModelZZ3:GetValue('ZZ3_PRODUT') ))

							//Atualiza saldo do item do contrato
							ZZ1->(RecLock("ZZ1",.F.))
							ZZ1->ZZ1_SALDO	:=  (ZZ1->ZZ1_SALDO + nDiff)
							ZZ1->(MsUnlock())

						Endif


					Endif
				endif

			EndIf

			dbSelectArea("ZZ3")
			ZZ3->(dbSetOrder(1))
			//ZZ3_FILIAL+ZZ3_NUMERO+ZZ3_MEDICA+ZZ3_CLIENT+ZZ3_LOJA+ZZ3_PRODUT+ZZ3_TES
			//verifica se existe o item na medi��o e verifica se terve mudan�a na quantidade medida
			If ZZ3->(dbSeek(oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_FILIAL')+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_NUMERO')+;
			oModelZZ2:GetValue('ZZ2_MEDICA')+oModelZZ2:GetValue('ZZ2_CLIENT')+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_LOJA')+;
			oModelZZ3:GetValue('ZZ3_PRODUT')+oModelZZ3:GetValue('ZZ3_TES') ))

				nDiff:= oModelZZ3:GetValue('ZZ3_QUANT') - ZZ3->ZZ3_QUANT

				If nDiff <> 0

					dbSelectArea("ZZ1")
					ZZ1->(dbSetOrder(3))

					If ZZ1->(dbSeek(oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_FILIAL')+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_NUMERO')+;
					oModelZZ2:GetValue('ZZ2_CLIENT')+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_LOJA')+;
					oModelZZ3:GetValue('ZZ3_ITEM')+oModelZZ3:GetValue('ZZ3_PRODUT')))

						//Atualiza saldo do item do contrato
						ZZ1->(RecLock("ZZ1",.F.))
						ZZ1->ZZ1_SALDO	:=  (ZZ1->ZZ1_SALDO - nDiff)
						ZZ1->(MsUnlock())

						oModel:GetModel('ZZ3DETAIL'):SetValue('ZZ3_SALDO',ZZ1->ZZ1_SALDO)

					Endif


				Endif
			ELSE //quando for um item novo, somente
				nDiff:= oModelZZ3:GetValue('ZZ3_QUANT')
				If nDiff <> 0
					dbSelectArea("ZZ1")
					ZZ1->(dbSetOrder(3))

					If ZZ1->(dbSeek(oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_FILIAL')+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_NUMERO')+;
					oModelZZ2:GetValue('ZZ2_CLIENT')+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_LOJA')+;
					oModelZZ3:GetValue('ZZ3_ITEM')+oModelZZ3:GetValue('ZZ3_PRODUT')))

						//Atualiza saldo do item do contrato
						ZZ1->(RecLock("ZZ1",.F.))
						ZZ1->ZZ1_SALDO	:=  (ZZ1->ZZ1_SALDO - nDiff)
						ZZ1->(MsUnlock())

						oModel:GetModel('ZZ3DETAIL'):SetValue('ZZ3_SALDO',ZZ1->ZZ1_SALDO)

					Endif
				Endif
			Endif

		Next

		FWFormCommit(oModel)

		IF FUNNAME() == "SASP038"
			MsgInfo("Ajustado a medi��o "+oModelZZ2:GetValue('ZZ2_MEDICA')+;
			" para o contrato "+oModelZZ3:GetValue('ZZ3_NUMERO'))
		ENDIF

		Return lRet
	elseif oModel:GetOperation() == 5

		// verificar se ja existe pedido de venda faturado para essa medi��o , caso exita impede a altera��o!
		cQuery:= " SELECT C5_NUM "
		cQuery+= " FROM "+RetSqlName("SC5")+" SC5 "
		cQuery+= " WHERE C5_YCONTRA = '"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_NUMERO')+"' "
		cQuery+= " AND SC5.D_E_L_E_T_='' "
		cQuery+= " AND C5_YMEDICA ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_MEDICA')+"' "
		cQuery+= " AND ("
		cQuery+= " C5_FILIAL ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV01FL')+"' "
		cQuery+= " AND C5_NUM ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV01')+"' "
		cQuery+= " AND C5_NOTA <> '' "
		cQuery+= " )"
		cQuery+= " OR ("
		cQuery+= " C5_FILIAL ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV02FL')+"' "
		cQuery+= " AND C5_NUM ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV02')+"' "
		cQuery+= " AND C5_NOTA <> '' "
		cQuery+= " )"
		cQuery+= " OR ("
		cQuery+= " C5_FILIAL ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV03FL')+"' "
		cQuery+= " AND C5_NUM ='"+oModel:GetModel( 'ZZ2MASTER' ):GetValue('ZZ2_PV03')+"' "
		cQuery+= " AND C5_NOTA <> '' "
		cQuery+= " )"

		TCQuery cquery new alias T01

		IF !T01->(EOF())
			oModel:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", 'Pedido j� faturado, impossibitando exclus�o da medi��o', "" )
			T01->(DbCloseArea())
			Return .F.
		ENDIF
		T01->(DbCloseArea())

		SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
		If SA1->(DbSeek(xFilial("SA1")+cCNPJ))
			cCliCol	:= SA1->A1_COD
			cLjCol	:= SA1->A1_LOJA
		EndIf

		SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
		ZZ2->(DbSetOrder(1))	//ZZ2_FILIAL+ZZ2_NUMERO
		ZZ3->(DbSetOrder(1))	//ZZ3_FILIAL+ZZ3_CONTRAT+ZZ3_ITEM
		SA1->(dbSetOrder(1))	//A1_FILIAL+A1_COD
		SB1->(DbSetOrder(1))	//B1_FILIAL+B1_COD

		cNaturez:= oModelZZ2:GetValue('ZZ2_NATMED')

		Begin Transaction

			For nCont := 1 To oModelZZ3:Length()
				oModelZZ3:GoLine(nCont)
				If oModelZZ3:IsDeleted()
					Loop
				EndIf
				If oModelZZ3:GetValue("ZZ3_QUANT") == 0
					Loop
				EndIf

				//VERIFICA O TIPO DE ITEM A SER MEDIDO SE � KIT OU PRODUTO   (K= KIT / P= PRODUTO)
				If alltrim(oModelZZ3:GetValue('ZZ3_TIPO')) == 'K' //lKit

					cquery1 := " select ZZ7_PRODUT,ZZ7_QUANT,ZZ7_PERDSC "
					cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 "
					cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
					cquery1 += " and D_E_L_E_T_ <> '*' "
					cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
					cquery1 += " and ZZ7_TIPO = 'A' "
					cquery1 += " ORDER BY ZZ7_PRODUT"
					TCQuery cquery1 new alias T03

					//aValVenda := ValVenda(oModel)

					While T03->(!EOF())

						nContIT++
						AADD(aItens,{})
						nTam	:= Len(aItens)
						AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)   	,nil})
						AADD(aItens[nTam],{"C6_PRODUTO"	,T03->ZZ7_PRODUT											,nil})
						AADD(aItens[nTam],{"C6_QTDVEN"	,T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT')				,nil})
						AADD(aItens[nTam],{"C6_QTDLIB"	,0															,nil})
						AADD(aItens[nTam],{"C6_PRCVEN"	,oModelZZ3:GetValue('ZZ3_VLUNIT')							,nil}) //nVlrItem
						AADD(aItens[nTam],{"C6_YTES"	,oModelZZ3:GetValue('ZZ3_TES')	                        	,nil})
						AADD(aItens[nTam],{"C6_TES"		,cTesTmp						                        	,nil})
						AADD(aItens[nTam],{"C6_YCONTRA"	,cContrato													,nil})
						AADD(aItens[nTam],{"C6_YMEDICA"	,cMedic														,nil})
						AADD(aItens[nTam],{"C6_YITCONT"	,oModelZZ3:GetValue('ZZ3_ITEM')								,nil})
						AADD(aItens[nTam],{"C6_YCODKIT" ,ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT'))						,nil})
						AADD(aItens[nTam],{"C6_YPAIKIT" ,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')),"ZZ7_CODPAI"))					,nil})
						AADD(aItens[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT')		,nil})

						If oModelZZ3:IsDeleted()
							aadd(aItens[nTam],{"AUTDELETA","S",Nil})
						ELSE
							aadd(aItens[nTam],{"AUTDELETA","N",Nil})
						EndIf

						AADD(aItens2,{})
						nTam	:= Len(aItens2)
						AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.),nil})
						AADD(aItens2[nTam],{"C6_PRODUTO",T03->ZZ7_PRODUT										,nil})
						AADD(aItens2[nTam],{"C6_QTDVEN"	,T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT')			,nil})
						AADD(aItens2[nTam],{"C6_QTDLIB"	,0														,nil})
						AADD(aItens2[nTam],{"C6_PRCVEN"	,oModelZZ3:GetValue('ZZ3_VLUNIT')						,nil})
						AADD(aItens2[nTam],{"C6_YTES",oModelZZ3:GetValue('ZZ3_TES')	                		,nil})
						AADD(aItens2[nTam],{"C6_TES"	,cTesTmp	                		,nil})
						AADD(aItens2[nTam],{"C6_YCONTRA",cContrato												,nil})
						AADD(aItens2[nTam],{"C6_YMEDICA",cMedic													,nil})
						AADD(aItens2[nTam],{"C6_YITCONT",oModelZZ3:GetValue('ZZ3_ITEM')							,nil})
						AADD(aItens2[nTam],{"C6_YCODKIT",ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT'))	  			,nil})//oModelZZ3:GetValue('000000000000003')
						AADD(aItens2[nTam],{"C6_YPAIKIT",Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')),"ZZ7_CODPAI"))					,nil})
						AADD(aItens2[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT')		,nil})

						If oModelZZ3:IsDeleted()
							aadd(aItens2[nTam],{"AUTDELETA","S",Nil})
						ELSE
							aadd(aItens2[nTam],{"AUTDELETA","N",Nil})
						EndIf



						T03->(DbSkip())
					EndDo

					T03->(DbCloseArea())
				Else
					nContIT++
					AADD(aItens,{})
					nTam	:= Len(aItens)

					AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.),nil})
					AADD(aItens[nTam],{"C6_PRODUTO"	,oModelZZ3:GetValue('ZZ3_PRODUT')	,nil})
					AADD(aItens[nTam],{"C6_QTDVEN"	,oModelZZ3:GetValue('ZZ3_QUANT')	,nil})
					AADD(aItens[nTam],{"C6_QTDLIB"	,0									,nil})
					AADD(aItens[nTam],{"C6_PRCVEN"	,oModelZZ3:GetValue('ZZ3_VLUNIT')	,nil})
					AADD(aItens[nTam],{"C6_YTES"	,oModelZZ3:GetValue('ZZ3_TES')		,nil})
					AADD(aItens[nTam],{"C6_TES"		,cTesTmp							,nil})
					AADD(aItens[nTam],{"C6_YITCONT"	,oModelZZ3:GetValue('ZZ3_ITEM')		,nil})
					AADD(aItens[nTam],{"C6_YCONTRA"	,cContrato							,nil})
					AADD(aItens[nTam],{"C6_YMEDICA"	,cMedic								,nil})

					If oModelZZ3:IsDeleted()
						aadd(aItens[nTam],{"AUTDELETA","S",Nil})
					ELSE
						aadd(aItens[nTam],{"AUTDELETA","N",Nil})
					EndIf


					AADD(aItens2,{})
					nTam	:= Len(aItens2)
					AADD(aItens2[nTam],{"C6_ITEM"		,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)    ,nil})
					AADD(aItens2[nTam],{"C6_PRODUTO"	,oModelZZ3:GetValue('ZZ3_PRODUT')	                    ,nil})
					AADD(aItens2[nTam],{"C6_QTDVEN"		,oModelZZ3:GetValue('ZZ3_QUANT')							,nil})
					AADD(aItens2[nTam],{"C6_QTDLIB"		,0															,nil})
					AADD(aItens2[nTam],{"C6_PRCVEN"	    ,oModelZZ3:GetValue('ZZ3_VLUNIT')				     	,nil})
					AADD(aItens2[nTam],{"C6_YTES"	,oModelZZ3:GetValue('ZZ3_TES')							,nil})
					AADD(aItens2[nTam],{"C6_TES"		,cTesTmp												,nil})
					AADD(aItens2[nTam],{"C6_YITCONT"	,oModelZZ3:GetValue('ZZ3_ITEM')							,nil})
					AADD(aItens2[nTam],{"C6_YCONTRA"	,cContrato												,nil})
					AADD(aItens2[nTam],{"C6_YMEDICA"	,cMedic													,nil})
					AADD(aItens2[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT')		,nil})

					If oModelZZ3:IsDeleted()
						aadd(aItens2[nTam],{"AUTDELETA","S",Nil})
					ELSE
						aadd(aItens2[nTam],{"AUTDELETA","N",Nil})
					EndIf


				EndIf

			Next

			nFret	:= 0
			AADD(aCab,{"C5_TIPO"		,"N"													,Nil})
			AADD(aCab,{"C5_CLIENTE"		,oModelZZ2:GetValue('ZZ2_CLIENT')						,Nil})
			AADD(aCab,{"C5_LOJACLI"		,oModelZZ2:GetValue('ZZ2_LOJA')							,Nil})
			AADD(aCab,{"C5_CONDPAG"		,POSICIONE("ZZ0",1,XFILIAL("ZZ0")+oModelZZ2:GetValue('ZZ2_NUMERO')+oModelZZ2:GetValue('ZZ2_CLIENT')+oModelZZ2:GetValue('ZZ2_LOJA'),"ZZ0_CPFIN")		,Nil}) //oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CPFIN')
			AADD(aCab,{"C5_NATUREZ"		,cNaturez												,Nil})
			AADD(aCab,{"C5_YTPVEN"		,oModelZZ2:GetValue('ZZ2_TIPO')							,Nil})
			AADD(aCab,{"C5_TPEDAV"		,"39"													,Nil})
			AADD(aCab,{"C5_YCONTRA"		,cContrato 												,Nil})
			AADD(aCab,{"C5_YMEDICA"		,cMedic   												,Nil})
			AADD(aCab,{"C5_TPFRETE"		,"C"    												,Nil})

			//If POSICIONE("ZZ0",1,XFILIAL("ZZ0")+oModelZZ2:GetValue('ZZ2_NUMERO')+oModelZZ2:GetValue('ZZ2_CLIENT')+oModelZZ2:GetValue('ZZ2_LOJA'),"ZZ0_FRTCLI") <> 0 //=="1"
			//nFret:= nVlrAdiant*POSICIONE("ZZ0",1,XFILIAL("ZZ0")+oModelZZ2:GetValue('ZZ2_NUMERO')+oModelZZ2:GetValue('ZZ2_CLIENT')+oModelZZ2:GetValue('ZZ2_LOJA'),"ZZ0_FRTCLI")/100
			//EndIf
			aCliente	:= {oModelZZ2:GetValue('ZZ2_CLIENT'),oModelZZ2:GetValue('ZZ2_LOJA'),POSICIONE("ZZ0",1,XFILIAL("ZZ0")+oModelZZ2:GetValue('ZZ2_NUMERO')+oModelZZ2:GetValue('ZZ2_CLIENT')+oModelZZ2:GetValue('ZZ2_LOJA'),"ZZ0_NOME")}

			if (__cUserID $ GetMV("SA_ALTPED"))
				Processa({|| lRet := Pedido(aCab,aItens,aCliente,cContrato,cMedicao,nFret,aItens2,oModel,5,cFilHonld) },"Processando","Aguarde...",.F.)
				If !lRet
					DisarmTransaction()
					oModel:SetErrorMessage('ZZ3DETAIL',,,,"ERRO", 'N�o foi possivel excluir o pedido de venda!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				ELSE //ELSEIF lRet .AND. (__cUserID $ GetMV("SA_ALTPED"))
					// retorna o saldo dos itens para o contrato
					For nCont := 1 To oModelZZ3:Length()
						oModelZZ3:GoLine(nCont)

						dbSelectArea("ZZ1")
						IF oModelZZ3:GetValue('ZZ3_TES') == "508" .OR. oModelZZ3:GetValue('ZZ3_TES') == "510"

							ZZ1->(dbSetOrder(2))
							ZZ1->(dbSeek(XFILIAL("ZZ1")+oModelZZ3:getvalue("ZZ3_NUMERO")+ZZ2->ZZ2_CLIENT+ZZ2->ZZ2_LOJA+oModelZZ3:getvalue("ZZ3_PRODUT")+oModelZZ3:getvalue("ZZ3_TES")))
						ELSE
							ZZ1->(dbSetOrder(3))
							ZZ1->(dbSeek(XFILIAL("ZZ1")+oModelZZ3:getvalue("ZZ3_NUMERO")+ZZ2->ZZ2_CLIENT+ZZ2->ZZ2_LOJA+oModelZZ3:getvalue("ZZ3_ITEM")+oModelZZ3:getvalue("ZZ3_PRODUT")))

						ENDIF
						ZZ1->(Reclock("ZZ1"),.F.)
						ZZ1->ZZ1_SALDO +=  oModelZZ3:getvalue("ZZ3_QUANT")
						ZZ1->(MsUnlock())
						ZZ1->(DBCLOSEAREA())

					Next

					cUpd := "  UPDATE "+RETSQLNAME("ZZR")+" " + CRLF
					cUpd += "  SET D_E_L_E_T_ = '*' " + CRLF
					cUpd += "  WHERE ZZR_FILIAL = '"+oModelZZ2:GetValue('ZZ2_FILIAL')+"' " + CRLF
					cUpd += "  AND ZZR_CONTRA = '"+oModelZZ2:GetValue('ZZ2_NUMERO')+"' " + CRLF
					cUpd += "  AND ZZR_MEDICA = '"+oModelZZ2:GetValue('ZZ2_MEDICA')+"' " + CRLF

					If (TCSQLExec(cUpd) < 0)
						Return MsgStop("TCSQLError() " + TCSQLError())
					EndIf


				EndIf
			else
				alert("Usu�rio sem permiss�o para excluir medi�ao!")
			endif

		End Transaction


		//U_SASP023(XFILIAL("ZZ0"),cContrato,aCliente[1],aCliente[2],dDatabase,TIME(),__cUserid,xFilial("ZZ2")+cContrato+cMedicao,'03','01-Exclus�o da Medi��o...')

		MsUnLockAll()

		IF (__cUserID $ GetMV("SA_ALTPED"))
			MsgInfo("Exclus�o da medi��o "+oModel:GETMODEL('ZZ3DETAIL'):GetValue('ZZ3_MEDICA')+;
			" para o contrato "+oModel:GETMODEL('ZZ3DETAIL'):GetValue('ZZ3_NUMERO'))


			//Deleta os Itens na ZZ3
			cUpd:="UPDATE "+RetSqlName("ZZ3")+" SET D_E_L_E_T_ ='*' WHERE "+;
			" ZZ3_FILIAL = '"+oModel:GETMODEL('ZZ3DETAIL'):GetValue('ZZ3_FILIAL')+"' AND "+;
			" ZZ3_NUMERO = '"+oModel:GETMODEL('ZZ3DETAIL'):GetValue('ZZ3_NUMERO')+"' AND "+;
			" ZZ3_MEDICA = '"+oModel:GETMODEL('ZZ3DETAIL'):GetValue('ZZ3_MEDICA')+"'  "
			TCSqlExec(cUpd)
			TcRefresh(RetSqlName("ZZ3"))

			//Deleta cabe�alho ZZ2
			cUpd:="UPDATE "+RetSqlName("ZZ2")+" SET D_E_L_E_T_ ='*' WHERE "+;
			" ZZ2_FILIAL = '"+oModel:GETMODEL('ZZ2MASTER'):GetValue('ZZ2_FILIAL')+"' AND "+;
			" ZZ2_NUMERO = '"+oModel:GETMODEL('ZZ2MASTER'):GetValue('ZZ2_NUMERO')+"' AND "+;
			" ZZ2_MEDICA = '"+oModel:GETMODEL('ZZ2MASTER'):GetValue('ZZ2_MEDICA')+"'  "
			TCSqlExec(cUpd)
			TcRefresh(RetSqlName("ZZ3"))
		ENDIF
		Return lRet
	endif

Return lRet

Static Function Pedido(aCab,aItens,aCliente,cContrato,cMedicao,nFret,aItens2,oModel,nOpc,cFilHonld)
	Local cFilBak			:= cFilAnt
	Local aCabCliente		:= aClone(aCab)
	Local aCabCol			:= aClone(aCab)
	Local cCNPJ			    := FWArrFilAtu()[18]					//Coligada
	Local cCNPJMatriz		:= FWArrFilAtu("01",cFilHonld)[18]	//Holding
	Local cCliCol           := cLjCol := ""
	Local nPosCliente		:= aScan(aCab,{|x| x[1]=="C5_CLIENTE"})
	Local nPosLoja		    := aScan(aCab,{|x| x[1]=="C5_LOJACLI"})
	Local nPosProd		    := aScan(aItens[1],{|x| x[1]=="C6_PRODUTO"})
	Local nPosItem		    := aScan(aItens[1],{|x| x[1]=="C6_ITEM"})
	Local nPosQuant		    := aScan(aItens[1],{|x| x[1]=="C6_QTDVEN"})
	Local nPosVlr			:= aScan(aItens[1],{|x| x[1]=="C6_PRCVEN"})
	Local nPosDel			:= aScan(aItens[1],{|x| x[1]=="AUTDELETA"})
	Local nPosTes			:= aScan(aItens[1],{|x| x[1]=="C6_YTES"})
	Local nPosArm1			:= aScan(aItens[1],{|x| x[1]=="C6_LOCAL"})
	Local nPosArm2			:= aScan(aItens2[1],{|x| x[1]=="C6_LOCAL"})
	Local cTesPV2			:= GetTes(SuperGetMv("SA_TESPV2",.F.,"",cFilHonld),aItens[1][nPosTes][2])		//Matriz-Comercial	//502:503;505:505
	Local cTesPV3			:= GetTes(SuperGetMv("SA_TESPV3",.F.,"",cFilHonld),aItens[1][nPosTes][2])		//Matriz-Cliente	//502:504;505:504
	Local cTesPC4			:= GetTes(SuperGetMv("SA_TESPC4",.F.,"",cFilAnt),aItens[1][nPosTes][2])		    //Comercial-Matriz	//502:016;505:016
	Local cPedido1,cPedido2,cPedido3
	Local aFornec			:= {,}
	Local aCabPC			:= {}
	Local aItensPcIclui 	:= {}
	Local aItensPC	:= {}
	Local nCont,aLinha
	Local lJaLiberado	:= .F.
	Local cPedido4   := ""
	Local cAreaPedido
	Local cFatMed			:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_FATEXP')
	//Local cFilHonld 
	Default aItens		:= {}
	Private lMsErroAuto	:= .F.
	Private cMat120Num	:= ""
	ProcRegua(4)

	AADD(aCabCliente,{"C5_NUM",oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV01'),Nil})
	AADD(aCabCliente,{"C5_FILIAL",oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV01FL'),Nil})

	cCliCol	:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLNF02') //SA1->A1_COD
	cLjCol	:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LJNF02') //SA1->A1_LOJA

	aFornec[1]	:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_FORNEC')  //SA2->A2_COD
	aFornec[2]	:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJFOR')  //SA2->A2_LOJA

	aCabCliente[nPosCliente][2]	:= aCliente[1]
	aCabCliente[nPosLoja][2]	:= aCliente[2]

	AADD(aCabCol,{"C5_NUM",oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV02'),Nil})
	AADD(aCabCol,{"C5_FILIAL",oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV02FL'),Nil})

	aCabCol[nPosCliente][2]		:= cCliCol
	aCabCol[nPosLoja][2]		:= cLjCol
	IncProc("Alterando pedido para o cliente")
	ProcessMessage()
	//Comercial - Cliente

	If cFatMed == "1"

		If !Empty(nPosArm1)
			For nCont:=1 to Len(aItens)
				aItens[nCont][nPosArm1][2]	:= SUPERGETMV("SA_ARSEXP", .F., "01")
			Next
		else

			For nCont:=1 to Len(aItens2)
				aItens2[nCont][nPosArm2][2]	:= SUPERGETMV("SA_ARSEXP", .F., "01")
			Next
		endif
	ENDIF

	lJaLiberado	:= .F.
	For nCont:=1 to Len(aItens)
		If SB1->(DbSeek(xFilial("SB1")+aItens[nCont][nPosProd][2])) .AND. SB1->B1_GRUPO=="0099" .AND. SB1->B1_YSUBGR=="31"
			lJaLiberado	:= .T.
			Exit
		EndIf
	Next

	cFilant := oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV01FL')

	//Grava��o Reclock ou Execauto
	If SuperGetMv("SA_TPGRV", ,"R") == "R" //Reclock

		AADD(aCabCliente,{"C5_FRETE",nFret,Nil})
		AADD(aCabCliente,{"C5_DESCONT",nDesc,Nil})

		cRet:= u_SASP039(aCabCliente,aItens,IIF(nOpc==5,5,4),"MATA410")

		If !empty(cRet[1])
			AutoGrLog("Erro ao gerar pedido de venda do Comercial para o cliente!")
			Aviso("Aten��o",cRet[1],{"Ok"})
			Return .F.
		Else
			cPedido1:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV01')
		Endif

	Else //ExecAuto

		AADD(aCabCliente,{"C5_FRETE",nFret,Nil})
		AADD(aCabCliente,{"C5_DESCONT",nDesc,Nil})

		cAreaPedido := getarea()

		If nOpc == 5 //Dele��o
			MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,4)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
		Endif

		lMsErroAuto:= .F.

		restarea(cAreaPedido)
		cAreaPedido := getarea()

		MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,IIF(nOpc==5,5,4))	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o

		If lMsErroAuto .and. FUNNAME() == "SASP038"
			AutoGrLog("Erro ao gerar pedido de venda do Comercial para o cliente!")
			MostraErro()
			Return .F.
		Else
			cPedido1	:= SC5->C5_NUM
		Endif
		restarea(cAreaPedido)
	Endif

	cFilant := cFilBak
	IF oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_TIPCTR') =='2'

		aSize(aCabCliente,Len(aCabCliente)-2) //Deleta Frete/Desconto para outras filiais

		IncProc("Alterando pedido na Matriz para o Comercial")
		ProcessMessage()

		//Matriz - Comercial
		cFilAnt	:= cFilHonld //"010105"	//Matriz
		lMsErroAuto	:= .F.
		For nCont:=1 to Len(aItens2)
			aItens2[nCont][nPosTes][2]	:= cTesPV2
		Next

		//Grava��o Reclock ou Execauto
		If SuperGetMv("SA_TPGRV",,"R") == "R" //Reclock

			cRet:= u_SASP039(aCabCol,aItens2,IIF(nOpc==5,5,4),"MATA410")

			If !empty(cRet[1])
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para o Comercial!")
				Aviso("Aten��o",cRet[1],{"Ok"})
				Return .F.
			Else
				cPedido2:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV02')
			Endif

		Else //ExecAuto

			cAreaPedido := getarea()

			If nOpc == 5 //Dele��o
				MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCol,aItens2,4)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
			Endif

			lMsErroAuto:= .F.

			restarea(cAreaPedido)
			cAreaPedido := getarea()

			MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCol , aItens2 , IIF(nOpc==5,5,4))	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o


			If lMsErroAuto .and. FUNNAME() == "SASP038"
				cFilAnt	:= cFilBak
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Comercial!")
				MostraErro()
				Return .F.
			Else
				cPedido2	:= SC5->C5_NUM
			Endif

			restarea(cAreaPedido)
		Endif

		//Matriz - Cliente
		IncProc("Alterando pedido na Matriz para o Cliente")
		ProcessMessage()
		lMsErroAuto	:= .F.
		For nCont:=1 to Len(aItens)
			aItens[nCont][nPosTes][2]:= cTesPV3
		Next

		aCabCliente[11][2]:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV03')
		aCabCliente[12][2]:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV03FL')
		AADD(aCabCliente,{"C5_DESCONT",nDesc,Nil})

		cFilant := oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV03FL')

		//Grava��o Reclock ou Execauto
		If SuperGetMv("SA_TPGRV",.F.,"R") == "R" //Reclock

			cRet:= u_SASP039(aCabCliente,aItens,IIF(nOpc==5,5,4),"MATA410")

			If !empty(cRet[1])
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Cliente!")
				Aviso("Aten��o",cRet[1],{"Ok"})
				Return .F.
			Else
				cPedido3:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV03')
			Endif

		Else //ExecAuto

			cAreaPedido := getarea()

			If nOpc == 5 //Dele��o
				MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,4)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
			Endif

			lMsErroAuto:= .F.

			restarea(cAreaPedido)
			cAreaPedido := getarea()

			MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,IIF(nOpc==5,5,4))	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o


			If lMsErroAuto .and. FUNNAME() == "SASP038"
				cFilAnt	:= cFilBak
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Cliente!")
				MostraErro()
				Return .F.
			Else
				cPedido3	:= SC5->C5_NUM
			Endif

			restarea(cAreaPedido)
		Endif

		aSize(aCabCliente,Len(aCabCliente)-1) //Deleta Desconto para outras filiais
		cFilAnt	:= cFilBak

		IncProc("Alterando pedido de compra para a Matriz")

		cPedido4	:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PC01')
		aadd(aCabPC,{"C7_FILIAL"	,oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PCFIL')})
		aadd(aCabPC,{"C7_NUM"		,cPedido4})
		aadd(aCabPC,{"C7_EMISSAO"	,dDataBase})
		aadd(aCabPC,{"C7_FORNECE"	,aFornec[1]})
		aadd(aCabPC,{"C7_LOJA"		,aFornec[2]})
		aadd(aCabPC,{"C7_COND"		,"001"})		//TODO VERIFICAR CONDI��O PAG
		aadd(aCabPC,{"C7_CONTATO"	,"AUTO"})
		aadd(aCabPC,{"C7_FILENT"	,cFilAnt})

		For nCont:= 1 To Len(aItens2)
			aLinha := {}

			aadd(aLinha,{"C7_ITEM"		,Padl(aItens2[nCont][nPosItem][2],GetSx3Cache("C7_ITEM","X3_TAMANHO"),"0")	,Nil})
			aadd(aLinha,{"C7_PRODUTO"	,aItens2[nCont][nPosProd][2]	,Nil})
			aadd(aLinha,{"C7_QUANT"		,aItens2[nCont][nPosQuant][2]	,Nil})
			aadd(aLinha,{"C7_PRECO"		,aItens2[nCont][nPosVlr][2]		,Nil})
			aadd(aLinha,{"C7_YTES"		,cTesPC4						,Nil})
			aadd(aLinha,{"C7_TES"		,"400"						,Nil})
			aadd(aLinha,{"C7_LOCAL"		,GETMV("SA_ENTLOCA")			,Nil})
			aadd(aLinha,{"C7_YCLAORC"	,"E"		    ,Nil})	//E-Estoque
			aadd(aLinha,{"C7_CC"		,"1025"		    ,Nil})	//1025-Almoxarifado
			aadd(aItensPC,aLinha)
		Next nCont
		For nCont:= 1 To Len(aItens2)
			aLinha := {}
			IF aItens2[nCont][len(aItens2[nCont])][2] == "S"
				LOOP
			ENDIF
			aadd(aLinha,{"C7_ITEM"		,Padl(aItens2[nCont][nPosItem][2],GetSx3Cache("C7_ITEM","X3_TAMANHO"),"0")	,Nil})
			aadd(aLinha,{"C7_PRODUTO"	,aItens2[nCont][nPosProd][2]	,Nil})
			aadd(aLinha,{"C7_QUANT"		,aItens2[nCont][nPosQuant][2]	,Nil})
			aadd(aLinha,{"C7_PRECO"		,aItens2[nCont][nPosVlr][2]		,Nil})
			aadd(aLinha,{"C7_YTES"		,cTesPC4						,Nil})
			aadd(aLinha,{"C7_TES"		,"400"						,Nil})
			aadd(aLinha,{"C7_LOCAL"		,GETMV("SA_ENTLOCA")			,Nil})
			aadd(aLinha,{"C7_YCLAORC"	,"E"		    ,Nil})	//E-Estoque
			aadd(aLinha,{"C7_CC"		,"1025"		    ,Nil})	//1025-Almoxarifado
			aadd(aItensPcIclui,aLinha)
		Next nCont

		//Grava��o Reclock ou Execauto
		If SuperGetMv("SA_TPGRV",.F.,"R") == "R" //Reclock

			cRet:= u_SASP039(aCabPC,aItensPC,IIF(nOpc==5,5,4),"MATA120")

			If !empty(cRet[1])
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Cliente!")
				Aviso("Aten��o",cRet[1],{"Ok"})
				Return .F.
			Else
				cPedido4:= oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PC01')
			Endif

		Elseif SuperGetMv("SA_TPGRV",.F.,"R") <> "R" //ExecAuto

			lMsErroAuto	:= .F.
			dbSelectArea("SC7")
			SC7->(dbSetOrder(1))
			If SC7->(dbSeek(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PCFIL')+cPedido4))
				MsExecAuto({|v,x,y,z| MATA120(v,x,y,z)}, 1,aCabPC,aItensPC,5)
			Endif

			if  nOpc <> 5 //ExecAuto
				lMsErroAuto	:= .F.
				cMat120Num	:= ""	//Variavel preenchida na A120Grava
				MsExecAuto({|v,x,y,z| MATA120(v,x,y,z)}, 1,aCabPC,aItensPcIclui,3)

				If lMsErroAuto
					AutoGrLog("Erro ao gerar pedido de compra!")
					MostraErro()
					Return .F.
				Else
					cPedido4	:= If(!Empty(cMat120Num),cMat120Num,cPedido4)
					dbSelectArea("SC7")
					dbSetOrder(1)
					If !MsSeek(xFilial("SC7")+cPedido4)
						AutoGrLog("Erro ao localizar pedido de compra gerado!")
						MostraErro()
						Return .F.
					EndIf
				Endif
			endif
		Endif
	ENDIF

	If nOpc <> 5 .AND. FUNNAME() == "SASP038"
		MsgInfo("Realizada altera��o da medi��o de n�mero : "+alltrim(cMedicao)+" para o contrato de numero: "+alltrim(cContrato)+" !","Mensagem")
	Endif


	if alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV01FL')) <> "" .AND. alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV01')) <> ""
		U_PESOMED(alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV01FL')),alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV01')),.F.)
	endif
	if alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV02FL')) <> "" .AND. alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV02')) <> ""
		U_PESOMED(alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV02FL')),alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV02')),.F.)
	endif
	if alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV03FL')) <> "" .AND. alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV03')) <> ""
		U_PESOMED(alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV03FL')),alltrim(oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_PV03')),.T.)
	endif

Return .T.


//Fun��o SASP034 - Fun��o para retornar o kit - Arvore.
Static Function ProcKit(oView)

	Local oModel:= FWModelActive()
	Local cKits	:= ""

	//Busca todos os itens do contrato que sejam kits

	cQuery:= "SELECT ZZ1_PRODUT FROM "+RetSqlName("ZZ1")+ " "
	cQuery+= " WHERE D_E_L_E_T_ =' ' AND "
	cQuery+= " ZZ1_FILIAL = '"+xFilial("ZZ1")+"' AND "
	cQuery+= " ZZ1_NUMERO = '"+oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_NUMERO')+"' AND "
	cQuery+= " ZZ1_CLIENT = '"+oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT')+"' AND	"
	cQuery+= " ZZ1_TIPO = 'K'  "

	TCQuery cQuery new alias T01KIT

	While !eof()
		cKits+= "'"+T01KIT->ZZ1_PRODUT+"',"
		T01KIT->(dbSkip())
	Enddo

	If !empty(cKits)
		cKits:= substr(cKits,1,len(cKits)-1)
	Endif

	T01KIT->(DbCloseArea())
	//U_SASP034('ALTMED')
	U_SASP034('ALTMED'," ( (ZZ7_CODIGO IN ("+cKits+")  ) OR (ZZ7_CODPAI IN ("+cKits+") ) )")

Return

// VALIDA��O DA LINHA , ONDE VAI SER VERIFICADO A EXISTENCIA DO PRODUTO NO CONTRATO E VERIFICA O SALDO DO MESMO
STATIC FUNCTION LINHAZZ3(oModelGrid,cId,xValor,oModel)

	Local oModelZZ3	:= oModelGrid:GetModel('ZZ3DETAIL')
	Local oModelZZ2	:= oModelGrid:GetModel('ZZ2MASTER')
	Local cProduto  := oModelZZ3:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')
	Local cClient   := oModelZZ2:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT')
	Local cLoja     := oModelZZ2:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJA')
	Local cNumero   := oModelZZ2:GetModel('ZZ2MASTER'):GetValue('ZZ2_NUMERO')
	Local cItem  := oModelZZ3:GetModel('ZZ3DETAIL'):GetValue('ZZ3_ITEM')

	dbSelectArea("ZZ1")
	dbSetOrder(3)

	IF dbseek(XFILIAL("ZZ1")+cNumero+cClient+cLoja+cItem+cProduto)

		IF !oModel:GetModel('ZZ3DETAIL'):GetValue('CANDELETE')
			Return .F.
		ELSE
			IF POSICIONE("ZZ1",2,XFILIAL("ZZ1")+cNumero+cClient+cLoja+cProduto,"ZZ1_SALDO") > 0

				IF POSICIONE("ZZ1",2,XFILIAL("ZZ1")+cNumero+cClient+cLoja+cProduto,"ZZ1_STATUS") == "I"
					oModelZZ3:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", 'Produto INATIVO no contrato!', 'Verificar produtos cadastrados no contrato!')
					Return .F.
				ELSE
					if !ProdOk(oModel,xValor)
						Return .F.
					else
						oModelZZ3:LoadValue( 'ZZ3DETAIL', 'ZZ3_SALDO', POSICIONE("ZZ1",2,XFILIAL("ZZ1")+cNumero+cClient+cLoja+cProduto,"ZZ1_SALDO") )
						ARMAZEM38(cProduto,oModel:GetModel('ZZ3DETAIL'):GETVALUE("ZZ3_TIPO"))
						Return .T.
					endif
				ENDIF
			ELSE
				oModelZZ3:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", 'Produto com saldo zerado no contrato!', 'Verificar produtos cadastrados no contrato!')
				Return .F.
			ENDIF
		ENDIF
	ELSE
		oModelZZ3:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", 'Produto n�o encontrado no contrato!', 'Verificar produtos cadastrados no contrato!')
		Return .F.
	ENDIF
	dbSelectArea("ZZ1")

RETURN .T.

STATIC FUNCTION ARMAZEM38(cProdut,cTipo)

	Local cFilanterior := ""
	Local cAreaAnterior := getarea()
	Local lMsErroAuto  := .F.

	procregua(100)

	if cTipo == "K"

		cquery1 := " SELECT ZZ7_PRODUT,ZZ7_QUANT,ZZ7_PERDSC, ZZ7_CATEGO  "
		cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 "
		cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
		cquery1 += " and D_E_L_E_T_ <> '*' "
		cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(cProdut) +"' "
		cquery1 += " and ZZ7_TIPO = 'A' "
		cquery1 += " ORDER BY ZZ7_PRODUT"
		TCQuery cquery1 new alias T03

		//cFilanterior := cfilant


		WHILE !T03->(EOF())

			dbSelectArea("SM0")
			dbGOTOP()                 // B9_FILIAL+B9_COD+B9_LOCAL+B9_DATA
			WHILE ! SM0->(EOF())
				//cfilant := SM0->M0_CODFIL
				IF SM0->M0_CODFIL == '100101'
					SM0->(DbSkip())
				ENDIF

				cquery1 := " SELECT *  "
				cquery1 += " from " + retsqlname('SB9') + " SB9 "
				cquery1 += " where B9_FILIAL = '" + SM0->M0_CODFIL + "' "
				cquery1 += " and D_E_L_E_T_ = '' "
				cquery1 += " and B9_COD = '"+ T03->ZZ7_PRODUT +"' "
				cquery1 += " and B9_LOCAL = '"+GETMV('SA_LOCFAT')+"' "
				TCQuery cquery1 new alias T07

				IF  T07->(eof())    // Avalia o retorno da pesquisa realizada

					cFilanterior := cfilant
					incproc("Aguarde...")
					Begin Transaction
						PARAMIXB1 := {}
						aadd(PARAMIXB1,{"B9_FILIAL",xfilial("SB9",SM0->M0_CODFIL),})
						aadd(PARAMIXB1,{"B9_COD",alltrim(T03->ZZ7_PRODUT),})
						aadd(PARAMIXB1,{"B9_LOCAL", GETMV('SA_LOCFAT'),})
						aadd(PARAMIXB1,{"B9_QINI",0,})
						//aadd(PARAMIXB1,{"B9_DATA",DDATABASE,})
						cfilant := SM0->M0_CODFIL
						MSExecAuto({|x,y| mata220(x,y)},PARAMIXB1,3)
						cfilant := cFilanterior
						If lMsErroAuto
							mostraerro()
						EndIf
					End Transaction

				ENDIF
				T07->(DBCLOSEAREA())
				SM0->(DbSkip())
				//cfilant := cFilanterior
			ENDDO
			T03->(DbSkip())
		ENDDO
		T03->(DBCLOSEAREA())

	elseif cTipo == "p"

		dbSelectArea("SM0")
		dbGOTOP()                 // B9_FILIAL+B9_COD+B9_LOCAL+B9_DATA
		WHILE ! SM0->(EOF())
			//cfilant := SM0->M0_CODFIL
			IF SM0->M0_CODFIL == '100101'
				SM0->(DbSkip())
			ENDIF

			cquery1 := " SELECT *  "
			cquery1 += " from " + retsqlname('SB9') + " SB9 "
			cquery1 += " where B9_FILIAL = '" + SM0->M0_CODFIL + "' "
			cquery1 += " and D_E_L_E_T_ = '' "
			cquery1 += " and B9_COD = '"+ cProdut +"' "
			cquery1 += " and B9_LOCAL = '"+GETMV('SA_LOCFAT')+"' "
			TCQuery cquery1 new alias T07

			IF  T07->(eof())    // Avalia o retorno da pesquisa realizada

				cFilanterior := cfilant
				incproc("Aguarde...")
				Begin Transaction
					PARAMIXB1 := {}
					aadd(PARAMIXB1,{"B9_FILIAL",xfilial("SB9",SM0->M0_CODFIL),})
					aadd(PARAMIXB1,{"B9_COD",alltrim(cProdut),})
					aadd(PARAMIXB1,{"B9_LOCAL", GETMV('SA_LOCFAT'),})
					aadd(PARAMIXB1,{"B9_QINI",0,})
					//aadd(PARAMIXB1,{"B9_DATA",DDATABASE,})
					cfilant := SM0->M0_CODFIL
					MSExecAuto({|x,y| mata220(x,y)},PARAMIXB1,3)
					cfilant := cFilanterior
					If lMsErroAuto
						mostraerro()
					EndIf
				End Transaction

			ENDIF
			T07->(DBCLOSEAREA())
			SM0->(DbSkip())
			//cfilant := cFilanterior
		ENDDO
	endif
	RestArea(cAreaAnterior)

RETURN

Static Function GetTes(cParam,cTes)
	Local cRetTES	:= ""
	Local aTES		:= {}
	Local aTmp		:= Separa(cParam,";",.T.)
	Local nPos
	aEval(aTmp,{|x| If(":"$x,AADD(aTES,Separa(x,":",.T.)),AADD(aTES,Separa(x+":",":",.T.)))})
	nPos	:= aScan(aTES,{|x| x[1]==cTes})
	If nPos>0
		cRetTES	:= aTES[nPos][2]
	EndIf
Return cRetTES

STATIC FUNCTION QUANTZZ3(oModelGrid,cId,xValor)
	Local lRet := .T.
	Local oModelZZ3	:= oModelGrid:GetModel('ZZ3DETAIL')
	Local oModel	:= FWModelActive()

	IF oModelZZ3:GetModel('ZZ3DETAIL'):GetValue('ZZ3_QUANT') == 0
		lRet := .F.
		oModelZZ3:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", 'Quantidades zerada n�o � permitido!')
	ENDIF
	IF oModelZZ3:GetModel('ZZ3DETAIL'):GetValue('ZZ3_QUANT') < 0
		lRet := .F.
		oModelZZ3:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", 'Quantidades negativas n�o � permitido!', 'Verificar o campo quantidade!')
	ENDIF

	nSaldo:= POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_NUMERO')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_TES'),"ZZ1_SALDO")

	nQTDZ3:= POSICIONE("ZZ3",1,XFILIAL("ZZ3")+oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_NUMERO')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_MEDICA')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_TES'),"ZZ3_QUANT")

	IF nSaldo - ( oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_QUANT') - nQTDZ3  ) < 0
		lRet := .F.
		oModelZZ3:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", 'Quantidade informada � superior ao Saldo!', 'Verificar produtos cadastrados no contrato!')
	ENDIF

RETURN lRet

Static Function ValVenda(oModel)
	Local aValVenda := {}
	Local aStruct	:= oModel:GetModel( 'ZZ3DETAIL' ):oFormModelStruct:GetFields()
	Local oModelZZ0	:= oModel:GetModel('ZZ0MASTER')
	Local oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')
	Local nValkit   := oModelZZ3:GetValue('ZZ3_VLUNIT')
	Local nDifer    := 0
	Local nDesc     := 0

	cquery := " Select COUNT(ZZ7_CODIGO) TOTAL "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery += " and D_E_L_E_T_ <> '*' "
	cquery += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
	cquery += " and ZZ7_TIPO = 'A'		"
	cquery += " and ZZ7_SUPLEM = 'S' " //Considera somente que n�o tem desconto
	TCQuery cquery new alias T0402

	nDesc :=  T0402->TOTAL * GETMV('SA_PRECDSC')

	T0402->(DBCLOSEAREA())

	nValkit -= nDesc

	/*
	cquery4 := " select COUNT(ZZ7_CODIGO) TOTAL "
	cquery4 += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery4 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery4 += " and D_E_L_E_T_ <> '*' "
	cquery4 += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
	cquery4 += " and ZZ7_TIPO = 'A' "
	TCQuery cquery4 new alias T04
	*/
	cquery := " Select COUNT(ZZ7_CODIGO) TOTAL "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery += " and D_E_L_E_T_ <> '*' "
	cquery += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
	cquery += " and ZZ7_TIPO = 'A'		"
	cquery += " and ZZ7_PERDSC = '0'	" //Considera somente que n�o tem desconto
	cquery += " and ZZ7_SUPLEM = 'N'	"
	cquery += " and ZZ7_CATEGO IN ('A','C','D','V')	"

	TCQuery cquery new alias T04
	FOR nX := 1 to T04->TOTAL
		aadd(aValVenda,{NoRound(nValkit / T04->TOTAL,2)})
		nDifer+= NoRound(nValkit / T04->TOTAL,2)
	NEXT

	If len(aValVenda) > 0 .and. (nValkit <> nDifer)
		aValVenda[1][1] := aValVenda[1][1] + (nValkit - nDifer)
	Endif

	T04->(dbclosearea())

	/*
	FOR nX := 1 to T04->TOTAL
	If nX <> T04->TOTAL .OR. T04->TOTAL == 1

	aadd(aValVenda,{NoRound(nValkit / T04->TOTAL,2)})

	//Se nX for igual ao tamanho do vetor, o calculo � o valor de rateio + o valor faltante dos outros itens.
	Else
	aadd(aValVenda,{NoRound(nValkit / T04->TOTAL,2)})

	nDifer := NoRound(nValkit - (NoRound(nValkit / T04->TOTAL,2) * T04->TOTAL) ,2)
	aValVenda[nX][1] := aValVenda[nX][1] + nDifer
	Endif
	NEXT

	T04->(dbclosearea())
	*/

Return aValVenda

Static Function FormLinPre(oStructZZ3,nLinha,cOperacao,cCampo)
	Local oModel	:= FWModelActive()
	If cOperacao=="CANSETVALUE"
		If cCampo=="ZB4_PRODUT" .and. !oStructZZ3:GetValue("CANDELETE")	//N�o deixa alterar o produto
			Return .F.
		EndIf
	EndIf
Return .T.


STATIC FUNCTION YSASPO36Ln(oView,oModel)
	LOCAL oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')

	if oModelZZ3:getvalue("ZZ3_QUANT") == 0
		oModel:SETVALUE('ZZ3DETAIL','ZZ3_QUANT',oModelZZ3:GETVALUE('ZZ3_SALDO'))
	ELSEif oModelZZ3:getvalue("ZZ3_QUANT") == oModelZZ3:getvalue("ZZ3_SALDO")
		oModel:SETVALUE('ZZ3DETAIL','ZZ3_QUANT',0)
	ENDIF

RETURN

// Fun��o para verificar o produto a ser medido est� na tabela de pre�o informada no parametro SA_TABPREC

STATIC FUNCTION ProdOk(oModel,cProd)
	Local oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')
	Local oModelZZ2	:= oModel:GetModel('ZZ2MASTER')
	Local lRet      := .T.
	Local cTabPad   := GETMV("SA_TABPREC")
	Local cTab02    := GETMV("SA_TABPR2")
	Local nVal      := 0
	Local cProdErro := ""
	Local cPesoErro := ""
	Local lPeso     := .F.


	// TRATAMENTO PARA PRODUTO
	If oModelZZ3:GETVALUE('ZZ3_TIPO') == 'P'

		nVal:= MaTabPrVen(cTabPad,oModelZZ3:GETVALUE('ZZ3_PRODUT'),1,oModelZZ2:GETVALUE('ZZ2_CLIENT'),oModelZZ2:GETVALUE('ZZ2_LOJA'))
		nVal2 := MaTabPrVen(cTab02,oModelZZ3:GETVALUE('ZZ3_PRODUT'),1,oModelZZ2:GETVALUE('ZZ2_CLIENT'),oModelZZ2:GETVALUE('ZZ2_LOJA'))
		if nVal == 0 .AND. nVal2 == 0
			lRet := .F.
			cProdErro += ""+alltrim(oModelZZ3:GETVALUE('ZZ3_PRODUT'))+"-"+alltrim(POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(oModelZZ3:GETVALUE('ZZ3_PRODUT')),"B1_DESC"))+""+ Chr(13) + Chr(10) +" "
		endif
		if posicione("SF4",1,XFILIAL("SF4")+oModelZZ3:GETVALUE('ZZ3_TES'),"F4_ESTOQUE") == "S" .AND. POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(oModelZZ3:GETVALUE('ZZ3_PRODUT')),"B1_PESO") == 0
			lPeso := .T.
			cPesoErro += ""+alltrim(oModelZZ3:GETVALUE('ZZ3_PRODUT'))+"-"+alltrim(POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(oModelZZ3:GETVALUE('ZZ3_PRODUT')),"B1_DESC"))+""+ Chr(13) + Chr(10) +" "
		endif

	ENDIF

	// TRATAMENTO PARA KIT
	If oModelZZ3:GETVALUE('ZZ3_TIPO') == 'K'
		cquery1 := " select * "
		cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 "
		cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
		cquery1 += " and D_E_L_E_T_ <> '*' "
		cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GETVALUE('ZZ3_PRODUT')) +"' "
		cquery1 += " and ZZ7_TIPO = 'A' "
		cquery1 += " ORDER BY ZZ7_PRODUT"
		TCQuery cquery1 new alias T07

		While T07->(!EOF())
			nVal:= MaTabPrVen(cTabPad,T07->ZZ7_PRODUT,1,oModelZZ2:GETVALUE('ZZ2_CLIENT'),oModelZZ2:GETVALUE('ZZ2_LOJA'))
			nVal2 := MaTabPrVen(cTab02,oModelZZ3:GETVALUE('ZZ3_PRODUT'),1,oModelZZ2:GETVALUE('ZZ2_CLIENT'),oModelZZ2:GETVALUE('ZZ2_LOJA'))
			if nVal == 0 .AND. nVal2 == 0
				lRet := .F.
				cProdErro += ""+alltrim(T07->ZZ7_PRODUT)+"-"+alltrim(POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(T07->ZZ7_PRODUT),"B1_DESC"))+""+ Chr(13) + Chr(10) +" "
			endif
			if posicione("SF4",1,XFILIAL("SF4")+oModelZZ3:GETVALUE('ZZ3_TES'),"F4_ESTOQUE") == "S" .AND. POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(T07->ZZ7_PRODUT),"B1_PESO") == 0
				lPeso := .T.
				cPesoErro += ""+alltrim(T07->ZZ7_PRODUT)+"-"+alltrim(POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(T07->ZZ7_PRODUT),"B1_DESC"))+""+ Chr(13) + Chr(10) +" "
			endif
			T07->(dbskip())
		ENDDO
		T07->(DBCLOSEAREA())
	ENDIF

	IF lRet == .F.
		ALERT("Os segunetes produtos, n�o est�o na tabela de pre�o. Produtos :"+ Chr(13) + Chr(10)+""+cProdErro+"")
	ENDIF
	IF lPeso
		lRet := .F.
		ALERT("Os segunetes produtos, n�o possuem peso e a TES selecionada movimenta estoque! Produtos :"+ Chr(13) + Chr(10)+""+cPesoErro+"")
	ENDIF

RETURN lRet

// fun��o onde vai ser verificado o tipo de medi��o e se devidos cadastros est�o corretos, caso contrario, cancela a medi��o
Static Function validTipo(cTipo,oModel)
	Local cEspecie  := ""
	Local nDias
	Local oModelZZ2 := oModel:GETMODEL("ZZ2MASTER")
	Local oModelZZ3 := oModel:GETMODEL("ZZ3DETAIL")
	Local aSerieEnvio := {}
	Local dMinima
	Local dDataMax
	Local dSolicita := oModelZZ2:GETVALUE("ZZ2_DTSOLI")

	//MONTA ARRAY COM A SERIE E ENVIO DE CADA PRODUTO, PABA BUSCAR A DATA MAXIMA DE ENTREGA
	FOR nAx := 1 to len(oModelZZ3:acols)
		oModelZZ3:GoLine(nAx)
		IF oModelZZ3:getvalue("ZZ3_TIPO") == "P"
			aadd(aSerieEnvio,{ALLTRIM(POSICIONE("SB1",1,XFILIAL("SB1")+alltrim(oModelZZ3:getvalue("ZZ3_PRODUT")),"B1_YSRPRD")),oModelZZ3:getvalue("ZZ3_ENVIO") })
		ELSEIF oModelZZ3:getvalue("ZZ3_TIPO") == "K"
			cquery1 := " SELECT ZZ7_SERIE "
			cquery1 += " FROM " + retsqlname('ZZ7') + " ZZ7 "
			cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
			cquery1 += " and D_E_L_E_T_ <> '*' "
			cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
			cquery1 += " and ZZ7_TIPO = 'A' "
			cquery1 += " ORDER BY ZZ7_PRODUT"
			TCQuery cquery1 new alias T13

			WHILE T13->(!EOF())
				aadd(aSerieEnvio,{ALLTRIM(T13->ZZ7_SERIE),oModelZZ3:getvalue("ZZ3_ENVIO") })
				T13->(DbSkip())
			ENDDO
			T13->(DBCLOSEAREA())
		ENDIF
	NEXT
	oModelZZ3:GoLine(1)

	cEspecie := POSICIONE("ZZG",1,XFILIAL("ZZG")+cTipo,'ZZG_ESPCTR')

	IF POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_FIXO')== 'S'
		IF !alltrim(POSICIONE("ZZM",2,XFILIAL("ZZM")+cTipo,'ZZM_TIPO'))==""
			dMinima:=POSICIONE("ZZM",2,XFILIAL("ZZM")+cTipo,'ZZM_DATA')
		ELSE
			FOR nAx:= 1 to len(aSerieEnvio)
				IF nAx == 1
					dMinima:=POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')
				ELSE
					IF !vazio(POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')) .AND. (POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA') < dMinima)
						dMinima:=POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')
					ENDIF
				ENDIF
			NEXT
		ENDIF
	ELSEIF POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_FIXO')== 'N'
		FOR nAx:= 1 to len(aSerieEnvio)
			IF nAx == 1
				nDias := POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')
			ELSE
				IF !vazio(POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')) .AND. (POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS') < nDias)
					nDias := POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')
				ENDIF
			ENDIF
		NEXT
	ENDIF

	IF !Empty(nDias)
		dDataMax := dSolicita + nDias
	elseIF !Empty(dMinima)
		dDataMax := dMinima
	ENDIF

RETURN dDataMax

User function ALTMEDZZ3(cCodKit2, aValor,lPrimeiro)

	Local oModel := FWModelActive()
	Local cCodKit	:= cCodKit2
	Local cTipo :=  ""
	Local cDesc := ""
	Local cQuery := ""
	Local nRegistros := 0
	Local cAliasT := GetNextAlias()
	Local nRateio := 0
	Local oModelZZ2 := oModel:GETMODEL("ZZ2MASTER")
	Local oModelZZ3 := oModel:GETMODEL("ZZ3DETAIL")
	Local nValContrato := POSICIONE("ZZ1",2,oModelZZ2:GETVALUE("ZZ2_FILIAL")+oModelZZ2:GETVALUE("ZZ2_NUMERO")+oModelZZ2:GETVALUE("ZZ2_CLIENT")+oModelZZ2:GETVALUE("ZZ2_LOJA")+cCodKit2+aValor[2],"ZZ1_VLRUN" )

	aValor := {nValContrato,aValor[2]}

	//Avo - Sintetico
	//   Pai - Sintetico
	//     Filho - Sintetico
	//     Filho - Analitico
	//Retorna somente o filho sint�tico.

	If !Empty(cCodKit)

		cTipo := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_TIPO")

		If cTipo == 'S'

			cQuery := ""
			cQuery += " SELECT ZZ7_CODIGO, ZZ7_DESCR "
			cQuery += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7 "
			cQuery += "  WHERE D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7_CODPAI = '" + cCodKit +  "' "
			cQuery += "    AND ZZ7_TIPO = 'S' "
			cQuery += "    AND ZZ7_CODIGO <> '" + cCodKit + "' "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasT,.F.,.T.)

			Count To nRegistros

			(cAliasT)->(DBGOTOP())

			if nRegistros > 0

				While (cAliasT)->(!EOF())

					U_ALTMEDZZ3((cAliasT)->ZZ7_CODIGO, 0,.F.)

					(cAliasT)->(DBSKIP())

				EndDo

				(cAliasT)->(DBCLOSEAREA())
			Else

				cDesc := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_DESCR" )
				//Codigo do kit, Descri��o do item, Quantidade, TES, Valor unitario, Tipo(k = Kit)
				AADD(aKit, {cCodKit,cDesc,1,aRet[2],1,'K'})

				(cAliasT)->(DBCLOSEAREA())

			EndIf

		Else
			Alert("Produto selecionado n�o � um kit. O tipo do produto deve ser SINT�TICO.")
		EndIf

	EndIf

	//Se aValor for booleano == .F. , o usu�rio deve ter clicado em Cancelar no ParamBox.
	if ValType(aValor) == 'L'
		aValor:={}
		aadd(aValor,1)
		aadd(aValor,'501')

	EndIf

	If lPrimeiro == .T.

		For nX := 1 to len(aKit)

			//Se nX for diferente do tamanho do vetor, o valor do rateio � o calculo da quantidade de itens * o valor informado na rotina de kits.
			If nX <> len(aKit) .OR. len(aKit) == 1

				nRateio := NoRound(aValor[1] / len(aKit),2)

				//Se nX for igual ao tamanho do vetor, o calculo � o valor de rateio + o valor faltante dos outros itens.
			Else
				nRateio := NoRound(aValor[1] / len(aKit),2)

				nDifer  := NoRound(aValor[1] - (nRateio * len(aKit)) ,2)
				nRateio := nRateio + nDifer

			Endif

			oModel:GetModel('ZZ3DETAIL'):SetValue('ZZ3_TIPO',aKit[nX][6])
			oModel:GetModel('ZZ3DETAIL'):SetValue('ZZ3_PRODUT',aKit[nX][1])
			oModel:GetModel('ZZ3DETAIL'):SetValue('ZZ3_DESCRI',aKit[nX][2])
			oModel:GetModel('ZZ3DETAIL'):SetValue('ZZ3_QUANT',aKit[nX][3])
			oModel:GetModel('ZZ3DETAIL'):SetValue('ZZ3_TES',aKit[nX][4])
			oModel:GetModel('ZZ3DETAIL'):SetValue('ZZ3_VLUNIT',nRateio)



			If nX < len(aKit)
				oModelZZ3:AddLine()
			EndIf

			GETDREFRESH()
			nDifer := 0

		next nX

		aKit := {}

	EndIf
Return

//Gatilho Saldo Final
User Function RETSLD38

	Local oModel	:= FWModelActive()

	nSaldo:= POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_NUMERO')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_TES'),"ZZ1_SALDO")

	nQTDZ3:= POSICIONE("ZZ3",1,XFILIAL("ZZ3")+oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_NUMERO')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_MEDICA')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_TES'),"ZZ3_QUANT")

	nRet:= nSaldo - ( oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_QUANT') - nQTDZ3  )

Return nRet

//Gatilho da TES
User Function FRetTES38

	Local oModel:= FWModelActive()

	cTES:= POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_NUMERO')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT'),"ZZ1_TES")

Return cTES

//Gatilho do Valor Unit�rio
User Function FRetVUNT38

	Local oModel:= FWModelActive()

	nVrUnit:= POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_NUMERO')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT'),"ZZ1_VLRUN")

Return nVrUnit

//Gatilho do Valor Total
User Function FRetVTOT38

	Local oModel:= FWModelActive()

	nVrTOT:= POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_NUMERO')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_CLIENT')+;
	oModel:GetModel('ZZ2MASTER'):GetValue('ZZ2_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT'),"ZZ1_VLRTOT")

Return nVrTOT

//Gatilho para a descri��o
User Function FRetDsc38
	Local oModel:= FWModelActive()
	Local cRet	:= ""
	dbSelectArea("SB1")
	SB1->(dbSetOrder(1))
	If SB1->(dbSeek(xFilial("SB1")+oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')))
		cRet:= SB1->B1_DESC
	Else
		dbSelectArea("ZZ7")
		ZZ7->(dbSetOrder(1))
		If ZZ7->(dbSeek(xFilial("ZZ7")+oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')))
			cRet:= ZZ7->ZZ7_DESCR
		Endif
	Endif

Return cRet

STATIC FUNCTION DESCFRET(oModel)
	Local aStruct	:= oModel:GetModel( 'ZZ3DETAIL' ):oFormModelStruct:GetFields()
	Local oModelZZ2	:= oModel:GetModel('ZZ2MASTER')
	Local oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')
	Local nValkit   := oModelZZ3:GetValue('ZZ3_VLUNIT')
	Local nDesc     := 0

	cquery := " Select COUNT(ZZ7_CODIGO) TOTAL "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery += " and D_E_L_E_T_ <> '*' "
	cquery += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
	cquery += " and ZZ7_TIPO = 'A'		"
	cquery += " and ZZ7_SUPLEM = 'S' " //Considera somente que n�o tem desconto
	TCQuery cquery new alias T0402

	nDesc :=  T0402->TOTAL * GETMV('SA_PRECDSC')

	T0402->(DBCLOSEAREA())

RETURN nDesc


//Valida��o se a Medi��o j� foi Faturada
Static Function ZZ2ValAct(oModel)

	If 	oModel:getOperation() == MODEL_OPERATION_UPDATE .or. ;
	oModel:getOperation() == MODEL_OPERATION_DELETE

		If !Empty(ZZ2->ZZ2_PV01NF)
			oModel:SetErrorMessage('ZZ2MASTER',,,,"ATEN��O", 'Medi��o j� faturada', 'Necess�rio exclus�o da Nota Fiscal')
			Return .F.
		Endif
	Endif

Return .T.

// FUN��O PARA ABRIR O PEDIDO 01

STATIC FUNCTION YPED01(oModel)

	Local oModelZZ2	    := oModel:GetModel('ZZ2MASTER')
	Local oModelZZ3	    := oModel:GetModel('ZZ3DETAIL')
	Local aPed01 := {}
	Local cfilatu := cFilant
	Local cArea := GetArea()
	Private aRotina    := {}
	Private nTipoPed    := 1
	Private cCadastro   := ''
	Private l120Auto    := .F.
	Private LINTGC

	//--Monta o aRotina para compatibilizacao
	AAdd( aRotina, { '' , '' , 0, 1 } )
	AAdd( aRotina, { '' , '' , 0, 2 } )
	AAdd( aRotina, { '' , '' , 0, 3 } )
	AAdd( aRotina, { '' , '' , 0, 4 } )
	AAdd( aRotina, { '' , '' , 0, 5 } )

	If select("T01") > 0
		dbSelectArea("T01")
		T01->(DBCLOSEAREA())
	Endif
	cquery := " SELECT C5_FILIAL,C5_NUM,SC5.R_E_C_N_O_ AS C5RECNO "
	cquery += " FROM "+ RETSQLNAME("ZZ2") +" ZZ2 ,"+ RETSQLNAME("SC5") +" SC5 "
	cquery += " WHERE ZZ2_FILIAL  = '"+oModelZZ2:GETVALUE("ZZ2_FILIAL")+"' "
	cquery += " AND ZZ2_MEDICA = '" +oModelZZ2:GETVALUE("ZZ2_MEDICA")+ "' "
	cquery += " AND ZZ2_CLIENT = '" +oModelZZ2:GETVALUE("ZZ2_CLIENT")+ "' "
	cquery += " AND ZZ2_LOJA = '" +oModelZZ2:GETVALUE("ZZ2_LOJA")+ "' "
	cquery += " AND ZZ2_NUMERO = '" +oModelZZ2:GETVALUE("ZZ2_NUMERO")+  "' "
	cquery += " AND ZZ2.D_E_L_E_T_ = '' AND SC5.D_E_L_E_T_='' "
	cquery += " AND C5_FILIAL=ZZ2_PV01FL "
	cquery += " AND C5_NUM = ZZ2_PV01 "

	TCQuery cquery new alias T01

	IF ! T01->(EOF())

		cFilant := T01->C5_FILIAL

		dbSelectArea("SC5")
		dbSetOrder(1)
		dbSeek(T01->C5_FILIAL+T01->C5_NUM)

		SC5->(a410Visual("SC5",RecNo(),2))

		//A410Visual ( "SC5", T01->C5RECNO, 2 )
	ELSE
		ALERT("Pedido 01 da medi��o "+oModelZZ2:GETVALUE("ZZ2_MEDICA")+" do contrato "+oModelZZ2:GETVALUE("ZZ2_NUMERO")+" n�o encontrado! ")
	ENDIF

	cFilant := cfilatu

	T01->(dbclosearea())
	RestArea( cArea )

RETURN

// FUN��O PARA ABRIR O PEDIDO 02

STATIC FUNCTION YPED02(oModel)

	Local oModelZZ2	    := oModel:GetModel('ZZ2MASTER')
	Local oModelZZ3	    := oModel:GetModel('ZZ3DETAIL')
	Local aPed02 := {}
	Local cfilatu := cFilant
	Local cArea := GetArea()
	Private aRotina    := {}
	Private nTipoPed    := 1
	Private cCadastro   := ''
	Private l120Auto    := .F.
	Private LINTGC

	//--Monta o aRotina para compatibilizacao
	AAdd( aRotina, { '' , '' , 0, 1 } )
	AAdd( aRotina, { '' , '' , 0, 2 } )
	AAdd( aRotina, { '' , '' , 0, 3 } )
	AAdd( aRotina, { '' , '' , 0, 4 } )
	AAdd( aRotina, { '' , '' , 0, 5 } )

	If select("T01") > 0
		dbSelectArea("T01")
		T01->(DBCLOSEAREA())
	Endif
	cquery := " SELECT C5_FILIAL,C5_NUM,SC5.R_E_C_N_O_ AS C5RECNO "
	cquery += " FROM "+ RETSQLNAME("ZZ2") +" ZZ2 ,"+ RETSQLNAME("SC5") +" SC5 "
	cquery += " WHERE ZZ2_FILIAL  = '"+oModelZZ2:GETVALUE("ZZ2_FILIAL")+"' "
	cquery += " AND ZZ2_MEDICA = '" +oModelZZ2:GETVALUE("ZZ2_MEDICA")+ "' "
	cquery += " AND ZZ2_CLIENT = '" +oModelZZ2:GETVALUE("ZZ2_CLIENT")+ "' "
	cquery += " AND ZZ2_LOJA = '" +oModelZZ2:GETVALUE("ZZ2_LOJA")+ "' "
	cquery += " AND ZZ2_NUMERO = '" +oModelZZ2:GETVALUE("ZZ2_NUMERO")+  "' "
	cquery += " AND ZZ2.D_E_L_E_T_ = '' AND SC5.D_E_L_E_T_='' "
	cquery += " AND C5_FILIAL=ZZ2_PV02FL "
	cquery += " AND C5_NUM = ZZ2_PV02 "

	TCQuery cquery new alias T01

	IF ! T01->(EOF())

		cFilant := T01->C5_FILIAL

		dbSelectArea("SC5")
		dbSetOrder(1)
		dbSeek(T01->C5_FILIAL+T01->C5_NUM)

		SC5->(a410Visual("SC5",RecNo(),2))

		//A410Visual ( "SC5", T01->C5RECNO, 2 )
	ELSE
		ALERT("Pedido 02 da medi��o "+oModelZZ2:GETVALUE("ZZ2_MEDICA")+" do contrato "+oModelZZ2:GETVALUE("ZZ2_NUMERO")+" n�o encontrado! ")
	ENDIF

	cFilant := cfilatu

	T01->(dbclosearea())
	RestArea( cArea )

RETURN


// FUN��O PARA ABRIR O PEDIDO 03

STATIC FUNCTION YPED03(oModel)

	Local oModelZZ2	    := oModel:GetModel('ZZ2MASTER')
	Local oModelZZ3	    := oModel:GetModel('ZZ3DETAIL')
	Local aPed03 := {}
	Local cfilatu := cFilant
	Local cArea := GetArea()
	Private aRotina    := {}
	Private nTipoPed    := 1
	Private cCadastro   := ''
	Private l120Auto    := .F.
	Private LINTGC

	//--Monta o aRotina para compatibilizacao
	AAdd( aRotina, { '' , '' , 0, 1 } )
	AAdd( aRotina, { '' , '' , 0, 2 } )
	AAdd( aRotina, { '' , '' , 0, 3 } )
	AAdd( aRotina, { '' , '' , 0, 4 } )
	AAdd( aRotina, { '' , '' , 0, 5 } )

	If select("T01") > 0
		dbSelectArea("T01")
		T01->(DBCLOSEAREA())
	Endif
	cquery := " SELECT C5_FILIAL,C5_NUM,SC5.R_E_C_N_O_ AS C5RECNO "
	cquery += " FROM "+ RETSQLNAME("ZZ2") +" ZZ2 ,"+ RETSQLNAME("SC5") +" SC5 "
	cquery += " WHERE ZZ2_FILIAL  = '"+oModelZZ2:GETVALUE("ZZ2_FILIAL")+"' "
	cquery += " AND ZZ2_MEDICA = '" +oModelZZ2:GETVALUE("ZZ2_MEDICA")+ "' "
	cquery += " AND ZZ2_CLIENT = '" +oModelZZ2:GETVALUE("ZZ2_CLIENT")+ "' "
	cquery += " AND ZZ2_LOJA = '" +oModelZZ2:GETVALUE("ZZ2_LOJA")+ "' "
	cquery += " AND ZZ2_NUMERO = '" +oModelZZ2:GETVALUE("ZZ2_NUMERO")+  "' "
	cquery += " AND ZZ2.D_E_L_E_T_ = '' AND SC5.D_E_L_E_T_='' "
	cquery += " AND C5_FILIAL=ZZ2_PV03FL "
	cquery += " AND C5_NUM = ZZ2_PV03 "

	TCQuery cquery new alias T01

	IF ! T01->(EOF())

		cFilant := T01->C5_FILIAL

		dbSelectArea("SC5")
		dbSetOrder(1)
		dbSeek(T01->C5_FILIAL+T01->C5_NUM)

		SC5->(a410Visual("SC5",RecNo(),2))

		//A410Visual ( "SC5", T01->C5RECNO, 2 )
	ELSE
		ALERT("Pedido 03 da medi��o "+oModelZZ2:GETVALUE("ZZ2_MEDICA")+" do contrato "+oModelZZ2:GETVALUE("ZZ2_NUMERO")+" n�o encontrado! ")
	ENDIF

	cFilant := cfilatu

	T01->(dbclosearea())
	RestArea( cArea )

RETURN

//Visualiza��o pelo F7 do KIT
Static Function VISKit

	Local oModel:= FWModelActive()

	U_SASP034(''," ( (ZZ7_CODIGO = '"+oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')+"') OR (ZZ7_CODPAI = '"+oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')+"'))")

Return

Static Function AtualizarP(oModel)

	Local oModelZZ2 := oModel:getmodel("ZZ2MASTER")
	Local oModelZZ3 := oModel:getmodel("ZZ3DETAIL")
	Local aProduto	:= {}
	Local cQuery	:= ""
	Local aArea := GetArea()
	Private aCabecalho  := {}         //Variavel que montar� o aHeader do grid
	Private aColsEx 	:= {}         //Vari�vel que receber� os dados
	
	IF alltrim(oModelZZ2:GetValue('ZZ2_STATUS')) == 'F'
	MsgAlert("Pesos n�o podem ser atualizados! Medi��o j� faturada!","ATEN��O!!!")
	return
	ENDIF

	FOR nAx := 1 to len(oModelZZ3:acols)
		oModelZZ3:GoLine(nAx)

		cQuery := " SELECT ZZ7_PRODUT, B1_DESC,B1_PESO,B1_MSBLQL FROM "+RetSqlName("ZZ7")+" ZZ7(NOLOCK) "
		cQuery += " INNER JOIN "+RetSqlName("SB1")+" B1(NOLOCK) "
		cQuery += " ON ZZ7_PRODUT = B1_COD "
		cQuery += " AND B1.D_E_L_E_T_ = '' "
		cQuery += " WHERE ZZ7.D_E_L_E_T_ = '' "
		cQuery += " AND ZZ7_CODPAI = '"+oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')+"' "
		cQuery += " GROUP BY ZZ7_PRODUT, B1_DESC,B1_PESO,B1_MSBLQL "
		cQuery += " ORDER BY ZZ7_PRODUT "
		TcQuery cQuery new Alias T10

		WHILE !T10->(EOF())

			AADD(aProduto,{T10->ZZ7_PRODUT, T10->B1_DESC,T10->B1_PESO,T10->B1_MSBLQL})
			T10->(DbSkip())
		ENDDO
		T10->(DbCloseArea())
	next

	processa( {|| Monta(aProduto) }, "Atualizando Peso dos produtos", "Processando aguarde...", .f.)

	RestArea(aArea)
return

Static Function Monta(aProduto)
	Local aBotoes	:= {}         //Vari�vel onde ser� incluido o bot�o para a legenda
	Local _nOpca 		:= 0 											//Opcao da confirmacao
	Local bOk 			:= {|| _nOpca:=1,_lRetorno:=.T.,oDlg:Montar() } 	//botao de ok
	Local bCancel 		:= {|| _nOpca:=0,_lRetorno:=.T.,oDlg:End() } 					//botao de cancelamento
	Local oRadMenu1
	Local aArea := GetArea()
	Local nRadMenu1 := 1
	Local aButtons := {}
	Local oTButton1 := Nil
	Local cPerg:= Padr("FFolGer",10)
	Local aaCampos  	:= {"B1_PESO"} //Vari�vel contendo o campo edit�vel no Grid
	Local aBotoes	:= {}         //Vari�vel onde ser� incluido o bot�o para a legenda
	Local _nOpca 		:= 0 											//Opcao da confirmacao
	Local oGet1
	Local cGet1 := "Define variable value"
	Local cTipo
	Local bOk 			:= {|| _nOpca:=1,_lRetorno:=.T.,oDlg:End() } 	//botao de ok
	Local bCancel 		:= {|| _nOpca:=0,_lRetorno:=.T.,oDlg:End() } 					//botao de cancelamento
	Local oRadMenu1
	Local nRadMenu1 := 1
	Local aButtons := {}
	Static oDlg
	Private oLista                    //Declarando o objeto do browser


	//Declarando os objetos de cores para usar na coluna de status do grid
	Private oVerde  	:= LoadBitmap( GetResources(), "BR_VERDE")
	Private oAzul  		:= LoadBitmap( GetResources(), "BR_AZUL")
	Private oVermelho	:= LoadBitmap( GetResources(), "BR_VERMELHO")
	Private oAmarelo	:= LoadBitmap( GetResources(), "BR_AMARELO")

	procregua(len(aProduto))	
	DEFINE MSDIALOG oDlg TITLE "TITULO" FROM 000, 000  TO 500, 1100  PIXEL
	//chamar a fun��o que cria a estrutura do aHeader
	CriaCabec()

	//Monta o browser com inclus�o, remo��o e atualiza��o
	oLista := MsNewGetDados():New( 053, 078, 415, 775, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "AllwaysTrue", aACampos,1, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aCabecalho, aColsEx)

	//Carregar os itens que ir�o compor o conteudo do grid
	Carregar(aProduto)

	//Alinho o grid para ocupar todo o meu formul�rio
	oLista:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT

	//Ao abrir a janela o cursor est� posicionado no meu objeto
	oLista:oBrowse:SetFocus()

	//Crio o menu que ir� aparece no bot�o A��es relacionadas
	aadd(aBotoes,{"NG_ICO_LEGENDA", {||Legenda()},"Legenda","Legenda"})

	//EnchoiceBar(oDlg, {|| oDlg:End() }, {|| oDlg:End() },,aBotoes)

	//ACTIVATE MSDIALOG oDlg CENTERED


	//@ 130, 069 MSGET oGet1 VAR cGet1 SIZE 073, 010 OF oDlg COLORS 0, 16777215 PIXEL //Campo de pesquisa

	Activate MsDialog oDlg On Init (EnchoiceBar(oDlg,bOk,bCancel,,)) Centered VALID _lRetorno

	IF _nOpca == 1
		Grava()
	endif
	RestArea(aArea)
return

Static Function CriaCabec()

	Local cAux := ""
	Aadd(aCabecalho, {;
	"",;//X3Titulo()
	"IMAGEM",;  //X3_CAMPO
	"@BMP",;		//X3_PICTURE
	3,;			//X3_TAMANHO
	0,;			//X3_DECIMAL
	".F.",;			//X3_VALID
	"",;			//X3_USADO
	"C",;			//X3_TIPO
	"",; 			//X3_F3
	"V",;			//X3_CONTEXT
	"",;			//X3_CBOX
	"",;			//X3_RELACAO
	"",;			//X3_WHEN
	"V"})			//
	Aadd(aCabecalho, {;
	GETSX3CACHE("B1_COD","X3_TITULO"),;//X3Titulo()
	"B1_COD",;  //X3_CAMPO
	GETSX3CACHE("B1_COD","X3_PICTURE"),;		//X3_PICTURE
	GETSX3CACHE("B1_COD","X3_TAMANHO"),;			//X3_TAMANHO
	GETSX3CACHE("B1_COD","X3_DECIMAL"),;			//X3_DECIMAL
	"",;			//X3_VALID
	GETSX3CACHE("B1_COD","X3_USADO"),;			//X3_USADO
	GETSX3CACHE("B1_COD","X3_TIPO"),;			//X3_TIPO
	GETSX3CACHE("B1_COD","X3_F3"),; 			//X3_F3
	GETSX3CACHE("B1_COD","X3_CONTEXT"),;			//X3_CONTEXT
	GETSX3CACHE("B1_COD","X3_CBOX"),;			//X3_CBOX
	GETSX3CACHE("B1_COD","X3_RELACAO"),;			//X3_RELACAO
	GETSX3CACHE("B1_COD","X3_WHEN")})			//X3_WHEN

	Aadd(aCabecalho, {;
	GETSX3CACHE("B1_DESC","X3_TITULO"),;//X3Titulo()
	"B1_DESC",;  //X3_CAMPO
	GETSX3CACHE("B1_DESC","X3_PICTURE"),;		//X3_PICTURE
	GETSX3CACHE("B1_DESC","X3_TAMANHO"),;			//X3_TAMANHO
	GETSX3CACHE("B1_DESC","X3_DECIMAL"),;			//X3_DECIMAL
	"",;			//X3_VALID
	GETSX3CACHE("B1_DESC","X3_USADO"),;			//X3_USADO
	GETSX3CACHE("B1_DESC","X3_TIPO"),;			//X3_TIPO
	GETSX3CACHE("B1_DESC","X3_F3"),; 			//X3_F3
	GETSX3CACHE("B1_DESC","X3_CONTEXT"),;			//X3_CONTEXT
	GETSX3CACHE("B1_DESC","X3_CBOX"),;			//X3_CBOX
	GETSX3CACHE("B1_DESC","X3_RELACAO"),;			//X3_RELACAO
	GETSX3CACHE("B1_DESC","X3_WHEN")})			//X3_WHEN
	Aadd(aCabecalho, {;
	GETSX3CACHE("B1_PESO","X3_TITULO"),;//X3Titulo()
	"B1_PESO",;  //X3_CAMPO
	GETSX3CACHE("B1_PESO","X3_PICTURE"),;		//X3_PICTURE
	GETSX3CACHE("B1_PESO","X3_TAMANHO"),;			//X3_TAMANHO
	GETSX3CACHE("B1_PESO","X3_DECIMAL"),;			//X3_DECIMAL
	"",;			//X3_VALID
	GETSX3CACHE("B1_PESO","X3_USADO"),;			//X3_USADO
	GETSX3CACHE("B1_PESO","X3_TIPO"),;			//X3_TIPO
	GETSX3CACHE("B1_PESO","X3_F3"),; 			//X3_F3
	GETSX3CACHE("B1_PESO","X3_CONTEXT"),;			//X3_CONTEXT
	GETSX3CACHE("B1_PESO","X3_CBOX"),;			//X3_CBOX
	GETSX3CACHE("B1_PESO","X3_RELACAO"),;			//X3_RELACAO
	GETSX3CACHE("B1_PESO","X3_WHEN")})			//X3_WHEN

Return

Static Function Carregar(aProdutos)
	Local cQuery := ''
	Local cAux := 0
	Local cTipo
	Local cTipC := ''
	Local i := 0

	For i := 1 to len(aProdutos)

		IF alltrim(aProdutos[i,4]) <> "1"
			aadd(aColsEx,{oAzul,aProdutos[i,1],aProdutos[i,2],aProdutos[i,3],.F.})
		ELSEIF alltrim(aProdutos[i,4]) == 2 .OR. EMPTY(aProdutos[i,4])
			aadd(aColsEx,{oVermelho,aProdutos[i,1],aProdutos[i,2],aProdutos[i,3],.F.})
		ENDIF
	Next

	//Setar array do aCols do Objeto.
	oLista:SetArray(aColsEx,.T.)

	//Atualizo as informa��es no grid
	oLista:Refresh()
Return

Static Function Grava()

	for x := 1 to len(oLista:AlastEdit)
		INCPROC()
		DbSelectArea("SB1")
		DbSetOrder(1)
		IF DbSeek(xFilial("SB1") + oLista:Acols[oLista:AlastEdit[x]][2])

			Reclock("SB1",.F.)

			SB1->B1_PESO := oLista:Acols[oLista:AlastEdit[x]][4]

			MsUnLock()
			AtuPedido(oLista:Acols[oLista:AlastEdit[x]][2])//Fun��o que atualiza todos os pesos dos pedidos de venda que tenham esses produtos. Obs: S� pedidos n�o faturados
		ENDIF

	next

	MsgInfo("PESO DOS PRODUTOS ATUALIZADO COM SUCESSO!")

return

Static Function AtuPedido(cProd)

	Local cQuery 	:= ''
	Local nPeso		:= 0 

	cQuery := " SELECT C6_FILIAL, C6_NUM FROM "+RetSqlName("SC6")+" C6 (NOLOCK) "
	cQuery += " INNER JOIN "+RetSqlName("SC5")+" C5 ON C6_FILIAL = C5_FILIAL AND C6_NUM = C5_NUM AND C5.D_E_L_E_T_ = '' 
	cQuery += " WHERE C6.D_E_L_E_T_ = '' "
	cQuery += " AND C6_PRODUTO = '"+cProd+"' "
	cQuery += " AND C6_NOTA = '' "
	cQuery += " GROUP BY C6_FILIAL, C6_NUM "
	cQuery += " ORDER BY 1,2 "
	TcQuery cQuery new Alias T10

	WHILE !T10->(EOF())

		cQuery := " SELECT C6_FILIAL, C6_NUM, C6_ITEM,C6_PRODUTO,B1_PESO,C6_QTDVEN FROM "+RetSqlName("SC6")+" C6 (NOLOCK) "
		cQuery += " INNER JOIN "+RetSqlName("SB1")+" B1 (NOLOCK) "
		cQuery += " ON C6_PRODUTO = B1_COD AND B1.D_E_L_E_T_ = '' "
		cQuery += " WHERE C6.D_E_L_E_T_ = '' "
		cQuery += " AND C6_FILIAL = '"+T10->C6_FILIAL+"' "
		cQuery += " AND C6_NUM = '"+T10->C6_NUM+"' "
		cQuery += " AND C6_NOTA = '' "
		cQuery += " GROUP BY C6_FILIAL, C6_NUM, C6_ITEM,C6_PRODUTO,B1_PESO,C6_QTDVEN "
		cQuery += " ORDER BY 1,2,3 "
		TcQuery cQuery new Alias T20

		WHILE !T20->(EOF())

			nPeso += T20->B1_PESO * T20->C6_QTDVEN

			T20->(DbSkip())	
		ENDDO


		DbSelectArea("SC5")
		DbSetOrder(1)
		IF DbSeek(T10->C6_FILIAL + T10->C6_NUM)

		RecLock("SC5",.F.)

		SC5->C5_PESOL	:= nPeso
		SC5->C5_PBRUTO	:= nPeso

		MsUnLock()
		SC5->(DbCloseArea())
		ENDIF
		nPeso := 0
		T20->(DbCloseArea())
		T10->(DbSkip())
	ENDDO
	T10->(DbCloseArea())
return
