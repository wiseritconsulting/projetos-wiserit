#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWMBROWSE.CH"
#include "dbtree.ch"
#include "topconn.ch"

/*/{Protheus.doc} SASP036
Medi��o de contratos
@author Diogo
@since 16/09/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User Function SASP036()

	Local oBrowse

	oBrowse:= FWmBrowse():New()
	oBrowse:SetAlias('ZZ2')
	oBrowse:SetDescription('MVC')
	oBrowse:SETMENUDEF('SASP036')
	oBrowse:Activate()

Return //nil


Static Function MenuDef()

	Local aRotina := {}

	ADD OPTION aRotina Title 'Pesquisa'      Action 'PesqBrw'          OPERATION 1 ACCESS 0
	ADD OPTION aRotina Title 'Visualizar'    Action 'VIEWDEF.SASP036'  OPERATION 2 ACCESS 0
	ADD OPTION aRotina Title 'Incluir'       Action 'VIEWDEF.SASP036'  OPERATION 3 ACCESS 0
	ADD OPTION aRotina Title 'Alterar'       Action 'VIEWDEF.SASP036'  OPERATION 4 ACCESS 0
	ADD OPTION aRotina Title 'Excluir'       Action 'VIEWDEF.SASP036'  OPERATION 5 ACCESS 0

Return aRotina

Static Function ModelDef()


	Local oStructSZ1	:= Nil
	Local oStructZZ3 := FWFormStruct(1,'ZZ3')
	Local oModel	 := FWLoadModel( 'SASP031' )

	oModel:aModelStruct[1][4]	:= {}

	oStructZZ3:AddTrigger( ;
	"ZZ3_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ3_TES"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_FTTES36() }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructZZ3:AddTrigger( ;
	"ZZ3_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ3_VLUNIT"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_FRTVNT36() }  )	// [04] Bloco de codigo de execu��o do gatilho



	oStructZZ3:AddTrigger( ;
	"ZZ3_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ3_VTOTAL"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_FRTVT36() }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructZZ3:AddTrigger( ;
	"ZZ3_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ3_DESCRI"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_FRTSC36(@oModel) }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructZZ3:AddTrigger( ;
	"ZZ3_QUANT"		,;										//[01] Id do campo de origem
	"ZZ3_VTOTAL"	,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ3_QUANT") * oModel:GetValue("ZZ3_VLUNIT") }  )	// [04] Bloco de codigo de execu��o do gatilho



	oStructZZ3:AddTrigger( ;
	"ZZ3_QUANT"		,;										//[01] Id do campo de origem
	"ZZ3_SALDO"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_RETD36(@oModel) }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructZZ3:AddTrigger( ;
	"ZZ3_QUANT"		,;										//[01] Id do campo de origem
	"ZZ3_VTOTAL"	,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ3_QUANT") * oModel:GetValue("ZZ3_VLUNIT") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ3:SetProperty("ZZ3_PRODUT",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| ITEMZZ1(oModelGrid,cId,xValor) })

	oStructZZ3:SetProperty("ZZ3_QUANT",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| QUANTZZ3(oModelGrid,cId,xValor) })

	oModel:AddGrid("ZZ3DETAIL", "ZZ0MASTER"/*cOwner*/,oStructZZ3, ,{ |oModelGrid, nLine, cAction, cField| RLINP36(oModelGrid, nLine, cAction, cField) },/*bPre*/,/*bPost*/,{ |oModel, lCopia| LoadZZ3( oModel, lCopia ) }/*Carga*/)

	oModel:SetRelation( 'ZZ3DETAIL', { { 'ZZ3_FILIAL', 'xFilial( "ZZ3" )' }, { 'ZZ3_NUMERO', 'ZZ0_NUMERO' }, { 'ZZ3_CLIENT', 'ZZ0_CLIENT' }, { 'ZZ3_LOJA', 'ZZ0_LOJA' } }, ZZ3->( IndexKey( 1 ) ) )

	// ADD RODAPE , ONDE S�O INFORMADOS OS TOTALIZADORES
	oModel:AddCalc( 'YCADZZ2CALC1', 'ZZ0MASTER', 'ZZ3DETAIL', 'ZZ3_QUANT', 'ZZ3__TOT01', 'SUM',,,'Total Itens',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"QTDMED") }*/ )
	oModel:AddCalc( 'YCADZZ2CALC1', 'ZZ0MASTER', 'ZZ3DETAIL', 'ZZ3_SALDO', 'ZZ3__TOT02', 'SUM',,,'Saldo dos Itens',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)
	oModel:AddCalc( 'YCADZZ2CALC1', 'ZZ0MASTER', 'ZZ3DETAIL', 'ZZ3_VTOTAL', 'ZZ3__TOT03', 'SUM',,,'Valor Total(R$)',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)

	oModel:GetModel("ZZ0MASTER" ):GetStruct():SetProperty("*",MODEL_FIELD_WHEN,{||.F.})
	oModel:SetOnlyQuery('ZZ0MASTER',.T.)
	///oModel:GetModel( 'ZZ3DETAIL' ):SetNoInsertLine()
	oModel:SetCommit({|oModel| ZZ3Commit(oModel) },.F.)

Return (oModel)

Static Function ViewDef()

	Local oModel := FWLoadModel('SASP036')
	Local oStructZZ0:= FWFormStruct(2,'ZZ0')
	Local oStructZZ3:= FWFormStruct(2,'ZZ3',{|cCampo| !(Alltrim(cCampo) $ "ZZ3_MEDICA")})
	Local oView:=FWFormView():New()

	oCalc1	:= FWCalcStruct( oModel:GetModel( 'YCADZZ2CALC1') )

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField('ZZ0MASTER',oStructZZ0)
	oView:AddGrid('ZZ3DETAIL',oStructZZ3)
	oView:AddField( 'VIEW_CALC', oCalc1, 'YCADZZ2CALC1' )

	oView:CreateHorizontalBox('SUPERIOR',40)
	oView:CreateHorizontalBox('INFERIOR',50)
	oView:CreateHorizontalBox('RODAPE',10)

	oView:SetOwnerView('ZZ0MASTER','SUPERIOR')
	oView:SetOwnerView('ZZ3DETAIL','INFERIOR')
	oView:SetOwnerView( 'VIEW_CALC', 'RODAPE' )

	oView:EnableTitleView('ZZ3DETAIL','Itens Medi��o')

	oView:SetProgressBar(.T.)

	oView:AddUserButton( 'Quantidade Auto', 'CLIPS', { |oView| YSASPO36(oView,oModel) }, , VK_F10,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} )
	oView:AddUserButton( 'Quantidade Item', 'CLIPS', { |oView| YSASPO36Ln(oView,oModel) }, , VK_F11,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} )

	oView:SetViewAction( 'BUTTONOK' ,                { |oView|         SetKey (VK_F10, nil),SetKey (VK_F11, nil) } )
	oView:SetViewAction( 'BUTTONCANCEL',             { |oView|         SetKey (VK_F10, nil),SetKey (VK_F11, nil) } )

Return oView


STATIC FUNCTION LoadZZ3(oGrid,lCopia)
	Local aArea   := GetArea()
	Local aFields := {}
	Local aRet    := {}
	Local cTmp    := ''
	Local cTmp1   := ''
	Local cQuery  := ''
	Local cFields := 'R_E_C_N_O_'
	Local nTam
	Local aStruct	:= oGrid:oFormModelStruct:GetFields()
	Local cProdIni	:= cProdFim	:= Space(GetSx3Cache("B1_COD","X3_TAMANHO"))
	Local aCategoria:= {'','ALUNO','PROFESSOR','COORDENA��O','OUTROS'}
	Local cTes		:= Space(GetSx3Cache("ZZ1_TES","X3_TAMANHO"))
	Local aParam	:= {}
	Local aRetParm	:= {}
	Local lOk		:= .F.
	Local cSerie	:= Space(GetSx3Cache("B1_YSERIE","X3_TAMANHO"))
	Local cEnvio	:= Space(GetSx3Cache("ZZ7_ENVIO","X3_TAMANHO"))
	private cFat		:={'TRIANGULAR','DIRETO'}


	Public  cTipMed	:= ""
	Public  dSolicita
	public _tpfat

	aAdd(aParam,{1,"Itens De"		,cProdIni	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{1,"Itens Ate"	,cProdFim	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{2,"Categoria"		,,aCategoria,80,,.F.,})
	aAdd(aParam,{1,"Tes"			,cTes		,GetSx3Cache("SF4_CODIGO","X3_PICTURE"),".T.","SF4",".T.",80,.F.})
	aAdd(aParam,{1,"Tipo de medi��o"	,Space(GetSx3Cache("ZZG_COD","X3_TAMANHO")),GetSx3Cache("ZZG_COD","X3_PICTURE"),".T.","ZZG",".T.",80,.T.})
	aAdd(aParam,{1,"Data Solicita��o"	,date(),"",".T.","",".T.",80,.T.})
	aAdd(aParam,{1,"Serie"				,cSerie	,GetSx3Cache("B1_YSERIE","X3_PICTURE"),".T.","ZZO",".T.",80,.F.})
	aAdd(aParam,{1,"Envio"				,cEnvio ,GetSx3Cache("B1_YVOL","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{2,"Faturamento"		,,cFat,80,,.F.,})

	If ParamBox(aParam,"Filtrar Medi��o",@aRetParm,{||.T.},,,,,,"U_SASP036",.T.,.T.)
		lOk	:= .T.
	EndIf

	oModel 		:= FWModelActive()

	// Pega campos que fazem parte da estrutura do objeto, para otimizar retorno da query
	cTmp   := GetNextAliwesas()
	cQuery := " SELECT ZZ1.*, ZZ7_DESCR, ZZ7_ENVIO,  ZZ7_PROCPL, ZZ7_PRODUT "
	cQuery += " FROM "+RetSqlName( 'ZZ1' ) + " ZZ1 "
	cQuery += " JOIN "+RetSqlName( 'ZZ7' ) + " ZZ7 "
	cQuery += " ON ZZ1.ZZ1_PRODUT = ZZ7.ZZ7_CODIGO "
	cQuery += " WHERE ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
	cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' "
	cQuery += " AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' "
	cQuery += " AND ZZ1_LOJA   = '"+ZZ0->ZZ0_LOJA+"' "
	cQuery += " AND ZZ1_STATUS <> 'I' "
	
	If lOk
		if !Empty(aRetParm[5])
			cTipMed := aRetParm[5]
		endif

		if !Empty(aRetParm[6])
			dSolicita := aRetParm[6]
		endif

		cSer	:= aRetParm[7]
		cEnv	:= aRetParm[8]

		cQuery += " AND ZZ1_PRODUT >='"+aRetParm[1]+"' "
		cQuery += " and ZZ1_PRODUT <='"+aRetParm[2]+"' "

		If !Empty(aRetParm[3])
			cQuery += " AND ZZ1_PRODUT IN( "
			cQuery += " select ZZ7_CODIGO AS PRODUTO "
			cQuery += " from "+RetSqlName( 'ZZ7' ) + " ZZ7 "
			cQuery += " INNER JOIN "+RetSqlName( 'ZZ1' ) + " ZZ1 ON ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
			cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' AND ZZ1_LOJA   = '"+ZZ0->ZZ0_LOJA+"' AND ZZ1_PRODUT >='"+aRetParm[1]+"' "
			cQuery += " and ZZ1_PRODUT <='"+aRetParm[2]+"' AND ZZ1_PRODUT=ZZ7_CODIGO AND ZZ1.D_E_L_E_T_=' ' "

			If !empty(cEnv)
				cQuery += " and ZZ7_ENVIO = '"+cEnv+"' "
			Endif

			If !empty(cSer)
				cQuery += " and ZZ7_SERIE = '"+cSer+"' "
			Endif

			cQuery += " WHERE ZZ7_CATEGO ='"+substr(aRetParm[3],1,1)+"' "

			cQuery += " UNION ALL "

			cQuery += " SELECT B1_COD AS PRODUTO "
			cQuery += " FROM "+RetSqlName( 'SB1' ) + " SB1 "
			cQuery += " INNER JOIN "+RetSqlName( 'ZZ1' ) + " ZZ1 ON ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
			cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' AND ZZ1_LOJA   = '"+ZZ0->ZZ0_LOJA+"'  AND ZZ1_PRODUT >='"+aRetParm[1]+"' "
			cQuery += " and ZZ1_PRODUT <='"+aRetParm[2]+"'  AND ZZ1_PRODUT=B1_COD AND ZZ1.D_E_L_E_T_=' ' "
			If !empty(cEnv)
				cQuery += " and B1_YVOL = '"+cEnv+"' "
			Endif

			If !empty(cSer)
				cQuery += " and B1_YSERIE = '"+cSer+"' "
			Endif

			cQuery += " WHERE B1_YCATEG='"+substr(aRetParm[3],1,1)+"' "
			cQuery += " ) "
		ENDIF

		// TODO Filtro Serie/Envio - SAS
		// Rafael Pinheiro - 06/10/2015
		// Incluido para permitir o filtro do serie e envio quando categoria estiver em branco

		If Empty(aRetParm[3])
			cQuery += " AND ZZ1_PRODUT IN( "
			cQuery += " select ZZ7_CODIGO AS PRODUTO "
			cQuery += " from "+RetSqlName( 'ZZ7' ) + " ZZ7 "
			cQuery += " INNER JOIN "+RetSqlName( 'ZZ1' ) + " ZZ1 ON ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
			cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' AND ZZ1_LOJA   = '"+ZZ0->ZZ0_LOJA+"'  AND ZZ1_PRODUT >='"+aRetParm[1]+"' "
			cQuery += " and ZZ1_PRODUT <='"+aRetParm[2]+"' AND ZZ1_PRODUT=ZZ7_CODIGO AND ZZ1.D_E_L_E_T_=' ' "

			If !empty(cEnv)
				cQuery += " and ZZ7_ENVIO = '"+cEnv+"' "
			Endif

			If !empty(cSer)
				cQuery += " and ZZ7_SERIE = '"+cSer+"' "
			Endif

			cQuery += " UNION ALL "

			cQuery += " SELECT B1_COD AS PRODUTO "
			cQuery += " FROM "+RetSqlName( 'SB1' ) + " SB1 "
			cQuery += " INNER JOIN "+RetSqlName( 'ZZ1' ) + " ZZ1 ON ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
			cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' AND ZZ1_LOJA   = '"+ZZ0->ZZ0_LOJA+"' AND ZZ1_PRODUT >='"+aRetParm[1]+"' "
			cQuery += " and ZZ1_PRODUT <='"+aRetParm[2]+"'  AND ZZ1_PRODUT=B1_COD AND ZZ1.D_E_L_E_T_=' ' "
			If !empty(cEnv)
				cQuery += " and B1_YVOL = '"+cEnv+"' "
			Endif

			If !empty(cSer)
				cQuery += " and B1_YSERIE = '"+cSer+"' "
			Endif

			cQuery += " ) "
		ENDIF

		//FIM - Filtro Serie/Envio - SAS


		If !Empty(aRetParm[4])
			cQuery += " AND ZZ1_TES='"+aRetParm[4]+"' "
		EndIf
	ENDIF
	cQuery += " AND ZZ1.D_E_L_E_T_=' '"
	cQuery += " AND ZZ7.D_E_L_E_T_=' '"
	cQuery += " AND ZZ1_STATUS <> 'I' "
	//cQuery += " ORDER BY ZZ1_ITEM "


	cQuery += " UNION ALL "

	cQuery += " SELECT ZZ1.*, ' ' AS ZZ7_DESCR, ' ' AS ZZ7_ENVIO, ' ' AS ZZ7_PROCPL,' ' AS ZZ7_PRODUT  "
	cQuery += " FROM "+RetSqlName( 'ZZ1' ) + " ZZ1 "
	cQuery += " JOIN "+RetSqlName( 'SB1' ) + " SB1 "
	cQuery += " ON ZZ1.ZZ1_PRODUT = SB1.B1_COD "
	cQuery += " WHERE ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
	cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' "
	cQuery += " AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' "
	cQuery += " AND ZZ1_LOJA   = '"+ZZ0->ZZ0_LOJA+"' "

	If lOk
		if !Empty(aRetParm[5])
			cTipMed := aRetParm[5]
		endif

		if !Empty(aRetParm[6])
			dSolicita := aRetParm[6]
		endif

		cSer	:= aRetParm[7]
		cEnv	:= aRetParm[8]

		cQuery += " AND ZZ1_PRODUT >='"+aRetParm[1]+"' "
		cQuery += " and ZZ1_PRODUT <='"+aRetParm[2]+"' "

		If !Empty(aRetParm[3])
			cQuery += " AND ZZ1_PRODUT IN( "
			cQuery += " select ZZ7_CODIGO AS PRODUTO "
			cQuery += " from "+RetSqlName( 'ZZ7' ) + " ZZ7 "
			cQuery += " INNER JOIN "+RetSqlName( 'ZZ1' ) + " ZZ1 ON ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
			cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' AND ZZ1_LOJA   = '"+ZZ0->ZZ0_LOJA+"' AND ZZ1_PRODUT >='"+aRetParm[1]+"' "
			cQuery += " and ZZ1_PRODUT <='"+aRetParm[2]+"' AND ZZ1_PRODUT=ZZ7_CODIGO AND ZZ1.D_E_L_E_T_=' ' "

			If !empty(cEnv)
				cQuery += " and ZZ7_ENVIO = '"+cEnv+"' "
			Endif

			If !empty(cSer)
				cQuery += " and ZZ7_SERIE = '"+cSer+"' "
			Endif

			cQuery += " WHERE ZZ7_CATEGO ='"+substr(aRetParm[3],1,1)+"' "

			cQuery += " UNION ALL "

			cQuery += " SELECT B1_COD AS PRODUTO "
			cQuery += " FROM "+RetSqlName( 'SB1' ) + " SB1 "
			cQuery += " INNER JOIN "+RetSqlName( 'ZZ1' ) + " ZZ1 ON ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
			cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' AND ZZ1_LOJA   = '"+ZZ0->ZZ0_LOJA+"' AND ZZ1_PRODUT >='"+aRetParm[1]+"' "
			cQuery += " and ZZ1_PRODUT <='"+aRetParm[2]+"'  AND ZZ1_PRODUT=B1_COD AND ZZ1.D_E_L_E_T_=' ' "
			If !empty(cEnv)
				cQuery += " and B1_YVOL = '"+cEnv+"' "
			Endif

			If !empty(cSer)
				cQuery += " and B1_YSERIE = '"+cSer+"' "
			Endif

			cQuery += " WHERE B1_YCATEG='"+substr(aRetParm[3],1,1)+"' "
			cQuery += " ) "
		ENDIF

		// TODO Filtro Serie/Envio - SAS
		// Rafael Pinheiro - 06/10/2015
		// Incluido para permitir o filtro do serie e envio quando categoria estiver em branco

		If Empty(aRetParm[3])
			cQuery += " AND ZZ1_PRODUT IN( "
			cQuery += " select ZZ7_CODIGO AS PRODUTO "
			cQuery += " from "+RetSqlName( 'ZZ7' ) + " ZZ7 "
			cQuery += " INNER JOIN "+RetSqlName( 'ZZ1' ) + " ZZ1 ON ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
			cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' AND ZZ1_LOJA   = '"+ZZ0->ZZ0_LOJA+"'  AND ZZ1_PRODUT >='"+aRetParm[1]+"' "
			cQuery += " and ZZ1_PRODUT <='"+aRetParm[2]+"' AND ZZ1_PRODUT=ZZ7_CODIGO AND ZZ1.D_E_L_E_T_=' ' "

			If !empty(cEnv)
				cQuery += " and ZZ7_ENVIO = '"+cEnv+"' "
			Endif

			If !empty(cSer)
				cQuery += " and ZZ7_SERIE = '"+cSer+"' "
			Endif

			cQuery += " UNION ALL "

			cQuery += " SELECT B1_COD AS PRODUTO "
			cQuery += " FROM "+RetSqlName( 'SB1' ) + " SB1 "
			cQuery += " INNER JOIN "+RetSqlName( 'ZZ1' ) + " ZZ1 ON ZZ1_FILIAL='"+ xFilial( 'ZZ1' ) + "' "
			cQuery += " AND ZZ1_NUMERO = '"+ZZ0->ZZ0_NUMERO+"' AND ZZ1_CLIENT = '"+ZZ0->ZZ0_CLIENT+"' AND ZZ1_LOJA   = '"+ZZ0->ZZ0_LOJA+"' AND ZZ1_PRODUT >='"+aRetParm[1]+"' "
			cQuery += " and ZZ1_PRODUT <='"+aRetParm[2]+"'  AND ZZ1_PRODUT=B1_COD AND ZZ1.D_E_L_E_T_=' ' "
			If !empty(cEnv)
				cQuery += " and B1_YVOL = '"+cEnv+"' "
			Endif

			If !empty(cSer)
				cQuery += " and B1_YSERIE = '"+cSer+"' "
			Endif

			cQuery += " ) "
		ENDIF

		//FIM - Filtro Serie/Envio - SAS


		If !Empty(aRetParm[4])
			cQuery += " AND ZZ1_TES='"+aRetParm[4]+"' "
		EndIf
	ENDIF
	cQuery += " AND ZZ1.D_E_L_E_T_=' '"
	cQuery += " AND SB1.D_E_L_E_T_=' '"
	cQuery += " AND ZZ1_STATUS <> 'I' "
	cQuery += " ORDER BY ZZ1_ITEM "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.F.,.T.)
	_tpfat:=aRetParm[9]
	if ProdOk(oGrid,cQuery,aRetParm[9]) == .F.
		RETURN aRet
	endif

	While (cTmp)->(!EOF())

		IF (cTmp)->ZZ1_STATUS == "I" .OR. (cTmp)->ZZ1_SALDO == 0
			(cTmp)->(DbSkip())
			loop
		ENDIF

		cStatus	:=	Posicione("ZZ0",1,xFilial("ZZ0")+(cTmp)->ZZ1_NUMERO+(cTmp)->ZZ1_CLIENT+(cTmp)->ZZ1_LOJA ,"ZZ0_STATUS" )
		cCliBlq	:=	Posicione("SA1",1,xFilial("SA1")+(cTmp)->ZZ1_CLIENT+(cTmp)->ZZ1_LOJA ,"A1_MSBLQL" )
		cDuplic	:=	Posicione("SF4",1,xFilial("SF4",(cTmp)->ZZ1_FILIAL)+(cTmp)->ZZ1_TES,"F4_DUPLIC")

		//Se cliente bloqueado e TES gera financeiro -> N�O Permitir gerar medi��o
		IF cCliBlq == "1" .and.  cDuplic == 'S'
			(cTmp)->(DbSkip())
			loop
		ENDIF

		//Se contrato bloqueado n�o gera Medi��o
		IF cStatus == "B"
			(cTmp)->(DbSkip())
			loop
		ENDIF
		
		AADD(aRet,{0,{}})
		nTam	:= Len(aRet)
		For nCont:=1 To Len(aStruct)

			AADD(aRet[nTam][2],CriaVar(aStruct[nCont][MODEL_FIELD_IDFIELD]))
			If aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_FILIAL"
				aRet[nTam][2][nCont]	:= cFilant
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_NUMERO"
				aRet[nTam][2][nCont]	:= ZZ0->ZZ0_NUMERO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_ITEM"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_ITEM
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_CLIENT"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_CLIENT
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_LOJA"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_LOJA
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_TES"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_TES
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_PRODUT"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_PRODUT

				If GETMV("SA_SB9")

					PROCESSA({|| ARMAZEMZZ3((cTmp)->ZZ1_PRODUT,(cTmp)->ZZ1_TIPO) }, "Verificando armazem!",,.F. )

				EndIf

			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_DESCRI"
				IF (cTmp)->ZZ1_TIPO == 'P'
					aRet[nTam][2][nCont]	:= POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM((cTmp)->ZZ1_PRODUT),"B1_DESC")
				ELSEIF (cTmp)->ZZ1_TIPO == 'K'
					aRet[nTam][2][nCont]	:= POSICIONE("ZZ7",1,XFILIAL("ZZ7")+ALLTRIM((cTmp)->ZZ1_PRODUT),"ZZ7_DESCR")	
				ENDIF
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_QUANT"
				aRet[nTam][2][nCont]	:= 0//(cTmp)->ZZ1_SALDO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_VLUNIT"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_VLRUN
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_TOTAL"
				aRet[nTam][2][nCont]	:= 0//(cTmp)->ZZ1_SALDO*(cTmp)->ZZ1_VLRUN
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_SALDO"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_SALDO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_TIPO"
				aRet[nTam][2][nCont]	:= (cTmp)->ZZ1_TIPO

			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD] $ "ZZ3_ENVIO|ZZ3_ENVPRV|"
				IF (cTmp)->ZZ1_TIPO == 'P'
					aRet[nTam][2][nCont]	:= POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM((cTmp)->ZZ1_PRODUT),"B1_YVOL")
				ELSEIF (cTmp)->ZZ1_TIPO == 'K'
					aRet[nTam][2][nCont]	:= POSICIONE("ZZ7",1,XFILIAL("ZZ7")+ALLTRIM((cTmp)->ZZ1_PRODUT),"ZZ7_ENVIO")
				ENDIF
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="ZZ3_PRODCP"   	
				if cTipMEd $ GETMV("SA_PEDCPL")   //cTipMed == '000001' 
					aRet[nTam][2][nCont]	:= (cTmp)->ZZ7_PROCPL
				ENDIF
			EndIf
		Next
		(cTmp)->(DbSkip())
	EndDo
	(cTmp)->(dbCloseArea())
	
		if Select("cTmp1") > 0
			cTmp1->(dbCloseArea())
		Endif
	
		cQuery := " SELECT COUNT(ZZ1_PRODUT) AS QTD FROM " + retsqlname('ZZ1') + " ZZ1 (NOLOCK) "
		cQuery += " JOIN " + retsqlname('SB1') + " SB1 " 
		cQuery += " ON ZZ1.ZZ1_PRODUT = SB1.B1_COD "
		cQuery += " WHERE ZZ1.D_E_L_E_T_ = '' AND SB1.D_E_L_E_T_ = ''AND SB1.B1_YTIPO = '12' AND ZZ1.ZZ1_STATUS = 'A' "
		cQuery += " AND ZZ1.ZZ1_FILIAL = '"+ZZ1->ZZ1_FILIAL+"' AND ZZ1.ZZ1_NUMERO = '"+ZZ1->ZZ1_NUMERO+"' "
		cQuery += " AND ZZ1.ZZ1_CLIENT = '"+ZZ1->ZZ1_CLIENT+"' AND ZZ1.ZZ1_LOJA = '"+ZZ1->ZZ1_LOJA+"' "
		
		TCQUERY cQuery NEW ALIAS "cTmp1"	
		
		IF cTipMEd $ GETMV("SA_PEDCPL") .AND. !EMPTY(cTmp1->QTD)
			MsgAlert("Por favor, gerar pedido complementar de material adapta��o")
		ENDIF
	
	RestArea(aArea)	

Return aRet

STATIC FUNCTION ARMAZEMZZ3(cProdut,cTipo)

	Local cFilanterior := ""
	Local cAreaAnterior := getarea()
	Local lMsErroAuto  := .F.

	procregua(100)

	if cTipo == "K"

		cquery1 := " SELECT ZZ7_PRODUT,ZZ7_QUANT,ZZ7_PERDSC, ZZ7_CATEGO  "
		cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 "
		cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
		cquery1 += " and D_E_L_E_T_ <> '*' "
		cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(cProdut) +"' "
		cquery1 += " and ZZ7_TIPO = 'A' "
		cquery1 += " ORDER BY ZZ7_PRODUT"
		TCQuery cquery1 new alias T03

		//cFilanterior := cfilant


		WHILE !T03->(EOF())

			dbSelectArea("SM0")
			SET FILTER TO M0_CODIGO = '01'
			dbGOTOP()                 // B9_FILIAL+B9_COD+B9_LOCAL+B9_DATA
			WHILE ! SM0->(EOF())

				/*				cFil := '100110'
				cFil2 := '110101'
				IF Alltrim(SM0->M0_CODFIL) == Alltrim(cFil) .OR. Alltrim(SM0->M0_CODFIL) == Alltrim(cFil2)
				SM0->(DbSkip())
				T03->(DBCLOSEAREA())
				RestArea(cAreaAnterior)
				return
				ENDIF			
				*/				

				cquery1 := " SELECT *  "
				cquery1 += " from " + retsqlname('SB9') + " SB9 "
				cquery1 += " where B9_FILIAL = '" + SM0->M0_CODFIL + "' "
				cquery1 += " and D_E_L_E_T_ = '' "
				cquery1 += " and B9_COD = '"+ T03->ZZ7_PRODUT +"' "
				cquery1 += " and B9_LOCAL = '"+GETMV('SA_LOCFAT')+"' "
				TCQuery cquery1 new alias T07

				IF  T07->(eof())    // Avalia o retorno da pesquisa realizada

					cFilanterior := cfilant

					Begin Transaction
						PARAMIXB1 := {}
						aadd(PARAMIXB1,{"B9_FILIAL",xfilial("SB9",SM0->M0_CODFIL),})
						aadd(PARAMIXB1,{"B9_COD",alltrim(T03->ZZ7_PRODUT),})
						aadd(PARAMIXB1,{"B9_LOCAL", GETMV('SA_LOCFAT'),})
						aadd(PARAMIXB1,{"B9_QINI",0,})
						cfilant := SM0->M0_CODFIL
						MSExecAuto({|x,y| mata220(x,y)},PARAMIXB1,3)
						cfilant := cFilanterior
						If lMsErroAuto
							mostraerro()
						EndIf
					End Transaction

					incproc("Aguarde...")
				ENDIF
				T07->(DBCLOSEAREA())
				SM0->(DbSkip())
				//cfilant := cFilanterior
			ENDDO
			T03->(DbSkip())
		ENDDO
		T03->(DBCLOSEAREA())

	elseif cTipo == "P"

		dbSelectArea("SM0")
		SET FILTER TO M0_CODIGO = '01'
		dbGOTOP()                 // B9_FILIAL+B9_COD+B9_LOCAL+B9_DATA
		WHILE ! SM0->(EOF())
			/*cFil := '100110'
			cFil2 := '110101'
			IF Alltrim(SM0->M0_CODFIL) == Alltrim(cFil) .OR. Alltrim(SM0->M0_CODFIL) == Alltrim(cFil2)
			SM0->(DbSkip())
			RestArea(cAreaAnterior)
			return
			ENDIF
			*/

			cquery1 := " SELECT *  "
			cquery1 += " from " + retsqlname('SB9') + " SB9 "
			cquery1 += " where B9_FILIAL = '" + SM0->M0_CODFIL + "' "
			cquery1 += " and D_E_L_E_T_ = '' "
			cquery1 += " and B9_COD = '"+ cProdut +"' "
			cquery1 += " and B9_LOCAL = '"+GETMV('SA_LOCFAT')+"' "
			TCQuery cquery1 new alias T07

			IF  T07->(eof())    // Avalia o retorno da pesquisa realizada

				cFilanterior := cfilant
				Begin Transaction
					PARAMIXB1 := {}
					aadd(PARAMIXB1,{"B9_FILIAL",xfilial("SB9",SM0->M0_CODFIL),})
					aadd(PARAMIXB1,{"B9_COD",alltrim(cProdut),})
					aadd(PARAMIXB1,{"B9_LOCAL", GETMV('SA_LOCFAT'),})
					aadd(PARAMIXB1,{"B9_QINI",0,})
					cfilant := SM0->M0_CODFIL
					MSExecAuto({|x,y| mata220(x,y)},PARAMIXB1,3)
					cfilant := cFilanterior
					If lMsErroAuto
						mostraerro()
					EndIf
				End Transaction

			ENDIF
			T07->(DBCLOSEAREA())
			SM0->(DbSkip())
		ENDDO
	endif
	RestArea(cAreaAnterior)

RETURN

Static Function ZZ3Commit(oModel)
	Local cContrato	:= oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_NUMERO')
	Local cCNPJ		:= FWArrFilAtu()[18] //Coligada
	Local nSaldo		:= 0
	Local nQtd			:= 0
	Local lRet			:= .T.
	Local nCont,nCont2
	Local cMedicao
	Local nbkpValVen
	Local aCab			:= {}
	Local aItens		:= {}
	Local aItens2		:= {}
	Local nQtdMed		:= 0
	Local nTam
	Local aStruct	:= oModel:GetModel( 'ZZ3DETAIL' ):oFormModelStruct:GetFields()
	Local oModelSZ1	:= oModel:GetModel('ZZ0MASTER')
	Local oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')
	Local aCliente	:= {}
	Local nContIT		:= 0
	Local nVlrItem	:= 0
	Local nVlrTotal	:= 0
	Local nVlrAdiant	:= 0
	Local lKit
	Local cTabPad		        := SuperGetMV("SA_TABPREC")
	Local cTab02                := SuperGetMV("SA_TABPR2")
	Local cTipoPrincipal		:= SuperGetMV("SA_TIPOMED")
	Local cTipoComplementar     := SuperGetMV("SA_TPCOMPL")
	Local nj
	Local aLocks		:= {}
	Local oView		:= FWViewActive()
	Local nValVenda := 1
	//MUDAN�A DE FILIAL QUANDO O FATURAMENTO DOR DIRETO
	Local cNewFil := ""
	Local aParam	:= {}
	Local aRetParm		:= {}
	Local lOk		:= .F.
	//TELA TIPO DE MEDI��O
	Local oButton1
	Local oButton2
	Local NCOMBOBO1
	Local oCheckBo1
	Local lCheckBo1 := .F.
	Local oCheckBo2
	Local lCheckBo2 := .F.
	Local nOpca := 0
	Local oSay1
	Local oDlg
	Local nValFret := 0
	//--------------------
	Local cFilorig := ""
	Local cFil := ""
	//RETORNA TODAS AS FILIAIS
	Local cAreabkp := getarea()
	Local cFilanterior := ""
	Local cFilopc := ""
	//Local aFilial := FWAllFilial()
	//----------------
	Private cTesTmp		:= "900"	//Tes temporaria para pedido

	Private nFret		:= 0
	Private nDesc		:= 0
	Private lMsErroAuto	:= .F.
	Private aValVenda := {}
	Private aFilTri :={'010105','020101'} //Filial da Holding

	if ALLTRIM(funname()) <> "RPC"
		oView	:= FWViewActive()
	else
		oView	:= oViewWS 	
		dSolicita := dDtSolWs

	ENDIF	

	IF LEN(omodel:getmodel('ZZ3DETAIL'):ACOLS) == 0
		RETURN .F.
	ENDIF
	// TELA PARA DEFINIR O TIPO DE MEDI��O  DIRETA OU TRIANGULAR
	IF oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FATDIR') == "3"
		WHILE nOpca == 0

			DEFINE MSDIALOG oDlg TITLE "TIPO DE MEDI��O" FROM 000, 000  TO 150, 300 COLORS 0, 16777215 PIXEL

			@ 012, 071 MSCOMBOBOX oComboBo1 VAR nComboBo1 ITEMS {_tpfat} SIZE 072, 010 OF oDlg COLORS 0, 16777215 PIXEL
			@ 012, 005 SAY oSay1 PROMPT "Tipo de Medi��o:" SIZE 049, 010 OF oDlg COLORS 0, 16777215 PIXEL
			@ 039, 075 BUTTON oButton1 PROMPT "OK" SIZE 037, 012 OF oDlg ACTION  (nOpca:=1,oDlg:end()) PIXEL
			@ 039, 115 BUTTON oButton2 PROMPT "Cancelar" SIZE 037, 012 OF oDlg ACTION  (nOpca:=2,oDlg:end()) PIXEL

			ACTIVATE MSDIALOG oDlg

		ENDDO
		IF nOpca == 2
			Return .F.
		ENDIF
		IF  nComboBo1 == "DIRETO"
			lCheckBo1 := .T.
			lCheckBo2 := .F.
		ELSEIF nComboBo1 == "TRIANGULAR"
			lCheckBo1 := .F.
			lCheckBo2 := .T.
		ENDIF
	ENDIF

	//TODO Weskley Silva: Nova Filial para triangula��o
	//Data: 29/09/2016

	IF oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FATDIR') == "2" .OR. (lCheckBo2 .AND. ! lCheckBo1)
		aAdd(aParam,{2,"Filial da Holding"		,'010105',aFilTri,50,"",.T.})
		If ParamBox(aParam,"Escolha de Filial",@aRetParm,{||.T.},,,,,,"U_SASP036",.T.,.T.)

			cFilOpc := aRetParm[1]
		else
			Alert("Cancelado pelo usuario")
			return
		endif
	Endif

	//TODO Tabela de pre�o da Filial 020101
	// Weskley Silva 04/10/2016
	IF cFilOpc == "020101"
		cTabPad := ALLTRIM(cTab02)
	endif	
	///------------------------------------------------------------


	IF oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FATDIR') == "1" .OR. (lCheckBo1 .AND. ! lCheckBo2)

		aAdd(aParam,{1,"Filial"		,space(GetSx3Cache("ZZ0_FILIAL","X3_TAMANHO"))	,GetSx3Cache("ZZ0_FILIAL","X3_PICTURE")," !Vazio()","SM0",".T.",80,.F.})

		If ParamBox(aParam,"Escolha de Filial",@aRetParm,{||.T.},,,,,,"U_SASP036",.T.,.T.)
			if EXISTCPO("SM0",cEmpAnt+aRetParm[1])
				lOk	:= .T.
			else
				alert("Filial digitada: "+aRetParm[1]+" n�o � valida , ser� considerada a filial corrente ")
			endif

		EndIf

		if lOk
			cNewFil := aRetParm[1]
		endif
	ENDIF
	//----------------------------------------------------------

	SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
	If SA1->(DbSeek(xFilial("SA1")+cCNPJ))
		cCliCol	:= SA1->A1_COD
		cLjCol	:= SA1->A1_LOJA
		cTabPad2:= iif(empty(SA1->A1_TABELA),cTabPad,SA1->A1_TABELA)
	EndIf

	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
	ZZ0->(DbSetOrder(1))	//ZZ0_FILIAL+ZZ0_NUMERO
	ZZ1->(DbSetOrder(1))	//ZZ1_FILIAL+ZZ1_CONTRAT+ZZ1_ITEM
	SA1->(dbSetOrder(1))	//A1_FILIAL+A1_COD
	SB1->(DbSetOrder(1))	//B1_FILIAL+B1_COD
	SB2->(DbSetOrder(1))	//B2_FILIAL+B2_COD+B2_LOCAL

	cEndE:= ""
	If SA1->(dbSeek(xFilial("SA1")+oModelSZ1:GetValue('ZZ0_CLIENT')+oModelSZ1:GetValue('ZZ0_LOJA'))) .AND. !Empty(SA1->A1_Tabela)
		cEndE	:=	Alltrim(SA1->A1_ENDENT)
	EndIf

	IF oModel:getOperation() == MODEL_OPERATION_UPDATE .OR. oModel:getOperation() == MODEL_OPERATION_INSERT

		//REGRA PARA MONTAR O SEQUENCIAL DA MEDI��O POR CONTRATO
		cquery2 := " select MAX(ZZ2_MEDICA) AS ZZ2_MEDICA "
		cquery2 += " from " + retsqlname('ZZ2') + " ZZ2 "
		cquery2 += " where ZZ2_FILIAL = '" + xfilial("ZZ2") + "' "
		cquery2 += " and ZZ2_NUMERO = '"+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')+"' "
		cquery2 += " AND ZZ2_CLIENT = '"+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+"' "
		cquery2 += " AND ZZ2_LOJA = '"+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')+"' " 

		TCQuery cquery2 new alias T02
		DBSELECTAREA("T02")
		IF Alltrim(T02->ZZ2_MEDICA) == ""
			cMedicao := STRZERO(1,GetSx3Cache("ZZ2_MEDICA","X3_TAMANHO"),0)
		ELSE
			cMedicao := STRZERO(VAL(T02->ZZ2_MEDICA)+1,GetSx3Cache("ZZ2_MEDICA","X3_TAMANHO"),0)
		ENDIF
		T02->(DBCLOSEAREA())
	//-------------------------------------------------------

		cNaturez:= space(GetSx3Cache("ZZ2_MEDICA","X3_TAMANHO"))
		Public cLocExp := space(GetSx3Cache("ZZ2_LOCEX","X3_TAMANHO"))
		cObsPd  := space(200)
		cObsNF  := cEndE

		aPergs	:= {}
		aRet	:= {}
		aAdd( aPergs ,{1,'Natureza',cNaturez,'@!',"Vazio() .OR. ExistCpo('SED')","SED",'.T.',60,.F.})
		aAdd( aPergs ,{1,'Observacao',cObsPd,'@!',"","",'.T.',60,.F.})
		aAdd( aPergs ,{1,'Local Exp.',cLocExp,'@!',"","X2",'.T.',60,.T.})

		if alltrim(funname()) <> "RPC"
			If ParamBox(aPergs,"Natureza",aRet,,,,,,,"_CDZZ3",.F.,.F.)

				cNaturez	:= aRet[1]
				cObsPd		:= aRet[2]
				cLocExp		:= aRet[3]

			Else
				oModel:SetErrorMessage('ZZ3DETAIL',,,,"ERRO", 'Opera��o cancelada pelo usuario!', '')
				oModel:lModify	:= .T.
				oView:oModel:lModify	:= .T.
				Return .F.
			endif
		ELSE	
			cNaturez	:= "" //cNatuWS
			cObsPd		:= cObWS
			cLocExp		:= cLocWs  
		ENDIF

		If Empty(cNaturez) .AND. alltrim(funname()) <> "RPC"
			oModel:SetErrorMessage('ZZ3DETAIL',,,,"ERRO", 'Necessario informar a natureza!', 'Informe a natureza!')
			oModel:lModify	:= .T.
			oView:oModel:lModify	:= .T.
			Return .F.
		EndIf

		if alltrim(funname()) == "RPC"
			cTipMed := cTpMedws
		endif

		If empty(dtos(validTipo(alltrim(cTipMed),oModel)))
			oModel:SetErrorMessage('SZ3DETAIL',,,,"ERRO", 'Medi��o n�o poder� ser confirmada', 'Verifique parametriza��o da Data M�xima de Entrega')
			oModel:lModify	:= .T.
			oView:oModel:lModify	:= .T.
			Return .F.
		Endif


		Begin Transaction

		SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
		If SA1->(DbSeek(xFilial("SA1")+cCNPJ))
			AADD(aLocks,{"SA1",SA1->(Recno())})
		EndIf

		dbSelectArea("ZZ2")
		RECLOCK("ZZ2", .T.)

		ZZ2->ZZ2_FILIAL	:= cFilant //xFilial("ZZ3")
		ZZ2->ZZ2_NUMERO	:= oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')
		ZZ2->ZZ2_CLIENT := oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')
		ZZ2->ZZ2_LOJA   := oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')
		ZZ2->ZZ2_NOME   := oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NOME')
		ZZ2->ZZ2_EST	:= POSICIONE("SA1",1,xFilial("SA1") + oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT') + oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'),"A1_EST")
		ZZ2->ZZ2_MEDICA := cMedicao //SEQUENCIAL
		ZZ2->ZZ2_LOCEX	:= cLocExp
		ZZ2->ZZ2_OBS	:= cObsPd
		ZZ2->ZZ2_DTGER  := DATE()
		ZZ2->ZZ2_HRGER  := TIME()
		ZZ2->ZZ2_STATUS := oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_STATUS')
		ZZ2->ZZ2_TIPO   := cTipMed
		ZZ2->ZZ2_DTSOLI := dSolicita
		IF ALLTRIM(FUNNAME()) == "RPC" 
			ZZ2->ZZ2_DTPREV	:= DATE()
			ZZ2->ZZ2_DTENTR := DATE()
		ELSE
			ZZ2->ZZ2_DTPREV	:= validTipo(alltrim(cTipMed),oModel)
		ENDIF
		ZZ2->ZZ2_DTMAX	:= validTipo(alltrim(cTipMed),oModel)
		ZZ2->ZZ2_NATMED := cNaturez
		ZZ2->ZZ2_PRIORI := "99"
		ZZ2->ZZ2_ETAPA  := GETMV("SA_ETPMED")
		ZZ2->ZZ2_FATEXP := "2"
		IF ALLTRIM(FUNNAME()) == "RPC" 
			ZZ2->ZZ2_TPID   := "E"
			ZZ2->ZZ2_IDPDV  := idPdvWS
			ZZ2->ZZ2_FATEXP := "1"
     	ENDIF

		MSUNLOCK()

		For nCont := 1 To oModelZZ3:Length()
			oModelZZ3:GoLine(nCont)
			If oModelZZ3:IsDeleted()
				Loop
			EndIf
			If oModelZZ3:GetValue("ZZ3_QUANT") == 0
				Loop
			EndIf


		dbSelectArea("ZZ3")
		RecLock("ZZ3",.T.)
		For nCont2:=1 To Len(aStruct)
			ZZ3->(&(aStruct[nCont2][MODEL_FIELD_IDFIELD])) := oModelZZ3:GetValue(aStruct[nCont2][MODEL_FIELD_IDFIELD])
		Next
		ZZ3->ZZ3_FILIAL	:= xFilial("ZZ3")
		ZZ3->ZZ3_NUMERO	:= oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')
		ZZ3->ZZ3_MEDICA := cMedicao
		IF ALLTRIM(FUNNAME()) == "RPC" 
			ZZ3->ZZ3_TPID   := "E"
			ZZ3->ZZ3_IDPDV  := idPdvWS
		ENDIF
		ZZ3->(MsUnLock())
		dbCloseArea("ZZ3")
		ZZ3->(ConfirmSX8())

		If ZZ1->(DbSeek(xFilial("ZZ1")+cContrato+oModelZZ3:GetValue('ZZ3_ITEM')))

			//VERIFICA O TIPO DE ITEM A SER MEDIDO SE � KIT OU PRODUTO   (K= KIT / P= PRODUTO)

			If SF4->(DbSeek(xFilial("SF4")+ZZ1->ZZ1_TES)) .AND. (SF4->F4_DUPLIC=="S" .OR. ZZ1->ZZ1_TES $ GETMV("SA_TESEXCE"))	//Gera Financeiro
				nVlrAdiant	+= ((oModelZZ3:GetValue('ZZ3_VLUNIT')*oModelZZ3:GetValue('ZZ3_QUANT')) )
				if alltrim(oModelZZ3:GetValue('ZZ3_TIPO')) == 'K'
					nVlrAdiant -= DESCFRET(oModel) * oModelZZ3:GetValue('ZZ3_QUANT')
				endif
			EndIf

			IF SELECT("T03") > 0
				T03->(DbCloseArea())
			endif	

			If Alltrim(oModelZZ3:GetValue('ZZ3_TIPO')) == 'K' 
			
				cquery1 := " SELECT ZZ7_PRODUT,ZZ7_QUANT,ZZ7_PERDSC, ZZ7_CATEGO , ZZ7_SUPLEM,ZZ7_CODIGO,ZZ7_CODPAI,ZZ7_PROCPL "
				cquery1 += " FROM " + retsqlname('ZZ7') + " ZZ7 "
				cquery1 += " WHERE ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
				cquery1 += " AND D_E_L_E_T_ <> '*' "
				cquery1 += " AND ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
				cquery1 += " AND ZZ7_TIPO = 'A' "
				cquery1 += " ORDER BY ZZ7_PRODUT"
				TCQuery cquery1 new alias T03
	
				aValVenda := ValVenda(oModel)	


				While T03->(!EOF())

					nContIT++
					nVlrItem2	:= MaTabPrVen(cTabPad,T03->ZZ7_PRODUT,1,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT'),oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))

				AADD(aItens,{})
				nTam	:= Len(aItens)
				AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	    ,nil})
				IF !EMPTY(T03->ZZ7_PROCPL) .AND. cTipMEd $ GETMV("SA_PEDCPL") 
					AADD(aItens[nTam],{"C6_PRODUTO"	,T03->ZZ7_PROCPL                                            ,nil})
				ELSE 
					AADD(aItens[nTam],{"C6_PRODUTO"	,T03->ZZ7_PRODUT                                            ,nil})
				ENDIF											
				AADD(aItens[nTam],{"C6_QTDVEN"	,T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT')				    ,nil})

				//TODO VALOR PARA PROFESSOR : RAFAEL PINHEIRO - 04/11/2015
				// ajustado para quando for professor considerar o valor do parametro.
				IF T03->ZZ7_PERDSC == 0 .AND. T03->ZZ7_CATEGO $ 'A | C | D | V ' .AND. T03->ZZ7_SUPLEM == 'N' //T03->ZZ7_PERDSC == 0 .AND. T03->ZZ7_CATEGO<>'P'
					AADD(aItens[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1] 								,nil}) //nVlrItem
				ELSEIF T03->ZZ7_CATEGO='P' // INCLU
					AADD(aItens[nTam],{"C6_PRCVEN"	,GETMV('SA_PRECDSC') 								    ,nil}) //nVlrItem
				ELSEIF T03->ZZ7_PERDSC == 100 .AND. T03->ZZ7_CATEGO<>'P'
					AADD(aItens[nTam],{"C6_PRCVEN"	,GETMV('SA_PRECDSC') 								    ,nil}) //nVlrItem
					nDesc +=(( GETMV('SA_PRECDSC')*T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT'))*(T03->ZZ7_PERDSC/100))
				ELSEIF T03->ZZ7_PERDSC == 0

					IF nVlrItem2 <> 0
						AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2             			,nil})
					ELSE
						AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1]		,nil})
				    ENDIF

			ENDIF

			AADD(aItens[nTam],{"C6_PRUNIT"	,0						 									,nil})
			AADD(aItens[nTam],{"C6_YTES"	,oModelZZ3:GetValue('ZZ3_TES')								,nil})
			AADD(aItens[nTam],{"C6_TES"		,cTesTmp													,nil})
			AADD(aItens[nTam],{"C6_YCONTRA"	,cContrato													,nil})
			AADD(aItens[nTam],{"C6_YMEDICA"	,cMedicao													,nil})
			AADD(aItens[nTam],{"C6_YITCONT"	,oModelZZ3:GetValue('ZZ3_ITEM')								,nil})
			AADD(aItens[nTam],{"C6_YCODKIT" ,T03->ZZ7_CODIGO ,nil}) //ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT'))					,nil})
			AADD(aItens[nTam],{"C6_YPAIKIT" ,T03->ZZ7_CODPAI ,nil})  //Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')),"ZZ7_CODPAI"))	,nil})
			AADD(aItens[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT')						     			,nil})

			If SB1->(DbSeek(xFilial("SB1")+T03->ZZ7_PRODUT)) .AND. SB2->(DbSeek(xFilial("SB2")+T03->ZZ7_PRODUT+SB1->B1_LOCPAD))
				AADD(aLocks,{"SB2",SB2->(Recno())})
			EndIf

			AADD(aItens2,{})
			nTam	:= Len(aItens2)
			AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)    ,nil})
			IF !EMPTY(T03->ZZ7_PROCPL) .AND. cTipMEd $ GETMV("SA_PEDCPL") 
				AADD(aItens2[nTam],{"C6_PRODUTO",T03->ZZ7_PROCPL	                                    ,nil})
			ELSE 
				AADD(aItens2[nTam],{"C6_PRODUTO",T03->ZZ7_PRODUT	                                    ,nil})
			ENDIF							
			AADD(aItens2[nTam],{"C6_QTDVEN"	,T03->ZZ7_QUANT*oModelZZ3:GetValue('ZZ3_QUANT')			    ,nil})
	
			IF T03->ZZ7_PERDSC == 0 .AND. T03->ZZ7_CATEGO $ 'A | C | D | V ' .AND. T03->ZZ7_SUPLEM == 'N' 
				IF nVlrItem2 <> 0
					AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2             								,nil})
					nValVenda ++
				ELSE
					AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1]							,nil})
					nValVenda ++
				ENDIF
			ELSEIF T03->ZZ7_CATEGO='P' // INCLU
				AADD(aItens2[nTam],{"C6_PRCVEN"	,GETMV('SA_PRECDSC') 								    ,nil}) //nVlrItem
			ELSEIF T03->ZZ7_PERDSC == 100 .AND. T03->ZZ7_CATEGO<>'P'
				AADD(aItens2[nTam],{"C6_PRCVEN"	,GETMV('SA_PRECDSC') 							    ,nil})

			ELSEIF T03->ZZ7_PERDSC == 0
				IF nVlrItem2 <> 0
					AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2             			,nil}) // AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1] 									,nil}) //nVlrItem
				ELSE
					AADD(aItens2[nTam],{"C6_PRCVEN"	,aValVenda[nValVenda][1]		,nil})
				ENDIF
			ENDIF

			AADD(aItens2[nTam],{"C6_PRUNIT"	,0						 							    ,nil})
			AADD(aItens2[nTam],{"C6_YTES"	,oModelZZ3:GetValue('ZZ3_TES')							,nil})
			AADD(aItens2[nTam],{"C6_TES"	,cTesTmp							                    ,nil})
			AADD(aItens2[nTam],{"C6_YCONTRA",cContrato												,nil})
			AADD(aItens2[nTam],{"C6_YMEDICA",cMedicao												,nil})
			AADD(aItens2[nTam],{"C6_YITCONT",oModelZZ3:GetValue('ZZ3_ITEM')							,nil})
			AADD(aItens2[nTam],{"C6_YCODKIT",T03->ZZ7_CODIGO ,nil}) //ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT'))	  			,nil})//oModelZZ3:GetValue('000000000000003')
			AADD(aItens2[nTam],{"C6_YPAIKIT",T03->ZZ7_CODPAI ,nil}) //Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')),"ZZ7_CODPAI"))					,nil})
			AADD(aItens2[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT')						     			,nil})

			T03->(DbSkip())
			nbkpValVen := nValVenda
			EndDo
			nValVenda := nbkpValVen
			T03->(DbCloseArea())


		Else					
			nContIT++
			nVlrItem2	:= MaTabPrVen(cTabPad,oModelZZ3:GetValue('ZZ3_PRODUT'),1,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT'),oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))

			AADD(aItens,{})
			nTam	:= Len(aItens)
			nVlrTotal	+= ZZ1->ZZ1_VLRUN*oModelZZ3:GetValue('ZZ3_QUANT')

			AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
			AADD(aItens[nTam],{"C6_PRODUTO"	,oModelZZ3:GetValue('ZZ3_PRODUT')	,nil})
			AADD(aItens[nTam],{"C6_QTDVEN"	,oModelZZ3:GetValue('ZZ3_QUANT')	,nil})
			AADD(aItens[nTam],{"C6_PRCVEN"	,ZZ1->ZZ1_VLRUN						,nil})
			AADD(aItens[nTam],{"C6_PRUNIT"	,0						 			,nil})
			AADD(aItens[nTam],{"C6_YTES"	,oModelZZ3:GetValue('ZZ3_TES')		,nil})
			AADD(aItens[nTam],{"C6_TES"		,cTesTmp		,nil})
			AADD(aItens[nTam],{"C6_YCONTRA"	,cContrato							,nil})
			AADD(aItens[nTam],{"C6_YMEDICA"	,cMedicao							,nil})
			AADD(aItens[nTam],{"C6_YITCONT"	,oModelZZ3:GetValue('ZZ3_ITEM')		,nil})
			AADD(aItens[nTam],{"C6_YCODKIT",ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT'))	  			,nil})
			AADD(aItens[nTam],{"C6_YPAIKIT",Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')),"ZZ7_CODPAI"))					,nil})
			AADD(aItens[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT')						     			,nil})


			If SB1->(DbSeek(xFilial("SB1")+oModelZZ3:GetValue('ZZ3_PRODUT'))) .AND. SB2->(DbSeek(xFilial("SB2")+oModelZZ3:GetValue('ZZ3_PRODUT')+SB1->B1_LOCPAD))
				AADD(aLocks,{"SB2",SB2->(Recno())})
			EndIf


			nVlrItem2	:= MaTabPrVen(cTabPad,oModelZZ3:GetValue('ZZ3_PRODUT'),1,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT'),oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))

			AADD(aItens2,{})
			nTam	:= Len(aItens2)
			AADD(aItens2[nTam],{"C6_ITEM"	    ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
			AADD(aItens2[nTam],{"C6_PRODUTO"	,oModelZZ3:GetValue('ZZ3_PRODUT')	                        ,nil})
			AADD(aItens2[nTam],{"C6_QTDVEN"	    ,oModelZZ3:GetValue('ZZ3_QUANT')							,nil})
			//AADD(aItens2[nTam],{"C6_QTDLIB"	    ,oModelZZ3:GetValue('ZZ3_QUANT')							,nil})
			IF nVlrItem2 <> 0
				AADD(aItens2[nTam],{"C6_PRCVEN"	    ,nVlrItem2 												,nil})
			ELSE
				AADD(aItens2[nTam],{"C6_PRCVEN"	    ,ZZ1->ZZ1_VLRUN											,nil})
			ENDIF
			AADD(aItens2[nTam],{"C6_PRUNIT"	,0						 									,nil})
			AADD(aItens2[nTam],{"C6_YTES"		,oModelZZ3:GetValue('ZZ3_TES')				        		,nil})
			AADD(aItens2[nTam],{"C6_TES"		,cTesTmp				        		,nil})
			AADD(aItens2[nTam],{"C6_YCONTRA"	,cContrato													,nil})
			AADD(aItens2[nTam],{"C6_YMEDICA"	,cMedicao													,nil})
			AADD(aItens2[nTam],{"C6_YITCONT"	,oModelZZ3:GetValue('ZZ3_ITEM')								,nil})
			AADD(aItens2[nTam],{"C6_YCODKIT",ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT'))	  			,nil})//oModelZZ3:GetValue('000000000000003')
			AADD(aItens2[nTam],{"C6_YPAIKIT",Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')),"ZZ7_CODPAI"))					,nil})
			AADD(aItens[nTam],{"C6_LOCAL"	,GETMV('SA_LOCFAT') 						     			,nil})
	 ENDIF

			/*					RecLock("ZZ1",.F.)
			ZZ1->ZZ1_SALDO	-= oModelZZ3:GetValue('ZZ3_QUANT')
			nQtdMed			+= oModelZZ3:GetValue('ZZ3_QUANT')
			ZZ1->(MsUnLock())
			*/		    
			
		

			//DisarmTransaction()
			//oModel:SetErrorMessage('ZZ3DETAIL',,,,"ERRO", 'N�o foi encontrado o item do contrato na base de dados!', 'Entre em contato com o administrador do sistema!')
			//Return .F.
	
	 ENDIF		
			Next

			nSaldo  := 1
			nFret	:= 0
			AADD(aCab,{"C5_TIPO"		,"N"													,Nil})
			AADD(aCab,{"C5_CLIENTE"		,""														,Nil})
			AADD(aCab,{"C5_LOJACLI"		,""														,Nil})
			AADD(aCab,{"C5_CONDPAG"		,"001"													,Nil})
			AADD(aCab,{"C5_NATUREZ"		,cNaturez												,Nil})
			AADD(aCab,{"C5_YCONTRA"		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')	,Nil})
			AADD(aCab,{"C5_YFILIAL"		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FILIAL')	,Nil})
			AADD(aCab,{"C5_YMEDICA"		,cMedicao  												,Nil})
			AADD(aCab,{"C5_TABELA"		,""														,Nil})
			AADD(aCab,{"C5_YTPVEN"		,cTipMed												,Nil})
			AADD(aCab,{"C5_TPEDAV"		,"39"													,Nil})
			AADD(aCab,{"C5_YOBS"		,cObsNF													,Nil})
			AADD(aCab,{"C5_YDTSOL"		,dSolicita												,Nil})
			AADD(aCab,{"C5_YLCEXPD"		,cLocExp												,Nil})

			IF ALLTRIM(FunName()) == "RPC" //ECOMMERCE
				AADD(aCab,{"C5_MENNOTA"		,cObWS												,Nil})
				AADD(aCab,{"C5_DESCFI"		,C5DescWS											,Nil}) //Desconto do WebService
			ENDIF

			if ! POSICIONE("ZZG",1,XFILIAL("ZZG")+cTipMed,"ZZG_ESPCTR") $ cTipoPrincipal .AND. POSICIONE("ZZG",1,XFILIAL("ZZG")+cTipMed,"ZZG_ESPCTR") $ cTipoComplementar
				nFret := nVlrAdiant*(oModelSZ1:GetValue('ZZ0_FRTEMP') / 100)
			elseif  POSICIONE("ZZG",1,XFILIAL("ZZG")+cTipMed,"ZZG_ESPCTR") $ cTipoPrincipal .AND. ! POSICIONE("ZZG",1,XFILIAL("ZZG")+cTipMed,"ZZG_ESPCTR") $ cTipoComplementar
				nFret := nVlrAdiant*(oModelSZ1:GetValue('ZZ0_FRTCLI') / 100)
			endif

			aCliente	:= {oModelSZ1:GetValue('ZZ0_CLIENT'),oModelSZ1:GetValue('ZZ0_LOJA'),oModelSZ1:GetValue('ZZ0_NOME')}

			cFilOrig := oModelSZ1:GetValue('ZZ0_FILIAL')

			Processa({|| lRet := Pedido(aCab,aItens,aCliente,cContrato,cMedicao,nFret,aItens2,oModel,lCheckBo1,lCheckBo2,cFilorig,cNewFil,cFilOpc,oModel) },"Criando Pedidos","Aguarde...",.F.)
			If !lRet
				DisarmTransaction()
				oModel:SetErrorMessage('ZZ3DETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
				Return .F.
			EndIf

		End Transaction
		MsUnLockAll()
	ENDIF	
Return lRet

Static Function Pedido(aCab,aItens,aCliente,cContrato,cMedicao,nFret,aItens2,oModel,lCheckBo1,lCheckBo2,cFilorig,cNewFil,cFilOpc,oModel)

	Local cFilBak			:= cFilAnt
	Local aCabCliente		:= aClone(aCab)
	Local aCabCol			:= aClone(aCab)
	Local cCNPJ			    := FWArrFilAtu()[18]			    //Coligada
	Local cCNPJMatriz		:= FWArrFilAtu("01",cFilOpc)[18]	//Holding
	Local cCliCol           := cLjCol := ""
	Local nPosCliente		:= aScan(aCab,{|x| x[1]=="C5_CLIENTE"})
	Local nPosLoja		    := aScan(aCab,{|x| x[1]=="C5_LOJACLI"})
	Local nPosProd		    := aScan(aItens[1],{|x| x[1]=="C6_PRODUTO"})
	Local nPosQuant		    := aScan(aItens[1],{|x| x[1]=="C6_QTDVEN"})
	Local nPosVlr			:= aScan(aItens[1],{|x| x[1]=="C6_PRCVEN"})
	Local nPosTes			:= aScan(aItens[1],{|x| x[1]=="C6_YTES"})
	Local nPosLocal1        := aScan(aItens[1],{|x| x[1]=="C6_LOCAL"})
	Local nPosKit           := aScan(aItens2[1],{|x| x[1]=="C6_YCODKIT"})
	Local nPosPai           := aScan(aItens2[1],{|x| x[1]=="C6_YPAIKIT"})
	Local nPosLocal2        := aScan(aItens2[1],{|x| x[1]=="C6_LOCAL"})
	Local cTesPV2			:= GetTes(SuperGetMv("SA_TESPV2",.F.,"",cFilAnt),aItens[1][nPosTes][2])		//Matriz-Comercial	//502:503;505:505
	Local cTesPV3			:= GetTes(SuperGetMv("SA_TESPV3",.F.,"",cFilAnt),aItens[1][nPosTes][2])		//Matriz-Cliente	//502:504;505:504
	Local cTesPC4			:= GetTes(SuperGetMv("SA_TESPC4",.F.,"",cFilAnt),aItens[1][nPosTes][2])		    //Comercial-Matriz	//502:016;505:016
	Local cPedido1,cPedido2,cPedido3
	Local aFornec			:= {,}
	Local aCabPC			:= {}
	Local aItensPC	:= {}
	Local nCont,aLinha
	Local lJaLiberado	:= .F.
	Local cPedido4   := ""
	Local oModelSZ1	:= oModel:GetModel('ZZ0MASTER')
	Local oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')
	Default aItens		:= {}
	Private lMsErroAuto	:= .F.
	Private cMat120Num	:= ""
	ProcRegua(4)

	cFilAnt := cFilorig

	cCliCol	:= SUBSTR(SuperGetMv( "SA_CLIEFIL",,, cFilAnt ),1,6)//SA1->A1_COD
	cLjCol	:= SUBSTR(SuperGetMv( "SA_CLIEFIL",,, cFilAnt ),8,9)//SA1->A1_LOJA

	//Fornecedor Rio 
	if cFilOpc == '020101'
		aFornec[1]	:= SUBSTR(GetMv( "SA_PCFORIO" ),1,6)
		aFornec[2]	:= SUBSTR(GetMv( "SA_PCFORIO" ),8,9)
	else
		aFornec[1]	:= SUBSTR(GetMv( "SA_PCFOR" ),1,6)
		aFornec[2]	:= SUBSTR(GetMv( "SA_PCFOR" ),8,9)
	endif

	aCabCliente[nPosCliente][2]	:= aCliente[1]
	aCabCliente[nPosLoja][2]	:= aCliente[2]
	aCabCol[nPosCliente][2]		:= cCliCol
	aCabCol[nPosLoja][2]		:= cLjCol
	IncProc("Criando pedido para o cliente")
	ProcessMessage()
	//Comercial - Cliente

	AADD(aCabCliente,{"C5_FILIAL",xFilial("SC5") ,Nil})
	AADD(aCabCliente,{"C5_FRETE",nFret,Nil})
	AADD(aCabCliente,{"C5_DESCONT",nDesc,Nil})
	AADD(aCabCliente,{"C5_TPFRETE","C",NIL})

	lJaLiberado	:= .F.
	For nCont:=1 to Len(aItens)
		If SB1->(DbSeek(xFilial("SB1")+aItens[nCont][nPosProd][2])) .AND. SB1->B1_GRUPO=="0099" .AND. SB1->B1_YSUBGR=="31"
			lJaLiberado	:= .T.
			Exit
		EndIf
	Next

	//Grava��o Reclock ou Execauto
	If SuperGetMv("SA_TPGRV",,"R") == "R" //Reclock

		cRet:= u_SASP039(aCabCliente,aItens,3,"MATA410")

		If !empty(cRet[1])
			AutoGrLog("Erro ao gerar pedido de venda do Comercial para o cliente!")
			Aviso("Aten��o",cRet[1],{"Ok"})
			Return .F.
		Else
			cPedido1:= cRet[2]
		Endif

	Else //ExecAuto

		IF alltrim(cNewFil) <> ""
			cFilant := cNewFil
		endif
		MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o

		If lMsErroAuto
			cFilant := cFilBak
			AutoGrLog("Erro ao gerar pedido de venda do Comercial para o cliente!")
			MostraErro()
			Return .F.
		Else
			cPedido1:= SC5->C5_NUM
		Endif
		cFilant := cFilBak
	Endif


	IF oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FATDIR') =='2' .OR. (oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FATDIR') == "3" .AND. lCheckBo2)

		aSize(aCabCliente,Len(aCabCliente)-3) //Deleta Frete/Desconto para outras filiais/filial

		IncProc("Criando pedido na Matriz para o Comercial")
		ProcessMessage()

		//Matriz - Comercial
		cFilAnt	:= cFilOpc //"010105"	//Matriz
		lMsErroAuto	:= .F.
		For nCont:=1 to Len(aItens2)
			//TODO Quando o Faturamento for de entre o CD do Rio e Barra America utilizar a Tes 511 para transferencia e 009 para entrada
			// Weskley Silva 08/11/2016
			IF cCliCol == '308933' .AND. cLjCol = '01' .AND. cFilAnt == '020101'
				aItens2[nCont][nPosTes][2] := '511'
				cTesPC4 := '009'
			ELSE
				aItens2[nCont][nPosTes][2]	:= cTesPV2
			endif
		Next

		AADD(aCabCol,{"C5_FILIAL",xFilial("SC5"),Nil})

		//Grava��o Reclock ou Execauto
		If SuperGetMv("SA_TPGRV",,"R") == "R" //Reclock
			cRet:= u_SASP039(aCabCol,aItens2,3,"MATA410")

			If !empty(cRet[1])
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Comercial!")
				Aviso("Aten��o",cRet[1],{"Ok"})
				Return .F.
			Else
				cPedido2:= cRet[2]
			Endif

		Else //ExecAuto

			MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCol,aItens2,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o

			If lMsErroAuto
				cFilAnt	:= cFilBak
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Comercial!")
				MostraErro()
				Return .F.
			Else
				cPedido2	:= SC5->C5_NUM
			Endif

		Endif

		//Matriz - Cliente
		IncProc("Criando pedido na Matriz para o Cliente")
		ProcessMessage() //ATUALIZA CLIENTE
		lMsErroAuto	:= .F.
		For nCont:=1 to Len(aItens)
			aItens[nCont][nPosTes][2]			:= cTesPV3
		Next

		AADD(aCabCliente,{"C5_FILIAL",xFilial("SC5"),Nil})
		AADD(aCabCliente,{"C5_FRETE",0,Nil})
		AADD(aCabCliente,{"C5_DESCONT",nDesc,Nil})
		AADD(aCabCliente,{"C5_TPFRETE","C",NIL})

		//Grava��o Reclock ou Execauto
		If SuperGetMv("SA_TPGRV",,"R") == "R" //Reclock
			cRet:= u_SASP039(aCabCliente,aItens,3,"MATA410")

			If !empty(cRet[1])
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Cliente!")
				Aviso("Aten��o",cRet[1],{"Ok"})
				Return .F.
			Else
				cPedido3:= cRet[2]
			Endif

		Else //ExecAuto

			MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o


			If lMsErroAuto
				cFilAnt	:= cFilBak
				AutoGrLog("Erro ao gerar pedido de venda da Matriz para Cliente!")
				MostraErro()
				Return .F.
			Else
				cPedido3	:= SC5->C5_NUM
			Endif
		Endif

		aSize(aCabCliente,Len(aCabCliente)-1) //Deleta Desconto para outras filiais
		cFilAnt	:= cFilBak

		IncProc("Criando pedido de compra para a Matriz")
		cPedido4	:= CriaVar('C7_NUM', .T.)
		aadd(aCabPC,{"C7_FILIAL"	,cFilAnt})
		aadd(aCabPC,{"C7_NUM"		,cPedido4})
		aadd(aCabPC,{"C7_EMISSAO"	,dDataBase})
		aadd(aCabPC,{"C7_FORNECE"	,aFornec[1]})
		aadd(aCabPC,{"C7_LOJA"		,aFornec[2]})
		aadd(aCabPC,{"C7_COND"		,"001"})		//TODO VERIFICAR CONDI��O PAG
		aadd(aCabPC,{"C7_CONTATO"	,"AUTO"})
		aadd(aCabPC,{"C7_FILENT"	,cFilAnt})

		For nCont:= 1 To Len(aItens2)
			aLinha := {}

			aadd(aLinha,{"C7_PRODUTO"	,aItens2[nCont][nPosProd][2]	,Nil})
			aadd(aLinha,{"C7_QUANT"		,aItens2[nCont][nPosQuant][2]	,Nil})
			aadd(aLinha,{"C7_PRECO"		,aItens2[nCont][nPosVlr][2]		,Nil})
			aadd(aLinha,{"C7_YCODKIT", aItens2[nCont][nPosKit][2], NIL})
			aadd(aLinha,{"C7_YPAIKIT", aItens2[nCont][nPosPai][2], NIL})

			aadd(aLinha,{"C7_YTES"		,cTesPC4						,Nil})
			aadd(aLinha,{"C7_TES"		,"400"						    ,Nil})
			aadd(aLinha,{"C7_YCONTRA"	,cContrato	 					,Nil})
			aadd(aLinha,{"C7_YMEDICA"	,cMedicao					    ,Nil})
			aadd(aLinha,{"C7_LOCAL"		,GETMV("SA_ENTLOCA")		    ,Nil})
			aadd(aLinha,{"C7_YCLAORC"	,"E"		                    ,Nil})	//E-Estoque
			aadd(aLinha,{"C7_CC"		,"1025"		                    ,Nil})	//1025-Almoxarifado
			aadd(aItensPC,aLinha)
		Next nCont
		lMsErroAuto	:= .F.
		cMat120Num		:= ""	//Variavel preenchida na A120Grava

		//Grava��o Reclock ou Execauto
		If SuperGetMv("SA_TPGRV",,"R") == "R" //Reclock
			cRet:= u_SASP039(aCabPC,aItensPC,3,"MATA120")

			If !empty(cRet[1])
				AutoGrLog("Erro ao gerar pedido de compras !")
				Aviso("Aten��o",cRet[1],{"Ok"})
				Return .F.
			Else
				cPedido4:= cRet[2]
			Endif

		Else //ExecAuto

			MsExecAuto({|x,y,z| MATA120(1,x,y,z)}, aCabPC,aItensPC,3)

			If lMsErroAuto
				AutoGrLog("Erro ao gerar pedido de compra!")
				MostraErro()
				Return .F.
			Else
				cPedido4	:= If(!Empty(cMat120Num),cMat120Num,cPedido4)
				dbSelectArea("SC7")
				dbSetOrder(1)
				If !MsSeek(xFilial("SC7")+cPedido4)
					AutoGrLog("Erro ao localizar pedido de compra gerado!")
					MostraErro()
					Return .F.
				EndIf
			Endif
		Endif
	Endif

	dbSelectArea("ZZ2")
	dbSetOrder(1)
	If DbSeek(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FILIAL')+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')+cMedicao+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))

		RecLock("ZZ2",.F.)

		ZZ2->ZZ2_FILIAL	:= oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FILIAL')
		ZZ2->ZZ2_NUMERO	:= cContrato
		ZZ2->ZZ2_MEDICA	:= cMedicao
		ZZ2->ZZ2_TIPCTR := IIF(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FATDIR') =='1','1',IIF(oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_FATDIR') == "3", IIF(lCheckBo2,'2','1'),'2'))
		ZZ2->ZZ2_CLIENT	:= aCliente[1]
		ZZ2->ZZ2_LOJA	:= aCliente[2]
		ZZ2->ZZ2_EST	:= POSICIONE("SA1",1,xFilial("SA1") + aCliente[1] + aCliente[2],"A1_EST")
		ZZ2->ZZ2_PV01FL	:= IIF(cNewFil == "", IIF(EMPTY(cPedido1),"",cFilAnt) , cNewFil)
		ZZ2->ZZ2_PV01	:= cPedido1

		IF ALLTRIM(FunName()) == "RPC"
			C5FilWS  := IIF(cNewFil == "", IIF(EMPTY(cPedido1),"",cFilAnt) , cNewFil)
			C5PedWS  := cPedido1
			ZZ2->ZZ2_STATUS := "L"
			ZZ2->ZZ2_ETAPA  := GETMV('SA_ETPFIM')		
		endif

		if alltrim(ZZ2->ZZ2_PV01FL) <> "" .AND. alltrim(ZZ2->ZZ2_PV01) <> ""
			U_PESOMED(alltrim(ZZ2->ZZ2_PV01FL),alltrim(ZZ2->ZZ2_PV01),.F.)
		endif

		ZZ2->ZZ2_CLNF01 := IIF(EMPTY(cPedido1),"",aCliente[1])
		ZZ2->ZZ2_LJNF01 := IIF(EMPTY(cPedido1),"",aCliente[2])
		ZZ2->ZZ2_PV02FL	:= IIF(EMPTY(cPedido2),"",cFilOpc)
		ZZ2->ZZ2_PV02	:= cPedido2

		if alltrim(ZZ2->ZZ2_PV02FL) <> "" .AND. alltrim(ZZ2->ZZ2_PV02) <> ""
			U_PESOMED(alltrim(ZZ2->ZZ2_PV02FL),alltrim(ZZ2->ZZ2_PV02),.F.)
		endif

		ZZ2->ZZ2_CLNF02 := IIF(EMPTY(cPedido2),"",cCliCol)
		ZZ2->ZZ2_LJNF02 := IIF(EMPTY(cPedido2),"",cLjCol)
		ZZ2->ZZ2_PV03FL	:= IIF(EMPTY(cPedido3),"",cFilOpc)
		ZZ2->ZZ2_PV03	:= cPedido3

		if alltrim(ZZ2->ZZ2_PV03FL) <> "" .AND. alltrim(ZZ2->ZZ2_PV03) <> ""
			U_PESOMED(alltrim(ZZ2->ZZ2_PV03FL),alltrim(ZZ2->ZZ2_PV03),.T.)
		endif

		ZZ2->ZZ2_CLNF03 := IIF(EMPTY(cPedido3),"",aCliente[1])
		ZZ2->ZZ2_LJNF03	:= IIF(EMPTY(cPedido3),"",aCliente[2])
		ZZ2->ZZ2_PCFIL	:= IIF(EMPTY(cPedido4),"",cFilAnt)
		ZZ2->ZZ2_PC01	:= cPedido4
		ZZ2->ZZ2_FORNEC := aFornec[1]
		ZZ2->ZZ2_LOJFOR := aFornec[2]
		MsUnLock()

	Endif

	dbSelectArea("ZZR")
	dbSetOrder(1)      // ZZR_FILIAL+ZZR_CONTRA+ZZR_MEDICA+ZZRETAPA

	RECLOCK("ZZR", .T.)

	ZZR_FILIAL  := ZZ2->ZZ2_FILIAL
	ZZR_CONTRA  := ZZ2->ZZ2_NUMERO
	ZZR_MEDICA  := ZZ2->ZZ2_MEDICA
	ZZR_EMISSA  := ZZ2->ZZ2_DTGER
	ZZR_CODCLI  := ZZ2->ZZ2_CLIENT
	ZZR_NOMECL  := POSICIONE("SA1",1,xFilial("SA1")+ZZ2->ZZ2_CLIENT+ZZ2->ZZ2_LOJA,"A1_NOME")
	ZZR_CODUSE  := RetCodUsr()
	ZZR_NOMUSE  := UsrFullName(RetCodUsr())
	ZZR_ETAPA   := "00"
	ZZR_NOMETP  := POSICIONE("SX5",1,XFILIAL("SX5")+'ZA'+"00","X5_DESCRI")
	ZZR_DATA    := date()
	ZZR_HORA    := time()
	ZZR_OBS     := "Medi��o Criada "

	MSUNLOCK()     // Destrava o registro


	// log para faturamento
	if lCheckBo1
		U_SASP023(XFILIAL("ZZ0"),cContrato,aCliente[1],aCliente[2],dDatabase,TIME(),__cUserid,xFilial("ZZ2")+cContrato+cMedicao,'15','01-Gerada Medi��o...Faturamento Direto...')
	Else
		U_SASP023(XFILIAL("ZZ0"),cContrato,aCliente[1],aCliente[2],dDatabase,TIME(),__cUserid,xFilial("ZZ2")+cContrato+cMedicao,'01','01-Gerada Medi��o...Faturamento Triangular...')
	EndIF

	FOR nAx := 1 to len(oModelZZ3:acols)//Atualiza o saldo da ZZ1 com tirando a quantidade medida
		oModelZZ3:GoLine(nAx)

		IF !empty(oModelZZ3:getvalue("ZZ3_QUANT"))
			DbSelectArea("ZZ1")
			DbSetOrder(3)
			IF DbSeek(oModelZZ3:getvalue("ZZ3_FILIAL") + oModelZZ3:getvalue("ZZ3_NUMERO") + oModelZZ3:getvalue("ZZ3_CLIENT") + oModelZZ3:getvalue("ZZ3_LOJA")  + oModelZZ3:getvalue("ZZ3_ITEM") + oModelZZ3:getvalue("ZZ3_PRODUT"))
				RecLock("ZZ1",.F.)

				ZZ1->ZZ1_SALDO -= oModelZZ3:getvalue("ZZ3_QUANT")

				MSUNLOCK()
			ENDIF
		ENDIF
	next

	MsgInfo("Realizada medi��o de n�mero : "+alltrim(cMedicao)+" para o contrato de numero: "+alltrim(cContrato)+" !","Mensagem")

Return .T.

USER FUNCTION PESOMED(cFil,cPedido,lTipo)

	LOCAL nPeso := 0
	Local aArea := getarea()

	cquery1 := " SELECT *
	cquery1 += " from " + retsqlname('SC6') + " SC6 "
	cquery1 += " where C6_FILIAL = '" + cFil + "' "
	cquery1 += " and D_E_L_E_T_ <> '*' "
	cquery1 += " and C6_NUM = '"+ cPedido +"' "
	TCQuery cquery1 new alias TPESO

	WHILE !TPESO->(eof())

		if !lTipo

			if posicione("SF4",1,XFILIAL("SF4")+TPESO->C6_YTES,"F4_ESTOQUE") == "S"
				nPeso += POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(TPESO->C6_PRODUTO),"B1_PESO") * TPESO->C6_QTDVEN
			else
				nPeso += POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(TPESO->C6_PRODUTO),"B1_PESO") * TPESO->C6_QTDVEN
			endif

		else

			nPeso += POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(TPESO->C6_PRODUTO),"B1_PESO") * TPESO->C6_QTDVEN

		endif

		TPESO->(dbskip())
	ENDDO

	TPESO->(DBCLOSEAREA())

	dbSelectArea("SC5")
	dbSetOrder(1)      // A1_FILIAL + A1_COD + A1_LOJA
	dbSeek(cFil + cPedido)

	IF FOUND()
		RECLOCK("SC5", .F.)

		SC5->C5_PESOL     := nPeso
		SC5->C5_PBRUTO    := nPeso

		MSUNLOCK()     // Destrava o registro
	ENDIF

	RestArea(aArea)
RETURN

Static Function GetTes(cParam,cTes)
	Local cRetTES	:= ""
	Local aTES		:= {}
	Local aTmp		:= Separa(cParam,";",.T.)
	Local nPos
	aEval(aTmp,{|x| If(":"$x,AADD(aTES,Separa(x,":",.T.)),AADD(aTES,Separa(x+":",":",.T.)))})
	nPos	:= aScan(aTES,{|x| x[1]==cTes})
	If nPos>0
		cRetTES	:= aTES[nPos][2]
	EndIf
Return cRetTES

STATIC FUNCTION QUANTZZ3(oModelGrid,cId,xValor)
	Local lRet := .T.
	Local oModelZZ3	:= oModelGrid:GetModel('ZZ3DETAIL')
	Local oModel	:= FWModelActive()

	IF xValor < 0
		lRet := .F.
		oModelZZ3:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", 'Quantidades negativas n�o � permitido !', 'Verificar campo de quantidade!')
	ENDIF

	nSaldo:= POSICIONE("ZZ1",3,XFILIAL("ZZ1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_ITEM')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT'),"ZZ1_SALDO")

	IF  xValor > nSaldo
		lRet := .F.
		oModelZZ3:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", 'Quantidade informada � superior ao Saldo!', 'Verificar produtos cadastrados no contrato!')
	ENDIF


RETURN lRet

STATIC FUNCTION DESCFRET(oModel)
	Local aStruct	:= oModel:GetModel( 'ZZ3DETAIL' ):oFormModelStruct:GetFields()
	Local oModelSZ1	:= oModel:GetModel('ZZ0MASTER')
	Local oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')
	Local nValkit   := oModelZZ3:GetValue('ZZ3_VLUNIT')
	Local nDesc     := 0

	cquery := " Select COUNT(ZZ7_CODIGO) TOTAL "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery += " and D_E_L_E_T_ <> '*' "
	cquery += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
	cquery += " and ZZ7_TIPO = 'A'		"
	cquery += " and ZZ7_SUPLEM = 'S' " //Considera somente que n�o tem desconto
	TCQuery cquery new alias T0402

	nDesc :=  T0402->TOTAL * GETMV('SA_PRECDSC')

	T0402->(DBCLOSEAREA())

RETURN nDesc

Static Function ValVenda(oModel)

	Local aStruct	:= oModel:GetModel( 'ZZ3DETAIL' ):oFormModelStruct:GetFields()
	Local oModelSZ1	:= oModel:GetModel('ZZ0MASTER')
	Local oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')
	Local nValkit   := oModelZZ3:GetValue('ZZ3_VLUNIT')
	Local nDifer    := 0
	Local nDesc     := 0

		cquery := " Select COUNT(ZZ7_CODIGO) TOTAL "
		cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
		cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
		cquery += " and D_E_L_E_T_ <> '*' "
		cquery += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
		cquery += " and ZZ7_TIPO = 'A'		"
		cquery += " and ZZ7_SUPLEM = 'S' " //Considera somente que n�o tem desconto
		TCQuery cquery new alias T0402

		nDesc :=  T0402->TOTAL * GETMV('SA_PRECDSC')

		T0402->(DBCLOSEAREA())

		nValkit -= nDesc

		cquery := " Select COUNT(ZZ7_CODIGO) TOTAL "
		cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
		cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
		cquery += " and D_E_L_E_T_ <> '*' "
		cquery += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
		cquery += " and ZZ7_TIPO = 'A'		"
		cquery += " and ZZ7_PERDSC = '0'	" //Considera somente que n�o tem desconto
		cquery += " and ZZ7_SUPLEM = 'N'	"
		cquery += " and ZZ7_CATEGO IN ('A','C','D','V')	"

		TCQuery cquery new alias T0401

		FOR nX := 1 to T0401->TOTAL
			aadd(aValVenda,{NoRound(nValkit / T0401->TOTAL,2)})
			nDifer+= NoRound(nValkit / T0401->TOTAL,2)
		NEXT

		If len(aValVenda) > 0 .and. (nValkit <> nDifer)
			aValVenda[1][1] := aValVenda[1][1] + (nValkit - nDifer)
		Endif

		T0401->(dbclosearea())

Return aValVenda

STATIC FUNCTION YSASPO36(oView,oModel)
	LOCAL oModelZZ3		:= oModel:GetModel('ZZ3DETAIL')
	Local cQuantidade	:= Ascan(oModelZZ3:aHeader,{|x|Upper(Alltrim(x[2])) == "ZZ3_QUANT"})
	Local nVlrUnit		:= Ascan(oModelZZ3:aHeader,{|x|Upper(Alltrim(x[2])) == "ZZ3_VLUNIT"})
	Local nTotal		:= Ascan(oModelZZ3:aHeader,{|x|Upper(Alltrim(x[2])) == "ZZ3_VTOTAL"})
	Local nSaldo		:= Ascan(oModelZZ3:aHeader,{|x|Upper(Alltrim(x[2])) == "ZZ3_SALDO"})
	FOR nAx := 1 to len(oModelZZ3:acols)
		oModelZZ3:GoLine(nAx)

		If oModelZZ3:IsDeleted()
			Loop
		EndIf

		If oModelZZ3:GetValue("ZZ3_QUANT") <> 0
			Loop
		EndIf

		if oModelZZ3:getvalue("ZZ3_QUANT") == 0
		/*oModelZZ3:acols[nAx][cQuantidade] := oModelZZ3:acols[nAx][10]
		oModelZZ3:acols[nAx][nSaldo] := 0
		oModelZZ3:acols[nAx][nTotal] := oModelZZ3:acols[nAx][cQuantidade] * oModelZZ3:acols[nAx][nVlrUnit]*/
		oModel:GetModel("ZZ3DETAIL"):SetValue("ZZ3_QUANT" , oModelZZ3:GETVALUE('ZZ3_SALDO'))
		//oModel:SETVALUE('ZZ3DETAIL','ZZ3_QUANT',oModelZZ3:GETVALUE('ZZ3_SALDO'))
		endif
	NEXT
	oModelZZ3:GoLine(1)

RETURN

STATIC FUNCTION YSASPO36Ln(oView,oModel)
	LOCAL oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')

	if oModelZZ3:getvalue("ZZ3_QUANT") == 0
		oModel:SETVALUE('ZZ3DETAIL','ZZ3_QUANT',oModelZZ3:GETVALUE('ZZ3_SALDO'))
	ELSE 
		oModel:SETVALUE('ZZ3DETAIL','ZZ3_QUANT',0)
	ENDIF

RETURN

// Fun��o para verificar o produto a ser medido est� na tabela de pre�o informada no parametro SA_TABPREC

STATIC FUNCTION ProdOk(oModel,cQuery,aRetparam9)
	Local oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')
	Local oModelSZ1	:= oModel:GetModel('ZZ0MASTER')
	Local lRet      := .T.
	Local cTabPad   := GETMV("SA_TABPREC")
	Local cTab02    := GETMV("SA_TABPR2")
	Local nVal      := 0
	Local nVal2     := 0
	Local cProdErro := ""
	Local cPesoErro := ""
	Local lPeso     := .F.
	Local cCliente	:= oModelSZ1:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')
	Local cLoja		:= oModelSZ1:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')

	TCQuery cQuery new alias T08


	WHILE T08->(!EOF())

		IF T08->ZZ1_STATUS == "I" .OR. T08->ZZ1_SALDO == 0
			T08->(DbSkip())
			loop
		ENDIF
		// TRATAMENTO PARA PRODUTO
		If T08->ZZ1_TIPO == 'P'

			if aRetparam9 == "Triangular"
				nVal:= MaTabPrVen(cTabPad,T08->ZZ1_PRODUT,1,cCliente,cLoja)
				nVal2 := MaTabPrVen(cTab02,T08->ZZ1_PRODUT,1,cCliente,cLoja)
				if nVal == 0 .AND. nVal2 == 0
					lRet := .F.
					cProdErro += ""+alltrim(T08->ZZ1_PRODUT)+"-"+alltrim(POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(T08->ZZ1_PRODUT),"B1_DESC"))+""+ Chr(13) + Chr(10) +" "
				endif
			ELSE
				nVal := POSICIONE("ZZ1",2,XFILIAL("ZZ1")+ZZ0->ZZ0_NUMERO+cCliente+cLoja+T08->ZZ1_PRODUT+T08->ZZ1_TES,"ZZ1_VLRUN")
				nVal2 := POSICIONE("ZZ1",2,XFILIAL("ZZ1")+ZZ0->ZZ0_NUMERO+cCliente+cLoja+T08->ZZ1_PRODUT+T08->ZZ1_TES,"ZZ1_VLRUN")
			ENDIF
			if posicione("SF4",1,XFILIAL("SF4")+T08->ZZ1_TES,"F4_ESTOQUE") == "S" .AND. POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(T08->ZZ1_PRODUT),"B1_PESO") == 0
				lPeso := .T.
				cPesoErro += ""+alltrim(T08->ZZ1_PRODUT)+"-"+alltrim(POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(T08->ZZ1_PRODUT),"B1_DESC"))+""+ Chr(13) + Chr(10) +" "
			endif

		ENDIF

		// TRATAMENTO PARA KIT
		If T08->ZZ1_TIPO == 'K'
			cquery1 := " Select ZZ7_PRODUT "
			cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 "
			cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
			cquery1 += " and D_E_L_E_T_ <> '*' "
			cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(T08->ZZ1_PRODUT) +"' "
			cquery1 += " and ZZ7_TIPO = 'A' "
			cquery1 += " ORDER BY ZZ7_PRODUT"
			TCQuery cquery1 new alias T07

			While T07->(!EOF())
				if aRetparam9 == "Triangular"
					nVal:= MaTabPrVen(cTabPad,T07->ZZ7_PRODUT,1,cCliente,cLoja)
					nVal2 := MaTabPrVen(cTab02,T08->ZZ1_PRODUT,1,cCliente,cLoja)
					if nVal == 0 .AND. nVal2 == 0
						lRet := .F.
						cProdErro += ""+alltrim(T07->ZZ7_PRODUT)+"-"+alltrim(POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(T07->ZZ7_PRODUT),"B1_DESC"))+""+ Chr(13) + Chr(10) +" "
					endif
				else
					nVal := POSICIONE("ZZ1",2,XFILIAL("ZZ1")+ZZ0->ZZ0_NUMERO+cCliente+cLoja+T07->ZZ7_PRODUT+T08->ZZ1_TES,"ZZ1_VLRUN")
					nVal2 := POSICIONE("ZZ1",2,XFILIAL("ZZ1")+ZZ0->ZZ0_NUMERO+cCliente+cLoja+T07->ZZ7_PRODUT+T08->ZZ1_TES,"ZZ1_VLRUN")
				endif
				if posicione("SF4",1,XFILIAL("SF4")+T08->ZZ1_TES,"F4_ESTOQUE") == "S" .AND. POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(T07->ZZ7_PRODUT),"B1_PESO") == 0
					lPeso := .T.
					cPesoErro += ""+alltrim(T07->ZZ7_PRODUT)+"-"+alltrim(POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(T07->ZZ7_PRODUT),"B1_DESC"))+""+ Chr(13) + Chr(10) +" "
				endif
				T07->(dbskip())
			ENDDO
			T07->(DBCLOSEAREA())
		ENDIF
		T08->(dbskip())
	ENDDO
	T08->(DBCLOSEAREA())

	IF lRet == .F.
		oModelZZ3:SetErrorMessage('ZZ3DETAIL',,,,"ATEN��O", "Os seguintes produtos, n�o est�o na tabela de pre�o. Produtos :"+ Chr(13) + Chr(10)+""+cProdErro+"", "Verificar produtos cadastrados na tabela "+alltrim(cTabPad)+"")
	ENDIF

RETURN lRet

// Fun��o onde vai ser verificado o tipo de medi��o e se devidos cadastros est�o corretos, caso contrario, cancela a medi��o
STATIC Function validTipo(cTipo,oModel)
	Local cEspecie  := ""
	Local nDias
	Local oModelZZ0 := oModel:GETMODEL("ZZ0MASTER")
	Local oModelZZ3 := oModel:GETMODEL("ZZ3DETAIL")
	Local aSerieEnvio := {}
	Local dMinima
	Local dDataMax	:= CtOD("")

	//MONTA ARRAY COM A SERIE E ENVIO DE CADA PRODUTO, PABA BUSCAR A DATA MAXIMA DE ENTREGA
	FOR nAx := 1 to len(oModelZZ3:acols)
		oModelZZ3:GoLine(nAx)

		If oModelZZ3:IsDeleted()
			Loop
		EndIf

		If oModelZZ3:GetValue("ZZ3_QUANT") == 0
			Loop
		EndIf

		IF oModelZZ3:getvalue("ZZ3_TIPO") == "P"
			aadd(aSerieEnvio,{ALLTRIM(POSICIONE("SB1",1,XFILIAL("SB1")+alltrim(oModelZZ3:getvalue("ZZ3_PRODUT")),"B1_YSRPRD")),oModelZZ3:getvalue("ZZ3_ENVIO") })
		ELSEIF oModelZZ3:getvalue("ZZ3_TIPO") == "K"
			cquery1 := " select ZZ7_SERIE "
			cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 "
			cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
			cquery1 += " and D_E_L_E_T_ <> '*' "
			cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZ3:GetValue('ZZ3_PRODUT')) +"' "
			cquery1 += " and ZZ7_TIPO = 'A' "
			cquery1 += " ORDER BY ZZ7_PRODUT"

			If select("T13") > 0
				dbSelectArea("T13")
				T13->(DBCLOSEAREA())
			Endif

			TCQuery cquery1 new alias T13
			WHILE T13->(!EOF())
				aadd(aSerieEnvio,{ALLTRIM(T13->ZZ7_SERIE),oModelZZ3:getvalue("ZZ3_ENVIO") })
				T13->(DbSkip())
			ENDDO
			T13->(DBCLOSEAREA())
		ENDIF
	NEXT
	oModelZZ3:GoLine(1)

	//TODO RAFAEL TESTE
	cEspecie := POSICIONE("ZZG",1,XFILIAL("ZZG")+cTipo,'ZZG_ESPCTR')

	IF POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_FIXO')== 'S'

		FOR nAx:= 1 to len(aSerieEnvio)
			IF nAx == 1
				dMinima:=POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')
			ELSE
				IF !vazio(POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')) .AND. (POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA') < dMinima)
					dMinima:=POSICIONE("ZZM",1,XFILIAL("ZZM")+PADR(aSerieEnvio[nAx][1],GetSx3Cache("ZZM_SERIE","X3_TAMANHO"))+PADR(aSerieEnvio[nAx][2],GetSx3Cache("ZZM_ENVIO","X3_TAMANHO")),'ZZM_DATA')
				ENDIF
			ENDIF
		NEXT

	ELSEIF POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_FIXO')== 'N'
		FOR nAx:= 1 to len(aSerieEnvio)
			IF nAx == 1
				nDias := POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')
			ELSE
				IF !vazio(POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')) .AND. (POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS') < nDias)
					nDias := POSICIONE("ZZL",1,XFILIAL("ZZL")+cEspecie,'ZZL_DIAS')
				ENDIF
			ENDIF
		NEXT
	ENDIF

	IF !Empty(nDias)
		dDataMax := dSolicita + nDias
	elseIF !Empty(dMinima)
		dDataMax := dMinima
	ENDIF

Return dDataMax

//Retorna Saldo Final
User Function RETD36

	Local oModel	:= FWModelActive()

	nSaldo:= POSICIONE("ZZ1",3,XFILIAL("ZZ1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_ITEM')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT'),"ZZ1_SALDO")

	nRet:= nSaldo - oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_QUANT')

Return nRet

//Valida��o da Linha
Static Function RLINP36( oModelGrid, nLinha, cAcao, cCampo )

	Local lRet				:= .T.
	Local oModel	:= FWModelActive()
	Local nQtd		:= oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_QUANT')
	Local cEnvio	:= oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_ENVIO')
	Local cEnvPRV	:= oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_ENVPRV')



	If ((oModel:getOperation()  = 3) .OR. (oModel:getOperation()  = 4)) .and. ;
	(nQtd > 0) .and. ;
	(empty(cEnvio) .or. empty(cEnvPRV))

		oModel:SetErrorMessage('SZ3DETAIL',,,,"ERRO", 'N�mero do envio n�o informado', 'Informe o c�digo do envio')
		Return .F.
	Endif


Return lRet

//Busca se o item est� no contrato
Static Function ITEMZZ1
	Local lRet				:= .T.
	Local oModel	:= FWModelActive()

	If POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT'),+;
	"ZZ1_PRODUT") <> +;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')

		oModel:SetErrorMessage('SZ3DETAIL',,,,"ERRO", 'Produto n�o pertence ao contrato', 'Verifique contrato')
		Return .F.
	Endif

Return lret


//Gatilho da TES
User Function FTTES36

	Local oModel:= FWModelActive()

	cTES:= POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT'),"ZZ1_TES")

Return cTES

//Gatilho do Valor Unit�rio
User Function FRTVNT36

	Local oModel:= FWModelActive()

	nVrUnit:= POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT'),"ZZ1_VLRUN")

Return nVrUnit

//Gatilho do Valor Total
User Function FRTVT36

	Local oModel:= FWModelActive()

	nVrTOT:= POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT'),"ZZ1_VLRTOT")

Return nVrTOT

//Gatilho para a descri��o
User Function FRTSC36
	Local oModel:= FWModelActive()
	Local cRet	:= ""

	cItem:= POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT'),"ZZ1_ITEM")

	oModel:SETVALUE('ZZ3DETAIL','ZZ3_ITEM',cItem)

	nSaldo:= POSICIONE("ZZ1",2,XFILIAL("ZZ1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_NUMERO')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+;
	oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')+;
	oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_TES'),"ZZ1_SALDO")

	oModel:SETVALUE('ZZ3DETAIL','ZZ3_SALDO',nSaldo)

	dbSelectArea("SB1")
	SB1->(dbSetOrder(1))
	If SB1->(dbSeek(xFilial("SB1")+oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')))
		cRet:= SB1->B1_DESC

		oModel:SETVALUE('ZZ3DETAIL','ZZ3_TIPO','P')
		oModel:SETVALUE('ZZ3DETAIL','ZZ3_ENVIO',SB1->B1_YVOL)
		oModel:SETVALUE('ZZ3DETAIL','ZZ3_ENVPRV',SB1->B1_YVOL)

	Else
		dbSelectArea("ZZ7")
		ZZ7->(dbSetOrder(1))
		If ZZ7->(dbSeek(xFilial("ZZ7")+oModel:GetModel('ZZ3DETAIL'):GetValue('ZZ3_PRODUT')))
			cRet:= ZZ7->ZZ7_DESCR
		Endif

		oModel:SETVALUE('ZZ3DETAIL','ZZ3_TIPO','K')
		oModel:SETVALUE('ZZ3DETAIL','ZZ3_ENVIO',ZZ7->ZZ7_ENVIO)
		oModel:SETVALUE('ZZ3DETAIL','ZZ3_ENVPRV',ZZ7->ZZ7_ENVIO)

	Endif

Return cRet