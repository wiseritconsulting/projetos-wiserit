#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"
#include "Rwmake.ch"

/*/{Protheus.doc} SASP033
Ativar ou desativar itens do contrato
@author Erenilson
@since 22/09/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function SASP033()

	Local cPerg := "SASP033"
	
	If ZZ0->ZZ0_STATUS = 'E'
		Alert("Contrato j� encerrado, n�o podendo ser modificado")
		Return
	Endif
	
	If ZZ0->ZZ0_STATUS = 'B'
		Alert("Contrato bloqueado, n�o podendo ser modificado")
		Return
	Endif
	
	If ZZ0_STATUS == 'A'
		Alert("Contrato n�o se encontra liberado, n�o podendo ser modificado")
		Return
	Endif
	
	AjustaSX1(cPerg)

	//Se for clicado em Ok chama a grid com os produtos a partir dos parametros selecionados.
	IF Pergunte(cPerg,.T.)
		
		BloqItem()
		
	ENDIF

Return

Static Function BloqItem()

	Local cQuery := ""
	Local cStatus := "" 
	Local _lRetorno := .F. //Validacao da dialog criada oDlg 
	Local _nOpca := 0 //Opcao da confirmacao 
	Local _aStruTrb := {} //Estrutura da tabela tempor�ria 
	Local _aBrowse := {} //Array do browse para demonstrac�o dos produtos
	Local bOk := {|| oBrwTrb:bMark := {| | Disp()}	,_nOpca:=1,_lRetorno:=.T.,oDlg:End() } //Bot�o de ok 
	Local bCancel := {|| _nOpca:=0,oBrwTrb:oBrowse:Refresh(),oDlg:End() } //Bot�o de cancelamento 	
	Private lInverte := .F. //Variaveis para o MsSelect 
	Private cMarca := GetMark() //Variaveis para o MsSelect 
	Private oBrwTrb //objeto do msselect 
	Private oDlg //Objeto Dialog
	

	If Select("TRB") > 0 
		DbSelectArea("TRB") 
		DbCloseArea()
	EndIf 
	
	If Select("XTABPROD") > 0 
		DbSelectArea("XTABPROD") 
		DbCloseArea()
	EndIf 
	
	cStatus := IIF(mv_par01==1,'A','I')
	
	cQuery := ""
	cQuery += " SELECT " 
	cQuery += "    ZZ1_ITEM, "
	cQuery += "    ZZ1_PRODUT, "
	cQuery += "    ZZ1_DESC, "
	cQuery += "    ZZ1_QUANT, "
	cQuery += "    ZZ1_SALDO, "
	cQuery += "    ZZ1_VLRUN, "
	cQuery += "    ZZ1_VLRTOT, "
	cQuery += "    CASE " 
	cQuery += "      WHEN ZZ1_TIPO = 'K' THEN 'KIT' "
	cQuery += "      WHEN ZZ1_TIPO = 'P' THEN 'PRODUTO' "
	cQuery += "    END AS ZZ1_TIPO "
	cQuery += "   FROM " + RETSQLNAME("ZZ1") + " ZZ1 "
	cQuery += "  WHERE D_E_L_E_T_ = '' "
	cQuery += "    AND ZZ1_NUMERO = '" + ZZ0->ZZ0_NUMERO + "' "
	cQuery += "    AND ZZ1_FILIAL = '" + ZZ0->ZZ0_FILIAL + "' "
	cQuery += "    AND ZZ1_STATUS = '" + cStatus + "' "	
	
	TcQuery cQuery New Alias XTABPROD 
	
	DBSELECTAREA("XTABPROD")	
	
	//Definindo os campos para a tabela tempor�ria
	AADD(_aStruTrb,{"OK" ,"C",2,0}) //Campo de OK
	AADD(_aStruTrb,{"CODITEM" ,"C",GetSX3Cache("ZZ1_ITEM","X3_TAMANHO"),0})      //Item do contrato
	AADD(_aStruTrb,{"CODPROD" ,"C",GetSX3Cache("B1_COD","X3_TAMANHO"),0})      //C�digo do produto
	AADD(_aStruTrb,{"DESCPROD" ,"C",GetSX3Cache("B1_DESC","X3_TAMANHO"),0})    //Descri��o do produto
	AADD(_aStruTrb,{"QUANTPROD" ,"N",GetSX3Cache("ZZ1_QUANT","X3_TAMANHO"),0})    //Quantidade do produto
	AADD(_aStruTrb,{"SALDOPROD" ,"N",GetSX3Cache("ZZ1_SALDO","X3_TAMANHO"),0})    //Saldo do produto
	AADD(_aStruTrb,{"VLRPROD" ,"C",GetSX3Cache("ZZ1_VLRUN","X3_TAMANHO"),0})    //Valor do produto
	AADD(_aStruTrb,{"VLRTOT" ,"C",GetSX3Cache("ZZ1_VLRTOT","X3_TAMANHO"),0})    //Valor total do produto
	AADD(_aStruTrb,{"TIPOPROD" ,"C",GetSX3Cache("ZZ1_TIPO","X3_TAMANHO"),0})    //Tipo do produto	
		
	//Definindo os campos para o GRID
	AADD(_aBrowse,{"OK" ,,""})  //Campo de OK
	AADD(_aBrowse,{"CODITEM" ,"","Item"})    //Descri��o do produto
	AADD(_aBrowse,{"CODPROD" ,,"C�digo"})      //C�digo do produto
	AADD(_aBrowse,{"DESCPROD" ,"","Descri��o"})    //Descri��o do produto
	AADD(_aBrowse,{"QUANTPROD" ,"","Quantidade"})    //Quantidade do produto
	AADD(_aBrowse,{"SALDOPROD" ,"","Saldo"})    //Saldo do produto
	AADD(_aBrowse,{"VLRPROD" ,"","Valor Unit�rio(R$)"})    //Valor do produto
	AADD(_aBrowse,{"VLRTOT" ,"","Total(R$)"})    //Valor total do produto
	AADD(_aBrowse,{"TIPOPROD" ,"","Tipo"})    //Tipo do produto
	
	_cArqEmp := CriaTrab(_aStruTrb,.T.)
	
	dbUseArea(.T.,__LocalDriver,_cArqEmp,"TRB")   
	cIndex := CriaTrab(nil,.f.)
	cChave := "CODPROD"
	IndRegua("TRB",cIndex,cChave,,,"Selecionando Registros...")	
	
	XTABPROD->(DBGOTOP())
	
	While XTABPROD->(!EOF())
	
		RecLock("TRB",.T.)
		
			TRB->OK := space(2)
			TRB->CODITEM := XTABPROD->ZZ1_ITEM
			TRB->CODPROD := XTABPROD->ZZ1_PRODUT
			TRB->DESCPROD := XTABPROD->ZZ1_DESC
			TRB->QUANTPROD := XTABPROD->ZZ1_QUANT
			TRB->SALDOPROD :=  XTABPROD->ZZ1_SALDO
			TRB->VLRPROD := TRANSFORM(XTABPROD->ZZ1_VLRUN, "@E 999999,999.99")  
			TRB->VLRTOT := TRANSFORM(XTABPROD->ZZ1_VLRTOT, "@E 999999,999.99")  	
			TRB->TIPOPROD := XTABPROD->ZZ1_TIPO		
		
		MsUnlock()	
	
	XTABPROD->(DBSKIP())
	EndDo
	
	XTABPROD->(DBCLOSEAREA())
	
	if mv_par01==1
	
		@ 001,001 TO 400,700 DIALOG oDlg TITLE OemToAnsi("Inativa��o de itens do contrato: " + ZZ0->ZZ0_NUMERO)
	
	Else
	
		@ 001,001 TO 400,700 DIALOG oDlg TITLE OemToAnsi("Ativa��o de itens do contrato: " + ZZ0->ZZ0_NUMERO)
	
	EndIf	
 	
	oBrwTrb := MsSelect():New("TRB","OK","",_aBrowse,@lInverte,@cMarca,{025,001,170,350})
	oBrwTrb:oBrowse:lCanAllmark := .T.	
	oBrwTrb:oBrowse:align := CONTROL_ALIGN_ALLCLIENT
	oBrwTrb:oBrowse:bAllMark := {|| MarcaTudo()}	 
	Eval(oBrwTrb:oBrowse:bGoTop) 
	oBrwTrb:oBrowse:Refresh()
	 
 	oDlg:lMaximized := .T.
	Activate MsDialog oDlg On Init (EnchoiceBar(oDlg,bOk,bCancel,,)) Centered VALID _lRetorno
	
	If _nOpca == 1	
		FAtiIna()
	Endif
	
	If Select("TRB") > 0 
		DbSelectArea("TRB") 
		DbCloseArea()
	EndIf 

Return

Static Function FAtiIna()

	Local cStatus := "" 
	Local cHora := ""
	Local cData := ""
	Local cUser := ""	
	
	cStatus := IIF(mv_par01==1,'I','A')
	cHora   := IIF(mv_par01==1,Time(),"")
	cData   := IIF(mv_par01==1,Date(),ctod(""))
	cUser   := IIF(mv_par01==1,RetCodUsr(),"")	
	nCont	:= 0
	DBSELECTAREA("TRB")
	TRB->(DBGOTOP())
	
	While TRB->(!EOF())
	
		If !Empty(TRB->OK)
		
			DBSELECTAREA("ZZ1")
			DBSETORDER(1)
			DBSEEK(ZZ0->ZZ0_FILIAL + ZZ0->ZZ0_NUMERO + TRB->CODITEM)
			
			If Found()
				nCont+=1
				RECLOCK("ZZ1",.F.)
				
					ZZ1->ZZ1_STATUS := cStatus
					ZZ1->ZZ1_HRINAT := cHora
					ZZ1->ZZ1_DTINAT := cData
					ZZ1->ZZ1_USINAT := cUser
				
				MSUNLOCK()
			
			EndIf		
		
		EndIf
	
		TRB->(DBSKIP())
	ENDDO
	
	TRB->(DBCLOSEAREA())
	
		//Identifica quantos registros existem para o contrato em quest�o
		cQuery:= "SELECT COUNT(*) QTD FROM "+RetSqlName("ZZ1")+" "
		cQuery+= " WHERE ZZ1_FILIAL+ZZ1_NUMERO+ZZ1_CLIENT+ZZ1_LOJA = '"+ZZ0->(ZZ0_FILIAL+ZZ0_NUMERO+ZZ0_CLIENT+ZZ0_LOJA)+"'"
		cQuery+= " AND D_E_L_E_T_ =' '  "
		cQuery+= " AND ZZ1_STATUS <> 'I'  "
		TcQuery cQuery New Alias XQRY331
		
		//Atualiza cabe�alho
		RecLock("ZZ0",.F.)
			ZZ0->ZZ0_STATUS	:=	IIf(XQRY331->QTD == 0,'I','L')
		MsUnlock()
		
		XQRY331->(dbCloseArea())

Return

Static Function  MarcaTudo() 

	// Guarda posi��o do ponteiro atual
	Local nReg := TRB->(RECNO())
	Local cMar := "  "

	// Inicio do Arquivo
	TRB->(DBGOTOP())

	// Verifica controle de marcacao
	If lInverte
		cMar := cMarca
		lInverte := .F.
	Else
		cMar := "  "
		lInverte := .T.
	EndIF

	// Percorre todo o TRB
	While !TRB->(EOF())
		// Atualiza a marca��o
		RecLock("TRB",.F.)				
				TRB->OK    := cMar				
		MsUnlock("TRB")
		// Pr�ximo registro
		TRB->(DBSKIP())

	EndDo

	// Volta o cursos para a posi��o antes de marcar desmarcar tudo
	TRB->(dbGoTo(nReg))

Return()


Static Function AjustaSX1(cPerg)

	PutSx1(cPerg, "01","Tipo de Inativa��o?",				  		 "","","mv_ch01","C",01,0,0,"C","","   ","","","mv_par01","1-Inativa��o","1-Inativa��o","1-Inativa��o","2-Ativa��o","2-Ativa��o","2-Ativa��o",,,,"","","","","","","")

Return