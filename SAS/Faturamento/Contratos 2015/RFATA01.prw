/*/{Protheus.doc} RFATA01
Rotina de altera��o de Cabe�alho chamado no Pedido de Vendas
@author Diogo
@since 10/09/2014
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User Function RFATA01()
Local nOpca := 0
Local aArea := GetArea()
Private cCadastro := "Altera��o cabe�alho"
Private aCpos := {} // CAMPOS que permitem edi��o

DbSelectArea("SX3")
DbSetOrder(1)
DbSeek("SC5")
	While SX3->(!Eof()) .AND. SX3->X3_ARQUIVO == "SC5"
		If X3Uso(SX3->X3_USADO) .and. !( Alltrim(SX3->X3_CAMPO) $ "C5_NUM|C5_TIPO|C5_CLIENTE|C5_LOJACLI|"+;
										"C5_TIPOCLI|C5_CONDPAG|C5_EMISSAO" )
			AAdd( aCpos, SX3->X3_CAMPO )
		Endif
		SX3->(dbSkip())	
	Enddo	
	
	
dbSelectArea("SC6")

Private aNoFields := {"C6_NUM","C6_QUANT","C6_QTDEMP","C6_QTDENT","C6_QTDEMP2","C6_QTDENT2"} // Campos que nao devem entrar no aHeader e aCols
Private n:= 1

FillGetDados(4,"SC6",1,/*cSeek*/,/*{|| &cWhile }*/,{||.T.},aNoFields,/*aYesFields*/,/*lOnlyYes*/,/*cQuery*/,/*bMontCols*/,.T.)

dbSelectArea("SC5")

nOpca := AxAltera("SC5",SC5->(Recno()),4, ,aCpos,,,,,,,,,.T.)

RestArea(aArea)

Return