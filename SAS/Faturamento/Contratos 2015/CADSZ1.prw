#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"

//-------------------------------------------------------------------
/*/{Protheus.doc} CADSZ1
Cadastro Contratos

@author Saulo Gomes Martins
@since 24/07/2012
/*/
//-------------------------------------------------------------------
Static aCacheTES	:= {}
USER FUNCTION CADSZ1()

	Local oBrowse
	aCacheTES	:= {}	//Limpa o Cache das TES
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('SZ1')
	oBrowse:SetDescription('Cadastro de Contrato')
	oBrowse:DisableDetails()
	oBrowse:AddLegend( "Empty(Z1_USERAPR) .AND. Z1_SALDO>0", "YELLOW", "Incluido"  )
	oBrowse:AddLegend( "Z1_SALDO>0", "GREEN", "Liberado"  )
	oBrowse:AddLegend( "Z1_SALDO==0", "RED", "Encerrado"  )
	oBrowse:SetMenuDef( 'CADSZ1' )
	//oBrowse:SetFilterDefault("Z1_MSFIL=='"+cFilAnt+"'")
	oBrowse:Activate()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional
@obs aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
9 - C�pia
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
	Local aRotina := {}
	ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PesqBrw"			OPERATION 1 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.CADSZ1"	OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.CADSZ1"	OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.CADSZ1"	OPERATION 4 ACCESS 143
	ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.CADSZ1"	OPERATION 5 ACCESS 144
	ADD OPTION aRotina TITLE "Imprimir"   ACTION "VIEWDEF.CADSZ1"	OPERATION 8 ACCESS 0
	//ADD OPTION aRotina TITLE "Copiar"     ACTION "VIEWDEF.CADSZ1"	OPERATION 9 ACCESS 0
	ADD OPTION aRotina TITLE "Aprova��o"  ACTION "U_YSZ1001()"		OPERATION 10 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Medi��o"    ACTION "U_YSZ1002"		OPERATION 4 ACCESS 0 DISABLE MENU
	//ADD OPTION aRotina TITLE "Estorno"    ACTION "U_CADSZ5"			OPERATION 4 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Medi��o Automatica"   ACTION "U_YSZ1004"		OPERATION 4 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Aditivo"    ACTION "U_YSZ1003"		OPERATION 4 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Add Item"    ACTION "U_YSZ1007"		OPERATION 4 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Ver Adiantamentos"    ACTION "U_YSZ1008"		OPERATION 2 ACCESS 0 DISABLE MENU

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	Local oStructSZ1 := Nil
	Local oStructSZ2 := Nil
	Local oModel     := Nil
	Local nI
	//-----------------------------------------
	//Monta a estrutura do formul�rio com base no dicion�rio de dados
	//-----------------------------------------
	oStructSZ1 := FWFormStruct(1,"SZ1")
	oStructSZ2 := FWFormStruct(1,"SZ2")

	oStructSZ2:AddTrigger( ;
		"Z2_QUANT"			,;										//[01] Id do campo de origem
	"Z2_TOTAL"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("Z2_QUANT") * oModel:GetValue("Z2_VLUNIT") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ2:AddTrigger( ;
		"Z2_VLUNIT"		,;										//[01] Id do campo de origem
	"Z2_TOTAL"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("Z2_QUANT") * oModel:GetValue("Z2_VLUNIT") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ2:AddTrigger( ;
		"Z2_PRODUTO"		,;										//[01] Id do campo de origem
	"Z2_DESC"			,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("SB1",oModel:GetValue('Z2_PRODUTO'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SB1",1,xFilial("SB1")+oModel:GetValue("Z2_PRODUTO"),"B1_DESC") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ2:AddTrigger( ;
		"Z2_PRODUTO"		,;										//[01] Id do campo de origem
	"Z2_DESC"			,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("SZ6",oModel:GetValue('Z2_PRODUTO'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SZ6",1,xFilial("SZ6")+oModel:GetValue("Z2_PRODUTO"),"Z6_DESC") }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructSZ2:AddTrigger( ;
	"Z2_PRODUTO"		,;										//[01] Id do campo de origem
	"Z2_DESCONT"		,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("SZ6",oModel:GetValue('Z2_PRODUTO'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_RetDscSZ1(@oModel) }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructSZ2:AddTrigger( ;
	"Z2_QUANT"		,;										//[01] Id do campo de origem
	"Z2_DESCONT"		,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("SZ6",oModel:GetValue('Z2_PRODUTO'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| u_RetDscSZ1(@oModel) }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructSZ2:AddTrigger( ;
	"Z2_PRODUTO"		,;										//[01] Id do campo de origem
	"Z2_VLRLIQ"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("Z2_TOTAL") - oModel:GetValue("Z2_DESCONT") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ2:AddTrigger( ;
	"Z2_QUANT"		,;										//[01] Id do campo de origem
	"Z2_VLRLIQ"		,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("Z2_TOTAL") - oModel:GetValue("Z2_DESCONT") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ1:AddTrigger( ;
		"Z1_CLIENTE"		,;										//[01] Id do campo de origem
	"Z1_NOME"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| SUBSTR(Posicione("SA1",1,xFilial("SA1")+oModel:GetValue('Z1_CLIENTE')+oModel:GetValue('Z1_LOJA'),"A1_NOME"),1,40) } )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ1:AddTrigger( ;
		"Z1_LOJA"		,;											//[01] Id do campo de origem
	"Z1_NOME"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| SUBSTR(Posicione("SA1",1,xFilial("SA1")+oModel:GetValue('Z1_CLIENTE')+oModel:GetValue('Z1_LOJA'),"A1_NOME"),1,40) } )	// [04] Bloco de codigo de execu��o do gatilho

	//-----------------------------------------
	//Monta o modelo do formul�rio
	//-----------------------------------------

	oModel:= MPFormModel():New("YCADSZ1",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
	oModel:AddFields("SZ1MASTER",/*cOwner*/, oStructSZ1 ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)

	oModel:AddGrid("SZ2DETAIL", "SZ1MASTER"/*cOwner*/,oStructSZ2, ,/*bLinePost*/,/*bPre*/,/*bPost*/,/*Carga*/)
	oModel:SetPrimaryKey({"Z1_FILIAL","Z1_CONTRAT"})
	//oModel:GetModel("SZ2DETAIL"):GetStruct():SetProperty("Z2_VLUNIT",MODEL_FIELD_VALID,{||SZ1ValCampo()})
	oModel:GetModel("SZ2DETAIL"):GetStruct():SetProperty("Z2_PRODUTO",MODEL_FIELD_VALID,{|oModel|SZ1ValProd(oModel) })
	oModel:GetModel("SZ2DETAIL"):GetStruct():SetProperty("Z2_TES",MODEL_FIELD_VALID,{|oModel|SZ1ValTES(oModel) })

	// Adiciona ao modelo uma estrutura de formul�rio de campos calculados
	// AddCalc(cId, cOwner , cIdForm , cIdField , cIdCalc, cOperation, bCond
	oModel:AddCalc( 'YCADSZ1CALC1', 'SZ1MASTER', 'SZ2DETAIL', 'Z2_QTDMED', 'Z2__TOT01', 'SUM',,,'Total Medida',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"QTDMED") }*/ )
	oModel:AddCalc( 'YCADSZ1CALC1', 'SZ1MASTER', 'SZ2DETAIL', 'Z2_VLRLIQ', 'Z2__TOT02', 'SUM',,,'Valor Total',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)
	oModel:AddCalc( 'YCADSZ1CALC1', 'SZ1MASTER', 'SZ2DETAIL', 'Z2_VLRLIQ', 'Z2__TOT03', 'SUM',{|oModel| cDuplic:="",GetCacheSF4(oModel:GetValue("SZ2DETAIL",'Z2_TES'),"F4_DUPLIC",@cDuplic) .and. cDuplic=="S" },,'Valor Venda',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"VENDA") }*/)

	oModel:GetModel("SZ1MASTER"):SetDescription("Contrato")
	oModel:GetModel("SZ2DETAIL"):SetDescription("Itens Contrato")
	oModel:GetModel("SZ2DETAIL"):SetMaxLine(1500)//WESKLEY SILVA - 20/08/2015
	oModel:SetRelation("SZ2DETAIL",{{"Z2_FILIAL",'xFilial("SZ2")'},{"Z2_CONTRAT","Z1_CONTRAT"}},SZ2->(IndexKey(1)))
	oModel:SetVldActive( { |oModel| SZ1ValAct( oModel ) } )
	oModel:SetCommit({|oModel| SZ1Commit(oModel) },.F.)
	oModel:SetCommit({|oModel| FWFormCommit(oModel) },.T.)

	oModel:AddRules( 'SZ2DETAIL', 'Z2_QUANT'	,'SZ2DETAIL'	,'Z2_PRODUTO'	,1 )
	oModel:AddRules( 'SZ2DETAIL', 'Z2_VLUNIT'	,'SZ2DETAIL'	,'Z2_PRODUTO'	,1 )
	oModel:AddRules( 'SZ2DETAIL', 'Z2_TOTAL'	,'SZ2DETAIL'	,'Z2_PRODUTO'	,1 )
	oModel:AddRules( 'SZ2DETAIL', 'Z2_TES'		,'SZ2DETAIL'	,'Z2_PRODUTO'	,1 )

Return(oModel)

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
	Local oStructSZ1	:= FWFormStruct( 2, 'SZ1' )
	Local oStructSZ2	:= FWFormStruct( 2, 'SZ2' )
	Local oModel		:= FWLoadModel( 'CADSZ1' )
	Local oView
	Local oCalc1
	oStructSZ2:RemoveField( 'Z2_CONTRAT' )

	// Cria o objeto de Estrutura
	oCalc1	:= FWCalcStruct( oModel:GetModel( 'YCADSZ1CALC1') )
	oView	:= FWFormView():New()

	//oView:SetUseCursor(.F.)
	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField( "SZ1MASTER",oStructSZ1)
	oView:AddGrid("SZ2DETAIL",oStructSZ2)
	oView:AddField( 'VIEW_CALC', oCalc1, 'YCADSZ1CALC1' )

	oView:CreateHorizontalBox("CABEC",30)
	oView:CreateHorizontalBox("GRID",60)
	oView:CreateHorizontalBox( 'RODAPE',10)

	oView:SetOwnerView( "SZ1MASTER","CABEC")
	oView:SetOwnerView( "SZ2DETAIL","GRID")
	oView:SetOwnerView( 'VIEW_CALC', 'RODAPE' )

	oView:SetProgressBar(.T.)
	oView:AddIncrementField("SZ2DETAIL","Z2_ITEM")
	oView:EnableTitleView('SZ2DETAIL','Itens Contrato')

	oView:AddUserButton( 'Procurar Kit', 'CLIPS', { |oView| ProcKit(oView) }, , VK_F4,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} )

Return oView

Static Function SZ1ValAct(oModel)
	Local lRet:= .T.
	Local cContrato
	If oModel:getOperation() == MODEL_OPERATION_INSERT
		If cFilAnt=="010105"
			oModel:SetErrorMessage('SZ1MASTER',,,,"ATEN��O", 'Impossivel realizar contratos na empresa Matriz',"Escolha outra filial para criar o contrato" )
			Return .F.
		EndIf
	ElseIf oModel:getOperation() == MODEL_OPERATION_DELETE
		cContrato	:= SZ1->Z1_CONTRAT
		If SZ1->Z1_QTDMED>0
			oModel:SetErrorMessage('SZ1MASTER',,,,"ATEN��O", 'Contrato j� possui medi��es realizadas, n�o pode ser excluido!', )
			Return .F.
		EndIf
		SE1->(DbSetOrder(1))	//E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
		If SE1->(DbSeek(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+cContrato)) .AND. !Empty(SE1->E1_BAIXA)
			oModel:SetErrorMessage('SZ1MASTER',,,,"ATEN��O", 'O titulo relacionando a esse contrato j� possui movimenta��es!', 'Para excluir � necessario excluir os movimentos primeiro!')
			Return .F.
		EndIf
	ElseIf oModel:getOperation() == MODEL_OPERATION_UPDATE
		If	!Empty(SZ1->Z1_USERAPR)
			oModel:SetErrorMessage('SZ1MASTER',,,,"ATEN��O", 'Contrato j� liberado, n�o pode ser alterado!', 'Para altera��o realize a adi��o do contrato!')
			Return .F.
		EndIf
	EndIf
Return lRet

Static Function SZ1Commit(oModel)
	Local cContrato	:= oModel:GetModel( 'SZ1MASTER' ):GetValue('Z1_CONTRAT')
	Local nSaldo		:= 0
	Local nVlAdiant	:= 0
	Local nQtd			:= 0
	Local lRet			:= .T.
	Local nCont
	Local aDados		:= {}
	Private lMsErroAuto	:= .F.
	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
	If	oModel:getOperation() == MODEL_OPERATION_UPDATE .OR.;
			oModel:getOperation() == MODEL_OPERATION_INSERT
		oModelSZ2	:= oModel:GetModel( 'SZ2DETAIL' )
		For nCont := 1 To oModelSZ2:Length()
			oModelSZ2:GoLine( nCont )
			If oModelSZ2:IsDeleted()
				Loop
			EndIf
			nSaldo	+= oModelSZ2:GetValue('Z2_QUANT')+oModelSZ2:GetValue('Z2_QTDADT')-oModelSZ2:GetValue('Z2_QTDMED')
			nQtd	+= oModelSZ2:GetValue('Z2_QUANT')//+oModelSZ2:GetValue('Z2_QTDADT')
			oModelSZ2:LoadValue("Z2_SALDO", oModelSZ2:GetValue('Z2_QUANT')+oModelSZ2:GetValue('Z2_QTDADT')-oModelSZ2:GetValue('Z2_QTDMED'))
			If SF4->(DbSeek(xFilial("SF4")+oModelSZ2:GetValue('Z2_TES')))  .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
				nVlAdiant	+= oModelSZ2:GetValue('Z2_VLRLIQ')
			EndIf
		Next
		oModel:LoadValue("SZ1MASTER", "Z1_QTDORIG", nQtd)
		oModel:LoadValue("SZ1MASTER", "Z1_SALDO", nSaldo)
		oModel:LoadValue("SZ1MASTER", "Z1_VLADT", nVlAdiant+(nVlAdiant*oModel:GetModel( 'SZ1MASTER' ):GetValue('Z1_FRETEC')/100))
	ElseIf oModel:getOperation() == MODEL_OPERATION_DELETE
		SE1->(DbSetOrder(1))	//E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
		Begin Transaction
			While SE1->(DbSeek(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+PadR(cContrato,GetSx3Cache("E1_NUM","X3_TAMANHO"))))
				AADD(aDados,{"E1_FILIAL"		,xFilial("SE1")		,"#"})
				AADD(aDados,{"E1_PREFIXO"	,SE1->E1_PREFIXO		,"#"})
				AADD(aDados,{"E1_NUM"		,SE1->E1_NUM			,"#"})
			/*AADD(aDados,{"E1_PARCELA"	,SE1->E1_PARCELA		,"#"})
			AADD(aDados,{"E1_TIPO"		,SE1->E1_TIPO			,"#"})
			AADD(aDados,{"INDEX"			,1						,"#"})*/
				MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 5)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
				If lMsErroAuto
					DisarmTransaction()
					AutoGrLog("Erro ao excluir o titulo!")
					AutoGrLog("Opera��o cancelada!")
					MostraErro()
					oModel:SetErrorMessage('SZ1MASTER',,,,"ERRO", 'Erro ao excluir o titulo, opera��o cancelada!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				Endif
			EndDo
		End Transaction
	EndIf
Return lRet

Static Function Total02(oModel,cTipo)
	Local nCont		:= 0
	Local nTotal		:= 0
	Local oModelSZ2	:= oModel:GetModel( 'SZ2DETAIL' )
	Local aSaveLines := FWSaveRows()
	Local cDuplic
	Default cTipo		:= "TOTAL"

	For nCont := 1 To oModelSZ2:Length()
		oModelSZ2:GoLine( nCont )
		If oModelSZ2:IsDeleted()
			Loop
		EndIf
		If cTipo=="TOTAL"
			nTotal	+= oModelSZ2:GetValue('Z2_VLRLIQ')
		ElseIf cTipo=="QTDMED"
			nTotal	+= oModelSZ2:GetValue('Z2_QTDMED')
		ElseIf cTipo=="VENDA"
			If GetCacheSF4(oModelSZ2:GetValue('Z2_TES'),"F4_DUPLIC",@cDuplic) .and. cDuplic=="S"	//Gera Financeiro
				nTotal	+= oModelSZ2:GetValue('Z2_VLRLIQ')
			EndIf
		EndIf
	Next
	FWRestRows( aSaveLines )
Return nTotal

/*/{Protheus.doc} YSZ1001
Aprova��o do documento
@author Saulo Gomes Martins
@since 16/09/2014
@version 1.0
@return lRet, Logico se opera��o realizada com sucesso
/*/
User Function YSZ1001(lMsg,cNomeUsr,cTime)
	Local aDados	:= {}
	Local aCondicao:= {}
	Default cNomeUsr	:= UsrFullName(RetCodUsr())
	Default cTime		:= Time()
	Default lMsg		:= .T.
	Private lMsErroAuto	:= .F.
	If !Empty(SZ1->Z1_USERAPR)
		If lMsg
			MsgAlert("Documento j� aprovado!")
		EndIf
		Return .F.
	Elseif lMsg .AND. !MsgYesNo("Deseja aprovar o documento?")
		Return .F.
	EndIf
	If SZ1->Z1_VLADT > 0 .OR. SZ1->Z1_VLADD >0

		nValFin	:= iif(SZ1->Z1_VLADD > 0 , SZ1->Z1_VLADD , SZ1->Z1_VLADT) //Verifica se j� teve item adicionado
		nDesc	:= FDescZ1(SZ1->Z1_CONTRAT,SZ1->Z1_CLIENTE,SZ1->Z1_LOJA) //Busca desconto referente a produto kit
		nValDsc	:= 0 //Total do desconto por parcela

		SE1->(dbSetOrder(1))
		Begin Transaction
			aCondicao	:= Condicao(nValFin,SZ1->Z1_CPFIN,,SZ1->Z1_DTCOND)
			If Empty(aCondicao)
				DisarmTransaction()
				AutoGrLog("Erro ao gerar parcelas do titulo!")
				AutoGrLog("Verifique a condi��o de pagamento e o valor total!")
				MostraErro()
				Return .F.
			EndIf

			//Verifica se j� existe T�tulo para o contrato
			cParc:= ""
			nTotD:= 0
			If SE1->(dbSeek(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+SZ1->Z1_CONTRAT)) //E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
				While SE1->(!eof()) .and. Alltrim(SE1->(E1_FILIAL+E1_PREFIXO+E1_NUM)) == Alltrim(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+SZ1->Z1_CONTRAT)
					cParc:= SE1->E1_PARCELA
					nTotD+=	SE1->E1_DECRESC
					SE1->(dbSkip())
				Enddo
			Endif
			If !empty(cParc)
				cParc:= soma1(cParc)
			Endif

			if nDesc > 0 //Existe produto Kit com desconto
				nValDsc:= Round(((nDesc-nTotD) / (len(aCondicao))),2) //Rateia desconto
			endif


			For nCont:=1 to Len(aCondicao)
				//xParc:= iif(empty(cParc),cValToChar(nCont),cParc)
				aDados	:= {}
				AADD(aDados,{"E1_PREFIXO"	,"A"+SUBSTR(CFILANT,5,2),nil})
				AADD(aDados,{"E1_NUM"		,SZ1->Z1_CONTRAT		,nil})
				AADD(aDados,{"E1_TIPO"		,"DP "					,nil})
				AADD(aDados,{"E1_PARCELA"	,RetAsc(nCont,GetSx3Cache("E1_PARCELA","X3_TAMANHO"),.T.),nil})
				AADD(aDados,{"E1_NATUREZ"	,SZ1->Z1_NATUREZ		,nil})
				AADD(aDados,{"E1_CLIENTE"	,SZ1->Z1_CLIENTE		,nil})
				AADD(aDados,{"E1_LOJA"		,SZ1->Z1_LOJA			,nil})
				AADD(aDados,{"E1_HIST"		,'CONTRATO '+SZ1->Z1_CONTRAT,nil})
				AADD(aDados,{"E1_CCC"		,GetMv("SA_CCC")		,nil})
				AADD(aDados,{"E1_VENCTO"		,aCondicao[nCont][1]	,nil})
				AADD(aDados,{"E1_NATUREZ"	,SZ1->Z1_NATUREZ		,nil})
				AADD(aDados,{"E1_VALOR"		,aCondicao[nCont][2]	,nil})
				//AADD(aDados,{"E1_DECRESC"	,nValDsc				,nil})
				lMsErroAuto	:= .F.
				MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
				If lMsErroAuto
					DisarmTransaction()
					AutoGrLog("Erro ao incluir o titulo!")
					AutoGrLog("Opera��o cancelada!")
					MostraErro()
					Return .F.
				Endif
				//cParc:= soma1(cParc)
			Next
	End Transaction

	EndIf

	RecLock("SZ1",.F.)
		SZ1->Z1_USERAPR	:= cNomeUsr
		SZ1->Z1_HORAAPR	:= cTime
		SZ1->Z1_VLADD	:= 0
	SZ1->(MsUnLock())
	If lMsg
		MsgInfo("Aprovado com sucesso!")
	EndIf
Return .T.

User Function YSZ1002()
	Local cContrato	:= SZ1->Z1_CONTRAT
	Local aDados		:= {}
	Local aParam	:= {}
	Local aRet		:= {}
	Local cExclui  

	
	// exclusao pedido

/*	aAdd( aParam ,{1,'Motivo de Exclus�o',cTpPdd,'@!',"","X1",'.T.',60,.T.})
	
	If !ParamBox(aParam,"Motivo de Exclus�o",@aRet,bOk,,,,,,,.T.,.T.)
		cExclui := aRet[1] 
		Return
	EndIf*/
	
	
	// fim exclusao pedido
	
	
	FWExecView ("Medi��o", "CADSZ3", MODEL_OPERATION_UPDATE ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
Return

User Function YSZ1003()
	Local cContrato	:= SZ1->Z1_CONTRAT
	Local aDados		:= {}
	FWExecView ("Aditivo", "CADSZ4", MODEL_OPERATION_UPDATE ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
Return


User Function YSZ1007()
	Local cContrato	:= SZ1->Z1_CONTRAT
	Local aDados		:= {}
	FWExecView ("Adicionar", "CADSZ7", MODEL_OPERATION_UPDATE ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
Return

User Function YSZ1004()
	Local aArea	:= GetArea()
	Local cCliIni	:= cCliFim	:= Space(GetSx3Cache("A1_COD","X3_TAMANHO"))
	Local cLjIni	:= cLjFim	:= Space(GetSx3Cache("A1_LOJA","X3_TAMANHO"))
	Local cProdIni	:= cProdFim	:= Space(GetSx3Cache("B1_COD","X3_TAMANHO"))
	Local aParam	:= {}
	Local aRet		:= {}
	Local cEmpr		:= space(2)
	Local bOk		:= {|| MsgYesNo("Confirmar a medi��o total dos contratos com parametros informados?") }
	Local nQuant	:= 0
	aAdd(aParam,{1,"Empresa"		,cEmpr		,"@!",".T.","SM0",".T.",80,.F.})
	aAdd(aParam,{1,"Cliente De"		,cCliIni	,GetSx3Cache("A1_COD","X3_PICTURE"),".T.","SA1",".T.",80,.F.})
	aAdd(aParam,{1,"Loja De"			,cLjIni	,GetSx3Cache("A1_LOJA","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{1,"Cliente Ate"	,cCliFim	,GetSx3Cache("A1_COD","X3_PICTURE"),".T.","SA1",".T.",80,.F.})
	aAdd(aParam,{1,"Loja Ate"		,cLjFim	,GetSx3Cache("A1_LOJA","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{1,"Produto De"		,cProdIni	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","SB1",".T.",80,.F.})
	aAdd(aParam,{1,"Produto Ate"		,cProdFim	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","SB1",".T.",80,.F.})
	If !ParamBox(aParam,"Medi��o Automatica",@aRet,bOk,,,,,,"U_YSZ1004",.T.,.T.)
		Return .F.
	Else
		cEmpCon	:= aRet[1]
		cCliIni	:= aRet[2]
		cLjIni	:= aRet[3]
		cCliFim	:= aRet[4]
		cLjFim	:= aRet[5]
		cProdIni:= aRet[6]
		cProdFim:= aRet[7]
		Processa({|| MedicaoAut(cEmpCon,cCliIni,cLjIni,cCliFim,cLjFim,cProdIni,cProdFim,@nQuant) },"Medi��o Automatica","Aguarde inicio...",.F.)
		MsgInfo("Quantidade de contratos processados: "+cValToChar(nQuant))
	EndIf
	SZ1->(DBClearFilter())
	RestArea(aArea)
Return

Static Function MedicaoAut(cEmpCon,cCliIni,cLjIni,cCliFim,cLjFim,cProdIni,cProdFim,nQuant)
	Local oModel,nCont
	Local cTmp	:= GetNextAlias()
	Local cQuery
	Default nQuant	:= 0
	ProcRegua(10)
	ProcessMessage() //ATUALIZA CLIENTE
	cQuery := ""
	cQuery += "SELECT R_E_C_N_O_ AS REGISTRO FROM "+RetSqlName( 'SZ1' ) + " SZ1"
	cQuery += " WHERE Z1_FILIAL='"+ cEmpCon + "'"
	cQuery += " AND Z1_USERAPR<>'"+SPACE(GetSx3Cache("Z1_USERAPR","X3_TAMANHO"))+"' "
	cQuery += " AND Z1_SALDO>0 "
	cQuery += " AND Z1_CLIENTE>='"+cCliIni+"' AND Z1_CLIENTE<='"+cCliFim+"'
	cQuery += " AND Z1_LOJA>='"+cLjIni+"' AND Z1_LOJA<='"+cLjFim+"'"
	cQuery += " AND SZ1.D_E_L_E_T_=' '"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.F.,.T.)
	SZ1->(DbSetOrder(1))	//Z1_FILIAL+Z1_CONTRAT
	While (cTmp)->(!EOF())
		SZ1->(DBGOTO((cTmp)->REGISTRO))
		IncProc("Contrato: "+SZ1->Z1_CONTRAT)
		ProcessMessage() //ATUALIZA CLIENTE
		lEdit			:= .F.
		oModel 		:= FWLoadModel( 'CADSZ3' )
		oModel:SetOperation( MODEL_OPERATION_UPDATE )
		//oModel:GetModel('SZ3DETAIL'):Activate()
		lRet := oModel:Activate()
		oModelSZ3	:= oModel:GetModel('SZ3DETAIL')
		If lRet
			For nCont := 1 To oModelSZ3:Length()
				oModelSZ3:GoLine(nCont)
				If oModelSZ3:IsDeleted()
					Loop
				EndIf
				cCodPro	:= Posicione("SB1",1,xFilial("SB1")+oModelSZ3:GetValue("Z3_PRODUTO"),"B1_COD")
				If !(cCodPro>=cProdIni .AND. cCodPro<=cProdFim)	//Filtro de Grupo de,ate
					Loop
				EndIf
				If !oModelSZ3:SetValue("Z3_QTDMED", oModelSZ3:GetValue("Z3_SALDO") )
					Exit
				Else
					lEdit	:= .T.
				EndIf
			Next
			If !lEdit
				oModel:DeActivate()
				(cTmp)->(DbSkip())
				Loop
			EndIf
		EndIf
		If lRet .and. ( lRet := oModel:VldData() )
			oModel:CommitData()
		EndIf
		If !lRet
			aErro   := oModel:GetErrorMessage()
			AutoGrLog( "Id do formul�rio de origem:" + ' [' + AllToChar( aErro[1]  ) + ']' )
			AutoGrLog( "Id do campo de origem:     " + ' [' + AllToChar( aErro[2]  ) + ']' )
			AutoGrLog( "Id do formul�rio de erro:  " + ' [' + AllToChar( aErro[3]  ) + ']' )
			AutoGrLog( "Id do campo de erro:       " + ' [' + AllToChar( aErro[4]  ) + ']' )
			AutoGrLog( "Id do erro:                " + ' [' + AllToChar( aErro[5]  ) + ']' )
			AutoGrLog( "Mensagem do erro:          " + ' [' + AllToChar( aErro[6]  ) + ']' )
			AutoGrLog( "Mensagem da solu��o:       " + ' [' + AllToChar( aErro[7]  ) + ']' )
			AutoGrLog( "Valor atribu�do:           " + ' [' + AllToChar( aErro[8]  ) + ']' )
			AutoGrLog( "Valor anterior:            " + ' [' + AllToChar( aErro[9]  ) + ']' )
		Else
			nQuant++
		EndIf
		oModel:DeActivate()
		If !lRet
			MostraErro()
			Return .F.
		EndIf
		(cTmp)->(DbSkip())
	EndDo
	(cTmp)->(DbCloseArea())
Return .T.

Static Function PrecoTab(cProduto)
	Local cTabPad		:= SuperGetMV("MV_TABPAD")
	Local oModel		:= FWModelActive()
	Local lKit			:= ExistCpo("SZ6",cProduto,1,,.F.)
	Local nValorTab	:= 0
	SA1->(dbSetOrder(1))	//A1_FILIAL+A1_COD
	SA1->(dbSeek(xFilial("SA1")+oModel:GetModel('SZ1MASTER'):GetValue('Z1_CLIENTE')+oModel:GetModel('SZ1MASTER'):GetValue('Z1_LOJA')))

	If lKit

		If SA1->(dbSeek(xFilial("SA1")+oModel:GetModel('SZ1MASTER'):GetValue('Z1_CLIENTE')+oModel:GetModel('SZ1MASTER'):GetValue('Z1_LOJA'))) .AND. !Empty(SA1->A1_Tabela)
			cTabPad	:= SA1->A1_TABELA
		EndIf

		SZ7->(DbSetOrder(1))
		SZ7->(DbSeek(xFilial("SZ7")+cProduto))
		While SZ7->(!EOF()) .AND. xFilial("SZ7")+cProduto==SZ7->(Z7_FILIAL+Z7_COD)
			nValorTab += SZ7->Z7_QUANT*MaTabPrVen(cTabPad;
				,SZ7->Z7_PRODUTO;
				,1;
				,oModel:GetModel('SZ1MASTER'):GetValue('Z1_CLIENTE');
				,oModel:GetModel('SZ1MASTER'):GetValue('Z1_LOJA'))
			SZ7->(DbSkip())
		EndDo
	Else
		nValorTab	:= MaTabPrVen(cTabPad;
			,cProduto;
			,1;
			,oModel:GetModel('SZ1MASTER'):GetValue('Z1_CLIENTE');
			,oModel:GetModel('SZ1MASTER'):GetValue('Z1_LOJA'))
	EndIf
	/*If nValorTab==0
		oModel:SetErrorMessage('SZ2DETAIL',"Z2_VLUNIT",,,"ATEN��O", 'Sem pre�o cadastrado', 'Atualize a tabela de pre�o!')
		Return .F.
	ElseIf nValorTab<>oModel:GetModel('SZ2DETAIL'):GetValue('Z2_VLUNIT')
		oModel:SetErrorMessage('SZ2DETAIL',"Z2_VLUNIT",,,"ATEN��O", 'Valor diferente da tabela de pre�o (R$ '+Transform(nValorTab,"@E 999,999,999,999.99")+')', 'Atualize a tabela de pre�o com o valor correto!')
		Return .F.
	EndIf*/
Return nValorTab

Static Function SZ1ValProd(oModelSZ2)
	Local lRet := ExistCpo("SZ6",oModelSZ2:GetValue('Z2_PRODUTO'),1,,.F.).OR.ExistCpo("SB1",oModelSZ2:GetValue('Z2_PRODUTO'),1,,.F.)
	Local oModel := FWModelActive()
	If !lRet
		oModel:SetErrorMessage('SZ2DETAIL',"Z2_PRODUTO",,,"ATEN��O", 'Produto/Kit n�o encontrado', 'Verifique se o Produto ou Kit esta cadastrado!')
	Else
		oModel:GetModel('SZ2DETAIL'):LoadValue('Z2_QUANT',0)
		oModel:GetModel('SZ2DETAIL'):SetValue('Z2_VLUNIT',PrecoTab(oModelSZ2:GetValue('Z2_PRODUTO')))
		oModel:GetModel('SZ2DETAIL'):LoadValue('Z2_TOTAL',0)
		oModel:GetModel('SZ2DETAIL'):LoadValue('Z2_TES',Space(GetSx3Cache("Z2_TES","X3_TAMANHO")))
	EndIf
Return lRet

Static Function ProcKit(oView)
	Local aCoors  := FWGetDialogSize( oMainWnd )
	Local oPanelUp, oPanelDown, oFWLayer, oBrowseDown, oBrowseUp,oRelacSZ7

	Private oDlgPrinc

	Define MsDialog oDlgPrinc Title 'Kit' From aCoors[1], aCoors[2] To aCoors[3], aCoors[4] Pixel
	oFWLayer := FWLayer():New()
	oFWLayer:Init( oDlgPrinc, .F., .T. )
	oFWLayer:AddLine( 'ALL', 100, .F. )
	oFWLayer:AddCollumn( 'L', 35, .T., 'ALL' )
	oFWLayer:AddCollumn( 'R', 65, .T., 'ALL' )
	oPanelUp	:= oFWLayer:GetColPanel( 'L', 'ALL' )
	oPanelDown	:= oFWLayer:GetColPanel( 'R', 'ALL' )

	oBrowseUp:= FWmBrowse():New()
	oBrowseUp:SetOwner( oPanelUp )                          // Aqui se associa o browse ao componente de tela
	oBrowseUp:SetDescription( "Codigo Kit" )
	oBrowseUp:SetAlias( 'SZ6' )
	oBrowseUp:SetMenuDef( 'CADSZ6' )                   // Define de onde virao os botoes deste browse
	oBrowseUp:SetProfileID( '1' )
	oBrowseUp:DisableDetails()
	oBrowseUp:SetWalkThru(.F.)
	oBrowseUp:SetAmbiente(.F.)
	oBrowseUp:ForceQuitButton()
	oBrowseUp:SetExecuteDef(1)
	oBrowseUp:Activate()

	oBrowseDown:= FWMBrowse():New()
	oBrowseDown:SetOwner( oPanelDown )
	oBrowseDown:SetDescription( 'Produtos do Kit' )
	oBrowseDown:SetMenuDef( '' )                       // Referencia uma funcao que nao tem menu para que nao exiba nenhum botao
	oBrowseDown:DisableDetails()
	oBrowseDown:SetAlias( 'SZ7' )
	oBrowseDown:SetProfileID( '2' )
	oBrowseDown:SetSeek(.F.)
	oRelacSZ7:= FWBrwRelation():New()
	oRelacSZ7:AddRelation( oBrowseUp  , oBrowseDown , { { 'Z7_FILIAL', 'xFilial( "SZ7" )' }, { 'Z7_COD' , 'Z6_COD'  } } )
	oRelacSZ7:Activate()
	oBrowseDown:Activate()
	oBrowseUp:SetFocus()

	Activate MsDialog oDlgPrinc Center

Return

User function YSZ1005()
	Local oModel := FWModelActive()
	Local cCodKit	:= SZ6->Z6_COD
	oDlgPrinc:End()
	oModel:GetModel('SZ2DETAIL'):SetValue('Z2_PRODUTO',cCodKit)
Return

Static Function SZ1ValTES(oModelSZ2)
	Local lRet := .T.
	Local oModel := FWModelActive()
	If !ExistCpo("SF4",oModelSZ2:GetValue('Z2_TES'),1,,.F.)	//N�o existi TES
		lRet	:= .F.
		oModel:SetErrorMessage('SZ2DETAIL',"Z2_TES",,,"ATEN��O", 'Tes n�o encontrado', 'Verifique se a TES est� cadastrada!')
	ElseIf oModelSZ2:GetValue('Z2_TES') <= "500"
		lRet	:= .F.
		oModel:SetErrorMessage('SZ2DETAIL',"Z2_TES",,,"ATEN��O", 'A Tes informada n�o � uma valida para saida', 'Verifique se a TES � de saida!')
	EndIf
Return lRet


Static Function FDescZ1(cContr,cCli,cLoj)
	Local nRet		:= 0
	Local cTabPad	:= SuperGetMV("MV_TABPAD")

	If SA1->(dbSeek(xFilial("SA1")+cCli+cLoj)) .AND. !Empty(SA1->A1_Tabela)
		cTabPad	:= SA1->A1_TABELA
	EndIf


	dbSelectArea("SZ2")
	SZ2->(dbSetOrder(1))
	SZ2->(dbSeek(xFilial("SZ2")+cContr))

	while SZ2->(!eof()) .and. Alltrim(xFilial("SZ2")+cContr) == Alltrim(SZ2->Z2_FILIAL+SZ2->Z2_CONTRAT)

	 	lKit:= ExistCpo("SZ6",SZ2->Z2_PRODUTO,1,,.F.)

			If lKit
				SZ7->(DbSetOrder(1))
				SZ7->(DbSeek(xFilial("SZ7")+Alltrim(SZ2->Z2_PRODUTO)))
				While SZ7->(!EOF()) .AND. Alltrim(xFilial("SZ7")+SZ2->Z2_PRODUTO)==Alltrim(SZ7->(Z7_FILIAL+Z7_COD))
					nVlrItem	:= MaTabPrVen(cTabPad;
												,SZ7->Z7_PRODUTO;
												,1;
												,cCli;
												,cLoj)

					//Verifica se tem desconto
					If SZ7->Z7_PERC > 0
						nRet+= (( nVlrItem*SZ7->Z7_QUANT*SZ2->Z2_QUANT)*(SZ7->Z7_PERC/100))
					Endif
					SZ7->(dbSkip())
				Enddo
			Endif
		SZ2->(dbSkip())
	Enddo

Return nRet

/*/{Protheus.doc} REtDscSZ1
Retorna Desconto conforme Kit de Produtos
@author Diogo
@since 13/10/2014
@version 1.0
/*/
User Function RetDscSZ1(oModelSZ2)
	Local nRet:= 0
	Local cTabPad	:= SuperGetMV("MV_TABPAD")
	Local aAreaZ1	:= GetArea()
	Local oModel	:= FWModelActive()

	if Empty(oModel:GetValue("SZ1MASTER","Z1_CLIENTE"))
		Return nRet
	endif

	lKit:= ExistCpo("SZ6",oModelSZ2:GetValue("Z2_PRODUTO"),1,,.F.)
	If SA1->(dbSeek(xFilial("SA1")+oModel:GetValue("SZ1MASTER","Z1_CLIENTE")+oModel:GetValue("SZ1MASTER","Z1_LOJA"))) .AND. !Empty(SA1->A1_Tabela)
		cTabPad	:= SA1->A1_TABELA
	EndIf

	If lKit
		SZ7->(DbSetOrder(1))
		SZ7->(DbSeek(xFilial("SZ7")+Alltrim(oModelSZ2:GetValue("Z2_PRODUTO"))))
		While SZ7->(!EOF()) .AND. Alltrim(xFilial("SZ7")+oModelSZ2:GetValue("Z2_PRODUTO"))==Alltrim(SZ7->(Z7_FILIAL+Z7_COD))
			nVlrItem	:= MaTabPrVen(cTabPad;
										,SZ7->Z7_PRODUTO;
										,1;
										,oModel:GetValue("SZ1MASTER","Z1_CLIENTE");
										,oModel:GetValue("SZ1MASTER","Z1_LOJA"))

				If SZ7->Z7_PERC > 0
					nRet+= (( nVlrItem*SZ7->Z7_QUANT*oModelSZ2:GetValue("Z2_QUANT"))*(SZ7->Z7_PERC/100))
				Endif
			SZ7->(dbSkip())
		Enddo
	Endif

	RestArea(aAreaZ1)

Return nRet

Static Function GetCacheSF4(cCodTES,cCampo,xRet)
	Local lRet	:= .F.
	Local nPos	:= aScan(aCacheTES,{|x| x[1]==cCodTES .and. x[2]==cCampo})
	If nPos>0
		xRet	:=	aCacheTES[nPos][3]
		Return .T.
	EndIf
	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
	If SF4->(DbSeek(xFilial("SF4")+cCodTES))
		AADD(aCacheTES,{cCodTES,cCampo,SF4->(&cCampo)})
		lRet	:= .T.
		xRet	:= SF4->(&cCampo)
	EndIf
Return lRet

User Function YSZ1008()
	Local oBrowse
	Local aLegenda	:= {}
	Local nCont
	Private cCadastro	:= "Adiantamentos"
	Private aRotina := {}
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('SE1')
	oBrowse:SetDescription('Adiantamentos')
	oBrowse:DisableDetails()
	oBrowse:SetMenuDef( '' )
	oBrowse:SetFilterDefault("E1_FILIAL=='"+xFilial("SE1")+"' .AND. E1_PREFIXO='"+"A"+SUBSTR(CFILANT,5,2)+"' .AND. E1_NUM='"+PADR(SZ1->Z1_CONTRAT,GetSx3Cache("E1_NUM","X3_TAMANHO"))+"' ")
	//oBrowse:AddButton("Visualizar","AXVISUAL",0,2,0)
	oBrowse:AddButton("Legenda","Fa040Legenda",0,2,0)
	aLegenda	:= Fa040Legenda("SE1")
	For nCont:=1 to Len(aLegenda)
		oBrowse:AddLegend( aLegenda[nCont][1], aLegenda[nCont][2], "")
	Next
	oBrowse:Activate()
Return