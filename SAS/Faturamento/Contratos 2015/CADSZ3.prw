#Include 'Protheus.ch'
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} CADSZ3
Medi��o
@author Saulo Gomes Martins
@since 12/09/2014
@version 1.0
/*/
User Function CADSZ3()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	Local oStructSZ1	:= Nil
	Local oStructSZ2	:= Nil
	Local oStructSZ3	:= FWFormStruct(1,"SZ3")
	Local oModel		:= FWLoadModel( 'CADSZ1' )
	Local nI
	oModel:aModelStruct[1][4]	:= {}
	//-----------------------------------------
	//Monta a estrutura do formul�rio com base no dicion�rio de dados
	//-----------------------------------------
	oStructSZ3:AddTrigger( ;
		"Z3_QTDMED"		,;										//[01] Id do campo de origem
	"Z3_TOTAL"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue('Z3_QTDMED')*oModel:GetValue('Z3_VLUNIT') }  )	// [04] Bloco de codigo de execu��o do gatilho

	oModel:AddGrid("SZ3DETAIL", "SZ1MASTER"/*cOwner*/,oStructSZ3, ,/*bLinePost*/,/*bPre*/,/*bPost*/,{ |oModel, lCopia| LoadSZ3( oModel, lCopia ) }/*Carga*/)
	oModel:SetRelation("SZ3DETAIL",{{"Z3_FILIAL",'xFilial("SZ3")'},{"Z3_CONTRAT","Z1_CONTRAT"}},SZ3->(IndexKey(1)))


	oModel:GetModel("SZ3DETAIL" ):GetStruct():SetProperty("Z3_QTDMED",MODEL_FIELD_VALID,{||SZ3ValCampo()})

	oModel:AddCalc( 'YCADSZ3CALC1', 'SZ1MASTER', 'SZ3DETAIL', 'Z3_QTDMED', 'Z3__TOT01', 'SUM',,,'Total Medida',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"QTDMED") }*/ )
	oModel:AddCalc( 'YCADSZ3CALC1', 'SZ1MASTER', 'SZ3DETAIL', 'Z3_QTDMED', 'Z3__TOT02', 'SUM',,,'Valor Total',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)

	oModel:GetModel("SZ1MASTER" ):GetStruct():SetProperty("*",MODEL_FIELD_WHEN,{||.F.})
	oModel:SetOnlyQuery('SZ1MASTER',.T.)
	//oModel:SetOnlyQuery('SZ2DETAIL',.T.)
	//oModel:GetModel("SZ3DETAIL" ):GetStruct():SetProperty("Z2_QTDADT",MODEL_FIELD_VALID,{||.T.})
	//oModel:GetModel( 'SZ2DETAIL' ):SetNoUpdateLine()
	oModel:GetModel( 'SZ3DETAIL' ):SetNoInsertLine()
	//oModel:GetModel( 'SZ3DETAIL' ):SetNoDeleteLine()
	oModel:SetVldActive( { |oModel| SZ3ValAct( oModel ) } )
	oModel:SetCommit({|oModel| SZ3Commit(oModel) },.F.)
	//oModel:SetCommit({|oModel| FWFormCommit(oModel) },.t.)

Return(oModel)

Static Function ViewDef()
	Local oStructSZ1	:= FWFormStruct( 2, 'SZ1' )
	Local oModel		:= FWLoadModel( 'CADSZ3' )
	Local oView		:= FWFormView():New()//FWLoadView('CADSZ1')
	Local oStructSZ3	:= FWFormStruct( 2, 'SZ3', {|cCampo| !(Alltrim(cCampo) $ "Z3_MEDICAO|Z3_CONTRAT|Z3_EMISSAO|Z3_RESPMED|Z3_HORA|Z3_CLIENTE|Z3_LOJA|Z3_NOME|Z3_MESREF|Z3_ANOREF|Z3_OK|Z3_PEDIDO")} )
	Local oCalc1		:= FWCalcStruct( oModel:GetModel( 'YCADSZ3CALC1') )

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField( "SZ1MASTER",oStructSZ1)
	oView:AddField( 'VIEW_CALC3', oCalc1, 'YCADSZ3CALC1' )
	oView:AddGrid("SZ3DETAIL",oStructSZ3)

	oView:CreateHorizontalBox("CABEC",30)
	oView:CreateHorizontalBox("GRID",60)
	oView:CreateHorizontalBox( 'RODAPE',10)

	oView:SetOwnerView( "SZ1MASTER","CABEC")
	oView:SetOwnerView( "SZ3DETAIL","GRID")
	oView:SetOwnerView( 'VIEW_CALC3', 'RODAPE' )

	oView:EnableTitleView('SZ3DETAIL','Itens da Medi��o')
	oView:SetProgressBar(.T.)
	//oView:SetViewProperty( '*', "GRIDSEEK"      )
	oView:AddUserButton( 'Preencher com Qtd Total', 'CLIPS', { |oView| MedirTotal(oView) } )
	oView:AddUserButton( 'Preencher Linha Qtd Total', 'CLIPS', { |oView| MedirLinhaT(oView) }, , 20,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} )
Return oView

Static Function SZ3ValAct( oModel )
	Local lRet			:= .T.
	Local cContrato
	Local cCNPJ		:= FWArrFilAtu()[18]					//Coligada
	Local cCNPJMatriz	:= FWArrFilAtu("01","010105")[18]	//Matriz
	SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
	SA2->(DbSetOrder(3))	//A2_FILIAL+A2_CGC
	If !SA1->(DbSeek(xFilial("SA1")+cCNPJ))
		oModel:SetErrorMessage('SZ1MASTER',,,,"ATEN��O", 'N�o � possivel realizar medi��o, n�o foi encontrado o cadastro de cliente dessa empresa.', 'Realize o cadastro de cliente dessa empresa!')
		Return .F.
	ElseIf !SA2->(DbSeek(xFilial("SA2")+cCNPJMatriz))
		oModel:SetErrorMessage('SZ1MASTER',,,,"ATEN��O", 'N�o � possivel realizar medi��o, n�o foi encontrado o cadastro de fornecedor da empresa Matriz.', 'Realize o cadastro de fornecedor da empresa!')
		Return .F.
	EndIf
	If oModel:getOperation() == MODEL_OPERATION_UPDATE
		If	Empty(SZ1->Z1_USERAPR)
			oModel:SetErrorMessage('SZ1MASTER',,,,"ATEN��O", 'Contrato n�o liberado, n�o pode ser feito a medi��o!', 'Liberar o contrato primeiro!')
			Return .F.
		ElseIf	SZ1->Z1_SALDO<=0
			oModel:SetErrorMessage('SZ1MASTER',,,,"ATEN��O", 'Medi��o do contrato j� realizado por completo!', 'Escolher outro contrato para realizar a medi��o.')
			Return .F.
		EndIf
	EndIf
Return lRet

Static Function Total02(oModel,cTipo)
	Local nCont		:= 0
	Local nTotal		:= 0
	Local oModelSZ3	:= oModel:GetModel( 'SZ3DETAIL' )
	Local aSaveLines := FWSaveRows()
	Default cTipo		:= "TOTAL"
	For nCont := 1 To oModelSZ3:Length()
		oModelSZ3:GoLine( nCont )
		If oModelSZ3:IsDeleted()
			Loop
		EndIf
		If cTipo=="TOTAL"
			nTotal	+= oModelSZ3:GetValue('Z3_TOTAL')
		ElseIf cTipo=="QTDMED"
			nTotal	+= oModelSZ3:GetValue('Z3_QTDMED')
		EndIf
	Next
	FWRestRows( aSaveLines )
Return nTotal

Static Function SZ3ValCampo()
	Local lRet			:= .T.  //Flag de retorno
	Local oModel		:= FWModelActive()
	Local nOperacao	:= oModel:GetOperation() //Controle de operacao
	Local cContrato	:= oModel:GetModel( 'SZ1MASTER' ):GetValue('Z1_CONTRAT')
	//SZ2->(DbSetOrder(1))	//Z2_FILIAL+Z2_CONTRAT+Z2_ITEM
	//If SZ2->(DbSeek(xFilial("SZ2")+cContrato+oModel:GetModel('SZ3DETAIL'):GetValue('Z3_ITEM')))
	If oModel:GetModel('SZ3DETAIL'):GetValue('Z3_QTDMED')<0
		oModel:SetErrorMessage('SZ3DETAIL',"Z3_QTDMED",,,"ATEN��O", 'Quantidade medida n�o pode ser menor zero', 'Verifique o valor informado!')
		Return .F.
	ElseIf oModel:GetModel('SZ3DETAIL'):GetValue('Z3_QTDMED')>oModel:GetModel('SZ3DETAIL'):GetValue('Z3_SALDO')
		oModel:SetErrorMessage('SZ3DETAIL',"Z3_QTDMED",,,"ATEN��O", 'Quantidade medida n�o pode ser maior que o saldo('+cValToChar(oModel:GetModel('SZ3DETAIL'):GetValue('Z3_SALDO'))+').', 'Verifique o valor informado!')
		Return .F.
	EndIf
	//EndIf
	//oModel:LoadValue("YCADSZ3CALC1", "Z3__TOT02", Total02(oModel,"TOTAL"))
Return lRet

Static Function SZ3Commit(oModel)
	Local cContrato	:= oModel:GetModel( 'SZ1MASTER' ):GetValue('Z1_CONTRAT')
	Local cCNPJ		:= FWArrFilAtu()[18] //Coligada
	Local nSaldo		:= 0
	Local nQtd			:= 0
	Local lRet			:= .T.
	Local nCont,nCont2
	Local cMedicao
	Local aCab			:= {}
	Local aItens		:= {}
	Local aItens2		:= {}
	Local nQtdMed		:= 0
	Local nTam
	Local aStruct	:= oModel:GetModel( 'SZ3DETAIL' ):oFormModelStruct:GetFields()
	Local oModelSZ1	:= oModel:GetModel('SZ1MASTER')
	Local oModelSZ3	:= oModel:GetModel('SZ3DETAIL')
	Local aCliente	:= {}
	Local nContIT		:= 0
	Local nVlrItem	:= 0
	Local nVlrTotal	:= 0
	Local nVlrAdiant	:= 0
	Local lKit
	Local cTabPad		:= SuperGetMV("MV_TABPAD")
	Local nj
	Local aLocks		:= {}
	Local oView		:= FWViewActive()
	Private nFret		:= 0
	Private nDesc		:= 0
	Private lMsErroAuto	:= .F.
	SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
	If SA1->(DbSeek(xFilial("SA1")+cCNPJ))
		cCliCol	:= SA1->A1_COD
		cLjCol	:= SA1->A1_LOJA
		cTabPad2:= iif(empty(SA1->A1_TABELA),cTabPad,SA1->A1_TABELA)
	EndIf

	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
	SZ1->(DbSetOrder(1))	//Z1_FILIAL+Z1_CONTRAT
	SZ2->(DbSetOrder(1))	//Z2_FILIAL+Z2_CONTRAT+Z2_ITEM
	SA1->(dbSetOrder(1))	//A1_FILIAL+A1_COD
	SB1->(DbSetOrder(1))	//B1_FILIAL+B1_COD
	SB2->(DbSetOrder(1))	//B2_FILIAL+B2_COD+B2_LOCAL
	If SA1->(dbSeek(xFilial("SA1")+oModelSZ1:GetValue('Z1_CLIENTE')+oModelSZ1:GetValue('Z1_LOJA'))) .AND. !Empty(SA1->A1_Tabela)
		cTabPad	:= SA1->A1_TABELA
	EndIf
	If	oModel:getOperation() == MODEL_OPERATION_UPDATE .OR.;
			oModel:getOperation() == MODEL_OPERATION_INSERT
		cMedicao	:= GETSXENUM("SZ3","Z3_MEDICAO")
		cNaturez:= space(GetSx3Cache("ED_CODIGO","X3_TAMANHO"))
		cLocExp := space(GetSx3Cache("C5_YLCEXPD","X3_TAMANHO"))
		cTpPdd  := space(6)
		cObsPd  := space(200)
		dDTSol  := dDatabase
		
		aPergs	:= {}
		aRet	:= {}
		aAdd( aPergs ,{1,'Natureza',cNaturez,'@!',"Vazio() .OR. ExistCpo('SED')","SED",'.T.',60,.F.})
		aAdd( aPergs ,{1,'Tipo Pedido',cTpPdd,'@!',"","X1",'.T.',60,.T.})
		aAdd( aPergs ,{1,'Observacao',cObsPd,'@!',"","",'.T.',60,.F.})
		aAdd( aPergs ,{1,'Data Solicitacao',dDTSol,'',"","",'.T.',60,.T.})
		aAdd( aPergs ,{1,'Local Exp.',cLocExp,'@!',"","X2",'.T.',60,.T.})
		
		If ParamBox(aPergs,"Natureza",aRet,,,,,,,"_CDSZ3",.F.,.F.)
			cNaturez := aRet[1]
			cTpPdd   := aRet[2]
			cObsPd   := aRet[3]
			dDTSol	 := aRet[4]
			cLocExp	 := aRet[5]	 
		Else
			oModel:SetErrorMessage('SZ3DETAIL',,,,"ERRO", 'Opera��o cancelada pelo usuario!', '')
			oModel:lModify	:= .T.
			oView:oModel:lModify	:= .T.
			Return .F.
		Endif
		If Empty(cNaturez)
			oModel:SetErrorMessage('SZ3DETAIL',,,,"ERRO", 'Necessario informar a natureza!', 'Informe a natureza!')
			oModel:lModify	:= .T.
			oView:oModel:lModify	:= .T.
			Return .F.
		EndIf
		If !ExistCpo('SX5','X1'+cTpPdd)
			oModel:SetErrorMessage('SZ3DETAIL',,,,"ERRO", 'Tipo de Pedido Inv�lido.!', 'Informe o Tipo de Pedido!')
			oModel:lModify	:= .T.
			oView:oModel:lModify	:= .T.
			Return .F.
		EndIf		
		
		
		
		//Begin transaction
			SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
			If SA1->(DbSeek(xFilial("SA1")+cCNPJ))
				AADD(aLocks,{"SA1",SA1->(Recno())})
			EndIf
			For nCont := 1 To oModelSZ3:Length()
				oModelSZ3:GoLine(nCont)
				If oModelSZ3:IsDeleted()
					Loop
				EndIf
				If oModelSZ3:GetValue("Z3_QTDMED")==0
					Loop
				EndIf
				RecLock("SZ3",.T.)
				For nCont2:=1 To Len(aStruct)
					SZ3->(&(aStruct[nCont2][MODEL_FIELD_IDFIELD]))	:= oModelSZ3:GetValue(aStruct[nCont2][MODEL_FIELD_IDFIELD])
				Next
				SZ3->Z3_FILIAL	:= xFilial("SZ3")
				SZ3->Z3_MEDICAO	:= cMedicao
				SZ3->(MsUnLock())
				SZ3->(ConfirmSX8())

				If SZ2->(DbSeek(xFilial("SZ2")+cContrato+oModelSZ3:GetValue('Z3_ITEM')))
					lKit			:= ExistCpo("SZ6",oModelSZ3:GetValue('Z3_PRODUTO'),1,,.F.)
					If lKit
						SZ7->(DbSetOrder(1))
						SZ7->(DbSeek(xFilial("SZ7")+oModelSZ3:GetValue('Z3_PRODUTO')))
						While SZ7->(!EOF()) .AND. xFilial("SZ7")+oModelSZ3:GetValue('Z3_PRODUTO')==SZ7->(Z7_FILIAL+Z7_COD)
							nContIT++
							nVlrItem	:= MaTabPrVen(cTabPad;
															,SZ7->Z7_PRODUTO;
															,1;
															,oModel:GetModel('SZ1MASTER'):GetValue('Z1_CLIENTE');
															,oModel:GetModel('SZ1MASTER'):GetValue('Z1_LOJA'))
							nVlrTotal	+= nVlrItem*SZ7->Z7_QUANT*oModelSZ3:GetValue('Z3_QTDMED')
							If SF4->(DbSeek(xFilial("SF4")+SZ2->Z2_TES)) .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
								nVlrAdiant	+= nVlrItem*SZ7->Z7_QUANT*oModelSZ3:GetValue('Z3_QTDMED')
							EndIf
							AADD(aItens,{})
							nTam	:= Len(aItens)
							AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
							AADD(aItens[nTam],{"C6_PRODUTO"	,SZ7->Z7_PRODUTO													,nil})
							AADD(aItens[nTam],{"C6_QTDVEN"	,SZ7->Z7_QUANT*oModelSZ3:GetValue('Z3_QTDMED')				,nil})
							AADD(aItens[nTam],{"C6_QTDLIB"	,SZ7->Z7_QUANT*oModelSZ3:GetValue('Z3_QTDMED')				,nil})
							AADD(aItens[nTam],{"C6_PRCVEN"	,nVlrItem															,nil})
							AADD(aItens[nTam],{"C6_TES"		,SZ2->Z2_TES														,nil})
							AADD(aItens[nTam],{"C6_YCONTRA"	,cContrato															,nil})
							AADD(aItens[nTam],{"C6_YMEDICA"	,cMedicao															,nil})
							AADD(aItens[nTam],{"C6_YITCONT"	,oModelSZ3:GetValue('Z3_ITEM')									,nil})
							AADD(aItens[nTam],{"C6_YCODKIT"	,oModelSZ3:GetValue('Z3_PRODUTO')								,nil})
							If SB1->(DbSeek(xFilial("SB1")+SZ7->Z7_PRODUTO)) .AND. SB2->(DbSeek(xFilial("SB2")+SZ7->Z7_PRODUTO+SB1->B1_LOCPAD))
								AADD(aLocks,{"SB2",SB2->(Recno())})
							EndIf

							nVlrItem2	:= MaTabPrVen(cTabPad2;
													,SZ7->Z7_PRODUTO;
													,1;
													,cCliCol;
													,cLjCol)

							AADD(aItens2,{})
							nTam	:= Len(aItens2)
							AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
							AADD(aItens2[nTam],{"C6_PRODUTO"	,SZ7->Z7_PRODUTO													,nil})
							AADD(aItens2[nTam],{"C6_QTDVEN"	,SZ7->Z7_QUANT*oModelSZ3:GetValue('Z3_QTDMED')				,nil})
							AADD(aItens2[nTam],{"C6_QTDLIB"	,SZ7->Z7_QUANT*oModelSZ3:GetValue('Z3_QTDMED')				,nil})
							AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2															,nil})
							AADD(aItens2[nTam],{"C6_TES"		,SZ2->Z2_TES														,nil})
							AADD(aItens2[nTam],{"C6_YCONTRA"	,cContrato															,nil})
							AADD(aItens2[nTam],{"C6_YMEDICA"	,cMedicao															,nil})
							AADD(aItens2[nTam],{"C6_YITCONT"	,oModelSZ3:GetValue('Z3_ITEM')									,nil})
							AADD(aItens2[nTam],{"C6_YCODKIT"	,oModelSZ3:GetValue('Z3_PRODUTO')								,nil})


							//Verifica se tem desconto
							If SZ7->Z7_PERC > 0
								nDesc+= (( nVlrItem*SZ7->Z7_QUANT*oModelSZ3:GetValue('Z3_QTDMED'))*(SZ7->Z7_PERC/100))
							Endif

							SZ7->(DbSkip())
						EndDo
					Else
						nContIT++
						AADD(aItens,{})
						nTam	:= Len(aItens)
						nVlrTotal	+= SZ2->Z2_VLUNIT*oModelSZ3:GetValue('Z3_QTDMED')
						If SF4->(DbSeek(xFilial("SF4")+SZ2->Z2_TES)) .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
							nVlrAdiant	+= SZ2->Z2_VLUNIT*oModelSZ3:GetValue('Z3_QTDMED')
						EndIf
						AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
						AADD(aItens[nTam],{"C6_PRODUTO"	,oModelSZ3:GetValue('Z3_PRODUTO')	,nil})
						AADD(aItens[nTam],{"C6_QTDVEN"	,oModelSZ3:GetValue('Z3_QTDMED')	,nil})
						AADD(aItens[nTam],{"C6_QTDLIB"	,oModelSZ3:GetValue('Z3_QTDMED')	,nil})
						AADD(aItens[nTam],{"C6_PRCVEN"	,SZ2->Z2_VLUNIT						,nil})
						AADD(aItens[nTam],{"C6_TES"		,SZ2->Z2_TES							,nil})
						AADD(aItens[nTam],{"C6_YCONTRA"	,cContrato								,nil})
						AADD(aItens[nTam],{"C6_YMEDICA"	,cMedicao								,nil})
						AADD(aItens[nTam],{"C6_YITCONT"	,oModelSZ3:GetValue('Z3_ITEM')		,nil})

						If SB1->(DbSeek(xFilial("SB1")+oModelSZ3:GetValue('Z3_PRODUTO'))) .AND. SB2->(DbSeek(xFilial("SB2")+oModelSZ3:GetValue('Z3_PRODUTO')+SB1->B1_LOCPAD))
							AADD(aLocks,{"SB2",SB2->(Recno())})
						EndIf

						nVlrItem2	:= MaTabPrVen(cTabPad2;
												,oModelSZ3:GetValue('Z3_PRODUTO');
												,1;
												,cCliCol;
												,cLjCol)

						AADD(aItens2,{})
						nTam	:= Len(aItens2)
						AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
						AADD(aItens2[nTam],{"C6_PRODUTO"	,oModelSZ3:GetValue('Z3_PRODUTO')	,nil})
						AADD(aItens2[nTam],{"C6_QTDVEN"	,oModelSZ3:GetValue('Z3_QTDMED')	,nil})
						AADD(aItens2[nTam],{"C6_QTDLIB"	,oModelSZ3:GetValue('Z3_QTDMED')	,nil})
						AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem2							,nil})
						AADD(aItens2[nTam],{"C6_TES"		,SZ2->Z2_TES							,nil})
						AADD(aItens2[nTam],{"C6_YCONTRA"	,cContrato								,nil})
						AADD(aItens2[nTam],{"C6_YMEDICA"	,cMedicao								,nil})
						AADD(aItens2[nTam],{"C6_YITCONT"	,oModelSZ3:GetValue('Z3_ITEM')		,nil})


					EndIf
					RecLock("SZ2",.F.)
					SZ2->Z2_SAlDO		-= oModelSZ3:GetValue('Z3_QTDMED')
					SZ2->Z2_QTDMED	+= oModelSZ3:GetValue('Z3_QTDMED')
					nQtdMed			+= oModelSZ3:GetValue('Z3_QTDMED')
					SZ2->Z2_DTMED		:= dDataBase
					SZ2->(MsUnLock())
				Else
					//DisarmTransaction()
					oModel:SetErrorMessage('SZ3DETAIL',,,,"ERRO", 'N�o foi encontrado o item do contrato na base de dados!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				EndIf
			Next
			If !u_MultLock(aLocks)
				//DisarmTransaction()
				oModel:SetErrorMessage('SZ3DETAIL',,,,"ERRO", 'Problema ao tentar reservar registros exclusivos!', 'Aguarde alguns minutos e tente de novo!')
				Return .F.
			EndIf
			If nQtdMed>0 .AND. SZ1->(DbSeek(xFilial("SZ1")+cContrato))
				RecLock("SZ1",.F.)
				SZ1->Z1_SALDO		-= nQtdMed
				SZ1->Z1_QTDMED	+= nQtdMed
				SZ1->(MsUnLock())
			EndIf

			//oModel:LoadValue("SZ1MASTER", "Z1_QTDORIG", nQtd)
			oModel:LoadValue("SZ1MASTER"	,"Z1_SALDO"	,nSaldo)
			nFret	:= 0
			AADD(aCab,{"C5_TIPO"		,"N"									,Nil})
			AADD(aCab,{"C5_CLIENTE"		,""										,Nil})
			AADD(aCab,{"C5_LOJACLI"		,""										,Nil})
			AADD(aCab,{"C5_CONDPAG"		,oModelSZ1:GetValue('Z1_CONDPAG')		,Nil})
			AADD(aCab,{"C5_NATUREZ"		,cNaturez								,Nil})
			AADD(aCab,{"C5_TPEDAV"		,cTpPdd									,Nil})
			AADD(aCab,{"C5_YOBS"		,cObsPd									,Nil})
			AADD(aCab,{"C5_YDTSOL"		,dDTSol									,Nil})
			AADD(aCab,{"C5_YLCEXPD"		,cLocExp								,Nil})
			
			
			If oModelSZ1:GetValue('Z1_FRETE')=="1"
				nFret:= nVlrAdiant*oModelSZ1:GetValue('Z1_FRETEC')/100
			ElseIf !(Alltrim(cTpPdd) $ "10|08") 
					nFret:= nVlrAdiant*oModelSZ1:GetValue('Z1_FRETEE')/100
			EndIf
			
			aCliente	:= {oModelSZ1:GetValue('Z1_CLIENTE'),oModelSZ1:GetValue('Z1_LOJA'),oModelSZ1:GetValue('Z1_NOME')}


			Processa({|| lRet := Pedido(aCab,aItens,aCliente,cContrato,cMedicao,nFret,aItens2) },"Criando Pedidos","Aguarde...",.F.)
			If !lRet
				//DisarmTransaction()
				oModel:SetErrorMessage('SZ3DETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
				Return .F.
			EndIf
		//End Transaction
		MsUnLockAll()
	EndIf
Return lRet

//FAZER O PEDIDO PARA VARIAS FILIAIS
Static Function Pedido(aCab,aItens,aCliente,cContrato,cMedicao,nFret,aItens2)
	Local cFilBak			:= cFilAnt
	Local aCabCliente		:= aClone(aCab)
	Local aCabCol			:= aClone(aCab)
	Local cCNPJ			:= FWArrFilAtu()[18]					//Coligada
	Local cCNPJMatriz		:= FWArrFilAtu("01","010105")[18]	//Holding
	Local cCliCol := cLjCol := ""
	Local nPosCliente		:= aScan(aCab,{|x| x[1]=="C5_CLIENTE"})
	Local nPosLoja		:= aScan(aCab,{|x| x[1]=="C5_LOJACLI"})
	Local nPosProd		:= aScan(aItens[1],{|x| x[1]=="C6_PRODUTO"})
	Local nPosQuant		:= aScan(aItens[1],{|x| x[1]=="C6_QTDVEN"})
	Local nPosVlr			:= aScan(aItens[1],{|x| x[1]=="C6_PRCVEN"})
	Local nPosTes			:= aScan(aItens[1],{|x| x[1]=="C6_TES"})
	Local cTesPV2			:= GetTes(SuperGetMv("SA_TESPV2",.F.,"","010105"),aItens[1][nPosTes][2])		//Matriz-Comercial	//502:503;505:505
	Local cTesPV3			:= GetTes(SuperGetMv("SA_TESPV3",.F.,"","010105"),aItens[1][nPosTes][2])		//Matriz-Cliente		//502:504;505:504
	Local cTesPC4			:= GetTes(SuperGetMv("SA_TESPC4",.F.,"",cFilAnt),aItens[1][nPosTes][2])		//Comercial-Matriz	//502:016;505:016
	Local cPedido1,cPedido2,cPedido3
	Local aFornec			:= {,}
	Local aCabPC			:= {}
	Local aItensPC	:= {}
	Local nCont,aLinha
	Local lJaLiberado	:= .F.
	//Local cTesPV1			:= SuperGetMv("SA_TESPV1",.F.,"",cFilAnt)		//Comercial-Cliente	//N�o usado, usa a TES cadastrada no contrato
	Default aItens		:= {}
	Private lMsErroAuto	:= .F.
	Private cMat120Num	:= ""
	ProcRegua(4)
	SB1->(DbSetOrder(1))	//B1_FILIAL+B1_COD
	SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
	If SA1->(DbSeek(xFilial("SA1")+cCNPJ))
		cCliCol	:= SA1->A1_COD
		cLjCol		:= SA1->A1_LOJA
	EndIf
	SA2->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
	If SA2->(DbSeek(xFilial("SA2")+cCNPJMatriz))
		aFornec[1]	:= SA2->A2_COD
		aFornec[2]	:= SA2->A2_LOJA
	EndIf
	aCabCliente[nPosCliente][2]	:= aCliente[1]
	aCabCliente[nPosLoja][2]		:= aCliente[2]
	aCabCol[nPosCliente][2]		:= cCliCol
	aCabCol[nPosLoja][2]			:= cLjCol
	IncProc("Criando pedido para o cliente")
	ProcessMessage() //ATUALIZA CLIENTE
	//Comercial - Cliente

	//AADD(aCabCliente,{"C5_MENPAD","Z01",Nil})	//Mensagem na NF
	AADD(aCabCliente,{"C5_FRETE",nFret,Nil})
	AADD(aCabCliente,{"C5_DESCONT",nDesc,Nil})
	lJaLiberado	:= .F.
	For nCont:=1 to Len(aItens)
		If SB1->(DbSeek(xFilial("SB1")+aItens[nCont][nPosProd][2])) .AND. SB1->B1_GRUPO=="0099" .AND. SB1->B1_YSUBGR=="31"
			lJaLiberado	:= .T.
			Exit
		EndIf
	Next

	MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o

	If lMsErroAuto
		AutoGrLog("Erro ao gerar pedido de venda do Comercial para o cliente!")
		MostraErro()
		Return .F.
	Else
		cPedido1	:= SC5->C5_NUM
		//log de inclusao de pedido solicitado pelo atendimento
		U_SASP021(cFilAnt,cPedido1,dDataBase,"I","TRI","")

	Endif

	aSize(aCabCliente,Len(aCabCliente)-2) //Deleta Frete/Desconto para outras filiais

	IncProc("Criando pedido na Matriz para o Comercial")
	ProcessMessage() //ATUALIZA CLIENTE

	//Matriz - Comercial
	cFilAnt	:= "010105"	//Matriz
	lMsErroAuto	:= .F.
	For nCont:=1 to Len(aItens2)
		aItens2[nCont][nPosTes][2]	:= cTesPV2
	Next

	MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCol,aItens2,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o


	If lMsErroAuto
		cFilAnt	:= cFilBak
		AutoGrLog("Erro ao gerar pedido de venda da Matriz para Comercial!")
		MostraErro()
		Return .F.
	Else
		cPedido2	:= SC5->C5_NUM
	Endif

	//Matriz - Cliente
	IncProc("Criando pedido na Matriz para o Cliente")
	ProcessMessage() //ATUALIZA CLIENTE
	lMsErroAuto	:= .F.
	For nCont:=1 to Len(aItens)
		aItens[nCont][nPosTes][2]			:= cTesPV3
	Next

	AADD(aCabCliente,{"C5_DESCONT",nDesc,Nil})
	//AADD(aCabCliente,{"C5_MENPAD","Z02",Nil})	//Mensagem na NF

	MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o

	aSize(aCabCliente,Len(aCabCliente)-1) //Deleta Desconto para outras filiais

	If lMsErroAuto
		cFilAnt	:= cFilBak
		AutoGrLog("Erro ao gerar pedido de venda da Matriz para Cliente!")
		MostraErro()
		Return .F.
	Else
		cPedido3	:= SC5->C5_NUM
	Endif
	cFilAnt	:= cFilBak

	IncProc("Criando pedido de compra para a Matriz")
	cPedido4	:= CriaVar('C7_NUM', .T.)
	aadd(aCabPC,{"C7_NUM"		,cPedido4})
	aadd(aCabPC,{"C7_EMISSAO"	,dDataBase})
	aadd(aCabPC,{"C7_FORNECE"	,aFornec[1]})
	aadd(aCabPC,{"C7_LOJA"		,aFornec[2]})
	aadd(aCabPC,{"C7_COND"		,"001"})		//TODO VERIFICAR CONDI��O PAG
	aadd(aCabPC,{"C7_CONTATO"	,"AUTO"})
	aadd(aCabPC,{"C7_FILENT"		,cFilAnt})

	For nCont:= 1 To Len(aItens2)
		aLinha := {}
		aadd(aLinha,{"C7_PRODUTO"	,aItens2[nCont][nPosProd][2]	,Nil})
		aadd(aLinha,{"C7_QUANT"		,aItens2[nCont][nPosQuant][2]	,Nil})
		aadd(aLinha,{"C7_PRECO"		,aItens2[nCont][nPosVlr][2]		,Nil})
		aadd(aLinha,{"C7_TES"		,cTesPC4						,Nil})
		aadd(aItensPC,aLinha)
	Next nCont
	lMsErroAuto	:= .F.
	cMat120Num		:= ""	//Variavel preenchida na A120Grava
	MsExecAuto({|x,y,z| MATA120(1,x,y,z)}, aCabPC,aItensPC,3)

	If lMsErroAuto
		AutoGrLog("Erro ao gerar pedido de compra!")
		MostraErro()
		Return .F.
	Else
		cPedido4	:= If(!Empty(cMat120Num),cMat120Num,cPedido4)
		dbSelectArea("SC7")
		dbSetOrder(1)
		If !MsSeek(xFilial("SC7")+cPedido4)
			AutoGrLog("Erro ao localizar pedido de compra gerado!")
			MostraErro()
			Return .F.
		EndIf
	Endif
	RecLock("SZ5",.T.)
	SZ5->Z5_FILIAL	:= xFilial("SZ5")
	SZ5->Z5_CONTRAT	:= cContrato
	SZ5->Z5_MEDICAO	:= cMedicao
	SZ5->Z5_CLIENTE	:= aCliente[1]
	SZ5->Z5_LOJA		:= aCliente[2]
	SZ5->Z5_NOME		:= aCliente[3]
	SZ5->Z5_EMPPV1	:= cFilAnt
	SZ5->Z5_PV01		:= cPedido1
	SZ5->Z5_EMPPV2	:= "010105"
	SZ5->Z5_PV02		:= cPedido2
	SZ5->Z5_EMPPV3	:= "010105"
	SZ5->Z5_PV03		:= cPedido3
	SZ5->Z5_EMPPC4	:= cFilAnt
	SZ5->Z5_PC04		:= cPedido4
	SZ5->Z5_NATUREZ		:= cNaturez
	If lJaLiberado
		SZ5->Z5_LIBERAC	:= "Z"
	EndIf
	SZ5->(MsUnLock())
Return .T.

Static Function LoadSZ3( oGrid, lCopia )
	Local aArea   := GetArea()
	Local aFields := {}
	Local aRet    := {}//FormLoadGrid(oGrid)
	Local cTmp    := ''
	Local cQuery  := ''
	Local cFields := 'R_E_C_N_O_'
	Local nTam
	Local aStruct	:= oGrid:oFormModelStruct:GetFields()
	Local cProdIni	:= cProdFim	:= Space(GetSx3Cache("B1_COD","X3_TAMANHO"))
	Local cCategoria	:= Space(GetSx3Cache("B1_YCATE","X3_TAMANHO"))
	Local cSerie		:= Space(GetSx3Cache("B1_YSERIE","X3_TAMANHO"))
	Local cVolume		:= Space(GetSx3Cache("B1_YVOL","X3_TAMANHO"))
	Local cTes			:= Space(GetSx3Cache("Z2_TES","X3_TAMANHO"))
	Local aParam	:= {}
	Local aRetParm		:= {}
	Local lOk		:= .F.
	aAdd(aParam,{1,"Produto De"		,cProdIni	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","SB1",".T.",80,.F.})
	aAdd(aParam,{1,"Produto Ate"	,cProdFim	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","SB1",".T.",80,.F.})
	aAdd(aParam,{1,"Categoria"		,cCategoria,GetSx3Cache("B1_YCATE","X3_PICTURE"),".T.","Z7",".T.",80,.F.})
	aAdd(aParam,{1,"Serie"			,cSerie	,GetSx3Cache("B1_YSERIE","X3_PICTURE"),".T.","Z1",".T.",80,.F.})
	aAdd(aParam,{1,"Volume"			,cVolume	,GetSx3Cache("B1_YVOL","X3_PICTURE"),".T.","Z3",".T.",80,.F.})
	aAdd(aParam,{1,"Tes"				,cTes		,GetSx3Cache("Z2_TES","X3_PICTURE"),".T.","SF4",".T.",80,.F.})
	If ParamBox(aParam,"Filtrar Medi��o",@aRetParm,{||.T.},,,,,,"U_CADSZ3",.T.,.T.)
		lOk	:= .T.
	EndIf
	oModel 		:= FWModelActive()

	// Pega campos que fazem parte da estrutura do objeto, para otimizar retorno da query
	cTmp   := GetNextAlias()
	cQuery := ""
	cQuery += "SELECT Z2_ITEM,Z2_PRODUTO,Z2_DESC,Z2_VLUNIT,Z2_SALDO FROM "+RetSqlName( 'SZ2' ) + " SZ2"
	cQuery += " LEFT JOIN "+RETSQLNAME("SB1")+" SB1"
	cQuery += " ON B1_FILIAL='"+xFilial("SB1")+"' AND B1_COD=Z2_PRODUTO AND SB1.D_E_L_E_T_=' '"
	cQuery += " WHERE Z2_FILIAL='"+ xFilial( 'SZ2' ) + "'"
	cQuery += " AND Z2_CONTRAT='"+SZ1->Z1_CONTRAT+"' AND Z2_SALDO>0"
	If lOk
		cQuery += " AND Z2_PRODUTO>='"+aRetParm[1]+"' and Z2_PRODUTO<='"+aRetParm[2]+"' "
		If !Empty(aRetParm[3])
			cQuery += " AND B1_YCATE='"+aRetParm[3]+"' "
		EndIf
		If !Empty(aRetParm[4])
			cQuery += " AND B1_YSERIE='"+aRetParm[4]+"' "
		EndIf
		If !Empty(aRetParm[5])
			cQuery += " AND B1_YVOL='"+aRetParm[5]+"' "
		EndIf
		If !Empty(aRetParm[6])
			cQuery += " AND Z2_TES='"+aRetParm[6]+"' "
		EndIf
	EndIf
	cQuery += " AND SZ2.D_E_L_E_T_=' '"
	cQuery += " ORDER BY Z2_ITEM"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.F.,.T.)
	While (cTmp)->(!EOF())
		If (cTmp)->Z2_SALDO<=0
			(cTmp)->(DbSkip())
			Loop
		EndIf
		AADD(aRet,{0,{}})
		nTam	:= Len(aRet)
		For nCont:=1 To Len(aStruct)
			AADD(aRet[nTam][2],CriaVar(aStruct[nCont][MODEL_FIELD_IDFIELD]))
			If aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_FILIAL"
				aRet[nTam][2][nCont]	:= xFilial("SZ3")
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_CONTRAT"
				aRet[nTam][2][nCont]	:= SZ1->Z1_CONTRAT
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_ITEM"
				aRet[nTam][2][nCont]	:= (cTmp)->Z2_ITEM
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_CLIENTE"
				aRet[nTam][2][nCont]	:= SZ1->Z1_CLIENTE
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_LOJA"
				aRet[nTam][2][nCont]	:= SZ1->Z1_LOJA
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_NOME"
				aRet[nTam][2][nCont]	:= SZ1->Z1_NOME
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_PRODUTO"
				aRet[nTam][2][nCont]	:= (cTmp)->Z2_PRODUTO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_DESC"
				aRet[nTam][2][nCont]	:= (cTmp)->Z2_DESC
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_QTDMED"
				aRet[nTam][2][nCont]	:= 0//(cTmp)->Z2_SALDO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_VLUNIT"
				aRet[nTam][2][nCont]	:= (cTmp)->Z2_VLUNIT
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_TOTAL"
				aRet[nTam][2][nCont]	:= 0//(cTmp)->Z2_SALDO*(cTmp)->Z2_VLUNIT
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z3_SALDO"
				aRet[nTam][2][nCont]	:= (cTmp)->Z2_SALDO
			EndIf
		Next
		(cTmp)->(DbSkip())
	EndDo
	(cTmp)->(dbCloseArea())
	RestArea(aArea)
Return aRet

Static Function MedirTotal(oView)
	Local oModel		:= FWModelActive()
	Local oModelSZ3	:= oModel:GetModel('SZ3DETAIL')
	Local aSaveLines	:= FWSaveRows()
	For nCont := 1 To oModelSZ3:Length()
		oModelSZ3:GoLine(nCont)
		If oModelSZ3:IsDeleted()
			Loop
		EndIf
		If !oModelSZ3:SetValue("Z3_QTDMED", oModelSZ3:GetValue("Z3_SALDO") )
			Exit
		EndIf
	Next
	FWRestRows( aSaveLines )
Return

Static Function MedirLinhaT(oView)
	Local oModel		:= FWModelActive()
	Local oModelSZ3	:= oModel:GetModel('SZ3DETAIL')
	//Local aSaveLines	:= FWSaveRows()
	If !oModelSZ3:SetValue("Z3_QTDMED", oModelSZ3:GetValue("Z3_SALDO") )
		Return
	EndIf
	If oModelSZ3:GetLine() < oModelSZ3:Length()
		oModelSZ3:GoLine(oModelSZ3:GetLine()+1)
	EndIf
	//FWRestRows( aSaveLines )
Return


/*/{Protheus.doc} GetTes
Localiza a TES para usar de acordo com a TES do contrato
@author Saulo Gomes Martins
@since 24/12/2014
@version 1.0
@param cParam, character, Parametro com as TES em formato string TES_CONTRATO:TES_PARA_USAR;TES_CONTRATO:TES_PARA_USAR Ex.: 501:510;502:511
@param cTes, character, Tes do contrato
@return cRetTES, Tes para usar
/*/
Static Function GetTes(cParam,cTes)
	Local cRetTES	:= ""
	Local aTES		:= {}
	Local aTmp		:= Separa(cParam,";",.T.)
	Local nPos
	aEval(aTmp,{|x| If(":"$x,AADD(aTES,Separa(x,":",.T.)),AADD(aTES,Separa(x+":",":",.T.)))})
	nPos	:= aScan(aTES,{|x| x[1]==cTes})
	If nPos>0
		cRetTES	:= aTES[nPos][2]
	EndIf
Return cRetTES