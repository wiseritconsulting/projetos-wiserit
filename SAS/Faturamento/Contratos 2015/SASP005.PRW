#include "protheus.ch"
#INCLUDE "TBICONN.CH" 
#include "TOTVS.CH"
#INCLUDE "rwmake.ch"

// importa��o de contrato (em homologa��o)

User Function SASP005()

	Local aRet := {}
	Local aParamBox := {}
	Local aReturn := {}
	Local cTabela		:= SuperGetMV("MV_TABPAD")
	
	
	//criado por castro em: 12-08-2015 inicio
	
	Local nSaldo := 0

	//criado por castro em: 12-08-2015	fim
	
	Private cCadastro := "Importacao"

	aAdd(aParamBox,{6,"Buscar arquivo",Space(50),"","","",50,.F.,"Todos os arquivos (*.csv) |*.csv"})
	
	If !ParamBox(aParamBox,"Par�metros...",@aRet)
	      MsgInfo("Cancelado pelo usu�rio.")
	      Return''
	Endif
	
	_cArq := @aRet[1]

	If !File(_cArq)
		MsgStop("O arquivo " +_cArq + " n�o foi encontrado.","ATENCAO")
		Return
	EndIf

	FT_FUse( Alltrim(_cArq) )
	FT_FGoTop()
	
	While !FT_FEof()
	
		nLinha := FT_FREADLN()
		
		
		// testar antes da producao
		if len(nLinha) <= 9
			FT_FSkip()
			LOOP
		endif
		
		aLinha := StrTokArr(nLinha,";")
		//,Z2_CONTRAT,Z2_PRODUTO,Z2_QUANT,Z2_TES,Z2_APFL,Z2_YTPC,Z2_MSFIL
		//.t.,'050101','000999','999999999',111.11,'505',4,'DESCRICAO APFL',4,'DESCRICAO TPC'		
		//valor fixo 000,filial,contrato,produto,valor fixo 000,valor,tes,aplica��o fluxo,valor fixo 000,tipo produto,valor fixo 000
	
		dbSelectArea("SZ1")
		dbSetOrder(1)
		dbGoTop()
		dbSelectArea("SB1")
		dbSetOrder(1)
		dbGoTop()
		dbSelectArea("SF4")
		dbSetOrder(1)
		dbGoTop()
		dbSelectArea("ZZD")
		dbSetOrder(1)
		dbGoTop()
		dbSelectArea("ZZE")
		dbSetOrder(1)		
		dbGoTop()
		
		//inserido por castro inicio
		
		
			DBSELECTAREA("SZ2")
			cFiltro_EV := "Z2_FILIAL == aLinha[2] .AND. Z2_CONTRAT == aLinha[3] .AND. SZ2->Z2_PRODUTO == aLinha[4]"
			SET FILTER TO &cFiltro_EV
			DBGOTOP()
			While !(SZ2->(Eof()))
				//RecLock("SEV",.F.)
				//SEV->EV_LA := ""
				//SEV->(MsUnlock())
				//SEV->(dbSkip())
				nSaldo += SZ2->Z2_SALDO
			ENDDO
			SET FILTER TO
			SZ2->(DBCloseArea())		
		
		
		//dbSelectArea("SZ2")
		//dbSetOrder(1)
		//dbGoTop()
		//IF SZ2->Z2_FILIAL ==aLinha[2] .AND. SZ2->Z2_CONTRAT == aLinha[3] .AND. SZ2->Z2_PRODUTO == aLinha[4]
		//	nSaldo += SZ2->Z2_SALDO
		//ENDIF
		
		//INSERIDO POR CASTRO fim
		
		AADD(aLinha,"")
		
		aLinha[1]  := .T.
		aLinha[5]  := Posicione("SB1",1,xFilial("SB1")+aLinha[4],"B1_DESC") // Produto
		aLinha[9]  := Posicione("ZZD",1,xFilial("ZZD")+aLinha[8],"ZZD_DESC") // Aplic Fluxo
		aLinha[11] := Posicione("ZZE",1,xFilial("ZZE")+aLinha[10],"ZZE_DESC") // Tipo
		
		SZ1->(dbSeek(xFilial("SZ1")+aLinha[3]))
		SZ1->Z1_CLIENTE
		SZ1->Z1_LOJA
		If SA1->(dbSeek(xFilial("SA1")+SZ1->Z1_CLIENTE+SZ1->Z1_LOJA)) .AND. !Empty(SA1->A1_TABELA)
			cTabela	:= SA1->A1_TABELA
		EndIf	
		
		
		if cFilAnt != aLinha[2] //(dbSeek(xFilial("SZ1") + aLinha[3]))
			aLinha[1] := .F.
			aLinha[12] := "Filial corrente diferente"		
		elseif !SZ1->(dbSeek(xFilial("SZ1") + aLinha[3]))
			aLinha[1] := .F.
			aLinha[12] := "Contrato N�o Encontrado"
		elseif !SB1->(dbSeek(xFilial("SB1") + aLinha[4]))
			aLinha[1] := .F.
			aLinha[12] += "Produto N�o Encontrato"
/*		elseif PrecoTab(aLinha[4],cTabela) <= 0 //ValType(val(aLinha[6])) <> 'N'
			aLinha[1] := .F.
			aLinha[12] += "Valor do Produto Invalido."*/			
		elseif !SF4->(dbSeek(xFilial("SF4") + aLinha[7]))
			aLinha[1] := .F.
			aLinha[12] += "TES N�o Encontrada"
		elseif !ZZD->(dbSeek(xFilial("ZZD") + aLinha[8]))
			aLinha[1] := .F.
			aLinha[12] += "Aplica��o N�o Encontrada"
		elseif !ZZE->(dbSeek(xFilial("ZZE") + aLinha[10]))
			aLinha[1] := .F.
			aLinha[12] += "Tipo N�o Encontrado"
	
		//inserido por castro inicio
		
		elseif nSaldo > SZ1->Z1_SALDO
			aLinha[1] := .F.
			aLinha[12] += "Saldo do Contrato N�o Deve Ser Negativo"
		
		//inserido por castro fim
					
		endif
		
		
		/*
		
		verificar se contrato existe
		verificar se produto existe
		verificar se produto existe no contrato
			- se existir aditiva ao item existente
			- se n�o existir cria um novo item
		verifica se a quantidade a ser aditivada � uma quantidade v�lida. (evitando deixar negativo)
		verifica se a tes existe
		verifica se a filial existe
		verifica se existe apfl
		verifica se existe ytpc
		
		apos a valida��es e confirma��o do usu�rio.
		
		altera SZ2 = 1: (Z2_SALDO += QUANTIDADE ADITIVADA) 2: (Z2_QTDADT += QUANTIDADE ADITIVADA)
		altera SZ1 = 1: (Z1_SALDO += QUANTIDADE ADITIVADA) 2: (Z1_QTDADT += QUANTIDADE ADITIVADA)
		
		
		*/
		
		aadd(aReturn,aLinha)
		FT_FSkip()
	End Do
		
		
		SASP005A(aReturn)
		
	//RESET ENVIRONMENT
Return



Static Function SASP005A(aReturn)

	Local oSim     := LoadBitmap(GetResources(),'br_verde') 
	Local oNao     := LoadBitmap(GetResources(),'br_vermelho') 

	DEFINE DIALOG oDlg TITLE "Importacao de Contrato" FROM 0,0 TO 550,950 PIXEL

    // Cria Browse
    oBrowse := TCBrowse():New( 01 , 01, 475, 250,,;
                              {},{},;
                              oDlg,,,,,{||},,,,,,,.F.,,.T.,,.F.,,, )
    
    // Seta array para o browse                            
    oBrowse:SetArray(aReturn) 
    
    // Adciona colunas
    oBrowse:AddColumn( TCColumn():New(''          ,{ || IIF(aReturn[oBrowse:nAt,1],oSim,oNao)},,,,,,.T.,.F.,,,,.F.,) )
    oBrowse:AddColumn( TCColumn():New('Filial'    ,{ || aReturn[oBrowse:nAt,2] },,,,"LEFT",,.F.,.T.,,,,.F.,) )
    oBrowse:AddColumn( TCColumn():New('Contrato'  ,{ || aReturn[oBrowse:nAt,3] },,,,"LEFT",,.F.,.T.,,,,.F.,) ) 
    oBrowse:AddColumn( TCColumn():New('Produto'   ,{ || aReturn[oBrowse:nAt,4] },,,,"LEFT",,.F.,.T.,,,,.F.,) ) 
    oBrowse:AddColumn( TCColumn():New('Descricao' ,{ || aReturn[oBrowse:nAt,5] },,,,"LEFT",,.F.,.T.,,,,.F.,) ) 
    oBrowse:AddColumn( TCColumn():New('Quantidade',{ || aReturn[oBrowse:nAt,6] },"@!",,,"RIGHT",,.F.,.T.,,,,.F.,) )
    oBrowse:AddColumn( TCColumn():New('TES'       ,{ || aReturn[oBrowse:nAt,7] },,,,"LEFT",,.F.,.T.,,,,.F.,) )
    oBrowse:AddColumn( TCColumn():New('Aplic.'    ,{ || aReturn[oBrowse:nAt,8] },,,,"LEFT",,.F.,.T.,,,,.F.,) )
    oBrowse:AddColumn( TCColumn():New('Descricao' ,{ || aReturn[oBrowse:nAt,9] },,,,"LEFT",,.F.,.T.,,,,.F.,) )
    oBrowse:AddColumn( TCColumn():New('Tipo'      ,{ || aReturn[oBrowse:nAt,10] },,,,"LEFT",,.F.,.T.,,,,.F.,) )
    oBrowse:AddColumn( TCColumn():New('Descricao' ,{ || aReturn[oBrowse:nAt,11] },,,,"LEFT",,.F.,.T.,,,,.F.,) )
    oBrowse:AddColumn( TCColumn():New('Ocorrencia',{ || aReturn[oBrowse:nAt,12] },,,,"LEFT",,.F.,.T.,,,,.F.,) )
    
    // Adiciona Botoes      
    TButton():New( 255, 335, "Processamento", oDlg,{|| Processa( {|| SASP005P(aReturn) },"Processando.. Aguarde..." )   /*SASP005P(aReturn)*/ },60,015,,,.F.,.T.,.F.,,.F.,,,.F. )
    TButton():New( 255, 405, "Sair", oDlg,{|| Close(oDlg) },60,015,,,.F.,.T.,.F.,,.F.,,,.F. )
    
  ACTIVATE DIALOG oDlg CENTERED 
  
   
  
Return

Static Function SASP005P(aReturn)

	Local cTabela		:= SuperGetMV("MV_TABPAD")
	Local nErro			:= 0
	Local cErroIT		:= ""
	Local y :=1
	Local i :=1
	
	ProcRegua(len(aReturn)) 

	for y:=1 to len(aReturn)
	
		if !aReturn[y][1]
			Alert('Existe n�o conformidade no aquivo informado.')
			Return
		endif
		
	next y


	BEGIN TRANSACTION  
	
		dbSelectArea("SZ2")
		dbSetOrder(3)
		dbGoTop()
		
		for i:=1 to len(aReturn)
		
		IncProc("Processando item : "+StrZero(i,3))
			
			SZ1->(dbSeek(xFilial("SZ1")+aReturn[i][3]))
			SZ1->Z1_CLIENTE
			SZ1->Z1_LOJA
			If SA1->(dbSeek(xFilial("SA1")+SZ1->Z1_CLIENTE+SZ1->Z1_LOJA)) .AND. !Empty(SA1->A1_TABELA)
				cTabela	:= SA1->A1_TABELA
			EndIf
		
			if SZ2->(dbSeek(xFilial("SZ2")+aReturn[i][3]+PADR(ALLTRIM(aReturn[i][4]),15)+aReturn[i][7]))
			
			IncProc("Processando item : "+StrZero(i,3)+" - "+SZ2->Z2_DESC)
			
						SZ1->(dbSeek(xFilial("SZ1")+aReturn[i][3]))
						
						RecLock("SZ4",.T.)
							SZ4->Z4_FILIAL	:= xFilial("SZ4")
							SZ4->Z4_CONTRAT := SZ2->Z2_CONTRAT
							SZ4->Z4_CLIENTE := SZ1->Z1_CLIENTE
							SZ4->Z4_LOJA    := SZ1->Z1_LOJA 
							SZ4->Z4_NOME    := SZ2->Z2_DESC
							SZ4->Z4_EMISSAO := ddatabase
							SZ4->Z4_HORA    := time()
							SZ4->Z4_RESPON  := cusername
							SZ4->Z4_ITEM    := SZ2->Z2_ITEM
							SZ4->Z4_PRODUTO := SZ2->Z2_PRODUTO
							SZ4->Z4_QTDORIG := SZ2->Z2_QUANT
							SZ4->Z4_QTDADT  := val(aReturn[i][6])
							SZ4->Z4_SALDO   := SZ2->Z2_SALDO + val(aReturn[i][6])
						SZ4->(MsUnLock())
							
						RecLock("SZ2",.F.)
							SZ2->Z2_QTDADT	+= val(aReturn[i][6])
							SZ2->Z2_SALDO   += val(aReturn[i][6])
						SZ2->(MsUnLock())
						
						RecLock("SZ1",.F.)
							SZ1->Z1_SALDO	+= val(aReturn[i][6])
							SZ1->Z1_VLADT   += SZ4->Z4_QTDADT*SZ2->Z2_VLUNIT
						SZ1->(MsUnLock())			
		
		
			Else
	
	
			IncProc("Processando item(novo) : "+StrZero(i,3)+" - "+aReturn[i][5])
	
						//SZ1->(dbSeek(xFilial("SZ1")+aReturn[i][3]))
						cTmp   := GetNextAlias()
						//cQuery := "SELECT MAX(Z2_ITEM)+1 as Z2_NXITEM FROM "+RetSQLName("SZ2")+ " WHERE Z2_FILIAL='"+xFilial("SZ2")+"' AND Z2_CONTRAT='"+aReturn[i][3]+"' AND D_E_L_E_T_=''"
						cQuery := "SELECT TOP 1 Z2_ITEM as Z2_NXITEM FROM "+RetSQLName("SZ2")+ " WHERE Z2_FILIAL='"+xFilial("SZ2")+"' AND Z2_CONTRAT='"+aReturn[i][3]+"' AND D_E_L_E_T_='' ORDER BY Z2_ITEM DESC"
						dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.F.,.T.)
						
						RecLock("SZ2",.T.)
							SZ2->Z2_FILIAL  := xFilial("SZ2")
							SZ2->Z2_MSFIL   := aReturn[i][2]
							SZ2->Z2_CONTRAT := aReturn[i][3]
							cNumSeq 		:= ((cTmp)->Z2_NXITEM)
							cNumSeq			:= Strzero(Val(RetASC(cNUmSeq,3,.F.))+1,3)
							cNumSeq 		:= RetASC(cNumSeq,3,.T.)
							SZ2->Z2_ITEM    := cNumSeq
							//SZ2->Z2_ITEM    := StrZero(SOMA1((cTmp)->Z2_NXITEM),3)
							SZ2->Z2_PRODUTO := aReturn[i][4]
							SZ2->Z2_DESC    := aReturn[i][5]
							SZ2->Z2_QUANT   := val(aReturn[i][6])
							
							vlrUnit :=  PrecoTab(aReturn[i][4],cTabela) //GetValor(cTabela,aReturn[i][4])
							SZ2->Z2_VLUNIT  := vlrUnit
							
							IF vlrUnit <= 0
								nErro++
								cErroIT += + CHR(10) + CHR(13) + "Contrato: " + aReturn[i][3] + ' - Produto:' + aReturn[i][4] + ' - Valor:' + str(vlrUnit)
							endif
							//MsgInfo("text","title")
							vlrDesc := RetDsc(aReturn[i][4],cTabela,val(aReturn[i][6]))
							SZ2->Z2_DESCONT := vlrDesc //RetDsc(cProduto,cTabela,nQuant)
							
							SZ2->Z2_TOTAL   := val(aReturn[i][6]) * vlrUnit
							
							SZ2->Z2_VLRLIQ  := (val(aReturn[i][6]) * vlrUnit)-vlrDesc
							SZ2->Z2_TES     := aReturn[i][7]
							SZ2->Z2_QTDMED  := 0
							SZ2->Z2_DTMED   := ctod("  /  /  ")
							SZ2->Z2_QTDADT  := 0
							SZ2->Z2_APFL    := aReturn[i][8]
							SZ2->Z2_YTPC    := aReturn[i][10]
							//SZ2->Z2_QTDADT	:= 0 //val(aReturn[i][6])
							SZ2->Z2_SALDO   := val(aReturn[i][6])
						SZ2->(MsUnLock())
						
						RecLock("SZ4",.T.)
							SZ4->Z4_FILIAL	:= xFilial("SZ4")
							SZ4->Z4_CONTRAT := aReturn[i][3]
							SZ4->Z4_CLIENTE := SZ1->Z1_CLIENTE
							SZ4->Z4_LOJA    := SZ1->Z1_LOJA 
							SZ4->Z4_NOME    := SZ1->Z1_NOME
							SZ4->Z4_EMISSAO := ddatabase
							SZ4->Z4_HORA    := time()
							SZ4->Z4_RESPON  := cusername
							SZ4->Z4_ITEM 	:= cNumSeq
							//SZ4->Z4_ITEM    := StrZero((cTmp)->Z2_NXITEM,3)
							SZ4->Z4_PRODUTO := SZ2->Z2_PRODUTO
							SZ4->Z4_QTDORIG := SZ2->Z2_QUANT
							SZ4->Z4_QTDADT  := val(aReturn[i][6])
							SZ4->Z4_SALDO   := SZ2->Z2_SALDO 
						SZ4->(MsUnLock())
							
							
						RecLock("SZ1",.F.)
							SZ1->Z1_SALDO	+= val(aReturn[i][6])
							SZ1->Z1_VLADT   += SZ4->Z4_QTDADT*SZ2->Z2_VLUNIT
						SZ1->(MsUnLock())	
									
	
			EndIf
			
		next i
		
		if nErro > 0
			Alert('N�o Importado. Existem produtos com valor igual zero.'+ cErroIT)
			DisarmTransaction() 
		else
			MSGINFO( "Importa��o concluida com sucesso.", "Importa��o" )
			Close(oDlg)
		end if
		
	END TRANSACTION

Return


Static Function PrecoTab(cProduto,cTabela)

	Local lKit			:= ExistCpo("SZ6",cProduto,1,,.F.)
	Local nValorTab		:= 0
	Local _cGetArea		:= GetArea()

	If lKit
		SZ7->(DbSetOrder(1))
		SZ7->(DbSeek(xFilial("SZ7")+cProduto))
		While SZ7->(!EOF()) .AND. (xFilial("SZ7")+PADR(cProduto,15))==SZ7->(Z7_FILIAL+Z7_COD)
			nValorTab += SZ7->Z7_QUANT * MaTabPrVen(cTabela,SZ7->Z7_PRODUTO,1,SZ1->Z1_CLIENTE,SZ1->Z1_LOJA) //GetValor(cTabela,SZ7->Z7_PRODUTO) //
			SZ7->(DbSkip())
		EndDo
	Else
		nValorTab	:= MaTabPrVen(cTabela,PADR(cProduto,15),1,SZ1->Z1_CLIENTE,SZ1->Z1_LOJA)  // GetValor(cTabela,cProduto) //
	EndIf
	
	RestArea(_cGetArea)

Return nValorTab


Static Function RetDsc(cProduto,cTabela,nQuant)
	Local nRet:= 0
	Local aAreaZ1	:= GetArea()

	lKit:= ExistCpo("SZ6",cProduto,1,,.F.)

	If lKit
		SZ7->(DbSetOrder(1))
		SZ7->(DbSeek(xFilial("SZ7")+Alltrim(cProduto)))
		While SZ7->(!EOF()) .AND. Alltrim(xFilial("SZ7")+cProduto)==Alltrim(SZ7->(Z7_FILIAL+Z7_COD))
			nVlrItem	:= MaTabPrVen(cTabela,SZ7->Z7_PRODUTO,1,SZ1->Z1_CLIENTE,SZ1->Z1_LOJA) //GetValor(cTabela,SZ7->Z7_PRODUTO) //
				If SZ7->Z7_PERC > 0
					nRet+= (( nVlrItem*SZ7->Z7_QUANT*nQuant)*(SZ7->Z7_PERC/100))
				Endif
			SZ7->(dbSkip())
		Enddo
	Endif

	RestArea(aAreaZ1)

Return nRet


Static Function GetValor(cTabela,cProduto)
	Local _AreaDA1	:= GetArea()
	Local vRet := 0
	
    DBSELECTAREA("DA1")
    DBSETORDER(1)                                         
    IF DBSEEK(XFILIAL("DA1")+cTabela+cProduto)
		vRet := DA1->DA1_PRCVEN 
	End if	
	RestArea(_AreaDA1)
	
Return vRet



	













