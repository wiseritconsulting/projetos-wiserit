#Include 'Protheus.ch'
/*/{Protheus.doc} SASR014
Relat�rio de Auditoria Opera��o Triangular
@author Diogo
@since 18/05/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function SASR014()

	Local oReport
	Private cPerg := PadR('SASR014',10)

	CriaSX1(cPerg)
	Pergunte(cPerg,.T.)

	oReport := reportDef()
	oReport:printDialog()
Return

static function reportDef()
	local oReport
	Local oSection1
	Local oSection2
	Local oSection3
	local cTitulo := 'Relat�rio Auditoria Opera��o Triangular'

	oReport := TReport():New('SASR014', cTitulo,cPerg, {|oReport| PrintReport(oReport)},"Relat�rio Auditoria Opera��o Triangular")
	oReport:SetLandscape()
	oReport:SetTotalInLine(.F.)
	oReport:ShowHeader()

	oSection1 	:= TRSection():New(oReport,"AUDITORIA",{"QRY"})
	//oSection1:SetTotalInLine(.T.)

	TRCell():New(oSection1, "Z5_EMPPV1"		, "QRY", 'FILIAL 1'	,,TamSX3("Z5_EMPPV1")[1]+5,,)
	TRCell():New(oSection1, "Z5_NF01"		, "QRY", 'NF 1  '	,,27,,,,.T.)
	TRCell():New(oSection1, "Z5_PV01"		, "QRY", 'PV 1'		,,TamSX3("Z5_PV01")[1]+1,,)
	TRCell():New(oSection1, "Z5_EMPPV2"		, "QRY", 'FILIAL 2'	,,TamSX3("Z5_EMPPV2")[1]+5,,)
	TRCell():New(oSection1, "Z5_NF02"		, "QRY", 'NF 2  '	,,27,,)
	TRCell():New(oSection1, "Z5_PV02"		, "QRY", 'PV 2'		,,TamSX3("Z5_PV02")[1]+1,,)
	TRCell():New(oSection1, "Z5_EMPPV3"		, "QRY", 'FILIAL 3'	,,TamSX3("Z5_EMPPV3")[1]+5,,)
	TRCell():New(oSection1, "Z5_NF03"		, "QRY", 'NF 3  '	,,27,,)
	TRCell():New(oSection1, "Z5_PV03"		, "QRY", 'PV 3'		,,TamSX3("Z5_PV03")[1]+1,,)
	TRCell():New(oSection1, "Z5_EMPPC4"		, "QRY", 'FILIAL 4'	,,TamSX3("Z5_EMPPC4")[1]+5,,)
	TRCell():New(oSection1, "Z5_NF04"		, "QRY", 'NF 4  '	,,27,,)
	TRCell():New(oSection1, "Z5_PC04"		, "QRY", 'PV 4'		,,TamSX3("Z5_PC04")[1]+1,,)
	TRCell():New(oSection1, "CMSG1"			, "QRY", 'OCOR NF 1',,200,,,,.T.)
	//TRCell():New(oSection1, "CMSG2"			, "QRY", 'OCOR NF 2',,200,,,,.T.)
	//TRCell():New(oSection1, "CMSG3"			, "QRY", 'OCOR NF 3',,200,,,,.T.)
	//TRCell():New(oSection1, "CMSG4"			, "QRY", 'OCOR NF 4',,200,,,,.T.)

		
return (oReport)

Static Function PrintReport(oReport)
	Local oSection1 := oReport:Section(1)
	Local cSetSql
	Local nTotQt := 0
	Local nTotPr := 0
	Private _cTpCli
	
	cSetSql := qryDefault()

	cSetSql := ChangeQuery(cSetSql)

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY", .F., .T.)

	DbSelectArea('QRY')

	dbGoTop()
	oReport:SetMeter(QRY->(RecCount()))

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)

	While QRY->(!Eof())

		If oReport:Cancel()
			Exit
		EndIf

		oReport:IncMeter()
		
		cRetErro	:= ""
		cRetErro1	:= ""
		cRetErro2	:= ""
		cRetErro3	:= ""
		cRetErro4	:= ""

			If QRY->CLICONT1 <> QRY->CLINF1 .and. !empty(strtran(QRY->NF1CONT1,"/",""))
				cRetErro1+= "NOTA 1 - CLIENTE ("+QRY->CLICONT1+;
							") DIFERENTE DA NOTA FISCAL ("+QRY->CLINF1+") "+chr(13)+chr(10)		
			Endif
			If QRY->NF1CONT1 <> QRY->NF1PV .and. !empty(strtran(QRY->NF1CONT1,"/","")) 
				cRetErro1+= "NOTA 1 ("+QRY->NF1CONT1+") DIVERGENTE DA NOTA FISCAL ("+QRY->NF1PV+") "+chr(13)+chr(10)		
			Endif
			If empty(QRY->CHVNF1) .and. !empty(strtran(QRY->NF1CONT1,"/",""))
				cRetErro1+= "NOTA 1 ("+QRY->NF1CONT1+") N�O FOI TRANSMITIDA "+chr(13)+chr(10)
			Endif

			If QRY->CLICONT2 <> QRY->CLINF2 .and. !empty(strtran(QRY->NF1CONT2,"/",""))
				cRetErro2+= "NOTA 2 - CLIENTE ("+QRY->CLICONT2+;
							") DIFERENTE DA NOTA FISCAL ("+QRY->CLINF2+") "+chr(13)+chr(10)		
			Endif

			If QRY->NF1CONT2 <> QRY->NF2PV .and. !empty(strtran(QRY->NF1CONT2,"/",""))
				cRetErro2+= "NOTA 2 ("+QRY->NF1CONT2+") DIVERGENTE DA NOTA FISCAL ("+QRY->NF2PV+") "+chr(13)+chr(10)		
			Endif

			If empty(QRY->CHVNF2) .and. !empty(strtran(QRY->NF1CONT2,"/","")) 
				cRetErro2+= "NOTA 2 ("+QRY->NF1CONT2+") N�O FOI TRANSMITIDA "+chr(13)+chr(10)
			Endif

			If QRY->CLICONT3 <> QRY->CLINF3 .and. !empty(strtran(QRY->NF1CONT3,"/",""))
				cRetErro3+= "NOTA 3 - CLIENTE ("+QRY->CLICONT3+;
							") DIFERENTE DA NOTA FISCAL ("+QRY->CLINF3+") "+chr(13)+chr(10)		
			Endif

			If QRY->NF1CONT3 <> QRY->NF3PV .and. !empty(strtran(QRY->NF1CONT3,"/",""))
				cRetErro3+= "NOTA 3 ("+QRY->NF1CONT3+") DIVERGENTE DA NOTA FISCAL ("+QRY->NF3PV+") "+chr(13)+chr(10)		
			Endif

			If empty(QRY->CHVNF3) .and. !empty(strtran(QRY->NF1CONT3,"/",""))
				cRetErro3+= "NOTA 3 ("+QRY->NF1CONT3+") N�O FOI TRANSMITIDA "+chr(13)+chr(10)
			Endif

			If QRY->FORCONT4 <> QRY->FORPC4
				cRetErro4+= "NOTA 4 - FORNECEDOR ("+QRY->FORCONT4+;
							") DIFERENTE DA NOTA FISCAL ("+QRY->FORPC4+") "+chr(13)+chr(10)		
			Endif

			If QRY->NF1CONT4 <> QRY->NF4PC .and. !empty(strtran(QRY->NF1CONT4,"/",""))
				cRetErro4+= "NOTA 4 ("+QRY->NF1CONT4+") DIVERGENTE DA NOTA FISCAL ("+QRY->NF4PC+") "+chr(13)+chr(10)		
			Endif
			
			If empty(cRetErro1+cRetErro2+cRetErro3+cRetErro4) .and.  mv_par03 == 2 //Listar apenas Inconsistentes
				dbSelectArea("QRY")
				QRY->(dbSkip())
				Loop
			Endif

			If !empty(cRetErro1+cRetErro2+cRetErro3+cRetErro4) .and.  mv_par03 == 1 //Listar apenas Consistentes
				dbSelectArea("QRY")
				QRY->(dbSkip())
				Loop
			Endif
			
			If mv_par03 <> 2 //N�o for inconsistente 
				If empty(cRetErro1) .and. !empty(strtran(QRY->NF1CONT1,"/",""))
					cRetErro1:= "NOTA 1 ENCONTRADA"+chr(13)+chr(10)
				Endif
				
				If empty(cRetErro2) .and. !empty(strtran(QRY->NF1CONT2,"/",""))
					cRetErro2:= "NOTA 2 ENCONTRADA"+chr(13)+chr(10)
				Endif
	
				If empty(cRetErro3) .and. !empty(strtran(QRY->NF1CONT3,"/",""))
					cRetErro3:= "NOTA 3 ENCONTRADA"+chr(13)+chr(10)
				Endif
	
				If empty(cRetErro4) .and. !empty(strtran(QRY->NF1CONT4,"/",""))
					cRetErro4:= "NOTA 4 ENCONTRADA"+chr(13)+chr(10)
				Endif
			Endif
			
			oSection1:Cell("Z5_EMPPV1"):SetValue(QRY->FIL1)
			oSection1:Cell("Z5_PV01"):SetValue(QRY->PV1CONT)
			oSection1:Cell("Z5_NF01"):SetValue(QRY->NF1CONT1)
		
			oSection1:Cell("Z5_EMPPV2"):SetValue(QRY->FIL2)
			oSection1:Cell("Z5_NF02"):SetValue(QRY->NF1CONT2)
			oSection1:Cell("Z5_PV02"):SetValue(QRY->PV2CONT)
		
			oSection1:Cell("Z5_EMPPV3"):SetValue(QRY->FIL3)
			oSection1:Cell("Z5_NF03"):SetValue(QRY->NF1CONT3)
			oSection1:Cell("Z5_PV03"):SetValue(QRY->PV3CONT)
		
			oSection1:Cell("Z5_EMPPC4"):SetValue(QRY->FIL4)
			oSection1:Cell("Z5_NF04"):SetValue(QRY->NF1CONT4)
			oSection1:Cell("Z5_PC04"):SetValue(QRY->PC4CONT)

			oSection1:Cell("CMSG1"):SetValue(cRetErro1+cRetErro2+cRetErro3+cRetErro4)
			//oSection1:Cell("CMSG2"):SetValue(cRetErro2)
			//oSection1:Cell("CMSG3"):SetValue(cRetErro3)
			//oSection1:Cell("CMSG4"):SetValue(cRetErro4)


		oSection1:PrintLine()
		oReport:SkipLine()
		
		dbSelectArea("QRY")
		QRY->(dbSkip())

	EndDo

	QRY->(dbCloseArea())

Return

static function CriaSX1(cPerg)

	PutSx1(cPerg, "01","Periodo De" 	,"Periodo De"		,"Periodo De"	,"mv_ch01","D",08,0,2,"G","" ,"","","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Periodo Ate" 	,"Periodo Ate"		,"Periodo Ate"	,"mv_ch02","D",08,0,2,"G","" ,"","","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","Inconsistentes" ,"Inconsistentes","Inconsistentes"	,"mv_ch03","C",01,0,1,"C","",""     ,"","","mv_par03","N�o","N�o","N�o","","Sim","Sim","Sim","Todos","Todos","Todos","","","","","")

Return

//Query do Relat�rio
Static function qryDefault()

	Local cQuery := ""
	
	cQuery:= " SELECT	"+; 	
	"		Z5_EMPPV1 FIL1,"+; //EMPRESA 01
	"		Z5_PV01 PV1CONT,"+;
	"		SC51.C5_NUM PV1,"+;
	"		SA11.A1_COD+'/'+SA11.A1_LOJA CLICONT1,"+;
	"		SC51.C5_CLIENTE+'/'+SC51.C5_LOJACLI CLIPV1, "+;
	"		SF21.F2_CLIENTE+'/'+SF21.F2_LOJA CLINF1,"+;
	"		Z5_NF01+'/'+Z5_SERIE1 NF1CONT1,"+;
	"		SF21.F2_DOC+'/'+SF21.F2_SERIE NF1PV,"+;
	"		SF21.F2_CHVNFE CHVNF1,"+;
	"		Z5_EMPPV2 FIL2,"+; //EMPRESA 02
	"		Z5_PV02 PV2CONT,"+; 
	"		SC52.C5_NUM PV2,"+;
	"		SA12.A1_COD+'/'+SA12.A1_LOJA CLICONT2,"+;
	"		SC52.C5_CLIENTE+'/'+SC52.C5_LOJACLI CLIPV2,"+;
	"		SF22.F2_CLIENTE+'/'+SF22.F2_LOJA CLINF2,"+;		
	"		Z5_NF02+'/'+Z5_SERIE2 NF1CONT2,"+;
	"		SF22.F2_DOC+'/'+SF22.F2_SERIE NF2PV,"+;
	"		SF22.F2_CHVNFE CHVNF2,"+; 
	"		Z5_EMPPV3 FIL3,"+; //EMPRESA 03
	"		Z5_PV03 PV3CONT,"+;
	"		SC53.C5_NUM PV3,"+;
	"		SA13.A1_COD+'/'+SA13.A1_LOJA CLICONT3,"+;
	"		SC53.C5_CLIENTE+'/'+SC53.C5_LOJACLI CLIPV3 ,"+;
	"		SF23.F2_CLIENTE+'/'+SF23.F2_LOJA CLINF3,"+;		
	"		Z5_NF03+'/'+Z5_SERIE3 NF1CONT3,"+;
	"		SF23.F2_DOC+'/'+SF23.F2_SERIE NF3PV,"+;
	"		SF23.F2_CHVNFE CHVNF3,"+;
	"		Z5_EMPPC4 FIL4,"+; //EMPRESA 04
	"		Z5_PC04 PC4CONT,"+;
	"		SC74.C7_NUM PV3,"+;
	"		SA24.A2_COD+'/'+SA24.A2_LOJA FORCONT4,"+;
	"		SC74.C7_FORNECE+'/'+SC74.C7_LOJA FORPC4 ,"+;
	"		SF14.F1_FORNECE+'/'+SF14.F1_LOJA FORNF4,"+;		
	"		Z5_NF04+'/'+Z5_SERIE4 NF1CONT4,"+;
	"		SF14.F1_DOC+'/'+SF14.F1_SERIE NF4PC,"+;
	"		SF14.F1_CHVNFE CHVNF4 "+;
	"	FROM "+RetSqlName("SZ5")+" (NOLOCK) SZ5"+; 
	"	LEFT JOIN "+RetSqlName("SF2")+" (NOLOCK) SF21 ON "+; 
	"		SF21.F2_FILIAL = Z5_EMPPV1 AND "+;
	"		SF21.F2_SERIE = Z5_SERIE1 AND "+;
	"		SF21.F2_DOC = Z5_NF01 AND "+;
	"		SF21.D_E_L_E_T_ = ' ' AND "+;
	"		Z5_NF01 <> ' ' "+;
	"	LEFT JOIN "+RetSqlName("SC5")+" (NOLOCK) SC51 ON "+; 
	"		SC51.C5_FILIAL = Z5_EMPPV1 AND "+;
	"		SC51.C5_NUM = Z5_PV01 AND "+;
	"		SC51.D_E_L_E_T_ = ' ' AND "+;
	"		Z5_PV01 <> ' ' AND "+;
	"		SF21.F2_EMISSAO BETWEEN '"+dTos(mv_par01)+"' AND '"+dTos(mv_par02)+"' 	"+;	
	"	LEFT JOIN "+RetSqlName("SA1")+" (NOLOCK) SA11 ON "+; 
	"		Z5_CLIENTE+Z5_LOJA = SA11.A1_COD+SA11.A1_LOJA  AND "+;
	"		SA11.D_E_L_E_T_ = ' ' AND "+;
	"		SA11.A1_FILIAL = ' ' "+;	
	"	LEFT JOIN "+RetSqlName("SF2")+" (NOLOCK) SF22 ON "+; 
	"		SF22.F2_FILIAL = Z5_EMPPV2 AND "+;
	"		SF22.F2_SERIE = Z5_SERIE2 AND "+;
	"		SF22.F2_DOC = Z5_NF02 AND "+;
	"		SF22.D_E_L_E_T_ = ' ' AND "+;
	"		Z5_NF02 <> ' '"+;
	"	LEFT JOIN "+RetSqlName("SC5")+" (NOLOCK) SC52 ON "+; 
	"		SC52.C5_FILIAL = Z5_EMPPV2 AND "+;
	"		SC52.C5_NUM = Z5_PV02 AND "+;
	"		SC52.D_E_L_E_T_ = ' ' AND "+;
	"		Z5_PV02 <> ' ' AND "+;
	"		SF21.F2_EMISSAO BETWEEN '"+dTos(mv_par01)+"' AND '"+dTos(mv_par02)+"' "+;
	"	LEFT JOIN "+RetSqlName("SA1")+" (NOLOCK) SA12 ON "+; 
	"		Z5_EMPPV1 = SA12.A1_YFILRES AND "+;
	"		SA12.D_E_L_E_T_ = ' ' AND "+;
	"		SA12.A1_FILIAL = ' ' "+;
	"	LEFT JOIN "+RetSqlName("SF2")+" (NOLOCK) SF23 ON "+; 
	"		SF23.F2_FILIAL = Z5_EMPPV3 AND "+;
	"		SF23.F2_SERIE = Z5_SERIE3 AND "+;
	"		SF23.F2_DOC = Z5_NF03 AND "+;
	"		SF23.D_E_L_E_T_ = ' ' AND "+;
	"		Z5_NF03 <> ' ' "+;
	"	LEFT JOIN "+RetSqlName("SC5")+" (NOLOCK) SC53 ON "+; 
	"		SC53.C5_FILIAL = Z5_EMPPV3 AND "+;
	"		SC53.C5_NUM = Z5_PV03 AND "+;
	"		SC53.D_E_L_E_T_ = ' ' AND "+;
	"		Z5_PV03 <> ' ' AND "+;
	"		SF21.F2_EMISSAO BETWEEN '"+dTos(mv_par01)+"' AND '"+dTos(mv_par02)+"' "+;
	"	LEFT JOIN "+RetSqlName("SA1")+" (NOLOCK) SA13 ON "+; 
	"		Z5_CLIENTE+Z5_LOJA = SA13.A1_COD+SA13.A1_LOJA  AND "+;
	"		SA13.D_E_L_E_T_ = ' ' AND "+;
	"		SA13.A1_FILIAL = ' ' "+;
	"	LEFT JOIN "+RetSqlName("SF1")+" (NOLOCK) SF14 ON "+; 
	"		SF14.F1_FILIAL = Z5_EMPPC4 AND "+;
	"		SF14.F1_SERIE = Z5_SERIE4 AND "+;
	"		SF14.F1_DOC = Z5_NF04 AND "+;
	"		SF14.D_E_L_E_T_ = ' ' AND "+;
	"		Z5_NF04 <> ' ' "+;
	"	LEFT JOIN "+RetSqlName("SC7")+" (NOLOCK) SC74 ON "+; 
	"		SC74.C7_FILIAL = Z5_EMPPC4 AND "+;
	"		SC74.C7_NUM = Z5_PC04 AND "+;
	"		SC74.D_E_L_E_T_ = ' ' AND "+;
	"		Z5_PC04 <> ' ' AND "+;
	"		SC74.C7_EMISSAO BETWEEN '"+dTos(mv_par01)+"' AND '"+dTos(mv_par02)+"' "+;
	"	LEFT JOIN "+RetSqlName("SA2")+" (NOLOCK) SA24 ON "+; 
	"		Z5_EMPPV3 = SA24.A2_YFILRES AND "+;
	"		SA24.D_E_L_E_T_ = ' ' AND "+;
	"		SA24.A2_FILIAL = ' ' "+;	
	"	WHERE SZ5.D_E_L_E_T_=' ' AND "+;
	"	SF21.F2_EMISSAO BETWEEN '"+dTos(mv_par01)+"' AND '"+dTos(mv_par02)+"' "

	cQuery+= "GROUP BY "+; 
	"		Z5_EMPPV1, "+;
	"		Z5_EMPPV2, "+;
	"		Z5_EMPPV3, "+;
	"		Z5_EMPPC4, "+;
	"		Z5_PV01, "+;
	"		SC51.C5_NUM ,"+;
	"		SA11.A1_COD+'/'+SA11.A1_LOJA,"+;
	"		SC51.C5_CLIENTE+'/'+SC51.C5_LOJACLI,"+;
	"		SF21.F2_CLIENTE+'/'+SF21.F2_LOJA,"+;
	"		Z5_NF01+'/'+Z5_SERIE1,"+;
	"		SF21.F2_DOC+'/'+SF21.F2_SERIE,"+;
	"		Z5_PV02,"+;
	"		SC52.C5_NUM,"+;
	"		SA12.A1_COD+'/'+SA12.A1_LOJA, "+;
	"		SC52.C5_CLIENTE+'/'+SC52.C5_LOJACLI, "+;
	"		SF22.F2_CLIENTE+'/'+SF22.F2_LOJA, "+;		
	"		Z5_NF02+'/'+Z5_SERIE2,"+;
	"		SF22.F2_DOC+'/'+SF22.F2_SERIE,"+;
	"		Z5_PV03, "+;
	"		SC53.C5_NUM,"+;
	"		SA13.A1_COD+'/'+SA13.A1_LOJA, "+;
	"		SC53.C5_CLIENTE+'/'+SC53.C5_LOJACLI, "+;
	"		SF23.F2_CLIENTE+'/'+SF23.F2_LOJA,"+;		
	"		Z5_NF03+'/'+Z5_SERIE3,"+;
	"		SF23.F2_DOC+'/'+SF23.F2_SERIE,"+;
	"		Z5_PC04, "+;
	"		SC74.C7_NUM , "+;
	"		SA24.A2_COD+'/'+SA24.A2_LOJA, "+;
	"		SC74.C7_FORNECE+'/'+SC74.C7_LOJA,"+;
	"		SF14.F1_FORNECE+'/'+SF14.F1_LOJA,"+;
	"		Z5_NF04+'/'+Z5_SERIE4,"+;
	"		SF14.F1_DOC+'/'+SF14.F1_SERIE,	"+;
	"		SF21.F2_CHVNFE, "+;
	"		SF22.F2_CHVNFE, "+;	
	"		SF23.F2_CHVNFE, "+;		
	"		SF14.F1_CHVNFE  "

Return cQuery