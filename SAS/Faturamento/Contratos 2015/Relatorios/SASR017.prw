#Include 'Protheus.ch'
/*/{Protheus.doc} SASR017
Relat�rio de Auditoria das Devolu��es da Opera��o Triangular
@author Diogo
@since 18/05/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function SASR017()

	Local oReport
	Private cPerg := PadR('SASR017',10)

	CriaSX1(cPerg)
	Pergunte(cPerg,.T.)

	oReport := reportDef()
	oReport:printDialog()
Return

static function reportDef()
	local oReport
	Local oSection1
	Local oSection2
	Local oSection3
	local cTitulo := 'Relat�rio Auditoria Devolu��es'

	oReport := TReport():New('SASR017', cTitulo,cPerg, {|oReport| PrintReport(oReport)},"Relat�rio Auditoria Devolu��es")
	oReport:SetLandscape()
	oReport:SetTotalInLine(.F.)
	oReport:ShowHeader()

	oSection1 	:= TRSection():New(oReport,"AUDITORIA",{"QRY"})
	//oSection1:SetTotalInLine(.T.)

	TRCell():New(oSection1, "CONTRATO"		, "QRY", 'CONTRATO'	,,TamSX3("Z5_CONTRAT")[1]+8,,)
	TRCell():New(oSection1, "MEDICAO"		, "QRY", 'MEDICAO'	,,TamSX3("Z5_MEDICAO")[1]+8,,)
	TRCell():New(oSection1, "CLIENTE"		, "QRY", 'CLIENTE'	,,TamSX3("A1_COD")[1]+8,,)
	TRCell():New(oSection1, "Z5_EMPPV1"		, "QRY", 'FILIAL 1'	,,TamSX3("Z5_EMPPV1")[1]+5,,)
	TRCell():New(oSection1, "Z5_NF01"		, "QRY", 'NF 1  '	,,27,,,,.T.)
	TRCell():New(oSection1, "Z5_EMPPV2"		, "QRY", 'FILIAL 2'	,,TamSX3("Z5_EMPPV2")[1]+5,,)
	TRCell():New(oSection1, "Z5_NF02"		, "QRY", 'NF 2  '	,,27,,)
	TRCell():New(oSection1, "Z5_EMPPV3"		, "QRY", 'FILIAL 3'	,,TamSX3("Z5_EMPPV3")[1]+5,,)
	TRCell():New(oSection1, "Z5_NF03"		, "QRY", 'NF 3  '	,,27,,)
	TRCell():New(oSection1, "Z5_EMPPC4"		, "QRY", 'FILIAL 4'	,,TamSX3("Z5_EMPPC4")[1]+5,,)
	TRCell():New(oSection1, "Z5_NF04"		, "QRY", 'NF 4  '	,,27,,)
	TRCell():New(oSection1, "CMSG1"			, "QRY", 'OCORRENCIAS',,200,,,,.T.)

		
return (oReport)

Static Function PrintReport(oReport)
	Local oSection1 := oReport:Section(1)
	Local cSetSql
	Local nTotQt := 0
	Local nTotPr := 0
	Private _cTpCli
	
	cSetSql := qryDefault()

	cSetSql := ChangeQuery(cSetSql)

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY", .F., .T.)

	DbSelectArea('QRY')

	dbGoTop()
	oReport:SetMeter(QRY->(RecCount()))

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)

	While QRY->(!Eof())

		If oReport:Cancel()
			Exit
		EndIf

		oReport:IncMeter()
		
		cRetErro	:= ""
		cRetErro1	:= ""
		cRetErro2	:= ""
		cRetErro3	:= ""
		cRetErro4	:= ""

			If QRY->CLICONT1 <> QRY->CLINF1 .and. !empty(strtran(QRY->NF1PV,"/",""))
				cRetErro1+= "NOTA 1 - CLIENTE ("+QRY->CLICONT1+chr(13)+chr(10)+;
							") DIFERENTE DA NOTA FISCAL ("+QRY->CLINF1+") "+chr(13)+chr(10)		
			Endif
			If QRY->NF1CONT1 <> QRY->NF1PV .and. !empty(strtran(QRY->NF1CONT1,"/","")) 
				cRetErro1+= "NOTA 1 ("+QRY->NF1CONT1+") DIVERGENTE DA NOTA FISCAL ("+QRY->NF1PV+") "+chr(13)+chr(10)		
			Endif
			If empty(QRY->CHVNF1) .and. !empty(strtran(QRY->NF1PV,"/",""))
				cRetErro1+= "NOTA 1 ("+QRY->NF1CONT1+") N�O FOI TRANSMITIDA "+chr(13)+chr(10)
			Endif

			If QRY->CLICONT2 <> QRY->CLINF2 .and. !empty(strtran(QRY->NF2PV,"/",""))
				cRetErro2+= "NOTA 2 - CLIENTE ("+QRY->CLICONT2+chr(13)+chr(10)+;
							") DIFERENTE DA NOTA FISCAL ("+QRY->CLINF2+") "+chr(13)+chr(10)		
			Endif

			If QRY->NF1CONT2 <> QRY->NF2PV .and. !empty(strtran(QRY->NF1CONT2,"/",""))
				cRetErro2+= "NOTA 2 ("+QRY->NF1CONT2+") DIVERGENTE DA NOTA FISCAL ("+QRY->NF2PV+") "+chr(13)+chr(10)		
			Endif

			If empty(QRY->CHVNF2) .and. !empty(strtran(QRY->NF2PV,"/","")) 
				cRetErro2+= "NOTA 2 ("+QRY->NF1CONT2+") N�O FOI TRANSMITIDA "+chr(13)+chr(10)
			Endif

			If QRY->CLICONT3 <> QRY->CLINF3 .and. !empty(strtran(QRY->NF3PV,"/",""))
				cRetErro3+= "NOTA 3 - CLIENTE ("+QRY->CLICONT3+chr(13)+chr(10)+;
							") DIFERENTE DA NOTA FISCAL ("+QRY->CLINF3+") "+chr(13)+chr(10)		
			Endif

			If QRY->NF1CONT3 <> QRY->NF3PV .and. !empty(strtran(QRY->NF1CONT3,"/",""))
				cRetErro3+= "NOTA 3 ("+QRY->NF1CONT3+") DIVERGENTE DA NOTA FISCAL ("+QRY->NF3PV+") "+chr(13)+chr(10)		
			Endif

			If empty(QRY->CHVNF3) .and. !empty(strtran(QRY->NF3PV,"/",""))
				cRetErro3+= "NOTA 3 ("+QRY->NF1CONT3+") N�O FOI TRANSMITIDA "+chr(13)+chr(10)
			Endif

			
			If QRY->CLICONT4 <> QRY->CLINF4
				cRetErro4+= "NOTA 4 - FORNECEDOR ("+QRY->CLICONT4+chr(13)+chr(10)+;
							") DIFERENTE DA NOTA FISCAL ("+QRY->CLINF4+") "+chr(13)+chr(10)		
			Endif

			If QRY->NF1CONT4 <> QRY->NF4PV .and. !empty(strtran(QRY->NF4PV,"/",""))
				cRetErro4+= "NOTA 4 ("+QRY->NF1CONT4+") DIVERGENTE DA NOTA FISCAL ("+QRY->NF4PV+") "+chr(13)+chr(10)		
			Endif
			
			If empty(cRetErro1+cRetErro2+cRetErro3+cRetErro4) .and.  mv_par03 == 2 //Listar apenas Inconsistentes
				dbSelectArea("QRY")
				QRY->(dbSkip())
				Loop
			Endif

			If !empty(cRetErro1+cRetErro2+cRetErro3+cRetErro4) .and.  mv_par03 == 1 //Listar apenas Consistentes
				dbSelectArea("QRY")
				QRY->(dbSkip())
				Loop
			Endif
			
			If mv_par03 <> 2 //N�o for inconsistente 
				If empty(cRetErro1) .and. !empty(strtran(QRY->NF1PV,"/",""))
					cRetErro1:= "NOTA 1 ENCONTRADA"+chr(13)+chr(10)
				Endif
				
				If empty(cRetErro2) .and. !empty(strtran(QRY->NF2PV,"/",""))
					cRetErro2:= "NOTA 2 ENCONTRADA"+chr(13)+chr(10)
				Endif
	
				If empty(cRetErro3) .and. !empty(strtran(QRY->NF1CONT3,"/",""))
					cRetErro3:= "NOTA 3 ENCONTRADA"+chr(13)+chr(10)
				Endif
	
				
				If empty(cRetErro4) .and. !empty(strtran(QRY->NF1CONT4,"/",""))
					cRetErro4:= "NOTA 4 ENCONTRADA"+chr(13)+chr(10)
				Endif
				
			Endif
			
			If 	!empty(strtran(QRY->NF1PV,"/","")) .or.;
				!empty(strtran(QRY->NF2PV,"/","")) .or.;
				!empty(strtran(QRY->NF3PV,"/","")) .or. ;
				!empty(strtran(QRY->NF4PV,"/",""))


				oSection1:Cell("CONTRATO"):SetValue(QRY->CONTRATO)
				oSection1:Cell("MEDICAO"):SetValue(QRY->MEDICAO)
				oSection1:Cell("CLIENTE"):SetValue(QRY->CLIENTE)

				oSection1:Cell("Z5_EMPPV1"):SetValue(QRY->FIL1)
				oSection1:Cell("Z5_NF01"):SetValue(QRY->NF1CONT1)
			
				oSection1:Cell("Z5_EMPPV2"):SetValue(QRY->FIL2)
				oSection1:Cell("Z5_NF02"):SetValue(QRY->NF1CONT2)
			
				oSection1:Cell("Z5_EMPPV3"):SetValue(QRY->FIL3)
				oSection1:Cell("Z5_NF03"):SetValue(QRY->NF1CONT3)
			
						
				oSection1:Cell("Z5_EMPPC4"):SetValue(QRY->FIL4)
				oSection1:Cell("Z5_NF04"):SetValue(QRY->NF1CONT4)
				
	
				oSection1:Cell("CMSG1"):SetValue(cRetErro1+cRetErro2+cRetErro3+cRetErro4)
	
			oSection1:PrintLine()
			oReport:SkipLine()
			
			Endif
		
		dbSelectArea("QRY")
		QRY->(dbSkip())

	EndDo

	QRY->(dbCloseArea())

Return

static function CriaSX1(cPerg)

	PutSx1(cPerg, "01","Periodo De" 	,"Periodo De"		,"Periodo De"	,"mv_ch01","D",08,0,2,"G","" ,"","","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Periodo Ate" 	,"Periodo Ate"		,"Periodo Ate"	,"mv_ch02","D",08,0,2,"G","" ,"","","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","Inconsistentes" ,"Inconsistentes","Inconsistentes"	,"mv_ch03","C",01,0,1,"C","",""     ,"","","mv_par03","N�o","N�o","N�o","","Sim","Sim","Sim","Todos","Todos","Todos","","","","","")

Return

//Query do Relat�rio
Static function qryDefault()

	Local cQuery := ""
	
	cQuery:=" SELECT "+chr(13)+chr(10)+;			
			" Z8_CONTRAT CONTRATO,"+chr(13)+chr(10)+;
			" Z8_MEDICAO MEDICAO,"+chr(13)+chr(10)+;
			" Z8_CLIENTE CLIENTE,"+chr(13)+chr(10)+;
			" Z8_LOJA LOJA,"+chr(13)+chr(10)+;
			" Z8_EMP01 FIL1,"+chr(13)+chr(10)+;		
			"  SA11.A1_COD+'/'+SA11.A1_LOJA CLICONT1,"+chr(13)+chr(10)+;		
 			" SF11.F1_FORNECE+'/'+SF11.F1_LOJA CLINF1,"+chr(13)+chr(10)+;
 			" Z8_NF01+'/'+Z8_SER01 NF1CONT1,"+chr(13)+chr(10)+;		
 			" SF11.F1_DOC+'/'+SF11.F1_SERIE NF1PV,"+chr(13)+chr(10)+;		
 			" SF11.F1_CHVNFE CHVNF1,"+chr(13)+chr(10)+;		
			"  Z8_EMPMTZ FIL2,"+chr(13)+chr(10)+;		
 			" SA22.A2_COD+'/'+SA22.A2_LOJA CLICONT2,"+chr(13)+chr(10)+;		
 			" SF22.F2_CLIENTE+'/'+SF22.F2_LOJA CLINF2,"+chr(13)+chr(10)+;
 			" Z8_NF02+'/'+Z8_SER02 NF1CONT2,"+chr(13)+chr(10)+;		
 			" SF22.F2_DOC+'/'+SF22.F2_SERIE NF2PV,"+chr(13)+chr(10)+;		
 			" SF22.F2_CHVNFE CHVNF2,"+chr(13)+chr(10)+;		
			"  Z8_EMPMTZ FIL3,"+chr(13)+chr(10)+;		
 			" SA13.A1_COD+'/'+SA13.A1_LOJA CLICONT3,"+chr(13)+chr(10)+;		
 			" SF13.F1_FORNECE+'/'+SF13.F1_LOJA CLINF3,"+chr(13)+chr(10)+;
 			" Z8_NFMTZ+'/'+Z8_SERMTZ NF1CONT3,"+chr(13)+chr(10)+;		
 			" SF13.F1_DOC+'/'+SF13.F1_SERIE NF3PV,"+chr(13)+chr(10)+;		
 			" SF13.F1_CHVNFE CHVNF3, "+chr(13)+chr(10)+;
			"  Z8_EMPMTZ FIL4,"+chr(13)+chr(10)+;		
 			" SA14.A1_COD+'/'+SA14.A1_LOJA CLICONT4,"+chr(13)+chr(10)+;		
 			" SF14.F1_FORNECE+'/'+SF14.F1_LOJA CLINF4,"+chr(13)+chr(10)+;
 			" Z8_NF02+'/'+Z8_SER02 NF1CONT4,"+chr(13)+chr(10)+;		
 			" SF14.F1_DOC+'/'+SF14.F1_SERIE NF4PV,"+chr(13)+chr(10)+;		
 			" SF14.F1_CHVNFE CHVNF4 "+chr(13)+chr(10)+;		
			"  FROM "+RetSqlName("SZ8")+" (NOLOCK) SZ8"+chr(13)+chr(10)+;	
 			" LEFT JOIN "+RetSqlName("SF1")+" (NOLOCK) SF11"+chr(13)+chr(10)+;  //NOTA 01
 			" ON"+chr(13)+chr(10)+; 		
 			" SF11.F1_FILIAL = Z8_EMP01 AND"+chr(13)+chr(10)+; 		
 			" SF11.F1_SERIE = Z8_SER01 AND"+chr(13)+chr(10)+; 		
 			" SF11.F1_DOC = Z8_NF01 AND"+chr(13)+chr(10)+; 		
 			" SF11.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+; 
 			" SF11.F1_TIPO = 'D' AND"+chr(13)+chr(10)+;  
			"  Z8_NF01 <> ' '"+chr(13)+chr(10)+; 	
 			" LEFT JOIN "+RetSqlName("SA1")+" (NOLOCK) SA11"+chr(13)+chr(10)+; 
 			" ON"+chr(13)+chr(10)+; 		
 			" Z8_CLIENTE+Z8_LOJA = SA11.A1_COD+SA11.A1_LOJA  AND"+chr(13)+chr(10)+; 		
 			" SA11.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+; 	
			"  SA11.A1_FILIAL = ' '"+chr(13)+chr(10)+; 	
			"  LEFT JOIN "+RetSqlName("SF2")+" (NOLOCK) SF22"+chr(13)+chr(10)+; //NOTA 02
 			" ON"+chr(13)+chr(10)+; 		
 			" SF22.F2_FILIAL = Z8_EMP01 AND"+chr(13)+chr(10)+; 		
 			" SF22.F2_SERIE = Z8_SER02 AND"+chr(13)+chr(10)+; 		
 			" SF22.F2_DOC = Z8_NF02 AND"+chr(13)+chr(10)+; 		
 			" SF22.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+; 	
 			" SF22.F2_TIPO = 'D' AND"+chr(13)+chr(10)+; 
			"  Z8_NF02 <> ' '"+chr(13)+chr(10)+;	
 			"  LEFT JOIN "+RetSqlName("SA2")+" (NOLOCK) SA22"+chr(13)+chr(10)+;
 			" ON"+chr(13)+chr(10)+; 		
 			" SA22.A2_COD+SA22.A2_LOJA = SF22.F2_CLIENTE+SF22.F2_LOJA  AND"+chr(13)+chr(10)+; 		
 			" SA22.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+; 	
 			" SA22.A2_FILIAL = ' '"+chr(13)+chr(10)+; 	
 			" LEFT JOIN "+RetSqlName("SF1")+" (NOLOCK) SF13"+chr(13)+chr(10)+; //NOTA 03
 			" ON"+chr(13)+chr(10)+; 		
 			" SF13.F1_FILIAL = Z8_EMPMTZ AND"+chr(13)+chr(10)+; 		
 			" SF13.F1_SERIE = Z8_SERMTZ AND"+chr(13)+chr(10)+; 		
 			" SF13.F1_DOC = Z8_NFMTZ AND"+chr(13)+chr(10)+; 		
 			" SF13.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+; 
 			" SF13.F1_TIPO = 'D' AND"+chr(13)+chr(10)+;  
 			" Z8_NFMTZ <> ' '"+chr(13)+chr(10)+;	
 			"  LEFT JOIN "+RetSqlName("SA1")+" (NOLOCK) SA13"+chr(13)+chr(10)+;
 			" ON"+chr(13)+chr(10)+; 		
 			" SA13.A1_COD+SA13.A1_LOJA = SF13.F1_FORNECE+SF13.F1_LOJA  AND"+chr(13)+chr(10)+; 		
 			" SA13.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+; 	
 			" SA13.A1_FILIAL = ' '"+chr(13)+chr(10)+; 	
 			" LEFT JOIN "+RetSqlName("SF1")+" (NOLOCK) SF14"+chr(13)+chr(10)+; //NOTA 04
 			" ON"+chr(13)+chr(10)+; 		
 			" SF14.F1_FILIAL = Z8_EMPMTZ AND"+chr(13)+chr(10)+; 		
 			" SF14.F1_SERIE = Z8_SER02 AND"+chr(13)+chr(10)+; 		
 			" SF14.F1_DOC = Z8_NF02 AND"+chr(13)+chr(10)+; 		
 			" SF14.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+; 		
 			" SF14.F1_TIPO = 'D' AND"+chr(13)+chr(10)+;  
 			" Z8_NF02 <> ' '"+chr(13)+chr(10)+;	
 			"  LEFT JOIN "+RetSqlName("SA1")+" (NOLOCK) SA14"+chr(13)+chr(10)+;
 			" ON"+chr(13)+chr(10)+; 		
 			" SA14.A1_COD+SA14.A1_LOJA = SF14.F1_FORNECE+SF14.F1_LOJA  AND"+chr(13)+chr(10)+; 		
 			" SA14.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+; 	
 			" SA14.A1_FILIAL = ' '"+chr(13)+chr(10)+; 	
			"  WHERE "+chr(13)+chr(10)+;
 			" SZ8.D_E_L_E_T_=' '  "+chr(13)+chr(10)+;
 			" ORDER BY Z8_CONTRAT,Z8_MEDICAO,Z8_CLIENTE "
 	
Return cQuery