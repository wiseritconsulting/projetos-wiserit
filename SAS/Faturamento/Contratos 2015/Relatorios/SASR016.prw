#Include 'Protheus.ch'
/*/{Protheus.doc} SASR016
Relat�rio Devolu��o sem Contrato
@author Diogo
@since 18/05/2016
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function SASR016()

	Local oReport
	Private cPerg := PadR('SSAR016',10)

	CriaSX1(cPerg)
	Pergunte(cPerg,.T.)

	oReport := reportDef()
	oReport:printDialog()
Return

static function reportDef()
	local oReport
	Local oSection1
	Local oSection2
	Local oSection3
	local cTitulo := 'Relat�rio Devolu��o Sem Contrato'

	oReport := TReport():New('SASR016', cTitulo,cPerg, {|oReport| PrintReport(oReport)},"Relat�rio Devolu��o sem Contrato")
	oReport:SetLandscape()
	oReport:SetTotalInLine(.F.)
	oReport:ShowHeader()

	oSection1 	:= TRSection():New(oReport,"AUDITORIA",{"QRY15"})

	TRCell():New(oSection1, "Z8_NF"			, "QRY15", 'NF-e'			,,27,,)
	TRCell():New(oSection1, "F2_EMISSAO"	, "QRY15", 'EMISSAO  '		,,TamSX3("F2_EMISSAO")[1]+3,,,,.T.)
	TRCell():New(oSection1, "CLIENTE"		, "QRY15", 'CLIENTE'		,,15,,)
	TRCell():New(oSection1, "RAZAO"			, "QRY15", 'RAZAO SOCIAL'	,,TamSX3("A1_NOME")[1]+5,,)
	TRCell():New(oSection1, "VALOR"			, "QRY15", 'VALOR TOTAL'			,"@E 99,99,999,999.99",GetSX3Cache("E2_VALOR","X3_TAMANHO"),,)


	oSection2 	:= TRSection():New(oReport,"NOTAS",{"QRY15"})

	TRCell():New(oSection2, "Z8_NF"			, "QRY15", 'NF-e'		,,27,,)
	TRCell():New(oSection2 , "NFD"			, "QRY15", 'NFD'		,,27,,)
	TRCell():New(oSection2 , "EMISSAO"		, "QRY15", 'EMISSAO'	,,TamSX3("F2_EMISSAO")[1]+3,,,,.T.)
	TRCell():New(oSection2 , "PRODUTO"		, "QRY15", 'PRODUTO'	,,TamSX3("B1_COD")[1]+1,,)
	TRCell():New(oSection2 , "DESCRICAO"	, "QRY15", 'DESCRICAO'	,,TamSX3("B1_DESC")[1]+1,,)
	TRCell():New(oSection2 , "QUANTIDADE"	, "QRY15", 'QUANTIDADE'	,"@E 999,999.99",TamSX3("D2_QUANT")[1]+4,,)
	TRCell():New(oSection2 , "VALOR"		, "QRY15", 'VALOR UNITARIO'		,"@E 99,99,999,999.99",TamSX3("D2_PRCVEN")[1]+4,,)
	TRCell():New(oSection2 , "TOTAL"		, "QRY15", 'VALOR TOTAL'		,"@E 99,99,999,999.99",TamSX3("D2_TOTAL")[1]+4,,)
	TRCell():New(oSection2 , "STATUS"		, "QRY15", 'STATUS'		,,45,,)
		
return (oReport)

Static Function PrintReport(oReport)
	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cSetSql
	Local nTotQt := 0
	Local nTotPr := 0
	Private _cTpCli

	//Notas Fiscais sem Contratos	
	FQryNFAvuls()
	If QRY15CTN->(!eof())
		oReport:PrintText("......NOTA FISCAL SEM CONTRATO......")
	Endif	

	dbGoTop()
	oReport:SetMeter(QRY15CTN->(RecCount()))
		
	while QRY15CTN->(!eof())

		If oReport:Cancel()
			Exit
		EndIf

		oReport:IncMeter()		

		oSection1:Init()
		oSection1:SetHeaderSection(.T.)
		
		oSection1:Cell("Z8_NF"):SetValue(QRY15CTN->DOCUMENTO)
		oSection1:Cell("F2_EMISSAO"):SetValue(stod(QRY15CTN->D1_EMISSAO))
		oSection1:Cell("CLIENTE"):SetValue(QRY15CTN->CLIENTE)
		oSection1:Cell("RAZAO"):SetValue(QRY15CTN->NOMECLI)
		oSection1:Cell("VALOR"):SetValue(QRY15CTN->VALOR)
		oSection1:Cell("VALOR"):SetPicture("@E 99,99,999,999.99")
		cStatus	:= 'Sem Contrato'
		cNF		:= QRY15CTN->DOCUMENTO
		oSection1:PrintLine()
		oSection1:Finish()
		
		oSection2:Init()
		FQueryDev(QRY15CTN->DOCUMENTO,QRY15CTN->D1_FILIAL,'1') //Itens da devolu��o
		
		while QRY15ITEM->(!eof())
			
			oSection2:Cell("Z8_NF"):SetValue(cNF)
			oSection2:Cell("NFD"):SetValue(QRY15ITEM->NOTA)
			oSection2:Cell("EMISSAO"):SetValue(stod(QRY15ITEM->EMISSAO))
			oSection2:Cell("PRODUTO"):SetValue(QRY15ITEM->PRODUTO)
			oSection2:Cell("DESCRICAO"):SetValue(QRY15ITEM->DESCRICAO)
			oSection2:Cell("QUANTIDADE"):SetValue(QRY15ITEM->QUANTIDADE)
			oSection2:Cell("VALOR"):SetValue(QRY15ITEM->VALOR)
			oSection2:Cell("STATUS"):SetValue(cStatus)

			oSection2:PrintLine()
			QRY15ITEM->(dbSkip())
		enddo	
		QRY15ITEM->(dbCloseArea())
		oSection2:Finish()		
		oReport:SkipLine()			
		dbSelectArea("QRY15CTN")
		QRY15CTN->(dbSkip())
	enddo 
	
	QRY15CTN->(dbCloseArea())

Return

Static function CriaSX1(cPerg)

	PutSx1(cPerg, "01","Cliente De" 	,"Cliente Ate"		,"Cliente De"	,"mv_ch01","C",GetSx3Cache("Z8_CLIENTE","X3_TAMANHO"),0,2,"G","" ,"SA1","","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Cliente Ate" 	,"Cliente Ate"		,"Cliente Ate"	,"mv_ch02","C",GetSx3Cache("Z8_CLIENTE","X3_TAMANHO"),0,2,"G","" ,"SA1","","","mv_par02"," ","","","","","","","","","","","","","","","")

Return


/*/{Protheus.doc} FQueryDev
Fun��o que retorna as Notas de Devolu��es
@author Diogo
@since 19/05/2016
@version 1.0
@param cNota, character, (C�digo da nota fiscal de origem)
@param cFil, character, (C�digo da filial da NF)
@param cTipo, character, (Tipo da Nota)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function FQueryDev(cNota,cFil,cTipo)

	Local cQuery := ""
	If cTipo $ '1_3_4' //Nota de devolu��o na Entrada

		cQuery:= 	" SELECT SD1.D1_DOC+'/'+SD1.D1_SERIE NOTA, "+chr(13)+chr(10)+;
					" 		SD1.D1_EMISSAO EMISSAO,"+chr(13)+chr(10)+;
					"		SD1.D1_COD PRODUTO,"+chr(13)+chr(10)+;
					"		SB1.B1_DESC DESCRICAO,"+chr(13)+chr(10)+;
					"		SD1.D1_QUANT QUANTIDADE,"+chr(13)+chr(10)+;
					"		SD1.D1_VUNIT VALOR,"+chr(13)+chr(10)+;
					"		SD1.D1_TOTAL TOTAL"+chr(13)+chr(10)+;
					"	FROM "+RetSqlName("SD1")+" (NOLOCK) SD1"+chr(13)+chr(10)+;
					"	JOIN "+RetSqlName("SB1")+" (NOLOCK) SB1 "+chr(13)+chr(10)+;
					"	ON B1_COD = D1_COD"+chr(13)+chr(10)+;
					"	AND SB1.D_E_L_E_T_=' '"+chr(13)+chr(10)+;
					"	WHERE  D1_NFORI+'/'+D1_SERIORI ='"+cNota+"' "+chr(13)+chr(10)+;
					"	AND D1_FILIAL ='"+cFil+"' "+chr(13)+chr(10)+;
					" AND NOT EXISTS ( "+chr(13)+chr(10)+;
					" SELECT 1"+chr(13)+chr(10)+;
	 				" FROM "+RetSqlName("SZ8")+" (NOLOCK) SZ8 "+chr(13)+chr(10)+;  
	 				" WHERE Z8_EMP01 = D1_FILIAL"+chr(13)+chr(10)+;
	 				" AND SZ8.D_E_L_E_T_ =' ' "+chr(13)+chr(10)+;
					" AND Z8_CLIENTE = D1_FORNECE"+chr(13)+chr(10)+;
					" AND Z8_NF01 = D1_DOC "+chr(13)+chr(10)+;
					" AND Z8_SER01 = D1_SERIE "+chr(13)+chr(10)+;
					"    )"+chr(13)+chr(10)+;
					" AND NOT EXISTS ("+chr(13)+chr(10)+;
					" SELECT 1"+chr(13)+chr(10)+;
					"  FROM "+RetSqlname("SZ8")+" (NOLOCK) SZ8 "+chr(13)+chr(10)+;  
					"  WHERE Z8_EMPMTZ = D1_FILIAL"+chr(13)+chr(10)+;
					" AND SZ8.D_E_L_E_T_ =' ' "+chr(13)+chr(10)+;
					" AND Z8_CLIENTE = D1_FORNECE"+chr(13)+chr(10)+;
					" AND Z8_NFMTZ = D1_DOC"+chr(13)+chr(10)+; 
					" AND Z8_SERMTZ = D1_SERIE "+chr(13)+chr(10)+;
					"    ) "+chr(13)+chr(10)+;
					" AND NOT EXISTS ("+chr(13)+chr(10)+;
					" SELECT 1"+chr(13)+chr(10)+;
					"  FROM "+RetSqlname("SZ8")+" (NOLOCK) SZ8 "+chr(13)+chr(10)+;  
					"  WHERE Z8_EMPMTZ = D1_FILIAL"+chr(13)+chr(10)+;
					" AND SZ8.D_E_L_E_T_ =' ' "+chr(13)+chr(10)+;
					" AND Z8_NF02 = D1_DOC"+chr(13)+chr(10)+; 
					" AND Z8_SER02 = D1_SERIE "+chr(13)+chr(10)+;
					"    ) "+chr(13)+chr(10)+;										
					"	AND SD1.D_E_L_E_T_ =' ' "
					
	Else
	
		cQuery:= 	" SELECT SD2.D2_DOC+'/'+SD2.D2_SERIE NOTA, "+chr(13)+chr(10)+;
					" 		SD2.D2_EMISSAO EMISSAO,"+chr(13)+chr(10)+;
					"		SD2.D2_COD PRODUTO,"+chr(13)+chr(10)+;
					"		SB1.B1_DESC DESCRICAO,"+chr(13)+chr(10)+;
					"		SD2.D2_QUANT QUANTIDADE,"+chr(13)+chr(10)+;
					"		SD2.D2_PRCVEN VALOR,"+chr(13)+chr(10)+;
					"		SD2.D2_TOTAL TOTAL"+chr(13)+chr(10)+;
					"	FROM "+RetSqlName("SD2")+" (NOLOCK) SD2"+chr(13)+chr(10)+;
					"	JOIN "+RetSqlName("SB1")+" (NOLOCK) SB1 "+chr(13)+chr(10)+;
					"	ON B1_COD = D2_COD"+chr(13)+chr(10)+;
					"	AND SB1.D_E_L_E_T_=' '"+chr(13)+chr(10)+;
					"	WHERE  D2_NFORI+'/'+D2_SERIORI ='"+cNota+"' "+chr(13)+chr(10)+;
					"	AND D2_FILIAL ='"+cFil+"' "+chr(13)+chr(10)+;
					"	AND SD2.D_E_L_E_T_ =' ' "	
	Endif

	cSetSql := ChangeQuery(cQuery)

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY15ITEM", .F., .T.)

	DbSelectArea('QRY15ITEM')

	QRY15ITEM->(dbGoTop())
					
Return


/*/{Protheus.doc} FQryNFAvuls
Notas Fiscais Avulsas
@author Diogo
@since 19/05/2015
@version 1.0
@param cFornece, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function FQryNFAvuls()

	Local cQuery:= ""
	cQuery:= 	" SELECT D1_FILIAL, D1_DOC+'/'+D1_SERIE,D1_EMISSAO, "+chr(13)+chr(10)+;
				" D1_NFORI+'/'+D1_SERIORI DOCUMENTO, "+chr(13)+chr(10)+;
				" A1_COD+'/'+A1_LOJA CLIENTE,"+chr(13)+chr(10)+;
				" A1_NOME NOMECLI,			 "+chr(13)+chr(10)+;
				" SUM(D1_TOTAL) VALOR		 "+chr(13)+chr(10)+; 
				" FROM "+RetSqlName("SD1")+" (NOLOCK) SD1 "+chr(13)+chr(10)+;
				" JOIN "+RetSqlName("SA1")+" (NOLOCK) SA1"+chr(13)+chr(10)+; 
				" ON SA1.A1_COD = SD1.D1_FORNECE AND"+chr(13)+chr(10)+;
				" SA1.A1_LOJA = SD1.D1_LOJA "+chr(13)+chr(10)+;
				" WHERE SD1.D1_TIPO ='D' "+chr(13)+chr(10)+;
				" AND SD1.D_E_L_E_T_ =' '  "+chr(13)+chr(10)+;
				" AND SD1.D1_FORNECE BETWEEN '"+mv_par01+"' AND '"+mv_par02+"' "+chr(13)+chr(10)+;
				" AND NOT EXISTS ( "+chr(13)+chr(10)+;
				" SELECT 1"+chr(13)+chr(10)+;
 				" FROM "+RetSqlName("SZ8")+" (NOLOCK) SZ8 "+chr(13)+chr(10)+;  
 				" WHERE Z8_EMP01 = D1_FILIAL"+chr(13)+chr(10)+;
 				" AND SZ8.D_E_L_E_T_ =' ' "+chr(13)+chr(10)+;
				" AND Z8_CLIENTE = D1_FORNECE"+chr(13)+chr(10)+;
				" AND Z8_NF01 = D1_DOC "+chr(13)+chr(10)+;
				" AND Z8_SER01 = D1_SERIE "+chr(13)+chr(10)+;
				"    )"+chr(13)+chr(10)+;
				" AND NOT EXISTS ("+chr(13)+chr(10)+;
				" SELECT 1"+chr(13)+chr(10)+;
				"  FROM "+RetSqlname("SZ8")+" (NOLOCK) SZ8 "+chr(13)+chr(10)+;  
				"  WHERE Z8_EMPMTZ = D1_FILIAL"+chr(13)+chr(10)+;
				" AND SZ8.D_E_L_E_T_ =' ' "+chr(13)+chr(10)+;
				" AND Z8_CLIENTE = D1_FORNECE"+chr(13)+chr(10)+;
				" AND Z8_NFMTZ = D1_DOC"+chr(13)+chr(10)+; 
				" AND Z8_SERMTZ = D1_SERIE "+chr(13)+chr(10)+;
				"    ) "+chr(13)+chr(10)+;
				" AND NOT EXISTS ("+chr(13)+chr(10)+;
				" SELECT 1"+chr(13)+chr(10)+;
				"  FROM "+RetSqlname("SZ8")+" (NOLOCK) SZ8 "+chr(13)+chr(10)+;  
				"  WHERE Z8_EMPMTZ = D1_FILIAL"+chr(13)+chr(10)+;
				" AND SZ8.D_E_L_E_T_ =' ' "+chr(13)+chr(10)+;
				" AND Z8_NF02 = D1_DOC"+chr(13)+chr(10)+; 
				" AND Z8_SER02 = D1_SERIE "+chr(13)+chr(10)+;
				"    ) "+chr(13)+chr(10)+;
				" GROUP BY D1_FILIAL, D1_DOC+'/'+D1_SERIE,D1_EMISSAO,D1_NFORI+'/'+D1_SERIORI,D1_SERIE, "+chr(13)+chr(10)+;
				" A1_COD+'/'+A1_LOJA, A1_NOME " 

	cSetSql := ChangeQuery(cQuery)

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY15CTN", .F., .T.)

	DbSelectArea('QRY15CTN')

	QRY15CTN->(dbGoTop())
			
Return