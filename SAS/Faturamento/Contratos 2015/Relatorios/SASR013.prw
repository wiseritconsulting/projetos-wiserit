#Include 'Protheus.ch'
#Include 'TopConn.ch'

//_____________________________________________________________________________
/*/{Protheus.doc} OSBQCTR
Relatorio de listagens de itens por contrato

@author HELIOMAR JUNIOR
@since 10 de ABRIL de 2014
@version P11
/*/
//_____________________________________________________________________________

User Function SASR013()

Local aRegs			:= {}
Private oReport
Private cPergCont	:= 'SASR013'
Private _astru		:={}
************************
*Monta pergunte do Log *
************************
AjustaSX1()

If Pergunte( cPergCONT, .T. )    
	Processa( { || ProcCont() }, 'Processando....' )
EndIf



Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ProcCont
Carrega os dados conforme parametrizacao feita pelo usuario;

@author HELIOMAR JUNIOR
@since 10 de ABRIL de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ProcCont()
Local cQuery	:= ""
Local aPagto    := {}

BeginSql Alias "TMP"

SELECT  F2_DOC			AS NF01,  
        Z5_NF03		    AS NF03,
		F2_SERIE		AS SERIE,
		F2_EMISSAO		AS DT_EMISSAO_NF01,
		Z5_EMPPV1       AS FILIAL,
		A1_CGC          AS CNPJ,
		A1_COD          AS COD_CLI,
		A1_NREDUZ       AS FANTASIA,		
		A1_NOME         AS RAZAO,
		A1_END          AS ENDERECO,
		A1_EST          AS UF,
		A1_MUN          AS CIDADE,
		A1_BAIRRO       AS BAIRRO,
		A1_CEP          AS CEP,
		D2_TES			AS TES,
        Z5_PV01         AS OS1, 
		C5_EMISSAO      AS DT_EMISSAO_OS,
		D2_COD          AS COD_PROD,
		B1_DESC         AS PROD,
		D2_QUANT        AS QTDE,
		A4_COD          AS COD_TRANSP,
		A4_NOME         AS TRANSP,		
		C5_SUGENT		AS DT_SUGERIDA,	
		CONVERT(DECIMAL(10,2), D2_VALFRE) AS FRETE
		
FROM SZ5010 A
JOIN SA1010 B ON(B.A1_COD = A.Z5_CLIENTE AND A.%NOTDEL%  )
JOIN SD2010 C ON( D2_DOC=Z5_NF01 AND D2_FILIAL=Z5_EMPPV1 AND C.%NOTDEL%  )
JOIN SB1010 D ON(C.D2_COD = D.B1_COD)
JOIN SF2010 E ON(F2_DOC=Z5_NF01 AND F2_FILIAL=Z5_EMPPV1 AND E.%NOTDEL%  )
JOIN SC5010 F ON(C5_NOTA=Z5_NF01 AND C5_FILIAL=Z5_EMPPV1 AND F.%NOTDEL%  )  
JOIN SA4010 G ON(G.A4_COD = F2_TRANSP AND G.%NOTDEL%  )
WHERE D2_TES NOT IN('013','014','015','016','503','504','512','996','997')
AND F2_EMISSAO BETWEEN  %EXP:MV_PAR01% AND %EXP:MV_PAR02%
AND C5_FILIAL=%EXP:MV_PAR03%

UNION 


SELECT  F2_DOC			AS NF01,  
        ''	            AS NF03,
		F2_SERIE		AS SERIE,
		F2_EMISSAO		AS DT_EMISSAO_NF01,
		C5_FILIAL       AS FILIAL,
		A1_CGC          AS CNPJ,
		A1_COD          AS COD_CLI,
		A1_NREDUZ       AS FANTASIA,		
		A1_NOME         AS RAZAO,
		A1_END          AS ENDERECO,
		A1_EST          AS UF,
		A1_MUN          AS CIDADE,
		A1_BAIRRO       AS BAIRRO,
		A1_CEP          AS CEP,
		D2_TES			AS TES,
        C5_NUM          AS OS1, 
		C5_EMISSAO      AS DT_EMISSAO_OS,
		D2_COD          AS COD_PROD,
		B1_DESC         AS PROD,
		D2_QUANT        AS QTDE,
		A4_COD          AS COD_TRANSP,
		A4_NOME         AS TRANSP,		
		C5_SUGENT		AS DT_SUGERIDA,	
		CONVERT(DECIMAL(10,2), D2_VALFRE) AS FRETE
		
FROM SC5010 A
JOIN SA1010 B ON(B.A1_COD = A.C5_CLIENTE AND A.%NOTDEL%  )
JOIN SD2010 C ON( D2_DOC=C5_NOTA AND D2_FILIAL=C5_FILIAL AND C.%NOTDEL%  )
JOIN SB1010 D ON(C.D2_COD = D.B1_COD)
JOIN SF2010 E ON(F2_DOC=C5_NOTA AND F2_FILIAL=C5_FILIAL AND E.%NOTDEL%  ) 
JOIN SA4010 G ON(G.A4_COD = F2_TRANSP AND G.%NOTDEL%  )
WHERE D2_TES NOT IN('013','014','015','016','503','504','512','996','997')
AND F2_EMISSAO BETWEEN  %EXP:MV_PAR01% AND %EXP:MV_PAR02%
AND C5_FILIAL=%EXP:MV_PAR03%
AND NOT EXISTS (SELECT 1 
                  FROM SZ5010  Z5
				  WHERE A.C5_NUM   = Z5.Z5_PV01
				  AND  A.C5_FILIAL = Z5.Z5_EMPPV1
				  AND  A.C5_NOTA   = Z5.Z5_NF01)

  
EndSql


oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
TMP->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author HELIOMAR JUNIOR
@since 10 de ABRIL de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportDef()
Local oFirst
Local nOrd	:= 1


oReport := TReport():New( 'TMP', 'Controle de Expedi��o', cPergCont, {|oReport| ReportPrint( oReport ), 'Controle de Expedi��o' } )
oReport:SetLandScape()
oReport:lParamReadOnly := .T.

oReport:SetTotalInLine( .F. )

oFirst := TRSection():New( oReport, 'Controle de Expedi��o', { 'TMP', 'SZ5', 'SC5', 'SA1','SD2','SB1','SF2','SA4' },,, )

oFirst:SetTotalInLine( .F. )


TRCell():New( oFirst, 'Nota1'		    ,'TMP', 'Nota1',			    "@!"						    ,09,,							{ || TMP->NF01 				       } )
TRCell():New( oFirst, 'Nota3'	       	,'TMP', 'Nota3' ,				"@!"							,09,,							{ || TMP->NF03  				   } )
TRCell():New( oFirst, 'Serie'	       	,'TMP', 'Serie' ,				"@!"							,01,,							{ || TMP->SERIE					   } )
TRCell():New( oFirst, 'Dt. Emissao'   	,'TMP', 'Dt. Emissao',			"@!"						  	,10,,							{ || STOD(TMP->DT_EMISSAO_NF01)	   } )
TRCell():New( oFirst, 'Filial'	   		,'TMP', 'Filial',				"@!"  							,06,,							{ || TMP->FILIAL				   } )
TRCell():New( oFirst, 'CNPJ'   		    ,'TMP', 'CNPJ' ,			    "@!"							,16,,							{ || TMP->CNPJ	    		       } ) 
TRCell():New( oFirst, 'Cliente'	   		,'TMP', 'Cliente',				"@!"  							,06,,							{ || TMP->COD_CLI	    		   } )
TRCell():New( oFirst, 'Fantasia'	   	,'TMP', 'Fantasia',				"@!" 						  	,30,,							{ || TMP->FANTASIA 	               } )
TRCell():New( oFirst, 'R. Social'	    ,'TMP', 'R. Social' ,	        "@!"							,30,,							{ || TMP->RAZAO		               } )
TRCell():New( oFirst, 'Endereco'		,'TMP', 'Endereco' ,	        "@!"							,35,,							{ || TMP->ENDERECO		           } )
TRCell():New( oFirst, 'Uf'			    ,'TMP', 'Uf',				    "@!"							,02,,							{ || TMP->UF				       } )
TRCell():New( oFirst, 'Cidade'		    ,'TMP', 'Cidade',			    "@!"							,30,,							{ || TMP->CIDADE      			   } )
TRCell():New( oFirst, 'Bairro'	        ,'TMP', 'Bairro',		        "@!"							,30,,							{ || TMP->BAIRRO				   } )
TRCell():New( oFirst, 'Cep'			    ,'TMP', 'Cep',					"@!"							,10,,							{ || TMP->CEP					   } )
TRCell():New( oFirst, 'TES'		        ,'TMP', 'TES',				    "@!"							,03,,							{ || TMP->TES				       } )
TRCell():New( oFirst, 'OS'		        ,'TMP', 'OS',			        "@!"							,09,,							{ || TMP->OS1                      } )
TRCell():New( oFirst, 'Dt. OS'		    ,'TMP', 'Dt. OS',			    ""							    ,10,,							{ || STOD(TMP->DT_EMISSAO_OS)      } )
TRCell():New( oFirst, 'Cod. Produto'    ,'TMP', 'Cod. Produto',			"@!"							,09,,							{ || TMP->COD_PROD			       } )
TRCell():New( oFirst, 'Produto'		    ,'TMP', 'Produto',			    "@!"							,30,,							{ || TMP->PROD				       } )
TRCell():New( oFirst, 'Quant.'	 	    ,'TMP', 'Quant.',			    "@E 999,999.99"					,08,,							{ || TMP->QTDE			           } )
TRCell():New( oFirst, 'Cod. Transp'		,'TMP', 'Cod. Transp',			"@!"							,05,,							{ || TMP->COD_TRANSP			   } )
TRCell():New( oFirst, 'Transp'		    ,'TMP', 'Transp',			    "@!"							,25,,							{ || TMP->TRANSP				   } )
TRCell():New( oFirst, 'Dt. Sugerida'	,'TMP', 'Dt. Sugerida',			" "							    ,10,,							{ || STOD(TMP->DT_SUGERIDA)		   } )
TRCell():New( oFirst, 'Frete'			,'TMP', 'Frete',				"@E 999,999.99"					,08,,							{ || TMP->FRETE				       } ) 

//oBreakT := TRBreak():New( oFirst, { || }, 'Vlr. Total' )

//TRFunction():New( oFirst:Cell( 'ValFat2' ),	'', 'SUM', oBreakT,,,, .F., .F. )

//oFirst:Cell( 'TOTAL' ):SetHeaderAlign( 'RIGHT' )

//oFirst:SetColSpace(9)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author HELIOMAR JUNIOR
@since 10 de ABRIL de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

MakeAdvplExpr( cPergCont )
oReport:Section(1):Enable()

dbSelectArea( 'TMP' )
oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author HELIOMAR JUNIOR
@since 10 de ABRIL de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function AjustaSX1()
Local aHelp	:= {}
Local aArea	:= GetArea()

SX1->( dbSetOrder(1) )

If !SX1->( dbSeek( cPergCont ) )
	Aadd( aHelp, { { 'Dt.Inicial.'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'Dt.Final.' 		}, 	{ '' }, { ' ' } } )
    Aadd( aHelp, { { 'Filial NF01' 		}, 	{ '' }, { ' ' } } )

	PutSx1( cPergCont, "01", "Dt. Inicial ?		", "", "", "mv_ch1", "D", 08, 0, 0, "C", "", "", "", "", "MV_PAR01", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[1][1], aHelp[1][2], aHelp[1][3] )
	PutSx1( cPergCont, "02", "Dt. Final ?      	", "", "", "mv_ch2", "D", 08, 0, 0, "C", "", "", "", "", "MV_PAR02", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[2][1], aHelp[2][2], aHelp[2][3] )
    PutSx1( cPergCont, "03", "Filial NF01 ?     ", "", "", "mv_ch3", "C", 08, 0, 0, "C", "", "SM0", "", "", "MV_PAR03", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[3][1], aHelp[3][2], aHelp[3][3] )

EndIf


RestArea( aArea )

Return( Nil)
