#Include 'Totvs.ch'

//_____________________________________________________________________________
/*/{Protheus.doc} OSBQCTR
Follow Up 

@author Heliomar Jr
@since 23 de Abril de 2015
@version P1.02
/*/
//_____________________________________________________________________________

User Function RCTR014()

Local aRegs			:= {}
Private oReport
Private cPergCont	:= 'RCTR014'
Private _astru		:={}
************************
*Monta pergunte do Log *
************************
AjustaSX1()

If Pergunte( cPergCONT, .T. )    
	Processa( { || ProcCont() }, 'Processando....' )
EndIf



Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ProcCont
Carrega os dados conforme parametrizacao feita pelo usuario;
Follow Up 

@author Heliomar Jr
@since 23 de Abril de 2015
@version P1.02
/*/
//_____________________________________________________________________________
Static Function ProcCont()
Local cQuery	:= ""
Local aPagto    := {}

BeginSql Alias "TMP"


SELECT  B.A1_COD				AS COD_CLI,
        D2_TES					AS TES,
        A.Z5_PV01				AS OS,
        A.Z5_EMPPV1				AS FILIAL_1,
        F2_DOC					AS NF_1,  
        A.Z5_EMPPV3				AS FILIAL_3,
        A.Z5_NF03				AS NF_3,
        (SUBSTRING(F2_EMISSAO,7,2) + '/' + SUBSTRING(F2_EMISSAO,5,2) + '/' + SUBSTRING(F2_EMISSAO,1,4))				AS DATA_EMISSAO_NOTA,
        (SUBSTRING(C5_EMISSAO,7,2) + '/' + SUBSTRING(C5_EMISSAO,5,2) + '/' + SUBSTRING(C5_EMISSAO,1,4))				AS DATA_EMISSAO_PEDIDO,
        (SUBSTRING(C5_YDTSOL,7,2) + '/' + SUBSTRING(C5_YDTSOL,5,2) + '/' + SUBSTRING(C5_YDTSOL,1,4))				AS DT_SOLICITACAO,
		(SUBSTRING(C5_SUGENT,7,2) + '/' + SUBSTRING(C5_SUGENT,5,2) + '/' + SUBSTRING(C5_SUGENT,1,4))				AS SUG_ENTREGA,
		(SUBSTRING(C5_FECENT,7,2) + '/' + SUBSTRING(C5_FECENT,5,2) + '/' + SUBSTRING(C5_FECENT,1,4))				AS DT_ENTREGA,
        A1_NOME					AS CLIENTE,
        C5_YNOMREC              AS RECEBEDOR,
        A4_NOME					AS TRANSP,
        C5_YCTRC                AS CTRC,
		F4_TEXTO                AS DESC_TES,
		A1_MUN					AS MUNICIPIO,
		A1_EST					AS ESTADO,
		D2_QUANT				AS QUANTIDADE,
		'FATURADA'			    AS STATUS_NF,
		D2_COD					AS COD_PROD, 
		B1_DESC					AS PRODUTO, 
		CONVERT(DECIMAL(10,2), D2_VALFRE)   AS FRETE,
		X5_DESCRI                           AS TIPO_PEDIDO,
		C5_YOBS                AS OBS			
		
FROM SZ5010 (NOLOCK) A
JOIN SA1010 (NOLOCK) B ON(B.A1_COD = A.Z5_CLIENTE AND A.%NOTDEL% AND B.%NOTDEL% )
JOIN SD2010 (NOLOCK) C ON( D2_DOC=Z5_NF01 AND D2_FILIAL=Z5_EMPPV1 AND C.%NOTDEL%)
JOIN SB1010 (NOLOCK) D ON(C.D2_COD = D.B1_COD AND D.%NOTDEL%)
JOIN SF2010 (NOLOCK) E ON(F2_DOC=Z5_NF01 AND F2_CLIENTE = Z5_CLIENTE AND F2_FILIAL=Z5_EMPPV1 AND E.%NOTDEL%)
JOIN SC5010 (NOLOCK) F ON(C5_NOTA=Z5_NF01 AND C5_CLIENTE = Z5_CLIENTE AND C5_FILIAL=Z5_EMPPV1 AND F.%NOTDEL%  )  
LEFT JOIN SA4010 (NOLOCK) G ON(G.A4_COD = F2_TRANSP AND G.D_E_L_E_T_='' )
JOIN SF4010 (NOLOCK) H ON(SUBSTRING(D2_FILIAL,5,2) = F4_FILIAL AND D2_TES = F4_CODIGO AND H.%NOTDEL%)  
LEFT JOIN SX5010 (NOLOCK) ON (C5_TPEDAV = X5_CHAVE AND X5_TABELA='X1' AND SX5010.D_E_L_E_T_='')   
WHERE D2_TES NOT IN('013','014','015','016','503','504','512','996','997')
//AND A1_CGC NOT IN('19002277000181','20443574000141','06267630000103','06267630000294','06267630000375','06267630000537','20443574000141','20433296000141','20112038000163','19483034000102','06267630000103') Linha comentada referente ao chamado 0110245
AND E.F2_EMISSAO >= %EXP:MV_PAR01% AND E.F2_EMISSAO <= %EXP:MV_PAR02%
AND C5_NOTA <> ''  
AND (YEAR(C5_SUGENT) >= 2014 or C5_SUGENT='')

UNION 


SELECT  A1_COD                  AS COD_CLI,
        D2_TES			        AS TES,
        C5_NUM                  AS OS,
        D2_FILIAL			    AS FILIAL_1,
        F2_DOC					AS NF_1,  
        ''				        AS FILIAL_3,
        ''				        AS NF_3,
        (SUBSTRING(F2_EMISSAO,7,2) + '/' + SUBSTRING(F2_EMISSAO,5,2) + '/' + SUBSTRING(F2_EMISSAO,1,4))				AS DATA_EMISSAO_NOTA,
        (SUBSTRING(C5_EMISSAO,7,2) + '/' + SUBSTRING(C5_EMISSAO,5,2) + '/' + SUBSTRING(C5_EMISSAO,1,4))				AS DATA_EMISSAO_PEDIDO,
        (SUBSTRING(C5_YDTSOL,7,2) + '/' + SUBSTRING(C5_YDTSOL,5,2) + '/' + SUBSTRING(C5_YDTSOL,1,4))				AS DT_SOLICITACAO,
		(SUBSTRING(C5_SUGENT,7,2) + '/' + SUBSTRING(C5_SUGENT,5,2) + '/' + SUBSTRING(C5_SUGENT,1,4))				AS SUG_ENTREGA,
		(SUBSTRING(C5_FECENT,7,2) + '/' + SUBSTRING(C5_FECENT,5,2) + '/' + SUBSTRING(C5_FECENT,1,4))				AS DT_ENTREGA,
        A1_NOME                 AS CLIENTE,
        C5_YNOMREC              AS NOME_RECEBEDOR,
        A4_NOME                 AS TRANSP,	
        C5_YCTRC                AS CTRC,
        F4_TEXTO                AS DESC_TES,
        A1_MUN                  AS MUNICIPIO,
        A1_EST                  AS ESTADO,
        D2_QUANT                AS QUANTIDADE,
        'FATURADO'              AS STATUS_NF,
       D2_COD					AS COD_PROD, 
		B1_DESC					AS PRODUTO, 
		CONVERT(DECIMAL(10,2), D2_VALFRE)   AS FRETE,
		X5_DESCRI                           AS TIPO_PEDIDO,
		C5_YOBS                AS OBS
        		
FROM SC5010 (NOLOCK) A
JOIN SA1010 (NOLOCK) B ON(B.A1_COD = A.C5_CLIENTE AND A.%NOTDEL% AND B.%NOTDEL% )
JOIN SD2010 (NOLOCK) C ON( D2_DOC=C5_NOTA AND D2_FILIAL=C5_FILIAL AND C.%NOTDEL% )
JOIN SB1010 (NOLOCK) D ON(C.D2_COD = D.B1_COD AND D.%NOTDEL%)
JOIN SF2010 (NOLOCK) E ON(F2_DOC=C5_NOTA AND F2_CLIENTE = C5_CLIENTE AND F2_FILIAL=C5_FILIAL AND E.%NOTDEL%) 
LEFT JOIN  SA4010 (NOLOCK) G ON(G.A4_COD = F2_TRANSP AND G.%NOTDEL% )
JOIN SF4010 (NOLOCK) H ON(SUBSTRING(D2_FILIAL,5,2) = F4_FILIAL AND D2_TES = F4_CODIGO AND H.%NOTDEL%) 
LEFT JOIN SX5010 (NOLOCK) ON(C5_TPEDAV = X5_CHAVE AND X5_TABELA='X1' AND SX5010.D_E_L_E_T_='')   
WHERE D2_TES NOT IN('013','014','015','016','503','504','512','996','997')
//AND A1_CGC NOT IN('19002277000181','20443574000141','06267630000103','06267630000294','06267630000375','06267630000537','20443574000141','20433296000141','20112038000163','19483034000102','06267630000103') Linha comentada referente ao chamado 0110245
AND E.F2_EMISSAO >= %EXP:MV_PAR01% AND E.F2_EMISSAO <= %EXP:MV_PAR02%
AND C5_NOTA <> '' 
AND (YEAR(C5_SUGENT) >= 2014 or C5_SUGENT='')
AND NOT EXISTS (SELECT 1 
                  FROM SZ5010  Z5
				  WHERE A.C5_NUM   = Z5.Z5_PV01
				  AND  A.C5_FILIAL = Z5.Z5_EMPPV1
				  AND  A.C5_NOTA   = Z5.Z5_NF01)
EndSql


oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
TMP->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

Follow Up 

@author Heliomar Jr
@since 23 de Abril de 2015
@version P1.02
/*/
//_____________________________________________________________________________
Static Function ReportDef()
Local oFirst
Local nOrd	:= 1


oReport := TReport():New( 'TMP', 'FOLLOW UP II', cPergCont, {|oReport| ReportPrint( oReport ), 'OPERACAO TRIANGULAR X AVULSO' } )
oReport:SetLandScape()
oReport:lParamReadOnly := .T.

oReport:SetTotalInLine( .F. )

oFirst := TRSection():New( oReport, 'FOLLOW UP', { 'TMP', 'SF2', 'SC5', 'SA1', 'SA4','SB1','SC6', 'SZ5','SF2', 'SF4','SD2' },,, )

oFirst:SetTotalInLine( .F. )


TRCell():New( oFirst, 'Cod_Cliente'			,'TMP', 'Cod_Cliente',				"@!"						    ,06,,							{ || TMP->COD_CLI 																			} )
TRCell():New( oFirst, 'Tes'	       			,'TMP', 'Tes' ,				    	"@!"							,03,,							{ || TMP->TES   																			} )
TRCell():New( oFirst, 'OS'	       			,'TMP', 'OS' ,						"@!"							,08,,							{ || TMP->OS																				} )
TRCell():New( oFirst, 'Filial_1'   			,'TMP', 'Filial_1',					"@!"						  	,06,,							{ || TMP->FILIAL_1																			} )
TRCell():New( oFirst, 'NF_1'	   			,'TMP', 'NF_1',						"@!"  							,09,,							{ || TMP->NF_1																				} )
TRCell():New( oFirst, 'Filial_3'   			,'TMP', 'Filial_3' ,				"@!"							,06,,							{ || TMP->FILIAL_3 	    																	} ) 
TRCell():New( oFirst, 'NF_3'	   			,'TMP', 'NF_3',						"@!"  							,09,,							{ || TMP->NF_3		    																	} )
TRCell():New( oFirst, 'Data_Emissao_Not'	,'TMP', 'Data_Emissao_Not',			"@!"							,08,,							{ || TMP->DATA_EMISSAO_NOTA																	} )
TRCell():New( oFirst, 'Data_Emissao_Ped'	,'TMP', 'Data_Emissao_OS',			"@!"							,08,,							{ || TMP->DATA_EMISSAO_PEDIDO																} )
TRCell():New( oFirst, 'Data_Solicitacao'	,'TMP', 'Data_Solicitacao_Pedido',	"@!"							,08,,							{ || TMP->DT_SOLICITACAO																	} )
TRCell():New( oFirst, 'DT Sug Entrega'		,'TMP', 'DT Sug Entrega' ,	    	"@!"							,08,,							{ || TMP->SUG_ENTREGA		    															} )
TRCell():New( oFirst, 'DT Entrega'			,'TMP', 'DT Entrega' ,	        	"@!"							,08,,							{ || TMP->DT_ENTREGA																		} )
TRCell():New( oFirst, 'Cliente'				,'TMP', 'Cliente',					"@!"							,40,,							{ || TMP->CLIENTE																			} )
TRCell():New( oFirst, 'Recebedor'			,'TMP', 'Recebedor',				"@!"							,30,,							{ || TMP->RECEBEDOR																			} )
TRCell():New( oFirst, 'Transportadora'		,'TMP', 'Transportadora',			"@!"							,40,,							{ || TMP->TRANSP																			} )
TRCell():New( oFirst, 'Ctrc'				,'TMP', 'Ctrc',						"@!"							,30,,							{ || TMP->CTRC																				} )
TRCell():New( oFirst, 'Desc Tes'			,'TMP', 'Des Tes',					"@!"							,20,,							{ || TMP->DESC_TES																			} )
TRCell():New( oFirst, 'Municipio'			,'TMP', 'Municipio',				"@!"							,60,,							{ || TMP->MUNICIPIO																			} )
TRCell():New( oFirst, 'Estado'				,'TMP', 'Estado',					"@!"							,20,,							{ || TMP->ESTADO																			} )
TRCell():New( oFirst, 'Quantidade'			,'TMP', 'Quantidade',				"@E 999,999.99"					,08,,							{ || TMP->QUANTIDADE																		} )
TRCell():New( oFirst, 'Status'				,'TMP', 'Status',					"@!"							,09,,							{ || TMP->STATUS_NF																			} )
TRCell():New( oFirst, 'Cod Produto'			,'TMP', 'Cod Produto',				"@!"							,15,,							{ || TMP->COD_PROD																			} )
TRCell():New( oFirst, 'Produto'				,'TMP', 'Produto',					"@!"							,50,,							{ || TMP->PRODUTO																			} )
TRCell():New( oFirst, 'Frete'				,'TMP', 'Frete',					"@E 999,999.99"					,08,,							{ || TMP->FRETE				    															} )
TRCell():New( oFirst, 'Tipo Pedido'			,'TMP', 'Tipo Pedido',				"@!"							,15,,							{ || TMP->TIPO_PEDIDO																		} )
TRCell():New( oFirst, 'OBS.'				,'TMP', 'OBS.',				    	"@!"							,50,,							{ || TMP->OBS				    															} )

//oBreakT := TRBreak():New( oFirst, { || }, 'Vlr. Total' )

//TRFunction():New( oFirst:Cell( 'ValFat2' ),	'', 'SUM', oBreakT,,,, .F., .F. )

//oFirst:Cell( 'TOTAL' ):SetHeaderAlign( 'RIGHT' )

//oFirst:SetColSpace(9)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

Follow Up 

@author Heliomar Jr
@since 23 de Abril de 2015
@version P1.02
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

MakeAdvplExpr( cPergCont )
oReport:Section(1):Enable()

dbSelectArea( 'TMP' )
oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

Follow Up 

@author Heliomar Jr
@since 23 de Abril de 2015
@version P1.02
/*/
//_____________________________________________________________________________
Static Function AjustaSX1()
Local aHelp	:= {}
Local aArea	:= GetArea()

SX1->( dbSetOrder(1) )

If !SX1->( dbSeek( cPergCont ) )
	Aadd( aHelp, { { 'Data Inicial.'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'Data Final.' 		}, 	{ '' }, { ' ' } } )
    //Aadd( aHelp, { { 'Filial NF01' 		}, 	{ '' }, { ' ' } } )

	PutSx1( cPergCont, "01", "Data Inicial ?		", "", "", "mv_ch1", "D", 08, 0, 0, "C", "", "", "", "", "MV_PAR01", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[1][1], aHelp[1][2], aHelp[1][3] )
	PutSx1( cPergCont, "02", "Data Final ?      	", "", "", "mv_ch2", "D", 08, 0, 0, "C", "", "", "", "", "MV_PAR02", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[2][1], aHelp[2][2], aHelp[2][3] )
   // PutSx1( cPergCont, "03", "Filial NF01 ?      	", "", "", "mv_ch3", "C", 08, 0, 0, "C", "", "", "", "", "MV_PAR03", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[3][1], aHelp[3][2], aHelp[3][3] )

EndIf


RestArea( aArea )

Return( Nil)
