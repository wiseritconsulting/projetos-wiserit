#Include 'Protheus.ch'
/*/{Protheus.doc} SASR018
Relatório CTE x Faturamento
@author Diogo
@since 18/05/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function SASR018()

	Local oReport
	Private cPerg := PadR('SASR018',10)
	
	CriaSX1(cPerg)
	Pergunte(cPerg,.T.)

	oReport := reportDef()
	oReport:printDialog()
Return

static function reportDef()
	local oReport
	Local oSection1
	Local oSection2
	Local oSection3
	local cTitulo := 'CTE x FAT'

	oReport := TReport():New('SASR018', cTitulo,cPerg, {|oReport| PrintReport(oReport)},"CTE x FAT")
	oReport:SetLandscape()
	oReport:SetTotalInLine(.F.)
	oReport:ShowHeader()

	oSection1 	:= TRSection():New(oReport,"CTE",{"QRY18"})

	TRCell():New(oSection1, "CTE"						, "QRY18", 'CTe'				,,27,,)
	TRCell():New(oSection1, "SERIE"						, "QRY18", 'SERIE  '			,,TamSX3("F2_SERIE")[1]+2,,,,.T.)
	TRCell():New(oSection1, "EMISSAO"					, "QRY18", 'EMISSAO'			,,15,,)
	TRCell():New(oSection1, "EMITENT"					, "QRY18", 'EMITENTE'			,,TamSX3("A1_COD")[1]+7,,)
	TRCell():New(oSection1, "RAZAO"					    , "QRY18", 'RAZAO SOCIAL'		,,TamSX3("A1_NOME")[1]+5,,)
	TRCell():New(oSection1, "VALOR MERCADORIA"			, "QRY18", 'VALOR MERCADORIA'	,"@E 99,99,999,999.99",GetSX3Cache("E2_VALOR","X3_TAMANHO"),,) // WESKLEY SILVA 16/06/2015
	TRCell():New(oSection1, "VALOR SERVICO"			    , "QRY18", 'VALOR SERVICO'		,"@E 99,99,999,999.99",GetSX3Cache("E2_VALOR","X3_TAMANHO"),,) // WESKLEY SILVA 16/06/2015


	oSection2 	:= TRSection():New(oReport,"NOTAS",{"QRY18"})

	TRCell():New(oSection2, "CTE"			, "QRY18", 'CTe'		,,27,,)
	TRCell():New(oSection2 , "NF"			, "QRY18", 'NF'			 ,,27,,)
	TRCell():New(oSection2 , "EMISSAO"		, "QRY18", 'EMISSAO'	 ,,TamSX3("F2_EMISSAO")[1]+3,,,,.T.)
	TRCell():New(oSection2 , "CLIENTE"		, "QRY18", 'CLIENTE'	 ,,TamSX3("A1_COD")[1]+5,,)
	TRCell():New(oSection2 , "RAZAO"		, "QRY18", 'RAZAO SOCIAL',,TamSX3("A1_NOME")[1]+5,,)
	TRCell():New(oSection2 , "UF"			, "QRY18", 'UF',,TamSX3("A1_EST")[1]+9,,)
	TRCell():New(oSection2 , "VALOR"		, "QRY18", 'VALOR FRETE'		,"@E 99,99,999,999.99",TamSX3("D2_PRCVEN")[1]+4,,)
	TRCell():New(oSection2 , "TOTAL"		, "QRY18", 'VALOR NOTA'			,"@E 99,99,999,999.99",TamSX3("D2_TOTAL")[1]+4,,)
	
	oBreak := TRBreak():New(oSection1,oSection1:Cell('CTE'),NIL,.F.)
	oBreak2:= TRBreak():New(oSection2,oSection2:Cell('CTE'),NIL,.F.)

	TRFunction():New(oSection2:Cell('VALOR'  ),NIL,"SUM",oBreak2,NIL,"@E 99,99,999,999.99",/*uFormula*/,.F.,.T.)		
	TRFunction():New(oSection2:Cell('TOTAL'  ),NIL,"SUM",oBreak2,NIL,"@E 99,99,999,999.99",/*uFormula*/,.F.,.T.)
		
return (oReport)

Static Function PrintReport(oReport)
	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cSetSql
	Local nTotQt := 0
	Local nTotPr := 0
	Private _cTpCli
	
	cSetSql := qryDefault()

	cSetSql := ChangeQuery(cSetSql)

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY18", .F., .T.)

	DbSelectArea('QRY18')

	dbGoTop()
	oReport:SetMeter(QRY18->(RecCount()))


	While QRY18->(!Eof())

		If oReport:Cancel()
			Exit
		EndIf

		oSection1:Init()
		oSection1:SetHeaderSection(.T.)
		oReport:IncMeter()
	
		oSection1:Cell("CTE"):SetValue(QRY18->CTE)
		oSection1:Cell("SERIE"):SetValue(QRY18->SERIE)
		oSection1:Cell("EMISSAO"):SetValue( substr(QRY18->EMISSAO,9,2)+"/"+substr(QRY18->EMISSAO,6,2)+"/"+substr(QRY18->EMISSAO,1,4) ) 
		oSection1:Cell("EMITENT"):SetValue(QRY18->EMITENT)
		oSection1:Cell("RAZAO"):SetValue(QRY18->RAZAO)
		oSection1:Cell("VALOR MERCADORIA"):SetValue(val(QRY18->VALOR_MERCADORIA)) // WESKLEY SILVA 16/06/2015
		oSection1:Cell("VALOR SERVICO"):SetValue(val(QRY18->VALOR_SERVICO)) // WESKLEY SILVA 16/06/2015

		oSection1:PrintLine()
		oReport:SkipLine()
		oSection1:Finish()
		cCTE:= QRY18->CTE
		
		aNotas:= strtokarr (QRY18->NOTAS , "|")
		FQryNf(aNotas,QRY18->FILIAL) //Notas Fiscais
		
		oSection2:Init()
		oSection2:SetHeaderSection(.T.)
		
		while QRY18ITENS->(!eof())

			oSection2:Cell("CTE"):SetValue(cCTE)
			oSection2:Cell("NF"):SetValue(QRY18ITENS->NF)
			oSection2:Cell("EMISSAO"):SetValue(stod(QRY18ITENS->EMISSAO))
			oSection2:Cell("CLIENTE"):SetValue(QRY18ITENS->CLIENTE)
			oSection2:Cell("RAZAO"):SetValue(QRY18ITENS->RAZAO)
			oSection2:Cell("UF"):SetValue(QRY18ITENS->UF)
			oSection2:Cell("VALOR"):SetValue(QRY18ITENS->VALOR)
			oSection2:Cell("TOTAL"):SetValue(QRY18ITENS->TOTAL)
			
			oSection2:PrintLine()
			QRY18ITENS->(dbSkip())
		enddo			

		oSection2:Finish()
		
		dbSelectArea("QRY18")
		QRY18->(dbSkip())

	EndDo

	QRY18->(dbCloseArea())
	//QRY18ITENS->(dbCloseArea()) WESKLEY SILVA 15/06/2015

Return

static function CriaSX1(cPerg)

	PutSx1(cPerg, "01","Transportadora De" 		,"Transportadora De"		,"Transportadora De"	,"mv_ch01","C",GetSx3Cache("A2_COD","X3_TAMANHO"),0,2,"G","" ,"SA2","","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Transportadora Ate" 	,"Transportadora Ate"		,"Transportadora Ate"	,"mv_ch02","C",GetSx3Cache("A2_COD","X3_TAMANHO"),0,2,"G","" ,"SA2","","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","CTE de" 				,"CTE de"					,"CTE de"				,"mv_ch03","C",GetSx3Cache("F1_DOC","X3_TAMANHO"),0,2,"G","" ,"","","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "04","CTE ate" 				,"CTE ate"					,"CTE ate"				,"mv_ch04","C",GetSx3Cache("F1_DOC","X3_TAMANHO"),0,2,"G","" ,"","","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "05","Periodo De" 			,"Periodo De"				,"Periodo De"			,"mv_ch05","D",08,0,2,"G","" ,"","","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "06","Periodo Ate" 			,"Periodo Ate"				,"Periodo Ate"			,"mv_ch06","D",08,0,2,"G","" ,"","","","mv_par02"," ","","","","","","","","","","","","","","","")

Return

//Query do Relatório
Static function qryDefault()

	Local cQuery := ""
	
	cQuery:= 	" SELECT CONHECIMENTO AS CTE,"+;
				"	F1_FILIAL AS FILIAL,"+;
				"	A2_COD AS EMITENT,"+;
				"	NOME_EMITENTE_2 AS RAZAO,"+;
				"	VALOR_NF_2 AS 'VALOR_MERCADORIA' ,"+;
				"	VALOR_TOTAL_SERVICO AS 'VALOR_SERVICO',"+;
				"	SERIE,"+;
				"	SUBSTRING(DATA_EMISSAO_DACTE,1,10) AS EMISSAO,"+;
				"	NOTAS_FISCAIS_2 NOTAS "+;
				"	FROM XMLCTE "+;
				" JOIN "+RetSqlName("SA2")+" SA2 ON "+;
				" A2_CGC = CNPJ_EMITENTE collate Latin1_General_CI_AI AND "+;  
				" SA2.D_E_L_E_T_ =' '"+;
				" JOIN "+RetSqlName("SF1")+" SF1 ON "+;
				" SF1.D_E_L_E_T_ =' ' AND "+;
				" F1_DOC = SUBSTRING(CONHECIMENTO,2,9)  collate Latin1_General_CI_AI"+;
				" AND A2_COD = F1_FORNECE collate Latin1_General_CI_AI"+;
				" AND A2_LOJA = F1_LOJA collate Latin1_General_CI_AI"+;
				" WHERE "+;
				" A2_COD BETWEEN '"+mv_par01+"' AND '"+mv_par02+"' "+;
				" AND F1_DOC BETWEEN '"+mv_par03+"' AND '"+mv_par04+"'  "+;
				" AND SUBSTRING(DATA_EMISSAO_DACTE,1,10) BETWEEN "+;
				" '"+substr(dTos(mv_par05),1,4)+"-"+substr(dTos(mv_par05),5,2)+"-"+substr(dTos(mv_par05),7,2)+"' "+;
				" AND '"+substr(dTos(mv_par06),1,4)+"-"+substr(dTos(mv_par06),5,2)+"-"+substr(dTos(mv_par06),7,2)+"' "+;
				" ORDER BY F1_FILIAL,CONHECIMENTO "
				

Return cQuery

Static Function FQryNf(aNotas,cFilNF)
	
	If select("QRY18ITENS") > 0
		QRY18ITENS->(dbCloseArea())
	Endif
	
	cQuery:= 	" SELECT F2_DOC AS NF, "+;
				" 		F2_EMISSAO AS EMISSAO, "+;
				" 		F2_CLIENTE AS CLIENTE, "+;
				" 		A1_NOME AS RAZAO, "+;
				" 		F2_EST AS UF, "+;
				" 		F2_FRETE AS VALOR, "+;
				" 		F2_VALBRUT AS TOTAL "+;
				" 		FROM "+RetSqlName("SF2")+" SF2 "+;
				" 		JOIN "+RetSqlName("SA1")+" SA1  ON "+;
				" 		A1_COD = F2_CLIENTE AND "+;
				" 		A1_LOJA = F2_LOJA AND "+;
				" 		SA1.D_E_L_E_T_ = ' ' "+;
				" 		WHERE SF2.D_E_L_E_T_ = ' '  "+;
				" 		AND SF2.F2_TIPO <> 'D' "+;
				" 		AND SF2.F2_FILIAL = '"+cFilNF+"' "+;
				" 		AND SF2.F2_DOC  IN ( "
				
				For i:= 1 To Len(aNotas)
					cQuery+= "'"+Alltrim(substr(aNotas[i],2,9))+"',"  
				Next
				
				cQuery:= substr(cQuery,1,len(cQuery)-1)+" ) "
				cQuery+= " ORDER BY F2_DOC, F2_EMISSAO"

	cSetSql := ChangeQuery(cQuery)

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY18ITENS", .F., .T.)

	DbSelectArea('QRY18ITENS')

Return