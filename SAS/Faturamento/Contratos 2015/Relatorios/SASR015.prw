#Include 'Protheus.ch'
/*/{Protheus.doc} SASR015
Relat�rio de Auditoria Opera��o Triangular (Devolu��o)
@author Diogo
@since 18/05/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function SASR015()

	Local oReport
	Private cPerg := PadR('SSAR015',10)

	CriaSX1(cPerg)
	Pergunte(cPerg,.T.)

	oReport := reportDef()
	oReport:printDialog()
Return

static function reportDef()
	local oReport
	Local oSection1
	Local oSection2
	Local oSection3
	local cTitulo := 'Relat�rio Opera��o Triangular (Devolu��o)'

	oReport := TReport():New('SASR015', cTitulo,cPerg, {|oReport| PrintReport(oReport)},"Relat�rio Opera��o Triangular (Devolu��o)")
	oReport:SetLandscape()
	oReport:SetTotalInLine(.F.)
	oReport:ShowHeader()

	oSection1 	:= TRSection():New(oReport,"CAB",{"QRY15"})

	TRCell():New(oSection1 , "NFTIPO"		, "QRY15", 'TIPO'			,,45,,)
	TRCell():New(oSection1, "Z8_NF"			, "QRY15", 'NF-e'			,,27,,)
	TRCell():New(oSection1, "F2_EMISSAO"	, "QRY15", 'EMISSAO  '		,,TamSX3("F2_EMISSAO")[1]+3,,,,.T.)
	TRCell():New(oSection1, "CLIENTE"		, "QRY15", 'CLIENTE'		,,15,,)
	TRCell():New(oSection1, "RAZAO"			, "QRY15", 'RAZAO SOCIAL'	,,TamSX3("A1_NOME")[1]+5,,)
	TRCell():New(oSection1, "VALOR"			, "QRY15", 'VALOR TOTAL'			,"@E 99,99,999,999.99",GetSX3Cache("E2_VALOR","X3_TAMANHO"),,)


	oSection2 	:= TRSection():New(oReport,"ITENS",{"QRY15"})

	TRCell():New(oSection2 , "NFTIPO"		, "QRY15", 'TIPO'		,,45,,)
	TRCell():New(oSection2, "Z8_NF"			, "QRY15", 'NF-e'		,,27,,)
	TRCell():New(oSection2 , "NFD"			, "QRY15", 'NFD'		,,27,,)
	TRCell():New(oSection2 , "EMISSAO"		, "QRY15", 'EMISSAO'	,,TamSX3("F2_EMISSAO")[1]+3,,,,.T.)
	TRCell():New(oSection2 , "PRODUTO"		, "QRY15", 'PRODUTO'	,,TamSX3("B1_COD")[1]+1,,)
	TRCell():New(oSection2 , "DESCRICAO"	, "QRY15", 'DESCRICAO'	,,TamSX3("B1_DESC")[1]+1,,)
	TRCell():New(oSection2 , "QUANTIDADE"	, "QRY15", 'QUANTIDADE'	,"@E 999,999.99",TamSX3("D2_QUANT")[1]+4,,)
	TRCell():New(oSection2 , "VALOR"		, "QRY15", 'VALOR UNITARIO'		,"@E 99,99,999,999.99",TamSX3("D2_PRCVEN")[1]+4,,)
	TRCell():New(oSection2 , "TOTAL"		, "QRY15", 'VALOR TOTAL'		,"@E 99,99,999,999.99",TamSX3("D2_TOTAL")[1]+4,,)
	TRCell():New(oSection2 , "STATUS"		, "QRY15", 'PROCESSADO'		,,45,,)
		
return (oReport)

Static Function PrintReport(oReport)
	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cSetSql
	Local nTotQt := 0
	Local nTotPr := 0
	Private _cTpCli
	
	cSetSql := Qry15Default()

	cSetSql := ChangeQuery(cSetSql)

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY15", .F., .T.)

	DbSelectArea('QRY15')

	QRY15->(dbGoTop())
	oReport:SetMeter(QRY15->(RecCount()))
	cClients:= ""

	While QRY15->(!Eof())

		If oReport:Cancel()
			Exit
		EndIf

		cStatus:= FBuscDEVZ8(QRY15->NOTA2,QRY15->Z5_EMPPV2)//Retorna Status se j� efetivado entrada
		QRY15SF1->(dbCloseArea())
		
		oReport:SkipLine()
		oReport:IncMeter()
		oReport:PrintText("NOTA FISCAL 01")
		
		//------------ NOTA 01 ---------------------
		oSection1:Init()
		oSection1:SetHeaderSection(.T.)
		//Nota 1
		oSection1:Cell("NFTIPO"):SetValue('NOTA 1')
		oSection1:Cell("Z8_NF"):SetValue(QRY15->NOTA1)
		oSection1:Cell("F2_EMISSAO"):SetValue(stod(QRY15->EMISSAONF1))
		oSection1:Cell("CLIENTE"):SetValue(QRY15->CLIENTE1)
		oSection1:Cell("RAZAO"):SetValue(QRY15->NOME1)
		oSection1:Cell("VALOR"):SetValue(QRY15->VALORNF1)
		oSection1:Cell("VALOR"):SetPicture("@E 99,99,999,999.99")
		oSection1:PrintLine()
		oReport:SkipLine()
		cNf:= QRY15->NOTA1
				
		oSection1:Finish()
		
		If  !(Alltrim(strtran(QRY15->CLIENTE1,"/","")) $ Alltrim(strtran(cClients,"/","")))
			cClients+= Alltrim(strtran(QRY15->CLIENTE1,"/",""))+","
		Endif	
		
		//Busca Informa��es da devolu��o
		oSection2:Init()
		FQueryDev(QRY15->NOTA1,QRY15->Z5_EMPPV1,'1')
		
		while QRY15ITEM->(!eof())


			oSection2:Cell("NFTIPO"):SetValue('NOTA 1')
			oSection2:Cell("Z8_NF"):SetValue(cNf)
			oSection2:Cell("NFD"):SetValue(QRY15ITEM->NOTA)
			oSection2:Cell("EMISSAO"):SetValue(stod(QRY15ITEM->EMISSAO))
			oSection2:Cell("PRODUTO"):SetValue(QRY15ITEM->PRODUTO)
			oSection2:Cell("DESCRICAO"):SetValue(QRY15ITEM->DESCRICAO)
			oSection2:Cell("QUANTIDADE"):SetValue(QRY15ITEM->QUANTIDADE)
			oSection2:Cell("VALOR"):SetValue(QRY15ITEM->VALOR)
			oSection2:Cell("TOTAL"):SetValue(QRY15ITEM->TOTAL)
			oSection2:Cell("STATUS"):SetValue(cStatus)

			oSection2:PrintLine()
			QRY15ITEM->(dbSkip())
		enddo	
		QRY15ITEM->(dbCloseArea())
		oSection2:Finish()
		oReport:SkipLine()
		//------------ NOTA 02 ---------------------

		oReport:PrintText("NOTA FISCAL 02")
		oSection1:Init()
		oSection1:SetHeaderSection(.T.)
		
		//Nota 2
		oSection1:Cell("NFTIPO"):SetValue('NOTA 2')
		oSection1:Cell("Z8_NF"):SetValue(QRY15->NOTA2)
		oSection1:Cell("F2_EMISSAO"):SetValue(stod(QRY15->EMISSAONF1))
		oSection1:Cell("CLIENTE"):SetValue(QRY15->CLIENTE2)
		oSection1:Cell("RAZAO"):SetValue(QRY15->NOME2)
		oSection1:Cell("VALOR"):SetValue(QRY15->VALORNF2)
		oSection1:Cell("VALOR"):SetPicture("@E 99,99,999,999.99")
			
		oSection1:PrintLine()
		oReport:SkipLine()
				
		oSection1:Finish()
		cNf:= QRY15->NOTA2

		If !(Alltrim(strtran(QRY15->CLIENTE2,"/","")) $ Alltrim(strtran(cClients,"/","")))
			cClients+= Alltrim(strtran(QRY15->CLIENTE2,"/",""))+","
		Endif	
		
		
		//Busca Informa��es da devolu��o
		oSection2:Init()
		FQueryDev(QRY15->NOTA2,QRY15->Z5_EMPPV2,'2')
		
		while QRY15ITEM->(!eof())

			oSection2:Cell("NFTIPO"):SetValue('NOTA 2')
			oSection2:Cell("Z8_NF"):SetValue(cNf)
			oSection2:Cell("NFD"):SetValue(QRY15ITEM->NOTA)
			oSection2:Cell("EMISSAO"):SetValue(stod(QRY15ITEM->EMISSAO))
			oSection2:Cell("PRODUTO"):SetValue(QRY15ITEM->PRODUTO)
			oSection2:Cell("DESCRICAO"):SetValue(QRY15ITEM->DESCRICAO)
			oSection2:Cell("QUANTIDADE"):SetValue(QRY15ITEM->QUANTIDADE)
			oSection2:Cell("VALOR"):SetValue(QRY15ITEM->VALOR)
			oSection2:Cell("STATUS"):SetValue(cStatus)

			oSection2:PrintLine()
			QRY15ITEM->(dbSkip())
		enddo	
		QRY15ITEM->(dbCloseArea())
		oSection2:Finish()		
		oReport:SkipLine()

		 //------------ NOTA 03 ---------------------
		oReport:PrintText("NOTA FISCAL 03")
		oSection1:Init()
		oSection1:SetHeaderSection(.T.)
		//Nota 3
		oSection1:Cell("NFTIPO"):SetValue('NOTA 3')
		oSection1:Cell("Z8_NF"):SetValue(QRY15->NOTA3)
		oSection1:Cell("F2_EMISSAO"):SetValue(stod(QRY15->EMISSAONF1))
		oSection1:Cell("CLIENTE"):SetValue(QRY15->CLIENTE3)
		oSection1:Cell("RAZAO"):SetValue(QRY15->NOME3)
		oSection1:Cell("VALOR"):SetValue(QRY15->VALORNF3)
		oSection1:Cell("VALOR"):SetPicture("@E 99,99,999,999.99")
			
		oSection1:PrintLine()
		oReport:SkipLine()
				
		oSection1:Finish()
		cNf:= QRY15->NOTA3
		
		If  !(Alltrim(strtran(QRY15->CLIENTE3,"/","")) $ Alltrim(strtran(cClients,"/","")))
			cClients+= Alltrim(strtran(QRY15->CLIENTE3,"/",""))+","
		Endif	
		
		//Busca Informa��es da devolu��o
		oSection2:Init()
		FQueryDev(QRY15->NOTA3,QRY15->Z5_EMPPV3,'3')
		
		while QRY15ITEM->(!eof())

			oSection2:Cell("NFTIPO"):SetValue('NOTA 3')
			oSection2:Cell("Z8_NF"):SetValue(cNf)
			oSection2:Cell("NFD"):SetValue(QRY15ITEM->NOTA)
			oSection2:Cell("EMISSAO"):SetValue(stod(QRY15ITEM->EMISSAO))
			oSection2:Cell("PRODUTO"):SetValue(QRY15ITEM->PRODUTO)
			oSection2:Cell("DESCRICAO"):SetValue(QRY15ITEM->DESCRICAO)
			oSection2:Cell("QUANTIDADE"):SetValue(QRY15ITEM->QUANTIDADE)
			oSection2:Cell("VALOR"):SetValue(QRY15ITEM->VALOR)
			oSection2:Cell("STATUS"):SetValue(cStatus)

			oSection2:PrintLine()
			QRY15ITEM->(dbSkip())
		enddo	
		QRY15ITEM->(dbCloseArea())
		oSection2:Finish()
		oReport:SkipLine()
		
		//------------ NOTA 04 ---------------------

		
		If !empty(QRY15->NOTA4)
			oReport:PrintText("NOTA FISCAL 04")
			oSection1:Init()
			oSection1:SetHeaderSection(.T.)
			
			//Nota 4
			oSection1:Cell("NFTIPO"):SetValue('NOTA 4')
			oSection1:Cell("Z8_NF"):SetValue(QRY15->NOTA4)
			oSection1:Cell("F2_EMISSAO"):SetValue(stod(QRY15->EMISSAONF1))
			oSection1:Cell("CLIENTE"):SetValue(QRY15->CLIENTE4)
			oSection1:Cell("RAZAO"):SetValue(QRY15->NOME4)
			oSection1:Cell("VALOR"):SetValue(QRY15->VALORNF4)
			oSection1:Cell("VALOR"):SetPicture("@E 99,99,999,999.99")
				
			oSection1:PrintLine()
			oReport:SkipLine()
					
			oSection1:Finish()
			cNf:= QRY15->NOTA4
			
			//Busca Informa��es da devolu��o
			oSection2:Init()
			FQueryDev(QRY15->NOTA4,QRY15->Z5_EMPPC4,'4')
			
			while QRY15ITEM->(!eof())
	
				oSection2:Cell("NFTIPO"):SetValue('NOTA 4')
				oSection2:Cell("Z8_NF"):SetValue(cNf)
				oSection2:Cell("NFD"):SetValue(QRY15ITEM->NOTA)
				oSection2:Cell("EMISSAO"):SetValue(stod(QRY15ITEM->EMISSAO))
				oSection2:Cell("PRODUTO"):SetValue(QRY15ITEM->PRODUTO)
				oSection2:Cell("DESCRICAO"):SetValue(QRY15ITEM->DESCRICAO)
				oSection2:Cell("QUANTIDADE"):SetValue(QRY15ITEM->QUANTIDADE)
				oSection2:Cell("VALOR"):SetValue(QRY15ITEM->VALOR)
				oSection2:Cell("STATUS"):SetValue(cStatus)
	
				oSection2:PrintLine()
				QRY15ITEM->(dbSkip())
			enddo	
			QRY15ITEM->(dbCloseArea())
			oSection2:Finish()		
		Endif	

		oReport:SkipLine()	
		
		//Notas 			
		dbSelectArea("QRY15")
		QRY15->(dbSkip())

	EndDo

	QRY15->(dbCloseArea())

Return

Static function CriaSX1(cPerg)

	PutSx1(cPerg, "01","Contrato De" 	,"Contrato De"		,"Contrato De"	,"mv_ch01","C",GetSx3Cache("Z8_CONTRAT","X3_TAMANHO"),0,0,"G","" ,"","","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Contrato Ate" 	,"Contrato Ate"		,"Contrato Ate"	,"mv_ch02","C",GetSx3Cache("Z8_CONTRAT","X3_TAMANHO"),0,2,"G","" ,"","","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","Medicao De" 	,"Medicao De"		,"Medicao De"	,"mv_ch03","C",GetSx3Cache("Z8_MEDICAO","X3_TAMANHO"),0,2,"G","" ,"","","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "04","Medicao Ate" 	,"Medicao Ate"		,"Medicao Ate"	,"mv_ch04","C",GetSx3Cache("Z8_MEDICAO","X3_TAMANHO"),0,2,"G","" ,"","","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "05","Cliente De" 	,"Cliente Ate"		,"Cliente De"	,"mv_ch05","C",GetSx3Cache("Z8_CLIENTE","X3_TAMANHO"),0,2,"G","" ,"SA1","","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "06","Cliente Ate" 	,"Cliente Ate"		,"Cliente Ate"	,"mv_ch06","C",GetSx3Cache("Z8_CLIENTE","X3_TAMANHO"),0,2,"G","" ,"SA1","","","mv_par06"," ","","","","","","","","","","","","","","","")

Return

//Query do Relat�rio
Static function Qry15bkpDefault()

	Local cQuery := ""
	cQuery:= 	" SELECT "+chr(13)+chr(10)+ ;
				"	Z8_EMP01,"+chr(13)+chr(10)+ ;
				"	Z8_EMPMTZ, "+chr(13)+chr(10)+ ;	 	
				"	Z8.Z8_CONTRAT,"+chr(13)+chr(10)+ ;
				"	Z8.Z8_MEDICAO,"+chr(13)+chr(10)+ ;
				"	Z8.Z8_NF01,"+chr(13)+chr(10)+ ;  
				"	Z8.Z8_NF02,"+chr(13)+chr(10)+ ;	
				"	Z8.Z8_ENTRADA,"+chr(13)+chr(10)+ ;
				"	Z8.Z8_EMPMTZ Z8_EMPMTZ ,"+chr(13)+chr(10)+ ; 	
				"	SZ5.Z5_EMPPV1 FILIAL1 ,"+chr(13)+chr(10)+ ; 	
				"	SF11.F1_DOC+'/'+SF11.F1_SERIE NOTA1,"+chr(13)+chr(10)+ ; 	
				"	SF11.F1_VALBRUT VALORNF1,"+chr(13)+chr(10)+ ; 	
				"	SF11.F1_EMISSAO EMISSAONF1,"+chr(13)+chr(10)+ ; 	
				"	SZ5.Z5_EMPPV2 FILIAL2 ,"+chr(13)+chr(10)+ ; 	
				"	SF22.F2_DOC+'/'+SF22.F2_SERIE NOTA2,"+chr(13)+chr(10)+ ; 	
				"	SF22.F2_VALBRUT VALORNF2,"+chr(13)+chr(10)+ ; 	
				"	SZ5.Z5_EMPPV3 FILIAL3 ,"+chr(13)+chr(10)+ ; 	
				"	SF13.F1_DOC+'/'+SF13.F1_SERIE NOTA3,"+chr(13)+chr(10)+ ; 	
				"	SF13.F1_VALBRUT VALORNF3,"+chr(13)+chr(10)+ ; 	
				"	SZ5.Z5_EMPPC4 FILIAL4 ,"+chr(13)+chr(10)+ ; 	
				"	SF14.F1_DOC+'/'+SF14.F1_SERIE NOTA4,"+chr(13)+chr(10)+ ; 	
				"	SF14.F1_VALBRUT VALORNF4,"+chr(13)+chr(10)+ ; 	
				"	SA11.A1_COD+'/'+SA11.A1_LOJA CLIENTE1,"+chr(13)+chr(10)+ ; 	
				"	SA11.A1_NOME NOME1,"+chr(13)+chr(10)+ ;  	
				"	SA12.A1_COD+'/'+SA12.A1_LOJA CLIENTE2,"+chr(13)+chr(10)+ ; 	
				"	SA12.A1_NOME NOME2,"+chr(13)+chr(10)+ ;  	
				"	SA13.A1_COD+'/'+SA13.A1_LOJA CLIENTE3,"+chr(13)+chr(10)+ ; 	
				"	SA13.A1_NOME NOME3,"+chr(13)+chr(10)+ ;  	
				"	SA24.A2_COD+'/'+SA24.A2_LOJA CLIENTE4,"+chr(13)+chr(10)+ ; 	
				"	SA24.A2_NOME NOME4"+chr(13)+chr(10)+ ;  
				"	FROM "+RetSqlName("SZ8")+" Z8 	JOIN "+RetSqlName("SZ5")+" SZ5"+chr(13)+chr(10)+ ; 	
				"	ON Z5_CONTRAT = Z8_CONTRAT"+chr(13)+chr(10)+ ; 	
				"	AND Z5_MEDICAO = Z8_MEDICAO"+chr(13)+chr(10)+ ; 	
				"	AND Z5_CLIENTE = Z8_CLIENTE"+chr(13)+chr(10)+ ; 	
				"	AND Z5_FILIAL = Z8_FILIAL"+chr(13)+chr(10)+ ; 	
				"	AND SZ5.D_E_L_E_T_ =' '"+chr(13)+chr(10)+ ;  	
				"	JOIN "+RetSqlName("SA1")+" SA11"+chr(13)+chr(10)+ ; 	
				"	ON Z5_CLIENTE = SA11.A1_COD"+chr(13)+chr(10)+ ; 	
				"	AND Z5_LOJA = SA11.A1_LOJA"+chr(13)+chr(10)+ ; 	
				"	AND SA11.D_E_L_E_T_ =' '"+chr(13)+chr(10)+ ;  	
				"	JOIN "+RetSqlName("SA1")+" SA12"+chr(13)+chr(10)+ ; 	
				"	ON Z5_MSFIL = SA12.A1_YFILRES"+chr(13)+chr(10)+ ; 	
				"	AND SA12.D_E_L_E_T_ =' '"+chr(13)+chr(10)+ ;  	
				"	JOIN "+RetSqlName("SA1")+" SA13"+chr(13)+chr(10)+ ; 	
				"	ON Z5_CLIENTE+Z5_LOJA = SA13.A1_COD+SA13.A1_LOJA"+chr(13)+chr(10)+ ;   	
				"	AND SA13.D_E_L_E_T_ =' '"+chr(13)+chr(10)+ ;  	
				"	JOIN "+RetSqlName("SA2")+" SA24"+chr(13)+chr(10)+ ; 	
				"	ON Z5_EMPPV3 = SA24.A2_YFILRES"+chr(13)+chr(10)+ ;  	
				"	AND SA24.D_E_L_E_T_ =' '"+chr(13)+chr(10)+ ;  	
				"	LEFT JOIN "+RetSqlName("SF1")+" (NOLOCK) SF11 ON"+chr(13)+chr(10)+ ;  //NOTA 01 		
				"	SF11.F1_FILIAL = Z8_EMP01 AND"+chr(13)+chr(10)+ ;  		
				"	SF11.F1_SERIE = Z8_SER01 AND"+chr(13)+chr(10)+ ;  		
				"	SF11.F1_DOC = Z8_NF01 AND"+chr(13)+chr(10)+ ;  		
				"	SF11.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+ ;  		
				"	Z8_NF01 <> ' '"+chr(13)+chr(10)+ ;  	
				"	LEFT JOIN "+RetSqlName("SF2")+" (NOLOCK) SF22 ON"+chr(13)+chr(10)+ ;  		
				"	SF22.F2_FILIAL = Z8_EMP01 AND"+chr(13)+chr(10)+ ;  		
				"	SF22.F2_SERIE = Z8_SER02 AND"+chr(13)+chr(10)+ ;   		
				"	SF22.F2_DOC = Z8_NF02 AND"+chr(13)+chr(10)+ ;   		
				"	SF22.F2_TIPO = 'D' AND"+chr(13)+chr(10)+ ;   		
				"	SF22.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+ ;
				"	Z8_NF02 <> ' '"+chr(13)+chr(10)+ ;  	
				"	LEFT JOIN "+RetSqlName("SF1")+" (NOLOCK) SF13 ON"+chr(13)+chr(10)+ ;  		
				"	SF13.F1_FILIAL = Z8_EMPMTZ AND"+chr(13)+chr(10)+ ;   		
				"	SF13.F1_SERIE = Z8_SERMTZ AND"+chr(13)+chr(10)+ ;   		
				"	SF13.F1_DOC = Z8_NFMTZ AND"+chr(13)+chr(10)+ ;   		
				"	SF13.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+ ;   
				"	SF13.F1_TIPO = 'D' AND"+chr(13)+chr(10)+ ;   				
				"	Z8_NFMTZ <> ' '"+chr(13)+chr(10)+ ;  	
				"	LEFT JOIN "+RetSqlName("SF1")+" (NOLOCK) SF14 ON"+chr(13)+chr(10)+ ;  		
				"	SF14.F1_FILIAL = Z8_EMPMTZ AND"+chr(13)+chr(10)+ ;   		
				"	SF14.F1_SERIE = Z8_SER02 AND"+chr(13)+chr(10)+ ;   		
				"	SF14.F1_DOC = Z8_NF02 AND"+chr(13)+chr(10)+ ;   		
				"	SF14.F1_TIPO = 'D' AND"+chr(13)+chr(10)+ ;   				
				"	SF14.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+ ;
				"	Z5_NF04 <> ' '"+chr(13)+chr(10)+ ;  
				"	WHERE Z8.D_E_L_E_T_ =' '"+chr(13)+chr(10)+ ;   
				"	AND Z8_CONTRAT BETWEEN '000154' AND '000154'"+chr(13)+chr(10)+ ;  
				"	AND Z8_MEDICAO BETWEEN '002860' AND '002900'"+chr(13)+chr(10)+ ;  
				"	AND SF11.F1_DOC+'/'+SF11.F1_SERIE <> ' '"+chr(13)+chr(10)+ ;
				"	AND SF22.F2_DOC+'/'+SF22.F2_SERIE <> ' '"+chr(13)+chr(10)+ ;
				"	AND SF13.F1_DOC+'/'+SF13.F1_SERIE <> ' '"+chr(13)+chr(10)+ ;
				"	GROUP BY "+chr(13)+chr(10)+ ;
				"	Z8_EMP01,"+chr(13)+chr(10)+ ;
				"	Z8_EMPMTZ, "+chr(13)+chr(10)+ ;	 	
				" 	SF11.F1_DOC+'/'+SF11.F1_SERIE ,"+chr(13)+chr(10)+ ;  	
				"	SF11.F1_VALBRUT ,"+chr(13)+chr(10)+ ;   	
				"	SF22.F2_DOC+'/'+SF22.F2_SERIE ,"+chr(13)+chr(10)+ ;  	
				"	SF22.F2_VALBRUT ,"+chr(13)+chr(10)+ ;  	
				"	SF11.F1_EMISSAO,"+chr(13)+chr(10)+ ;  	
				"	SF13.F1_DOC+'/'+SF13.F1_SERIE ,"+chr(13)+chr(10)+ ;  	
				"	SF13.F1_VALBRUT ,"+chr(13)+chr(10)+ ;  	
				"	SF14.F1_DOC+'/'+SF14.F1_SERIE ,"+chr(13)+chr(10)+ ;  	
				"	SF14.F1_VALBRUT,"+chr(13)+chr(10)+ ;  	
				"	SA11.A1_COD+'/'+SA11.A1_LOJA,"+chr(13)+chr(10)+ ;  	
				"	SA11.A1_NOME,"+chr(13)+chr(10)+ ; 	 	
				"	SA12.A1_COD+'/'+SA12.A1_LOJA,"+chr(13)+chr(10)+ ;  	
				"	SA12.A1_NOME,"+chr(13)+chr(10)+ ; 	 	
				"	SA13.A1_COD+'/'+SA13.A1_LOJA,"+chr(13)+chr(10)+ ;  	
				"	SA13.A1_NOME,"+chr(13)+chr(10)+ ; 	 	
				"	SA24.A2_COD+'/'+SA24.A2_LOJA,"+chr(13)+chr(10)+ ;  	
				"	SA24.A2_NOME,"+chr(13)+chr(10)+ ; 	 	
				"	SZ5.Z5_EMPPV1,"+chr(13)+chr(10)+ ; 	 	
				"	SZ5.Z5_EMPPV2,"+chr(13)+chr(10)+ ; 	 	
				"	SZ5.Z5_EMPPV3,"+chr(13)+chr(10)+ ; 	 	
				"	SZ5.Z5_EMPPC4,"+chr(13)+chr(10)+ ;	 	
				"	Z8.Z8_CONTRAT,"+chr(13)+chr(10)+ ; 	 	
				"	Z8.Z8_MEDICAO,"+chr(13)+chr(10)+ ; 	 	
				"	Z8.Z8_NF01,"+chr(13)+chr(10)+ ;	 	 	
				"	Z8.Z8_ENTRADA, "+chr(13)+chr(10)+ ;
				"	Z8.Z8_EMPMTZ,Z8.Z8_NF02"+chr(13)+chr(10)+ ; 	 
				"	ORDER BY SZ5.Z5_EMPPV1, Z8.Z8_CONTRAT, Z8.Z8_MEDICAO, Z8.Z8_NF01 ,Z8.Z8_NF02"	

Return cQuery


/*/{Protheus.doc} FQueryDev
Fun��o que retorna as Notas de Devolu��es
@author Diogo
@since 19/05/2015
@version 1.0
@param cNota, character, (C�digo da nota fiscal de origem)
@param cFil, character, (C�digo da filial da NF)
@param cTipo, character, (Tipo da Nota)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function FQueryDev(cNota,cFil,cTipo)

	Local cQuery := ""
	If cTipo $ '1_2_3' //Nota de devolu��o na Entrada

		cQuery:= 	" SELECT SD1.D1_DOC+'/'+SD1.D1_SERIE NOTA, "+chr(13)+chr(10)+ ;
					" 		D1_ITEM ITEM,"+chr(13)+chr(10)+ ;
					" 		SD1.D1_EMISSAO EMISSAO,"+chr(13)+chr(10)+ ;
					"		SD1.D1_COD PRODUTO,"+chr(13)+chr(10)+ ;
					"		SB1.B1_DESC DESCRICAO,"+chr(13)+chr(10)+ ;
					"		SD1.D1_QUANT QUANTIDADE,"+chr(13)+chr(10)+ ;
					"		SD1.D1_VUNIT VALOR,"+chr(13)+chr(10)+ ;
					"		SD1.D1_TOTAL TOTAL"+chr(13)+chr(10)+ ;
					"	FROM "+RetSqlName("SD1")+" SD1"+chr(13)+chr(10)+ ;
					"	JOIN "+RetSqlName("SB1")+" SB1 "+chr(13)+chr(10)+ ;
					"	ON B1_COD = D1_COD"+chr(13)+chr(10)+ ;
					"	AND SB1.D_E_L_E_T_=' '"
					
					If cTipo='1'
						cQuery+= " JOIN "+RetSqlName("SZ8")+" SZ8 "
						cQuery+= " ON Z8_NF01 = D1_DOC "	
						cQuery+= " AND Z8_SER01 = D1_SERIE "
						cQuery+= " AND Z8_EMP01 = D1_FILIAL  "
						cQuery+= " AND SZ8.D_E_L_E_T_ =' '  "
					Elseif cTipo='3'
						cQuery+= " JOIN "+RetSqlName("SZ8")+" SZ8 "
						cQuery+= " ON Z8_NFMTZ = D1_DOC "	
						cQuery+= " AND Z8_SERMTZ = D1_SERIE "
						cQuery+= " AND Z8_EMPMTZ = D1_FILIAL  "
						cQuery+= " AND SZ8.D_E_L_E_T_ =' '  "
					Endif
				

					cQuery+="	WHERE  D1_NFORI+'/'+D1_SERIORI  ='"+cNota+"' "+chr(13)+chr(10)+ ;
					"	AND D1_FILIAL ='"+cFil+"' "+chr(13)+chr(10)+ ;					
					"	AND SD1.D_E_L_E_T_ =' ' "+chr(13)+chr(10)+ ;
					"	ORDER BY SD1.D1_DOC+'/'+SD1.D1_SERIE,D1_ITEM "
	Else
	
		cQuery:= 	" SELECT SD2.D2_DOC+'/'+SD2.D2_SERIE NOTA, "+chr(13)+chr(10)+ ;
					" 		SD2.D2_ITEM ITEM,"+chr(13)+chr(10)+ ;
					" 		SD2.D2_EMISSAO EMISSAO,"+chr(13)+chr(10)+ ;
					"		SD2.D2_COD PRODUTO,"+chr(13)+chr(10)+ ;
					"		SB1.B1_DESC DESCRICAO,"+chr(13)+chr(10)+ ;
					"		SD2.D2_QUANT QUANTIDADE,"+chr(13)+chr(10)+ ;
					"		SD2.D2_PRCVEN VALOR,"+chr(13)+chr(10)+ ;
					"		SD2.D2_TOTAL TOTAL"+chr(13)+chr(10)+ ;
					"	FROM "+RetSqlName("SD2")+" SD2"+chr(13)+chr(10)+ ;
					"	JOIN "+RetSqlName("SB1")+" SB1 "+chr(13)+chr(10)+ ;
					"	ON B1_COD = D2_COD"+chr(13)+chr(10)+ ;
					"	AND SB1.D_E_L_E_T_=' '"+chr(13)+chr(10)+ ;
					"	WHERE  D2_NFORI+'/'+D2_SERIORI  ='"+cNota+"' "+chr(13)+chr(10)+ ;
					"	AND D2_FILIAL ='"+cFil+"' "+chr(13)+chr(10)+ ;
					"	AND SD2.D_E_L_E_T_ =' ' "+chr(13)+chr(10)+ ;
					"	ORDER BY SD2.D2_DOC+'/'+SD2.D2_SERIE,D2_ITEM "	
	Endif

	cSetSql := ChangeQuery(cQuery)

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY15ITEM", .F., .T.)

	DbSelectArea('QRY15ITEM')

	QRY15ITEM->(dbGoTop())
					
Return


/*/{Protheus.doc} FQryNFAvuls
Notas Fiscais Avulsas
@author Diogo
@since 19/05/2015
@version 1.0
@param cFornece, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function FQryNFAvuls(cClients)

	Local cQuery:= ""
	Local aClient:= strtokarr (cClients , ",")
	cQuery:= 	" SELECT D1_FILIAL, D1_DOC+'/'+D1_SERIE,D1_EMISSAO, "+chr(13)+chr(10)+ ;
				" D1_NFORI+'/'+D1_SERIORI DOCUMENTO, "+chr(13)+chr(10)+ ;
				" A1_COD+'/'+A1_LOJA CLIENTE,"+chr(13)+chr(10)+ ;
				" A1_NOME NOMECLI,			 "+chr(13)+chr(10)+ ;
				" SUM(D1_TOTAL) VALOR		 "+chr(13)+chr(10)+ ; 
				" FROM "+RetSqlName("SD1")+" SD1 "+chr(13)+chr(10)+ ;
				" JOIN "+RetSqlName("SA1")+" SA1"+chr(13)+chr(10)+ ; 
				" ON SA1.A1_COD = SD1.D1_FORNECE AND"+chr(13)+chr(10)+ ;
				" SA1.A1_LOJA = SD1.D1_LOJA "+chr(13)+chr(10)+ ;
				" LEFT JOIN "+RetSqlName("SZ8")+" Z81"+chr(13)+chr(10)+ ;
				" ON SD1.D1_FILIAL = Z81.Z8_EMP01 AND"+chr(13)+chr(10)+ ;
				" SD1.D1_SERIE = Z81.Z8_SER01 AND"+chr(13)+chr(10)+ ;
				" SD1.D1_DOC = Z81.Z8_NF01 AND "+chr(13)+chr(10)+ ;
				" SD1.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+ ;
				" SD1.D1_TIPO='D' AND"+chr(13)+chr(10)+ ;
				" Z81.Z8_NF01 <> ' ' "+chr(13)+chr(10)+ ;
				" LEFT JOIN "+RetSqlName("SZ8")+" Z83"+chr(13)+chr(10)+ ; 
				" ON SD1.D1_FILIAL = Z83.Z8_EMPMTZ AND"+chr(13)+chr(10)+ ;
				" SD1.D1_SERIE = Z83.Z8_SERMTZ AND"+chr(13)+chr(10)+ ;
				" SD1.D1_DOC = Z83.Z8_NFMTZ AND "+chr(13)+chr(10)+ ;
				" SD1.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+ ;
				" SD1.D1_TIPO='D' AND"+chr(13)+chr(10)+ ;
				" Z83.Z8_NFMTZ <> ' ' "+chr(13)+chr(10)+ ;
				" WHERE SD1.D1_TIPO ='D' "+chr(13)+chr(10)+ ;
				" AND Z81.Z8_CONTRAT IS NULL"+chr(13)+chr(10)+ ;
				" AND SD1.D1_FORNECE BETWEEN '"+mv_par05+"' AND '"+mv_par06+"' "+chr(13)+chr(10)+ ;
				" AND Z83.Z8_CONTRAT IS NULL "+chr(13)+chr(10)+ ;
				" AND SD1.D1_FORNECE+SD1.D1_LOJA IN ( "
				For i:=1 To len(aClient)
					cQuery+= "'"+aClient[i]+"',"
				Next
				cQuery:= substr(cQuery,1,len(cQuery)-1)
				cQuery+= " ) "
				cQuery+= " GROUP BY D1_FILIAL, D1_DOC+'/'+D1_SERIE,D1_EMISSAO,D1_NFORI+'/'+D1_SERIORI,D1_SERIE, "+chr(13)+chr(10)+ ;
				" A1_COD+'/'+A1_LOJA, A1_NOME " 

	cSetSql := ChangeQuery(cQuery)

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY15CTN", .F., .T.)

	DbSelectArea('QRY15CTN')

	QRY15CTN->(dbGoTop())
				
Return


/*/{Protheus.doc} FBuscDEVZ8
Retorna se j� foi dado entrada na Nota Fiscal de Devolu��o na Matriz
@author Diogo
@since 20/05/2015
@version 1.0
@param cNf, character, (Descri��o do par�metro)
@param cFil, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function FBuscDEVZ8(cNf,cFil)

	Local cQuery:= ""
	Local cRet	:= "SIM"
	cQuery:= 	" SELECT TOP 1 D1_DOC "+chr(13)+chr(10)+ ;
				" FROM "+RetSqlName("SD1")+" SD1 "+chr(13)+chr(10)+ ;
				" WHERE D1_TIPO ='D' "+chr(13)+chr(10)+ ;
				" AND D1_NFORI+'/'+D1_SERIORI = '"+cNf+"' "+chr(13)+chr(10)+ ;
				" AND D1_FILIAL = '"+cFil+"' "+chr(13)+chr(10)+ ;
				" AND D_E_L_E_T_ =  ' ' "
	cSetSql := ChangeQuery(cQuery)

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY15SF1", .F., .T.)

	DbSelectArea('QRY15SF1')

	QRY15SF1->(dbGoTop())
	
	If QRY15SF1->(eof())
		cRet:= 'NAO'
	Endif
		
Return cRet

Static Function Qry15Default()
	Local cQuery:= ""
	cQuery:=" SELECT "+chr(13)+chr(10)+ ;
			"	Z5_EMPPV1,Z5_EMPPV2,		"+chr(13)+chr(10)+ ;
			"	Z5_EMPPV3,		"+chr(13)+chr(10)+ ;
			"	Z5_EMPPC4,		"+chr(13)+chr(10)+ ;		
			"	Z5_NF01,"+chr(13)+chr(10)+ ;
			"	SA11.A1_COD+'/'+SA11.A1_LOJA CLIENTE1,"+chr(13)+chr(10)+ ;
			"	SA11.A1_NOME NOME1,"+chr(13)+chr(10)+ ;
			"	SF21.F2_DOC+'/'+SF21.F2_SERIE NOTA1,"+chr(13)+chr(10)+ ;
			"	SF21.F2_EMISSAO EMISSAONF1,"+chr(13)+chr(10)+ ;
			"	SF21.F2_VALBRUT VALORNF1,"+chr(13)+chr(10)+ ;
			"	Z5_NF02,"+chr(13)+chr(10)+ ;
			"	SA12.A1_COD+'/'+SA12.A1_LOJA CLIENTE2,"+chr(13)+chr(10)+ ;
			"	SA12.A1_NOME NOME2,"+chr(13)+chr(10)+ ;
			"	SF22.F2_DOC+'/'+SF22.F2_SERIE NOTA2,"+chr(13)+chr(10)+ ;
			"	SF22.F2_EMISSAO EMISSAONF2,"+chr(13)+chr(10)+ ;
			"	SF22.F2_VALBRUT VALORNF2,"+chr(13)+chr(10)+ ;
			"	Z5_NF03,"+chr(13)+chr(10)+ ; 
			"	SA13.A1_COD+'/'+SA13.A1_LOJA CLIENTE3,"+chr(13)+chr(10)+ ;
			"	SA13.A1_NOME NOME3,"+chr(13)+chr(10)+ ;
			"	SF23.F2_DOC+'/'+SF23.F2_SERIE NOTA3,"+chr(13)+chr(10)+ ;
			"	SF23.F2_EMISSAO EMISSAONF3,"+chr(13)+chr(10)+ ;
			"	SF23.F2_VALBRUT VALORNF3,"+chr(13)+chr(10)+ ;
			"	Z5_NF04,"+chr(13)+chr(10)+ ;
			"	SA24.A2_COD+'/'+SA24.A2_LOJA CLIENTE4,"+chr(13)+chr(10)+ ;
			"	SA24.A2_NOME NOME4,"+chr(13)+chr(10)+ ;
			"	SF14.F1_DOC+'/'+SF14.F1_SERIE NOTA4,"+chr(13)+chr(10)+ ;
			"	SF14.F1_EMISSAO EMISSAONF4,"+chr(13)+chr(10)+ ;
			"	SF14.F1_VALBRUT VALORNF4"+chr(13)+chr(10)+ ;
			"	FROM"+chr(13)+chr(10)+ ;
			"	SZ5010 SZ5"+chr(13)+chr(10)+ ;
			"	JOIN SZ8010 Z8 ON"+chr(13)+chr(10)+ ; 
			"	Z5_CONTRAT = Z8_CONTRAT AND Z8_MEDICAO = Z5_MEDICAO"+chr(13)+chr(10)+ ; 
			"	AND Z5_CLIENTE = Z8_CLIENTE"+chr(13)+chr(10)+ ; 	
			"	AND Z5_FILIAL = Z8_FILIAL"+chr(13)+chr(10)+ ;
			"	AND Z8.D_E_L_E_T_=' '"+chr(13)+chr(10)+ ;
			"	JOIN SA1010 SA11"+chr(13)+chr(10)+ ; 	
			"	ON Z5_CLIENTE = SA11.A1_COD"+chr(13)+chr(10)+ ; 	
			"	AND Z5_LOJA = SA11.A1_LOJA"+chr(13)+chr(10)+ ; 	
			"	AND SA11.D_E_L_E_T_ =' '"+chr(13)+chr(10)+ ;  	
			"	JOIN SA1010 SA12"+chr(13)+chr(10)+ ; 	
			"	ON Z5_MSFIL = SA12.A1_YFILRES"+chr(13)+chr(10)+ ; 	
			"	AND SA12.D_E_L_E_T_ =' '"+chr(13)+chr(10)+ ;  	
			"	JOIN SA1010 SA13"+chr(13)+chr(10)+ ; 	
			"	ON Z5_CLIENTE+Z5_LOJA = SA13.A1_COD+SA13.A1_LOJA"+chr(13)+chr(10)+ ;   	
			"	AND SA13.D_E_L_E_T_ =' '"+chr(13)+chr(10)+ ;  	
			"	JOIN SA2010 SA24"+chr(13)+chr(10)+ ; 	
			"	ON Z5_EMPPV3 = SA24.A2_YFILRES"+chr(13)+chr(10)+ ;  	
			"	AND SA24.D_E_L_E_T_ =' '"+chr(13)+chr(10)+ ;
			"	LEFT JOIN SF2010 (NOLOCK) SF21 ON"+chr(13)+chr(10)+ ;  		
			"	SF21.F2_FILIAL = Z5_EMPPV1 AND"+chr(13)+chr(10)+ ;  		
			"	SF21.F2_SERIE = Z5_SERIE1 AND"+chr(13)+chr(10)+ ;  		
			"	SF21.F2_DOC = Z5_NF01 AND"+chr(13)+chr(10)+ ;  		
			"	SF21.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+ ;  		
			"	Z5_NF01 <> ' '"+chr(13)+chr(10)+ ;  	
			"	LEFT JOIN SF2010 (NOLOCK) SF22 ON"+chr(13)+chr(10)+ ;  		
			"	SF22.F2_FILIAL = Z5_EMPPV2 AND"+chr(13)+chr(10)+ ;  		
			"	SF22.F2_SERIE = Z5_SERIE2 AND"+chr(13)+chr(10)+ ;  		
			"	SF22.F2_DOC = Z5_NF02 AND"+chr(13)+chr(10)+ ;  		
			"	SF22.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+ ;  		
			"	Z5_NF02 <> ' '"+chr(13)+chr(10)+ ;  	
			"	LEFT JOIN SF2010 (NOLOCK) SF23 ON"+chr(13)+chr(10)+ ;  		
			"	SF23.F2_FILIAL = Z5_EMPPV3 AND"+chr(13)+chr(10)+ ;  		
			"	SF23.F2_SERIE = Z5_SERIE3 AND"+chr(13)+chr(10)+ ;
			"	SF23.F2_DOC = Z5_NF03 AND"+chr(13)+chr(10)+ ;  		
			"	SF23.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+ ;  		
			"	Z5_NF03 <> ' '"+chr(13)+chr(10)+ ;  	
			"	LEFT JOIN SF1010 (NOLOCK) SF14 ON"+chr(13)+chr(10)+ ;  		
			"	SF14.F1_FILIAL = Z5_EMPPC4 AND"+chr(13)+chr(10)+ ;  		
			"	SF14.F1_SERIE = Z5_SERIE4 AND"+chr(13)+chr(10)+ ;  		
			"	SF14.F1_DOC = Z5_NF04 AND"+chr(13)+chr(10)+ ;  		
			"	SF14.D_E_L_E_T_ = ' ' AND"+chr(13)+chr(10)+ ;  		
			"	Z5_NF04 <> ' '"+chr(13)+chr(10)+ ;  	
			"	WHERE SZ5.D_E_L_E_T_=' '"+chr(13)+chr(10)+ ;
			"	AND Z5_CONTRAT BETWEEN '"+mv_par01+"' AND '"+mv_par02+"'"+chr(13)+chr(10)+ ;  
			"	AND Z5_MEDICAO BETWEEN '"+mv_par03+"' AND '"+mv_par04+"'"+chr(13)+chr(10)+ ;  
			" 	AND Z5_CLIENTE BETWEEN '"+mv_par05+"' AND '"+mv_par06+"'"+chr(13)+chr(10)+ ;			
			"	GROUP BY Z5_NF01,"+chr(13)+chr(10)+ ;
			"	Z5_NF02,"+chr(13)+chr(10)+ ;
			"	Z5_NF03,"+chr(13)+chr(10)+ ; 
			"	Z5_NF04,"+chr(13)+chr(10)+ ;
			"	SA11.A1_COD+'/'+SA11.A1_LOJA,"+chr(13)+chr(10)+ ;
			"	SA11.A1_NOME,"+chr(13)+chr(10)+ ;
			"	SA12.A1_COD+'/'+SA12.A1_LOJA,"+chr(13)+chr(10)+ ;
			"	SA12.A1_NOME,"+chr(13)+chr(10)+ ;
			"	SA13.A1_COD+'/'+SA13.A1_LOJA,"+chr(13)+chr(10)+ ;
			"	SA13.A1_NOME,"+chr(13)+chr(10)+ ;
			"	SA24.A2_COD+'/'+SA24.A2_LOJA,"+chr(13)+chr(10)+ ;
			"	SA24.A2_NOME,"+chr(13)+chr(10)+ ;
			"	SF21.F2_DOC+'/'+SF21.F2_SERIE,"+chr(13)+chr(10)+ ;
			"	SF22.F2_DOC+'/'+SF22.F2_SERIE,"+chr(13)+chr(10)+ ;
			"	SF23.F2_DOC+'/'+SF23.F2_SERIE,"+chr(13)+chr(10)+ ;
			"	SF14.F1_DOC+'/'+SF14.F1_SERIE,"+chr(13)+chr(10)+ ;
			"	SF21.F2_EMISSAO,"+chr(13)+chr(10)+ ;
			"	SF21.F2_VALBRUT,"+chr(13)+chr(10)+ ;
			"	SF22.F2_EMISSAO,"+chr(13)+chr(10)+ ;
			"	SF22.F2_VALBRUT,"+chr(13)+chr(10)+ ;
			"	SF23.F2_EMISSAO,"+chr(13)+chr(10)+ ;
			"	SF23.F2_VALBRUT,"+chr(13)+chr(10)+ ;
			"	SF14.F1_EMISSAO,"+chr(13)+chr(10)+ ;
			"	SF14.F1_VALBRUT,"+chr(13)+chr(10)+ ;
			"	Z5_EMPPV1,Z5_EMPPV2,		"+chr(13)+chr(10)+ ;
			"	Z5_EMPPV3,		"+chr(13)+chr(10)+ ;
			"	Z5_EMPPC4		"
	
	 
Return cQuery