#Include 'Protheus.ch'
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} CADSZ4
Aditivo
@author Saulo Gomes Martins
@since 15/09/2014
@version 1.0
/*/
User Function CADSZ4()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	Local oStructSZ1	:= Nil
	Local oStructSZ2	:= Nil
	Local oStructSZ4	:= FWFormStruct(1,"SZ4")
	Local oModel		:= FWLoadModel( 'CADSZ1' )
	Local nI
	//-----------------------------------------
	//Monta a estrutura do formul�rio com base no dicion�rio de dados
	//-----------------------------------------

	oModel:AddGrid("SZ4DETAIL", "SZ1MASTER"/*cOwner*/,oStructSZ4, ,/*bLinePost*/,/*bPre*/,/*bPost*/,{ |oModel, lCopia| LoadSZ4( oModel, lCopia ) }/*Carga*/)
	oModel:SetRelation("SZ4DETAIL",{{"Z4_FILIAL",'xFilial("SZ4")'},{"Z4_CONTRAT","Z1_CONTRAT"}},SZ4->(IndexKey(1)))


	oModel:GetModel("SZ4DETAIL" ):GetStruct():SetProperty("Z4_QTDADT",MODEL_FIELD_VALID,{||SZ4ValCampo()})

	oModel:GetModel("SZ1MASTER" ):GetStruct():SetProperty("*",MODEL_FIELD_WHEN,{||.F.})
	oModel:GetModel( 'SZ4DETAIL' ):SetNoInsertLine()
	oModel:GetModel( 'SZ4DETAIL' ):SetNoDeleteLine()
	oModel:SetVldActive( { |oModel| SZ4ValAct( oModel ) } )
	oModel:SetCommit({|oModel| SZ4Commit(oModel) },.F.)
	//oModel:SetCommit({|oModel| FWFormCommit(oModel) },.t.)

Return(oModel)

Static Function ViewDef()
	Local oModel		:= FWLoadModel( 'CADSZ4' )
	Local oView		:= FWLoadView('CADSZ1')
	Local oStructSZ4	:= FWFormStruct( 2, 'SZ4', {|cCampo| !(Alltrim(cCampo) $ "Z4_CONTRAT|Z4_CLIENTE|Z4_LOJA|Z4_NOME|Z4_EMISSAO|Z4_HORA|Z4_RESPON")} )
	//Local oCalc1		:= FWCalcStruct( oModel:GetModel( 'YCADSZ4CALC1') )
	//oView:AddField( 'VIEW_CALC3', oCalc1, 'YCADSZ4CALC1' )
	//oStructSZ2:RemoveField('Z3_MEDICAO')
	oView:SetModel(oModel)
	oView:AddGrid("SZ4DETAIL",oStructSZ4)
	oView:SetOwnerView( "SZ4DETAIL","GRID")
	//oView:SetOwnerView( 'VIEW_CALC4', 'RODAPE' )
	oView:EnableTitleView('SZ4DETAIL','Itens Aditivo Contratual')
	oView:SetProgressBar(.T.)
	//oView:SetViewProperty( '*', "GRIDSEEK"      )
	//oView:AddUserButton( 'Preencher com Qtd Total', 'CLIPS', { |oView| MedirTotal(oView) } )
Return oView

Static Function SZ4ValAct( oModel )
	Local lRet:= .T.
	Local cContrato
	If oModel:getOperation() == MODEL_OPERATION_UPDATE
		If	Empty(SZ1->Z1_USERAPR)
			oModel:SetErrorMessage('SZ1MASTER',,,,"ATEN��O", 'Contrato n�o liberado, n�o pode ser feito o Aditivo!', 'Realize a altera��o normal do contrato!')
			Return .F.
		EndIf
	EndIf
Return lRet

Static Function Total02(oModel,cTipo)
	Local nCont		:= 0
	Local nTotal		:= 0
	Local oModelSZ4	:= oModel:GetModel( 'SZ4DETAIL' )
	Local aSaveLines := FWSaveRows()
	Default cTipo		:= "TOTAL"
	For nCont := 1 To oModelSZ4:Length()
		oModelSZ4:GoLine( nCont )
		If oModelSZ4:IsDeleted()
			Loop
		EndIf
		If cTipo=="TOTAL"
			nTotal	+= oModelSZ4:GetValue('Z4_TOTAL')
		ElseIf cTipo=="QTDMED"
			nTotal	+= oModelSZ4:GetValue('Z4_QTDMED')
		EndIf
	Next
	FWRestRows( aSaveLines )
Return nTotal

Static Function SZ4ValCampo()
	Local lRet			:= .T.  //Flag de retorno
	Local oModel		:= FWModelActive()
	Local nOperacao	:= oModel:GetOperation() //Controle de operacao
	Local cContrato	:= oModel:GetModel( 'SZ1MASTER' ):GetValue('Z1_CONTRAT')
	If oModel:GetModel('SZ4DETAIL'):GetValue('Z4_SALDO')+oModel:GetModel('SZ4DETAIL'):GetValue('Z4_QTDADT')<0
		oModel:SetErrorMessage('SZ4DETAIL',"Z4_QTDADT",,,"ATEN��O", 'Quantidade aditivo n�o pode ser menor zero', 'Verifique o valor informado!')
		Return .F.
	EndIf
Return lRet

Static Function SZ4Commit(oModel)
	Local cContrato	:= oModel:GetModel( 'SZ1MASTER' ):GetValue('Z1_CONTRAT')
	Local nSaldo		:= 0
	Local nQtd			:= 0
	Local nVlTotal	:= 0
	Local nVlAdi		:= 0
	Local lRet			:= .T.
	Local nCont,nCont2,nParcela
	Local cQuery
	Local aDados		:= {}
	Local nQtdAdt		:= 0
	Local aStruct	:= oModel:GetModel( 'SZ4DETAIL' ):oFormModelStruct:GetFields()
	Local oModelSZ4	:= oModel:GetModel('SZ4DETAIL')
	Local aAreaSZ1	:= SZ1->(GetArea())
	Local aAreaSZ2	:= SZ2->(GetArea())
	Private lMsErroAuto	:= .F.
	SZ1->(DbSetOrder(1))	//Z1_FILIAL+Z1_CONTRAT
	SZ2->(DbSetOrder(1))	//Z2_FILIAL+Z2_CONTRAT+Z2_ITEM
	If	oModel:getOperation() == MODEL_OPERATION_UPDATE .OR.;
			oModel:getOperation() == MODEL_OPERATION_INSERT
		Begin transaction
			For nCont := 1 To oModelSZ4:Length()
				oModelSZ4:GoLine(nCont)
				If oModelSZ4:IsDeleted()
					Loop
				EndIf
				If oModelSZ4:GetValue("Z4_QTDADT")==0
					Loop
				EndIf
				RecLock("SZ4",.T.)
				For nCont2:=1 To Len(aStruct)
					SZ4->(&(aStruct[nCont2][MODEL_FIELD_IDFIELD]))	:= oModelSZ4:GetValue(aStruct[nCont2][MODEL_FIELD_IDFIELD])
				Next
				SZ4->Z4_FILIAL	:= xFilial("SZ4")
				SZ4->(MsUnLock())
				If SZ2->(DbSeek(xFilial("SZ2")+cContrato+oModelSZ4:GetValue('Z4_ITEM')))
					RecLock("SZ2",.F.)
				Else
					RecLock("SZ2",.T.)
					SZ2->Z2_ITEM		:= ""
					SZ2->Z2_PRODUTO	:= oModelSZ4:GetValue('Z4_PRODUTO')
					SZ2->Z2_DESC		:= ""
					SZ2->Z2_QUANT		:= 0
					SZ2->Z2_VLUNIT	:= 0
					SZ2->Z2_TOTAL		:= 0
					SZ2->Z2_TES		:= ""
					SZ2->Z2_QTDMED	:= 0
					SZ2->Z2_QTDADT	:= 0
					SZ2->Z2_CONTRAT	:= cContrato
				EndIf
				SZ2->Z2_QTDADT	+= oModelSZ4:GetValue('Z4_QTDADT')
				SZ2->Z2_SALDO		+= oModelSZ4:GetValue('Z4_QTDADT')
				nVlAdi				+= oModelSZ4:GetValue('Z4_QTDADT')*SZ2->Z2_VLUNIT
				//SZ2->Z2_TOTAL		:= (SZ2->Z2_QUANT+SZ2->Z2_QTDADT)*SZ2->Z2_VLUNIT
				//nVlTotal			+= SZ2->Z2_TOTAL
				nQtdAdt			+= oModelSZ4:GetValue('Z4_QTDADT')
				SZ2->(MsUnLock())
			Next
			If nQtdAdt<>0
				If !SZ1->(DbSeek(xFilial("SZ1")+cContrato))
					DisarmTransaction()
					oModel:SetErrorMessage('SZ4DETAIL',"",,,"ATEN��O", 'Problema ao atualizar saldo do contrato, contrato n�o encontrado.', 'Informe a TI!')
					SZ1->(RestArea(aAreaSZ1))
					SZ2->(RestArea(aAreaSZ2))
					Return .F.
				EndIf
				RecLock("SZ1",.F.)
				SZ1->Z1_SALDO		+= nQtdAdt
				//SZ1->Z1_VLADT		:= nVlTotal
				//SZ1->Z1_USERAPR		:= " "
				//SZ1->Z1_VLADD		+= oModelSZ4:GetValue('Z4_QTDADT')*SZ2->Z2_VLUNIT
				SZ1->(MsUnLock())
			/* OS ADITIVOS N�O IR� GERAR O TITULO DE IMEDIADO, O TITULO SER� GERADO MANUALMENTE!
			aCondicao	:= Condicao(Abs(nVlAdi),SZ1->Z1_CPFIN)
			If Empty(aCondicao)
				DisarmTransaction()
				oModel:SetErrorMessage('SZ4DETAIL',,,,"ERRO", 'Erro ao gerar parcelas do titulo', 'Verifique a condi��o de pagamento e o valor total!')
				Return .F.
			EndIf
			cQuery := "SELECT MAX(E1_PARCELA) AS E1_PARCELA FROM "+RetSqlName( 'SE1' ) + " SE1"
			cQuery += " WHERE E1_FILIAL='"+ xFilial( 'SE1' ) + "'"
			cQuery += " AND E1_NUM='"+SZ1->Z1_CONTRAT+"'"
			cQuery += " AND SE1.D_E_L_E_T_=' '"
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSE1",.F.,.T.)
			nParcela		:= 1
			If TMPSE1->(!EOF())
				nParcela	:= Val(RetAsc(TMPSE1->E1_PARCELA,GetSx3Cache("E1_PARCELA","X3_TAMANHO"),.F.))
			EndIf
			TMPSE1->(DbCloseArea())
			For nCont:=1 to Len(aCondicao)	//TODO VERIFICAR SE O ADITIVO IR� SEGUIR AS PARCELAS
				nParcela++
				AADD(aDados,{"E1_PREFIXO"	,"SZ1"					,nil})
				AADD(aDados,{"E1_NUM"		,SZ1->Z1_CONTRAT		,nil})
				AADD(aDados,{"E1_TIPO"		,If(nVlAdi>0,"DP ","NCC")					,nil})
				AADD(aDados,{"E1_PARCELA"	,RetAsc(nParcela,GetSx3Cache("E1_PARCELA","X3_TAMANHO"),.T.),nil})
				AADD(aDados,{"E1_NATUREZ"	,SZ1->Z1_NATUREZ		,nil})
				AADD(aDados,{"E1_CLIENTE"	,SZ1->Z1_CLIENTE		,nil})
				AADD(aDados,{"E1_LOJA"		,SZ1->Z1_LOJA			,nil})
				AADD(aDados,{"E1_VENCTO"		,aCondicao[nCont][1]	,nil})
				AADD(aDados,{"E1_NATUREZ"	,SZ1->Z1_NATUREZ		,nil})
				AADD(aDados,{"E1_VALOR"		,aCondicao[nCont][2]	,nil})
				MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
				lMsErroAuto	:= .F.
				If lMsErroAuto
					DisarmTransaction()
					AutoGrLog("Erro ao incluir o titulo!")
					AutoGrLog("Opera��o cancelada!")
					MostraErro()
					oModel:SetErrorMessage('SZ4DETAIL',,,,"ERRO", 'Erro ao incluir o titulo!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				Endif
			Next*/
			EndIf
		End Transaction
	EndIf
	SZ1->(RestArea(aAreaSZ1))
	SZ2->(RestArea(aAreaSZ2))
Return lRet

Static Function LoadSZ4( oGrid, lCopia )
	Local aArea   := GetArea()
	Local aFields := {}
	Local aRet    := {}//FormLoadGrid(oGrid)
	Local cTmp    := ''
	Local cQuery  := ''
	Local cFields := 'R_E_C_N_O_'
	Local nTam
	Local aStruct	:= oGrid:oFormModelStruct:GetFields()
	Local cProdIni	:= cProdFim	:= Space(GetSx3Cache("B1_COD","X3_TAMANHO"))
	Local cCategoria	:= Space(GetSx3Cache("B1_YCATE","X3_TAMANHO"))
	Local cSerie		:= Space(GetSx3Cache("B1_YSERIE","X3_TAMANHO"))
	Local cVolume		:= Space(GetSx3Cache("B1_YVOL","X3_TAMANHO"))
	Local cTes			:= Space(GetSx3Cache("Z2_TES","X3_TAMANHO"))
	Local aParam		:= {}
	Local aRetParm	:= {}
	Local lOk			:= .F.
	aAdd(aParam,{1,"Produto De"		,cProdIni	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","SB1",".T.",80,.F.})
	aAdd(aParam,{1,"Produto Ate"	,cProdFim	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","SB1",".T.",80,.F.})
	aAdd(aParam,{1,"Categoria"		,cCategoria,GetSx3Cache("B1_YCATE","X3_PICTURE"),".T.","Z7",".T.",80,.F.})
	aAdd(aParam,{1,"Serie"			,cSerie	,GetSx3Cache("B1_YSERIE","X3_PICTURE"),".T.","Z1",".T.",80,.F.})
	aAdd(aParam,{1,"Volume"			,cVolume	,GetSx3Cache("B1_YVOL","X3_PICTURE"),".T.","Z3",".T.",80,.F.})
	aAdd(aParam,{1,"Tes"				,cTes		,GetSx3Cache("Z2_TES","X3_PICTURE"),".T.","SF4",".T.",80,.F.})
	If ParamBox(aParam,"Filtrar Medi��o",@aRetParm,{||.T.},,,,,,"U_CADSZ3",.T.,.T.)
		lOk	:= .T.
	EndIf
	oModel 		:= FWModelActive()

	// Pega campos que fazem parte da estrutura do objeto, para otimizar retorno da query
	cTmp   := GetNextAlias()
	cQuery := ""
	cQuery += "SELECT Z2_ITEM,Z2_PRODUTO,Z2_DESC,Z2_VLUNIT,Z2_QUANT,Z2_QTDADT,Z2_SALDO FROM "+RetSqlName( 'SZ2' ) + " SZ2"
	cQuery += " LEFT JOIN "+RETSQLNAME("SB1")+" SB1"
	cQuery += " ON B1_FILIAL='"+xFilial("SB1")+"' AND B1_COD=Z2_PRODUTO AND SB1.D_E_L_E_T_=' '"
	cQuery += " WHERE Z2_FILIAL='"+ xFilial( 'SZ2' ) + "'"
	cQuery += " AND Z2_CONTRAT='"+SZ1->Z1_CONTRAT+"' "
	If lOk
		cQuery += " AND Z2_PRODUTO>='"+aRetParm[1]+"' and Z2_PRODUTO<='"+aRetParm[2]+"' "
		If !Empty(aRetParm[3])
			cQuery += " AND B1_YCATE='"+aRetParm[3]+"' "
		EndIf
		If !Empty(aRetParm[4])
			cQuery += " AND B1_YSERIE='"+aRetParm[4]+"' "
		EndIf
		If !Empty(aRetParm[5])
			cQuery += " AND B1_YVOL='"+aRetParm[5]+"' "
		EndIf
		If !Empty(aRetParm[6])
			cQuery += " AND Z2_TES='"+aRetParm[6]+"' "
		EndIf
	EndIf
	cQuery += " AND SZ2.D_E_L_E_T_=' '"
	cQuery += " ORDER BY Z2_ITEM"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.F.,.T.)
	While (cTmp)->(!EOF())
		AADD(aRet,{0,{}})
		nTam	:= Len(aRet)
		For nCont:=1 To Len(aStruct)
			AADD(aRet[nTam][2],CriaVar(aStruct[nCont][MODEL_FIELD_IDFIELD]))
			If aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_FILIAL"
				aRet[nTam][2][nCont]	:= xFilial("SZ4")
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_CONTRAT"
				aRet[nTam][2][nCont]	:= SZ1->Z1_CONTRAT
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_ITEM"
				aRet[nTam][2][nCont]	:= (cTmp)->Z2_ITEM
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_CLIENTE"
				aRet[nTam][2][nCont]	:= SZ1->Z1_CLIENTE
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_LOJA"
				aRet[nTam][2][nCont]	:= SZ1->Z1_LOJA
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_NOME"
				aRet[nTam][2][nCont]	:= SZ1->Z1_NOME
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_PRODUTO"
				aRet[nTam][2][nCont]	:= (cTmp)->Z2_PRODUTO
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_DESC"
				aRet[nTam][2][nCont]	:= (cTmp)->Z2_DESC
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_QTDORIG"
				aRet[nTam][2][nCont]	:= (cTmp)->(Z2_QUANT+Z2_QTDADT)
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_QTDADT"
				aRet[nTam][2][nCont]	:= 0
			ElseIf aStruct[nCont][MODEL_FIELD_IDFIELD]=="Z4_SALDO"
				aRet[nTam][2][nCont]	:= (cTmp)->Z2_SALDO
			EndIf
		Next
		(cTmp)->(DbSkip())
	EndDo
	(cTmp)->(dbCloseArea())
	RestArea(aArea)
Return aRet