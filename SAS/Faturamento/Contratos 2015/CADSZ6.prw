#Include 'Protheus.ch'
#INCLUDE 'FWMBROWSE.CH'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} CADSZ6
Kit de produtos
@author Saulo Gomes Martins
@since 16/09/2014
@version 1.0
/*/
User Function CADSZ6()
	Local oBrowse
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('SZ6')
	oBrowse:SetDescription('Kit')
	oBrowse:DisableDetails()
	oBrowse:SetMenuDef( 'CADSZ6' )
	oBrowse:Activate()
Return

Static Function MenuDef()
	Local aRotina := {}
	If FWIsInCallStack("U_CADSZ1")
		ADD OPTION aRotina TITLE "Selecionar"  ACTION "U_YSZ1005"			OPERATION 2 ACCESS 0
	Else
		ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PesqBrw"			OPERATION 1 ACCESS 0 DISABLE MENU
		ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.CADSZ6"	OPERATION 2 ACCESS 0 DISABLE MENU
		ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.CADSZ6"	OPERATION 3 ACCESS 0
		ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.CADSZ6"	OPERATION 4 ACCESS 143
		ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.CADSZ6"	OPERATION 5 ACCESS 144
		ADD OPTION aRotina TITLE "Imprimir"   ACTION "VIEWDEF.CADSZ6"	OPERATION 8 ACCESS 0
		ADD OPTION aRotina TITLE "Copiar"     ACTION "VIEWDEF.CADSZ6"	OPERATION 9 ACCESS 0
	EndIF
Return aRotina


//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	Local oStructSZ6 := Nil
	Local oStructSZ7 := Nil
	Local oModel     := Nil
	//-----------------------------------------
	//Monta a estrutura do formul�rio com base no dicion�rio de dados
	//-----------------------------------------
	oStructSZ6 := FWFormStruct(1,"SZ6")
	oStructSZ7 := FWFormStruct(1,"SZ7")
	oStructSZ7:AddTrigger( ;
		"Z7_PRODUTO"		,;										//[01] Id do campo de origem
		"Z7_DESC"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SB1",1,xFilial("SB1")+oModel:GetValue('Z7_PRODUTO'),"B1_DESC") }  )	// [04] Bloco de codigo de execu��o do gatilho
	
	//-----------------------------------------
	//Monta o modelo do formul�rio
	//-----------------------------------------	
	oModel:= MPFormModel():New("YCADSZ6",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
	oModel:AddFields("SZ6MASTER",/*cOwner*/, oStructSZ6 ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)
	
	oModel:AddGrid("SZ7DETAIL", "SZ6MASTER"/*cOwner*/,oStructSZ7, ,/*bLinePost*/,/*bPre*/,/*bPost*/,/*Carga*/)
	oModel:SetPrimaryKey({"Z6_FILIAL","Z6_COD"})
	
	oModel:GetModel("SZ7DETAIL"):SetDescription("Itens do Kit")
	oModel:SetRelation("SZ7DETAIL",{{"Z7_FILIAL",'xFilial("SZ7")'},{"Z7_COD","Z6_COD"}},SZ7->(IndexKey(1)))
		
	oModel:AddCalc( 'YCADSZ6CALC1', 'SZ6MASTER', 'SZ7DETAIL', 'Z7_PRODUTO', 'Z7__TOT01', 'COUNT',,,'Quant Reg', )
	oModel:AddCalc( 'YCADSZ6CALC1', 'SZ6MASTER', 'SZ7DETAIL', 'Z7_QUANT', 'Z7__TOT02', 'SUM',,,'Quant Total', )
Return(oModel)


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
	Local oStructSZ6	:= FWFormStruct( 2, 'SZ6' )
	Local oStructSZ7	:= FWFormStruct( 2, 'SZ7' )
	Local oModel		:= FWLoadModel( 'CADSZ6' )
	Local oView
	Local oCalc1
	oCalc1	:= FWCalcStruct( oModel:GetModel( 'YCADSZ6CALC1') )
	oStructSZ7:RemoveField( 'Z7_COD' )
	oView	:= FWFormView():New()
	
	//oView:SetUseCursor(.F.)
	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)
	
	oView:AddField("SZ6MASTER",oStructSZ6)
	oView:AddGrid("SZ7DETAIL",oStructSZ7)
	oView:AddField( 'VIEW_CALC', oCalc1, 'YCADSZ6CALC1' )
	
	oView:CreateHorizontalBox("CABEC",20)
	oView:CreateHorizontalBox("GRID",70)
	oView:CreateHorizontalBox("RODAPE",10)
	
	oView:EnableTitleView('SZ7DETAIL','Produtos do Kit')
	
	oView:SetOwnerView( "SZ6MASTER","CABEC")
	oView:SetOwnerView( "SZ7DETAIL","GRID")
	oView:SetOwnerView( "VIEW_CALC","RODAPE")
	
	oView:SetProgressBar(.T.)
Return oView