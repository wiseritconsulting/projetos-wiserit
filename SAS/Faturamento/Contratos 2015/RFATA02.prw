#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWIZARD.CH"
#INCLUDE "TOPCONN.CH"

/*/{Protheus.doc} RFATA02
Importa��o das Tabelas
@author Diogo
@since 05/09/2014
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function RFATA02

	// ######################################################
	//		Variaveis
	// ######################################################

	Local oWizard
	Local nMetGlob
	Local nMetParc
	Local oRadioArq
	Local nRadioArq	:= 1
	Local cText
	Local cFile		:= Replicate( " ", 80 )
	Local cHeader 	:= "Importa��o de dados"
	Local cTpArq	:= "Delimitado (*.csv)|*.csv|"
	Local cDelim	:= AllTrim(SuperGetMV("MV_TPDELI",.F.,';'))
	Local nLinCabec	:= 1	// Padr�o sem linha de cabe�alho
	Local cCabec	:= ""	// String com o cabe�alho do arquivo original, se houver
	Local nQtdCab	:= 1	// String com o cabe�alho do arquivo original, se houver
	Local cNmAlias	:= "SZ2"
	Local cTipo		:= "2" //Importa��o + Log

	Private INCLUI	:= .T.
	Private ALTERA	:= .F.

	cText 	:= 	 "Esta rotina tem por objetivo importar registros, atrav�s " + ;
				 "de um arquivo padr�o CSV (delimitado) , e armazena-los na tabela "+ ;
				 "correspondente do sistema."+ CRLF + ;
				 "Os nomes das colunas devem ser os mesmos nomes de campos a serem atualizados."+ CRLF + CRLF + ;
				 "Ao final da importa��o ser� gerado um arquivo de log contendo as "+ ;
				 "inconsist�ncias."

	// ######################################################
	//		Primeiro Painel - Abertura
	// ######################################################

	DEFINE WIZARD oWizard		TITLE "Importa��o de dados" ;
								HEADER cHeader ;
								MESSAGE "Apresenta��o." ;
								TEXT cText ;
								NEXT { || .T. } ;
								FINISH {|| .T.} PANEL

	// ######################################################
	//		Segundo Painel - Arquivo e Contrato
	// ######################################################

	CREATE PANEL oWizard 	HEADER cHeader ;
							MESSAGE "Selecione os tabela que deseja importar" ;
							BACK {|| .T. } ;
							NEXT {|| .T. } ;
							FINISH {|| .F. } ;
							PANEL

	oPanel := oWizard:GetPanel( 2 )

	@ 15, 08 GROUP oGrpCon 	TO 120, 230 LABEL "Cadastro a ser importado" OF oPanel PIXEL DESIGN

	@ 25,35 Radio oRadioArq Var nRadioArq Items	"Contrato",;
												"Refer�ncias Clientes",;
												"T�tulos a Receber";
												3D 	Size 170,10 Of oPanel PIXEL DESIGN ;
												ON CHANGE ImpChgRadio(nRadioArq,@cNmAlias)


	// ######################################################
	//		Segundo Painel - Arquivo e Contrato
	// ######################################################

	CREATE PANEL oWizard 	HEADER cHeader ;
							MESSAGE "Selecione o arquivo para importa��o." ;
							BACK {|| .T. } ;
							NEXT {|| ! empty( cDelim ) .and. ! empty( cFile ) } ;
							FINISH {|| .F. } ;
							PANEL

	oPanel := oWizard:GetPanel( 3 )

	@ 10, 08 GROUP oGrpCon 	TO 40, 280 LABEL "Selecione um arquivo." ;
								OF oPanel ;
								PIXEL ;
								DESIGN

	@ 20, 15 MSGET oArq 	VAR cFile WHEN .F. OF oPanel SIZE 140, 10 PIXEL ;
							MESSAGE "Utilize o bot�o ao lado para selecionar" ;

	DEFINE SBUTTON oButArq 	FROM 21, 160 ;
								TYPE 14 ;
								ACTION cFile := cGetFile(cTpArq, , 0, "SERVIDOR\", .T., GETF_LOCALHARD + GETF_NETWORKDRIVE) ;
								OF oPanel ;
								ENABLE


	@ 50, 08 GROUP oGrpCon 	TO 130, 280 LABEL "Informe as configura��es do arquivo." ;
								OF oPanel ;
								PIXEL ;
								DESIGN

  	@ 60,20 SAY "Delimitador" OF oPanel SIZE 35,8 PIXEL
	@ 60,60 MSGET oDelim	VAR cDelim  ;
							PICTURE "@!" ;
							VALID !empty(cDelim) ;
							MESSAGE "Informe um delimitador de campo." ;
							OF oPanel SIZE 10,8 PIXEL

  	@ 80,20 SAY "Tipo" OF oPanel SIZE 35,8 PIXEL
	@ 80,60 COMBOBOX oTipo Var cTipo ITEMS {"1=Somente Log","2=Log + Importa��o"} SIZE 200,010 OF oPanel PIXEL

	// ######################################################
	//		Terceiro Painel - Confirmacao  / Processamento
	// ######################################################

	CREATE PANEL oWizard 	HEADER cHeader ;
							MESSAGE "Confirma��o dos dados e in�cio de processamento." ;
							BACK {|| .T. } ;
							NEXT {|| .T. } ;
							FINISH {|| .F. } ;
							PANEL

	oPanel := oWizard:GetPanel( 4 )

	@ 010, 010 SAY "Arquivo" OF oPanel SIZE 140, 8 PIXEL
	@ 010, 050 SAY cFile  OF oPanel SIZE 140, 8 COLOR CLR_HBLUE PIXEL

	@ 030, 010 SAY  "Delimitador" OF oPanel SIZE 140, 8 PIXEL
	@ 030, 050 SAY  cDelim  OF oPanel SIZE 140, 8 COLOR CLR_HBLUE PIXEL


	@ 050, 010 SAY  "Alias" OF oPanel SIZE 140, 8 PIXEL
	@ 050, 050 SAY  cNmAlias  OF oPanel SIZE 140, 8 COLOR CLR_HBLUE PIXEL


	@ 070, 010 SAY  "Tipo Proc.:" OF oPanel SIZE 140, 8 PIXEL
	@ 070, 050 SAY  IIf(cTipo=="1","Somente Log","Log+Importa��o")  OF oPanel SIZE 140, 8 COLOR CLR_HBLUE PIXEL


	// ######################################################
	//		Quarto Painel - Processamento
	// ######################################################

	CREATE PANEL oWizard 	HEADER cHeader ;
							MESSAGE "Processamento da Importa��o." ;
							BACK {|| .F. } ;
							NEXT {|| .T. } ;
							FINISH {|| .T. } ;
							EXEC {|| CursorWait(), IMPCADPro( oMetGlob, nRadioArq, cFile, cDelim, cTipo ), CursorArrow() } ;
							PANEL

	oPanel := oWizard:GetPanel( 5 )

	@ 25, 30 SAY "Importa��o" OF oPanel SIZE 140, 8 PIXEL
	@ 40, 30 METER oMetGlob 	VAR nMetGlob ;
								TOTAL 100 ;
								SIZE 224,10 OF oPanel PIXEL UPDATE DESIGN ;
								BARCOLOR CLR_BLACK,CLR_WHITE ;
								COLOR CLR_WHITE,CLR_BLACK ;
							 	NOPERCENTAGE

	ACTIVATE WIZARD oWizard CENTER

Return NIL


// #########################################################################################
// Programa  : Import
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 28/06/13 | Diogo     |
// ---------+-------------------+-----------------------------------------------------------
// #########################################################################################

Static Function IMPCADPro( oMetGlob, nRadioArq, cFile, cDelim,cTipo )

	// ######################################################
	//		Declaracao de Variaveis
	// ######################################################

	Local aArea		:= GetArea()
	Local lFirst	:= .T.
	Local cLinha 	:= ""
	Local aHeader	:= {}
	Local nHdl		:= 	0
	Local cEnvServ	:= GetEnvServer()
	Local cIniFile	:= GetADV97()
	Local cEnd		:= GetPvProfString(cEnvServ,"StartPath","",cIniFile)
	Local cDtHr		:= DtoS(dDataBase)+"-"+Substr(time(),1,2)+"-"+Substr(time(),4,2)+"-"+Substr(time(),7,2)
	Local cPath		:= "\IMPORT\"
	Local cTipoLog	:= "Import_"
	Local cNomeLog	:=	cPath+cTipoLog+cDtHr+"_Log.txt"
	Local cArq		:=	'c:\temp\testediogo.txt' //cEnd+cNomeLog
	Local cLin		:= ""
	Local cCdAlias	:= ""
	Local nQtReg	:= 0
	Local nQtNOk	:= 0
	Local nQtOk		:= 0
	Local aLog		:= {}
	Local lGrava	:= (cTipo == "2")
	Local cRotina	:= ""
	Local nCont		:= 0

	MAKEDIR(cEnd+cPath)

	// ######################################################
	//		Validacao do arquivo para importacao
	// ######################################################

	If !File(cFile) .OR. Empty(cFile)
		ApMsgStop("Problemas com arquivo informado!")
		RestArea(aArea)
		Return
	EndIf

	// ######################################################
	//		Identifica Alias de importacao
	// ######################################################

	Do Case
		Case nRadioArq == 1		// Clientes
			cCdAlias	:= "SZ2"
			cRotina	:= "SZ2"

		Case nRadioArq == 2		// Referencias Clientes
			cCdAlias	:= "SAO"
			cRotina	:= "SAO"

		Case nRadioArq == 3		// Titulos a Receber
			cCdAlias	:= "SE1"
			cRotina	:= "FINA040"

		OtherWise
			ApMsgStop("Nao existe tratamento para importa��o deste tipo de arquivo!")
			Return
	EndCase

	// ######################################################
	//		Inicia Log
	// ######################################################

	AAdd(aLog, Replicate( '=', 80 ) )
	AAdd(aLog, 'INICIANDO O LOG - I M P O R T A C A O   D E   D A D O S' )
	AAdd(aLog, Replicate( '-', 80 ) )
	AAdd(aLog, 'DATABASE...........: ' + DtoC( dDataBase ) )
	AAdd(aLog, 'DATA...............: ' + DtoC( Date() ) )
	AAdd(aLog, 'HORA...............: ' + Time() )
	AAdd(aLog, 'ENVIRONMENT........: ' + GetEnvServer() )
	AAdd(aLog, 'PATCH..............: ' + GetSrvProfString( 'StartPath', '' ) )
	AAdd(aLog, 'ROOT...............: ' + GetSrvProfString( 'RootPath', '' ) )
	AAdd(aLog, 'VERS�O.............: ' + GetVersao() )
	AAdd(aLog, 'M�DULO.............: ' + 'SIGA' + cModulo )
	AAdd(aLog, 'EMPRESA / FILIAL...: ' + SM0->M0_CODIGO + '/' + SM0->M0_CODFIL )
	AAdd(aLog, 'NOME EMPRESA.......: ' + Capital( Trim( SM0->M0_NOME ) ) )
	AAdd(aLog, 'NOME FILIAL........: ' + Capital( Trim( SM0->M0_FILIAL ) ) )
	AAdd(aLog, 'USU�RIO............: ' + SubStr( cUsuario, 7, 15 ) )
	AAdd(aLog, 'TABELA IMPORT......: ' + cCdAlias )
	AAdd(aLog, 'ARQUIVO IMPORT.....: ' + cFile )
	AAdd(aLog, 'DELIMITADOR........: ' + cDelim )
	AAdd(aLog, 'MODO PROCESSAMENTO.: ' + IIf(lGrava,"Atualizacao","Simulacao") )
	AAdd(aLog, Replicate( ':', 80 ) )
	AAdd(aLog, '' )

	AAdd(aLog, "Import = INICIO - Data "+DtoC(dDataBase)+ " as "+Time() )

	// ######################################################
	//		Leitura do arquivo
	// ######################################################

	FT_FUSE(cFile)

	nTot	:= FT_FLASTREC()
	nAtu	:= 0

	oMetGlob:SetTotal(nTot)
	CursorWait()

	FT_FGOTOP()

	While !FT_FEOF()

		nAtu++
		oMetGlob:Set(nAtu)

		cLinha := LeLinha() //FT_FREADLN()

		If Empty(cLinha)
			FT_FSKIP()
			Loop
		EndIf

		// ######################################################
		//		Tratamento de colunas
		// ######################################################

		aCols	:= {}
		aCols	:= TrataCols(cLinha,cDelim)

		If lFirst

			aHeader := aClone(aCols)
			lFirst := .F.

			// ######################################################
			//		Valida nomes das colunas
			// ######################################################

			cCpos := ImpVldCols(cCdAlias,aHeader)

			If !Empty(cCpos)
				ApMsgStop("Problemas na estrutura do arquivo, faltam as seguintes colunas "+cCpos)
				Return
			EndIf

		Else

			nQtReg++

			// ######################################################
			//		Validacao de campos obrigatorios
			// ######################################################

			cMsg := ImpObrigat(cCdAlias,aCols,aHeader)

			If !Empty(cMsg)
				AtuLog("NO MOT: CAMPOS OBRIGATORIOS - REGISTRO IGNORADO - "+cMsg,@aLog,nAtu)
				nQtNOk++
				FT_FSKIP()
				Loop
			EndIf

			// ######################################################
			//		Chamada de rotina automatica de inclusao
			// ######################################################

			If lGrava

				aRet := {}
				aRet := ImpGrava(cCdAlias,cRotina,aCols,aHeader)

				If aRet[1]
					nQtOk++
					AtuLog("OK MOT:REGISTRO INCLUIDO"+aRet[2],@aLog,nAtu)
				Else
					AtuLog("NO MOT: PROBLEMAS NA GRAVACAO ROTINA AUTOMATICA - "+cRotina+" - "+aRet[2],@aLog,nAtu)
					nQtNOk++
				EndIF

			Else

				nQtOk++
				AtuLog("OK MOT:REGISTRO INCLUIDO",@aLog,nAtu)

			EndIf
		EndIf

		FT_FSKIP()
	End

	FT_FUSE()

	AAdd(aLog, "Import = Total de Registros = "+ Alltrim(Str(nQtReg)))
	AAdd(aLog, "Import = Registros Nao importados = "+ Alltrim(Str(nQtNOk)))
	AAdd(aLog, "Import = Registros importados = "+ Alltrim(Str(nQtOk)))
	AAdd(aLog, "Import = FIM Data "+DtoC(dDataBase)+ " as "+Time() )

	// ######################################################
	//		Finaliza arquivo de Log
	// ######################################################

	nHdl  := 	fCreate(cArq)

	If nHdl == -1
		MsgAlert("O arquivo  "+cArq+" nao pode ser criado!","Atencao!")
		fClose(nHdl)
		fErase(cArq)
		RestArea(aArea)
	 	Return()
	EndIf

	For nCont:=1 to Len(aLog)

		cLin += aLog[nCont] + CHR(13)+CHR(10)

		If fWrite(nHdl,cLin,Len(cLin)) != Len(cLin)
			fClose(nHdl)
		    fErase(cArq)
		    cLin:=""
			RestArea(aArea)
		    Return()
		EndIf

		cLin:=""

	Next

	fClose(nHdl)


	u_RefazSZ1() //Refaz SZ1
	ApMsgInfo("Verifique arquivo de log "+cArq)

	RestArea(aArea)

Return


// #########################################################################################
// Programa  : AtuLog
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 28/06/13 | Diogo     |
// ---------+-------------------+-----------------------------------------------------------
// #########################################################################################

Static Function AtuLog(cMsg,aLog,nAtu)

	AAdd(aLog, " Import = Linha " + StrZero(nAtu,12) + " = " + " LOG = " + cMsg)

Return


// #########################################################################################
// Programa  : LeLinha
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 28/06/13 | Diogo     |
// ---------+-------------------+-----------------------------------------------------------
// #########################################################################################

Static Function LeLinha()

	Local cLinhaTmp		:= ""
	Local cLinhaM100	:= ""

	cLinhaTmp := FT_FReadLN()

	If !Empty(cLinhaTmp)

		cIdent:= Substr(cLinhaTmp,1,1)

		If Len(cLinhaTmp) < 1023
			cLinhaM100 := cLinhaTmp
		Else

			cLinAnt := cLinhaTmp
			cLinhaM100 += cLinAnt

			Ft_FSkip()

			cLinProx:= Ft_FReadLN()

			If Len(cLinProx) >= 1023 .and. Substr(cLinProx,1,1) <> cIdent

				While Len(cLinProx) >= 1023 .and. Substr(cLinProx,1,1) <> cIdent .and. !Ft_fEof()

					cLinhaM100 += cLinProx

					Ft_FSkip()

					cLinProx := Ft_fReadLn()

					If Len(cLinProx) < 1023 .and. Substr(cLinProx,1,1) <> cIdent
						cLinhaM100 += cLinProx
					Endif

				Enddo

			Else
				cLinhaM100 += cLinProx
			Endif

		Endif

	Endif

Return(cLinhaM100)


// #########################################################################################
// Programa  : TrataCols
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 28/06/13 | Diogo     |
// ---------+-------------------+-----------------------------------------------------------
// #########################################################################################

Static Function TrataCols(cLinha,cSep)

	Local aRet		:= {}
	Local nPosSep	:= 0

	nPosSep	:= At(cSep,cLinha)

	While nPosSep <> 0

		AAdd(aRet, SubStr(cLinha,1,nPosSep-1)  )

		cLinha	:= SubStr(cLinha,nPosSep+1)
	 	nPosSep	:= At(cSep,cLinha)

	EndDo

	AAdd(aRet, cLinha )

Return aRet


// #########################################################################################
// Programa  : RetCol
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 28/06/13 | Diogo     |
// ---------+-------------------+-----------------------------------------------------------
// #########################################################################################

Static Function RetCol(cCpo,aCols,aHeader)

	Local cRet		:= ""
	Local nPos		:= 0
	Local aSX3Area	:= SX3->(GetArea())

	nPos := AScan(aHeader,{|x| Upper(AllTrim(x)) == Upper(Alltrim(cCpo)) })

	If !Empty(nPos)

		If Upper(AllTrim(aCols[nPos])) <> "NULL"

			DbSelectArea("SX3")
			DbSetOrder(2)

			If MsSeek(cCpo)
				If SX3->X3_TIPO == "D"
					cRet := StoD(AllTrim(aCols[nPos]))
				ElseIf SX3->X3_TIPO == "N"
					cRet := Val(AllTrim(aCols[nPos]))
				Else
					cRet := PadR(Upper(AllTrim(aCols[nPos])),TamSX3(cCpo)[1])
				EndIf
			Else
				cRet := Upper(AllTrim(aCols[nPos]))
			EndIf

		EndIf

	EndIf

	SX3->(RestArea(aSX3Area))

Return cRet


// #########################################################################################
// Programa  : ImpVldCols
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 28/06/13 | Diogo     |
// ---------+-------------------+-----------------------------------------------------------
// #########################################################################################

Static Function ImpVldCols(cCdAlias,aHeader)

	Local cRet := ""

	Local AObrig:= {}

	dbSelectArea("SX3")
	dbSetOrder(1)
	dbSeek(cCdAlias)

	While !Eof() .And. (SX3->x3_arquivo == cCdAlias)
		IF X3USO(SX3->x3_usado)
			If X3Obrigat(SX3->X3_CAMPO)
				AADD(AObrig,Alltrim(SX3->x3_campo))
			Endif
		Endif
		dbSkip()
	End


	For i:= 1 to len(AObrig)
		If Empty(RetCol(AObrig[i],aCols,aHeader))
			cRet += " / Coluna "+AObrig[i]+" n�o informada! / "
		Endif
	Next


Return cRet


// #########################################################################################
// Programa  : ImpObrigat
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 28/06/13 | Diogo     |
// ---------+-------------------+-----------------------------------------------------------
// #########################################################################################

Static Function ImpObrigat(cCdAlias,aCols,aHeader)

	Local cRet := ""
	Local AObrig:= {}

	dbSelectArea("SX3")
	dbSetOrder(1)
	dbSeek(cCdAlias)

	While !Eof() .And. (SX3->x3_arquivo == cCdAlias)
		IF X3USO(SX3->x3_usado)
			If X3Obrigat(SX3->X3_CAMPO)
				AADD(AObrig,Alltrim(SX3->x3_campo))
			Endif
		Endif
		dbSkip()
	End


	For i:= 1 to len(AObrig)
		If Empty(RetCol(AObrig[i],aCols,aHeader))
			cRet += " / Coluna "+AObrig[i]+" esta vazia! "
		Endif
	Next


Return cRet


// #########################################################################################
// Programa  : ImpGrava
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 28/06/13 | Diogo     |
// ---------+-------------------+-----------------------------------------------------------
// #########################################################################################

Static Function ImpGrava(cCdAlias,cRotina,aCols,aHeader)

	Local nX			:= 0
	Local cRotAuto		:= ""
	Local lOk			:= .F.
	Local cMsg			:= ""
	Local lGeraNumSeq	:= .T.
	Local cArqErro		:= "ERRO_AUTO.TXT"
	Local lTemFilial	:= .F.
	Local cCpoFilial
	Local cFilAlias		:= xFilial(cCdAlias)
	Local aItens		:= {}
	Private lMsHelpAuto	:= .T.
	Private lMsErroAuto	:= .F.
	Private aReg		:= {}


	// ######################################################
	//		Monta array com os campos do registro
	// ######################################################

	For nX:=1 to Len(aHeader)

		AAdd(aReg, {	Upper(Alltrim(aHeader[nX])),;
						RetCol(Alltrim(aHeader[nX]),aCols,aHeader),;
						Nil} )

		// ######################################################
		//	Verifica se informou filial no arquivo
		// ######################################################

		If Upper(Alltrim(aHeader[nX])) == cCpoFilial
			lTemFilial := .T.
	    EndIf

	Next

		// ######################################################
		//		Chamada da rotina ExecAuto
		// ######################################################

		DbSelectArea(cCdAlias)

		if cCdAlias $ "SA1_SE1"
			cRotAuto := "MSExecAuto({|x,y|" +cRotina+"(x,y)},aReg,3)"

		elseif cCdAlias == "SZ2" //Refer�ncias do Cliente
			lMsErroAuto:= .F.
			cRotAuto := "u_GrvSZ2(@aReg)"

		else
			cRotAuto := "MSExecAuto({|x,y,k,w|" +cRotina+"(x,y,k,w)},NIL,NIL,aReg,3)"
		endif
		&cRotAuto

		If lMsErroAuto
			MostraErro( GetSrvProfString("Startpath","") , cArqErro )
			cMsg := MemoRead(  GetSrvProfString("Startpath","") + '\' + cArqErro )
		Else
			lOk := .T.
		EndIf

Return {lOk, cMsg }


// #########################################################################################
// Programa  : ImpChgRadio
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 28/06/13 | Diogo     |
// ---------+-------------------+-----------------------------------------------------------
// #########################################################################################

Static Function ImpChgRadio(nRadioArq,cNmAlias)

	Do Case
		Case nRadioArq == 1
			cNmAlias := "Clientes (SA1 - MATA030)"
		Case nRadioArq == 2
			cNmAlias := "Referencias Clientes"
		Case nRadioArq == 3
			cNmAlias := "T�tulos a Receber (SE1 - FINA040)"
		Case nRadioArq == 4
			cNmAlias := "Beneficiarios (SRQ - GPEA280)"
		Case nRadioArq == 5
			cNmAlias := "Programa��o de Ferias (SRF - GPEA050)"
		Case nRadioArq == 6
			cNmAlias := "Afastamento (SR8 - GPEA240)"
		Case nRadioArq == 7
			cNmAlias := "Historico de Salario (SR7 - GPEA250)"
		Case nRadioArq == 8
			cNmAlias := "Acumulados (SRD - GPEA120)"
		Case nRadioArq == 9
			cNmAlias := "Hist�rico Valores Salariais (SR3)"
		OtherWise
			cNmAlias := "Processamento nao implementado"
	EndCase

Return

/*/{Protheus.doc} Grava��o das refer�ncias do cliente
(long_description)
@author Diogo
@since 20/09/2014
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function GrvSZ2

	For i:=1 To len(aReg)


		//Busca Cliente e Loja
		For nY:= 1 To 7

			if aReg[nY][1] == "Z2_MSFIL"
				cZ2Filial := padr(substr(right("000000"+Alltrim(aReg[nY][2]),6),1,2),GetSx3Cache("Z1_FILIAL","X3_TAMANHO"))
			endif

			if aReg[nY][1] == "Z2_CONTRAT"
				cContr := right("000000"+Alltrim(aReg[nY][2]),6)
			endif

			if aReg[nY][1] == "Z2_PRODUTO"
				cProd := aReg[nY][2]
			endif

			if aReg[nY][1] == "Z2_QUANT"
				nQtde := aReg[nY][2]
			endif


		Next


			cCli:= Posicione("SZ1",1,cZ2Filial+cContr,"Z1_CLIENTE")
			cLoja:= Posicione("SZ1",1,cZ2Filial+cContr,"Z1_LOJA")

			dbSelectArea("SA1")
			SA1->(dbSetOrder(1))
			SA1->(dbSeek(xFilial("SA1")+cCli+cLoja))

			nDesc	:= 0
			nVlrItem2:= BuscPreco(cProd)


		RecLock("SZ2",.T.)

		For nY:= 1 To 7
			if !empty(aReg[nY][1])

				if Alltrim(aReg[nY][1]) = "Z2_VLUNIT"
					&(aReg[nY][1])	:= nVlrItem2
				elseif Alltrim(aReg[nY][1]) = "Z2_DESC"
					&(aReg[nY][1])	:= Posicione("SB1",1,xFilial("SB1")+alltrim(cProd),"B1_DESC")
				elseif aReg[nY][1] == "Z2_MSFIL"
					&(aReg[nY][1])	:= right("000000"+alltrim(aReg[nY][2]),6)
				elseif aReg[nY][1] == "Z2_CONTRAT"
				&(aReg[nY][1])		:= right("000000"+Alltrim(aReg[nY][2]),6)
				else
					&(aReg[nY][1])	:= aReg[nY][2]
				endif
			endif
		Next
		SZ2->Z2_VLUNIT	:= nVlrItem2
		SZ2->Z2_ITEM	 	:= FMaxZ2(cZ2Filial,SZ2->Z2_CONTRAT)
		SZ2->Z2_DESCONT	:= nDesc
		SZ2->Z2_TOTAL		:= (nVlrItem2*nQtde) //- nDesc
		SZ2->Z2_VLRLIQ	:= SZ2->Z2_TOTAL-SZ2->Z2_DESCONT
		SZ2->Z2_SALDO		:= nQtde
		SZ2->Z2_FILIAL	:= Alltrim(cZ2Filial)
		SZ2->Z2_DESC		:= Posicione("SB1",1,xFilial("SB1")+alltrim(cProd),"B1_DESC")

		SZ2->(MsUnlock())
		i:= len(aReg)

	Next

Return

User Function GrvTES

	For i:=1 To len(aReg)

		RecLock("SF4",.T.)

		For nY:= 1 To 31
			if !empty(aReg[nY][1])
				&(aReg[nY][1])	:= aReg[nY][2]
			endif
		Next
		SF4->(MsUnlock())
		i:= len(aReg)

	Next

Return

User Function RefazSZ1

dbSelectArea("SZ1")
dbGotop()

While SZ1->(!eof())
	If Empty(SZ1->Z1_USERAPR)	//S� altera os contratos n�o aprovados
		RecLock("SZ1",.F.)
			SZ1->Z1_QTDORIG	:= FBuscQtd(SZ1->Z1_FILIAL,SZ1->Z1_CONTRAT,1)
			SZ1->Z1_SALDO		:= FBuscQtd(SZ1->Z1_FILIAL,SZ1->Z1_CONTRAT,2)
			SZ1->Z1_QTDMED	:= FBuscQtd(SZ1->Z1_FILIAL,SZ1->Z1_CONTRAT,3)
			nVlAdiant			:= FBuscQtd(SZ1->Z1_FILIAL,SZ1->Z1_CONTRAT,4)
			SZ1->Z1_VLADT		:= nVlAdiant+(nVlAdiant*SZ1->Z1_FRETEC/100)
		MsUnlock()
	EndIf
	SZ1->(dbSkip())
EndDo

Return

Static Function FBuscQtd(cFil,Contr,nTipo)

	If nTipo == 1 //Quantidade
		cQuery:= "SELECT SUM(Z2_QUANT) QUANT FROM "+RetSqlName("SZ2")+" SZ2 "
		cQuery+= " WHERE SZ2.D_E_L_E_T_=' ' AND Z2_FILIAL = '"+cFil+"' "
		cQuery+= " AND Z2_CONTRAT = '"+Contr+"' "
	ElseIf nTipo == 2 //Saldo
		cQuery:= "SELECT SUM(Z2_QUANT - Z2_QTDMED) QUANT FROM "+RetSqlName("SZ2")+" SZ2 "
		cQuery+= " WHERE SZ2.D_E_L_E_T_=' ' AND Z2_FILIAL = '"+cFil+"' "
		cQuery+= " AND Z2_CONTRAT = '"+Contr+"' "
	ElseIf nTipo == 3 //Medi��o
		cQuery:= "SELECT SUM(Z2_QTDMED) QUANT FROM "+RetSqlName("SZ2")+" SZ2 "
		cQuery+= " WHERE SZ2.D_E_L_E_T_=' ' AND Z2_FILIAL = '"+cFil+"' "
		cQuery+= " AND Z2_CONTRAT = '"+Contr+"' "
	ElseIf nTipo == 4 //Adiantamento
		cQuery:= "SELECT SUM(Z2_VLRLIQ) QUANT FROM "+RetSqlName("SZ2")+" SZ2 "
		cQuery+= " LEFT JOIN "+RetSqlName("SF4")+" SF4"
		cQuery+= " ON F4_FILIAL='"+xFilial("SF4")+"' AND F4_CODIGO=Z2_TES AND SF4.D_E_L_E_T_=' '"
		cQuery+= " WHERE SZ2.D_E_L_E_T_=' ' "
		cQuery+= " AND Z2_FILIAL = '"+cFil+"' "
		cQuery+= " AND Z2_CONTRAT = '"+Contr+"' "
		cQuery+= " AND F4_DUPLIC = 'S' "
	Endif

	 TcQuery cQuery New Alias XSZ2

	 nRet:= XSZ2->QUANT

	 XSZ2->(dbCloseArea())

Return nRet

Static Function FMaxZ2(cFil,Contr)
	cQuery:= "SELECT MAX(Z2_ITEM) ITEM FROM "+RetSqlName("SZ2")+" SZ2 "
	cQuery+= " WHERE SZ2.D_E_L_E_T_=' ' AND Z2_FILIAL = '"+cFil+"' "
	cQuery+= " AND Z2_CONTRAT = '"+Contr+"' "

	 TcQuery cQuery New Alias XSZ2

	 If XSZ2->(!eof()) .and. !empty(XSZ2->ITEM)
	    nRet:= soma1(XSZ2->ITEM)
	 Else
	 	nRet:= "001"
	 Endif

	 XSZ2->(dbCloseArea())

Return nRet

User Function AtuDA1

	dbSelectArea("DA1")
	DA1->(dbSetOrder(1))

	cTab:= DA1->DA1_CODTAB
	cCont:= "0001"

	while DA1->(!eof())
		if 	cTab<> DA1->DA1_CODTAB
			cCont:= "0001"
			cTab:= DA1->DA1_CODTAB
		endif

		RecLock("DA1",.F.)
		     DA1->DA1_ITEM := cCont
		MsUnlock()

		cCont:= soma1(cCont)

	     DA1->(dbSkip())
	enddo

Return

Static Function BuscPreco(cCod)

	lKit := ExistCpo("SZ6",cCod,1,,.F.)
	nRet := 0

	If lKit
		nVlrTotal:= 0
		SZ7->(DbSetOrder(1))
			SZ7->(DbSeek(xFilial("SZ7")+cCod))
				While SZ7->(!EOF()) .AND. xFilial("SZ7")+cCod==SZ7->(Z7_FILIAL+Z7_COD)
						nVlrItem	:= MaTabPrVen(SA1->A1_TABELA;
												,SZ7->Z7_PRODUTO;
													,1;
													,cCli;
													,cLoja)
						nVlrTotal+= nVlrItem*SZ7->Z7_QUANT

						If SZ7->Z7_PERC > 0
							nDesc+= (( nVlrItem*SZ7->Z7_QUANT*nQtde)*(SZ7->Z7_PERC/100))
						Endif

					SZ7->(DbSkip())
		EndDo
		nRet:= nVlrTotal
	Else //N�o � Kit

				nRet	:= MaTabPrVen(SA1->A1_TABELA ;
										,cProd ;
										,1;
										,cCli;
										,cLoja)

	Endif

	If nRet = 0
	   	If SB1->(DbSeek(xFilial("SB1")+cCod))
		   nRet	:= SB1->B1_PRV1
		Endif
	Endif

Return nRet