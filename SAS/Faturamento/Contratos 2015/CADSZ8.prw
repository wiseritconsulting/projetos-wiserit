#Include 'Protheus.ch'
#INCLUDE 'FWMBROWSE.CH'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} CADSZ8
Devolu��es Opera��o Triangular
@author Saulo Gomes Martins
@since 30/09/2014
@version 1.0
/*/
User Function CADSZ8(cFiltro)
	Local oBrowse
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('SZ8')
	oBrowse:SetDescription('Devolu��es Opera��o Triangular')
	oBrowse:DisableDetails()
	oBrowse:AddLegend( "Z8_ENTRADA='S'", "GREEN", "Completo"  )
	oBrowse:AddLegend( "Z8_ENTRADA<>'S'", "RED", "Entrada Matriz Pendente"  )
	If !Empty(cFiltro)
		oBrowse:SetFilterDefault(cFiltro)
	EndIf
	oBrowse:SetMenuDef( 'CADSZ8' )
	oBrowse:Activate()
	/*Private oMarkSZ8 := FWMarkBrowse():New()
	Default cFiltro	:= ""
	oMarkSZ8:SetAlias('SZ8')
	oMarkSZ8:SetSemaphore(.T.)
	oMarkSZ8:SetDescription('Devolu��es Opera��o Triangular')
	oMarkSZ8:SetFieldMark( 'Z8_OK' )
	oMarkSZ8:SetAllMark( { || oMarkSZ8:AllMark() } )

	oMarkSZ8:SetMenuDef( 'CADSZ8' )
	oMarkSZ8:Activate()*/
Return

Static Function MenuDef()
	Local aRotina := {}
	ADD OPTION aRotina TITLE 'Visualizar'	 				ACTION 'VIEWDEF.CADSZ8' OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE 'Criar Entrada Matriz'	 	ACTION 'U_CADSZ8A' OPERATION 6 ACCESS 0
	//ADD OPTION aRotina TITLE "Excluir"						ACTION "VIEWDEF.CADSZ8"	OPERATION 5 ACCESS 144

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	Local oStructSZ8 := Nil
	Local oModel     := Nil
	//-----------------------------------------
	//Monta a estrutura do formul�rio com base no dicion�rio de dados
	//-----------------------------------------
	oStructSZ8 := FWFormStruct(1,"SZ8")

	//-----------------------------------------
	//Monta o modelo do formul�rio
	//-----------------------------------------
	oModel:= MPFormModel():New("YCADSZ8",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
	oModel:AddFields("SZ8MASTER",/*cOwner*/, oStructSZ8 ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)
	oModel:SetPrimaryKey({"Z8_FILIAL","Z8_EMP01","Z8_NF01","Z8_SER01","Z8_CLIENTE","Z8_LOJA","Z8_NF02","Z8_SER02"})
	oModel:SetCommit({|oModel| SZ8Commit(oModel) },.F.)
	oModel:SetCommit({|oModel| FWFormCommit(oModel) },.t.)

Return(oModel)


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
	Local oStructSZ8	:= FWFormStruct( 2, 'SZ8' )
	Local oModel		:= FWLoadModel( 'CADSZ8' )
	Local oView
	Local oCalc1
	oView	:= FWFormView():New()

	//oView:SetUseCursor(.F.)
	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField("SZ8MASTER",oStructSZ8)
	oView:CreateHorizontalBox("CABEC",100)
	oView:SetOwnerView( "SZ8MASTER","CABEC")
	oView:SetProgressBar(.T.)
Return oView

Static Function SZ8Commit(oModel)
	Local cBakFil	:= cFilAnt
	Local cEmp01	:= oModel:GetModel( 'SZ8MASTER' ):GetValue('Z8_EMP01')
	Local cNF01	:= oModel:GetModel( 'SZ8MASTER' ):GetValue('Z8_NF01')
	Local cSer01	:= oModel:GetModel( 'SZ8MASTER' ):GetValue('Z8_SER01')
	Local cCliente:= oModel:GetModel( 'SZ8MASTER' ):GetValue('Z8_CLIENTE')
	Local cLoja	:= oModel:GetModel( 'SZ8MASTER' ):GetValue('Z8_LOJA')
	Local cNF02	:= oModel:GetModel( 'SZ8MASTER' ):GetValue('Z8_NF02')
	Local cSer01	:= oModel:GetModel( 'SZ8MASTER' ):GetValue('Z8_SER02')
	Local cEmpMtz	:= oModel:GetModel( 'SZ8MASTER' ):GetValue('Z8_EMPMTZ')
	Local cNFMtz	:= oModel:GetModel( 'SZ8MASTER' ):GetValue('Z8_NFMTZ')
	Local cSerMtz	:= oModel:GetModel( 'SZ8MASTER' ):GetValue('Z8_SERMTZ')
	Local aClientFil	:= {,}		//Cliente Filial
	Local aClientMtz	:= {,}		//Cliente Matriz
	Local cCNPJMatriz	:= FWArrFilAtu("01",cEmpMtz)[18]		//Matriz
	Local cCNPJColiga	:= FWArrFilAtu(cEmpAnt,cEmp01)[18]		//Coligada
	SA2->(DbSetOrder(3))	//A2_FILIAL+A2_CGC
	SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
	If SA2->(DbSeek(xFilial("SA2")+cCNPJMatriz))
		aFornec[1]	:= SA2->A2_COD
		aFornec[2]	:= SA2->A2_LOJA
	Else
		Aviso("Aviso","Necessario ter a Matriz cadastrado como fornecedor!",{"Ok"})
		Return
	EndIf

	If SA1->(DbSeek(xFilial("SA1","010105")+cCNPJColiga))
		aClientFil[1]	:= SA1->A1_COD
		aClientFil[2]	:= SA1->A1_LOJA
	Else
		Aviso("Aviso","Necessario essa coligada ter cadastrado como Cliente!",{"Ok"})
		Return
	EndIf

	If SA1->(DbSeek(xFilial("SA1","010105")+cCNPJMatriz))
		aClientMtz[1]	:= SA1->A1_COD
		aClientMtz[2]	:= SA1->A1_LOJA
	Else
		Aviso("Aviso","Necessario a Matriz ter cadastrado como Cliente!",{"Ok"})
		Return
	EndIf

	Begin Transaction

		lRet	:= StaticCall(CADSZ5,FCancNfEnt,cEmpMtz,cNFMtz,cSerMtz,aClientFil)	//Excluir a Entrada na Matriz
		If lRet
			lRet := FCancFat(cEmp01,cNF02,cSer02)	//Excluir a NF de Saida para Matriz
			//MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCab,aItens,5)
			If lRet
				lRet := StaticCall(CADSZ5,FCancNfEnt,cEmp01,cNF01,cSer01,{cCliente,cLoja})		//Excluir a Entrada do Cliente
			EndIf
		EndIf
		If !lRet
			DisarmTransaction()
		EndIf
	End Transaction

	cFilAnt	:= cBakFil
Return

User Function CADSZ8A()
	Local lOk	:= .F.
	Local aFornec	:= {"",""}
	Local aDocSF1	:= {"",""}
	Local aClientFil	:= {"",""}
	Local cMsgErro	:= ""
	Local cCNPJMatriz	:= FWArrFilAtu("01","010105")[18]		//Matriz
	Local cCNPJColiga	:= FWArrFilAtu(cEmpAnt,cFilAnt)[18]	//Coligada
	SA2->(DbSetOrder(3))	//A2_FILIAL+A2_CGC
	SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
	If !SoftLock("SZ8")
		Return
	EndIf
	If SZ8->Z8_ENTRADA=="S"
		MsgAlert("Entrada j� foi confirmada anteriomente!")
		Return
	ElseIf !MsgYesNo("Confirmar Entrada na Matriz?")
		Return
	EndIF
	If SA2->(DbSeek(xFilial("SA2")+cCNPJMatriz))
		aFornec[1]	:= SA2->A2_COD
		aFornec[2]	:= SA2->A2_LOJA
	Else
		Aviso("Aviso","Necessario ter a Matriz cadastrado como fornecedor!",{"Ok"})
		Return
	EndIf
	If SA1->(DbSeek(xFilial("SA1","010105")+cCNPJColiga))
		aClientFil[1]	:= SA1->A1_COD
		aClientFil[2]	:= SA1->A1_LOJA
	Else
		Aviso("Aviso","Necessario essa coligada ter cadastrado como Cliente!",{"Ok"})
		Return
	EndIf
	Processa({|| ProcessMessage(),lOk := StaticCall(CADSZ5,EntradaSF2,"010105",cFilAnt,SZ8->Z8_NF02,SZ8->Z8_SER02,aFornec,aClientFil,@aDocSF1,@cMsgErro,1,{SZ8->Z8_NF01,SZ8->Z8_SER01,SZ8->Z8_EMP01,SZ8->Z8_CLIENTE,SZ8->Z8_LOJA},.T.) },"Criando Nota de Entrada!","Aguarde ...",.F.)
	If lOk
		RecLock("SZ8",.F.)
		SZ8->Z8_ENTRADA	:= "S"
	EndIf
	MsUnLock()
Return