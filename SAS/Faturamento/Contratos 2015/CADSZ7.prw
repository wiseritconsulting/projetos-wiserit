#Include 'Protheus.ch'
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} CADSZ7
Aditivo
@author Diogo Costa
@since 01/10/2014
@version 1.0
/*/
User Function CADSZ7()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	Local oStructSZ1	:= FWFormStruct(1,"SZ1")
	Local oStructSZ2	:= FWFormStruct(1,"SZ2")
	Local oModel		:= FWLoadModel( 'CADSZ1' )
	Local nI
	//-----------------------------------------
	//Monta a estrutura do formul�rio com base no dicion�rio de dados
	//-----------------------------------------

/*
	oStructSZ2:AddTrigger( ;
		"Z2_QUANT"			,;										//[01] Id do campo de origem
	"Z2_TOTAL"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("Z2_QUANT") * oModel:GetValue("Z2_VLUNIT") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ2:AddTrigger( ;
		"Z2_VLUNIT"		,;										//[01] Id do campo de origem
	"Z2_TOTAL"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("Z2_QUANT") * oModel:GetValue("Z2_VLUNIT") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ2:AddTrigger( ;
		"Z2_PRODUTO"		,;										//[01] Id do campo de origem
	"Z2_DESC"			,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("SB1",oModel:GetValue('Z2_PRODUTO'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SB1",1,xFilial("SB1")+oModel:GetValue("Z2_PRODUTO"),"B1_DESC") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ2:AddTrigger( ;
		"Z2_PRODUTO"		,;										//[01] Id do campo de origem
	"Z2_DESC"			,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("SZ6",oModel:GetValue('Z2_PRODUTO'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SZ6",1,xFilial("SZ6")+oModel:GetValue("Z2_PRODUTO"),"Z6_DESC") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ1:AddTrigger( ;
		"Z1_CLIENTE"		,;										//[01] Id do campo de origem
	"Z1_NOME"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SA1",1,xFilial("SA1")+oModel:GetValue('Z1_CLIENTE')+oModel:GetValue('Z1_LOJA'),"A1_NOME") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ1:AddTrigger( ;
		"Z1_LOJA"		,;											//[01] Id do campo de origem
	"Z1_NOME"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SA1",1,xFilial("SA1")+oModel:GetValue('Z1_CLIENTE')+oModel:GetValue('Z1_LOJA'),"A1_NOME") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructSZ2:AddTrigger( ;
	"Z2_QUANT"		,;										//[01] Id do campo de origem
	"Z2_SALDO"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("Z2_QUANT") }  )	// [04] Bloco de codigo de execu��o do gatilho
*/
	//oModel:AddGrid("Z2DETAL", "SZ1MASTER"/*cOwner*/,oStructSZ2, ,/*bLinePost*/,/*bPre*/,/*bPost*/,/*Carga*/)
	//oModel:SetRelation("Z2DETAL",{{"Z2_FILIAL",'xFilial("SZ2")'},{"Z2_CONTRAT","Z1_CONTRAT"}},SZ2->(IndexKey(1)))

	oModel:GetModel("SZ2DETAIL"):GetStruct():SetProperty("Z2_PRODUTO",MODEL_FIELD_VALID,{|oModel|SZ1ValProd(oModel) })

	oModel:GetModel("SZ1MASTER" ):GetStruct():SetProperty("*",MODEL_FIELD_WHEN,{||.F.})
	oModel:GetModel( 'SZ2DETAIL' ):GetStruct():SetProperty("*",MODEL_FIELD_WHEN,{||FValidLin(oModel)})

	oModel:SetVldActive( { |oModel| SZ2ValAct( oModel ) } )

	oModel:SetCommit({|oModel| SZ7Commit(oModel) },.F.)
	oModel:SetCommit({|oModel| FWFormCommit(oModel) },.t.)


Return(oModel)

Static Function ViewDef()
	Local oModel		:= FWLoadModel( 'CADSZ7' )
	Local oView		:= FWLoadView('CADSZ1')
	Local oStructSZ1	:= FWFormStruct( 2, 'SZ1' )
	Local oStructSZ2	:= FWFormStruct( 2, 'SZ2', {|cCampo| !(Alltrim(cCampo) $ "Z2_CONTRAT|Z2_DTMED|Z2_QTDADT|Z2_QTDMED|")} )
	oView:SetModel(oModel)
	/*oView:AddGrid("SZ2DETAIL",oStructSZ2)
	//oView:AddField( "SZ1MASTER",oStructSZ1)
	oView:SetOwnerView( "SZ2DETAIL","GRID")
	oView:SetProgressBar(.T.)
	oView:AddIncrementField("SZ2DETAIL","Z2_ITEM")
	oView:EnableTitleView('SZ2DETAIL','Itens Contrato')*/

Return oView

Static Function SZ2ValAct( oModel )
	Local lRet:= .T.
	Local cContrato
	If oModel:getOperation() == MODEL_OPERATION_UPDATE
		If	Empty(SZ1->Z1_USERAPR)
			oModel:SetErrorMessage('SZ1MASTER',,,,"ATEN��O", 'Contrato n�o liberado, n�o pode ser feito o Aditivo!', 'Realize a altera��o normal do contrato!')
			Return .F.
		EndIf
	EndIf
Return lRet

Static Function SZ1ValProd(oModelSZ2)
	Local lRet := ExistCpo("SZ6",oModelSZ2:GetValue('Z2_PRODUTO'),1,,.F.).OR.ExistCpo("SB1",oModelSZ2:GetValue('Z2_PRODUTO'),1,,.F.)
	Local oModel := FWModelActive()
	If !lRet
		oModel:SetErrorMessage('SZ2DETAIL',"Z2_PRODUTO",,,"ATEN��O", 'Produto/Kit n�o encontrado', 'Verifique se o Produto ou Kit esta cadastrado!')
	Else
		oModel:GetModel('SZ2DETAIL'):LoadValue('Z2_QUANT',0)
		oModel:GetModel('SZ2DETAIL'):SetValue('Z2_VLUNIT',PrecoTab(oModelSZ2:GetValue('Z2_PRODUTO')))
		oModel:GetModel('SZ2DETAIL'):LoadValue('Z2_TOTAL',0)
		oModel:GetModel('SZ2DETAIL'):LoadValue('Z2_TES',Space(GetSx3Cache("Z2_TES","X3_TAMANHO")))
	EndIf
Return lRet

Static Function PrecoTab(cProduto)
	Local cTabPad		:= SuperGetMV("MV_TABPAD")
	Local oModel		:= FWModelActive()
	Local lKit			:= ExistCpo("SZ6",cProduto,1,,.F.)
	Local nValorTab	:= 0
	SA1->(dbSetOrder(1))	//A1_FILIAL+A1_COD
	If SA1->(dbSeek(xFilial("SA1")+oModel:GetModel('SZ1MASTER'):GetValue('Z1_CLIENTE')+oModel:GetModel('SZ1MASTER'):GetValue('Z1_LOJA'))) .AND. !Empty(SA1->A1_Tabela)
		cTabPad	:= SA1->A1_TABELA
	EndIf
	If lKit
		SZ7->(DbSetOrder(1))
		SZ7->(DbSeek(xFilial("SZ7")+cProduto))
		While SZ7->(!EOF()) .AND. xFilial("SZ7")+cProduto==SZ7->(Z7_FILIAL+Z7_COD)
			nValorTab += SZ7->Z7_QUANT*MaTabPrVen(cTabPad;
				,SZ7->Z7_PRODUTO;
				,1;
				,oModel:GetModel('SZ1MASTER'):GetValue('Z1_CLIENTE');
				,oModel:GetModel('SZ1MASTER'):GetValue('Z1_LOJA'))
			SZ7->(DbSkip())
		EndDo
	Else
		nValorTab	:= MaTabPrVen(cTabPad;
			,cProduto;
			,1;
			,oModel:GetModel('SZ1MASTER'):GetValue('Z1_CLIENTE');
			,oModel:GetModel('SZ1MASTER'):GetValue('Z1_LOJA'))
	EndIf
	alert(str(nValorTab))
Return nValorTab

/*/{Protheus.doc} FValidLin
N�o permitir editar itens j� gravados para o contrato
@author Diogo
@since 01/10/2014
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Static Function FValidLin(oModel)
	Local lRet			:= .T.
	Local nOperation	:= oModel:GetOperation()
	Local cProdut		:= oModel:GetValue( 'SZ2DETAIL', 'Z2_PRODUTO' )
	Local cItem			:= oModel:GetValue( 'SZ2DETAIL', 'Z2_ITEM' )
	Local cContr		:= oModel:GetValue( 'SZ1MASTER', 'Z1_CONTRAT' )

	dbSelectArea("SZ2")
	SZ2->(dbSetOrder(1))
	if SZ2->(dbSeek(xFilial("SZ2")+cContr+cItem))
		lRet:= .F.
	endif

Return lRet


Static Function SZ7Commit(oModel)
	Local cContrato	:= oModel:GetModel( 'SZ1MASTER' ):GetValue('Z1_CONTRAT')
	Local nSldAdt	:= oModel:GetModel( 'SZ1MASTER' ):GetValue('Z1_VLADT')
	Local nSaldo		:= 0
	Local nVlAdiant	:= 0
	Local nQtd		:= 0
	Local lRet		:= .T.
	Local nCont
	Local aDados	:= {}
	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
	If	oModel:getOperation() == MODEL_OPERATION_UPDATE .OR.;
			oModel:getOperation() == MODEL_OPERATION_INSERT
		oModelSZ2	:= oModel:GetModel( 'SZ2DETAIL' )
		For nCont := 1 To oModelSZ2:Length()
			oModelSZ2:GoLine( nCont )
			If oModelSZ2:IsDeleted()
				Loop
			EndIf
			nSaldo	+= oModelSZ2:GetValue('Z2_QUANT')+oModelSZ2:GetValue('Z2_QTDADT')-oModelSZ2:GetValue('Z2_QTDMED')
			nQtd	+= oModelSZ2:GetValue('Z2_QUANT')
			oModelSZ2:LoadValue("Z2_SALDO", oModelSZ2:GetValue('Z2_QUANT')+oModelSZ2:GetValue('Z2_QTDADT')-oModelSZ2:GetValue('Z2_QTDMED'))
			If SF4->(DbSeek(xFilial("SF4")+oModelSZ2:GetValue('Z2_TES')))  .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
				nVlAdiant	+= oModelSZ2:GetValue('Z2_TOTAL')
			EndIf
		Next
		//oModel:LoadValue("SZ1MASTER", "Z1_QTDORIG", nQtd)
		oModel:LoadValue("SZ1MASTER", "Z1_SALDO", nSaldo)
		//oModel:LoadValue("SZ1MASTER", "Z1_USERAPR", " ")
		//oModel:LoadValue("SZ1MASTER", "Z1_VLADT",(nVlAdiant+(nVlAdiant*oModel:GetModel( 'SZ1MASTER' ):GetValue('Z1_FRETEC')/100)))
		//oModel:LoadValue("SZ1MASTER", "Z1_VLADD",(nVlAdiant+(nVlAdiant*oModel:GetModel( 'SZ1MASTER' ):GetValue('Z1_FRETEC')/100)) - nSldAdt)
	EndIf
Return lRet