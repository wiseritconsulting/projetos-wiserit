#Include 'Protheus.ch'
#include "Topconn.ch"

/*/{Protheus.doc} RFATA03
Verifica e corrigi informa��es dos contratos
@author Saulo Gomes Martins
@since 25/11/2014
@version 1.0
/*/
User Function RFATA03()
	Local aPergs	:= {}
	Local aRet		:= {}
	Local cContrIni	:= Space(GetSx3Cache("Z1_CONTRAT","X3_TAMANHO"))
	Local cContrFim	:= Space(GetSx3Cache("Z1_CONTRAT","X3_TAMANHO"))
	aAdd( aPergs ,{1,"Contrato de"	,cContrIni,,".T.","",'.T.',60,.F.})
	aAdd( aPergs ,{1,"Contrato Ate"	,cContrFim,,".T.","",'.T.',60,.F.})
	If !ParamBox(aPergs,"Ajustar Contratos",@aRet,,,,,,,,.F.,.F.)
		Return
	EndIf
	Processa({|| RunProcess(aRet[1],aRet[2]) },"Ajustando contratos","Aguarde ...",.F.)
Return

Static Function RunProcess(cContrIni,cContrFim)
	Local cNomeUsr
	Local cTime
	Local aLogs		:= {}
	Local nCont
	Local nQtdTotal	:= 0
	Local nRegistro	:= 0
	Local dDataBak	:= dDataBase
	Local cFilBak		:= cFilAnt
	Local cQuery		:= "SELECT COUNT(*) AS QUANT FROM "+RETSQLNAME("SZ1")+" SZ1 WHERE Z1_CONTRAT>='"+cContrIni+"' AND Z1_CONTRAT<='"+cContrFim+"' AND D_E_L_E_T_=' '"
	Local cQuery2		:= "SELECT R_E_C_N_O_ AS REGSZ1 FROM "+RETSQLNAME("SZ1")+" SZ1 WHERE Z1_CONTRAT>='"+cContrIni+"' AND Z1_CONTRAT<='"+cContrFim+"' AND D_E_L_E_T_=' ' ORDER BY Z1_FILIAL,Z1_CONTRAT"
	TCQUERY cQuery New Alias TMPFT03
	nQtdTotal	:= TMPFT03->QUANT
	ProcRegua(nQtdTotal)
	TMPFT03->(DbCloseArea())

	TCQUERY cQuery2 New Alias TMPSZ1

	//BeginTran()	//S� teste, depois alterar!
	While TMPSZ1->(!EOF())
		SZ1->(DbGoTo(TMPSZ1->REGSZ1))
		nRegistro++
		IncProc(cValToChar(nRegistro)+"/"+cValToChar(nQtdTotal)+" Analisando contrato:"+SZ1->Z1_FILIAL+SZ1->Z1_CONTRAT)
		ProcessMessage() //ATUALIZA CLIENTE
		If Empty(SZ1->Z1_MSFIL)
			AADD(aLogs,{{SZ1->Z1_FILIAL,SZ1->Z1_CONTRAT},"Problema, Filial n�o preenchido!"})
			TMPSZ1->(dbSkip())
			Loop
		EndIf
		cFilAnt			:= SZ1->Z1_MSFIL
		nVlAdiant			:= StaticCall(RFATA02,FBuscQtd,SZ1->Z1_FILIAL,SZ1->Z1_CONTRAT,4)
		If SZ1->Z1_VLADT==0 .and. (nVlAdiant+(nVlAdiant*SZ1->Z1_FRETEC/100))>0
			AADD(aLogs,{{SZ1->Z1_FILIAL,SZ1->Z1_CONTRAT},"Corrigido valor do adiantamento de: "+cValToChar(SZ1->Z1_VLADT)+" para: "+cValToChar((nVlAdiant+(nVlAdiant*SZ1->Z1_FRETEC/100)))})
			RecLock("SZ1",.F.)
			SZ1->Z1_VLADT	:= (nVlAdiant+(nVlAdiant*SZ1->Z1_FRETEC/100))
			SZ1->(MsUnLock())
		EndIf
		If !Empty(SZ1->Z1_USERAPR)	//S� altera os contratos aprovados
			If SZ1->Z1_VLADT>0 .and. !SE1->(DbSeek(xFilial("SE1",SZ1->Z1_MSFIL)+"A"+SUBSTR(SZ1->Z1_MSFIL,5,2)+PadR(SZ1->Z1_CONTRAT,GetSx3Cache("E1_NUM","X3_TAMANHO"))))
				//Tem valor do adiantamento, mas n�o localizou o titulo
				cNomeUsr	:= SZ1->Z1_USERAPR
				cTime		:= SZ1->Z1_HORAAPR
				RecLock("SZ1",.F.)
				SZ1->Z1_USERAPR	:= ""
				SZ1->Z1_HORAAPR	:= ""
				MsUnLock()
				If SZ1->Z1_DTCOND<dDataBase
					dDataBase	:= SZ1->Z1_DTCOND
				EndIf
				If !u_YSZ1001(.F.,cNomeUsr,cTime)
					SZ1->(DbGoTo(TMPSZ1->REGSZ1))
					RecLock("SZ1",.F.)
					SZ1->Z1_USERAPR	:= cNomeUsr
					SZ1->Z1_HORAAPR	:= cTime
					MsUnLock()
					AADD(aLogs,{{SZ1->Z1_FILIAL,SZ1->Z1_CONTRAT},"Problema ao criar titulo!"})
				Else
					AADD(aLogs,{{SZ1->Z1_FILIAL,SZ1->Z1_CONTRAT},"Contrato Reaprovado!"})
				EndIf
				SZ1->(DbGoTo(TMPSZ1->REGSZ1))
				dDataBase	:= dDataBak
			EndIf
		Else
			If SZ1->Z1_VLADT>0 .and. SE1->(DbSeek(xFilial("SE1",SZ1->Z1_MSFIL)+"A"+SUBSTR(SZ1->Z1_MSFIL,5,2)+PadR(SZ1->Z1_CONTRAT,GetSx3Cache("E1_NUM","X3_TAMANHO"))))
				RecLock("SZ1",.F.)
				SZ1->Z1_USERAPR	:= "Administrador"
				SZ1->Z1_HORAAPR	:= "12:00"
				MsUnLock()
				AADD(aLogs,{{SZ1->Z1_FILIAL,SZ1->Z1_CONTRAT},"Contrato com titulo, Reaprovado!"})
			EndIf
		EndIf
		TMPSZ1->(dbSkip())
	EndDo
	TMPSZ1->(DbCloseArea())
	If Empty(aLogs)
		MsgInfo("Nenhuma altera��o efetuada!")
		//DisarmTransaction()
		Return
	EndIf
	AutoGrLog("Log de altera��es")
	For nCont:=1 to Len(aLogs)
		AutoGrLog("Filial:"+aLogs[nCont][1][1])
		AutoGrLog("Contrato:"+aLogs[nCont][1][2])
		AutoGrLog(aLogs[nCont][2])
	Next
	MostraErro()
	//DisarmTransaction()
	cFilAnt	:= cFilBak
Return


cQuery	:= "SELECT * FROM ( "
cQuery	+= " SELECT Z1_FILIAL,Z1_CONTRAT,Z1_MSFIL,Z2_VLRLIQ,(Z2_TOTAL-Z2_DESCONT) AS TOTALSZ2,Z1_VLADT,Z1_USERAPR,E1_FILIAL,E1_PREFIXO,E1_NUM,E1_VALOR, "
cQuery	+= " CASE "
cQuery	+= "  WHEN RTRIM(Z1_USERAPR)='' THEN 'NAO APROVADO' "
cQuery	+= "  ELSE 'APROVADO' "
cQuery	+= " END AS APROVACAO "
cQuery	+= " FROM "
cQuery	+= " SZ1010 SZ1 "
cQuery	+= " FULL JOIN  "
cQuery	+= " (SELECT E1_FILIAL,E1_PREFIXO,E1_NUM,D_E_L_E_T_,SUM(E1_VALOR) AS E1_VALOR FROM SE1010 TSE1 "
cQuery	+= " WHERE "
cQuery	+= " TSE1.D_E_L_E_T_=' ' "
cQuery	+= " GROUP BY E1_FILIAL,E1_PREFIXO,E1_NUM,D_E_L_E_T_) "
cQuery	+= "  SE1 "
cQuery	+= " ON E1_FILIAL=Z1_FILIAL AND E1_PREFIXO='A'+SUBSTRING(Z1_MSFIL,5,2) AND RTRIM(E1_NUM)=Z1_CONTRAT AND SZ1.D_E_L_E_T_=' ' "
cQuery	+= " LEFT JOIN ( SELECT Z2_FILIAL,Z2_CONTRAT,SUM(Z2_VLRLIQ) AS Z2_VLRLIQ, SUM(Z2_TOTAL) AS Z2_TOTAL, SUM(Z2_DESCONT) AS Z2_DESCONT "
cQuery	+= " FROM SZ2010 TSZ2 "
cQuery	+= " LEFT JOIN SF4010 TSF4 "
cQuery	+= " ON F4_FILIAL=Z2_FILIAL AND F4_CODIGO=Z2_TES AND TSF4.D_E_L_E_T_=' ' "
cQuery	+= " WHERE TSZ2.D_E_L_E_T_=' ' AND F4_DUPLIC='S' "
cQuery	+= " GROUP BY Z2_FILIAL,Z2_CONTRAT "
cQuery	+= " ) SZ2 "
cQuery	+= " ON Z2_FILIAL=Z1_FILIAL AND Z2_CONTRAT=Z1_CONTRAT "
cQuery	+= " WHERE "
cQuery	+= " (SUBSTRING(E1_PREFIXO,1,1)='A' OR E1_PREFIXO IS NULL) "
cQuery	+= " AND (SZ1.D_E_L_E_T_=' ' OR Z1_CONTRAT IS NULL) "
cQuery	+= " ) AS TAB1 "
cQuery	+= " WHERE "
cQuery	+= " (ROUND(Z2_VLRLIQ,2,1)<>ROUND(TOTALSZ2,2,1) OR ROUND(Z1_VLADT,2,1)<>ROUND(Z2_VLRLIQ,2,1) OR ROUND(E1_VALOR,2,1)<>ROUND(Z1_VLADT,2,1)) "
cQuery	+= " AND APROVACAO='APROVADO' "
cQuery	+= " ORDER BY Z1_CONTRAT "

