#Include 'Protheus.ch'
#INCLUDE 'FWMBROWSE.CH'
#INCLUDE 'FWMVCDEF.CH'
#Include 'Topconn.ch'

/*/{Protheus.doc} CADSZ5
Relacionamento Opera��o triangular
@author Saulo Gomes Martins
@since 16/09/2014
@version 1.0
/*/
User Function CADSZ5()
	Local oColumn
	Private oMark 	:= FWMarkBrowse():New()
	cFiltro			:= "!Empty(SZ5->Z5_NF01) .OR. SZ5->Z5_LIBERAC $ 'L|Z' .OR. Posicione('SA1',1,xFilial('SA1')"+;
		"+SZ5->Z5_CLIENTE"+;
		"+SZ5->Z5_LOJA"+;
		",'A1_YBLOQ') <> 'S' "
	oMark:SetAlias('SZ5')
	oMark:SetSemaphore(.T.)
	oMark:SetDescription('Opera��o Triangular')
	oMark:SetFieldMark( 'Z5_OK' )
	oMark:SetAllMark( { || oMark:AllMark() } )
	oMark:AddLegend( "!Empty(SZ5->Z5_NF01) .AND. !Empty(SZ5->Z5_NF02) .AND. !Empty(SZ5->Z5_NF03) .AND. !Empty(SZ5->Z5_NF04)", "RED", "Faturado")
	oMark:AddLegend( &("{|| SZ5->Z5_LIBERAC == ' ' .AND. (Empty(SZ5->Z5_NF01) .AND. SA1->(Posicione('SA1',1,xFilial('SA1')+SZ5->Z5_CLIENTE+SZ5->Z5_LOJA,'A1_YBLOQ'))=='S') }"),"BR_PRETO", "Bloqueado")
	oMark:AddFilter("Cliente Bloqueado","A1_YBLOQ='S'",,,"SA1")
	oMark:AddFilter("Registro n�o liberado","SZ5->Z5_LIBERAC == ' '")
	oMark:AddLegend( "Empty(SZ5->Z5_NF01) .AND. Empty(SZ5->Z5_NF02) .AND. Empty(SZ5->Z5_NF03) .AND. Empty(SZ5->Z5_NF04)", "GREEN", "N�o Faturado")
	//oMark:SetFilterDefault("Z5_MSFIL=='"+cFilAnt+"'")
	oMark:oBrowse:SetColumns({{GetSx3Cache("C5_FECENT","X3_TITULO"),{|| POSICIONE("SC5",1,xFilial("SC5",SZ5->Z5_EMPPV1)+SZ5->Z5_PV01,"C5_FECENT")}}})
	oMark:oBrowse:SetColumns({{GetSx3Cache("C5_SUGENT","X3_TITULO"),{|| POSICIONE("SC5",1,xFilial("SC5",SZ5->Z5_EMPPV1)+SZ5->Z5_PV01,"C5_SUGENT")}}})
	If !(Alltrim(RetCodUsr()) $ Alltrim(GetMv("SA_USUCONT")))
		oMark:SetFilterDefault(cFiltro)
	Endif
	oMark:SetMenuDef( 'CADSZ5' )
	oMark:Activate()
Return

Static Function MenuDef()
	Local aRotina := {}
	Local aNota	:= {}
	Local aPedido	:= {}
	Local aDevol	:= {}
	ADD OPTION aRotina	TITLE 'Visualizar'	 				ACTION 'VIEWDEF.CADSZ5' OPERATION 2 ACCESS 0
	ADD OPTION aPedido	TITLE 'Alterar Pedido'				ACTION 'U_CADSZ5E' OPERATION 4 ACCESS 0
	ADD OPTION aPedido	TITLE "Alterar Dados Entrega"		ACTION "U_CADSZ5H()"	OPERATION 6 ACCESS 0
	ADD OPTION aPedido	TITLE 'Visualizar Pedido Cliente'	ACTION 'U_CADSZ5I' OPERATION 2 ACCESS 0
	ADD OPTION aRotina	TITLE 'Pedido'		 				ACTION aPedido OPERATION 2 ACCESS 0
	ADD OPTION aNota		TITLE "Gerar Notas"	 				ACTION "U_CADSZ5A()"	OPERATION 6 ACCESS 0
	ADD OPTION aNota		TITLE "Excluir Notas" 				ACTION "U_CADSZ5B()"	OPERATION 6 ACCESS 0
	ADD OPTION aRotina	TITLE 'Nota'		 					ACTION aNota OPERATION 2 ACCESS 0
	ADD OPTION aDevol		TITLE "Devolu��o"						ACTION "U_CADSZ5D()"	OPERATION 6 ACCESS 0
	ADD OPTION aDevol		TITLE "Visualizar Devolu��o"		ACTION "U_CADSZ5F()"	OPERATION 6 ACCESS 0
	ADD OPTION aRotina	TITLE 'Devolu��o'		 				ACTION aDevol OPERATION 2 ACCESS 0
	ADD OPTION aRotina	TITLE "Excluir Medi��o"				ACTION "U_CADSZ5C()"	OPERATION 6 ACCESS 0
	If (Alltrim(RetCodUsr()) $ Alltrim(GetMv("SA_USUCONT")))
		ADD OPTION aRotina	TITLE "Liberar Manualmente"		ACTION "U_CADSZ5G()"	OPERATION 6 ACCESS 0
	EndIf
Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	Local oStructSZ5 := Nil
	Local oModel     := Nil
	//-----------------------------------------
	//Monta a estrutura do formul�rio com base no dicion�rio de dados
	//-----------------------------------------
	oStructSZ5 := FWFormStruct(1,"SZ5")

	//-----------------------------------------
	//Monta o modelo do formul�rio
	//-----------------------------------------
	oModel:= MPFormModel():New("YCADSZ5",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
	oModel:AddFields("SZ5MASTER",/*cOwner*/, oStructSZ5 ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)
	oModel:SetPrimaryKey({"Z5_FILIAL","Z5_CONTRAT","Z5_MEDICAO","Z5_EMPPV1","Z5_PV01"})

Return(oModel)


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
	Local oStructSZ5	:= FWFormStruct( 2, 'SZ5' )
	Local oModel		:= FWLoadModel( 'CADSZ5' )
	Local oView
	Local oCalc1
	oView	:= FWFormView():New()

	//oView:SetUseCursor(.F.)
	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField("SZ5MASTER",oStructSZ5)

	oView:CreateHorizontalBox("CABEC",100)

	oView:SetOwnerView( "SZ5MASTER","CABEC")

	oView:SetProgressBar(.T.)
Return oView

/*/{Protheus.doc} CADSZ5A
Gera��o das notas fiscais
@author Diogo
@since 17/09/2014
@version 1.0
/*/
User Function CADSZ5A
	Local aArea	:= GetArea()
	Local cMarca	:= oMark:Mark()
	Local aDocFat	:= {}
	Local lLockX6	:= .F.
	Local lLockX5	:= .F.
	Local cFilSx5 := xFilial("SX5")
	Local nCnt01  := 0
	Local aLocks	:= {}
	Private _lOk	:= .T.
	Private cAlias	:= getNextAlias()
	Private cDocEnt,cSerEnt,cMsgErro := ""

	If !MsgYesNo("Confirmar a Gera��o das Notas selecionadas?")
		Return
	EndIf

	QueryMarca(cAlias)

	IF ExistBlock("CHGX5FIL")
		cFilSx5 := ExecBlock("CHGX5FIL",.f.,.f.)
	Endif

//FATURAMENTO : RAFAEL //	Begin Transaction
		While !(cAlias)->(Eof())
			lLockX6	:= .F.
/*/ 			
			If GETMV("MV_NUMITEN",.T.) .and. "SX6" $ Alltrim(SuperGetMv("SA_TABLOC",.F.,"SX6_SX5_SB2"))
				AADD(aLocks,{"SX6",SX6->(Recno())})
			EndIf
			SX5->(DbSetOrder(1)) 
			If SX5->(DbSeek( cFilSx5 + "01" + PadR("1",GetSx3Cache("F2_SERIE","X3_TAMANHO")),.F. )) .and. ;
				"SX5" $ Alltrim(SuperGetMv("SA_TABLOC",.F.,"SX6_SX5_SB2"))
				
				AADD(aLocks,{"SX5",SX5->(Recno())})
			EndIf
			If SX5->(DbSeek( "010105" + "01" + PadR("1",GetSx3Cache("F2_SERIE","X3_TAMANHO")),.F. )) .and. ;
				"SX5" $ Alltrim(SuperGetMv("SA_TABLOC",.F.,"SX6_SX5_SB2")) 
				AADD(aLocks,{"SX5",SX5->(Recno())})
			EndIf
*/
			If !Empty((cAlias)->Z5_NF01)
				cMsgErro+="Notas j� faturadas anteriormente !!!"+chr(13)+chr(10)
				_lOk:= .F.
			Endif

			If !_lOk
				// FATURAMENTO : RAFAEL // DisarmTransaction()
				Exit
			Endif

			//Verifica se tem estoque dispon�vel para o Pedido de Vendas da Matriz
			_lOk := FEstDspZ5((cAlias)->Z5_EMPPV2,(cAlias)->Z5_PV02,@aLocks)
			If !_lOk
				cMsgErro	 += "Sem Estoque!"+chr(13)+chr(10)
				// FATURAMENTO : RAFAEL // DisarmTransaction()
				Exit
			Endif

			/* FATURAMENTO : RAFAEL 
			If SuperGetMv("SA_FILAOP",.F.,.T.) //Ativa ou n�o o Loc de tabelas
				If !u_MultLock(aLocks)
					// FATURAMENTO : RAFAEL // DisarmTransaction()
					_lOk	:= .F.
					cMsgErro	+= "Problema ao tentar reservar registros exclusivos, tente de novo em alguns minutos!"
					Exit
				EndIF
			Endif	
			*/		
			
			ccClient	:= Posicione("SC5",1,(cAlias)->Z5_EMPPV1+(cAlias)->Z5_PV01,"C5_CLIENTE")
			ccLoja		:= Posicione("SC5",1,(cAlias)->Z5_EMPPV1+(cAlias)->Z5_PV01,"C5_LOJACLI")

			//Verifica se o cliente est� bloqueado
			If Posicione("SA1",1,xFilial("SA1",(cAlias)->Z5_EMPPV1)+ccClient+ccLoja,"A1_YBLOQ") == "S"
					ALERT("O Cliente("+ccClient+"/"+ccLoja+") selecionado est� Bloqueado.... "+chr(10)+chr(13)+"Favor verificar junto ao setor Financeiro...")
					// FATURAMENTO : RAFAEL // DisarmTransaction()
					Exit
			Endif

			aDocFat := {}
			//Nota Fiscal de Sa�da da Matriz para o Comercial
			Processa({|| _lOk := FGeraFat((cAlias)->Z5_EMPPV2,(cAlias)->Z5_PV02,@aDocFat,@cMsgErro,(cAlias)->Z5_CONTRAT) },"Nota Fiscal de Sa�da da Matriz para o Comercial","Aguarde ...",.F.)
			GravarSZ5(3,"Z5_NF02","Z5_SERIE2",(cAlias)->Z5_EMPPV2,(cAlias)->Z5_PV02,aDocFat)
			cDocEnt:= aDocFat[1]
			cSerEnt:= aDocFat[2]
			GravDest(cAlias,cDocEnt,cSerEnt)	//Grava campos F2_FILDEST,F2_FORDES,F2_LOJADES para gravar a CHVNFE no Danfe de entrada

			If !_lOk
				// FATURAMENTO : RAFAEL // DisarmTransaction()
				Exit
			Endif


			//Nota Fiscal de Entrada do Comercial
			Processa({|| FatuPC((cAlias)->Z5_EMPPC4,(cAlias)->Z5_PC04,5,"Z5_NF04","Z5_SERIE4",cDocEnt,cSerEnt) },"Nota Fiscal de Entrada da Matriz para o Comercial","Aguarde ...",.F.)

			If !_lOk
				// FATURAMENTO : RAFAEL // DisarmTransaction()
				Exit
			Endif

			aDocFat := {}
			//Nota Fiscal de Sa�da do Comercial para Cliente
			Processa({|| _lOk := FGeraFat((cAlias)->Z5_EMPPV1,(cAlias)->Z5_PV01,@aDocFat,@cMsgErro,(cAlias)->Z5_CONTRAT) },"Nota Fiscal de Sa�da do Comercial para Cliente","Aguarde ...",.F.)
			GravarSZ5(2,"Z5_NF01","Z5_SERIE1",(cAlias)->Z5_EMPPV1,(cAlias)->Z5_PV01,aDocFat)

			If !_lOk
				// FATURAMENTO : RAFAEL // DisarmTransaction()
				Exit
			Endif

			aDocFat := {}
			//Nota Fiscal de Sa�da da Matriz para o Cliente
			Processa({|| _lOk := FGeraFat((cAlias)->Z5_EMPPV3,(cAlias)->Z5_PV03,@aDocFat,@cMsgErro,(cAlias)->Z5_CONTRAT) },"Nota Fiscal de Sa�da da Matriz para o Cliente","Aguarde ...",.F.)
			GravarSZ5(4,"Z5_NF03","Z5_SERIE3",(cAlias)->Z5_EMPPV3,(cAlias)->Z5_PV03,aDocFat)

			If !_lOk
				// FATURAMENTO : RAFAEL // DisarmTransaction()
				Exit
			Endif
			(cAlias)->(dbSkip())
		Enddo
	// FATURAMENTO : RAFAEL // End Transaction

	MsUnLockAll()

	If !_lOk
		Aviso("Aviso",cMsgErro+"Processo n�o realizado!",{"Ok"})
	Else
		Aviso("Aviso","Processo finalizado com sucesso",{"Ok"})
	Endif

	(cAlias)->(dbCloseArea())

	dbSelectArea("SZ5")
	SZ5->(MsUnlock())
	RestArea(aArea)
	TirarMarca()

Return

/*/{Protheus.doc} FGeraFat
Fatura o Pedido de venda
@author Diogo/Saulo
@since 17/09/2014
@version 1.0
@param cEmpFat, String, Filial onde esta o pedido de venda para faturar
@param cPedFat, String, Numero do pedido de venda para faturar
@param aDocFat, Array, Array que recebe por referencia para gravar o Numero da NF e Serie
@param cMsgErro, String, String que recebe por referencia para gravar a msg de erro
/*/
Static Function FGeraFat(cEmpFat,cPedFat,aDocFat,cMsgErro,cContr)
	Local aArea		:= getArea()
	Local aPvlNfs		:= {}
	Local cSerie		:= PadR("1",GetSx3Cache("F2_SERIE","X3_TAMANHO"))
	Local cFilBkp		:= cFilAnt
	Local cNFSaida	:= ""
	Local lRet			:= .T.
	Default aDocFat	:= {}
	Default cMsgErro	:= ""
	cFilAnt := cEmpFat

	ProcRegua(4)
	IncProc("Gerando Nota Fiscal de Sa�da")
	ProcessMessage()

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	If !(SC9->(DbSeek(cEmpFat+cPedFat))) //Caso n�o tenha sido Liberado, chama para realizar a Libera��o do Pedido
		FLibPedSZ5(cEmpFat,cPedFat)
	Endif

	dbSelectArea("SC9")
	SC9->(dbSetOrder(1))

	If SC9->(DbSeek(cEmpFat+cPedFat))
		While SC9->(!Eof()) .AND. SC9->C9_FILIAL == cEmpFat .AND. SC9->C9_PEDIDO == cPedFat

			SC6->(DbSeek(xFilial("SC6")+SC9->C9_PEDIDO+SC9->C9_ITEM+SC9->C9_PRODUTO))
			SE4->(DbSeek(xFilial("SE4")+SC5->C5_CONDPAG))
			SB1->(DbSeek(xFilial("SB1")+SC9->C9_PRODUTO))
			SB2->(DbSeek(xFilial("SB2")+SC9->C9_PRODUTO+SC9->C9_LOCAL))
			SF4->(DbSeek(xFilial("SF4")+SC6->C6_TES))

			aAdd(aPvlNfs,{;
				SC9->C9_PEDIDO,;
				SC9->C9_ITEM,;
				SC9->C9_SEQUEN,;
				SC9->C9_QTDLIB,;
				SC9->C9_PRCVEN,;
				SC9->C9_PRODUTO,;
				.F.,;
				SC9->(RECNO()),;
				SC5->(RECNO()),;
				SC6->(RECNO()),;
				SE4->(RECNO()),;
				SB1->(RECNO()),;
				SB2->(RECNO()),;
				SF4->(RECNO());
				})
			SC9->(DbSkip())
		EndDo
	EndIf

	If Len(aPvlNfs) == 0
		cMsgErro += "Pedido "+cPedFat+" n�o liberado"+ CHR(10) + CHR(13)
		_lOk:= .F.
		RestArea(aArea)
		cFilAnt:= cFilBkp
		lRet	:= .F.
		Return lRet
	endif

	dDatSZ5:= Ddatabase //Data a considerar para o inicio da condi��o de pagamento

	If !empty(cContr)
		dDatSZ5:= Posicione("SZ1",1,xFilial("SZ1")+cContr,"Z1_DTCOND")
		If Empty(dTos(dDatSZ5))
			dDatSZ5:=Ddatabase
		Endif
	endif
	//Verifica se a numera��o da NF est� em uso:
	dbSelectArea("SX5")
	dbSetOrder(1)


/*

	-- FATURAMENTO PARALELO : RAFAEL
	
	If SX5->(MsSeek(cEmpFat+"01"+cSerie))
			nVezes := 0
			While ( !SX5->(MsRLock()) )
				nVezes ++
				IncProc("Aguardando libera��o de numera��o da Nota Fiscal de Sa�da ... "+cValtochar(nVezes))
				If ( nVezes > 30 )
					Exit
				EndIf
				Sleep(1000)
			EndDo
	Endif		
	
*/
	
	IncProc("Gerando Nota Fiscal de Sa�da")	

	//Gera documento de saida
	Pergunte("MT460A",.F.)
	cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .T. , .T. , .F. , .F., 0, 0, .F., .F.)
	// cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .F. , .F. , .F. , .F., 0, 0, .F., .F.)

	If Empty(cNFSaida)
		_lOk:= .F.
		If Empty(cNFSaida)
			cMsgErro += "N�o foi poss�vel gerar a Nota fiscal de Sa�da da Filial "+cFilAnt+ CHR(10) + CHR(13)
		EndIf
	Else
		aDocFat	:= {cNFSaida,cSerie}
	Endif
	IncProc("Nota Fiscal de Sa�da gerada")
	
/*	
	-- FATURAMENTO PARALELO : RAFAEL
	
	dbSelectArea("SX5")
	If !SX5->(MsRLock()) .and. SX5->(!eof())
		SX5->(MsUnlock())
	Endif	
*/

	RestArea(aArea)
	cFilAnt:= cFilBkp
Return lRet

Static Function GravarSZ5(cOrd,cDocNF,cSerNF,cEmpFat,cPedFat,aDocFat)
	Default aDocFat	:= {"",""}
	dbSelectArea("SZ5")
	SZ5->(dbSetOrder(cOrd))
	if SZ5->(dbSeek(cEmpFat+cPedFat))
		RecLock("SZ5",.F.)
		&(cDocNF) := aDocFat[1]
		&(cSerNF) := aDocFat[2]
		SZ5->(MsUnlock())
	endif
Return

/*/{Protheus.doc} FatuPC
Gera documento de entrada referente ao Pedido de Compra
@author Diogo/Saulo
@since 18/09/2014
@version 1.0
/*/
Static Function FatuPC(cEmpFat,cPedFat,cOrd,cCampNF,cCampSer,cDocNF,cSerieNF)
	Local aCabec := {}
	Local aItens := {}
	Local aLinha := {}
	Local cFilBkp:= cFilAnt
	Private lMsErroAuto := .F.
	cFilAnt	:= cEmpFat

	ProcRegua(4)
	IncProc("Gerando Nota Fiscal de Entrada")
	ProcessMessage()

	dbSelectArea("SC7")
	SC7->(dbSetOrder(1))
	SC7->(dbSeek(xFilial("SC7",cEmpFat)+cPedFat))

	aadd(aCabec,{"F1_TIPO"   ,"N"})
	aadd(aCabec,{"F1_FORMUL" ,"N"})
	aadd(aCabec,{"F1_DOC"    ,cDocNF})
	aadd(aCabec,{"F1_SERIE"  ,cSerieNF})
	aadd(aCabec,{"F1_EMISSAO",dDataBase})
	aadd(aCabec,{"F1_FORNECE",SC7->C7_FORNECE})
	aadd(aCabec,{"F1_LOJA"   ,SC7->C7_LOJA})
	aadd(aCabec,{"F1_ESPECIE","SPED"})
	aadd(aCabec,{"E2_NATUREZ",GetMv("SA_NATPAG")})

	While SC7->(!eof()) .and. SC7->C7_FILIAL+SC7->C7_NUM == xFilial("SC7",cEmpFat)+cPedFat
		aLinha := {}
		Aadd(aLinha,{"D1_COD"     	,SC7->C7_PRODUTO		,Nil})
		Aadd(aLinha,{"D1_ITEM"     	,SC7->C7_ITEM			,Nil})
		Aadd(aLinha,{"D1_QUANT"		,SC7->C7_QUANT			,Nil})
		Aadd(aLinha,{"D1_VUNIT"		,SC7->C7_PRECO			,Nil})
		Aadd(aLinha,{"D1_TOTAL"		,SC7->C7_TOTAL			,Nil})
		Aadd(aLinha,{"D1_TES"		,SC7->C7_TES			,Nil})
		Aadd(aLinha,{"D1_PEDIDO"	,SC7->C7_NUM			,Nil})
		Aadd(aLinha,{"D1_ITEMPC"	,SC7->C7_ITEM			,Nil})
		Aadd(aLinha,{"D1_LOCAL"		,SC7->C7_LOCAL			,Nil})

		NNR->(dbSetOrder(1))
		NNR->(DBGOTOP())
		If (!NNR->(DbSeek(xFilial("NNR")+SC7->C7_LOCAL)))
			AGRA045(;
				{{"NNR_CODIGO",SC7->C7_LOCAL,NIL};
				,{"NNR_DESCRI","Armazem "+SC7->C7_LOCAL,NIL};
				};
				,3,{},{})
		EndIf
		aadd(aItens,aLinha)
		SC7->(DbSkip())
	EndDo

	MATA103(aCabec,aItens)
	If !lMsErroAuto
		_lOk:= .T.
		If cOrd > 0
			dbSelectArea("SZ5")
			SZ5->(dbSetOrder(cOrd))
			if SZ5->(dbSeek(cEmpFat+cPedFat))
				RecLock("SZ5",.F.)
				&(cCampNF)	:= SF1->F1_DOC
				&(cCampSer)	:= SF1->F1_SERIE
				SZ5->(MsUnlock())
			endif
		EndIf
	Else
		_lOk:= .F.
		Mostraerro()
		cMsgErro += "N�o foi poss�vel gerar a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
	EndIf

	cFilAnt:= cFilBkp

Return _lOk
/*/{Protheus.doc} FEstDspZ5
Verifica se tem Estoque Dispon�vel
@author Diogo
@since 17/09/2014
@version 1.0
/*/
Static Function FEstDspZ5(cEmpFat,cPedFat,aLocks)
	Local lRet	:= .T.
	Local cMsg	:= ""
	Default aLocks	:= {}

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	If SC6->(DbSeek(cEmpFat+cPedFat))

		While SC6->(!Eof()) .AND. SC6->C6_FILIAL == cEmpFat .AND. SC6->C6_NUM == cPedFat
			dbSelectArea("SB2")
			dbSetOrder(1)
			If SB2->(DbSeek(xFilial("SB2",cEmpFat)+SC6->C6_PRODUTO+SC6->C6_LOCAL)) //.and. ; FATURAMENTO RAFAEL
				//"SB2" $ Alltrim(SuperGetMv("SA_TABLOC",.F.,"SX6_SX5_SB2"))
				//AADD(aLocks,{"SB2",SB2->(Recno())})
				//nQuantEst:= SaldoSb2(,.F.)
				nQuantEst:= SaldoSb2(,.F.,,,,,,,.F.)
				
				//ALERT(nQuantEst)
				
				If nQuantEst < SC6->C6_QTDVEN
					cMsg+= "Produto "+Alltrim(SC6->C6_PRODUTO)+" - "+;
						Alltrim(Posicione("SB1",1,xFilial("SB1")+SC6->C6_PRODUTO,"B1_DESC"))+" sem estoque dispon�vel "+;
						"(Saldo disponivel "+cValtoChar(nQuantEst)+" / "+;
						"Quantidade requisitada "+cValtoChar(SC6->C6_QTDVEN)+")"+chr(13)+chr(10)
					lRet:= .F.
				Endif
		//	FATURAMENTO ----	If SB2->(DbSeek(xFilial("SB2")+SC6->C6_PRODUTO+SC6->C6_LOCAL)) .and. ;
		//			"SB2" $ Alltrim(SuperGetMv("SA_TABLOC",.F.,"SX6_SX5_SB2"))
		//			AADD(aLocks,{"SB2",SB2->(Recno())})
		//		EndIf
			Else
				cMsg+= "Produto "+Alltrim(SC6->C6_PRODUTO)+" - "+;
					Alltrim(Posicione("SB1",1,xFilial("SB1")+SC6->C6_PRODUTO,"B1_DESC"))+" sem estoque dispon�vel "+;
					"(Saldo disponivel 0 /"+;
					"Quantidade requisitada "+cValtoChar(SC6->C6_QTDVEN)+")"+chr(13)+chr(10)
				lRet:= .F.
			Endif

			dbSelectArea("SC6")
			SC6->(DbSkip())
		EndDo

	EndIf

	If !lRet
		//Aviso("Aviso",cMsg,{"Ok"})
		cMsgErro	+= cMsg
	Endif

Return lRet

/*/{Protheus.doc} CADSZ5B
Respons�vel pela exclus�o das Notas Fiscais
@author Diogo
@since 18/09/2014
@version 1.0
/*/
User Function CADSZ5B
	Local cMarca		:= oMark:Mark()
	Local cCNPJMatriz	:= FWArrFilAtu("01","010105")[18]	//Matriz
	Local aFornec		:= {"",""}
	Private _lOk	:= .T.
	Private cAlias	:= getNextAlias()
	Private cDocEnt,cSerEnt,cMsgErro := ""
	
	

	
	
	
	

	SA2->(DbSetOrder(3))	//A2_FILIAL+A2_CGC
	If SA2->(DbSeek(xFilial("SA2")+cCNPJMatriz))
		aFornec[1]	:= SA2->A2_COD
		aFornec[2]	:= SA2->A2_LOJA
	Else
		Aviso("Aviso","Necessario ter a Matriz cadastrado como fornecedor!",{"Ok"})
		Return
	EndIf

	If !MsgYesNo("Confirmar a exclus�o das notas selecionadas?")
		Return
	EndIf

	QueryMarca(cAlias)
	
	
	// -----------------------------------------------
	// rafael inicio exclusao
	// -----------------------------------------------
	
	
	IF GETMV("SA_VLDEXC")
	
			_SASArea := getArea()
			
			_qDelDoc := "SELECT F2_FILIAL, F2_DOC, F2_SERIE, F2_DAUTNFE, F2_HAUTNFE FROM SF2010 WHERE " 
			_qDelDoc += " (F2_FILIAL='"+(cAlias)->Z5_EMPPV1+"' AND F2_DOC = '"+(cAlias)->Z5_NF01+"') OR "
			_qDelDoc += " (F2_FILIAL='"+(cAlias)->Z5_EMPPV2+"' AND F2_DOC = '"+(cAlias)->Z5_NF02+"') OR "
			_qDelDoc += " (F2_FILIAL='"+(cAlias)->Z5_EMPPV3+"' AND F2_DOC = '"+(cAlias)->Z5_NF03+"') AND D_E_L_E_T_=''" 
			
			IF Select("SDEL")>0
				SDEL->(dbCloseArea())
			Endif
			
			TCQUERY _qDelDoc NEW ALIAS "SDEL"	
			
			
			MemoWrite( "c:/temp/testSave.txt", _qDelDoc )
			 
			 
			While !(SDEL->(eof()))
			
				_HrCanc := SuperGetMv( "MV_SPEDEXC" , .F. , 24 , SDEL->F2_FILIAL  )
				_HrEmis := SubtHoras(STOD(SDEL->F2_DAUTNFE),SDEL->F2_HAUTNFE, Date(), substr(Time(),1,2)+":"+substr(Time(),4,2))
				
				
				If _HrEmis > _HrCanc
					cMsgErr_="N�o foi possivel excluir as notas, pois a NF-e "+SDEL->F2_DOC+" est� fora do prazo de cancelamento que � de " + Alltrim(STR(_HrCanc)) +" horas "+chr(13)+chr(10)
					Aviso("Aviso",cMsgErr_+"Processo n�o realizado!",{"Ok"})
					Return
				EndIf	
			
				DbSkip()
			End Do
			
			RestArea(_SASArea)
			
	ENDIF
	
	// -----------------------------------------------
	// rafael exclusao
	// -----------------------------------------------
	
	

	// Begin Transaction FATURAMENTO : RAFAEL

		While !(cAlias)->(Eof())

			If Empty((cAlias)->Z5_NF01)
				cMsgErro+="N�o existi Nota Fiscal Faturada, selecione um registro j� faturados !!!"+chr(13)+chr(10)
				_lOk:= .F.
			Endif

			If !_lOk
				// DisarmTransaction()     FATURAMENTO : RAFAEL
				Exit
			Endif


			//Exclui Nota Fiscal de Sa�da do Comercial para Cliente
			Processa({|| FCancFat((cAlias)->Z5_EMPPV1,(cAlias)->Z5_NF01,(cAlias)->Z5_SERIE1) },"Nota Fiscal de Sa�da do Comercial para o Cliente","Aguarde ...",.F.)
			GravarSZ5(2,"Z5_NF01","Z5_SERIE1",(cAlias)->Z5_EMPPV1,(cAlias)->Z5_PV01,{"",""})

			If !_lOk
				// DisarmTransaction()      FATURAMENTO : RAFAEL
				Exit
			Endif

			//Exclui Nota Fiscal de Sa�da da Matriz para o Comercial
			Processa({||FCancFat((cAlias)->Z5_EMPPV2,(cAlias)->Z5_NF02,(cAlias)->Z5_SERIE2) },"Nota Fiscal de Sa�da da Matriza para o Comercial","Aguarde ...",.F.)
			GravarSZ5(3,"Z5_NF02","Z5_SERIE2",(cAlias)->Z5_EMPPV2,(cAlias)->Z5_PV02,{"",""})

			If !_lOk
				//DisarmTransaction()   FATURAMENTO : RAFAEL
				Exit
			Endif

			//Exclui Nota Fiscal de Sa�da da Matriz para o Cliente
			Processa({||FCancFat((cAlias)->Z5_EMPPV3,(cAlias)->Z5_NF03,(cAlias)->Z5_SERIE3) },"Nota Fiscal de Sa�da da Matriz para o Cliente","Aguarde ...",.F.)
			GravarSZ5(4,"Z5_NF03","Z5_SERIE3",(cAlias)->Z5_EMPPV3,(cAlias)->Z5_PV03,{"",""})

			If !_lOk
				// DisarmTransaction()    FATURAMENTO : RAFAEL
				Exit
			Endif

			//Exclui Nota Fiscal de Entrada
			Processa({||FCancNfEnt((cAlias)->Z5_EMPPC4,(cAlias)->Z5_NF04,(cAlias)->Z5_SERIE4,aFornec) },"Nota Fiscal de Entrada","Aguarde ...",.F.)
			GravarSZ5(5,"Z5_NF04","Z5_SERIE4",(cAlias)->Z5_EMPPC4,(cAlias)->Z5_PC04,{"",""})

			If !_lOk
				// DisarmTransaction()       FATURAMENTO : RAFAEL
				Exit
			Endif


			(cAlias)->(dbSkip())
		Enddo

	// End Transaction  FATURAMENTO : RAFAEL

	If !_lOk
		Aviso("Aviso",cMsgErro+"Processo n�o realizado!",{"Ok"})
	Else
		Aviso("Aviso","Documentos exclu�dos com sucesso",{"Ok"})
	Endif

	(cAlias)->(dbCloseArea())

	dbSelectArea("SZ5")
	SZ5->(MsUnlock())

	TirarMarca()

Return

/*/{Protheus.doc} FCancFat
Faz exclus�o das Notas Fiscais de Sa�da
@author Diogo
@since 18/09/2014
@version 1.0
/*/

Static Function FCancFat(cEmpFat,cDocNF,cSerNF)

	Local cFilBkp	:= cFilAnt
	Local nSpedExc:= GetNewPar("MV_SPEDEXC",24)
	Local lRet		:= .T.
	
	conout("nSpedExc:"+str(nSpedExc))
	cFilAnt:= cEmpFat

	aRegSD2:= {}
	aRegSE1:= {}
	aRegSE2:= {}

	_lMostraCTB	:= .F.
	_lAglCTB	:= .F.
	_lContab	:= .F.
	_lCarteira	:= .T.

	dbSelectArea("SF2")
	SF2->(dbSetOrder(1))
	SF2->(dbSeek(cEmpFat+cDocNF+cSerNF))

	ProcRegua(4)
	IncProc("Excluindo Nota Fiscal de Sa�da "+cDocNF)
	ProcessMessage()

	//Verifica se poder� realizar a exclus�o da nota fiscal
	If !Empty(SF2->F2_DAUTNFE)
		nHoras := SubtHoras(SF2->F2_DAUTNFE,SF2->F2_HAUTNFE, Date(), substr(Time(),1,2)+":"+substr(Time(),4,2))
		If nHoras > nSpedExc
			cMsgErro+="N�o foi possivel excluir a(s) nota(s), pois o prazo para o cancelamento da(s) NF-e � de " + Alltrim(STR(nSpedExc)) +" horas "+chr(13)+chr(10)
			_lOk	:= .F.
			lRet	:= .F.
			Return lRet
		EndIf
	Endif

	cMarca := GetMark(,"SF2","F2_OK")

	RecLock("SF2",.F.)
	SF2->F2_OK := cMarca
	MSUNLOCK()

	_lRetExc := MaCanDelF2("SF2",SF2->(RecNo()),@aRegSD2,@aRegSE1,@aRegSE2)

	If _lRetExc

		SF2->(MaDelNFS(aRegSD2,aRegSE1,aRegSE2,_lMostraCTB,_lAglCTB,_lContab,_lCarteira))

	Else
		AutoGrLog("Erro na exclus�o do Documento de Sa�da da Filial "+cFilAnt)
		Mostraerro()
		cMsgErro+="Erro na exclus�o do Documento de Sa�da da Filial "+cFilAnt+chr(13)+chr(10)
		_lOk:= .F.
	EndIf
	cFilAnt:= cFilBkp

Return

/*/{Protheus.doc} FCancNfEnt
Efetua estorno do Documento de Entrada
@author Diogo
@since 18/09/2014
@version 1.0
/*/
Static Function FCancNfEnt(cEmpFat,cDocNF,cSerNF,aFornece)

	Local aCabec := {}
	Local aItens := {}
	Local aLinha := {}
	Local cFilBkp:= cFilAnt
	Local lRet		:= .T.
	Private lMsErroAuto := .F.
	cFilAnt	:= cEmpFat

	dbSelectArea("SF1")
	SF1->(dbSetOrder(1))	//F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA+F1_TIPO
	SF1->(dbSeek(xFilial("SF1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]))

	ProcRegua(4)
	IncProc("Excluindo Nota Fiscal de Entrada "+cDocNF)
	ProcessMessage()

	aadd(aCabec,{"F1_TIPO"   ,SF1->F1_TIPO})
	aadd(aCabec,{"F1_FORMUL" ,SF1->F1_FORMUL})
	aadd(aCabec,{"F1_DOC"    ,SF1->F1_DOC})
	aadd(aCabec,{"F1_SERIE"  ,SF1->F1_SERIE})
	aadd(aCabec,{"F1_EMISSAO",SF1->F1_EMISSAO})
	aadd(aCabec,{"F1_FORNECE",SF1->F1_FORNECE})
	aadd(aCabec,{"F1_LOJA"   ,SF1->F1_LOJA})
	aadd(aCabec,{"F1_ESPECIE",SF1->F1_ESPECIE})

	dbSelectArea("SD1")
	SD1->(dbSetOrder(1))	//D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
	SD1->(dbSeek(xFilial("SD1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]))


	While SD1->(!eof()) .and. SD1->D1_FILIAL+SD1->D1_DOC+SD1->D1_SERIE == xFilial("SD1",cEmpFat)+cDocNF+cSerNF
		aLinha := {}

		Aadd(aLinha,{"D1_COD"     	,SD1->D1_COD			,Nil})
		Aadd(aLinha,{"D1_ITEM"     	,SD1->D1_ITEM			,Nil})
		Aadd(aLinha,{"D1_QUANT"		,SD1->D1_QUANT			,Nil})
		Aadd(aLinha,{"D1_VUNIT"		,SD1->D1_VUNIT			,Nil})
		Aadd(aLinha,{"D1_TOTAL"		,SD1->D1_TOTAL			,Nil})
		Aadd(aLinha,{"D1_PEDIDO"		,SD1->D1_PEDIDO			,Nil})
		Aadd(aLinha,{"D1_ITEMPC"		,SD1->D1_ITEMPC			,Nil})
		Aadd(aLinha,{"D1_LOCAL"		,SD1->D1_LOCAL			,Nil})

		NNR->(dbSetOrder(1))
		NNR->(DBGOTOP())
		If (!NNR->(DbSeek(xFilial("NNR")+SD1->D1_LOCAL)))
			AGRA045(;
				{{"NNR_CODIGO",SD1->D1_LOCAL,NIL};
				,{"NNR_DESCRI","Armazem "+SD1->D1_LOCAL,NIL};
				};
				,3,{},{})
		EndIf
		aadd(aItens,aLinha)
		SD1->(DbSkip())
	EndDo

	MATA103(aCabec,aItens,5)

	If !lMsErroAuto

	Else
		_lOk:= .F.
		lRet	:= .F.
		Mostraerro()
		cMsgErro += "N�o foi poss�vel excluir a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
	EndIf

	cFilAnt:= cFilBkp

Return lRet


Static Function FLibPedSZ5(cEmpFat,cPedFat)

	Local aArea1:= getArea()
	Local aCab	:= {}
	Local aItem	:= {}

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	aCab:= {	{"C5_TIPO"		,SC5->C5_TIPO		,NIL};
		,{"C5_NUM"		,SC5->C5_NUM		,NIL};
		,{"C5_CLIENTE"	,SC5->C5_CLIENTE	,NIL};
		,{"C5_CLIENT"	,SC5->C5_CLIENT		,NIL};
		,{"C5_LOJACLI"	,SC5->C5_LOJACLI	,NIL}; //		,{"C5_TPEDAV"	,'23'				,NIL};
		,{"C5_CONDPAG"	,SC5->C5_CONDPAG	,NIL};
		,{"C5_EMISSAO"  ,SC5->C5_EMISSAO	,NIL}}


	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	If SC6->(DbSeek(cEmpFat+cPedFat))

		While SC6->(!Eof()) .AND. SC6->C6_FILIAL == cEmpFat .AND. SC6->C6_NUM == cPedFat

			aAdd(aItem,{ {"C6_PRODUTO"	,SC6->C6_PRODUTO,NIL};
				,{"C6_ITEM"		,SC6->C6_ITEM   ,NIL};
				,{"C6_QTDVEN"	,SC6->C6_QTDVEN ,NIL};
				,{"C6_PRCVEN"	,SC6->C6_PRCVEN ,NIL};
				,{"C6_VALOR"	,SC6->C6_VALOR	,NIL};
				,{"C6_TES"		,SC6->C6_TES	,NIL};
				,{"C6_CLI"		,SC6->C6_CLI	,NIL};
				,{"C6_QTDLIB"	,SC6->C6_QTDVEN	,NIL};
				,{"C6_LOJA"		,SC6->C6_LOJA	,NIL}})


			SC6->(DbSkip())
		EndDo

		lMsErroAuto := .F.

		MSExecAuto({|x,y,z|Mata410(x,y,z)},aCab,aItem,4)

		If lMsErroAuto
			MostraErro()
			cMsgErro += "Pedido "+cPedFat+" n�o foi liberado"+ CHR(10) + CHR(13)
			_lOk:= .F.
		Endif

	Endif

	RestArea(aArea1)
Return

/*/{Protheus.doc} CADSZ5C
Exclus�o da Medi��o
@author Diogo
@since 18/09/2014
@version 1.0
/*/
User Function CADSZ5C
	Local cMarca	:= oMark:Mark()
	Private _lOk	:= .T.
	Private cAlias	:= getNextAlias()
	Private cDocEnt,cSerEnt,cMsgErro := ""

	if !(__cUserID $ GetMV("SA_ALTPED"))//Weskley 06/05/2015
		Msginfo("Usu�rio sem permiss�o para excluir o Medi��o ","Permissao de Acesso")
		return 
	endif	

	If !MsgYesNo("Confirmar a exclus�o das Medi��es selecionadas?")
		Return
	EndIf

	QueryMarca(cAlias)

	Begin Transaction
		While !(cAlias)->(Eof())

			If !Empty((cAlias)->Z5_NF01)
				cMsgErro+="Exclus�o n�o permitida! "+chr(13)+chr(10)+"Dever� excluir as Notas Fiscais anteriormente."+chr(13)+chr(10)
				_lOk:= .F.
			Endif

			If !_lOk
				DisarmTransaction()
				Exit
			Endif

			//Exclui tabela de Opera��o Triangular(SZZ5)
			dbSelectArea("SZ5")
			SZ5->(dbSetOrder(1))
			If SZ5->(dbSeek(xFilial("SZ5")+(cAlias)->Z5_CONTRAT+(cAlias)->Z5_MEDICAO))
				RecLock("SZ5",.F.)
				SZ5->(dbDelete())
				SZ5->(MsUnlock())
			Endif

			//Exclui tabela de Medi��o(SZ3)
			DbSelectArea("SZ3")
			SZ3->(DbSetOrder(1))
			While SZ3->(dbSeek(xFilial("SZ3")+(cAlias)->Z5_CONTRAT+(cAlias)->Z5_MEDICAO))	//SZ3->(!EOF()) .AND.
				//Volta saldo e qtd medida
				SZ2->(dbSeek(xFilial("SZ2")+SZ3->Z3_CONTRAT+SZ3->Z3_ITEM))
				SZ2->(Reclock("SZ2",.F.))
				SZ2->Z2_QTDMED	-= SZ3->Z3_QTDMED
				SZ2->Z2_SALDO		+= SZ3->Z3_QTDMED
				SZ2->(MsUnlock())

				SZ1->(dbSetOrder(1))
				If SZ1->(dbSeek(xFilial("SZ1")+SZ3->Z3_CONTRAT))
					RecLock("SZ1",.F.)
					SZ1->Z1_QTDMED	-= SZ3->Z3_QTDMED
					SZ1->Z1_SALDO		+= SZ3->Z3_QTDMED
					SZ1->(MsUnlock())
				Endif

				RecLock("SZ3",.F.)
				SZ3->(dbDelete())
				SZ3->(MsUnlock())

				//SZ3->(dbSkip())
			Enddo

			dbSelectArea((cAlias))

			//Estorno dos Pedidos
			//log de inclusao de pedido solicitado pelo atendimento
			U_SASP021((cAlias)->Z5_EMPPV1,(cAlias)->Z5_PV01,dDataBase,"E","TRI","")

			//Exclui Pedido de Venda de Sa�da do Comercial para Cliente
			Processa({|| FCancPed((cAlias)->Z5_EMPPV1,(cAlias)->Z5_PV01,(cAlias)->Z5_NF01,(cAlias)->Z5_SERIE1,2,"Z5_NF01","Z5_SERIE1") },"Estorno do Pedido de Vendas do Comercial para o Cliente","Aguarde ...",.F.)



			If !_lOk
				DisarmTransaction()
				Exit
			Endif

			//Exclui Pedido de Venda de Sa�da da Matriz para o Comercial
			Processa({||FCancPed((cAlias)->Z5_EMPPV2,(cAlias)->Z5_PV02,(cAlias)->Z5_NF02,(cAlias)->Z5_SERIE2,3,"Z5_NF02","Z5_SERIE2") },"Estorno do Pedido de Vendas da Matriza para o Comercial","Aguarde ...",.F.)

			If !_lOk
				DisarmTransaction()
				Exit
			Endif

			//Exclui Pedido de Venda da Matriz para o Cliente
			Processa({||FCancPed((cAlias)->Z5_EMPPV3,(cAlias)->Z5_PV03,(cAlias)->Z5_NF03,(cAlias)->Z5_SERIE3,4,"Z5_NF03","Z5_SERIE3") },"Estorno do Pedido de Vendas da Matriz para o Cliente","Aguarde ...",.F.)

			If !_lOk
				DisarmTransaction()
				Exit
			Endif

			//Exclui Pedido de compra de Entrada
			Processa({||FCancPC((cAlias)->Z5_EMPPC4,(cAlias)->Z5_PC04,(cAlias)->Z5_NF04,(cAlias)->Z5_SERIE4,5,"Z5_NF04","Z5_SERIE4") },"Estorno do Pedido de Compras","Aguarde ...",.F.)

			If !_lOk
				DisarmTransaction()
				Exit
			Endif

			(cAlias)->(dbSkip())
		Enddo
	End Transaction

	If !_lOk
		Aviso("Erro",cMsgErro+"Processo n�o realizado, verificar erro na rotina!",{"Ok"})
	Else
		Aviso("Aviso","Processo finalizado com sucesso",{"Ok"})

	Endif

	(cAlias)->(dbCloseArea())

	dbSelectArea("SZ5")
	SZ5->(MsUnlock())

	TirarMarca()

Return

/*/{Protheus.doc} FCancPed
Cancelamento dos Pedidos de Vendas
Utilizado no estorno da medi��o
@author Diogo
@since 19/09/2014
@version 1.0
/*/
Static Function FCancPed(cEmpFat,cPedFat,cDocNF,cSerNF,cOrd,cCampNF,cCampSer)

	Local aArea		:= getArea()
	Local cFilBkp	:= cFilAnt
	Local aItem		:= {}

	cFilAnt := cEmpFat


	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	ProcRegua(4)
	IncProc("Estorno do Pedido de Vendas "+SC5->C5_NUM)
	ProcessMessage()

	aCab:= {	{"C5_TIPO"	,SC5->C5_TIPO		,NIL};
		,{"C5_NUM"			,SC5->C5_NUM		,NIL};
		,{"C5_CLIENTE"		,SC5->C5_CLIENTE	,NIL};
		,{"C5_CLIENT"		,SC5->C5_CLIENT		,NIL};
		,{"C5_LOJACLI"		,SC5->C5_LOJACLI	,NIL};//		,{"C5_TPEDAV"		,'23'				,NIL};		
		,{"C5_CONDPAG"		,SC5->C5_CONDPAG	,NIL};
		,{"C5_EMISSAO" 		,SC5->C5_EMISSAO	,NIL}}


	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	If SC6->(DbSeek(cEmpFat+cPedFat))
		While SC6->(!Eof()) .AND. SC6->C6_FILIAL == cEmpFat .AND. SC6->C6_NUM == cPedFat
			aAdd(aItem,{ {"C6_PRODUTO"	,SC6->C6_PRODUTO,NIL};
				,{"C6_ITEM"		,SC6->C6_ITEM   ,NIL};
				,{"C6_QTDVEN"	,SC6->C6_QTDVEN ,NIL};
				,{"C6_PRCVEN"	,SC6->C6_PRCVEN ,NIL};
				,{"C6_VALOR"	,SC6->C6_VALOR	,NIL};
				,{"C6_TES"		,SC6->C6_TES	,NIL};
				,{"C6_CLI"		,SC6->C6_CLI	,NIL};
				,{"C6_LOJA"		,SC6->C6_LOJA	,NIL}})
			SC6->(DbSkip())
		EndDo

		//Altera��o para desliberar o documento!
		lMsErroAuto := .F.
		MSExecAuto({|x,y,z|Mata410(x,y,z)},aCab,aItem,4) //Altera��o

		lMsErroAuto := .F.
		MSExecAuto({|x,y,z|Mata410(x,y,z)},aCab,aItem,5) //Exclus�o

		If lMsErroAuto
			MostraErro()
			cMsgErro += "Pedido "+SC5->C5_NUM+" n�o pode ser excluido"+ CHR(10) + CHR(13)
			_lOk:= .F.
		Endif
	Endif
	RestArea(aArea)
	cFilAnt := cFilBkp
Return


/*/{Protheus.doc} FCancPC
Cancelamento dos Pedidos de Compras
Utilizado no estorno da medi��o
@author Diogo
@since 19/09/2014
@version 1.0
/*/
Static Function FCancPC(cEmpFat,cPedFat,cDocNF,cSerNF,cOrd,cCampNF,cCampSer)
	Local aArea		:= getArea()
	Local cFilBkp		:= cFilAnt
	Local aCabPC		:= {}
	Local aItensPC	:= {}
	Local aLinha		:= {}

	cFilAnt := cEmpFat


	dbSelectArea("SC7")
	SC7->(dbSetOrder(1))
	SC7->(dbSeek(cEmpFat+cPedFat))

	ProcRegua(4)
	IncProc("Estorno do Pedido de Compras "+SC7->C7_NUM)
	ProcessMessage()

	aadd(aCabPC,{"C7_NUM"		,SC7->C7_NUM})
	aadd(aCabPC,{"C7_EMISSAO"	,SC7->C7_EMISSAO})
	aadd(aCabPC,{"C7_FORNECE"	,SC7->C7_FORNECE})
	aadd(aCabPC,{"C7_LOJA"		,SC7->C7_LOJA})
	aadd(aCabPC,{"C7_COND"		,SC7->C7_COND})
	aadd(aCabPC,{"C7_CONTATO"	,SC7->C7_CONTATO})
	aadd(aCabPC,{"C7_FILENT"		,SC7->C7_FILENT})


	cChav:= cEmpFat+cPedFat

	SC7->(dbSeek(cEmpFat+cPedFat))


	while SC7->C7_FILIAL+SC7->C7_NUM == cChav .and. SC7->(!eof())

		aadd(aLinha,{"C7_PRODUTO"	,SC7->C7_PRODUTO		,Nil})
		aadd(aLinha,{"C7_QUANT"		,SC7->C7_QUANT			,Nil})
		aadd(aLinha,{"C7_PRECO"		,SC7->C7_PRODUTO		,Nil})
		aadd(aLinha,{"C7_TES"		,SC7->C7_TES			,Nil})
		aadd(aItensPC,aLinha)

		SC7->(dbSkip())
	enddo

	lMsErroAuto	:= .F.
	MsExecAuto({|x,y,z| MATA120(1,x,y,z)}, aCabPC,aItensPC,5)

	If lMsErroAuto
		MostraErro()
		cMsgErro += "Pedido "+SC7->C7_NUM+" n�o pode ser excluido"+ CHR(10) + CHR(13)
		_lOk:= .F.
	Endif

	RestArea(aArea)
	cFilAnt := cFilBkp
Return

/*/{Protheus.doc} CADSZ5D
Devolu��o notas
@author Saulo Gomes Martins
@since 23/09/2014
@version 1.0
/*/
User Function CADSZ5D
	Local aArea	:= GetArea()
	Processa({|| DevOPTriang() },"Selecionando NF para devolu��o","Aguarde ...",.F.)
	RestArea(aArea)
Return

Static Function DevOPTriang()
	Local cMarca		:= oMark:Mark()
	Local lCliente	:= .T.		//Devolu��o usa cliente
	Local lFornece	:= .T.		//Utiliza Fornecedor
	Local cDocSF2   	:= ''
	Local cDocSF1		:= ''
	Local cCNPJMatriz	:= FWArrFilAtu("01","010105")[18]	//Matriz
	Local cCNPJColiga	:= FWArrFilAtu(cEmpAnt,cFilAnt)[18]	//Coligada
	Local aFornec		:= {,}
	Local aClientFil	:= {,}
	Local aClientMtz	:= {,}
	Local aDocDev		:= {}
	Local aDocSF1		:= {}
	Local aDocSF2		:= {}

	Local aLinha    := {}
	Local aItens    := {}
	Local cTipoNF   := "SPED" // INCLUIDO RAFAEL SOLICITA��O RAISSA (Chamado#2015051113000589 � Par�metro para devolu��o - Protheus)
	Local lPoder3   := .T.
	Local nHpP3     := 0

	Private lCADSZ5D	:= .F.
	Private cAlias	:= getNextAlias()

	Private cCliente := CriaVar("F2_CLIENTE",.F.)
	Private cLoja    := CriaVar("F2_LOJA",.F.)
	Private cQrDvF2  := ""
	PRIVATE cTipo		:= "D"

	ProcessMessage()

	SA2->(DbSetOrder(3))	//A2_FILIAL+A2_CGC
	SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
	If SA2->(DbSeek(xFilial("SA2")+cCNPJMatriz))
		aFornec[1]	:= SA2->A2_COD
		aFornec[2]	:= SA2->A2_LOJA
	Else
		Aviso("Aviso","Necessario ter a Matriz cadastrado como fornecedor!",{"Ok"})
		Return
	EndIf

	If SA1->(DbSeek(xFilial("SA1","010105")+cCNPJColiga))
		aClientFil[1]	:= SA1->A1_COD
		aClientFil[2]	:= SA1->A1_LOJA
	Else
		Aviso("Aviso","Necessario essa coligada ter cadastrado como Cliente!",{"Ok"})
		Return
	EndIf

	If SA1->(DbSeek(xFilial("SA1","010105")+cCNPJMatriz))
		aClientMtz[1]	:= SA1->A1_COD
		aClientMtz[2]	:= SA1->A1_LOJA
	Else
		Aviso("Aviso","Necessario a Matriz ter cadastrado como Cliente!",{"Ok"})
		Return
	EndIf

	QueryMarca(cAlias)

	While !(cAlias)->(Eof())
		If Empty((cAlias)->Z5_NF01)
			(cAlias)->(DbSkip())
			Loop
		Endif
		IncProc("Procurando NF para devolu��o...")
		ProcessMessage()
		#IFDEF TOP
			cDocSF2 += "('"+(cAlias)->(Z5_NF01+Z5_SERIE1)+"')"
		#ELSE
			cDocSF2 += "( SD2->D2_DOC == '" + (cAlias)->Z5_NF01 + "' .And. SD2->D2_SERIE == '" + (cAlias)->Z5_SERIE1 + "' ) "
		#ENDIF
		lCADSZ5D	:= .T.
		lCADSZ5D := StaticCall(MATA103,M103FilDv,@aLinha,@aItens,cDocSF2,(cAlias)->Z5_CLIENTE,(cAlias)->Z5_LOJA,.F.,@cTipoNF,@lPoder3,.T.,@nHpP3) .and. Len(aItens)>0
		If Len(aItens)==0
			Help(" ",1,"DSNOTESDT")
		EndIF
		If lCADSZ5D
			//Chama tela padr�o para dar entrada na devolu��o
			lMsErroAuto	:= .F.
			A103ProcDv("SF1",0,3,lCliente,(cAlias)->Z5_CLIENTE,(cAlias)->Z5_LOJA,cDocSF2)
			If lMsErroAuto
				lCADSZ5D := .F.
			EndIf
		EndIf

		// Begin Transaction        FATURAMENTO : RAFAEL
		
		
			If lCADSZ5D	//Confirmou a grava��o
				IncProc("Criando NF de devolu��o para a Matriz")
				ProcessMessage()
				//Faz NF de saida(devolu��o de compras) Comercial para Matriz
				aCliente	:= {SF1->F1_FORNECE,SF1->F1_LOJA}
				aDocSF2	:= {}
				aDocDev	:= {SF1->F1_DOC,SF1->F1_SERIE,SF1->F1_FILIAL,SF1->F1_FORNECE,SF1->F1_LOJA}
				lCADSZ5D := DevPV(SF1->F1_DOC,SF1->F1_SERIE,aCliente,aFornec,@aDocSF2)
				If !lCADSZ5D
					// DisarmTransaction()       FATURAMENTO : RAFAEL
				EndIf
				aCabecNF2:= {}
				aLinhaNF2:= {}

				//Entrada de Devolu��o
				If lCADSZ5D
					IncProc("Criando NF de Entrada da devolu��o do Comercial para a Matriz")
					ProcessMessage()
					lCADSZ5D := EntradaSF2("010105",cFilAnt,aDocSF2[1],aDocSF2[2],aFornec,aClientFil,aDocSF1,,1,aDocDev,.F.)

					IncProc("Criando NF de Entrada da devolu��o Cliente para Matriz")
					ProcessMessage()
					aDocMtz	:= {}
					lCADSZ5D := EntradaDEVF2("010105",aCabecNF2,aLinhaNF2,(cAlias)->Z5_NF03,(cAlias)->Z5_SERIE3,@aDocMtz) //EntradaSF2("010101","010101",aDocSF2[1],aDocSF2[2],aFornec,aClientFil,aDocSF1,,2,aDocDev)

				EndIf
			EndIf
			If !lCADSZ5D
				// DisarmTransaction()    FATURAMENTO : RAFAEL
			Else
				RecLock("SZ8",.T.)
				SZ8->Z8_FILIAL	:= xFilial("SZ8")
				SZ8->Z8_CONTRAT	:= (cAlias)->Z5_CONTRAT
				SZ8->Z8_MEDICAO	:= (cAlias)->Z5_MEDICAO
				SZ8->Z8_DATA		:= dDataBase
				SZ8->Z8_EMP01		:= cFilAnt
				SZ8->Z8_NF01		:= aDocDev[1]				//CLIENTE PARA COMERCIAL
				SZ8->Z8_SER01		:= aDocDev[2]
				SZ8->Z8_CLIENTE	:= (cAlias)->Z5_CLIENTE
				SZ8->Z8_LOJA		:= (cAlias)->Z5_LOJA
				SZ8->Z8_NF02		:= aDocSF2[1]				//COMERCIAL PARA MATRIZ
				SZ8->Z8_SER02		:= aDocSF2[2]
				SZ8->Z8_EMPMTZ	:= "010105"
				SZ8->Z8_NFMTZ		:= aDocMtz[1]				//CLIENTE PARA MATRIZ
				SZ8->Z8_SERMTZ	:= aDocMtz[2]
				SZ8->Z8_ENTRADA	:= "N"
				SZ8->(MsUnLock())
			EndIf
		// End Transaction       FATURAMENTO : RAFAEL
		MsUnLockAll()
		If lCADSZ5D
			FWExecView("Devolu��o", "CADSZ8", MODEL_OPERATION_VIEW ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
		EndIf
		(cAlias)->(DbSkip())
	EndDo
	(cAlias)->(DbCloseArea())
	TirarMarca()
Return

Static Function QueryMarca(cAlias)
	Local cMarca	:= oMark:Mark()
	BeginSql Alias cAlias
		SELECT * FROM %table:SZ5% SZ5 WHERE Z5_FILIAL=%xFilial:SZ5% AND SZ5.Z5_OK = %Exp:cMarca% and SZ5.D_E_L_E_T_ = ' '
	EndSql
Return

Static Function TirarMarca()
	Local aAreaSZ5	:= SZ5->(GetArea())
	Local cMarca		:= oMark:Mark()
	SZ5->(DbSetOrder(6))	//Z5_FILIAL+Z5_OK
	While SZ5->(DbSeek(xFilial("SZ5")+cMarca))
		oMark:MarkRec()
	EndDO
	SZ5->(RestArea(aAreaSZ5))
	oMark:Refresh(.T.)	//Atualiza e posiciona no primeiro registro
Return


/*/{Protheus.doc} DevPV
Realiza a devolu��o para a Matriz conforme entrada de NF do cliente
@author Saulo Gomes Martins
@since 25/09/2014
@version 1.0
@param cNFDev, character, NF de entrada do cliente
@param cSerieDev, character, Serie de entrada do cliente
@param aFornec, array, Array com o codigo do fornecedor Matriz
/*/
Static Function DevPV(cNFDev,cSerieDev,aCliente,aFornec,aDocFat)
	Local aCab		:= {}
	Local aItens	:= {}
	Local nCont	:= 0
	Local nTam
	Local cChaveSD1
	Local aAreaSD1
	Local cMsgErro:= ""
	Default aDocFat	:= {}
	Private lMsErroAuto	:= .F.
	SD1->(DbSetOrder(1))//D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
	SZ5->(DbSetOrder(7))//Z5_EMPPV1+Z5_NF01+Z5_SERIE1
	SF4->(DbSetOrder(1))//F4_FILIAL+F4_CODIGO

	SD1->(DbSeek(xFilial("SD1")+cNFDev+cSerieDev+aCliente[1]+aCliente[2]))
	AADD(aCab,{"C5_TIPO"			,"D"						,Nil})
	AADD(aCab,{"C5_CLIENTE"		,aFornec[1]				,Nil})
	AADD(aCab,{"C5_LOJA"			,aFornec[2]				,Nil})
	AADD(aCab,{"C5_TPEDAV"			,'23'				,Nil})
	AADD(aCab,{"C5_CONDPAG"		,"001"						,Nil})

	While SD1->(!EOF()) .AND. SD1->(D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA)==xFilial("SD1")+cNFDev+cSerieDev+aCliente[1]+aCliente[2]
		nCont++
		AADD(aItens,{})
		aAreaSD1	:= SD1->(GetArea())
		nQtdD1		:= SD1->D1_QUANT
		nVlrD1		:= SD1->D1_VUNIT
		nTam		:= Len(aItens)
		SZ5->(DbSeek(SD1->(D1_FILIAL+D1_NFORI+D1_SERIORI)))		//Amarra��o das NFs
		cChaveSD1	:= xFilial("SD1")+SZ5->Z5_NF04+SZ5->Z5_SERIE4+aFornec[1]+aFornec[2]+SD1->(D1_COD+StrZero(Val(D1_ITEMORI),GetSx3Cache("D1_ITEM","X3_TAMANHO")))
		If SD1->(DbSeek(cChaveSD1))									//D1 DE ENTRADA DA NF DA MATRIZ
			SF4->(DbSeek(xFilial("SF4")+SD1->D1_TES))					//TES DE DEVOLU��O DO ITEM
			AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nCont,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
			AADD(aItens[nTam],{"C6_PRODUTO"	,SD1->D1_COD					,nil})
			AADD(aItens[nTam],{"C6_QTDVEN"	,nQtdD1				,nil})
			AADD(aItens[nTam],{"C6_QTDLIB"	,nQtdD1				,nil})
			AADD(aItens[nTam],{"C6_PRCVEN"	,nVlrD1				,nil})
			AADD(aItens[nTam],{"C6_TES"		,SF4->F4_TESDV				,nil})
			AADD(aItens[nTam],{"C6_NFORI"	,SD1->D1_DOC					,nil})
			AADD(aItens[nTam],{"C6_SERIORI"	,SD1->D1_SERIE				,nil})
			AADD(aItens[nTam],{"C6_ITEMORI"	,SD1->D1_ITEM					,nil})
		Else
			AutoGrLog("N�o foi encontrado a NF de entrada!")
			AutoGrLog("Devolu��o para matriz n�o realizada!")
			AutoGrLog("Chave SD1:"+cChaveSD1)
			MostraErro()
			Return .F.
		EndIf
		RestArea(aAreaSD1)
		SD1->(DbSkip())
	EndDo
	MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCab,aItens,3)
	If lMsErroAuto
		AutoGrLog("Erro ao gerar pedido de devolu��o!")
		MostraErro()
		Return .F.
	Endif

	If !FGeraFat(cFilAnt,SC5->C5_NUM,@aDocFat,@cMsgErro,"")
		AutoGrLog("Erro ao Faturar NF de devolu��o!")
		AutoGrLog(cMsgErro)
		MostraErro()
		Return .F.
	EndIf

Return .T.

/*/{Protheus.doc} EntradaSF2
Cria NF de entrada na de acordo com NF de Saida da Filial
@author Saulo
@since 25/09/2014
@version 1.0
/*/
Static Function EntradaSF2(cFilSF1,cFilSF2,cNFiscal,cSerie,aFornec,aClientFil,aDocSF1,cMsgErro,cSeq,aDocDev,lGrava)
	Local aCabec := {}
	Local aItens := {}
	Local aLinha := {}
	Local cFilBkp:= cFilAnt
	Local nCont	:= 0
	Local aAreaSD2
	Local cChaveSD2
	Default cMsgErro	:= ""
	Default lGrava	:= .F.
	Private lMsErroAuto := .F.
	cFilAnt	:= cFilSF1
	aCabecNF2:= {}
	aLinhaNF2:= {}

	SF2->(dbSetOrder(1))	//F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO
	SD2->(dbSetOrder(3))	//D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM
	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO

	If cSeq == 2 //Cliente para Matriz
		aFornec[1]	:= Posicione("SF2",1,xFilial("SF2",cFilSF2)+cNFiscal+cSerie,"F2_CLIENTE")
		aFornec[2]	:= Posicione("SF2",1,xFilial("SF2",cFilSF2)+cNFiscal+cSerie,"F2_LOJA")
	Endif

	If !SF2->(dbSeek(xFilial("SF2",cFilSF2)+cNFiscal+cSerie+aFornec[1]+aFornec[2]))
		AutoGrLog("N�o foi encontrado a NF de saida!")
		AutoGrLog("Entrada da devolu��o n�o realizada!")
		AutoGrLog("Chave SF2:"+xFilial("SF2",cFilSF2)+cNFiscal+cSerie+aFornec[1]+aFornec[2])
		MostraErro()
		Return .F.
	EndIf
	SD2->(dbSeek(xFilial("SD2",cFilSF2)+cNFiscal+cSerie+aFornec[1]+aFornec[2]))

	If cSeq == 2 //Cliente para Matriz
		aClientFil[1]	:= SF2->F2_CLIENTE
		aClientFil[2]	:= SF2->F2_LOJA
	Endif


	aadd(aCabec,{"F1_TIPO"   ,"D"})
	aadd(aCabec,{"F1_FORMUL" ,"N"})
	aadd(aCabec,{"F1_DOC"    ,SF2->F2_DOC})
	aadd(aCabec,{"F1_SERIE"  ,SF2->F2_SERIE})
	aadd(aCabec,{"F1_EMISSAO",dDataBase})
	aadd(aCabec,{"F1_FORNECE",aClientFil[1]})
	aadd(aCabec,{"F1_LOJA"   ,aClientFil[2]})
	aadd(aCabec,{"F1_ESPECIE","SPED"})

	aCabecNF2:= aClone(aCabec)
	aCons:= {}

	While SD2->(!EOF()) .and. SD2->(D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA) == xFilial("SD2",cFilSF2)+cNFiscal+cSerie+aFornec[1]+aFornec[2]
		aLinha := {}
		nCont	++
		aAreaSD2	:= SD2->(GetArea())

		nQtdD2		:= SD2->D2_QUANT
		nPrcVen		:= SD2->D2_PRCVEN
		nTotGer		:= SD2->D2_TOTAL

		If cSeq == 1 //Comercial para a Matriz
			cChaveSD2	:= xFilial("SD2")+SD2->(D2_NFORI+D2_SERIORI+aClientFil[1]+aClientFil[2]+D2_COD+StrZero(Val(D2_ITEMORI),GetSx3Cache("D2_ITEM","X3_TAMANHO")))
		else //Cliente para Matriz

			If SD1->(dbSeek(aDocDev[3]+aDocDev[1]+aDocDev[2]+aDocDev[4]+aDocDev[5]+SD2->(D2_COD))) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
				//+RetAsc(cvaltochar(nVlItem),GetSx3Cache("C7_ITEM","X3_TAMANHO"))

				aItDup:= FBuscItDup(aDocDev[3]+aDocDev[1]+aDocDev[2]+aDocDev[4]+aDocDev[5]+SD2->(D2_COD))

				If len(aItDup) > 1
					For i:=1 To len(aItDup)
						If Ascan(aCons,aItDup[i]) = 0
							Aadd(aCons,aItDup[i]) //Itens considerados
							SD1->(dbSeek(aDocDev[3]+aDocDev[1]+aDocDev[2]+aDocDev[4]+aDocDev[5]+SD2->(D2_COD)+RetAsc(aItDup[i],GetSx3Cache("C7_ITEM","X3_TAMANHO"))))
							i:= len(aItDup)+1
						Endif
					Next
				Else

					If Ascan(aCons,SD1->D1_ITEM) > 0
						dbSelectArea("SD2")
						nCont-=1
						SD2->(DbSkip())
						Loop

					Else
						Aadd(aCons,SD1->D1_ITEM) //Itens considerados
					Endif
				endif

				nQtdD2		:= SD1->D1_QUANT
				nPrcVen		:= SD1->D1_VUNIT
				nTotGer		:= SD1->D1_TOTAL

				if nQtdD2 = 0 .or. nPrcVen = 0
					dbSelectArea("SD2")
					nCont-=1
					SD2->(DbSkip())
					Loop

				Endif

			Else
				dbSelectArea("SD2")
				nCont-=1
				SD2->(DbSkip())
				Loop
			Endif

			cChaveSD2	:= SD2->(D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM)
		endif

		If SD2->(DbSeek(cChaveSD2))									//D2 DE SAIDA DA NF DA MATRIZ
			SF4->(DbSeek(xFilial("SF4")+SD2->D2_TES))					//TES DE DEVOLU��O DO ITEM
			Aadd(aLinha,{"D1_COD"     	,SD2->D2_COD										,Nil})
			Aadd(aLinha,{"D1_ITEM"     	,RetAsc(nCont,GetSx3Cache("C7_ITEM","X3_TAMANHO"),.T.)		,Nil})
			Aadd(aLinha,{"D1_QUANT"		,nQtdD2											,Nil})
			Aadd(aLinha,{"D1_VUNIT"		,nPrcVen										,Nil})
			Aadd(aLinha,{"D1_TOTAL"		,nTotGer										,Nil})
			Aadd(aLinha,{"D1_TES"		,SF4->F4_TESDV									,Nil})
			Aadd(aLinha,{"D1_LOCAL"		,SD2->D2_LOCAL									,Nil})
			Aadd(aLinha,{"D1_NFORI"		,SD2->D2_DOC										,Nil})
			Aadd(aLinha,{"D1_SERIORI"	,SD2->D2_SERIE									,Nil})
			Aadd(aLinha,{"D1_ITEMORI"	,SD2->D2_ITEM										,Nil})


			NNR->(dbSetOrder(1))
			NNR->(DBGOTOP())
			If (!NNR->(DbSeek(xFilial("NNR")+SD2->D2_LOCAL)))
				AGRA045(;
					{{"NNR_CODIGO",SD2->D2_LOCAL,NIL};
					,{"NNR_DESCRI","Armazem "+SD2->D2_LOCAL,NIL};
					};
					,3,{},{})
			EndIf
		Else
			AutoGrLog("N�o foi encontrado a NF de saida!")
			AutoGrLog("Entrada da devolu��o n�o realizada!")
			AutoGrLog("Chave SD2:"+cChaveSD2)
			MostraErro()
			Return .F.
		EndIf

		aadd(aItens,aLinha)
		SD2->(RestArea(aAreaSD2))
		SD2->(DbSkip())
	EndDo

	aLinhaNF2:= aClone(aItens)

	lRet:= .T.
	If lGrava
		MATA103(aCabec,aItens,3)
		If !lMsErroAuto
			lRet	:= .T.
			aDocSF1	:= {SF1->F1_DOC,SF1->F1_SERIE}
		Else
			AutoGrLog("N�o foi poss�vel gerar a Nota fiscal de Entrada!")
			Mostraerro()
			cMsgErro += "N�o foi poss�vel gerar a Nota fiscal de Entrada "+ CHR(10) + CHR(13)
			lRet	:= .F.
		EndIf
	EndIf
	cFilAnt:= cFilBkp
Return lRet


Static Function VerificaTES(cTES)
	Local lContinua	:= .T.
	SF4->(DbSetOrder(1))
	If SF4->(MsSeek(xFilial("SF4")+cTES))
		If Empty(SF4->F4_TESDV) .Or. !(SF4->(MsSeek(xFilial("SF4")+SF4->F4_TESDV)))
			Help(" ",1,"DSNOTESDEV")
			lContinua := .F.
		EndIf
	Else
		lContinua	:= .F.
	EndIf
Return lContinua

User Function CADSZ5E()
	Local cFilBak	:= cFilAnt
	Local aPergs	:= {}
	Local aRet		:= {}
	Local cCampo
	Local aCampC5	:= {}
	Local nCont,i
	Local aCampoAdd	:= StrTokArr(SuperGetMv("SA_OP3ADDC",.F.,""),",")	//Parametro para adicionar novos campos
	If !SoftLock("SZ5")
		Return
	EndIf
	cFilAnt	:= SZ5->Z5_EMPPV1
	aCampC5	:= {}
	DbSelectArea("SC5")
	DbSetOrder(1)
	If !Empty(SZ5->Z5_NF01)
		SC5->(MsUnLock())
		MsgAlert("Pedido j� faturado!")
		cFilAnt	:= cFilBak
		Return
	EndIf
	If !SC5->(DbSeek(xFilial("SC5")+SZ5->Z5_PV01))
		SC5->(MsUnLock())
		MsgAlert("Pedido n�o encontrato!")
		cFilAnt	:= cFilBak
		Return
	Endif
	Aadd(aCampC5,{"C5_TRANSP"	,SC5->C5_TRANSP})
	Aadd(aCampC5,{"C5_VOLUME1"	,SC5->C5_VOLUME1})
	Aadd(aCampC5,{"C5_MENNOTA"	,SC5->C5_MENNOTA})
	Aadd(aCampC5,{"C5_ESPECI1"	,SC5->C5_ESPECI1})
	Aadd(aCampC5,{"C5_FECENT"	,SC5->C5_FECENT})
	Aadd(aCampC5,{"C5_SUGENT"	,SC5->C5_SUGENT})
	Aadd(aCampC5,{"C5_PESOL"		,SC5->C5_PESOL})
	Aadd(aCampC5,{"C5_PBRUTO"	,SC5->C5_PBRUTO})
	Aadd(aCampC5,{"C5_TPFRETE"	,SC5->C5_TPFRETE})
	Aadd(aCampC5,{"C5_YCONF"		,SC5->C5_YCONF})
	Aadd(aCampC5,{"C5_YSEPAR"	,SC5->C5_YSEPAR})
	Aadd(aCampC5,{"C5_YENDENT"	,SC5->C5_YENDENT})
	//Aadd(aCampC5,{"C5_TPEDAV"	, '23'}) //TIPO PEDIDO
	For nCont:=1 to Len(aCampoAdd)
		Aadd(aCampC5,{aCampoAdd[nCont]	,SC5->(&aCampoAdd[nCont])})
	Next
	For nCont:=1 to Len(aCampC5)
		If !Empty(GetSx3Cache(aCampC5[nCont][1],"X3_CBOX"))
			aCBox	:= RetSx3Box(GetSx3Cache(aCampC5[nCont][1],"X3_CBOX"),,,1,)
			aCombo	:= {}
			For i:=1 to Len(aCBox)
				AADD(aCombo,aCBox[i][3])
			Next
			aAdd( aPergs ,{2,GetSx3Cache(aCampC5[nCont][1],"X3_TITULO")	,aCBox[aScan(aCBox,{|x| ALLTRIM(x[2])==ALLTRIM(aCampC5[nCont][2])})][3],aCombo,60,".T.",.F.})
			AADD(aRet,aCBox[aScan(aCBox,{|x| ALLTRIM(x[2])==ALLTRIM(aCampC5[nCont][2])})][3])
		Else
			aAdd( aPergs ,{1,GetSx3Cache(aCampC5[nCont][1],"X3_TITULO")	,aCampC5[nCont][2],GetSx3Cache(aCampC5[nCont][1],"X3_PICTURE"),"M->"+aCampC5[nCont][1]+":=MV_PAR"+STRZERO(nCont,2,0)+",__ReadVar:='MV_PAR"+STRZERO(nCont,2,0)+"',"+GetSx3Cache(aCampC5[nCont][1],"X3_VALID"),GetSx3Cache(aCampC5[nCont][1],"X3_F3"),".T.",60,.F.})
			AADD(aRet,aCampC5[nCont][2])
		EndIf
	Next
	If !ParamBox(aPergs,"Alterar Pedido: "+SZ5->Z5_PV01+" Contrato: "+SZ5->Z5_CONTRAT,@aRet,,,,,,,,.F.,.F.)
		Return
	EndIf

	//Replica campos para os pedidos do mesmo Contrato
	If SC5->(DbSeek(xFilial("SC5")+SZ5->Z5_PV01))
		RecLock("SC5",.F.)
		For i:=1 To len(aCampC5)
			If !Empty(GetSx3Cache(aCampC5[i][1],"X3_CBOX"))
				aCBox	:= RetSx3Box(GetSx3Cache(aCampC5[i][1],"X3_CBOX"),,,1,)
				SC5->(&(aCampC5[i][1])) := aCBox[aScan(aCBox,{|x| ALLTRIM(x[3])==ALLTRIM(aRet[i])})][2]
			Else
				SC5->(&(aCampC5[i][1])) := aRet[i]
			EndIf
		Next
		If SC5->C5_YENDENT $ "S_Sim"
			SA1->(dbSetOrder(1))
			SA1->(dbSeek(xFilial("SA1")+SC5->C5_CLIENTE+SC5->C5_LOJACLI))
			SC5->C5_MENNOTA:= SA1->A1_ENDENT
		Endif	

		MsUnlock()
	Endif

	If SC5->(DbSeek(SZ5->Z5_EMPPV2+SZ5->Z5_PV02))
		RecLock("SC5",.F.)
		For i:=1 To len(aCampC5)
			If !Empty(GetSx3Cache(aCampC5[i][1],"X3_CBOX"))
				aCBox	:= RetSx3Box(GetSx3Cache(aCampC5[i][1],"X3_CBOX"),,,1,)
				SC5->(&(aCampC5[i][1])) := aCBox[aScan(aCBox,{|x| ALLTRIM(x[3])==ALLTRIM(aRet[i])})][2]
			Else
				SC5->(&(aCampC5[i][1])) := aRet[i]
			EndIf
		Next
		If SC5->C5_YENDENT $ "S_Sim"
			SA1->(dbSetOrder(1))
			SA1->(dbSeek(xFilial("SA1")+SC5->C5_CLIENTE+SC5->C5_LOJACLI))
			SC5->C5_MENNOTA:= SA1->A1_ENDENT
		Endif	
		MsUnlock()
		
	Endif

	If SC5->(DbSeek(SZ5->Z5_EMPPV3+SZ5->Z5_PV03))
		RecLock("SC5",.F.)
		For i:=1 To len(aCampC5)
			If !Empty(GetSx3Cache(aCampC5[i][1],"X3_CBOX"))
				aCBox	:= RetSx3Box(GetSx3Cache(aCampC5[i][1],"X3_CBOX"),,,1,)
				SC5->(&(aCampC5[i][1])) := aCBox[aScan(aCBox,{|x| ALLTRIM(x[3])==ALLTRIM(aRet[i])})][2]
			Else
				SC5->(&(aCampC5[i][1])) := aRet[i]
			EndIf
		Next
		If SC5->C5_YENDENT $ "S_Sim"
			SA1->(dbSetOrder(1))
			SA1->(dbSeek(xFilial("SA1")+SC5->C5_CLIENTE+SC5->C5_LOJACLI))
			SC5->C5_MENNOTA:= SA1->A1_ENDENT
		Endif	

		MsUnlock()
	Endif
	SC5->(MsUnLock())
	MsgInfo("Pedido Alterado com sucesso!")
	cFilAnt	:= cFilBak
Return

User Function CADSZ5I
	If !SC5->(DbSeek(xFilial("SC5")+SZ5->Z5_PV01))
		SC5->(MsUnLock())
		MsgAlert("Pedido n�o encontrato!")
		cFilAnt	:= cFilBak
		Return
	Endif
	MATA410(,,2,,"A410Visual")
Return

User Function CADSZ5F()
	U_CADSZ8("Z8_CONTRAT='"+SZ5->Z5_CONTRAT+"' .AND. Z8_MEDICAO='"+SZ5->Z5_MEDICAO+"' .AND. Z8_CLIENTE='"+SZ5->Z5_CLIENTE+"' .AND. Z8_LOJA='"+SZ5->Z5_LOJA+"' ")
Return

Static Function FBuscItDup(cChav)
	Local aRet:= {}

	cSql := "SELECT D1_ITEM,COUNT(*) FROM " + RetSqlName("SD1")
	cSql += " WHERE D_E_L_E_T_ =' ' AND "
	cSql += " D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD = '"+ cChav +"'"
	cSql += " GROUP BY D1_ITEM "

	TCQUERY cSql NEW ALIAS "YDUPD2"

	while YDUPD2->(!eof())
		Aadd(aRet,YDUPD2->D1_ITEM)
		YDUPD2->(dbSkip())
	enddo

	YDUPD2->(dbCloseArea())

Return aRet

Static Function EntradaDEVF2(cFilSF1,aCabecNF2,aLinhaNF2,cNFiscal,cSerie,aDocFiscal,cMsgErro)
	Local aCabec := {}
	Local aItens := {}
	Local aLinha := {}
	Local cFilBkp:= cFilAnt
	Local cChaveSD2,cFilSx5
	Default cMsgErro	:= ""
	Private lMsErroAuto := .F.
	cFilAnt	:= cFilSF1
	aFornec	:= {"",""}

	SF2->(dbSetOrder(1))	//F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO
	SD2->(dbSetOrder(3))	//D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM
	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO

	aFornec[1]	:= Posicione("SF2",1,xFilial("SF2",cFilSF1)+cNFiscal+cSerie,"F2_CLIENTE")
	aFornec[2]	:= Posicione("SF2",1,xFilial("SF2",cFilSF1)+cNFiscal+cSerie,"F2_LOJA")

	If !SF2->(dbSeek(xFilial("SF2",cFilSF1)+cNFiscal+cSerie+aFornec[1]+aFornec[2]))
		AutoGrLog("N�o foi encontrado a NF de saida!")
		AutoGrLog("Entrada da devolu��o n�o realizada!")
		AutoGrLog("Chave SF2:"+xFilial("SF2",cFilSF1)+cNFiscal+cSerie+aFornec[1]+aFornec[2])
		MostraErro()
		Return .F.
	EndIf

	cFilSx5		:= xFilial("SX5")
	If ExistBlock("CHGX5FIL")
		cFilSx5 := ExecBlock("CHGX5FIL",.f.,.f.)
	EndIf
	For i:= 1 To Len(aCabecNF2)
		if Empty(aCabecNF2[i])
			Loop
		ElseIf aCabecNF2[i][1]="F1_DOC"
			aCabecNF2[i][2]:= ""//SF2->F2_DOC
			aCabecNF2[i][2]:=	NxtSX5Nota("1  ",.T.,SuperGetMV("MV_TPNRNFS"))
			/*If SX5->(DbSeek(cFilSX5+"01"+PADR("1",LEN(SX5->X5_CHAVE)),.F.))
			aCabecNF2[i][2]:= SX5->X5_DESCRI
		EndIf*/
	elseif aCabecNF2[i][1]="F1_SERIE"
		aCabecNF2[i][2]:= "1  "//SF2->F2_SERIE
	elseif aCabecNF2[i][1]="F1_FORMUL"
		aCabecNF2[i][2]:= "S"	//DEVOLU��O DO CLIENTE PARA MATRIZ, SEMPRE COM FORMULARIO PROPRIO
	elseif aCabecNF2[i][1]="F1_FORNECE"
		aCabecNF2[i][2]:= SF2->F2_CLIENTE
	elseif aCabecNF2[i][1]="F1_LOJA"
		aCabecNF2[i][2]:= SF2->F2_LOJA
	endif
Next

For i:= 1 To len(aLinhaNF2)

	nPrcVen		:= 0
	nPosCod		:= 1  //aScan(aLinhaNF2[i][1],"D1_COD")
	nPosItem	:= len(aLinhaNF2[i]) //aScan(aLinhaNF2[i][1],"D1_ITEMORI")
	cChaveSD2	:= SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA)+aLinhaNF2[i][nPosCod][2]+aLinhaNF2[i][nPosItem][2]
	SD2->(dbSetOrder(3))
	if SD2->(dbSeek(cChaveSD2))
		nPrcVen := SD2->D2_PRUNIT
		cTES	:= SD2->D2_TES
		cFilD2	:= SD2->D2_FILIAL
	endif

	For nX:= 1 To len(aLinhaNF2[i])

		if aLinhaNF2[i][nX][1]="D1_NFORI"
			aLinhaNF2[i][nX][2]:= SF2->F2_DOC
		elseif aLinhaNF2[i][nX][1]="D1_SERIORI"
			aLinhaNF2[i][nX][2]:= SF2->F2_SERIE
		elseif aLinhaNF2[i][nX][1]="D1_VUNIT" .AND. nPrcVen > 0
			aLinhaNF2[i][nX][2]:= nPrcVen
		elseif aLinhaNF2[i][nX][1]="D1_TOTAL" .AND. nPrcVen > 0
			aLinhaNF2[i][nX][2]:= nQtdVen*nPrcVen
		elseif aLinhaNF2[i][nX][1]="D1_QUANT"
			nQtdVen:= aLinhaNF2[i][nX][2]
		elseif aLinhaNF2[i][nX][1]="D1_TES" //Verificar
			aLinhaNF2[i][nX][2]:= Posicione("SF4",1,xFilial("SF4",cFilD2)+cTES,"F4_TESDV")
		endif

		If aLinhaNF2[i][nX][1]="D1_LOCAL"

			NNR->(dbSetOrder(1))
			NNR->(DBGOTOP())

			If (!NNR->(DbSeek(xFilial("NNR")+aLinhaNF2[i][nX][2])))
				AGRA045(;
					{{"NNR_CODIGO",aLinhaNF2[i][nX][2],NIL};
					,{"NNR_DESCRI","Armazem "+aLinhaNF2[i][nX][2],NIL};
					};
					,3,{},{})
			endif
		endif
	Next

Next

MATA103(aCabecNF2,aLinhaNF2,3)
If !lMsErroAuto
	lRet	:= .T.
	aDocFiscal	:= {SF1->F1_DOC,SF1->F1_SERIE}
Else
	AutoGrLog("N�o foi poss�vel gerar a Nota fiscal de Entrada!")
	Mostraerro()
	cMsgErro += "N�o foi poss�vel gerar a Nota fiscal de Entrada "+ CHR(10) + CHR(13)
	lRet	:= .F.
EndIf

cFilAnt:= cFilBkp
Return lRet

User Function CADSZ5G
	If SZ5->Z5_LIBERAC $ "L|Z"
		MsgAlert("J� liberado!")
	ElseIf MsgYesno("Confirmar libera��o manual?")
		RecLock("SZ5",.F.)
		SZ5->Z5_LIBERAC	:= "L"
		SZ5->(MsUnLock())
		MsgInfo("Liberado com sucesso!")
	EndIf
	TirarMarca()
Return

User Function CADSZ5H
	Local aArea	:= GetArea()
	Local aPergs	:= {}
	Local aRet		:= {}
	Local dData	:= CTOD("")
	If !SoftLock("SZ5")
		Return
	EndIf
	SF2->(DbSetOrder(1))	//F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO
	SC5->(DbSetOrder(1))	//C5_FILIAL+C5_NUM
	If SC5->(DbSeek(xFilial("SC5",SZ5->Z5_EMPPV1)+SZ5->Z5_PV01))
		dData	:= SC5->C5_FECENT
	Else
		Alert("Pedido n�o encontrado!")
	EndIf
	cCampo	:= "C5_FECENT"
	aAdd( aPergs ,{1,GetSx3Cache(cCampo,"X3_TITULO")	,SC5->(&cCampo),GetSx3Cache(cCampo,"X3_PICTURE"),".T.","",'.T.',60,.F.})
	cCampo	:= "Z5_NOMRECE"
	aAdd( aPergs ,{1,GetSx3Cache(cCampo,"X3_TITULO"),SZ5->(&cCampo),GetSx3Cache(cCampo,"X3_PICTURE"),".T.","",'.T.',60,.F.})
	cCampo	:= "Z5_CTRC"
	aAdd( aPergs ,{1,GetSx3Cache(cCampo,"X3_TITULO"),SZ5->(&cCampo),GetSx3Cache(cCampo,"X3_PICTURE"),".T.","",'.T.',60,.F.})
	If ParamBox(aPergs,"Alterar dados entrega"+SZ5->Z5_PV01,aRet,,,,,,,)
		RecLock("SZ5",.F.)
		SZ5->Z5_NOMRECE	:= aRet[2]
		SZ5->Z5_CTRC		:= aRet[3]
		SZ5->(MsUnLock())
		RecLock("SC5",.F.)
		SC5->C5_FECENT	:= aRet[1]
		SC5->C5_YNOMREC	:= aRet[2]
		SC5->C5_YCTRC	:= aRet[3]
		SC5->(MsUnLock())
		If SF2->(DbSeek(xFilial("SF2",SZ5->Z5_EMPPV1)+SZ5->(Z5_NF01+Z5_SERIE1+Z5_CLIENTE+Z5_LOJA)))
			RecLock("SF2",.F.)
			SF2->F2_DTENTR	:= aRet[1]
			SC5->(MsUnLock())
		EndIf
		MsgInfo("Altera��o completa!")
	EndIf
	DbUnLockAll()
	RestArea(aArea)
Return

/*/{Protheus.doc} GravDest
Grava campos F2_FILDEST,F2_FORDES,F2_LOJADES para gravar a CHVNFE no Danfe de entrada
@author Saulo Gomes Martins
@since 28/11/2014
@version 1.0
/*/
Static Function GravDest(cAlias,cDocEnt,cSerEnt)
	If SF2->(FieldPos("F2_FILDEST"))>0 .And. SF2->(FieldPos("F2_FORDES"))>0 .And. SF2->(FieldPos("F2_LOJADES"))>0
		SF2->(dbSetOrder(1))	//F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO
		SC5->(dbSetOrder(1))	//C5_FILIAL+C5_NUM
		SC7->(dbSetOrder(1))	//C7_FILIAL+C7_NUM+C7_ITEM+C7_SEQUEN
		If SC5->(DbSeek(xFilial("SC5",(cAlias)->Z5_EMPPV2)+(cAlias)->Z5_PV02))
			If SF2->(dbSeek(xFilial("SF2",(cAlias)->Z5_EMPPV2)+PadR(cDocEnt,TamSX3("D2_DOC")[1])+cSerEnt+SC5->(C5_CLIENTE+C5_LOJACLI)))
				If SC7->(DbSeek(xFilial("SC7",(cAlias)->Z5_EMPPC4)+(cAlias)->Z5_PC04))
					RecLock("SF2",.F.)
					SF2->F2_FILDEST:= (cAlias)->Z5_EMPPC4
					SF2->F2_FORDES := SC7->C7_FORNECE
					SF2->F2_LOJADES:= SC7->C7_LOJA
					SF2->F2_FORMDES:= "N"
					MsUnlock()
				EndIf
			EndIf
		EndIf
	EndIf
Return