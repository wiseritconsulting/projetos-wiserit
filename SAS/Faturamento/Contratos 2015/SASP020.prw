#include "protheus.ch"
#include "topconn.ch"

User Function SASP020

	Local nQtd := 0
	Local nQTot := 0
	Local cContrat := space(GetSx3Cache("Z1_CONTRAT","X3_TAMANHO"))
	Local cMedicao := space(GetSx3Cache("Z3_MEDICAO","X3_TAMANHO"))	
	Local cLocExp := space(GetSx3Cache("C5_YLCEXPD","X3_TAMANHO"))	
	Local aPergs := {}
	Local aRet := {}
	
	aAdd( aPergs ,{1,'Contrato'		,cContrat	,'@!',"","QSZ3"    ,'.T.',60,.T.})
	aAdd( aPergs ,{1,'Medicao'	    ,cMedicao	,'@!',""	   ,"" ,'.T.',60,.T.})
	//aAdd( aPergs ,{1,'Local Exp.'	,cLocExp	,'@!',""	   ,"X2" ,'.T.',60,.T.})
	
	dbSelectArea("SZ1")
	dbSelectArea("SZ2")
	
	CloseQry()
	
	IF ParamBox(aPergs,"Medicao",aRet,,,,,,,"MEDNAT",.F.,.F.)
		cContrat   := aRet[1]
		cMedicao   := aRet[2]
		//cLocExp    := aRet[3] 
	else
		MsgInfo('Abortado pelo usu�rio!!!','Encerrado.')
		Return
	EndIF
		
	cSQLZ5 := "SELECT COUNT(*) AS EXIST FROM SZ5010 WHERE Z5_FILIAL='"+xFilial("SZ5")+"' AND Z5_MEDICAO='"+cMedicao+"' AND Z5_CONTRAT='"+cContrat+"' AND D_E_L_E_T_=''"
	TCQUERY cSQLZ5 NEW ALIAS "QSZ5"
	
	if (QSZ5->EXIST > 0)
		MsgInfo('A medi��o informada('+cContrat+'/'+cMedicao+') existe na SZ5.','Aten��o:' + StrZero(QSZ5->EXIST,4))
		Return
	else
	
		cSQLZ3 := "SELECT * FROM SZ3010 WHERE Z3_FILIAL='"+xFilial("SZ3")+"' AND Z3_MEDICAO='"+cMedicao+"' AND Z3_CONTRAT='"+cContrat+"' AND D_E_L_E_T_=''"
		TCQUERY cSQLZ3 NEW ALIAS "QSZ3"
		
		QSZ3->(dbGoTop())
		
		While !QSZ3->(Eof())
		
			cItem := QSZ3->Z3_ITEM
			cProd := QSZ3->Z3_PRODUTO
			cCtr  := QSZ3->Z3_CONTRAT
			cMedi := QSZ3->Z3_MEDICAO
			nQtd  := QSZ3->Z3_QTDMED
			nQTot := nQTot + nQtd
			nRecno := QSZ3->R_E_C_N_O_
			
			//Z2_FILIAL+Z2_CONTRAT+Z2_ITEM
			if SZ2->(dbSeek(xFilial("SZ2")+cContrat+cItem))
				RecLock("SZ2",.F.)
					SZ2->Z2_QTDMED	-= nQtd
					SZ2->Z2_SALDO   += nQtd
				SZ2->(MsUnLock())
			else
				MsgInfo('O registro ('+cContrat+'/'+cItem+') n�o foi encontrato.'+chr(10)+chr(13)+;
					   ' Favor enviar print dessa mensagem via Chamado para TI.' , 'Item')
			endif
			
			QSZ3->(dbSkip())
			
			SZ3->(dbGoTo(nRecno))
			
				RecLock("SZ3",.f.)
					SZ3->(dbDelete())
				SZ3->(MsUnLock())
			
		End Do
			
			//Z1_FILIAL+Z1_CONTRAT
			if SZ1->(dbSeek(xFilial("SZ1")+cContrat))
				RecLock("SZ1",.F.)
					SZ1->Z1_SALDO	 += nQTot
					SZ1->Z1_QTDMED   -= nQTot
				SZ1->(MsUnLock())
			else
				MsgInfo('O registro ('+cContrat+') n�o foi encontrato.'+chr(10)+chr(13)+;
					   ' Favor enviar print dessa mensagem via Chamado para TI.' , 'Contrato')
	
			endif 
			
			MsgInfo('Processamento Finalizado...','Concluido')				
	endif
	
Return



Static Function CloseQry()

	IF Select("QSZ3")>0
		QSZ3->(dbCloseArea())
	Endif
	
	IF Select("QSZ5")>0
		QSZ5->(dbCloseArea())
	Endif

Return 