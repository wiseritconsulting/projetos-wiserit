#INCLUDE 'TOTVS.CH'
#INCLUDE 'RWMAKE.CH'
#Include "Topconn.ch"
#Include 'Protheus.ch'

User Function Teste09

 
 	Local aCoors		:= FwGetDialogSize( oMainWnd )
	Private nAt			:= At("-","")
	Private cLojFil		:= SubStr("",1,nAt-1)
	Private oDlgEst
	Private dVencIni	:= cTod("")
	Private dVencFim	:= cTod("")
	Private oGet1,oGet2,oGet3
	Private oPanel1
	Private oSay1,oSay2,oSay3,oSay4,oSay5
	Private aDadEst		:= {}
	Private nValTotG	:= 0 //Total da Grid
		
	oFont1 :=  TFont():New("Verdana" ,10,-10,,.F.,,,,,.F.)   //** Titulo das Grades
	DEFINE MSDIALOG oDlgEst TITLE "Estoque do Produto" FROM 000,000  TO 350,650 COLORS 0, 16777215 PIXEL
	oFWLayer := FWLayer():New()
	oFWLayer:Init( oDlgEst, .F., .T. )

	/////
	
	oFWLayer:addLine( '1', 20, .F. )  
	oFWLayer:addLine( '2', 70, .F. )

	oPanel1 := oFWLayer:getLinePanel( '1' )
	oPanel2 := oFWLayer:getLinePanel( '2' )

	//oFWLayer:setColSplit('1',CONTROL_ALIGN_RIGHT,,{|| NIL })
	
	//////

	@ 005,001 SAY 	oSay1 PROMPT	"In�cio"	SIZE 040,009 OF oPanel1 COLORS 0,16777215 PIXEL
	@ 005,022 MSGET	oGet1 VAR		dVencIni	SIZE 078,013  OF oPanel1 COLORS 0, 16777215 PIXEL

	@ 005,130 SAY 	oSay2 PROMPT	"Final"		SIZE 040,009 OF oPanel1 COLORS 0,16777215 PIXEL
	@ 005,151 MSGET	oGet2 VAR		dVencFim	SIZE 078,013 Valid Processa({||grvGridEst()},"Aguarde...","Processando...",.F.) OF oPanel1 COLORS 0, 16777215 PIXEL
	

    Private colors := {CLR_HRED, -1}

	/*
	oGet1:oFont:= oFont1
	oSay1:oFont:= oFont1
	oGet2:oFont:= oFont1
	oSay2:oFont:= oFont1
	*/

	@ 140, 001 BUTTON oButSair PROMPT "Sair" Action (oDlgEst:End()) SIZE 060, 013 OF oPanel1 PIXEL

	oBrwEst := FWBrowse():New(oPanel2)
	oBrwEst:SetDataArray()
	oBrwEst:SetArray(@aDadEst)
	oBrwEst:SetDescription('Estoque')
	oBrwEst:SetOwner( oPanel2 )
	oBrwEst:SetProfileID("4")
	oBrwEst:SetDoubleClick({|| nil })
	oBrwEst:SetFontBrowse(oFont1)
	oBrwEst:SetLineHeight(20)
	oBrwEst:DisableReport()
	oBrwEst:DisableConfig()

	oBrwEst:SetColumns({{;
				"Filial",;  					   				// T�tulo da coluna
				{|| If(Empty(aDadEst),"",ALLTRIM(aDadEst[oBrwEst:At(),1])) },;		// Code-Block de carga dos dados
				"C",;                       			// Tipo de dados
				"@!",;                       			// M�scara
				1,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				30,;                       			// Tamanho
				GetSx3Cache("E2_FILIAL","X3_DECIMAL"),;                       			// Decimal
				.F.,;                                       			// Indica se permite a edi��o
				{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
				NIL,;                       			// Indica se exibe imagem
				nil,;                     			// Code-Block de execu��o do duplo clique
				NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
				{||.T.},;                                   			// Code-Block de execu��o do clique no header
				.F.,;                                       			// Indica se a coluna est� deletada
				.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
				}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			)
	oBrwEst:SetColumns({{;
				"Estoque",;  					   				// T�tulo da coluna
				{|| If(Empty(aDadEst),0,aDadEst[oBrwEst:At(),2]) },;		// Code-Block de carga dos dados
				"N",;                       			// Tipo de dados
				GetSx3Cache("B2_QATU","X3_PICTURE"),;                       			// M�scara
				0,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				GetSx3Cache("B2_QATU","X3_TAMANHO"),;                       			// Tamanho
				GetSx3Cache("B2_QATU","X3_DECIMAL"),;                       			// Decimal
				.F.,;                                       			// Indica se permite a edi��o
				{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
				NIL,;                       			// Indica se exibe imagem
				nil,;                     			// Code-Block de execu��o do duplo clique
				NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
				{||.T.},;                                   			// Code-Block de execu��o do clique no header
				.F.,;                                       			// Indica se a coluna est� deletada
				.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
				}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			)

	oBrwEst:SetColumns({{;
				"Estoque",;  					   				// T�tulo da coluna
				{|| If(Empty(aDadEst),0,aDadEst[oBrwEst:At(),2]) },;		// Code-Block de carga dos dados
				"N",;                       			// Tipo de dados
				GetSx3Cache("B2_QATU","X3_PICTURE"),;                       			// M�scara
				0,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				GetSx3Cache("B2_QATU","X3_TAMANHO"),;                       			// Tamanho
				GetSx3Cache("B2_QATU","X3_DECIMAL"),;                       			// Decimal
				.F.,;                                       			// Indica se permite a edi��o
				{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
				NIL,;                       			// Indica se exibe imagem
				nil,;                     			// Code-Block de execu��o do duplo clique
				NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
				{||.T.},;                                   			// Code-Block de execu��o do clique no header
				.F.,;                                       			// Indica se a coluna est� deletada
				.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
				}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			)
			
	oBrwEst:SetColumns({{;
				"Compra",;  					   				// T�tulo da coluna
				{|| If(Empty(aDadEst),0,aDadEst[oBrwEst:At(),2]) },;		// Code-Block de carga dos dados
				"N",;                       			// Tipo de dados
				GetSx3Cache("B2_QATU","X3_PICTURE"),;                       			// M�scara
				0,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				GetSx3Cache("B2_QATU","X3_TAMANHO"),;                       			// Tamanho
				GetSx3Cache("B2_QATU","X3_DECIMAL"),;                       			// Decimal
				.F.,;                                       			// Indica se permite a edi��o
				{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
				NIL,;                       			// Indica se exibe imagem
				nil,;                     			// Code-Block de execu��o do duplo clique
				NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
				{||.T.},;                                   			// Code-Block de execu��o do clique no header
				.F.,;                                       			// Indica se a coluna est� deletada
				.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
				}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			)
	
	
		oBrwEst:SetColumns({{;
				"Venda",;  					   				// T�tulo da coluna
				{|| If(Empty(aDadEst),0,aDadEst[oBrwEst:At(),2]) },;		// Code-Block de carga dos dados
				"N",;                       			// Tipo de dados
				GetSx3Cache("B2_QATU","X3_PICTURE"),;                       			// M�scara
				0,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				GetSx3Cache("B2_QATU","X3_TAMANHO"),;                       			// Tamanho
				GetSx3Cache("B2_QATU","X3_DECIMAL"),;                       			// Decimal
				.F.,;                                       			// Indica se permite a edi��o
				{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
				NIL,;                       			// Indica se exibe imagem
				nil,;                     			// Code-Block de execu��o do duplo clique
				NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
				{||.T.},;                                   			// Code-Block de execu��o do clique no header
				.F.,;                                       			// Indica se a coluna est� deletada
				.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
				}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			)
			
			
	
	oBrwEst:SetWidthColumns({4,11,12})
	oBrwEst:Activate()

	aDadEst:= {}

	oButSair:oFont:= oFont1

	//oDlg:lMaximized := .T.
	oGet1:SetFocus()
	ACTIVATE MSDIALOG oDlgEst

 	
Return