#include "protheus.ch"
#include "topconn.ch"
#INCLUDE "rwmake.ch"

User Function DEV009()

Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.

Private cString := "ZNF"

dbSelectArea("ZNF")
dbSetOrder(1)

AxCadastro(cString,"Mensagem Nota de Devolu��o",cVldExc,cVldAlt)

Return
