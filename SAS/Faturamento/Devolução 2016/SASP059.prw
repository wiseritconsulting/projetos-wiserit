#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#Include "RWMAKE.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"

/*/{Protheus.doc} SASP059
TELA DE AJUSTE KIT NOS PEDIDOS
@author Jo�o Filho 
@since 24/03/2016
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

Static aCacheTES	:= {}

user function SASP059()

	Local aArea := getarea()
	Local oBrowse
	Local aCoors	 := FwGetDialogSize( oMainWnd )
	Local oFWLayer,oPanelTop1,oView
	Local bBlockClique
	Local aFiltros   := {}
	Private aItens := {} 
	Private oDlg
	Private aRotina	:= {}
	Static nOK       := 1
	Static nFilial   := 2
	Static nPedido   := 3
	Static nItem   	 := 4
	Static nProd	 := 5
	Static nDesPro   := 6
	Static nItemCot  := 7	
	Static nCodKit   := 8
	Static nPaiKit   := 9
	Static nQTDVEN   := 10
	Static nPRCVEN   := 11
	Static nVALOR    := 12
	Static nZ7KIT	 := 13
	Static nZ7PAI	 := 14
	Static nContra	 := 15
	Static nMedica	 := 16

	aItens := U_RKitSC6(aItens)
	
	if len(aItens) == 0
	 	return
	endif

	oDlg		:= MSDialog():New( aCoors[1],aCoors[2],aCoors[3],aCoors[4],"Ajuste de Kit",,,.F.,,,,,,.T.,,,.T. )
	//oDlg		:= MSDialog():New( 0,0,500,700,"Planejamento e Controle de Expedi��o",,,.F.,,,,,,.T.,,,.T. )
	oFWLayer	:= FWLayer():New()
	oFWLayer:Init( oDlg, .F., .T. )
	oFWLayer:AddLine( 'TOP1', 100, .F. )
	oPanelTop1 := oFWLayer:getLinePanel( 'TOP1' )

	oBrowse	    := FWFormBrowse():New()
	oMarkBrow	:= oBrowse:FWBrowse()
	oMarkBrow:SetDataArray()
	oMarkBrow:SetArray(@aItens)
	oMarkBrow:SetDescription('Ajuste Pedido - Kit')
	oMarkBrow:SetOwner( oDlg )
	oMarkBrow:SetProfileID("4")
	oMarkBrow:SetMenuDef("")
	oMarkBrow:SetDoubleClick({|| nil })

	bBlockClique := {|| CLICKZ7() }

	// bot�es

	//oMarkBrow:AddButton("Atualizar",{||YATUALIZ(aItens,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Bloquear",{||YBLOQMED(aItens,oMarkBrow,1)},,,)
	//oMarkBrow:AddButton("Desbloquear",{||YDSBLQMED(aItens,oMarkBrow,1)},,,)
	//oMarkBrow:AddButton("Transportadora",{||YTRANSP(aItens,oMarkBrow,1)},,,)
	//oMarkBrow:AddButton("Confirmar ",{||YCONFIRMA(aItens,oMarkBrow,1)},,,)
	//oMarkBrow:AddButton("Pedido 01 ",{||YPED01(aItens,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Pedido 02 ",{||YPED02(aItens,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Pedido 03 ",{||YPED03(aItens,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Alterar Medi��o ",{||YALTMED(aItens,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Impressao ",{||YIMPRIMIR(aItens,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Historico ",{||YHISTOR(aItens,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Placa de embarque ",{||YPLACA(aItens,oMarkBrow)},,,)

	//oMarkBrow:AddButton("Devolver",{||U_DEV002(aItens,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Devolver",{||FWExecView('Inclusao por FWExecView','DEV002', MODEL_OPERATION_INSERT, , { || .T. }, , , )},,,)//aButtons
	oMarkBrow:AddButton("Alterar",{||AtuKitC6(aItens,oMarkBrow)},,,)
	oMarkBrow:AddButton("Atualizar",{||KitC6OK(aItens,oMarkBrow)},,,)

	// legenda
	oMarkBrow:AddLegend({||aItens[oMarkBrow:At()][nZ7KIT]  == "" }  ,"BR_PRETO",     "Kit nao definido")
	oMarkBrow:AddLegend({||aItens[oMarkBrow:At()][nZ7KIT]  <> "" }  ,"BR_VERDE",     "Kit definido")

	//----------------------------------------------------------------------------------------------------------------
	/*
	// FILTROS 
	oBrowse:SetUseFilter()

	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nFilial)+"]",GetSx3Cache("ZZR_FILIAL","X3_TITULO"),"C",GetSx3Cache("ZZR_FILIAL","X3_TAMANHO"),0,GetSx3Cache("ZZR_FILIAL","X3_PICTURE")} )
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nContra)+"]",GetSx3Cache("ZZR_CONTRA","X3_TITULO"),"C",GetSx3Cache("ZZR_CONTRA","X3_TAMANHO"),0,GetSx3Cache("ZZR_CONTRA","X3_PICTURE")} )
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nTes)+"]" ,GetSx3Cache("ZZV_TES","X3_TITULO"),"C",GetSx3Cache("ZZV_TES","X3_TAMANHO"),0,GetSx3Cache("ZZV_TES","X3_PICTURE")} )
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nMedia)+"]" ,GetSx3Cache("ZZR_MEDICA","X3_TITULO"),"C",GetSx3Cache("ZZR_MEDICA","X3_TAMANHO"),0,GetSx3Cache("ZZR_MEDICA","X3_PICTURE")} )
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nTpMed)+"]" ,GetSx3Cache("ZZ2_TIPCTR","X3_TITULO"),"C",GetSx3Cache("ZZ2_TIPCTR","X3_TAMANHO"),0,GetSx3Cache("ZZ2_TIPCTR","X3_PICTURE")} )	
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nCodCli)+"]",GetSx3Cache("ZZR_CODCLI","X3_TITULO"),"C",GetSx3Cache("ZZR_CODCLI","X3_TAMANHO"),0,GetSx3Cache("ZZR_CODCLI","X3_PICTURE")} )
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nLjClia)+"]",GetSx3Cache("ZZ2_LOJA","X3_TITULO"),"C",GetSx3Cache("ZZ2_LOJA","X3_TAMANHO"),0,GetSx3Cache("ZZ2_LOJA","X3_PICTURE")} )
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nNomCli)+"]",GetSx3Cache("ZZ2_NOME","X3_TITULO"),"C",GetSx3Cache("ZZ2_NOME","X3_TAMANHO"),0,GetSx3Cache("ZZ2_NOME","X3_PICTURE")} )
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nDtGera)+"]"  ,GetSx3Cache("ZZ2_DTGER","X3_TITULO"),"D",GetSx3Cache("ZZ2_DTGER","X3_TAMANHO"),0,GetSx3Cache("ZZ2_DTGER","X3_PICTURE")} )
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nNF01)+"]",GetSx3Cache("ZZ2_PV01NF","X3_TITULO"),"C",GetSx3Cache("ZZ2_PV01NF","X3_TAMANHO"),0,GetSx3Cache("ZZ2_PV01NF","X3_PICTURE")} )
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nNF02)+"]",GetSx3Cache("ZZ2_PV01NF","X3_TITULO"),"C",GetSx3Cache("ZZ2_PV01NF","X3_TAMANHO"),0,GetSx3Cache("ZZ2_PV01NF","X3_PICTURE")} )
	Aadd( aFiltros, { "aItens[oBrowse:At()]["+cvaltochar(nNF03)+"]",GetSx3Cache("ZZ2_PV01NF","X3_TITULO"),"C",GetSx3Cache("ZZ2_PV01NF","X3_TAMANHO"),0,GetSx3Cache("ZZ2_PV01NF","X3_PICTURE")} )


	oBrowse:SetFieldFilter(aFiltros)
	*/

	// BOX
	oMarkBrow:AddMarkColumns(;
	{||If(aItens[oMarkBrow:At()][nOk]=="","LBNO","LBTICK")};	//Imagem da marca
	,bBlockClique/*{|| YINVERTE(oMarkBrow)}*/;
	,{|| YINVERTE(oMarkBrow)})

	//Filial
	oMarkBrow:SetColumns({{;
	"Filial",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nFilial],"")},;       // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C6_FILIAL","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C6_FILIAL","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Numero do Pedido
	oMarkBrow:SetColumns({{;
	"Pedido",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nPedido],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C6_NUM","X3_TAMANHO"),;                         	   // Tamanho
	GetSx3Cache("C6_NUM","X3_DECIMAL"),;                       	       // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Numero Item
	oMarkBrow:SetColumns({{;
	"Item",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nItem],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C6_ITEM","X3_TAMANHO"),;                         	   // Tamanho
	GetSx3Cache("C6_ITEM","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Produto
	oMarkBrow:SetColumns({{;
	"Produto",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nProd],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C6_PRODUTO","X3_TAMANHO"),;                         	   // Tamanho
	GetSx3Cache("C6_PRODUTO","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Descri��o Produto
	oMarkBrow:SetColumns({{;
	"Desc Produto",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nDesPro],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C6_PRODUTO","X3_TAMANHO"),;                         	   // Tamanho
	GetSx3Cache("C6_PRODUTO","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Itens do Contrato
	oMarkBrow:SetColumns({{;
	"Item Contrato",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nItemCot],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ3_ITEM","X3_TAMANHO"),;                         	   // Tamanho
	GetSx3Cache("ZZ3_ITEM","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// kIT NO PEDIDO
	oMarkBrow:SetColumns({{;
	"kIt do Produto",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nCodKit],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ7_CODIGO","X3_TAMANHO"),;                         	   // Tamanho
	GetSx3Cache("ZZ7_CODIGO","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// kIT PAI NO PEDIDO
	oMarkBrow:SetColumns({{;
	"Pai do kIt ",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nPaiKit],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ7_CODPAI","X3_TAMANHO"),;                         	   // Tamanho
	GetSx3Cache("ZZ7_CODPAI","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Quantidade 
	oMarkBrow:SetColumns({{;
	"Quantidade ",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nQTDVEN],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C6_QTDVEN","X3_TAMANHO"),;                         	   // Tamanho
	GetSx3Cache("C6_QTDVEN","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)


	// Pro�o venda   
	oMarkBrow:SetColumns({{;
	"Pro�o Venda ",;  					   			                   // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nPRCVEN],"")},;       // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C6_PRCVEN","X3_TAMANHO"),;                            // Tamanho
	GetSx3Cache("C6_PRCVEN","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Valor 
	oMarkBrow:SetColumns({{;
	"Valor ",;  					   			                   // T�tulo da coluna
	{|| If(!Empty(aItens),cvaltochar(aItens[oMarkBrow:At()][nVALOR]),"")},;       // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C6_VALOR","X3_TAMANHO"),;                            // Tamanho
	GetSx3Cache("C6_VALOR","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Kit do produto 
	oMarkBrow:SetColumns({{;
	"Kit ZZ7 ",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nZ7KIT],"")},;        // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ7_CODIGO","X3_TAMANHO"),;                             // Tamanho
	GetSx3Cache("ZZ7_CODIGO","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Descr 
	oMarkBrow:SetColumns({{;
	" Pai do Kit ZZ7 ",;  					   			                 // T�tulo da coluna
	{|| If(!Empty(aItens),aItens[oMarkBrow:At()][nZ7PAI],"")},;        // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ7_CODPAI","X3_TAMANHO"),;                             // Tamanho
	GetSx3Cache("ZZ7_CODPAI","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)


	oMarkBrow:SetMenuDef( 'SASP059' )

	oMarkBrow:Activate()
	oDlg:Activate(,,,.T.)
	RestArea(aArea)
	//Reset Environment
	aItens := {}

Return

user function RKitSC6(aItens)

	Local cArea     := GetArea()
	Local cNum      := ""
	Local cFil      := ""
	Local aParam    := {}
	Local aRetParm	:= {}
	Local lOk := .F.
	Public FIMDZZ7 := ""

	aAdd(aParam,{1,"Filial Pedido"  ,"      ","@!",'.T.',"SM0",".T.",6,.T.})
	aAdd(aParam,{1,"Informe Pedido"  ,"      ","@!",'.T.',,".T.",6,.T.})
	aAdd(aParam,{1,"Filial Medicao Pedido"  ,"      ","@!",'.T.',"SM0",".T.",6,.T.})

	If ParamBox(aParam,"Filtrar Pedidos",@aRetParm,{||.T.},,,,,,"U_SASP059",.T.,.T.)
		lOk	:= .T.
	EndIf

	if lOk
		cFil    := aRetParm[1]
		cNum    := aRetParm[2]
		FIMDZZ7 := aRetParm[3]
	endif

	cQuery := " SELECT  C6_FILIAL,C6_NUM,C6_ITEM,C6_PRODUTO,C6_DESCRI,C6_YCODKIT,C6_YITCONT,"
	cQuery += " C6_YPAIKIT,C6_QTDVEN,C6_PRCVEN,C6_VALOR,C6_YCONTRA,C6_YMEDICA " 
	cQuery += " FROM "+RetSqlName("SC6")+" SC6 	"
	cQuery += " WHERE C6_NUM = '"+cNum+"'	"
	cQuery += "  AND C6_FILIAL = '"+cFil+"'	"
	cQuery += " AND SC6.D_E_L_E_T_ = ' '		"
	cQuery += " ORDER BY C6_PRODUTO				"	

	If SELECT("T01")>0
		T01->(dbCloseArea())
	Endif

	TCQUERY cQuery NEW ALIAS T01

	If T01->(Eof())
		Alert("Nenhum Registro localizado!!")
		T01->(DbCloseArea())
		Return aItens
	EndIf

	WHILE !T01->(EOF())
		aadd(aItens,;
		{"",;
		ALLTRIM(T01->C6_FILIAL),;
		ALLTRIM(T01->C6_NUM),;
		ALLTRIM(T01->C6_ITEM),;
		ALLTRIM(T01->C6_PRODUTO),;
		ALLTRIM(T01->C6_DESCRI),;
		ALLTRIM(T01->C6_YITCONT) ,;
		ALLTRIM(T01->C6_YCODKIT) ,;		
		ALLTRIM(T01->C6_YPAIKIT) ,;
		T01->C6_QTDVEN ,;
		T01->C6_PRCVEN ,;
		T01->C6_VALOR,;
		IIF(KitPaiOk(mv_par01 ,T01->C6_YCONTRA,T01->C6_YMEDICA,T01->C6_YCODKIT,T01->C6_YPAIKIT,T01->C6_PRODUTO ) ,T01->C6_YCODKIT ,"") ,;
		IIF(KitPaiOk(mv_par01 ,T01->C6_YCONTRA,T01->C6_YMEDICA,T01->C6_YCODKIT,T01->C6_YPAIKIT,T01->C6_PRODUTO ) ,T01->C6_YPAIKIT ,""),;
		ALLTRIM(T01->C6_YCONTRA),;
		ALLTRIM(T01->C6_YMEDICA)})
		T01->(dbSkip())
	ENDDO

	T01-> (dbclosearea())
return aItens

Static Function KitPaiOk(cfili,cContr,cMedica,cKit,cPai,cProd)

	Local lRet  := .F.
	Local nCont := 0

	//Seleciona a medi��o referente ao Pedido
	cQuery := " SELECT ZZ3_PRODUT  "
	cQuery += " FROM "+RetSqlName("ZZ3")+" ZZ3		"
	cQuery += " WHERE ZZ3_NUMERO = '"+cContr+"'	"
	cQuery += " AND ZZ3_MEDICA = '"+cMedica+"'	"
	cQuery += " AND ZZ3_FILIAL = '"+FIMDZZ7+"'	"
	//cQuery += " AND ZZ3_PRODUT = '"+cKit+"'	"
	cQuery += " AND ZZ3.D_E_L_E_T_ = ' '			"
	cQuery += " AND ZZ3_TIPO = 'K' 					"

	If SELECT("T02")>0
		T02->(dbCloseArea())
	Endif

	TCQUERY cQuery NEW ALIAS T02

	While T02->(!eof())

		//Busca itens do Kit referente a medi��o
		cQuery := " SELECT ZZ7_CODPAI,ZZ7_DESCR,ZZ7_PRODUT "
		cQuery += " FROM "+RetSqlName("ZZ7")+" ZZ7 "
		cQuery += " WHERE ZZ7_CODPAI = '"+T02->ZZ3_PRODUT+"' "
		cQuery += " AND ZZ7_PRODUT = '"+cProd+"'  "
		cQuery += " AND ZZ7.D_E_L_E_T_ =' '  "
		cQuery += " AND ZZ7_TIPO = 'A' "

		TCQUERY cQuery NEW ALIAS T03

		if !T03->(eof())
			nCont ++
		endif

		T03->(dbCloseArea())

		dbSelectArea("T02")
		T02->(dbSkip())
	Enddo

	if nCont = 1
		lRet := .T.
	endif

	T02->(dbCloseArea())

Return lRet


Static FUNCTION CLICKZ7()
	Local nTem  := 0
	Local cTipo := "" 
	Local nCont := 0
	//Public cFilZZ7
	//Public cConZZ7
	//Public cMedZZ7
	//Public cKitZZ7

	IF aItens[oMarkBrow:At()][nOk] == ""
		for nAx := 1 to len(aItens)


			if alltrim(aItens[nAx][nOK]) == '1' 
				nCont ++
			endif
			IF nCont == 1
				Msginfo("Selecione apenas um item")
				return
			endif	

		next

		aItens[oMarkBrow:At()][nOk]:= "1" 			

	else
		aItens[oMarkBrow:At()][nOk] := ""
	endif	

RETURN

Static Function AtuKitC6(aItens,oMarkBrow)

	Local aParam := {}
	Local aRetParm	:= {}
	Local LOK := .F.
	Local cCodKit	:= Space(GetSx3Cache("ZZ7_CODIGO","X3_TAMANHO"))
	//Public Filzz7   := aItens[oMarkBrow:At()][nFilial]
	Public Cotrz7   := aItens[oMarkBrow:At()][nContra]
	Public Medzz7   := aItens[oMarkBrow:At()][nMedica]
	Public Prdzz7   := aItens[oMarkBrow:At()][nProd]


	AAdd(aParam,{1,"Informe Kit" ,cCodKit,"@!",,"KITZZ7",".T.",80,.F.}) 	 

	If ParamBox(aParam,"Escolha da transportadora",@aRetParm,{||.T.},,,,,,"U_SASP059",.T.,.T.)
		lOk	:= .T.
	EndIf

	if lOk
		// kit
		aItens[oMarkBrow:At()][nZ7KIT]   := alltrim(aRetParm[1])

		//item contrato
		cQuery := " SELECT ZZ1_ITEM "
		cQuery += " FROM "+RetSqlName("ZZ1")+" ZZ1 "
		cQuery += " WHERE ZZ1_FILIAL = '"+FIMDZZ7+"' "
		cQuery += " AND ZZ1_NUMERO = '"+Cotrz7+"'  "
		cQuery += " AND ZZ1_PRODUT = '"+alltrim(aRetParm[1])+"'  "
		cQuery += " AND ZZ1.D_E_L_E_T_ =' '  "

		TCQUERY cQuery NEW ALIAS T04

		aItens[oMarkBrow:At()][nItemCot] := T04->ZZ1_ITEM 

		T04->(dbCloseArea())

		//cod pai
		cQuery := " select ZZ7_CODPAI "
		cQuery += " from "+RetSqlName("ZZ7")+" ZZ7 "
		cQuery += " where ZZ7_CODIGO =	'"+alltrim(aRetParm[1])+"' "
		cQuery += " AND ZZ7_CATEGO IN ('A','P','C')
		cQuery += " AND D_E_L_E_T_=''

		TCQUERY cQuery NEW ALIAS T04

		aItens[oMarkBrow:At()][nZ7PAI] := T04->ZZ7_CODPAI 

		T04->(dbCloseArea())

	endif

	YDESMARCA()
	oMarkBrow:refresh()
Return


// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO DESMARCADO
STATIC FUNCTION YDESMARCA()

	FOR nAx := 1 to len(aItens)
		aItens[nAx][nOK] := ""
	NEXT


RETURN

STATIC FUNCTION KitC6OK(aItens,oMarkBrow)


	for nAx := 1 to len(aItens)

		if alltrim(aItens[nAx][nZ7KIT]) == '' .OR. alltrim(aItens[nAx][nZ7PAI]) == ''
			Msginfo("AJUSTAR ITENS PENDENTES")
			return
		endif

	next

	for nAx := 1 to len(aItens)

		dbSelectArea("SC6")
		dbSetOrder(1)      // C6_FILIAL, C6_NUM, C6_ITEM, C6_PRODUTO, R_E_C_N_O_, D_E_L_E_T_
		IF dbSeek(aItens[nAx][nFilial] + aItens[nAx][nPedido] + aItens[nAx][nItem] + aItens[nAx][nProd] )     // Busca exata

			RECLOCK("SC6", .F.)

			SC6->C6_YITCONT := aItens[nAx][nItemCot]
			SC6->C6_YCODKIT := aItens[nAx][nZ7KIT]
			SC6->C6_YPAIKIT := aItens[nAx][nZ7PAI]
			SC6->C6_YAJKIT  := SC6->C6_YAJKIT+"/ Usuario "+UsrFullName(RetCodUsr())+"-Data " + cvaltochar(date()) + "-Hora " + cvaltochar(time())  
			MSUNLOCK()     // Destrava o registro

		ENDIF
	next
	
	Msginfo("Atualiza��o Concluida")
	oMarkBrow:refresh()
RETURN
/*
//Seleciona a medi��o referente ao Pedido
cQuery := " SELECT ZZ3_PRODUT  "
cQuery += " FROM "+RetSqlName("ZZ3")+" ZZ3		"
cQuery += " WHERE ZZ3_NUMERO = '"+T1->C6_YCONTRA+"'	"
cQuery += " AND ZZ3_MEDICA = '"+T1->C6_YMEDICA+"'	"
cQuery += " AND ZZ3_FILIAL = '"+mv_par01+"'	"
cQuery += " AND ZZ3.D_E_L_E_T_ = ' '			"
cQuery += " AND ZZ3_TIPO = 'K' 					"

If SELECT("T02")>0
T02->(dbCloseArea())
Endif

TCQUERY cQuery NEW ALIAS T02

While T02->(!eof())

//Busca itens do Kit referente a medi��o
cQuery := " SELECT ZZ7_CODPAI,ZZ7_DESCR,ZZ7_PRODUT "
cQuery += " FROM "+RetSqlName("ZZ7")+" ZZ7 "
cQuery += " WHERE ZZ7_CODPAI = '"+T2->ZZ3_PRODUT+"' "
cQuery += " AND ZZ7.D_E_L_E_T_ =' '  "
cQuery += " AND ZZ7_TIPO = 'A' "

TCQUERY cQuery NEW ALIAS T03

while T03->(!eof())

aadd(aKit,{ T03->ZZ7_CODPAI 	,;
T03->ZZ7_DESCR 	,;
T03->ZZ7_PRODUT 	;
};
)

T03->(dbSkip())
enddo

T03->(dbCloseArea())

dbSelectArea("T02")
T02->(dbSkip())
Enddo

T02->(dbCloseArea())
*/

