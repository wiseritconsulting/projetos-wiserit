#include 'parmtype.ch'
#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"
#include "dbtree.ch"



/*/{Protheus.doc} DEV002
Altera��o da Medi��o
@author Jo�o Filho / Diogo
@since 16/09/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User Function DEV002()

	oBrowse:= FWmBrowse():New()
	oBrowse:SetAlias('ZZH')
	oBrowse:SetDescription('Medi��o')

	oBrowse:AddLegend( "ZZH_STATUS == 'BL' .AND. !(ZZH_STFAT $ ('F |N2|N3|N4')) ", "BLACK", "Aguardando Libera��o"  )
	oBrowse:AddLegend( "ZZH_STATUS == 'LA' .AND. !(ZZH_STFAT $ ('F |N2|N3|N4')) ", "GREEN", "Em Cota��o"  )
	oBrowse:AddLegend( "ZZH_STATUS == 'RP' .AND. !(ZZH_STFAT $ ('F |N2|N3|N4')) ", "BR_CANCEL","Reprovada"  )
	oBrowse:AddLegend( "ZZH_STATUS == 'LB' .AND. !(ZZH_STFAT $ ('F |N2|N3|N4')) ", "BR_AZUL", "Aguardando Faturamento"  )
	oBrowse:AddLegend( "ZZH_STATUS == 'LB' .AND. ZZH_STFAT   $ ('F |N2|N3|N4')",   "RED", "Faturado"  )
	oBrowse:AddLegend( "ZZH_STATUS == 'AJ' ",   "BR_MARROM", "Aguardando Valida��o Ajuste"  )
	oBrowse:AddLegend( "ZZH_STATUS == 'AV' ",   "BR_BRANCO", "Ajuste Validado"  )
	oBrowse:AddLegend( "ZZH_STATUS == 'AN' ",   "BR_LARANJA", "Ajuste n�o Validado"  )

	oBrowse:SETMENUDEF('DEV002')
	oBrowse:Activate()

Return

Static Function MenuDef()
	Local aRotina := {}
	Public aKit := {}

	ADD OPTION aRotina Title 'Pesquisa'      Action 'PesqBrw'         OPERATION 1 ACCESS 0
	ADD OPTION aRotina Title 'Visualizar'    Action 'VIEWDEF.DEV002'  OPERATION 2 ACCESS 0
	ADD OPTION aRotina Title 'Alterar'       Action 'VIEWDEF.DEV002'  OPERATION 4 ACCESS 0
	ADD OPTION aRotina Title 'Excluir'       Action 'VIEWDEF.DEV002'  OPERATION 5 ACCESS 0
	ADD OPTION aRotina Title 'Transportadora e frete'    Action 'U_TRANZZH()'   OPERATION 5 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina Title 'Liberar'                   Action 'U_LIBERZZH()'  OPERATION 5 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina Title 'Validar Ajuste'            Action 'U_AJUZZH()'    OPERATION 5 ACCESS 0 DISABLE MENU


Return aRotina

Static Function ModelDef()

	Local oStructZZH := FWFORMSTRUCT(1,'ZZH')
	Local oStructZZV := FWFORMSTRUCT(1,'ZZV')
	Local oModel     := MPFormModel():New('DEV002M')

	oStructZZH:SetProperty("ZZH_PEDDEV",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| LOJAZZH(oModelGrid,cId,xValor) })
	oStructZZV:SetProperty("ZZV_QUANT",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| QUANTZZV(oModelGrid,cId,xValor) })
	oStructZZH:SetProperty("ZZH_OBS",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| OBSZZH(oModelGrid,cId,xValor) })
	oStructZZV:SetProperty("ZZV_MEDORG",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| MDORGZZV(oModelGrid,cId,xValor,oModel) })
	oStructZZV:SetProperty("ZZV_PRODUT",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| LINHAZZV(oModelGrid,cId,xValor,oModel) })

	oModel:AddFields('ZZHMASTER',/*pai*/,oStructZZH)

	oModel:AddGrid("ZZVDETAIL", "ZZHMASTER"/*cOwner*/,oStructZZV, ,/*bLinePost*/,/*bPre*/,/*bPost*/,/*Carga*/)

	oModel:SetRelation('ZZVDETAIL',{{'ZZV_FILIAL','xFilial("ZZV")'},{"ZZV_NUMERO","ZZH_NUMERO"},{"ZZV_MEDICA","ZZH_MEDICA"}},ZZV->(IndexKey(1)))


	oModel:SetPrimaryKey({"ZZH_FILIAL","ZZH_NUMERO","ZZH_CLIENT","ZZH_LOJA","ZZH_MEDICA"})

	if FunName() == "DEV001"
		oModel:SetActivate({|oModel,| LOADZZV(oModel) })
	endif
	oModel:SetVldActivate( { |oModel| VALDALT( oModel ) } )
	oModel:SetOnlyQuery('ZZHMASTER',.T.)
	oModel:SetCommit({|oModel| ZZHCommit(oModel) },.F.)

Return (oModel)

Static Function ViewDef()

	Local oModel    := FWLoadModel('DEV002')
	Local oStructZZH:= FWFormStruct(2,'ZZH', {|cCampo|  !AllTrim(cCampo)+"|" $ "ZZH_USERGR|ZZH_HRAPRO|ZZH_USERAP|ZZH_PC01|ZZH_PC03FL|ZZH_PC03|"})
	Local oStructZZV:= FWFormStruct(2,'ZZV')
	Local oView:=FWFormView():New()

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField('ZZHMASTER',oStructZZH)
	oView:AddGrid('ZZVDETAIL',oStructZZV)


	oView:CreateHorizontalBox('SUPERIOR',50)
	oView:CreateHorizontalBox('INFERIOR',50)


	oView:SetOwnerView('ZZHMASTER','SUPERIOR')
	oView:SetOwnerView('ZZVDETAIL','INFERIOR')

	oView:EnableTitleView('ZZVDETAIL','Itens Devolu��o')

	oView:SetProgressBar(.T.)
	oView:AddIncrementField("ZZVDETAIL","ZZV_ITEM")

	oView:AddUserButton( 'Procurar Kit', 'CLIPS', { |oView| ProcKit(oView) }, , VK_F4,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} )


Return oView

STATIC FUNCTION LOADZZV(oModel)

	Local aRet := {}
	Local aArea   := GetArea()
	Local cTmp    := ''
	Local cQuery  := ''
	Local cFields := 'R_E_C_N_O_'
	Local nTam
	Local oModelZZV := oModel:GetModel( 'ZZVDETAIL' )
	Local oModelZZH := oModel:GetModel( 'ZZHMASTER' )
	Local cMedicao := ""
	Local cContra  := ""
	Local cFilCont := ""
	Local cCodCli  := ""
	Local cCodLj   := ""
	Local cEspecie := ""

	//sequencial medicao de devolu��o
	cquery2 := " select MAX(ZZH_MEDICA) AS ZZH_MEDICA "
	cquery2 += " from " + retsqlname('ZZH') + " ZZH (NOLOCK) "
	cquery2 += " where ZZH_FILIAL = " + aSelect[1] + " "
	//cquery2 += " and D_E_L_E_T_ = '' "
	cquery2 += " and ZZH_NUMERO in("+ aSelect[2] +") "

	TCQuery cquery2 new alias T02
	DBSELECTAREA("T02")
	IF Alltrim(T02->ZZH_MEDICA) == ""
		cMedicao := STRZERO(1,GetSx3Cache("ZZH_MEDICA","X3_TAMANHO"),0)
	ELSE
		cMedicao := STRZERO(VAL(T02->ZZH_MEDICA)+1,GetSx3Cache("ZZH_MEDICA","X3_TAMANHO"),0)
	ENDIF

	T02->(DBCLOSEAREA())

	// tratamento para a grid

	cTmp   := GetNextAlias()
	cQuery := " SELECT * "
	cQuery += " FROM "+RetSqlName( 'ZZ3' ) + " ZZ3 (NOLOCK) "
	cQuery += " WHERE ZZ3_FILIAL in("+ aSelect[1] + ") "
	cQuery += " AND ZZ3_NUMERO in("+ aSelect[2] +") "
	cQuery += " AND ZZ3_CLIENT in("+ aSelect[4] +") "
	cQuery += " AND ZZ3_LOJA   in("+ aSelect[5] +") "
	cQuery += " AND ZZ3_MEDICA in("+ aSelect[3] +") "
	cQuery += " AND D_E_L_E_T_ =''  "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.F.,.T.)

	cContra  := (cTmp)->ZZ3_NUMERO
	cFilCont := (cTmp)->ZZ3_FILIAL
	cCodCli  := (cTmp)->ZZ3_CLIENT
	cCodLj   := (cTmp)->ZZ3_LOJA

	While (cTmp)->(!EOF())
		oModelZZV:AddLine()
		oModel:SetValue("ZZVDETAIL", "ZZV_FILIAL" , (cTmp)->ZZ3_FILIAL )
		oModel:SetValue("ZZVDETAIL", "ZZV_NUMERO" , (cTmp)->ZZ3_NUMERO )
		oModel:SetValue("ZZVDETAIL", "ZZV_MEDICA" , cMedicao)
		oModel:SetValue("ZZVDETAIL", "ZZV_CLIENT" , (cTmp)->ZZ3_CLIENT)
		oModel:SetValue("ZZVDETAIL", "ZZV_LOJA"   , (cTmp)->ZZ3_LOJA)
		oModel:SetValue("ZZVDETAIL", "ZZV_TES"    , (cTmp)->ZZ3_TES)
		oModel:SetValue("ZZVDETAIL", "ZZV_PRODUT" , (cTmp)->ZZ3_PRODUT)
		if	(cTmp)->ZZ3_TIPO == "K"
			oModel:SetValue("ZZVDETAIL", "ZZV_DESCRI" , POSICIONE("ZZ7",1,XFILIAL("ZZ7")+(cTmp)->ZZ3_PRODUT,'ZZ7_DESCR'))
		elseif (cTmp)->ZZ3_TIPO == "P"
			oModel:SetValue("ZZVDETAIL", "ZZV_DESCRI" , POSICIONE("SB1",1,XFILIAL("SB1")+(cTmp)->ZZ3_PRODUT,'B1_DESC'))
		endif		
		oModel:SetValue("ZZVDETAIL", "ZZV_ENVIO"  , (cTmp)->ZZ3_ENVIO)
		oModel:SetValue("ZZVDETAIL", "ZZV_ENVIO"  , (cTmp)->ZZ3_ENVIO)
		oModel:SetValue("ZZVDETAIL", "ZZV_MEDORG" , (cTmp)->ZZ3_MEDICA)
		oModel:SetValue("ZZVDETAIL", "ZZV_ITEMMD" , (cTmp)->ZZ3_ITEM)  
		oModel:SetValue("ZZVDETAIL", "ZZV_TIPO"   , (cTmp)->ZZ3_TIPO)
		oModel:SetValue("ZZVDETAIL", "ZZV_SALDO"  , (cTmp)->ZZ3_QUANT - (cTmp)->ZZ3_QTDEV0 )
		oModel:SetValue("ZZVDETAIL", "ZZV_QTDEVO" , (cTmp)->ZZ3_QTDEV0 )


		cquery := " select * "
		cquery += " from " + retsqlname('ZZ2') + " ZZ2 (NOLOCK) "
		cquery += " where ZZ2.D_E_L_E_T_ = '' "
		cquery += " and ZZ2_FILIAL = '"+(cTmp)->ZZ3_FILIAL+"' "
		cquery += " and ZZ2_NUMERO = '"+(cTmp)->ZZ3_NUMERO+"' "
		cquery += " and ZZ2_CLIENT = '"+(cTmp)->ZZ3_CLIENT+"' "
		cquery += " and ZZ2_LOJA = '"+(cTmp)->ZZ3_LOJA+"' "
		cquery += " and ZZ2_MEDICA = '"+(cTmp)->ZZ3_MEDICA+"' "

		TCQuery cquery new alias TZH
		DBSELECTAREA("TZH")

		// verifica o tipo de medi��o - triangular ou direta
		if ! cEspecie $ ALLTRIM(TZH->ZZ2_TIPCTR)
			cEspecie += ALLTRIM(TZH->ZZ2_TIPCTR)
		endif

		oModel:SetValue("ZZVDETAIL", "ZZV_NF1ORG" , TZH->ZZ2_PV01FL+TZH->ZZ2_SER01+TZH->ZZ2_PV01NF+(cTmp)->ZZ3_PRODUT) // FILIAL + SERIE + DOC + ITEM
		oModel:SetValue("ZZVDETAIL", "ZZV_NF2ORG" , TZH->ZZ2_PV02FL+TZH->ZZ2_SER02+TZH->ZZ2_PV02NF+(cTmp)->ZZ3_PRODUT)
		oModel:SetValue("ZZVDETAIL", "ZZV_NF3ORG" , TZH->ZZ2_PV03FL+TZH->ZZ2_SER03+TZH->ZZ2_PV03NF+(cTmp)->ZZ3_PRODUT)

		TZH->(dbCloseArea())
		(cTmp)->(DbSkip())
	enddo 	

	// regra para bloquear a inclusao de linhas na grid 
	OMODEL:AMODELSTRUCT[1][4][1][3]:NMAXLINE := OMODEL:AMODELSTRUCT[1][4][1][3]:NLINE 
	//===================================================

	(cTmp)->(dbCloseArea())
	RestArea(aArea)

	//------------------------------//
	// tratamento para o cabe�alho	//
	//------------------------------//

	cQuery3 := " SELECT * "
	cQuery3 += " FROM " + retsqlname('ZZ2') + " ZZ2 (NOLOCK) "
	cQuery3 += " WHERE ZZ2_NUMERO = '"+ cContra +"' AND  "
	cQuery3 += " ZZ2_FILIAL = '"+ cFilCont +"' AND  "
	cQuery3 += " ZZ2_CLIENT = '"+ cCodCli +"' AND  "
	cQuery3 += " ZZ2_LOJA = '"+ cCodLj +"' AND  "
	cQuery3 += " D_E_L_E_T_ = ''  "

	TCQuery cQuery3 new alias T03
	DBSELECTAREA("T03")

	oModel:SetValue("ZZHMASTER", "ZZH_FILIAL", T03->ZZ2_FILIAL)
	oModel:SetValue("ZZHMASTER", "ZZH_NUMERO", T03->ZZ2_NUMERO)
	oModel:SetValue("ZZHMASTER", "ZZH_CLIENT", T03->ZZ2_CLIENT)
	oModel:SetValue("ZZHMASTER", "ZZH_LOJA",   T03->ZZ2_LOJA)
	oModel:SetValue("ZZHMASTER", "ZZH_NOME",   POSICIONE("SA1",1,XFILIAL("SA1")+T03->ZZ2_CLIENT+T03->ZZ2_LOJA,"A1_NOME"))
	oModel:SetValue("ZZHMASTER", "ZZH_MEDICA", cMedicao)
	oModel:SetValue("ZZHMASTER", "ZZH_ESPEC",  if(cEspecie == "1" ,"01",if(cEspecie == "2" ,"02", "03" ) ) )
	oModel:LoadValue("ZZHMASTER", "ZZH_CONTRI", POSICIONE("SA1",1,XFILIAL("SA1")+T03->ZZ2_CLIENT,"A1_CONTRIB") )

	T03->(DBCLOSEAREA())
	RestArea(aArea)

RETURN 

//REGRA PARA RETORNAR ITEM DA NF DE ORIGEM
Static Function ITEMSD2(cFil,cSerie,cNf,cProd,cMedica)

	Local cRet := "" 

	cquery := " select * "
	cquery += " from " + retsqlname('SD2') + " SD2 "
	cquery += " where SD2.D_E_L_E_T_ = '' " 
	cquery += " and D2_FILIAL  = '"+cFil+"' "
	cquery += " and D2_SERIE   = '"+cSerie+"' "
	cquery += " and D2_DOC     = '"+cNf+"' "
	cquery += " and D2_COD     = '"+cProd+"' "
	cquery += " and D2_YMEDICA = '"+cMedica+"' "

	TCQuery cquery new alias TD2
	DBSELECTAREA("TD2")

	cRet := TD2->D2_ITEM

	TD2->(DBCLOSEAREA())

RETURN cRet

// VALIDA��O DE QUANTIDADE 

STATIC FUNCTION QUANTZZV(oModelGrid,cId,xValor)

	Local lRet      := .T.
	Local aArea     := GetArea()
	Local oModel	:= FWModelActive()
	Local oModelZZH	:= oModel:GetModel('ZZHMASTER')
	Local oModelZZV	:= oModel:GetModel('ZZVDETAIL')
	Local cZ3Quant  := POSICIONE("ZZ3",1,XFILIAL("ZZV")+oModelZZV:GETVALUE("ZZV_NUMERO")+oModelZZV:GETVALUE("ZZV_MEDORG")+oModelZZV:GETVALUE("ZZV_CLIENT")+oModelZZV:GETVALUE("ZZV_LOJA")+oModelZZV:GETVALUE("ZZV_PRODUT")+oModelZZV:GETVALUE("ZZV_TES"),"ZZ3_QUANT")
	Local cZ3Qtdev  := POSICIONE("ZZ3",1,XFILIAL("ZZV")+oModelZZV:GETVALUE("ZZV_NUMERO")+oModelZZV:GETVALUE("ZZV_MEDORG")+oModelZZV:GETVALUE("ZZV_CLIENT")+oModelZZV:GETVALUE("ZZV_LOJA")+oModelZZV:GETVALUE("ZZV_PRODUT")+oModelZZV:GETVALUE("ZZV_TES"),"ZZ3_QTDEV0")

	If xValor > oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_SALDO') 
		MSGINFO("Quantidade superior ao valor de Saldo !!")	
		lRet := .F.
	elseif xValor < 0
		MSGINFO("Quantidade negativa ou igual a zero, s�o valores invalidos !!")	
		lRet := .F.
	Endif


	if oModel:GetOperation() == 4 .and. lRet

		dbSelectArea("ZZV")
		dbSetOrder(1)
		dbSeek(XFILIAL("ZZV")+oModelZZV:GETVALUE("ZZV_NUMERO")+oModelZZV:GETVALUE("ZZV_MEDICA")+oModelZZV:GETVALUE("ZZV_CLIENT")+oModelZZV:GETVALUE("ZZV_LOJA")+oModelZZV:GETVALUE("ZZV_PRODUT")+oModelZZV:GETVALUE("ZZV_TES"),.T.)
		IF	oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_PRODUT') == ZZV->ZZV_PRODUT
			if  oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_QUANT') ==  ZZV->ZZV_QUANT 
				oModel:SetValue("ZZVDETAIL", "ZZV_MOV" , 0 )
				oModel:SetValue("ZZVDETAIL", "ZZV_SALDO" , cZ3Quant - (cZ3Qtdev + oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_MOV')) )
				oModel:SetValue("ZZVDETAIL", "ZZV_QTDEVO" , cZ3Qtdev + oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_MOV') )
			ELSEif  oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_QUANT') <> ZZV->ZZV_QUANT   
				oModel:SetValue("ZZVDETAIL", "ZZV_MOV" , oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_QUANT') - ZZV->ZZV_QUANT )
				oModel:SetValue("ZZVDETAIL", "ZZV_SALDO" ,  cZ3Quant - (cZ3Qtdev + oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_MOV')) )
				oModel:SetValue("ZZVDETAIL", "ZZV_QTDEVO" , cZ3Qtdev + oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_MOV') )
			ENDIF
		ELSE // PRODUTO NOVO
			oModel:SetValue("ZZVDETAIL", "ZZV_MOV" , oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_QUANT')  )
			oModel:SetValue("ZZVDETAIL", "ZZV_SALDO" ,  cZ3Quant - (cZ3Qtdev + oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_MOV')) )
			oModel:SetValue("ZZVDETAIL", "ZZV_QTDEVO" , cZ3Qtdev + oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_MOV') )
		ENDIF
	endif

	RestArea(aArea)


RETURN lRet

// VALIDACAO DO MINIMO DE CARACTERIS ESCRITOS NA OBS QUANDO 
Static Function OBSZZH(oModelGrid,cId,xValor)

	Local lRet := .T.
	Local oModel	:= FWModelActive()

	IF oModel:GetModel('ZZHMASTER'):GetValue('ZZH_ESPEC') == "02" .AND. LEN( ALLTRIM( oModel:GetModel('ZZHMASTER'):GetValue('ZZH_OBS') ) ) < 15 
		MsgInfo("Informar uma OBS com no m�nimo 15 caracteres! ")
		lRet := .F.
	ENDIF 

Return lRet

// VALIDACAO DO PEDIDO DE DEVOLU��O , QUANDO A ORIGEM FOR "LOJINHA" 
Static Function LOJAZZH(oModelGrid,cId,xValor)

	Local lRet := .T.
	Local oModel	:= FWModelActive()

	IF oModel:GetModel('ZZHMASTER'):GetValue('ZZH_ORIGEM') == "01" .AND. LEN( ALLTRIM( oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PEDDEV') ) ) == 0 
		MsgInfo("Informar o pedido de devolu��o! ")
		lRet := .F.
	ENDIF 

Return lRet

Static Function ZZHCommit(oModel)

	Local aArea   := GetArea()
	Local lRet:= .T.
	Local oModelZZH	:= oModel:GetModel('ZZHMASTER')
	Local oModelZZV	:= oModel:GetModel('ZZVDETAIL')
	Local aContri:= {'Sim','N�o'}
	Local aParam	:= {}
	Local aRetParm	:= {}
	Local lOk		:= .F.
	Local aStruct	:= oModel:GetModel( 'ZZVDETAIL' ):oFormModelStruct:GetFields()
	Local nVlrItem  
	Local cLocal    := ""	
	Local aCabDE01   := {}
	Local aCab       := {}
	Local aItens     := {}
	Local aItens2    := {}
	Local aItens3    := {}
	Local aLinha     := {}
	Local aItensDE01 := {}
	Local aItensPC02 := {}
	Local aItensDE03 := {}
	Local aLinDe01   := {}
	Local aLinPc02   := {}
	Local aLinDe03   := {}		
	Local aSlado     := {}
	Local nContIT    := 0
	Local cNaturez     := "2210"
	Local cContrato  := ""
	Local cMedicao := ""
	Local cEspecie := oModelZZH:GETVALUE("ZZH_ESPEC")
	Local cNfOri := ""
	Local cSeriOri := ""
	Local cItemori := ""
	Local cFil02 := ""
	Local nFret  := 0
	Local nDescont   := 0
	Local nDesconto  := 0
	Local nItem := 0
	Local cTesPed  := ""
	Local cUpd  := ""
	Local nLivros := 0

	Local cCLI01 := ""
	Local cLOJ01 := ""
	Local cCLI02 := ""
	Local cLOJ02 := ""
	Local cCLI03 := ""
	Local cLOJ03 := "" 

	Local cFilPV1  := ""
	Local cPedido1 := ""
	Local cNFPD01  := ""
	Local cNFSR01  := ""
	Local cCLI01   := ""
	Local cLOJ01   := ""

	Local cFilPV1  := ""
	Local cPedido1 := ""
	Local cNFPD01 := ""
	Local cNFSR01   := ""

	Local cFilPV2  := ""
	Local cPedido2 := ""
	Local cNFPD02 := ""
	Local cNFSR02  := ""

	Local cFilPV3  := ""
	Local cPedido3 := ""
	Local cNFPD03 := ""
	Local cNFSR03  := ""
	Local cArmazem := GETMV("SA_ARMZDEV")
	Local cArmTran := SuperGetMV("SA_ARMZTRA",,"02")

	Local oDlg ,oSay1 , oButton1 ,oButton2
	Local nOpca := 0
	Local nComboBo1 
	Private cMSG       := ""
	PRIVATE cTIPOMED   := ""

	Private cFilDoc01 := ""
	Private cDoc01 := ""
	Private cNfDoc01 := ""
	Private cSrDoc01 := ""
	Private cCliDoc01 := ""
	Private cLjDoc01:= "" 

	Private cFilDoc02 := ""
	Private cDoc02 := ""
	Private cNfDoc02 := ""
	Private cSrDoc02 := ""
	Private cCliDoc02 := ""
	Private cLjDoc02 := ""
	Private cFilPC02 := ""
	Private cPC02 := ""
	Private cForPc02 := ""
	Private cLjPc02:= "" 
	Private cForn02  := ""
	Private clOJFOR02  := ""

	Private cFilDoc03 := ""
	Private cDoc03 := ""
	Private cDoc03NF := ""
	Private cSrDoc03 := ""
	Private cCliDoc03 := "" 
	Private cLjDoc03:= "" 
	Private cTesTmp	:=	"900"	//Tes temporaria para pedido

	//ALTERA��O DA MEDI��O

	if oModel:GetOperation() == 4

		if !(oModelZZH:GetValue('ZZH_STATUS') == 'BL' .AND. !(oModelZZH:GetValue('ZZH_STFAT') $ ('F |N2|N3|N4')))
			MsgInfo("Medi��o n�o pode ser alterada!!")
			Return .F.
		endif

		IF oModelZZH:GetValue('ZZH_STFAT')  $ ("F |N2|N3|N4")
			MsgInfo("Medi��o j� faturada !!!")
			Return .F.
		ENDIF

		Begin Transaction

			// ALTERA��O DOS CAMPOS DO CABE�ALHO  (ZZH)

			dbSelectArea("ZZH")
			RECLOCK("ZZH", .F.)
			dbseek(oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_FILIAL") + oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_NUMERO") + oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_MEDICA") + oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_CLIENT") + oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_LOJA"))

			ZZH->ZZH_FILIAL	  := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_FILIAL") 
			ZZH->ZZH_NUMERO	  := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_NUMERO") 
			ZZH->ZZH_CLIENT   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_CLIENT")
			ZZH->ZZH_LOJA     := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_LOJA")
			ZZH->ZZH_NOME     := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_NOME")
			ZZH->ZZH_MEDICA   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_MEDICA")		
			ZZH->ZZH_OBS      := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_OBS")
			ZZH->ZZH_TPDEV    := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_TPDEV")
			ZZH->ZZH_ESPEC    := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_ESPEC")
			ZZH->ZZH_TRANSP   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_TRANSP")
			ZZH->ZZH_NOMTRA   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_NOMTRA")
			ZZH->ZZH_FRETE    := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_FRETE")
			ZZH->ZZH_TES      := oModel:Getmodel("ZZVDETAIL"):GetValue("ZZV_TES")
			ZZH->ZZH_USERGR   := retcodusr()
			ZZH->ZZH_ORIGEM   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_ORIGEM")
			ZZH->ZZH_STATUS   := "BL"

			MSUNLOCK()


			For nCont := 1 To oModelZZV:Length()
				oModelZZV:GoLine(nCont)

				AADD(aSlado,{oModelZZV:GetValue("ZZV_MOV")})
				If oModelZZV:IsDeleted()
					AADD(aSlado,{-1 * oModelZZV:GetValue("ZZV_QUANT")})
				EndIf			

				// VALIDA A QUANTIDADE DIGITADA 
				If oModelZZV:GetValue("ZZV_QUANT") == 0 .OR.  ( oModelZZH:GetValue("ZZH_COLETA") == "01" .AND. oModelZZH:GetValue("ZZH_QUATVL") == 0)
					Loop
				EndIf

				nItem ++
				oModel:SetValue("ZZVDETAIL", "ZZV_ITEM" , sTRZERO(nItem,3))


				// BUCA DE PEDIDOS E NOTAS DO ITEM DA MEDI��O
				cquery1 := " SELECT * "
				cquery1 += " from " + retsqlname('ZZ2') + " ZZ2 (NOLOCK) "
				cquery1 += " where "
				cquery1 += " D_E_L_E_T_ = '' "
				cquery1 += " and ZZ2_NUMERO = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_NUMERO')) +"' "
				cquery1 += " and ZZ2_MEDICA = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_MEDORG')) +"' "
				cquery1 += " and ZZ2_FILIAL = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_FILIAL')) +"' "
				cquery1 += " and ZZ2_CLIENT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_CLIENT')) +"' "
				cquery1 += " and ZZ2_LOJA   = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_LOJA')) +"' "
				TCQuery cquery1 new alias T04


				cPcfil   := T04->ZZ2_PCFIL
				cDocPc   := T04->ZZ2_NFE1
				cSerpc   := T04->ZZ2_SER1
				cPcfor	 := T04->ZZ2_FORNEC
				cLojPc	 := T04->ZZ2_LOJFOR

				cFilPV1  := T04->ZZ2_PV01FL
				cPedido1 := T04->ZZ2_PV01
				cNFPD01  := T04->ZZ2_PV01NF
				cNFSR01  := T04->ZZ2_SER01
				cCLI01   := T04->ZZ2_CLNF01
				cLOJ01   := T04->ZZ2_LJNF01

				cFilPV2  := T04->ZZ2_PV02FL
				cPedido2 := T04->ZZ2_PV02
				cNFPD02  := T04->ZZ2_PV02NF
				cNFSR02  := T04->ZZ2_SER02
				cCLI02   := T04->ZZ2_CLNF02
				cLOJ02   := T04->ZZ2_LJNF02
				cForn02  := T04->ZZ2_FORNEC
				clOJFOR02:= T04->ZZ2_LOJFOR

				cFilPV3  := T04->ZZ2_PV03FL
				cPedido3 := T04->ZZ2_PV03
				cNFPD03  := T04->ZZ2_PV03NF
				cNFSR03  := T04->ZZ2_SER03
				cCLI03   := T04->ZZ2_CLNF03
				cLOJ03   := T04->ZZ2_LJNF03

				T04->(DBCLOSEAREA())

				// VERIFICA SE � KIT
				If alltrim(oModelZZV:GetValue('ZZV_TIPO')) == 'K'  // CASO SEJA Kit

					cquery1 := " SELECT * "
					cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 (NOLOCK) "
					cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
					cquery1 += " and D_E_L_E_T_ = '' "
					cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
					cquery1 += " and ZZ7_TIPO = 'A' "
					cquery1 += " ORDER BY ZZ7_PRODUT"
					TCQuery cquery1 new alias T03


					While T03->(!EOF())

						nContIT++					

						If !oModelZZV:IsDeleted()

							//VERIFICA SE EXISTE ITEM COM NCM DE LIVRO
							IF ALLTRIM(POSICIONE("SB1",1,XFILIAL("SB1")+T03->ZZ7_PRODUT,"B1_POSIPI")) == GETMV("SA_NCMPRO")
								nLivros += 1
							ENDIF

							// PEDIDO 01

							cquery1 := " SELECT * "
							cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
							cquery1 += " where C6_FILIAL = '" +cFilPV1+ "' "
							cquery1 += " and D_E_L_E_T_ = '' "
							cquery1 += " and C6_NUM = '"+ cPedido1 +"' "
							cquery1 += " and C6_CLI = '"+ cCLI01 +"' "
							cquery1 += " and C6_LOJA = '"+ cLOJ01 +"' "
							cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
							cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
							cquery1 += " AND C6_YCODKIT = '"+ T03->ZZ7_CODIGO +"' "

							TCQuery cquery1 new alias T05
							IF T05->(EOF())
								cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
								cquery4 += " WHERE D_E_L_E_T_ = '' "
								cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
								cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
								IF Select("T44") > 0
									T44->(DbCloseArea())
								ENDIF
								TcQuery cquery4 new Alias T44
								IF T44->(EOF())
									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.
								ELSE
									cquery1 := " SELECT * "
									cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
									cquery1 += " where C6_FILIAL = '" +cFilPV1+ "' "
									cquery1 += " and D_E_L_E_T_ = '' "
									cquery1 += " and C6_NUM = '"+ cPedido1 +"' "
									cquery1 += " and C6_CLI = '"+ cCLI01 +"' "
									cquery1 += " and C6_LOJA = '"+ cLOJ01 +"' "
									cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
									cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
									cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
									IF Select("T55") > 0
										T55->(DbCloseArea())
									ENDIF
									TcQuery cquery1 new Alias T55

									IF !T55->(EOF())

										DbSelectArea("SC6")
										DbSetOrder(1)
										IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

											nVlrItem	:=  T55->C6_PRCVEN
											cLocal      :=  T55->C6_LOCAL 
											cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
											cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
											cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
											nValUni		:=  T55->C6_PRUNIT
											nDescont    :=  T55->C6_DESCONT
											cTesPed     :=  T55->C6_TES
											T05->(DBCLOSEAREA())
											RecLock("SC6",.F.)

											SC6->C6_YCODKIT := T44->ZZ7_CODIGO
											SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

											MSUNLOCK()

										ELSE

											MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
											DisarmTransaction()
											T05->(DBCLOSEAREA())
											T03->(DBCLOSEAREA())
											Return .F.


										ENDIf

									ELSE
										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.
									ENDIF	
								ENDIF	
								T44->(DBCLOSEAREA())
								T55->(DBCLOSEAREA())
							ELSE
								//PEGA VALOR DO PEDIDO DO ENVIADO 
								nVlrItem	:=  T05->C6_PRCVEN
								cLocal      :=  T05->C6_LOCAL 
								cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
								cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
								cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)
								nValUni		:=  T05->C6_PRUNIT
								nDescont    :=  T05->C6_DESCONT
								cTesPed     :=  T05->C6_TES
								T05->(DBCLOSEAREA())
							ENDIF
							cTesPed := Posicione("SF4",1,xFilial("SF4",cFilPV1)+cTesPed,"F4_TESDV")
							IF ALLTRIM(cTesPed) == ""
								MSGINFO("TES do pedido 01 : "+ cTesPed +" ,n�o possui TES de devolu��o")		
								T03->(DbCloseArea())
								DisarmTransaction()
								Return .F.		
							ENDIF

							aLinDe01 := {}
							AADD(aLinDe01,{"D1_ITEM"       ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)                                                 ,Nil})
							AADD(aLinDe01,{"D1_COD"        ,T03->ZZ7_PRODUT   				     						                                          ,Nil})
							AADD(aLinDe01,{"D1_NFORI"     ,cNfOri         						                                                                  ,Nil})
							AADD(aLinDe01,{"D1_SERIORI"   ,cSeriOri                                                                                                 ,Nil})
							AADD(aLinDe01,{"D1_ITEMORI"   ,cItemori					                                                                              ,Nil})
							AADD(aLinDe01,{"D1_QUANT"     ,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT')                                                           ,Nil})
							AADD(aLinDe01,{"D1_VUNIT"     ,nVlrItem                                                                                                 ,Nil})
							AADD(aLinDe01,{"D1_TES"       ,cTesPed       													                                          ,Nil})
							AADD(aLinDe01,{"D1_YCODKIT"   ,ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))                                                                ,Nil})
							AADD(aLinDe01,{"D1_YPAIKIT"   ,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),"ZZ7_CODPAI"))        ,Nil})					
							AADD(aLinDe01,{"D1_TOTAL"     ,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT') * nVlrItem                  ,Nil})
							AADD(aLinDe01,{"D1_LOCAL"     ,cArmazem                                                                                                  ,Nil})
							Aadd(aItensDE01,aLinDe01)

							IF T03->ZZ7_PERDSC == 100 .AND. T03->ZZ7_CATEGO<>'P'
								nDesconto +=(( GETMV('SA_PRECDSC')*T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT'))*(T03->ZZ7_PERDSC/100))
							endif

						endif
						// PEDIDO 02

						if	cEspecie == '02' .OR. cEspecie == '03'
							cquery1 := " SELECT * "
							cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
							cquery1 += " where C6_FILIAL = '" +cFilPV2+ "' "
							cquery1 += " and D_E_L_E_T_ = '' "
							cquery1 += " and C6_NUM = '"+ cPedido2 +"' "
							cquery1 += " and C6_CLI = '"+ cCLI02 +"' "
							cquery1 += " and C6_LOJA = '"+ cLOJ02 +"' "
							cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
							cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
							cquery1 += " AND C6_YCODKIT = '"+ T03->ZZ7_CODIGO  +"' "

							TCQuery cquery1 new alias T05
							IF T05->(EOF())
								cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
								cquery4 += " WHERE D_E_L_E_T_ = '' "
								cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
								cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
								IF Select("T44") > 0
									T44->(DbCloseArea())
								ENDIF
								TcQuery cquery4 new Alias T44
								IF T44->(EOF())
									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.
								ELSE
									cquery1 := " SELECT * "
									cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
									cquery1 += " where C6_FILIAL = '" +cFilPV2+ "' "
									cquery1 += " and D_E_L_E_T_ = '' "
									cquery1 += " and C6_NUM = '"+ cPedido2 +"' "
									cquery1 += " and C6_CLI = '"+ cCLI02 +"' "
									cquery1 += " and C6_LOJA = '"+ cLOJ02 +"' "
									cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
									cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
									cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
									IF Select("T55") > 0
										T55->(DbCloseArea())
									ENDIF
									TcQuery cquery1 new Alias T55

									IF !T55->(EOF())

										DbSelectArea("SC6")
										DbSetOrder(1)
										IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

											nVlrItem	:=  T55->C6_PRCVEN
											cLocal      :=  T55->C6_LOCAL 
											cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
											cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
											cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
											nValUni		:=  T55->C6_PRUNIT
											nDescont    :=  T55->C6_DESCONT
											cTesPed     :=  T55->C6_TES
											T05->(DBCLOSEAREA())
											RecLock("SC6",.F.)

											SC6->C6_YCODKIT := T44->ZZ7_CODIGO
											SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

											MSUNLOCK()

										ELSE

											MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
											DisarmTransaction()
											T05->(DBCLOSEAREA())
											T03->(DBCLOSEAREA())
											Return .F.


										ENDIf

									ELSE
										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.
									ENDIF	
								ENDIF		
								T44->(DBCLOSEAREA())
								T55->(DBCLOSEAREA())
							ELSE
								//PEGA VALOR DO PEDIDO DO ENVIADO 
								nVlrItem	:=  T05->C6_PRCVEN
								cLocal      :=  T05->C6_LOCAL 
								cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
								cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )
								cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
								cFil02		:=	T05->C6_FILIAL
								nValUni		:=  T05->C6_PRUNIT
								nDescont    :=  T05->C6_DESCONT
								cTesPed     :=  T05->C6_TES
								T05->(DBCLOSEAREA())
							ENDIF
							//ITEM DOC DE ENTRADA  // D1_FILIAL, D1_DOC, D1_SERIE, D1_FORNECE, D1_LOJA, D1_COD, D1_ITEM, R_E_C_N_O_, D_E_L_E_T_
							cquery1 := " SELECT D1_ITEM "
							cquery1 += " FROM " + retsqlname('SD1') + " SD1 (NOLOCK) "
							cquery1 += " JOIN " + retsqlname('SC7') + " SC7 (NOLOCK) "
							cquery1 += " ON C7_NUM = D1_PEDIDO "
							cquery1 += " AND C7_FILIAL = D1_FILIAL "
							cquery1 += " AND C7_FORNECE = D1_FORNECE "
							cquery1 += " AND C7_LOJA = D1_LOJA "
							cquery1 += " AND C7_PRODUTO = D1_COD "
							cquery1 += " AND C7_ITEM = D1_ITEMPC "
							cquery1 += " WHERE  D1_FILIAL = '"+cPcfil+"' "
							cquery1 += " AND  D1_DOC = '"+cDocPc+"' "
							cquery1 += " AND  D1_SERIE = '"+cSerpc+"' "
							cquery1 += " AND  D1_FORNECE = '"+cPcfor+"' "
							cquery1 += " AND  D1_LOJA = '"+cLojPc+"' "
							cquery1 += " AND  D1_COD = '"+T03->ZZ7_PRODUT+"' "
							cquery1 += " AND  C7_YCODKIT = '"+T03->ZZ7_CODIGO+"' "

							TCQuery cquery1 new alias ITPC

							IF ITPC->(EOF())
							cquery1 := " SELECT D1_ITEM "
							cquery1 += " FROM " + retsqlname('SD1') + " SD1 (NOLOCK) "
							cquery1 += " JOIN " + retsqlname('SC7') + " SC7 (NOLOCK) "
							cquery1 += " ON C7_NUM = D1_PEDIDO "
							cquery1 += " AND C7_FILIAL = D1_FILIAL "
							cquery1 += " AND C7_FORNECE = D1_FORNECE "
							cquery1 += " AND C7_LOJA = D1_LOJA "
							cquery1 += " AND C7_PRODUTO = D1_COD "
							cquery1 += " AND C7_ITEM = D1_ITEMPC "
							cquery1 += " WHERE  D1_FILIAL = '"+cPcfil+"' "
							cquery1 += " AND  D1_DOC = '"+cDocPc+"' "
							cquery1 += " AND  D1_SERIE = '"+cSerpc+"' "
							cquery1 += " AND  D1_FORNECE = '"+cPcfor+"' "
							cquery1 += " AND  D1_LOJA = '"+cLojPc+"' "
							cquery1 += " AND  D1_COD = '"+T03->ZZ7_PRODUT+"' "
							cquery1 += " AND  C7_YCODKIT = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
							TcQuery cquery1 new Alias ITPC1
							
							IF !ITPC1->(EOF())
							cItemori	:=  ALLTRIM(ITPC1->D1_ITEM)
							ITPC1->(DBCLOSEAREA())
							ELSE
							ITPC1->(DBCLOSEAREA())
							ENDIF
							
							ELSE
							cItemori	:=  ALLTRIM(ITPC->D1_ITEM) //ALLTRIM(T05->C6_ITEM)
							
							ENDIF
							
							ITPC->(DBCLOSEAREA())




							AADD(aItens2,{})
							nTam	:= Len(aItens2)
							AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.),nil})
							AADD(aItens2[nTam],{"C6_PRODUTO",T03->ZZ7_PRODUT,nil})
							AADD(aItens2[nTam],{"C6_NFORI"	,cNfOri			,nil})
							AADD(aItens2[nTam],{"C6_SERIORI",cSeriOri		,nil})
							AADD(aItens2[nTam],{"C6_ITEMORI",STRZERO(val(cItemori), 4, 0),nil})
							AADD(aItens2[nTam],{"C6_TES"	,cTesTmp		,nil})

							if cTesPed $ GetMv("SA_TESBON")    // se for bonifica��o 
								AADD(aItens2[nTam],{"C6_YTES","518",nil}) 
							else
								AADD(aItens2[nTam],{"C6_YTES","512",nil})
							endif

							AADD(aItens2[nTam],{"C6_QTDVEN"	,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT'),nil})
							AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem	,nil})
							AADD(aItens2[nTam],{"C6_YCONTRA",ALLTRIM(oModelZZV:GetValue('ZZV_NUMERO'))	,nil})
							AADD(aItens2[nTam],{"C6_YMEDICA",ALLTRIM(oModelZZV:GetValue('ZZV_MEDICA'))	,nil})
							AADD(aItens2[nTam],{"C6_YITCONT",oModelZZV:GetValue('ZZV_ITEM')				,nil})
							AADD(aItens2[nTam],{"C6_YCODKIT",ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))	,nil})
							AADD(aItens2[nTam],{"C6_YPAIKIT",Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),"ZZ7_CODPAI")),nil})
							AADD(aItens2[nTam],{"C6_LOCAL"	,cArmazem									,nil})
							If oModelZZV:IsDeleted()
								aadd(aItens2[nTam],{"AUTDELETA","S",Nil})
							ELSE	
								aadd(aItens2[nTam],{"AUTDELETA","N",Nil})
							EndIf

							If !oModelZZV:IsDeleted()

								aLinPc02 := {}					
								aadd(aLinPc02,{"C7_PRODUTO"	,T03->ZZ7_PRODUT		                                	,nil})
								aadd(aLinPc02,{"C7_QUANT"	,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT')				,nil})
								aadd(aLinPc02,{"C7_PRECO"	,nVlrItem                                        			,nil})
								aadd(aLinPc02,{"C7_TOTAL"	,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT') * nVlrItem	,nil})
								aadd(aLinPc02,{"C7_VLDESC"	,0                                                  		,nil})
								aadd(aLinPc02,{"C7_TES"		,"400"			                                        	,nil})
								aadd(aLinPc02,{"C7_YTES"	,"014"			                                        	,nil})								
								aadd(aItensPC02,aLinPc02)
							ENDIF

							If !oModelZZV:IsDeleted()

								// PEDIDO 03
								cquery1 := " SELECT * "
								cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
								cquery1 += " where C6_FILIAL = '" +cFilPV3+ "' "
								cquery1 += " and D_E_L_E_T_ = '' "
								cquery1 += " and C6_NUM = '"+ cPedido3 +"' "
								cquery1 += " and C6_CLI = '"+ cCLI03 +"' "
								cquery1 += " and C6_LOJA = '"+ cLOJ03 +"' "
								cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
								cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
								cquery1 += " AND C6_YCODKIT = '"+ T03->ZZ7_CODIGO +"' "

								TCQuery cquery1 new alias T05
								IF T05->(EOF())
									cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
									cquery4 += " WHERE D_E_L_E_T_ = '' "
									cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
									cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
									IF Select("T44") > 0
										T44->(DbCloseArea())
									ENDIF
									TcQuery cquery4 new Alias T44
									IF T44->(EOF())
										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.
									ELSE
										cquery1 := " SELECT * "
										cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
										cquery1 += " where C6_FILIAL = '" +cFilPV3+ "' "
										cquery1 += " and D_E_L_E_T_ = '' "
										cquery1 += " and C6_NUM = '"+ cPedido3 +"' "
										cquery1 += " and C6_CLI = '"+ cCLI03 +"' "
										cquery1 += " and C6_LOJA = '"+ cLOJ03 +"' "
										cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
										cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
										cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
										IF Select("T55") > 0
											T55->(DbCloseArea())
										ENDIF
										TcQuery cquery1 new Alias T55

										IF !T55->(EOF())

											DbSelectArea("SC6")
											DbSetOrder(1)
											IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

												nVlrItem	:=  T55->C6_PRCVEN
												cLocal      :=  T55->C6_LOCAL 
												cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
												cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
												cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
												nValUni		:=  T55->C6_PRUNIT
												nDescont    :=  T55->C6_DESCONT
												cTesPed     :=  T55->C6_TES
												T05->(DBCLOSEAREA())
												RecLock("SC6",.F.)

												SC6->C6_YCODKIT := T44->ZZ7_CODIGO
												SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

												MSUNLOCK()

											ELSE

												MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
												DisarmTransaction()
												T05->(DBCLOSEAREA())
												T03->(DBCLOSEAREA())
												Return .F.


											ENDIf

										ELSE
											MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
											DisarmTransaction()
											T05->(DBCLOSEAREA())
											T03->(DBCLOSEAREA())
											Return .F.
										ENDIF	
									ENDIF	

									T44->(DBCLOSEAREA())
									T55->(DBCLOSEAREA())
								ELSE
									//PEGA VALOR DO PEDIDO DO ENVIADO 
									nVlrItem	:=  T05->C6_PRCVEN
									cLocal      :=  T05->C6_LOCAL 
									cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
									cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
									cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )
									nValUni		:=  T05->C6_PRUNIT
									nDescont    :=  T05->C6_DESCONT
									cTesPed     :=  T05->C6_TES
									T05->(DBCLOSEAREA())
								ENDIF
								cTesPed := Posicione("SF4",1,xFilial("SF4",cFilPV3)+cTesPed,"F4_TESDV")
								IF ALLTRIM(cTesPed) == ""
									MSGINFO("TES do pedido 03 : "+ cTesPed +" ,n�o possui TES de devolu��o")		
									T03->(DbCloseArea())
									DisarmTransaction()
									Return .F.		
								ENDIF

								aLinDe03 := {}
								AADD(aLinDe03,{"D1_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,Nil})
								AADD(aLinDe03,{"D1_COD"		,T03->ZZ7_PRODUT											,Nil})
								AADD(aLinDe03,{"D1_NFORI"	,cNfOri														,Nil})
								AADD(aLinDe03,{"D1_SERIORI"	,cSeriOri													,Nil})
								AADD(aLinDe03,{"D1_ITEMORI"	,cItemori								                    ,Nil})
								AADD(aLinDe03,{"D1_QUANT"	,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT')				,Nil})
								AADD(aLinDe03,{"D1_VUNIT"	,nVlrItem													,Nil})
								AADD(aLinDe03,{"D1_TES"		,cTesPed    												,Nil})
								AADD(aLinDe03,{"D1_YCODKIT"	,ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))          		,Nil})
								AADD(aLinDe03,{"D1_YPAIKIT"	,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),"ZZ7_CODPAI")),Nil})					
								AADD(aLinDe03,{"D1_LOCAL"	,cArmazem         											,Nil})
								Aadd(aItensDE03,aLinDe03)

							ENDIF
						endif
						T03->(DbSkip())

					EndDo

					T03->(DbCloseArea())
					// CASO SEJA PRODUTO AVULSO
				Else
					nContIT++

					If !oModelZZV:IsDeleted()

						//VERIFICA SE EXISTE ITEM COM NCM DE LIVRO
						IF ALLTRIM(POSICIONE("SB1",1,XFILIAL("SB1")+oModelZZV:GetValue('ZZV_PRODUT'),"B1_POSIPI")) == GETMV("SA_NCMPRO")
							nLivros += 1
						ENDIF

						// PEDIDO 01
						cquery1 := " SELECT * "
						cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
						cquery1 += " where C6_FILIAL = '" +cFilPV1+ "' "
						cquery1 += " and D_E_L_E_T_ = '' "
						cquery1 += " and C6_NUM = '"+ cPedido1 +"' "
						cquery1 += " and C6_CLI = '"+ cCLI01 +"' "
						cquery1 += " and C6_LOJA = '"+ cLOJ01 +"' "
						cquery1 += " AND C6_PRODUTO = '"+ oModelZZV:GetValue('ZZV_PRODUT') +"' "
						cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT"))+"'  "

						TCQuery cquery1 new alias T05
						IF T05->(EOF())
							cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
							cquery4 += " WHERE D_E_L_E_T_ = '' "
							cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
							cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
							IF Select("T44") > 0
								T44->(DbCloseArea())
							ENDIF
							TcQuery cquery4 new Alias T44
							IF T44->(EOF())
								MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
								DisarmTransaction()
								T05->(DBCLOSEAREA())
								T03->(DBCLOSEAREA())
								Return .F.
							ELSE
								cquery1 := " SELECT * "
								cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
								cquery1 += " where C6_FILIAL = '" +cFilPV1+ "' "
								cquery1 += " and D_E_L_E_T_ = '' "
								cquery1 += " and C6_NUM = '"+ cPedido1 +"' "
								cquery1 += " and C6_CLI = '"+ cCLI01 +"' "
								cquery1 += " and C6_LOJA = '"+ cLOJ01 +"' "
								cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
								cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
								cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
								IF Select("T55") > 0
									T55->(DbCloseArea())
								ENDIF
								TcQuery cquery1 new Alias T55

								IF !T55->(EOF())

									DbSelectArea("SC6")
									DbSetOrder(1)
									IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

										nVlrItem	:=  T55->C6_PRCVEN
										cLocal      :=  T55->C6_LOCAL 
										cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
										cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
										cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
										nValUni		:=  T55->C6_PRUNIT
										nDescont    :=  T55->C6_DESCONT
										cTesPed     :=  T55->C6_TES
										T05->(DBCLOSEAREA())
										RecLock("SC6",.F.)

										SC6->C6_YCODKIT := T44->ZZ7_CODIGO
										SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

										MSUNLOCK()

									ELSE

										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.


									ENDIf

								ELSE
									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.
								ENDIF	
							ENDIF	

							T44->(DBCLOSEAREA())
							T55->(DBCLOSEAREA())
						ELSE
							//PEGA VALOR DO PEDIDO DO ENVIADO 
							nVlrItem	:=  T05->C6_PRCVEN
							cLocal      :=  T05->C6_LOCAL 
							cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
							cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
							cFil02		:=	T05->C6_FILIAL
							cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )
							nValUni		:=  T05->C6_PRUNIT
							nDescont	:=  T05->C6_DESCONT
							cTesPed     :=  T05->C6_TES
							T05->(DBCLOSEAREA())

						ENDIF
						cTesPed := Posicione("SF4",1,xFilial("SF4",cFilPV1)+cTesPed,"F4_TESDV")
						IF ALLTRIM(cTesPed) == ""
							MSGINFO("TES do pedido 01 : "+ cTesPed +" ,n�o possui TES de devolu��o")		
							DisarmTransaction()
							Return .F.		
						ENDIF

						aLinDe01 := {}
						AADD(aLinDe01,{"D1_ITEM"      ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)		,Nil})
						AADD(aLinDe01,{"D1_COD"       ,oModelZZV:GetValue('ZZV_PRODUT')								,Nil})
						AADD(aLinDe01,{"D1_NFORI"     ,cNfOri														,Nil})
						AADD(aLinDe01,{"D1_SERIORI"   ,cSeriOri														,Nil})
						AADD(aLinDe01,{"D1_ITEMORI"   ,cItemori	  													,Nil})
						AADD(aLinDe01,{"D1_QUANT"     ,oModelZZV:GetValue('ZZV_QUANT')								,Nil})
						AADD(aLinDe01,{"D1_VUNIT"     ,nVlrItem														,Nil})
						AADD(aLinDe01,{"D1_TES"       ,cTesPed														,Nil})
						AADD(aLinDe01,{"D1_TOTAL"     ,oModelZZV:GetValue('ZZV_QUANT') * nVlrItem					,Nil})
						AADD(aLinDe01,{"D1_LOCAL"     ,cArmazem														,Nil})
						Aadd(aItensDE01,aLinDe01)

					ENDIF

					// PEDIDO 02
					if cEspecie == '02' .OR. cEspecie == '03'

						cquery1 := " SELECT * "
						cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
						cquery1 += " where C6_FILIAL = '" +cFilPV2+ "' "
						cquery1 += " and D_E_L_E_T_ = '' "
						cquery1 += " and C6_NUM = '"+ cPedido2 +"' "
						cquery1 += " and C6_CLI = '"+ cCLI02 +"' "
						cquery1 += " and C6_LOJA = '"+ cLOJ02 +"' "
						cquery1 += " AND C6_PRODUTO = '"+ oModelZZV:GetValue('ZZV_PRODUT') +"' "
						cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT"))+"'  "

						TCQuery cquery1 new alias T05
						IF T05->(EOF())
							cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
							cquery4 += " WHERE D_E_L_E_T_ = '' "
							cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
							cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
							IF Select("T44") > 0
								T44->(DbCloseArea())
							ENDIF
							TcQuery cquery4 new Alias T44
							IF T44->(EOF())
								MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
								DisarmTransaction()
								T05->(DBCLOSEAREA())
								T03->(DBCLOSEAREA())
								Return .F.
							ELSE
								cquery1 := " SELECT * "
								cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
								cquery1 += " where C6_FILIAL = '" +cFilPV2+ "' "
								cquery1 += " and D_E_L_E_T_ = '' "
								cquery1 += " and C6_NUM = '"+ cPedido2 +"' "
								cquery1 += " and C6_CLI = '"+ cCLI02 +"' "
								cquery1 += " and C6_LOJA = '"+ cLOJ02 +"' "
								cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
								cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
								cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
								IF Select("T55") > 0
									T55->(DbCloseArea())
								ENDIF
								TcQuery cquery1 new Alias T55

								IF !T55->(EOF())

									DbSelectArea("SC6")
									DbSetOrder(1)
									IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

										nVlrItem	:=  T55->C6_PRCVEN
										cLocal      :=  T55->C6_LOCAL 
										cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
										cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
										cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
										nValUni		:=  T55->C6_PRUNIT
										nDescont    :=  T55->C6_DESCONT
										cTesPed     :=  T55->C6_TES
										T05->(DBCLOSEAREA())
										RecLock("SC6",.F.)

										SC6->C6_YCODKIT := T44->ZZ7_CODIGO
										SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

										MSUNLOCK()

									ELSE

										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.


									ENDIf

								ELSE
									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.
								ENDIF	
							ENDIF	

							T44->(DBCLOSEAREA())
							T55->(DBCLOSEAREA())
						ELSE
							//PEGA VALOR DO PEDIDO DO ENVIADO 
							nVlrItem	:=  T05->C6_PRCVEN
							cLocal      :=  T05->C6_LOCAL 
							cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
							cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )
							cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
							nValUni		:=  T05->C6_PRUNIT
							cTesPed     :=  T05->C6_TES
							T05->(DBCLOSEAREA())
						ENDIF
						//ITEM DOC DE ENTRADA  // D1_FILIAL, D1_DOC, D1_SERIE, D1_FORNECE, D1_LOJA, D1_COD, D1_ITEM, R_E_C_N_O_, D_E_L_E_T_
						cquery1 := " SELECT * "
						cquery1 += " FROM " + retsqlname('SD1') + " SD1 (NOLOCK) "
						cquery1 += " WHERE  D1_FILIAL = '"+cPcfil+"' "
						cquery1 += " AND  D1_DOC = '"+cDocPc+"' "
						cquery1 += " AND  D1_SERIE = '"+cSerpc+"' "
						cquery1 += " AND  D1_FORNECE = '"+cPcfor+"' "
						cquery1 += " AND  D1_LOJA = '"+cLojPc+"' "
						cquery1 += " AND  D1_COD = '"+oModelZZV:GetValue('ZZV_PRODUT')+"' "

						TCQuery cquery1 new alias ITPC

						cItemori	:=  ALLTRIM(ITPC->D1_ITEM)

						ITPC->(DBCLOSEAREA())



						cTesPed := Posicione("SF4",1,xFilial("SF4",cFilPV2)+cTesPed,"F4_TESDV")
						IF ALLTRIM(cTesPed) == ""
							MSGINFO("TES do pedido 02 : "+ cTesPed +" ,n�o possui TES de devolu��o")		
							DisarmTransaction()
							Return .F.		
						ENDIF

						AADD(aItens2,{})
						nTam	    := Len(aItens2)		
						AADD(aItens2[nTam],{"C6_FILIAL"	,oModelZZH:GetValue('ZZH_FILIAL')								,nil})		
						AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)		,nil})
						AADD(aItens2[nTam],{"C6_PRODUTO"	,oModelZZV:GetValue('ZZV_PRODUT')							,nil})
						AADD(aItens2[nTam],{"C6_NFORI"	,cNfOri						     								,nil})
						AADD(aItens2[nTam],{"C6_SERIORI"	,cSeriOri						     						,nil})
						AADD(aItens2[nTam],{"C6_ITEMORI"	,cItemori    		     				,nil})
						AADD(aItens2[nTam],{"C6_TES"	,cTesTmp														,nil})

						if cTesPed $ GetMv("SA_TESBON")     // se for bonifica��o 
							AADD(aItens2[nTam],{"C6_YTES"	,"518",nil}) 
						else
							AADD(aItens2[nTam],{"C6_YTES"	,"512",nil})
						endif

						AADD(aItens2[nTam],{"C6_QTDVEN"	,oModelZZV:GetValue('ZZV_QUANT')								,nil})
						AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem        												,nil})
						AADD(aItens2[nTam],{"C6_YCONTRA",oModelZZV:GetValue('ZZV_NUMERO')								,nil})
						AADD(aItens2[nTam],{"C6_YMEDICA",oModelZZV:GetValue('ZZV_MEDICA')								,nil})
						AADD(aItens2[nTam],{"C6_YITCONT",oModelZZV:GetValue('ZZV_ITEM')									,nil})
						AADD(aItens2[nTam],{"C6_YCODKIT",ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))	  					,nil})
						AADD(aItens2[nTam],{"C6_YPAIKIT",Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),"ZZ7_CODPAI")),nil})
						AADD(aItens2[nTam],{"C6_LOCAL"  ,cArmazem														,nil})
						If oModelZZV:IsDeleted()
							aadd(aItens2[nTam],{"AUTDELETA","S",Nil})
						ELSE	
							aadd(aItens2[nTam],{"AUTDELETA","N",Nil})
						EndIf

						If !oModelZZV:IsDeleted()
							aLinPc02 := {}					
							aadd(aLinPc02,{"C7_PRODUTO"	,oModelZZV:GetValue('ZZV_PRODUT')                       	,nil})
							aadd(aLinPc02,{"C7_QUANT"	,oModelZZV:GetValue('ZZV_QUANT')							,nil})
							aadd(aLinPc02,{"C7_PRECO"	,nVlrItem                                        			,nil})
							aadd(aLinPc02,{"C7_TOTAL"	,oModelZZV:GetValue('ZZV_QUANT') * nVlrItem     			,nil})
							aadd(aLinPc02,{"C7_VLDESC"	,0                                                  		,nil})
							aadd(aLinPc02,{"C7_TES"		,"400"			                                        	,nil})
							aadd(aLinPc02,{"C7_YTES"	,"014"			                                        	,nil})								

							aadd(aItensPC02,aLinPc02)
						ENDIF


						If !oModelZZV:IsDeleted()
							// PEDIDO 03
							cquery1 := " SELECT * "
							cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
							cquery1 += " where C6_FILIAL = '" + cFilPV3 + "' "
							cquery1 += " and D_E_L_E_T_ = '' "
							cquery1 += " and C6_NUM = '"+ cPedido3 +"' "
							cquery1 += " and C6_CLI = '"+ cCLI03 +"' "
							cquery1 += " and C6_LOJA = '"+ cLOJ03 +"' "
							cquery1 += " AND C6_PRODUTO = '"+ oModelZZV:GetValue('ZZV_PRODUT') +"' "
							cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT"))+"'  "

							TCQuery cquery1 new alias T05
							IF T05->(EOF())
								cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
								cquery4 += " WHERE D_E_L_E_T_ = '' "
								cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
								cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
								IF Select("T44") > 0
									T44->(DbCloseArea())
								ENDIF
								TcQuery cquery4 new Alias T44
								IF T44->(EOF())
									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.
								ELSE
									cquery1 := " SELECT * "
									cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
									cquery1 += " where C6_FILIAL = '" + cFilPV3 + "' "
									cquery1 += " and D_E_L_E_T_ = '' "
									cquery1 += " and C6_NUM = '"+ cPedido3 +"' "
									cquery1 += " and C6_CLI = '"+ cCLI03 +"' "
									cquery1 += " and C6_LOJA = '"+ cLOJ03 +"' "
									cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
									cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
									cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
									IF Select("T55") > 0
										T55->(DbCloseArea())
									ENDIF
									TcQuery cquery1 new Alias T55

									IF !T55->(EOF())

										DbSelectArea("SC6")
										DbSetOrder(1)
										IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

											nVlrItem	:=  T55->C6_PRCVEN
											cLocal      :=  T55->C6_LOCAL 
											cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
											cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
											cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
											nValUni		:=  T55->C6_PRUNIT
											nDescont    :=  T55->C6_DESCONT
											cTesPed     :=  T55->C6_TES
											T05->(DBCLOSEAREA())
											RecLock("SC6",.F.)

											SC6->C6_YCODKIT := T44->ZZ7_CODIGO
											SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

											MSUNLOCK()

										ELSE

											MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
											DisarmTransaction()
											T05->(DBCLOSEAREA())
											T03->(DBCLOSEAREA())
											Return .F.


										ENDIf

									ELSE
										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.
									ENDIF	
								ENDIF	
								T44->(DBCLOSEAREA())
								T55->(DBCLOSEAREA())
							ELSE
								//PEGA VALOR DO PEDIDO DO ENVIADO 
								nVlrItem	:=  T05->C6_PRCVEN
								cLocal      :=  T05->C6_LOCAL 
								cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
								cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
								cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)
								nValUni		:=  T05->C6_PRUNIT
								nDescont    :=  T05->C6_DESCONT
								cTesPed     :=  T05->C6_TES
								T05->(DBCLOSEAREA())
							ENDIF
							cTesPed := Posicione("SF4",1,xFilial("SF4",cFilPV3)+cTesPed,"F4_TESDV")
							IF ALLTRIM(cTesPed) == ""
								MSGINFO("TES do pedido 03 : "+ cTesPed +" ,n�o possui TES de devolu��o")		
								DisarmTransaction()
								Return .F.		
							ENDIF

							aLinDe03 := {}
							AADD(aLinDe03,{"D1_ITEM"       ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)			,Nil})
							AADD(aLinDe03,{"D1_COD"        ,oModelZZV:GetValue('ZZV_PRODUT')								,Nil})
							AADD(aLinDe03,{"D1_NFORI"     ,cNfOri															,Nil})
							AADD(aLinDe03,{"D1_SERIORI"   ,cSeriOri															,Nil})
							AADD(aLinDe03,{"D1_ITEMORI"   ,cItemori															,Nil})
							AADD(aLinDe03,{"D1_QUANT"     ,oModelZZV:GetValue('ZZV_QUANT')				                    ,Nil})
							AADD(aLinDe03,{"D1_VUNIT"     ,nVlrItem															,Nil})
							AADD(aLinDe03,{"D1_TES"       ,cTesPed															,Nil})
							AADD(aLinDe03,{"D1_YCODKIT"   ,ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))						,Nil})
							AADD(aLinDe03,{"D1_YPAIKIT"   ,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),"ZZ7_CODPAI"))	,Nil})					
							AADD(aLinDe03,{"D1_LOCAL"     ,cArmazem         ,Nil})
							Aadd(aItensDE03,aLinDe03)

						ENDIF
					endif

				EndIf
			NEXT



			AADD(aCab,{"C5_NUM"			,oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PV02')		,Nil})
			AADD(aCab,{"C5_NATUREZ"		,cNaturez												,Nil})
			AADD(aCab,{"C5_YCONTRA"		,oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NUMERO')	,Nil})
			AADD(aCab,{"C5_YFILIAL"		,oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FILIAL')	,Nil})
			AADD(aCab,{"C5_YMEDICA"		,oModel:GetModel('ZZHMASTER'):GetValue('ZZH_MEDICA')	,Nil})
			AADD(aCab,{"C5_YTPVEN"		,"DEVOL"												,Nil})
			AADD(aCab,{"C5_TPEDAV"		,"39"													,Nil})

			aadd(aCabDE01,{"F1_TIPO"   ,"D" })
			aadd(aCabDE01,{"F1_FORMUL" ,"S"})
			aadd(aCabDE01,{"F1_DOC"    ,"" })
			aadd(aCabDE01,{"F1_SERIE"  ,Padr("1",GetSx3Cache("F1_SERIE","X3_TAMANHO")) })
			aadd(aCabDE01,{"F1_EMISSAO",ddatabase })
			aadd(aCabDE01,{"F1_FORNECE","" })
			aadd(aCabDE01,{"F1_LOJA"   ,"" })
			aadd(aCabDE01,{"F1_ESPECIE",Padr("SPED",GetSx3Cache("F1_ESPECIE","X3_TAMANHO")) })
			aadd(aCabDE01,{"F1_ESPECI1","CAIXA"})
			aadd(aCabDE01,{"F1_VOLUME1",oModelZZH:GetValue("ZZH_QUATVL")})			


			aCliente	:= {oModelZZH:GetValue('ZZH_CLIENT'),oModelZZH:GetValue('ZZH_LOJA'),oModelZZH:GetValue('ZZH_NOME')}

			cFilOrig := oModelZZH:GetValue('ZZH_FILIAL')

			cContrato
			cMedicao
			nFret
			cEspecie := oModelZZH:GetValue("ZZH_ESPEC")

			// Incluido por Weskley (10/06/2016), para buscar a filial do pedido de venda na nota de entrada. 
			cFilPed1 := cFilPV1


			//PEDIDO 01
			Processa({|| lRet := Devolu1(aCabDE01,aItensDE01,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilPed1,cCLI01,cLOJ01,cNFPD01,cNFSR01,nDesconto,"A") },"Alterando Pedido de Compra","Aguarde...",.F.)

			If !lRet
				DisarmTransaction()
				oModel:SetErrorMessage('ZZVDETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
				Return .F.
			EndIf

			//ZZH_ESPECIA ::::> 01=Direto / 02=Triangular / 03=Ambos     
			if	cEspecie == '02' .OR. cEspecie == '03'

				//PEDIDO 02                 aCab,aItens2,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilorig,cCLI02,cLOJ02,cNFPD02,cNFSR02,aItensPC02,cTipo 			
				Processa({|| lRet := Devolu2(aCab,aItens2,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilOrig,cCLI02,cLOJ02,cNFPD02,cNFSR02,aItensPC02,"A") },"Alteando Pedido Devolu��o ","Aguarde...",.F.)


				If !lRet
					DisarmTransaction()
					oModel:SetErrorMessage('ZZVDETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				EndIf

				//PEDIDO 03						
				Processa({|| lRet := Devolu3(aCabDE01,aItensDE03,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilOrig,cCLI03,cLOJ03,cNFPD03,cNFSR03,nDesconto,"A") },"Alterando Pedido de Compra","Aguarde...",.F.)

				If !lRet
					DisarmTransaction()
					oModel:SetErrorMessage('ZZVDETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				EndIf
			endif

			For nCont := 1 To oModelZZV:Length()
				oModelZZV:GoLine(nCont)

				if oModelZZV:IsDeleted()

					dbSelectArea("ZZ3")
					dbSetOrder(1)

					IF dbSeek(XFILIAL("ZZV")+oModelZZV:GETVALUE("ZZV_NUMERO")+oModelZZV:GETVALUE("ZZV_MEDORG")+oModelZZV:GETVALUE("ZZV_CLIENT")+oModelZZV:GETVALUE("ZZV_LOJA")+oModelZZV:GETVALUE("ZZV_PRODUT")+oModelZZV:GETVALUE("ZZV_TES"),.T.)
						RecLock("ZZ3",.F.)

						IF ZZ3->ZZ3_QTDEV0  - oModelZZV:GetValue("ZZV_QUANT") < 0 
							ZZ3->ZZ3_QTDEV0 := 0
						ELSE
							ZZ3->ZZ3_QTDEV0  -=  oModelZZV:GetValue("ZZV_QUANT")
						ENDIF
						ZZ3->(MsUnLock())
					ENDIF

					dbCloseArea("ZZ3")

				else

					dbSelectArea("ZZ3")
					dbSetOrder(1)

					IF dbSeek(XFILIAL("ZZV")+oModelZZV:GETVALUE("ZZV_NUMERO")+oModelZZV:GETVALUE("ZZV_MEDORG")+oModelZZV:GETVALUE("ZZV_CLIENT")+oModelZZV:GETVALUE("ZZV_LOJA")+oModelZZV:GETVALUE("ZZV_PRODUT")+oModelZZV:GETVALUE("ZZV_TES"),.T.)
						RecLock("ZZ3",.F.)

						ZZ3->ZZ3_QTDEV0  +=  oModelZZV:GetValue("ZZV_MOV")
						ZZ3->(MsUnLock())
					ENDIF

					dbCloseArea("ZZ3")

				endif

				oModel:SetValue("ZZVDETAIL", "ZZV_MOV" , 0)

			NEXT


			dbSelectArea("ZZV")
			dbSetOrder(1)
			IF dbSeek(XFILIAL("ZZV")+oModelZZV:GETVALUE("ZZV_NUMERO")+oModelZZV:GETVALUE("ZZV_MEDICA")+oModelZZV:GETVALUE("ZZV_CLIENT")+oModelZZV:GETVALUE("ZZV_LOJA")+oModelZZV:GETVALUE("ZZV_PRODUT")+oModelZZV:GETVALUE("ZZV_TES"),.T.)

				nItem := 0
				RecLock("ZZV",.F.)

				For nCont2:=1 To Len(aStruct)
					nItem ++
					oModel:SetValue("ZZVDETAIL", "ZZV_ITEM" , sTRZERO(nItem,3))

					ZZV->(&(aStruct[nCont2][MODEL_FIELD_IDFIELD])) := oModelZZV:GetValue(aStruct[nCont2][MODEL_FIELD_IDFIELD])
				Next

				ZZV->ZZV_FILIAL	:= oModel:Getmodel("ZZVDETAIL"):GetValue("ZZV_FILIAL")

				ZZV->(MsUnLock())
			ENDIF
			dbCloseArea("ZZV")


			dbSelectArea("ZZH")
			dbSetOrder(1)

			If DbSeek(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FILIAL')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NUMERO')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_MEDICA'))

				RecLock("ZZH",.F.)
				ZZH->ZZH_LIVRO := IIF(nLivros > 0 ,"S","N")
				ZZH->(MsUnLock())				
			endif

			dbCloseArea("ZZH")

		END TRANSACTION

		MsUnLockAll()

		msgInfo(cMSG)

		FWFormCommit(oModel)

		Return lRet
		//////////////////////////
		//						//
		//EXCLUSAO DA MEDICAO 	//
		//////////////////////////	

	elseif oModel:GetOperation() == 5

		IF oModelZZH:GetValue('ZZH_STFAT')  $ ("F |N2|N3|N4")
			MsgInfo("Medi��o j� faturada !!!")
			Return .F.
		ENDIF

		Begin Transaction

			aCliente	:= {oModelZZH:GetValue('ZZH_CLIENT'),oModelZZH:GetValue('ZZH_LOJA'),oModelZZH:GetValue('ZZH_NOME')}

			cFilOrig := oModelZZH:GetValue('ZZH_FILIAL')

			cContrato
			cMedicao
			nFret
			cEspecie := oModelZZH:GetValue("ZZH_ESPEC")

			// Incluido por Weskley (10/06/2016), para buscar a filial do pedido de venda na nota de entrada.
			cFilPed1 := cFilOrig  //cFilPV1

			//PEDIDO 01
			Processa({|| lRet := Devolu1(aCabDE01,aItensDE01,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilPed1,cCLI01,cLOJ01,cNFPD01,cNFSR01,nDesconto,"E") },"Alterando Pedido de Compra","Aguarde...",.F.)

			If !lRet
				DisarmTransaction()
				oModel:SetErrorMessage('ZZVDETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
				Return .F.
			EndIf

			//ZZH_ESPECIA ::::> 01=Direto / 02=Triangular / 03=Ambos     
			if	cEspecie == '02' .OR. cEspecie == '03'

				//PEDIDO 02				     aCab,aItens2,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilorig,cCLI02,cLOJ02,cNFPD02,cNFSR02,aItensPC02,cTipo
				Processa({|| lRet := Devolu2(aCab,aItens2,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilOrig,cCLI02,cLOJ02,cNFPD02,cNFSR02,aItensPC02,"E") },"Alteando Pedido Devolu��o ","Aguarde...",.F.)


				If !lRet
					DisarmTransaction()
					oModel:SetErrorMessage('ZZVDETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				EndIf

				//PEDIDO 03				
				Processa({|| lRet := Devolu3(aCabDE01,aItensDE03,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilOrig,cCLI03,cLOJ03,cNFPD03,cNFSR03,nDesconto,"E") },"Alterando Pedido de Compra","Aguarde...",.F.)

				If !lRet
					DisarmTransaction()
					oModel:SetErrorMessage('ZZVDETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				EndIf
			endif

			For nCont := 1 To oModelZZV:Length()
				oModelZZV:GoLine(nCont)

				dbSelectArea("ZZ3")
				dbSetOrder(1)

				IF dbSeek(XFILIAL("ZZV")+oModelZZV:GETVALUE("ZZV_NUMERO")+oModelZZV:GETVALUE("ZZV_MEDORG")+oModelZZV:GETVALUE("ZZV_CLIENT")+oModelZZV:GETVALUE("ZZV_LOJA")+oModelZZV:GETVALUE("ZZV_PRODUT")+oModelZZV:GETVALUE("ZZV_TES"),.T.)
					RecLock("ZZ3",.F.)

					IF ZZ3->ZZ3_QTDEV0  - oModelZZV:GetValue("ZZV_QUANT") < 0 
						ZZ3->ZZ3_QTDEV0 := 0
					ELSE 
						ZZ3->ZZ3_QTDEV0  -=  oModelZZV:GetValue("ZZV_QUANT")
					ENDIF
					ZZ3->(MsUnLock())
				else
					DisarmTransaction()
					oModel:SetErrorMessage('ZZVDETAIL',,,,"ERRO", 'N�o foi possivel atualizar saldo da medi��o de devolu��o!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				ENDIF

				dbCloseArea("ZZ3")

			NEXT

			cUpd:="UPDATE "+RetSqlName("ZZH")+" SET D_E_L_E_T_ ='*' WHERE "+;
			" ZZH_FILIAL = '"+oModel:GETMODEL('ZZHMASTER'):GetValue('ZZH_FILIAL')+"' AND "+;
			" ZZH_NUMERO = '"+oModel:GETMODEL('ZZHMASTER'):GetValue('ZZH_NUMERO')+"' AND "+; 
			" ZZH_MEDICA = '"+oModel:GETMODEL('ZZHMASTER'):GetValue('ZZH_MEDICA')+"' AND "+;
			" ZZH_CLIENT = '"+oModel:GETMODEL('ZZHMASTER'):GetValue('ZZH_CLIENT')+"' AND "+;
			" ZZH_LOJA   = '"+oModel:GETMODEL('ZZHMASTER'):GetValue('ZZH_LOJA')+"' "  
			TCSqlExec(cUpd)
			TcRefresh(RetSqlName("ZZH"))


		END TRANSACTION

		MsUnLockAll()

		msgInfo(cMSG)


		FWFormCommit(oModel)

		Return lRet


		//////////////////////////
		//						//
		//INCLUS�O DA MEDICAO 	//
		//////////////////////////
	elseif oModel:GetOperation() == 3 
		// VALIDA��ES ANTES DE CONFIRMAR A GERA��O DA MEDICAO
		/////////////////////////////////////////////////////

		WHILE nOpca == 0

			DEFINE MSDIALOG oDlg TITLE "TIPO DE MEDI��O" FROM 000, 000  TO 150, 300 COLORS 0, 16777215 PIXEL

			@ 012, 071 MSCOMBOBOX oComboBo1 VAR nComboBo1 ITEMS {"DIRETO","TRIANGULAR"} SIZE 072, 010 OF oDlg COLORS 0, 16777215 PIXEL
			@ 012, 005 SAY oSay1 PROMPT "Tipo de Medi��o:" SIZE 049, 010 OF oDlg COLORS 0, 16777215 PIXEL
			@ 039, 075 BUTTON oButton1 PROMPT "Ok" SIZE 037, 012 OF oDlg ACTION  (nOpca:=1,oDlg:end()) PIXEL
			@ 039, 115 BUTTON oButton2 PROMPT "Cancelar" SIZE 037, 012 OF oDlg ACTION  (nOpca:=2,oDlg:end()) PIXEL

			ACTIVATE MSDIALOG oDlg

		ENDDO
		IF nOpca == 2
			Return .F.
		ENDIF
		IF  nComboBo1 == "DIRETO"
			oModel:LoadValue("ZZHMASTER", "ZZH_ESPEC", "01")
			cEspecie := "01"
		ELSEIF nComboBo1 == "TRIANGULAR"
			oModel:LoadValue("ZZHMASTER", "ZZH_ESPEC", "02")
			cEspecie := "02"
		ENDIF


		IF oModelZZH:GetValue('ZZH_ESPEC') == "03" .AND. LEN( ALLTRIM( oModelZZH:GetValue('ZZH_OBS') ) ) < 15 
			MsgInfo("Informar uma OBS com no m�nimo 15 caracteres! ")
			RETURN .F.
		ENDIF 


		IF oModelZZH:GetValue('ZZH_ORIGEM') == "01" .AND. LEN( ALLTRIM( oModelZZH:GetValue('ZZH_PEDDEV') ) ) == 0 
			MsgInfo("Informar o pedido de devolu��o! ")
			RETURN .F.
		ENDIF 

		////////////////////////////////////////////////////////////////////////////////////////////////////
		//ESCLOHA DO TIPO DE MEDI��O INDEPENDENTE DO CADASTRO DE CLITEN (CONTRIBUINTE OU NAO CONTRIBUINTE)//
		////////////////////////////////////////////////////////////////////////////////////////////////////

		IF oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_CONTRI") == "1"
			IF MsgYesNo("Deseja fazer medicao como cliente CONTRIBUINTE ?","Aviso")
				cTIPOMED := "CONTRI"
			ELSE
				IF MsgYesNo("Confirma a gera��o da medi��o como cliente N�O CONTRIBUINTE ?","Aviso")
					cTIPOMED := "NCONTRI"
				ELSE
					RETURN .F.
				endif				
			endif
		else
			cTIPOMED := "NCONTRI"
		endif

		////////////////////////////////////////////////////////////////////////////////////////////////////

		Begin Transaction

			dbSelectArea("ZZH")
			RECLOCK("ZZH", .T.)

			ZZH->ZZH_FILIAL	  := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_FILIAL") //cFilant //xFilial("ZZ3")
			ZZH->ZZH_NUMERO	  := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_NUMERO") 
			ZZH->ZZH_CLIENT   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_CLIENT")
			ZZH->ZZH_LOJA     := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_LOJA")
			ZZH->ZZH_NOME     := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_NOME")
			ZZH->ZZH_MEDICA   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_MEDICA")		
			ZZH->ZZH_DTGER    := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_DTGER")
			ZZH->ZZH_HRGER	  := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_HRGER")
			ZZH->ZZH_STATUS   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_STATUS")
			ZZH->ZZH_DTSOLI   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_DTSOLI")
			ZZH->ZZH_ANOCOM   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_ANOCOM")
			ZZH->ZZH_ENVIO    := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_ENVIO")
			ZZH->ZZH_OBS      := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_OBS")
			ZZH->ZZH_TPDEV    := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_TPDEV")
			ZZH->ZZH_ESPEC    := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_ESPEC")
			ZZH->ZZH_COLETA   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_COLETA")
			ZZH->ZZH_TRANSP   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_TRANSP")
			ZZH->ZZH_NOMTRA   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_NOMTRA")
			ZZH->ZZH_FRETE    := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_FRETE")
			ZZH->ZZH_TES      := oModel:Getmodel("ZZVDETAIL"):GetValue("ZZV_TES")
			ZZH->ZZH_TPMED    := IIF(cTIPOMED == "CONTRI","1",iif(cTIPOMED == "NCONTRI","2",))
			ZZH->ZZH_USERGR   := retcodusr()
			ZZH->ZZH_CONTRI   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_CONTRI")
			ZZH->ZZH_PEDDEV   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_PEDDEV")
			ZZH->ZZH_QUATVL   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_QUATVL")
			ZZH->ZZH_STATUS   := "BL"
			ZZH->ZZH_STFAT    := IIF(cTIPOMED == "CONTRI","N1",iif(cTIPOMED == "NCONTRI","L",))
			ZZH->ZZH_STACOM   := GETMV("SA_STINDEV")
			ZZH->ZZH_ORIGEM   := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_ORIGEM")

			MSUNLOCK()


			For nCont := 1 To oModelZZV:Length()
				oModelZZV:GoLine(nCont)
				If oModelZZV:IsDeleted()
					Loop
				EndIf
				If oModelZZV:GetValue("ZZV_QUANT") == 0 .OR.  ( oModelZZH:GetValue("ZZH_COLETA") == "01" .AND. oModelZZH:GetValue("ZZH_QUATVL") == 0)
					Loop
				EndIf
				nItem ++
				oModel:SetValue("ZZVDETAIL", "ZZV_ITEM" , sTRZERO(nItem,3))
				dbSelectArea("ZZV")
				RecLock("ZZV",.T.)
				For nCont2:=1 To Len(aStruct)
					ZZV->(&(aStruct[nCont2][MODEL_FIELD_IDFIELD])) := oModelZZV:GetValue(aStruct[nCont2][MODEL_FIELD_IDFIELD])
				Next


				ZZV->ZZV_FILIAL	:= oModel:Getmodel("ZZVDETAIL"):GetValue("ZZV_FILIAL")

				ZZV->(MsUnLock())
				dbCloseArea("ZZV")
				ZZV->(ConfirmSX8())

				// BUCA DE PEDIDOS E NOTAS DO ITEM DA MEDI��O
				cquery1 := " SELECT * "
				cquery1 += " from " + retsqlname('ZZ2') + " ZZ2 (NOLOCK) "
				cquery1 += " where "
				cquery1 += " D_E_L_E_T_ = '' "
				cquery1 += " and ZZ2_NUMERO = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_NUMERO')) +"' "
				cquery1 += " and ZZ2_MEDICA = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_MEDORG')) +"' "
				cquery1 += " and ZZ2_FILIAL = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_FILIAL')) +"' "
				cquery1 += " and ZZ2_CLIENT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_CLIENT')) +"' "
				cquery1 += " and ZZ2_LOJA   = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_LOJA')) +"' "
				TCQuery cquery1 new alias T04

				cPcfil   := T04->ZZ2_PCFIL
				cDocPc   := T04->ZZ2_NFE1
				cSerpc   := T04->ZZ2_SER1
				cPcfor	 := T04->ZZ2_FORNEC
				cLojPc	 := T04->ZZ2_LOJFOR

				cFilPV1  := T04->ZZ2_PV01FL
				cPedido1 := T04->ZZ2_PV01
				cNFPD01  := T04->ZZ2_PV01NF
				cNFSR01  := T04->ZZ2_SER01
				cCLI01   := T04->ZZ2_CLNF01
				cLOJ01   := T04->ZZ2_LJNF01

				cFilPV2  := T04->ZZ2_PV02FL
				cPedido2 := T04->ZZ2_PV02
				cNFPD02  := T04->ZZ2_PV02NF
				cNFSR02  := T04->ZZ2_SER02
				cCLI02   := T04->ZZ2_CLNF02
				cLOJ02   := T04->ZZ2_LJNF02
				cForn02  := T04->ZZ2_FORNEC
				clOJFOR02:= T04->ZZ2_LOJFOR

				cFilPV3  := T04->ZZ2_PV03FL
				cPedido3 := T04->ZZ2_PV03
				cNFPD03  := T04->ZZ2_PV03NF
				cNFSR03  := T04->ZZ2_SER03
				cCLI03   := T04->ZZ2_CLNF03
				cLOJ03   := T04->ZZ2_LJNF03

				T04->(DBCLOSEAREA())

				If alltrim(oModelZZV:GetValue('ZZV_TIPO')) == 'K'  // CASO SEJA Kit

					cquery1 := " SELECT * " // ZZ7_CODPAI, ZZ7_PRODUT,ZZ7_QUANT,ZZ7_PERDSC, ZZ7_CATEGO , ZZ7_SUPLEM "
					cquery1 += " from " + retsqlname('ZZ7') + " ZZ7 (NOLOCK) "
					cquery1 += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
					cquery1 += " and D_E_L_E_T_ = '' "
					cquery1 += " and ZZ7_CODPAI = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
					cquery1 += " and ZZ7_TIPO = 'A' "
					cquery1 += " ORDER BY ZZ7_PRODUT"
					TCQuery cquery1 new alias T03

					While T03->(!EOF())

						nContIT++	

						//VERIFICA SE EXISTE ITEM COM NCM DE LIVRO
						IF ALLTRIM(POSICIONE("SB1",1,XFILIAL("SB1")+T03->ZZ7_PRODUT,"B1_POSIPI")) == GETMV("SA_NCMPRO")
							nLivros += 1
						ENDIF				

						// PEDIDO 01


						cquery1 := " SELECT * "
						cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
						cquery1 += " where C6_FILIAL = '" +cFilPV1+ "' "
						cquery1 += " and D_E_L_E_T_ = '' "
						cquery1 += " and C6_NUM = '"+ cPedido1 +"' "
						cquery1 += " and C6_CLI = '"+ cCLI01 +"' "
						cquery1 += " and C6_LOJA = '"+ cLOJ01 +"' "
						cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
						cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
						cquery1 += " AND C6_YCODKIT = '"+ T03->ZZ7_CODIGO +"' "

						TCQuery cquery1 new alias T05
						IF T05->(EOF())
							cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
							cquery4 += " WHERE D_E_L_E_T_ = '' "
							cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
							cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
							IF Select("T44") > 0
								T44->(DbCloseArea())
							ENDIF
							TcQuery cquery4 new Alias T44
							IF T44->(EOF())
								MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
								DisarmTransaction()
								T05->(DBCLOSEAREA())
								T03->(DBCLOSEAREA())
								Return .F.
							ELSE
								cquery1 := " SELECT * "
								cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
								cquery1 += " where C6_FILIAL = '" +cFilPV1+ "' "
								cquery1 += " and D_E_L_E_T_ = '' "
								cquery1 += " and C6_NUM = '"+ cPedido1 +"' "
								cquery1 += " and C6_CLI = '"+ cCLI01 +"' "
								cquery1 += " and C6_LOJA = '"+ cLOJ01 +"' "
								cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
								cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
								cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
								IF Select("T55") > 0
									T55->(DbCloseArea())
								ENDIF
								TcQuery cquery1 new Alias T55

								IF !T55->(EOF())

									DbSelectArea("SC6")
									DbSetOrder(1)
									IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

										nVlrItem	:=  T55->C6_PRCVEN
										cLocal      :=  T55->C6_LOCAL 
										cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
										cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
										cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
										nValUni		:=  T55->C6_PRUNIT
										nDescont    :=  T55->C6_DESCONT
										cTesPed     :=  T55->C6_TES
										T05->(DBCLOSEAREA())

										RecLock("SC6",.F.)

										SC6->C6_YCODKIT := T44->ZZ7_CODIGO
										SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

										MSUNLOCK()

									ELSE

										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.


									ENDIf

								ELSE
									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.
								ENDIF	
							ENDIF	
							T44->(DBCLOSEAREA())
							T55->(DBCLOSEAREA())
						ELSE
							//PEGA VALOR DO PEDIDO DO ENVIADO 
							nVlrItem	:=  T05->C6_PRCVEN
							cLocal      :=  T05->C6_LOCAL 
							cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
							cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
							cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
							nValUni		:=  T05->C6_PRUNIT
							nDescont    :=  T05->C6_DESCONT
							cTesPed     :=  T05->C6_TES
							T05->(DBCLOSEAREA())
						ENDIF
						cTesPed := Posicione("SF4",1,xFilial("SF4",cFilPV1)+cTesPed,"F4_TESDV")
						IF ALLTRIM(cTesPed) == ""
							MSGINFO("TES do pedido 01 : "+ cTesPed +" ,n�o possui TES de devolu��o")		
							T03->(DbCloseArea())
							DisarmTransaction()
							Return .F.		
						ENDIF

						aLinDe01 := {}
						AADD(aLinDe01,{"D1_ITEM"       ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.),Nil})
						AADD(aLinDe01,{"D1_COD"        ,T03->ZZ7_PRODUT,Nil})
						AADD(aLinDe01,{"D1_NFORI"     ,cNfOri,Nil})
						AADD(aLinDe01,{"D1_SERIORI"   ,cSeriOri,Nil})
						AADD(aLinDe01,{"D1_ITEMORI"   ,cItemori,Nil})
						AADD(aLinDe01,{"D1_QUANT"     ,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT'),Nil})
						AADD(aLinDe01,{"D1_VUNIT"     ,nVlrItem,Nil})
						AADD(aLinDe01,{"D1_TES"       ,cTesPed,Nil})
						AADD(aLinDe01,{"D1_YCODKIT"   ,ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),Nil})
						AADD(aLinDe01,{"D1_YPAIKIT"   ,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),"ZZ7_CODPAI")),Nil})					
						IF oModelZZH:GetValue('ZZH_ESPEC') == "01" .AND. cTIPOMED == "NCONTRI"
							AADD(aLinDe01,{"D1_LOCAL"     ,cArmTran,Nil})
						ELSE
							AADD(aLinDe01,{"D1_LOCAL"     ,cArmazem,Nil})
						ENDIF
						Aadd(aItensDE01,aLinDe01)

						IF T03->ZZ7_PERDSC == 100 .AND. T03->ZZ7_CATEGO<>'P'
							nDesconto +=(( GETMV('SA_PRECDSC')*T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT'))*(T03->ZZ7_PERDSC/100))
						endif

						// PEDIDO 02

						if	cEspecie == '02' .OR. cEspecie == '03'
							cquery1 := " SELECT * "
							cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
							cquery1 += " where C6_FILIAL = '" +cFilPV2+ "' "
							cquery1 += " and D_E_L_E_T_ = '' "
							cquery1 += " and C6_NUM = '"+ cPedido2 +"' "
							cquery1 += " and C6_CLI = '"+ cCLI02 +"' "
							cquery1 += " and C6_LOJA = '"+ cLOJ02 +"' "
							cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
							cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
							cquery1 += " AND C6_YCODKIT = '"+ T03->ZZ7_CODIGO +"' "

							TCQuery cquery1 new alias T05
							IF T05->(EOF())
								cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
								cquery4 += " WHERE D_E_L_E_T_ = '' "
								cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
								cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
								IF Select("T44") > 0
									T44->(DbCloseArea())
								ENDIF
								TcQuery cquery4 new Alias T44
								IF T44->(EOF())
									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.
								ELSE
									cquery1 := " SELECT * "
									cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
									cquery1 += " where C6_FILIAL = '" +cFilPV2+ "' "
									cquery1 += " and D_E_L_E_T_ = '' "
									cquery1 += " and C6_NUM = '"+ cPedido2 +"' "
									cquery1 += " and C6_CLI = '"+ cCLI02 +"' "
									cquery1 += " and C6_LOJA = '"+ cLOJ02 +"' "
									cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
									cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
									cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
									IF Select("T55") > 0
										T55->(DbCloseArea())
									ENDIF
									TcQuery cquery1 new Alias T55

									IF !T55->(EOF())

										DbSelectArea("SC6")
										DbSetOrder(1)
										IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

											nVlrItem	:=  T55->C6_PRCVEN
											cLocal      :=  T55->C6_LOCAL 
											cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
											cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
											cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
											nValUni		:=  T55->C6_PRUNIT
											nDescont    :=  T55->C6_DESCONT
											cTesPed     :=  T55->C6_TES
											T05->(DBCLOSEAREA())
											RecLock("SC6",.F.)

											SC6->C6_YCODKIT := T44->ZZ7_CODIGO
											SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

											MSUNLOCK()

										ELSE

											MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
											DisarmTransaction()
											T05->(DBCLOSEAREA())
											T03->(DBCLOSEAREA())
											Return .F.


										ENDIf

									ELSE
										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.
									ENDIF	
								ENDIF	

								T44->(DBCLOSEAREA())
								T55->(DBCLOSEAREA())
							ELSE
								//PEGA VALOR DO PEDIDO DO ENVIADO 
								nVlrItem	:=  T05->C6_PRCVEN
								cLocal      :=  T05->C6_LOCAL 
								cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
								cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )
								cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
								cFil02		:=	T05->C6_FILIAL
								nValUni		:=  T05->C6_PRUNIT
								nDescont    :=  T05->C6_DESCONT
								cTesPed     :=  T05->C6_TES
								T05->(DBCLOSEAREA())
							ENDIF

							//ITEM DOC DE ENTRADA  // D1_FILIAL, D1_DOC, D1_SERIE, D1_FORNECE, D1_LOJA, D1_COD, D1_ITEM, R_E_C_N_O_, D_E_L_E_T_
							cquery1 := " SELECT D1_ITEM "
							cquery1 += " FROM " + retsqlname('SD1') + " SD1 (NOLOCK) "
							cquery1 += " JOIN " + retsqlname('SC7') + " SC7 (NOLOCK) "
							cquery1 += " ON C7_NUM = D1_PEDIDO "
							cquery1 += " AND C7_FILIAL = D1_FILIAL "
							cquery1 += " AND C7_FORNECE = D1_FORNECE "
							cquery1 += " AND C7_LOJA = D1_LOJA "
							cquery1 += " AND C7_PRODUTO = D1_COD "
							cquery1 += " AND C7_ITEM = D1_ITEMPC "
							cquery1 += " WHERE  D1_FILIAL = '"+cPcfil+"' "
							cquery1 += " AND  D1_DOC = '"+cDocPc+"' "
							cquery1 += " AND  D1_SERIE = '"+cSerpc+"' "
							cquery1 += " AND  D1_FORNECE = '"+cPcfor+"' "
							cquery1 += " AND  D1_LOJA = '"+cLojPc+"' "
							cquery1 += " AND  D1_COD = '"+T03->ZZ7_PRODUT+"' "
							cquery1 += " AND  C7_YCODKIT = '"+T03->ZZ7_CODIGO+"' "
							TCQuery cquery1 new alias ITPC
								
							IF ITPC->(EOF())
							cquery1 := " SELECT D1_ITEM "
							cquery1 += " FROM " + retsqlname('SD1') + " SD1 (NOLOCK) "
							cquery1 += " JOIN " + retsqlname('SC7') + " SC7 (NOLOCK) "
							cquery1 += " ON C7_NUM = D1_PEDIDO "
							cquery1 += " AND C7_FILIAL = D1_FILIAL "
							cquery1 += " AND C7_FORNECE = D1_FORNECE "
							cquery1 += " AND C7_LOJA = D1_LOJA "
							cquery1 += " AND C7_PRODUTO = D1_COD "
							cquery1 += " AND C7_ITEM = D1_ITEMPC "
							cquery1 += " WHERE  D1_FILIAL = '"+cPcfil+"' "
							cquery1 += " AND  D1_DOC = '"+cDocPc+"' "
							cquery1 += " AND  D1_SERIE = '"+cSerpc+"' "
							cquery1 += " AND  D1_FORNECE = '"+cPcfor+"' "
							cquery1 += " AND  D1_LOJA = '"+cLojPc+"' "
							cquery1 += " AND  D1_COD = '"+T03->ZZ7_PRODUT+"' "
							cquery1 += " AND  C7_YCODKIT = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
							TcQuery cquery1 new Alias ITPC1
							
							IF !ITPC1->(EOF())
							cItemori	:=  ALLTRIM(ITPC1->D1_ITEM)
							ITPC1->(DBCLOSEAREA())
							ELSE
							ITPC1->(DBCLOSEAREA())
							ENDIF
							
							ELSE
							cItemori	:=  ALLTRIM(ITPC->D1_ITEM) //ALLTRIM(T05->C6_ITEM)
							
							ENDIF
							
							ITPC->(DBCLOSEAREA())
							




							AADD(aItens2,{})
							nTam	:= Len(aItens2)
							AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.),nil})
							AADD(aItens2[nTam],{"C6_PRODUTO",T03->ZZ7_PRODUT										,nil})
							AADD(aItens2[nTam],{"C6_NFORI"	,cNfOri						     						,nil})
							AADD(aItens2[nTam],{"C6_SERIORI",cSeriOri						     					,nil})
							AADD(aItens2[nTam],{"C6_ITEMORI",cItemori   		     								,nil})
							AADD(aItens2[nTam],{"C6_TES"	,cTesTmp												,nil})

							if cTesPed $ GetMv("SA_TESBON")    // se for bonifica��o 
								AADD(aItens2[nTam],{"C6_YTES"	,"518"												,nil}) 
							else
								AADD(aItens2[nTam],{"C6_YTES"	,"512"												,nil})
							endif

							AADD(aItens2[nTam],{"C6_QTDVEN"	,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT')			,nil})
							AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem												,nil})

							//tes
							AADD(aItens2[nTam],{"C6_YCONTRA",ALLTRIM(oModelZZV:GetValue('ZZV_NUMERO'))				,nil})
							AADD(aItens2[nTam],{"C6_YMEDICA",ALLTRIM(oModelZZV:GetValue('ZZV_MEDICA')) 				,nil})
							AADD(aItens2[nTam],{"C6_YITCONT",oModelZZV:GetValue('ZZV_ITEM')							,nil})
							AADD(aItens2[nTam],{"C6_YCODKIT",ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))	  			,nil})
							AADD(aItens2[nTam],{"C6_YPAIKIT",Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),"ZZ7_CODPAI"))					,nil})
							AADD(aItens2[nTam],{"C6_LOCAL"	,cArmazem					                			,nil})

							aLinPc02 := {}					
							aadd(aLinPc02,{"C7_PRODUTO"	,T03->ZZ7_PRODUT		                                			,nil})
							aadd(aLinPc02,{"C7_QUANT"	,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT')						,nil})
							aadd(aLinPc02,{"C7_PRECO"	,nVlrItem                                        					,nil})
							aadd(aLinPc02,{"C7_TOTAL"	,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT') * nVlrItem   		,nil})
							aadd(aLinPc02,{"C7_VLDESC"	,0                                                  				,nil})
							aadd(aLinPc02,{"C7_TES"		,"400"			                                        			,nil})
							aadd(aLinPc02,{"C7_YTES"	,"014"			                                        			,nil})								

							aadd(aItensPC02,aLinPc02)
						endif

						// PEDIDO 03
						if	cEspecie == '02' .OR. cEspecie == '03'
							cquery1 := " SELECT * "
							cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
							cquery1 += " where C6_FILIAL = '" +cFilPV3+ "' "
							cquery1 += " and D_E_L_E_T_ = '' "
							cquery1 += " and C6_NUM = '"+ cPedido3 +"' "
							cquery1 += " and C6_CLI = '"+ cCLI03 +"' "
							cquery1 += " and C6_LOJA = '"+ cLOJ03 +"' "
							cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
							cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
							cquery1 += " AND C6_YCODKIT = '"+ T03->ZZ7_CODIGO +"' "

							TCQuery cquery1 new alias T05
							IF T05->(EOF())
								cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
								cquery4 += " WHERE D_E_L_E_T_ = '' "
								cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
								cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
								IF Select("T44") > 0
									T44->(DbCloseArea())
								ENDIF
								TcQuery cquery4 new Alias T44
								IF T44->(EOF())
									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.
								ELSE
									cquery1 := " SELECT * "
									cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
									cquery1 += " where C6_FILIAL = '" +cFilPV3+ "' "
									cquery1 += " and D_E_L_E_T_ = '' "
									cquery1 += " and C6_NUM = '"+ cPedido3 +"' "
									cquery1 += " and C6_CLI = '"+ cCLI03 +"' "
									cquery1 += " and C6_LOJA = '"+ cLOJ03 +"' "
									cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
									cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
									cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
									IF Select("T55") > 0
										T55->(DbCloseArea())
									ENDIF
									TcQuery cquery1 new Alias T55

									IF !T55->(EOF())

										DbSelectArea("SC6")
										DbSetOrder(1)
										IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

											nVlrItem	:=  T55->C6_PRCVEN
											cLocal      :=  T55->C6_LOCAL 
											cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
											cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
											cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
											nValUni		:=  T55->C6_PRUNIT
											nDescont    :=  T55->C6_DESCONT
											cTesPed     :=  T55->C6_TES
											T05->(DBCLOSEAREA())
											RecLock("SC6",.F.)

											SC6->C6_YCODKIT := T44->ZZ7_CODIGO
											SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

											MSUNLOCK()

										ELSE

											MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
											DisarmTransaction()
											T05->(DBCLOSEAREA())
											T03->(DBCLOSEAREA())
											Return .F.


										ENDIf

									ELSE
										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.
									ENDIF	
								endif	
								T44->(DBCLOSEAREA())
								T55->(DBCLOSEAREA())
							ELSE
								//PEGA VALOR DO PEDIDO DO ENVIADO 
								nVlrItem	:=  T05->C6_PRCVEN
								cLocal      :=  T05->C6_LOCAL 
								cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
								cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
								cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)
								nValUni		:=  T05->C6_PRUNIT
								nDescont    :=  T05->C6_DESCONT
								cTesPed     :=  T05->C6_TES
								T05->(DBCLOSEAREA())
							ENDIF
							cTesPed := Posicione("SF4",1,xFilial("SF4",cFilPV3)+cTesPed,"F4_TESDV")
							IF ALLTRIM(cTesPed) == ""
								MSGINFO("TES do pedido 03 : "+ cTesPed +" ,n�o possui TES de devolu��o")		
								T03->(DbCloseArea())
								DisarmTransaction()
								Return .F.		
							ENDIF

							aLinDe03 := {}
							AADD(aLinDe03,{"D1_ITEM"       ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)			,Nil})
							AADD(aLinDe03,{"D1_COD"        ,T03->ZZ7_PRODUT													,Nil})
							AADD(aLinDe03,{"D1_NFORI"     ,cNfOri															,Nil})
							AADD(aLinDe03,{"D1_SERIORI"   ,cSeriOri															,Nil})
							AADD(aLinDe03,{"D1_ITEMORI"   ,cItemori															,Nil})
							AADD(aLinDe03,{"D1_QUANT"     ,T03->ZZ7_QUANT*oModelZZV:GetValue('ZZV_QUANT')					,Nil})
							AADD(aLinDe03,{"D1_VUNIT"     ,nVlrItem															,Nil})
							AADD(aLinDe03,{"D1_TES"       ,cTesPed															,Nil})
							AADD(aLinDe03,{"D1_YCODKIT"   ,ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))						,Nil})
							AADD(aLinDe03,{"D1_YPAIKIT"   ,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),"ZZ7_CODPAI")),Nil})					
							AADD(aLinDe03,{"D1_LOCAL"     ,cArmazem         												,Nil})
							Aadd(aItensDE03,aLinDe03)
						endif
						T03->(DbSkip())
					EndDo

					T03->(DbCloseArea())
				Else
					nContIT++

					//VERIFICA SE EXISTE ITEM COM NCM DE LIVRO
					IF ALLTRIM(POSICIONE("SB1",1,XFILIAL("SB1")+oModelZZV:GetValue('ZZV_PRODUT'),"B1_POSIPI")) == GETMV("SA_NCMPRO")
						nLivros += 1
					ENDIF			

					// PEDIDO 01
					cquery1 := " SELECT * "
					cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
					cquery1 += " where C6_FILIAL = '" +cFilPV1+ "' "
					cquery1 += " and D_E_L_E_T_ = '' "
					cquery1 += " and C6_NUM = '"+ cPedido1 +"' "
					cquery1 += " and C6_CLI = '"+ cCLI01 +"' "
					cquery1 += " and C6_LOJA = '"+ cLOJ01 +"' "
					cquery1 += " AND C6_PRODUTO = '"+ oModelZZV:GetValue('ZZV_PRODUT') +"' "
					cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT"))+"'  "

					TCQuery cquery1 new alias T05
					IF T05->(EOF())
						cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
						cquery4 += " WHERE D_E_L_E_T_ = '' "
						cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
						cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
						IF Select("T44") > 0
							T44->(DbCloseArea())
						ENDIF
						TcQuery cquery4 new Alias T44
						IF T44->(EOF())
							MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
							DisarmTransaction()
							T05->(DBCLOSEAREA())
							T03->(DBCLOSEAREA())
							Return .F.
						ELSE
							cquery1 := " SELECT * "
							cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
							cquery1 += " where C6_FILIAL = '" +cFilPV1+ "' "
							cquery1 += " and D_E_L_E_T_ = '' "
							cquery1 += " and C6_NUM = '"+ cPedido1 +"' "
							cquery1 += " and C6_CLI = '"+ cCLI01 +"' "
							cquery1 += " and C6_LOJA = '"+ cLOJ01 +"' "
							cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
							cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
							cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
							IF Select("T55") > 0
								T55->(DbCloseArea())
							ENDIF
							TcQuery cquery1 new Alias T55

							IF !T55->(EOF())

								DbSelectArea("SC6")
								DbSetOrder(1)
								IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

									nVlrItem	:=  T55->C6_PRCVEN
									cLocal      :=  T55->C6_LOCAL 
									cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
									cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
									cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
									nValUni		:=  T55->C6_PRUNIT
									nDescont    :=  T55->C6_DESCONT
									cTesPed     :=  T55->C6_TES
									T05->(DBCLOSEAREA())
									RecLock("SC6",.F.)

									SC6->C6_YCODKIT := T44->ZZ7_CODIGO
									SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

									MSUNLOCK()

								ELSE

									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.


								ENDIf

							ELSE
								MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
								DisarmTransaction()
								T05->(DBCLOSEAREA())
								T03->(DBCLOSEAREA())
								Return .F.
							ENDIF	
						endif	

						T44->(DBCLOSEAREA())
						T55->(DBCLOSEAREA())
					ELSE
						//PEGA VALOR DO PEDIDO DO ENVIADO 
						nVlrItem	:=  T05->C6_PRCVEN
						cLocal      :=  T05->C6_LOCAL 
						cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
						cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
						cFil02		:=	T05->C6_FILIAL
						cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)
						nValUni		:=  T05->C6_PRUNIT
						nDescont	:=  T05->C6_DESCONT
						cTesPed     :=  T05->C6_TES
						T05->(DBCLOSEAREA())

					ENDIF
					cTesPed := Posicione("SF4",1,xFilial("SF4",cFilPV1)+cTesPed,"F4_TESDV")
					IF ALLTRIM(cTesPed) == ""
						MSGINFO("TES do pedido 01 : "+ cTesPed +" ,n�o possui TES de devolu��o")		
						DisarmTransaction()
						Return .F.		
					ENDIF

					aLinDe01 := {}
					AADD(aLinDe01,{"D1_ITEM"      ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)          ,Nil})
					AADD(aLinDe01,{"D1_COD"       ,oModelZZV:GetValue('ZZV_PRODUT')      						   ,Nil})
					AADD(aLinDe01,{"D1_NFORI"     ,cNfOri         						                           ,Nil})
					AADD(aLinDe01,{"D1_SERIORI"   ,cSeriOri                                                          ,Nil})
					AADD(aLinDe01,{"D1_ITEMORI"   ,cItemori                                      ,Nil})
					AADD(aLinDe01,{"D1_QUANT"     ,oModelZZV:GetValue('ZZV_QUANT')                                   ,Nil})
					AADD(aLinDe01,{"D1_VUNIT"     ,nVlrItem                                                          ,Nil})
					AADD(aLinDe01,{"D1_TES"       ,cTesPed												           ,Nil})
					AADD(aLinDe01,{"D1_LOCAL"     ,cArmazem         ,Nil})
					Aadd(aItensDE01,aLinDe01)


					// PEDIDO 02

					if	cEspecie == '02' .OR. cEspecie == '03'
						cquery1 := " SELECT * "
						cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
						cquery1 += " where C6_FILIAL = '" +cFilPV2+ "' "
						cquery1 += " and D_E_L_E_T_ = '' "
						cquery1 += " and C6_NUM = '"+ cPedido2 +"' "
						cquery1 += " and C6_CLI = '"+ cCLI02 +"' "
						cquery1 += " and C6_LOJA = '"+ cLOJ02 +"' "
						cquery1 += " AND C6_PRODUTO = '"+ oModelZZV:GetValue('ZZV_PRODUT') +"' "
						cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT"))+"'  "

						TCQuery cquery1 new alias T05
						IF T05->(EOF())
							cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
							cquery4 += " WHERE D_E_L_E_T_ = '' "
							cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
							cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
							IF Select("T44") > 0
								T44->(DbCloseArea())
							ENDIF
							TcQuery cquery4 new Alias T44
							IF T44->(EOF())
								MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
								DisarmTransaction()
								T05->(DBCLOSEAREA())
								T03->(DBCLOSEAREA())
								Return .F.
							ELSE
								cquery1 := " SELECT * "
								cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
								cquery1 += " where C6_FILIAL = '" +cFilPV2+ "' "
								cquery1 += " and D_E_L_E_T_ = '' "
								cquery1 += " and C6_NUM = '"+ cPedido2 +"' "
								cquery1 += " and C6_CLI = '"+ cCLI02 +"' "
								cquery1 += " and C6_LOJA = '"+ cLOJ02 +"' "
								cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
								cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
								cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
								IF Select("T55") > 0
									T55->(DbCloseArea())
								ENDIF
								TcQuery cquery1 new Alias T55

								IF !T55->(EOF())

									DbSelectArea("SC6")
									DbSetOrder(1)
									IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

										nVlrItem	:=  T55->C6_PRCVEN
										cLocal      :=  T55->C6_LOCAL 
										cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
										cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
										cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
										nValUni		:=  T55->C6_PRUNIT
										nDescont    :=  T55->C6_DESCONT
										cTesPed     :=  T55->C6_TES
										T05->(DBCLOSEAREA())
										RecLock("SC6",.F.)

										SC6->C6_YCODKIT := T44->ZZ7_CODIGO
										SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

										MSUNLOCK()

									ELSE

										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.


									ENDIf

								ELSE
									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.
								ENDIF	
							endif	
							T44->(DBCLOSEAREA())
							T55->(DBCLOSEAREA())
						ELSE

							//PEGA VALOR DO PEDIDO DO ENVIADO 
							nVlrItem	:=  T05->C6_PRCVEN
							cLocal      :=  T05->C6_LOCAL 
							cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
							cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )
							cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
							nValUni		:=  T05->C6_PRUNIT
							cTesPed     :=  T05->C6_TES
							T05->(DBCLOSEAREA())

						ENDIF
						//cItemori	:=  ALLTRIM(T05->C6_ITEM)

						//ITEM DOC DE ENTRADA  // D1_FILIAL, D1_DOC, D1_SERIE, D1_FORNECE, D1_LOJA, D1_COD, D1_ITEM, R_E_C_N_O_, D_E_L_E_T_
						cquery1 := " SELECT * "
						cquery1 += " FROM " + retsqlname('SD1') + " SD1 (NOLOCK) "
						cquery1 += " WHERE  D1_FILIAL = '"+cPcfil+"' "
						cquery1 += " AND  D1_DOC = '"+cDocPc+"' "
						cquery1 += " AND  D1_SERIE = '"+cSerpc+"' "
						cquery1 += " AND  D1_FORNECE = '"+cPcfor+"' "
						cquery1 += " AND  D1_LOJA = '"+cLojPc+"' "
						cquery1 += " AND  D1_COD = '"+oModelZZV:GetValue('ZZV_PRODUT')+"' "

						TCQuery cquery1 new alias ITPC

						cItemori	:=  ALLTRIM(ITPC->D1_ITEM)

						ITPC->(DBCLOSEAREA())



						cTesPed := Posicione("SF4",1,xFilial("SF4",cFilPV2)+cTesPed,"F4_TESDV")
						IF ALLTRIM(cTesPed) == ""
							MSGINFO("TES do pedido 02 : "+ cTesPed +" ,n�o possui TES de devolu��o")		
							DisarmTransaction()
							Return .F.		
						ENDIF

						AADD(aItens2,{})
						nTam:= Len(aItens2)		
						AADD(aItens2[nTam],{"C6_FILIAL"	,oModelZZH:GetValue('ZZH_FILIAL')								,nil})		
						AADD(aItens2[nTam],{"C6_ITEM"	,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)		,nil})
						AADD(aItens2[nTam],{"C6_PRODUTO"	,oModelZZV:GetValue('ZZV_PRODUT')							,nil})
						AADD(aItens2[nTam],{"C6_NFORI"	,cNfOri						     								,nil})
						AADD(aItens2[nTam],{"C6_SERIORI"	,cSeriOri						     						,nil})
						AADD(aItens2[nTam],{"C6_ITEMORI"	,cItemori    		     				                    ,nil})
						AADD(aItens2[nTam],{"C6_TES"	,cTesTmp														,nil})

						if cTesPed $ GetMv("SA_TESBON")     // se for bonifica��o 
							AADD(aItens2[nTam],{"C6_YTES"	,"518"														,nil}) 
						else
							AADD(aItens2[nTam],{"C6_YTES"	,"512"														,nil})
						endif

						AADD(aItens2[nTam],{"C6_QTDVEN"	,oModelZZV:GetValue('ZZV_QUANT')								,nil})
						AADD(aItens2[nTam],{"C6_PRCVEN"	,nVlrItem        												,nil})
						AADD(aItens2[nTam],{"C6_YCONTRA",oModelZZV:GetValue('ZZV_NUMERO')								,nil})
						AADD(aItens2[nTam],{"C6_YMEDICA",oModelZZV:GetValue('ZZV_MEDICA')								,nil})
						AADD(aItens2[nTam],{"C6_YITCONT",oModelZZV:GetValue('ZZV_ITEM')									,nil})
						AADD(aItens2[nTam],{"C6_YCODKIT",ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))	  					,nil})
						AADD(aItens2[nTam],{"C6_YPAIKIT",Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),"ZZ7_CODPAI"))					,nil})
						AADD(aItens2[nTam],{"C6_LOCAL"  ,cArmazem														,nil})

						aLinPc02 := {}					
						aadd(aLinPc02,{"C7_PRODUTO"	,oModelZZV:GetValue('ZZV_PRODUT')                       ,nil})
						aadd(aLinPc02,{"C7_QUANT"	,oModelZZV:GetValue('ZZV_QUANT')						,nil})
						aadd(aLinPc02,{"C7_PRECO"	,nVlrItem                                        		,nil})
						aadd(aLinPc02,{"C7_TOTAL"	,oModelZZV:GetValue('ZZV_QUANT') * nVlrItem         	,nil})
						aadd(aLinPc02,{"C7_VLDESC"	,0                                                  	,nil})
						aadd(aLinPc02,{"C7_TES"		,"400"			                                       	,nil})
						aadd(aLinPc02,{"C7_YTES"	,"014"			                                       	,nil})								

						aadd(aItensPC02,aLinPc02)
					endif

					// PEDIDO 03
					if	cEspecie == '02' .OR. cEspecie == '03'
						cquery1 := " SELECT * "
						cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
						cquery1 += " where C6_FILIAL = '" + cFilPV3 + "' "
						cquery1 += " and D_E_L_E_T_ = '' "
						cquery1 += " and C6_NUM = '"+ cPedido3 +"' "
						cquery1 += " and C6_CLI = '"+ cCLI03 +"' "
						cquery1 += " and C6_LOJA = '"+ cLOJ03 +"' "
						cquery1 += " AND C6_PRODUTO = '"+ oModelZZV:GetValue('ZZV_PRODUT') +"' "
						cquery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT"))+"'  "

						TCQuery cquery1 new alias T05
						IF T05->(EOF())
							cquery4 := " SELECT ZZ7_CODIGO FROM "+RetSqlName("ZZ7")+" "
							cquery4 += " WHERE D_E_L_E_T_ = '' "
							cquery4 += " AND ZZ7_CODPAI = '"+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))+"' "
							cquery4 += " AND ZZ7_PRODUT = '"+T03->ZZ7_PRODUT+"' "
							IF Select("T44") > 0
								T44->(DbCloseArea())
							ENDIF
							TcQuery cquery4 new Alias T44
							IF T44->(EOF())
								MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
								DisarmTransaction()
								T05->(DBCLOSEAREA())
								T03->(DBCLOSEAREA())
								Return .F.
							ELSE
								cquery1 := " SELECT * "
								cquery1 += " from " + retsqlname('SC6') + " SC6 (NOLOCK) "
								cquery1 += " where C6_FILIAL = '" + cFilPV3 + "' "
								cquery1 += " and D_E_L_E_T_ = '' "
								cquery1 += " and C6_NUM = '"+ cPedido3 +"' "
								cquery1 += " and C6_CLI = '"+ cCLI03 +"' "
								cquery1 += " and C6_LOJA = '"+ cLOJ03 +"' "
								cquery1 += " AND C6_PRODUTO = '"+ T03->ZZ7_PRODUT +"' "
								cQuery1 += " AND C6_QTDVEN  = '"+cValtoChar(POSICIONE("ZZ3",2,oModelZZV:GetValue('ZZV_FILIAL')+oModelZZV:GetValue('ZZV_NUMERO')+oModelZZV:GetValue('ZZV_MEDORG')+oModelZZV:GetValue('ZZV_CLIENT')+oModelZZV:GetValue('ZZV_LOJA')+oModelZZV:GetValue('ZZV_ITEMMD'),"ZZ3_QUANT") * T03->ZZ7_QUANT)+"'  "
								cquery1 += " AND C6_YCODKIT = '"+ ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')) +"' "
								IF Select("T55") > 0
									T55->(DbCloseArea())
								ENDIF
								TcQuery cquery1 new Alias T55

								IF !T55->(EOF())

									DbSelectArea("SC6")
									DbSetOrder(1)
									IF DbSeek(T55->C6_FILIAL + T55->C6_NUM + T55->C6_ITEM + T55->C6_PRODUTO)

										nVlrItem	:=  T55->C6_PRCVEN
										cLocal      :=  T55->C6_LOCAL 
										cNfOri      :=	ALLTRIM(T55->C6_NOTA) 
										cSeriOri    :=	ALLTRIM(T55->C6_SERIE)
										cItemori	:=  POSICIONE("SD2",8,T55->C6_FILIAL+T55->C6_NUM+T55->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)    // D2_FILIAL, D2_PEDIDO, D2_ITEMPV, R_E_C_N_O_, D_E_L_E_T_
										nValUni		:=  T55->C6_PRUNIT
										nDescont    :=  T55->C6_DESCONT
										cTesPed     :=  T55->C6_TES
										T05->(DBCLOSEAREA())
										RecLock("SC6",.F.)

										SC6->C6_YCODKIT := T44->ZZ7_CODIGO
										SC6->C6_YPAIKIT := ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))

										MSUNLOCK()

									ELSE

										MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
										DisarmTransaction()
										T05->(DBCLOSEAREA())
										T03->(DBCLOSEAREA())
										Return .F.


									ENDIf

								ELSE
									MsgInfo("Item n�o encontrado, verificar Pedido de Venda da Medi��o de Saida : Filial "+cFilPV1+" , Pedido"+cPedido1+",Produto"+T03->ZZ7_PRODUT+"")
									DisarmTransaction()
									T05->(DBCLOSEAREA())
									T03->(DBCLOSEAREA())
									Return .F.
								ENDIF	
							ENDIF	

							T44->(DBCLOSEAREA())
							T55->(DBCLOSEAREA())
						ELSE
							//PEGA VALOR DO PEDIDO DO ENVIADO 
							nVlrItem	:=  T05->C6_PRCVEN
							cLocal      :=  T05->C6_LOCAL 
							cNfOri      :=	ALLTRIM(T05->C6_NOTA) 
							cSeriOri    :=	ALLTRIM(T05->C6_SERIE)
							cItemori	:=  POSICIONE("SD2",8,T05->C6_FILIAL+T05->C6_NUM+T05->C6_ITEM,"D2_ITEM" )//ALLTRIM(T05->C6_ITEM)
							nValUni		:=  T05->C6_PRUNIT
							nDescont    :=  T05->C6_DESCONT
							cTesPed     :=  T05->C6_TES
							T05->(DBCLOSEAREA())
						ENDIF
						cTesPed := Posicione("SF4",1,xFilial("SF4",cFilPV3)+cTesPed,"F4_TESDV")
						IF ALLTRIM(cTesPed) == ""
							MSGINFO("TES do pedido 03 : "+ cTesPed +" ,n�o possui TES de devolu��o")		
							DisarmTransaction()
							Return .F.		
						ENDIF

						aLinDe03 := {}
						AADD(aLinDe03,{"D1_ITEM"       ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)          ,Nil})
						AADD(aLinDe03,{"D1_COD"        ,oModelZZV:GetValue('ZZV_PRODUT')	     							 ,Nil})
						AADD(aLinDe03,{"D1_NFORI"     ,cNfOri         						                             ,Nil})
						AADD(aLinDe03,{"D1_SERIORI"   ,cSeriOri                                                          ,Nil})
						AADD(aLinDe03,{"D1_ITEMORI"   ,cItemori					                                       ,Nil})
						AADD(aLinDe03,{"D1_QUANT"     ,oModelZZV:GetValue('ZZV_QUANT')				                    ,Nil})
						AADD(aLinDe03,{"D1_VUNIT"     ,nVlrItem                                                          ,Nil})
						AADD(aLinDe03,{"D1_TES"       ,cTesPed												           ,Nil})
						AADD(aLinDe03,{"D1_YCODKIT"   ,ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT'))          ,Nil})
						AADD(aLinDe03,{"D1_YPAIKIT"   ,Alltrim(POSICIONE("ZZ7",1,xFilial("ZZ7")+ALLTRIM(oModelZZV:GetValue('ZZV_PRODUT')),"ZZ7_CODPAI"))         ,Nil})					
						AADD(aLinDe03,{"D1_LOCAL"     ,cArmazem         ,Nil})
						Aadd(aItensDE03,aLinDe03)
					endif
				EndIf
			NEXT


			AADD(aCab,{"C5_TIPO"		,"D"													,Nil})
			AADD(aCab,{"C5_CLIENTE"		,""														,Nil})
			AADD(aCab,{"C5_LOJACLI"		,""														,Nil})
			AADD(aCab,{"C5_CONDPAG"		,"001"													,Nil})
			AADD(aCab,{"C5_NATUREZ"		,cNaturez												,Nil})
			AADD(aCab,{"C5_YCONTRA"		,oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NUMERO')	,Nil})
			AADD(aCab,{"C5_YFILIAL"		,oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FILIAL')	,Nil})
			AADD(aCab,{"C5_YMEDICA"		,oModel:GetModel('ZZHMASTER'):GetValue('ZZH_MEDICA')	,Nil})
			AADD(aCab,{"C5_YTPVEN"		,"DEVOL"												,Nil})
			AADD(aCab,{"C5_TPEDAV"		,"39"													,Nil})
			AADD(aCab,{"C5_YOBS"	,"DEVOLUCAO"											    ,Nil})
			AADD(aCab,{"C5_YDTSOL"			,DATE()												,Nil})
			AADD(aCab,{"C5_YLCEXPD"		,cArmazem												,Nil})



			aadd(aCabDE01,{"F1_TIPO"   ,"D"})
			IF cTIPOMED == "CONTRI"			
				aadd(aCabDE01,{"F1_FORMUL" ,"N"})
				aadd(aCabDE01,{"F1_DOC"    ,NxtSX5Nota("1  ",.T.,SuperGetMV("MV_TPNRNFS"))}) //getsxenum("SF1","F1_DOC",,1)})
				aadd(aCabDE01,{"F1_SERIE"  ,"2"})
			ELSEIF cTIPOMED == "NCONTRI"
				aadd(aCabDE01,{"F1_FORMUL" ,"S"})				
				aadd(aCabDE01,{"F1_DOC"    ,NxtSX5Nota("1  ",.T.,SuperGetMV("MV_TPNRNFS"))}) //getsxenum("SF1","F1_DOC",,1)})
				aadd(aCabDE01,{"F1_SERIE"  ,"1"})
			ENDIF
			aadd(aCabDE01,{"F1_EMISSAO",dDataBase})
			aadd(aCabDE01,{"F1_FORNECE",""})
			aadd(aCabDE01,{"F1_LOJA"   ,""})
			aadd(aCabDE01,{"F1_ESPECIE","SPED"})
			aadd(aCabDE01,{"F1_ESPECI1","CAIXA"})
			aadd(aCabDE01,{"F1_VOLUME1",oModelZZH:GetValue("ZZH_QUATVL")})

			aCliente	:= {oModelZZH:GetValue('ZZH_CLIENT'),oModelZZH:GetValue('ZZH_LOJA'),oModelZZH:GetValue('ZZH_NOME')}

			cFilOrig := oModelZZH:GetValue('ZZH_FILIAL')

			cContrato
			cMedicao
			nFret
			cEspecie := oModelZZH:GetValue("ZZH_ESPEC")

			// Incluido por Weskley (10/06/2016), para buscar a filial do pedido de venda na nota de entrada.
			cFilPed1 := cFilOrig //cFilPV1


			//PEDIDO 01
			Processa({|| lRet := Devolu1(aCabDE01,aItensDE01,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilPed1,cCLI01,cLOJ01,cNFPD01,cNFSR01,nDesconto,"I") },"Criando Documento de Entrada","Aguarde...",.F.)

			If !lRet
				DisarmTransaction()
				oModel:SetErrorMessage('ZZVDETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
				Return .F.
			EndIf

			//ZZH_ESPECIA ::::> 01=Direto / 02=Triangular / 03=Ambos     
			if	cEspecie == '02' .OR. cEspecie == '03'

				//PEDIDO 02			         aCab,aItens2,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilOrig,cCLI02,cLOJ02,cNFPD02,cNFSR02,aItensPC02,"I",cFilPV2,cForn02,clOJFOR02
				Processa({|| lRet := Devolu2(aCab,aItens2,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilOrig,cCLI02,cLOJ02,cNFPD02,cNFSR02,aItensPC02,"I",cFilPV2,cForn02,clOJFOR02) },"Criando Documento de Entrada","Aguarde...",.F.)


				If !lRet
					DisarmTransaction()
					oModel:SetErrorMessage('ZZVDETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				EndIf

				//PEDIDO 03						
				Processa({|| lRet := Devolu3(aCabDE01,aItensDE03,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilOrig,cCLI03,cLOJ03,cNFPD03,cNFSR03,nDesconto,"I") },"Criando Documento de Entrada","Aguarde...",.F.)

				If !lRet
					DisarmTransaction()
					oModel:SetErrorMessage('ZZVDETAIL',,,,"ERRO", 'N�o foi possivel adicionar o pedido de venda!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				EndIf
			endif


			dbSelectArea("ZZH")
			dbSetOrder(1)
			If DbSeek(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FILIAL')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NUMERO')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_MEDICA'))

				RecLock("ZZH",.F.)

				ZZH->ZZH_PC01FL := cFilDoc01
				ZZH->ZZH_PC01   := ""
				if cTIPOMED == 'CONTRI'
					ZZH->ZZH_PRNF01	:= cNfDoc01
				ELSE
					ZZH->ZZH_PC01NF	:= cNfDoc01
				ENDIF
				ZZH->ZZH_SER01	:= cSrDoc01
				ZZH->ZZH_FONF01 := cCliDoc01
				ZZH->ZZH_LJNF01 := cLjDoc01
				ZZH->ZZH_NF02FL := cFilDoc02
				ZZH->ZZH_PV02   := cDoc02
				ZZH->ZZH_CLNF02 := cCliDoc02
				ZZH->ZZH_LJNF02 := cLjDoc02
				ZZH->ZZH_PC02FL := cFilPC02    
				ZZH->ZZH_PC02   := cPC02      
				ZZH->ZZH_FOPC02 := cForPc02   
				ZZH->ZZH_LJFO02 := cLjPc02
				ZZH->ZZH_PC03FL := ""
				ZZH->ZZH_PC03   := ""
				if cTIPOMED == 'CONTRI'
					ZZH->ZZH_PRNF03 := cDoc03NF
				ELSE
					ZZH->ZZH_NF03   := cDoc03NF
				ENDIF
				ZZH->ZZH_NF03FL := cFilDoc03
				ZZH->ZZH_SER03	:= cSrDoc03
				ZZH->ZZH_FONF03 := cCliDoc03
				ZZH->ZZH_LJNF03 := cLjDoc03

				ZZH->ZZH_LIVRO  := IIF(nLivros > 0,"S","N")

				MsUnLock()

			Endif

		END TRANSACTION

		msgInfo(cMSG)

		cquery2 := " select * "
		cquery2 += " from " + retsqlname('ZZV') + " ZZV (NOLOCK) "
		cquery2 += " where D_E_L_E_T_ = '' " 
		cquery2 += " AND ZZV_FILIAL = " + oModelZZV:GetValue("ZZV_FILIAL") + " "
		cquery2 += " AND ZZV_NUMERO = " + oModelZZV:GetValue("ZZV_NUMERO") + " "
		cquery2 += " AND ZZV_MEDICA = " + oModelZZV:GetValue("ZZV_MEDICA") + " "
		cquery2 += " AND ZZV_CLIENT = " + oModelZZV:GetValue("ZZV_CLIENT") + " "
		cquery2 += " AND ZZV_LOJA   = " + oModelZZV:GetValue("ZZV_LOJA") + " "

		TCQuery cquery2 new alias TZZV2
		WHILE !TZZV2->(EOF())

			cUpd:= " UPDATE "+RetSqlName("ZZ3")+ " SET "
			cUpd+= " ZZ3_QTDEV0 += '"+cvaltochar(TZZV2->ZZV_QUANT)+"' "
			cUpd+= " WHERE ZZ3_FILIAL = '"+TZZV2->ZZV_FILIAL+"' AND ZZ3_NUMERO = '"+TZZV2->ZZV_NUMERO+"' "
			cUpd+= " AND ZZ3_MEDICA   = '"+TZZV2->ZZV_MEDORG+"' AND ZZ3_CLIENT = '"+TZZV2->ZZV_CLIENT+"' "				
			cUpd+= " AND ZZ3_LOJA    = '"+TZZV2->ZZV_LOJA+"'    AND ZZ3_ITEM   = '"+TZZV2->ZZV_ITEMMD+"' "								
			//cUpd+= " AND D_E_L_E_T_= '' "
			If TCSQLEXEC(cUpd) < 0
				ALERT("Erro no processo: "+cUpd+"  -> FAVOR ENVIAR ERRO PARA TI")

				RETURN .F.
			EndIf
			TZZV2->(DbSkip())
		ENDDO
		TZZV2->(DbCloseArea())
		Return lRet
	endif
Return lRet

Static Function Devolu1(aCabDE01,aItensDE01,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilorig,cCLI01,cLOJ01,cNFPD01,cNFSR01,nDesconto,cTipo)

	Local cFilBak			:= cFilAnt
	Local aCabCliente		:= aClone(aCabDE01)
	Local nPosCliente	    
	Local nPosLoja		    
	Local nPosDoc		    
	Local cCliCol           := cLjCol := ""
	Local cPedido1			:= ""	
	Local aDesc             := {}
	Local aCabPc			:= {}
	Local aItenPcIn			:= {}
	Local aLinha 			:= {}
	Local aItensD1			:= {}
	Local cPedCom           := ""
	Local cDoc				:= ""
	Local cNota             := IIF(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_TPMED')=='1',oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PRNF01'),oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PC01NF'))
	Local nTotal            := 0
	Local nPosQuant			:= 6
	Local nPosPreco			:= 7
	Local nPesoNF			:= 0
	Private lMsErroAuto	:= .F.
	ProcRegua(4)

	cFilAnt := cFilorig

	if cTipo == 'I'
		nPosCliente	    := aScan(aCabDE01,{|x| x[1]=="F1_FORNECE"})
		nPosLoja		:= aScan(aCabDE01,{|x| x[1]=="F1_LOJA"})
		nPosDoc		    := aScan(aCabDE01,{|x| x[1]=="F1_DOC"})

		IncProc("Criando ...")
		ProcessMessage()
		//Cliente - Comercial
		aCabCliente[nPosCliente][2]	:= cCLI01
		aCabCliente[nPosLoja][2]	:= cLOJ01
		aCabCliente[nPosDoc][2]	    := NxtSX5Nota("1  ",.T.,SuperGetMV("MV_TPNRNFS")) //getsxenum("SF1","F1_DOC",,1)

		AADD(aCabCliente,{"F1_FILIAL",xFilial("SF1")})
		AADD(aCabCliente,{"F1_DESCONT",nDesconto})


		if nDesconto > 0

			for nAx:= 1 TO len(aItensDE01)
				nTotal += aItensDE01[nAx][nPosQuant][2] * aItensDE01[nAx][nPosPreco][2]
			NEXT
			If  nDesconto > nTotal
				cFilant := cFilBak
				AutoGrLog("Desconto superior ao valor dos itens !")
				MostraErro()
				Return .F.
			endif
			aDesc := RATDESC(len(aItensDE01),nDesconto,nTotal,aItensDE01,nPosQuant,nPosPreco)

			FOR nAx:= 1 to len(aItensDE01)
				Aadd(aItensDE01[nAx],{'D1_VALDESC',aDesc[nAx][1] ,NIL})
			NEXT
		endif

		for x := 1 to len(aitensde01)
			nPesoNF += POSICIONE("SB1",1, xFilial("SB1") + aItensDE01[x][2][2], "B1_PESO")
		next
		AADD(aCabCliente,{"F1_PBRUTO",nPesoNF})

		lMsErroAuto	:= .F.
		MSExecAuto({|x,y,z| MATA140(x,y,z)}, aCabCliente, aItensDE01 , 3)


		If lMsErroAuto
			cFilant := cFilBak
			AutoGrLog("Erro ao gerar Pedido de compra, Cliente para a Comercial !")
			MostraErro()
			Return .F.
		Else
			confirmsx8()
			cPc	:= SF1->F1_DOC
		Endif

		cMSG += "Prenota, Filial : "+cFilAnt+" Codigo: "+SF1->F1_DOC+""+CHR(13)+CHR(10)+""


		cFilDoc01  := ALLTRIM(cFilAnt) 
		cDoc01     := ""
		cNfDoc01   := ALLTRIM(SF1->F1_DOC)
		cSrDoc01   := ALLTRIM(SF1->F1_SERIE)  
		cCliDoc01  := cCLI01
		cLjDoc01   := cLOJ01


		cFilant := cFilOrig

		///////////////
		// ALTERA��O //
		///////////////

	elseif cTipo == 'A'
		IncProc("Alterando ...")
		ProcessMessage()

		nPosCliente	    := aScan(aCabDE01,{|x| x[1]=="F1_FORNECE"})
		nPosLoja		:= aScan(aCabDE01,{|x| x[1]=="F1_LOJA"})
		nPosDoc		    := aScan(aCabDE01,{|x| x[1]=="F1_DOC"})

		aCabCliente[nPosCliente][2]	:= cCLI01
		aCabCliente[nPosLoja][2]	:= cLOJ01

		AADD(aCabCliente,{"F1_FILIAL",xFilial("SF1")})
		AADD(aCabCliente,{"F1_DESCONT",nDesconto})


		//Cliente - Comercial

		if nDesconto > 0

			FOR nAx:= 1 to len(aItensDE01)
				nTotal += aItensDE01[nAx][nPosQuant][2] * aItensDE01[nAx][nPosPreco][2]
			NEXT
			If  nDesconto >  nTotal
				cFilant   := cFilBak
				AutoGrLog("Desconto superior ao valor dos itens !")
				MostraErro()
				Return .F.
			endif
			aDesc := RATDESC(len(aItensDE01),nDesconto,nTotal,aItensDE01,nPosQuant,nPosPreco)

			FOR nAx:= 1 to len(aItensDE01)
				Aadd(aItensDE01[nAx],{"D1_VALDESC",aDesc[nAx][1] ,NIL})
			NEXT
		endif



		aSF1:=	{}
		lMsErroAuto	:= .F.
		dbSelectArea("SF1")
		SF1->(dbSetOrder(1))
		If SF1->(dbSeek(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PC01FL') + cNota + oModel:GetModel('ZZHMASTER'):GetValue('ZZH_SER01')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FONF01')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_LJNF01')+"D"))
			aAdd(aSF1,	{'F1_FILIAL'	,SF1->F1_FILIAL		,NIL})
			aAdd(aSF1,	{'F1_TIPO'		,SF1->F1_TIPO		,NIL})
			aAdd(aSF1,	{'F1_FORMUL'	,SF1->F1_FORMUL		,NIL})
			aAdd(aSF1,	{'F1_DOC'		,SF1->F1_DOC   		,NIL})
			aAdd(aSF1,	{'F1_SERIE'		,SF1->F1_SERIE		,NIL})
			aAdd(aSF1,	{'F1_EMISSAO'	,SF1->F1_EMISSAO	,NIL})
			aAdd(aSF1,	{'F1_FORNECE'	,SF1->F1_FORNECE	,NIL})
			aAdd(aSF1,	{'F1_LOJA'		,SF1->F1_LOJA		,NIL})
			aAdd(aSF1,	{'F1_VOLUME1'	,SF1->F1_VOLUME1	,NIL})

			cChave	:=	SF1->(F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)
			aCabCliente[nPosDoc][2]	:= SF1->F1_DOC

			dbSelectArea("SD1")
			SD1->(dbSetOrder(1))
			SD1->(dbSeek(cChave)) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA

			aItensD1:={}
			While SD1->(!eof()) .and. cChave == SD1->(D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA)
				aLinha := {}

				aAdd(aLinha,	{'D1_FILIAL'	,SD1->D1_FILIAL		,NIL})
				aAdd(aLinha,	{'D1_ITEM'		,SD1->D1_ITEM		,NIL})
				aAdd(aLinha,	{'D1_COD'		,SD1->D1_COD		,NIL})
				aAdd(aLinha,	{'D1_QUANT'		,SD1->D1_QUANT		,NIL})
				aAdd(aLinha,	{'D1_VUNIT'		,SD1->D1_VUNIT		,NIL})
				aAdd(aLinha,	{'D1_TOTAL'		,SD1->D1_TOTAL		,NIL})

				AAdd(aItensD1, aLinha)

				SD1->(dbSkip())
			Enddo  

			MSExecAuto({|x,y,z| MATA140(x,y,z)}, aSF1, aItensD1 , 5)

			cFilant := cFilBak
			If lMsErroAuto
				cFilant := cFilBak
				AutoGrLog("Erro ao excluir Pr� Nota, Cliente para a Comercial !")
				MostraErro()
				Return .F.
			endif

			MSExecAuto({|x,y,z| MATA140(x,y,z)}, aCabCliente, aItensDE01 , 3)

			If lMsErroAuto
				cFilant := cFilBak
				AutoGrLog("Erro ao Alterar Pr� Nota, Cliente para a Comercial !")
				MostraErro()
				Return .F.
			endif

			cMSG += "Alterado Pr� Nota, Filial : "+cFilAnt+" Codigo: "+SF1->F1_DOC+""+CHR(13)+CHR(10)+""

			cFilant := cFilOrig
		ELSE
			cFilant := cFilBak
			AutoGrLog("Pr� nota n�o encontrada !")
			MostraErro()
			Return .F.
		Endif


	ELSEIF  cTipo == 'E'    // TRATAMENTO PARA EXCLUSAO DO PEDIDO DE COMPRA 

		IncProc("Excluindo ...")
		ProcessMessage()

		aSF1:=	{}
		lMsErroAuto	:= .F.
		dbSelectArea("SF1")
		SF1->(dbSetOrder(1))
		If SF1->(dbSeek(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PC01FL') + cNota + oModel:GetModel('ZZHMASTER'):GetValue('ZZH_SER01')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FONF01')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_LJNF01')+"D"))
			aAdd(aSF1,	{'F1_FILIAL'	,SF1->F1_FILIAL		,NIL})
			aAdd(aSF1,	{'F1_TIPO'		,SF1->F1_TIPO		,NIL})
			aAdd(aSF1,	{'F1_FORMUL'	,SF1->F1_FORMUL		,NIL})
			aAdd(aSF1,	{'F1_DOC'		,SF1->F1_DOC   		,NIL})
			aAdd(aSF1,	{'F1_SERIE'		,SF1->F1_SERIE		,NIL})
			aAdd(aSF1,	{'F1_EMISSAO'	,SF1->F1_EMISSAO	,NIL})
			aAdd(aSF1,	{'F1_FORNECE'	,SF1->F1_FORNECE	,NIL})
			aAdd(aSF1,	{'F1_LOJA'		,SF1->F1_LOJA		,NIL})

			cChave	:=	SF1->(F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)
			dbSelectArea("SD1")
			SD1->(dbSetOrder(1))
			SD1->(dbSeek(cChave)) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA

			aItensD1:={}
			While SD1->(!eof()) .and. cChave == SD1->(D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA)
				aLinha := {}

				aAdd(aLinha,	{'D1_FILIAL'	,SD1->D1_FILIAL		,NIL})
				aAdd(aLinha,	{'D1_ITEM'		,SD1->D1_ITEM		,NIL})
				aAdd(aLinha,	{'D1_COD'		,SD1->D1_COD		,NIL})
				aAdd(aLinha,	{'D1_QUANT'		,SD1->D1_QUANT		,NIL})
				aAdd(aLinha,	{'D1_VUNIT'		,SD1->D1_VUNIT		,NIL})
				aAdd(aLinha,	{'D1_TOTAL'		,SD1->D1_TOTAL		,NIL})

				AAdd(aItensD1, aLinha)

				SD1->(dbSkip())
			Enddo  

			MSExecAuto({|x,y,z| MATA140(x,y,z)}, aSF1, aItensD1 , 5)

		Endif
		cFilant := cFilBak
		If lMsErroAuto
			cFilant := cFilBak
			AutoGrLog("Erro ao excluir Pr� Nota, Cliente para a Comercial !")
			MostraErro()
			Return .F.
		endif


		cMSG += "Pr� Nota excluida , Filial : "+cFilAnt+" Codigo: "+cPedCom+""+CHR(13)+CHR(10)+"" 

		cFilant := cFilOrig

	endif


Return .T.

Static Function Devolu2(aCab,aItens2,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilorig,cCLI02,cLOJ02,cNFPD02,cNFSR02,aItensPC02,cTipo,cFilPV2,cForn02,clOJFOR02)

	Local cFilBak			:= cFilAnt
	Local aCabCol			:= aClone(aCab)
	Local cCliCol           := cLjCol := ""
	Local nPosCliente		
	Local nPosLoja		   
	Local nPosTes			
	Local nPosDoc
	Local cTesDEV2			
	Local cPedido1
	Local cFor              := ""
	Local cLojFor           := ""
	Local cChave			:= ""
	Local nTam				:= 0
	Local aCabPc            := {} 
	Local aSC6				:= {}
	Local aSC5				:= {}
	Local aSC7				:= {}
	Local aLinPc			:= {}
	Local cPedCom 			:= ""
	Local cCNPJCLI   	    := POSICIONE("SA1",1,XFILIAL("SA1")+cCLI02+cLOJ02,"A1_CGC")
	Local cCNPJColiga	    := FWArrFilAtu(cEmpAnt,cFilAnt)[18]	//Coligada
	Local cCli              := ""
	Local cLojCli           := ""
	Private lMsErroAuto	:= .F.
	ProcRegua(4)

	cFilAnt := cFilorig

	if cTipo == "I"

		nPosCliente		:= aScan(aCab,{|x| x[1]=="C5_CLIENTE"})
		nPosLoja		:= aScan(aCab,{|x| x[1]=="C5_LOJACLI"})
		nPosTes			:= aScan(aItens2[1],{|x| x[1]=="C6_YTES"})
		cTesDEV2		:= GetTes(SuperGetMv("SA_TESDEV2",.F.,"","010105"),aItens2[1][nPosTes][2])		


		//Comercial -> Matriz
		IncProc("Criando pedido na Comercial para a Matriz ")
		ProcessMessage()

		cCli    :=  SUBSTR(GetMv( "SA_PCFOR" ),1,6)
		cLojCli :=  SUBSTR(GetMv( "SA_PCFOR" ),8,9) 

		aCabCol[nPosCliente][2]		:= cCli
		aCabCol[nPosLoja][2]		:= cLojCli

		AADD(aCabCol,{"C5_FILIAL",xFilial("SC5",cFilAnt),Nil})

	elseif cTipo == "A"

		nPosCliente		:= aScan(aCab,{|x| x[1]=="C5_CLIENTE"})
		nPosLoja		:= aScan(aCab,{|x| x[1]=="C5_LOJACLI"})
		nPosTes			:= aScan(aItens2[1],{|x| x[1]=="C6_YTES"})
		cTesDEV2		:= GetTes(SuperGetMv("SA_TESDEV2",.F.,"","010105"),aItens2[1][nPosTes][2])		

		//Comercial -> Matriz
		IncProc("Alterando pedido na Comercial para a Matriz ")
		ProcessMessage()

		AADD(aCabCol,{"C5_FILIAL",xFilial("SC5",cFilAnt),Nil})

	elseif cTipo == "E"
		//Comercial -> Matriz
		IncProc("Excluindo pedido na Comercial para a Matriz ")
		ProcessMessage()

	endif

	if cTipo == "I"
		MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCol,aItens2,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
		If lMsErroAuto
			cFilAnt	:= cFilBak
			AutoGrLog("Erro ao gerar pedido de venda da Comercial para Matriz!")
			MostraErro()
			Return .F.
		Else
			cDoc02	:= SC5->C5_NUM

			cFilDoc02  := ALLTRIM(SC5->C5_Filial) 
			cDoc02     := ALLTRIM(SC5->C5_NUM)
			cNfDoc02   := ""
			cSrDoc02   := ""
			cCliDoc02  := cCli
			cLjDoc02   := cLojCli
		Endif

		cMSG += "Pedido de Venda de devolucao, Filial : "+cFilAnt+" Codigo: "+SC5->C5_NUM+""+CHR(13)+CHR(10)+""

	ELSEif cTipo == "A"

		MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCol,aItens2,4)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o

		If lMsErroAuto
			cFilAnt	:= cFilBak
			AutoGrLog("Erro ao alterar pedido de venda da Comercial para Matriz!")
			MostraErro()
			Return .F.
		Else
			cPedido2	:= SC5->C5_NUM

		Endif

		cMSG += "Alterado Pedido de Venda de devolucao, Filial : "+cFilAnt+" Codigo: "+SC5->C5_NUM+""+CHR(13)+CHR(10)+""

	ELSEif cTipo == "E"

		aSF1:=	{}
		lMsErroAuto	:= .F.
		dbSelectArea("SC5")
		SC5->(dbSetOrder(1))
		If SC5->(dbSeek(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NF02FL')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PV02')))
			AADD(aSC5,{"C5_FILIAL"		,SC5->C5_FILIAL													,Nil})
			AADD(aSC5,{"C5_NUM"		    ,SC5->C5_NUM													,Nil})
			AADD(aSC5,{"C5_TIPO"		,SC5->C5_TIPO													,Nil})
			AADD(aSC5,{"C5_CLIENTE"		,SC5->C5_CLIENTE												,Nil})
			AADD(aSC5,{"C5_LOJACLI"		,SC5->C5_LOJACLI												,Nil})
			AADD(aSC5,{"C5_CONDPAG"		,SC5->C5_CONDPAG												,Nil})
			AADD(aSC5,{"C5_NATUREZ"		,SC5->C5_NATUREZ												,Nil})


			cChave	:=	SC5->(C5_FILIAL+C5_NUM)

			dbSelectArea("SC6")
			SC6->(dbSetOrder(1))
			SC6->(dbSeek(cChave)) //C6_FILIAL+C6_NUM

			aSC6:={}
			While SC6->(!eof()) .and. cChave == SC6->(C6_FILIAL+C6_NUM)

				AADD(aSC6,{})
				nTam := Len(aSC6)		
				AADD(aSC6[nTam],{"C6_FILIAL"	,SC6->C6_FILIAL								,nil})		
				AADD(aSC6[nTam],{"C6_ITEM"	    ,SC6->C6_ITEM		                        ,nil})
				AADD(aSC6[nTam],{"C6_PRODUTO"	,SC6->C6_PRODUTO							,nil})
				AADD(aSC6[nTam],{"C6_NFORI"	    ,SC6->C6_NFORI					     		,nil})
				AADD(aSC6[nTam],{"C6_SERIORI"	,SC6->C6_SERIORI							,nil})
				AADD(aSC6[nTam],{"C6_ITEMORI"	,SC6->C6_ITEMORI    		     			,nil})
				AADD(aSC6[nTam],{"C6_TES"	    ,SC6->C6_TES								,nil}) 
				AADD(aSC6[nTam],{"C6_QTDVEN"	,SC6->C6_QTDVEN								,nil})
				AADD(aSC6[nTam],{"C6_PRCVEN"	,SC6->C6_PRCVEN								,nil})

				SC6->(dbSkip())
			Enddo  

			MsExecAuto({|x,y,z| MATA410(x,y,z) }, aSC5,aSC6,5)
			If lMsErroAuto
				cFilant := cFilBak
				AutoGrLog("Erro ao excluir Pedido de venda , Comercial para Matriz!")
				MostraErro()
				Return .F.
			endif

		endif

		cMSG += "Excluido Pedido de Venda de devolucao, Filial : "+cFilAnt+" Codigo: "+SC5->C5_NUM+""+CHR(13)+CHR(10)+""

	ENDIF

	if cTipo == "I"
		IncProc("Criando pedido de compra Holding para Subsidiaria ... ")
		ProcessMessage()
	ELSEif cTipo == "A"
		IncProc("Aleterando pedido de compra Holding para Subsidiaria ... ")
		ProcessMessage()
	ELSEif cTipo == "E"
		IncProc("Excluindo pedido de compra Holding para Subsidiaria ... ")
		ProcessMessage()
	ENDIF
	//cFilant := cFilBak

	if cTipo == "I"
		//cFor    :=	SUBSTR(GetMv( "SA_PCFOR" ),1,6)
		//cLojFor :=	SUBSTR(GetMv( "SA_PCFOR" ),8,9)

		cFor 	:= SUBSTR(SuperGetMv( "SA_CLIPEDI",,, cFilDoc01 ),1,6)
		cLojFor := SUBSTR(SuperGetMv( "SA_CLIPEDI",,, cFilDoc01 ),8,9)


		cFilAnt := "010105"

		cPedCom	:= CriaVar('C7_NUM', .T.)
		Aadd(aCabPc,{"C7_FILIAL"		,cFilAnt  })
		aadd(aCabPc,{"C7_NUM"		    ,cPedCom}) //GETSXENUM("SC7","C7_NUM",,1)})
		aadd(aCabPc,{"C7_EMISSAO"	    ,ddatabase})
		aadd(aCabPc,{"C7_FORNECE"	    ,cFor})
		aadd(aCabPc,{"C7_LOJA"		    ,cLojFor})
		aadd(aCabPc,{"C7_COND"			,"001"})
		aadd(aCabPC,{"C7_CONTATO"		,"AUTO"})
		aadd(aCabPC,{"C7_FILENT"  		,cFilant})
		aadd(aCabPC,{"C7_VLDESC"		,0  })

		lMsErroAuto	:= .F.

		MsExecAuto({|u,x,y,z| MATA120(u,x,y,z)},1, aCabPC,aItensPC02,3)

		If lMsErroAuto
			cFilAnt	:= cFilBak
			AutoGrLog("Erro ao gerar pedido de compra da Holding para Subsidiaria!")
			MostraErro()
			Return .F.
		Else
			confirmsx8()
			cPc	:= SC7->C7_NUM
		Endif

		cFilPC02  := ALLTRIM(cFilAnt) 
		cPC02     := ALLTRIM(cPedCom)
		cForPc02  := cFor
		cLjPc02   := cLojFor

		cMSG += "Pedido de Compra, Filial : "+cFilAnt+" Codigo: "+cPedCom+""+CHR(13)+CHR(10)+""

		cFilant := cFilOrig

	elseif cTipo == "A"

		//cFor    :=	SUBSTR(GetMv( "SA_PCFOR" ),1,6)
		//cLojFor :=	SUBSTR(GetMv( "SA_PCFOR" ),8,9)

		cFilAnt := "010105"

		cFor 	:= SUBSTR(SuperGetMv( "SA_CLIPEDI",,, cFilDoc01 ),1,6)
		cLojFor := SUBSTR(SuperGetMv( "SA_CLIPEDI",,, cFilDoc01 ),8,9)

		cPedCom	:= oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PC02')
		Aadd(aCabPc,{"C7_FILIAL"		,cFilAnt  })
		aadd(aCabPc,{"C7_NUM"		    ,cPedCom}) //GETSXENUM("SC7","C7_NUM",,1)})
		aadd(aCabPc,{"C7_EMISSAO"	    ,ddatabase})
		aadd(aCabPc,{"C7_FORNECE"	    ,cFor})
		aadd(aCabPc,{"C7_LOJA"		    ,cLojFor})
		aadd(aCabPc,{"C7_COND"			,"001"})
		aadd(aCabPC,{"C7_CONTATO"		,"AUTO"})
		aadd(aCabPC,{"C7_FILENT"  		,cFilant})
		aadd(aCabPC,{"C7_VLDESC"  		,0})

		lMsErroAuto	:= .F.
		dbSelectArea("SC7")
		SC7->(dbSetOrder(1))
		If SC7->(dbSeek(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PC02FL')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PC02')))
			MATA120(1,aCabPC,aItensPC02,5)
		Endif

		If lMsErroAuto
			cFilant := cFilBak
			AutoGrLog("Erro ao excluir Pedido de compra da Holding para Subsidiaria!")
			MostraErro()
			Return .F.
		endif

		lMsErroAuto := .F.
		MATA120(1,aCabPC,aItensPC02,3)


		If lMsErroAuto
			cFilant := cFilBak
			AutoGrLog("Erro ao gerar Pedido de compra da Holding para Subsidiaria!")
			MostraErro()
			Return .F.
		Else
			confirmsx8()
			cPc	:= SC7->C7_NUM
		Endif


		cMSG += "Alterado Pedido de Compra, Filial : "+cFilAnt+" Codigo: "+cPedCom+""+CHR(13)+CHR(10)+""  

		cFilPC02  := ALLTRIM(cFilAnt) 
		cPC02     := ALLTRIM(cPedCom)
		cForPc02  := cFor
		cLjPc02   := cLojFor

		cFilant := cFilOrig

	elseif cTipo == "E"

		//cFor    :=	SUBSTR(GetMv( "SA_PCFOR" ),1,6)
		//cLojFor :=	SUBSTR(GetMv( "SA_PCFOR" ),8,9)

		cFilAnt := "010105"
		cFor 	:= SUBSTR(SuperGetMv( "SA_CLIPEDI" ),1,6)
		cLojFor := SUBSTR(SuperGetMv( "SA_CLIPEDI" ),8,9)


		cPedCom	:= oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PC02')
		Aadd(aCabPc,{"C7_FILIAL"		,cFilAnt  })
		aadd(aCabPc,{"C7_NUM"		    ,cPedCom}) 
		aadd(aCabPc,{"C7_FORNECE"	    ,cFor})
		aadd(aCabPc,{"C7_LOJA"		    ,cLojFor})
		aadd(aCabPc,{"C7_COND"			,"001"})
		aadd(aCabPC,{"C7_CONTATO"		,"AUTO"})
		aadd(aCabPC,{"C7_FILENT"  		,cFilant})

		cChave := cFilAnt+cPedCom

		dbSelectArea("SC7")
		SC7->(dbSetOrder(1))
		SC7->(dbSeek(cChave)) //C7_FILIAL+C7_NUM

		aSC7:={}
		While SC7->(!eof()) .and. cChave == SC7->(C7_FILIAL+C7_NUM)

			aLinPc := {}					
			aadd(aLinPc,{"C7_PRODUTO"	,SC7->C7_PRODUTO						                ,nil})
			aadd(aLinPc,{"C7_QUANT"		,SC7->C7_QUANT											,nil})
			aadd(aLinPc,{"C7_PRECO"		,SC7->C7_PRECO                                     		,nil})
			aadd(aLinPc,{"C7_TES"		,SC7->C7_TES	                                        ,nil})
			aadd(aSC7,aLinPc)

			SC7->(dbSkip())
		Enddo  

		lMsErroAuto	:= .F.
		dbSelectArea("SC7")
		SC7->(dbSetOrder(1))
		If SC7->(dbSeek(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PC02FL')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PC02')))
			MATA120(1,aCabPC,aSC7,5)
		Endif

		If lMsErroAuto
			cFilant := cFilBak
			AutoGrLog("Erro ao excluir Pedido de compra da Holding para Subsidiaria!")
			MostraErro()
			Return .F.
		endif

		cMSG += "Excluido Pedido de Compra, Filial : "+cFilAnt+" Codigo: "+cPedCom+""+CHR(13)+CHR(10)+""

	ENDIF

Return .T.

Static Function Devolu3(aCabDE01,aItensDE03,aCliente,cContrato,cMedicao,nFret,oModel,cEspecie,cFilOrig,cCLI03,cLOJ03,cNFPD03,cNFSR03,nDesconto,cTipo)

	Local cFilBak			:= cFilAnt
	Local aCabCliente		:= aClone(aCabDE01)
	Local nPosCliente		
	Local nPosLoja		    
	Local nPosDoc		    
	Local aDesc             := {}
	Local aCabPc			:= {}
	Local cPedido1			:= ""	
	Local cPedCom           := ""
	Local cChave			:= ""
	Local cNota             := IIF(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_TPMED')=='1',oModel:GetModel('ZZHMASTER'):GetValue('ZZH_PRNF03'),oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NF03'))
	Local aItenPcIn			:= {}
	Local aLinha 			:= {}
	Local aItensD1          := {}
	Local aSF1				:= {}
	Local nTotal            := 0
	Local nPosQuant			:= 6
	Local nPosPreco			:= 7
	Local nPesoNF			:= 0
	Private lMsErroAuto	:= .F.

	ProcRegua(4)

	cFilAnt := "010105" // matriz

	if cTipo == "I" .OR. cTipo == "A"

		nPosCliente		:= aScan(aCabDE01,{|x| x[1]=="F1_FORNECE"})
		nPosLoja		:= aScan(aCabDE01,{|x| x[1]=="F1_LOJA"})
		nPosDoc		    := aScan(aCabDE01,{|x| x[1]=="F1_DOC"})

		aCabCliente[nPosCliente][2]	:= cCLI03
		aCabCliente[nPosLoja][2]	:= cLOJ03
		aCabCliente[nPosDoc][2]	    := NxtSX5Nota("1  ",.T.,SuperGetMV("MV_TPNRNFS")) //getsxenum("SF1","F1_DOC",,1)

		AADD(aCabCliente,{"F1_FILIAL",xFilial("SF1")})
		AADD(aCabCliente,{"F1_DESCONT",nDesconto})


		if nDesconto > 0

			FOR nAx:= 1 to len(aItensDE03)
				nTotal += aItensDE03[nAx][nPosQuant][2] * aItensDE03[nAx][nPosPreco][2]
			NEXT
			If nDesconto > nTotal 
				cFilant := cFilBak
				AutoGrLog("Desconto superior ao valor dos itens !")
				MostraErro()
				Return .F.
			endif
			aDesc := RATDESC(len(aItensDE03),nDesconto,nTotal,aItensDE03,nPosQuant,nPosPreco)

			FOR nAx:= 1 to len(aItensDE03)
				Aadd(aItensDE03[nAx],{"D1_VALDESC",aDesc[nAx][1] ,NIL})
			NEXT
		endif

	ENDIF

	IF cTipo == "I"
		//Cliente -> Matriz
		IncProc("Criando Pr� Nota Cliente para a Matriz... ")
		ProcessMessage() //ATUALIZA CLIENTE

		for x := 1 to len(aItensDE03)
			nPesoNF += POSICIONE("SB1",1, xFilial("SB1") + aItensDE03[x][2][2], "B1_PESO")
		next
		AADD(aCabCliente,{"F1_PBRUTO",nPesoNF})

		lMsErroAuto	:= .F.


		lMsErroAuto	:= .F.
		MSExecAuto({|x,y,z| MATA140(x,y,z)}, aCabCliente, aItensDE03 , 3)

		If lMsErroAuto
			cFilant := cFilBak
			AutoGrLog("Erro ao gerar Pr� Nota, Cliente para a Matriz !")
			MostraErro()
			Return .F.
		Else
			confirmsx8()
			cPc	:= SF1->F1_DOC
		Endif

		cMSG += "Pr� Nota, Filial : "+cFilAnt+" Codigo: "+SF1->F1_DOC+""+CHR(13)+CHR(10)+""


		cFilDoc03  := ALLTRIM(cFilAnt) 
		cDoc03     := ""
		cDoc03NF   := ALLTRIM(SF1->F1_DOC)
		cSrDoc03   := ALLTRIM(SF1->F1_SERIE)  
		cCliDoc03  := cCLI03
		cLjDoc03   := cLOJ03

		cFilant := cFilOrig


	ELSEIF cTipo == "A"
		//Cliente -> Matriz
		IncProc("Alterando Pr� nota do Cliente para a Matriz... ")
		ProcessMessage() //ATUALIZA CLIENTE

		aSF1:=	{}
		lMsErroAuto	:= .F.
		dbSelectArea("SF1")
		SF1->(dbSetOrder(1))
		If SF1->(dbSeek(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NF03FL')+ cNota + oModel:GetModel('ZZHMASTER'):GetValue('ZZH_SER03')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FONF03')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_LJNF03')+"D"))

			aAdd(aSF1,	{'F1_FILIAL'	,SF1->F1_FILIAL		,NIL})
			aAdd(aSF1,	{'F1_TIPO'		,SF1->F1_TIPO		,NIL})
			aAdd(aSF1,	{'F1_FORMUL'	,SF1->F1_FORMUL		,NIL})
			aAdd(aSF1,	{'F1_DOC'		,SF1->F1_DOC   		,NIL})
			aAdd(aSF1,	{'F1_SERIE'		,SF1->F1_SERIE		,NIL})
			aAdd(aSF1,	{'F1_EMISSAO'	,SF1->F1_EMISSAO	,NIL})
			aAdd(aSF1,	{'F1_FORNECE'	,SF1->F1_FORNECE	,NIL})
			aAdd(aSF1,	{'F1_LOJA'		,SF1->F1_LOJA		,NIL})
			aAdd(aSF1,	{'F1_VOLUME1'	,SF1->F1_VOLUME1	,NIL})

			cChave	:=	SF1->(F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)
			aCabCliente[nPosDoc][2]	:= SF1->F1_DOC

			dbSelectArea("SD1")
			SD1->(dbSetOrder(1))
			SD1->(dbSeek(cChave)) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA

			aItensD1:={}
			While SD1->(!eof()) .and. cChave == SD1->(D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA)
				aLinha := {}

				aAdd(aLinha,	{'D1_FILIAL'	,SD1->D1_FILIAL		,NIL})
				aAdd(aLinha,	{'D1_ITEM'		,SD1->D1_ITEM		,NIL})
				aAdd(aLinha,	{'D1_COD'		,SD1->D1_COD		,NIL})
				aAdd(aLinha,	{'D1_QUANT'		,SD1->D1_QUANT		,NIL})
				aAdd(aLinha,	{'D1_VUNIT'		,SD1->D1_VUNIT		,NIL})
				aAdd(aLinha,	{'D1_TOTAL'		,SD1->D1_TOTAL		,NIL})

				AAdd(aItensD1, aLinha)

				SD1->(dbSkip())
			Enddo  

			MSExecAuto({|x,y,z| MATA140(x,y,z)}, aSF1, aItensD1 , 5)
			If lMsErroAuto
				cFilant := cFilBak
				AutoGrLog("Erro ao alterar Pr� Nota, Cliente para a Comercial !")
				MostraErro()
				Return .F.
			endif

			MSExecAuto({|x,y,z| MATA140(x,y,z)}, aCabCliente, aItensDE03 , 3)

			If lMsErroAuto
				cFilant := cFilBak
				AutoGrLog("Erro ao alterar Pr� Nota, Cliente para a Comercial !")
				MostraErro()
				Return .F.
			endif


			cMSG += "Alterado Pr� Nota , Filial : "+cFilAnt+" Codigo: "+SF1->F1_DOC+""+CHR(13)+CHR(10)+"" 

			cFilant := cFilOrig
		else
			cFilant := cFilBak
			AutoGrLog("Pr� nota n�o encontrada !")
			MostraErro()
			Return .F.

		Endif



	ELSEIF cTipo == "E"

		//Cliente -> Matriz
		IncProc("Excluindo Pr� nota do Cliente para a Matriz... ")
		ProcessMessage() //ATUALIZA CLIENTE

		aSF1:=	{}
		lMsErroAuto	:= .F.
		dbSelectArea("SF1")
		SF1->(dbSetOrder(1))
		If SF1->(dbSeek(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NF03FL')+ cNota + oModel:GetModel('ZZHMASTER'):GetValue('ZZH_SER03')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FONF03')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_LJNF03')+"D"))
			aAdd(aSF1,	{'F1_FILIAL'	,SF1->F1_FILIAL		,NIL})
			aAdd(aSF1,	{'F1_TIPO'		,SF1->F1_TIPO		,NIL})
			aAdd(aSF1,	{'F1_FORMUL'	,SF1->F1_FORMUL		,NIL})
			aAdd(aSF1,	{'F1_DOC'		,SF1->F1_DOC   		,NIL})
			aAdd(aSF1,	{'F1_SERIE'		,SF1->F1_SERIE		,NIL})
			aAdd(aSF1,	{'F1_EMISSAO'	,SF1->F1_EMISSAO	,NIL})
			aAdd(aSF1,	{'F1_FORNECE'	,SF1->F1_FORNECE	,NIL})
			aAdd(aSF1,	{'F1_LOJA'		,SF1->F1_LOJA		,NIL})

			cChave	:=	SF1->(F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)
			dbSelectArea("SD1")
			SD1->(dbSetOrder(1))
			SD1->(dbSeek(cChave)) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA

			aItensD1:={}
			While SD1->(!eof()) .and. cChave == SD1->(D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA)
				aLinha := {}

				aAdd(aLinha,	{'D1_FILIAL'	,SD1->D1_FILIAL		,NIL})
				aAdd(aLinha,	{'D1_ITEM'		,SD1->D1_ITEM		,NIL})
				aAdd(aLinha,	{'D1_COD'		,SD1->D1_COD		,NIL})
				aAdd(aLinha,	{'D1_QUANT'		,SD1->D1_QUANT		,NIL})
				aAdd(aLinha,	{'D1_VUNIT'		,SD1->D1_VUNIT		,NIL})
				aAdd(aLinha,	{'D1_TOTAL'		,SD1->D1_TOTAL		,NIL})

				AAdd(aItensD1, aLinha)

				SD1->(dbSkip())
			Enddo  

			MSExecAuto({|x,y,z| MATA140(x,y,z)}, aSF1, aItensD1 , 5)

		Endif
		cFilant := cFilBak
		If lMsErroAuto
			cFilant := cFilBak
			AutoGrLog("Erro ao excluir Pr� Nota, Cliente para a Comercial !")
			MostraErro()
			Return .F.
		endif

		cMSG += "Excluido Pr� Nota , Filial : "+cFilAnt+" Codigo: "+SF1->F1_DOC+""+CHR(13)+CHR(10)+""   

		cFilant := cFilOrig

	ENDIF
Return .T.

Static Function GetTes(cParam,cTes)
	Local cRetTES	:= ""
	Local aTES		:= {}
	Local aTmp		:= Separa(cParam,";",.T.)
	Local nPos
	aEval(aTmp,{|x| If(":"$x,AADD(aTES,Separa(x,":",.T.)),AADD(aTES,Separa(x+":",":",.T.)))})
	nPos	:= aScan(aTES,{|x| x[1]==cTes})
	If nPos>0
		cRetTES	:= aTES[nPos][2]
	EndIf

Return cRetTES

User FUNCTION TRANZZH()

	Local lOk	:= .F.
	Local cArea := GetArea()
	Local cTransportadora	:= Space(GetSx3Cache("A4_COD","X3_TAMANHO"))
	Local cModal	:= Space(GetSx3Cache("ZZ2_MODAL","X3_TAMANHO"))
	Local aModal    := {'AQUAVIARIO','RODOVIARIO','FERROVIARIO ','AEREO'}
	Local cTabFret	:= Space(GetSx3Cache("ZZ2_TABFRT","X3_TAMANHO"))
	Local aTabFret	:= {'EXPRESSO','CONVENCIONAL'}
	Local aParam := {}
	Local aRetParm	:= {}
	Local aTransp := {}
	Local dDataPre := ctod("")
	Local dDataMax := ctod("")
	Local cModal := ""
	Local cTab := ""
	Local nItens := 0
	Local aFrete := {}
	Local nAx := 1 
	Local cNota1 := IIF(ZZH->ZZH_TPMED =='1',ZZH->ZZH_PRNF01,ZZH->ZZH_PC01NF) 
	Local cNota3 :=	IIF(ZZH->ZZH_TPMED =='1',ZZH->ZZH_PRNF03,ZZH->ZZH_NF03)
	Local oModel:= FWModelActive() 

	if ZZH->ZZH_COLETA == "02" 
		MsgInfo("Medi��o sem !! ")
		Return
	endif

	if !(ZZH->ZZH_STATUS == 'LA' .AND. !(ZZH->ZZH_STFAT $ ('F |N2|N3|N4')))
		MsgInfo("Medi��o n�o est� em cota��o !! ")
		Return
	endif

	//Private nFrete := 0
	//Private nUlpar := 0 

	aAdd(aParam,{1,"Transportadora"		,cTransportadora	,GetSx3Cache("A4_COD","X3_PICTURE"),"existCpo('SA4')","SA4",".T.",80,.F.})
	aAdd(aParam,{2,"Modal"      		,cModal	,aModal,80,,.F.,})
	aAdd(aParam,{1,"Valor cotado"  		,0	,"@E 999,999,999.99 ",'.T.',,".T.",80,.F.})
	aAdd(aParam,{2,"Tabela Frete" 		,cTabFret	,aTabFret,80,,.F.,})
	aAdd(aParam,{1,"Data Previs�o",dDataPre,"",'.T.',"",'.T.',60,.F.})

	If ParamBox(aParam,"Escolha da transportadora",@aRetParm,{||.T.},,,,,,"U_DEV002",.T.,.T.)
		lOk	:= .T.
	EndIf

	if lOk

		IF aRetParm[2] == "AQUAVIARIO"
			cModal  := "AQ"
		ELSEIF	aRetParm[2] == "RODOVIARIO"
			cModal  := "RD"
		ELSEIF	aRetParm[2] == "FERROVIARIO"
			cModal  := "FR"
		ELSEIF	aRetParm[2] == "AEREO"
			cModal  := "AE"
		ENDIF

		IF aRetParm[4] == "EXPRESSO"
			cTab  := "1"
		ELSEIF	aRetParm[4] == "CONVENCIONAL"
			cTab  := "2"
		ENDIF

		DBSELECTAREA("ZZH")
		DBSETORDER(1)
		DBSEEK(ZZH->ZZH_FILIAL + ZZH->ZZH_NUMERO + ZZH->ZZH_MEDICA + ZZH->ZZH_CLIENT + ZZH->ZZH_LOJA )

		If Found()

			RECLOCK("ZZH", .F.)

			ZZH->ZZH_TRANSP := aRetParm[1]
			ZZH->ZZH_NOMTRA := POSICIONE("SA4",1,XFILIAL("SA4")+aRetParm[1],"A4_NOME")
			ZZH->ZZH_TPTRAN := cModal
			ZZH->ZZH_TABFRE := cTab
			ZZH->ZZH_FRETE  := aRetParm[3]	
			ZZH->ZZH_DTPREV := aRetParm[5]	
			ZZH->ZZH_STATUS := "LB"

			MSUNLOCK()

		EndIf

		//  PEDIDO DE COMPRA 1
		dbSelectArea("SF1")
		dbSetOrder(1)      // F1_FILIAL + F1_DOC + F1_SERIE + F1_FORNECE + F1_LOJA + F1_TIPO

		dbSeek(ZZH->ZZH_PC01FL + cNota1	+ ZZH->ZZH_SER01 + ZZH->ZZH_FONF01 + ZZH->ZZH_LJNF01 + "D")

		IF FOUND()    // Avalia o retorno da pesquisa realizada
			RECLOCK("SF1", .F.)

			F1_TRANSP := aRetParm[1]

			MSUNLOCK()     // Destrava o registro
			MSGINFO("Atualizado transportado e frete para a medi��o e os Documentos de Entradas")

		ENDIF

		SF1->(dbCloseArea())

		// PEDIDO 3
		dbSelectArea("SF1")
		dbSetOrder(1)      // F1_FILIAL + F1_DOC + F1_SERIE + F1_FORNECE + F1_LOJA + F1_TIPO
		dbSeek(ZZH->ZZH_NF03FL + cNota3 + ZZH->ZZH_SER03 + ZZH->ZZH_FONF03 + ZZH->ZZH_LJNF03 + "D" )

		IF FOUND()    // Avalia o retorno da pesquisa realizada
			RECLOCK("SF1", .F.)
			F1_TRANSP := aRetParm[1]
			MSUNLOCK()     // Destrava o registro
		ENDIF

		SF1->(dbCloseArea())

		aRetParm :={}  //limpando array
		aParam   :={}  //limpando array

		RestArea( cArea )
	endif

Return

Static Function RATFRETE(nItens,nVal)

	Local aFrete := {}
	Local nParce := nVal / nItens
	Local nTotal := 0

	for nAx := 1 to nItens
		aadd(aFrete,{NoRound(nParce,2)})		
		nTotal += aFrete[nAx][1]
	next

	if nTotal < nVal
		aFrete[len(aFrete)][1] += nVal - nTotal 
	endif

RETURN aFrete

Static Function RATDESC(nItens,nVal,nVenda,aItens,nPosQuant,nPosVal)

	Local aDesc  := {}
	Local nParce := nVal / nItens
	Local nTotal := 0
	Local nValor   := 0
	Local nAcumula := 0

	for nAx := 1 to nItens
		nPeso  := aItens[nAx][nPosQuant][2] * (aItens[nAx][nPosVal][2] / nVenda)  
		nParce := nPeso * nVal
		// ADD NOS ITENS 
		aadd(aDesc,{NoRound(nParce ,2)})
		// VALOR JA RATIADO 
		nAcumula += NoRound(nParce ,2)
	next
	if nAcumula < nVal
		aDesc[len(aDesc)][1] += nVal - nAcumula 
	endif

RETURN aDesc

User Function LIBERZZH()

	Local oView   := FWViewActive()
	Local cStatus := ""

	IF !retcodusr() $ getmv("SA_USEMAST") .AND. ZZH_TPMED == "1"
		MsgInfo("Usuario sem permiss�o a liberar a Medi��o de Devolu��o , somente usu�rios Master podem realizar essa rotina!")
		RETURN
	ENDIF

	if MsgYesNo("Medi��o aprovada ?","Aprova��o da Medi��o")
		if ZZH->ZZH_COLETA == "02"
			cStatus := "LB" // LIBERADA PELO APROVADOR
		ELSE
			cStatus := "LA" // LIBERADA PELO APROVADOR
		ENDIF	
	else
		cStatus := "RP"
	endif

	IF MsgYesNo("Deseja confirmar a "+ if(cStatus == "LA" .or. cStatus == "LB","Libera��o","Reprova��o" ) +" ?","Aviso")

		DBSELECTAREA("ZZH")
		DBSETORDER(1)
		DBSEEK(ZZH->ZZH_FILIAL + ZZH->ZZH_NUMERO + ZZH->ZZH_MEDICA + ZZH->ZZH_CLIENT + ZZH->ZZH_LOJA )

		If Found()

			RECLOCK("ZZH", .F.)

			ZZH->ZZH_STATUS := cStatus
			ZZH->ZZH_USERAP := retcodusr()
			ZZH->ZZH_DTAPRO := ddatabase
			ZZH->ZZH_HRAPRO := TIME()	
			IF ZZH->ZZH_COLETA == "02"
				ZZH->ZZH_MATENT:= "01"
			ENDIF	

			MSUNLOCK()
		ENDIF
	endif	
	oView:REFRESH()
RETURN

// FUN��O PARA LIBERAR AJUSTE

User Function AJUZZH()

	Local oView   := FWViewActive()
	Local oModel  := FWModelActive()
	Local cStatus := ""

	if ZZH->ZZH_STATUS == 'AJ'

		IF MSGYESNO("Aprovar ajuste ?")
			cStatus:= 'AV'
		else
			IF MSGYESNO("Reprovar ajuste ?")
				cStatus:= 'AN'
			ENDIF
		endif
	endif

	if cStatus <> ""
		DBSELECTAREA("ZZH")
		DBSETORDER(1)
		DBSEEK(ZZH->ZZH_FILIAL + ZZH->ZZH_NUMERO + ZZH->ZZH_MEDICA + ZZH->ZZH_CLIENT + ZZH->ZZH_LOJA )

		If Found()

			RECLOCK("ZZH", .F.)

			ZZH->ZZH_STATUS := cStatus

			MSUNLOCK()
		ENDIF
	endif

Return 


//Fun��o SASP034 - Fun��o para retornar o kit - Arvore.
Static Function ProcKit(oView)	

	Local oModel:= FWModelActive()
	Local cKits	:= ""

	IF ALLTRIM(oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_MEDORG')) == ""
		MsgInfo("Preencha primeiro a medi��o de origem!!")
		Return
	ENDIF
	//Busca todos os itens do contrato que sejam kits

	cQuery:= "SELECT ZZ3_PRODUT FROM "+RetSqlName("ZZ3")+ " (NOLOCK) "
	cQuery+= " WHERE  "
	cQuery+= " ZZ3_FILIAL = '"+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FILIAL')+"' AND "
	cQuery+= " ZZ3_NUMERO = '"+oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_NUMERO')+"' AND "
	cQuery+= " ZZ3_CLIENT = '"+oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_CLIENT')+"' AND "
	cQuery+= " ZZ3_MEDICA = '"+oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_MEDORG')+"' AND "
	cQuery+= " ZZ3_LOJA   = '"+oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_LOJA')+"'   AND "
	cQuery+= " ZZ3_TIPO = 'K'  "
	cQuery+= " AND D_E_L_E_T_ =''  "

	TCQuery cQuery new alias T01KIT

	While !eof()
		cKits+= "'"+T01KIT->ZZ3_PRODUT+"',"
		T01KIT->(dbSkip())
	Enddo

	If !empty(cKits)
		cKits:= substr(cKits,1,len(cKits)-1)
	Endif 

	T01KIT->(DbCloseArea())
	//U_SASP034('ALTMED')
	U_SASP034('DEVOL'," ( (ZZ7_CODIGO IN ("+cKits+")  ) OR (ZZ7_CODPAI IN ("+cKits+") ) )")

Return

// VALIDA��O DA LINHA , ONDE VAI SER VERIFICADO A EXISTENCIA DO PRODUTO NA MEDI�AO DE ORIGEM E A QUANTIDADE JA DEVOLVIDA 
Static Function LINHAZZV(oModelGrid,cId,xValor,oModel)

	Local oModelZZV	:= oModel:GetModel('ZZVDETAIL')
	Local oModelZZH	:= oModel:GetModel('ZZHMASTER')
	Local cProduto  := oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_PRODUT')
	Local cMedica   := oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_MEDORG')
	Local cClient   := oModel:GetModel('ZZHMASTER'):GetValue('ZZH_CLIENT')
	Local cLoja     := oModel:GetModel('ZZHMASTER'):GetValue('ZZH_LOJA')
	Local cNumero   := oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NUMERO')
	Local cTes      := oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_TES')

	if oModel:GetOperation() == 4
		dbSelectArea("ZZ3")
		dbSetOrder(1)

		IF dbseek(XFILIAL("ZZ3")+cNumero+cMedica+cClient+cLoja+cProduto+cTes) 

			IF oModelZZV:IsDeleted()
				Return .F.	
			ELSE

				IF ZZ3->ZZ3_QTDEV0 < ZZ3->ZZ3_QUANT //POSICIONE("ZZ3",1,oModelZZH:GETVALUE('ZZH_FILIAL')+cNumero+cMedica+cClient+cLoja+cProduto,cTes,"ZZ3_QTDEV0") < POSICIONE("ZZ3",1,oModelZZH:GETVALUE('ZZH_FILIAL')+cNumero+cMedica+cClient+cLoja+cProduto,cTes,"ZZ3_QUANT") 

					oModel:SetValue("ZZVDETAIL", "ZZV_TIPO" , ZZ3->ZZ3_TIPO)
					oModel:SetValue("ZZVDETAIL", "ZZV_SALDO" , ZZ3->ZZ3_QUANT - ZZ3->ZZ3_QTDEV0)
					oModel:SetValue("ZZVDETAIL", "ZZV_QTDEVO" , ZZ3->ZZ3_QTDEV0)
					oModel:SetValue("ZZVDETAIL", "ZZV_ENVIO" , ZZ3->ZZ3_ENVIO)
					oModel:SetValue("ZZVDETAIL", "ZZV_ITEMMD" , ZZ3->ZZ3_ITEM)			
					oModel:SetValue("ZZVDETAIL", "ZZV_MOV" , oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_QUANT') )

					dbSelectArea("ZZ2")
					dbSetOrder(1)
					dbseek(XFILIAL("ZZ2")+cNumero+cMedica+cClient+cLoja) 

					oModel:SetValue("ZZVDETAIL", "ZZV_FILIAL" , ZZ2->ZZ2_FILIAL)
					oModel:SetValue("ZZVDETAIL", "ZZV_NF1ORG" , ZZ2->ZZ2_PV01FL+ZZ2->ZZ2_SER01+ZZ2->ZZ2_PV01NF+cProduto) // FILIAL + SERIE + DOC + ITEM
					oModel:SetValue("ZZVDETAIL", "ZZV_NF2ORG" , ZZ2->ZZ2_PV02FL+ZZ2->ZZ2_SER02+ZZ2->ZZ2_PV02NF+cProduto)
					oModel:SetValue("ZZVDETAIL", "ZZV_NF3ORG" , ZZ2->ZZ2_PV03FL+ZZ2->ZZ2_SER03+ZZ2->ZZ2_PV03NF+cProduto)		

					RETURN .T.

				ELSEIF POSICIONE("ZZ3",1,oModelZZH:GETVALUE('ZZH_FILIAL')+cNumero+cMedica+cClient+cLoja+cProduto,cTes,"ZZ3_QTDEV0") == POSICIONE("ZZ3",1,oModelZZH:GETVALUE('ZZH_FILIAL')+cNumero+cMedica+cClient+cLoja+cProduto,cTes,"ZZ3_QUANT")		
					MsgInfo('Produto 100% devolvido!')
					Return .F.	
				ENDIF
			ENDIF	
		ELSE 
			MsgInfo('Produto n�o encontrado na medi��o de origem !')
			Return .F.
		ENDIF	
		dbSelectArea("ZZ3")

	ENDIF
RETURN .T.

// VALIDA MEDI��O DE ORIGEM DIGITADA 
Static Function MDORGZZV(oModelGrid,cId,xValor,oModel)

	Local oModelZZV	:= oModel:GetModel('ZZVDETAIL')
	Local oModelZZH	:= oModel:GetModel('ZZHMASTER')
	Local lRet := .T.
	Local cFil      := oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FILIAL')
	Local cNumero   := oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NUMERO')
	Local cMedica   := oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_MEDORG')
	Local cClient   := oModel:GetModel('ZZHMASTER'):GetValue('ZZH_CLIENT')
	Local cLoja     := oModel:GetModel('ZZHMASTER'):GetValue('ZZH_LOJA')
	Local cTipo     := oModel:GetModel('ZZHMASTER'):GetValue('ZZH_ESPEC')
	Local oModel    := FWModelActive()
	Local cMedOrig  := POSICIONE("ZZ2",1,cFil+cNumero+xValor+cClient+cLoja,"ZZ2_MEDICA")
	Local cTes      := ""

	if oModel:GetOperation() == 4

		cQuery:= " SELECT ZZ3_TES FROM "+RetSqlName("ZZ3")+ " (NOLOCK) "
		cQuery+= " WHERE "
		cQuery+= " ZZ3_FILIAL = '"+cFil+"' AND "
		cQuery+= " ZZ3_NUMERO = '"+cNumero+"' AND "
		cQuery+= " ZZ3_MEDICA = '"+xValor+"' AND "
		cQuery+= " ZZ3_CLIENT = '"+cClient+"' AND	"
		cQuery+= " ZZ3_LOJA = '"+cLoja+"' "
		cQuery+= " D_E_L_E_T_ = ''  "

		TCQuery cQuery new alias T20

		cTes := T20->ZZ3_TES

		T20->(DBCLOSEAREA())

		dbSelectArea("ZZ2")
		dbSetOrder(1)

		IF dbseek(cFil+cNumero+xValor+cClient+cLoja)
			if VAL(ZZ2->ZZ2_TIPCTR) == VAL(cTipo)
				if ALLTRIM(ZZ2->ZZ2_PV01NF) == ""
					MsgInfo('Medi��o n�o faturada!')
					lRet := .F.
				ELSE
					if cTes <> oModel:GetModel('ZZVDETAIL'):GetValue('ZZV_TES')
						MSGINFO("TES DIVERGENTE !!. Escolha uma medi��o de origem com mesma TES")
						lRet := .F.
					ENDIF
				ENDIF
			ELSE
				MsgInfo('Medi��o com tipo divergente!')
				lRet := .F.
			ENDIF
		else
			MsgInfo('Medi��o nao Encontrada!')
			lRet := .F.
		ENDIF 

	endif
Return lRet

// CARREGA CODIGO DO KI 
User Function MEDZZV(cCodKit, aValor,lPrimeiro)

	Local oModel    := FWModelActive()

	oModel:GetModel('ZZVDETAIL'):SetValue('ZZV_PRODUT',cCodKit)
	if	ALLTRIM(POSICIONE("ZZ7",1,XFILIAL("ZZ7")+cCodKit,'ZZ7_DESCR')) <> ""
		oModel:SetValue("ZZVDETAIL", "ZZV_DESCRI" , POSICIONE("ZZ7",1,XFILIAL("ZZ7")+cCodKit,'ZZ7_DESCR'))
		oModel:SetValue("ZZVDETAIL", "ZZV_TIPO" , "K")
	else
		oModel:SetValue("ZZVDETAIL", "ZZV_DESCRI" , POSICIONE("SB1",1,XFILIAL("SB1")+cCodKit,'B1_DESC'))
		oModel:SetValue("ZZVDETAIL", "ZZV_TIPO" , "P")
	endif	



Return

Static Function VALDALT(oModel)
	Local lRet := .T.

	IF  ZZH->ZZH_STFAT  $ ("F |N2|N3|N4") .AND. (oModel:GetOperation() == 4 .OR. oModel:GetOperation() == 5 )
		MsgInfo("Medi��o j� faturada !!!")
		Return .F.
	EndIf

	if !(ZZH->ZZH_STATUS == 'BL' .AND. !(ZZH->ZZH_STFAT $ ('F |N2|N3|N4'))) .AND. (oModel:GetOperation() == 4 )
		MsgInfo("Medi��o n�o pode ser alterada!!")
		Return .F.
	endif


Return lRet