#include 'protheus.ch'
#include 'parmtype.ch'

user function RCOMCZZ7() //(cFil,cContrato,cMedica,cProd)

	//Local cCampo    := POSICIONE('ZA7',1,xFilial('ZA7')+M->ZA4_CODVEN,'ZA7_TIPO') //AllTrim(Upper(ReadVar()))
	Local cQuery	:= ""
	Local aCabec    := {}
	Local aOrde		:= {}
	Local lRet
	Local Backreadvar	:= __readvar
	Local cFil := FIMDZZ7   
	Local cContrato := Cotrz7   
	Local cMedica := Medzz7   
	Local cProd := Prdzz7   
	//VER TUDO
	aCabec	:= {"Codigo Kit","Descricao"," Produto"}
	aOrde	:= {"ZZ7_CODPAI","ZZ7_DESCR","ZZ7_PRODUT"}	

	//VER TUDO
	//Seleciona a medi��o referente ao Pedido
		cQuery := " SELECT ZZ7_CODPAI,ZZ7_DESCR,ZZ7_PRODUT "
		cQuery += " FROM "+RetSqlName("ZZ7")+" ZZ7 "
		cQuery += " WHERE ZZ7_CODPAI IN ( " 
			cQuery += " SELECT ZZ3_PRODUT  "
			cQuery += " FROM "+RetSqlName("ZZ3")+" ZZ3		"
			cQuery += " WHERE ZZ3_NUMERO = '"+cContrato+"'	"
			cQuery += " AND ZZ3_MEDICA = '"+cMedica+"'	"
			cQuery += " AND ZZ3_FILIAL = '"+cFil+"'	"
			//cQuery += " AND ZZ3_PRODUT = '"+cKit+"'	"
			cQuery += " AND ZZ3.D_E_L_E_T_ = ' '	"
			cQuery += " AND ZZ3_TIPO = 'K' 			"
		cQuery += " )  "
		cQuery += " AND ZZ7.D_E_L_E_T_ =' '  "
		cQuery += " AND ZZ7_TIPO = 'A' "
		cQuery += " AND ZZ7_PRODUT = '"+cProd+"'  "


	lRet:= U_RConEspT(cQuery,aCabec,aOrde,1,"Kits")
	__readvar := Backreadvar

Return lRet