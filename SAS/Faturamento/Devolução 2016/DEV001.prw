#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#Include "RWMAKE.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"


/*/{Protheus.doc} DEV001
TELA DE MEDICAO DE DEVOLU��O
@author Jo�o Filho 
@since 14/12/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

Static aCacheTES	:= {}

User function DEV001()

	Local aArea := getarea()
	Local oBrowse
	Local aCoors	 := FwGetDialogSize( oMainWnd )
	Local oFWLayer,oPanelTop1,oView
	Local bBlockClique
	Local aFiltros   := {}
	Private aMedicao := {} 
	Private oDlg
	Private aRotina	:= {}
	Static nFilial   := 1
	Static nContra   := 2
	Static nMedia    := 3
	Static nCodCli   := 4
	Static nLjClia   := 5
	Static nNomCli   := 6	
	Static nDtGera   := 7
	Static nNF01     := 8
	Static nNF02     := 9
	Static nNF03     := 10
	Static nOK       := 11
	Static nTpMed	 := 12
	Static nTes		 := 13

	aMedicao := U_RDevolu(aMedicao)


	oDlg		:= MSDialog():New( aCoors[1],aCoors[2],aCoors[3],aCoors[4],"Medi��o de Devolu��a",,,.F.,,,,,,.T.,,,.T. )
	//oDlg		:= MSDialog():New( 0,0,500,700,"Planejamento e Controle de Expedi��o",,,.F.,,,,,,.T.,,,.T. )
	oFWLayer	:= FWLayer():New()
	oFWLayer:Init( oDlg, .F., .T. )
	oFWLayer:AddLine( 'TOP1', 100, .F. )
	oPanelTop1 := oFWLayer:getLinePanel( 'TOP1' )

	oBrowse	    := FWFormBrowse():New()
	oMarkBrow	:= oBrowse:FWBrowse()
	oMarkBrow:SetDataArray()
	oMarkBrow:SetArray(@aMedicao)
	oMarkBrow:SetDescription('Devolu��o')
	oMarkBrow:SetOwner( oDlg )
	oMarkBrow:SetProfileID("4")
	oMarkBrow:SetMenuDef("")
	oMarkBrow:SetDoubleClick({|| nil })

	bBlockClique := {|| U_CLICKDV() }

	// bot�es

	//oMarkBrow:AddButton("Atualizar",{||YATUALIZ(aMedicao,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Bloquear",{||YBLOQMED(aMedicao,oMarkBrow,1)},,,)
	//oMarkBrow:AddButton("Desbloquear",{||YDSBLQMED(aMedicao,oMarkBrow,1)},,,)
	//oMarkBrow:AddButton("Transportadora",{||YTRANSP(aMedicao,oMarkBrow,1)},,,)
	//oMarkBrow:AddButton("Confirmar ",{||YCONFIRMA(aMedicao,oMarkBrow,1)},,,)
	//oMarkBrow:AddButton("Pedido 01 ",{||YPED01(aMedicao,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Pedido 02 ",{||YPED02(aMedicao,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Pedido 03 ",{||YPED03(aMedicao,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Alterar Medi��o ",{||YALTMED(aMedicao,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Impressao ",{||YIMPRIMIR(aMedicao,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Historico ",{||YHISTOR(aMedicao,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Placa de embarque ",{||YPLACA(aMedicao,oMarkBrow)},,,)

	//oMarkBrow:AddButton("Devolver",{||U_DEV002(aMedicao,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Devolver",{||FWExecView('Inclusao por FWExecView','DEV002', MODEL_OPERATION_INSERT, , { || .T. }, , , )},,,)//aButtons
	oMarkBrow:AddButton("Devolver",{||FilDEV02(aMedicao,oMarkBrow)},,,)

	// legenda
	oMarkBrow:AddLegend({||aMedicao[oMarkBrow:At()][nNF01]  == "" }  ,"BR_PRETO",     "Medi��o n�o Faturada")
	oMarkBrow:AddLegend({||aMedicao[oMarkBrow:At()][nNF01]  <> "" }  ,"BR_VERDE",     "Medi��o Faturada")
		
	//----------------------------------------------------------------------------------------------------------------

	// FILTROS 
	oBrowse:SetUseFilter()

	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nFilial)+"]",GetSx3Cache("ZZR_FILIAL","X3_TITULO"),"C",GetSx3Cache("ZZR_FILIAL","X3_TAMANHO"),0,GetSx3Cache("ZZR_FILIAL","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nContra)+"]",GetSx3Cache("ZZR_CONTRA","X3_TITULO"),"C",GetSx3Cache("ZZR_CONTRA","X3_TAMANHO"),0,GetSx3Cache("ZZR_CONTRA","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nTes)+"]" ,GetSx3Cache("ZZV_TES","X3_TITULO"),"C",GetSx3Cache("ZZV_TES","X3_TAMANHO"),0,GetSx3Cache("ZZV_TES","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nMedia)+"]" ,GetSx3Cache("ZZR_MEDICA","X3_TITULO"),"C",GetSx3Cache("ZZR_MEDICA","X3_TAMANHO"),0,GetSx3Cache("ZZR_MEDICA","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nTpMed)+"]" ,GetSx3Cache("ZZ2_TIPCTR","X3_TITULO"),"C",GetSx3Cache("ZZ2_TIPCTR","X3_TAMANHO"),0,GetSx3Cache("ZZ2_TIPCTR","X3_PICTURE")} )	
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nCodCli)+"]",GetSx3Cache("ZZR_CODCLI","X3_TITULO"),"C",GetSx3Cache("ZZR_CODCLI","X3_TAMANHO"),0,GetSx3Cache("ZZR_CODCLI","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nLjClia)+"]",GetSx3Cache("ZZ2_LOJA","X3_TITULO"),"C",GetSx3Cache("ZZ2_LOJA","X3_TAMANHO"),0,GetSx3Cache("ZZ2_LOJA","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nNomCli)+"]",GetSx3Cache("ZZ2_NOME","X3_TITULO"),"C",GetSx3Cache("ZZ2_NOME","X3_TAMANHO"),0,GetSx3Cache("ZZ2_NOME","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nDtGera)+"]"  ,GetSx3Cache("ZZ2_DTGER","X3_TITULO"),"D",GetSx3Cache("ZZ2_DTGER","X3_TAMANHO"),0,GetSx3Cache("ZZ2_DTGER","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nNF01)+"]",GetSx3Cache("ZZ2_PV01NF","X3_TITULO"),"C",GetSx3Cache("ZZ2_PV01NF","X3_TAMANHO"),0,GetSx3Cache("ZZ2_PV01NF","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nNF02)+"]",GetSx3Cache("ZZ2_PV01NF","X3_TITULO"),"C",GetSx3Cache("ZZ2_PV01NF","X3_TAMANHO"),0,GetSx3Cache("ZZ2_PV01NF","X3_PICTURE")} )
	Aadd( aFiltros, { "aMedicao[oBrowse:At()]["+cvaltochar(nNF03)+"]",GetSx3Cache("ZZ2_PV01NF","X3_TITULO"),"C",GetSx3Cache("ZZ2_PV01NF","X3_TAMANHO"),0,GetSx3Cache("ZZ2_PV01NF","X3_PICTURE")} )


	oBrowse:SetFieldFilter(aFiltros)



	// BOX
	oMarkBrow:AddMarkColumns(;
	{||If(aMedicao[oMarkBrow:At()][nOk]=="","LBNO","LBTICK")};	//Imagem da marca
	,bBlockClique/*{|| YINVERTE(oMarkBrow)}*/;
	,{|| YINVERTE(oMarkBrow)})

	//Filial
	oMarkBrow:SetColumns({{;
	"Filial",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nFilial],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_FILIAL","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_FILIAL","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Numero do Contrato
	oMarkBrow:SetColumns({{;
	"Numero",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nContra],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_NUMERO","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_NUMERO","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// TES
	oMarkBrow:SetColumns({{;
	"Tes",;  						   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nTes],"")},;      // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ3_TES","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ3_TES","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)


	//Medicao
	oMarkBrow:SetColumns({{;
	"Medi�ao",;  					   				// T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nMedia],"")},;		// Code-Block de carga dos dados
	"C",;                       			// Tipo de dados
	"@!",;                       			// M�scara
	1,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_MEDICAO","X3_TAMANHO"),;                       			// Tamanho
	GetSx3Cache("ZZ2_MEDICAO","X3_DECIMAL"),;                       			// Decimal
	.F.,;                                       			// Indica se permite a edi��o
	{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       			// Indica se exibe imagem
	bBlockClique,;                     			// Code-Block de execu��o do duplo clique
	NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			// Code-Block de execu��o do clique no header
	.F.,;                                       			// Indica se a coluna est� deletada
	.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Tipo da Medicao
	oMarkBrow:SetColumns({{;
	"Tipo Medi�ao",;  					   				// T�tulo da coluna
	{|| If(!Empty(aMedicao),if(aMedicao[oMarkBrow:At()][nTpMed]=="1","Direto",if(aMedicao[oMarkBrow:At()][nTpMed]=="2","Triangular","Indefinido")),"")},;		// Code-Block de carga dos dados
	"C",;                       			// Tipo de dados
	"@!",;                       			// M�scara
	1,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	12,;                       			// Tamanho
	0,;                       			// Decimal
	.F.,;                                       			// Indica se permite a edi��o
	{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       			// Indica se exibe imagem
	bBlockClique,;                     			// Code-Block de execu��o do duplo clique
	NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			// Code-Block de execu��o do clique no header
	.F.,;                                       			// Indica se a coluna est� deletada
	.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//cod cliente
	oMarkBrow:SetColumns({{;
	"Cod Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nCodCli],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_CLIENT","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_CLIENT","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//Loja Cliente
	oMarkBrow:SetColumns({{;
	"Loja Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nLjClia],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_LOJA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_LOJA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// NOME CLIENTE
	oMarkBrow:SetColumns({{;
	"Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nNomCli],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A1_NOME","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("A1_NOME","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//DATA GERA��O DA MEDICAO
	oMarkBrow:SetColumns({{;
	"Data Ger.Medi��o",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nDtGera],"")},;  // Code-Block de carga dos dados
	"D",;                                                        	   // Tipo de dados
	"",;                       			                               // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_DTGER","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_DTGER","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//NF 01
	oMarkBrow:SetColumns({{;
	"Num NF 01",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nNF01],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_PV01NF","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_PV01NF","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//NF 02
	oMarkBrow:SetColumns({{;
	"Num NF 02",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nNF02],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_PV01NF","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_PV01NF","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//NF 03
	oMarkBrow:SetColumns({{;
	"Num NF 03",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aMedicao),aMedicao[oMarkBrow:At()][nNF03],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("ZZ2_PV01NF","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("ZZ2_PV01NF","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)


	oMarkBrow:SetMenuDef( 'DEV001' )

	oMarkBrow:Activate()

	oDlg:Activate(,,,.T.)

	RestArea(aArea)

	//Reset Environment
	aMedicao := {}

Return

user function RDevolu(aMedicao)

	Local cArea   := GetArea()
	Local cStatus := ""
	Local cFilAcesso := ""
	Local cNomeUser
	Local aRetUser := {}
	Local cDepto
	Local cEspecie := ""
	Local cNomeUser := ""

	cNomeUser := substr(cUsuario,7,15)

	// Defino a ordem
	PswOrder(2) // Ordem de nome

	// Efetuo a pesquisa, definindo se pesquiso usu�rio ou grupo
	If PswSeek(cNomeUser,.T.)

		// Obtenho o resultado conforme vetor
		aRetUser := PswRet(2)

		cDepto   := upper(alltrim(aRetUser[1,12]))

	EndIf
	IF LEN(aRetUser) > 0
		For nAx := 1 to len(aRetUser[1][6])
			if nAx == 1
				cFilAcesso += "'"+substr(aRetUser[1][6][nAx],3,6)+"'"
			else
				cFilAcesso +=" , '"+substr(aRetUser[1][6][nAx],3,6)+"'"
			endif

		Next
	ENDIF
	
	cQuery := " SELECT DISTINCT ZZ3_TES ,ZZ2_FILIAL , ZZ2_NUMERO , ZZ2_MEDICA ,ZZ2_CLIENT ,ZZ2_LOJA , ZZ2_DTGER , ZZ2_PV01NF , ZZ2_PV02NF , ZZ2_PV03NF , ZZ2_TIPCTR "
	cQuery += " FROM   "+RetSqlName("ZZ3")+" ZZ3 ,  "
	cQuery += " (  "
	cQuery += " SELECT * 
	cQuery += " FROM "+RetSqlName("ZZ2")+" ZZ2  "
	cQuery += " WHERE  ZZ2.D_E_L_E_T_ = ''  "
	if cFilAcesso <> "" .and. cFilAcesso <> "'@@'"
		cQuery += " AND    ZZ2_FILIAL IN ("+cFilAcesso+")  "
	endif
	cQuery += " AND    ZZ2_PV01NF <> ''  "
	cQuery += " ) A "
	cQuery += " WHERE A.ZZ2_FILIAL  = ZZ3_FILIAL "
	cQuery += " AND A.ZZ2_NUMERO  = ZZ3_NUMERO "
	cQuery += " AND A.ZZ2_MEDICA  = ZZ3_MEDICA "
	cQuery += " AND A.ZZ2_CLIENT  = ZZ3_CLIENT "
	cQuery += " AND A.ZZ2_LOJA    = ZZ3_LOJA "
	
	cQuery += " ORDER BY ZZ2_FILIAL , ZZ2_NUMERO , ZZ2_MEDICA  "
	TcQuery cQuery New Alias T01

	WHILE !T01->(EOF())
		cStatus := ""
		// VIRIFICA��O DO BLOQUEIO DE CLIENTE

		if ALLTRIM(T01->ZZ2_PV01NF) == ""
			cStatus := "1"
		elseif ALLTRIM(T01->ZZ2_PV01NF) <> ""
			cStatus := "2"
		endif

		//cEspecie := POSICIONE("ZZG",1,XFILIAL("ZZG") + T01->ZZ2_TIPO, "ZZG->ZZG_ESPCTR" )

		aadd(aMedicao,;
		{ALLTRIM(T01->ZZ2_FILIAL),;
		ALLTRIM(T01->ZZ2_NUMERO),;
		ALLTRIM(T01->ZZ2_MEDICA),;
		ALLTRIM(T01->ZZ2_CLIENT),;
		ALLTRIM(T01->ZZ2_LOJA),;
		ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+T01->ZZ2_CLIENT+T01->ZZ2_LOJA ,"A1_NOME")) ,;
		STOD(ALLTRIM(T01->ZZ2_DTGER))  ,;		
		ALLTRIM(T01->ZZ2_PV01NF) ,;
		ALLTRIM(T01->ZZ2_PV02NF) ,;
		ALLTRIM(T01->ZZ2_PV03NF) ,;
		"",;
		ALLTRIM(T01->ZZ2_TIPCTR) ,;
		ALLTRIM(T01->ZZ3_TES)})
		T01->(dbSkip())
	ENDDO

	T01-> (dbclosearea())

return aMedicao

USER FUNCTION CLICKDV()
	Local nTem  := 0
	Local cTipo := "" 

	IF aMedicao[oMarkBrow:At()][nOk] == ""
		for nAx := 1 to len(aMedicao)


			if alltrim(aMedicao[nAx][nFilial]) + alltrim(aMedicao[nAx][nContra]) + alltrim(aMedicao[nAx][nCodCli]) + alltrim(aMedicao[nAx][nLjClia]) <> alltrim(aMedicao[oMarkBrow:At()][nFilial]) + alltrim(aMedicao[oMarkBrow:At()][nContra]) + alltrim(aMedicao[oMarkBrow:At()][nCodCli]) + alltrim(aMedicao[oMarkBrow:At()][nLjClia]) .and. aMedicao[nAx][nOK] == '1' 
				nTem := 1
				MSGINFO("Selecionado contrato diferentes, selecione apenas medi��es do mesmo contrato !")
				RETURN
			endif
			IF alltrim(aMedicao[nAx][nTpMed]) <> alltrim(aMedicao[oMarkBrow:At()][nTpMed]) .and. aMedicao[nAx][nOK] == '1'

				nTem  := 2				
				MSGINFO("Medi��es com diferente tipo, selecione apenas medi��es do tipo "+ if(alltrim(aMedicao[nAx][nTpMed]) == "1","DIRETO",if(alltrim(aMedicao[nAx][nTpMed]) == "2","TRIANGULAR","TIPO NAO IDENTIFICADO")) +" !") //01 DIRETO 
				RETURN

			endif
			IF alltrim(aMedicao[nAx][nTes]) <> alltrim(aMedicao[oMarkBrow:At()][nTes]) .and. aMedicao[nAx][nOK] == '1'

				nTem  := 3				
				MSGINFO("Medi��es com diferente TES, selecione apenas medi��es com TES: "+ alltrim(aMedicao[nAx][nTes]) +" !") //01 DIRETO 
				RETURN

			endif
			

		next

		if nTem == 0
			aMedicao[oMarkBrow:At()][nOk]:= "1" 			
		endif

	else
		aMedicao[oMarkBrow:At()][nOk] := ""
	endif	

RETURN

STATIC FUNCTION YATUALIZ(aMedicao,oMarkBrow)


	aMedicao:= {}
	U_RDevolu(aMedicao)
	oMarkBrow:SetArray(@aMedicao)
	oMarkBrow:refresh()
	oBrowse:refresh()
	oMarkBrow:goto(1,.T.)

Return

// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO MARCADO
STATIC FUNCTION YINVERTE(oMarkBrow)

	FOR nAx := 1 to len(aMedicao)
		IF aMedicao[nAx][nOK] == "1"
			aMedicao[nAx][nOK] := ""
		ELSE
			aMedicao[nAx][nOK] := "1"
		ENDIF
	NEXT

	oMarkBrow:refresh()
RETURN

//-------------------------------------------------------------------

//-------------------------------------------------------------------
Static Function MenuDef()

	Local aRotina := {}
	Public cFlag := ""
	Public aKit := {}

	ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PesqBrw"			OPERATION 1 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.DEV001"	OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.DEV001"	OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.DEV001"	OPERATION 4 ACCESS 143
	ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.DEV001"	OPERATION 5 ACCESS 144
	ADD OPTION aRotina TITLE "Imprimir"   ACTION "VIEWDEF.DEV001"	OPERATION 8 ACCESS 0

	//If (Alltrim(RetCodUsr()) $ Alltrim(GetMv("SA_USUAPRO")))
	//	ADD OPTION aRotina TITLE "Aprova��o"  ACTION "YZZ0001()"		OPERATION 10 ACCESS 0 DISABLE MENU
	//EndIf

	//ADD OPTION aRotina TITLE "Medi��o"    				ACTION "YZZ0002()"	OPERATION 4 ACCESS 0 DISABLE MENU
	//ADD OPTION aRotina TITLE "Medi��o Automatica"   	ACTION "U_SASP035()"	OPERATION 4 ACCESS 0 DISABLE MENU
	//ADD OPTION aRotina TITLE "Encerramento"    			ACTION "YZZ0009()"	OPERATION 4 ACCESS 0 DISABLE MENU
	//ADD OPTION aRotina TITLE "Aditivo"    				ACTION "YZZ0003()"	OPERATION 4 ACCESS 0 DISABLE MENU
	//ADD OPTION aRotina TITLE "Ativar/Inativar Itens"    ACTION "U_SASP033()"	OPERATION 4 ACCESS 0 DISABLE MENU
	//ADD OPTION aRotina TITLE "Ver Adiantamentos"    	ACTION "YZZ0008()"	OPERATION 2 ACCESS 0 DISABLE MENU
	//If (Alltrim(RetCodUsr()) $ Alltrim(GetMv("SA_USUAPRO")))
	//	ADD OPTION aRotina TITLE "Bloquear Contrato"    	ACTION "YZZ0006()"	OPERATION 4 ACCESS 0 DISABLE MENU
	//	ADD OPTION aRotina TITLE "Desbloquear Contrato"    	ACTION "YZZ0007()"	OPERATION 4 ACCESS 0 DISABLE MENU
	//Endif	

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	Local oStructZZ2 := Nil
	Local oStructZZ3 := Nil
	Local oModel     := Nil
	Local nI

	//-----------------------------------------
	//Monta a estrutura do formul�rio com base no dicion�rio de dados
	//-----------------------------------------
	oStructZZ2 := FWFormStruct(1,"ZZ2")
	oStructZZ3 := FWFormStruct(1,"ZZ3")
	/*
	oStructZZ0:AddTrigger( ;
	"ZZ0_CLIENT"		,;										//[01] Id do campo de origem
	"ZZ0_NOME"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SA1",1,xFilial("SA1")+oModel:GetValue("ZZ0_CLIENT"),"SA1->A1_NOME") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ1:AddTrigger( ;
	"ZZ1_QUANT"			,;										//[01] Id do campo de origem
	"ZZ1_VLRTOT"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ1_QUANT") * oModel:GetValue("ZZ1_VLRUN") }  )	// [04] Bloco de codigo de execu��o do gatilho

	//Atualizar o saldo conforme as medi��es.	
	oStructZZ1:AddTrigger( ;
	"ZZ1_QUANT"			,;										//[01] Id do campo de origem
	"ZZ1_SALDO"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ1_QUANT") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ1:AddTrigger( ;
	"ZZ1_VLRUN"		,;										//[01] Id do campo de origem
	"ZZ1_VLRTOT"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ1_QUANT") * oModel:GetValue("ZZ1_VLRUN") }  )	// [04] Bloco de codigo de execu��o do gatilho


	oStructZZ1:AddTrigger( ;
	"ZZ1_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ1_DESC"			,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("SB1",oModel:GetValue('ZZ1_PRODUT'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SB1",1,xFilial("SB1")+oModel:GetValue("ZZ1_PRODUT"),"SB1->B1_DESC") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ1:AddTrigger( ;
	"ZZ1_PRODUT"		,;										//[01] Id do campo de origem
	"ZZ1_VLRDSC"		,;										//[02] Id do campo de destino
	{ |oModel| ExistCpo("ZZ7",oModel:GetValue('ZZ1_PRODUT'),1,,.F.) }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| RETDSCZZ0(@oModel) }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ0:AddTrigger( ;
	"ZZ1_CLIENT"		,;										//[01] Id do campo de origem
	"ZZ1_NOME"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SA1",1,xFilial("SA1")+oModel:GetValue('ZZ0_CLIENT')+oModel:GetValue('ZZ0_LOJA'),"A1_NOME") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ0:AddTrigger( ;
	"ZZ1_LOJA"		,;											//[01] Id do campo de origem
	"ZZ1_NOME"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| Posicione("SA1",1,xFilial("SA1")+oModel:GetValue('ZZ0_CLIENTE')+oModel:GetValue('ZZ0_LOJA'),"A1_NOME") }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ1:AddTrigger( ;
	"ZZ1_VLRUN"		,;										//[01] Id do campo de origem
	"ZZ1_VLRLIQ"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ1_QUANT") * oModel:GetValue("ZZ1_VLRUN") - (oModel:GetValue("ZZ1_VLRDSC") * oModel:GetValue("ZZ1_QUANT")) }  )	// [04] Bloco de codigo de execu��o do gatilho

	oStructZZ1:AddTrigger( ;
	"ZZ1_QUANT"			,;										//[01] Id do campo de origem
	"ZZ1_VLRLIQ"			,;										//[02] Id do campo de destino
	{ |oModel| .T. }	,;										//[03] Bloco de codigo de valida��o da execu��o do gatilho
	{ |oModel| oModel:GetValue("ZZ1_QUANT") * oModel:GetValue("ZZ1_VLRUN") - (oModel:GetValue("ZZ1_VLRDSC") * oModel:GetValue("ZZ1_QUANT")) }  )	// [04] Bloco de codigo de execu��o do gatilho

	*/
	//-----------------------------------------
	//Monta o modelo do formul�rio
	//-----------------------------------------

	oModel:= MPFormModel():New("YCADZZ2",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)

	oModel:AddFields("ZZ2MASTER",/*cOwner*/, oStructZZ2 ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)		

	oModel:AddGrid("ZZ3DETAIL", "ZZ2MASTER"/*cOwner*/,oStructZZ3, ,/*bLinePost*/,/*bPre*/,/*bPost*/,/*Carga*/)

	//oModel:SetActivate({|oModel| LoadZZ0(oModel) })


	oModel:SetPrimaryKey({"ZZ2_FILIAL","ZZ2_NUMERO","ZZ2_MEDICA","ZZ2_CLIENT","ZZ2_LOJA"})
	//oModel:GetModel("ZZ1DETAIL"):GetStruct():SetProperty("ZZ1_PRODUT",MODEL_FIELD_VALID,{|oModel|ZZ0ValProd(oModel) })
	//oModel:GetModel("ZZ1DETAIL"):GetStruct():SetProperty("ZZ1_TES",MODEL_FIELD_VALID,{|oModel|ZZ0ValTES(oModel) })

	// Adiciona ao modelo uma estrutura de formul�rio de campos calculados
	//oModel:AddCalc( 'YCADZZ0CALC1', 'ZZ2MASTER', 'ZZ3DETAIL', 'ZZ1_QUANT', 'ZZ1__TOT01', 'SUM',,,'Total Itens',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"QTDMED") }*/ )
	//oModel:AddCalc( 'YCADZZ0CALC1', 'ZZ2MASTER', 'ZZ3DETAIL', 'ZZ1_SALDO', 'ZZ1__TOT02', 'SUM',,,'Saldo dos Itens',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)
	//oModel:AddCalc( 'YCADZZ0CALC1', 'ZZ2MASTER', 'ZZ3DETAIL', 'ZZ1_VLRTOT', 'ZZ1__TOT03', 'SUM',,,'Valor Total(R$)',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)
	//oModel:AddCalc( 'YCADZZ0CALC1', 'ZZ2MASTER', 'ZZ3DETAIL', 'ZZ1_VLRLIQ', 'ZZ1__TOT04', 'SUM',,,'Valor Total Liquido(R$)',/*{|oModel, nTotalAtual, xValor, lSomando|  Total02(@oModel,"TOTAL") }*/)

	//oModel:GetModel( 'ZZ3DETAIL' ):SetUniqueLine( { 'ZZ3_PRODUT','ZZ3_TES' } )

	oModel:GetModel("ZZ2MASTER"):SetDescription("Medi��o")
	oModel:GetModel("ZZ3DETAIL"):SetDescription("Itens da medi��o")
	oModel:SetRelation("ZZ3DETAIL",{{"ZZ3_FILIAL",'xFilial("ZZ3")'},{"ZZ3_NUMERO","ZZ2_NUMERO"},{"ZZ3_MEDICA","ZZ2_MEDICA"},{"ZZ3_CLIENT","ZZ2_CLIENT"},{"ZZ3_LOJA","ZZ2_LOJA"}},ZZ3->(IndexKey(1)))
	//oModel:SetVldActive( { |oModel| ZZ0ValAct( oModel ) } )
	//oModel:SetCommit({|oModel| SZ1Commit(oModel) },.F.)
	oModel:SetCommit({|oModel| FWFormCommit(oModel) },.T.)

	//oModel:AddRules( 'ZZ1DETAIL', 'ZZ1_QUANT'	,'ZZ1DETAIL'	,'ZZ1_PRODUT'	,1 )
	//oModel:AddRules( 'ZZ1DETAIL', 'ZZ1_VLRUN'	,'ZZ1DETAIL'	,'ZZ1_PRODUT'	,1 )
	//oModel:AddRules( 'ZZ1DETAIL', 'ZZ1_VLRTOT'	,'ZZ1DETAIL'	,'ZZ1_PRODUT'	,1 )
	//oModel:AddRules( 'ZZ1DETAIL', 'ZZ1_TES'		,'ZZ1DETAIL'	,'ZZ1_PRODUT'	,1 )

Return(oModel)

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
	Local oStructZZ2	:= FWFormStruct( 2, 'ZZ2' )
	Local oStructZZ3	:= FWFormStruct( 2, 'ZZ3' )   //,{|cCampo| !AllTrim(cCampo)+"|" $ "ZZ1_FILIAL|ZZ1_LOJA|ZZ1_CLIENT|ZZ1_NOME|ZZ1_LOJA|"})
	Local oModel		:= FWLoadModel ( 'DEV001' )	
	Local oCalc1	

	Local oView

	//oStructZZ1:RemoveField( 'ZZ1_NUMERO' )	

	// Cria o objeto de Estrutura
	//oCalc1	:= FWCalcStruct( oModel:GetModel( 'YCADZZ0CALC1') )
	oView	:= FWFormView():New()

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField( "ZZ2MASTER",oStructZZ2)
	oView:AddGrid("ZZ3DETAIL",oStructZZ3)
	//oView:AddField( 'VIEW_CALC', oCalc1, 'YCADZZ0CALC1' )

	oView:CreateHorizontalBox("CABEC",30)
	oView:CreateHorizontalBox("GRID",60)
	//oView:CreateHorizontalBox( 'RODAPE',10)

	oView:SetOwnerView( "ZZ2MASTER","CABEC")
	oView:SetOwnerView( "ZZ3DETAIL","GRID")
	//oView:SetOwnerView( 'VIEW_CALC', 'RODAPE' )

	oView:SetProgressBar(.T.)
	oView:AddIncrementField("ZZ3DETAIL","ZZ3_ITEM")
	oView:EnableTitleView('ZZ3DETAIL','Itens Medicao')

	//oView:AddUserButton( 'Procurar Kit', 'CLIPS', { |oView| ProcKit(oView) }, , VK_F4,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE,MODEL_OPERATION_VIEW} )
	//oView:AddUserButton( 'Visualizar Kit', 'CLIPS', { |oView| VISKit(oView) }, , VK_F7,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE,MODEL_OPERATION_VIEW} )

Return oView

//Carga na ZZ0 quando for um contrato gerado a partir do Encerramento - Transferencia
Static Function LoadZZ0(oModel)

	Local cQuery := ""
	Local oModelZZ1 := oModel:GetModel( 'ZZ1DETAIL' )

	oModel:SetValue("ZZ2MASTER", "ZZ2_FILIAL", ZZ2->ZZ2_FILIAL)	
	oModel:SetValue("ZZ2MASTER", "ZZ2_NUMCRM", ZZ2->ZZ2_NUMCRM)
	oModel:SetValue("ZZ2MASTER", "ZZ2_CLIENT", ZZ2->ZZ2_CLIENT)
	oModel:SetValue("ZZ2MASTER", "ZZ2_LOJA",   ZZ2->ZZ2_LOJA)
	oModel:SetValue("ZZ2MASTER", "ZZ2_NOME",   ZZ2->ZZ2_NOME)
	oModel:SetValue("ZZ2MASTER", "ZZ2_NATURE", ZZ2->ZZ2_NATURE)
	oModel:SetValue("ZZ2MASTER", "ZZ2_ANOCOM", ZZ2->ZZ2_ANOCOM)
	oModel:SetValue("ZZ2MASTER", "ZZ2_CODCRM", ZZ2->ZZ2_CODCRM)
	oModel:SetValue("ZZ2MASTER", "ZZ2_CCUSTO", ZZ2->ZZ2_CCUSTO)
	oModel:SetValue("ZZ2MASTER", "ZZ2_CONORI", ZZ2->ZZ2_NUMERO)
	oModel:SetValue("ZZ2MASTER", "ZZ2_CLIORI", ZZ2->ZZ2_CLIENT)
	oModel:SetValue("ZZ2MASTER", "ZZ2_LOJORI", ZZ2->ZZ2_LOJA)

	cQuery := "" 
	cQuery += " SELECT * " 
	cQuery += "   FROM " + RETSQLNAME("ZZ3")
	cQuery += "  WHERE D_E_L_E_T_ = '' "
	cQuery += "    AND ZZ3_FILIAL = '" + ZZ2->ZZ2_FILIAL + "' "
	cQuery += "    AND ZZ3_NUMERO = '" + ZZ2->ZZ2_NUMERO + "' "
	cQuery += "    AND ZZ3_MEDICA = '" + ZZ2->ZZ2_MEDICA + "' "
	cQuery += "    AND ZZ3_CLIENT = '" + ZZ2->ZZ2_CLIENT + "' "
	cQuery += "    AND ZZ3_LOJA   = '" + ZZ2->ZZ2_LOJA + "' "
	//cQuery += "    AND ZZ1_SALDO > 0 "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XZZ3",.F.,.T.)

	While XZZ3->(!EOF())

		oModelZZ1:AddLine()

		oModel:SetValue("ZZ3DETAIL", "ZZ1_PRODUT", XZZ3->ZZ1_PRODUT)
		oModel:SetValue("ZZ3DETAIL", "ZZ1_QUANT", XZZ3->ZZ1_SALDO)	
		oModel:SetValue("ZZ3DETAIL", "ZZ1_TES", XZZ3->ZZ1_TES)
		oModel:SetValue("ZZ3DETAIL", "ZZ1_VLRUN", XZZ3->ZZ1_VLRUN)			
		oModel:SetValue("ZZ3DETAIL", "ZZ1_STATUS", XZZ3->ZZ1_STATUS)		

		XZZ3->(DBSKIP())	
	EndDo
	XZZ3->(DBCLOSEAREA())

Return

Static Function ZZ0ValAct(oModel)
	Local lRet:= .T.
	Local cContrato
	If oModel:getOperation() == MODEL_OPERATION_INSERT
		If cFilAnt=="010105"
			oModel:SetErrorMessage('ZZ0MASTER',,,,"ATEN��O", 'Impossivel realizar contratos na empresa Matriz',"Escolha outra filial para criar o contrato" )
			Return .F.
		EndIf
	ElseIf oModel:getOperation() == MODEL_OPERATION_DELETE
		cContrato	:= ZZ0->ZZ0_NUMERO
		SE1->(DbSetOrder(1))	//E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO

		If	!Empty(ZZ0->ZZ0_APROV) .and. empty(cFlag)

			oModel:SetErrorMessage('ZZ0MASTER',,,,"ATEN��O", 'Contrato j� liberado, n�o pode ser excluido!', '')
			Return .F.

		elseIf SE1->(DbSeek(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+padl(cContrato,9,'0'))) .AND. !Empty(SE1->E1_BAIXA)
			oModel:SetErrorMessage('ZZ0MASTER',,,,"ATEN��O", 'O titulo relacionando a esse contrato j� possui movimenta��es!', 'Para excluir � necessario excluir os movimentos primeiro!')
			Return .F.
		EndIf
	ElseIf oModel:getOperation() == MODEL_OPERATION_UPDATE

		//Verifica se h� aprovador e se a variavel cFlag est� vazia. A cFlag � como uma flag para informar se n�o � uma medi��o.
		If	!Empty(ZZ0->ZZ0_APROV) .and. empty(cFlag)

			oModel:SetErrorMessage('ZZ0MASTER',,,,"ATEN��O", 'Contrato j� liberado, n�o pode ser alterado!', 'Para altera��o realize a adi��o do contrato!')
			Return .F.

		ElseIf Alltrim(cFlag) == 'YZZ0003'

			Return .T.

		EndIf
	EndIf
Return lRet

Static Function SZ1Commit(oModel)

	Local cContrato	:= oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_NUMERO')
	Local cClient :=  oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_CLIENT')
	Local cLoja :=  oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_LOJA')
	Local nSaldo		:= 0
	Local nVlAdiant	:= 0
	Local nQtd			:= 0
	Local lRet			:= .T.
	Local nCont
	Local aDados		:= {}
	Local cUpdate := ""
	Local nAdiant := 0
	Local nEstoque := 0
	Local dDataPgto := ''
	Local cMsg := ""	
	Local oView := FWViewActive()

	Private lMsErroAuto	:= .F.	


	If	(oModel:getOperation() == MODEL_OPERATION_UPDATE) .OR. 	(oModel:getOperation() == MODEL_OPERATION_INSERT)

		oModelZZ1	:= oModel:GetModel( 'ZZ1DETAIL' )
		For nCont := 1 To oModelZZ1:Length()

			oModelZZ1:GoLine( nCont )
			If oModelZZ1:IsDeleted()
				Loop
			EndIf

			If SF4->(DbSeek(xFilial("SF4")+oModelZZ1:GetValue('ZZ1_TES')))  .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
				nAdiant	+= oModelZZ1:GetValue('ZZ1_VLRLIQ') 
			EndIf

			//incluido por rafael pinheiro - 02/11/2015
			// verifica se tes movimenta estoque
			If SF4->(DbSeek(xFilial("SF4")+oModelZZ1:GetValue('ZZ1_TES')))  .AND. SF4->F4_ESTOQUE=="N"	//Gera Financeiro
				nEstoque	+= oModelZZ1:GetValue('ZZ1_VLRLIQ') 
			EndIf
			//fim - incluido por rafael pinheiro - 02/11/2015

		Next
	EndIf

	//VALOR DO FRETE SER� CONSIDERADO SOMENTE O QUE GERA ESTOQUE.
	nAdiant	+=  (nAdiant-nEstoque) * (oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_FRTCLI') / 100) // modificado por rafael pinheiro 02/11/2015
	dDataPgto :=  oModel:GetModel( 'ZZ0MASTER' ):GetValue('ZZ0_DTCOND')

	//oModel:LoadValue("ZZ0MASTER","ZZ0_ADIANT",nAdiant) 
	// Rotina inclusa por Weskley Silva 19/10/2015
	if oModel:getOperation() == 3 .OR. oModel:getOperation() == 4

		oModel:LoadValue("ZZ0MASTER","ZZ0_ADIANT",nAdiant) 

	EndIf	
	// -----------------------------------------------------
	oView:REFRESH()

	If nAdiant > 0 .AND. Empty(StrTran(dtoc(dDataPgto),'/',''))	

		oModel:SetErrorMessage('ZZ0MASTER',,,,"ATEN��O", "O valor de adiantamento � maior que zero, o campo de data de pagamento deve ser preenchido para que seja gerado titulo no contas a receber.", "Verifique o campo de data para pagamento(ZZ0_DTCOND).")

		Return .F.			
	EndIf	

	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
	If	(oModel:getOperation() == MODEL_OPERATION_UPDATE) .OR. 	(oModel:getOperation() == MODEL_OPERATION_INSERT)

		oModelZZ1	:= oModel:GetModel( 'ZZ1DETAIL' )
		For nCont := 1 To oModelZZ1:Length()
			oModelZZ1:GoLine( nCont )
			If oModelZZ1:IsDeleted()
				Loop
			EndIf

			If SF4->(DbSeek(xFilial("SF4")+oModelZZ1:GetValue('ZZ1_TES')))  .AND. SF4->F4_DUPLIC=="S"	//Gera Financeiro
				//nVlAdiant	+= oModelZZ1:GetValue('Z2_VLRLIQ')
			EndIf
		Next

		//Se variavel cFlag estiver preenchida, sinaliza que � uma transfer�ncia.
		If !empty(cFlag) .AND. ZZ0->ZZ0_NUMERO <> cContrato

			cUpdate := ""
			cUpdate := " UPDATE " + RETSQLNAME("ZZ1")
			cUpdate += "    SET ZZ1_SALDO = 0 "
			cUpdate += "  WHERE D_E_L_E_T_ = '' "
			cUpdate += "    AND ZZ1_FILIAL = '" + ZZ0->ZZ0_FILIAL + "' "
			cUpdate += "    AND ZZ1_NUMERO = '" + ZZ0->ZZ0_NUMERO + "' "

			TCSQLEXEC(cUpdate)

			RECLOCK("ZZ0",.F.)

			ZZ0->ZZ0_OBSERV := ZZ0->ZZ0_OBSERV + Mv_Par02 + "Contrato " + ZZ0->ZZ0_NUMERO + " foi transferido para o contrato " + cContrato +". " 
			ZZ0->ZZ0_STATUS := 'I'

			MSUNLOCK()

		EndIf

		cMsg := ""

		If	(oModel:getOperation() == MODEL_OPERATION_UPDATE) 
			cMsg += "Alterado contrato: " + CHR(13) + CHR(10)
		ELSE
			cMsg += "Incluido contrato: " + CHR(13) + CHR(10)
		EndIf

		cMsg += "Filial: " + XFILIAL("ZZ0") + CHR(13) + CHR(10)
		cMsg += "Contrato: " + cContrato  + CHR(13) + CHR(10)
		cMsg += "Cliente: " +  cClient + "-" + POSICIONE("SA1",1,XFILIAL("SA1") + cClient+cLoja,"SA1->A1_NREDUZ")
		cMsg += "Loja: " + cLoja + CHR(13) + CHR(10)

		MessageBox(cMsg, "TOTVS", 64)

	ElseIf oModel:getOperation() == MODEL_OPERATION_DELETE
		SE1->(DbSetOrder(1))	//E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
		Begin Transaction
			While SE1->(DbSeek(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+PadL(cContrato,GetSx3Cache("E1_NUM","X3_TAMANHO"))))
				AADD(aDados,{"E1_FILIAL"		,xFilial("SE1")		,"#"})
				AADD(aDados,{"E1_PREFIXO"	,SE1->E1_PREFIXO		,"#"})
				AADD(aDados,{"E1_NUM"		,SE1->E1_NUM			,"#"})
				/*AADD(aDados,{"E1_PARCELA"	,SE1->E1_PARCELA		,"#"})
				AADD(aDados,{"E1_TIPO"		,SE1->E1_TIPO			,"#"})
				AADD(aDados,{"INDEX"			,1						,"#"})*/
				MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 5)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
				If lMsErroAuto
					DisarmTransaction()
					AutoGrLog("Erro ao excluir o titulo!")
					AutoGrLog("Opera��o cancelada!")
					MostraErro()
					oModel:SetErrorMessage('ZZ0MASTER',,,,"ERRO", 'Erro ao excluir o titulo, opera��o cancelada!', 'Entre em contato com o administrador do sistema!')
					Return .F.
				Endif
			EndDo
		End Transaction
	EndIf

Return lRet

Static Function Total02(oModel,cTipo)
	Local nCont		:= 0
	Local nTotal		:= 0
	Local oModelZZ1	:= oModel:GetModel( 'ZZ1DETAIL' )
	Local aSaveLines := FWSaveRows()
	Local cDuplic
	Default cTipo		:= "TOTAL"

	For nCont := 1 To oModelZZ1:Length()
		oModelZZ1:GoLine( nCont )
		If oModelZZ1:IsDeleted()
			Loop
		EndIf
		If cTipo == "TOTAL"
			nTotal	+= oModelZZ1:GetValue('Z2_VLRLIQ')
		ElseIf cTipo=="QTDMED"
			nTotal	+= oModelZZ1:GetValue('Z2_QTDMED')
		ElseIf cTipo=="VENDA"
			If GetCacheSF4(oModelZZ1:GetValue('ZZ1_TES'),"F4_DUPLIC",@cDuplic) .and. cDuplic=="S"	//Gera Financeiro
				nTotal	+= oModelZZ1:GetValue('Z2_VLRLIQ')
			EndIf
		EndIf
	Next
	FWRestRows( aSaveLines )
Return nTotal

/*/{Protheus.doc} YZZ0001
Aprova��o do documento
@author Erenilson
@since 10/08/2015
@version 1.0
@return lRet, Logico se opera��o realizada com sucesso
/*/
Static  Function YZZ0001(lMsg,cNomeUsr,cTime)
	Local aDados	:= {}
	Local aCondicao:= {}
	Local nValFin := 0
	Local nDesc := 0
	Local nCount := 0
	Local cMsg := ""
	Local cQuery := ""
	Local cQuery2 := ""
	Local nFrete := 0
	Default cNomeUsr	:= UsrFullName(RetCodUsr())
	Default cTime		:= Time()
	Default lMsg		:= .T.
	Private lMsErroAuto	:= .F.	

	If !Empty(ZZ0->ZZ0_APROV)
		If lMsg
			MsgAlert("Documento j� aprovado!")
		EndIf
		Return .F.
	Elseif lMsg .AND. !MsgYesNo("Deseja aprovar o documento?")
		Return .F.
	EndIf

	//incluido por rafael pinheiro - 02/11/2015
	U_RF041(ZZ0->ZZ0_FILIAL,ZZ0->ZZ0_NUMERO)
	//fim - incluido por rafael pinheiro - 02/11/2015


	If ZZ0->ZZ0_ADIANT >0 .AND. ZZ0->ZZ0_CONFNF == '1'

		nValFin	:= ZZ0->ZZ0_ADIANT //Verifica se j� teve item adicionado
		nDesc	:= FDescZ1(ZZ0->ZZ0_NUMERO,ZZ0->ZZ0_CLIENT,ZZ0->ZZ0_LOJA) //Busca desconto referente a produto kit
		nValDsc	:= 0 //Total do desconto por parcela

		SE1->(dbSetOrder(1))
		Begin Transaction
			aCondicao	:= Condicao(nValFin,ZZ0->ZZ0_CPFIN,,ZZ0->ZZ0_DTCOND)
			If Empty(aCondicao)
				DisarmTransaction()
				AutoGrLog("Erro ao gerar parcelas do titulo!")
				AutoGrLog("Verifique a condi��o de pagamento e o valor total!")
				MostraErro()
				Return .F.
			EndIf

			//Verifica se j� existe T�tulo para o contrato
			cParc:= ""
			nTotD:= 0
			If SE1->(dbSeek(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+PADL(ZZ1->ZZ1_NUMERO,9,'0'))) //E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
				While SE1->(!eof()) .and. Alltrim(SE1->(E1_FILIAL+E1_PREFIXO+E1_NUM)) == Alltrim(xFilial("SE1")+"A"+SUBSTR(CFILANT,5,2)+PADL(ZZ1->ZZ1_NUMERO,9,'0'))
					cParc:= SE1->E1_PARCELA
					nTotD+=	SE1->E1_DECRESC
					SE1->(dbSkip())
				Enddo
			Endif
			If !empty(cParc)
				cParc:= soma1(cParc)
			Endif

			if nDesc > 0 //Existe produto Kit com desconto
				nValDsc:= Round(((nDesc-nTotD) / (len(aCondicao))),2) //Rateia desconto
			endif

			cQuery := ""
			cQuery += " SELECT SUM(ZZ1_VLRLIQ) AS ADIANT "
			cQuery += "   FROM " + RETSQLNAME("ZZ1") + " ZZ1 "
			cQuery += "  INNER JOIN " + RETSQLNAME("SF4") + " SF4 "
			cQuery += "     ON SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) = SF4.F4_FILIAL "
			cQuery += "    AND ZZ1.ZZ1_TES = SF4.F4_CODIGO "
			cQuery += "   WHERE ZZ1.ZZ1_FILIAL = '" + ZZ0->ZZ0_FILIAL + "' "
			cQuery += "     AND ZZ1.ZZ1_NUMERO = '" + ZZ0->ZZ0_NUMERO + "' "
			cQuery += "     AND ZZ1.D_E_L_E_T_ = '' "
			cQuery += "     AND SF4.D_E_L_E_T_ = '' " 
			cQuery += "     AND SF4.F4_DUPLIC = 'S' "

			cQuery := ChangeQuery(cQuery)

			DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"XZZ3A")		

			cQuery2 := ""
			cQuery2 += " SELECT SUM(ZZ1_VLRLIQ) AS DECRE "
			cQuery2 += "   FROM " + RETSQLNAME("ZZ1") + " ZZ1 "
			cQuery2 += "  INNER JOIN " + RETSQLNAME("SF4") + " SF4 "
			cQuery2 += "     ON SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) = SF4.F4_FILIAL "
			cQuery2 += "    AND ZZ1.ZZ1_TES = SF4.F4_CODIGO "
			cQuery2 += "   WHERE ZZ1.ZZ1_FILIAL = '" + ZZ0->ZZ0_FILIAL + "' "
			cQuery2 += "     AND ZZ1.ZZ1_NUMERO = '" + ZZ0->ZZ0_NUMERO + "' "
			cQuery2 += "     AND ZZ1.D_E_L_E_T_ = '' "
			cQuery2 += "     AND SF4.D_E_L_E_T_ = '' " 
			cQuery2 += "     AND SF4.F4_ESTOQUE = 'N' "

			cQuery2 := ChangeQuery(cQuery2)

			DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery2),"XZZ3B")

			nFrete := NOROUND( (XZZ3A->ADIANT - XZZ3B->DECRE) * (ZZ0->ZZ0_FRTCLI / 100 ) ,2)

			XZZ3A->(DBCLOSEAREA())
			XZZ3B->(DBCLOSEAREA())

			For nCont:=1 to Len(aCondicao)
				aDados	:= {}
				AADD(aDados,{"E1_PREFIXO"	,"A"+SUBSTR(CFILANT,5,2),nil})
				AADD(aDados,{"E1_NUM"		,PADL(ZZ0->ZZ0_NUMERO,9,'0')		,nil})
				AADD(aDados,{"E1_TIPO"		,"ADT"					,nil})
				AADD(aDados,{"E1_PARCELA"	,RetAsc(nCont,GetSx3Cache("E1_PARCELA","X3_TAMANHO"),.T.),nil})
				AADD(aDados,{"E1_NATUREZ"	,ZZ0->ZZ0_NATUREZ		,nil})
				AADD(aDados,{"E1_CLIENTE"	,ZZ0->ZZ0_CLIENT		,nil})
				AADD(aDados,{"E1_LOJA"		,ZZ0->ZZ0_LOJA			,nil})
				AADD(aDados,{"E1_HIST"		,'CONTRATO: '+ZZ0->ZZ0_NUMERO + "Tipo do Titulo: Adiantamento",nil})
				AADD(aDados,{"E1_CCC"		,GetMv("SA_CCC")		,nil})
				AADD(aDados,{"E1_VENCTO"		,aCondicao[nCont][1]	,nil})
				AADD(aDados,{"E1_YCONTRA"		,ZZ0->ZZ0_NUMERO,nil})
				AADD(aDados,{"E1_YFILIAL"		,ZZ0->ZZ0_FILIAL,nil})					
				AADD(aDados,{"E1_VALOR"		,aCondicao[nCont][2]	,nil})
				AADD(aDados,{"E1_YVLFRET"		, NOROUND(nFrete / Len(aCondicao),2) 	,nil})
				AADD(aDados,{"E1_YCONTRA",ZZ0->ZZ0_NUMERO	,nil})
				lMsErroAuto	:= .F.
				MsExecAuto( { |x,y| FINA040(x,y)} , aDados, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
				If lMsErroAuto
					DisarmTransaction()
					AutoGrLog("Erro ao incluir o titulo!")
					AutoGrLog("Opera��o cancelada!")
					MostraErro()
					Return .F.
				Else
					nCount++					
				Endif
			Next
		End Transaction

	EndIf

	If nCount > 0

		cMsg := ""
		cMsg += "Titulo gerado no contas a receber: " + CHR(13) + CHR(10)
		cMsg += "Filial: " + XFILIAL("SE1") + CHR(13) + CHR(10)
		cMsg += "No. Titulo: " + PADL(ZZ0->ZZ0_NUMERO,9,'0') + CHR(13) + CHR(10)
		cMsg += "Cliente: " +  ZZ0->ZZ0_CLIENT + "-" + POSICIONE("SA1",1,XFILIAL("SA1") + ZZ0->ZZ0_CLIENT,"SA1->A1_NREDUZ")

		MessageBox(cMsg, "TOTVS", 64)

	EndIf

	RecLock("ZZ0",.F.)
	ZZ0->ZZ0_APROV	:= __cUserID //cNomeUsr
	ZZ0->ZZ0_HRAPRO	:= cTime
	ZZ0->ZZ0_DTAPROV := DATE()
	ZZ0->ZZ0_STATUS := "L"		
	ZZ0->(MsUnLock())

Return .T.



// rafael pinheiro - 28/10/2015
Static Function YZZ0035()
	Local cContrato	:= ZZ0->ZZ0_NUMERO
	Local aDados		:= {}

	cFlag := "SASP035"

	If ZZ0->ZZ0_STATUS = 'I'
		Alert("Contrato inativado! N�o poder� ser realizada medi��o.")
	ElseIf ZZ0->ZZ0_STATUS = 'B'
		Alert("Contrato bloqueado! N�o poder� ser realizada medi��o.")
	ElseIf !empty(ZZ0->ZZ0_APROV)
		FWExecView ("Medi��o Automatica", "SASP035", MODEL_OPERATION_UPDATE ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
	Else
		Alert("Contrato ainda n�o foi aprovado. Aprove-o e em seguida execute a medi��o.")
	EndIf		

Return
// fim - rafael pinheiro - 28/10/2015

Static Function YZZ0002()
	Local cContrato	:= ZZ0->ZZ0_NUMERO
	Local aDados		:= {}

	cFlag := "SASP036"

	If ZZ0->ZZ0_STATUS = 'I'
		Alert("Contrato inativado! N�o poder� ser realizada medi��o.")
	ElseIf ZZ0->ZZ0_STATUS = 'B'
		Alert("Contrato bloqueado! N�o poder� ser realizada medi��o.")
	ElseIf !empty(ZZ0->ZZ0_APROV)
		FWExecView ("Medi��o", "SASP036", MODEL_OPERATION_UPDATE ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
	Else
		Alert("Contrato ainda n�o foi aprovado. Aprove-o e em seguida execute a medi��o.")
	EndIf		

Return

//Aditivo de Contrato
Static Function YZZ0003()
	Local cContrato	:= ZZ0->ZZ0_NUMERO
	Local aDados		:= {}	

	//Local aParam1	:= {}
	//Local aRetParm1 := {}
	//Local lOk1		:= .F.

	Public lOk		:= .F.
	Public aParam	:= {}
	Public aRetParm	:= {}	

	Public cProdIni	:= Space(GetSx3Cache("B1_COD","X3_TAMANHO"))
	Public cProdFim	:= Space(GetSx3Cache("B1_COD","X3_TAMANHO"))
	Public cTes		:= Space(GetSx3Cache("ZZ1_TES","X3_TAMANHO"))
	Public cSerie	:= Space(GetSx3Cache("B1_YSERIE","X3_TAMANHO"))
	Public cVolume	:= Space(GetSx3Cache("B1_YVOL","X3_TAMANHO"))
	Public aTipoAdd	:= {'Quantidade / Financeiro','Produto'}



	aAdd(aParam,{1,"Itens De"		,cProdIni	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{1,"Itens Ate"	,cProdFim	,GetSx3Cache("B1_COD","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{1,"Tes"			,cTes		,GetSx3Cache("SF4_CODIGO","X3_PICTURE"),".T.","SF4",".T.",80,.F.})
	aAdd(aParam,{1,"Serie"				,cSerie	,GetSx3Cache("B1_YSERIE","X3_PICTURE"),".T.","ZZO",".T.",80,.F.})
	aAdd(aParam,{1,"Volume"				,cVolume ,GetSx3Cache("B1_YVOL","X3_PICTURE"),".T.","Z3",".T.",80,.F.})
	aAdd(aParam,{2,"Tipo de Aditivo" ,,aTipoAdd,80,,.T.})

	cFlag := 'YZZ0003'

	If ZZ0->ZZ0_STATUS = 'I'
		Alert("Contrato inativado! N�o poder� ser realizada medi��o.")
	ElseIf ZZ0->ZZ0_STATUS = 'B'
		Alert("Contrato bloqueado! N�o poder� ser realizada medi��o.")
	ElseIf !empty(ZZ0->ZZ0_APROV)
		If ParamBox(aParam,"Filtrar Aditivo",@aRetParm,{||.T.},,,,,,"U_SASP042",.T.,.T.)
			FWExecView ("Aditivo", "SASP042", MODEL_OPERATION_UPDATE ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
		EndIf	
	Else
		Alert("Contrato ainda n�o foi aprovado. Aprove-o e em seguida execute a adi��o.")
	EndIf	


Return

//Desbloqueio de Contrato
Static Function YZZ0007()

	if ZZ0_STATUS== 'B'
		If MsgYesNo("Deseja desbloquear o contrato "+ZZ0->ZZ0_NUMERO+" ?")
			RecLock('ZZ0',.F.)
			ZZ0->ZZ0_STATUS	:=	'L'
			MsUnlock()
			Aviso("Aten��o","Contrato "+ZZ0->ZZ0_NUMERO+" foi desbloqueado.",{"Ok"})
		Endif
	ELSE
		Alert("Somente contrato bloqueado pode ser desbloqueado.")
	EndIf		

Return

//Encerramento de Contrato
Static Function YZZ0009()

	Local cPerg := "YZZ9C"

	//Verifica se tem medi��o em aberta para o contrato
	cSelec:= "SELECT COUNT(*) QTD FROM "+RetSqlName("ZZ2")+" ZZ2"
	cSelec+= " WHERE ZZ2.D_E_L_E_T_ =' ' AND "
	cSelec+= " ZZ2_FILIAL+ZZ2_NUMERO+ZZ2_CLIENT+ZZ2_LOJA = '"+ZZ0->(ZZ0_FILIAL+ZZ0_NUMERO+ZZ0_CLIENT+ZZ0_LOJA)+"' AND "
	cSelec+= " ZZ2_PV01 <> ' ' AND "
	cSelec+= " ZZ2_PV01NF = ' '  "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cSelec),"XQ031A",.F.,.T.)

	If XQ031A->QTD > 0
		XQ031A->(dbCloseArea())
		Aviso("Aten��o","Contrato "+ZZ0->ZZ0_NUMERO+" tem medi��es em aberto," +chr(13)+chr(10)+;
		" n�o podendo ser inativado ou transferido",{"Ok"})
		Return
	Endif

	XQ031A->(dbCloseArea())

	AjustaSX1(cPerg)

	If Pergunte(cPerg,.T.)

		If mv_par01 == 1 //Inativa��o

			RecLock("ZZ0",.F.)
			ZZ0->ZZ0_STATUS 	:= 'I'					
			ZZ0->ZZ0_OBSERV 	:= ZZ0->ZZ0_OBSERV + " " + Mv_Par02				
			ZZ0->(MsUnLock())				
			Aviso("Aten��o","Contrato "+ZZ0->ZZ0_NUMERO+" inativado!!!",{"Ok"})

		ElseIf mv_par01 == 2 //Transfer�ncia

			cFlag := "SASP031"
			FWExecView ("Medi��o", "SASP031", MODEL_OPERATION_INSERT,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )

		EndIf

	EndIf

Return

Static Function MedicaoAut(cEmpCon,cCliIni,cLjIni,cCliFim,cLjFim,cProdIni,cProdFim,nQuant)
	Local oModel,nCont
	Local cTmp	:= GetNextAlias()
	Local cQuery
	Default nQuant	:= 0
	ProcRegua(10)
	ProcessMessage() //ATUALIZA CLIENTE

	cQuery := ""
	cQuery += "SELECT R_E_C_N_O_ AS REGISTRO FROM "+RetSqlName( 'ZZ0' ) + " ZZ0 "
	cQuery += " WHERE ZZ0_FILIAL='"+ cEmpCon + "'"
	cQuery += " AND ZZ0_APROV<>'"+SPACE(GetSx3Cache("ZZ0_APROV","X3_TAMANHO"))+"' "
	//cQuery += " AND Z1_SALDO>0 "
	cQuery += " AND ZZ0_CLIENT>='"+cCliIni+"' AND ZZ0_CLIENT0<='"+cCliFim+"'
	cQuery += " AND ZZ0_LOJA>='"+cLjIni+"' AND ZZ0_LOJA<='"+cLjFim+"'"
	cQuery += " AND ZZ0.D_E_L_E_T_=' '"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.F.,.T.)
	ZZ0->(DbSetOrder(1))	//Z1_FILIAL+Z1_CONTRAT
	While (cTmp)->(!EOF())
		ZZ0->(DBGOTO((cTmp)->REGISTRO))
		IncProc("Contrato: "+ZZ0->Z0_CONTRAT)
		ProcessMessage() //ATUALIZA CLIENTE
		lEdit			:= .F.
		oModel 		:= FWLoadModel( 'CADZZ2' )
		oModel:SetOperation( MODEL_OPERATION_UPDATE )
		lRet := oModel:Activate()
		oModelZZ3	:= oModel:GetModel('ZZ3DETAIL')
		If lRet
			For nCont := 1 To oModelZZ3:Length()
				oModelZZ3:GoLine(nCont)
				If oModelZZ3:IsDeleted()
					Loop
				EndIf
				cCodPro	:= Posicione("SB1",1,xFilial("SB1")+oModelZZ2:GetValue("ZZ3_PRODUT"),"B1_COD")
				If !(cCodPro>=cProdIni .AND. cCodPro<=cProdFim)	//Filtro de Grupo de,ate
					Loop
				EndIf
				If !oModelSZ3:SetValue("ZZ3_QUANT", oModelSZ3:GetValue("ZZ3_SALDO") )
					Exit
				Else
					lEdit	:= .T.
				EndIf
			Next
			If !lEdit
				oModel:DeActivate()
				(cTmp)->(DbSkip())
				Loop
			EndIf
		EndIf
		If lRet .and. ( lRet := oModel:VldData() )
			oModel:CommitData()
		EndIf
		If !lRet
			aErro   := oModel:GetErrorMessage()
			AutoGrLog( "Id do formul�rio de origem:" + ' [' + AllToChar( aErro[1]  ) + ']' )
			AutoGrLog( "Id do campo de origem:     " + ' [' + AllToChar( aErro[2]  ) + ']' )
			AutoGrLog( "Id do formul�rio de erro:  " + ' [' + AllToChar( aErro[3]  ) + ']' )
			AutoGrLog( "Id do campo de erro:       " + ' [' + AllToChar( aErro[4]  ) + ']' )
			AutoGrLog( "Id do erro:                " + ' [' + AllToChar( aErro[5]  ) + ']' )
			AutoGrLog( "Mensagem do erro:          " + ' [' + AllToChar( aErro[6]  ) + ']' )
			AutoGrLog( "Mensagem da solu��o:       " + ' [' + AllToChar( aErro[7]  ) + ']' )
			AutoGrLog( "Valor atribu�do:           " + ' [' + AllToChar( aErro[8]  ) + ']' )
			AutoGrLog( "Valor anterior:            " + ' [' + AllToChar( aErro[9]  ) + ']' )
		Else
			nQuant++
		EndIf
		oModel:DeActivate()
		If !lRet
			MostraErro()
			Return .F.
		EndIf
		(cTmp)->(DbSkip())
	EndDo
	(cTmp)->(DbCloseArea())
Return .T.

Static Function PrecoTab(cProduto)
	Local cTabPad		:= SuperGetMV("MV_TABPAD")
	Local oModel		:= FWModelActive()
	Local lKit			:= ExistCpo("ZZ7",cProduto,1,,.F.)
	Local nValorTab	:= 0
	SA1->(dbSetOrder(1))	//A1_FILIAL+A1_COD
	SA1->(dbSeek(xFilial("SA1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA')))
	cTabPad	:= SA1->A1_TABELA

	If lKit

		If SA1->(dbSeek(xFilial("SA1")+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT')+oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))) .AND. !Empty(SA1->A1_Tabela)
			cTabPad	:= SA1->A1_TABELA
		EndIf

		ZZ7->(DbSetOrder(1))
		ZZ7->(DbSeek(xFilial("ZZ7")+cProduto))
		While ZZ7->(!EOF()) .AND. xFilial("ZZ7")+cProduto==ZZ7->(ZZ7_FILIAL+ZZ7_CODIGO)
			nValorTab += ZZ7->ZZ7_QUANT*MaTabPrVen(cTabPad;
			,ZZ7->ZZ7_CODIGO;
			,1;
			,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT');
			,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))
			ZZ7->(DbSkip())
		EndDo
	Else
		nValorTab	:= MaTabPrVen(cTabPad;
		,cProduto;
		,1;
		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_CLIENT');
		,oModel:GetModel('ZZ0MASTER'):GetValue('ZZ0_LOJA'))
	EndIf
Return nValorTab

Static Function ZZ0ValProd(oModelZZ1)
	Local lRet := ExistCpo("ZZ7",oModelZZ1:GetValue('ZZ1_PRODUT'),1,,.F.).OR.ExistCpo("SB1",oModelZZ1:GetValue('ZZ1_PRODUT'),1,,.F.)
	Local oModel := FWModelActive()
	If !lRet
		oModel:SetErrorMessage('ZZ1DETAIL',"ZZ1_PRODUT",,,"ATEN��O", 'Produto/Kit n�o encontrado', 'Verifique se o Produto ou Kit esta cadastrado!')
	Else
		oModel:GetModel('ZZ1DETAIL'):LoadValue('ZZ1_QUANT',0)
		oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_VLRUN',PrecoTab(oModelZZ1:GetValue('ZZ1_PRODUT')))
		oModel:GetModel('ZZ1DETAIL'):LoadValue('ZZ1_VLRTOT',0)
		oModel:GetModel('ZZ1DETAIL'):LoadValue('ZZ1_TES',Space(GetSx3Cache("ZZ1_TES","X3_TAMANHO")))
	EndIf
Return lRet

//Fun��o SASP034 - Fun��o para retornar o kit - Arvore.
Static Function ProcKit(oView)	

	U_SASP034('KIT')

Return

//Visualiza��o pelo F7 do KIT
Static Function VISKit

	Local oModel:= FWModelActive()

	U_SASP034(''," ( (ZZ7_CODIGO = '"+oModel:GetModel('ZZ1DETAIL'):GetValue('ZZ1_PRODUT')+"') OR (ZZ7_CODPAI = '"+oModel:GetModel('ZZ1DETAIL'):GetValue('ZZ1_PRODUT')+"'))")

Return

//A vari�vel LPrimeiro, � um "flag" para verificar se � a primeira chamada da fun��o recursiva iniciada no fonte SASP034.
Static function YZZ0005(cCodKit2, aRet,lPrimeiro)

	Local oModel := FWModelActive()
	Local cCodKit	:= cCodKit2
	Local cTipo :=  ""
	Local cDesc := ""
	Local cQuery := ""
	Local cQuery9 := ""
	Local oModelZZ1 := oModel:GetModel( 'ZZ1DETAIL' )
	Local nRegistros := 0
	Local cAliasT := GetNextAlias()	
	Local nRateio := 0
	Local nPerDes := 0
	Local cQuery8 := ""
	Local nValorDes := 0
	Local oView := FWViewActive()

	//Avo - Sintetico
	//   Pai - Sintetico
	//     Filho - Sintetico
	//     Filho - Analitico	
	//Retorna somente o filho sint�tico.

	If !Empty(cCodKit)

		cTipo := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_TIPO")

		If cTipo == 'S' 	

			cQuery := ""
			cQuery += " SELECT ZZ7_CODIGO, ZZ7_DESCR "
			cQuery += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7 "
			cQuery += "  WHERE D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7_CODPAI = '" + cCodKit +  "' "
			cQuery += "    AND ZZ7_TIPO = 'S' "
			cQuery += "    AND ZZ7_CODIGO <> '" + cCodKit + "' "   

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasT,.F.,.T.)

			Count To nRegistros

			(cAliasT)->(DBGOTOP())

			if nRegistros > 0

				While (cAliasT)->(!EOF())												

					YZZ0005((cAliasT)->ZZ7_CODIGO, 0,.F.)								

					(cAliasT)->(DBSKIP())				

				EndDo

				(cAliasT)->(DBCLOSEAREA())
			Else
				//TODO kits!

				cQuery8 := ""
				cQuery8 += " SELECT COUNT(*) AS QTD "
				cQuery8 += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7"
				cQuery8 += "  WHERE D_E_L_E_T_ = '' "
				cQuery8 += "    AND ZZ7.ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
				cQuery8 += "    AND ZZ7.ZZ7_CODPAI = '" + cCodKit + "' "
				cQuery8 += "    AND ZZ7.ZZ7_PERDSC = 100 "  

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery8),"PERCDES",.F.,.T.)

				cDesc := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_DESCR" )
				bProf := Posicione("ZZ7",1,XFILIAL("ZZ7") + cCodKit, "ZZ7->ZZ7_CATEGO" )

				If PERCDES->QTD > 0					

					nPerDes := PERCDES->QTD		

				Else

					nPerDes := 0

				EndIf

				PERCDES->(DBCLOSEAREA())

				//Codigo do kit, Descri��o do item, Quantidade, TES, Valor unitario, Tipo(k = Kit), Percentual Desconto
				AADD(aKit, {cCodKit,cDesc,1,"",1,'K',nPerDes})

				(cAliasT)->(DBCLOSEAREA())

			EndIf

		Else		
			Alert("Produto selecionado n�o � um kit. O tipo do produto deve ser SINT�TICO.")
		EndIf

	EndIf

	//Se nValor for booleano == .F. , o usu�rio deve ter clicado em Cancelar no ParamBox.	
	if ValType(aRet) == 'L'

		nValor := 1

	EndIf

	If lPrimeiro == .T. 

		For nX := 1 to len(aKit)				

			cQuery := ""
			cQuery += " SELECT ZZK_PORCEN "
			cQuery += "   FROM " + RETSQLNAME("ZZK") + " ZZK "
			cQuery += "  INNER JOIN  " + RETSQLNAME("ZZ7") + " ZZ7 "
			cQuery += "     ON ZZK.ZZK_FILIAL = ZZ7.ZZ7_FILIAL "
			cQuery += "    AND ZZK.ZZK_SERIE = ZZ7.ZZ7_SERIE "
			cQuery += "    AND ZZK.ZZK_ENVIO = ZZ7.ZZ7_ENVIO "
			cQuery += "  WHERE ZZK.D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7.D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7.ZZ7_CODIGO = '" + aKit[nX][1] + "' "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"PERCRAT",.F.,.T.)

			If PERCRAT->(!EOF())

				nRateio := ROUND(aRet[1] * (PERCRAT->ZZK_PORCEN / 100),2)

			Else

				Alert("O kit " + aKit[nX][1] + " n�o possui porcentagem de rateio definido, valor do produto ser� R$ 1. Verifique o cadastro do kit e tente novamente." )
				nRateio := 1

			EndIf

			PERCRAT->(DBCLOSEAREA())						

			If aKit[nX][7] > 0
				nValorDes := aKit[nX][7] * GETMV("SA_PRECDSC")

			Else

				nValorDes := 0

			EndIf

			// TODO KIT PROFESSOR
			// Inicio - Rafael Pinheiro 
			// Data: 05/10/2015
			// Objetivo: Kit do professor valor fixo de R$ 10,00

			if Select("QSASPrf") > 0
				dbCloseArea("QSASPrf")
			endif

			nProf = 0

			cQrySAS := ""
			cQrySAS += " SELECT COUNT(*) AS QTD "
			cQrySAS += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7"
			cQrySAS += "  WHERE D_E_L_E_T_ = '' "
			cQrySAS += "    AND ZZ7.ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
			cQrySAS += "    AND ZZ7.ZZ7_CODPAI = '" + aKit[nX][1] + "' "
			cQrySAS += "    AND ZZ7.ZZ7_CATEGO = 'P' "  

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySAS),"QSASPrf",.F.,.T.)

			nProf := QSASPrf->QTD

			if nProf > 0
				nRateio := nProf * 10
				nValorDes := 0
			endif

			dbCloseArea("QSASPrf")

			// FIM - Rafael Pinheiro

			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_PRODUT',aKit[nX][1])
			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_DESC',aKit[nX][2])
			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_QUANT',aKit[nX][3])
			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_TES',aRet[2])
			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_VLRDSC',nValorDes)


			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_VLRUN',nRateio + nValorDes)	


			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_TIPO',aKit[nX][6])
			oModel:GetModel('ZZ1DETAIL'):SetValue('ZZ1_VLRTOT',nRateio + nValorDes)				

			If nX < len(aKit) 
				oModelZZ1:AddLine()
				oView:Refresh()
			EndIf

			GETDREFRESH()
			nDifer := 0

		next nX

		aKit := {}

	EndIf


Return

Static Function ZZ0ValTES(oModelZZ1)
	Local lRet := .T.
	Local oModel := FWModelActive()
	If !ExistCpo("SF4",oModelZZ1:GetValue('ZZ1_TES'),1,,.F.)	//N�o existi TES
		lRet	:= .F.
		oModel:SetErrorMessage('ZZ1DETAIL',"ZZ1_TES",,,"ATEN��O", 'Tes n�o encontrado', 'Verifique se a TES est� cadastrada!')
	ElseIf oModelZZ1:GetValue('ZZ1_TES') <= "500"
		lRet	:= .F.
		oModel:SetErrorMessage('ZZ1DETAIL',"ZZ1_TES",,,"ATEN��O", 'A Tes informada n�o � uma valida para saida', 'Verifique se a TES � de saida!')
	EndIf
Return lRet


//Desconto do kit
Static Function FDescZ1(cContr,cCli,cLoj)
	Local nRet		:= 0
	Local cTabPad	:= SuperGetMV("MV_TABPAD")

	If SA1->(dbSeek(xFilial("SA1")+cCli+cLoj)) .AND. !Empty(SA1->A1_Tabela)
		cTabPad	:= SA1->A1_TABELA
	EndIf

	dbSelectArea("ZZ1")
	ZZ1->(dbSetOrder(1))
	ZZ1->(dbSeek(xFilial("ZZ1")+cContr))

	while ZZ1->(!eof()) .and. Alltrim(xFilial("ZZ1")+cContr) == Alltrim(ZZ1->ZZ1_FILIAL+ZZ1->ZZ1_NUMERO)

		lKit:= ExistCpo("ZZ7",ZZ7->ZZ7_CODIGO,1,,.F.)

		If lKit
			ZZ7->(DbSetOrder(1))
			ZZ7->(DbSeek(xFilial("ZZ7")+Alltrim(ZZ1->ZZ1_PRODUT)))
			While ZZ7->(!EOF()) .AND. Alltrim(xFilial("ZZ7")+ZZ1->ZZ1_PRODUT)==Alltrim(ZZ7->(ZZ7_FILIAL+ZZ7_CODIGO))
				nVlrItem	:= MaTabPrVen(cTabPad;
				,ZZ7->ZZ7_PRODUT;
				,1;
				,cCli;
				,cLoj)

				//Verifica se tem desconto
				If ZZ7->ZZ7_PERDSC > 0
					nRet+= (( nVlrItem*ZZ7->ZZ7_QUANT*ZZ1->ZZ1_QUANT)*(ZZ7->ZZ7_PERDSC/100))
				Endif
				ZZ7->(dbSkip())
			Enddo
		Endif
		ZZ1->(dbSkip())
	Enddo

Return nRet

/*/{Protheus.doc} RETDSCZZ0
Retorna Desconto conforme Kit de Produtos
@author Diogo
@since 22/09/2015
@version 1.0
/*/
Static Function RETDSCZZ0(oModelZZ1)
	Local nRet:= 0
	Local cTabPad	:= SuperGetMV("MV_TABPAD")
	Local aAreaZ1	:= GetArea()
	Local oModel	:= FWModelActive()

	if Empty(oModel:GetValue("ZZ0MASTER","ZZ0_CLIENT"))
		Return nRet
	endif

	lKit:= ExistCpo("ZZ7",oModelZZ1:GetValue("ZZ1_PRODUT"),1,,.F.)
	If SA1->(dbSeek(xFilial("SA1")+oModel:GetValue("ZZ0MASTER","ZZ0_CLIENT")+oModel:GetValue("ZZ0MASTER","ZZ0_LOJA"))) .AND. !Empty(SA1->A1_Tabela)
		cTabPad	:= SA1->A1_TABELA
	EndIf

	If lKit
		ZZ7->(DbSetOrder(1))
		ZZ7->(DbSeek(xFilial("ZZ7")+Alltrim(oModelZZ1:GetValue("ZZ1_PRODUT"))))
		While ZZ7->(!EOF()) .AND. Alltrim(xFilial("ZZ7")+oModelZZ1:GetValue("ZZ1_PRODUT"))==Alltrim(ZZ7->(ZZ7_FILIAL+ZZ7_CODIGO	))
			nVlrItem	:= MaTabPrVen(cTabPad;
			,ZZ7->ZZ7_PRODUT;
			,1;
			,oModel:GetValue("ZZ0MASTER","ZZ0_CLIENT");
			,oModel:GetValue("ZZ0MASTER","ZZ0_LOJA"))

			If ZZ7->ZZ7_PERDSC > 0
				nRet+= (( nVlrItem*ZZ7->ZZ7_QUANT*oModelZZ1:GetValue("ZZ1_QUANT"))*(ZZ7->ZZ7_PERDSC/100))
			Endif
			ZZ7->(dbSkip())
		Enddo
	Endif

	RestArea(aAreaZ1)

Return nRet

Static Function GetCacheSF4(cCodTES,cCampo,xRet)
	Local lRet	:= .F.
	Local nPos	:= aScan(aCacheTES,{|x| x[1]==cCodTES .and. x[2]==cCampo})
	If nPos>0
		xRet	:=	aCacheTES[nPos][3]
		Return .T.
	EndIf
	SF4->(DbSetOrder(1))	//F4_FILIAL+F4_CODIGO
	If SF4->(DbSeek(xFilial("SF4")+cCodTES))
		AADD(aCacheTES,{cCodTES,cCampo,SF4->(&cCampo)})
		lRet	:= .T.
		xRet	:= SF4->(&cCampo)
	EndIf
Return lRet

Static Function YZZ0008()
	Local oBrowse
	Local aLegenda	:= {}
	Local nCont
	Private cCadastro	:= "Adiantamentos"
	Private aRotina := {}

	//rafael pinheiro - 28/10/2015

	If (ZZ0->ZZ0_STATUS == 'A')
		Alert("Contrato ainda n�o aprovado.")
		Return
	EndIf		

	// fim - rafael pinheiro - 28/10/2015


	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('SE1')
	oBrowse:SetDescription('Adiantamentos')
	oBrowse:DisableDetails()
	oBrowse:SetMenuDef( '' )
	//oBrowse:SetFilterDefault("E1_FILIAL=='"+xFilial("SE1", ZZ0->ZZ0_FILIAL)+"' .AND. E1_NUM='"+PADL(ZZ0->ZZ0_NUMERO,GetSx3Cache("E1_NUM","X3_TAMANHO"),'0')+"' .AND. E1_TIPO $ 'ADT|NCC|'")
	oBrowse:SetFilterDefault("E1_YCONTRA == '" + ZZ0->ZZ0_NUMERO + "' .AND. E1_YFILIAL == '" + ZZ0->ZZ0_FILIAL +"' .AND. E1_TIPO $ 'ADT|NCC|'")
	oBrowse:AddButton("Legenda","Fa040Legenda",0,2,0)
	aLegenda	:= Fa040Legenda("SE1")
	For nCont:=1 to Len(aLegenda)
		oBrowse:AddLegend( aLegenda[nCont][1], aLegenda[nCont][2], "")
	Next
	oBrowse:Activate()
Return

//Bloqueio do Contrato
Static Function YZZ0006()

	If ZZ0_STATUS== 'E'
		Alert("Contrato encerrado! N�o poder� ser bloqueado.")
	ELSEIF ZZ0_STATUS== 'A'
		Alert("Contrato n�o aprovado! N�o poder� ser bloqueado.")
	Else
		If MsgYesNo("Deseja bloquear o contrato "+ZZ0->ZZ0_NUMERO+" ?")
			RecLock('ZZ0',.F.)
			ZZ0->ZZ0_STATUS	:=	'B'
			MsUnlock()
			Aviso("Aten��o","Contrato "+ZZ0->ZZ0_NUMERO+" foi bloqueado.",{"Ok"})
		Endif
	EndIf		

Return


Static Function AjustaSX1(cPerg)

	Local aHelp := {"Ativa / Inativa item a partir da sele��o"}

	PutSx1(cPerg, "01","Tipo de Inativa��o?",		  		 "","","mv_ch01","C",01,0,0,"C","","   ","","","mv_par01","1-Inativa��o","1-Inativa��o","1-Inativa��o","2-Transfer�ncia","2-Transfer�ncia","2-Transfer�ncia",,,,"","","","",,"","")
	PutSx1(cPerg, "02","Observa��o?",				  		 "","","mv_ch02","C",50,0,0,"G","",,"","","mv_par02"," ","","","","","","","","","","","","","","","")

Return


Static Function Data031(nAdiant, dDataPgto)

	Local lOk := .F.	

	If nAdiant > 0 .AND. Empty(dtoc(dDataPgto))

		Alert("O valor de adiantamento � maior que zero, o campo de data de pagamento deve ser preenchido para que seja gerado titulo no contas a receber.")
		lOk := .F.	
	Else

		lOk := .T.

	EndIf

Return lOk

Static Function AnoCom()

	Local cAno := cValtoChar(Year(DATE())) + '=' + cValtoChar(Year(DATE())) + ';'

	For nX:= 1 To 10

		cAno += cValTochar(Year(DATE()) + nX) + '=' + cValtochar(Year(DATE())+ nX) + ';'

	Next nX

Return cAno

Static Function FilDEV02(aMedicao,oMarkBrow)

	Private aSelect  := {"","","","",""}
	For nAx := 1 to len(aMedicao)

		IF aMedicao[nAx][11] <> "1"
			LOOP
		ELSE 
		
			IF ALLTRIM(aSelect[1]) == ""
				aSelect[1]  += "'"+ aMedicao[nAx][1] +"'"
			ELSE 
			//	aSelect[1]  += ",'"+aMedicao[nAx][1] +"'"
			ENDIF
	
			IF ALLTRIM(aSelect[2]) == ""
				aSelect[2]  += "'"+ aMedicao[nAx][2] +"'"
			ELSE 
				aSelect[2]  += ",'"+aMedicao[nAx][2] +"'"
			ENDIF

			IF ALLTRIM(aSelect[3]) == ""
				aSelect[3]  += "'"+ aMedicao[nAx][3] +"'"
			ELSE 
				aSelect[3]  += ",'"+aMedicao[nAx][3] +"'"
			ENDIF

			IF ALLTRIM(aSelect[4]) == ""
				aSelect[4]  += "'"+ aMedicao[nAx][4] +"'"
			ELSE 
				aSelect[4]  += ",'"+aMedicao[nAx][4] +"'"
			ENDIF

			IF ALLTRIM(aSelect[5]) == ""
				aSelect[5]  += "'"+ aMedicao[nAx][5] +"'"
			ELSE 
				aSelect[5]  += ",'"+aMedicao[nAx][5] +"'"
			ENDIF
		
		ENDIF		
	next
	
	IF LEN(aSelect[1]) == 0
		RETURN
	ENDIF

	FWExecView('Inclusao por FWExecView','DEV002', MODEL_OPERATION_INSERT, , { || .T. }, , , )
	aSelect := {}

Return
