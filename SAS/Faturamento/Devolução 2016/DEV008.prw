#Include 'Protheus.ch'
#Include 'Topconn.ch'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} RF020
Tela de Acompanhamento de Entrega DEVOLUCAO
@type function
@author joao Filho 
@since 04/02/2016
@version 1.0
/*/

User Function DEV008()
	Local aColumns
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Private aDados	:= {}
	Private oBrowse := {}
	Private aCampSX3	:= {{"ZZH_FILIAL","ZZH->ZZH_FILIAL"};
		,{"ZZH_NUMERO","ZZH->ZZH_NUMERO"};
		,{"ZZH_MEDICA","ZZH->ZZH_MEDICA"};
		,{"ZZH_STACOM","ZZH->ZZH_STACOM"};
		,{"X5_DESCRI","POSICIONE('SX5',1,XFILIAL('SX5',ZZH->ZZH_FILIAL)+'ZB'+ZZH->ZZH_STACOM,'X5_DESCRI')"};
		,{"ZZH_DTENTR","ZZH->ZZH_DTENTR"};
		,{"ZZH_PC01NF","ZZH->ZZH_PC01NF"};
		,{"ZZH_SER01","ZZH->ZZH_SER01"};
		,{"F1_EMISSAO","POSICIONE('SF1',1,XFILIAL('SF1',ZZH->ZZH_PC01FL)+ZZH->ZZH_PC01NF + ZZH->ZZH_SER01,'F1_EMISSAO')"};//TODO INCLUIR DATA DA EMISSAO
		,{"ZZH_NF02","ZZH->ZZH_NF02"};
		,{"ZZH_SER02","ZZH->ZZH_SER02"};
		,{"F2_EMISSAO","POSICIONE('SF2',1,XFILIAL('SF2',ZZH->ZZH_NF02FL)+ZZH->ZZH_NF02 + ZZH->ZZH_SER02,'F2_EMISSAO')"};//TODO INCLUIR DATA DA EMISSAO
		,{"ZZH_NF03","ZZH->ZZH_NF03"};
		,{"ZZH_SER03","ZZH->ZZH_SER03"};
		,{"F1_EMISSAO","POSICIONE('SF1',1,XFILIAL('SF1',ZZH->ZZH_NF03FL)+ZZH->ZZH_NF03 + ZZH->ZZH_SER03,'F1_EMISSAO')"};//TODO INCLUIR DATA DA EMISSAO
		,{"ZZH_TPDEV","ZZH->ZZH_TPDEV"};
		,{"X5_DESCRI","POSICIONE('SX5',1, XFILIAL('SX5') + 'ZV' + ZZH->ZZH_TPDEV, 'X5_DESCRI' )"};
		,{"A1_END","POSICIONE('SA1',1,xFilial('SA1',ZZH->ZZH_FILIAL)+ZZH->(ZZH_CLIENT+ZZH_LOJA),'A1_END')"};
		,{"ZZH_TES","ZZH->ZZH_TES"};
		,{"F4_TEXTO","POSICIONE('SF4',1,xFilial('SF4',ZZH->ZZH_FILIAL)+ZZH->ZZH_TES,'F4_TEXTO')"};
		,{"ZZH_OBS","ZZH->ZZH_OBS"};
		,{"ZZH_QUATVL","ZZH->ZZH_QUATVL"};
		,{"F1_VALBRUT","POSICIONE('SF1',1,XFILIAL('SF1',ZZH->ZZH_PC01FL)+ZZH->ZZH_PC01NF + ZZH->ZZH_SER01,'F1_VALBRUT')"}; 
		,{"ZZH_DTPREV","ZZH->ZZH_DTPREV"};
		,{"ZZH_STATUS","ZZH->ZZH_STATUS"};		
		,{"ZZH_DTGER","ZZH->ZZH_DTGER"};
		,{"ZZH_CLIENT","ZZH->ZZH_CLIENT"};
		,{"ZZH_LOJA","ZZH->ZZH_LOJA"};
		,{"ZZH_NOME","ZZH->ZZH_NOME"};
		,{"ZZH_DTSOLI","ZZH->ZZH_DTSOLI"};
		,{"NDIAS","ZZH->ZZH_DTSOLI-DDATABASE","Dias","N",3,0,""};
		,{"ZZH_TRANSP","ZZH->ZZH_TRANSP"};
		,{"A4_NOME","POSICIONE('SA4',1,xFilial('SA4')+ZZH->ZZH_TRANSP,'SA4->A4_NOME')"};
		,{"ZZH_FRETE","ZZH->ZZH_FRETE"};		
		,{"A1_MUN","POSICIONE('SA1',1,xFilial('SA1',ZZH->ZZH_FILIAL)+ZZH->(ZZH_CLIENT+ZZH_LOJA),'A1_MUN')"};
		,{"A1_EST","POSICIONE('SA1',1,xFilial('SA1')+ZZH->(ZZH_CLIENT+ZZH_LOJA),'A1_EST')"}}//;
	
	oDlg := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Tela de Acompanhamento de Entrega Devolu��o',,,,nOr(WS_VISIBLE,WS_POPUP),,,,,.T.,,,,)
	oBrowForm	:= FWFormBrowse():New()
	oBrowse := oBrowForm:FWBrowse()
	oBrowse:SetDataTable(.T.)
	oBrowse:SetAlias("ZZH")
	oBrowse:SetSeek()
	oBrowse:SetDescription('Tela de Acompanhamento de Entrega Devolucao')
	oBrowse:DisableDetails()
	oBrowse:SetUseFilter(.T.)
	oBrowse:SetDBFFilter(.T.)
	
	bAtualiza	:= {|| oBrowse:Refresh(),oBrowse:GoTop() }
	nTempo:= SuperGetMv("SA_TMPREF",.F.,30)*1000 
	oTimer:= TTimer():New(nTempo,bAtualiza,oDlg)
	oTimer:Activate()
	
	
	oBrowse:SetMenuDef( 'DEV008' )
	
	oBrowse:AddButton('Hist�rico OS', 'U_HISTDEV()', , 4, 0)
	
	oBrowse:AddLegend( " ZZH->ZZH_DTPREV-DDATABASE >=  0 " , "BR_VERDE", 'NO PRAZO' )
	//oBrowse:AddLegend( " ZZH->ZZH_DTSOLI-DDATABASE <  0 " , "BR_RED", 'ATRASADO' )
	
	//Legenda
	dbSelectArea("ZZS")
	ZZS->(dbGotop())
	ZZS->(dbSetOrder(1))
	while ZZS->(!eof())
		
		oBrowse:AddLegend( 	"ZZH->ZZH_DTPREV-DDATABASE <  0 .AND. "+;
							"abs(ZZH->ZZH_DTPREV-DDATABASE) >= "+cValtochar(ZZS->ZZS_ATRDE)+" .AND."+;
							"abs(ZZH->ZZH_DTPREV-DDATABASE) <= "+cValtochar(ZZS->ZZS_ATRATE),+;
							fLegZZS(ZZS->ZZS_LEGEND), ZZS->ZZS_DESCR  )

		ZZS->(dbSkip())
	enddo

	oBrowse:AddFilter("ATRASADOS "," ZZH->ZZH_DTPREV < DDATABASE " )

	aColumns	:= DefCampos()
	oBrowse:SetColumns(aColumns)

	oBrowse:SetFilterDefault( "ZZH_STFAT  == 'F' .AND. ZZH_COLETA <> '02' " ) //Somente Faturados	
	//oBrowse:SetFilterDefault( "if(ZZH->ZZH_ESPEC == '01',ALLTRIM(ZZH->ZZH_PC01NF) <> '',ALLTRIM(ZZH->ZZH_PC01NF) <>'' .AND. ALLTRIM(ZZH->ZZH_NF02) <> '' .AND. ALLTRIM(ZZH->ZZH_NF03) <> '')" ) //Somente Faturados
	
	oBrowForm:Activate(oDlg)
	oDlg:Activate(,,,.T.)

Return

Static Function DefCampos()
	Local aColumns	:= {}
	Local aColumn
	SX3->(DbSetOrder(2))	//X3_CAMPO
	For nCont:=1 to Len(aCampSX3)
		If SX3->(DbSeek(PADR(aCampSX3[nCont][1],10)))
			aColumn := {;
				GetCamDef(aCampSX3[nCont],3,,GetSx3Cache(aCampSX3[nCont][1],"X3_TITULO"));				//[n][01] T�tulo da coluna
				,&("{|| "+GetCamDef(aCampSX3[nCont],2,,GetTabDados(aCampSX3[nCont]))+"}");				//[n][02] Code-Block de carga dos dados
				,GetSx3Cache(aCampSX3[nCont][1],"X3_TIPO");					//[n][03] Tipo de dados
				,GetSx3Cache(aCampSX3[nCont][1],"X3_PICTURE");				//[n][04] M�scara
				,If(GetSx3Cache(aCampSX3[nCont][1],"X3_TIPO")=="N",2,1);	//[n][05] Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				,GetCamDef(aCampSX3[nCont],5,,GetSx3Cache(aCampSX3[nCont][1],"X3_TAMANHO"));				//[n][06] Tamanho
				,GetCamDef(aCampSX3[nCont],6,,GetSx3Cache(aCampSX3[nCont][1],"X3_DECIMAL"));				//[n][07] Decimal
				,.F.;						//[n][08] Indica se permite a edi��o
				,{||.T.};					//[n][09] Code-Block de valida��o da coluna ap�s a edi��o
				,.F.;						//[n][10] Indica se exibe imagem
				,{||nil};					//[n][11] Code-Block de execu��o do duplo clique
				,"";						//[n][12] Vari�vel a ser utilizada na edi��o (ReadVar)
				,{||nil};					//[n][13] Code-Block de execu��o do clique no header
				,.F.;						//[n][14] Indica se a coluna est� deletada
				,.F.;						//[n][15] Indica se a coluna ser� exibida nos detalhes do Browse
				,{};						//[n][16] Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			}
		Else
			aColumn := {;
				GetCamDef(aCampSX3[nCont],3,1);				//[n][01] T�tulo da coluna
				,&("{|| "+GetTabDados(aCampSX3[nCont])+"}");		//[n][02] Code-Block de carga dos dados
				,GetCamDef(aCampSX3[nCont],4,,"C");					//[n][03] Tipo de dados
				,GetCamDef(aCampSX3[nCont],7,,"");				//[n][04] M�scara
				,If(GetCamDef(aCampSX3[nCont],4,,"C")=="N",2,1);	//[n][05] Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
				,GetCamDef(aCampSX3[nCont],5,,10);				//[n][06] Tamanho
				,GetCamDef(aCampSX3[nCont],6,,0);					//[n][07] Decimal
				,.F.;						//[n][08] Indica se permite a edi��o
				,{||.T.};					//[n][09] Code-Block de valida��o da coluna ap�s a edi��o
				,.F.;						//[n][10] Indica se exibe imagem
				,{||nil};					//[n][11] Code-Block de execu��o do duplo clique
				,"";						//[n][12] Vari�vel a ser utilizada na edi��o (ReadVar)
				,{||nil};					//[n][13] Code-Block de execu��o do clique no header
				,.F.;						//[n][14] Indica se a coluna est� deletada
				,.F.;						//[n][15] Indica se a coluna ser� exibida nos detalhes do Browse
				,{};						//[n][16] Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
			}
		EndIf
		Aadd(aColumns, aColumn )
	Next
Return aColumns

Static Function GetCamDef(aCampo,nIndice,nIndDef,cDefault)
	Default cDefault	:= ""
	If Len(aCampo)>=nIndice .and. ValType(aCampo[nIndice])<>"U"
		Return aCampo[nIndice]
	ElseIf ValType(nIndDef)=="N" .and. nIndDef>0
		Return aCampo[nIndDef]
	Else
		Return cDefault
	EndIf
Return cDefault

Static Function GetTabDados(aCampo)
	Local cRet	:= ""
	If SubStr(aCampo[1],1,4)=="ZZ2_"
		cRet	:= "ZZ2->"+aCampo[1]
	Else
		If Len(aCampo)>=2
			cRet	:= aCampo[2]
		Else
			cRet	:= "''"
		EndIf
	EndIf
Return cRet

Static Function fLegZZS(cCOR)

	Local cRet:=""
	Local nAt
	aValores:= Separa(GetSx3Cache("ZZS_LEGEND","X3_CBOX"),";",.T.) 
	nPos			:= aScan(aValores,{|x| Left(x,Len(cCOR))==cCOR})
	If nPos>0
		nAt		:= At("=",aValores[nPos])
		cRet	:= SubStr(aValores[nPos],nAt+1)
	EndIf

Return cRet

//Calcula total dos Itens
Static Function VZZ2TOT
	cQuery:=" SELECT SUM(ZZV_VTOTAL) ZZV_VTOTAL FROM  "+RetSqlName("ZZV")+" ZZV "
	cQuery+=" WHERE ZZV.D_E_L_E_T_=' ' AND "
	cQuery+=" ZZV_FILIAL+ZZV_NUMERO+ZZV_MEDICA = '"+xFilial('ZZV',ZZH->ZZH_FILIAL)+ZZH->(ZZH_NUMERO+ZZH_MEDICA)+"' "

	If SELECT("Qa020")>0
		Qa020->(dbCloseArea())
	Endif
	TCQUERY cQuery NEW ALIAS Qa020
	
	nRet:= Qa020->ZZ3_VTOTAL
	
	Qa020->(dbCloseArea())	
	
Return nRet

//Hist�rico de Status de Acompanhamento
USER Function HISTDEV() 
	Local aDad20		:= {}
	Private cFilContr	:=	ZZH->ZZH_FILIAL	
	Private cContra		:=	ZZH->ZZH_NUMERO
	Private cMedic		:=	ZZH->ZZH_MEDICA	
	 
	
	cQuery := " SELECT ZZT_FILIAL,ZZT_CONTRA,"
	cQuery += " ZZT_MEDICA,ZZT_CODIGO,"
	cQuery += " ZZT_USUARI,ZZT_DTINC,"
	cQuery += " ZZT_HORA,ZZT_STATUS,X5_DESCRI, ZZT_OBS " 
	cQuery += " FROM "+RetSqlName("ZZT")+" ZZT "
	cQuery += " JOIN "+RetSqlName("SX5")+" SX5 "
	cQuery += " ON ZZT_STATUS = X5_CHAVE "
	cQuery += " AND X5_TABELA = 'ZB' "
	cQuery += " WHERE ZZT.D_E_L_E_T_ =' ' "
	cQuery += " AND SX5.D_E_L_E_T_ =' ' "
	cQuery += " AND ZZT_FILIAL= '"+xFilial("ZZT",cFilContr)+"' "
	cQuery += " AND ZZT_CONTRA = '"+cContra+"' "
	cQuery += " AND ZZT_MEDICA = '"+cMedic+"' "
	cQuery += " AND ZZT_TIPO   = 'DV' "
	cQuery += " ORDER BY ZZT_CODIGO "	
	
	TCQUERY cQuery NEW ALIAS "TQRY020"
	
	If TQRY020->(eof())
		Aadd(aDad20,{"","","","","","","","","","",""})
	Endif	
	
	while TQRY020->(!eof())
		aadd(aDad20, {	TQRY020->ZZT_FILIAL,;
						TQRY020->ZZT_CONTRA,;
						TQRY020->ZZT_MEDICA,;
						TQRY020->ZZT_CODIGO,;
						TQRY020->ZZT_USUARI,;
						Upper(Alltrim(UsrRetName(TQRY020->ZZT_USUARI))),;
						dtoc(stod(TQRY020->ZZT_DTINC)),;
						TQRY020->ZZT_HORA,;
						TQRY020->ZZT_STATUS,;
						TQRY020->X5_DESCRI,;
						TQRY020->ZZT_OBS;
						})
		TQRY020->(dbSkip())
	enddo
		
	TQRY020->(dbCloseArea())
	aCoors		:= FwGetDialogSize( oMainWnd )
 	DEFINE DIALOG oDlgHist TITLE "Hist�rico Status / Contrato: "+Alltrim(cContra)+"- Medi��o: "+Alltrim(cMedic) FROM aCoors[1],aCoors[2] TO aCoors[3],aCoors[4] PIXEL

    // Cria browse
    oBrowHist := MsBrGetDBase():New( 0, 0, aCoors[3]+87, 260 ,,,, oDlgHist,,,,,,,,,,,, .F., "", .T.,, .F.,,, ) //COLUNA e LINHA
    // Define vetor para a browse
    oBrowHist:SetArray(aDad20)
    
    oFont  := TFont():New( "Arial",0,-11,,.T.,0,,700,.F.,.F.,,,,,, )
    TButton():New( 265	,006,"Incluir",,{|| IncZZTDV(@aDad20),oBrowHist:Refresh()  },048,016,,oFont,,.T.,,"Incluir",,,,.F. )
    
		
    // Cria colunas do browse
    oBrowHist:AddColumn(TCColumn():New("Filial",{ || aDad20[oBrowHist:nAt,1] }; 
        ,GetSx3Cache("ZZT_FILIAL","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 
    
    oBrowHist:AddColumn(TCColumn():New("Contrato",{ || aDad20[oBrowHist:nAt,2] }; 
        ,GetSx3Cache("ZZT_CONTRA","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 
    
    oBrowHist:AddColumn(TCColumn():New("Medi��o",{ || aDad20[oBrowHist:nAt,3] }; 
        ,GetSx3Cache("ZZT_MEDICA","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 
    
    oBrowHist:AddColumn(TCColumn():New("C�digo",{ || aDad20[oBrowHist:nAt,4] }; 
        ,GetSx3Cache("ZZT_CODIGO","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Usu�rio",{ || aDad20[oBrowHist:nAt,5] }; 
        ,GetSx3Cache("ZZT_USUARI","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Nome Usu�rio",{ || aDad20[oBrowHist:nAt,6] }; 
        ,"@!",,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Data Inclus�o",{ || aDad20[oBrowHist:nAt,7] }; 
        ,GetSx3Cache("ZZT_DTINC","X3_PICTURE"),,,"CENTER",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Hora",{ || aDad20[oBrowHist:nAt,8] }; 
        ,GetSx3Cache("ZZT_HORA","X3_PICTURE"),,,"CENTER",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Status",{ || aDad20[oBrowHist:nAt,9] }; 
        ,GetSx3Cache("ZZT_STATUS","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Desc. Status",{ || aDad20[oBrowHist:nAt,10] }; 
        ,GetSx3Cache("ZZT_STATUS","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:AddColumn(TCColumn():New("Observa��o",{ || aDad20[oBrowHist:nAt,11] }; 
        ,GetSx3Cache("ZZT_OBS","X3_PICTURE"),,,"LEFT",,.F.,.F.,,,,.F.,)) 

    oBrowHist:Refresh()
  ACTIVATE DIALOG oDlgHist CENTERED 
  
Return

//Inclus�o ZZT
Static Function IncZZTDV(aDad20)
	Local aPergs	:= {}
	Local aRetOpc	:= {}
	Local cStat		:= space(3)
	Local cOBS		:= space(GetSx3Cache("ZZT_OBS","X3_TAMANHO"))
	Local lOk		:= .F.
	Local dDataE 	:= dDatabase
	
	IF ZZH->ZZH_STACOM == GETMV("SA_STFIMDE")
		MsgInfo("Medi��o j� Finalizada !")
		Return
	ENDIF
	
	aAdd( aPergs ,{1,"Status " 	,cStat	,"@!" ,'.T.',"ZB" ,'.T.',80 ,.T.})
	aAdd( aPergs ,{1,"Observa��o" ,cOBS	,"@!" ,'.T.',""   ,'.T.',80 ,.F.})
	aAdd( aPergs ,{1,"Data Entrega:" ,dDataE,"@!" ,'.T.',"",'.T.',80 ,.F. })
	
	If ParamBox(aPergs,"Modifica��o de Status",aRetOpc,,,,,,,"_RF20",.F.,.F.)
		
		if aRetOpc[1]== GETMV("SA_MATENTR") .and. alltrim(dtos(aRetOpc[3])) == ""
			Msginfo('Status n�o atualizado , informar a data de entrega !!! ')
			return		
		endif
		
		if aRetOpc[1]== GETMV("SA_STFIMDE") 
			if !MsgYesNo("Deseja incluir o status de FINALIZADA ?")
				return  
			endif
		endif
		
		Begin Transaction
		
		cMaxZZT:= FMaxZZT()
		RecLock('ZZT',.T.)
			ZZT->ZZT_STATUS	:=	aRetOpc[1]
			ZZT->ZZT_OBS	:=	aRetOpc[2]
			ZZT->ZZT_FILIAL	:=	xFilial("ZZT",cFilContr)
			ZZT->ZZT_CONTRA	:=	cContra
			ZZT->ZZT_MEDICA	:=	cMedic
			ZZT->ZZT_CODIGO	:=	cMaxZZT
			ZZT->ZZT_USUARI	:=	RetCodUsr()
			ZZT->ZZT_DTINC	:=	Date()
			ZZT->ZZT_HORA	:=	substr(Time(),1,8)
			ZZT->ZZT_TIPO	:=	"DV"
		MsUnlock()

		aadd(aDad20, {	xFilial("ZZT",cFilContr),;
						cContra,;
						cMedic,;
						cMaxZZT,;
						RetCodUsr(),;
						Upper(Alltrim(UsrRetName(RetCodUsr()))),;
						dtoc(Date()),;
						substr(Time(),1,8),;
						aRetOpc[1],;
						Posicione("SX5",1,xFilial("SX5")+"ZB"+aRetOpc[1],"X5_DESCRI"),;
						aRetOpc[2];
						})
		oBrowHist:SetArray(aDad20)
		
		End Transaction
		
		//Gravar �ltimo status na ZZH 
		dbSelectArea("ZZH")
		ZZH->(dbSetOrder(1))
		If ZZH->(dbSeek(xFilial("ZZH",ZZH->ZZH_FILIAL)+ZZH->ZZH_NUMERO+ZZH->ZZH_MEDICA+ZZH->ZZH_CLIENT+ZZH->ZZH_LOJA) )
			RecLock("ZZH",.F.)
				ZZH->ZZH_STACOM	  := aRetOpc[1]
				ZZH->ZZH_DTENTR   := aRetOpc[3]
				IF ALLTRIM(aRetOpc[1]) == GETMV("SA_MATENTR")
					ZZH->ZZH_MATENT   := "01"
				ENDIF
				
			MsUnlock()
		Endif
		
		
		MsgInfo("Hist�rico gravado com sucesso!")
		TcRefresh(RetSqlName("ZZH"))
	Endif
	
Return

//Calcula o sequencial da ZZT
Static Function FMaxZZT

	Local nRetSeq:= 0
	
	cQuery:= "SELECT ISNULL(MAX(ZZT_CODIGO),0) NMAX FROM "+RetSqlName("ZZT")+ ""
	cQuery+= " WHERE D_E_L_E_T_ =' ' AND "
	cQuery+= " ZZT_FILIAL = '"+xFilial("ZZT",cFilContr)+"' AND "
	cQuery+= " ZZT_CONTRA = '"+cContra+"' AND "
	cQuery+= " ZZT_MEDICA = '"+cMedic+"'  "
	
	TcQuery cQuery NEW Alias "QMAXZZT"
	
	nRetSeq:= soma1(QMAXZZT->NMAX,GetSx3Cache("ZZT_CODIGO","X3_TAMANHO"))
	
	QMAXZZT->(dbCloseArea())

Return nRetSeq