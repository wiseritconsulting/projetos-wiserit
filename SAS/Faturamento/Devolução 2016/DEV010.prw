#include 'parmtype.ch'
#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"
#include "dbtree.ch"



/*/{Protheus.doc} DEV002
AJUSTE DE MEDI��O
@author Jo�o Filho / Diogo
@since 12/02/2016
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User Function DEV010()

	oBrowse:= FWmBrowse():New()
	oBrowse:SetAlias('ZZH')
	oBrowse:SetDescription('Medi��o')


	oBrowse:SETMENUDEF('DEV010')
	oBrowse:Activate()

Return

Static Function MenuDef()
	Local aRotina := {}
	Public aKit := {}

	// MENU

	ADD OPTION aRotina Title 'Pesquisa'      Action 'PesqBrw'         OPERATION 1 ACCESS 0
	ADD OPTION aRotina Title 'Visualizar'    Action 'VIEWDEF.DEV002'  OPERATION 2 ACCESS 0
	//ADD OPTION aRotina TITLE "Incluir"     ACTION "VIEWDEF.DEV002"  OPERATION 3 ACCESS 0
	ADD OPTION aRotina Title 'Alterar'       Action 'VIEWDEF.DEV002'  OPERATION 4 ACCESS 0
	ADD OPTION aRotina Title 'Excluir'       Action 'VIEWDEF.DEV002'  OPERATION 5 ACCESS 0
	ADD OPTION aRotina Title 'Ajuste'        Action 'U_AJUSTZZH()'    OPERATION 6 ACCESS 0 DISABLE MENU


Return aRotina

Static Function ModelDef()

	Local oStructZZH := FWFORMSTRUCT(1,'ZZH')
	Local oStructZZV := FWFORMSTRUCT(1,'ZZV')
	Local oModel     := MPFormModel():New('DEV002M')

	//oStructZZH:SetProperty("ZZH_PEDDEV",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| LOJAZZH(oModelGrid,cId,xValor) })
	oStructZZV:SetProperty("ZZV_QTDENT",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| QUANTZZV(oModelGrid,cId,xValor) })
	//oStructZZH:SetProperty("ZZH_OBS",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| OBSZZH(oModelGrid,cId,xValor) })
	//oStructZZV:SetProperty("ZZV_MEDORG",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| MDORGZZV(oModelGrid,cId,xValor,oModel) })
	//oStructZZV:SetProperty("ZZV_PRODUT",MODEL_FIELD_VALID,{|oModelGrid,cId,xValor| LINHAZZV(oModelGrid,cId,xValor,oModel) })

	oModel:AddFields('ZZHMASTER',/*pai*/,oStructZZH)

	oModel:AddGrid("ZZVDETAIL", "ZZHMASTER"/*cOwner*/,oStructZZV, ,/*bLinePost*/,/*bPre*/,/*bPost*/,/*Carga*/)

	oModel:SetRelation('ZZVDETAIL',{{'ZZV_FILIAL','xFilial("ZZV")'},{"ZZV_NUMERO","ZZH_NUMERO"},{"ZZV_MEDICA","ZZH_MEDICA"}},ZZV->(IndexKey(1)))


	//oModel:SetPrimaryKey({'ZZ2_FILIAL','ZZ2_NUMERO','ZZ2_MEDICA'})
	oModel:SetPrimaryKey({"ZZH_FILIAL","ZZH_NUMERO","ZZH_CLIENT","ZZH_LOJA","ZZH_MEDICA"})


	///oModel:SetActivate({|oModel,| LOADZZV(oModel) })

	oModel:SetOnlyQuery('ZZHMASTER',.T.)

	oModel:SetCommit({|oModel| ZZHCommit(oModel) },.F.)

Return (oModel)

Static Function ViewDef()

	Local oModel    := FWLoadModel('DEV010')
	Local oStructZZH:= FWFormStruct(2,'ZZH',)
	//Local oStructZZH:= FWFormStruct(2,'ZZH', {|cCampo|  AllTrim(cCampo)+"|" $ "ZZH_OBSAJU|ZZH_TPAJU"})
	Local oStructZZV:= FWFormStruct(2,'ZZV')
	Local oView:=FWFormView():New()

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField('ZZHMASTER',oStructZZH)
	oView:AddGrid('ZZVDETAIL',oStructZZV)


	oView:CreateHorizontalBox('SUPERIOR',50)
	oView:CreateHorizontalBox('INFERIOR',50)


	oView:SetOwnerView('ZZHMASTER','SUPERIOR')
	oView:SetOwnerView('ZZVDETAIL','INFERIOR')

	oView:EnableTitleView('ZZVDETAIL','Itens Devolu��o')

	oView:SetProgressBar(.T.)
	oView:AddIncrementField("ZZVDETAIL","ZZV_ITEM")


Return oView
// NAO UTILIZADO
/*
STATIC FUNCTION LOADZZV(oModel)

	Local aRet := {}
	Local aArea   := GetArea()
	Local cTmp    := ''
	Local cQuery  := ''
	Local cFields := 'R_E_C_N_O_'
	Local nTam
	Local oModelZZV := oModel:GetModel( 'ZZVDETAIL' )
	Local oModelZZH := oModel:GetModel( 'ZZHMASTER' )
	Local cMedicao := ""
	Local cContra  := ""
	Local cFilCont := ""
	Local cCodCli  := ""
	Local cCodLj   := ""
	Local cEspecie := ""
	//Local aStruct	:= oGrid:oFormModelStruct:GetFields()

	// tratamento para a grid

	cTmp   := GetNextAlias()
	cQuery := " SELECT * "
	cQuery += " FROM "+RetSqlName( 'ZZV' ) + " ZZV "
	cQuery += " WHERE ZZV_FILIAL in("+ ZZH->ZZH_FILIAL + ") "
	cQuery += " AND ZZV_NUMERO in("+ ZZH->ZZH_NUMERO +") "
	cQuery += " AND ZZV_CLIENT in("+ ZZH->ZZH_CLIENT +") "
	cQuery += " AND ZZV_LOJA   in("+ ZZH->ZZH_LOJA +") "
	cQuery += " AND ZZV_MEDICA in("+ ZZH->ZZH_MEDICA +") "
	cQuery += " AND D_E_L_E_T_ =''  "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTmp,.F.,.T.)

	cContra  := (cTmp)->ZZV_NUMERO
	cFilCont := (cTmp)->ZZV_FILIAL
	cCodCli  := (cTmp)->ZZV_CLIENT
	cCodLj   := (cTmp)->ZZV_LOJA
	cMedicao := (cTmp)->ZZV_MEDICA

	While (cTmp)->(!EOF())
		oModelZZV:AddLine()
		oModel:SetValue("ZZVDETAIL", "ZZV_FILIAL" , (cTmp)->ZZV_FILIAL )
		oModel:SetValue("ZZVDETAIL", "ZZV_NUMERO" , (cTmp)->ZZV_NUMERO )
		oModel:SetValue("ZZVDETAIL", "ZZV_MEDICA" , (cTmp)->ZZV_MEDICA )
		oModel:SetValue("ZZVDETAIL", "ZZV_CLIENT" , (cTmp)->ZZV_CLIENT)
		oModel:SetValue("ZZVDETAIL", "ZZV_LOJA"   , (cTmp)->ZZV_LOJA)
		oModel:SetValue("ZZVDETAIL", "ZZV_TES"    , (cTmp)->ZZV_TES)
		oModel:SetValue("ZZVDETAIL", "ZZV_PRODUT" , (cTmp)->ZZV_PRODUT)
		if	(cTmp)->ZZV_TIPO == "K"
			oModel:SetValue("ZZVDETAIL", "ZZV_DESCRI" , POSICIONE("ZZ7",1,XFILIAL("ZZ7")+(cTmp)->ZZV_PRODUT,'ZZ7_DESCR'))
		elseif (cTmp)->ZZV_TIPO == "P"
			oModel:SetValue("ZZVDETAIL", "ZZV_DESCRI" , POSICIONE("SB1",1,XFILIAL("SB1")+(cTmp)->ZZV_PRODUT,'B1_DESC'))
		endif		
		oModel:SetValue("ZZVDETAIL", "ZZV_ENVIO"  , (cTmp)->ZZV_ENVIO)
		oModel:SetValue("ZZVDETAIL", "ZZV_MEDORG" , (cTmp)->ZZV_MEDORG)
		oModel:SetValue("ZZVDETAIL", "ZZV_ITEMMD" , (cTmp)->ZZV_ITEM)  
		oModel:SetValue("ZZVDETAIL", "ZZV_TIPO"   , (cTmp)->ZZV_TIPO)
		oModel:SetValue("ZZVDETAIL", "ZZV_SALDO"  , (cTmp)->ZZV_QUANT - (cTmp)->ZZV_QTDEVO )
		oModel:SetValue("ZZVDETAIL", "ZZV_QTDEVO" , (cTmp)->ZZV_QTDEVO )


		(cTmp)->(DbSkip())
	enddo 	

	// regra para bloquear a inclusao de linhas na grid 
	OMODEL:AMODELSTRUCT[1][4][1][3]:NMAXLINE := OMODEL:AMODELSTRUCT[1][4][1][3]:NLINE 
	//===================================================

	(cTmp)->(dbCloseArea())
	RestArea(aArea)

	//------------------------------//
	// tratamento para o cabe�alho	//
	//------------------------------//

	cQuery3 := " SELECT * "
	cQuery3 += " FROM " + retsqlname('ZZH') + " ZZH "
	cQuery3 += " WHERE ZZH_NUMERO = '"+ cContra +"' AND  "
	cQuery3 += " ZZH_FILIAL = '"+ cFilCont +"' AND  "
	cQuery3 += " ZZH_CLIENT = '"+ cCodCli +"' AND  "
	cQuery3 += " ZZH_LOJA = '"+ cCodLj +"' AND  "
	cQuery3 += " ZZH_MEDICA = '"+ cMedicao +"' AND  "
	cQuery3 += " D_E_L_E_T_ = ''  "

	TCQuery cQuery3 new alias T03
	DBSELECTAREA("T03")

	oModel:SetValue("ZZHMASTER", "ZZH_FILIAL", T03->ZZH_FILIAL)
	oModel:SetValue("ZZHMASTER", "ZZH_NUMERO", T03->ZZH_NUMERO)
	oModel:SetValue("ZZHMASTER", "ZZH_CLIENT", T03->ZZH_CLIENT)
	oModel:SetValue("ZZHMASTER", "ZZH_LOJA",   T03->ZZH_LOJA)
	oModel:SetValue("ZZHMASTER", "ZZH_NOME",   POSICIONE("SA1",1,XFILIAL("SA1")+T03->ZZH_CLIENT+T03->ZZH_LOJA,"A1_NOME"))
	oModel:SetValue("ZZHMASTER", "ZZH_MEDICA", cMedicao)
	oModel:SetValue("ZZHMASTER", "ZZH_ESPEC",  T03->ZZH_ESPEC )
	oModel:SetValue("ZZHMASTER", "ZZH_CONTRI", POSICIONE("SA1",1,XFILIAL("SA1")+T03->ZZH_CLIENT,"A1_CONTRIB") )

	T03->(DBCLOSEAREA())
	RestArea(aArea)


RETURN
*/
Static Function  QUANTZZV(oModelGrid,cId,xValor)

	Local lRet      := .T.
	Local aArea     := GetArea()
	Local oModel	:= FWModelActive()
	Local oModelZZH	:= oModel:GetModel('ZZHMASTER')
	Local oModelZZV	:= oModel:GetModel('ZZVDETAIL')
	Local nDiver    := 0

	if xValor < 0
		MsgInfo("Valor digitado n�o permitido !")
		Return .F.
	endif

	nDiver := oModelZZV:GetValue("ZZV_QTDENT") - oModelZZV:GetValue("ZZV_QUANT")

	oModel:SetValue("ZZVDETAIL", "ZZV_DVRENT", nDiver )


Return lRet

Static Function ZZHCommit(oModel)

	Local oModel	:= FWModelActive()
	Local oModelZZH	:= oModel:GetModel('ZZHMASTER')
	Local oModelZZV	:= oModel:GetModel('ZZVDETAIL')
	Local nAjuste   := 0
	
	if ALLTRIM(oModelZZH:GetValue("ZZH_TPAJU")) == ""
		MsgInfo("Informar Tipo de ajuste!!")
		Return .F.
	endif
	
	if oModelZZH:GetValue("ZZH_TPAJU") == "S" //.OR. oModelZZH:GetValue("ZZH_TPAJU") == ""
		Return .T.
	elseif (oModelZZH:GetValue("ZZH_TPAJU") == "D" .OR. oModelZZH:GetValue("ZZH_TPAJU") == "V" .or. oModelZZH:GetValue("ZZH_TPAJU") == "A") .AND. ALLTRIM(oModelZZH:GetValue("ZZH_OBSAJU")) == ""
		MsgInfo("Informar OBS de ajuste!!")
		Return .F.
	endif
	
	For nCont := 1 To oModelZZV:Length()
		oModelZZV:GoLine(nCont)
		
		if oModelZZV:GetValue("ZZV_DVRENT") == 0
			loop
		endif
		
		nAjuste ++
		
		dbSelectArea("ZZV")
		dbSetOrder(1)
		IF dbSeek(XFILIAL("ZZV")+oModelZZV:GETVALUE("ZZV_NUMERO")+oModelZZV:GETVALUE("ZZV_MEDICA")+oModelZZV:GETVALUE("ZZV_CLIENT")+oModelZZV:GETVALUE("ZZV_LOJA")+oModelZZV:GETVALUE("ZZV_PRODUT")+oModelZZV:GETVALUE("ZZV_TES"),.T.)

			RecLock("ZZV",.F.)


			ZZV->ZZV_DVRENT	:= oModel:Getmodel("ZZVDETAIL"):GetValue("ZZV_DVRENT")
			ZZV->ZZV_QTDENT	:= oModel:Getmodel("ZZVDETAIL"):GetValue("ZZV_QTDENT")

			ZZV->(MsUnLock())

		endif
	next

	dbSelectArea("ZZH")
	dbSetOrder(1)

	If DbSeek(oModel:GetModel('ZZHMASTER'):GetValue('ZZH_FILIAL')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_NUMERO')+oModel:GetModel('ZZHMASTER'):GetValue('ZZH_MEDICA'))

		RecLock("ZZH",.F.)
		ZZH->ZZH_OBSAJU := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_OBSAJU")
		ZZH->ZZH_TPAJU  := oModel:Getmodel("ZZHMASTER"):GetValue("ZZH_TPAJU")
		IF nAjuste > 0
			ZZH->ZZH_STATUS := "AJ"
		ENDIF
		ZZH->(MsUnLock())				
	endif

	dbCloseArea("ZZH")



Return .T.