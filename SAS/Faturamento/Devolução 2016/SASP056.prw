#Include 'Protheus.ch'
#include "topconn.ch"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} SASP056
Faturamento das Devolu��es em JOB
@author Diogo Costa
@since 28/01/2016
@version P11
@obs	ZZH_STFAT
P-Pendente Faturamento
Q-Pendente Cancelamento
F-Faturado
C-Cancelado
EF-Erro Faturamento
EC-Erro Cancelamento
/*/
User Function SASP056()
	Local cQuery
	Local lErro		:= .F.
	Local oError
	Local cMsgErro
	Private cSASStaErr	:= "E"

	Sleep(2000)
	//If !LockByName("SASP056-4",.F.,.F.)
	//	ConOut("Processso ja inicializado!")
	//	Return
	//EndIf

	RpcSetEnv("01","010101",,,"FAT","U_SASP056",{})
	cQuery	:= "SELECT TOP 1 R_E_C_N_O_ AS REGZZH FROM "+RETSQLNAME("ZZH")+" ZZH WHERE ZZH_STFAT In ('P','Q','N2','N4')  ORDER BY ZZH_PRIORI"
	// Seta que RecLock, em caso de falha, nao vai fazer retry automatico

	SetLoopLock(.F.)
	While !killapp()
		Monitor("Devolu��o:|Aguardando tarefas...")	//Atualiza mensagem no monitor
		TCQuery cQuery new alias TMPZZH
		If TMPZZH->(EOF())
			TMPZZH->(DbCloseArea())
			return
			Sleep(10000)
			Loop
		EndIF
		lErro		:= .F.
		cMsgErro	:= ""
		ZZH->(DbGoTo(TMPZZH->REGZZH))
		RecLock("ZZH",.F.)
		oError := ErrorBlock({|e| lErro := .T.,ErroLog(e,cSASStaErr) } )
		dDatabase := date()
		If ALLTRIM(ZZH->ZZH_STFAT) $ "P|N2|N4"	//Pendente Faturamento
			Faturar(@lErro,@cMsgErro)
		ElseIf ALLTRIM(ZZH->ZZH_STFAT)=="Q"	//Pendente Cancelar Faturamento
			Cancelar(@lErro,@cMsgErro)
		EndIf
		Logs(Replicate("=",30))
		ZZH->(MsUnLock())
		ErrorBlock(oError)
		TMPZZH->(DbCloseArea())
		RETURN
	EndDo
	UnLockByName("SASP056-4",.F.,.F.)

Return

Static Function Faturar(lErro,cMsgErro)
	Local cCodUsr	:= "000000"
	Local aDocFat	:= {}
	cSASStaErr		:= "EF"
	Monitor("Devolu��o:Processando Contrato:"+ZZH->ZZH_NUMERO+" medi��o:"+ZZH->ZZH_MEDICA)	//Atualiza mensagem no monitor
	Logs("Contrato:"+ZZH->ZZH_NUMERO)
	Logs("Processando Medicao:"+ZZH->ZZH_MEDICA)
	Begin Sequence



		//Gera��o da Nota de Entrada 1 (Cliente -> Subsidi�ria)
		If !lErro .and. !Empty(ZZH->ZZH_PC01NF) .AND. ZZH->ZZH_TPMED =='2'  //ENTRADA NO COMERCIAL - N�O CONTRIBUINTE
			Monitor("Devolu��o:Processando Contrato:"+ZZH->ZZH_NUMERO+" medi��o:"+ZZH->ZZH_MEDICA+"|"+" CLASSIFICANDO NOTA:"+ZZH->ZZH_PC01NF)
			Logs("CLASSIFICANDO NOTA :"+ZZH->ZZH_PC01FL+"|"+ZZH->ZZH_PC01NF)
			Begin Transaction
				If FatuPC(ZZH->ZZH_PC01FL,@aDocFat,@cMsgErro,ZZH->ZZH_PC01NF,ZZH->ZZH_SER01,ZZH->ZZH_FONF01,ZZH->ZZH_LJNF01) .and. len(aDocFat) > 0
					RecLock("ZZH",.F.)
					ZZH->ZZH_PC01NF	:= aDocFat[1]
					ZZH->ZZH_SER01	:= aDocFat[2]
					MsUnLock()
				ElseIF !"j� foi classificada." $ cMsgErro
					DisarmTransaction()
					Logs("PC01:"+cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf

		if !lErro
			ConOut("Entrada 01 ok")
		endif

		//Gera��o da Nota de Sa�da 2 (Subsidi�ria -> Holding)
		//Regra: Se cliente contribuinte -> obrigat�rio a digita��o da Nota 3
		// Se cliente n�o contribuinte -> Gera Nota Fiscal
		If !lErro .AND. Empty(ZZH->ZZH_NF02) .AND. !Empty(ZZH->ZZH_PV02) .AND. ( (ZZH->ZZH_TPMED='1' .AND. ZZH->ZZH_STFAT = 'N2') .OR. (ZZH->ZZH_TPMED='2') )	//SAIDA DA MATRIZ PARA COMERCIAL
			Monitor("Devolu��o:Processando Contrato:"+ZZH->ZZH_NUMERO+" medi��o:"+ZZH->ZZH_MEDICA+"|"+"SAIDA DA SUBSIDIARIA para HOLDING Pedido:"+ZZH->ZZH_NF02FL+"|"+ZZH->ZZH_PV02)	//Atualiza mensagem no monitor
			Logs("GERANDO NOTA DE SA�DA Pedido:"+ZZH->ZZH_NF02FL+"|"+ZZH->ZZH_PV02)
			Begin Transaction
				If FGeraFat(ZZH->ZZH_NF02FL,ZZH->ZZH_PV02,@aDocFat,@cMsgErro) .and. len(aDocFat) > 0
					RecLock("ZZH",.F.)
					ZZH->ZZH_NF02	:= aDocFat[1]
					ZZH->ZZH_SER02	:= aDocFat[2]
					ZZH->ZZH_LOG	:= ""
					MsUnLock()
				Else
					DisarmTransaction()
					Logs("PV02:"+cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf

		if !lErro
			ConOut("saida 02 ok") 
		endif

		//Gera��o da Nota de Entrada 3
		If !lErro .and. !Empty(ZZH->ZZH_NF03) .and. ZZH->ZZH_TPMED =='2' //Entrada para n�o contribuinte (Cliente -> Holding) 
			Monitor("Devolu��o:Processando Contrato:"+ZZH->ZZH_NUMERO+" medi��o:"+ZZH->ZZH_MEDICA+"|"+"CLASSIFICANDO NOTA:"+ZZH->ZZH_NF03FL+"|"+ZZH->ZZH_NF03)
			Logs("CLASSIFICANDO NOTA :"+ZZH->ZZH_NF03FL+"|"+ZZH->ZZH_NF03)
			Begin Transaction
				If FatuPC(ZZH->ZZH_NF03FL,@aDocFat,@cMsgErro,ZZH->ZZH_NF03,ZZH->ZZH_SER03,ZZH->ZZH_FONF03,ZZH->ZZH_LJNF03) .and. len(aDocFat) > 0
					RecLock("ZZH",.F.)
					ZZH->ZZH_NF03	:= aDocFat[1]
					ZZH->ZZH_SER03	:= aDocFat[2]
					MsUnLock()
				Elseif !"j� foi classificada." $ cMsgErro
					DisarmTransaction()
					Logs("PC03:"+cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		if !lErro
			ConOut("Entrada 03 ok")
		endif
				
		If !lErro

			RecLock("ZZH",.F.)
			ZZH_STFAT:='F'
			MsUnLock()
		EndIf

		If !lErro
			//Mensagem da nota fiscal
			U_DEV007(ZZH->ZZH_FILIAL,ZZH->ZZH_NUMERO,ZZH->ZZH_MEDICA,ZZH->ZZH_CLIENT,ZZH->ZZH_LOJA,ZZH->ZZH_PC01FL,ZZH->ZZH_PC01NF,ZZH->ZZH_PC01NF,ZZH->ZZH_SER01,"NF1",ZZH->ZZH_LIVRO)

			U_DEV007(ZZH->ZZH_FILIAL,ZZH->ZZH_NUMERO,ZZH->ZZH_MEDICA,ZZH->ZZH_CLIENT,ZZH->ZZH_LOJA,ZZH->ZZH_NF02FL,ZZH->ZZH_PV02,ZZH->ZZH_NF02,ZZH->ZZH_SER02,"NF2",ZZH->ZZH_LIVRO)

			U_DEV007(ZZH->ZZH_FILIAL,ZZH->ZZH_NUMERO,ZZH->ZZH_MEDICA,ZZH->ZZH_CLIENT,ZZH->ZZH_LOJA,ZZH->ZZH_NF03FL,ZZH->ZZH_NF03,ZZH->ZZH_NF03,ZZH->ZZH_SER03,"NF3",ZZH->ZZH_LIVRO)
			//FATURADO
		ENDIF

	End Sequence
	MsUnLockAll()
	GETMV("MV_NUMITEN",.T.)
	SX6->(MsRUnLock())
	If lErro

		If ZZH->ZZH_TPMED = '1' //Contribuinte
			If ZZH->ZZH_STFAT = 'N1'
				cSASStaErr	:= 'F1'
			Elseif ZZH->ZZH_STFAT = 'N2'
				cSASStaErr	:= 'F2'
			Elseif ZZH->ZZH_STFAT = 'N3'
				cSASStaErr	:= 'F3'
			Endif		
		Else //N�o contribuinte
			cSASStaErr	:='EF'
		Endif


		RecLock("ZZH",.F.)
		ZZH_STFAT:=cSASStaErr//'EF'
		MsUnLock()
	EndIf
Return

Static Function Cancelar(lErro,cMsgErro)
	Local cCodUsr	 := "000000"
	Local aCabDE01   := {}
	Local aLinDe01   := {}
	Local aItensDE01 := {}
	Local lMsErroAuto 
	Local cFilBack

	cSASStaErr	:= "EC"
	Monitor("Devolu��o:Cancelar Faturamento Contrato:"+ZZH->ZZH_NUMERO+" Medi��o:"+ZZH->ZZH_MEDICA)	//Atualiza mensagem no monitor
	Logs("Contrato:"+ZZH->ZZH_NUMERO)
	Logs("Processando Medicao:"+ZZH->ZZH_MEDICA)
	Begin Sequence

		

		If !lErro .AND. !Empty(ZZH->ZZH_NF02)
			Logs("CANCELANDO NF, SAIDA DA MATRIZ PARA COMERCIAL NF:"+ZZH->ZZH_NF02FL+"|"+ZZH->ZZH_NF02)
			Begin Transaction
				If FCancFat(ZZH->ZZH_NF02FL,ZZH->ZZH_NF02,ZZH->ZZH_SER02,@cMsgErro)
					RecLock("ZZH",.F.)
					ZZH->ZZH_NF02	:= ""
					ZZH->ZZH_SER02	:= ""
					MsUnLock()
				Else
					DisarmTransaction()
					Logs(cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		
		If !lErro .AND. !Empty(ZZH->ZZH_PC01NF)
			Logs("CANCELANDO NF, SAIDA DA COMERCIAL PARA O CLIENTE NF:"+ZZH->ZZH_PC01FL+"|"+ZZH->ZZH_PC01NF)
			Begin Transaction
				If FCancNfEnt(ZZH->ZZH_PC01FL,ZZH->ZZH_PC01NF,ZZH->ZZH_SER01,"",@cMsgErro,ZZH->ZZH_FONF01,ZZH->ZZH_LJNF01)
					
					cFilBack := cFilant
					
					cFilant := ZZH->ZZH_PC01FL
				
					// GERANDO NOVA PRE-NOTA
					cQuery := "select * "
					cQuery += "FROM  " + retsqlname('SF1') + " SF1 (nolock) "
					cQuery += "where  F1_FILIAL  = '" + ZZH->ZZH_PC01FL + "' "
					cQuery += "AND    F1_DOC     = '" + ZZH->ZZH_PC01NF + "' "
					cQuery += "AND    F1_SERIE   = '" + ZZH->ZZH_SER01  + "' "
					cQuery += "AND    F1_FORNECE = '" + ZZH->ZZH_FONF01 + "' "
					cQuery += "AND    F1_LOJA    = '" + ZZH->ZZH_LJNF01 + "' "
					cQuery += "AND    D_E_L_E_T_ = '*' "

					TcQuery cQuery new alias TF1
					
					IF TF1->(eof())
						conout("NF1 N�O DELETADA - MOTIVO DO ERRO -")
					ENDIF
					
					while !TF1->(eof())

						aadd(aCabDE01,{"F1_FILIAL" ,TF1->F1_FILIAL})
						aadd(aCabDE01,{"F1_TIPO"   ,TF1->F1_TIPO})
						aadd(aCabDE01,{"F1_FORMUL" ,TF1->F1_FORMUL})
						aadd(aCabDE01,{"F1_DOC"    ,NxtSX5Nota("1  ",.T.,SuperGetMV("MV_TPNRNFS"))}) //getsxenum("SF1","F1_DOC",,1)})
						aadd(aCabDE01,{"F1_SERIE"  ,TF1->F1_SERIE})						
						aadd(aCabDE01,{"F1_EMISSAO",dDataBase})
						aadd(aCabDE01,{"F1_FORNECE",TF1->F1_FORNECE})
						aadd(aCabDE01,{"F1_LOJA"   ,TF1->F1_LOJA})
						aadd(aCabDE01,{"F1_ESPECIE",TF1->F1_ESPECIE})
						aadd(aCabDE01,{"F1_ESPECI1",TF1->F1_ESPECI1})
						aadd(aCabDE01,{"F1_VOLUME1",TF1->F1_VOLUME1})		
						aadd(aCabDE01,{"F1_DESCONT",TF1->F1_DESCONT})
						
						TF1->(DbSkip())
					enddo 

					TF1->(dbclosearea())
					
					cQuery := "select D1_ITEM,D1_COD,MAX(R_E_C_N_O_) RECNO "
					cQuery += "FROM  " + retsqlname('SD1') + " SD1  (nolock) "
					cQuery += "where  D1_FILIAL  = '" + ZZH->ZZH_PC01FL + "' "
					cQuery += "AND    D1_DOC     = '" + ZZH->ZZH_PC01NF + "' "
					cQuery += "AND    D1_SERIE   = '" + ZZH->ZZH_SER01  + "' "
					cQuery += "AND    D1_FORNECE = '" + ZZH->ZZH_FONF01 + "' "
					cQuery += "AND    D1_LOJA    = '" + ZZH->ZZH_LJNF01 + "' "
					cQuery += "AND    D_E_L_E_T_ = '*' "
					cQuery += "GROUP BY D1_ITEM,D1_COD "


					TcQuery cQuery new alias TD1

					WHILE !TD1->(EOF())

						SD1->(dbGoto(TD1->RECNO))

						aLinDe01 := {}

						//AADD(aItens[nTam],{"D1_FILIAL"     ,oModelZZH:GetValue('ZZH_FILIAL')          ,Nil})
						AADD(aLinDe01,{"D1_ITEM"      ,SD1->D1_ITEM                                                 ,Nil})
						AADD(aLinDe01,{"D1_COD"       ,SD1->D1_COD                                            ,Nil})
						AADD(aLinDe01,{"D1_NFORI"     ,SD1->D1_NFORI                                                 ,Nil})
						AADD(aLinDe01,{"D1_SERIORI"   ,SD1->D1_SERIORI                                               ,Nil})
						AADD(aLinDe01,{"D1_ITEMORI"   ,SD1->D1_ITEMORI                                               ,Nil})
						AADD(aLinDe01,{"D1_QUANT"     ,SD1->D1_QUANT                                                 ,Nil})
						AADD(aLinDe01,{"D1_VUNIT"     ,SD1->D1_VUNIT                                                 ,Nil})
						//AADD(aLinDe01,{"D1_DESC"      ,nDescont                                                    ,Nil})
						AADD(aLinDe01,{"D1_TES"       ,SD1->D1_TES                                                   ,Nil})
						AADD(aLinDe01,{"D1_YCODKIT"   ,SD1->D1_YCODKIT                                               ,Nil})
						AADD(aLinDe01,{"D1_YPAIKIT"   ,SD1->D1_YPAIKIT                                               ,Nil})     
						AADD(aLinDe01,{"D1_LOCAL"     ,/*GETMV("SA_ARMNF2")*/SD1->D1_LOCAL                                                ,Nil})
						AADD(aLinDe01,{"D1_CC"        ,"9006"                                                        ,Nil})
//						Aadd(aLinha,  {"D1_YDTENTR"	  ,Ddatabase		                                              ,Nil})
						Aadd(aItensDE01,aLinDe01)

						TD1->(DbSkip())
					ENDDO
					
					TD1->(dbclosearea())

					lMsErroAuto	:= .F.
					//MATA140(aCabCliente,aItensDE01,3,.T.)
					MSExecAuto({|x,y,z| MATA140(x,y,z)}, aCabDE01, aItensDE01 , 3)


					If lMsErroAuto
						cFilant := cFilBak
						AutoGrLog("Erro ao gerar Pedido de compra, Cliente para a Comercial !")
						MostraErro()
						Return .F.
					Else

						RecLock("ZZH",.F.)
						ZZH->ZZH_PC01NF	:= ALLTRIM(SF1->F1_DOC)
						ZZH->ZZH_SER01	:= ALLTRIM(SF1->F1_SERIE)
						MsUnLock()
						
						aCabDE01   := {}
						aItensDE01 := {}
						
						cFilant := cFilback 
					ENDIF
				Else
					DisarmTransaction()
					Logs(cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		
		If !lErro .AND. !Empty(ZZH->ZZH_NF03)
			Logs("CANCELANDO NF, SAIDA MATRIZ PARA CLIENTE NF:"+ZZH->ZZH_NF03FL+"|"+ZZH->ZZH_NF03)
			Begin Transaction
				If FCancNfEnt(ZZH->ZZH_NF03FL,ZZH->ZZH_NF03,ZZH->ZZH_SER03,"",@cMsgErro,ZZH->ZZH_FONF03,ZZH->ZZH_LJNF03)//FCancNfEnt(ZZH->ZZH_NF03FL,ZZH->ZZH_NF03,ZZH->ZZH_SER03,@cMsgErro)
					
					cFilant := ZZH->ZZH_NF03FL
					// GERANDO NOVA PRE-NOTA
					cQuery := "select * "
					cQuery += "FROM  " + retsqlname('SF1') + " SF1 (nolock) "
					cQuery += "where  F1_FILIAL  = '" + ZZH->ZZH_NF03FL + "' "
					cQuery += "AND    F1_DOC     = '" + ZZH->ZZH_NF03 + "' "
					cQuery += "AND    F1_SERIE   = '" + ZZH->ZZH_SER03  + "' "
					cQuery += "AND    F1_FORNECE = '" + ZZH->ZZH_FONF03 + "' "
					cQuery += "AND    F1_LOJA    = '" + ZZH->ZZH_LJNF03 + "' "
					cQuery += "AND    D_E_L_E_T_ = '*' "

					TcQuery cQuery new alias TF1
					
					IF TF1->(eof())
						conout("NF3 N�O DELETADA - MOTIVO DO ERRO -")
					ENDIF

					while !TF1->(eof())

	
						aadd(aCabDE01,{"F1_FILIAL" ,TF1->F1_FILIAL})
						aadd(aCabDE01,{"F1_TIPO"   ,TF1->F1_TIPO})
						aadd(aCabDE01,{"F1_FORMUL" ,TF1->F1_FORMUL})
						aadd(aCabDE01,{"F1_DOC"    ,NxtSX5Nota("1  ",.T.,SuperGetMV("MV_TPNRNFS"))}) //getsxenum("SF1","F1_DOC",,1)})
						aadd(aCabDE01,{"F1_SERIE"  ,TF1->F1_SERIE})						
						aadd(aCabDE01,{"F1_EMISSAO",dDataBase})
						aadd(aCabDE01,{"F1_FORNECE",TF1->F1_FORNECE})
						aadd(aCabDE01,{"F1_LOJA"   ,TF1->F1_LOJA})
						aadd(aCabDE01,{"F1_ESPECIE",TF1->F1_ESPECIE})
						aadd(aCabDE01,{"F1_ESPECI1",TF1->F1_ESPECI1})
						aadd(aCabDE01,{"F1_VOLUME1",TF1->F1_VOLUME1})		
						aadd(aCabDE01,{"F1_DESCONT",TF1->F1_DESCONT})
						
						TF1->(DbSkip())
					enddo 

					TF1->(dbclosearea())

					cQuery := "select D1_ITEM,D1_COD,MAX(R_E_C_N_O_) RECNO "
					cQuery += "FROM  " + retsqlname('SD1') + " SD1  (nolock) "
					cQuery += "where  D1_FILIAL  = '" + ZZH->ZZH_PC01FL + "' "
					cQuery += "AND    D1_DOC     = '" + ZZH->ZZH_PC01NF + "' "
					cQuery += "AND    D1_SERIE   = '" + ZZH->ZZH_SER01  + "' "
					cQuery += "AND    D1_FORNECE = '" + ZZH->ZZH_FONF01 + "' "
					cQuery += "AND    D1_LOJA    = '" + ZZH->ZZH_LJNF01 + "' "
					cQuery += "AND    D_E_L_E_T_ = '*' "
					cQuery += "GROUP BY D1_ITEM,D1_COD "


					TcQuery cQuery new alias TD1

					WHILE !TD1->(EOF())

						SD1->(dbGoto(TD1->RECNO))

						aLinDe01 := {}

						//AADD(aItens[nTam],{"D1_FILIAL"     ,oModelZZH:GetValue('ZZH_FILIAL')          ,Nil})
						AADD(aLinDe01,{"D1_ITEM"      ,SD1->D1_ITEM                                                 ,Nil})
						AADD(aLinDe01,{"D1_COD"       ,SD1->D1_COD                                            ,Nil})
						AADD(aLinDe01,{"D1_NFORI"     ,SD1->D1_NFORI                                                 ,Nil})
						AADD(aLinDe01,{"D1_SERIORI"   ,SD1->D1_SERIORI                                               ,Nil})
						AADD(aLinDe01,{"D1_ITEMORI"   ,SD1->D1_ITEMORI                                               ,Nil})
						AADD(aLinDe01,{"D1_QUANT"     ,SD1->D1_QUANT                                                 ,Nil})
						AADD(aLinDe01,{"D1_VUNIT"     ,SD1->D1_VUNIT                                                 ,Nil})
						//AADD(aLinDe01,{"D1_DESC"      ,nDescont                                                    ,Nil})
						AADD(aLinDe01,{"D1_TES"       ,SD1->D1_TES                                                   ,Nil})
						AADD(aLinDe01,{"D1_YCODKIT"   ,SD1->D1_YCODKIT                                               ,Nil})
						AADD(aLinDe01,{"D1_YPAIKIT"   ,SD1->D1_YPAIKIT                                               ,Nil})     
						AADD(aLinDe01,{"D1_LOCAL"     ,/*GETMV("SA_ARMNF2")*/SD1->D1_LOCAL                                                 ,Nil})
						AADD(aLinDe01,{"D1_CC"        ,"9006"                                                        ,Nil})
//						Aadd(aLinha,{"D1_YDTENTR"	  ,Ddatabase		                                             ,Nil})
						Aadd(aItensDE01,aLinDe01)

						TD1->(DbSkip())
					ENDDO
					
					TD1->(dbclosearea())

					lMsErroAuto	:= .F.
					//MATA140(aCabCliente,aItensDE01,3,.T.)
					MSExecAuto({|x,y,z| MATA140(x,y,z)}, aCabDE01, aItensDE01 , 3)


					If lMsErroAuto
						cFilant := cFilBak
						AutoGrLog("Erro ao gerar Pedido de compra, Matriz para o Cliente !")
						MostraErro()
						Return .F.
					Else
						RecLock("ZZH",.F.)
						ZZH->ZZH_NF03	:= ALLTRIM(SF1->F1_DOC)
						ZZH->ZZH_SER03	:= ALLTRIM(SF1->F1_SERIE)
						MsUnLock()
						
						aCabDE01   := {}
						aItensDE01 := {}
						
						cFilant := cFilback
						
					ENDIF
				Else
					DisarmTransaction()
					Logs(cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		If !lErro
			RecLock("ZZH",.F.)
			ZZH_STFAT := iif(ZZH->ZZH_TPMED='2', 'C', 'N1')
			MsUnLock()
		EndIf

		// GEREAR NOVAS PRE-NOTAS A CLASSIFICAR
		If !lErro

		ENDIF

	End Sequence
	MsUnLockAll()
	GETMV("MV_NUMITEN",.T.)
	SX6->(MsRUnLock())
	If lErro
		If ZZH->ZZH_TPMED = '1' //Contribuinte
			If ZZH->ZZH_STFAT = 'N1'
				cSASStaErr	:= 'E1'
			Elseif ZZH->ZZH_STFAT = 'N2'
				cSASStaErr	:= 'E2'
			Elseif ZZH->ZZH_STFAT = 'N3'
				cSASStaErr	:= 'E3'
			Endif		
		Else //N�o contribuinte
			cSASStaErr	:='EC'
		Endif
		RecLock("ZZH",.F.)
		ZZH_STFAT:=cSASStaErr
		MsUnLock()
	EndIf
Return


Static Function ErroLog(oErro,cStatus)
	DisarmTransaction()
	Logs(oErro:Description+CRLF+oErro:ErrorStack+CRLF+oErro:ErrorEnv,.T.)
	RecLock("ZZH",.F.)
	ZZH_STFAT :=cStatus	//Altera o status para erro
	MsUnLock()
	StartJob("U_SASP056",getenvserver(),.F.)	//Inicia um nova thread
	__Quit()									//Finaliza a thread atual
Return


/*/{Protheus.doc} FGeraFat
Fatura o Pedido de venda
@author Saulo
@since 03/09/2015
@version 1.0
@param cEmpFat, String, Filial onde esta o pedido de venda para faturar
@param cPedFat, String, Numero do pedido de venda para faturar
@param aDocFat, Array, Array que recebe por referencia para gravar o Numero da NF e Serie
@param cMsgErro, String, String que recebe por referencia para gravar a msg de erro
/*/
Static Function FGeraFat(cEmpFat,cPedFat,aDocFat,cMsgErro)
	Local aArea			:= GetArea()
	Local aPvlNfs		:= {}
	Local cSerie		:= PadR("1",GetSx3Cache("F2_SERIE","X3_TAMANHO"))
	Local cFilBkp		:= cFilAnt
	Local cNFSaida		:= ""
	Local lRet			:= .T.
	Local nVezes
	Default aDocFat		:= {}
	Default cMsgErro	:= ""
	cFilAnt := cEmpFat

	//ProcRegua(4)
	Logs("Gerando Nota Fiscal de Sa�da")
	ProcessMessage()

	U_PESOMED(cEmpFat,cPedFat)

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	If !(SC9->(DbSeek(cEmpFat+cPedFat))) //Caso n�o tenha sido Liberado, chama para realizar a Libera��o do Pedido
	Endif
	FLibPedSZ5(cEmpFat,cPedFat,@cMsgErro)

	dbSelectArea("SC9")
	SC9->(dbSetOrder(1))

	If SC9->(DbSeek(cEmpFat+cPedFat))
		While SC9->(!Eof()) .AND. SC9->C9_FILIAL == cEmpFat .AND. SC9->C9_PEDIDO == cPedFat
			cProd = SC9->C9_PRODUTO
			cQuant = CVALTOCHAR(SC9->C9_QTDLIB)
			If !Empty(SC9->C9_BLEST) .or. !Empty(SC9->C9_BLCRED)
				cSASStaErr	:= "BE"
				cMsgErro	:= cEmpFat +"|"+cPedFat +"| Pedido com Bloqueio | Bloqueio de estoque/Credito "
				Return .F.
			ELSEIF !Empty(SC9->C9_BLEST) 
				cSASStaErr	:= "BE"
				cMsgErro	:= cEmpFat +"|"+cPedFat +" | "+ cProd +" | "+ cQuant +" | Pedido com Bloqueio | Bloqueio de estoque "
				Return .F.
			ELSEIF  !Empty(SC9->C9_BLCRED)
				cSASStaErr	:= "BE"
				cMsgErro	:= cEmpFat +"|"+cPedFat +"| Pedido com Bloqueio | Bloqueio de Credito "
				Return .F.
			ENDIF
	
			SC6->(DbSeek(xFilial("SC6")+SC9->C9_PEDIDO+SC9->C9_ITEM+SC9->C9_PRODUTO))

			SE4->(DbSeek(xFilial("SE4")+SC5->C5_CONDPAG))
			SB1->(DbSeek(xFilial("SB1")+SC9->C9_PRODUTO))
			SB2->(DbSeek(xFilial("SB2")+SC9->C9_PRODUTO+SC9->C9_LOCAL))
			SF4->(DbSeek(xFilial("SF4")+SC6->C6_TES))

			aAdd(aPvlNfs,{;
			SC9->C9_PEDIDO,;
			SC9->C9_ITEM,;
			SC9->C9_SEQUEN,;
			SC9->C9_QTDLIB,;
			SC9->C9_PRCVEN,;
			SC9->C9_PRODUTO,;
			.F.,;
			SC9->(RECNO()),;
			SC5->(RECNO()),;
			SC6->(RECNO()),;
			SE4->(RECNO()),;
			SB1->(RECNO()),;
			SB2->(RECNO()),;
			SF4->(RECNO());
			})
			SC9->(DbSkip())
		EndDo
	EndIf

	If Len(aPvlNfs) == 0
		cMsgErro := "Pedido "+cPedFat+" n�o liberado"+ CHR(10) + CHR(13)
		RestArea(aArea)
		cFilAnt:= cFilBkp
		lRet	:= .F.
		Return lRet
	endif

	//Verifica se a numera��o da NF est� em uso:
	dbSelectArea("SX5")
	dbSetOrder(1)

	If SX5->(MsSeek(cEmpFat+"01"+cSerie))
		nVezes := 0
		While ( !SX5->(MsRLock()) )
			nVezes ++
			Logs("Aguardando libera��o de numera��o da Nota Fiscal de Sa�da ... "+cValtochar(nVezes))
			If ( nVezes > 30 )
				Exit
			EndIf
			Sleep(1000)
		EndDo
	Endif

	Logs("Gerando Nota Fiscal de Sa�da")

	//Gera documento de saida
	Pergunte("MT460A",.F.)
	cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .F. , .F. , .F. , .F., 0, 0, .F., .F.)
	//cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .T. , .T. , .F. , .F., 0, 0, .F., .F.) 

	If Empty(cNFSaida)
		cMsgErro := "N�o foi poss�vel gerar a Nota fiscal de Sa�da da Filial "+cFilAnt+ CHR(10) + CHR(13)
	Else
		aDocFat	:= {cNFSaida,cSerie}
		Logs("Nota Fiscal de Sa�da gerada")
	Endif

	dbSelectArea("SX5")
	//If !SX5->(MsRLock()) .and. SX5->(!EOF())
	SX5->(MsUnlock())
	//Endif

	RestArea(aArea)
	cFilAnt:= cFilBkp
Return lRet


/*/{Protheus.doc} FLibPedSZ5
Liberar Pedido de Venda
@type function
@author Saulo Gomes Martins
@since 03/09/2015
@version P11
@param cEmpFat, character, Empresa do Pedido
@param cPedFat, character, Numero do Pedido
@param cErro, character, Erro enviado por referencia
/*/
Static Function FLibPedSZ5(cEmpFat,cPedFat,cMsgErro)
	Local aArea1:= GetArea()
	Local aCab	:= {}
	Local aItem	:= {}
	Local lRet	:= .T.
	Default cMsgErro	:= ""

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	aCab:= {	{"C5_TIPO"		,SC5->C5_TIPO		,NIL};
	,{"C5_NUM"		,SC5->C5_NUM		,NIL};
	,{"C5_CLIENTE"	,SC5->C5_CLIENTE	,NIL};
	,{"C5_CLIENT"	,SC5->C5_CLIENT		,NIL};
	,{"C5_LOJACLI"	,SC5->C5_LOJACLI	,NIL};
	,{"C5_CONDPAG"	,SC5->C5_CONDPAG	,NIL};
	,{"C5_YFILIAL"	,SC5->C5_YFILIAL	,NIL};
	,{"C5_YMEDICA"	,SC5->C5_YMEDICA	,NIL};
	,{"C5_YTPVEN"	,SC5->C5_YTPVEN		,NIL};
	,{"C5_EMISSAO"  ,SC5->C5_EMISSAO	,NIL}}


	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	If SC6->(DbSeek(cEmpFat+cPedFat))
		While SC6->(!Eof()) .AND. SC6->C6_FILIAL == cEmpFat .AND. SC6->C6_NUM == cPedFat
			aAdd(aItem,{ {"C6_PRODUTO"	,SC6->C6_PRODUTO,NIL};
			,{"C6_ITEM"		,SC6->C6_ITEM   ,NIL};
			,{"C6_QTDVEN"	,SC6->C6_QTDVEN ,NIL};
			,{"C6_PRCVEN"	,SC6->C6_PRCVEN ,NIL};
			,{"C6_VALOR"	,SC6->C6_VALOR	,NIL};
			,{"C6_TES"		,If(!Empty(SC6->C6_YTES),SC6->C6_YTES,SC6->C6_TES)	,NIL};
			,{"C6_YTES"		,SC6->C6_YTES	,NIL};
			,{"C6_CLI"		,SC6->C6_CLI	,NIL};
			,{"C6_QTDLIB"	,SC6->C6_QTDVEN	,NIL};
			,{"C6_LOJA"		,SC6->C6_LOJA	,NIL};
			,{"C6_NFORI"	,SC6->C6_NFORI	,NIL};
			,{"C6_SERIORI"	,SC6->C6_SERIORI,NIL};
			,{"C6_ITEMORI"	,SC6->C6_ITEMORI,NIL};
			,{"C6_YCONTRA"	,SC6->C6_YCONTRA,NIL};
			,{"C6_YMEDICA"	,SC6->C6_YMEDICA,NIL};
			,{"C6_YITCONT"	,SC6->C6_YITCONT,NIL};
			,{"C6_YCODKIT"	,SC6->C6_YCODKIT,NIL};
			,{"C6_YPAIKIT"	,SC6->C6_YPAIKIT,NIL};
			,{"C6_LOCAL"	,GetMv("SA_ARMNF2"),NIL}})
			SC6->(DbSkip())
		EndDo

		lMsErroAuto := .F.
		//		__cInternet	:= nil
		MSExecAuto({|x,y,z|Mata410(x,y,z)},aCab,aItem,4)
		If lMsErroAuto
			cMsgErro 	:= MostraErro()
			cMsgErro	+= "Pedido "+cPedFat+" n�o foi liberado"+ CHR(10) + CHR(13)
			lRet		:= .F.
		Endif
	Endif

	RestArea(aArea1)
Return lRet

/*/{Protheus.doc} FatuPC
Gera documento de entrada referente ao Pedido de Compra
@author Diogo/Saulo
@since 04/09/2015
@version 1.0
/*/
Static Function FatuPC(cEmpFat,aDocFat,cMsgErro,cDocNF,cSerieNF,cForn,cLojaFor)
	Local aCabec	:= {}
	Local aItens	:= {}
	Local aLinha	:= {}
	Local cFilBkp	:= cFilAnt
	Local lOk		:= .T.
	Local cCFOP := ""
	Private lMsErroAuto := .F.
	cFilAnt	:= cEmpFat

	//ProcRegua(4)
	Logs("Gerando Nota Fiscal de Entrada")
	ProcessMessage()

	dbSelectArea("SF1")
	SF1->(dbSetOrder(1))
	If SF1->(dbSeek(xFilial("SF1",cEmpFat)+cDocNF+cSerieNF+cForn+cLojaFor))

		RecLock("SF1",.F.)
		SF1->F1_EMISSAO:= dDataBase
		MsUnlock()

		aadd(aCabec,{"F1_FILIAL"	,SF1->F1_FILIAL})
		aAdd(aCabec,{'F1_TIPO'		,SF1->F1_TIPO})
//		aadd(aCabec,{"F1_TIPO"   	,SF1->F1_TIPO})
		aadd(aCabec,{"F1_FORMUL" 	,SF1->F1_FORMUL})
		aadd(aCabec,{"F1_DOC"    	,SF1->F1_DOC})
		aadd(aCabec,{"F1_SERIE"  	,SF1->F1_SERIE})
		aadd(aCabec,{"F1_EMISSAO"  	,SF1->F1_EMISSAO})
		aadd(aCabec,{"F1_FORNECE"	,SF1->F1_FORNECE})
		aadd(aCabec,{"F1_LOJA"   	,SF1->F1_LOJA})
		aadd(aCabec,{"F1_ESPECIE"	,SF1->F1_ESPECIE})
		aadd(aCabec,{"F1_COND"   	,SF1->F1_COND})
		aadd(aCabec,{"F1_DESCONT"	,SF1->F1_DESCONT})
		aadd(aCabec,{"F1_VOLUME1"	,SF1->F1_VOLUME1})
		aadd(aCabec,{"F1_ESPECI1"	,SF1->F1_ESPECI1})
		aadd(aCabec,{"F1_YFILIAL"	,SF1->F1_YFILIAL})
		aadd(aCabec,{"F1_YCONTRA"	,SF1->F1_YCONTRA})
		aadd(aCabec,{"F1_YMEDICA"	,SF1->F1_YMEDICA})
		aadd(aCabec,{"E2_NATUREZ"	,GetMv("SA_NATDEV")})

		NNR->(dbSetOrder(1))

		cChave	:=	xFilial("SD1",cEmpFat)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA
		SD1->(dbSetOrder(1))
		SD1->(dbSeek(cChave)) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA
		
		// Valida��o se a Nota fiscal j� est� classificada
		//if Empty(SD1->D1-TES)
		//	Return lOk
		//endif
			
		While SD1->(!eof()) .and. Alltrim(cChave) == Alltrim(SD1->(D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA)) 

			cTES :=	FTESDev()	
			cCFOP := POSICIONE("SF4",1,XFILIAL("SF4") + cTes,"SF4->F4_CF")		
			
			aLinha := {}
			Aadd(aLinha,{"D1_FILIAL"    ,SD1->D1_FILIAL			,Nil})
			Aadd(aLinha,{"D1_ITEM"     	,SD1->D1_ITEM			,Nil})
			Aadd(aLinha,{"D1_COD"     	,SD1->D1_COD			,Nil})
			Aadd(aLinha,{"D1_QUANT"		,SD1->D1_QUANT			,Nil})
			Aadd(aLinha,{"D1_VUNIT"		,SD1->D1_VUNIT			,Nil})
			Aadd(aLinha,{"D1_TOTAL"		,SD1->D1_TOTAL			,Nil})
			Aadd(aLinha,{"D1_TES"		,cTES					,Nil})
			Aadd(aLinha,{"D1_CF"		,cCFOP					,Nil})
			Aadd(aLinha,{"D1_LOCAL"		,SD1->D1_LOCAL			,Nil})
			Aadd(aLinha,{"D1_NFORI"		,SD1->D1_NFORI			,Nil})
			Aadd(aLinha,{"D1_SERIORI"	,SD1->D1_SERIORI		,Nil})
			Aadd(aLinha,{"D1_VALDESC"	,SD1->D1_VALDESC		,Nil})
			Aadd(aLinha,{"D1_ITEMORI"	,SD1->D1_ITEMORI		,Nil})
			Aadd(aLinha,{"D1_VALDESC"	,SD1->D1_VALDESC		,Nil})
			Aadd(aLinha,{"D1_YCODKIT"	,SD1->D1_YCODKIT		,Nil})
			Aadd(aLinha,{"D1_YPAIKIT"	,SD1->D1_YPAIKIT		,Nil})
			Aadd(aLinha,{"D1_CC"	    ,"9006"		            ,Nil})
			
			
			If (!NNR->(MsSeek(xFilial("NNR")+GETMV("SA_ENTDEV"))))//(!NNR->(MsSeek(xFilial("NNR")+SC7->C7_LOCAL)))
				AGRA045(;
				{{"NNR_CODIGO",GETMV("SA_ENTDEV"),NIL};
				,{"NNR_DESCRI","Armazem "+GETMV("SA_ENTDEV"),NIL};
				};
				,3,{},{})
			EndIf
			aadd(aItens,aLinha)
			SD1->(DbSkip())
		EndDo

		MSExecAuto({|x,y,z| MATA103(x,y,z) },aCabec,aItens,4)

		If !lMsErroAuto
			lOk:= .T.
			aDocFat	:= {SF1->F1_DOC,SF1->F1_SERIE}
		Else
			lOk:= .F.
			cMsgErro := Mostraerro()
			cMsgErro += "N�o foi poss�vel gerar a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
		EndIf

	Endif

	cFilAnt:= cFilBkp

Return lOk

/*/{Protheus.doc} FCancFat
Faz exclus�o das Notas Fiscais de Sa�da
@author Saulo Gomes Martins
@since 11/09/2015
@version 1.0
/*/

Static Function FCancFat(cEmpFat,cDocNF,cSerNF,cMsgErro)
	Local cFilBkp	:= cFilAnt
	Local nSpedExc	:= GetNewPar("MV_SPEDEXC",24)
	Local lRet		:= .T.
	Local lMostraCTB:= .F.
	Local lAglCTB	:= .F.
	Local lContab	:= .F.
	Local lCarteira	:= .T.
	Local lRetExc
	Local aRegSD2	:= {}
	Local aRegSE1	:= {}
	Local aRegSE2	:= {}
	Local nHoras
	Default cMsgErro	:= ""
	cFilAnt:= cEmpFat

	dbSelectArea("SF2")
	SF2->(dbSetOrder(1))
	If SF2->(!dbSeek(cEmpFat+cDocNF+cSerNF))
		Logs("Registro n�o encotrado! "+cEmpFat+cDocNF+cSerNF)	//N�o dar erro, mas coloca em Log
		lRet	:= .T.
		Return lRet
	EndIF

	Logs("Excluindo Nota Fiscal de Sa�da "+cDocNF)
	//Verifica se poder� realizar a exclus�o da nota fiscal
	If !Empty(SF2->F2_DAUTNFE)
		nHoras := SubtHoras(SF2->F2_DAUTNFE,SF2->F2_HAUTNFE,Date(),SubStr(Time(),1,2)+":"+SubStr(Time(),4,2))
		If nHoras > nSpedExc
			cMsgErro+="N�o foi possivel excluir a(s) nota(s), pois o prazo para o cancelamento da(s) NF-e � de " + Alltrim(STR(nSpedExc)) +" horas "+chr(13)+chr(10)
			lRet	:= .F.
			Return lRet
		EndIf
	Endif

	cMarca := GetMark(,"SF2","F2_OK")

	RecLock("SF2",.F.)
	SF2->F2_OK := cMarca
	MSUNLOCK()

	lRetExc := MaCanDelF2("SF2",SF2->(RecNo()),@aRegSD2,@aRegSE1,@aRegSE2)

	If lRetExc
		SF2->(MaDelNFS(aRegSD2,aRegSE1,aRegSE2,lMostraCTB,lAglCTB,lContab,lCarteira))
	Else
		AutoGrLog("Erro na exclus�o do Documento de Sa�da da Filial "+cFilAnt)
		cMsgErro:=Mostraerro()
		lRet	:= .F.
	EndIf
	cFilAnt:= cFilBkp
Return lRet

/*/{Protheus.doc} FCancNfEnt
Efetua estorno do Documento de Entrada
@author Saulo Gomes Martins
@since 11/09/2015
@version P11
/*/
//ZZH->ZZH_PC01FL,ZZH->ZZH_PC01NF,ZZH->ZZH_SER01,@cMsgErro,ZZH->ZZH_FONF01,ZZH->ZZH_LJNF01
Static Function FCancNfEnt(cEmpFat,cDocNF,cSerNF,cNumPC,cMsgErro,cFonf01,cLjnf01)
	Local aCabec 		:= {}
	Local aItens		:= {}
	Local aLinha 		:= {}
	Local cFilBkp		:= cFilAnt
	Local lRet			:= .T.
	Local aFornece
	Private lMsErroAuto := .F.
	Default cMsgErro	:= ""
	cFilAnt				:= cEmpFat
	dbSelectArea("SC7")
	SC7->(dbSetOrder(1))
	SC7->(dbSeek(xFilial("SC7",cEmpFat)+cNumPC))
	aFornece	:= {SC7->C7_FORNECE,SC7->C7_LOJA}

	aFornece[1]	:= cFonf01
	aFornece[2]	:= cLjnf01


	dbSelectArea("SF1")
	SF1->(dbSetOrder(1)) //F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA+F1_TIPO
	If SF1->(!dbSeek(xFilial("SF1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]))
		conout("nf nao encontrada"+xFilial("SF1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]+"")
		cMsgErro := "Chave n�o encontrada: "+xFilial("SF1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]
		Return .F.
	EndIf
	Logs("Excluindo Nota Fiscal de Entrada "+cDocNF)

	aadd(aCabec,{"F1_TIPO"   ,SF1->F1_TIPO})
	aadd(aCabec,{"F1_FORMUL" ,SF1->F1_FORMUL})
	aadd(aCabec,{"F1_DOC"    ,SF1->F1_DOC})
	aadd(aCabec,{"F1_SERIE"  ,SF1->F1_SERIE})
	aadd(aCabec,{"F1_EMISSAO",SF1->F1_EMISSAO})
	aadd(aCabec,{"F1_FORNECE",SF1->F1_FORNECE})
	aadd(aCabec,{"F1_LOJA"   ,SF1->F1_LOJA})
	aadd(aCabec,{"F1_ESPECIE",SF1->F1_ESPECIE})
	aadd(aCabec,{"F1_YFILIAL",SF1->F1_YFILIAL})
	aadd(aCabec,{"F1_YCONTRA",SF1->F1_YCONTRA})
	aadd(aCabec,{"F1_YMEDICA",SF1->F1_YMEDICA})

	dbSelectArea("SD1")
	SD1->(dbSetOrder(1))	//D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
	SD1->(dbSeek(xFilial("SD1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]))

	While SD1->(!eof()) .and. SD1->D1_FILIAL+SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA == xFilial("SD1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]
		aLinha := {}
		Aadd(aLinha,{"D1_COD"     	,SD1->D1_COD			,Nil})
		Aadd(aLinha,{"D1_ITEM"     	,SD1->D1_ITEM			,Nil})
		Aadd(aLinha,{"D1_QUANT"		,SD1->D1_QUANT			,Nil})
		Aadd(aLinha,{"D1_VUNIT"		,SD1->D1_VUNIT			,Nil})
		Aadd(aLinha,{"D1_TOTAL"		,SD1->D1_TOTAL			,Nil})
		Aadd(aLinha,{"D1_PEDIDO"	,SD1->D1_PEDIDO			,Nil})
		Aadd(aLinha,{"D1_ITEMPC"	,SD1->D1_ITEMPC			,Nil})
		Aadd(aLinha,{"D1_LOCAL"		,/*GETMV("SA_ARMNF2")*/SD1->D1_LOCAL			,Nil})
		AADD(aLinha,{"D1_CC"        ,"9006"                 ,Nil})
//		Aadd(aLinha,{"D1_YDTENTR"	,Ddatabase		        ,Nil})

		NNR->(dbSetOrder(1))
		If (!NNR->(MsSeek(xFilial("NNR")+SD1->D1_LOCAL)))
			AGRA045(;
			{{"NNR_CODIGO",SD1->D1_LOCAL,NIL};
			,{"NNR_DESCRI","Armazem "+SD1->D1_LOCAL,NIL};
			};
			,3,{},{})
		EndIf
		aadd(aItens,aLinha)
		SD1->(DbSkip())
	EndDo

	//	MATA103(aCabec,aItens,5)
	MSExecAuto({|x,y,z| MATA103(x,y,z) },aCabec,aItens,5)

	If !lMsErroAuto
	Else
		lRet	:= .F.
		cMsgErro += Mostraerro()
		cMsgErro := "N�o foi poss�vel excluir a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
	EndIf
	cFilAnt:= cFilBkp

Return lRet

/*/{Protheus.doc} Logs
Grava log em arquivo texto no servidor
@author Saulo Gomes Martins
@since 22/04/2015
@version 1.0
@param cTexto, character, Texto do log
/*/
Static Function Logs(cTexto,lErro)
	Local cCodLog	:= SubStr(DTOS(Date()),1,6)
	Local cPasta	:= "\log\SASP056"
	Local cTexLog
	Default lErro	:= .F.
	FWMakeDir(cPasta)
	If File(cPasta+"\"+cCodLog+".txt")
		nHdl := fopen(cPasta+"\"+cCodLog+".txt" , 2 + 64 )
	Else
		nHdl := FCREATE(cPasta+"\"+cCodLog+".txt", FC_NORMAL)
	EndIf
	FSeek(nHdl, 0, 2)
	FWrite(nHdl, DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+CRLF )
	//cErroJob	+= DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+CRLF
	fclose(nHdl)
	If lErro
		RecLock("ZZH",.F.)
		cTexLog			:= Alltrim(ZZH->ZZH_LOG)
		ZZH->ZZH_LOG	:= cTexLog+DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+chr(10)
		MsUnLock()
		lErroJob	:= .T.
	EndIf
Return

Static Function Monitor(cMsg)

	tcsqlexec("INSERT INTO YJOB (JOBLOG) VALUES ('"+cMsg+"')")
	PutGlbValue("SASP056",DTOC(date(),"dd/mm/yy")+"|"+time()+cMsg)
	PtInternal(1,DTOC(date(),"dd/mm/yy")+"|"+time()+"|"+cMsg)	//Atualiza mensagem no monitor
Return

/*/{Protheus.doc} FTESDev
Identifica a TES de Devolu��o 
@author author
@since 04/02/2016
@version version
@example
(examples)
@see (links_or_references)
/*/
Static Function FTESDev
	Local cRet:= ""
	dbSelectArea("SD2")
	SD2->(dbSetOrder(3)) //D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM
	If SD2->(dbSeek(SD1->D1_FILIAL+SD1->D1_NFORI+SD1->D1_SERIORI+SD1->D1_FORNECE+SD1->D1_LOJA+SD1->D1_COD+SD1->D1_ITEMORI))
		cRet:= Posicione("SF4",1,xFilial("SF4",SD2->D2_FILIAL)+SD2->D2_TES,"F4_TESDV")
	Endif	
Return cRet
