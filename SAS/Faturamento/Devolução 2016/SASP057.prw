#Include 'Protheus.ch'
#INCLUDE 'FWMBROWSE.CH'
#INCLUDE 'FWMVCDEF.CH'

/*/{Protheus.doc} SASP057
Tela para marca��o de faturamento da Devolu��o
@type function
@author Diogo Costa
@since 02/02/2016
@version P11
/*/
User Function SASP057()
	Local oDlg
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Local cTextHead	:= "QLabel { color: rgb(39,82,102); font-size: 12px; font-weight: bold;  } "
	Local oFWLayer
	Local aOperacao	:= {" ","Nota 1 Contrib.","Nota 2 Contrib.","Nota 3 Contrib.","Devolver N�o Contribuinte","Pendente de Entrada","Cancelar NF","Abortar Fatur./Cancel.","Alterar Prioridade"}
	Private aObjTela	:= {}
	Private cOperacao	:= aOperacao[1]
	Private oMark
	Private nPenFat		:= 0
	Private nPenCan		:= 0
	Private nPenErr		:= 0
	Private nPenTFat	:= 0
	Private nPenTCan	:= 0
	Private nPenTErr	:= 0
	Private cHoraAtu	:= ""
	Private cMonitor	:= ""
	//StartJob("U_SASP058",getenvserver(),.F.)	//Inicia um nova thread
	AtualTot()
	oDlg := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Faturamento',,,,nOr(WS_VISIBLE,WS_POPUP),,,,,.T.,,,,)

	@00,00 MSPANEL oBar SIZE 33,33 of oDlg
	oBar:Align := CONTROL_ALIGN_TOP
	oBtn := TButton():New(01,01,"     Atualizar",oBar ,{|| AtualTot(),oMark:Refresh(.F.) },50,20,,,.F.,.T.,.F.,,.F.,,,.F. )
	oBtn:SetCss(CssbtnImg("reload.png","#006386"))
	oSay := TSay():New(001,060, {||"Pendente Faturamento Filial: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(008,060, {||"Pendente Cancelamento Filial: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(015,060, {||"Erros Filial: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(001,160, {||Transform(nPenFat,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(008,160, {||Transform(nPenCan,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(015,160, {||Transform(nPenErr,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(001,200, {||"Pendente Faturamento Total: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(008,200, {||"Pendente Cancelamento Total: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(015,200, {||"Erros Total: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(001,300, {||Transform(nPenTFat,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(008,300, {||Transform(nPenTCan,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(015,300, {||Transform(nPenTErr,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(001,350, {||"Opera��o:"},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	TComboBox():New(008,350,{|u|if(PCount()>0,cOperacao:=u,cOperacao)},;
	aOperacao,090,20,oBar,,{|| TrocaOperacao() };
	,,,,.T.,,,,,,,,,'cOperacao')

	oSay := TSay():New(024,010, {||"Status JOB:"+cMonitor},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(015,600, {||"Atualizado:"+cHoraAtu},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)

	oMark 	:= FWMarkBrowse():New()
	oMark:SetAlias('ZZH')
	oMark:SetSemaphore(.T.)
	oMark:SetValid({|| Valid() })
	oMark:SetAfterMark({|| dBClick()})
	oMark:SetDescription('Faturamento')
	oMark:SetFieldMark( 'ZZH_OK' )
	oMark:SetAllMark({|| MarcaTudo() })

	oMark:AddLegend( "ZZH_STFAT=='C '"                      ,"BR_CANCEL"    ,"Cancelada")
	oMark:AddLegend( "ZZH_STFAT=='L '"						,"BR_LARANJA"	,"Liberado N�o Contribuinte")
	oMark:AddLegend( "ZZH_STFAT $ 'E1|E2|E3' "				,"BR_PRETO"	    ,"Erro Cancelar Contrib.")
	oMark:AddLegend( "ZZH_STFAT $ 'F1|F2|F3' "				,"BR_MARROM"	,"Erro Faturar  Contrib.") 
	oMark:AddLegend( "ZZH_STFAT=='EC'"						,"BR_CINZA"		,"Erro Cancelar N�o Contrib.")
	oMark:AddLegend( "ZZH_STFAT=='EF'"						,"BR_PRETO"		,"Erro Faturar  N�o Contrib.")
	oMark:AddLegend( "ZZH_STFAT=='F '"						,"BR_VERDE"		,"Faturado")
	oMark:AddLegend( "ZZH_STFAT=='Q '"						,"BR_AZUL"		,"Pendente Cancelamento")
	oMark:AddLegend( "ZZH_STFAT=='P '"						,"BR_AMARELO"	,"Pendente Faturamento")
	oMark:AddLegend( "ZZH_STFAT $ 'N1|N2|N3' "				,"BR_VERMELHO"	,"Nota Contribuinte")

	oMark:SetMenuDef( 'SASP057' )
	oMark:Activate(oDlg)
	oTimer := TTimer():New(3000,{||AtualTot(),aEval(aObjTela,{|x| x:Refresh()})}, oDlg )
	oTimer:Activate()
	oDlg:Activate(,,,.T.)
Return

Static Function TrocaOperacao()


	Local cEtpfim := ''

	LimparMarca()

	If Empty(cOperacao)
		oMark:SetFilterDefault(".T.")

	ElseIf cOperacao=="Nota 1 Contrib."
		oMark:SetFilterDefault("ZZH_STFAT $ 'N1' .AND. ZZH_TPMED='1' .and. ZZH->ZZH_STATUS <> 'BL' .AND. ((ZZH->ZZH_COLETA = '01' .AND. ZZH->ZZH_TRANSP <> '') .OR. (ZZH->ZZH_COLETA = '02' .AND. ZZH->ZZH_TRANSP = '') )")

	ElseIf cOperacao=="Nota 2 Contrib."
		oMark:SetFilterDefault("ZZH_STFAT $ 'N2' .AND. ZZH_TPMED='1' .AND. empty(ZZH->ZZH_PC02NF) .AND. !empty(ZZH->ZZH_NF02) ")

	ElseIf cOperacao=="Nota 3 Contrib."
		oMark:SetFilterDefault("ZZH_STFAT $ 'N3' .AND. ZZH_TPMED='1' ")

	ElseIf cOperacao=="Devolver N�o Contribuinte"
		oMark:SetFilterDefault("(ZZH_STFAT $ ' |C |L ' .OR. ZZH_STFAT == 'EF') .AND. ZZH_TPMED = '2' .AND. ZZH->ZZH_STATUS <> 'BL' .AND. ((ZZH->ZZH_COLETA = '01' .AND. ZZH->ZZH_TRANSP <> '') .OR. (ZZH->ZZH_COLETA = '02' .AND. ZZH->ZZH_TRANSP = '') )")

	ElseIf cOperacao=="Cancelar NF"
		oMark:SetFilterDefault("ALLTRIM(ZZH_STFAT) $ 'F|EC|N1|N2|N3' ")

	ElseIf cOperacao=="Abortar Fatur./Cancel."
		oMark:SetFilterDefault("ZZH_STFAT=='P '.or.ZZH_STFAT='Q '")

	Elseif cOperacao=="Pendente de Entrada"
		//Situa��o Finalizada, Nota 2 Gerada e Opera��o N�o sendo Direta
		oMark:SetFilterDefault("ZZH_STFAT $ 'F ' .AND. ((empty(ZZH->ZZH_PC02NF) .and. !empty(ZZH->ZZH_PC02) .and. ZZH->ZZH_ESPEC  <> '01' .AND. ZZH_MATENT == '01') .or.(ZZH->ZZH_ESPEC == '01' .AND. ZZH->ZZH_TPAJU == '' .AND. (ZZH_STACOM == '"+GETMV('SA_MATENTR')+"' .or. ZZH->ZZH_COLETA == '02' ) )) ")
		//oMark:SetFilterDefault("ZZH_STFAT $ 'F ' .AND. empty(ZZH->ZZH_PC02NF) .and. !empty(ZZH->ZZH_PC02) .and. ZZH->ZZH_ESPEC  <> '01' .AND. ZZH_MATENT == '01' ")

	ElseIf cOperacao=="Alterar Prioridade"
		oMark:SetFilterDefault(".T.")
	EndIf
	oMark:ExecuteFilter()
	AtualTot()
	oMark:Refresh(.T.)
Return

Static Function MenuDef()
	Local aRotina := {}
	Local aNota	:= {}
	Local aPedido	:= {}
	Local aDevol	:= {}
	ADD OPTION aRotina	TITLE 'Visualizar'	 					ACTION 'U_SASP057B' OPERATION 2 ACCESS 0
	ADD OPTION aRotina	TITLE 'Processar Marcados'	 			ACTION 'U_SASP057A()' OPERATION 4 ACCESS 0
	ADD OPTION aRotina	TITLE 'Re-Liberar'	 		            ACTION 'U_SASP057C()' OPERATION 4 ACCESS 0
	ADD OPTION aRotina	TITLE 'Informe Nota 3'		 			ACTION 'U_SASP057E()' OPERATION 4 ACCESS 0
Return aRotina

Static Function dBClick()
	If Empty(ZZH->ZZH_TRANSP) .AND. (Empty(ZZH->ZZH_TABFRE) .AND. ZZH->ZZH_FRETE == 0)
		MsgAlert("Medi��o n�o cont�m transportadora e frete.", "Devolu��o")
	Elseif Empty(ZZH->ZZH_TRANSP)
		MsgAlert("Medi��o n�o cont�m transportadora", "Devolu��o")
	Elseif Empty(ZZH->ZZH_TABFRE) .AND. ZZH->ZZH_FRETE == 0
		MsgAlert("Medi��o n�o cont�m frete.", "Devolu��o")				
	Endif
Return

Static Function Valid()
	nCount := 0

	If Empty(cOperacao)
		MsgAlert("Escolha a opera��o primeiro", "Devolu��o")
		Return .F.
	EndIf	
Return .T.

// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO MARCADO
STATIC FUNCTION YINVERTE(oMark)
	if empty(ZZH->ZZH_OK)
		oMark:Mark()
	endif
RETURN

User Function SASP057A(cTitulo,cStatus)

	Local lProcess	:=	.T.
	Local cMsgErro	:= ""
	Local aDocFat	:= {}


	if !MsgYesNo("Confirmar Processamento?")
		Return 
	ENDIF

	If Empty(cOperacao)
		MsgAlert("Escolha a opera��o")
		Return
	ElseIf cOperacao=="Devolver N�o Contribuinte"
		cTitulo	:= cOperacao
		cStatus	:= "P"

	ElseIf cOperacao=="Nota 1 Contrib."
		lProcess:=.F.
		If ZZH->ZZH_STFAT = 'N1' .and. !empty(ZZH->ZZH_PRNF01)
			//Entrada da Nota 1 
			Processa({|lEnd| FATNfEntr(ZZH->ZZH_PC01FL,@aDocFat,@cMsgErro,ZZH->ZZH_PRNF01,ZZH->ZZH_SER01,@lEnd) },'Carregando Nota Fiscal',"Aguarde...",.T.)
			If len(aDocFat) > 0

				If ZZH->ZZH_ESPEC = '01' //Direto
					cSt	:=	'F'
				Else //Triangular
					cSt	:=	'N3'
				Endif

				RecLock("ZZH",.F.)
				ZZH->ZZH_STFAT	:=	cSt
				ZZH->ZZH_PRNF01	:= 	''
				ZZH->ZZH_PC01NF	:= aDocFat[1]
				ZZH->ZZH_SER01	:= aDocFat[2]
				MsUnlock()

				MsgInfo("Gerado Nota Fiscal de Entrada Cliente para Subsidi�ria")

			Endif

		Endif	

	Elseif cOperacao=="Nota 2 Contrib."		
		lProcess:= .F.
		//Entrada da Nota 2
		RecLock("ZZH",.F.)
		ZZH->ZZH_STFAT	:=	'F' //Inicializa a grava��o da NF2 de Entrada (Subsidi�ria -> Holding)
		MsUnlock()
		U_SASP057F()
		MsgInfo("Gerado Nota Fiscal de Entrada Holding para Subsidiaria")	

	Elseif cOperacao=="Nota 3 Contrib." .and. !empty(ZZH->ZZH_PRNF03)
		lProcess:= .F.
		//Entrada da Nota 3
		Processa({|lEnd| FATNfEntr(ZZH->ZZH_NF03FL,@aDocFat,@cMsgErro,ZZH->ZZH_PRNF03,ZZH->ZZH_SER03,@lEnd) },'Carregando Nota Fiscal',"Aguarde...",.T.)
		If len(aDocFat) > 0
			RecLock("ZZH",.F.)
			ZZH->ZZH_STFAT	:=	'N2' //Inicializa a grava��o da NF2 (Subsidi�ria -> Holding)
			ZZH->ZZH_PRNF03	:= 	''
			ZZH->ZZH_NF03	:= aDocFat[1]
			ZZH->ZZH_SER03	:= aDocFat[2]
			MsUnlock()	
			MsgInfo("Gerado Nota Fiscal de Entrada Cliente para Holding")

		Endif

	Elseif cOperacao=="Pendente de Entrada" 
		lProcess:= .F.
		U_SASP057F()

	ElseIf cOperacao=="Cancelar NF"
		If !MsgYesNo("Confirmar cancelamento da NF?")
			Return
		EndIf
		cTitulo	:= cOperacao
		cStatus	:= "Q "
	ElseIf cOperacao=="Abortar Fatur./Cancel."
		cTitulo	:= cOperacao
		cStatus	:= "  "
	ElseIf cOperacao=="Alterar Prioridade"
		cTitulo	:= cOperacao
		cStatus	:= nil

	Else
		Return
	EndIf
	If lProcess
		Processa({|lEnd| RunProc(cTitulo,cStatus,@lEnd) },cTitulo,"Aguarde...",.T.)
	Endif

Return


Static Function RunProc(cTitulo,cStatus,lEnd)
	Local cAlias		:= GetNextAlias()
	Local aPergs		:= {}
	Local aRet			:= {}
	Local cPrior		:= "99"
	Local cAvisos		:= ""
	If !QueryMarca(cAlias)
		MsgAlert("N�o foi marcado nenhum item!")
		(cAlias)->(DbCloseArea())
		Return .F.
	EndIF
	aAdd( aPergs ,{1,'Prioridade',cPrior,'99',"","",'.T.',60,.F.})
	If ParamBox(aPergs,cTitulo,aRet,,,,,,,"",.F.,.F.)
		While (cAlias)->(!EOF()) .and. !lEnd
			IncProc("Contrato:"+ZZH->ZZH_NUMERO+",Medi��o:"+ZZH->ZZH_MEDICA)
			ZZH->(DbGoTo((cAlias)->REGZZH))
			If !(ZZH->(SimpleLock()) .and. RecLock("ZZH",.F.))
				cAvisos			+= "N�o � possivel realizar opera��o, registro em uso. Contrato:"+ZZH->ZZH_NUMERO+"Medi��o:"+ZZH->ZZH_MEDICA+CRLF
				(cAlias)->(DbSkip())
				Loop
			EndIf
			ZZH->(DbGoTo((cAlias)->REGZZH))	//Atualizar registro posicionado!
			If cStatus=="  " .and. (ZZH->ZZH_STFAT=="F " .OR. ZZH->ZZH_STFAT=="C ")
				cAvisos			+= "N�o � possivel abortar, opera��o j� concluida. Contrato:"+ZZH->ZZH_NUMERO+"Medi��o:"+ZZH->ZZH_MEDICA+CRLF
				ZZH->ZZH_OK		:= Space(Len(ZZH->ZZH_OK))
				MsUnLock()
				(cAlias)->(DbSkip())
				Loop
			EndIF
			If ValType(cStatus)=="C"
				ZZH->ZZH_STFAT	:= cStatus
			EndIf
			ZZH->ZZH_PRIORI	:= StrZero(Val(aRet[1]),Len(ZZH->ZZH_STFAT))
			ZZH->ZZH_OK		:= Space(Len(ZZH->ZZH_OK))
			ZZH->ZZH_DTFAT  := DATE()
			ZZH->ZZH_USFAT  := UsrFullName(RetCodUsr())
			MsUnLock()
			(cAlias)->(DbSkip())
		EndDO
		oMark:Refresh(.T.)	//Atualiza e posiciona no primeiro registro
		AtualTot()
		If !Empty(cAvisos)
			MsgAlert(cAvisos)
		EndIF
		If lEnd
			MsgAlert("Altera��o cancelada!"+chr(13)+"Alguns registros podem ter sido alterado.")
		Else
			MsgInfo("Opera��o concluida!")
		EndIf
	EndIf
	(cAlias)->(DbCloseArea())
Return

Static Function QueryMarca(cAlias)
	Local cMarca	:= oMark:Mark()
	BeginSql Alias cAlias
	SELECT R_E_C_N_O_ AS REGZZH FROM %table:ZZH% ZZH WHERE ZZH_FILIAL=%xFilial:ZZH% AND ZZH.ZZH_OK = %Exp:cMarca% and ZZH.D_E_L_E_T_ = ' '
	EndSql
Return (cAlias)->(!EOF())

Static Function LimparMarca()
	Local cAlias	:= GetNextAlias()
	QueryMarca(cAlias)
	While (cAlias)->(!EOF())
		ZZH->(DbGoTo((cAlias)->REGZZH))
		RecLock("ZZH")
		ZZH->ZZH_OK		:= Space(Len(ZZH->ZZH_OK))
		MsUnLock()
		(cAlias)->(DbSkip())
	EndDO
Return

Static Function QueryDsMr(cAlias)
	Local cMarca	:= oMark:Mark()
	BeginSql Alias cAlias
	SELECT R_E_C_N_O_ AS REGZZH FROM %table:ZZH% ZZH WHERE ZZH_FILIAL=%xFilial:ZZH% AND ZZH.ZZH_OK <> %Exp:cMarca% and ZZH.D_E_L_E_T_ = ' '
	EndSql
Return (cAlias)->(!EOF())

Static Function MarcaTudo()
	Local cAlias	:= GetNextAlias()
	QueryDsMr(cAlias)
	While (cAlias)->(!EOF())
		alert("entrou")
		ZZH->(DbGoTo((cAlias)->REGZZH))
		RecLock("ZZH")
		ZZH->ZZH_OK		:= cMarca
		//alert("cMarca")
		MsUnLock()
		(cAlias)->(DbSkip())
	EndDO
Return

Static Function SasMark(oMark)
	MarcaTudo()
	AtualTot()
	oMark:Refresh(.T.)
RETURN


Static Function AtualTot()
	Local cAlias	:= GetNextAlias()
	Local aMonitor	:= {}
	Local nPos
	Local cFilZZH	:= xFilial("ZZH",cFilAnt)
	Local aArea		:= GetArea()
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZH% ZZH WHERE ZZH_FILIAL=%exp:cFilZZH% AND ZZH_STFAT='P ' and ZZH.D_E_L_E_T_ = ' '
	EndSql
	nPenFat	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZH% ZZH WHERE ZZH_FILIAL=%exp:cFilZZH% AND ZZH_STFAT='Q ' and ZZH.D_E_L_E_T_ = ' '
	EndSql
	nPenCan	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZH% ZZH WHERE ZZH_FILIAL=%exp:cFilZZH% AND (ZZH_STFAT like 'E%' OR ZZH_STFAT like 'BE') and ZZH.D_E_L_E_T_ = ' '
	EndSql
	nPenErr	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	//TOTAL
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZH% ZZH WHERE ZZH_STFAT='P ' and ZZH.D_E_L_E_T_ = ' '
	EndSql
	nPenTFat	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZH% ZZH WHERE ZZH_STFAT='Q ' and ZZH.D_E_L_E_T_ = ' '
	EndSql
	nPenTCan	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:ZZH% ZZH WHERE (ZZH_STFAT like 'E%' OR ZZH_STFAT like 'BE') and ZZH.D_E_L_E_T_ = ' '
	EndSql
	nPenTErr	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	cHoraAtu	:= TIME()
	cMonitor	:= GetGlbValue("SASP056")
	If ValType(cMonitor)=="U"
		cMonitor	:= ""
	EndIf
	RestArea(aArea)
	ProcessMessage()
Return

User Function SASP057C()

	Local cStat	:=	""
	If Alltrim(ZZH_STFAT)=='EF' .OR. Alltrim(ZZH_STFAT)=='EC'

		DBSELECTAREA("ZZH")
		DBSETORDER(1)
		DBSEEK(ZZH->ZZH_FILIAL + ZZH->ZZH_NUMERO + ZZH->ZZH_MEDICA + ZZH->ZZH_CLIENT + ZZH->ZZH_LOJA)

		If Found()

			If ZZH->ZZH_STFAT $ 'E1|F1'
				cStat:='N1'
			Elseif ZZH->ZZH_STFAT $ 'E2|F2'
				cStat:='N2'
			Elseif ZZH->ZZH_STFAT $ 'E3|F3'
				cStat:='N3'
			Elseif ZZH->ZZH_STFAT $ 'EC|EF'
				cStat:='L'
			Endif

			RECLOCK("ZZH", .F.)

			ZZH->ZZH_STFAT := cStat
			ZZH->ZZH_LOG := MSMM("",80)

			MSUNLOCK()

		EndIf

	Else

		MessageBox("O contrato n�o est� com ERROS. Verifique e tente novamente.","TOTVS",48)

	EndIf

Return

Static Function CssbtnImg(cImg,cCor)
	Local cRet	:= " QPushButton{ background-image: url(rpo:"+cImg+")"+;
	" ;border-style:solid"+;
	" ;border-width:5px"+;
	" ;background-repeat: none; margin: 1px "+;
	" ;background-color: "+cCor+""+;
	" ;border-radius: 6px"+;
	" ;font-weight: bold;font-size: 12px"+;
	" ;color: rgb(255,255,255) "+;
	"}"+;
	"QPushButton:hover { "+;
	" ;background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop:0 "+cCor+", stop:0.5 "+cCor+", stop:1 white)"+;
	"}"
Return cRet


User Function SASP057B
	FWExecView("Medi��o Devolu��o", "DEV002", MODEL_OPERATION_VIEW ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
Return


User Function SASP057X()
	Local lCliente	:= .T.		//Devolu��o usa cliente
	Local lFornece	:= .T.		//Utiliza Fornecedor
	Local cDocSF2   	:= ''
	Local cDocSF1		:= ''
	Local cCNPJMatriz	:= FWArrFilAtu("01","010105")[18]	//Matriz
	Local cCNPJColiga	:= FWArrFilAtu(cEmpAnt,cFilAnt)[18]	//Coligada
	Local aFornec		:= {,}
	Local aClientFil	:= {,}
	Local aDocDev		:= {}
	Local aDocSF1		:= {}
	Local aDocSF2		:= {}

	Local aLinha    := {}
	Local aItens    := {}
	Local cTipoNF   := ""
	Local lPoder3   := .T.
	Local nHpP3     := 0

	Private lCADSZ5D	:= .F.
	Private cAlias	:= getNextAlias()

	Private cCliente := CriaVar("F2_CLIENTE",.F.)
	Private cLoja    := CriaVar("F2_LOJA",.F.)
	Private cQrDvF2  := ""
	PRIVATE cTipo		:= "D"

	ProcessMessage()

	SA2->(DbSetOrder(3))	//A2_FILIAL+A2_CGC
	SA1->(DbSetOrder(3))	//A1_FILIAL+A1_CGC
	If SA2->(DbSeek(xFilial("SA2")+cCNPJMatriz))
		aFornec[1]	:= SA2->A2_COD
		aFornec[2]	:= SA2->A2_LOJA
	Else
		Aviso("Aviso","Necessario ter a Matriz cadastrado como fornecedor!",{"Ok"})
		Return
	EndIf

	If SA1->(DbSeek(xFilial("SA1","010105")+cCNPJColiga))
		aClientFil[1]	:= SA1->A1_COD
		aClientFil[2]	:= SA1->A1_LOJA
	Else
		Aviso("Aviso","Necessario essa coligada ter cadastrado como Cliente!",{"Ok"})
		Return
	EndIf

	While !(cAlias)->(Eof())
		If Empty((cAlias)->Z5_NF01)
			(cAlias)->(DbSkip())
			Loop
		Endif
		IncProc("Procurando NF para devolu��o...")
		ProcessMessage()
		#IFDEF TOP
		cDocSF2 += "('"+(cAlias)->(Z5_NF01+Z5_SERIE1)+"')"
		#ELSE
		cDocSF2 += "( SD2->D2_DOC == '" + (cAlias)->Z5_NF01 + "' .And. SD2->D2_SERIE == '" + (cAlias)->Z5_SERIE1 + "' ) "
		#ENDIF
		lCADSZ5D	:= .T.
		lCADSZ5D := StaticCall(MATA103,M103FilDv,@aLinha,@aItens,cDocSF2,(cAlias)->Z5_CLIENTE,(cAlias)->Z5_LOJA,.F.,@cTipoNF,@lPoder3,.T.,@nHpP3) .and. Len(aItens)>0
		If Len(aItens)==0
			Help(" ",1,"DSNOTESDT")
		EndIF
		If lCADSZ5D
			//Chama tela padr�o para dar entrada na devolu��o
			lMsErroAuto	:= .F.
			A103ProcDv("SF1",0,3,lCliente,(cAlias)->Z5_CLIENTE,(cAlias)->Z5_LOJA,cDocSF2)
			If lMsErroAuto
				lCADSZ5D := .F.
			EndIf
		EndIf

		Begin Transaction
			If lCADSZ5D	//Confirmou a grava��o
				IncProc("Criando NF de devolu��o para a Matriz")
				ProcessMessage()
				//Faz NF de saida(devolu��o de compras) Comercial para Matriz
				aCliente	:= {SF1->F1_FORNECE,SF1->F1_LOJA}
				aDocSF2	:= {}
				aDocDev	:= {SF1->F1_DOC,SF1->F1_SERIE,SF1->F1_FILIAL,SF1->F1_FORNECE,SF1->F1_LOJA}
				lCADSZ5D := DevPV(SF1->F1_DOC,SF1->F1_SERIE,aCliente,aFornec,@aDocSF2)
				If !lCADSZ5D
					DisarmTransaction()
				EndIf
				aCabecNF2:= {}
				aLinhaNF2:= {}

				//Entrada de Devolu��o
				If lCADSZ5D
					IncProc("Criando NF de Entrada da devolu��o do Comercial para a Matriz")
					ProcessMessage()
					lCADSZ5D := EntradaSF2("010105",cFilAnt,aDocSF2[1],aDocSF2[2],aFornec,aClientFil,aDocSF1,,1,aDocDev,.F.)

					IncProc("Criando NF de Entrada da devolu��o Cliente para Matriz")
					ProcessMessage()
					aDocMtz	:= {}
					lCADSZ5D := EntradaDEVF2("010105",aCabecNF2,aLinhaNF2,(cAlias)->Z5_NF03,(cAlias)->Z5_SERIE3,@aDocMtz) //EntradaSF2("010101","010101",aDocSF2[1],aDocSF2[2],aFornec,aClientFil,aDocSF1,,2,aDocDev)

				EndIf
			EndIf
			If !lCADSZ5D
				DisarmTransaction()
			Else
				RecLock("SZ8",.T.)
				SZ8->Z8_FILIAL	:= xFilial("SZ8")
				SZ8->Z8_CONTRAT	:= (cAlias)->Z5_CONTRAT
				SZ8->Z8_MEDICAO	:= (cAlias)->Z5_MEDICAO
				SZ8->Z8_DATA		:= dDataBase
				SZ8->Z8_EMP01		:= cFilAnt
				SZ8->Z8_NF01		:= aDocDev[1]				//CLIENTE PARA COMERCIAL
				SZ8->Z8_SER01		:= aDocDev[2]
				SZ8->Z8_CLIENTE	:= (cAlias)->Z5_CLIENTE
				SZ8->Z8_LOJA		:= (cAlias)->Z5_LOJA
				SZ8->Z8_NF02		:= aDocSF2[1]				//COMERCIAL PARA MATRIZ
				SZ8->Z8_SER02		:= aDocSF2[2]
				SZ8->Z8_EMPMTZ	:= "010105"
				SZ8->Z8_NFMTZ		:= aDocMtz[1]				//CLIENTE PARA MATRIZ
				SZ8->Z8_SERMTZ	:= aDocMtz[2]
				SZ8->Z8_ENTRADA	:= "N"
				SZ8->(MsUnLock())
			EndIf
		End Transaction
		MsUnLockAll()
		If lCADSZ5D
			FWExecView("Devolu��o", "CADSZ8", MODEL_OPERATION_VIEW ,/*oDlg*/ , {||.T.},/*bOk*/ ,/*nPercReducao*/ ,/*aEnableButtons*/ , /*bCancel*/ )
		EndIf
		(cAlias)->(DbSkip())
	EndDo
	(cAlias)->(DbCloseArea())
	TirarMarca()
Return

Static Function FATNfEntr(cEmpFat,aDocFat,cMsgErro,cDocNF,cSerieNF)
	Local aCabec	:= {}
	Local aItens	:= {}
	Local aPRENF	:= {}
	Local aLinha	:= {}
	Local cFilBkp	:= cFilAnt
	Local lOk		:= .T.
	Local aPergs	:=	{}
	Local aRet		:=  {}
	Local cCodF1DOC	:= 	ZZH->ZZH_NF03
	Local cCodF1SER	:=	ZZH->ZZH_SER03
	Local cEmisF1	:= dDatabase
	Private lMsErroAuto := .F.
	cFilAnt	:= cEmpFat

	ProcRegua(100)

	dbSelectArea("SF1")
	SF1->(dbSetOrder(1))
	If SF1->(dbSeek(xFilial("SF1",cEmpFat)+cDocNF+cSerieNF))

		aAdd( aPergs ,{1,GetSx3Cache("F1_DOC","X3_TITULO"),;
		cCodF1DOC,,".T.",,'.T.',60,.F.})

		aAdd( aPergs ,{1,GetSx3Cache("F1_SERIE","X3_TITULO"),;
		cCodF1SER,,".T.",,'.T.',60,.F.})

		aAdd( aPergs ,{1,GetSx3Cache("F1_EMISSAO","X3_TITULO"),;
		cEmisF1,,".T.",,'.T.',60,.F.})

		If !ParamBox(aPergs,"Preencher dados",aRet,,,,,,,)
			Return
		EndIf

		aCabPreNf:= {} //Cabe�alho Pr�-Nota

		aadd(aCabPreNf,{"F1_FILIAL"		,SF1->F1_FILIAL})
		aAdd(aCabPreNf,{'F1_TIPO'		,SF1->F1_TIPO})
		aadd(aCabPreNf,{"F1_FORMUL" 	,SF1->F1_FORMUL})
		aadd(aCabPreNf,{"F1_DOC"    	,SF1->F1_DOC})
		aadd(aCabPreNf,{"F1_SERIE"  	,SF1->F1_SERIE})
		aadd(aCabPreNf,{"F1_EMISSAO"  	,SF1->F1_EMISSAO})
		aadd(aCabPreNf,{"F1_FORNECE"	,SF1->F1_FORNECE})
		aadd(aCabPreNf,{"F1_LOJA"   	,SF1->F1_LOJA})
		aadd(aCabPreNf,{"F1_ESPECIE"	,SF1->F1_ESPECIE})
		aadd(aCabPreNf,{"F1_DESCONT"	,SF1->F1_DESCONT})
		aadd(aCabPreNf,{"F1_YFILIAL"	,SF1->F1_YFILIAL})
		aadd(aCabPreNf,{"F1_YCONTRA"	,SF1->F1_YCONTRA})
		aadd(aCabPreNf,{"F1_YMEDICA"	,SF1->F1_YMEDICA})


		aadd(aCabec,{"F1_FILIAL"	,SF1->F1_FILIAL})
		aAdd(aCabec,{'F1_TIPO'		,SF1->F1_TIPO})
		aadd(aCabec,{"F1_FORMUL" 	,'N'})
		aadd(aCabec,{"F1_DOC"    	,aRet[1]})
		aadd(aCabec,{"F1_SERIE"  	,aRet[2]})
		aadd(aCabec,{"F1_EMISSAO"  	,aRet[3]})
		aadd(aCabec,{"F1_FORNECE"	,SF1->F1_FORNECE})
		aadd(aCabec,{"F1_LOJA"   	,SF1->F1_LOJA})
		aadd(aCabec,{"F1_ESPECIE"	,SF1->F1_ESPECIE})
		aadd(aCabec,{"F1_DESCONT"	,SF1->F1_DESCONT})
		aadd(aCabec,{"F1_YFILIAL"	,SF1->F1_YFILIAL})
		aadd(aCabec,{"F1_YCONTRA"	,SF1->F1_YCONTRA})
		aadd(aCabec,{"F1_YMEDICA"	,SF1->F1_YMEDICA})

		NNR->(dbSetOrder(1))

		cChave	:=	xFilial("SD1",cEmpFat)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA
		SD1->(dbSetOrder(1))
		SD1->(dbSeek(cChave)) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA

		While SD1->(!eof()) .and. Alltrim(cChave) == Alltrim(SD1->(D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA)) 

			incProc()
			aLinha 		:= {}
			aLinPreNF	:= {}
			cTES		:= FTESDev()	

			//Exclus�o da Pr�-Nota
			Aadd(aLinPreNF,{"D1_FILIAL"    ,SD1->D1_FILIAL			,Nil})
			Aadd(aLinPreNF,{"D1_ITEM"     	,SD1->D1_ITEM			,Nil})
			Aadd(aLinPreNF,{"D1_COD"     	,SD1->D1_COD			,Nil})
			Aadd(aLinPreNF,{"D1_QUANT"		,SD1->D1_QUANT			,Nil})
			Aadd(aLinPreNF,{"D1_VUNIT"		,SD1->D1_VUNIT			,Nil})
			Aadd(aLinPreNF,{"D1_TOTAL"		,SD1->D1_TOTAL			,Nil})
			Aadd(aLinPreNF,{"D1_TES"		,SD1->D1_TES			,Nil})
			Aadd(aLinPreNF,{"D1_LOCAL"		,GETMV("SA_ARMNF2")/*SD1->D1_LOCAL*/			,Nil})
			Aadd(aLinPreNF,{"D1_VALDESC"	,SD1->D1_VALDESC		,Nil})
			Aadd(aLinha,{"D1_YDTENTR"	    ,Ddatabase		        ,Nil})


			aadd(aPRENF,aLinPreNF)

			//Gera��o da Nota Fiscal
			Aadd(aLinha,{"D1_FILIAL"    ,SD1->D1_FILIAL			,Nil})
			Aadd(aLinha,{"D1_ITEM"     	,SD1->D1_ITEM			,Nil})
			Aadd(aLinha,{"D1_COD"     	,SD1->D1_COD			,Nil})
			Aadd(aLinha,{"D1_QUANT"		,SD1->D1_QUANT			,Nil})
			Aadd(aLinha,{"D1_VUNIT"		,SD1->D1_VUNIT			,Nil})
			Aadd(aLinha,{"D1_TOTAL"		,SD1->D1_TOTAL			,Nil})
			Aadd(aLinha,{"D1_TES"		,IIF(empty(cTES),"001",cTES),Nil})
			Aadd(aLinha,{"D1_LOCAL"		,GETMV("SA_ARMNF2")		,Nil})
			Aadd(aLinha,{"D1_VALDESC"	,SD1->D1_VALDESC		,Nil})
			Aadd(aLinha,{"D1_NFORI"		,SD1->D1_NFORI			,Nil})
			Aadd(aLinha,{"D1_SERIORI"	,SD1->D1_SERIORI		,Nil})
			Aadd(aLinha,{"D1_ITEMORI"	,SD1->D1_ITEMORI		,Nil})
			Aadd(aLinha,{"D1_YCODKIT"	,SD1->D1_YCODKIT		,Nil})
			Aadd(aLinha,{"D1_YPAIKIT"	,SD1->D1_YPAIKIT		,Nil})
			Aadd(aLinha,{"D1_YDTENTR"	,Ddatabase		        ,Nil})

			If (!NNR->(MsSeek(xFilial("NNR")+GETMV("SA_ENTLOCAL"))))//(!NNR->(MsSeek(xFilial("NNR")+SC7->C7_LOCAL)))
				AGRA045(;
				{{"NNR_CODIGO",GETMV("SA_ENTLOCAL"),NIL};
				,{"NNR_DESCRI","Armazem "+GETMV("SA_ENTLOCAL"),NIL};
				};
				,3,{},{})
			EndIf
			aadd(aItens,aLinha)
			SD1->(DbSkip())
		EndDo

		incProc(60)

		MATA103(aCabec,aItens,3,.T.)

		IF MSGNOYES("Deseja faturar essa medi��o?","Medi��o Devolu��o")
			If !lMsErroAuto
				lOk:= .T.
				aDocFat	:= {SF1->F1_DOC,SF1->F1_SERIE}

				lMsErroAuto := .F.

				//Exclus�o da Pr�-Nota
				lMsErroAuto	:= .F.
				MSExecAuto({|x,y,z| MATA140(x,y,z)}, aCabPreNf, aPRENF , 5)

				If lMsErroAuto
					MostraErro()
				Endif	

			Else
				lOk:= .F.
				cMsgErro := Mostraerro()
				cMsgErro += "N�o foi poss�vel gerar a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
			EndIf
		ELSE
			lOk:= .F.
		ENDIF
	Endif

	cFilAnt:= cFilBkp

Return lOk

//Preenchimento dos dados da Nota Fiscal 
User Function SASP057E
	Local aPergs	:=	{}
	Local aRet		:=  {}
	Local cCodF1DOC	:= 	ZZH->ZZH_NF03FL
	Local cCodF1SER	:=	ZZH->ZZH_SER03

	If ZZH->ZZH_STFAT <> 'N1'
		MsgInfo("Nota n�o poder� ser atualizada ")
		Return
	Endif

	aAdd( aPergs ,{1,GetSx3Cache("F1_DOC","X3_TITULO"),;
	cCodF1DOC,,".T.",,'.T.',60,.F.})

	aAdd( aPergs ,{1,GetSx3Cache("F1_SERIE","X3_TITULO"),;
	cCodF1SER,,".T.",,'.T.',60,.F.})

	If !ParamBox(aPergs,"Preencher dados",aRet,,,,,,,)
		Return
	Else
		RecLock("ZZH",.F.)
		ZZH->ZZH_NF03FL	:=	aRet[1]
		ZZH->ZZH_SER03	:=	aRet[2]
		MsUnlock()
		MsgInfo("Informa��o atualizada")
	EndIf
Return

//TES de devolu��o
Static Function FTESDev
	Local cRet:= ""
	dbSelectArea("SD2")
	SD2->(dbSetOrder(3)) //D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM
	If SD2->(dbSeek(SD1->D1_FILIAL+SD1->D1_NFORI+SD1->D1_SERIORI+SD1->D1_FORNECE+SD1->D1_LOJA+SD1->D1_COD+SD1->D1_ITEMORI))
		cRet:= Posicione("SF4",1,xFilial("SF4",SD2->D2_FILIAL)+SD2->D2_TES,"F4_TESDV")
	Endif	
Return cRet


/*/{Protheus.doc} SASP057F
Entrada Matriz
@author Diogo
@since 05/02/2016
@version version
@example
(examples)
@see (links_or_references)
/*/

User Function SASP057F
	If ZZH->ZZH_STFAT = 'F' //Finalizado
		Processa({|lEnd| SASP057G(@lEnd) },'Processando Entrada Matriz',"Aguarde...",.T.)
	Else
		MsgInfo("Rotina dispon�vel somente para Faturado")
	Endif	
Return

/*/{Protheus.doc} SASP057G
Processamento da Entrada para a Matriz
@author Diogo
@since 05/02/2016
@version version
@example
(examples)
@see (links_or_references)
/*/
Static Function SASP057G(lEnd)

	Local aDocFat	:= {}
	Local cMsgErro	:= ""

	IF ZZH->ZZH_ESPEC <> "01"
		If FatuPED(ZZH->ZZH_PC02FL,ZZH->ZZH_PC02,@aDocFat,@cMsgErro,ZZH->ZZH_NF02,ZZH->ZZH_SER02)
			RecLock("ZZH",.F.)
			ZZH->ZZH_PC02NF	:= aDocFat[1]
			ZZH->ZZH_SR02PC	:= aDocFat[2]
			MsUnLock()
			MsgInfo("Processado Entrada para a Matriz - Nota Fiscal "+aDocFat[1]+" / S�rie "+aDocFat[2])

			//fun��o para informar as quantidades que deram entrada 
			FWExecView('Inclusao por FWExecView','DEV010', MODEL_OPERATION_UPDATE, , { || .T. }, , , )

		Else
			MsgInfo("Erro na gera��o da Nota Fiscal "+cMsgErro)
		EndIF
	ELSE //DIRETA

		//fun��o para informar as quantidades que deram entrada 
		FWExecView('Inclusao por FWExecView','DEV010', MODEL_OPERATION_UPDATE, , { || .T. }, , , )
	ENDIF

Return


/*/{Protheus.doc} FatuPed
Gera documento de entrada referente ao Pedido de Compra
@author Diogo
@since 04/09/2015
@version 1.0
/*/
Static Function FatuPED(cEmpFat,cPedFat,aDocFat,cMsgErro,cDocNF,cSerieNF)
	Local aCabec	:= {}
	Local aItens	:= {}
	Local aLinha	:= {}
	Local cFilBkp	:= cFilAnt
	Local lOk		:= .T.
	Local dDataBase := date()
	Private lMsErroAuto := .F.
	cFilAnt	:= cEmpFat

	ProcRegua(100)
	IncProc("Gerando Nota Fiscal de Entrada")

	dbSelectArea("SC7")
	SC7->(dbSetOrder(1))
	SC7->(dbSeek(xFilial("SC7",cEmpFat)+cPedFat))

	aadd(aCabec,{"F1_TIPO"   ,"N"})
	aadd(aCabec,{"F1_FORMUL" ,"N"})
	aadd(aCabec,{"F1_DOC"    ,cDocNF})
	aadd(aCabec,{"F1_SERIE"  ,cSerieNF})
	aadd(aCabec,{"F1_EMISSAO",POSICIONE("SF2", 1, ZZH->ZZH_FILIAL + cDocNF + cSerieNF, "F2_EMISSAO")})
	aadd(aCabec,{"F1_FORNECE",SC7->C7_FORNECE})
	aadd(aCabec,{"F1_LOJA"   ,SC7->C7_LOJA})
	aadd(aCabec,{"F1_ESPECIE","SPED"})
	aadd(aCabec,{"F1_YFILIAL",ALLTRIM(SC7->C7_FILIAL)})
	aadd(aCabec,{"F1_YCONTRA",ALLTRIM(SC7->C7_YCONTRA)})
	aadd(aCabec,{"F1_YMEDICA",ALLTRIM(SC7->C7_YMEDICA)})
	aadd(aCabec,{"F1_NATUREZ",GetMv("SA_NATPAG")})
	NNR->(dbSetOrder(1))

	While SC7->(!eof()) .and. SC7->C7_FILIAL+SC7->C7_NUM == xFilial("SC7",cEmpFat)+cPedFat

		IncProc("Gerando Nota Fiscal de Entrada")

		If !Empty(SC7->C7_YTES)
			RecLock("SC7",.F.)
			SC7->C7_TES	:= SC7->C7_YTES
			MsUnLock()
		EndIf
		aLinha := {}
		Aadd(aLinha,{"D1_COD"     	,SC7->C7_PRODUTO		,Nil})
		Aadd(aLinha,{"D1_ITEM"     	,SC7->C7_ITEM			,Nil})
		Aadd(aLinha,{"D1_QUANT"		,SC7->C7_QUANT			,Nil})
		Aadd(aLinha,{"D1_VUNIT"		,SC7->C7_PRECO			,Nil})
		Aadd(aLinha,{"D1_TOTAL"		,SC7->C7_TOTAL			,Nil})
		Aadd(aLinha,{"D1_PEDIDO"	,SC7->C7_NUM			,Nil})
		Aadd(aLinha,{"D1_TES"		,SC7->C7_TES			,Nil})
		Aadd(aLinha,{"D1_ITEMPC"	,SC7->C7_ITEM			,Nil})
		Aadd(aLinha,{"D1_LOCAL"		,GETMV("SA_ARMNF2")     ,Nil})
		Aadd(aLinha,{"D1_YCLAORC"   ,"E"                    ,Nil})
		Aadd(aLinha,{"D1_YCO"    	,GETMV("SA_YCODEV")     ,Nil})
		Aadd(aLinha,{"D1_CC"       ,"1025"                  ,Nil})
		Aadd(aLinha,{"D1_YDTENTR"	,Ddatabase		        ,Nil})




		If (!NNR->(MsSeek(xFilial("NNR")+GETMV("SA_ENTLOCAL"))))//(!NNR->(MsSeek(xFilial("NNR")+SC7->C7_LOCAL)))
			AGRA045(;
			{{"NNR_CODIGO",GETMV("SA_ENTLOCAL"),NIL};
			,{"NNR_DESCRI","Armazem "+GETMV("SA_ENTLOCAL"),NIL};
			};
			,3,{},{})
		EndIf
		aadd(aItens,aLinha)
		SC7->(DbSkip())
	EndDo

	MSExecAuto({|x,y,z| MATA103(x,y,z) },aCabec,aItens,3)
	If !lMsErroAuto
		lOk:= .T.
		aDocFat	:= {SF1->F1_DOC,SF1->F1_SERIE}
	Else
		lOk:= .F.
		cMsgErro := Mostraerro()
		cMsgErro += "N�o foi poss�vel gerar a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
	EndIf

	cFilAnt:= cFilBkp

Return lOk