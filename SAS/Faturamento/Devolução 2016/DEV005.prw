#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#include "topconn.ch"

/*/{Protheus.doc} BRLC028
tipo devolušao
@author Diogo
@since 21/10/2015
@version version
@example
(examples)
@see (links_or_references)
/*/

User Function DEV005()
	
	Local oBrowse	
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('SX5')
	oBrowse:SetDescription('Tipo de Devolucao')
	oBrowse:DisableDetails()

	oBrowse:SetFilterDefault( "X5_TABELA=='TD'" )

	oBrowse:SetMenuDef( 'DEV005' ) 
	oBrowse:Activate()

Return

Static Function MenuDef()

	Local aRotina := {}	
	
	ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PesqBrw"			OPERATION 1 ACCESS 0 DISABLE MENU
	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.DEV005"	OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.DEV005"	OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.DEV005"	OPERATION 4 ACCESS 143
	ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.DEV005"	OPERATION 5 ACCESS 144
	ADD OPTION aRotina TITLE "Imprimir"   ACTION "VIEWDEF.DEV005"	OPERATION 8 ACCESS 0
	
Return aRotina

Static Function ModelDef()
	
	Local oStructSX5 := Nil
	Local oModel := ""
	
	oStructSX5 := FWFormStruct(1,"SX5")
		
	oModel:= MPFormModel():New("YCADSX5",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
	
	oModel:AddFields("SX5MASTER",/*cOwner*/, oStructSX5 ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)
	
	oModel:SetPrimaryKey({"X5_TABELA"})	
	
Return (oModel)

Static Function ViewDef()

	Local oStructSX5	:= FWFormStruct( 2, 'SX5' )	
	Local oModel		:= FWLoadModel( 'DEV005' )
	
	Local oView
		
	oView	:= FWFormView():New()

	oView:SetModel(oModel)
	oView:EnableControlBar(.T.)

	oView:AddField( "SX5MASTER",oStructSX5)
	
Return oView