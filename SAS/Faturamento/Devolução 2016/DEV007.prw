#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWMBROWSE.CH"
#include "dbtree.ch"
#include "topconn.ch"

User Function DEV007(cFIL,cNUM,cMEDICA,cCLT,cLJ,cFLPV,cPV,cNota,cSer,cNf,cLivro)

	Local _nota := ""
	Local _serie := ""
	Local cCodKit := ""
	Local cCodPai := ""
	Local cQuery := ""
	Local cEmpres := ""
	Local cEndereco := ""
	Local cFilBkp	:= cFilAnt
	cFilAnt	:= cFLPV
	_nota := cNota
	_serie := cSer

	/*

	cQuery := ""
	cQuery += " UPDATE " + RETSQLNAME("SE1")
	cQuery += " SET E1_YCONTRA = '" + cNUM + "', "
	cQuery += "     E1_YMEDICA = '" + cMEDICA + "', "
	cQuery += "     E1_YFILIAL = '" + cFLPV + "', "
	cQuery += " WHERE D_E_L_E_T_ = '' "
	cQuery += "   AND E1_FILIAL = '" + XFILIAL("SE1",cFLPV) + "' "
	cQuery += "   AND E1_PEDIDO = '" + cPV + "' "
	cQuery += "   AND E1_ORIGEM = 'MATA460' "

	TCSQLExec(cQuery)

	DbSelectArea("SD2")
	DbSetOrder(3)
	DbSeek(xFilial("SD2",cFLPV)+ _nota+ _serie)

	WHILE !eof() .and. alltrim(cFLPV) + alltrim(_nota) + alltrim(_serie) == alltrim(SD2->D2_FILIAL)+alltrim(SD2->D2_DOC)+alltrim(SD2->D2_SERIE)

	cCodKit := POSICIONE("SC6",2,XFILIAL("SD2",cFLPV) + SD2->D2_COD + SD2->D2_PEDIDO, "C6_YCODKIT")
	cCodPai := POSICIONE("SC6",2,XFILIAL("SD2",cFLPV) + SD2->D2_COD + SD2->D2_PEDIDO, "C6_YPAIKIT")

	RECLOCK("SD2",.F.)

	SD2->D2_YCODKIT := cCodKit
	SD2->D2_YPAIKIT := cCodPai
	SD2->D2_YFILIAL := SC5->C5_YFILIAL
	SD2->D2_YCONTRA := SC5->C5_YCONTRA
	SD2->D2_YMEDICA := SC5->C5_YMEDICA

	MSUNLOCK()

	SD2->(dbSkip())
	ENDDO

	*/


	cMsgNF:= "" //Mensagem para a Nota Fiscal

	//NF01
	cquery1 := " SELECT * "
	cquery1 += " from " + retsqlname('ZZH') + " ZZH "
	cquery1 += " where  "
	cquery1 += " D_E_L_E_T_ = '' "
	cquery1 += " and ZZH_FILIAL  = '"+ ALLTRIM(cFIL) +"' "
	cquery1 += " and ZZH_NUMERO = '"+ ALLTRIM(cNUM) +"' "
	cquery1 += " and ZZH_MEDICA = '"+ ALLTRIM(cMEDICA) +"' "
	cquery1 += " and ZZH_CLIENT = '"+ ALLTRIM(cCLT) +"' "
	cquery1 += " and ZZH_LOJA   = '"+ ALLTRIM(cLJ) +"' "
	cquery1 += " and ZZH_PC01FL = '"+ ALLTRIM(cFLPV) +"' "
	cquery1 += " and ZZH_PC01NF = '"+ ALLTRIM(cNota) +"' "
	cquery1 += " and ZZH_SER01  = '"+ ALLTRIM(cSer) +"' "

	TCQuery cquery1 new alias T03


	IF ! T03->(EOF())  .and. Alltrim(T03->ZZH_PC01NF) ==  ALLTRIM(cNota) .and. cNf == "NF1"
		CONOUT("NF01")
		dbSelectArea("SA1")
		SA1->(dbSetOrder(1))
		SA1->(dbSeek(xFilial("SA1")+T03->ZZH_FONF01+T03->ZZH_LJNF01))

		cUFCLI:= SUBSTR(SuperGetMv( "MV_ESTADO",,, T03->ZZH_PC01FL ),1,6)//SA1->A1_EST
		cEndereco := "End. Entrega: " + SA1->A1_ENDENT + " "

		//CNPJ(Holding), IE(Holding), (Raz�o social Holding), (N�mero da NF1), (Data da emiss�o)

		dbSelectArea("ZNF")
		ZNF->(dbSetOrder(1))
		If ZNF->(dbSeek(xFilial("ZNF")+cUFCLI))

			If Alltrim(T03->ZZH_PC01NF) == Alltrim(cNota) //Mensagem Nota 01
				IF	cLivro == "N"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF1
				ELSEIF cLivro == "S"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF1
				ENDIF
			Elseif Alltrim(T03->ZZH_NF02) == Alltrim(cNota) //Mensagem Nota 02
				IF	cLivro == "N"
					cMsgNF:= ZNF->ZNF_MSGNF2
				ELSEIF cLivro == "S"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF2
				ENDIF
			Elseif Alltrim(T03->ZZH_NF03) == Alltrim(cNota) //Mensagem Nota 03
				IF	cLivro == "N"
					cMsgNF:= ZNF->ZNF_MSGNF3
				ELSEIF cLivro == "S"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF3
				ENDIF
			Endif
		Endif

		//nPosEmp:= at("#EMPRESA", cMsgNF) //#EMPRESA010101#

		//If nPosEmp > 0

		cEmpres := T03->ZZH_NF03FL//substr(cMsgNF,nPosEmp+8,6)
		cCgcSA1	:= FWArrFilAtu(cEmpAnt, cEmpres)[18]
		SA1->(dbSetOrder(3))
		If SA1->(dbSeek(xFilial("SA1")+cCgcSA1))
			cMsgNF:= strtran(cMsgNF,"#CNPJ#",Alltrim(SA1->A1_CGC))
			cMsgNF:= strtran(cMsgNF,"#RAZAO#",Alltrim(SA1->A1_NOME))
			cMsgNF:= strtran(cMsgNF,"#IE#",Alltrim(SA1->A1_INSCR))
		Endif

		/*Else
		cMsgNF:= strtran(cMsgNF,"#CNPJ#","")
		cMsgNF:= strtran(cMsgNF,"#RAZAO#","")
		cMsgNF:= strtran(cMsgNF,"#IE#","")

		Endif

		cMsgNF:= strtran(cMsgNF,"#EMPRESA"+cEmpres+"#","")*/

		cMsgNF:= strtran(cMsgNF,"#NF1#",Alltrim(cNota))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO1#",DTOC(POSICIONE("SF1",1,T03->ZZH_PC01FL + T03->ZZH_PC01NF + T03->ZZH_SER01 + T03->ZZH_FONF01 + T03->ZZH_LJNF01  ,"F1_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF2#",Alltrim(T03->ZZH_NF02))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO2#",DTOC(POSICIONE("SF2",1,T03->ZZH_NF02FL + T03->ZZH_NF02 + T03->ZZH_SER02 + T03->ZZH_CLNF02 + T03->ZZH_LJNF02 ,"F2_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF3#",Alltrim(T03->ZZH_NF03))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO3#",DTOC(POSICIONE("SF1",1,T03->ZZH_NF03FL + T03->ZZH_NF03 + T03->ZZH_SER03 + T03->ZZH_FONF03 + T03->ZZH_LJNF03 ,"F1_EMISSAO")))

		DBSELECTAREA("SF1")
		DBSETORDER(1)
		DBSEEK(cFLPV + _nota + _serie)

		If Found()

			RECLOCK("SF1",.F.)

			SF1->F1_YFILIAL := T03->ZZH_PC01FL
			SF1->F1_YCONTRA := T03->ZZH_NUMERO
			SF1->F1_YMEDICA := T03->ZZH_MEDICA
			SF1->F1_YMSGNF 	:= cEndereco +  cMsgNF

			MSUNLOCK()

		EndIf
	ENDIF

	T03->(DBCLOSEAREA())

	//NF02
	cquery1 := " SELECT * "
	cquery1 += " from " + retsqlname('ZZH') + " ZZH "
	cquery1 += " where  "
	cquery1 += " D_E_L_E_T_ = '' "
	cquery1 += " and ZZH_FILIAL  = '"+ ALLTRIM(cFIL) +"' "
	cquery1 += " and ZZH_NUMERO = '"+ ALLTRIM(cNUM) +"' "
	cquery1 += " and ZZH_MEDICA = '"+ ALLTRIM(cMEDICA) +"' "
	cquery1 += " and ZZH_CLIENT = '"+ ALLTRIM(cCLT) +"' "
	cquery1 += " and ZZH_LOJA   = '"+ ALLTRIM(cLJ) +"' "
	cquery1 += " and ZZH_NF02FL = '"+ ALLTRIM(cFLPV) +"' "
	cquery1 += " and ZZH_NF02   = '"+ ALLTRIM(cNota) +"' "
	cquery1 += " and ZZH_SER02  = '"+ ALLTRIM(cSer) +"' "

	TCQuery cquery1 new alias T03


	IF ! T03->(EOF()) .AND. Alltrim(T03->ZZH_NF02) ==  ALLTRIM(cNota)  .and. cNf == "NF2"  .AND. T03->ZZH_ESPEC == "02"
		CONOUT("NF02")
		dbSelectArea("SA1")
		SA1->(dbSetOrder(1))
		SA1->(dbSeek(xFilial("SA1")+T03->ZZH_CLNF02+T03->ZZH_LJNF02))

		cUFCLI:= SUBSTR(SuperGetMv( "MV_ESTADO",,, T03->ZZH_NF02FL ),1,6)//SA1->A1_EST

		//CNPJ(Holding), IE(Holding), (Raz�o social Holding), (N�mero da NF1), (Data da emiss�o)

		dbSelectArea("ZNF")
		ZNF->(dbSetOrder(1))
		If ZNF->(dbSeek(xFilial("ZNF")+cUFCLI))

			If Alltrim(T03->ZZH_PC01NF) == Alltrim(cNota) //Mensagem Nota 01
				IF	cLivro == "N"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF1
				ELSEIF cLivro == "S"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF1
				ENDIF
			Elseif Alltrim(T03->ZZH_NF02) == Alltrim(cNota) //Mensagem Nota 02
				IF	cLivro == "N"
					cMsgNF:= ZNF->ZNF_MSGNF2
				ELSEIF cLivro == "S"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF2
				ENDIF
			Elseif Alltrim(T03->ZZH_NF03) == Alltrim(cNota) //Mensagem Nota 03
				IF	cLivro == "N"
					cMsgNF:= ZNF->ZNF_MSGNF3
				ELSEIF cLivro == "S"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF3
				ENDIF
			Endif
		Endif


		//nPosEmp:= at("#EMPRESA", cMsgNF) //#EMPRESA010101#

		//If nPosEmp > 0

		cEmpres:= T03->ZZH_NF03FL//substr(cMsgNF,nPosEmp+8,6)
		cCgcSA1	:= FWArrFilAtu(cEmpAnt, cEmpres)[18]
		SA1->(dbSetOrder(3))
		If SA1->(dbSeek(xFilial("SA1")+cCgcSA1))
			cMsgNF:= strtran(cMsgNF,"#CNPJ#",Alltrim(SA1->A1_CGC))
			cMsgNF:= strtran(cMsgNF,"#RAZAO#",Alltrim(SA1->A1_NOME))
			cMsgNF:= strtran(cMsgNF,"#IE#",Alltrim(SA1->A1_INSCR))
		Endif
		/*
		Else
		cMsgNF:= strtran(cMsgNF,"#CNPJ#","")
		cMsgNF:= strtran(cMsgNF,"#RAZAO#","")
		cMsgNF:= strtran(cMsgNF,"#IE#","")

		Endif

		cMsgNF:= strtran(cMsgNF,"#EMPRESA"+cEmpres+"#","")*/

		cMsgNF:= strtran(cMsgNF,"#NF1#",Alltrim(T03->ZZH_PC01NF))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO1#",DTOC(POSICIONE("SF1",1,T03->ZZH_PC01FL + T03->ZZH_PC01NF + T03->ZZH_SER01 + T03->ZZH_FONF01 + T03->ZZH_LJNF01  ,"F1_EMISSAO")))
		
		cMsgNF:= strtran(cMsgNF,"#NF2#",Alltrim(T03->ZZH_NF02))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO2#",DTOC(POSICIONE("SF2",1,T03->ZZH_NF02FL + T03->ZZH_NF02 + T03->ZZH_SER02 + T03->ZZH_CLNF02 + T03->ZZH_LJNF02 ,"F2_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF3#",Alltrim(T03->ZZH_NF03))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO3#",DTOC(POSICIONE("SF1",1,T03->ZZH_NF03FL + T03->ZZH_NF03 + T03->ZZH_SER03 + T03->ZZH_FONF03 + T03->ZZH_LJNF03 ,"F1_EMISSAO")))



		DBSELECTAREA("SF2")
		DBSETORDER(1)
		DBSEEK(cFLPV + _nota + _serie)

		If Found()

			RECLOCK("SF2",.F.)

			SF2->F2_YFILIAL := T03->ZZH_NF02FL
			SF2->F2_YCONTRA := T03->ZZH_NUMERO
			SF2->F2_YMEDICA := T03->ZZH_MEDICA
			SF2->F2_YMSGNF 	:= cMsgNF

			MSUNLOCK()

		EndIf
	ENDIF

	T03->(DBCLOSEAREA())

	//NF03
	cquery1 := " SELECT * "
	cquery1 += " from " + retsqlname('ZZH') + " ZZH "
	cquery1 += " where  "
	cquery1 += " D_E_L_E_T_ = '' "
	cquery1 += " and ZZH_FILIAL  = '"+ ALLTRIM(cFIL) +"' "
	cquery1 += " and ZZH_NUMERO = '"+ ALLTRIM(cNUM) +"' "
	cquery1 += " and ZZH_MEDICA = '"+ ALLTRIM(cMEDICA) +"' "
	cquery1 += " and ZZH_CLIENT = '"+ ALLTRIM(cCLT) +"' "
	cquery1 += " and ZZH_LOJA   = '"+ ALLTRIM(cLJ) +"' "
	cquery1 += " and ZZH_NF03FL = '"+ ALLTRIM(cFLPV) +"' "
	cquery1 += " and ZZH_NF03 = '"+ ALLTRIM(cNota) +"' "
	cquery1 += " and ZZH_SER03  = '"+ ALLTRIM(cSer) +"' "

	TCQuery cquery1 new alias T05


	IF ! T05->(EOF()) .AND.  alltrim(T05->ZZH_NF03) ==  ALLTRIM(cNota) .and. cNf == "NF3" .AND. T05->ZZH_ESPEC == "02"
		CONOUT("NF03")
		dbSelectArea("SA1")
		SA1->(dbSetOrder(1))
		SA1->(dbSeek(xFilial("SA1")+T05->ZZH_FONF03+T05->ZZH_LJNF03))
		
		cUFCLI:= SUBSTR(SuperGetMv( "MV_ESTADO",,, T05->ZZH_NF03FL ),1,6)//SA1->A1_EST
		cEndereco := "End. Entrega: "  +  SA1->A1_ENDENT + " "
		//CNPJ(Holding), IE(Holding), (Raz�o social Holding), (N�mero da NF1), (Data da emiss�o)

		dbSelectArea("ZNF")
		ZNF->(dbSetOrder(1))
		If ZNF->(dbSeek(xFilial("ZNF")+cUFCLI))

			If Alltrim(T05->ZZH_PC01NF) == Alltrim(cNota) //Mensagem Nota 01
				IF	cLivro == "N"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF1
				ELSEIF cLivro == "S"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF1
				ENDIF
			Elseif Alltrim(T05->ZZH_NF02) == Alltrim(cNota) //Mensagem Nota 02
				IF	cLivro == "N"
					cMsgNF:= ZNF->ZNF_MSGNF2
				ELSEIF cLivro == "S"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF2
				ENDIF
			Elseif Alltrim(T05->ZZH_NF03) == Alltrim(cNota) //Mensagem Nota 03
				IF	cLivro == "N"
					cMsgNF:= ZNF->ZNF_MSGNF3
				ELSEIF cLivro == "S"
					cMsgNF:= ZNF->ZNF_MSGPAD+ZNF->ZNF_MSGNF3
				ENDIF
			Endif
		Endif

		//nPosEmp:= at("#EMPRESA", cMsgNF) //#EMPRESA010101#

		//If nPosEmp > 0
		cEmpres:= T05->ZZH_PC01FL//substr(cMsgNF,nPosEmp+8,6)
		cCgcSA1	:= FWArrFilAtu(cEmpAnt, cEmpres)[18]
		SA1->(dbSetOrder(3))
		If SA1->(dbSeek(xFilial("SA1")+cCgcSA1))
			cMsgNF:= strtran(cMsgNF,"#CNPJ#",Alltrim(SA1->A1_CGC))
			cMsgNF:= strtran(cMsgNF,"#RAZAO#",Alltrim(SA1->A1_NOME))
			cMsgNF:= strtran(cMsgNF,"#IE#",Alltrim(SA1->A1_INSCR))
		Endif
		/*
		Else
		cMsgNF:= strtran(cMsgNF,"#CNPJ#","")
		cMsgNF:= strtran(cMsgNF,"#RAZAO#","")
		cMsgNF:= strtran(cMsgNF,"#IE#","")

		Endif

		cMsgNF:= strtran(cMsgNF,"#EMPRESA"+cEmpres+"#","")*/

		cMsgNF:= strtran(cMsgNF,"#NF1#",Alltrim(T05->ZZH_PC01NF))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO1#",DTOC(POSICIONE("SF1",1,T05->ZZH_PC01FL + T05->ZZH_PC01NF + T05->ZZH_SER01 + T05->ZZH_FONF01 + T05->ZZH_LJNF01  ,"F1_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF2#",Alltrim(T05->ZZH_NF02))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO2#",DTOC(POSICIONE("SF2",1,T05->ZZH_NF02FL + T05->ZZH_NF02 + T05->ZZH_SER02 + T05->ZZH_CLNF02 + T05->ZZH_LJNF02 ,"F2_EMISSAO")))

		cMsgNF:= strtran(cMsgNF,"#NF3#",Alltrim(T05->ZZH_NF03))
		cMsgNF:= strtran(cMsgNF,"#EMISSAO3#",DTOC(POSICIONE("SF1",1,T05->ZZH_NF03FL + T05->ZZH_NF03 + T05->ZZH_SER03 + T05->ZZH_FONF03 + T05->ZZH_LJNF03 ,"F1_EMISSAO")))

		//POSICIONE("SF2",1,T05->ZZ2_PV01FL + T05->ZZ2_PV01NF + T05->ZZ2_SER01+ T05->ZZ2_CLNF01 + T05->ZZ2_LJNF01  ,"F2_EMISSAO")


		DBSELECTAREA("SF1")
		DBSETORDER(1)
		DBSEEK(cFLPV + _nota + _serie)

		If Found()

			RECLOCK("SF1",.F.)

			SF1->F1_YFILIAL := T05->ZZH_NF03FL
			SF1->F1_YCONTRA := T05->ZZH_NUMERO
			SF1->F1_YMEDICA := T05->ZZH_MEDICA
			SF1->F1_YMSGNF 	:= cEndereco + cMsgNF

			MSUNLOCK()

		EndIf
	ENDIF

	T05->(DBCLOSEAREA())

	cFilAnt	:= cFilBkp
REturn