 #include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "Topconn.ch"
#INCLUDE "Rwmake.ch"


User Function PWCli()

	Processa( {|| ProcCli() }, "Aguarde...", "Importando registros...",.F.)	

Return

Static Function ProcCli()

	Local cQryPDV
	Local cQryCli

	//cQryPDV := "SELECT * FROM PWCLI WHERE Integrado=0 and ID IN (SELECT CliID FROM PWPED WHERE CliEscola='102450')"
	cQryPDV := "SELECT * FROM PWCLI WHERE Integrado = 0 AND Celular <> ''  AND Email <> '' " //and ID IN (SELECT CliID FROM PWPED WHERE CliEscola='102450')"

	TCQUERY cQryPDV NEW ALIAS "WCLI"
	
	ProcRegua(RecCount())
	
	While !WCLI->(Eof()) 
	
		cID := WCLI->ID
	
		IncProc()
		dbSelectArea("SA1")
		dbSetOrder(3)
		dbSeek(xFilial("SA1")+WCLI->CPF)	
		
		IF found()
		
			cCodCli := SA1->A1_COD
			cCodLoj := SA1->A1_LOJA
		
			TCSQLEXEC("UPDATE [54.94.250.138].[PDVSAS].[dbo].[Clientes] SET CodCliente='"+cCodCli+"', Loja='"+cCodLoj+"', Integrado=1 WHERE ID="+str(cID))
			
			WCLI->(dbSkip())
			LOOP
			
		Endif
		
		aVetor:={       {"A1_LOJA"		,"01"				,Nil},;
						{"A1_PESSOA"	,"F"				,Nil},;
		            	{"A1_NOME"		,WCLI->NOME			,Nil},;
		            	{"A1_NREDUZ"	,WCLI->NOME			,Nil},;
		            	{"A1_END"		,RTRIM(WCLI->LOGRADOURO) + ',' + cValtoChar(WCLI->NUMERO)	 ,Nil},;
		            	{"A1_TIPO"		,"F"				,Nil},;
		            	{"A1_EST"		,WCLI->ESTADO		,Nil},;
		            	{"A1_COD_MUN"	,WCLI->CODIBGE		,Nil},;
		            	{"A1_MUN"		,WCLI->MUNICIPIO	,Nil},;
		            	{"A1_BAIRRO"	,WCLI->BAIRRO		,Nil},;
		            	{"A1_CEP"		,WCLI->CEP			,Nil},;
		            	{"A1_DDD"       ,WCLI->DDD      	,Nil},; 
		            	{"A1_TEL"		,IIF(EMPTY(WCLI->TELEFONEFIXO),WCLI->CELULAR,WCLI->TELEFONEFIXO)	,Nil},;
		            	{"A1_CGC"		,WCLI->CPF			,Nil},;
		            	{"A1_INSCR"		,"ISENTO"			,Nil},;
		                {"A1_PAIS"      ,"105"      		,Nil},; 
		                {"A1_CODPAIS"   ,"01058"    		,Nil},; 
		                {"A1_EMAIL"		,WCLI->EMAIL		,Nil},;
		                {"A1_YPORTAL"	,"6"				,Nil},;
		                {"A1_RISCO"		,"A"				,Nil},;
		                {"A1_YCODTCF"	,"500"				,Nil}}
		            
		
			lMsErroAuto := .F. 
		
			MSExecAuto({|x,y| Mata030(x,y)},aVetor,3)
			 
			If lMsErroAuto 
					AutoGrLog("Erro ao incluir registro!")
					AutoGrLog("CPF n�o integrado. " + WCLI->NOME)
					AutoGrLog("Opera��o cancelada!")
					MostraErro()
			Else
				TCSQLEXEC("UPDATE [54.94.250.138].[PDVSAS].[dbo].[Clientes] SET CodCliente='"+SA1->A1_COD+"', Loja='"+SA1->A1_LOJA+"', Integrado=1 WHERE ID="+str(cID))
			EndIf 
			
			dbSelectArea("WCLI")		
			WCLI->(dbSkip())
	
	End Do

	WCLI->(dbCloseArea())

Return