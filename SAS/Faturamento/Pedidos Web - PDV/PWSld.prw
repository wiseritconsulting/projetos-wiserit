#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH"
#include 'parmtype.ch'
#INCLUDE "Topconn.ch"
#INCLUDE "Rwmake.ch"
 
 
User Function PWSld(cProdut, cLocal,cFil)

	Local lMsErroAuto  := .F.
	Local usrArea := GetArea()
	
			cquery1 := " SELECT *  "
			cquery1 += " from " + retsqlname('SB9') + " SB9 "
			cquery1 += " where B9_FILIAL = '" + cFil + "' "
			cquery1 += " and D_E_L_E_T_ = '' "
			cquery1 += " and B9_COD = '"+ cProdut +"' "
			cquery1 += " and B9_LOCAL = '" + cLocal + "' "
			cquery1 += " and B9_FILIAL <> '100101'"
			TCQuery cquery1 new alias T07

			IF  T07->(eof())    // Avalia o retorno da pesquisa realizada

				//Begin Transaction
					PARAMIXB1 := {}
					aadd(PARAMIXB1,{"B9_FILIAL",alltrim(cFil),})
					aadd(PARAMIXB1,{"B9_COD",alltrim(cProdut),})
					aadd(PARAMIXB1,{"B9_LOCAL", cLocal,})
					aadd(PARAMIXB1,{"B9_QINI",0,})
					MSExecAuto({|x,y| mata220(x,y)},PARAMIXB1,3)
					If lMsErroAuto
						mostraerro()
					EndIf
				//End Transaction
			ENDIF
			T07->(DBCLOSEAREA())

			RestArea(usrArea)
RETURN