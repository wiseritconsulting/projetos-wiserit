#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "Topconn.ch"
#INCLUDE "Rwmake.ch"


User Function PWPed()

	Processa( {|| U_PedidoMF() }, "Aguarde...", "Importando registros...",.F.)	

Return

// QUANDO MANIFESTO = 1 (GERA 4 MEDI��ES E 4 PEDIDOS)
USER Function PedidoMF(nId)

	private _teste := .f.
	private nValor 	:= 0
	private nSuplem 	:= 0
	private nProd 	:= 0
	private _DF		:= .F. 
	private _pedido
	private _INPROD := ""
	private _local := ""
	private _TOTdesc := 0
	private _dkittot := 0
	private nValUni := 0
	private nValDesc := 0

	cErro := "Integrado com sucesso!"
	aCabec2 := {}
	aLinha2 := {} 
	aItens2 := {} 
	
	
	// ------------------------------------------------
	// LISTA TODOS OS PEDIDOS AINDA N�O INTEGRADOS
	// ------------------------------------------------
	if nId != nil
		cQryPDV := "SELECT * FROM PWPED WHERE situacao_id=2 AND CodCliente <> '' AND CodProtheus <> '' ORDER BY Id"
	else
		cQryPDV := "SELECT * FROM PWPED WHERE situacao_id=2 AND CodCliente <> '' AND CodProtheus <> '' ORDER BY Id "
	endif
		
	if Select("WPED") > 0
		WPED->(dbCloseArea())
	Endif
	
	TCQUERY cQryPDV NEW ALIAS "WPED"
	
		IF WPED->manifesto != "1"
			_DF:= .T.		
		endif
		// Pedido
		// TODO While Pedido
		if WPED->(Eof())
			cErro := "N�o foram encontrados dados!"
		endif
		
		While !WPED->(Eof())
	
				begin transaction
		
			_pedido := WPED->id
			nValor   := WPED->ValorTotal
			nValUni  := WPED->ValorUnit
			_cProduto := WPED->CodProtheus
			cFil     := WPED->Filial
			cContrat := WPED->Contrato
			cLoja    := WPED->Loja
			cCodCli  := WPED->CodCliente
			cCodEsc  := WPED->CliEscola
			cLojaEsc := WPED->LjEscola
			nQuant   := WPED->Quantidade
			bManif   := WPED->Manifesto
			_local   := WPED->Armazem
			nValDesc := WPED->ValorDescontoEquivalent
		
			if Select("IKIT") > 0
				IKIT->(dbCloseArea())
			Endif
			
			if Select("DEST") > 0
				DEST->(dbCloseArea())
			Endif

			// ------------------------------------------------
			// SELECIONA OS ITENS DO KIT PARA GERAR O PEDIDO
			// ------------------------------------------------
			
			_DESC:= "SELECT COUNT(*) AS QUANT FROM "+RETSQLNAME("ZZ7")+" WHERE ZZ7_CODPAI='"+_cProduto+"' AND D_E_L_E_T_ = '' "
			TCQUERY _DESC NEW ALIAS "DEST"
			_descv := 1
			if !DEST->(EOF()) .and. DEST->QUANT > 0 
				_VALDES:=round(_TOTdesc/DEST->QUANT,2)
			endif
	
			cQryKIT := "SELECT ZZ7_CODIGO, ZZ7_SERIE, ZZ7_ENVIO,ZZ7_DESCR FROM "+RETSQLNAME("ZZ7")+" WHERE ZZ7_CODPAI='"+_cProduto+"' AND D_E_L_E_T_ = '' ORDER BY ZZ7_ENVIO"
			TCQUERY cQryKIT NEW ALIAS "IKIT"
		
			// Sub Item do Pedido
			// TODO While Sub Item Pedido Web
			aCabec := {}
			aLinha := {} 
			aItens := {}
			ProcRegua(RecCount())
			
					
			While !IKIT->(Eof())
				IncProc()	
				
				//if (IKIT->ZZ7_ENVIO == '03')	
					
				cKit = IKIT->ZZ7_CODIGO
				cSerie = IKIT->ZZ7_SERIE
				cEnvio = IKIT->ZZ7_ENVIO
				cDescr = IKIT->ZZ7_DESCR

				cMedicao := NumMedic(cFil,cContrat)
				
				RECLOCK("ZZ2",.T.)
					ZZ2->ZZ2_FILIAL = cFil
					ZZ2->ZZ2_TIPCTR = "1"
					ZZ2->ZZ2_NUMERO = cContrat
					ZZ2->ZZ2_CLIENT = cCodEsc
					ZZ2->ZZ2_LOJA   = cLojaEsc
					ZZ2->ZZ2_MEDICA = cMedicao
					ZZ2->ZZ2_DTGER  = ddatabase
					ZZ2->ZZ2_HRGER  = "00:00"
					ZZ2->ZZ2_STATUS = "L"
					ZZ2->ZZ2_LOCEX  = "001"
					ZZ2->ZZ2_TIPO   = "000005"
					ZZ2->ZZ2_DTSOLI = ddatabase
					ZZ2->ZZ2_DTPREV = ddatabase
					ZZ2->ZZ2_DTMAX  = ddatabase+30
					ZZ2->ZZ2_PV01FL = cFil
					ZZ2->ZZ2_CLNF01 = cCodCli
					ZZ2->ZZ2_LJNF01 = cLoja
					ZZ2->ZZ2_ETAPA = "40"
					ZZ2->ZZ2_FATEXP = "1"
					ZZ2->ZZ2_DTENTR = DDATABASE
					ZZ2->ZZ2_IDPDV = STRZERO(_pedido,6)
				MSUNLOCK()
				
				nMedica := RECNO()
				nVlrPrd := nValor * (QValor(cEnvio,cSerie)/100)
				nVlrPrd := Round(nVlrPrd,GetSx3Cache("C6_VALOR","X3_DECIMAL"))
				aSplKit := qSuplem(cKit)
				aVlakit:= qSuplem(cKit)
				nVlrFinal := nVlrPrd / (aVlakit[2]) // Valor do Kit principal 
				_deskit:= (aSplKit[1]*10) // Desconto do suplemento
				_valKit := _deskit * nQuant
				aDesc := Desc(_cProduto)
				_TOTdesc := _valKit + (VAL(nValDesc) / (aDesc[1])) // Valor do desconto no pedido de venda 
				
				RECLOCK("ZZ3",.T.)
					ZZ3->ZZ3_FILIAL = cFil
					ZZ3->ZZ3_NUMERO = cContrat
					ZZ3->ZZ3_CLIENT = cCodEsc
					ZZ3->ZZ3_LOJA   = cLojaEsc
					ZZ3->ZZ3_PRODUT = cKit
					ZZ3->ZZ3_DESCRI = cDescr
					ZZ3->ZZ3_QUANT  = nQuant
					IF bManif == "1"
						ZZ3->ZZ3_TES    = "508"
					ELSE
						ZZ3->ZZ3_TES    = "510"
					ENDIF
					ZZ3->ZZ3_SALDO  = 0
					ZZ3->ZZ3_ENVIO  = cEnvio
					ZZ3->ZZ3_ENVPRV = cEnvio
					ZZ3->ZZ3_MEDICA = cMedicao
					ZZ3->ZZ3_TIPO   = "K"
					ZZ3->ZZ3_VLUNIT = nVlrPrd / nQuant
					ZZ3->ZZ3_VTOTAL = nVlrPrd
					ZZ3->ZZ3_ITEM   = '001'
				MSUNLOCK()
				
				TCSQLEXEC("UPDATE ZZ1010 SET ZZ1_SALDO = ZZ1_SALDO - "+str(nQuant)+" WHERE ZZ1_FILIAL='"+cFil+"' AND ZZ1_NUMERO='"+cContrat+"' AND ZZ1_CLIENT='"+cCodEsc+"' AND ZZ1_LOJA='"+cLojaEsc+"' AND ZZ1_PRODUT='"+cKit+"' AND ZZ1_TES = '508' AND D_E_L_E_T_ = '' ")

				aCabec := {}
			
			// ------------------------------------------------
			// CABEC DO PEDIDO DE VENDA
			// ------------------------------------------------
			
			aadd(aCabec,{"C5_TIPO" 		,"N"		,Nil}) 
			aadd(aCabec,{"C5_CLIENTE"	,cCodCli	,Nil})     
			aadd(aCabec,{"C5_LOJACLI"	,cLoja	    ,Nil}) 
			aadd(aCabec,{"C5_TIPOCLI"	,"F"		,Nil})
			aadd(aCabec,{"C5_TABELA"	,"   "		,Nil})      
			aadd(aCabec,{"C5_CONDPAG"	,"001"		,Nil}) 
			aadd(aCabec,{"C5_TPEDAV"	,"39"		,Nil})
			aadd(aCabec,{"C5_YCONTRA"	, cContrat	,Nil}) 
			aadd(aCabec,{"C5_TPFRETE"	, "S"	,Nil}) 
			_VALDES := _TOTdesc  
			aadd(aCabec,{"C5_DESCONT"   ,_VALDES ,Nil})
					
			_DESCV++
			aCabec2:=ACLONE(aCabec)
							
			// ------------------------------------------------
			// SELECIONA OS ITENS DO KIT PARA GERAR O PEDIDO
			// ------------------------------------------------
			
				if Select("QITM") > 0
					QITM->(dbCloseArea())
				Endif
				cQryITM := "SELECT * FROM FN_PEDIDOS_VENDA('"+cKit+"') WHERE  D_E_L_E_T_=''"
				TCQUERY cQryITM NEW ALIAS "QITM"				

				// Analitico do Kit
				// Gera��o do Pedido
				// TODO While Analitico do Kit
				nX := 0
				While !QITM->(Eof())

						nX++
						
						U_PWSld(QITM->ZZ7_PRODUT,_local,cFil)
										
						aLinha := {}    
						AADD(aLinha,{"C6_ITEM"		,StrZero(nX,2)			,Nil})    
						AADD(aLinha,{"C6_PRODUTO"	,QITM->ZZ7_PRODUT		,Nil})    
						AADD(aLinha,{"C6_QTDVEN"	,nQuant					,Nil})    
						
						if  QITM->ZZ7_SUPLEM  == "S" 
							nNovoVlr := 10
						else
							nNovoVlr := Round(nVlrFinal,GetSx3Cache("C6_VALOR","X3_DECIMAL"))
							nNovoVlr := nNovoVlr / nQuant
						endif
						

						AADD(aLinha,{"C6_PRCVEN"	,nNovoVlr		            ,Nil})    
						AADD(aLinha,{"C6_PRUNIT"	,nNovoVlr		            ,Nil})
						AADD(aLinha,{"C6_VALOR"		,nQuant * (nNovoVlr)		,Nil})
						IF bManif == "1"
							AADD(aLinha,{"C6_TES"		,"508"					    ,Nil})   
							AADD(aLinha,{"C6_YTES"		,"508"					    ,Nil})
						else 
							AADD(aLinha,{"C6_TES"		,"510"					    ,Nil})   
							AADD(aLinha,{"C6_YTES"		,"510"					    ,Nil})
						endif
						AADD(aLinha,{"C6_YCONTRA"	,cContrat				    ,Nil})
						AADD(aLinha,{"C6_YMEDICA"	,cMedicao				    ,Nil})
						AADD(aLinha,{"C6_YITCONT"	,""						    ,Nil})
						AADD(aLinha,{"C6_YCODKIT" 	, cKit			    	    ,Nil})
						AADD(aLinha,{"C6_YPAIKIT" 	, _cProduto				    ,Nil})
						AADD(aLinha,{"C6_LOCAL"		, _local	                ,Nil})
						AADD(aItens,aLinha) 
						AADD(aItens2,aLinha) 
						QITM->(dbSkip())
				End Do
				
						lMsErroAuto := .F. 
						
						bkpfil := cFilAnt
						cFilAnt := cFil
						MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabec,aItens,3)	
						cFilAnt	:= bkpfil
						
						If lMsErroAuto 
								AutoGrLog("Erro ao incluir registro!")
								AutoGrLog("Opera��o cancelada!")
								MostraErro()
								aErro := GetAutoGRLog()  
   								cErro:= "Erro no MSEXECAUTO do Pedido de Venda. Verifique o Log no Server."
		  				For nX := 1 To Len(aErro)
		   					cErro += aErro[nX] + Chr(13)+Chr(10)
		  				Next nX
		  				
						Else
							RECLOCK("ZZ2",.F.)
							ZZ2->ZZ2_PV01 := SC5->C5_NUM
							MSUNLOCK()
							aItens := {}
							aLinha := {}
							aCabec := {}
						EndIf 
						
						
				dbSelectArea("IKIT")
				_INPROD+=" '"+_cProduto+"',"				
				
				//endif
				IKIT->(dbSkip())
				
			End Do
		
			cHtmlPage := Httpget('http://pedidosweb.portalsas.com.br/Pedidos/UpdateStatusPedidos?idPedido='+ALLTRIM(CVALTOCHAR(WPED->id))+'&status=4')
						
			WPED->(dbSkip())
			if _DF .AND. (_PEDIDO != WPED->Id .or. EOF())
				_INPROD := SUBSTR(_INPROD,1,LEN(_INPROD)-1)
				PedidoDF()
				_INPROD := ""
				_dkittot := 0
			endif
			end transaction
	end do
Return cErro


Static Function QValor(envio,serie)

	cQryCnt := "SELECT ZZK_PORCEN FROM ZZK010 WHERE ZZK_SERIE='"+serie+"' AND ZZK_ENVIO='"+envio+"' AND D_E_L_E_T_=''"
	TCQUERY cQryCnt NEW ALIAS "WSPL"
		nPercent := WSPL->ZZK_PORCEN
	WSPL->(dbCloseArea())

Return nPercent

Static Function NumMedic(cFil,cNum)
	
	Local _yArea := GetArea()
	Local cQuery
		
		cQuery := " select MAX(ZZ2_MEDICA) AS ZZ2_MEDICA "
		cQuery += " from " + retsqlname('ZZ2') + " ZZ2 "
		cQuery += " where ZZ2_FILIAL = '" + cFil + "' "
		cQuery += " and ZZ2_NUMERO = '"+ cNum +"' "
		cQuery += " and D_E_L_E_T_ = '' "
	
		TCQuery cQuery new alias T02
		DBSELECTAREA("T02")
		IF Alltrim(T02->ZZ2_MEDICA) == ""
			cMedicao := STRZERO(1,GetSx3Cache("ZZ2_MEDICA","X3_TAMANHO"),0)
		ELSE
			cMedicao := STRZERO(VAL(T02->ZZ2_MEDICA)+1,GetSx3Cache("ZZ2_MEDICA","X3_TAMANHO"),0)
		ENDIF
		T02->(DBCLOSEAREA())
		
	RestArea(_yArea)
	
Return cMedicao


Static Function qSuplem(cKit)

	cquery := " Select COUNT(ZZ7_CODIGO) TOTAL "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where "
	cquery += "  D_E_L_E_T_ <> '*' "
	cquery += " and ZZ7_CODPAI = '"+ cKit +"' "
	cquery += " and ZZ7_TIPO = 'A'		"
	cquery += " and ZZ7_SUPLEM = 'S' " 
	TCQuery cquery new alias T0402
		nSuplem :=  T0402->TOTAL 
	T0402->(DBCLOSEAREA())
	
	cquery := " Select COUNT(ZZ7_CODIGO) TOTAL "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where "
	cquery += "  D_E_L_E_T_ <> '*' "
	cquery += " and ZZ7_CODPAI = '"+ cKit +"' "
	cquery += " and ZZ7_TIPO = 'A'		"
	cquery += " and ZZ7_SUPLEM = 'N' " 
	TCQuery cquery new alias T0402
		nKit :=  T0402->TOTAL 
	T0402->(DBCLOSEAREA())
	
Return {nSuplem,nKit}

// Fun�ao para retornar a quantidade de envios, utilizado para o rateio do desconto.
Static Function Desc(_cProduto)
	cQuery := " SELECT COUNT(*) QTD "
	cQuery += " FROM " + RETSQLNAME('ZZ7') + " ZZ7 "
    cQuery += " WHERE ZZ7_CODPAI = '"+ _cProduto +"' "
    cQuery += " AND D_E_L_E_T_ = '' "
    TcQuery cQuery new Alias T0403
    	nDespdv := T0403->QTD
    T0403->(DBCLOSEAREA())
return {nDespdv}


// faturamento entrega futura....
Static function PedidoDF

			if Select("IKIT") > 0
				IKIT->(dbCloseArea())
			Endif
			
			cQryKIT := " SELECT ZZ7_CODIGO, ZZ7_SERIE, ZZ7_ENVIO,ZZ7_DESCR FROM ZZ7010 WHERE ZZ7_CODPAI IN ("+_INPROD+") AND D_E_L_E_T_ = '' "
			TCQUERY cQryKIT NEW ALIAS "IKIT"
			
		
			// Sub Item do Pedido
			// TODO While Sub Item Pedido Web
			aCabec := {}
			aLinha := {} 
			aItens := {}
			ProcRegua(RecCount())
			
				IncProc()
			
				cMedicao := NumMedic(cFil,cContrat)

				RECLOCK("ZZ2",.T.)
					ZZ2->ZZ2_FILIAL = cFil
					ZZ2->ZZ2_TIPCTR = "1"
					ZZ2->ZZ2_NUMERO = cContrat
					ZZ2->ZZ2_CLIENT = cCodEsc
					ZZ2->ZZ2_LOJA   = cLojaEsc
					ZZ2->ZZ2_MEDICA = cMedicao
					ZZ2->ZZ2_DTGER  = ddatabase
					ZZ2->ZZ2_HRGER  = "00:00"
					ZZ2->ZZ2_STATUS = "L"
					ZZ2->ZZ2_LOCEX  = "001"
					ZZ2->ZZ2_TIPO   = "000005"
					ZZ2->ZZ2_DTSOLI = ddatabase
					ZZ2->ZZ2_DTPREV = ddatabase
					ZZ2->ZZ2_DTMAX  = ddatabase+30
					ZZ2->ZZ2_PV01FL = cFil
					ZZ2->ZZ2_CLNF01 = cCodCli
					ZZ2->ZZ2_LJNF01 = cLoja
					ZZ2->ZZ2_ETAPA = "40"
					ZZ2->ZZ2_FATEXP = "1"
					ZZ2->ZZ2_DTENTR = ddatabase
					ZZ2->ZZ2_IDPDV = STRZERO(_pedido,6)
				MSUNLOCK()
			
			nX := 0
			_Ndesc := 0
			While !IKIT->(Eof())
				cKit = IKIT->ZZ7_CODIGO
				cSerie = IKIT->ZZ7_SERIE
				cEnvio = IKIT->ZZ7_ENVIO
				cDescr = IKIT->ZZ7_DESCR	
				
				nMedica := RECNO()
				nVlrPrd := nValor * (QValor(cEnvio,cSerie)/100)
				nVlrPrd := Round(nVlrPrd,GetSx3Cache("C6_VALOR","X3_DECIMAL"))
				aSplKit := qSuplem(cKit)
				aVlakit:= qSuplem(cKit)
				nVlrFinal := nVlrPrd / (aVlakit[2]) // Valor do Kit principal 	
				_deskit:= (aSplKit[1]*10)
				_valKit := _deskit * nQuant
				aDesc := Desc(_cProduto)
				_TOTdesc := _valKit + (VAL(nValDesc) / (aDesc[1])) // Valor do desconto no pedido de venda 
				
				_Ndesc := _Ndesc + _TOTdesc
				
				
				RECLOCK("ZZ3",.T.)
					ZZ3->ZZ3_FILIAL = cFil
					ZZ3->ZZ3_NUMERO = cContrat
					ZZ3->ZZ3_CLIENT = cCodEsc
					ZZ3->ZZ3_LOJA   = cLojaEsc
					ZZ3->ZZ3_PRODUT = cKit
					ZZ3->ZZ3_DESCRI = cDescr
					ZZ3->ZZ3_QUANT  = nQuant
					ZZ3->ZZ3_TES    = "509"
					ZZ3->ZZ3_SALDO  = 0
					ZZ3->ZZ3_ENVIO  = cEnvio
					ZZ3->ZZ3_ENVPRV = cEnvio
					ZZ3->ZZ3_MEDICA = cMedicao
					ZZ3->ZZ3_TIPO   = "K"
					ZZ3->ZZ3_VLUNIT = nVlrPrd / nQuant
					ZZ3->ZZ3_VTOTAL = nVlrPrd 
					ZZ3->ZZ3_ITEM   = '001'
				MSUNLOCK()
							
			
			// ------------------------------------------------
			// CABEC DO PEDIDO DE VENDA
			// ------------------------------------------------
			aCabec := {}
				
			_VALTOT := _Ndesc
			
			aadd(aCabec,{"C5_TIPO" 		,"N"		,Nil}) 
			aadd(aCabec,{"C5_CLIENTE"	,cCodCli	,Nil})     
			aadd(aCabec,{"C5_LOJACLI"	,cLoja		,Nil}) 
			aadd(aCabec,{"C5_TIPOCLI"	,"F"		,Nil})     
			aadd(aCabec,{"C5_TABELA"	,"   "		,Nil})    
			aadd(aCabec,{"C5_CONDPAG"	,"001"		,Nil}) 
			aadd(aCabec,{"C5_TPEDAV"	,"39"		,Nil})
			aadd(aCabec,{"C5_YCONTRA"	, cContrat	,Nil}) 
			aadd(aCabec,{"C5_TPFRETE"	, "S"	    ,Nil}) 
			aadd(aCabec,{"C5_DESCONT"	,_VALTOT 	,Nil})
			
			aCabec2:=ACLONE(aCabec)
			
			// ------------------------------------------------
			// SELECIONA OS ITENS DO KIT PARA GERAR O PEDIDO
			// ------------------------------------------------
			
				if Select("QITM") > 0
					QITM->(dbCloseArea())
				Endif
				cQryITM := "SELECT * FROM FN_PEDIDOS_VENDA('"+cKit+"') WHERE  D_E_L_E_T_=''"
				TCQUERY cQryITM NEW ALIAS "QITM"				

				// Analitico do Kit
				// Gera��o do Pedido
				// TODO While Analitico do Kit
	
				While !QITM->(Eof())

						nX++
						
						U_PWSld(QITM->ZZ7_PRODUT,_local,cFil)
					
						aLinha := {}    
						AADD(aLinha,{"C6_ITEM"		,StrZero(nX,2)			,Nil})    
						AADD(aLinha,{"C6_PRODUTO"	,QITM->ZZ7_PRODUT		,Nil})    
						AADD(aLinha,{"C6_QTDVEN"	,nQuant					,Nil})    
						
						
						if  QITM->ZZ7_SUPLEM  == "S" 
							nNovoVlr := 10
						else
							nNovoVlr := Round(nVlrFinal,GetSx3Cache("C6_VALOR","X3_DECIMAL"))
							nNovoVlr := nNovoVlr / nQuant
						endif

						AADD(aLinha,{"C6_PRCVEN"	,nNovoVlr	            ,Nil})    
						AADD(aLinha,{"C6_PRUNIT"	,nNovoVlr	            ,Nil})
						AADD(aLinha,{"C6_VALOR"		,nQuant * (nNovoVlr)	,Nil})
						AADD(aLinha,{"C6_TES"		,"509"				    ,Nil})   
						AADD(aLinha,{"C6_YTES"		,"509"				    ,Nil})
						AADD(aLinha,{"C6_YCONTRA"	,cContrat				,Nil})
						AADD(aLinha,{"C6_YMEDICA"	,cMedicao				,Nil})
						AADD(aLinha,{"C6_YITCONT"	,""						,Nil})
						AADD(aLinha,{"C6_YCODKIT" 	, cKit			    	,Nil})
						AADD(aLinha,{"C6_YPAIKIT" 	, _cProduto				,Nil})
						AADD(aLinha,{"C6_LOCAL"		, _local            	,Nil})
						AADD(aItens,aLinha)
						AADD(aItens2,aLinha)  
						QITM->(dbSkip())
				End Do
										
				dbSelectArea("IKIT")
				IKIT->(dbSkip())
				
			End Do
			
						lMsErroAuto := .F. 
						
						bkpfil := cFilAnt
						cFilAnt := cFil
						MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabec,aItens,3)	
						cFilAnt	:= bkpfil
						
						If lMsErroAuto 
								AutoGrLog("Erro ao incluir registro!")
								AutoGrLog("Opera��o cancelada!")
								MostraErro()
								aErro := GetAutoGRLog()  
   								cErro+= " / Erro no MSEXECAUTO do Pedido de Venda do manifesto. Verifique o Log no Server."
		  				For nX := 1 To Len(aErro)
		   					cErro += aErro[nX] + Chr(13)+Chr(10)
		  				Next nX
		  				
						Else
							RECLOCK("ZZ2",.F.)
								ZZ2->ZZ2_PV01 := SC5->C5_NUM
							MSUNLOCK()
							aItens := {}
							aLinha := {}
							aCabec := {}
						EndIf 
							
return
