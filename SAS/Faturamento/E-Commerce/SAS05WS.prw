#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "APWEBSRV.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#include "topconn.ch"
#include "vkey.ch"

//---------------------------------------------------------------//
// AUTOR: JOAO FILHO TOTVS-CE               DATA 23/08/16        //
//---------------------------------------------------------------//
// OBJ  :                                //
//---------------------------------------------------------------//



WsService wms_aditivo	Description "Integra��o Aditivo WS"

	WsData Aditivo								AS wms_integracao_Aditivo
	WsData Retorno								AS wms_aditivo_Retorno

	WsMethod CadastrarAditivo		            Description "Aditivo"


EndWsService


WsStruct wms_aditivo_Retorno
	WsData dData					AS DATE
	WsData cHora				    AS String		
	WsData cOcorrencia			    AS String	
	WsData cObs 				    AS String OPTIONAL
	/*
	01 � ERRO NA INTEGRACAO
	02 � INCLUIDO COM SUCESSO
	*/

EndWsStruct

WsStruct wms_integracao_Aditivo


	WsData Filial_Origem		AS String   
	WsData Num_contrato         AS STRING  
	WsData Cliente_cod			AS STRING
	WsData Loja_cod             AS STRING
	WsData cTesAd               AS STRING 
	WsData itens                AS Array Of wms_integracao_ItemAditivo

EndWsStruct

WsStruct wms_integracao_ItemAditivo

	WsData item_ItemSec		      AS STRING 
	WsData item_Produto			  AS STRING 
	WsData item_Quantida          AS integer	
	WsData item_Tipo              AS STRING      // P = produto K = kit 	
	WsData item_ValUnit			  AS FLOAT
	WsData item_Opera			  AS STRING      // A - acrescimo / D - decrescimo / F - FINANCEIRO


EndWsStruct



WsMethod CadastrarAditivo	wsReceive Aditivo  wsSend Retorno	wsService wms_aditivo

	Local cNumContra := ""
	Local aProd := {}
	Local nSec  := 0 
	Local nTotDesc := 0
	Local nAdianta := 0
	Local aArea := getarea()
	Local lRet
	Local cTipoItem := ""
	Local aItenRep  := {}
	Private cTes  := ""
	Public cFlag 
	Public aKitws := {}
	Public oModel 
	Public oViewWS

	//definir usuario admim
	__cUserID := '000089'
	//definir a TES
	//cTes := ALLTRIM(Aditivo:cTesAd)
	IF ALLTRIM(Aditivo:cTesAd) == "M"  //bloc k p
		cTes := GetMV("SA_TESMANI")
	ELSEIF ALLTRIM(Aditivo:cTesAd) == "EF"  
		cTes := GetMV("SA_TESDTF")
	ELSEIF ALLTRIM(Aditivo:cTesAd) == "NC" //bloc k p
		cTes := GetMV("SA_TESNC")	
	ENDIF
	
	//chamda da fun��o para validar campo a campo
	IF ALLTRIM(Aditivo:Filial_Origem) == ""
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Filial nao informada"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	IF !ExistCpo( "SA1", ALLTRIM(Aditivo:Cliente_cod) + ALLTRIM(Aditivo:Loja_cod ) ,1 )
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Cliente nao cadastrado, verificar o campo Cliente_cod e Loja_cod!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	IF ALLTRIM(POSICIONE("ZZ0",1,ALLTRIM(Aditivo:Filial_Origem) + ALLTRIM(Aditivo:Num_contrato) + ALLTRIM(Aditivo:Cliente_cod) + ALLTRIM(Aditivo:Loja_cod ),"ZZ0_NUMERO")) == ""
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Contrato nao encontrado!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	IF ALLTRIM(POSICIONE("ZZ0",1,ALLTRIM(Aditivo:Filial_Origem) + ALLTRIM(Aditivo:Num_contrato) + ALLTRIM(Aditivo:Cliente_cod) + ALLTRIM(Aditivo:Loja_cod ),"ZZ0_STATUS")) <> "L"
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Contrato nao aprovado!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	if len(Aditivo:itens) == 0
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Itens nao iformados! "
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif	

	if alltrim(Aditivo:cTesAd) <> "M" .and. alltrim(Aditivo:cTesAd) <> "EF" .and. alltrim(Aditivo:cTesAd) <> "NC"
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Tipo de TES nao informada! Verificar campo cTesAd "
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	FOR nAux := 1 To len(Aditivo:itens)

		if 	 nAux == 1
			aadd(aItenRep,{Alltrim(Aditivo:itens[nAux]:item_Produto),Alltrim(cTes)}) 
		else 
			FOR nItem := 1 TO LEN(aItenRep)
				IF	aItenRep[nItem][1] == Alltrim(Aditivo:itens[nAux]:item_Produto)  .and. aItenRep[nItem][2] == Alltrim(cTes)
					::Retorno:cOcorrencia := "01" 
					::Retorno:cObs := "N�o � permitido itens com mesmo PRODUTO e mesma TES nos itens  "
					::Retorno:dData := DATE()
					::Retorno:cHora := TIME()
					RESTAREA( aArea )
					Return .T.
				ENDIF
			NEXT
			aadd(aItenRep,{Alltrim(Aditivo:itens[nAux]:item_Produto),Alltrim(cTes)}) 
		endif

		if Alltrim(Aditivo:itens[nAux]:item_Opera) <> "A" .AND. Alltrim(Aditivo:itens[nAux]:item_Opera) <> "D" .AND. Alltrim(Aditivo:itens[nAux]:item_Opera) <> "F" 
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Informar a peracao do aditivo (A - Acrescimo / D - Decrescimo / F - Financeiro) no 	item "+Alltrim(Aditivo:itens[nAux]:item_ItemSec)+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif

		if Alltrim(Aditivo:itens[nAux]:item_Opera) == "F" .and.  Aditivo:itens[nAux]:item_Quantida <> 0 
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Aditivo financeiro nao permite alterar quantidade do produto, informar quantidade igual a 0(zero) no item "+Alltrim(Aditivo:itens[nAux]:item_ItemSec)+" ! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif
		if Alltrim(Aditivo:itens[nAux]:item_Opera) <> "F" .and.  Aditivo:itens[nAux]:item_Quantida == 0 
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Quantidade do item " +Alltrim(Aditivo:itens[nAux]:item_ItemSec)+ " esta zerada! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif

		cQuery := " SELECT  * "
		cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
		cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Aditivo:Filial_Origem) +"' "
		cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Aditivo:Num_contrato) +"' "
		cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Aditivo:Cliente_cod) +"' "
		cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Aditivo:Loja_cod) +"' "
		cQuery += " AND   ZZ1_PRODUT = '"+ ALLTRIM(Aditivo:itens[nAux]:item_Produto) +"' "
		cQuery += " AND   ZZ1_TES    = '"+ ALLTRIM(cTes) +"' "	
		cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
		TcQuery cQuery New Alias TZZ1

		if Alltrim(Aditivo:itens[nAux]:item_Opera) == "D" .AND. (TZZ1->ZZ1_SALDO - Aditivo:itens[nAux]:item_Quantida) < 0

			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs  := "Valor de decrescimo superior ao saldo do contrato no item "+Alltrim(Aditivo:itens[nAux]:item_ItemSec)+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			TZZ1->(DBCLOSEAREA())
			Return .T.
		endif

		TZZ1->(DBCLOSEAREA())

		if Alltrim(Aditivo:itens[nAux]:item_ItemSec) == ""
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Sequencial nao iformados do item "+Alltrim(Aditivo:itens[nAux]:item_ItemSec)+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif

		if Alltrim(Aditivo:itens[nAux]:item_Tipo) <> "P" .and. Alltrim(Aditivo:itens[nAux]:item_Tipo) <> "K"
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Tipo de produto nao informado no item "+Aditivo:itens[nAux]:item_ItemSec+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif

		if Aditivo:itens[nAux]:item_Quantida < 0
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Quantidade invalidado do item "+Aditivo:itens[nAux]:item_ItemSec+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif

		if Alltrim(Aditivo:itens[nAux]:item_Tipo) == "P" .and. ALLTRIM(POSICIONE("SB1",1,XFILIAL("SB1")+Alltrim(Aditivo:itens[nAux]:item_Produto),"B1_COD")) == ""
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Produto invalido do item "+Aditivo:itens[nAux]:item_ItemSec+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )		
			Return .T.
		elseif Alltrim(Aditivo:itens[nAux]:item_Tipo) == "K" .and. ALLTRIM(POSICIONE("ZZ7",1,XFILIAL("ZZ7")+Alltrim(Aditivo:itens[nAux]:item_Produto),"ZZ7_CODIGO")) == ""
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Kit invalido do item "+Aditivo:itens[nAux]:item_ItemSec+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )		
			Return .T.
		endif

	next

	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(alltrim("01"),alltrim(Aditivo:Filial_Origem),,,,GetEnvServer(),{"SX3","SX6","SE1","SIX"}) //EMPRESA D1
	cfilant := Aditivo:Filial_Origem


	aItens := {}

	FOR nAux := 1 to len(Aditivo:itens)

		cQuery := " SELECT  * "
		cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
		cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Aditivo:Filial_Origem) +"' "
		cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Aditivo:Num_contrato) +"' "
		cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Aditivo:Cliente_cod) +"' "
		cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Aditivo:Loja_cod) +"' "
		cQuery += " AND   ZZ1_PRODUT = '"+ ALLTRIM(Aditivo:itens[nAux]:item_Produto) +"' "
		cQuery += " AND   ZZ1_TES = '"+ ALLTRIM(cTes) +"' "	
		cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
		TcQuery cQuery New Alias TZZ1

		IF TZZ1->(EOF()) .AND.	Aditivo:itens[nAux]:item_Tipo == "P"
			aadd(aItens,{Aditivo:itens[nAux]:item_Produto,Aditivo:itens[nAux]:item_Quantida,"","P",Aditivo:itens[nAux]:item_ValUnit,"P",Aditivo:itens[nAux]:item_Opera}) // ADITIVO DE KIT
			TZZ1->(DBCLOSEAREA())
		ELSEIF !TZZ1->(EOF()) .AND.	Aditivo:itens[nAux]:item_Tipo == "P"
			aadd(aItens,{Aditivo:itens[nAux]:item_Produto,Aditivo:itens[nAux]:item_Quantida,TZZ1->ZZ1_ITEM,"Q",Aditivo:itens[nAux]:item_ValUnit,"P",Aditivo:itens[nAux]:item_Opera}) // ADITIVO DE KIT
			TZZ1->(DBCLOSEAREA())	
		ELSEIF TZZ1->(EOF()) .AND.	Aditivo:itens[nAux]:item_Tipo == "K"
			aadd(aItens,{Aditivo:itens[nAux]:item_Produto,Aditivo:itens[nAux]:item_Quantida,"","P",Aditivo:itens[nAux]:item_ValUnit,"P",Aditivo:itens[nAux]:item_Opera}) // ADITIVO DE KIT
			TZZ1->(DBCLOSEAREA())	
		ELSEIF !TZZ1->(EOF()) .AND.	Aditivo:itens[nAux]:item_Tipo == "K"
			aadd(aItens,{TZZ1->ZZ1_PRODUT,Aditivo:itens[nAux]:item_Quantida,TZZ1->ZZ1_ITEM,"Q",Aditivo:itens[nAux]:item_ValUnit,Aditivo:itens[nAux]:item_Tipo,Aditivo:itens[nAux]:item_Opera}) // ADITIVO DE QUANTIDADE
			TZZ1->(DBCLOSEAREA())
		ELSE	
			TZZ1->(DBCLOSEAREA())
			LOOP
		ENDIF


	NEXT

	//VERIFICA SE EXISTE ITENS A SEREM ADITIVADOS
	IF LEN(aItens) > 0 
		//ASORT(aItens,,, { |x, y| x[4] < y[4] } ) 
		Begin Transaction

			cQuery := " SELECT  * "
			cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
			cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Aditivo:Filial_Origem) +"' "
			cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Aditivo:Num_contrato) +"' "
			cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Aditivo:Cliente_cod) +"' "
			cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Aditivo:Loja_cod) +"' "
			cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
			TcQuery cQuery New Alias TZZ1

			//posicionar no contrato 
			dbSelectArea("ZZ0")
			dbSetOrder(1) //ZZ0_FILIAL, ZZ0_NUMERO, ZZ0_CLIENT, ZZ0_LOJA
			dbSeek(TZZ1->ZZ1_FILIAL + TZZ1->ZZ1_NUMERO + TZZ1->ZZ1_CLIENT + TZZ1->ZZ1_LOJA) 

			oModel := FWLoadModel('SASP042') //                   // CARREGANDO O MODEL 
			oModel:SetOperation( MODEL_OPERATION_INSERT )         // DIGO O TIPO DE OPERA��O (INCLUS�O)
			oModel:Activate()                                     // ATIVO O MODEL

			//POSICIONA NO MODEL DO CABE�ALHO PARA SETAR AS VARIAVEIS 
			oModelZZ0 := oModel:GetModel('ZZ0MASTER')  

			oModel:LoadValue("ZZ0MASTER", "ZZ0_FILIAL", ZZ0_FILIAL) 
			oModel:LoadValue("ZZ0MASTER", "ZZ0_NUMCRM", ZZ0_NUMCRM)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_NUMERO", ZZ0_NUMERO)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_CLIENT", ZZ0_CLIENT)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_LOJA",   ZZ0_LOJA)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_NOME",   ZZ0_NOME)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_NATURE", ZZ0_NATURE)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_EMISSA", ZZ0_EMISSA)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_VIGINI", ZZ0_VIGINI)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_VIGFIM", ZZ0_VIGFIM)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_ANOCOM", ZZ0_ANOCOM)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_CODCRM", ZZ0_CODCRM)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_DTASSI", ZZ0_DTASSI)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_FRTCLI", ZZ0_FRTCLI)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_FRTEMP", ZZ0_FRTEMP)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_ADIANT", ZZ0_ADIANT)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_CPFIN",  ZZ0_CPFIN)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_CCUSTO", ZZ0_CCUSTO)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_FATDIR", ZZ0_FATDIR)   	
			oModel:LoadValue("ZZ0MASTER", "ZZ0_CONFNF", ZZ0_CONFNF)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_TIPCTR", ZZ0_TIPCTR)

			TZZ1->(DBCLOSEAREA())

			oModelZZ6 := oModel:GetModel('ZZ6DETAIL') 

			FOR  nAux := 1 to len(aItens)

				IF aItens[nAux][4] == "P"
					cQuery := " SELECT  TOP (1) * "
					cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
					cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Aditivo:Filial_Origem) +"' "
					cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Aditivo:Num_contrato) +"' "
					cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Aditivo:Cliente_cod) +"' "
					cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Aditivo:Loja_cod) +"' "
					cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
					cQuery += " ORDER BY  ZZ1_ITEM DESC " 
					TcQuery cQuery New Alias TZZ1

					oModel:LoadValue("ZZ6DETAIL", "ZZ6_FILIAL", TZZ1->ZZ1_FILIAL)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_NUMERO", TZZ1->ZZ1_NUMERO)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_ITEM",   STRZERO(VAL(TZZ1->ZZ1_ITEM) + nAux, 3, 0))
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_CLIENT", TZZ1->ZZ1_CLIENT)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_LOJA", TZZ1->ZZ1_LOJA)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_TES", cTes)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_PRODUT", alltrim(aItens[nAux][1]))
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_DESCRI", POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(aItens[nAux][1]),"B1_DESC"))
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_QUANT",  aItens[nAux][2]) //aItens[nAux][1]
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRUN",  aItens[nAux][5]) //aItens[nAux][5]
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRTOT", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLANT", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTANT", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLATU", aItens[nAux][2])
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTATU", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_TPPROD","P")
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_STATUS", "A")
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRDSC", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_TIPO", "N")

					// DIPARANDO OS GATILHOS 
					oModel:SetValue("ZZ6DETAIL", "ZZ6_PRODUT", alltrim(aItens[nAux][1]))
					oModel:SetValue("ZZ6DETAIL", "ZZ6_QUANT",  aItens[nAux][2]) //aItens[nAux][1]
					oModel:SetValue("ZZ6DETAIL", "ZZ6_QTATU", aItens[nAux][2])
					oModel:SetValue("ZZ6DETAIL", "ZZ6_VLRUN",  aItens[nAux][5]) //aItens[nAux][5]
					oModel:SetValue("ZZ6DETAIL", "ZZ6_VLRTOT", aItens[nAux][2] * aItens[nAux][5])

					TZZ1->(DBCLOSEAREA())
					if nAux < len(aItens)
						oModelZZ6:AddLine()
					endif
				ELSEIF aItens[nAux][4] == "Q"

					cQuery := " SELECT  * "
					cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
					cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Aditivo:Filial_Origem) +"' "
					cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Aditivo:Num_contrato) +"' "
					cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Aditivo:Cliente_cod) +"' "
					cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Aditivo:Loja_cod) +"' "
					cQuery += " AND   ZZ1_PRODUT = '"+ aItens[nAux][1] +"' "
					cQuery += " AND   ZZ1_TES = '"+ ALLTRIM(cTes) +"' "	
					cQuery += " AND   ZZ1_ITEM = '"+ aItens[nAux][3] +"' "	
					cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
					cQuery += " ORDER BY  ZZ1_ITEM DESC " 
					TcQuery cQuery New Alias TZZ1

					if ALLTRIM(Aditivo:itens[nAux]:item_Opera) == "A"
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_FILIAL", TZZ1->ZZ1_FILIAL)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_NUMERO", TZZ1->ZZ1_NUMERO)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_ITEM",   TZZ1->ZZ1_ITEM)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_CLIENT", TZZ1->ZZ1_CLIENT)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_LOJA",   TZZ1->ZZ1_LOJA)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_TES",    TZZ1->ZZ1_TES)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_PRODUT", alltrim(TZZ1->ZZ1_PRODUT))
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_DESCRI", TZZ1->ZZ1_DESC)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_QUANT",  aItens[nAux][2]) //aItens[nAux][1]
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRUN",  aItens[nAux][5] ) //aItens[nAux][5]
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRTOT", 0)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLANT", TZZ1->ZZ1_SALDO )
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTANT", TZZ1->ZZ1_QUANT)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLATU", TZZ1->ZZ1_SALDO + aItens[nAux][2])
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTATU", TZZ1->ZZ1_QUANT + aItens[nAux][2])
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_TPPROD",TZZ1->ZZ1_TIPO)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_STATUS", "A")
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRDSC", TZZ1->ZZ1_VLRDSC)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_TIPO", "A")

						// DIPARANDO OS GATILHOS 
						oModel:SetValue("ZZ6DETAIL", "ZZ6_QUANT",  aItens[nAux][2]) //aItens[nAux][1]
						oModel:SetValue("ZZ6DETAIL", "ZZ6_VLRTOT", aItens[nAux][2] * aItens[nAux][5])

					elseif ALLTRIM(Aditivo:itens[nAux]:item_Opera) == "D"

						oModel:LoadValue("ZZ6DETAIL", "ZZ6_FILIAL", TZZ1->ZZ1_FILIAL)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_NUMERO", TZZ1->ZZ1_NUMERO)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_ITEM",   TZZ1->ZZ1_ITEM)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_CLIENT", TZZ1->ZZ1_CLIENT)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_LOJA",   TZZ1->ZZ1_LOJA)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_TES",    TZZ1->ZZ1_TES)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_PRODUT", alltrim(TZZ1->ZZ1_PRODUT))
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_DESCRI", TZZ1->ZZ1_DESC)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_QUANT",  aItens[nAux][2]) //aItens[nAux][1]
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRUN",  aItens[nAux][5] ) //aItens[nAux][5]
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRTOT", 0)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLANT", TZZ1->ZZ1_SALDO )
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTANT", TZZ1->ZZ1_QUANT)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLATU", TZZ1->ZZ1_SALDO - aItens[nAux][2])
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTATU", TZZ1->ZZ1_QUANT - aItens[nAux][2])
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_TPPROD",TZZ1->ZZ1_TIPO)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_STATUS", "A")
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRDSC", TZZ1->ZZ1_VLRDSC)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_TIPO", "D")

						// DIPARANDO OS GATILHOS 
						oModel:SetValue("ZZ6DETAIL", "ZZ6_QUANT",  aItens[nAux][2]) //aItens[nAux][1]
						oModel:SetValue("ZZ6DETAIL", "ZZ6_VLRTOT", aItens[nAux][2] * aItens[nAux][5])

					ELSEif ALLTRIM(Aditivo:itens[nAux]:item_Opera) == "F"

						oModel:LoadValue("ZZ6DETAIL", "ZZ6_FILIAL", TZZ1->ZZ1_FILIAL)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_NUMERO", TZZ1->ZZ1_NUMERO)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_ITEM",   TZZ1->ZZ1_ITEM)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_CLIENT", TZZ1->ZZ1_CLIENT)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_LOJA",   TZZ1->ZZ1_LOJA)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_TES",    TZZ1->ZZ1_TES)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_PRODUT", alltrim(TZZ1->ZZ1_PRODUT))
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_DESCRI", TZZ1->ZZ1_DESC)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_QUANT",  TZZ1->ZZ1_QUANT) //aItens[nAux][1]
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRUN",  aItens[nAux][5]) //aItens[nAux][5]
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRTOT", 0)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLANT", TZZ1->ZZ1_SALDO )
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTANT", TZZ1->ZZ1_QUANT)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLATU", TZZ1->ZZ1_SALDO )
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTATU", TZZ1->ZZ1_QUANT )
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_TPPROD",TZZ1->ZZ1_TIPO)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_STATUS", "A")
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRDSC", TZZ1->ZZ1_VLRDSC)
						oModel:LoadValue("ZZ6DETAIL", "ZZ6_TIPO", "A")

						// DIPARANDO OS GATILHOS 
						oModel:SetValue("ZZ6DETAIL", "ZZ6_QUANT",  TZZ1->ZZ1_QUANT) //aItens[nAux][1]
						oModel:SetValue("ZZ6DETAIL", "ZZ6_VLRTOT", TZZ1->ZZ1_QUANT * aItens[nAux][5])	

					endif
					TZZ1->(DBCLOSEAREA())

					if nAux < len(aItens)
						IF !oModel:VldData()
							::Retorno:cOcorrencia := "01" 
							::Retorno:cObs := OMODEL:GETERRORMESSAGE()[6]
							::Retorno:dData := DATE()
							::Retorno:cHora := TIME()
							DisarmTransaction()
							RESTAREA( aArea )
							Return .T.

						ENDIF
						oModelZZ6:AddLine()
					endif

				ENDIF
			NEXT

			If oModel:VldData()
				oViewWS:=FWFormView():New()
				oViewWS:SetModel(oModel)
				//If ValType(oView)=="O"
				//	oView:REFRESH()
				//EndIf
				//oModel:CommitData() //swerro.log
				lRet := StaticCall(SASP042,ZZ6Commit,oModel)	
				IF !lRet
					::Retorno:cOcorrencia := "01" 
					::Retorno:cObs := "Erro no execauto de aditivo"
					::Retorno:dData := DATE()
					::Retorno:cHora := TIME()
					DisarmTransaction()
					RESTAREA( aArea )
					Return .T.
				ELSE
					::Retorno:cOcorrencia := "02" 
					::Retorno:cObs := "Aditivo gerado com sucesso!!"
					::Retorno:dData := DATE()
					::Retorno:cHora := TIME()	
				endif 	
			ELSE
				::Retorno:cOcorrencia := "01" 
				::Retorno:cObs := OMODEL:GETERRORMESSAGE()[6]
				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()
				DisarmTransaction()
				RESTAREA( aArea )
				Return .T.
			ENDIF


		End   Transaction

	ENDIF

return .T.