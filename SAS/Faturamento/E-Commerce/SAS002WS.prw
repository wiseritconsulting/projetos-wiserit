#Include 'Protheus.ch'
#Include "apwebsrv.ch"

WsService CadFornecedor;
		Description "Cadastro de Fornecedores"
		
	WsData filial as String	
	WsData nome as String
	WsData nomeReduz as String	
	WsData endereco as String
	WsData tipo as String
	WsData estado as String
	WsData codmun as String
	WsData munic as String
	WsData pais as String	
	WsData loja as String
	WsData cgc as String
	WsData inscest as String //Inscri��o Estadual
	WsData bairro as String
	WsData cep as String
	WsData telefone as String
	WsData email as String
	WsData cError as String
	WsData aError as ARRAY of String
	WsData Fornecedor as ForSas	
	
	wsMethod cadastroFornecedor;
		Description "Cadastro de Fornecedor utilizando ExecAuto"	
	
EndWsService

WsStruct ForSas
	
	WsData filial as String	
	WsData nome as String
	WsData nomeReduz as String	
	WsData endereco as String
	WsData tipo as String
	WsData estado as String
	WsData codmun as String
	WsData munic as String
	WsData pais as String	
	WsData loja as String
	WsData cgc as String
	WsData inscest as String
	WsData bairro as String
	WsData cep as String
	WsData telefone as String
	WsData email as String
	
EndWsStruct

WsMethod cadastroFornecedor wsReceive filial,nome,loja,nomeReduz,endereco,estado,codmun,munic,pais,cgc,inscest,bairro,cep,telefone,email,tipo wsSend aError wsService CadFornecedor
		
	aDados := {}	
	lMsErroAuto := .F.
	cError := ""
	lIncluir := .T.
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(alltrim("01"),alltrim(XFILIAL("SA2",filial)),,,,GetEnvServer(),{"SX3","SX6","SA2","SIX"}) //EMPRESA D1
	
	Self:Fornecedor       := WSClassNew("ForSas")
	Self:Fornecedor:nome := AllTrim(nome)
	Self:Fornecedor:loja := IIF(empty(loja),"01",alltrim(loja))
	Self:Fornecedor:nomeReduz := AllTrim(nomeReduz)
	Self:Fornecedor:endereco := AllTrim(endereco)
	Self:Fornecedor:tipo := AllTrim(tipo)
	Self:Fornecedor:estado := AllTrim(estado)
	Self:Fornecedor:codmun := AllTrim(codmun)
	Self:Fornecedor:munic := AllTrim(munic)
	Self:Fornecedor:pais := AllTrim(pais)
	Self:Fornecedor:cgc := AllTrim(cgc)
	Self:Fornecedor:inscest := AllTrim(inscest)
	Self:Fornecedor:bairro := AllTrim(bairro)
	Self:Fornecedor:cep := AllTrim(cep)
	Self:Fornecedor:telefone := AllTrim(telefone)
	Self:Fornecedor:email := AllTrim(email)
	
	If EMPTY(POSICIONE("SA2",3,XFILIAL("SA2",filial) + Self:Fornecedor:cgc,"A2_NOME"))	
	
		aDados :={	{"A2_TIPO",Self:Fornecedor:tipo,NIL },;				
				{"A2_NOME", Self:Fornecedor:nome,NIL} ,;
				{"A2_LOJA", Self:Fornecedor:loja,NIL} ,;
				{"A2_NREDUZ", Self:Fornecedor:nomeReduz,NIL} ,;				
		       {"A2_END",Self:Fornecedor:endereco,NIL} ,;
		       {"A2_EST",Self:Fornecedor:estado,NIL} ,;		       
		       {"A2_TEL",Self:Fornecedor:telefone,NIL} ,;		      
		       {"A2_CGC",Self:Fornecedor:cgc,NIL} ,;
		       {"A2_BAIRRO",Self:Fornecedor:bairro,NIL} ,;
		       {"A2_CEP",Self:Fornecedor:cep,NIL} ,;
		       {"A2_PAIS",Self:Fornecedor:pais,NIL} ,;
		       {"A2_COD_MUN",Self:Fornecedor:codmun,NIL},;
		       {"A2_EMAIL",Self:Fornecedor:email,NIL},;
		       {"A2_INSCR",Self:Fornecedor:inscest,NIL},;
		       {"A2_MUN",Self:Fornecedor:munic,NIL}}
		
		lIncluir := .T.
		MSExecAuto({|x,y| Mata020(x,y)},aDados,3)
		
	Else
	
		aDados :={	{"A2_TIPO",Self:Fornecedor:tipo,NIL },;				
				{"A2_NOME", Self:Fornecedor:nome,NIL} ,;
				{"A2_COD",  POSICIONE("SA2",3,XFILIAL("SA2",filial) + Self:Fornecedor:cgc,"A2_COD")	,NIL} ,;	
				{"A2_LOJA", POSICIONE("SA2",3,XFILIAL("SA2",filial) + Self:Fornecedor:cgc,"A2_LOJA")	,NIL} ,;
				{"A2_NREDUZ", Self:Fornecedor:nomeReduz,NIL} ,;				
		       {"A2_END",Self:Fornecedor:endereco,NIL} ,;
		       {"A2_EST",Self:Fornecedor:estado,NIL} ,;		       
		       {"A2_TEL",Self:Fornecedor:telefone,NIL} ,;		       
		       {"A2_CGC",Self:Fornecedor:cgc,NIL} ,;
		       {"A2_BAIRRO",Self:Fornecedor:bairro,NIL} ,;
		       {"A2_CEP",Self:Fornecedor:cep,NIL} ,;
		       {"A2_PAIS",Self:Fornecedor:pais,NIL} ,;
		       {"A2_COD_MUN",Self:Fornecedor:codmun,NIL},;
		       {"A2_EMAIL",Self:Fornecedor:email,NIL},;
		       {"A2_INSCR",Self:Fornecedor:inscest,NIL},;
		       {"A2_MUN",Self:Fornecedor:munic,NIL}}	
		
		lIncluir := .F.
		MSExecAuto({|x,y| Mata020(x,y)},aDados,4)	
	
	EndIf
	
	If lMsErroAuto
	
	 	AADD(Self:aError,"")
	 	AADD(Self:aError,"")
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
	 	
	 Else
	 
	 	AADD(Self:aError,SA2->A2_COD)
	 	AADD(Self:aError,SA2->A2_LOJA)
	 	AADD(Self:aError,"1")
	 	
	 	If lIncluir 
	 		AADD(Self:aError,"Incluido com sucesso.")
	 	Else
	 		AADD(Self:aError,"Alterado com sucesso.")
	 	EndIf	
	
	EndIf
	
	conout(Self:aError[1])	 				
	
return .T.