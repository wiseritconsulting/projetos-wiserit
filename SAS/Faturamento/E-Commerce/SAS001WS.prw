#Include 'Protheus.ch'
#Include "apwebsrv.ch"



WsService CadCliente;
		Description "Cadastro de Clientes"
		
	WsData filial as String	
	WsData nome as String
	WsData nomeReduz as String
	WsData loja as String
	WsData endereco as String
	WsData pessoa as String
	WsData telefone as String
	WsData email as String
	WsData estado as String
	WsData codmun as String
	WsData munic as String
	WsData pais as String
	WsData cgc as String
	WsData bairro as String
	WsData cep as String
	WsData cError as String
	WsData cliente as CliSas
	WsData aError as ARRAY of String
			
	wsMethod cadastroCliente;
		Description "Cadastro de Cliente utilizando ExecAuto"	
	
EndWsService

//Estrutura do cliente SAS
WsStruct CliSas
	
	WsData nome as String
	WsData loja as String
	WsData nomeReduz as String
	WsData endereco as String
	WsData pessoa as String
	WsData telefone as String
	WsData email as String
	WsData estado as String
	WsData codmun as String
	WsData munic as String
	WsData pais as String
	WsData cgc as String
	WsData bairro as String
	WsData cep as String
	
EndWsStruct

//M�todo de cadastro de cliente.

WsMethod cadastroCliente wsReceive filial,nome,loja,nomeReduz,endereco,estado,codmun,munic,pais,cgc,bairro,cep,telefone,email,pessoa wsSend aError wsService CadCliente
		
	aDados := {}	
	lMsErroAuto := .F.
	lIncluir := .T.	
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(alltrim("01"),alltrim(XFILIAL("SA1",filial)),,,,GetEnvServer(),{"SX3","SX6","SA1","SIX"}) //EMPRESA D1
	
	
	Self:cliente       := WSClassNew("CliSas")
	Self:cliente:nome := AllTrim(nome)
	Self:cliente:loja := IIF(empty(loja),"01",alltrim(loja))
	Self:cliente:nomeReduz := AllTrim(nomeReduz)
	Self:cliente:endereco := AllTrim(endereco)
	Self:cliente:pessoa := AllTrim(pessoa)
	Self:cliente:estado := AllTrim(estado)
	Self:cliente:codmun := AllTrim(codmun)
	Self:cliente:munic := AllTrim(munic)
	Self:cliente:pais := AllTrim(pais)
	Self:cliente:cgc := AllTrim(cgc)
	Self:cliente:bairro := AllTrim(bairro)
	Self:cliente:cep := AllTrim(cep)
	Self:cliente:telefone := Alltrim(telefone)
	Self:cliente:email := Alltrim(email)
		
	//Verifica se o cliente j� existe na base, se n�o existir, executa EXECAUTO de inclus�o.
	If EMPTY(POSICIONE("SA1",3,XFILIAL("SA1",filial) + Self:cliente:cgc,"A1_NOME"))	
	
		aDados :={	{"A1_PESSOA",Self:cliente:pessoa,NIL },;	
				{"A1_YCODTCF","500",NIL},;
				{"A1_NOME", Self:cliente:nome,NIL} ,;
				{"A1_LOJA", Self:cliente:loja,NIL} ,;
				{"A1_NREDUZ", Self:cliente:nomeReduz,NIL} ,;				
		       {"A1_END",Self:cliente:endereco,NIL} ,;
		       {"A1_EST",Self:cliente:estado,NIL} ,;		       
		       {"A1_TEL",Self:cliente:telefone,NIL} ,;
		       {"A1_TIPO","F",NIL} ,;
		       {"A1_CGC",Self:cliente:cgc,NIL} ,;
		       {"A1_BAIRRO",Self:cliente:bairro,NIL} ,;
		       {"A1_CEP",Self:cliente:cep,NIL} ,;
		       {"A1_PAIS",Self:cliente:pais,NIL} ,;
		       {"A1_COD_MUN",Self:cliente:codmun,NIL},;
		       {"A1_EMAIL",Self:cliente:email,NIL},;
		       {"A1_MUN",Self:cliente:munic,NIL}}
		      
		       
		MSExecAuto({|x,y| Mata030(x,y)},aDados,3)
		lIncluir := .T.		
			
	Else
	
		aDados :={	{"A1_PESSOA",Self:cliente:pessoa,NIL },;
				{"A1_COD",POSICIONE("SA1",3,XFILIAL("SA1",filial) + Self:cliente:cgc,"A1_COD"),NIL},;	
				{"A1_NOME", Self:cliente:nome,NIL} ,;
				{"A1_NREDUZ", Self:cliente:nomeReduz,NIL} ,;
				{"A1_LOJA", "01",NIL} ,;
		       {"A1_END",Self:cliente:endereco,NIL} ,;
		       {"A1_EST",Self:cliente:estado,NIL} ,;		       
		       {"A1_TIPO","F",NIL} ,;
		       {"A1_CGC",Self:cliente:cgc,NIL} ,;
		       {"A1_BAIRRO",Self:cliente:bairro,NIL} ,;
		       {"A1_CEP",Self:cliente:cep,NIL} ,;
		       {"A1_COD_MUN",Self:cliente:codmun,NIL},;
		       {"A1_MUN",Self:cliente:munic,NIL}}
		
		MSExecAuto({|x,y| Mata030(x,y)},aDados,4)
		lIncluir := .F.	
	EndIf
	
	
	If lMsErroAuto	
	 	
	 	AADD(Self:aError,"")
	 	AADD(Self:aError,"")
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
	 	
	 Else
	 
	 	AADD(Self:aError,SA1->A1_COD)
	 	AADD(Self:aError,SA1->A1_LOJA)
	 	AADD(Self:aError,"1")
	 	
	 	If lIncluir 
	 		AADD(Self:aError,"Incluido com sucesso.")
	 	Else
	 		AADD(Self:aError,"Alterado com sucesso.")
	 	EndIf	
	
	EndIf
	
	conout(Self:aError[1])
	CONOUT(SA1->A1_NOME)	 				
	
return .T.