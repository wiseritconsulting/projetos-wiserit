#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "APWEBSRV.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#include "topconn.ch"
#include "vkey.ch"

//---------------------------------------------------------------//
// AUTOR: JOAO FILHO TOTVS-CE               DATA 13/07/16        //
//---------------------------------------------------------------//
// OBJ  :                                //
//---------------------------------------------------------------//



WsService wms_integracao	Description "Integra��o Medi��o WMS"

	WsData Medicao								AS wms_integracao_Medicao
	WsData Retorno								AS wms_integracao_Retorno

	WsMethod CadastrarMedicao		            Description "Cadastro de Medi��o"


EndWsService


WsStruct wms_integracao_Retorno
	WsData dData					AS DATE
	WsData cHora				    AS String		
	WsData cOcorrencia			    AS String	
	WsData cObs 				    AS String OPTIONAL
	WsData cRet 				    AS String OPTIONAL
	/*
	01 � ERRO NA INTEGRACAO
	02 � INCLUIDO COM SUCESSO
	*/

EndWsStruct

WsStruct wms_integracao_Medicao

	WsData Filial_Origem		AS String   
	WsData Num_contrato         AS STRING  
	WsData Cliente_cod			AS STRING
	WsData Loja_cod             AS STRING
	WsData Pai_cod              AS STRING
	WsData Pai_loj              AS STRING
	//WsData Est_cliente			AS STRING 
	WsData Obs                  AS STRING
	//WsData Natu_zz2             AS STRING // Natureza
	WsData cOperacao            AS STRING // 3 - incluir / 4- Alterar / 5- excluit
	WsData cMedicao             AS STRING // informar somente quando for EXCLUS�O OU ALTERA��O
	WsData cIdPDV               AS STRING 
	WsData cNomeAluno           AS STRING 
	WsData cNFEscola            AS STRING
	WsData cMan_Dtf             AS STRING // M - manifesto / EF - data futura / NC - NOTA CHEIA
	WsData itens                AS Array Of wms_integracao_ItemMedicao
	WsData nDesconto            AS Integer

EndWsStruct

WsStruct wms_integracao_ItemMedicao

	WsData item_ItemSec		      AS STRING 
	WsData item_Produto			  AS STRING 
	WsData item_Quantida          AS integer	
	WsData item_Envio             AS STRING
	WsData item_Tipo              AS STRING      // p = produto k = kit 	
	WsData item_ValUnit			  AS FLOAT       // buscar na tabela de pre�o  	     

EndWsStruct


WsMethod CadastrarMedicao	wsReceive Medicao  wsSend Retorno	wsService wms_integracao


	Local cNumContra := ""
	Local aProd := {}
	Local nSec  := 0 
	Local nTotDesc := 0
	Local nAdianta := 0
	Local aArea := getarea()
	Local lRet
	Local cTipoItem := ""
	Local aItenRep  := {}
	Private cTes  := ""
	Public cFlag 
	Public aKitws := {}
	Public oModel 
	Public cNatuWS
	Public cObWS
	Public cLocWs
	Public cTpMedws
	Public dDtSolWs
	Public Pai_cod   // codigo do cliente - pai do aluno 
	Public Pai_loj
	Public idPdvWS
	Public oViewWS
	Public C5FilWS   //pedido de venda criando , usado para alterar campo customizado
	Public C5PedWS
	Public C5DescWS //Desconto do Ecommerce
	
	C5DescWs := MEDICAO:nDesconto

	//definir usuario admim
	__cUserID := '000089'
	//definir a TES
	IF ALLTRIM(MEDICAO:cMan_Dtf) == "M"  //bloc k p
		cTes := GetMV("SA_TESMANI")
	ELSEIF ALLTRIM(MEDICAO:cMan_Dtf) == "EF"  
		cTes := GetMV("SA_TESDTF")
	ELSEIF ALLTRIM(MEDICAO:cMan_Dtf) == "NC" //bloc k p
		cTes := GetMV("SA_TESNC")	
	ENDIF

	//chamda da fun��o para validar campo a campo
	IF ALLTRIM(MEDICAO:Filial_Origem) == ""
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Filial nao informada"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	IF !ExistCpo( "SA1", ALLTRIM(MEDICAO:Cliente_cod) + ALLTRIM(MEDICAO:Loja_cod ) ,1 )
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Cliente nao cadastrado, verificar o campo Cliente_cod e Loja_cod!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	IF !ExistCpo( "SA1", ALLTRIM(MEDICAO:Pai_cod) + ALLTRIM(MEDICAO:Pai_loj ) ,1 )
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Cliente 'PAI' nao cadastrado, verificar os campos Pai_cod e Pai_loj!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif	

	IF ALLTRIM(POSICIONE("ZZ0",1,ALLTRIM(MEDICAO:Filial_Origem) + ALLTRIM(MEDICAO:Num_contrato) + ALLTRIM(MEDICAO:Cliente_cod) + ALLTRIM(MEDICAO:Loja_cod ),"ZZ0_NUMERO")) == ""
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Contrato nao encontrado!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	IF ALLTRIM(POSICIONE("ZZ0",1,ALLTRIM(MEDICAO:Filial_Origem) + ALLTRIM(MEDICAO:Num_contrato) + ALLTRIM(MEDICAO:Cliente_cod) + ALLTRIM(MEDICAO:Loja_cod ),"ZZ0_STATUS")) <> "L"
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Contrato nao aprovado!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	//ZZ2_FILIAL, ZZ2_NUMERO, ZZ2_MEDICA, ZZ2_CLIENT, ZZ2_LOJA, R_E_C_N_O_, D_E_L_E_T_
	IF ALLTRIM(MEDICAO:cOperacao) <> "3" .AND. ALLTRIM(POSICIONE("ZZ2",1,ALLTRIM(MEDICAO:Filial_Origem) + ALLTRIM(MEDICAO:Num_contrato) + ALLTRIM(MEDICAO:cMedicao) + ALLTRIM(MEDICAO:Cliente_cod) + ALLTRIM(MEDICAO:Loja_cod ),"ZZ2_MEDICA")) == ""
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Medicao na encontrada!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif
	
	IF ALLTRIM(MEDICAO:cOperacao) <> "3" .AND. ALLTRIM(POSICIONE("ZZ2",1,ALLTRIM(MEDICAO:Filial_Origem) + ALLTRIM(MEDICAO:Num_contrato) + ALLTRIM(MEDICAO:cMedicao) + ALLTRIM(MEDICAO:Cliente_cod) + ALLTRIM(MEDICAO:Loja_cod ),"ZZ2_PV01NF")) <> ""
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Medicao ja faturada, nao pode ser alterada ou excluida!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	/*IF ALLTRIM(POSICIONE("SED",1,XFILIAL("SED",ALLTRIM(MEDICAO:Filial_Origem)) + ALLTRIM(MEDICAO:Natu_zz2 ) ,"ED_CODIGO" )) == ""
	::Retorno:cOcorrencia := "01" 
	::Retorno:cObs := "Natureza nao cadastrada, verificar campo Natu_zz2!"
	::Retorno:dData := DATE()
	::Retorno:cHora := TIME()
	RESTAREA( aArea )
	Return .T.
	endif
	*/
	IF  ALLTRIM(MEDICAO:cOperacao) <> "3" .AND. ALLTRIM(MEDICAO:cOperacao) <> "4" .AND. ALLTRIM(MEDICAO:cOperacao) <> "5"  
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Operacao nao definida!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif

	if alltrim(Medicao:cIdPDV) == ""
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "ID do PDV nao informado"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif	

	if alltrim(Medicao:cNomeAluno) == ""
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Nome do aluno nao iformado"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif	

	if alltrim(Medicao:cNFEscola) == ""
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Nota Fiscal de referencia nao informada!"
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif	

	if alltrim(Medicao:cMan_Dtf) <> "M" .and. alltrim(Medicao:cMan_Dtf) <> "EF" .and. alltrim(Medicao:cMan_Dtf) <> "NC"
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Tipo de nota nao informada! Verificar campo cMan_Dtf "
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif	

	if len(Medicao:itens) == 0
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Itens nao iformados! "
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )
		Return .T.
	endif	

	FOR nAux := 1 To len(Medicao:itens)

		if 	 nAux == 1
			cTipoItem := Alltrim(Medicao:itens[nAux]:item_Tipo)
		elseif cTipoItem <> Alltrim(Medicao:itens[nAux]:item_Tipo) .and. (ALLTRIM(MEDICAO:cMan_Dtf) == "M" .or. ALLTRIM(MEDICAO:cMan_Dtf) == "EF")  
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Itens com diferentes tipos, favor enviar somente KITS (K) ou somente PRODUTOS (P) "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif

		if 	 nAux == 1
			aadd(aItenRep,{Alltrim(Medicao:itens[nAux]:item_Produto),Alltrim(cTes)}) 
		else 
			FOR nItem := 1 TO LEN(aItenRep)
				IF	aItenRep[nItem][1] == Alltrim(Medicao:itens[nAux]:item_Produto)  .and. aItenRep[nItem][2] == Alltrim(cTes)
					::Retorno:cOcorrencia := "01" 
					::Retorno:cObs := "Nao e permitido itens com mesmo PRODUTO e mesma TES"
					::Retorno:dData := DATE()
					::Retorno:cHora := TIME()
					RESTAREA( aArea )
					aItenRep := {}
					Return .T.
				ENDIF
			NEXT
			aadd(aItenRep,{Alltrim(Medicao:itens[nAux]:item_Produto),Alltrim(cTes)}) 
		endif

		if Alltrim(Medicao:itens[nAux]:item_ItemSec) == ""
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Sequencial nao iformados do item "+nAux+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif
		if Alltrim(Medicao:itens[nAux]:item_Tipo) <> "P" .and. Alltrim(Medicao:itens[nAux]:item_Tipo) <> "K"
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Tipo de produto nao informado no item "+Medicao:itens[nAux]:item_ItemSec+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif
		if Medicao:itens[nAux]:item_Quantida <= 0 
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Quantidade invalidado do item "+Medicao:itens[nAux]:item_ItemSec+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif
		if Medicao:itens[nAux]:item_ValUnit <= 0 
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Valor unitario invalidado do item "+Medicao:itens[nAux]:item_ItemSec+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif
		if alltrim(Medicao:itens[nAux]:item_Envio) ==""
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Envio nao informado do item "+Medicao:itens[nAux]:item_ItemSec+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )
			Return .T.
		endif

		if Alltrim(Medicao:itens[nAux]:item_Tipo) == "P" .and. ALLTRIM(POSICIONE("SB1",1,XFILIAL("SB1")+Alltrim(Medicao:itens[nAux]:item_Produto),"B1_COD")) == ""
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Produto invalido do item "+Medicao:itens[nAux]:item_ItemSec+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )		
			Return .T.
		elseif Alltrim(Medicao:itens[nAux]:item_Tipo) == "K" .and. ALLTRIM(POSICIONE("ZZ7",1,XFILIAL("ZZ7")+Alltrim(Medicao:itens[nAux]:item_Produto),"ZZ7_CODIGO")) == ""
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Kit invalido do item "+Medicao:itens[nAux]:item_ItemSec+"! "
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )		
			Return .T.
		endif


		cQuery := " SELECT  ZZ1_ITEM "
		cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
		cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Medicao:Filial_Origem) +"' "
		cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Medicao:Num_contrato) +"' "
		cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Medicao:Cliente_cod) +"' "
		cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Medicao:Loja_cod) +"' "
		cQuery += " AND   ZZ1_PRODUT = '"+ ALLTRIM(Medicao:itens[nAux]:item_Produto) +"' "
		cQuery += " AND   ZZ1_TES    = '"+ ALLTRIM(cTes) +"' "
		cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
		TcQuery cQuery New Alias TZZ1

		if TZZ1->(EOF()) .and. ALLTRIM(Medicao:itens[nAux]:item_Tipo) == "K"
			::Retorno:cOcorrencia := "01" 
			::Retorno:cObs := "Produdo do item "+Alltrim(Medicao:itens[nAux]:item_ItemSec)+" nao cadastrado no contrato com a TES "+ALLTRIM(cTes)
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()
			RESTAREA( aArea )		
			Return .T.
		endif

		TZZ1->(DBCLOSEAREA())

		/*IF Alltrim(Medicao:itens[nAux]:item_Tipo) == "K" .and. ALLTRIM(POSICIONE("ZZ1",1,ALLTRIM(MEDICAO:Filial_origem) + ALLTRIM(MEDICAO:Cliente_cod) + ALLTRIM(MEDICAO:Loja_cod ) + Alltrim(Medicao:itens[nAux]:item_Produto),"ZZ1_ITEM")) == ""
		::Retorno:cOcorrencia := "01" 
		::Retorno:cObs := "Kit invalido do item "+Medicao:itens[nAux]:item_ItemSec+" , nao possui no contrato! "
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		RESTAREA( aArea )		
		Return .T.
		ENDIF 
		*/
	next

	Pai_cod := alltrim(Medicao:Pai_cod)
	Pai_loj := alltrim(Medicao:Pai_loj)

	idPdvWS := Medicao:cIdPDV

	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(alltrim("01"),alltrim(Medicao:Filial_Origem),,,,GetEnvServer(),{"SX3","SX6","SE1","SIX"}) //EMPRESA D1
	cfilant := Medicao:Filial_Origem

	if alltrim(Medicao:cOperacao) == "3" // inclus�o de medi��o 
		// PEGAR CONTRATO
		cQuery := " SELECT  * "
		cQuery += " FROM  "+RETSQLNAME("ZZ0")+" ZZ0 "
		cQuery += " WHERE ZZ0_FILIAL = '"+ ALLTRIM(Medicao:Filial_Origem) +"' "
		cQuery += " AND   ZZ0_NUMERO = '"+ ALLTRIM(Medicao:Num_contrato) +"' "
		cQuery += " AND   ZZ0_CLIENT = '"+ ALLTRIM(Medicao:Cliente_cod) +"' "
		cQuery += " AND   ZZ0_LOJA   = '"+ ALLTRIM(Medicao:Loja_cod) +"' "
		cQuery += " AND   ZZ0.D_E_L_E_T_ = '' " 
		TcQuery cQuery New Alias TZZ0

		Begin Transaction
			//Chamada de fun��o para verificar os produtos informados 
			//	-Caso o kit n�o exista no contarto , ser� gerado um aditivo do kit.
			//	-Caso o Kit n�o tenha saldo , ser� feito um aditivo de quantidade.
			KitWS() 

			oModel  := FWLoadModel('SASP036')//                   // CARREGANDO O MODEL 
			oModel:SetOperation( MODEL_OPERATION_INSERT )         // DIGO O TIPO DE OPERA��O (INCLUS�O)
			oModel:Activate()                                     // ATIVO O MODEL



			//POSICIONA NO MODEL DO CABE�ALHO PARA SETAR AS VARIAVEIS 
			oModelZZ0 := oModel:GetModel('ZZ0MASTER')  


			IF !TZZ0->(EOF())

				cNatuWS := TZZ0->ZZ0_NATURE
				cObWS   := "Aluno(a): " + MEDICAO:cNomeAluno + " | " + chr(10) + "Nf Principal da Escola: "+ MEDICAO:cNFEscola + " | " + chr(10) + MEDICAO:Obs + chr(10)
				cLocWs  := "001"  //definido pelo usuario como 0001 //MEDICAO:LocExpd
				cTpMedws := "000033" //definido como sendo sempre livrarias 000033 //MEDI��O:Tipo_med 
				dDtSolWs := dDataBase

				oModel:LoadValue("ZZ0MASTER", "ZZ0_FILIAL", TZZ0->ZZ0_FILIAL) 
				oModel:LoadValue("ZZ0MASTER", "ZZ0_NUMCRM", TZZ0->ZZ0_NUMCRM)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_NUMERO", TZZ0->ZZ0_NUMERO)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CLIENT", TZZ0->ZZ0_CLIENT)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_LOJA",   TZZ0->ZZ0_LOJA)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_NOME",   TZZ0->ZZ0_NOME)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_NATURE", TZZ0->ZZ0_NATURE)				
				oModel:LoadValue("ZZ0MASTER", "ZZ0_EMISSA", TZZ0->ZZ0_EMISSA)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_VIGINI", stod(TZZ0->ZZ0_VIGINI))
				oModel:LoadValue("ZZ0MASTER", "ZZ0_VIGFIM", stod(TZZ0->ZZ0_VIGFIM))
				oModel:LoadValue("ZZ0MASTER", "ZZ0_ANOCOM", TZZ0->ZZ0_ANOCOM)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CODCRM", TZZ0->ZZ0_CODCRM)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_DTASSI", stod(TZZ0->ZZ0_DTASSI))
				oModel:LoadValue("ZZ0MASTER", "ZZ0_FRTCLI", TZZ0->ZZ0_FRTCLI)//Medicao:Frtcli)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_FRTEMP", TZZ0->ZZ0_FRTEMP)//Medicao:FrtEmp)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_ADIANT", TZZ0->ZZ0_ADIANT)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CPFIN",  TZZ0->ZZ0_CPFIN)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CCUSTO", TZZ0->ZZ0_CCUSTO)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_FATDIR", "1")   // FOI DEFINIDO QUE SEMPRE SER� FATURAMENTO DIRETO 
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CONFNF", TZZ0->ZZ0_CONFNF)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_TIPCTR", TZZ0->ZZ0_TIPCTR)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_SITUAC", 'N')
				oModel:LoadValue("ZZ0MASTER", "ZZ0_STATUS", "A")
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CONORI", '')
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CLIORI", '')
				oModel:LoadValue("ZZ0MASTER", "ZZ0_LOJORI", '')
				oModel:LoadValue("ZZ0MASTER", "ZZ0_APROV",  '')  

				oModelZZ3 := oModel:GetModel('ZZ3DETAIL')    

				FOR nAux := 1 To len(Medicao:itens)
					oModelZZ3:GoLine(nAux)

					oModel:SetValue("ZZ3DETAIL", "ZZ3_FILIAL", TZZ0->ZZ0_FILIAL)
					oModel:SetValue("ZZ3DETAIL", "ZZ3_NUMERO", TZZ0->ZZ0_NUMERO)
					oModel:SetValue("ZZ3DETAIL", "ZZ3_CLIENT", TZZ0->ZZ0_CLIENT)
					oModel:SetValue("ZZ3DETAIL", "ZZ3_LOJA",   TZZ0->ZZ0_LOJA)

					cQuery := " SELECT  ZZ1_ITEM "
					cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
					cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Medicao:Filial_Origem) +"' "
					cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Medicao:Num_contrato) +"' "
					cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Medicao:Cliente_cod) +"' "
					cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Medicao:Loja_cod) +"' "
					cQuery += " AND   ZZ1_PRODUT = '"+ ALLTRIM(Medicao:itens[nAux]:item_Produto) +"' "
					cQuery += " AND   ZZ1_TES    = '"+ ALLTRIM(cTes) +"' "
					cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
					TcQuery cQuery New Alias TZZ1

					oModel:SetValue("ZZ3DETAIL", "ZZ3_ITEM",   TZZ1->ZZ1_ITEM  )

					TZZ1->(DBCLOSEAREA())

					oModel:LoadValue("ZZ3DETAIL", "ZZ3_PRODUT", alltrim(Medicao:itens[nAux]:item_Produto))
					oModel:SetValue("ZZ3DETAIL", "ZZ3_PRODUT", alltrim(Medicao:itens[nAux]:item_Produto))
					IF alltrim(Medicao:itens[nAux]:item_Tipo) == 'P'
						oModel:LoadValue("ZZ3DETAIL", "ZZ3_DESCRI", POSICIONE("SB1",1,XFILIAL("SB1")+Medicao:itens[nAux]:item_Produto,"B1_DESC"))
					ELSEIF alltrim(Medicao:itens[nAux]:item_Tipo) == 'K'
						oModel:LoadValue("ZZ3DETAIL", "ZZ3_DESCRI", POSICIONE("ZZ7",1,XFILIAL("ZZ7")+Medicao:itens[nAux]:item_Produto,"ZZ7_DESCR"))
					ENDIF
					oModel:LoadValue("ZZ3DETAIL", "ZZ3_QUANT",  Medicao:itens[nAux]:item_Quantida)
					oModel:SetValue("ZZ3DETAIL", "ZZ3_QUANT",  Medicao:itens[nAux]:item_Quantida)
					oModel:SetValue("ZZ3DETAIL", "ZZ3_TES",    cTes)
					IF Medicao:itens[nAux]:item_Tipo == 'P'
						oModel:SetValue("ZZ3DETAIL", "ZZ3_ENVIO", POSICIONE("SB1",1,XFILIAL("SB1")+alltrim(Medicao:itens[nAux]:item_Produto),"B1_YVOL"))
						oModel:SetValue("ZZ3DETAIL", "ZZ3_ENVPRV", POSICIONE("SB1",1,XFILIAL("SB1")+alltrim(Medicao:itens[nAux]:item_Produto),"B1_YVOL"))
					ELSEIF Medicao:itens[nAux]:item_Tipo == 'K'
						oModel:SetValue("ZZ3DETAIL", "ZZ3_ENVIO", POSICIONE("ZZ7",1,XFILIAL("ZZ7")+alltrim(Medicao:itens[nAux]:item_Produto),"ZZ7_ENVIO"))
						oModel:SetValue("ZZ3DETAIL", "ZZ3_ENVPRV", POSICIONE("ZZ7",1,XFILIAL("ZZ7")+alltrim(Medicao:itens[nAux]:item_Produto),"ZZ7_ENVIO"))
					ENDIF
					oModel:SetValue("ZZ3DETAIL", "ZZ3_TIPO",   Medicao:itens[nAux]:item_Tipo) 
					oModel:SetValue("ZZ3DETAIL", "ZZ3_VLUNIT", Medicao:itens[nAux]:item_ValUnit) 
					oModel:SetValue("ZZ3DETAIL", "ZZ3_VTOTAL", Medicao:itens[nAux]:item_Quantida * Medicao:itens[nAux]:item_ValUnit)  // total item

					if nAux < len(Medicao:itens)

						IF !oModel:VldData()
							::Retorno:cOcorrencia := "01" 
							::Retorno:cObs := OMODEL:GETERRORMESSAGE()[6]
							::Retorno:dData := DATE()
							::Retorno:cHora := TIME()
							DisarmTransaction()
							RESTAREA( aArea )
							Return .T.

						ENDIF
						GETDREFRESH()
						oModelZZ3:AddLine()
						GETDREFRESH()
					endif

				NEXT

				If oModel:VldData() 
					oViewWS:=FWFormView():New()
					oViewWS:SetModel(oModel)
					//If ValType(oView)=="O"
					//	oView:REFRESH()
					//EndIf
					//oModel:CommitData() //swerro.log
					lRet := StaticCall(SASP036,ZZ3Commit,oModel)
					if !lRet
						::Retorno:cOcorrencia := "01" 
						::Retorno:cObs := "Erro ao executar execAuto na rotina de Inclusao"
						::Retorno:dData := DATE()
						::Retorno:cHora := TIME()
						DisarmTransaction()
						RESTAREA( aArea )
						return .T.
					endif
				else
					::Retorno:cOcorrencia := "01" 
					::Retorno:cObs := OMODEL:GETERRORMESSAGE()[6]
					::Retorno:dData := DATE()
					::Retorno:cHora := TIME()
					DisarmTransaction()
					RESTAREA( aArea )
					Return .T.
				EndIf
			ELSE
				::Retorno:cOcorrencia := "01" 
				::Retorno:cObs := "Contrato nao encontrado"
				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()
				DisarmTransaction()
				RESTAREA( aArea )
				TZZ0->(DBCLOSEAREA())
				Return .T.

			ENDIF

			//ATUALIZAR CAMPOS PEDIDOS
			CampoCust (C5FilWS,C5PedWS,idPdvWS)	

		END Transaction
		::Retorno:cOcorrencia := "02" 
		::Retorno:cObs := "Inclusao da medicao com sucesso! 
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()
		::Retorno:cRet  := ZZ2->ZZ2_MEDICA 

		TZZ0->(DBCLOSEAREA())

	ELSEIF alltrim(Medicao:cOperacao) == "5" // Exclus�o de medi��o 

		Begin Transaction
			// Localizar medicao
			cQuery := " SELECT  * "
			cQuery += " FROM  "+RETSQLNAME("ZZ2")+" ZZ2 "
			cQuery += " WHERE ZZ2_FILIAL = '"+ ALLTRIM(Medicao:Filial_Origem) +"' "
			cQuery += " AND   ZZ2_NUMERO = '"+ ALLTRIM(Medicao:Num_contrato) +"' "
			cQuery += " AND   ZZ2_MEDICA = '"+ ALLTRIM(Medicao:cMedicao) +"' "
			cQuery += " AND   ZZ2_CLIENT = '"+ ALLTRIM(Medicao:Cliente_cod) +"' "
			cQuery += " AND   ZZ2_LOJA   = '"+ ALLTRIM(Medicao:Loja_cod) +"' "
			cQuery += " AND   ZZ2.D_E_L_E_T_ = '' " 
			TcQuery cQuery New Alias TZZ2

			if TZZ2->(EOF())
				::Retorno:cOcorrencia := "01" 
				::Retorno:cObs := "Medicao nao encontrado"
				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()
				DisarmTransaction()
				RESTAREA( aArea )
				TZZ2->(DBCLOSEAREA())
				Return .T.

			endif

			//POSICINANDO A TABELA DE MEDI��O PARA A OPERA��O DE EXCLUS�O
			dbSelectArea("ZZ2")
			dbSetOrder(1)
			dbSeek(TZZ2->ZZ2_FILIAL + TZZ2->ZZ2_NUMERO + TZZ2->ZZ2_MEDICA + TZZ2->ZZ2_CLIENT + TZZ2->ZZ2_LOJA) //ZZ2_FILIAL, ZZ2_NUMERO, ZZ2_MEDICA, ZZ2_CLIENT, ZZ2_LOJA

			oModel  := FWLoadModel('SASP038')//                   // CARREGANDO O MODEL 
			oModel:SetOperation( MODEL_OPERATION_DELETE )         // DIGO O TIPO DE OPERA��O (INCLUS�O)
			oModel:Activate()                                     // ATIVO O MODEL

			//POSICIONA NO MODEL DO CABE�ALHO PARA SETAR AS VARIAVEIS 
			oModelZZ2 := oModel:GetModel('ZZ2MASTER')  

			If oModel:VldData() 
				oViewWS:=FWFormView():New()
				oViewWS:SetModel(oModel)
				//If ValType(oView)=="O"
				//	oView:REFRESH()
				//EndIf
				//oModel:CommitData() //swerro.log
				lRet := StaticCall(SASP038,ZZ3Commit,oModel)
				if !lRet
					::Retorno:cOcorrencia := "01" 
					::Retorno:cObs := "Erro ao executar execAuto na rotina de Inc�usao"
					::Retorno:dData := DATE()
					::Retorno:cHora := TIME()
					DisarmTransaction()
					RESTAREA( aArea )
					TZZ2->(DBCLOSEAREA()) 

					return .T.
				endif
			else
				::Retorno:cOcorrencia := "01" 
				::Retorno:cObs := OMODEL:GETERRORMESSAGE()[6]
				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()
				DisarmTransaction()
				RESTAREA( aArea )
				TZZ2->(DBCLOSEAREA())
				Return .T.	
			EndIf

			if lRet
				::Retorno:cOcorrencia := "02" 
				::Retorno:cObs := "Medicao excluida com sucesso "
				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()
			endif
		END Transaction
		TZZ2->(DBCLOSEAREA())

	ELSEIF alltrim(Medicao:cOperacao) == "4" // Altera��o de medi��o 

		//PRIMEIRO � FEITO A EXCLUS�O DA MEDI��O PARA DEPOIS GERAR UMA NOVA MEDI��O 
		Begin Transaction

			//------------------//
			//					//
			// EXCLUS�O         //
			//					//
			//------------------//	


			// Localizar medicao
			cQuery := " SELECT  * "
			cQuery += " FROM  "+RETSQLNAME("ZZ2")+" ZZ2 "
			cQuery += " WHERE ZZ2_FILIAL = '"+ ALLTRIM(Medicao:Filial_Origem) +"' "
			cQuery += " AND   ZZ2_NUMERO = '"+ ALLTRIM(Medicao:Num_contrato) +"' "
			cQuery += " AND   ZZ2_MEDICA = '"+ ALLTRIM(Medicao:cMedicao) +"' "
			cQuery += " AND   ZZ2_CLIENT = '"+ ALLTRIM(Medicao:Cliente_cod) +"' "
			cQuery += " AND   ZZ2_LOJA   = '"+ ALLTRIM(Medicao:Loja_cod) +"' "
			cQuery += " AND   ZZ2.D_E_L_E_T_ = '' " 
			TcQuery cQuery New Alias TZZ2

			//POSICINANDO A TABELA DE MEDI��O PARA A OPERA��O DE EXCLUS�O
			dbSelectArea("ZZ2")
			dbSetOrder(1)
			dbSeek(TZZ2->ZZ2_FILIAL + TZZ2->ZZ2_NUMERO + TZZ2->ZZ2_MEDICA + TZZ2->ZZ2_CLIENT + TZZ2->ZZ2_LOJA) //ZZ2_FILIAL, ZZ2_NUMERO, ZZ2_MEDICA, ZZ2_CLIENT, ZZ2_LOJA

			oModel  := FWLoadModel('SASP038')//                   // CARREGANDO O MODEL 
			oModel:SetOperation( MODEL_OPERATION_DELETE )         // DIGO O TIPO DE OPERA��O (INCLUS�O)
			oModel:Activate()                                     // ATIVO O MODEL

			//POSICIONA NO MODEL DO CABE�ALHO PARA SETAR AS VARIAVEIS 
			oModelZZ2 := oModel:GetModel('ZZ2MASTER')  

			If oModel:VldData() 
				oViewWS:=FWFormView():New()
				oViewWS:SetModel(oModel)
				//If ValType(oView)=="O"
				//	oView:REFRESH()
				//EndIf
				//oModel:CommitData() //swerro.log
				lRet := StaticCall(SASP038,ZZ3Commit,oModel)
				if !lRet
					::Retorno:cOcorrencia := "01" 	
					::Retorno:cObs := "Erro ao executar execAuto na rotina de exclusao"
					::Retorno:dData := DATE()
					::Retorno:cHora := TIME()
					DisarmTransaction()
					RESTAREA( aArea )
					return .T.
				endif
			else
				::Retorno:cOcorrencia := "01" 
				::Retorno:cObs := OMODEL:GETERRORMESSAGE()[6]
				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()
				DisarmTransaction()
				RESTAREA( aArea )
				Return .T.	
			EndIf

			//TZZ2->(DBCLOSEAREA())

			//----------------------//
			//                      //
			// INCLUS�O             //
			//                      //
			//----------------------//
			IF ALLTRIM(MEDICAO:cMan_Dtf) == "M" 
				cTes := GetMV("SA_TESMANI")
			ELSEIF ALLTRIM(MEDICAO:cMan_Dtf) == "EF" 
				cTes := GetMV("SA_TESDTF")
			ELSEIF ALLTRIM(MEDICAO:cMan_Dtf) == "NC" 
				cTes := GetMV("SA_TESNC")	
			ENDIF
			// PEGAR CONTRATO
			cQuery := " SELECT  * "
			cQuery += " FROM  "+RETSQLNAME("ZZ0")+" ZZ0 "
			cQuery += " WHERE ZZ0_FILIAL = '"+ ALLTRIM(Medicao:Filial_Origem) +"' "
			cQuery += " AND   ZZ0_NUMERO = '"+ ALLTRIM(Medicao:Num_contrato) +"' "
			cQuery += " AND   ZZ0_CLIENT = '"+ ALLTRIM(Medicao:Cliente_cod) +"' "
			cQuery += " AND   ZZ0_LOJA   = '"+ ALLTRIM(Medicao:Loja_cod) +"' "
			cQuery += " AND   ZZ0.D_E_L_E_T_ = '' " 
			TcQuery cQuery New Alias TZZ0

			//Chamada de fun��o para verificar os produtos informados 
			//	-Caso o kit n�o exista no contarto , ser� gerado um aditivo do kit.
			//	-Caso o Kit n�o tenha saldo , ser� feito um aditivo de quantidade.
			KitWS() 


			oModel  := FWLoadModel('SASP036')//                   // CARREGANDO O MODEL 
			oModel:SetOperation( MODEL_OPERATION_INSERT )         // DIGO O TIPO DE OPERA��O (INCLUS�O)
			oModel:Activate()                                     // ATIVO O MODEL

			//POSICIONA NO MODEL DO CABE�ALHO PARA SETAR AS VARIAVEIS 
			oModelZZ0 := oModel:GetModel('ZZ0MASTER')  

			IF !TZZ0->(EOF())

				cNatuWS := TZZ0->ZZ0_NATURE
				cObWS   := "Aluno(a): " + MEDICAO:cNomeAluno + " | " + chr(10) + "Nf Principal da Escola: "+ MEDICAO:cNFEscola + " | " + chr(10) + MEDICAO:Obs + chr(10)
				cLocWs  := "001"  //definido pelo usuario como 0001 //MEDICAO:LocExpd
				cTpMedws := "000033" //definido como sendo sempre livrarias 000033 //MEDI��O:Tipo_med 
				dDtSolWs := dDataBase

				oModel:LoadValue("ZZ0MASTER", "ZZ0_FILIAL", TZZ0->ZZ0_FILIAL) 
				oModel:LoadValue("ZZ0MASTER", "ZZ0_NUMCRM", TZZ0->ZZ0_NUMCRM)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_NUMERO", TZZ0->ZZ0_NUMERO)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CLIENT", TZZ0->ZZ0_CLIENT)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_LOJA",   TZZ0->ZZ0_LOJA)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_NOME",   TZZ0->ZZ0_NOME)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_NATURE", TZZ0->ZZ0_NATURE)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_EMISSA", TZZ0->ZZ0_EMISSA)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_VIGINI", stod(TZZ0->ZZ0_VIGINI))
				oModel:LoadValue("ZZ0MASTER", "ZZ0_VIGFIM", stod(TZZ0->ZZ0_VIGFIM))
				oModel:LoadValue("ZZ0MASTER", "ZZ0_ANOCOM", TZZ0->ZZ0_ANOCOM)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CODCRM", TZZ0->ZZ0_CODCRM)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_DTASSI", stod(TZZ0->ZZ0_DTASSI))
				oModel:LoadValue("ZZ0MASTER", "ZZ0_FRTCLI", TZZ0->ZZ0_FRTCLI)//Medicao:Frtcli)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_FRTEMP", TZZ0->ZZ0_FRTEMP)//Medicao:FrtEmp)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_ADIANT", TZZ0->ZZ0_ADIANT)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CPFIN",  TZZ0->ZZ0_CPFIN)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CCUSTO", TZZ0->ZZ0_CCUSTO)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_FATDIR", "1")   // FOI DEFINIDO QUE SEMPRE SER� FATURAMENTO DIRETO 	
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CONFNF", TZZ0->ZZ0_CONFNF)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_TIPCTR", TZZ0->ZZ0_TIPCTR)
				oModel:LoadValue("ZZ0MASTER", "ZZ0_SITUAC", 'N')
				oModel:LoadValue("ZZ0MASTER", "ZZ0_STATUS", "A")
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CONORI", '')
				oModel:LoadValue("ZZ0MASTER", "ZZ0_CLIORI", '')
				oModel:LoadValue("ZZ0MASTER", "ZZ0_LOJORI", '')
				oModel:LoadValue("ZZ0MASTER", "ZZ0_APROV",  '')  

				oModelZZ3 := oModel:GetModel('ZZ3DETAIL')    

				FOR nAux := 1 To len(Medicao:itens)
					oModelZZ3:GoLine(nAux)

					oModel:SetValue("ZZ3DETAIL", "ZZ3_FILIAL", TZZ0->ZZ0_FILIAL)
					oModel:SetValue("ZZ3DETAIL", "ZZ3_NUMERO", TZZ0->ZZ0_NUMERO)
					oModel:SetValue("ZZ3DETAIL", "ZZ3_CLIENT", TZZ0->ZZ0_CLIENT)
					oModel:SetValue("ZZ3DETAIL", "ZZ3_LOJA",   TZZ0->ZZ0_LOJA)

					cQuery := " SELECT  ZZ1_ITEM "
					cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
					cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Medicao:Filial_Origem) +"' "
					cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Medicao:Num_contrato) +"' "
					cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Medicao:Cliente_cod) +"' "
					cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Medicao:Loja_cod) +"' "
					cQuery += " AND   ZZ1_PRODUT = '"+ ALLTRIM(Medicao:itens[nAux]:item_Produto) +"' "
					cQuery += " AND   ZZ1_TES    = '"+ ALLTRIM(cTes) +"' "
					cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
					TcQuery cQuery New Alias TZZ1

					oModel:SetValue("ZZ3DETAIL", "ZZ3_ITEM",   TZZ1->ZZ1_ITEM  )

					TZZ1->(DBCLOSEAREA())

					oModel:LoadValue("ZZ3DETAIL", "ZZ3_PRODUT", alltrim(Medicao:itens[nAux]:item_Produto))
					oModel:SetValue("ZZ3DETAIL", "ZZ3_PRODUT", alltrim(Medicao:itens[nAux]:item_Produto))
					IF alltrim(Medicao:itens[nAux]:item_Tipo) == 'P'
						oModel:LoadValue("ZZ3DETAIL", "ZZ3_DESCRI", POSICIONE("SB1",1,XFILIAL("SB1")+Medicao:itens[nAux]:item_Produto,"B1_DESC"))
					ELSEIF alltrim(Medicao:itens[nAux]:item_Tipo) == 'K'
						oModel:LoadValue("ZZ3DETAIL", "ZZ3_DESCRI", POSICIONE("ZZ7",1,XFILIAL("ZZ7")+Medicao:itens[nAux]:item_Produto,"ZZ7_DESCR"))
					ENDIF
					oModel:LoadValue("ZZ3DETAIL", "ZZ3_QUANT",  Medicao:itens[nAux]:item_Quantida)
					oModel:SetValue("ZZ3DETAIL", "ZZ3_QUANT",  Medicao:itens[nAux]:item_Quantida)
					oModel:SetValue("ZZ3DETAIL", "ZZ3_TES",    cTes)
					IF Medicao:itens[nAux]:item_Tipo == 'P'
						oModel:SetValue("ZZ3DETAIL", "ZZ3_ENVIO", POSICIONE("SB1",1,XFILIAL("SB1")+alltrim(Medicao:itens[nAux]:item_Produto),"B1_YVOL"))
						oModel:SetValue("ZZ3DETAIL", "ZZ3_ENVPRV", POSICIONE("SB1",1,XFILIAL("SB1")+alltrim(Medicao:itens[nAux]:item_Produto),"B1_YVOL"))
					ELSEIF Medicao:itens[nAux]:item_Tipo == 'K'
						oModel:SetValue("ZZ3DETAIL", "ZZ3_ENVIO", POSICIONE("ZZ7",1,XFILIAL("ZZ7")+alltrim(Medicao:itens[nAux]:item_Produto),"ZZ7_ENVIO"))
						oModel:SetValue("ZZ3DETAIL", "ZZ3_ENVPRV", POSICIONE("ZZ7",1,XFILIAL("ZZ7")+alltrim(Medicao:itens[nAux]:item_Produto),"ZZ7_ENVIO"))
					ENDIF
					oModel:SetValue("ZZ3DETAIL", "ZZ3_TIPO",   Medicao:itens[nAux]:item_Tipo) 
					oModel:SetValue("ZZ3DETAIL", "ZZ3_VLUNIT", Medicao:itens[nAux]:item_ValUnit) 
					oModel:SetValue("ZZ3DETAIL", "ZZ3_VTOTAL", Medicao:itens[nAux]:item_Quantida * Medicao:itens[nAux]:item_ValUnit)  // total item

					if nAux < len(Medicao:itens)

						IF !oModel:VldData()
							::Retorno:cOcorrencia := "01" 
							::Retorno:cObs := OMODEL:GETERRORMESSAGE()[6]
							::Retorno:dData := DATE()
							::Retorno:cHora := TIME()
							DisarmTransaction()
							RESTAREA( aArea )
							Return .T.

						ENDIF
						GETDREFRESH()
						oModelZZ3:AddLine()
						GETDREFRESH()
					endif
					
				NEXT

				If oModel:VldData() 
					oViewWS:=FWFormView():New()
					oViewWS:SetModel(oModel)
					//If ValType(oView)=="O"
					//	oView:REFRESH()
					//EndIf
					//oModel:CommitData() //swerro.log
					lRet := StaticCall(SASP036,ZZ3Commit,oModel)
					if !lRet
						::Retorno:cOcorrencia := "01" 
						::Retorno:cObs := "Erro ao executar execAuto na rotina de inclusao"
						::Retorno:dData := DATE()
						::Retorno:cHora := TIME()
						DisarmTransaction()
						RESTAREA( aArea )
						return .T.
					endif
				else
					::Retorno:cOcorrencia := "01" 
					::Retorno:cObs := OMODEL:GETERRORMESSAGE()[6]
					::Retorno:dData := DATE()
					::Retorno:cHora := TIME()
					DisarmTransaction()
					RESTAREA( aArea )
					Return .T.
				EndIf
			ELSE
				::Retorno:cOcorrencia := "01" 
				::Retorno:cObs := "Contrato nao encontrado"
				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()
				DisarmTransaction()
				RESTAREA( aArea )
				TZZ2->(DBCLOSEAREA())
				Return .T.
			ENDIF

			//ATUALIZANDO CAMPOS 
			CampoCust (C5FilWS,C5PedWS,idPdvWS)	

			if lRet
				::Retorno:cOcorrencia := "02" 
				::Retorno:cObs := "Medi��o alterada com sucesso, novo codigo: "+ZZ2->ZZ2_MEDICA+" "
				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()
				::Retorno:cRet  := ZZ2->ZZ2_MEDICA 
			endif
		End Transaction
		
		TZZ2->(DBCLOSEAREA())
		TZZ0->(DBCLOSEAREA())

	ENDIF

return .T.


// fun�ao de aditivo 
Static Function KitWS()
	Local lRet := .T.
	Local aItens := {}

	FOR nAux := 1 to len(Medicao:itens)

		cQuery := " SELECT  * "
		cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
		cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Medicao:Filial_Origem) +"' "
		cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Medicao:Num_contrato) +"' "
		cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Medicao:Cliente_cod) +"' "
		cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Medicao:Loja_cod) +"' "
		cQuery += " AND   ZZ1_PRODUT = '"+ ALLTRIM(Medicao:itens[nAux]:item_Produto) +"' "
		cQuery += " AND   ZZ1_TES = '"+ ALLTRIM(cTes) +"' "	
		cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
		TcQuery cQuery New Alias TZZ1

		IF TZZ1->(EOF()) .AND.	Medicao:itens[nAux]:item_Tipo == "P"
			aadd(aItens,{Medicao:itens[nAux]:item_Produto,Medicao:itens[nAux]:item_Quantida,"","P",Medicao:itens[nAux]:item_ValUnit,"P"}) // ADITIVO DE KIT
			TZZ1->(DBCLOSEAREA())
		ELSEIF !TZZ1->(EOF()) .AND. TZZ1->ZZ1_SALDO < Medicao:itens[nAux]:item_Quantida
			aadd(aItens,{TZZ1->ZZ1_PRODUT,Medicao:itens[nAux]:item_Quantida - TZZ1->ZZ1_SALDO,TZZ1->ZZ1_ITEM,"Q",Medicao:itens[nAux]:item_ValUnit,Medicao:itens[nAux]:item_Tipo}) // ADITIVO DE QUANTIDADE
			TZZ1->(DBCLOSEAREA())
		ELSE	
			TZZ1->(DBCLOSEAREA())
			//LOOP
		ENDIF


	NEXT

	//VERIFICA SE EXISTE ITENS A SEREM ADITIVADOS
	IF LEN(aItens) > 0 
		ASORT(aItens,,, { |x, y| x[4] < y[4] } ) 
		Begin Transaction


			//posicionar no contrato 
			dbSelectArea("ZZ0")
			dbSetOrder(1) //ZZ0_FILIAL, ZZ0_NUMERO, ZZ0_CLIENT, ZZ0_LOJA
			dbSeek(TZZ0->ZZ0_FILIAL + TZZ0->ZZ0_NUMERO + TZZ0->ZZ0_CLIENT + TZZ0->ZZ0_LOJA) 

			oModel := FWLoadModel('SASP042') //                   // CARREGANDO O MODEL 
			oModel:SetOperation( MODEL_OPERATION_INSERT )         // DIGO O TIPO DE OPERA��O (INCLUS�O)
			oModel:Activate()                                     // ATIVO O MODEL

			//POSICIONA NO MODEL DO CABE�ALHO PARA SETAR AS VARIAVEIS 
			oModelZZ0 := oModel:GetModel('ZZ0MASTER')  

			oModel:LoadValue("ZZ0MASTER", "ZZ0_FILIAL", ZZ0_FILIAL) 
			oModel:LoadValue("ZZ0MASTER", "ZZ0_NUMCRM", ZZ0_NUMCRM)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_NUMERO", ZZ0_NUMERO)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_CLIENT", ZZ0_CLIENT)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_LOJA",   ZZ0_LOJA)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_NOME",   ZZ0_NOME)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_NATURE", ZZ0_NATURE)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_EMISSA", ZZ0_EMISSA)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_VIGINI", ZZ0_VIGINI)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_VIGFIM", ZZ0_VIGFIM)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_ANOCOM", ZZ0_ANOCOM)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_CODCRM", ZZ0_CODCRM)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_DTASSI", ZZ0_DTASSI)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_FRTCLI", ZZ0_FRTCLI)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_FRTEMP", ZZ0_FRTEMP)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_ADIANT", ZZ0_ADIANT)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_CPFIN",  ZZ0_CPFIN)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_CCUSTO", ZZ0_CCUSTO)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_FATDIR", ZZ0_FATDIR)   	
			oModel:LoadValue("ZZ0MASTER", "ZZ0_CONFNF", ZZ0_CONFNF)
			oModel:LoadValue("ZZ0MASTER", "ZZ0_TIPCTR", ZZ0_TIPCTR)

			oModelZZ6 := oModel:GetModel('ZZ6DETAIL') 

			FOR  nAux := 1 to len(aItens)

				IF aItens[nAux][4] == "P"
				
					cQuery := " SELECT  TOP (1) * "
					cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
					cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Medicao:Filial_Origem) +"' "
					cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Medicao:Num_contrato) +"' "
					cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Medicao:Cliente_cod) +"' "
					cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Medicao:Loja_cod) +"' "
					cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
					cQuery += " ORDER BY  ZZ1_ITEM DESC " 
					TcQuery cQuery New Alias TZZ1

					oModel:LoadValue("ZZ6DETAIL", "ZZ6_FILIAL", TZZ1->ZZ1_FILIAL)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_NUMERO", TZZ1->ZZ1_NUMERO)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_ITEM",   STRZERO(VAL(TZZ1->ZZ1_ITEM) + nAux, 3, 0))
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_CLIENT", TZZ1->ZZ1_CLIENT)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_LOJA", TZZ1->ZZ1_LOJA)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_TES", cTes)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_PRODUT", alltrim(aItens[nAux][1]))
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_DESCRI", POSICIONE("SB1",1,XFILIAL("SB1")+ALLTRIM(aItens[nAux][1]),"B1_DESC"))
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_QUANT",  aItens[nAux][2]) //aItens[nAux][1]
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRUN",  aItens[nAux][5]) //aItens[nAux][5]
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRTOT", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLANT", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTANT", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLATU", aItens[nAux][2])
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTATU", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_TPPROD","P")
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_STATUS", "A")
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRDSC", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_TIPO", "N")

					// DIPARANDO OS GATILHOS 
					oModel:SetValue("ZZ6DETAIL", "ZZ6_PRODUT", alltrim(aItens[nAux][1]))
					oModel:SetValue("ZZ6DETAIL", "ZZ6_QUANT",  aItens[nAux][2]) //aItens[nAux][1]
					oModel:SetValue("ZZ6DETAIL", "ZZ6_QTATU", aItens[nAux][2])
					oModel:SetValue("ZZ6DETAIL", "ZZ6_VLRUN",  aItens[nAux][5]) //aItens[nAux][5]
					oModel:SetValue("ZZ6DETAIL", "ZZ6_VLRTOT", aItens[nAux][2] * aItens[nAux][5])

					TZZ1->(DBCLOSEAREA())
					if nAux < len(aItens)
						oModelZZ6:AddLine()
					endif
				ELSEIF aItens[nAux][4] == "Q"

					cQuery := " SELECT  * "
					cQuery += " FROM  "+RETSQLNAME("ZZ1")+" ZZ1 "
					cQuery += " WHERE ZZ1_FILIAL = '"+ ALLTRIM(Medicao:Filial_Origem) +"' "
					cQuery += " AND   ZZ1_NUMERO = '"+ ALLTRIM(Medicao:Num_contrato) +"' "
					cQuery += " AND   ZZ1_CLIENT = '"+ ALLTRIM(Medicao:Cliente_cod) +"' "
					cQuery += " AND   ZZ1_LOJA   = '"+ ALLTRIM(Medicao:Loja_cod) +"' "
					cQuery += " AND   ZZ1_PRODUT = '"+ aItens[nAux][1] +"' "
					cQuery += " AND   ZZ1_TES = '"+ ALLTRIM(cTes) +"' "	
					cQuery += " AND   ZZ1_ITEM = '"+ aItens[nAux][3] +"' "	
					cQuery += " AND   ZZ1.D_E_L_E_T_ = '' " 
					cQuery += " ORDER BY  ZZ1_ITEM DESC " 
					TcQuery cQuery New Alias TZZ1

					oModel:LoadValue("ZZ6DETAIL", "ZZ6_FILIAL", TZZ1->ZZ1_FILIAL)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_NUMERO", TZZ1->ZZ1_NUMERO)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_ITEM",   TZZ1->ZZ1_ITEM)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_CLIENT", TZZ1->ZZ1_CLIENT)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_LOJA",   TZZ1->ZZ1_LOJA)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_TES",    cTes)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_PRODUT", alltrim(TZZ1->ZZ1_PRODUT))
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_DESCRI", TZZ1->ZZ1_DESC)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_QUANT",  aItens[nAux][2]) //aItens[nAux][1]
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRUN",  TZZ1->ZZ1_VLRUN ) //aItens[nAux][5]
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRTOT", 0)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLANT", TZZ1->ZZ1_SALDO )
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTANT", TZZ1->ZZ1_QUANT)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_SLATU", TZZ1->ZZ1_SALDO + aItens[nAux][2])
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_QTATU", TZZ1->ZZ1_QUANT + aItens[nAux][2])
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_TPPROD",TZZ1->ZZ1_TIPO)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_STATUS", "A")
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_VLRDSC", TZZ1->ZZ1_VLRDSC)
					oModel:LoadValue("ZZ6DETAIL", "ZZ6_TIPO", "A")

					// DIPARANDO OS GATILHOS 
					oModel:SetValue("ZZ6DETAIL", "ZZ6_QUANT",  aItens[nAux][2]) //aItens[nAux][1]
					oModel:SetValue("ZZ6DETAIL", "ZZ6_VLRTOT", aItens[nAux][2] * TZZ1->ZZ1_VLRUN)

					TZZ1->(DBCLOSEAREA())

					if nAux < len(aItens)
						IF !oModel:VldData()
							::Retorno:cOcorrencia := "01" 
							::Retorno:cObs := OMODEL:GETERRORMESSAGE()[6]
							::Retorno:dData := DATE()
							::Retorno:cHora := TIME()
							DisarmTransaction()
							RESTAREA( aArea )
							Return .T.

						ENDIF
						oModelZZ6:AddLine()
					endif

				ENDIF
			NEXT

			If oModel:VldData()
				oViewWS:=FWFormView():New()
				oViewWS:SetModel(oModel)
				//If ValType(oView)=="O"
				//	oView:REFRESH()
				//EndIf
				//oModel:CommitData() //swerro.log
				lRet := StaticCall(SASP042,ZZ6Commit,oModel)	
				IF !lRet
					::Retorno:cOcorrencia := "01" 
					::Retorno:cObs := "Erro no execauto de aditivo"
					::Retorno:dData := DATE()
					::Retorno:cHora := TIME()
					DisarmTransaction()
					RESTAREA( aArea )
					Return .T.
				endif 	
			ELSE
				::Retorno:cOcorrencia := "01" 
				::Retorno:cObs := OMODEL:GETERRORMESSAGE()[6]
				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()
				DisarmTransaction()
				RESTAREA( aArea )
				Return .T.
			ENDIF
		End   Transaction

	ENDIF

Return lRet


STATIC FUNCTION MONTAKIT(cKit)

	Local nRegistros := 0
	Local cTipo :=  ""
	Local cDesc := ""
	Local nRateio := 0
	Local nPerDes := 0
	Local cQuery8 := ""
	Local nValorDes := 0
	Local cAliasT := GetNextAlias()
	Local nRateio
	Local nDesconto

	cQuery := ""
	cQuery += " SELECT ZZ7_CODIGO, ZZ7_DESCR "
	cQuery += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7 "
	cQuery += "  WHERE D_E_L_E_T_ = '' "
	cQuery += "    AND ZZ7_CODPAI = '" + cKit +  "' "
	cQuery += "    AND ZZ7_TIPO = 'S' "
	cQuery += "    AND ZZ7_CODIGO <> '" + cKit + "' "  
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasT,.F.,.T.)

	Count To nRegistros

	(cAliasT)->(DBGOTOP())

	if nRegistros > 0

		While (cAliasT)->(!EOF())												

			MONTAKIT((cAliasT)->ZZ7_CODIGO)								

			(cAliasT)->(DBSKIP())				

		EndDo

		(cAliasT)->(DBCLOSEAREA())
	Else
		nRateio   := RATEIOKIT(cKit)
		nDesconto := DESCKIT(cKit)
		AADD(aKitws, {cKit,nRateio,nDesconto,(cAliasT)->ZZ7_DESCR})

		(cAliasT)->(DBCLOSEAREA())

	EndIf



RETURN 


Static function CampoCust (cFil,cPed,cIDPDV)


	dbSelectArea("SC5")
	dbSetOrder(1)      // C5_FILIAL+C5_NUM
	dbSeek(cFil + cPed)     // Busca exata

	IF FOUND()    // Avalia o retorno da pesquisa realizada
		RECLOCK("SC5", .F.)

		SC5->C5_YTPID   := "E"
		SC5->C5_YIDPDV  := cIDPDV

		MSUNLOCK()     // Destrava o registro
	ENDIF

	dbSelectArea("SC6")
	dbSetOrder(1)
	dbSeek(cFil + cPed)
	WHILE ALLTRIM(SC6->C6_NUM) == ALLTRIM(cPed) .and. ALLTRIM(SC6->C6_FILIAL) == ALLTRIM(cFil)

		Reclock("SC6",.F.)
		SC6->C6_YTPID   := "E"
		SC6->C6_YIDPDV  := cIDPDV
		MsUnlock()

		SC6->(dbSkip())
	Enddo

Return

static function RATEIOKIT(cKit)

	LOCAL nRateio 

	cQuery := ""
	cQuery += " SELECT ZZK_PORCEN "
	cQuery += "   FROM " + RETSQLNAME("ZZK") + " ZZK "
	cQuery += "  INNER JOIN  " + RETSQLNAME("ZZ7") + " ZZ7 "
	cQuery += "     ON ZZK.ZZK_FILIAL = ZZ7.ZZ7_FILIAL "
	cQuery += "    AND ZZK.ZZK_SERIE = ZZ7.ZZ7_SERIE "
	cQuery += "    AND ZZK.ZZK_ENVIO = ZZ7.ZZ7_ENVIO "
	cQuery += "  WHERE ZZK.D_E_L_E_T_ = '' "
	cQuery += "    AND ZZ7.D_E_L_E_T_ = '' "
	cQuery += "    AND ZZ7.ZZ7_CODIGO = '" + cKit + "' "
	TcQuery cQuery New Alias TRAT

	If TRAT->(!EOF())
		//nRateio := ROUND(aRet[1] * (PERCRAT->ZZK_PORCEN / 100),2)
		nRateio := TRAT->ZZK_PORCEN / 100
	Else
		nRateio := 1
	EndIf

	TRAT->(DBCLOSEAREA())
RETURN nRateio

static function DESCKIT(cKit)

	LOCAL nDesconto 

	cQuery8 := ""
	cQuery8 += " SELECT COUNT(*) AS QTD "
	cQuery8 += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7"
	cQuery8 += "  WHERE D_E_L_E_T_ = '' "
	cQuery8 += "    AND ZZ7.ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
	cQuery8 += "    AND ZZ7.ZZ7_CODPAI = '" + cKit + "' "
	cQuery8 += "    AND ZZ7.ZZ7_SUPLEM = 'S' "  
	TcQuery cQuery8 New Alias TDESC

	If TDESC->(!EOF())

		nDesconto := TDESC->QTD * 10

	EndIf
	TDESC->(DBCLOSEAREA())
RETURN nDesconto