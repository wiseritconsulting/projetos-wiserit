#Include 'Protheus.ch'
#Include "apwebsrv.ch"

/*

	Dever� criar os seguintes parametros:
	MV_YPREWEB: Prefixo WebService Ecommerce  
	MV_YTIWEBP: Tipo titulo a pagar WebService Ecommerce                    
	MV_YTIWEBR: Tipo titulo a receber WebService Ecommerce 
	MV_YNARWEB: Natureza titulo a receber Webservice Ecommerce
	MV_YNAPWEB: Natureza titulo a pagar webservice Ecommerce
	
	MV_YCCRWEB: Centro de custo a receber Webservice Ecommerce
	MV_YCCPWEB: Centro de custo a pagar Webservice Ecommerce
	
*/

WsService FinSASWEB;
		Description "WebServices de movimenta��es Financeiras - ECommerce"
		
	WsData filial as String
	WsData cNum as String
	WsData cliente as String
	WsData loja as String
	WsData fornecedor as String
	WsData bancochq as String
	WsData agenciachq as String
	WsData cheque as String 
	WsData contachq as String
	WsData tipo as String
	WsData natureza as String
	WsData vencimento as String
	WsData dataBaixa as String
	WsData parcela as String
	WsData contrato as String	
	WsData valor as Float
	WsData idPDV as String
	WsData TpId As String
	WsData PaiKit As String
	WsData NSU As String
	WsData venctoChq as String
	WsData recnoBanco as Integer
	
	WsData aError as ARRAY OF String
	WsData cError as String
	
		
	wsMethod GeraTitRec;
		Description "Inclus�o de titulo a Receber"
		
	wsMethod GeraTitPag;
		Description "Inclus�o de titulo a Pagar"
		
	wsMethod ExcTitRec;
		Description "Exclus�o de titulo a Receber"
		
	wsMethod ExcTitPag;
		Description "Exclus�o de titulo a Pagar"	
		
	wsMethod BaixaTitRec;
		Description "Baixa de titulo a Receber"
		
	wsMethod BaixaTitCheque;
		Description "Baixa de titulo a Receber com Cheque"
	
	wsMethod CanBaixaRec;
		Description "Cancela Baixa de titulo a Receber"
		
	wsMethod ExcBaixaRec;
		Description "Exclui Baixa de titulo a Receber"	
		
	wsMethod BaixaTitPag;
		Description "Baixa de titulo a Pagar"
		
	wsMethod CanBaixaPag;
		Description "Cancela Baixa de titulo a Receber"
		
	wsMethod ExcBaixaPag;
		Description "Exclui Baixa de titulo a Receber "		
		
	
EndWsService

WsMethod GeraTitRec wsReceive filial,cNum,cliente,loja,tipo,vencimento,parcela,valor,idPDV,TpId,PaiKit, NSU,contrato wsSend aError wsService FinSASWEB
		
	aDados := {}
	lMsErroAuto := .F.
	cError := ""
	cQuery := ""
	Self:cNum := ""
	aArea := GEtArea()
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(Substring(filial,1,2),filial,,,,GetEnvServer(),{"SX3","SX6","SE1","SIX"}) //EMPRESA D1
	
	cFilant := filial
	
	/*cQuery := ""
	cQuery += " SELECT ISNULL(MAX(E1_NUM),'0') AS NUM "
	cQuery += "   FROM " + RETSQLNAME("SE1") + " SE1 "
	cQuery += "  WHERE  SE1.E1_PREFIXO = '" + GETMV("MV_YPREWEB") + "' "
	
	dbUseArea( .T.,"TOPCONN", TcGenQry( ,,cQuery), "XSE1", .T., .F. )
	
	Self:cNum := STRZERO(Val(SOMA1(XSE1->NUM)),GETSX3CACHE("E1_NUM","X3_TAMANHO"))
	
	XSE1->(DBCLOSEAREA())*/
	
	aDados :={{ "E1_FILIAL",   alltrim(XFILIAL("SE1",filial)), NIL},;
		{ "E1_PREFIXO"  , GETMV("MV_YPREWEB")            , NIL },;
		{ "E1_NUM"      , cNum           , NIL },;
		{ "E1_PARCELA"      , parcela          , NIL },;
		{ "E1_TIPO"     , IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo), NIL },;
		{ "E1_NATUREZ"  , GETMV("MV_YNARWEB")              , NIL },;
		{ "E1_CLIENTE"  , cliente         , NIL },;
		{ "E1_LOJA"     , loja          , NIL },;
		{ "E1_CCC"     , GETMV("MV_YCCRWEB")           , NIL },;
		{ "E1_HIST"     , "Ref. Cobran�a clientes 2017"           , NIL },;
		{ "E1_YFILIAL"     , filial          , NIL },;
		{ "E1_YCONTRA"     , contrato          , NIL },;
		{ "E1_EMISSAO"  , dDataBase, NIL },;
		{ "E1_VENCTO"   , stod(vencimento), NIL },;
		{ "E1_VENCREA"  , stod(vencimento), NIL },;
		{ "E1_VALOR"    , valor, NIL },;
		{ "E1_YIDPDV"  , idPDV            , NIL },;
		{ "E1_YTPID"  , "E"            , NIL },;
		{ "E1_YNSU"  , "0000000001"            , NIL },;
		{ "E1_YPAIKIT"  , PaiKit            , NIL }}
            		
	MSExecAuto({|x,y| FINA040(x,y)},aDados,3)
	
	If lMsErroAuto
	
		AADD(Self:aError,"")
	 	AADD(Self:aError,"")
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE1->E1_NUM)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Incluido com sucesso.")		
	
	EndIf
	
	RestArea(aArea)
	
	conout(Self:aError[4])
	
return .T.

WsMethod GeraTitPag wsReceive filial,cNum,fornecedor,loja,tipo,vencimento,parcela,valor,idPDV,TpId,PaiKit wsSend aError wsService FinSASWEB
		
	aDados := {}
	lMsErroAuto := .F.
	cError := ""
	cQuery := ""
	aArea := GEtArea()
	
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(Substring(filial,1,2),filial,,,,GetEnvServer(),{"SX3","SX6","SE2","SIX"}) //EMPRESA D1
	
	cfilAnt := filial

	/*Self:cNum := ""
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(alltrim("01"),filial,,,,GetEnvServer(),{"SX3","SX6","SE2","SIX"}) //EMPRESA D1
	
	
	cQuery := ""
	cQuery += " SELECT ISNULL(MAX(E2_NUM),'0') AS NUM "
	cQuery += "   FROM " + RETSQLNAME("SE2") + " SE2 "
	cQuery += "  WHERE SE2.E2_PREFIXO = '" + GETMV("MV_YPREWEB") + "' "
	
	dbUseArea( .T.,"TOPCONN", TcGenQry( ,,cQuery), "XSE2", .T., .F. )
	
	Self:cNum := STRZERO(Val(SOMA1(XSE2->NUM)),GETSX3CACHE("E2_NUM","X3_TAMANHO"))
	
	XSE2->(DBCLOSEAREA())*/
	
	aDados :={{ "E2_FILIAL",   alltrim(XFILIAL("SE2",filial)), NIL},;
		{ "E2_PREFIXO"  , GETMV("MV_YPREWEB")             , NIL },;
		{ "E2_NUM"      , cNum           , NIL },;
		{ "E2_PARCELA"      , parcela          , NIL },;
		{ "E2_TIPO"     , IIF(EMPTY(tipo),ALLTRIM(GETMV("MV_YTIWEBP")), PADR(tipo, GETSX3CACHE("E2_TIPO","X3_TAMANHO"), " ") )              , NIL },;
		{ "E2_NATUREZ"  , ALLTRIM(GETMV("MV_YNAPWEB"))             , NIL },;
		{ "E2_FORNECE"  , fornecedor        , NIL },;
		{ "E2_CCD"  , GETMV("MV_YCCPWEB")        , NIL },;
		{ "E2_HIST"  , "Ref. Tarifa de Cart�o"      , NIL },;
		{ "E2_LOJA"     , loja         , NIL },;
		{ "E2_EMISSAO"  , dDataBase, NIL },;
		{ "E2_VENCTO"   , stod(vencimento), NIL },;
		{ "E2_VENCREA"  , stod(vencimento), NIL },;
		{ "E2_VALOR"    , valor, NIL } }
		
		/*{ "E2_YIDPDV"  , idPDV            , NIL },;
		{ "E2_YTPID"  , TpId            , NIL },;
		{ "E2_YPAIKIT"  , PaiKit            , NIL },;*/
            		
	MSExecAuto({|x,y,z| FINA050(x,y,z)},aDados,,3)
	
	If lMsErroAuto
	
		AADD(Self:aError,"")
	 	AADD(Self:aError,"")
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE2->E2_NUM)
	 	AADD(Self:aError,SE2->E2_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Incluido com sucesso.")
	
	EndIf
	
	conout(Self:aError[4])
	RestArea(aArea)
	
return .T.

WsMethod ExcTitRec wsReceive filial,cNum,tipo,cliente,loja,parcela wsSend aError wsService FinSASWEB

	aDados := {}
	lMsErroAuto := .F.
	cError := ""
	aArea := GetArea()
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(Substring(filial,1,2),alltrim(filial),,,,GetEnvServer(),{"SX3","SX6","SE1","SIX"}) //EMPRESA D1
	
	cFilAnt := filial
	
	DBSELECTAREA("SE1")
	DBSETORDER(1)
	DBSEEK(XFILIAL("SE1",filial) + PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ") + PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ") +  parcela + IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo) )
	
	
	aDados :={	{ "E1_FILIAL",   XFILIAL("SE1",filial), NIL},;
		{ "E1_PREFIXO"  ,  PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E1_PREFIXO","X3_TAMANHO"), " ")             , NIL },;
		{ "E1_NUM"      , PADR(cNum, GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ")           , NIL },;
		{ "E1_TIPO"      , PADR(tipo, GETSX3CACHE("E1_TIPO","X3_TAMANHO"), " ")           , NIL },;
		{ "E1_PARCELA"      , parcela           , NIL },;	
		{ "E1_LOJA"     , loja         , NIL } }
            		
	MSExecAuto({|x,y| FINA040(x,y)},aDados,5)
	
	If lMsErroAuto
	
		AADD(Self:aError,SE1->E1_NUM)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE1->E1_NUM)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Excluido com sucesso.")
	
	EndIf
	
	conout(Self:aError[4])
	RestArea(aArea)
	
return .T.

WsMethod ExcTitPag wsReceive filial,cNum,tipo,fornecedor,loja,parcela wsSend aError wsService FinSASWEB

	aDados := {}
	lMsErroAuto := .F.
	cError := ""
	aArea := GEtArea()
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(Substring(filial,1,2),alltrim(filial),,,,GetEnvServer(),{"SX3","SX6","SE2","SIX"}) //EMPRESA D1
	
	cFilAnt := filial
	
	DBSELECTAREA("SE2")
	DBSETORDER(1)
	DBSEEK(XFILIAL("SE2",filial) + PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ") + PADR(cNum,GETSX3CACHE("E2_NUM","X3_TAMANHO"), " ") +  parcela + IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo) )
	
	
	aDados :={	{ "E2_FILIAL",   XFILIAL("SE2",filial), NIL},;
		{ "E2_PREFIXO"  ,  PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ")             , NIL },;
		{ "E2_NUM"      , PADR(cNum,GETSX3CACHE("E2_NUM","X3_TAMANHO"), " ")          , NIL },;
		{ "E2_TIPO"      , PADR(tipo, GETSX3CACHE("E2_TIPO","X3_TAMANHO"), " ")        , NIL },;
		{ "E2_PARCELA"      , parcela           , NIL },;
		{ "E2_FORNECEDOR"  , fornecedor         , NIL },;
		{ "E2_LOJA"     , loja         , NIL } }
            		
	MSExecAuto({|x,y,z| FINA050(x,y,z)},aDados,,5)
	
	If lMsErroAuto
	
		AADD(Self:aError,SE2->E2_NUM)
	 	AADD(Self:aError,SE2->E2_PARCELA)
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE2->E2_NUM)
	 	AADD(Self:aError,SE2->E2_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Excluido com sucesso.")
	
	EndIf
	
	RestArea(aArea)
	conout(Self:aError[4])
	
return .T.


WsMethod BaixaTitRec wsReceive filial,cNum,parcela,tipo,recnoBanco,dataBaixa,valor wsSend aError wsService FinSASWEB

	aDados := {}
	lMsErroAuto := .F.
	cError := ""
	cQuery := ""	
	banco := ""
	agencia := ""
	conta := ""	
	aArea := GEtArea()
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv("01",alltrim(filial),,,,GetEnvServer(),{"SX3","SX6","SE1","SIX","SA6"}) //EMPRESA D1	
	
	cFilant := filial
	
	/*DBSELECTAREA("SA6")
	DBGOTO(recnoBanco)
	
	banco := PADR(SA6->A6_COD,GETSX3CACHE("A6_COD","X3_TAMANHO"), " ") 
	agencia := PADR(SA6->A6_AGENCIA,GETSX3CACHE("A6_AGENCIA","X3_TAMANHO"), " ") 
	conta := PADR(SA6->A6_NUMCON,GETSX3CACHE("A6_NUMCON","X3_TAMANHO"), " ")*/
	
	cQuery := ""
	cQuery += " SELECT A6_COD, A6_AGENCIA, A6_NUMCON "
	cQuery += "   FROM "+ RETSQLNAME("SA6") + " SA6 "
	cQuery += "  WHERE SA6.D_E_L_E_T_ = '' "
	cQuery += "    AND SA6.R_E_C_N_O_ = '" +  CVALTOCHAR(recnoBanco) + "' "
	
	dbUseArea( .T.,"TOPCONN", TcGenQry( ,,cQuery), "XSA6", .T., .F. )
	
	banco := XSA6->A6_COD 
	agencia := XSA6->A6_AGENCIA 
	conta := XSA6->A6_NUMCON	
	
	XSA6->(DBCLOSEAREA())	  
	
	DBSELECTAREA("SE1")
	DBSETORDER(1)
	IF DBSEEK(XFILIAL("SE1",filial) + PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E1_PREFIXO","X3_TAMANHO"), " ") + PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ") +  parcela + IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo) )
	
	
	aDados := {{"E1_FILIAL"      ,XFILIAL("SE1",filial)          ,Nil    },;
           {"E1_PREFIXO"  , PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E1_PREFIXO","X3_TAMANHO"), " ")                  ,Nil    },;			
           {"E1_NUM"      ,PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ")           ,Nil    },;
           {"E1_PARCELA"      ,parcela           ,Nil    },;
           {"E1_TIPO"     ,IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo)                 ,Nil    },;
           {"AUTMOTBX"    ,"NOR"                  ,Nil    },;
           {"AUTBANCO"    ,banco                 ,Nil    },;
           {"AUTAGENCIA"  ,agencia                ,Nil    },;
           {"AUTCONTA"    ,conta          ,Nil    },;
           {"AUTDTBAIXA"  ,stod(databaixa)              ,Nil    },;
           {"AUTDTCREDITO",stod(databaixa)              ,Nil    },;
           {"AUTHIST"     ,"Valor Recebido de Clientes-Cliente: " +  SE1->E1_CLIENTE, Nil    },;
           {"AUTJUROS"    ,0                      ,Nil,.T.},;
           {"AUTVALREC"   ,valor                   ,Nil    }}  
           
           
     /* aDados := {{"E1_FILIAL"      ,XFILIAL("SE1",filial)          ,Nil    },;
           {"E1_PREFIXO"  , PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E1_PREFIXO","X3_TAMANHO"), " ")                  ,Nil    },;			
           {"E1_NUM"      ,PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ")           ,Nil    },;
           {"E1_PARCELA"      ,parcela           ,Nil    },;
           {"E1_TIPO"     ,IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo)                 ,Nil    },;
           {"AUTMOTBX"    ,"NOR"                  ,Nil    },;
           {"AUTBANCO"    ,PADR("CX1",GETSX3CACHE("A6_COD","X3_TAMANHO"), " ")                 ,Nil    },;
           {"AUTAGENCIA"  ,PADR("00001",GETSX3CACHE("A6_AGENCIA","X3_TAMANHO")," " )                 ,Nil    },;
           {"AUTCONTA"    ,  "0000000001"  ,Nil    },;
           {"AUTDTBAIXA"  ,dDATAbase             ,Nil    },;
           {"AUTDTCREDITO",dDataBase              ,Nil    },;
           {"AUTHIST"     ,"BAIXA DO ECOMERCE"          ,Nil    },;
           {"AUTJUROS"    ,0                      ,Nil,.T.},;
           {"AUTVALREC"   ,valor                   ,Nil    }}  */
           
           //PADR("92",GETSX3CACHE("A6_NUMCON","X3_TAMANHO"), " ")
   cFilant := filial         		
	MSExecAuto({|x,y| Fina070(x,y)},aDados,3) 
	
	If lMsErroAuto
	
		AADD(Self:aError,cNum)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE1->E1_NUM)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Baixa realizada com sucesso.")
	
	EndIf
	
	RestArea(aArea)
	conout(Self:aError[4])
	
	ELSE
	
		AADD(Self:aError,cNum)
	 	AADD(Self:aError,"")
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,"TITULO N�O ENCONTRADO:" + XFILIAL("SE1",filial) + PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E1_PREFIXO","X3_TAMANHO"), " ") + PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ") +  parcela + IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo))
	
	ENDIF
	//RpcCLEARENV()
	
return .T.

WsMethod CanBaixaRec wsReceive filial,cNum,parcela,tipo,recnoBanco,dataBaixa,valor wsSend aError wsService FinSASWEB

	aDados := {}
	lMsErroAuto := .F.
	cError := ""	
	banco := ""
	agencia := ""
	conta := ""
	aArea := GEtArea()
		
	/*WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(Substring(filial,1,2),alltrim(filial),,,,GetEnvServer(),{"SX3","SX6","SE1","SIX","SA6"})*/
	
	cFilAnt := filial
	
	DBSELECTAREA("SA6")
	DBGOTO(recnoBanco)
	
	banco := SA6->A6_COD
	agencia := SA6->A6_AGENCIA
	conta := SA6->A6_NUMCON
	
	DBSELECTAREA("SE1")
	DBSETORDER(1)
	DBSEEK(XFILIAL("SE1",filial) + PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ") + PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ") +  parcela + IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo) )
	
	aDados := {{"E1_FILIAL"      ,XFILIAL("SE1",filial)          ,Nil    },;
           {"E1_PREFIXO"  , PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ")                  ,Nil    },;			
           {"E1_NUM"      ,PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ")          ,Nil    },;
           {"E1_PARCELA"      ,parcela           ,Nil    },;
           {"E1_TIPO"     ,IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo)                 ,Nil    },;
           {"AUTMOTBX"    ,"NOR"                  ,Nil    },;
           {"AUTBANCO"    ,banco                 ,Nil    },;
           {"AUTAGENCIA"  ,agencia                ,Nil    },;
           {"AUTCONTA"    ,conta           ,Nil    },;
           {"AUTDTBAIXA"  ,stod(databaixa)              ,Nil    },;
           {"AUTDTCREDITO",stod(databaixa)              ,Nil    },;
           {"AUTHIST"     ,"CANCELAMENTO ECOMERCE"          ,Nil    },;
           {"AUTJUROS"    ,0                      ,Nil,.T.},;
           {"AUTVALREC"   ,valor                   ,Nil    }}     
            		
	MSExecAuto({|x,y| Fina070(x,y)},aDados,5) 
	
	If lMsErroAuto
	
		AADD(Self:aError,SE1->E1_NUM)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE1->E1_NUM)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Baixa cancelada com sucesso.")
	
	EndIf
	
	RestArea(aArea)
	conout(Self:aError[4])
	
return .T.

WsMethod ExcBaixaRec wsReceive filial,cNum,parcela,tipo,recnoBanco,dataBaixa,valor wsSend aError wsService FinSASWEB

	aDados := {}
	lMsErroAuto := .F.
	cError := ""
	banco := ""
	agencia := ""
	conta := ""	
	aArea := GEtArea()
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(Substring(filial,1,2),alltrim(filial),,,,GetEnvServer(),{"SX3","SX6","SE1","SIX","SA6"}) //EMPRESA D1
	
	DBSELECTAREA("SA6")
	DBGOTO(recnoBanco)
	
	cFilAnt := filial
	
	banco := SA6->A6_COD
	agencia := SA6->A6_AGENCIA
	conta := SA6->A6_NUMCON
	
	DBSELECTAREA("SE1")
	DBSETORDER(1)
	DBSEEK(XFILIAL("SE1",filial) + PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ") + PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ") +  parcela + IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo) )
	
	aDados := {{"E1_FILIAL"      ,XFILIAL("SE1",filial)          ,Nil    },;
           {"E1_PREFIXO"  , PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ")                  ,Nil    },;			
           {"E1_NUM"      ,PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ")          ,Nil    },;
           {"E1_PARCELA"      ,parcela           ,Nil    },;
           {"E1_TIPO"     ,IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo)                 ,Nil    },;
           {"AUTMOTBX"    ,"NOR"                  ,Nil    },;
           {"AUTBANCO"    ,banco                 ,Nil    },;
           {"AUTAGENCIA"  ,agencia                ,Nil    },;
           {"AUTCONTA"    ,conta           ,Nil    },;
           {"AUTDTBAIXA"  ,stod(databaixa)             ,Nil    },;
           {"AUTDTCREDITO",stod(databaixa)              ,Nil    },;
           {"AUTHIST"     ,"CANCELAMENTO ECOMERCE"          ,Nil    },;
           {"AUTJUROS"    ,0                      ,Nil,.T.},;
           {"AUTVALREC"   ,valor                   ,Nil    }}     
            		
	MSExecAuto({|x,y| Fina070(x,y)},aDados,6) 
	
	If lMsErroAuto
	
		AADD(Self:aError,SE1->E1_NUM)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE1->E1_NUM)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Exclus�o com sucesso.")
	
	EndIf
	
	RestArea(aArea)
	conout(Self:aError[4])
	
return .T.


WsMethod BaixaTitPag wsReceive filial,cNum,parcela,tipo,recnoBanco,dataBaixa,valor wsSend aError wsService FinSASWEB

	aDados := {}
	aArea := GEtArea()
	lMsErroAuto := .F.
	cError := ""	
	banco := ""
	agencia := ""
	conta := ""
		
	//WSDLDBGLEVEL(2)
	//RPCSetType(3)
	//RpcSetEnv(Substring(filial,1,2),alltrim(filial),,,,GetEnvServer(),{"SX3","SX6","SE2","SIX","SA6"})
	
	cFilAnt := filial
	
	/*DBSELECTAREA("SA6")
	DBGOTO(recnoBanco)
	
	banco := SA6->A6_COD
	agencia := SA6->A6_AGENCIA
	conta := SA6->A6_NUMCON*/
	
	
	cQuery := ""
	cQuery += " SELECT A6_COD, A6_AGENCIA, A6_NUMCON "
	cQuery += "   FROM "+ RETSQLNAME("SA6") + " SA6 "
	cQuery += "  WHERE SA6.D_E_L_E_T_ = '' "
	cQuery += "    AND SA6.R_E_C_N_O_ = '" +  CVALTOCHAR(recnoBanco) + "' "
	
	dbUseArea( .T.,"TOPCONN", TcGenQry( ,,cQuery), "XSA6", .T., .F. )
	
	banco := XSA6->A6_COD 
	agencia := XSA6->A6_AGENCIA 
	conta := XSA6->A6_NUMCON	
	
	XSA6->(DBCLOSEAREA())	 
	
	DBSELECTAREA("SE2")
	DBSETORDER(1)
	DBSEEK(XFILIAL("SE2",filial) + PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ") + PADR(cNum,GETSX3CACHE("E2_NUM","X3_TAMANHO"), " ") +  parcela + IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo) )
	
	aDados := {{"E2_FILIAL"      ,XFILIAL("SE2",filial)          ,Nil    },;
           {"E2_PREFIXO"  , PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ")                  ,Nil    },;			
           {"E2_NUM"      ,PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ")          ,Nil    },;
           {"E2_PARCELA"      ,parcela           ,Nil    },;
           {"E2_TIPO"     ,IIF(EMPTY(tipo),ALLTRIM(GETMV("MV_YTIWEBP")), PADR(tipo, GETSX3CACHE("E2_TIPO","X3_TAMANHO"), " ") )                 ,Nil    },;
           {"AUTMOTBX"    ,"NOR"                  ,Nil    },;
           {"AUTBANCO"    ,banco                 ,Nil    },;
           {"AUTAGENCIA"  ,agencia                ,Nil    },;
           {"AUTCONTA"    ,conta           ,Nil    },;
           {"AUTDTBAIXA"  ,stod(databaixa)              ,Nil    },;
           {"AUTDTCREDITO",stod(databaixa)              ,Nil    },;
           {"AUTHIST"     ,"Ref. Tarifa nome operadora - Bandeira - modalidade de pagamento."          ,Nil    },;
           {"AUTJUROS"    ,0                      ,Nil,.T.},;
           {"AUTVALREC"   ,valor                   ,Nil    }}     
            		
	MSExecAuto({|x,y| Fina080(x,y)},aDados,3) 
	
	If lMsErroAuto
	
		AADD(Self:aError,cNum)
	 	AADD(Self:aError,SE2->E2_PARCELA)
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE2->E2_NUM)
	 	AADD(Self:aError,SE2->E2_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Baixa realizada com sucesso.")
	
	EndIf
	
	conout(Self:aError[4])
	
	RestArea(aArea)
	
return .T.

WsMethod CanBaixaPag wsReceive filial,cNum,parcela,tipo,recnoBanco,dataBaixa,valor wsSend aError wsService FinSASWEB

	aDados := {}
	aArea := GEtArea()
	lMsErroAuto := .F.
	cError := ""
	banco := ""
	agencia := ""
	conta := ""	
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(Substring(filial,1,2),alltrim(filial),,,,GetEnvServer(),{"SX3","SX6","SE2","SIX","SA6"})
	
	cFilAnt := filial
	
	DBSELECTAREA("SA6")
	DBGOTO(recnoBanco)
	
	banco := SA6->A6_COD
	agencia := SA6->A6_AGENCIA
	conta := SA6->A6_NUMCON
	
	DBSELECTAREA("SE2")
	DBSETORDER(1)
	DBSEEK(XFILIAL("SE2",filial) + PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ") + PADR(cNum,GETSX3CACHE("E2_NUM","X3_TAMANHO"), " ") +  parcela + IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo) )
	
	aDados := {{"E2_FILIAL"      ,XFILIAL("SE2",filial)          ,Nil    },;
           {"E2_PREFIXO"  , PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ")                  ,Nil    },;			
           {"E2_NUM"      ,PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ")          ,Nil    },;
           {"E2_PARCELA"      ,parcela           ,Nil    },;
           {"E2_TIPO"     ,IIF(EMPTY(tipo),GETMV("MV_YTIWEBP"),tipo)                 ,Nil    },;
           {"AUTMOTBX"    ,"NOR"                  ,Nil    },;
           {"AUTBANCO"    ,banco                 ,Nil    },;
           {"AUTAGENCIA"  ,agencia                ,Nil    },;
           {"AUTCONTA"    ,conta           ,Nil    },;
           {"AUTDTBAIXA"  ,stod(databaixa)              ,Nil    },;
           {"AUTDTCREDITO",stod(databaixa)              ,Nil    },;
           {"AUTHIST"     ,"BAIXA DO ECOMERCE"          ,Nil    },;
           {"AUTJUROS"    ,0                      ,Nil,.T.},;
           {"AUTVALREC"   ,valor                   ,Nil    }}     
            		
	MSExecAuto({|x,y| Fina080(x,y)},aDados,5) 
	
	If lMsErroAuto
	
		AADD(Self:aError,cNum)
	 	AADD(Self:aError,SE2->E2_PARCELA)
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE2->E2_NUM)
	 	AADD(Self:aError,SE2->E2_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Cancelamento da baixa com sucesso.")
	
	EndIf
	
	conout(Self:aError[4])
	REstArea(aArea)
	
return .T.

WsMethod ExcBaixaPag wsReceive filial,cNum,parcela,tipo,recnoBanco,dataBaixa,valor wsSend aError wsService FinSASWEB

	aDados := {}
	lMsErroAuto := .F.
	cError := ""
	banco := ""
	agencia := ""
	conta := ""
	aArea := GEtArea()	
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(Substring(filial,1,2),alltrim(filial),,,,GetEnvServer(),{"SX3","SX6","SE2","SIX","SA6"}) //EMPRESA D1
	
	cFilAnt := filial
	
	DBSELECTAREA("SA6")
	DBGOTO(recnoBanco)
	
	banco := SA6->A6_COD
	agencia := SA6->A6_AGENCIA
	conta := SA6->A6_NUMCON
	
	DBSELECTAREA("SE2")
	DBSETORDER(1)
	DBSEEK(XFILIAL("SE2",filial) + PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ") + PADR(cNum,GETSX3CACHE("E2_NUM","X3_TAMANHO"), " ") +  parcela + IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo) )
	
	aDados := {{"E2_FILIAL"      ,XFILIAL("SE2",filial)          ,Nil    },;
           {"E2_PREFIXO"  , PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E2_PREFIXO","X3_TAMANHO"), " ")                  ,Nil    },;			
           {"E2_NUM"      ,PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ")           ,Nil    },;
           {"E2_PARCELA"      ,parcela           ,Nil    },;
           {"E2_TIPO"     ,IIF(EMPTY(tipo),GETMV("MV_YTIWEBP"),tipo)                 ,Nil    },;
           {"AUTMOTBX"    ,"NOR"                  ,Nil    },;
           {"AUTBANCO"    ,banco                 ,Nil    },;
           {"AUTAGENCIA"  ,agencia                ,Nil    },;
           {"AUTCONTA"    ,conta           ,Nil    },;
           {"AUTDTBAIXA"  ,dataBaixa              ,Nil    },;
           {"AUTDTCREDITO",dataBaixa              ,Nil    },;
           {"AUTHIST"     ,"BAIXA DO ECOMERCE"          ,Nil    },;
           {"AUTJUROS"    ,0                      ,Nil,.T.},;
           {"AUTVALREC"   ,valor                   ,Nil    }}     
            		
	MSExecAuto({|x,y| Fina080(x,y)},aDados,6) 
	
	If lMsErroAuto
	
		AADD(Self:aError,cNum)
	 	AADD(Self:aError,SE2->E2_PARCELA)
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE2->E2_NUM)
	 	AADD(Self:aError,SE2->E2_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Exclus�o da baixa com sucesso.")
	
	EndIf
	
	conout(Self:aError[4])
	RestArea(aArea)
	
return .T.


WsMethod BaixaTitCheque  wsReceive filial,cNum,parcela,tipo,recnoBanco, dataBaixa, valor, bancochq, agenciachq, contachq, cheque, venctochq wsSend aError wsService FinSASWEB

	aBaixa := {}
	lMsErroAuto := .F.
	cError := ""	
	aColsSEF:= {}
	aGRV:= {}
	banco := ""
	agencia := ""
	conta := ""
	//aArea := GEtArea()
		
	WSDLDBGLEVEL(2)
	RPCSetType(3)
	RpcSetEnv(Substring(filial,1,2),alltrim(filial),,,,GetEnvServer(),{"SX3","SX6","SE1","SIX","SEF","SA6"}) //EMPRESA D1
	
	cFilAnt := filial	
	
	
	
	/*aDados := {{"E1_FILIAL"      ,XFILIAL("SE1",filial)          ,Nil    },;
           {"E1_PREFIXO"  , PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E1_PREFIXO","X3_TAMANHO"), " ")                  ,Nil    },;			
           {"E1_NUM"      ,cNum           ,Nil    },;
           {"E1_PARCELA"      ,parcela           ,Nil    },;
           {"E1_TIPO"     ,IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo)                 ,Nil    },;
           {"AUTMOTBX"    ,"NOR"                  ,Nil    },;
           {"AUTBANCO"    ,banco                 ,Nil    },;
           {"AUTAGENCIA"  ,agencia                ,Nil    },;
           {"AUTCONTA"    ,conta           ,Nil    },;
           {"AUTDTBAIXA"  ,dDataBase              ,Nil    },;
           {"AUTDTCREDITO",dDataBase              ,Nil    },;
           {"AUTHIST"     ,"BAIXA DO ECOMERCE"          ,Nil    },;
           {"AUTJUROS"    ,0                      ,Nil,.T.},;
           {"AUTCHEQUE"    ,""                    ,Nil,.T.},;
           {"AUTVALREC"   ,valor                   ,Nil    }}*/
           
   DBSELECTAREA("SE1")
	DBSETORDER(1)
	DBSEEK(XFILIAL("SE1",filial) + PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E1_PREFIXO","X3_TAMANHO"), " ") + PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ") +  parcela + IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo) )
           
     AADD( aBaixa, { "E1_PREFIXO" 	, PADR(GETMV("MV_YPREWEB"),GETSX3CACHE("E1_PREFIXO","X3_TAMANHO"))		, Nil } )	// 01
			AADD( aBaixa, { "E1_NUM"     	, PADR(cNum,GETSX3CACHE("E1_NUM","X3_TAMANHO"), " ") 		 	, Nil } )	// 02
			AADD( aBaixa, { "E1_PARCELA" 	, parcela		, Nil } )	// 03
			AADD( aBaixa, { "E1_TIPO"    	,IIF(EMPTY(tipo),GETMV("MV_YTIWEBR"),tipo)			, Nil } )	// 04
			AADD( aBaixa, { "E1_CLIENTE"	, SE1->E1_CLIENTE		, Nil } )	// 05
			AADD( aBaixa, { "E1_LOJA"    	, SE1->E1_LOJA			, Nil } )	// 06
			AADD( aBaixa, { "E1_VALOR"    	, SE1->E1_VALOR				, Nil } )	// 06
			AADD( aBaixa, { "AUTMOTBX"  	, "NOR"					, Nil } )	// 07
			AADD( aBaixa, { "AUTBANCO"  	, banco					, Nil } )	// 08
			AADD( aBaixa, { "AUTAGENCIA"  	, agencia					, Nil } )	// 09
			AADD( aBaixa, { "AUTCONTA"  	, conta					, Nil } )	// 10
			AADD( aBaixa, { "AUTDTBAIXA"	, STOD(dataBaixa)	, Nil } )	// 11
			AADD( aBaixa, { "AUTHIST"   	, "BAIXA DO ECOMERCE"				, Nil } )	// 12 
			AADD( aBaixa, { "AUTDESCONT" 	, 0						, Nil } )	// 13
			AADD( aBaixa, { "AUTMULTA"	 	, 0						, Nil } )	// 14
			AADD( aBaixa, { "AUTJUROS" 		, 0						, Nil } )	// 15
			AADD( aBaixa, { "AUTOUTGAS" 	, 0						, Nil } )	// 16
			AADD( aBaixa, { "AUTVLRPG"  	, 0        				, Nil } )	// 17
			AADD( aBaixa, { "AUTVLRME"  	, 0						, Nil } )	// 18
			AADD( aBaixa, { "AUTCHEQUE"  	, ""					, Nil } )	// 19     
            		
	MSExecAuto({|x,y| Fina070(x,y)},aBaixa,3) 
	
	GravaChqCR("01","FINA070",aColsSEF)
	
	If lMsErroAuto
	
		AADD(Self:aError,cNum)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"2")
	 	AADD(Self:aError,MostraErro())
		
		lMsErroAuto := .F.
	 	
	Else	 
		
		AADD(Self:aError,SE1->E1_NUM)
	 	AADD(Self:aError,SE1->E1_PARCELA)
	 	AADD(Self:aError,"1")
	 	AADD(Self:aError,"Baixa realizada com sucesso.")
	 	
	 	RECLOCK("SEF",.T.)
	 	
	 	SEF->EF_FILIAL := XFILIAL("SEF") 
	 	SEF->EF_BANCO		:= bancochq
		SEF->EF_AGENCIA	:= agenciachq
		SEF->EF_CONTA		:= contachq
		SEF->EF_NUM		:= cheque
		SEF->EF_VALOR		:= valor
		SEF->EF_VALORBX	:= valor
  		SEF->EF_EMITENT	:= POSICIONE("SA1",1,XFILIAL("SA1") + SE1->E1_CLIENTE + SE1->E1_LOJA, "A1_NOME" )
  		SEF->EF_DATA		:= dDataBase
  		SEF->EF_VENCTO		:= stod(venctoChq)
  		SEF->EF_CPFCNPJ	:= POSICIONE("SA1",1,XFILIAL("SA1") + SE1->E1_CLIENTE + SE1->E1_LOJA, "A1_CGC" )
  		SEF->EF_TEL			:=  POSICIONE("SA1",1,XFILIAL("SA1") + SE1->E1_CLIENTE + SE1->E1_LOJA, "A1_TEL" )
  		SEF->EF_HIST		:= ""
  		SEF->EF_CLIENTE	:= SE1->E1_CLIENTE
		SEF->EF_LOJACLI	:= SE1->E1_LOJA
  		SEF->EF_PREFIXO	:= SE1->E1_PREFIXO
		SEF->EF_TITULO		:= SE1->E1_NUM
		SEF->EF_PARCELA	:= SE1->E1_PARCELA
		SEF->EF_TIPO		:= SE1->E1_TIPO
		SEF->EF_CART		:= "R"
		SEF->EF_ORIGEM		:= "FINA070"	
		SEF->EF_USADOBX := "S" 
		
		MSUNLOCK()	
	
	EndIf
	
	conout(Self:aError[4])
	//RestArea(aArea)
	
return .T.