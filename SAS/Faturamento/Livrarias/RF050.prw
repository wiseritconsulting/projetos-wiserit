#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"
#Include "RWMAKE.CH"
#include 'parmtype.ch'
#INCLUDE "TBICONN.CH"
#INCLUDE "TBICODE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} RF050
//TODO Tela PCE para Pedidos Intercompany
@author Diogo
@since 03/11/2016
@version undefined
@example
(examples)
@see (links_or_references)
/*/
User Function RF050()

	Local aArea		 := GetArea()
	Local aCoors	 := FwGetDialogSize( oMainWnd )
	Local oFWLayer,oPanelTop1,oView
	Local cOk        := "B"
	Local aFiltros   := {}
	Local bBlockClique
	Private oBrowse
	Private aPedidos := {}

	Static nFiliala  	:= 1
	Static nPed    		:= 2
	Static nLocEx  		:= 3
	Static nDLocEx 		:= 4
	Static nNF    		:= 5
	Static nSerie  		:= 6
	Static nCodClia  	:= 7
	Static nljClia   	:= 8
	Static nNomClia   	:= 9
	Static nDtGera   	:= 10
	Static nStatusa  	:= 11
	Static nobsa     	:= 12
	Static nFilPC   	:= 13
	Static nNumPC   	:= 14
	Static nNFPC 	  	:= 15
	Static nEtapa   	:= 16
	Static nImpres   	:= 17
	Static nTipo   		:= 18
	Static nUF   		:= 19
	Static nMun   		:= 20
	Static nOk       	:= 21

	Private oDlg
	Private aRotina	:= {}
	bBlockClique := {|| Clique2() }

	aPedidos := U_RF50iTEM(aPedidos)

	oDlg		:= MSDialog():New( aCoors[1],aCoors[2],aCoors[3],aCoors[4],"Planejamento e Controle de Expedi��o",,,.F.,,,,,,.T.,,,.T. )
	oFWLayer	:= FWLayer():New()
	oFWLayer:Init( oDlg, .F., .T. )
	oFWLayer:AddLine( 'TOP1', 100, .F. )
	oPanelTop1 := oFWLayer:getLinePanel( 'TOP1' )

	oBrowse	    := FWFormBrowse():New()
	oMarkBrow	:= oBrowse:FWBrowse()
	oMarkBrow:SetDataArray()
	oMarkBrow:SetArray(@aPedidos)
	oMarkBrow:SetDescription('Pedidos Intercompany')
	oMarkBrow:SetOwner( oDlg )
	oMarkBrow:SetProfileID("4")
	oMarkBrow:SetMenuDef("")
	oMarkBrow:SetDoubleClick({|| nil })
	bBlockClique	:= {|| U_CLICK50() }

	// bot�es

	oMarkBrow:AddButton("Atualizar",{||YATUALIZ(aPedidos,oMarkBrow)},,,)
	oMarkBrow:AddButton("Alterar Pedido ",{||YALTPED()},,,)
	oMarkBrow:AddButton("Vis. Pedido Venda",{||YVISPV()},,,)
	oMarkBrow:AddButton("Vis. Pedido Compra",{||YVISPC()},,,)

	oMarkBrow:AddButton("Confirmar ",{||YCONFIRMA(aPedidos,oMarkBrow,1)},,,)
	
	oMarkBrow:AddButton("Impressao OS ",{||YIMPRIMIR(aPedidos,oMarkBrow)},,,)
	//oMarkBrow:AddButton("Placa de Embarque ",{||YPEMBARQUE(aPedidos,oMarkBrow)},,,)
	oMarkBrow:AddButton("Impressao OS Cega ",{||YIMPCEGA(aPedidos,oMarkBrow)},,,)
	oMarkBrow:AddButton("Historico ",{||YHISTOR(aPedidos,oMarkBrow)},,,)

	// FILTROS
	oBrowse:SetUseFilter()

	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nFiliala)+"]",GetSx3Cache("C5_FILIAL","X3_TITULO"),"C",GetSx3Cache("C5_FILIAL","X3_TAMANHO"),0,GetSx3Cache("C5_FILIAL","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nPed)+"]",  GetSx3Cache("C5_NUM","X3_TITULO"),"C",GetSx3Cache("C5_NUM","X3_TAMANHO"),0,GetSx3Cache("C5_NUM","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nCodClia)+"]",GetSx3Cache("C5_CLIENTE","X3_TITULO"),"C",GetSx3Cache("C5_CLIENTE","X3_TAMANHO"),0,GetSx3Cache("C5_CLIENTE","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nljClia)+"]", GetSx3Cache("C5_LOJACLI","X3_TITULO"),"C",GetSx3Cache("C5_LOJACLI","X3_TAMANHO"),0,GetSx3Cache("C5_LOJACLI","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nNomClia)+"]", GetSx3Cache("A1_NOME","X3_TITULO"),"C",GetSx3Cache("A1_NOME","X3_TAMANHO"),0,GetSx3Cache("C5_LOJACLI","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nDtGera)+"]", GetSx3Cache("C5_EMISSAO","X3_TITULO"),"D",GetSx3Cache("C5_EMISSAO","X3_TAMANHO"),0,GetSx3Cache("C5_EMISSAO","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nStatusa)+"]",GetSx3Cache("C5_YSTOS","X3_TITULO"),"C",GetSx3Cache("C5_YSTOS","X3_TAMANHO"),0,GetSx3Cache("C5_YSTOS","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nobsa)+"]",   GetSx3Cache("C5_YOBOS","X3_TITULO"),"C",GetSx3Cache("C5_YOBOS","X3_TAMANHO"),0,GetSx3Cache("C5_YOBOS","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nFilPC)+"]", GetSx3Cache("C5_YFILPCI","X3_TITULO"),"C",GetSx3Cache("C5_YFILPCI","X3_TAMANHO"),0,GetSx3Cache("C5_YFILPCI","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nNumPC)+"]",GetSx3Cache("C5_YNUMIC","X3_TITULO"),"C",GetSx3Cache("C5_YNUMIC","X3_TAMANHO"),0,GetSx3Cache("C5_YNUMIC","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nEtapa)+"]",  GetSx3Cache("C5_YETAPA","X3_TITULO"),"C",GetSx3Cache("C5_YETAPA","X3_TAMANHO"),0,GetSx3Cache("C5_YETAPA","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nImpres)+"]", GetSx3Cache("C5_YIMPRES","X3_TITULO"),"C",GetSx3Cache("C5_YIMPRES","X3_TAMANHO"),0,GetSx3Cache("C5_YIMPRES","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nTipo)+"]", GetSx3Cache("C5_YPEDTV","X3_TITULO"),"C",GetSx3Cache("C5_YPEDTV","X3_TAMANHO"),0,GetSx3Cache("C5_YPEDTV","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nUF)+"]", GetSx3Cache("A1_EST","X3_TITULO"),"C",GetSx3Cache("A1_EST","X3_TAMANHO"),0,GetSx3Cache("A1_EST","X3_PICTURE")} )
	Aadd( aFiltros, { "aPedidos[oBrowse:At()]["+cvaltochar(nMun)+"]", GetSx3Cache("A1_MUN","X3_TITULO"),"C",GetSx3Cache("A1_MUN","X3_TAMANHO"),0,GetSx3Cache("A1_MUN","X3_PICTURE")} )
	
	oBrowse:SetFieldFilter(aFiltros)

	// legenda
	oMarkBrow:AddLegend({||aPedidos[oMarkBrow:At()][nTipo]=="Transferencia"}   	,"BR_VERMELHO"	,	"Transfer�ncia")
	oMarkBrow:AddLegend({||aPedidos[oMarkBrow:At()][nTipo]=="Manifesto"}   		,"BR_AMARELO"	,	"Manifesto")
	oMarkBrow:AddLegend({||aPedidos[oMarkBrow:At()][nTipo]=="Venda"}  			,"BR_VERDE"		,	"Venda")


	// BOX
	oMarkBrow:AddMarkColumns(;
	{||If(aPedidos[oMarkBrow:At()][nOk]=="","LBNO","LBTICK")};	//Imagem da marca
	,bBlockClique/*{|| YINVERTE(oMarkBrow)}*/;
	,{|| YINVERTE(oMarkBrow)})


	//filial
	oMarkBrow:SetColumns({{;
	"Filial",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nFiliala],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_FILIAL","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C5_FILIAL","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//Pedido
	oMarkBrow:SetColumns({{;
	"Pedido",;  					   				// T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nPed],"")},;		// Code-Block de carga dos dados
	"C",;                       			// Tipo de dados
	"@!",;                       			// M�scara
	1,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_NUM","X3_TAMANHO"),;                       			// Tamanho
	GetSx3Cache("C5_NUM","X3_DECIMAL"),;                       			// Decimal
	.F.,;                                       			// Indica se permite a edi��o
	{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       			// Indica se exibe imagem
	bBlockClique,;                     			// Code-Block de execu��o do duplo clique
	NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			// Code-Block de execu��o do clique no header
	.F.,;                                       			// Indica se a coluna est� deletada
	.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//Local Expedi��o
	oMarkBrow:SetColumns({{;
	"Cod Expedi��o",;  					   				// T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nlocEX],"")},;		// Code-Block de carga dos dados
	"C",;                       			// Tipo de dados
	"@!",;                       			// M�scara
	1,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_YLOCEX","X3_TAMANHO"),;                       			// Tamanho
	GetSx3Cache("C5_YLOCEX","X3_DECIMAL"),;                       			// Decimal
	.F.,;                                       			// Indica se permite a edi��o
	{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       			// Indica se exibe imagem
	bBlockClique,;                     			// Code-Block de execu��o do duplo clique
	NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			// Code-Block de execu��o do clique no header
	.F.,;                                       			// Indica se a coluna est� deletada
	.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//Desr. Local Expedi��o
	oMarkBrow:SetColumns({{;
	"Local Expedi��o",;  					   				// T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nDlocEX],"")},;		// Code-Block de carga dos dados
	"C",;                       			// Tipo de dados
	"@!",;                       			// M�scara
	1,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("X5_DESCRI","X3_TAMANHO"),;                       			// Tamanho
	GetSx3Cache("X5_DESCRI","X3_DECIMAL"),;                       			// Decimal
	.F.,;                                       			// Indica se permite a edi��o
	{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       			// Indica se exibe imagem
	bBlockClique,;                     			// Code-Block de execu��o do duplo clique
	NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			// Code-Block de execu��o do clique no header
	.F.,;                                       			// Indica se a coluna est� deletada
	.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	//Nota
	oMarkBrow:SetColumns({{;
	"Nota",;  					   				// T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nNF],"")},;		// Code-Block de carga dos dados
	"C",;                       			// Tipo de dados
	"@!",;                       			// M�scara
	1,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_NOTA","X3_TAMANHO"),;                       			// Tamanho
	GetSx3Cache("C5_NOTA","X3_DECIMAL"),;                       			// Decimal
	.F.,;                                       			// Indica se permite a edi��o
	{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       			// Indica se exibe imagem
	bBlockClique,;                     			// Code-Block de execu��o do duplo clique
	NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			// Code-Block de execu��o do clique no header
	.F.,;                                       			// Indica se a coluna est� deletada
	.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//S�rie
	oMarkBrow:SetColumns({{;
	"S�rie",;  					   				// T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nSerie],"")},;		// Code-Block de carga dos dados
	"C",;                       			// Tipo de dados
	"@!",;                       			// M�scara
	1,;					   				// Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_SERIE","X3_TAMANHO"),;                       			// Tamanho
	GetSx3Cache("C5_SERIE","X3_DECIMAL"),;                       			// Decimal
	.F.,;                                       			// Indica se permite a edi��o
	{||.T.},;                                   			// Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       			// Indica se exibe imagem
	bBlockClique,;                     			// Code-Block de execu��o do duplo clique
	NIL,;                                       			// Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			// Code-Block de execu��o do clique no header
	.F.,;                                       			// Indica se a coluna est� deletada
	.T.,;                     				   				// Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			// Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//cod cliente
	oMarkBrow:SetColumns({{;
	"Cod Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nCodClia],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_CLIENTE","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C5_CLIENTE","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	//Loja Cliente
	oMarkBrow:SetColumns({{;
	"Loja Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nLjClia],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_LOJACLI","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C5_LOJACLI","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	// NOME CLIENTE
	oMarkBrow:SetColumns({{;
	"Cliente",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nNomClia],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A1_NOME","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("A1_NOME","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Estado do cliente
	oMarkBrow:SetColumns({{;
	"Estado",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),posicione("SA1",1,xFilial("SA1") + aPedidos[oMarkBrow:At()][nCodClia] + aPedidos[oMarkBrow:At()][nLjClia],"A1_EST"),"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A1_EST","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("A1_EST","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Municipio do cliente
	oMarkBrow:SetColumns({{;
	"Municipio",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),posicione("SA1",1,xFilial("SA1") + aPedidos[oMarkBrow:At()][nCodClia] + aPedidos[oMarkBrow:At()][nLjClia],"A1_MUN"),"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A1_MUN","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("A1_MUN","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	//DATA GERA��O DA MEDICAO
	oMarkBrow:SetColumns({{;
	"Emiss�o",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nDtGera],"")},;  // Code-Block de carga dos dados
	"D",;                                                        	   // Tipo de dados
	"",;                       			                               // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_EMISSAO","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C5_EMISSAO","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	//Status
	oMarkBrow:SetColumns({{;
	"Status OS",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nStatusa],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_YSTOS","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C5_YSTOS","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)	
	
	// TIPO
	oMarkBrow:SetColumns({{;
	"Tipo ",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nTipo],"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	10,; 									                      	   // Tamanho
	GetSx3Cache("C5_YPEDTV","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	
	// obs
	oMarkBrow:SetColumns({{;
	"Obs",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nobsa],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_YOBOS","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C5_YOBOS","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	oMarkBrow:SetColumns({{;
	"Filial Ped Compra",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos), aPedidos[oMarkBrow:At()][nFilPC]+" - "+POSICIONE("SX5",1,XFILIAL("SX5")+"X2"+aPedidos[oMarkBrow:At()][nFilPC],"X5_DESCRI") ,"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	40,;                       	   										// Tamanho
	GetSx3Cache("C5_YFILPCI","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Numero do Pedido de Compras
	oMarkBrow:SetColumns({{;
	"Pedido Compras",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nNumPC],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_YNUMIC","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C5_YNUMIC","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Numero Nota Entrada
	oMarkBrow:SetColumns({{;
	"Nota Entrada",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nNFPC],"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_NOTA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C5_NOTA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)


	// ETAPA
	oMarkBrow:SetColumns({{;
	"Etapa ",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nEtapa],"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	35,;                       	   // Tamanho
	GetSx3Cache("C5_YETAPA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

	// Impressa ?
	oMarkBrow:SetColumns({{;
	"Impressa ",;			  					   			               // T�tulo da coluna
	{|| If(!Empty(aPedidos),aPedidos[oMarkBrow:At()][nImpres],"")},;   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("C5_YETAPA","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("C5_YETAPA","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)

		
	oMarkBrow:Activate()
	oDlg:Activate(,,,.T.)
	RestArea(aArea)

	//Reset Environment
	aPedidos := {}
Return

User Function RF50ITEM(aPedidos)

	Local cArea   := GetArea()
	Local cStatus := ""
	Local cFilAcesso := ""
	Local cNomeUser
	Local aRetUser := {}
	Local cDepto
	
	cQuery := " SELECT  "
	cQuery += "	C5_FILIAL, C5_NUM, C5_YLOCEX, C5_NOTA, C5_SERIE, C5_CLIENTE,C5_LOJACLI,C5_EMISSAO,C5_YSTOS,C5_YOBOS, " 
	cQuery += "	C5_YFILPCI,C5_YNUMIC,C5_YETAPA,C5_YIMPRES,C5_YPEDTV,A1_NOME,A1_EST,A1_MUN "
	cQuery += " FROM "+RetSqlName("SC5")+" SC5 "
	cQuery += " JOIN "+RetSqlName("SA1")+" SA1 "
	cQuery += " ON C5_CLIENTE+C5_LOJACLI = A1_COD+A1_LOJA "
	cQuery += " WHERE SC5.D_E_L_E_T_ = ' ' "
	cQuery += " AND SA1.D_E_L_E_T_ = ' ' "
	//cQuery += " AND C5_YPEDIDO = 'I' " //Intercompany
	//cQuery += " AND C5_YEXPED = ' ' " //Intercompany
	cQuery += " AND C5_YPEDTV <>  ' ' "
	cQuery += " ORDER BY C5_FILIAL, C5_NUM "
	
	TcQuery cQuery New Alias T01

	WHILE !T01->(EOF())
		
		cTPed:= ''
		If ALLTRIM(T01->C5_YPEDTV) == 'T'
			cTPed:= 'Transferencia'
		Elseif ALLTRIM(T01->C5_YPEDTV) == 'M'
			cTPed:= 'Manifesto'
		Elseif ALLTRIM(T01->C5_YPEDTV) == 'V'
			cTPed:= 'Venda'
		Endif
		
		cNfEntr:= ""
		
		If !empty(T01->C5_YNUMIC)
			cQuery:= "SELECT TOP 1 D1_DOC FROM "+RetSqlName("SD1")+ " SD1 "
			cQuery+= "WHERE D_E_L_E_T_ = ' ' AND "
			cQuery+= "D1_FILIAL = '"+T01->C5_YFILPCI+"' AND " 
			cQuery+= "D1_PEDIDO = '"+T01->C5_YNUMIC+"'  "
			TcQuery cQuery new Alias TSD1
			
			cNfEntr:= TSD1->D1_DOC
				
			TSD1->(dbCloseArea()) 
		Endif
		
		aadd(aPedidos,{ALLTRIM(T01->C5_FILIAL),;
		ALLTRIM(T01->C5_NUM),;
		ALLTRIM(T01->C5_YLOCEX),;
		iif(empty(T01->C5_YLOCEX)," ",Posicione("SX5",1,xFilial("SX5")+"X2"+ALLTRIM(T01->C5_YLOCEX),"X5_DESCRI" )),;
		ALLTRIM(T01->C5_NOTA),;
		ALLTRIM(T01->C5_SERIE),;
		ALLTRIM(T01->C5_CLIENTE),;
		ALLTRIM(T01->C5_LOJACLI),;
		ALLTRIM(T01->A1_NOME ) 	,;
		STOD(ALLTRIM(T01->C5_EMISSAO)) ,;
		ALLTRIM(T01->C5_YSTOS)	,;
		ALLTRIM(T01->C5_YOBOS)	,;
		iif(empty(T01->C5_YFILPCI)," ",Alltrim(T01->C5_YFILPCI)+"-"+FWFilName(cEmpAnt,T01->C5_YFILPCI)) ,;
		ALLTRIM(T01->C5_YNUMIC)	,;
		cNfEntr					,;
		ALLTRIM(T01->C5_YETAPA)+iif(!empty(ALLTRIM(T01->C5_YETAPA))," - "+Posicione("SX5",1,xFilial("SX5")+"ZA"+ALLTRIM(T01->C5_YETAPA),"X5_DESCRI"),"" )	,;
		ALLTRIM(T01->C5_YIMPRES),;
		cTPed					,;
		ALLTRIM(T01->A1_EST)	,;
		ALLTRIM(T01->A1_MUN)	,;
		"" })
		T01->(dbSkip())

	ENDDO

	T01-> (dbclosearea())

Return aPedidos

USER FUNCTION CLICK50()

	IF(aPedidos[oMarkBrow:At()][nOk]=="",aPedidos[oMarkBrow:At()][nOk]:= "1",aPedidos[oMarkBrow:At()][nOk] := "")

RETURN


STATIC FUNCTION YATUALIZ(aPedidos,oMarkBrow)

	aPedidos:= {}
	U_RF50ITEM(aPedidos)
	oMarkBrow:SetArray(@aPedidos)
	oMarkBrow:refresh()
	oBrowse:refresh()
	oMarkBrow:goto(1,.T.)


Return



// Fun��o para confirmar as medi��es selecionadas
//  - medi��es bloqueadas n�o podaram ser confirmadas
//  - medi��es com cliente bloqueado n�o pode ser confirmada
//  - medi��es com estoque indispon�vel n�o pode ser confirmadas
STATIC FUNCTION YCONFIRMA(aPedidos,oMarkBrow,cnum)

	Local aConfirma := {}
	Local nAx := 1
	Local cArea := GetArea()
	Local aPergs	:= {}
	Local dDataExp := ctod("")
	Local lOk := .F.
	Local aRetParm	:= {}

	aAdd( aPergs ,{1,"Data Expedi��o     : " ,dDataExp	,""	,'.T.',""	,'.T.',50	,.F.})

	If ParamBox(aPergs,"Expedi��o",@aRetParm,{||.T.},,,,,,"U_RF050",.T.,.T.)
		lOk	:= .T.
	else
			RestArea( cArea )
			YDESMARCA()
			oMarkBrow:refresh()
		Return
	EndIf

	FOR nAx := 1 to LEN(aPedidos)
		if aPedidos[nAx][nOk] == "1"

			// verificar se est� bloqueada
			if "10" <> substr(aPedidos[nAx][nEtapa],1,2) //Etapa j� iniciada
				ALERT("A confirma��o � somente permitida pedidos sem inicializa��o no processo do PCE")
				YDESMARCA()
				oMarkBrow:refresh()
				RETURN

			ELSE
				aPedidos[nAx][nStatusa] := "OK"
				aPedidos[nAx][nEtapa] 	:= "20"+" - "+Posicione("SX5",1,xFilial("SX5")+"ZA"+"20","X5_DESCRI")

				aadd(aConfirma,{aPedidos[nAx][nFiliala],;
				aPedidos[nAx][nPed]  	,;
				aPedidos[nAx][nLocEx]  	,;
				aPedidos[nAx][nDLocEx] 	,;
				aPedidos[nAx][nNF]  	,;
				aPedidos[nAx][nSerie] 	,;
				aPedidos[nAx][nCodClia],;
				aPedidos[nAx][nljClia] ,;
				aPedidos[nAx][nNomClia],;
				aPedidos[nAx][nDtGera] ,;
				aPedidos[nAx][nStatusa],;
				aPedidos[nAx][nobsa]   ,;
				aPedidos[nAx][nNumPC],;
				aPedidos[nAx][nFilPC] ,;
				aPedidos[nAx][nOk]     ,;
				aPedidos[nAx][nEtapa]  ,;
				nAx})

			endif
		endif
	NEXT

	FOR nAx := 1 to len(aConfirma)

		//ATUALIZANDO O CADASTRO DE MEDI��O
		dbSelectArea("SC5")
		SC5->(dbSetOrder(1))
		SC5->(dbSeek(aConfirma[nAx][nFiliala] + aConfirma[nAx][nPed]))

		IF FOUND()    // Avalia o retorno da pesquisa realizada
			RECLOCK("SC5", .F.)

			If lOk
				SC5->C5_YDTEXP 	:= aRetParm[1]
				SC5->C5_YSTOS 	:= "OK"
			Else
				SC5->C5_YDTEXP	:= date()
			EndIf

				SC5->C5_YETAPA  := '20'
			MSUNLOCK()     // Destrava o registro
		ENDIF

		//ATUALIZANDO O CADASTRO DE HIST�RICO
		dbSelectArea("ZC3")
		dbSetOrder(1)      // ZC3_FILIAL+ZC3_PEDIDO+ZC3_ETAPA

		
		RECLOCK("ZC3", .T.)

		ZC3_FILIAL := SC5->C5_FILIAL
		ZC3_PEDIDO := SC5->C5_NUM
		ZC3_EMISSA := SC5->C5_EMISSAO
		ZC3_CODCLI := SC5->C5_CLIENTE
		ZC3_NOMECL := POSICIONE("SA1",1,xFilial("SA1")+SC5->C5_CLIENTE+SC5->C5_LOJACLI,"A1_NOME")
		ZC3_CODUSE := RetCodUsr()
		ZC3_NOMUSE := UsrFullName(RetCodUsr())
		ZC3_ETAPA  := '10'
		ZC3_NOMETP := POSICIONE("SX5",1,XFILIAL("SX5")+"ZA"+'10',"X5_DESCRI")
		ZC3_DATA   := date()
		ZC3_HORA   := time()
		ZC3_OBS    := "MEDICAO CONFIRMADA "

		MSUNLOCK()     // Destrava o registro
		
	next

	MsgInfo("Confirma��o realizada")
	
	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
RETURN



//Impress�o do Pedido de Vendas
STATIC FUNCTION YIMPRIMIR(aPedidos,oMarkBrow)

	Local aImprime  := {}
	Local nCont     := 0
	Local cArea     := GetArea()

	cFilPed	:=	aPedidos[oMarkBrow:At()][nFiliala] 
	cNum	:=	aPedidos[oMarkBrow:At()][nPed]	
	cCli	:=	aPedidos[oMarkBrow:At()][nCodClia] 
	cLoja	:=	aPedidos[oMarkBrow:At()][nljClia]
	 
	U_SASR099(cFilPed, cNum , cCli, cLoja)

	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
RETURN

//Impress�o do Pedido de Vendas - Conf. Cega
STATIC FUNCTION YIMPCEGA(aPedidos,oMarkBrow)

	Local aImprime  := {}
	Local nCont     := 0
	Local cArea     := GetArea()

	cFilPed	:=	aPedidos[oMarkBrow:At()][nFiliala] 
	cNum	:=	aPedidos[oMarkBrow:At()][nPed]	
	cCli	:=	aPedidos[oMarkBrow:At()][nCodClia] 
	cLoja	:=	aPedidos[oMarkBrow:At()][nljClia]
	 
	U_SASR097(cFilPed, cNum , cCli, cLoja)

	RestArea( cArea )
	YDESMARCA()
	oMarkBrow:refresh()
RETURN


//FUN��O PARA HIST�RICO DE OS
Static Function YHISTOR(aPedidos,oMarkBrow)

	Local nCont := 0
	Local aHist := {}
	Local oGet1
	Local cGet1 := "Define variable value"
	Local oGet2
	Local cGet2 := "Define variable value"
	Local oSay1
	Local oSay2
	Local oSButton1
	Static oDlg

	FOR nAx := 1 to LEN(aPedidos)

		if aPedidos[nAx][nOk] == "1"
			nCont ++
			
			aadd(aHist,{aPedidos[nAx][nFiliala],;
			aPedidos[nAx][nPed]  ,;
			aPedidos[nAx][nDtGera] ,;
			aPedidos[nAx][nCodClia],;
			aPedidos[nAx][nljClia] ,;
			aPedidos[nAx][nNomClia],;
			aPedidos[nAx][nobsa]   ,;
			aPedidos[nAx][nDtGera] ,; //Data 
			aPedidos[nAx][nDtGera] ,; //Hora
			aPedidos[nAx][nEtapa]  ,;
			aPedidos[nAx][nEtapa]  ,;
			aPedidos[nAx][nOk]     ,;
			nAx})
		endif
		if nCont > 1
			alert("Selecione apena uma medi��o para Alterar ")
			YDESMARCA()
			oMarkBrow:refresh()
			RETURN
		endif

	next
	if nCont == 0
		alert("Selecione uma medi��o para Alterar ")
		YDESMARCA()
		oMarkBrow:refresh()
		RETURN
	endif

	cGet1 := aHist[1][nFiliala]
	cGet2 := aHist[1][nPed]

	DEFINE MSDIALOG oDlg TITLE "Hist�rico de Medi��o" FROM 000, 000  TO 300, 500 COLORS 0, 16777215 PIXEL

	gridhistor(aHist)
	@ 017, 009 SAY oSay1 PROMPT "Filial:" SIZE 037, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 017, 045 MSGET oGet1 VAR cGet1 SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 017, 125 SAY oSay2 PROMPT "Pedido:" SIZE 037, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 017, 155 MSGET oGet2 VAR cGet2 SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	DEFINE SBUTTON oSButton1 FROM 132, 211 TYPE 01 OF oDlg ACTION  (oDlg:end()) ENABLE

	ACTIVATE MSDIALOG oDlg CENTERED

	YDESMARCA()
	oMarkBrow:refresh()
Return

//------------------------------------------------
Static Function gridhistor(aHist)
	//------------------------------------------------
	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {}
	Local aAlterFields := {}
	Static oMSNewGe1

	aadd (aFields,{"ZC3_FILIAL"})
	aadd (aFields,{"ZC3_PEDIDO"})
	aadd (aFields,{"ZC3_EMISSA"})
	aadd (aFields,{"ZC3_CODCLI"})
	aadd (aFields,{"ZC3_NOMECL"})
	aadd (aFields,{"ZC3_CODUSE"})
	aadd (aFields,{"ZC3_NOMUSE"})
	aadd (aFields,{"ZC3_OBS"   })
	aadd (aFields,{"ZC3_DATA"  })
	aadd (aFields,{"ZC3_HORA"  })
	aadd (aFields,{"ZC3_ETAPA"})
	aadd (aFields,{"ZC3_NOMETP"})

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX][1]))
			Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

	// Define field values
	For nX := 1 to Len(aFields)
		If DbSeek(aFields[nX][1])
			Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
		Endif
	Next nX
	Aadd(aFieldFill, .F.)
	//Aadd(aColsEx, aFieldFill)

	cQuery:= " SELECT ZC3_FILIAL , ZC3_PEDIDO , ZC3_EMISSA ,ZC3_CODCLI ,ZC3_NOMECL ,ZC3_CODUSE ,ZC3_NOMUSE ,ZC3_OBS ,ZC3_DATA ,ZC3_HORA ,ZC3_ETAPA , ZC3_NOMETP"
	cQuery+= " FROM "+ RETSQLNAME("ZC3") +" ZC3 "
	cQuery+= " WHERE ZC3_FILIAL = '"+aHist[1][nFiliala]+"' "
	cQuery+= " AND   ZC3_PEDIDO = '"+aHist[1][nPed]+"' "
	cQuery+= " AND   D_E_L_E_T_ = '' "


	TCQuery cquery new alias T01

	WHILE !T01->(EOF())
		aadd(aColsEx,{T01->ZC3_FILIAL,;
		T01->ZC3_PEDIDO,;
		T01->ZC3_EMISSA,;
		T01->ZC3_CODCLI,;
		T01->ZC3_NOMECL,;
		T01->ZC3_CODUSE,;
		T01->ZC3_NOMUSE,;
		T01->ZC3_OBS,;
		dtoc(stod(T01->ZC3_DATA)),;
		T01->ZC3_HORA,;
		T01->ZC3_ETAPA,;
		T01->ZC3_NOMETP,;
		.F.})
		

		T01->(dbSkip())
	ENDDO
	T01->(DBCLOSEAREA())
	oMSNewGe1 := MsNewGetDados():New( 042, 003, 114, 244, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return


Static function valid23()

	Local lRet := .F.

return lRet

// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO DESMARCADO
STATIC FUNCTION YDESMARCA()

	FOR nAx := 1 to len(aPedidos)
		aPedidos[nAx][nOK] := ""
	NEXT


RETURN

// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO MARCADO
STATIC FUNCTION YMARCA()

	FOR nAx := 1 to len(aPedidos)
		aPedidos[nAx][nOK] := "1"
	NEXT


RETURN

// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO MARCADO
STATIC FUNCTION YINVERTE(oMarkBrow)

	FOR nAx := 1 to len(aPedidos)
		IF aPedidos[nAx][nOK] == "1"
			aPedidos[nAx][nOK] := ""
		ELSE
			aPedidos[nAx][nOK] := "1"
		ENDIF
	NEXT

	oMarkBrow:refresh()
RETURN

Static Function GetValor(cFilPV, cPedVen)

	Local cQuery := ""
	Local nValTotal := 0

	cQuery := ""
	cQuery += " SELECT SUM(C6_VALOR) AS TOTAL "
	cQuery += "   FROM " + RETSQLNAME("SC6") + " SC6 "
	cQuery += "  WHERE SC6.D_E_L_E_T_ = '' "
	cQuery += "    AND SC6.C6_FILIAL = '" + XFILIAL("SC6",cFilPV) + "' "
	cQuery += "    AND SC6.C6_NUM ='" + cPedVen + "' "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XC6",.F.,.T.)

	nValTotal := Round(XC6->TOTAL,2)

	nValTotal := (nValTotal + POSICIONE("SC5",1,XFILIAL("SC5",cFilPV) + cPedVen,"C5_FRETE") ) - POSICIONE("SC5",1,XFILIAL("SC5",cFilPV) + cPedVen,"C5_DESCONT")

	XC6->(DBCLOSEAREA())

Return nValTotal




Static Function YALTPED
	cCadastro	:= "Altera��o de pedido"
	inclui		:= .F.
	altera		:= .T.		
	lBloqueio	:= .F.
	aRotina 	:= {}
	cFilBkp		:= cFilAnt 
			
	cFilAnt:= aPedidos[oMarkBrow:At()][nFiliala]
	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(aPedidos[oMarkBrow:At()][nFiliala]+aPedidos[oMarkBrow:At()][nPed]))
	
	If empty(SC5->C5_NOTA)
		MatA410(Nil, Nil, Nil, Nil, "A410Altera")
	Else
		MsgInfo("Pedido n�o poder� ser alterado pois j� foi gerado Nota Fiscal")
	Endif
	
	cFilAnt:= cFilBkp
		 
Return

//Visualizar Pedido de Vendas
Static Function YVISPV
	Local aItem:= {}
	dbSelectArea("SC5")

	cFilBkp		:= cFilAnt 
			
	cFilAnt:= aPedidos[oMarkBrow:At()][nFiliala]
	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(aPedidos[oMarkBrow:At()][nFiliala]+aPedidos[oMarkBrow:At()][nPed]))
	
	aCab:= {	{"C5_TIPO"		,SC5->C5_TIPO		,NIL};
	,{"C5_NUM"		,SC5->C5_NUM		,NIL};
	,{"C5_CLIENTE"	,SC5->C5_CLIENTE	,NIL};
	,{"C5_CLIENT"	,SC5->C5_CLIENT		,NIL};
	,{"C5_LOJACLI"	,SC5->C5_LOJACLI	,NIL};
	,{"C5_CONDPAG"	,SC5->C5_CONDPAG	,NIL};
	,{"C5_EMISSAO"  ,SC5->C5_EMISSAO	,NIL}}


	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(DbSeek(SC5->C5_FILIAL+SC5->C5_NUM))

		While SC6->(!Eof()) .AND. SC6->C6_FILIAL == SC5->C5_FILIAL .AND. SC6->C6_NUM == SC5->C5_NUM
			aAdd(aItem,{ {"C6_PRODUTO"	,SC6->C6_PRODUTO,NIL};
			,{"C6_ITEM"		,SC6->C6_ITEM   ,NIL};
			,{"C6_QTDVEN"	,SC6->C6_QTDVEN ,NIL};
			,{"C6_PRCVEN"	,SC6->C6_PRCVEN ,NIL};
			,{"C6_VALOR"	,SC6->C6_VALOR	,NIL};
			,{"C6_TES"		,SC6->C6_TES	,NIL};
			,{"C6_CLI"		,SC6->C6_CLI	,NIL};
			,{"C6_QTDLIB"	,SC6->C6_QTDVEN	,NIL};
			,{"C6_LOJA"		,SC6->C6_LOJA	,NIL};
			,{"C6_LOCAL"	,SC6->C6_LOCAL  ,NIL}})

			SC6->(DbSkip())
		EndDo

		lMsErroAuto := .F.
		
		MSExecAuto({|x,y,z|Mata410(x,y,z)},aCab,aItem,2) 

	cFilAnt:= cFilBkp

Return

//Visualizar Pedido de Compras
Static Function YVISPC
	Local aCabPC	:= {}
	Local aItensPC	:= {}
	Local cFilB		:= cFilAnt	
	
	If empty(aPedidos[oMarkBrow:At()][nNumPC])
		MsgInfo("Pedido de Compras n�o gerado")
		Return
	Endif
	
	dbSelectArea("SC7")
	SC7->(dbSetOrder(1))
	If SC7->(dbSeek(substr(aPedidos[oMarkBrow:At()][nFilPC],1,6)+aPedidos[oMarkBrow:At()][nNumPC]))  
		cFilAnt:= SC7->C7_FILIAL
		Mata120(1,,,2)
	Endif	
	cFilAnt	:= cFilB
Return

Static Function YPEMBARQUE(aPedidos,oMarkBrown)

	Local aImprimi := {}
	cArea := GetArea()
	
	
		AAdd(aImprimi,{aPedidos[nAx][nFiliala], aPedidos[nAx][nPed]})
	
		U_SASR124(aImprimi)
		
		U_SASR124(aPedidos[nAx][nFilial], aPedidos[nAx][nPed])
	
	RestArea(cArea)
	YDESMARCA()
	oMarkBrow:refresh()
	
Return