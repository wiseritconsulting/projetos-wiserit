#Include 'Protheus.ch'
#include "topconn.ch"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} SASP050
Faturamento em JOB
@author TOTVS
@since 17/10/2016
@version P11
/*/
User Function SASP129()
	Local cQuery
	Local lErro		:= .F.
	Local oError
	Local cMsgErro
	Private cSASStaErr	:= "E"
	Sleep(2000)
	//If !LockByName("SASP050-5",.F.,.F.)
	ConOut("Processso ja inicializado!")
	//Return
	//EndIf
	RpcSetEnv("01","010101",,,"FAT","U_SASP129",{})
	//__cInterNet	:= nil
	cQuery	:= " SELECT TOP 1 R_E_C_N_O_ AS REGSC5 FROM "+RETSQLNAME("SC5")+" SC5 "
	cQuery	+= " WHERE C5_YSTATUS In ('P','Q','EN') AND C5_YTFARM = 'S' AND C5_YPRIORI <> ' ' ORDER BY C5_YPRIORI "
	// Seta que RecLock, em caso de falha, nao vai fazer retry automatico
	SetLoopLock(.F.)
	While !killapp()
		Monitor("Faturamento|Aguardando tarefas...")	//Atualiza mensagem no monitor
		TCQuery cQuery new alias TMPSC5
		If TMPSC5->(EOF())
			TMPSC5->(DbCloseArea())
			return
			Sleep(10000)
			Loop
		EndIF
		lErro		:= .F.
		cMsgErro	:= ""
		SC5->(DbGoTo(TMPSC5->REGSC5))
		RecLock("SC5",.F.)
		oError := ErrorBlock({|e| lErro := .T.,ErroLog(e,cSASStaErr) } )
		dDatabase := date()
		If ALLTRIM(SC5->C5_YSTATUS)=="P" .or. ALLTRIM(SC5->C5_YSTATUS)==""	//Pendente Faturamento
			Faturar(@lErro,@cMsgErro)
		ElseIf ALLTRIM(SC5->C5_YSTATUS)=="Q"	//Pendente Cancelar Faturamento
			Cancelar(@lErro,@cMsgErro)
		//ElseIf ALLTRIM(SC5->C5_YSTATUS)=="EN"	//Entrada na Nota Fisca na Filial de destino
			//EntraNF(@lErro,@cMsgErro)
		EndIf
		Logs(Replicate("=",30))
		SC5->(MsUnLock())
		ErrorBlock(oError)
		if (Select("TMPSC5") > 0 )
			TMPSC5->(DbCloseArea())
		Endif

		RETURN
	EndDo
	//UnLockByName("SASP050-5",.F.,.F.)


Return

Static Function Faturar(lErro,cMsgErro)
	Local cCodUsr	:= "000000"
	Local aDocFat	:= {}
	Local cPcFat	:= {}
	Local cFilPc   
	Local cForPc  
	Local cLjForPc  
	Local aArea
	Local cCgcCli
	Local cCgcPv

	cSASStaErr		:= "EF"
	Monitor("Faturamento:Processando Filial:"+ SC5->C5_FILIAL +"  pedido"+SC5->C5_NUM )	//Atualiza mensagem no monitor
	Logs("Filial:"+SC5->C5_FILIAL)
	Logs("Processando Pedido:"+SC5->C5_NUM)
	Begin Sequence
		If !lErro 
			Monitor("Faturamento:Processando Filial:"+SC5->C5_FILIAL+" Pedido:"+SC5->C5_NUM )	//Atualiza mensagem no monitor
			Logs("Faturamento Filial: "+SC5->C5_FILIAL+" Pedido:"+SC5->C5_NUM )
			Begin Transaction
				If FGeraFat(SC5->C5_FILIAL,SC5->C5_NUM,@aDocFat,@cMsgErro)
					RecLock("SC5",.F.)
					SC5->C5_YLOG	:= "NOTA: "+ aDocFat[1] +" SERIE:"+ aDocFat[2] +""
					SC5->C5_YSTATUS := "F "
					MsUnLock()

				Else
					DisarmTransaction()
					Logs("Pedido de Venda :"+cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf

		If !lErro 	

			aArea := getarea()

			cCgcCli := POSICIONE("SA1",1,XFILIAL("SA1") + SC5->C5_CLIENTE + SC5->C5_LOJACLI,"A1_CGC")

			DbSelectArea("SM0")
			dbGOTOP() 
			While SM0->(!EOF())
				If ALLTRIM(SM0->M0_CGC) == ALLTRIM(cCgcCli)
					cFilPc	:= ALLTRIM(SM0->M0_CODFIL)
				EndIF
				IF ALLTRIM(SM0->M0_CODFIL) == ALLTRIM(SC5->C5_FILIAL)
					cCgcPv  := ALLTRIM(SM0->M0_CGC)
				endif
				SM0->(DbSkip())
			EndDo

			RestArea(aArea)

			cForPc    := POSICIONE("SA2",3,"      " + cCgcPv ,"A2_COD")
			cLjForPc  := POSICIONE("SA2",3,"      " + cCgcPv ,"A2_LOJA")


			Monitor("Criando Pedido de Compra " )	//Atualiza mensagem no monitor
			Logs("Criando Pedido de Compra " )
			Begin Transaction
				If FGeraPc(cFilPc,cForPc,cLjForPc,@cPcFat,@cMsgErro)
					RecLock("SC5",.F.)
					SC5->C5_YLOG	 := SC5->C5_YLOG + "  Pedido de compra na Filial: "+ cPcFat[1][1] +" Pedido:"+ cPcFat[2][1] +""
					SC5->C5_YFILPCIC := cPcFat[1][1]
					SC5->C5_YNUMIC   := cPcFat[2][1]

					MsUnLock()

				Else
					DisarmTransaction()
					Logs("Pedido de Compra :"+cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf

	End Sequence
	Monitor(" ")
	MsUnLockAll()
	GETMV("MV_NUMITEN",.T.)
	SX6->(MsRUnLock())
	If lErro
		RecLock("SC5",.F.)
		C5_YSTATUS = cSASStaErr//'EF'
		MsUnLock()
	EndIf
Return

Static Function Cancelar(lErro,cMsgErro)
	Local cCodUsr	:= "000000"
	Local aDocFat	:= {}
	Local cPcFat	:= {}
	Local cFilPc   
	Local cForPc  
	Local cLjForPc  
	Local aArea
	Local cCgcCli
	Local cCgcPv

	cSASStaErr	:= "EC"
	Monitor("Faturamento:Cancelar Faturamento Filial:"+SC5->C5_FILIAL+" Pedido:"+SC5->C5_NUM)	//Atualiza mensagem no monitor
	Logs("Filial:"+SC5->C5_FILIAL)
	Logs("Pedido:"+SC5->C5_NUM)
	Begin Sequence
		
		if !lErro  .and. !Empty(SC5->C5_NOTA)
		Begin Transaction
		If FCancFat(SC5->C5_FILIAL,SC5->C5_NOTA,SC5->C5_SERIE,@cMsgErro)
		RecLock("SC5",.F.)
					SC5->C5_NOTA	:= ""
					SC5->C5_SERIE	:= ""
					MsUnLock()
				Else
					DisarmTransaction()
					Logs(cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf
		
		// Verifica a possibilidade de excluir o pedido de compra 
		If !lErro .and. alltrim(SC5->C5_YFILPCIC + SC5->C5_YNUMIC) <> ""

			aArea := getarea()

			cCgcCli := POSICIONE("SA1",1,XFILIAL("SA1") + SC5->C5_CLIENTE + SC5->C5_LOJACLI,"A1_CGC")

			DbSelectArea("SM0")
			dbGOTOP() 
			While SM0->(!EOF())
				If ALLTRIM(SM0->M0_CGC) == ALLTRIM(cCgcCli)
					cFilPc	:= ALLTRIM(SM0->M0_CODFIL)
				EndIF
				IF ALLTRIM(SM0->M0_CODFIL) == ALLTRIM(SC5->C5_FILIAL)
					cCgcPv  := ALLTRIM(SM0->M0_CGC)
				endif
				SM0->(DbSkip())
			EndDo

			RestArea(aArea)

			cForPc    := POSICIONE("SA2",3,"      " + cCgcPv ,"A2_COD")
			cLjForPc  := POSICIONE("SA2",3,"      " + cCgcPv ,"A2_LOJA")
			Logs("CANCELANDO Pedido de Compra, Filial:"+SC5->C5_YFILPCIC+" Pedido "+SC5->C5_YNUMIC)

			//verifica se j� foi da entrada na nota 
			dbSelectArea("SF1")
			dbSetOrder(1)				
			If dbSeek(cFilPc+SC5->C5_NOTA+SC5->C5_SERIE+cForPc+cLjForPc)	
				dbclosearea("SF1")
				Begin Transaction
					if FCancNfEnt(cFilPc,SC5->C5_NOTA,SC5->C5_SERIE,SC5->C5_YNUMIC,@cMsgErro)
						RecLock("SC5",.F.)
						SC5->C5_YLOG	:= " "
						MsUnLock()
					else
						DisarmTransaction()
						Logs(cMsgErro,.T.)
						lErro		:= .T.
						BREAK
					endif
				End Transaction
			ENDIF
		endif

		If !lErro
			RecLock("SC5",.F.)
			SC5->C5_YSTATUS = 'C'
			SC5->C5_YFILPCIC := ""
			SC5->C5_YNUMIC := ""
			MsUnLock()
		EndIf
	End Sequence
	Monitor(" ")
	MsUnLockAll()
	GETMV("MV_NUMITEN",.T.)
	SX6->(MsRUnLock())
	If lErro
		RecLock("SC5",.F.)
		SC5->C5_YSTATUS = cSASStaErr//'EC'
		MsUnLock()
	EndIf
Return


Static Function EntraNF(lErro,cMsgErro)
	Local cCodUsr	:= "000000"
	Local aDocFat	:= {}
	Local cPcFat	:= {}
	Local cFilPc   
	Local cForPc  
	Local cLjForPc  
	Local aArea
	Local cCgcCli
	Local cCgcPv

	cSASStaErr		:= "EF"

	Monitor("Faturamento: Entrada na Nota Fiscal Filial:"+ SC5->C5_YFILPCIC +"  pedido"+SC5->C5_YNUMIC )	//Atualiza mensagem no monitor
	Logs("Filial:"+SC5->C5_YFILPCIC)
	Logs("Processando Pedido:"+SC5->C5_YNUMIC)
	Begin Sequence
		If !lErro 
			Monitor("Faturamento: Entrada na Nota Fiscal Filial:"+ SC5->C5_YFILPCIC +"  pedido"+SC5->C5_YNUMIC  )	//Atualiza mensagem no monitor
			Logs("Faturamento Filial: "+SC5->C5_YFILPCIC+" Pedido:"+SC5->C5_YNUMIC )
			Begin Transaction
				If FatuPC(SC5->C5_YFILPCIC,SC5->C5_YNUMIC,@aDocFat,@cMsgErro,SC5->C5_NOTA,SC5->C5_SERIE)   // FGeraFat(SC5->C5_FILIAL,SC5->C5_NUM,@aDocFat,@cMsgErro)
					RecLock("SC5",.F.)
					SC5->C5_YLOG	  := SC5->C5_YLOG + " Doc de Entrada: "+ aDocFat[1] +" SERIE:"+ aDocFat[2] +""
					SC5->C5_YSTATUS   := "FF"
					MsUnLock()

				Else
					DisarmTransaction()
					Logs("Documento de Entrada :"+cMsgErro,.T.)
					lErro		:= .T.
					BREAK
				EndIF
			End Transaction
		EndIf

	End Sequence
	Monitor(" ")
	MsUnLockAll()
	GETMV("MV_NUMITEN",.T.)
	SX6->(MsRUnLock())
	If lErro
		RecLock("SC5",.F.)
		C5_YSTATUS = cSASStaErr//'EF'
		MsUnLock()
	EndIf
Return


Static Function ErroLog(oErro,cStatus)
	DisarmTransaction()
	Logs(oErro:Description+CRLF+oErro:ErrorStack+CRLF+oErro:ErrorEnv,.T.)
	RecLock("SC5",.F.)
	C5_YSTATUS = cStatus	//Altera o status para erro
	MsUnLock()
	StartJob("U_SASP129",getenvserver(),.F.)	//Inicia um nova thread
	__Quit()									//Finaliza a thread atual
Return



/*/{Protheus.doc} FGeraFat
Fatura o Pedido de venda
@author TOTVS
@since 17/10/2016
@version 1.0
@param cEmpFat, String, Filial onde esta o pedido de venda para faturar
@param cPedFat, String, Numero do pedido de venda para faturar
@param aDocFat, Array, Array que recebe por referencia para gravar o Numero da NF e Serie
@param cMsgErro, String, String que recebe por referencia para gravar a msg de erro
/*/
Static Function FGeraFat(cEmpFat,cPedFat,aDocFat,cMsgErro)
	Local aArea			:= GetArea()
	Local aPvlNfs		:= {}
	Local cSerie		:= PadR("1",GetSx3Cache("F2_SERIE","X3_TAMANHO"))
	Local cFilBkp		:= cFilAnt
	Local cNFSaida		:= ""
	Local lRet			:= .T.
	Local nVezes
	Default aDocFat		:= {}
	Default cMsgErro	:= ""
	cFilAnt := cEmpFat

	//ProcRegua(4)
	Logs("Gerando Nota Fiscal de Sa�da")
	ProcessMessage()

	U_PESOMED(cEmpFat,cPedFat)

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))



	If !(SC9->(DbSeek(cEmpFat+cPedFat))) //Caso n�o tenha sido Liberado, chama para realizar a Libera��o do Pedido
	Endif
	FLibPedSZ5(cEmpFat,cPedFat,@cMsgErro)

	aTesdig = GETAREA()

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	cTesdig = SC6->C6_TES


	RESTAREA(aTesdig)

	dbSelectArea("SC9")
	SC9->(dbSetOrder(1))

	If SC9->(DbSeek(cEmpFat+cPedFat))
		While SC9->(!Eof()) .AND. SC9->C9_FILIAL == cEmpFat .AND. SC9->C9_PEDIDO == cPedFat
			cProd = SC9->C9_PRODUTO
			cQuant = CVALTOCHAR(SC9->C9_QTDLIB)
			IF GETMV("SA_BLEST")
				IF cTesdig != "527"
					If !Empty(SC9->C9_BLEST) .and. !Empty(SC9->C9_BLCRED)
						cSASStaErr	:= "BE"
						cMsgErro	:= cEmpFat +"|"+cPedFat +"| Pedido com Bloqueio | Bloqueio de estoque/Credito "
						Return .F.
					ELSEIF !Empty(SC9->C9_BLEST)
						cSASStaErr	:= "BE"
						cMsgErro	:= cEmpFat +"|"+cPedFat +" | "+ cProd +" | "+ cQuant +" | Pedido com Bloqueio | Bloqueio de estoque "
						Return .F.
					ELSEIF  !Empty(SC9->C9_BLCRED)
						cSASStaErr	:= "BE"
						cMsgErro	:= cEmpFat +"|"+cPedFat +"| Pedido com Bloqueio | Bloqueio de Credito "
						Return .F.
					ENDIF
				ENDIF
			endif
			SC6->(DbSeek(xFilial("SC6")+SC9->C9_PEDIDO+SC9->C9_ITEM+SC9->C9_PRODUTO))

			SE4->(DbSeek(xFilial("SE4")+SC5->C5_CONDPAG))
			//SE4->(DBSEEK(XFILIAL("SE4") + '001')
			SB1->(DbSeek(xFilial("SB1")+SC9->C9_PRODUTO))
			SB2->(DbSeek(xFilial("SB2")+SC9->C9_PRODUTO+SC9->C9_LOCAL))
			SF4->(DbSeek(xFilial("SF4")+SC6->C6_TES))

			aAdd(aPvlNfs,{;
			SC9->C9_PEDIDO,;
			SC9->C9_ITEM,;
			SC9->C9_SEQUEN,;
			SC9->C9_QTDLIB,;
			SC9->C9_PRCVEN,;
			SC9->C9_PRODUTO,;
			.F.,;
			SC9->(RECNO()),;
			SC5->(RECNO()),;
			SC6->(RECNO()),;
			SE4->(RECNO()),;
			SB1->(RECNO()),;
			SB2->(RECNO()),;
			SF4->(RECNO());
			})
			SC9->(DbSkip())
		EndDo
	EndIf

	If Len(aPvlNfs) == 0
		cMsgErro := "Pedido "+cPedFat+" n�o liberado"+ CHR(10) + CHR(13)
		RestArea(aArea)
		cFilAnt:= cFilBkp
		lRet	:= .F.
		Return lRet
	endif

	//Verifica se a numera��o da NF est� em uso:
	dbSelectArea("SX5")
	dbSetOrder(1)

	If SX5->(MsSeek(cEmpFat+"01"+cSerie))
		nVezes := 0
		While ( !SX5->(MsRLock()) )
			nVezes ++
			Logs("Aguardando libera��o de numera��o da Nota Fiscal de Sa�da ... "+cValtochar(nVezes))
			If ( nVezes > 30 )
				Exit
			EndIf
			Sleep(1000)
		EndDo
	Endif

	Logs("Gerando Nota Fiscal de Sa�da")

	//Gera documento de saida
	Pergunte("MT460A",.F.)
	cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .F. , .F. , .F. , .F., 0, 0, .F., .F.)   
	//cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .T. , .T. , .F. , .F., 0, 0, .F., .F.) //CONTAB ONLINE - PRODUCAO

	If Empty(cNFSaida)
		cMsgErro := "N�o foi poss�vel gerar a Nota fiscal de Sa�da da Filial "+cFilAnt+ CHR(10) + CHR(13)
	Else
		aDocFat	:= {cNFSaida,cSerie}
		Logs("Nota Fiscal de Sa�da gerada")
	Endif

	dbSelectArea("SX5")
	//If !SX5->(MsRLock()) .and. SX5->(!EOF())
	SX5->(MsUnlock())
	//Endif

	RestArea(aArea)
	cFilAnt:= cFilBkp
Return lRet

/*
GERANDO PEDIDO DE COMPRA NA FILIAL DE DESTINO 
AUTO : JOAO FILHO
*/

Static Function FGeraPc(cFilPc,cForPc,cLjForPc,cPcFat,cMsgErro)


	Local lRet       := .T.
	Local cFilBkp	 := cFilAnt
	Local aCabPC     := {}
	Local aLinha     := {}
	Local aItensPC   := {}
	Local cPc
	Default cPcFat   := {}
	Default cMsgErro := {}

	cFilAnt := cFilPc

	cPc	:= CriaVar('C7_NUM', .T.)
	aadd(aCabPC,{"C7_FILIAL"	,cFilPc})
	aadd(aCabPC,{"C7_NUM"		,cPc})
	aadd(aCabPC,{"C7_EMISSAO"	,dDataBase})
	aadd(aCabPC,{"C7_FORNECE"	,cForPc})
	aadd(aCabPC,{"C7_LOJA"		,cLjForPc})
	aadd(aCabPC,{"C7_COND"		,SC5->C5_CONDPAG})		//TODO VERIFICAR CONDI��O PAG
	aadd(aCabPC,{"C7_CONTATO"	,"AUTO"})
	aadd(aCabPC,{"C7_FILENT"	,cFilPc})

	cQuery	:= "SELECT * FROM "+RETSQLNAME("SC6")+" SC6 WHERE SC6.D_E_L_E_T_ = '' " 
	cQuery	+= " AND C6_FILIAL = '"+ SC5->C5_FILIAL +"' AND C6_NUM = '"+ SC5->C5_NUM +"' "
	TcQuery cQuery New Alias T07

	WHILE !T07->(EOF())

		/*
		Regra da TES
		501 - 001 (VENDA)
		511 - 009 (TRANSFERENCIA)
		507 - 007 (MANIFESTO) ''
		*/
		
		If SC5->C5_YPEDTV  = 'V' //Venda
			cTes:= GetMv("SA_TESEPDV") 
		Elseif  SC5->C5_YPEDTV  = 'T' //Transfer�ncia
			cTes:= GetMv("SA_TESETRF")
		Elseif SC5->C5_YPEDTV  = 'M' //Manifesto
			cTes:= GetMv("SA_TESEMNF")
		Endif
		
		
		aLinha := {}

		aadd(aLinha,{"C7_PRODUTO"	,T07->C6_PRODUTO	,Nil})
		aadd(aLinha,{"C7_QUANT"		,T07->C6_QTDVEN  	,Nil})
		aadd(aLinha,{"C7_PRECO"		,T07->C6_PRCVEN		,Nil})
		aadd(aLinha,{"C7_TES"		,cTes				,Nil})
		aadd(aLinha,{"C7_LOCAL"		,T07->C6_LOCAL		,Nil})
		aadd(aLinha,{"C7_YCLAORC"	,"E"	         	,Nil})
		aadd(aLinha,{"C7_YNOTA"	    ,SC5->C5_NOTA       ,Nil})
		aadd(aItensPC,aLinha)

		T07->(DbSkip())
	ENDDO
	T07->(DbCloseArea())


	lMsErroAuto	:= .F.
	cMat120Num		:= ""	//Variavel preenchida na A120Grava

	MsExecAuto({|x,y,z| MATA120(1,x,y,z)}, aCabPC,aItensPC,3)

	If lMsErroAuto
		AutoGrLog("Erro ao gerar pedido de compra!")
		MostraErro()
		Return .F.
	Else
		cPc	:= If(!Empty(cMat120Num),cMat120Num,cPc)
		dbSelectArea("SC7")
		dbSetOrder(1)
		If !MsSeek(xFilial("SC7")+cPc)
			AutoGrLog("Erro ao localizar pedido de compra gerado!")
			MostraErro()
			Return .F.
		EndIf
		AADD(cPcFat,{SC7->C7_FILIAL})
		AADD(cPcFat,{SC7->C7_NUM})
	Endif

	cFilAnt := cFilBkp

Return lRet


/*
VERIFICA E EXCLUI O PEDIDO DE COMPRAS
AUTO : JOAO FILHO
*/

Static Function FCancPc(cFilPc,cForPc,cLjForPc,cPcFat,cMsgErro,cNumPc)


	Local lRet       := .T.
	Local cFilBkp	 := cFilAnt
	Local aCabPC     := {}
	Local aLinha     := {}
	Local aItensPC   := {}
	Local cPc
	Default cPcFat   := {}
	Default cMsgErro := {}

	cFilAnt := cFilPc

	// VERIFICA SE JA TEVE ENTRADA DE NOTA
	IF POSICIONE("SC7",1,XFILIAL("SX7")+cNumPc , "C7_QUJE") <> 0

		AutoGrLog("Pedido de compra da filial :"+XFILIAL("SX7")+" de numero :"+cNumPc+" n�o pode ser excluido !")
		cFilAnt := cFilBkp
		Return .F.

	ENDIF

	cPc	:= cNumPc
	aadd(aCabPC,{"C7_FILIAL"	,cFilPc})
	aadd(aCabPC,{"C7_NUM"		,cPc})
	aadd(aCabPC,{"C7_EMISSAO"	,dDataBase})
	aadd(aCabPC,{"C7_FORNECE"	,cForPc})
	aadd(aCabPC,{"C7_LOJA"		,cLjForPc})
	aadd(aCabPC,{"C7_COND"		,SC5->C5_CONDPAG})		//TODO VERIFICAR CONDI��O PAG
	aadd(aCabPC,{"C7_CONTATO"	,"AUTO"})
	aadd(aCabPC,{"C7_FILENT"	,cFilPc})

	//aadd(aCabPC,{"C7_YCONTRA"	,cContrato})
	//aadd(aCabPC,{"C7_YMEDICA"	,cMedicao})

	cQuery	:= "SELECT * FROM "+RETSQLNAME("SC7")+" SC7 WHERE SC7.D_E_L_E_T_ = '' AND C7_FILIAL = '"+ SC5->C5_YFILPCIC +"' AND C7_NUM = '"+ SC5->C5_YNUMIC +"' "
	TcQuery cQuery New Alias T07

	WHILE !T07->(EOF())

		//For nCont:= 1 To Len(aItens2)
		aLinha := {}

		aadd(aLinha,{"C7_PRODUTO"	,T07->C7_PRODUTO	,Nil})
		aadd(aLinha,{"C7_QUANT"		,T07->C7_QUANT  	,Nil})
		aadd(aLinha,{"C7_PRECO"		,T07->C7_PRECO		,Nil})
		aadd(aLinha,{"C7_TES"		,T07->C7_TES		,Nil})
		aadd(aLinha,{"C7_LOCAL"		,T07->C7_LOCAL		,Nil})
		aadd(aLinha,{"C7_YCLAORC"	,T07->C7_YCLAORC   	,Nil})
		aadd(aLinha,{"C7_YNOTA"	    ,SC5->C5_NOTA       ,Nil})
		//	aadd(aLinha,{"C7_CC"		,"1025"		        ,Nil})	//1025-Almoxarifado
		aadd(aItensPC,aLinha)
		//Next nCont
		T07->(DbSkip())
	ENDDO
	T07->(DbCloseArea())


	lMsErroAuto	:= .F.
	cMat120Num		:= ""	//Variavel preenchida na A120Grava

	MsExecAuto({|x,y,z| MATA120(1,x,y,z)}, aCabPC,aItensPC,5)

	If lMsErroAuto
		AutoGrLog("Erro ao excluir pedido de compra!")
		MostraErro()
		Return .F.
	Endif

	cFilAnt := cFilBkp

Return lRet

/*/{Protheus.doc} FLibPedSZ5
Liberar Pedido de Venda
@type function
@author Joao Filho
@since 17/10/2016
@version P11
@param cEmpFat, character, Empresa do Pedido
@param cPedFat, character, Numero do Pedido
@param cErro, character, Erro enviado por referencia
/*/
Static Function FLibPedSZ5(cEmpFat,cPedFat,cMsgErro)
	Local aArea1:= GetArea()
	Local aCab	:= {}
	Local aItem	:= {}
	Local lRet	:= .T.
	Default cMsgErro	:= ""

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	aCab:= {	{"C5_TIPO"		,SC5->C5_TIPO		,NIL};
	,{"C5_NUM"		,SC5->C5_NUM		,NIL};
	,{"C5_CLIENTE"	,SC5->C5_CLIENTE	,NIL};
	,{"C5_CLIENT"	,SC5->C5_CLIENT		,NIL};
	,{"C5_LOJACLI"	,SC5->C5_LOJACLI	,NIL};
	,{"C5_CONDPAG"	,SC5->C5_CONDPAG	,NIL};
	,{"C5_PESOL"	,SC5->C5_PESOL		,NIL};
	,{"C5_PBRUTO"	,SC5->C5_PBRUTO	,NIL};
	,{"C5_EMISSAO"  ,SC5->C5_EMISSAO	,NIL}}

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))



	If SC6->(DbSeek(cEmpFat+cPedFat))
		While SC6->(!Eof()) .AND. SC6->C6_FILIAL == cEmpFat .AND. SC6->C6_NUM == cPedFat
			aAdd(aItem,{ {"C6_PRODUTO"	,SC6->C6_PRODUTO,NIL};
			,{"C6_ITEM"		,SC6->C6_ITEM   ,NIL};
			,{"C6_QTDVEN"	,SC6->C6_QTDVEN ,NIL};
			,{"C6_PRCVEN"	,SC6->C6_PRCVEN ,NIL};
			,{"C6_VALOR"	,SC6->C6_VALOR	,NIL};
			,{"C6_TES"		,SC6->C6_TES	,NIL};
			,{"C6_CLI"		,SC6->C6_CLI	,NIL};
			,{"C6_QTDLIB"	,SC6->C6_QTDVEN	,NIL};
			,{"C6_LOJA"		,SC6->C6_LOJA	,NIL};
			,{"C6_YCODKIT"	,SC6->C6_YCODKIT,NIL};
			,{"C6_LOCAL"	,SC6->C6_LOCAL  ,NIL}})

			SC6->(DbSkip())
		EndDo

		lMsErroAuto := .F.
		//		__cInternet	:= nil
		MSExecAuto({|x,y,z|Mata410(x,y,z)},aCab,aItem,4)
		If lMsErroAuto
			cMsgErro 	:= MostraErro()
			cMsgErro	+= "Pedido "+cPedFat+" n�o foi liberado"+ CHR(10) + CHR(13)
			lRet		:= .F.
		Endif
	Endif

	RestArea(aArea1)
Return lRet

/*/{Protheus.doc} FatuPC
Gera documento de entrada referente ao Pedido de Compra
@author Diogo/Saulo
@since 17/10/2016
@version 1.0
/*/
Static Function FatuPC(cEmpFat,cPedFat,aDocFat,cMsgErro,cDocNF,cSerieNF)
	Local aCabec	:= {}
	Local aItens	:= {}
	Local aLinha	:= {}
	Local cFilBkp	:= cFilAnt
	Local lOk		:= .T.
	Local cEntarm := ZZ2->ZZ2_TIPCTR
	Private lMsErroAuto := .F.

	cFilAnt	:= cEmpFat


	//ProcRegua(4)
	Logs("Gerando Nota Fiscal de Entrada")
	ProcessMessage()

	dbSelectArea("SC7")
	SC7->(dbSetOrder(1))
	SC7->(dbSeek(xFilial("SC7",cEmpFat)+cPedFat))

	aadd(aCabec,{"F1_TIPO"   ,"N"})
	aadd(aCabec,{"F1_FORMUL" ,"N"})
	aadd(aCabec,{"F1_DOC"    ,cDocNF})
	aadd(aCabec,{"F1_SERIE"  ,cSerieNF})
	aadd(aCabec,{"F1_EMISSAO",dDataBase})
	aadd(aCabec,{"F1_FORNECE",SC7->C7_FORNECE})
	aadd(aCabec,{"F1_LOJA"   ,SC7->C7_LOJA})
	aadd(aCabec,{"F1_ESPECIE","SPED"})
	aadd(aCabec,{"F1_YFILIAL",ALLTRIM(SC7->C7_FILIAL)})
	//aadd(aCabec,{"F1_YCONTRA",ALLTRIM(SC7->C7_YCONTRA)})
	//aadd(aCabec,{"F1_YMEDICA",ALLTRIM(SC7->C7_YMEDICA)})
	aadd(aCabec,{"F1_NATUREZ",GetMv("SA_NATPAG")})
	NNR->(dbSetOrder(1))
	_linha :=1
	While SC7->(!eof()) .and. SC7->C7_FILIAL+SC7->C7_NUM == xFilial("SC7",cEmpFat)+cPedFat
		If !Empty(SC7->C7_YTES)
			RecLock("SC7",.F.)
			SC7->C7_TES	:= SC7->C7_YTES
			MsUnLock()
		EndIf
		aLinha := {}
		Aadd(aLinha,{"D1_COD"     	,SC7->C7_PRODUTO		,Nil})
		//Aadd(aLinha,{"D1_ITEM"     	,SC7->C7_ITEM	    ,Nil})
		Aadd(aLinha,{"D1_ITEM"     	,strzero(_linha,4)		,Nil})
		Aadd(aLinha,{"D1_QUANT"		,SC7->C7_QUANT			,Nil})
		Aadd(aLinha,{"D1_VUNIT"		,SC7->C7_PRECO			,Nil})
		Aadd(aLinha,{"D1_TOTAL"		,SC7->C7_TOTAL			,Nil})
		Aadd(aLinha,{"D1_PEDIDO"	,SC7->C7_NUM			,Nil})
		Aadd(aLinha,{"D1_CC"		,SC7->C7_CC				,Nil})
		Aadd(aLinha,{"D1_TES"		,SC7->C7_TES			,Nil})
		Aadd(aLinha,{"D1_ITEMPC"	,SC7->C7_ITEM			,Nil})
		Aadd(aLinha,{"D1_LOCAL"		,SC7->C7_LOCAL       	,Nil})// Faturamento Direto (Weskley Silva 11/02/2016)
		Aadd(aLinha,{"D1_YDTENTR"	,dDataBase           	,Nil})
		//Aadd(aLinha,{"D1_YCO"		,SC7->C7_YCO			,Nil})
		//Aadd(aLinha,{"D1_YCLAORC"	,SC7->C7_YCLAORC		,Nil})

		_linha++
		If (!NNR->(MsSeek(xFilial("NNR")+SC7->C7_LOCAL)))//(!NNR->(MsSeek(xFilial("NNR")+SC7->C7_LOCAL)))
			AGRA045(;
			{{"NNR_CODIGO",SC7->C7_LOCAL,NIL};
			,{"NNR_DESCRI","Armazem "+SC7->C7_LOCAL,NIL};
			};
			,3,{},{})

		EndIf
		aadd(aItens,aLinha)
		SC7->(DbSkip())
	EndDo

	MSExecAuto({|x,y,z| MATA103(x,y,z) },aCabec,aItens,3)
	If !lMsErroAuto
		lOk:= .T.
		aDocFat	:= {SF1->F1_DOC,SF1->F1_SERIE}
	Else
		lOk:= .F.
		cMsgErro := Mostraerro()
		cMsgErro += "N�o foi poss�vel gerar a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
	EndIf

	cFilAnt:= cFilBkp

Return lOk

/*/{Protheus.doc} FCancFat
Faz exclus�o das Notas Fiscais de Sa�da
@author Joao Filho
@since 17/10/2016
@version 1.0
/*/

Static Function FCancFat(cEmpFat,cDocNF,cSerNF,cMsgErro)
	Local cFilBkp	:= cFilAnt
	Local nSpedExc	:= GetNewPar("MV_SPEDEXC",24)
	Local lRet		:= .T.
	Local lMostraCTB:= .F.
	Local lAglCTB	:= .F.
	Local lContab	:= .F.
	Local lCarteira	:= .T.
	Local lRetExc
	Local aRegSD2	:= {}
	Local aRegSE1	:= {}
	Local aRegSE2	:= {}
	Local nHoras
	Default cMsgErro	:= ""
	cFilAnt:= cEmpFat

	dbSelectArea("SF2")
	SF2->(dbSetOrder(1))
	If SF2->(!dbSeek(cEmpFat+cDocNF+cSerNF))
		Logs("Registro n�o encotrado! "+cEmpFat+cDocNF+cSerNF)	//N�o dar erro, mas coloca em Log
		lRet	:= .T.
		Return lRet
	EndIF

	Logs("Excluindo Nota Fiscal de Sa�da "+cDocNF)
	//Verifica se poder� realizar a exclus�o da nota fiscal
	If !Empty(SF2->F2_DAUTNFE)
		nHoras := SubtHoras(SF2->F2_DAUTNFE,SF2->F2_HAUTNFE,Date(),SubStr(Time(),1,2)+":"+SubStr(Time(),4,2))
		If nHoras > nSpedExc
			cMsgErro+="N�o foi possivel excluir a(s) nota(s), pois o prazo para o cancelamento da(s) NF-e � de " + Alltrim(STR(nSpedExc)) +" horas "+chr(13)+chr(10)
			lRet	:= .F.
			Return lRet
		EndIf
	Endif

	cMarca := GetMark(,"SF2","F2_OK")

	RecLock("SF2",.F.)
	SF2->F2_OK := cMarca
	MSUNLOCK()

	lRetExc := MaCanDelF2("SF2",SF2->(RecNo()),@aRegSD2,@aRegSE1,@aRegSE2)

	If lRetExc
		SF2->(MaDelNFS(aRegSD2,aRegSE1,aRegSE2,lMostraCTB,lAglCTB,lContab,lCarteira))
	Else
		AutoGrLog("Erro na exclus�o do Documento de Sa�da da Filial "+cFilAnt)
		cMsgErro:=Mostraerro()
		lRet	:= .F.
	EndIf
	cFilAnt:= cFilBkp
Return lRet

/*/{Protheus.doc} FCancNfEnt
Efetua estorno do Documento de Entrada
@author Joao Filho
@since 17/10/2016
@version P11
/*/
Static Function FCancNfEnt(cEmpFat,cDocNF,cSerNF,cNumPC,cMsgErro)
	Local cCodUsr		:= "000000"
	Local aCabec 		:= {}
	Local aItens		:= {}
	Local aLinha 		:= {}
	Local cFilBkp		:= cFilAnt
	Local lRet			:= .T.
	Local aFornece
	Private lMsErroAuto := .F.
	Default cMsgErro	:= ""
	cFilAnt				:= cEmpFat
	dbSelectArea("SC7")
	SC7->(dbSetOrder(1))
	SC7->(dbSeek(xFilial("SC7",cEmpFat)+cNumPC))
	aFornece	:= {SC7->C7_FORNECE,SC7->C7_LOJA}

	dbSelectArea("SF1")
	SF1->(dbSetOrder(1))	//F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA+F1_TIPO
	If SF1->(!dbSeek(xFilial("SF1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]))
		cMsgErro := "Chave n�o encontrada: "+xFilial("SF1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]
		Return .F.
	EndIf
	Logs("Excluindo Nota Fiscal de Entrada "+cDocNF)

	aadd(aCabec,{"F1_TIPO"   ,SF1->F1_TIPO})
	aadd(aCabec,{"F1_FORMUL" ,SF1->F1_FORMUL})
	aadd(aCabec,{"F1_DOC"    ,SF1->F1_DOC})
	aadd(aCabec,{"F1_SERIE"  ,SF1->F1_SERIE})
	aadd(aCabec,{"F1_EMISSAO",SF1->F1_EMISSAO})
	aadd(aCabec,{"F1_FORNECE",SF1->F1_FORNECE})
	aadd(aCabec,{"F1_LOJA"   ,SF1->F1_LOJA})
	aadd(aCabec,{"F1_ESPECIE",SF1->F1_ESPECIE})

	dbSelectArea("SD1")
	SD1->(dbSetOrder(1))	//D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
	SD1->(dbSeek(xFilial("SD1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]))

	While SD1->(!eof()) .and. SD1->D1_FILIAL+SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA == xFilial("SD1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]
		aLinha := {}
		Aadd(aLinha,{"D1_COD"     	,SD1->D1_COD			,Nil})
		Aadd(aLinha,{"D1_ITEM"     	,SD1->D1_ITEM			,Nil})
		Aadd(aLinha,{"D1_QUANT"		,SD1->D1_QUANT			,Nil})
		Aadd(aLinha,{"D1_VUNIT"		,SD1->D1_VUNIT			,Nil})
		Aadd(aLinha,{"D1_TOTAL"		,SD1->D1_TOTAL			,Nil})
		Aadd(aLinha,{"D1_PEDIDO"	,SD1->D1_PEDIDO			,Nil})
		Aadd(aLinha,{"D1_ITEMPC"	,SD1->D1_ITEMPC			,Nil})
		Aadd(aLinha,{"D1_LOCAL"		,SD1->D1_LOCAL			,Nil})

		NNR->(dbSetOrder(1))
		If (!NNR->(MsSeek(xFilial("NNR")+SD1->D1_LOCAL)))
			AGRA045(;
			{{"NNR_CODIGO",SD1->D1_LOCAL,NIL};
			,{"NNR_DESCRI","Armazem "+SD1->D1_LOCAL,NIL};
			};
			,3,{},{})
		EndIf
		aadd(aItens,aLinha)
		SD1->(DbSkip())
	EndDo

	//	MATA103(aCabec,aItens,5)
	MSExecAuto({|x,y,z| MATA103(x,y,z) },aCabec,aItens,5)

//	If SF1->(DBSeek(xFilial("SF1",cEmpFat)+cDocNF+cSerNF+aFornece[1]+aFornece[2]))
//		MSExecAuto({|x,y,w,z| mata103(x,y,w,z)},,,5,.F.)
//	EndIf
	
	If !lMsErroAuto
	Else
		lRet	:= .F.
		cMsgErro += Mostraerro()
		cMsgErro := "N�o foi poss�vel excluir a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
	EndIf
	cFilAnt:= cFilBkp

Return lRet

/*/{Protheus.doc} Logs
Grava log em arquivo texto no servidor
@author Joao Filho
@since 17/10/2016
@version 1.0
@param cTexto, character, Texto do log
/*/
Static Function Logs(cTexto,lErro)
	Local cCodLog	:= SubStr(DTOS(Date()),1,6)
	Local cPasta	:= "\log\SASP129"
	Local cTexLog
	Default lErro	:= .F.
	FWMakeDir(cPasta)
	If File(cPasta+"\"+cCodLog+".txt")
		nHdl := fopen(cPasta+"\"+cCodLog+".txt" , 2 + 64 )
	Else
		nHdl := FCREATE(cPasta+"\"+cCodLog+".txt", FC_NORMAL)
	EndIf
	FSeek(nHdl, 0, 2)
	FWrite(nHdl, DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+CRLF )
	//cErroJob	+= DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+CRLF
	fclose(nHdl)
	If lErro
		RecLock("SC5",.F.)
		cTexLog			:= Alltrim(SC5->C5_YLOG)
		SC5->C5_YLOG	:= DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+chr(10)+cTexLog
		MsUnLock()
		lErroJob	:= .T.
	EndIf
Return

Static Function Monitor(cMsg)

	tcsqlexec("INSERT INTO YJOB (JOBLOG) VALUES ('"+cMsg+"')")
	PutGlbValue("SASP129",DTOC(date(),"dd/mm/yy")+"|"+time()+cMsg)
	PtInternal(1,DTOC(date(),"dd/mm/yy")+"|"+time()+"|"+cMsg)	//Atualiza mensagem no monitor
Return