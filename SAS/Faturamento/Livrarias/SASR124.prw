#include "tbiconn.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "protheus.ch"
#include "topconn.ch"

user function SASR124(aImprimi)
	
	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .T.
	Local oPrinter
	Local cLocal          := "c:\temp\"
	Local cCodINt25 := "010105000000"					
	Local oFont1 	:= TFont():New( "Arial",,28,,.T.,,,,,  .T. )
	Local oFont2 	:= TFont():New( "Arial",,110,,.T.,,,,,  .F. )
	Local oFont3 	:= TFont():New( "Arial",,28,,.T.,,,,,  .F. )
	Local oFont4 	:= TFont():New( "Arial",,24,,.F.,,,,,  .F. )
	Local oFont5 	:= TFont():New( "Arial",,12,,.T.,,,,,  .T. )
	Local oPrn      := FwMSPrinter():New( 'Placa de embarque_livrarias' , 6 , .F. , , .T. )
	Local cFilePrint := ""
	Local nLin := 150
	Local nTmLin := 10
	Local nSalto := 20
	Local cQuery := ""
	Local aArea := GetArea()
	Local cSeq2 := ""
	Local cFilialPed := aImprimi[1,1]
	Local cNumPed := aImprimi[1,2]
	
	cSeq2 := Seq()


	dbSelectArea("SC5")
	dbSetOrder(1)
	SC5->(dbSeek(cFilialPed+cNumPed))

	if !found()
		Alert("Pedido n�o encontrado")
		restarea(aArea)
		return
	endif
	
	
	//IF cSerie <> "99"

	cQuery := "SELECT C5.C5_CLIENTE, "
	cQuery += "A1.A1_NOME, "
	cQuery += "C5.C5_TRANSP, "
	cQuery += "CASE WHEN A4.A4_NOME = '' "
	cQuery += "THEN 'N�o Informado' "
	cQuery += "WHEN A4.A4_NOME IS NULL "
	cQuery += "THEN '' "
	cQuery += "END AS A4_NOME, "
	cQuery += "C5.C5_NUM, "
	cQuery += "ZC4.ZC4_ENVIO, "
	cQuery += "ZC4.ZC4_SERIE, "
	cQuery += "ZC4.ZC4_VOLUME, "
	cQuery += "SUM(ZC4.ZC4_VOLUME) AS 'TOTALVOLUMES' "
	cQuery += "FROM ZC4010 ZC4 "
	cQuery += "INNER JOIN SC5010 C5 ON "
	cQuery += "C5.C5_FILIAL = ZC4.ZC4_FILIAL AND "
	cQuery += "C5.C5_NUM = ZC4.ZC4_PEDIDO "
	cQuery += "LEFT JOIN SA4010 A4 ON "
	cQuery += "C5.C5_TRANSP = A4.A4_COD "
	cQuery += "INNER JOIN SA1010 A1 ON "
	cQuery += "A1.A1_COD = C5.C5_CLIENTE "
	cQuery += "WHERE ZC4.ZC4_PEDIDO = '"+ cFilialPed +"' "
	cQuery += "AND ZC4.ZC4_FILIAL = '"+ cNumPed +"' "
	cQuery += "AND ZC4.D_E_L_E_T_ = '' "
	cQuery += "AND C5.D_E_L_E_T_ = '' "
	cQuery += "GROUP BY C5.C5_CLIENTE, "
	cQuery += "A1.A1_NOME, "
	cQuery += "C5.C5_TRANSP, "
	cQuery += "A4.A4_NOME, "
	cQuery += "C5.C5_NUM, "
	cQuery += "ZC4.ZC4_VOLUME, "
	cQuery += "ZC4.ZC4_SERIE, "
	cQuery += "ZC4.ZC4_ENVIO "

	//IF !(cSerie=="" .OR. cEnv=="")
		//cQuery +=" HAVING "
		//cQuery +=" ZZQ_CODSER = '"+cSerie+"'"
		//cQuery +=" AND ZZQ_ENVIO = '"+cEnv+"'"
		//cQuery +=" AND ZZQ_CATEGO = '"+cCatego+"'"
	//ENDIF

	//MEMOWRITE("C:\TEMP\SQLR032.TXT", cQuery)

	TcQuery cQuery New Alias PE
	
	cCodINt25 := SC5->(C5_FILIAL+C5_NUM)
	oPrinter := FWMSPrinter():New('pembarque_000000.PD_', IMP_PDF, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetLandscape()
	
	
	WHILE !PE->(EOF())

	
		oPrinter:StartPage()

		oPrinter:FWMSBAR("CODE128" /*cTypeBar*/,5/*nRow*/ ,45/*nCol*/ ,cCodINt25  /*cCode*/,oPrinter/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.04 /*nWidth*/,2/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)


		oPrinter:Say ( 120, 100, PADL(cSeq2,3,"0"),  oFont2 ) 	
		oPrinter:Say ( nLin, 600, cCodINt25,  oFont5 )	

		oPrinter:Say ( 230, 20, "CLIENTE",  oFont1 ) 
		oPrinter:Say ( 260, 30, PE->C5_CLIENTE + " - " + PE->A1_NOME,  oFont4 )
		oPrinter:Say ( 290, 20, "TRANSPORTADORA",  oFont1 )
		oPrinter:Say ( 320, 30, IIF(!EMPTY(PE->C5_TRANSP),PE->C5_TRANSP + ' - ' + POSICIONE("SA4",1,xFilial("SA4")+PE->C5_TRANSP,"A4_NOME")," #### TRANSPORTADORA NAO INFORMADA #### ") ,  oFont4 )
		oPrinter:Say ( 350, 20, "SERIE",  oFont1 )
		oPrinter:Say ( 380, 30, IIF(!EMPTY(PE->ZC4_SERIE),PE->ZC4_SERIE + ' - ' + POSICIONE("ZZO",1,PE->ZC4_SERIE,"ZZO_DESC"), " #### S�RIE N�O INFORMADA / N�O PREENCHIDA #### "),  oFont4 )
		oPrinter:Say ( 410, 20, "ENVIO",  oFont1 )
		oPrinter:Say ( 440, 30, PE->ZC4_ENVIO,  oFont4 )
		oPrinter:Say ( 470, 20, "VOLUME",  oFont1 )
		oPrinter:Say ( 500, 30, cValToChar(PE->ZC4_VOLUME),  oFont4 )
		oPrinter:Say ( 530, 20, "TOTAL VOLUMES",  oFont1 )
		oPrinter:Say ( 560, 30, cValToChar(PE->TOTALVOLUMES),  oFont4 )

		oPrinter:EndPage()
		
		PE->(dbskip())
		
	ENDDO
	
	oPrinter:Preview()
	
	PE->(dbCloseArea())
	
	RestArea(aArea)
	
Return

Static Function Seq()


	Local cSeque := 0
	Local cFili := cfilAnt

	cfilAnt := SC5->C5_FILIAL

	cSeque := GetMv( "SA_PLEMBAR")

	cSeque := Soma1(cvaltochar(cSeque))

	If val(cSeque) > 500
		PUTMV("SA_PLEMBAR",1)
		cSeque := 1
	else	 
		PUTMV("SA_PLEMBAR",val(cSeque)) 
	endif

	cfilAnt := cFili

Return(cSeque)	
return