#include "protheus.ch"
#include "dbtree.ch"
#include "topconn.ch"
#INCLUDE "fwbrowse.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "fwbrowse.ch"

/*/{Protheus.doc} Kit de Produtos - Chamado no Pedido de Vendas
(long_description)
@author Diogo Costa
@since 13/08/2015
@version 1.0
/*/

User Function SASP132(ccodacu,cFiltr)
	Local oBrowse
	Local aCoors			:= FWGetDialogSize( oMainWnd )
	Local bClique	:= {|| ;
	Processa({|| aAreaZZ7:=ZZ7->(GetArea()),oBrowse:SetFilterDefault(".T."),RestArea(aAreaZZ7),Geraarv(cRotChm,;
	If(Len(RTRIM(ZZ7->ZZ7_CODIGO))<=5,"  ZZ7_CODIGO ='"+ZZ7->ZZ7_CODIGO+"' OR ZZ7_CODPAI='"+ZZ7->ZZ7_CODIGO+"' AND D_E_L_E_T_='' "," ZZ7_CODIGO LIKE '"+RTRIM(ZZ7->ZZ7_CODIGO)+"%' ");
	);
	},'Cadastro de Kits');
	,oBrowse:SetFilterDefault("ZZ7_TIPO=='S'"),If(Empty(cRotChm),nil,oDlg:end())}
	Local nTam		:= 0
	Private cString	:= ""
	Default cFiltr	:= ""
	Private cRotChm	:= ccodacu
	Public cRet132	:= "" 

	Default cFiltr	:= ""
	Private cRotChm	:= ccodacu
	If Empty(cFiltr)
		oDlg := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Kits',,,,nOr(WS_VISIBLE,WS_POPUP),,,,,.T.,,,,)
		oBrowse			:= FWFORMBROWSE():New()
		oBrowse:SetDataTable(.T.)
		oBrowse:DisableDetails()
		oBrowse:SetAlias("ZZ7")
		oBrowse:SetSeek()
		oBrowse:SetProfileID("SASP132")
		oBrowse:SetUseFilter()
		oBrowse:SetFilterDefault("ZZ7_TIPO=='S'")
		oBrowse:AddButton("Abrir Arvore",bClique,,3,,.F.)

		ADD COLUMN oColumn DATA { || Iif(ZZ7->ZZ7_TIPO=="S","folder5","folder12") }			TITLE " "		IMAGE DOUBLECLICK bClique SIZE 001 OF oBrowse
		ADD COLUMN oColumn DATA { || Space(2*Len(RTrim(ZZ7->ZZ7_CODPAI)))+Iif(ZZ7->ZZ7_TIPO=="S",ZZ7->ZZ7_CODIGO,ZZ7_PRODUT)   } DOUBLECLICK bClique	TITLE GetSx3Cache("ZZ7_CODIGO","X3_TITULO")    	PICTURE GetSx3Cache("ZZ7_CODIGO","X3_PICTURE") SIZE  GetSx3Cache("ZZ7_CODIGO","X3_TAMANHO") OF oBrowse
		ADD COLUMN oColumn DATA { || ZZ7->ZZ7_DESCR   } DOUBLECLICK bClique		TITLE GetSx3Cache("ZZ7_DESCR","X3_TITULO")    	PICTURE GetSx3Cache("ZZ7_DESCR","X3_PICTURE") SIZE  GetSx3Cache("ZZ7_DESCR","X3_TAMANHO") OF oBrowse
		oBrowse:Activate(oDlg)

		oDlg:Activate(,,,.T.)
	Else
		Processa({|| Geraarv(cRotChm,cFiltr) },'Cadastro de Kits')
	EndIf

return

/*/{Protheus.doc} Geraarv
Gera��o da Arvore
@author author
@since 14/08/2015
@version version
@return return, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function Geraarv(ccodacu,cFiltr)

	Local oButton1
	Local oButton2
	Local oButton3
	Local oButton4
	Local oPanel1
	Local cUserInclusao  := GETMV('SA_ZZ7INCL')
	Local cUserAlteracao := GETMV('SA_ZZ7ALTR')
	Local cUserExclusao  := GETMV('SA_ZZ7EXCL')
	Local cSoma := 045

	Local cquery := ""
	Local calias1 := ""
	Local cpaitodos := "000000"
	Local oDlg
	Private oTreeP34
	Private aParams	:= {}
	Private  cNaoCodPai := "'0'"
	Private lGerar		:= .F.
	Private cFiltr_ := cFiltr

	ProcRegua(0)
	DEFINE MSDIALOG oDlg TITLE "Kits" FROM 000, 000  TO 570, 1300 COLORS 0, 16777215 PIXEL

	@ 015, 572 BUTTON oButton1 PROMPT "Pesquisar" SIZE 064, 013 OF oDlg ACTION ZZ7PESQ(oTreeP34) PIXEL
	@ 030, 572 BUTTON oButton1 PROMPT "Filtrar" SIZE 064, 013 OF oDlg ACTION ZZ7FIL(oTreeP34) PIXEL

	@ cSoma, 572 BUTTON oButton3 PROMPT "Visualizar" SIZE 064, 013 OF oDlg ACTION ZZ7VISUAL(oTreeP34:GETCARGO(),oTreeP34) PIXEL
	cSoma += 15
	@ cSoma, 572 BUTTON oButton5 PROMPT "Atualizar" SIZE 064, 013 OF oDlg ACTION (lGerar:= .T.,oDlg:END() ) PIXEL
	cSoma += 15

	@ cSoma, 572 BUTTON oButton6 PROMPT "Selecionar" SIZE 064, 013 OF oDlg ACTION (lSair:= .T.,oDlg:END(),cRet132:= oTreeP34:GETCARGO() ) PIXEL 
	cSoma += 15	


	@ cSoma, 572 BUTTON oButton6 PROMPT "Sair" SIZE 064, 013 OF oDlg ACTION (lSair:= .T.,oDlg:END()) PIXEL

	oTreeP34 := DbTree():New(2,2,((oDlg:nClientHeight)/2)-15,((oDlg:nClientWidth)/2)-100,oDlg,,,.T.,.F.)


	oTreeP34:BeginUpdate()


	// select para verificar toda a �rea
	cquery := " select ZZ7_CODIGO as CODIGO , ZZ7_CODIGO , ZZ7_DESCR , ZZ7_PRODUT, ZZ7_CODPAI,ZZ7_TIPO,R_E_C_N_O_ "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery += " and D_E_L_E_T_ = ' ' "
	//cquery += " and ZZ7_TIPO = 'S' "
	If !empty(cFiltr_)
		cquery += " and "+Alltrim(cFiltr_)+ ""
	Endif

	cquery += "  ORDER BY ZZ7_TIPO DESC,ZZ7_CODPAI, CODIGO "

	If Select("TO1") > 0
		dbSelectArea("TO1")
		TO1->(dbCloseArea())
	Endif

	TCQuery cquery new alias TO1

	while TO1->(!eof())

		//Incproc('Processando KITS...')

		ccargo := TO1->ZZ7_CODIGO

		oTreeP34:TreeSeek(TO1->ZZ7_CODPAI)

		If TO1->ZZ7_TIPO = 'A'
			oTreeP34:AddItem(Alltrim(TO1->ZZ7_PRODUT) + " - " + Posicione("SB1",1,xFilial("SB1")+TO1->ZZ7_PRODUT,"B1_DESC" ),TO1->ZZ7_CODIGO,"FOLDER12","FOLDER13",,,2)
			Incproc('Processando KITS...: '+TO1->ZZ7_CODIGO)
		Else
			oTreeP34:AddItem(Alltrim(TO1->ZZ7_CODIGO) + " - " + TO1->ZZ7_DESCR,TO1->ZZ7_CODIGO,"FOLDER6","FOLDER6",,,2)
			Incproc('Processando KITS...: '+TO1->ZZ7_CODIGO)
		Endif

		oTreeP34:PTCollapse()

		TO1->(dbskip())

	enddo

	TO1->(dbclosearea())

	oTreeP34:EndUpdate()

	oTreeP34:PTCollapse()

	Activate MsDialog oDlg CENTERED
	//	ZZ7->(DbSetFilter(&("{|| "+cFiltro+" }"),cFiltro))
	Release object oTreeP34

	If lGerar
		Geraarv(cRotChm,cFiltr)
	Endif

return

/*/{Protheus.doc} ZZ7ATUALIZA
Atualiza��o da arvore
@author author
@since 14/08/2015
@version version
@return return, return_description
@example
(examples)
@see (links_or_references)
/*/
STATIC FUNCTION ZZ7ATUALIZA(oTreeP34,aParams,cSeek,cPai)

	aRetDados:= {}
	oTreeP34:Reset()

	oTreeP34:BeginUpdate()
	Private cSelecion:= ""

	cNaoCodPai := "'0'"

	If !empty(aParams[1])
		cSelecion:= aParams[1]
	Endif

	// select para verificar toda a �rea

	cquery := " Select ZZ7_CODIGO as CODIGO , ZZ7_CODIGO , ZZ7_DESCR , ZZ7_CODPAI,ZZ7_TIPO,ZZ7_PRODUT "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery += " and D_E_L_E_T_ <> '*' "
	If !empty(cFiltr_)
		cquery += " and "+Alltrim(cFiltr_)+ ""
	Endif
	If !empty(aParams[1])
		//cquery += " and (ZZ7_CODPAI LIKE '%"+aParams[1]+"%' or ZZ7_CODIGO LIKE '%"+aParams[1]+"%') "
		cquery += " and ZZ7_CODIGO in ( "+FRetRecurs(aParams[1])+")  "
		cquery += " and ((ZZ7_TIPO = 'A' AND ZZ7_CODPAI= ('"+cPai+"')) OR ZZ7_TIPO = 'S')  "
	Endif
	cquery += "  ORDER BY ZZ7_TIPO DESC,ZZ7_CODPAI,CODIGO"


	If Select("T01") > 0
		dbSelectArea("T01")
		T01->(dbCloseArea())
	Endif

	TCQuery cquery new alias T01

	while T01->(!eof())

		Incproc('Gerando KITS...')

		ccargo := T01->ZZ7_CODIGO

		oTreeP34:TreeSeek(T01->ZZ7_CODPAI)

		If T01->ZZ7_TIPO = 'A'
			oTreeP34:AddItem(Alltrim(T01->ZZ7_PRODUT) + " - " + Posicione("SB1",1,xFilial("SB1")+T01->ZZ7_PRODUT,"B1_DESC" ),T01->ZZ7_CODIGO,"FOLDER12","FOLDER13",,,2)
		Else
			oTreeP34:AddItem(Alltrim(T01->ZZ7_CODIGO) + " - " + T01->ZZ7_DESCR,T01->ZZ7_CODIGO,"FOLDER6","FOLDER6",,,2)
		Endif
		oTreeP34:PTCollapse()

		T01->(dbskip())

	enddo

	If !empty(cSeek)
		oTreeP34:TreeSeek(cSeek)
	Endif

	T01->(dbclosearea())

	oTreeP34:EndUpdate()

	//oTreeP34:Refresh()

	Release object oTreeP34

RETURN

Static Function FRetRecurs(cParm)

	Local cRet:= ""
	Local cQry:= ""

	cQry := " Select ZZ7_CODIGO "
	cQry += " from " + retsqlname('ZZ7') + " ZZ7 "
	cQry += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cQry += " and D_E_L_E_T_ <> '*' "
	cQry += " and ZZ7_CODPAI = '"+cParm+"' "
	cQry += "  ORDER BY ZZ7_CODIGO "

	If Select("TREC001") > 0
		dbSelectArea("TREC001")
		TREC001->(dbCloseArea())
	Endif

	TCQuery cQry new alias TREC001

	If TREC001->(eof()) //Final da itera��o
		TREC001->(dbCloseArea())
	Else
		while !TREC001->(eof())
			Aadd(aRetDados,TREC001->ZZ7_CODIGO)
			//FRetRecurs(aRetDados[len(aRetDados)])
			TREC001->(dbSkip())
		enddo

		TREC001->(dbCloseArea())

		If aRetDados[len(aRetDados)] <> cParm
			FRetRecurs(aRetDados[len(aRetDados)])
		Endif

	Endif

	For i:=1 To len(aRetDados)
		cRet+= "'"+aRetDados[i]+"',"
	Next

	If len(aRetDados) > 0
		cRet+= "'"+cSelecion+"'"
	Endif
	If len(aRetDados) == 0
		cRet+= "''"
	Endif

Return cRet


/*/{Protheus.doc} ZZ7INCLUI
Inclus�o do registro ZZ7
@author author
@since 14/08/2015
@version version
@return return, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function ZZ7INCLUI(oTreeP34)
	cCadastro:= "Cadastro do kit"
	cNUMZ7	:= ""
	cNUMPAI	:= ""
	Inclui	:= .T.

	M->ZZ7_CODPAI:=oTreeP34:GETCARGO()
	AxInclui("ZZ7",,3,,,,"U_FRETSQ132()")


	PUTMV("SA_GERKIT", "N")

	If !empty(cNUMZ7)
		Aviso("Confirma��o","Confirmado a inclus�o do kit "+cNUMZ7,{"Ok"})

	Endif

Return

User Function FRetSq132()

	Local lRet:= .T.

	while .T.
		If AllTrim(SuperGetMV("SA_GERKIT",.F.,'N')) == 'N'
			exit
		endif
	enddo

	PUTMV("SA_GERKIT", "S")


	Begin Transaction


		//Verifica se existe outra numera��o para a mesma sequencia
		cQuery:="SELECT COUNT(*) QTD FROM "+RetSqlName("ZZ7")+" ZZ7 "
		cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
		cQuery+=" ZZ7_CODPAI = '"+M->ZZ7_CODPAI+"' AND "
		cQuery+=" ZZ7_SEQ = '"+M->ZZ7_SEQ+"' "

		TCQuery cquery new alias TQrySEQ

		If TQrySEQ->(!eof()) .and. M->ZZ7_TIPO = 'A' .and. TQrySEQ->QTD > 0 //Analitico

			//Verifica se existe outra numera��o para a mesma sequencia
			cQuery:="SELECT MAX(ZZ7_SEQ)+1 QTD FROM "+RetSqlName("ZZ7")+" ZZ7 "
			cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
			cQuery+=" ZZ7_CODPAI = '"+M->ZZ7_CODPAI+"' "

			TCQuery cquery new alias TQrySEQ01

			cProxSeq	:= 	cValtochar(TQrySEQ01->QTD)
			M->ZZ7_SEQ	:=	cProxSeq

			TQrySEQ01->(dbCloseArea())

			Aviso("Aten��o","A numera��o j� foi utilizada para outro cadastro, nova numera��o utilizada: "+cProxSeq,{"Ok"})

		Endif

		TQrySEQ->(dbCloseArea())

		If  M->ZZ7_TIPO = 'S'

			//Verifica se j� incluido outra numera��o
			cQuery:="SELECT COUNT(*) QTD FROM "+RetSqlName("ZZ7")+" ZZ7 "
			cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
			cQuery+=" ZZ7_CODIGO = '"+M->ZZ7_CODIGO+"' AND "
			cQuery+=" ZZ7_TIPO = 'S' "

			TCQuery cquery new alias TQrySEQ

			If TQrySEQ->(!eof()) .and. TQrySEQ->QTD > 0 //Analitico

				//Verifica se existe outra numera��o para a mesma sequencia
				cQuery:="SELECT MAX(ZZ7_SEQ)+1 QTD FROM "+RetSqlName("ZZ7")+" ZZ7 "
				cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
				cQuery+=" ZZ7_CODIGO = '"+M->ZZ7_CODIGO+"' AND "
				cQuery+=" ZZ7_TIPO = 'S' "

				TCQuery cquery new alias TQrySEQ01

				cProxSeq		:= 	cValtochar(TQrySEQ01->QTD)
				M->ZZ7_SEQ		:=	cProxSeq
				M->ZZ7_CODIGO	:=	Alltrim(M->ZZ7_CODPAI)+Alltrim(M->ZZ7_SEQ)

				TQrySEQ01->(dbCloseArea())

				Aviso("Aten��o","C�digo do kit j� utilizado para anteriormente, nova numera��o utilizada "+M->ZZ7_CODIGO,{"Ok"})
				lRet:= .T.
			Endif

			TQrySEQ->(dbCloseArea())

		Endif

	End Transaction

	PUTMV("SA_GERKIT", "N")

	If lRet
		cNUMZ7:= M->ZZ7_CODIGO
	Endif

Return lRet


/*/{Protheus.doc} ZZ7Altera
Altera��o do registro da ZZ7
@author author
@since 14/08/2015
@version version
@return return, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function ZZ7ALTERA(cCargo,oTreeP34)

	Local cCargo
	Local Cod  := ""
	Local _Cod := ""
	Local cPaiIni	:= Space(GetSx3Cache("ZZ7_CODIGO","X3_TAMANHO"))
	Local lOk := .F.
	Local aParam := {}
	Local aRetParm		:= {}
	cCadastro	:= "Cadastro do kit"

	cQuery:=" SELECT COUNT(ZZ7_CODIGO) as QUANT FROM "+RetSqlName("ZZ7")+" ZZ7 "
	cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
	cQuery+=" ZZ7_CODIGO = '"+cCargo+"'  "
	If Select("T03") > 0
		dbSelectArea("T03")
		T03->(dbCloseArea())
	Endif
	TcQuery cQuery new Alias T03

	If T03->QUANT == 1
		_Cod := POSICIONE("ZZ7",1,XFILIAL("ZZ7")+cCargo,"ZZ7_CODIGO")
		cQuery:=" SELECT R_E_C_N_O_  FROM "+RetSqlName("ZZ7")+" ZZ7 "
		cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
		cQuery+=" ZZ7_CODIGO = '"+cCargo+"'  "

		If Select("T17") > 0
			dbSelectArea("T17")
			T17->(dbCloseArea())
		Endif

		TcQuery cQuery new Alias T17

		BEGIN TRANSACTION
			AxAltera("ZZ7",T17->R_E_C_N_O_,4,,,,,,,,,,)
		END TRANSACTION

		T17->(dbCloseArea())

	ELSEIF  T03->QUANT > 1
		aAdd(aParam,{1,"Cod Pai"		,cPaiIni	,GetSx3Cache("ZZ7_CODIGO","X3_PICTURE"),".T.","",".T.",80,.F.})
		If ParamBox(aParam,"Filtrar Medi��o",@aRetParm,{||.T.},,,,,,"U_SASP036",.T.,.T.)
			lOk	:= .T.
		EndIf

		if lOk .and. !Empty(aRetParm[1])
			_Cod := POSICIONE("ZZ7",1,XFILIAL("ZZ7")+cCargo+aRetParm[1],"ZZ7_CODIGO")
			cQuery:=" SELECT R_E_C_N_O_ FROM "+RetSqlName("ZZ7")+" ZZ7 "
			cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
			cQuery+=" ZZ7_CODPAI = '"+aRetParm[1]+"' AND "
			cQuery+=" ZZ7_CODIGO = '"+_Cod+"'  "

			If Select("T17") > 0
				dbSelectArea("T17")
				T17->(dbCloseArea())
			Endif


			TcQuery cQuery new Alias T17

			BEGIN TRANSACTION
				AxAltera("ZZ7",T17->R_E_C_N_O_,3,,,,,,,,,,)
			END TRANSACTION

			T17->(dbCloseArea())
		endif
	Endif

	T03->(dbclosearea())
	//ZZ7ATUALIZA(oTreeP34,{""},"")


RETURN

/*/{Protheus.doc} ZZ7VISUAL
Visualiza��o do Item
@author Jo�o Filho
@since 14/08/2015
@version version
@return return, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function ZZ7VISUAL(cCargo,oTreeP34)

	Local cCargo
	Local cAlias:= oTreeP34:cArqTree
	Local cPaiIni	:= Space(GetSx3Cache("ZZ7_CODIGO","X3_TAMANHO"))
	Local lOk := .F.
	Local aParam := {}
	Local aRetParm		:= {}
	Local _Cod 		:= ""

	cCadastro:="Cadastro de Kit"

	cQuery:=" SELECT COUNT(ZZ7_CODIGO) as QUANT FROM "+RetSqlName("ZZ7")+" ZZ7 "
	cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
	cQuery+=" ZZ7_CODIGO = '"+cCargo+"'  "

	If Select("QZZ7VIS") > 0
		dbSelectArea("QZZ7VIS")
		QZZ7VIS->(dbCloseArea())
	Endif

	TcQuery cQuery new Alias QZZ7VIS

	If QZZ7VIS->QUANT == 1
		_Cod := POSICIONE("ZZ7",1,XFILIAL("ZZ7")+cCargo,"ZZ7_CODIGO")
		cQuery:=" SELECT R_E_C_N_O_  FROM "+RetSqlName("ZZ7")+" ZZ7 "
		cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
		cQuery+=" ZZ7_CODIGO = '"+cCargo+"'  "

		If Select("T16") > 0
			dbSelectArea("T16")
			T16->(dbCloseArea())
		Endif

		TcQuery cQuery new Alias T16

		AxVisual("ZZ7",T16->R_E_C_N_O_,2,,,,,,,,,,)

		T16->(dbCloseArea())

	ELSEIF  QZZ7VIS->QUANT > 1
		aAdd(aParam,{1,"Cod Pai"		,cPaiIni	,GetSx3Cache("ZZ7_CODIGO","X3_PICTURE"),".T.","",".T.",80,.F.})
		If ParamBox(aParam,"Filtrar Medi��o",@aRetParm,{||.T.},,,,,,"U_SASP036",.T.,.T.)
			lOk	:= .T.
		EndIf

		if lOk .and. !Empty(aRetParm[1])
			_Cod := POSICIONE("ZZ7",1,XFILIAL("ZZ7")+cCargo+aRetParm[1],"ZZ7_CODIGO")
			cQuery:=" SELECT R_E_C_N_O_ FROM "+RetSqlName("ZZ7")+" ZZ7 "
			cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
			cQuery+=" ZZ7_CODPAI = '"+aRetParm[1]+"' AND "
			cQuery+=" ZZ7_CODIGO = '"+_Cod+"'  "

			If Select("T16") > 0
				dbSelectArea("T16")
				T16->(dbCloseArea())
			Endif

			TcQuery cQuery new Alias T16

			AxVisual("ZZ7",T16->R_E_C_N_O_,2,,,,,,,,,,)

			T16->(dbCloseArea())
		endif
	Endif

	QZZ7VIS->(dbCloseArea())


RETURN

/*/{Protheus.doc} ZZ7DEL
Exclus�o do item na arvore
@author Jo�o Filho
@since 14/08/2015
@version version
@return return, return_description
@example
(examples)
@see (links_or_references)
/*/

Static Function ZZ7DEL(cCargo,oTreeP34)

	Local cCargo
	Local Cod := ""

	ZZ7->(dbSetOrder(1))
	If !(ZZ7->(dbSeek(xFilial("ZZ7")+cCargo)))
		Aviso("Aten��o","Item n�o localizado ("+alltrim(cCargo)+")",{"Ok"})
		Return .F.
	Endif

	If  ZZ7->ZZ7_TIPO = 'S'
		cQuery:=" SELECT COUNT(*) QTD FROM "+RetSqlName("ZZ3")+" ZZ3 "
		cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
		cQuery+=" ZZ3_PRODUT = '"+ZZ7->ZZ7_CODIGO+"' "

		If Select("QZZ702") > 0
			dbSelectArea("QZZ702")
			QZZ702->(dbCloseArea())
		Endif

		TcQuery cQuery new Alias QZZ702

		If QZZ702->QTD > 0
			Aviso("Aten��o","N�o permitido exclus�o, Kit j� utilizado para medi��o ",{"ok"})
			QZZ702->(dbCloseArea())
			Return .F.
		Endif

		QZZ702->(dbCloseArea())
	ELSEIF ZZ7->ZZ7_TIPO = 'A'
		cQuery:=" SELECT COUNT(*) QTD FROM "+RetSqlName("ZZ3")+" ZZ3 "
		cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
		cQuery+=" ZZ3_PRODUT = '"+ZZ7->ZZ7_CODPAI+"' "

		If Select("QZZ702") > 0
			dbSelectArea("QZZ702")
			QZZ702->(dbCloseArea())
		Endif

		TcQuery cQuery new Alias QZZ702

		If QZZ702->QTD > 0
			Aviso("Aten��o","N�o permitido exclus�o, Kit j� utilizado para medi��o ",{"ok"})
			QZZ702->(dbCloseArea())
			Return .F.
		Endif

		QZZ702->(dbCloseArea())
	Endif

	If ZZ7->ZZ7_TIPO = 'S' //N�vel Sint�tico
		cQuery:="SELECT COUNT(*) QTD FROM "+RetSqlName("ZZ7")+" ZZ7 "
		cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
		cQuery+=" ZZ7_CODPAI = '"+ZZ7->ZZ7_CODIGO+"' AND "
		cQuery+=" ZZ7_TIPO IN ('A','S') "

		If Select("QZZ701") > 0
			dbSelectArea("QZZ701")
			QZZ701->(dbCloseArea())
		Endif

		TcQuery cQuery new Alias QZZ701

		If QZZ701->QTD > 0
			Aviso("Aten��o","N�o permitido exclus�o, necess�rio excluir os filhos do n�vel selecionado",{"ok"})
			QZZ701->(dbCloseArea())
			Return .F.
		Endif

		QZZ701->(dbCloseArea())
	Endif

	If MsgYesNo("Confirma exclus�o do Kit selecionado ?")
		RecLock("ZZ7",.F.)
		ZZ7->(dbDelete())
		MsUnlock()

		//ZZ7ATUALIZA(oTreeP34,{""},"")

		Aviso("Aten��o","Exclus�o realizada com sucesso",{"Ok"})
	Endif

Return .T.



/*/{Protheus.doc} ZZ7PESQ
Pesquisa na arvore
@author Diogo
@since 14/08/2015
@version version
@return return, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function ZZ7PESQ(oTreeP34)

	Local cDesc		:= Space(GetSx3Cache("ZZ7_DESCR","X3_TAMANHO"))
	Local cCodKit	:= Space(GetSx3Cache("ZZ7_CODIGO","X3_TAMANHO"))
	Local aParam	:= {}
	Local aRet		:= {}
	Local Alias   := oTreeP34:cArqTree


	aAdd(aParam,{1,"Kit / Produto "		,cCodKit	,GetSx3Cache("ZZ7_CODIGO","X3_PICTURE"),".T.","",".T.",80,.F.})
	If !ParamBox(aParam,"Consulta Kit",@aRet,,,,,,,"U_YZZ0132",.T.,.T.)
		Return .F.
	Else
		cquery := " Select TOP 1 * "
		cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
		cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
		cquery += " and ZZ7_CODIGO = '"+aRet[1]+"' "
		cquery += " and D_E_L_E_T_ <> '*' "
		cquery += "  ORDER BY ZZ7_TIPO DESC,ZZ7_CODPAI,ZZ7_CODIGO"

		If Select("T01") > 0
			dbSelectArea("T01")
			T01->(dbCloseArea())
		Endif

		TCQuery cquery new alias T01

		IF T01->ZZ7_TIPO = 'A'
			(Alias)->(dbsetorder(4))
			(Alias)->(dbSeek(aRet[1],.F.))
			if !alltrim(T01->ZZ7_CODPAI) $ cNaoCodPai
				oTreeP34:TreeSeek(T01->ZZ7_CODPAI)
				cNaoCodPai+=",'"+alltrim(T01->ZZ7_CODPAI)+"'"
			else
				(Alias)->(dbskip())

				cquery := " Select TOP 1 * "
				cquery += " from " + retsqlname('ZZ7') + " ZZ7  "
				cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
				cquery += " and ZZ7_CODIGO NOT IN ("+cNaoCodPai+") "
				cquery += " and ZZ7_TIPO = 'S'  "
				cquery += " and D_E_L_E_T_ <> '*' "
				cquery += " AND ZZ7_CODIGO = ( "
				cquery += " 					SELECT TOP 1 ZZ7_CODPAI "
				cquery += " 					FROM  ZZ7010 ZZ7 "
				cquery += " 					WHERE ZZ7_TIPO = 'A'  "
				cquery += " 					and D_E_L_E_T_ <> '*' "
				cquery += " 					AND ZZ7_CODIGO = '"+aRet[1]+"' "
				cquery += " 					and ZZ7_CODPAI NOT IN ("+cNaoCodPai+") "
				cquery += " 				  ) "
				cquery += " ORDER BY ZZ7_TIPO DESC,ZZ7_CODPAI,ZZ7_CODIGO "
				TCQuery cquery new alias T05

				oTreeP34:TreeSeek(T05->ZZ7_CODIGO)
				cNaoCodPai+=",'"+alltrim(T05->ZZ7_CODIGO)+"'"
				T05->(dbclosearea())
			endif
		ELSEIF T01->ZZ7_TIPO = 'S'
			oTreeP34:TreeSeek(aRet[1])
			cNaoCodPai := "'0'"
		ENDIF
		T01->(dbclosearea())

	Endif

Return

/*/{Protheus.doc} ZZ7FIL
Pesquisa na arvore
@author Diogo
@since 14/08/2015
@version version
@return return, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function ZZ7FIL

	Local cDesc		:= Space(GetSx3Cache("ZZ7_DESCR","X3_TAMANHO"))
	Local cCodKit	:= Space(GetSx3Cache("ZZ7_CODIGO","X3_TAMANHO"))
	Local aParam	:= {}
	Local aRet		:= {}

	aAdd(aParam,{1,"Kit  "			,cCodKit,GetSx3Cache("ZZ7_CODIGO","X3_PICTURE")	,".T.","",".T.",80,.F.})
	aAdd(aParam,{1,"Descri��o  "	,cDesc	,GetSx3Cache("ZZ7_DESCR","X3_PICTURE")	,".T.","",".T.",80,.F.})

	If !ParamBox(aParam,"Consulta Kit",@aRet,,,,,,,"U_YZZ0132",.T.,.T.)
		Return .F.
	Else
		if alltrim(aRet[1])<>""
			ZZ7ATUALIZA(oTreeP34,{aRet[1]},,aRet[1])
		else
			cQuery:="SELECT *  FROM "+RetSqlName("ZZ7")+" ZZ7 "
			cQuery+=" WHERE D_E_L_E_T_ =' ' AND "
			cQuery+=" ZZ7_DESCR LIKE '%"+ALLTRIM(aRet[2])+"%' "

			TcQuery cQuery new Alias QZZ702

			ZZ7ATUALIZA(oTreeP34,{QZZ702->ZZ7_CODIGO},,ALLTRIM(QZZ702->ZZ7_CODIGO))

			QZZ702->(DBCLOSEAREA())
		endif

	Endif

Return

Static Function fPergsP132

	Local nValT		:= 0
	Local aParam	:= {}
	Local aRet		:= {}
	Local aPrm		:= {}
	Local cTES		:= space(GetSx3Cache("F4_CODIGO","X3_TAMANHO"))

	aAdd(aParam,{1,"Valor Total ? "	,nValT	,GetSx3Cache("E2_VALOR","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{1,"TES ? "	,cTES	,GetSx3Cache("F4_CODIGO","X3_PICTURE"),".T.","SF4",".T.",80,.F.})

	If !ParamBox(aParam,"Consulta Kit",@aRet,,,,,,,"U_YZZ1321",.T.,.T.)
		Return .F.
	Else
		aPrm:= {aRet[1],aRet[2]}
	Endif

Return aPrm

Static Function fPergsAlter

	Local nValT		:= 0
	Local aParam	:= {}
	Local aRet		:= {}
	Local aPrm		:= {}
	Local cTES		:= space(GetSx3Cache("F4_CODIGO","X3_TAMANHO"))

	//aAdd(aParam,{1,"Valor Total ? "	,nValT	,GetSx3Cache("E2_VALOR","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aParam,{1,"TES ? "	,cTES	,GetSx3Cache("F4_CODIGO","X3_PICTURE"),".T.","SF4",".T.",80,.F.})

	If !ParamBox(aParam,"Consulta Kit",@aRet,,,,,,,"U_YZZ1321",.T.,.T.)
		Return .T.
	Else
		aPrm:= {0,aRet[1]}
	Endif

Return aPrm


STATIC FUNCTION ATUALIZAPES(oTreeP34,aParams,cSeek)


	aRetDados:= {}
	oTreeP34:Reset()
	oTreeP34:BeginUpdate()
	Private cSelecion:= ""

	If !empty(aParams[1])
		cSelecion:= aParams[1]
	Endif

	// select para verificar toda a �rea

	cquery := " Select ZZ7_CODIGO, ZZ7_DESCR , ZZ7_CODPAI,ZZ7_TIPO,ZZ7_PRODUT "
	cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
	cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
	cquery += " and ZZ7_CODPAI not in ("+cNaoCodPai+") "
	cquery += " and ZZ7_CODIGO not in ("+cNaoCod+") "
	cquery += " and D_E_L_E_T_ = ' ' "
	If !empty(cFiltr_)
		cquery += " and "+Alltrim(cFiltr_)+ ""
	Endif
	If !empty(aParams[1])
		//cquery += " and (ZZ7_CODPAI LIKE '%"+aParams[1]+"%' or ZZ7_CODIGO LIKE '%"+aParams[1]+"%') "
		cquery += " and (ZZ7_CODIGO in ( "+FRetRecurs(aParams[1])+") ) "
	Endif
	cquery += "  ORDER BY ZZ7_TIPO DESC,ZZ7_CODPAI,ZZ7_CODIGO"

	If Select("T01") > 0
		dbSelectArea("T01")
		T01->(dbCloseArea())
	Endif

	TCQuery cquery new alias T01

	while T01->(!eof())

		Incproc('Gerando KITS...')

		ccargo := T01->ZZ7_CODIGO

		oTreeP34:TreeSeek(T01->ZZ7_CODPAI)

		If T01->ZZ7_TIPO = 'A'
			oTreeP34:AddItem(Alltrim(T01->ZZ7_PRODUT) + " - " + Posicione("SB1",1,xFilial("SB1")+T01->ZZ7_PRODUT,"B1_DESC" ),T01->ZZ7_CODIGO,"FOLDER12","FOLDER13",,,2)
		Else
			oTreeP34:AddItem(Alltrim(T01->ZZ7_CODIGO) + " - " + T01->ZZ7_DESCR,T01->ZZ7_CODIGO,"FOLDER6","FOLDER6",,,2)
		Endif

		T01->(dbskip())

	enddo

	If !empty(cSeek)
		oTreeP34:TreeSeek(cSeek)

		cquery := " Select TOP 1  ZZ7_CODIGO, ZZ7_DESCR ,ZZ7_TIPO, ZZ7_CODPAI,ZZ7_TIPO,ZZ7_PRODUT "
		cquery += " from " + retsqlname('ZZ7') + " ZZ7 "
		cquery += " where ZZ7_FILIAL = '" + xfilial("ZZ7") + "' "
		cquery += " and D_E_L_E_T_ <> '*' "
		cquery += " and ZZ7_CODIGO ='"+cSeek+"' "
		
		If Select("T02") > 0
			dbSelectArea("T02")
			T02->(dbCloseArea())
		Endif

		TCQuery cquery new alias T02

		IF  T02->ZZ7_TIPO == 'A'
			cNaoCodPai += ",'"+alltrim(T02->ZZ7_CODPAI)+"'"
		ELSEIF T02->ZZ7_TIPO == 'S'
			cNaoCod += ",'"+alltrim(T02->ZZ7_CODIGO)+"'"
		ENDIF

		T02->(dbclosearea())

	Endif

	T01->(dbclosearea())

	oTreeP34:EndUpdate()

	oTreeP34:Refresh()

	Release object oTreeP34

Return