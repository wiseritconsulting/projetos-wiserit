#include 'protheus.ch'
#include 'parmtype.ch'

//*********************************************************************************************************//
// Autor:    Joao Filho 																				   //
// Objetivo: Fun��o que valida o tipo de pedido de venda C5_YPEDTV, QUANDO FOR DO TIPO TRANSFERENCIA ("T") //
//           VERIFICA SE O CLIENTE INFORMADO � REFERENTE A UMA FILIAL DO SAS.                              //
//*********************************************************************************************************//


user function SASP131()
	Local lRet := .T.
	Local aArea
	Local cCgcCli
	Local cFilPc := ""


	if  ALLTRIM(M->C5_YPEDTV) == "T"

		aArea := getarea()

		cCgcCli := POSICIONE("SA1",1,XFILIAL("SA1") + M->C5_CLIENTE + M->C5_LOJACLI,"A1_CGC")

		DbSelectArea("SM0")
		dbGOTOP() 
		While SM0->(!EOF())
			If ALLTRIM(SM0->M0_CGC) == ALLTRIM(cCgcCli)
				cFilPc	:= ALLTRIM(SM0->M0_CODFIL)
			EndIF
			SM0->(DbSkip())
		EndDo
		
		if alltrim(cFilPc) == ""
			lRet := .F.
		endif
		RestArea(aArea)

	endif

return lRet