#Include "TopConn.Ch"
#Include "Protheus.Ch"
#Include "Totvs.Ch"
#Include "Rwmake.Ch"
#Include "TbiConn.Ch"
#Include "TbiCode.Ch"

/*/{Protheus.doc} RF051
Tela Controle de Titularidade de OS - Intercompany
@author Diogo
@since 30/10/2016
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User Function RF051()

	Private aDados     := {} //Array com os campos do MsBrGetDBase.
	Private aDados2    := {} //Array com os campos para inclus�o tabela ZZQ
	Static oDlg

	PREPARE ENVIRONMENT EMPRESA '01' Filial '050101' //USER 'totvs' PASSWORD 'totvs' TABLES ""

	FRF051()

Return

Static Function FRF051()

	__cInterNet		  := nil
	Private oLista
	Private cMvLibFat := GetMv("SA_ETAPA")  //Par�metro para identifica��o da fase para faturamento
	Private cMvFetapa := GetMv("SA_FETAPA") //Par�metro para identifica��o da fase final do processo
	Private cCodEtp   := ""
	Private _nOpc     := 0
	Private cOrdem    := Space(20)
	Private cPedPCE := Space(GetSx3Cache("ZC3_PEDIDO","X3_TAMANHO"))
	Private cCodCli   := Space(GetSx3Cache("ZC3_CODCLI","X3_TAMANHO"))
	Private cCliente  := Space(GetSx3Cache("ZC3_NOMECL","X3_TAMANHO"))
	Private cCodUse   := Space(15)
	Private cNomeUse  := Space(GetSx3Cache("ZC3_NOMUSE","X3_TAMANHO"))
	Private cCodEtp   := Space(GetSx3Cache("ZC3_ETAPA","X3_TAMANHO"))
	Private cEtapa    := Space(GetSx3Cache("ZC3_NOMETP","X3_TAMANHO"))
	Private cCodPetp  := Space(GetSx3Cache("ZC3_ETAPA","X3_TAMANHO"))
	Private cPetapa   := Space(GetSx3Cache("ZC3_NOMETP","X3_TAMANHO"))
	Private dEmissao  := Space(GetSx3Cache("ZC3_EMISSA","X3_TAMANHO"))
	Private cObs      := Space(GetSx3Cache("ZC3_OBS","X3_TAMANHO"))
	Private cDesSer   := ""


	oFont  := TFont():New( "Arial",0,18,,.T.,0,,700,.F.,.F.,,,,,, )
	oFont2 := TFont():New( "Arial",0,22,,.T.,0,,700,.F.,.F.,,,,,, )
	DEFINE MSDIALOG oDlg TITLE "Controle de Titularidade de OS" FROM 000, 000  TO 700, 900 COLORS 0, 16777215 PIXEL
	oDlg:SetFont(oFont)

	oSayOrdem	:= TSay():New( 020,020,{||"Pedido"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,032,008)
	oGet1	    := TGet():New( 032,020,{|u|If(PCount()>0,cOrdem:=u,cOrdem)},,100,015,'@!',{||FBuscaPed(cOrdem),ItensBrow(),.T.},,,oFont2,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cOrdem",,)

	oSayEmi		:= TSay():New( 020,125,{||"Emiss�o"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,040,008)
	oGet2		:= TGet():New( 032,125,{|u| If(PCount()>0,dEmissao:=u,dEmissao)},,080,015,'@!',{||},,,oFont2,,,.T.,"",,{||.F.},.F.,.F.,,.F.,.F.,"","dEmissao",,)

	oSayFAtu	:= TSay():New( 020,210,{||"Fase Atual"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,052,008)
	oGet3		:= TGet():New( 032,210,{|u| If(PCount()>0,cEtapa:=u,cEtapa)},,200,015,'@!',{|| .T. },,,oFont2,,,.T.,"",,{||.F.},.F.,.F.,,.F.,.F.,"","cEtapa",,)

	oSayFatu	:= TSay():New( 055,020,{||"Cliente"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,052,008)
	oGet4		:= TGet():New( 065,020,{|u| If(PCount()>0,cCliente:=u,cCliente)},,300,015,'@!',{|| .T. },,,oFont2,,,.T.,"",,{||.F.},.F.,.F.,,.F.,.F.,"","cCliente",,)

	oSaycResp	:= TSay():New( 090,020,{||"C�digo Resp."},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,080,008)
	oGet5		:= TGet():New( 100,020,{|u| If(PCount()>0,cCodUse:=u,cCodUse)},,050,015,'@!',{||FBuscaUser()},,,oFont2,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cCodUse",,)

	oSaycResp	:= TSay():New( 090,080,{||"Respons�vel Atual"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,080,008)
	oGet6		:= TGet():New( 100,080,{|u| If(PCount()>0,cNomeUse:=u,cNomeUse)},,300,015,'@!',{||},,,oFont2,,,.T.,"",,{||.F.},.F.,.F.,,.F.,.F.,"","cNomeUse",,)

	oSayFAtu2	:= TSay():New( 125,020,{||"Pr�xima Fase"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,080,008)
	oGet7		:= TGet():New( 135,020,{|u| If(PCount()>0,cPetapa:=u,cPetapa)},,200,015,'@!',{|| .T. },,,oFont2,,,.T.,"",,{||.F.},.F.,.F.,,.F.,.F.,"","cPetapa",,)

	oSayObs		:= TSay():New( 155,020,{||"Observa��o"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,080,008)
	oGet8		:= TGet():New( 165,020,{|u| If(PCount()>0,cObs:=u,cObs)},,300,015,'@!',{|| .T. },,,oFont2,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cObs",,)

	oSBtnConf   := TButton():New( 300,020,"Confimar(F5)",oDlg,{|| IIf(FBtOk(cOrdem),_nOpc:= 1,_nOpc:= 0) },0100,030,,oFont,,.T.,,"Confirmar",,,,.F. )
	oSBtnConf:SetCss(CssbtnImg("ok.png","#006386"))

	oSBtnCanc   := TButton():New( 300,150,"Cancelar(F7)",,{|| oDlg:End(),_nOpc:= 2 },0100,030,,oFont,,.T.,,"Cancelar",,,,.F. )
	oSBtnCanc:SetCss(CssbtnImg("cancel.png","#006386"))

	oDlg:lMaximized := .T.

	//Confirmar
	SET KEY VK_F5 TO
	SETKEY(VK_F5,{||IIf(FBtOk(cOrdem),_nOpc:= 1,_nOpc:= 0)})

	//Cancelar
	SET KEY VK_F7 TO
	SETKEY(VK_F7,{||oDlg:End()})


	ACTIVATE MSDIALOG oDlg CENTERED

	If _nOpc == 1 //Confirma��o
		FRF051()
	Endif

Return

Static Function FBuscaPed(cOrdem)

	Local cCodEtapa := ""

	If Select("T01") <> 0
		T01->(DbCloseArea())
	EndIf

	cCodEtapa := POSICIONE("SC5",1,SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6) , "C5_YETAPA")

		//1 - Filial + Pedido
		cQuery := " SELECT C5_FILIAL,C5_NUM,C5_YNUMIC,C5_CLIENTE,C5_LOJACLI,C5_EMISSAO,C5_YETAPA "
		cQuery += " FROM "+RetSqlName("SC5")+" SC5	"
		cQuery += " WHERE  SC5.D_E_L_E_T_ = ''  "
		cQuery += " AND C5_FILIAL ='" + SubStr(cOrdem,1,6) + "' "
		cQuery += " AND C5_NUM ='" + SubStr(cOrdem,7,6) + "'"
		cQuery += " ORDER BY C5_FILIAL ,C5_NUM  "

		cQuery := ChangeQuery(cQuery)

		DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T01")

		cPedPCE:= T01->C5_NUM
		cCodCli  := T01->C5_CLIENTE
		cCliente := Posicione("SA1",1,xFilial("SA1")+T01->C5_CLIENTE+T01->C5_LOJACLI,"A1_NOME")
		dEmissao := sToD(T01->C5_EMISSAO)
		cCodEtp  := T01->C5_YETAPA
		cEtapa   := POSICIONE("SX5",1,xFilial("SX5")+"ZA"+T01->C5_YETAPA,"X5_DESCRI")

		T01->(dbclosearea())

		If Select("T02") <> 0
			T02->(DbCloseArea())
		EndIf

		//2 - Fases do processo
		cQuery := " SELECT TOP	1 X5_TABELA,X5_CHAVE,X5_DESCRI "
		cQuery += " FROM "+RetSqlName("SX5")+" SX5  "
		cQuery += " WHERE  SX5.D_E_L_E_T_ = ''  "
		cQuery += " AND X5_TABELA ='ZA'  "
		cQuery += " AND X5_CHAVE >'" + cCodEtp + "'"
		cQuery += " ORDER BY X5_CHAVE "

		cQuery := ChangeQuery(cQuery)

		DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T02")


		If !Empty(cCodEtp)
			cCodPetp := T02->X5_CHAVE
			cPetapa  := T02->X5_DESCRI
		EndIF

		T02->(dbclosearea())

	
Return .T.

Static Function FBuscaUser()

	Local cColigada := ""
	Local cChapa := ""
	Local cCodUse2 := cCodUse

	cColigada := Substring(cCodUse,1,2)
	cChapa := Substring(cCodUse,3,5)

	If Substring(cColigada,1,1) == '0'

		cColigada := Substring(cColigada,2,1)

	EndIf


	If Select("T03") <> 0
		T03->(DbCloseArea())
	EndIf

	//3 - C�digo Respons�vel
	cQuery := " SELECT CODCOLIGADA,CHAPA,NOME "
	cQuery += " FROM "+RetSqlName("VW_USUARIOS_RM")+"VW_USUARIOS_RM  "
	cQuery += " WHERE CRACHA_SAS = '"+ cCodUse + "' "
	cQuery += " ORDER BY CODCOLIGADA,CHAPA"

	cQuery := ChangeQuery(cQuery)

	DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T03")

	cCodUse  := T03->CHAPA
	cNomeUse := T03->NOME

	T03->(dbclosearea())

	cCodUse := cCodUse2

Return

Static Function ItensBrow()

	Local cDescri := ""

	If cCodEtp == cMvLibFat .AND. !Empty(cOrdem) //Valida��o do c�digo da etapa com par�metro SA_ETAPA para cria��o do grid
		aDados:= {}
		Aadd(aDados,{"","","",0,"","","","","",""})

		oLista := TSay():New( 185,020,{||"Volume"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,090,008)
		oLista  := MsBrGetDBase():New( 195 ,020,400,100,,,, oDlg,,,,,,,,,,,, .F., "", .T.,, .F.,,{"4"}, )
		oLista:SetArray(aDados)


		If type("oLista:nAt") <> "U"
			oLista:AddColumn(TCColumn():New("Filial ",{ || aDados[oLista:nAt,1] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("N�mero ",{ || aDados[oLista:nAt,2] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("Descri��o						",{ || aDados[oLista:nAt,3] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("Volume ",{ || aDados[oLista:nAt,4] },GetSx3Cache("ZZ3_QUANT ","X3_PICTURE"),,,"RIGHT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("S�rie                                 ",{ || aDados[oLista:nAt,5] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("Envio                                 ",{ || aDados[oLista:nAt,6] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("Grupo                                 ",{ || aDados[oLista:nAt,7] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("Categoria                             ",{ || aDados[oLista:nAt,8] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("Produto                               ",{ || aDados[oLista:nAt,9] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:AddColumn(TCColumn():New("Descri��o                             ",{ || aDados[oLista:nAt,10] },,,,"LEFT",,.F.,.F.,,,,.F.,))
			oLista:blDblClick :=	{||FInfoVol() }
		Endif


		If Select("T04") <> 0
			T04->(DbCloseArea())
		EndIf

		cQuery := " SELECT C6_FILIAL,C6_NUM,ZZ7_SERIE,ZZ7_ENVIO,ZZ7_GRUPO,ZZ7_CATEGO AS TCATEGO, 'K' AS TIPO, ZZ7_CODIGO AS B1_COD, ZZ7_DESCR AS B1_DESC, "
		cQuery += " CASE ZZ7_CATEGO WHEN 'A' THEN 'Aluno' "
		cQuery += " WHEN 'P' THEN 'Professor' "
		cQuery += " WHEN 'C' THEN 'Coordenacao' "
		cQuery += " WHEN 'O' THEN 'Outros' end ZZ7_CATEGOB "
		cQuery += " FROM "+RetSqlName("SC6")+" SC6  "
		cQuery += " INNER JOIN " +RetSqlName("ZZ7")+" ZZ7  "
		cQuery += "		ON SC6.C6_YCODKIT = ZZ7.ZZ7_CODIGO "
		cQuery += " WHERE SC6.D_E_L_E_T_ = ''  "
		cQuery += " AND ZZ7.D_E_L_E_T_ = ''  "
		cQuery += " AND C6_FILIAL ='" + SubStr(cOrdem,1,6) + "'"
		cQuery += " AND C6_NUM ='" + SubStr(cOrdem,7,6) + "'"
		cQuery += " AND C6_YCODKIT <>  ' '  "
		cQuery += " GROUP BY C6_FILIAL,C6_NUM,ZZ7_SERIE,ZZ7_ENVIO,ZZ7_GRUPO, ZZ7_CATEGO , ZZ7_DESCR,ZZ7_CODIGO

		cQuery += " UNION "

		cQuery += " SELECT C6_FILIAL,C6_NUM,'99' AS ZZ7_SERIE ,'-' AS ZZ7_ENVIO ,'-' AS ZZ7_GRUPO,'-' AS TCATEGO, 'P' AS TIPO, '-' AS B1_COD, 'PRODUTO AVULSO' AS B1_DESC, "
		cQuery += " '-' AS ZZ7_CATEGOB "
		cQuery += " FROM "+RetSqlName("SC6")+" SC6  "
		cQuery += " INNER JOIN " +RetSqlName("SB1")+" SB1  "
		cQuery += "		ON SC6.C6_PRODUTO  = SB1.B1_COD " 
		cQuery += " WHERE SC6.D_E_L_E_T_ = ''  "
		cQuery += " AND SB1.D_E_L_E_T_ = ''  "
		cQuery += " AND C6_FILIAL ='" + SubStr(cOrdem,1,6) + "'"
		cQuery += " AND C6_NUM ='" + SubStr(cOrdem,7,6) + "'"
		cQuery += " AND C6_YCODKIT =  ' '  "
		cQuery += " GROUP BY C6_FILIAL,C6_NUM " 
		cQuery += " ORDER BY C6_FILIAL,C6_NUM "

		cQuery := ChangeQuery(cQuery)

		DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T04")

		cCodSer := T04->ZZ7_SERIE

		If T04->(EOF())
			Aadd(aDados,{"","","",0,"","","","","",""})
		Else
			aDados:= {}
		EndIf

		aDados2 := {}
		aadd(aDados2,{ALLTRIM(T04->C6_FILIAL),;
		ALLTRIM(T04->C6_NUM),;
		ALLTRIM(T04->ZZ7_SERIE),;
		ALLTRIM(T04->ZZ7_ENVIO),;
		ALLTRIM(T04->ZZ7_GRUPO),;
		ALLTRIM(T04->TCATEGO),;
		ALLTRIM(T04->B1_COD),;
		ALLTRIM(T04->B1_DESC)})

		While !T04->(EOF())

			If T04->TIPO == 'K'
				cDescri := Alltrim(SUBSTR(ALLTRIM(POSICIONE("ZZO",1,ALLTRIM(T04->ZZ7_SERIE),"ZZO->ZZO_DESC" )) + " - " + ;
				Alltrim(T04->ZZ7_CATEGOB)+" - " + ;
				Alltrim(POSICIONE("ZZP",1,XFILIAL("ZZP")+T04->ZZ7_GRUPO,"ZZP_DESCRI")) +" - " + ;
				T04->ZZ7_ENVIO+" Envio"        ,0,60))

				aadd(aDados,{ALLTRIM(T04->C6_FILIAL),;
				ALLTRIM(T04->C6_NUM),;
				ALLTRIM(T04->B1_DESC),;
				0,;
				ALLTRIM(T04->ZZ7_SERIE),;
				ALLTRIM(T04->ZZ7_ENVIO),;
				ALLTRIM(T04->ZZ7_GRUPO),;
				ALLTRIM(T04->TCATEGO),;
				ALLTRIM(T04->B1_COD),;
				ALLTRIM(T04->B1_DESC)})

			Else

				cDescri := T04->B1_DESC

				aadd(aDados,{ALLTRIM(T04->C6_FILIAL),;
				ALLTRIM(T04->C6_NUM),;
				ALLTRIM(cDescri),;
				0,;
				ALLTRIM(T04->ZZ7_SERIE),;
				ALLTRIM(T04->ZZ7_ENVIO),;
				ALLTRIM(T04->ZZ7_GRUPO),;
				ALLTRIM(T04->TCATEGO),;
				ALLTRIM(T04->B1_COD),;
				ALLTRIM(T04->B1_DESC)})
			EndIf

			T04->(dbSkip())
		EndDo

		If Select("T05") <> 0
			T05->(DbCloseArea())
		EndIf

		//5- Consulta para retornar descri��o da serie, tabela ZZO.
		cQuery := " SELECT ZZO_DESC "
		cQuery += " FROM "+RetSqlName("ZZO") + "  ZZO  "
		cQuery += " WHERE ZZO.D_E_L_E_T_  = ''
		cQuery += " AND ZZO_COD ='" + cCodSer + "' "

		cQuery := ChangeQuery(cQuery)

		DbUsearea(.T.,"TOPCONN",TCGenQry(,,cQuery),"T05")

		cDesSer := T05->ZZO_DESC

		T05->(dbclosearea())

		T04->(dbclosearea())

		oLista:SetArray(aDados)
		oLista:refresh()
		oLista:Refresh()
		getDRefresh()

	Else
		Aadd(aDados,{"","","",0,"","","","","",""})

		oLista := TSay():New( 185,020,{||"Volume"},,,oFont,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,090,008)
		oLista  := MsBrGetDBase():New( 195 ,020,400,100,,,, oDlg,,,,,,,,,,,, .F., "", .T.,, .F.,,{"4"}, )
		oLista:SetArray(aDados)
		//oLista:= nil
	EndIf

Return .T.

Static Function FBtOk(cOrdem)

	Local lRet     := .T.
	Local nVolume := 0
	Local cQuery := ""

	If Empty(cOrdem)
		Alert("N�o � possivel confirmar um item vazio")
		Return lRet
	EndIf

	//Valida cod etapa na medi��o
	If Empty(cCodEtp)
		Alert("Campo C�d. Etapa n�o preenchido no pedido. Verificar conteudo do campo.")
		Return .F.
	EndIf

	//VERIFICAR SE A MEDI��O FOI BLOQUEADA DEPOIS DE FATURADA

	If POSICIONE("SC5",1,SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6), "SC5->C5_YETAPA") == alltrim(getmv("SA_ETPFAT")) .AND.	 ;
			alltrim(POSICIONE("SC5",1,SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6), "SC5->C5_YSTOS")) == "B"
		Alert("Pedido bloqueado ap�s faturamento, verificar com setor do PCE! ")
		Return .F.
	EndIf

	If len(aDados) = 0 
		Alert("N�o h� dados")
		Return .F.
	Endif
	
	//Valida se informou o respons�vel
	If Empty(cNomeUse)
		Alert("Respons�vel n�o informado")
		Return .F.
	Endif

	If lRet
		
		nVolTot:= 0
		DbSelectArea("ZC3")
		dbSetOrder(1)
		If dbSeek(SubStr(cOrdem,1,6) + SubStr(cOrdem,7,6) + cCodEtp )
			alert("Registro j� cadastrado")
			Return lRet
		Else
			If cCodPetp = '40' //Faturamento
				
				For i:= 1 To len(aDados)
					nVolTot+=aDados[i][4]
				Next
				
				//Grava volume no Pedido
				dbSelectArea("SC5")
				SC5->(dbSetOrder(1))
				SC5->(dbSeek(SubStr(cOrdem,1,6)+cPedPCE))
				
				RecLock("SC5",.F.)
					SC5->C5_VOLUME1 := nVolTot
					SC5->C5_ESPECI1 := "CX"
				MsUnlock()
				
				dbSelectArea("ZC4")
				
				For nY:=1 To Len(aDados)
					RecLock("ZC4",.T.)
						ZC4->ZC4_FILIAL := SubStr(cOrdem,1,6)
						ZC4->ZC4_PEDIDO := cPedPCE
						ZC4->ZC4_CODCLI := cCodCli 
						ZC4->ZC4_COD	:= aDados[nY][9] //Codigo
						ZC4->ZC4_VOLUME	:= aDados[nY][4] //Volume
						ZC4->ZC4_SERIE  := aDados[nY][5] //Serie
						ZC4->ZC4_ENVIO  := aDados[NY][6] //Envio
						ZC4->ZC4_DESC   := aDados[nY][3] //Descri��o
					MsUnlock()
				Next
				
			Endif	

			RecLock("ZC3", .T.)
				ZC3->ZC3_FILIAL := SubStr(cOrdem,1,6)
				ZC3->ZC3_PEDIDO := cPedPCE
				ZC3->ZC3_EMISSA := dEmissao
				ZC3->ZC3_CODCLI := cCodCli 
				ZC3->ZC3_NOMECL := cCliente
				ZC3->ZC3_CODUSE := cCodUse
				ZC3->ZC3_NOMUSE := cNomeUse
				ZC3->ZC3_ETAPA  := cCodEtp
				ZC3->ZC3_NOMETP := cEtapa
				ZC3->ZC3_OBS    := cObs
				ZC3->ZC3_DATA   := Date()
				ZC3->ZC3_HORA   := Time()
				ZC3->ZC3_VOLUME := nVolTot
			MsUnLock()
			
			
		EndIf
	Endif

	
	aDados:= {}
	Aadd(aDados,{"","","",0,"","","","","",""})
	oLista:SetArray(aDados)
	oLista:Refresh()

	If cCodEtp < cMvFetapa
		RecLock("SC5",.F.)
		SC5->C5_YETAPA 	:= cCodPetp
		SC5->C5_YOBS 	:= cObs
			If SC5->C5_YETAPA = '40'
				SC5->C5_YSTATUS := 'P '
				SC5->C5_YTFARM := 'S'
			EndIf
		MsUnLock()
	Endif

	If cCodPetp = '40' //Imprime Etiqueta 
		dbSelectArea("SC5")
		U_SASR098()
		U_SASR123(AllTrim(cOrdem))
	Endif


	cOrdem    := Space(20)
	cCodCli   := Space(GetSx3Cache("ZC3_CODCLI","X3_TAMANHO"))
	cCliente  := Space(GetSx3Cache("ZC3_NOMECL","X3_TAMANHO"))
	cCodUse   := Space(GetSx3Cache("ZC3_CODUSE","X3_TAMANHO"))
	cNomeUse  := Space(GetSx3Cache("ZC3_NOMUSE","X3_TAMANHO"))
	cCodEtp   := Space(GetSx3Cache("ZC3_ETAPA","X3_TAMANHO"))
	cEtapa    := Space(GetSx3Cache("ZC3_NOMETP","X3_TAMANHO"))
	cPetapa   := Space(GetSx3Cache("ZC3_NOMETP","X3_TAMANHO"))
	dEmissao  := Space(GetSx3Cache("ZC3_EMISSA","X3_TAMANHO"))
	cObs      := Space(GetSx3Cache("ZC3_OBS","X3_TAMANHO"))

	oGet1:BUFFER := Space(20)

	oGet1:ReFresh()
	oGet2:ReFresh()
	oGet3:ReFresh()
	oGet4:ReFresh()
	oGet5:ReFresh()
	oGet6:ReFresh()
	oGet7:ReFresh()
	oGet8:ReFresh()

	oGet1:SetFocus()
	GetdRefresh()

	If lRet
		msGInfo("Registro inclu�do com sucesso")
		oDlg:End()
	Endif

Return lRet

Static Function FInfoVol
	Local aPergs	:= {}
	Local aRetOpc	:= {}
	Local nVol		:= 0

	If Type("oLista:nAt") <> "U"
		nVol:= aDados[oLista:nAt,4]
	Else
		Return
	Endif

	aAdd( aPergs ,{1,"Volume",	nVol	,GetSx3Cache("C6_QTDVEN","X3_PICTURE") ,'.T.',"" ,'.T.',80 ,.T.})

	If ParamBox(aPergs,"Informe Volume",aRetOpc,,,,,,,"_RF51",.F.,.F.)
		aDados[oLista:nAt,4]:=	aRetOpc[1]
		oLista:Refresh()
		//U_SASR032(ALLTRIM(cOrdem),aDados[oLista:nAt,7],aDados[oLista:nAt,6])
	EndIf

Return

Static Function CssbtnImg(cImg,cCor)
	Local cRet := " QPushButton{ background-image: url(rpo:"+cImg+")"+;
	" ;border-style:solid"+;
	" ;border-width:5px"+;
	" ;background-repeat: none; margin: 1px "+;
	" ;background-color: "+cCor+""+;
	" ;border-radius: 6px"+;
	" ;font-weight: bold;font-size: 12px"+;
	" ;color: rgb(255,255,255) "+;
	"}"+;
	"QPushButton:hover { "+;
	" ;background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop:0 "+cCor+", stop:0.5 "+cCor+", stop:1 white)"+;
	"}"
Return cRet