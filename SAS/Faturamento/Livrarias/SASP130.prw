#Include 'Protheus.ch'
#INCLUDE 'FWMBROWSE.CH'
#INCLUDE 'FWMVCDEF.CH'
#include 'TopConn.CH'

/*/{Protheus.doc} SASP051
Tela para marca��o de faturamento pedidos intercompany
@type function
@author Joao Filho
@since 13/10/16
@version P11
/*/
User Function SASP130()
	Local oDlg
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Local cTextHead	:= "QLabel { color: rgb(39,82,102); font-size: 12px; font-weight: bold;  } "
	Local oFWLayer
	Local aOperacao	:= {" ","Faturar","Cancelar NF","Abortar Fatur./Cancel.","Entrada do Doc"}	
	Private aObjTela	:= {}
	Private cOperacao	:= aOperacao[1]
	Private oMark
	Private nPenFat		:= 0
	Private nPenCan		:= 0
	Private nPenErr		:= 0
	Private nPenTFat	:= 0
	Private nPenTCan	:= 0
	Private nPenTErr	:= 0
	Private cHoraAtu	:= ""
	Private cDataAtu	:= ""
	Private cMonitor	:= ""
	Private lFatLiv		:= .F.
	Private aCampos		:= {}

	//StartJob("U_SASP058",getenvserver(),.F.)	//Inicia um nova thread
	AtualTot()
	oDlg := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Faturamento',,,,nOr(WS_VISIBLE,WS_POPUP),,,,,.T.,,,,)
	@00,00 MSPANEL oBar SIZE 33,33 of oDlg
	oBar:Align := CONTROL_ALIGN_TOP
	oBtn := TButton():New(01,01,"     Atualizar",oBar ,{|| AtualTot(),oMark:Refresh(.F.) },50,20,,,.F.,.T.,.F.,,.F.,,,.F. )
	oBtn:SetCss(CssbtnImg("reload.png","#006386"))
	oSay := TSay():New(001,060, {||"Pendente Faturamento Filial: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(008,060, {||"Pendente Cancelamento Filial: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(015,060, {||"Erros Filial: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(001,160, {||Transform(nPenFat,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(008,160, {||Transform(nPenCan,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(015,160, {||Transform(nPenErr,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(001,200, {||"Pendente Faturamento Total: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(008,200, {||"Pendente Cancelamento Total: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(015,200, {||"Erros Total: "},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	oSay := TSay():New(001,300, {||Transform(nPenTFat,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(008,300, {||Transform(nPenTCan,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(015,300, {||Transform(nPenTErr,"@E 999,999,999,999")},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	oSay := TSay():New(001,350, {||"Opera��o:"},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	TComboBox():New(008,350,{|u|if(PCount()>0,cOperacao:=u,cOperacao)},;
	aOperacao,090,20,oBar,,{|| TrocaOperacao() };
	,,,,.T.,,,,,,,,,'cOperacao')

	oSay := TSay():New(008,550, {||"Data JOB: "+DToC(cDataAtu)},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)

	oSay := TSay():New(024,010, {||"Status JOB:"+cMonitor},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)

	oSay := TSay():New(024,550, {||"Atualizado: "+cHoraAtu},oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,;
	.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)

	oMark 	:= FWMarkBrowse():New()
	oMark:SetAlias('SC5')	
	oMark:SetSemaphore(.T.)
	oMark:SetValid({|| Valid() })
	oMark:SetDescription('Faturamento')
	oMark:SetFieldMark( 'C5_OK' )
	oMark:SetAllMark({|| MarcaTudo() })
	oMark:SetFilterDefault("C5_YTFARM = 'S'")

	oMark:SetUseFilter(.T.)	

	oMark:AddLegend( "C5_YSTATUS=='FF' .AND. C5_YTFARM = 'S'"  ,"BR_AZUL_CLARO"		,"Faturado SAIDA/ENTRADA ")
	oMark:AddLegend( "C5_YSTATUS=='F ' .AND. C5_YTFARM = 'S'"  ,"BR_VERDE"		,"Faturado")
	oMark:AddLegend( "C5_YSTATUS=='C ' .AND. C5_YTFARM = 'S'"  ,"BR_VERMELHO"	,"Cancelado")
	oMark:AddLegend( "(C5_YSTATUS=='P ' ) .AND. C5_YTFARM = 'S' " ,"BR_AMARELO"	,"Pendente Faturamento")
	oMark:AddLegend( "C5_YSTATUS=='Q ' .AND. C5_YTFARM = 'S'"  ,"BR_AZUL"		,"Pendente Cancelamento")
	oMark:AddLegend( "(C5_YSTATUS=='Q '.OR.C5_YSTATUS=='P ') .AND. C5_YTFARM = 'S' "	   ,"BR_AMARELO"	,"Pendentes")
	oMark:AddLegend( "C5_YSTATUS=='EF' .AND. C5_YTFARM = 'S' "	,"BR_PRETO"		,"Erro Faturamento")
	oMark:AddLegend( "C5_YSTATUS=='EC' .AND. C5_YTFARM = 'S' "	,"BR_CINZA"		,"Erro Cancelamento")
	oMark:AddLegend( "(C5_YSTATUS=='EF'.OR.C5_YSTATUS=='EC') .AND. C5_YTFARM = 'S' "  ,"BR_PRETO"		,"Erros")
	oMark:AddLegend( "C5_YSTATUS=='BE' .AND. C5_YTFARM = 'S' "						   ,"BR_PINK"		,"Bloqueio Estoque")
	oMark:SetMenuDef( 'SASP130' )	

	oMark:Activate(oDlg)
	oTimer := TTimer():New(3000,{||AtualTot(),aEval(aObjTela,{|x| x:Refresh()})}, oDlg )
	oTimer:Activate()
	oDlg:Activate(,,,.T.)
Return

Static Function TrocaOperacao()

	LimparMarca()

	If Empty(cOperacao)
		oMark:SetFilterDefault("C5_YTFARM = 'S'")
	ElseIf cOperacao=="Faturar"
		oMark:SetFilterDefault("C5_YSTATUS $ '  |C |L |P |EF|BE' .AND. C5_YPEDIDO == 'I' .AND. C5_YTFARM == 'S' .AND. "+; 
								"( (C5_YEXPED <> 'CE') .OR. (C5_YEXPED = 'CE' .AND. C5_YETAPA = '40') ) ")  //Liberado para faturamento
	
	ElseIf cOperacao=="Cancelar NF"
		oMark:SetFilterDefault("C5_YSTATUS $ 'F |EC|BE|FF' .AND. C5_YPEDIDO == 'I' .AND. C5_YTFARM == 'S'")
	ElseIf cOperacao=="Abortar Fatur./Cancel."
		oMark:SetFilterDefault("(C5_YSTATUS=='P '.or. C5_YSTATUS='Q ') .AND. C5_YPEDIDO == 'I' .AND. C5_YTFARM == 'S' ")
	ElseIf cOperacao=="Entrada do Doc"	
		oMark:SetFilterDefault("C5_YSTATUS=='F ' .AND. C5_YPEDIDO == 'I' .AND. C5_YTFARM == 'S' .AND. alltrim(C5_YFILPCIC) <> '' .AND. alltrim(C5_YNUMIC) <> ''")
	EndIf

	AtualTot()
	oMark:Refresh(.T.)

Return


Static Function MenuDef()
	Local aRotina := {}
	Local aNota	:= {}
	Local aPedido	:= {}
	Local aDevol	:= {}
	ADD OPTION aRotina	TITLE 'Processar Marcados'	 ACTION 'U_SASP130A()' OPERATION 4 ACCESS 0
	ADD OPTION aRotina	TITLE 'Pedido Venda'         ACTION 'U_SASP130B()' OPERATION 4 ACCESS 0
	ADD OPTION aRotina	TITLE 'Pedido Compras'       ACTION 'U_SASP130D()' OPERATION 4 ACCESS 0
	ADD OPTION aRotina	TITLE 'Etiqueta'             ACTION 'U_SASR098()' OPERATION 4 ACCESS 0
Return aRotina


Static Function Valid()
	If Empty(cOperacao)
		MsgAlert("Escolha a opera��o primeiro")
		Return .F.
	EndIf
Return .T.

// FUN��O PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO MARCADO
STATIC FUNCTION YINVERTE(oMark)
	if empty(SC5->C5_OK)
		oMark:Mark()
	endif
RETURN

User Function SASP130A(cTitulo,cStatus)
	If Empty(cOperacao)
		MsgAlert("Escolha a opera��o")
		Return
	ElseIf cOperacao=="Faturar"
		cTitulo	:= cOperacao
		cStatus	:= "P "
	ElseIf cOperacao=="Cancelar NF"
		If !MsgYesNo("Confirmar cancelamento da NF?")
			Return
		EndIf
		cTitulo	:= cOperacao
		cStatus	:= "Q "
	ElseIf cOperacao=="Abortar Fatur./Cancel."
		cTitulo	:= cOperacao
		cStatus	:= "L "
	ElseIf cOperacao=="Entrada do Doc"
		cTitulo	:= cOperacao
		cStatus	:= "EN"
	Else
		Return
	EndIf

	Processa({|lEnd| RunProc(cTitulo,cStatus,@lEnd) },cTitulo,"Aguarde...",.T.)

Return



Static Function RunProc(cTitulo,cStatus,lEnd)
	Local cAlias		:= GetNextAlias()
	Local aPergs		:= {}
	Local aRet			:= {}
	Local cPrior		:= "99"
	Local cAvisos		:= ""

	Local nValMed       
	Local cP01
	Local cP02
	Local cP03
	
	Local aDocFat	:= {}
	Local cMsgErro 	:= {}


	If !QueryMarca(cAlias) 
		MsgAlert("N�o foi marcado nenhum item!")
		(cAlias)->(DbCloseArea())
		Return .F.
	EndIF

	//Verificar se o cliente esta bloqueado 
	While (cAlias)->(!EOF()) .and. !lEnd
		IF	ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1") + SC5->C5_CLIENTE + SC5->C5_LOJACLI ,"A1_YBLOQ")) == "S"
			MsgAlert("Pedido "+SC5->C5_NUM+" com cliente bloqueado !!")
			(cAlias)->(DbCloseArea())
			Return .F.
		ENDIF
		(cAlias)->(DbSkip())
	ENDDO
	(cAlias)->(dbgotop())
	
	If cStatus <> 'EN' 
	
	aAdd( aPergs ,{1,'Prioridade',cPrior,'99',"","",'.T.',60,.F.})
	
	If ParamBox(aPergs,cTitulo,aRet,,,,,,,"",.F.,.F.)
		While (cAlias)->(!EOF()) .and. !lEnd
			IncProc("Filial:"+SC5->C5_FILIAL+",Pedido:"+SC5->C5_NUM)
			SC5->(DbGoTo((cAlias)->REGSC5))
			If !(SC5->(SimpleLock()) .and. RecLock("SC5",.F.))
				cAvisos			+= "N�o � possivel realizar opera��o, registro em uso. Filial:"+SC5->C5_FILIAL+" Pedido:"+SC5->C5_NUM
				(cAlias)->(DbSkip())
				Loop
			EndIf
			SC5->(DbGoTo((cAlias)->REGSC5))	//Atualizar registro posicionado!
			If cStatus=="  " .and. (SC5->C5_YSTATUS == "F " .OR. SC5->C5_YSTATUS == "C ")
				cAvisos			+= "N�o � possivel abortar, opera��o j� concluida. Filial:"+SC5->C5_FILIAL+" Pedido:"+SC5->C5_NUM
				SC5->C5_OK		:= Space(Len(SC5->C5_OK))
				MsUnLock()
				(cAlias)->(DbSkip())
				Loop
			EndIF
			If ValType(cStatus)=="C"
				SC5->C5_YSTATUS:= cStatus
			EndIf
			SC5->C5_YPRIORI	:= StrZero(Val(aRet[1]),Len(ZZ2->ZZ2_STATUS))
			SC5->C5_OK		:= Space(Len(SC5->C5_OK))
			MsUnLock()
			(cAlias)->(DbSkip())
		EndDO
		oMark:Refresh(.T.)	//Atualiza e posiciona no primeiro registro
		AtualTot()
		If !Empty(cAvisos)
			MsgAlert(cAvisos)
		EndIF
		If lEnd
			MsgAlert("Altera��o cancelada!"+chr(13)+"Alguns registros podem ter sido alterado.")
		Else
			MsgInfo("Opera��o concluida!")
		EndIf
	EndIf
	
	Else //Processamento da Entrada
	
		cLocal	:= Space(Len(SC5->C5_YLOCAL))
		cFilAnt	:= SC5->C5_YFILPCI	
	
		aAdd(aPergs ,{1,'Armazem Entrada',cLocal,'@!',".T.","NNR",'.T.',60,.F.})
		
		If ParamBox(aPergs,cTitulo,aRet,,,,,,,"",.F.,.F.)
			While (cAlias)->(!EOF()) .and. !lEnd
				IncProc("Filial:"+SC5->C5_FILIAL+",Pedido:"+SC5->C5_NUM)
				SC5->(DbGoTo((cAlias)->REGSC5))
				If !(SC5->(SimpleLock()) .and. RecLock("SC5",.F.))
					cAvisos			+= "N�o � possivel realizar opera��o, registro em uso. Filial:"+SC5->C5_FILIAL+" Pedido:"+SC5->C5_NUM
					(cAlias)->(DbSkip())
					Loop
				EndIf
				SC5->(DbGoTo((cAlias)->REGSC5))	//Atualizar registro posicionado!
				If cStatus=="  " .and. (SC5->C5_YSTATUS == "F " .OR. SC5->C5_YSTATUS == "C ")
					cAvisos			+= "N�o � possivel abortar, opera��o j� concluida. Filial:"+SC5->C5_FILIAL+" Pedido:"+SC5->C5_NUM
					SC5->C5_OK		:= Space(Len(SC5->C5_OK))
					MsUnLock()
					(cAlias)->(DbSkip())
					Loop
				EndIF
				If ValType(cStatus)=="C"
					SC5->C5_YSTATUS:= cStatus
				EndIf
				SC5->C5_OK		:= Space(Len(SC5->C5_OK))
				MsUnLock()
				
				//Atualiza o Armaz�m conforme o informado
				cChave:= SC5->C5_YFILPCI+SC5->C5_YNUMIC 
				dbSelectArea("SC7")
				SC7->(dbSetOrder(1))
				If SC7->(dbSeek(cChave))
					While SC7->(!eof()) .AND. cChave == SC7->(C7_FILIAL+C7_NUM) 
						Reclock("SC7",.F.)
							SC7->C7_LOCAL := aRet[1] 	
						MsUnlock()
						SC7->(dbSkip())
					Enddo 
				Endif

				FatuPC(SC5->C5_YFILPCIC,SC5->C5_YNUMIC,@aDocFat,@cMsgErro,SC5->C5_NOTA,SC5->C5_SERIE) //Entrada da Nota Fiscal
				
				//Verifica se foi dado entrada
				cQuery:= "SELECT D1_PEDIDO FROM "+RetSqlName("SD1")+ " SD1 "
				cQuery+= "WHERE SD1.D_E_L_E_T_ = ' ' AND "
				cQuery+= "D1_FILIAL+D1_PEDIDO = '"+cChave+"' "
				
				TcQuery cQuery new Alias TSD1
				
				If TSD1->(!eof()) .and. !empty(TSD1->D1_PEDIDO) //Verifica se foi dado entrada para o pedido!
					RecLock("SC5",.F.)
						SC5->C5_YLOG	  := SC5->C5_YLOG + " Doc de Entrada: "+ aDocFat[1] +" SERIE:"+ aDocFat[2] +""
						SC5->C5_YSTATUS   := "FF"
					MsUnLock()
				Else //Retorna para processar novamente
					RecLock("SC5",.F.)
						SC5->C5_YSTATUS   := "F"
					MsUnLock()
				Endif	
				
				TSD1->(dbCloseArea())
				(cAlias)->(DbSkip())
			EndDO
		Endif	
			
	
	Endif
	
	(cAlias)->(DbCloseArea())
Return

Static Function QueryMarca(cAlias)
	Local cMarca	:= oMark:Mark()
	BeginSql Alias cAlias
	SELECT R_E_C_N_O_ AS REGSC5 FROM %table:SC5% SC5 WHERE C5_FILIAL=%xFilial:SC5% AND  C5_OK = %Exp:cMarca% and SC5.D_E_L_E_T_ = ' '
	EndSql
Return (cAlias)->(!EOF())

Static Function LimparMarca()
	Local cAlias	:= GetNextAlias()
	QueryMarca(cAlias)
	While (cAlias)->(!EOF())
		SC5->(DbGoTo((cAlias)->REGSC5))
		RecLock("SC5")
		SC5->C5_OK		:= Space(Len(SC5->C5_OK))
		MsUnLock()
		(cAlias)->(DbSkip())
	EndDO
Return

Static Function QueryDsMr(cAlias)
	Local cMarca	:= oMark:Mark()
	BeginSql Alias cAlias
	SELECT R_E_C_N_O_ AS REGSC5 FROM %table:SC5% SC5 WHERE C5_FILIAL=%xFilial:SC5% AND C5_OK <> %Exp:cMarca% and SC5.D_E_L_E_T_ = ' '
	EndSql
Return (cAlias)->(!EOF())

Static Function MarcaTudo()
	MsgInfo("Marca Tudo")
Return

Static Function SasMark(oMark)
	MarcaTudo()
	AtualTot()
	oMark:Refresh(.T.)
RETURN


Static Function AtualTot()
	Local cAlias	:= GetNextAlias()
	Local aMonitor	:= {}
	Local nPos
	Local cFilSC5	:= xFilial("SC5",cFilAnt)
	Local aArea		:= GetArea()
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:SC5% SC5 WHERE C5_FILIAL=%exp:cFilSC5% AND C5_YSTATUS='P ' and SC5.D_E_L_E_T_ = ' '
	EndSql
	nPenFat	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:SC5% SC5 WHERE C5_FILIAL=%exp:cFilSC5% AND C5_YSTATUS='Q ' and SC5.D_E_L_E_T_ = ' '
	EndSql
	nPenCan	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:SC5% SC5 WHERE C5_FILIAL=%exp:cFilSC5% AND (C5_YSTATUS like 'E%' OR C5_YSTATUS like 'BE') and SC5.D_E_L_E_T_ = ' '
	EndSql
	nPenErr	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	//TOTAL
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:SC5% SC5 WHERE C5_YSTATUS='P ' and SC5.D_E_L_E_T_ = ' '
	EndSql
	nPenTFat	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:SC5% SC5 WHERE C5_YSTATUS='Q ' and SC5.D_E_L_E_T_ = ' '
	EndSql
	nPenTCan	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	BeginSql Alias cAlias
	SELECT COUNT(*) AS TOTPEND FROM %table:SC5% SC5 WHERE (C5_YSTATUS like 'E%' OR C5_YSTATUS like 'BE') and SC5.D_E_L_E_T_ = ' '
	EndSql
	nPenTErr	:= (cAlias)->TOTPEND
	(cAlias)->(DbCloseArea())
	cHoraAtu	:= TIME()
	cDataAtu	:= DATE()
	cMonitor	:= GetGlbValue("SASP129")
	If ValType(cMonitor)=="U"
		cMonitor	:= ""
	EndIf
	RestArea(aArea)
	ProcessMessage()
Return

User Function SASP130C()


	If Alltrim(C5_YSTATUS)=='EF' .OR. Alltrim(C5_YSTATUS)=='EC'

		DBSELECTAREA("SC5")
		DBSETORDER(1)
		DBSEEK(SC5->C5_FILIAL + SC5->C5_NUM + SC5->C5_CLIENTE + SC5->C5_LOJACLI)

		If Found()

			RECLOCK("SC5", .F.)

			SC5->C5_YSTATUS := 'L'
			SC5->C5_YLOG := MSMM("",80)

			MSUNLOCK()

		EndIf

	Else

		MessageBox("O pedido n�o est� com ERROS. Verifique e tente novamente.","TOTVS",48)

	EndIf

Return



Static Function CssbtnImg(cImg,cCor)
	Local cRet	:= " QPushButton{ background-image: url(rpo:"+cImg+")"+;
	" ;border-style:solid"+;
	" ;border-width:5px"+;
	" ;background-repeat: none; margin: 1px "+;
	" ;background-color: "+cCor+""+;
	" ;border-radius: 6px"+;
	" ;font-weight: bold;font-size: 12px"+;
	" ;color: rgb(255,255,255) "+;
	"}"+;
	"QPushButton:hover { "+;
	" ;background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop:0 "+cCor+", stop:0.5 "+cCor+", stop:1 white)"+;
	"}"
Return cRet

Static Function FatuPC(cEmpFat,cPedFat,aDocFat,cMsgErro,cDocNF,cSerieNF)
	Local aCabec	:= {}
	Local aItens	:= {}
	Local aLinha	:= {}
	Local cFilBkp	:= cFilAnt
	Local lOk		:= .T.
	Local cEntarm := ZZ2->ZZ2_TIPCTR
	Private lMsErroAuto := .F.

	cFilAnt	:= cEmpFat


	dbSelectArea("SC7")
	SC7->(dbSetOrder(1))
	SC7->(dbSeek(xFilial("SC7",cEmpFat)+cPedFat))

	aadd(aCabec,{"F1_TIPO"   ,"N"})
	aadd(aCabec,{"F1_FORMUL" ,"N"})
	aadd(aCabec,{"F1_DOC"    ,cDocNF})
	aadd(aCabec,{"F1_SERIE"  ,cSerieNF})
	aadd(aCabec,{"F1_EMISSAO",dDataBase})
	aadd(aCabec,{"F1_FORNECE",SC7->C7_FORNECE})
	aadd(aCabec,{"F1_LOJA"   ,SC7->C7_LOJA})
	aadd(aCabec,{"F1_ESPECIE","SPED"})
	aadd(aCabec,{"F1_YFILIAL",ALLTRIM(SC7->C7_FILIAL)})
	aadd(aCabec,{"F1_NATUREZ",GetMv("SA_NATPAG")})
	NNR->(dbSetOrder(1))
	_linha :=1
	While SC7->(!eof()) .and. SC7->C7_FILIAL+SC7->C7_NUM == xFilial("SC7",cEmpFat)+cPedFat
		If !Empty(SC7->C7_YTES)
			RecLock("SC7",.F.)
			SC7->C7_TES	:= SC7->C7_YTES
			MsUnLock()
		EndIf
		aLinha := {}
		Aadd(aLinha,{"D1_COD"     	,SC7->C7_PRODUTO		,Nil})
		Aadd(aLinha,{"D1_ITEM"     	,strzero(_linha,4)		,Nil})
		Aadd(aLinha,{"D1_QUANT"		,SC7->C7_QUANT			,Nil})
		Aadd(aLinha,{"D1_VUNIT"		,SC7->C7_PRECO			,Nil})
		Aadd(aLinha,{"D1_TOTAL"		,SC7->C7_TOTAL			,Nil})
		Aadd(aLinha,{"D1_PEDIDO"	,SC7->C7_NUM			,Nil})
		Aadd(aLinha,{"D1_CC"		,SC7->C7_CC				,Nil})
		Aadd(aLinha,{"D1_TES"		,SC7->C7_TES			,Nil})
		Aadd(aLinha,{"D1_ITEMPC"	,SC7->C7_ITEM			,Nil})
		Aadd(aLinha,{"D1_LOCAL"		,SC7->C7_LOCAL       	,Nil})// Faturamento Direto (Weskley Silva 11/02/2016)
		Aadd(aLinha,{"D1_YDTENTR"	,dDataBase           	,Nil})

		_linha++
		If (!NNR->(MsSeek(xFilial("NNR")+SC7->C7_LOCAL)))//(!NNR->(MsSeek(xFilial("NNR")+SC7->C7_LOCAL)))
			AGRA045(;
			{{"NNR_CODIGO",SC7->C7_LOCAL,NIL};
			,{"NNR_DESCRI","Armazem "+SC7->C7_LOCAL,NIL};
			};
			,3,{},{})
		EndIf
		aadd(aItens,aLinha)
		SC7->(DbSkip())
	EndDo

	MATA103(aCabec,aItens,3,.T.)
	
	//MSExecAuto({|x,y,z| MATA103(x,y,z) },aCabec,aItens,3)
	If !lMsErroAuto
		lOk:= .T.
		aDocFat	:= {SF1->F1_DOC,SF1->F1_SERIE}
	Else
		lOk:= .F.
		cMsgErro := Mostraerro()
		cMsgErro += "N�o foi poss�vel gerar a Nota fiscal de Entrada da Filial "+cFilAnt+ CHR(10) + CHR(13)
	EndIf

	cFilAnt:= cFilBkp

Return lOk

//Visualiza��o do Pedido de Venda
User Function SASP130B
	
	Local aItem:= {}
	dbSelectArea("SC5")

	aCab:= {	{"C5_TIPO"		,SC5->C5_TIPO		,NIL};
	,{"C5_NUM"		,SC5->C5_NUM		,NIL};
	,{"C5_CLIENTE"	,SC5->C5_CLIENTE	,NIL};
	,{"C5_CLIENT"	,SC5->C5_CLIENT		,NIL};
	,{"C5_LOJACLI"	,SC5->C5_LOJACLI	,NIL};
	,{"C5_CONDPAG"	,SC5->C5_CONDPAG	,NIL};
	,{"C5_EMISSAO"  ,SC5->C5_EMISSAO	,NIL}}


	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(DbSeek(SC5->C5_FILIAL+SC5->C5_NUM))

		While SC6->(!Eof()) .AND. SC6->C6_FILIAL == SC5->C5_FILIAL .AND. SC6->C6_NUM == SC5->C5_NUM
			aAdd(aItem,{ {"C6_PRODUTO"	,SC6->C6_PRODUTO,NIL};
			,{"C6_ITEM"		,SC6->C6_ITEM   ,NIL};
			,{"C6_QTDVEN"	,SC6->C6_QTDVEN ,NIL};
			,{"C6_PRCVEN"	,SC6->C6_PRCVEN ,NIL};
			,{"C6_VALOR"	,SC6->C6_VALOR	,NIL};
			,{"C6_TES"		,SC6->C6_TES	,NIL};
			,{"C6_CLI"		,SC6->C6_CLI	,NIL};
			,{"C6_QTDLIB"	,SC6->C6_QTDVEN	,NIL};
			,{"C6_LOJA"		,SC6->C6_LOJA	,NIL};
			,{"C6_YCODKIT"	,SC6->C6_YCODKIT,NIL};
			,{"C6_LOCAL"	,SC6->C6_LOCAL  ,NIL}})

			SC6->(DbSkip())
		EndDo

		lMsErroAuto := .F.
		
		MSExecAuto({|x,y,z|Mata410(x,y,z)},aCab,aItem,2) 


Return

//Visualiza��o do Pedido de Compra
User Function SASP130D

	Local aCabPC	:= {}
	Local aItensPC	:= {}
	Local cFilB		:= cFilAnt	
	dbSelectArea("SC7")
	If SC7->(dbSeek(SC5->(C5_YFILPCI+C5_YNUMIC)))
		cFilAnt:= SC7->C7_FILIAL
		Mata120(1,,,2)
	Endif	
	
	cFilAnt	:= cFilB
Return