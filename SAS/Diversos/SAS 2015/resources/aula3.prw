#include "protheus.ch"

User Function Mbrw2Sx5()

Local cAlias := "SX5"

Private cCadastro := "Arquivo de Tabelas"
Private aRotina := {}
Private cDelFunc := ".T." // Validacao para a exclusao. Pode-se utilizar ExecBlock

AADD(aRotina,{"Pesquisar"  ,"AxPesqui" ,0,1})
AADD(aRotina,{"Visualizar" ,"U_SX52Vis",0,2})
AADD(aRotina,{"Incluir"    ,"U_SX52Inc",0,3})
AADD(aRotina,{"Alterar"    ,"U_SX52Alt",0,4})
AADD(aRotina,{"Excluir"    ,"U_SX52Exc",0,5})

dbSelectArea(cAlias)
dbSetOrder(1)
mBrowse(6,1,22,75,cAlias)

Return

User Function SX52INC(cAlias,nReg,nOpc)

// Local nUsado     := 0
Local cTitulo       := "Inclusao de itens - Arquivo de Tabelas"
Local aCab          := {}//Array com descricao dos campos do Cabecalho
Local aRoda         := {}//Array com descricao dos campos do Rodape do Modelo 2
Local aGrid         :={80,005,050,300} //Array com coordenadas da GetDados no modelo 2 - Padrao: {44,5,118,315}

//Linha Inicial - Coluna Inicial - +Qts Linhas - +Qts Colunas: {080,005,050,300}

Local cLinhaOk      :="AllwaysTrue()" //Validacoes a linha da GetDados do modelo 2
Local cTudoOK       :="AllwaysTrue()" //Validacao geral da GetDados do Modelo 2 
Local lRetMod2      := .F. //Retorno da Funcao Modelo2 - .T. Confirmo / .F. Cancelou
Local nColuna       := 0

// Variaveis para GetDados()
Private aCols       :={}
Private aHeader     :={}

//Variaveis para campos da Enchoice()
Private cX5Filial   := xFilial("SX5")
Private cX5Tabela   := SPACE(5)

//Montagem do array de cabe�alho 
//AADD(aCab,{"Variavel"  ,{L,C} ,"Titulo","Picture","Valid","F3",lEnable})
AADD(aCab,{"cX5Filial" ,{015,010} , "Filial","@!",,,.F.})
AADD(aCab,{"cX5Tabela" ,{015,080} , "Tabela","@!",,,.T.})

//Montagem do aHeader
AADD(aHeader,{"Chave"     ,"X5_CHAVE","@!",5,0,"AllwaysTrue()","","C","","R"})
AADD(aHeader,{"Descricao" ,"X5_DESCRI","@!",40,0,"AllwaysTrue()","","C","","R"})

//Montagem do aCols
aCols := Array(1,Len(aHeader)+1)

//Inicializacao do aCols
For nColuna := 1 to len(aHeader)

	if aHeader[nColuna][8] == "C"
		aCols[1][nColuna] := SPACE(aHeader[nColuna][4])
	ElseIf aHeader[nColuna][8] == "N"
	    aCols[1][nColuna] := 0
	ElseIf aHeader[nColuna][8] =="D"   
		aCols[1][nColuna] := CTOD("")
	ElseIf aHeader[nColuna][8] == "L"
	    aCols[1][nColuna] := .F.
	ElseIf aHeader[nColuna][8] == "M"
		aCols[1][nColuna] := "" 
	Endif

Next nColuna

aCols[1][Len(aHeader)+1] := .F. //Linha n�o deletada
lRetMod2 := Modelo2(cTitulo,aCab,aRoda,aGrid,nOpc,cLinhaOk,cTudoOK)

If lRetMod2
	//MsgInfo("Voc� confirmou a opera��o","MBRW2SX5")
	For nLinha := 1 to len(aCols)
		//Campos de Cabe�alho
		Reclock("SX5",.T.)
		SX5->X5_FILIAL := cX5Filial
		SX5->X5_TABELA :=cX5Tabela
		//Campos do aCols
		//SX5->X5_CHAVE  := aCols[nLinha][1]
		//SX5->X5_DESCRI := aCols[nLinha][2]
		For nColuna := 1 to Len(aHeader)
			SX5->&(aHeader[nColuna][2]) := aCols[nLinha][nColuna]
		Next nColuna
		MsUnlock()
Next nLinha	
Else
		MsgAlert("Voc� cancelou a opera��o","MBRW2SX5")
EndIf
Return 
			   	 	
		
		
		
		
		
		
		
		
		









