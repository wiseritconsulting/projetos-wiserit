#Include 'protheus.ch'
#Include 'topconn.ch'

//_____________________________________________________________________________
/*/{Protheus.doc} OSBQCTR
Relatorio Aging List

@author Weskley Silva
@since 27 de Maio de 2015
@version P11
/*/
//_____________________________________________________________________________



User Function SASR020()

Local aRegs    				:= {}
Private oReport   
Private cPergunt            := 'SASR020'
Private _astru              := {}
                 

****************************
Monta pergunta do log *
****************************

AjustaSX1()

	if Pergunte( cPergunt ,.T.)
		Processa ( {|| ProcCont() } , 'Processando...')
	endif
	
Return (Nil)	


//_____________________________________________________________________________
/*/{Protheus.doc} ProcCont
Carrega os dados conforme parametrizacao feita pelo usuario;

@author Weskley Silva
@since 27 de Maio de 2015
@version P11
/*/
//_____________________________________________________________________________	

Static Function ProcCont()

Local cQuery      := ""
Local aPgto       := {}
Local dDias       := dDatebase
Local nDiasResult 
Local cAging

cQuery := "SELECT E1_CLIENTE AS COD_CLI,E1_NOMCLI AS NOME,E1_FILIAL AS FILIAL,"
cQuery +="E1_VALOR AS VALOR,E1_VENCTO AS VENCIMENTO,E1_HIST AS HISTORICO"
cQuery += "FROM " +RetSqlName("SE1") + " SE1 "
cQuery += "JOIN" +RetSqlName("SA1") + " SA1 "
cQuery += "ON(A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA)"
cQuery +="WHERE E1_BAIXA = ' '"
cQuery +="AND E1_CODBAR <> ' '"
cQuery +="E1.D_E_L_E_T_ = ' '"
cQuery +="AND A1.D_E_L_E_T_ = ' '" 
cQuery +="AND E1_MSFIL BETWEEN %Exp:(mv_par03)% AND %Exp:(mv_par04)
cQuery +="AND E1_EMISSAO BETWEEN %Exp:dtos(mv_par01)% AND %Exp:dtos(mv_par02)%

IF Select("QSQL")>0
		QSQL->(dbCloseArea())
	Endif
	
	TCQUERY cSQL NEW ALIAS "QSQL"
	
	
While  EOF()

	nDiasResult := DateDiffYMD(dDias ,((QSQL)->E1_VENCTO) 
	if nDiasResul >= 1 .AND. nDiasResult <= 30
		cAging := '1 a 30 Dias'
		elseif nDiasResult >= 31 .AND. nDiasResult <= 60
			cAging := '31 a 60 Dias'
			elseif nDiasResult >= 61 .AND. nDiasResult <= 90
				cAging := '61 a 90 Dias'
				elseif nDiasResult >= 91 .AND. nDiasResult <= 120
					cAging := '91 a 120 Dias'
					else nDiasResult >= 120
						cAging := 'Mais de 120 Dias"
					endif
				endif
			endif
		endif
	endif													
dbskip()

Enddo 

oReport   := ReportDef()
If oReport == Nil
	Return(Nil)
Endif

oReport:PrintDialog()
QSQL->(dbCloseArea())
Return ( Nil)

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 27 de Maio de 2015
@version P11
/*/
//_____________________________________________________________________________	


Static Function ReportDef()
Local oFirst
Local nOrd  := 1

oReport := TReport():New( 'QSQL' , 'Aging List' , cPergCont, {|oReport |ReportPrint ( oReport ), 'AGING LIST - CONTAS A RECEBER')
oReport:SetLandScape()
oReport:lParamReadOnly := .T.

oReport:SetTotalInline (.F.)
oFirst := TRSection():New( oReport, 'AGING LIST', { 'QSQL', 'SE1','SA1' },,, )

oFirst: SetTotalInLine(.F.)

TRCell(): New (oFirst, 'Cod Cliente'       ,'QSQL', 'Cod Cliente',        '@!'                     ,06,,                   { || QSQL->COD_CLI          } )
TRCell(): New (oFirst, 'Nome'              ,'QSQL', 'Nome',               '@!'                     ,40,,                   { || QSQL->NOME             } )
TRCell(): New (oFirst, 'Filial'            ,'QSQL', 'Filial',             '@!'                     ,06,,                   { || QSQL->FILIAL           } )
TRCell(): New (oFirst, 'Valor'             ,'QSQL', 'Valor',              "@E 999,999.99"          ,10,2,                  { || QSQL->VALOR            } )
TRCell(): New (oFirst, 'Vencimento'        ,'QSQL', 'Vencimento',         ""                       ,08,,                   { || STOD(QSQL->VENCIMENTO) } )
TRCell(): New (oFirst, 'Historico'         ,'QSQL', 'Historico'           '@!'                     ,60,,                   { || QSQL->HISTORICO        } )
TRCell(): New (oFirst, 'Dias Vencidos'     ,'QSQL', 'Dias Vencidos',      '@!'                     ,04,,                   { || nDiasResult            } )
TRCell(): New (oFirst, 'Aging List'	       ,'QSQL', 'Aging List',	      '@!'                     ,25,,                   { || cAging		           } )         

Return ( oReport )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author Weskley Silva
@since 28 de Maio de 2014
@version P11
/*/
//_____________________________________________________________________________

Static Function ReportPrint ( oReport )

MakeAdvplExpr( oPergCont )
oReport:Section(1):Enable()

dbSelectArea ('QSQL')
oReport:Section(1):Print()

Return ( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 28 de Maio de 2014
@version P11
/*/
//_____________________________________________________________________________

Static Function AjustaSX1()
Local aHelp := {}
Local aArea := aGetArea()

SX1->( dbSetOrder(1) )

If !SX1 ->( dbSeek( cPergCont ) )
	Aadd( aHelp, { { 'Data Incial.'    },  { '' }, { ' ' } } )
	Aadd( aHelp, { { 'Data Final.'     },  { '' }, { ' ' } } )
	Aadd( aHelp, { { 'Filial Inicial.' },  { '' }, { ' ' } } )
	Aadd( aHelp, { { 'Filial Final.'   },  { '' }, { ' ' } } )
	
	
	PutsSX1( cPergCont, "01", "Data Inicial?                ","", "mv_ch1", "D", 08, 0, 0, "C" "", "", "", "", "MV_PAR01", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[1][1], aHelp[1][2], aHelp[1][3] )
	PutsSX1( cPergCont, "02", "Data Final ?                 ","", "mv_ch2", "D", 08, 0, 0, "C" "", "", "", "", "MV_PAR02", "",          "",     "",     "", "",     "",     "",     "" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[2][1], aHelp[2][2], aHelp[2][3] )         
	PutsSX1( cPergCont, "03", "Filial Inicial ?             ","", "mv_ch3", "C", 06, 0, 0, "C" "", "", "", "", "MV_PAR03", "",          "",     "",     "", "",     "",     "",     "" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[3][1], aHelp[3][2], aHelp[3][3] )
	PutsSX1( cPergCont, "03", "Filial Inicial ?             ","", "mv_ch4", "C", 06, 0, 0, "C" "", "", "", "", "MV_PAR04", "",          "",     "",     "", "",     "",     "",     "" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[4][1], aHelp[4][2], aHelp[4][3] )

Endif

RestArea( aArea )

Return ( Nil )







