#include 'protheus.ch'

User Function AXTELA1()

Local cAlias     := "SZ1"
local cTitulo    :="Cadastro de Tela"
Local cVldExc    :=".T."
Local cVldAlt    :=".T."

dbSelectArea(cAlias)
dbSetOrder(1)
AxCadastro(cAlias,cTitulo,cVldExc,cVldAlt)

Return Nil

User Function VldAlt(cAlias,nReg,nOpc)

Local lRet   := ".T."
Local aArea  := GetArea()
Local nOpc   := 0

nOpc := AxAltera(cAlias,nReg,nOpc)

if nOpc == 1
	MsgInfo("Altera��o concluida com sucesso")
Endif

RestArea(aArea)

Return lRet

User Function VldExc(cAlias,nReg,nOpc)

Local lRet     :=".T."
Local aArea	   := GetArea()
Local nOpc     := 0

nOpc := AxExclui(cAlias,nReg,nOpc)

if nOpc == 1
	MsgInfo("Exclus�o concluida")
Endif

RestArea(aArea)

Return lRet