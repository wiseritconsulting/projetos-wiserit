#include "rwmake.ch"
#include "Topconn.ch"
#include "Protheus.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���P.Entrada �  F200TIT � Autor �   Carlos Meneses      � Data � 23/09/14 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � No momento da baixa do t�tulo via cnab o sistema dever�    ���
��� gerar NCC                                                             ���
�������������������������������������������������������������������������Ĵ��
����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

USER FUNCTION F200TIT()

Local aArea    := GetArea()
Local aAreaSE1 := SE1->(GetArea())
Local aArray   := {}
Local nCont
Local cChavSE1 := SE5->(E5_CLIFOR+E5_LOJA+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO)

Private lMsErroAuto := .F.

If SUBSTR(ALLTRIM(SE5->E5_PREFIXO),1,1)$"A" .AND. ALLTRIM(SE5->(E5_MOTBX+E5_RECPAG))=="NORR"  //Prefixo tiver a primeira posi��o "A" (t�tulos gerados por Contrato) e Baixa Normal e for Recebimento
	
	Begin Transaction
	
	dbSelectArea("SE1")
	SE1->(dbgotop())
	dbSetOrder(2)
	
	If dbSeek(xFilial("SE1")+SE1->(cChavSE1))
		
		aArray := {	{ "E1_FILIAL"  , SE5->E5_FILIAL     , NIL },;
		{ "E1_PREFIXO"  , SE5->E5_PREFIXO   			, NIL },;
		{ "E1_NUM"      , SE5->E5_NUMERO				, NIL },;
		{ "E1_PARCELA"  , SE5->E5_PARCELA   			, NIL },;
		{ "E1_TIPO"     , "NCC"  						, NIL },;
		{ "E1_NATUREZ"  , SE5->E5_NATUREZ				, NIL },;
		{ "E1_CLIENTE"  , SE5->E5_CLIFOR   				, NIL },;
		{ "E1_LOJA"		, SE5->E5_LOJA	    			, NIL },;
		{ "E1_NOMCLI"	, POSICIONE("SA1",1,xFilial("SA1")+SE5->(E5_CLIFOR+E5_LOJA),"A1_NREDUZ")	, NIL },;
		{ "E1_EMISSAO"  , SE5->E5_DATA					, NIL },;
		{ "E1_VENCTO"   , SE5->E5_DATA					, NIL },;
		{ "E1_VENCREA"  , SE5->E5_DATA					, NIL },;
		{ "E1_BAIXA"  	, CTOD(" / / ")					, NIL },;
		{ "E1_MOVIMEN" 	, CTOD(" / / ")					, NIL },;
		{ "E1_VALLIQ" 	, 0        						, NIL },;
		{ "E1_OK" 		, SPACE(02)						, NIL },;
		{ "E1_SALDO" 	, SE5->E5_VALOR					, NIL },;
		{ "E1_VALOR"    , SE5->E5_VALOR					, NIL },;
		{ "E1_HIST"     , POSICIONE("SE1",2,xFilial("SE1")+SE5->(E5_FILIAL+E5_CLIFOR+E5_LOJA+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO),"E1_HIST")	, NIL } }
		
		MsExecAuto( { |x,y| FINA040(x,y)}, aArray, 3 )
		
		If lMsErroAuto
			Mostraerro()
			DisarmTransaction()
		Endif
		
	EndIf
	
	End Transaction
	
EndIf

lMsErroAuto := .F.

RestArea(aAreaSE1)
RestArea(aArea)

Return
