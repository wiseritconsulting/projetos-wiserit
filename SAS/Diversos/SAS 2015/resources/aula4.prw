//+--------------------------------------------------------------------+
//Rotina | Mod3Inc | Autor | Weskley Silva (rLeg) | Data | 01.01.2007 |
//+--------------------------------------------------------------------+
//Descr. | Funcao Exemplo do prototipo Modelo3.
//+--------------------------------------------------------------------+

#Include "Protheus.ch"

User Function xModelo3()
Private cCadastro :="Prototipo Modelo 3"
Private aRotina :={}
Private oCliente
Private oTotal
Private oClientes := ""
Private nTotal := 0

Private bCampo := {|nField| FieldName(nField) }
Private aSize := {}
Private aInfo := {}
Private aObj := {}
Private aPobj := {}
Private aPGet := {}

//Retorna a area ultil das janelas Protheus
aSize := MsAdvSize()

//Ser� ultilizado tres areas na janela
//1� - Enclhoice, sendo 80 pontos pixel
//2� - MsGetDados, o que sobrar em pontos pixel � para objeto
//3� - Rodap� que � a propria janela, sendo 15 pontos pixel
AADD( aObj, { 100,080, .T., .F. })
AADD( aObj, { 100,100, .T., .F. })
AADD( aObj, { 100,100, .T., .F. })

//Calculo automatico sa dimensoes dos objetos (altura/largura) em pixel
aInfo := { aSize[1], aSize[2], aSize[3], aSize[4], 3,3}
aPObj := MsObjSize( aInfo, aObj )

//Calculo automatico de dimensoes dos objetos MSGET 
aPGet := MsObjGetPos( (aSize[3] - aSize[1], 315, { {004, 024, 240, 270} } )

AADD( aRotina, {"Pesquisar"   ,"AxPesqui" ,0,1})
AADD( aRotina, {"Visualizar"  ,'U_Mod3Mnt',0,2})
AADD( aRotina, {"Incluir"     ,'U_Mod3Inc',0,3})
AADD( aRotina, {"Alterar"     ,'U_Mod3Mnt',0,4})
AADD( aRotina, {"Excluir"     ,'U_Mod3Mnt',0,5})

dbSelectArea("ZA1")
dbSetOrder(1)
dbGoTop()
MBrowse(,,,,"ZA1")
Return






