/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RECXHORA  �Autor  �Marcio de Lima      � Data �  09/13/05   ���
�������������������������������������������������������������������������͹��
���Desc.     � Tem o objetivo de realizar um pareto com tres series       ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP7                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

#include "rwmake.ch"
#include "topconn.ch"

User Function RECXHORA()
Local   Titulo           := "Distribui��o de RECURSO x HOMENS HORA"
Local   cDesc1           := "Obs: O Codigo do Recurso devera ser preechido conforme exemplo."
Local   cDesc2           := "     FT####/XXXXXX/MART##/IF####/PP1###/"
Local   cDesc3           := " "
Local   cPict            := " "
Local   Cabec1           := " "
Local   Cabec2           := " "
Local   imprime          := .T.
Local   aOrd             := {}
Local   cDestino         := space(30)
Private AdcTitu          := ""
Private aVetRel          := {}
Private aTotRel          := {}
Private TitRel           := 'RECURSO             '
Private cQuebra          := SPACE(07)
Private lEnd             := .F.
Private lAbortPrint      := .F.
Private limite           := 132
Private nLin             := 80
Private nTipo            := 15
Private nLastKey         := 0    
Private cbtxt            := Space(10)
Private cbcont           := 00
Private CONTFL           := 01
Private m_pag            := 01
Private xRecH            := .t.
Private tamanho          := "G"
Private nomeprog         := "RECXHORA" // Coloque aqui o nome do programa para impressao no cabecalho
Private aReturn          := {"Zebrado", 1, "Administracao", 2, 2, 1, "", 1}
Private cString          := "   " //dicionario.     
Private cStrSql          := " "                     
Private wnrel            := "RECXHORA"
Private cPerg            := "RECHOR"
Private aPerg            := {}   
Private aVetGr01  		 := {}
Private aVetGr02  	  	 := {}
Private aVetGr03         := {}

Aadd(aPerg,{cPerg,"Da Data    ?    ","D",08,0,"G","","","","","","",""})
Aadd(aPerg,{cPerg,"Ate a Data ?    ","D",08,0,"G","","","","","","",""})
Aadd(aPerg,{cPerg,"Do Tipo        :","C",02,0,"G","","44","","","","",""})
Aadd(aPerg,{cPerg,"Ate o Tipo     :","C",02,0,"G","","44","","","","",""})
Aadd(aPerg,{cPerg,"Do Produto     :","C",15,0,"G","","SB1","","","","",""})
Aadd(aPerg,{cPerg,"Ate o Produto  :","C",15,0,"G","","SB1","","","","",""})
Aadd(aPerg,{cPerg,"Selec. Recurso ?","C",03,0,"C","","","SIM","NAO","","",""})
Aadd(aPerg,{cPerg,"Recursos ......","C",90,0,"G","","","","","","",""})
Aadd(aPerg,{cPerg,"Recursos ......","C",90,0,"G","","","","","","",""})
Aadd(aPerg,{cPerg,"Recursos ......","C",90,0,"G","","","","","","",""})
Aadd(aPerg,{cPerg,"Recursos ......","C",90,0,"G","","","","","","",""})
Aadd(aPerg,{cPerg,"Recursos ......","C",90,0,"G","","","","","","",""})
Aadd(aPerg,{cPerg,"Recursos ......","C",90,0,"G","","","","","","",""})
Aadd(aPerg,{cPerg,"Grafico ?      ","C",03,0,"C","","","SIM","NAO","","",""})

ExecBlock("TestSX1",.F.,.F.,{cPerg,aPerg})

Pergunte(cPerg,.F.)

wnrel := SetPrint(cString,wnrel,cPerg,titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.T.,Tamanho)

If (nLastKey == 27)
	Return
Endif
                  
//============================================================================================================================================================================================================================"
//Recurso                24/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00   00/00
//1------------------  9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99
//20                   22      30      38      46      54      62      70      78      86      94      102     110     118     126     134     142     150     158     166     174     182     190     198     206     214             

MsgRun("Aguarde, Gerando informa��es do Relatorio !..",, {|| GERAEFI(TITULO)})

Titulo:=Titulo+AdcTiTu 

SetDefault(aReturn,cString)

//If (nLastKey == 27)
//	Return
//Endif

If xRecH
   Help("",1,"","Ajuda","Nao existe informa��o.",1,0)
   dbSelectArea("TM0")
   dbCloseArea()

   dbSelectArea("TM1")
   dbCloseArea()

   dbSelectArea("TM2")
   dbCloseArea()
   RETURN .t.
Endif         


RptStatus({|| RunReport(Cabec1,Cabec2,Titulo,nLin) },Titulo)

Set Device To Screen

dbSelectArea("TM0")
dbCloseArea()

dbSelectArea("TM1")
dbCloseArea()

dbSelectArea("TM2")
dbCloseArea()

If aReturn[5] == 1
	dbcommitAll()
	ourspool(wnrel)
Endif

MS_FLUSH()

Return

*******************************
Static Function GERAEFI(TITULO)
*******************************  
xstring=space(1)
if MV_PAR07=1
   MV_PAR08:=alltrim(MV_PAR08)+alltrim(MV_PAR09)+alltrim(MV_PAR10)+alltrim(MV_PAR11)+alltrim(MV_PAR12)+alltrim(MV_PAR13)
   For cnt  := 1 TO len(alltrim(MV_PAR08))
       MV_PAR08:=iif(substr(MV_PAR08,cnt,1) $ 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/',MV_PAR08,Stuff(MV_PAR08,cnt,1,' '))
   Next cnt
   xString="("+iif(len(alltrim(substr(MV_PAR08,01,06)))>0,"'"+substr(MV_PAR08,01,06)+"'","")
   For cnt  := 8 TO len(alltrim(MV_PAR08)) step 7
       xString=xString+iif(len(alltrim(substr(MV_PAR08,cnt,06)))>0,",'"+substr(MV_PAR08,cnt,06)+"'","")
   Next cnt
   xString=" and H.H1_CODIGO in "+xString+")"
endif

cStrSql :="select H.H1_DESCRI,S.H6_MOBEFE,S.H6_TIPO,S.H6_TEMPO,S.H6_DATAINI,S.H6_DATAFIN  from "+retsqlname("SH6")+" S inner join "+retsqlname("SH1")+" H ON (H.H1_CODIGO=S.H6_RECURSO and  H.D_E_L_E_T_<>'*') where S.H6_FILIAL='"+XFILIAL("SH6")+"' and S.H6_TIPO='I'"
cStrSql +="and  S.H6_MOTIVO>='"+MV_PAR03+"' and S.H6_MOTIVO<='"+MV_PAR04+"' and S.D_E_L_E_T_<>'*' and S.H6_DATAFIN  >='"+DTOS(MV_PAR01)+"' and S.H6_DATAFIN  <='"+DTOS(MV_PAR02)+"'                                                                        "
cStrSql +="and S.H6_PRODUTO>='"+MV_PAR05+"' and S.H6_PRODUTO<='"+MV_PAR06+"'                                                                                                                                                                               "
cStrSql +=xString+" "
cStrSql +="union                                                                                                                                                                                                                                                                                  "
cStrSql +="select H.H1_DESCRI,S.H6_MOBEFE,S.H6_TIPO,S.H6_TEMPO,S.H6_DATAINI,S.H6_DATAFIN  from "+retsqlname("SH6")+" S inner join "+retsqlname("SH1")+" H ON (H.H1_CODIGO=S.H6_RECURSO and  H.D_E_L_E_T_<>'*') where S.H6_FILIAL='"+XFILIAL("SH6")+"' and S.H6_TIPO='P'"
cStrSql +="and S.D_E_L_E_T_<>'*' and S.H6_DATAFIN  >='"+DTOS(MV_PAR01)+"' and S.H6_DATAFIN  <='"+DTOS(MV_PAR02)+"'                                                                                                                                                                                "
cStrSql +="and S.H6_PRODUTO>='"+MV_PAR05+"' and S.H6_PRODUTO<='"+MV_PAR06+"'                                                                                                                                                                               "
cStrSql +=xString+" "
cStrSql +="order by H.H1_DESCRI,S.H6_DATAFIN                                                                                                                                                                                                                                                      "
TCQUERY cStrSql ALIAS "TM0" NEW             

aTempStru := {}
Aadd(aTempStru,{"RECU","C",20,0})
Aadd(aTempStru,{"DM01","C",08,0})
Aadd(aTempStru,{"DM02","C",08,0})
Aadd(aTempStru,{"DM03","C",08,0})
Aadd(aTempStru,{"DM04","C",08,0})
Aadd(aTempStru,{"DM05","C",08,0})
Aadd(aTempStru,{"DM06","C",08,0})
Aadd(aTempStru,{"DM07","C",08,0})
Aadd(aTempStru,{"DM08","C",08,0})
Aadd(aTempStru,{"DM09","C",08,0})
Aadd(aTempStru,{"DM10","C",08,0})
Aadd(aTempStru,{"DM11","C",08,0})
Aadd(aTempStru,{"DM12","C",08,0})
Aadd(aTempStru,{"DM13","C",08,0})
Aadd(aTempStru,{"DM14","C",08,0})
Aadd(aTempStru,{"DM15","C",08,0})
Aadd(aTempStru,{"DM16","C",08,0})
Aadd(aTempStru,{"DM17","C",08,0})
Aadd(aTempStru,{"DM18","C",08,0})
Aadd(aTempStru,{"DM19","C",08,0})
Aadd(aTempStru,{"DM20","C",08,0})
Aadd(aTempStru,{"DM21","C",08,0})
Aadd(aTempStru,{"DM22","C",08,0})
Aadd(aTempStru,{"DM23","C",08,0})
Aadd(aTempStru,{"DM24","C",08,0})
Aadd(aTempStru,{"DM25","C",08,0})
Aadd(aTempStru,{"DM26","C",08,0})
Aadd(aTempStru,{"DM27","C",08,0})
Aadd(aTempStru,{"DM28","C",08,0})
Aadd(aTempStru,{"DM29","C",08,0})
Aadd(aTempStru,{"DM30","C",08,0})
Aadd(aTempStru,{"DM31","C",08,0})
cArqTrab2 := CriaTrab(aTempStru,.T.)

dbUseArea( .T.,, cArqTrab2, "TM2",.F.,.F.)
IndRegua("TM2",cArqTrab2,"RECU",,,"Gerando Registros...")

aTempStru := {}
Aadd(aTempStru,{"RECU","C",20,0})
Aadd(aTempStru,{"REDM","C",04,0})
Aadd(aTempStru,{"RERE","N",08,2})
cArqTrab1 := CriaTrab(aTempStru,.T.)

dbUseArea( .T.,, cArqTrab1, "TM1",.F.,.F.)
IndRegua("TM1",cArqTrab1,"RECU+REDM",,,"Gerando Registros...")

dbSelectArea("TM0")
dbgotop()
while !eof()                                  
      xRecH=.f.
      tchv=H1_DESCRI+SUBSTR(H6_DATAFIN,5,4)
      xchv=H1_DESCRI+H6_DATAFIN
      horatdia=0         
      xrec=substr(H1_DESCRI,1,40)
      xdia=substr(H6_DATAFIN,5,4)
      while xchv=H1_DESCRI+H6_DATAFIN .and. !eof() 
            horatdia=iif(H6_MOBEFE=0,0,(fConvHr(val(STRTRAN(TM0->H6_TEMPO,':','.')),'D')*H6_MOBEFE))+horatdia 
            dbskip()
      end                                 
      dbSelectArea("TM1")      
      dbseek(tchv)
      if eof()
         append blank
      endif                          
      replace RECU with xrec,REDM with xdia, RERE with horatdia
      dbSelectArea("TM0")
end

If xRecH
   RETURN .t.
Endif         

dbSelectArea("SH1")      
dbgotop()
c=1
aVetRel := {}
aTotRel := {}
datx=MV_PAR01
For nct := 1 To 31
    xcam='DM'+strzero(nct,2)
    aAdd(aVetRel,{xcam,substr(dtos(datx),5,4)})
    aAdd(aTotRel,{0.0})
    aTotRel[nct]=0.0
    datx++
next   
while !eof()                                  
      dbSelectArea("TM1")
      dbseek(substr(SH1->H1_DESCRI,1,20))
      if found()
         dbSelectArea("TM2")      
         append blank
         replace RECU with substr(SH1->H1_DESCRI,1,20)     
      endif          
      dbSelectArea("SH1")      
      dbskip()
enddo                         
dbSelectArea("TM1")
dbgotop()
while !eof()  
      chvx=RECU
      dbSelectArea("TM2")
      dbseek(chvx)
      if found()
         For nct := 1 To 31
             if aVetRel[nct][2]=TM1->REDM
                xcam=aVetRel[nct][1]
                replace &xcam with transform(TM1->RERE,"@E 9999.99")
                aTotRel[nct]=aTotRel[nct]+TM1->RERE
             endif
         next    
      endif                          
      dbSelectArea("TM1")
      dbskip() 
enddo
dbSelectArea("TM2")
append blank         
replace RECU with replicate('-',22)
For nct := 1 To 31
    xcam='DM'+strzero(nct,2)
    replace &xcam with replicate('-',08)
next
append blank         
ultdia=substr(DTOC(MV_PAR02),1,5)
For nct := 1 To 31
    xcam='DM'+strzero(nct,2)
    replace &xcam with transform(aTotRel[nct],"@E 9999.99")
    if aTotRel[nct]>0.0
       titrel=titrel+space(3)+substr(aVetRel[nct][2],3,4)+'/'+substr(aVetRel[nct][2],1,2)
       ultdia=substr(aVetRel[nct][2],3,4)+'/'+substr(aVetRel[nct][2],1,2)
    endif
    Aadd(aVetGr03,{.t.,substr(aVetRel[nct][2],3,4)+'/'+substr(aVetRel[nct][2],1,2),aTotRel[nct]})
    Aadd(aVetGr02,{.f.,substr(aVetRel[nct][2],3,4)+'/'+substr(aVetRel[nct][2],1,2),0})
    Aadd(aVetGr01,{.f.,substr(aVetRel[nct][2],3,4)+'/'+substr(aVetRel[nct][2],1,2),0})
next

AdcTitu=' - Periodo ['+substr(DTOC(MV_PAR01),1,5)+' a '+ultdia+']'

if MV_PAR14=1
   u_gergrafo(titulo+AdcTitu)
endif
return .t.

Static Function RunReport(Cabec1,Cabec2,Titulo,nLin)
pag=1

@ prow(),pcol() psay chr(27)+chr(33)+chr(5)
store 0 to horadisp,horapdia,horatdia

dbSelectArea("TM2")
SetRegua(RecCount())
dbgotop()
while !eof()                                  
   	incregua() 
      If nlin>62   
         if pag>1
            @ nlin,  0 psay "============================================================================================================================================================================================================================"
         endif
         Cabec(Titulo,Cabec1,Cabec2,NomeProg,tamanho,nTipo)
  		   @ 6,  0 psay titrel
  	      pag++
         nlin=8                        
      endif         
      @ nlin,000 psay RECU
      xcol=021
      For nct := 1 To 31
          if aTotRel[nct]>0.0
             xcam=aVetRel[nct][1]
             @ nlin,xcol psay &xcam
             xcol=xcol+8
          endif      
      next
      nlin++
      dbskip()
enddo
if nlin<80
   @ nlin,  0 psay "============================================================================================================================================================================================================================"
endif          
SET DEVICE TO SCREEN
Return
