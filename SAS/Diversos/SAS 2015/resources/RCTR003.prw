#Include 'Protheus.ch'
#Include 'TopConn.ch'


//_____________________________________________________________________________
/*/{Protheus.doc} OSBQCTR
Relatorio de listagens de itens por contrato

@author Weskley Silva
@since 12 de Dezembro de 2014
@version P11
/*/
//_____________________________________________________________________________

User Function RCTR003()

Local aRegs			:= {}
Private oReport
Private cPergCont	:= 'RCONT003'
Private _astru		:={}
************************
*Monta pergunte do Log *
************************
AjustaSX1()

If Pergunte( cPergCONT, .T. )    
	Processa( { || ProcCont() }, 'Processando....' )
EndIf



Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ProcCont
Carrega os dados conforme parametrizacao feita pelo usuario;

@author Weskley Silva
@since 12 de Dezembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ProcCont()
Local cQuery	:= ""
Local aPagto    := {}

BeginSql Alias "TMP"


SELECT  DISTINCT
    A1.A1_COD AS 'COD_CLI',  
    C6_TES       AS 'TES',  
    Z5_PV01      AS 'OS',
    N3.Z5_EMPPV1 AS 'FILIAL_1', 
    F2_DOC      AS 'NF_1',
    N3.Z5_EMPPV3 as 'FILIAL_3',
    Z5_NF03 AS 'NF_3',
    F2_EMISSAO AS 'DATA_E',                           
    C5_SUGENT AS 'SUG_ENTREGA',
    CASE WHEN C5_FECENT <> '' THEN C5_FECENT ELSE '' END AS 'DT_ENTREGA',
    A1.A1_NOME AS 'CLIENTE',
    C5_YNOMREC AS 'RECEBEDOR',
    A4.A4_NOME AS 'TRANSP',
    C5_YCTRC AS 'CTRC', 
    F4_TEXTO AS 'DESC_TES', 
    A1.A1_MUN AS 'MUNICIPIO',
    A1.A1_EST AS 'ESTADO',
    C6_QTDVEN AS 'QUANTIDADE',
    CASE WHEN C5_NOTA <> '' THEN 'FATURADO'
    END AS 'STATUS_NF',
    C6_PRODUTO AS 'COD_PRO',
    C6_DESCRI  AS 'PRODUTO'
  
    FROM %table:SC5% C5       
      JOIN (SELECT Z5_CONTRAT,Z5_EMPPV1,Z5_CLIENTE,Z5_PV01,Z5_NF01,Z5_EMPPV3,Z5_PV03,Z5_NF03,Z5_MSFIL FROM %table:SZ5% Z5 WHERE Z5.%NOTDEL%) N3
                ON ((N3.Z5_EMPPV1 = C5_FILIAL OR N3.Z5_EMPPV3=C5_FILIAL) AND N3.Z5_CLIENTE=C5_CLIENTE 
                AND (N3.Z5_PV01 = C5_NUM OR N3.Z5_PV03 = C5_NUM) AND (N3.Z5_NF01 = C5_NOTA OR N3.Z5_NF03 = C5_NOTA))   
                   
      JOIN %table:SC6% C6 ON(C5.C5_FILIAL = C6.C6_FILIAL
                   AND C5_LOJACLI = C6.C6_LOJA 
                   AND C5.C5_CLIENT = C6.C6_CLI 
                   AND C5_NUM = C6_NUM AND C5_CONTRA = C6_CONTRAT)
     JOIN %table:SA1% A1 ON(A1_COD = C5.C5_CLIENT 
                   AND A1_LOJA = C5_LOJACLI) 
     JOIN %table:SA4% A4 ON(A4.A4_COD = C5.C5_TRANSP)   
     JOIN %table:SF4% F4 ON(F4_CODIGO = C6_TES AND F4_FILIAL = SUBSTRING(C6_FILIAL,1,2))
     LEFT JOIN %table:SF2% F2 ON(F2.F2_DOC = Z5_NF01 AND F2.F2_CLIENT = Z5_CLIENTE AND F2.F2_FILIAL = Z5_MSFIL)
     
     WHERE 
        C5.%NOTDEL%
        AND C6.%NOTDEL%
        AND A1.%NOTDEL%
        AND A4.%NOTDEL%
        AND C5_NOTA <> ''
        and Z5_PV01 = C5_NUM
        and F2_EMISSAO >= %Exp:dtos(mv_par01)% AND F2_EMISSAO <= %Exp:dtos(mv_par02)%
        and year(C5_SUGENT) >= 2014 
        
         
GROUP BY  C6_TES, 
          C5_FILIAL,
          C5_EMISSAO,
          C5_SUGENT,
          C5_FECENT,
          A1.A1_NOME,
          A4.A4_NOME,
          A1.A1_MUN,
          A1.A1_EST,        
          C5_NOTA,
          C6_PRODUTO,
          C6_DESCRI,
          F4_TEXTO,
          C5_NUM,
          C6_QTDVEN,
          C5_YNOMREC,
          C5_YCTRC,
          C5_YNOMREC,
          A1.A1_COD,
          C5_NUM,
          Z5_PV01,
          N3.Z5_PV03,
          N3.Z5_NF03,
          N3.Z5_EMPPV1,
          N3.Z5_EMPPV3,
          F2_DOC,
          F2_EMISSAO
      
   
UNION

SELECT DISTINCT
    A1.A1_COD AS 'COD_CLI',    
    C6_TES AS 'TES', 
    C5_NUM AS 'OS',
    C5_FILIAL AS 'FILIAL_1', 
    C5_NOTA AS 'NF_1',
    '' AS 'FILIAL_3', 
    '' AS 'NF_3',
    F2_EMISSAO AS 'DATA_E',
    C5_SUGENT AS 'SUG_ENTREGA',
    CASE WHEN C5_FECENT <> '' THEN C5_FECENT ELSE '' END AS 'DT_ENTREGA',
    A1.A1_NOME AS 'CLIENTE',
    C5_YNOMREC AS 'NOME_RECEBEDOR',
    A4.A4_NOME AS 'TRANSP',
    C5_YCTRC   AS 'CTRC',
    F4_TEXTO   AS 'DESC_TES',
    A1.A1_MUN  AS 'MUNICIPIO',
    A1.A1_EST  AS 'ESTADO',
    C6_QTDVEN  AS 'QUANTIDADE',
    CASE WHEN C5_NOTA <> '' THEN 'FATURADO'
    END        AS 'STATUS_NF',
    C6_PRODUTO AS 'COD_PRO',
    C6_DESCRI  AS 'PRODUTO'
    
    FROM %table:SF2% F2
    JOIN %table:SC5% C5 ON(F2.F2_DOC = C5_NOTA AND F2.F2_CLIENT = C5_CLIENTE AND F2.F2_FILIAL = C5_FILIAL)
      JOIN %table:SC6% C6 ON(C5.C5_FILIAL = C6.C6_FILIAL 
                   AND C5_LOJACLI = C6.C6_LOJA 
                   AND C5.C5_CLIENT = C6.C6_CLI 
                   AND C5_NUM = C6_NUM)
     JOIN %table:SA1% A1 ON(A1_COD = C5.C5_CLIENT 
                   AND A1_LOJA = C5_LOJACLI) 
     JOIN %table:SA4% A4 ON(A4.A4_COD = C5.C5_TRANSP)   
     JOIN %table:SF4% F4 ON(F4_CODIGO = C6_TES AND F4_FILIAL = SUBSTRING(C6_FILIAL,1,2))
     LEFT JOIN (SELECT Z5.Z5_EMPPV1,Z5_EMPPV2,Z5_EMPPV3,Z5.Z5_NF01,Z5.Z5_NF02,Z5.Z5_NF03,Z5.Z5_CLIENTE,F2.F2_CLIENT,Z5.D_E_L_E_T_ 
					FROM %table:SZ5% Z5 JOIN %table:SF2% F2 ON(Z5.Z5_EMPPV2=F2.F2_FILIAL AND Z5.Z5_NF02=F2.F2_DOC AND Z5.Z5_SERIE2=F2.F2_SERIE)) Z5 
					ON((Z5_EMPPV1=C5.C5_FILIAL OR Z5_EMPPV2=C5.C5_FILIAL OR Z5_EMPPV3=C5.C5_FILIAL) AND (Z5.Z5_CLIENTE=C5.C5_CLIENT OR Z5.F2_CLIENT=C5.C5_CLIENT) 
					AND(Z5.Z5_NF01=C5.C5_NOTA OR Z5.Z5_NF02=C5.C5_NOTA OR Z5.Z5_NF03=C5.C5_NOTA))         
     
     WHERE 
         C5.%NOTDEL%
        AND C6.%NOTDEL%
        AND A1.%NOTDEL%
        AND A4.%NOTDEL%
        AND C5_NOTA <> ' '
        and year(C5_SUGENT) >= 2014  
        and F2_EMISSAO >= %Exp:dtos(mv_par01)% AND F2_EMISSAO <= %Exp:dtos(mv_par02)%
        AND Z5.Z5_CLIENTE IS NULL
        
GROUP BY  C6_TES,
          C5_FILIAL,
          C5_EMISSAO,
          C5_SUGENT,
          C5_FECENT,
          A1.A1_NOME,
          A4.A4_NOME,
          A1.A1_MUN,
          A1.A1_EST,        
          C5_NOTA,
          C6_PRODUTO,
          C6_DESCRI,
          F4_TEXTO,
          C5_NUM,
          C6_QTDVEN,
          C5_YNOMREC,
          C5_YCTRC,
          C5_YNOMREC,
          A1.A1_COD,
          F2_DOC,
          F2_EMISSAO,
          Z5_EMPPV1 , 
		  Z5_NF01
EndSql


oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
TMP->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 12 de Novembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportDef()
Local oFirst
Local nOrd	:= 1


oReport := TReport():New( 'TMP', 'FOLLOW UP', cPergCont, {|oReport| ReportPrint( oReport ), 'OPERACAO TRIANGULAR X AVULSO' } )
oReport:SetLandScape()
oReport:lParamReadOnly := .T.

oReport:SetTotalInLine( .F. )

oFirst := TRSection():New( oReport, 'FOLLOW UP', { 'TMP', 'SF2', 'SC5', 'SA1', 'SA4', 'SC6', 'SZ5', 'SF4' },,, )

oFirst:SetTotalInLine( .F. )


TRCell():New( oFirst, 'Cod_Cliente'		,'TMP', 'Cod_Cliente',			"@!"						    ,06,,							{ || TMP->COD_CLI 				} )
TRCell():New( oFirst, 'Tes'	       		,'TMP', 'Cliente' ,				"@!"							,03,,							{ || TMP->TES   				} )
TRCell():New( oFirst, 'OS'	       		,'TMP', 'OS' ,					"@!"							,08,,							{ || TMP->OS					} )
TRCell():New( oFirst, 'Filial_1'   		,'TMP', 'Filial_1',				"@!"						  	,06,,							{ || TMP->FILIAL_1				} )
TRCell():New( oFirst, 'NF_1'	   		,'TMP', 'NF_1',					"@!"  							,09,,							{ || TMP->NF_1					} )
TRCell():New( oFirst, 'Filial_3'   		,'TMP', 'Filial_3' ,			"@!"							,06,,							{ || TMP->FILIAL_3 	    		} ) 
TRCell():New( oFirst, 'NF_3'	   		,'TMP', 'NF_3',					"@!"  							,09,,							{ || TMP->NF_3		    		} )
TRCell():New( oFirst, 'Data'	   		,'TMP', 'Data',					""							  	,08,,							{ || STOD(TMP->DATA_E)   	    } )
TRCell():New( oFirst, 'DT Sug Entrega'	,'TMP', 'DT Sug Entrega' ,	    ""								,08,,							{ || STOD(TMP->SUG_ENTREGA)		} )
TRCell():New( oFirst, 'DT Entrega'		,'TMP', 'DT Entrega' ,	        ""								,08,,							{ || STOD(TMP->DT_ENTREGA)		} )
TRCell():New( oFirst, 'Cliente'			,'TMP', 'Cliente',				"@!"							,40,,							{ || TMP->CLIENTE				} )
TRCell():New( oFirst, 'Recebedor'		,'TMP', 'Recebedor',			"@!"							,30,,							{ || TMP->RECEBEDOR				} )
TRCell():New( oFirst, 'Transportadora'	,'TMP', 'Transportadora',		"@!"							,40,,							{ || TMP->TRANSP				} )
TRCell():New( oFirst, 'Ctrc'			,'TMP', 'Ctrc',					"@!"							,30,,							{ || TMP->CTRC					} )
TRCell():New( oFirst, 'Desc Tes'		,'TMP', 'Des Tes',				"@!"							,20,,							{ || TMP->DESC_TES				} )
TRCell():New( oFirst, 'Municipio'		,'TMP', 'Municipio',			"@!"							,60,,							{ || TMP->MUNICIPIO				} )
TRCell():New( oFirst, 'Estado'			,'TMP', 'Estado',				"@!"							,20,,							{ || TMP->ESTADO				} )
TRCell():New( oFirst, 'Quantidade'		,'TMP', 'Quantidade',			"@E 999,999.99"					,08,,							{ || TMP->QUANTIDADE			} )
TRCell():New( oFirst, 'Status'			,'TMP', 'Status',				"@!"							,09,,							{ || TMP->STATUS_NF				} )
TRCell():New( oFirst, 'Cod Produto'		,'TMP', 'Cod Produto',			"@!"							,15,,							{ || TMP->COD_PRO				} )
TRCell():New( oFirst, 'Produto'			,'TMP', 'Produto',				"@!"							,50,,							{ || TMP->PRODUTO				} )

//oBreakT := TRBreak():New( oFirst, { || }, 'Vlr. Total' )

//TRFunction():New( oFirst:Cell( 'ValFat2' ),	'', 'SUM', oBreakT,,,, .F., .F. )

//oFirst:Cell( 'TOTAL' ):SetHeaderAlign( 'RIGHT' )

//oFirst:SetColSpace(9)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author Francisco Valdeni
@since 24 de Novembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

MakeAdvplExpr( cPergCont )
oReport:Section(1):Enable()

dbSelectArea( 'TMP' )
oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 12 de Dezembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function AjustaSX1()
Local aHelp	:= {}
Local aArea	:= GetArea()

SX1->( dbSetOrder(1) )

If !SX1->( dbSeek( cPergCont ) )
	Aadd( aHelp, { { 'Data Inicial.'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'Data Final.' 		}, 	{ '' }, { ' ' } } )


	PutSx1( cPergCont, "01", "Data Inicial ?		", "", "", "mv_ch1", "D", 08, 0, 0, "C", "", "", "", "", "MV_PAR01", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[1][1], aHelp[1][2], aHelp[1][3] )
	PutSx1( cPergCont, "02", "Data Final ?      	", "", "", "mv_ch2", "D", 08, 0, 0, "C", "", "", "", "", "MV_PAR02", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[2][1], aHelp[2][2], aHelp[2][3] )

EndIf

RestArea( aArea )

Return( Nil)
