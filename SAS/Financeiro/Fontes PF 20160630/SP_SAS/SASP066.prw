#INCLUDE "rwmake.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FONT.CH"
#include "TOTVS.ch"
#INCLUDE "COLORS.CH"	
	
	
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �SASP066   � Autor � Francisco Valdeni  � Data �    23/05/16 ���
�����������������������������������������������������t�������������������͹��
���Descricao � Impress�o do Pedido Financeiro                             ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico Sistema Ari de S� (SAS)                         ���
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
User Function SASP066(cNumero,cCod,cAno,_cFilial,lTlConf)

//���������������������������������������������������������������������Ŀ
//� Declaracao de Variaveis                                             �
//�����������������������������������������������������������������������
Local cDesc1        := "Este programa tem como objetivo imprimir os "
Local cDesc2        := "pedidos financeiros - P.F com os "
Local cDesc3        := "parametros informados pelo usu�rio."                                                     
Local cPict         := ""
Local titulo       	:= "Pedido Financeiro - P.F"
Local nLin         	:= 80                          
Local Cabec1       	:= ""
Local Cabec2      	:= ""                                
Local imprime     	:= .T.
Local aOrd 			:= {} 
Local cNomeArq      := "" 
Local aArea			:= GetArea()
Private	cNatureza	:= ""
Private cConta		:= ""
Private cDescCta	:= ""
Private lEnd        := .T.
Private lAbortPrint	:= .T.
Private limite     	:= 220
Private tamanho    	:= "G"
Private nomeprog   	:= "SASP066" //Coloque aqui o nome do programa para impressao no cabecalho
Private nTipo      	:= 18
Private aReturn    	:= { "Zebrado", 10, "Administracao", 1, 2, 1, "", 1} 
Private nLastKey   	:= 0
Private cbtxt      	:= Space(10)
Private cbcont     	:= 00
Private CONTFL     	:= 01
Private m_pag      	:= 01
Private wnrel      	:= "SASP066" // Coloque aqui o nome do arquivo usado para impressao em disco
Private cString 	:= "ZA9"             
Private cNum 		:= cNumero
Private cCli    	:= ""
Private cSub    	:= ""
Private cNom    	:= ""
Private cSer    	:= ""
Private lRetorno	:= .T.	
Private aEquip  	:= {} 
Private	cTitulo		:= "Imp. P.F - Pedido Financeiro"
Private nLin		:= 0
Private	nLinas		:= 0
Private	i			:= 0
Private _cCod		:= cCod
Private _cAno		:= cAno  
Private _lTlConf	:= lTlConf
Private _xFilial	:= _cFilial


nTipo := If(aReturn[4]==1,15,18)   

IF !Empty(Alltrim(_cFilial))
	dbSelectArea("ZA7")
	ZA7->(dbsetOrder(2))
	ZA7->(dbGotop())
	ZA7->(dbSeek(_cFilial+cNum))
Else
	dbSelectArea("ZA7")
	ZA7->(dbsetOrder(2))
	ZA7->(dbGotop())
	ZA7->(dbSeek(xFilial("ZA7")+cNum))
ENDIF



//���������������������������������������������������������������������Ŀ
//� Processamento. RPTSTATUS monta janela com a regua de processamento. �
//�����������������������������������������������������������������������

RptStatus({|| SASP066Imp(Cabec1,Cabec2,Titulo,nLin) },Titulo)

ZA9->(dbCloseArea())
RestArea(aArea)
Return lRetorno    

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Fun��o    �ESTR050   � Autor � Andreia J.da Silva � Data �    12/02/07 ���
�������������������������������������������������������������������������͹��
���Descri��o � Impressao do Protocolo de Entrega de Selo			 	  ���
���          � 												              ���
��������������������������������������������������������������'            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function SASP066Imp(Cabec1,Cabec2,Titulo,nLin)
Local oDlg := Nil
Local nA

Private nPagina := 0
Private ArialN09     := TFont():New("Arial"      ,,09  ,,.F.,,,,,.F.)
Private ArialN10     := TFont():New("Arial"      ,,10  ,,.F.,,,,,.F.)
Private ArialN10B    := TFont():New("Arial"      ,,10  ,,.T.,,,,,.F.)
Private ArialN12     := TFont():New("Arial"      ,,12  ,,.F.,,,,,.F.)
Private ArialN12B    := TFont():New("Arial"      ,,12  ,,.T.,,,,,.F.)
Private cLogo 		 := GetSrvProfString ("STARTPATH","")+"LGMID.PNG"/*+Alltrim(cEmpAnt)+"*/
           


Begin Sequence
oPrint := TMSPrinter():New()
oPrint:SetPortrait()
oPrint:SetLandScape(.T.)
oPrint:StartPage()

Define FONT oFont1 NAME "Arial" Size 0,10 of oPrint BOLD
Define FONT oFont2 NAME "Arial" Size 0,10 of oPrint 
Define FONT oFont3 NAME "Arial" Size 0,12 of oPrint 
Define FONT oFont4 NAME "Arial" Size 0,08 of oPrint BOLD
Define FONT oFont5 NAME "Arial" Size 0,16 of oPrint BOLD
Define FONT oFont6 NAME "Courier New" BOLD
Define FONT oFont7 NAME "Arial" Size 0,10 of oPrint BOLD


//���������������������������������������������������������������������Ŀ
//� Monta a interface padrao com o usuario...                           �
//�����������������������������������������������������������������������

IF _lTlConf

	DEFINE MSDIALOG oDlg FROM 264,182 TO 441,613 TITLE cTitulo OF oDlg PIXEL
	@ 004,010 TO 082,157 LABEL "" OF oDlg PIXEL
	
	@ 015,017 SAY "Esta rotina tem por objetivo imprimir" 	OF oDlg PIXEL Size 150,010 FONT oFont6 COLOR CLR_HBLUE
	@ 030,017 SAY "os Pedidos financeiros - P.F " OF oDlg PIXEL Size 150,010 FONT oFont6 COLOR CLR_HBLUE
	@ 045,017 SAY "referente ao Documento" 					OF oDlg PIXEL Size 150,010 FONT oFont6 COLOR CLR_HBLUE
	@ 060,017 SAY "N�mero.: "+Alltrim(cNum) 				OF oDlg PIXEL Size 150,010 FONT oFont6 COLOR CLR_HBLUE                                         
	
	@  6,167 BUTTON "&Imprime" SIZE 036,012 ACTION ImpCarta(1)   	OF oDlg PIXEL
	@ 28,167 BUTTON "&Setup"   SIZE 036,012 ACTION oPrint:Setup()   OF oDlg PIXEL
	@ 49,167 BUTTON "Pre&view" SIZE 036,012 ACTION ImpCarta(2) 		OF oDlg PIXEL
	@ 70,167 BUTTON "Sai&r"    SIZE 036,012 ACTION oDlg:End() 		OF oDlg PIXEL
	
	ACTIVATE MSDIALOG oDlg CENTERED
	
Else 
	ImpCarta(2)
ENDIF
	
	
	
	MS_FLUSH()
	
	End Sequence
	
	lRetorno := .T.

Return


Static Function ImpCarta(nOpc)

oPrint:StartPage()

TECCabec(cNum)
TECCabec2(cNum)
TECCabecItem()
ImpItens()
TECRodape()

oPrint:Endpage()


IF nOpc == 1
	oPrint:Print()
Elseif nOpc == 2
	oPrint:Preview()
ENDIF


oPrint:EndPage()

MS_FLUSH()  



Return .T.


////////////////////////////////////////////////////////////
//CABECALHO PRINCIPAL                                       
///////////////////////////////////////////////////////////
Static Function TECCabec()

nPagina++ //-- Variavel Private

oPrint:SayBitmap(0005,0100,cLogo,300,260) // Logo SAS
oPrint:Say(0100,2970,"Folha....: " + StrZero(nPagina,3),ArialN10)
oPrint:Say(0150,2970,"Data.....: " + Dtoc(dDataBase),ArialN10) 

oPrint:Line(0200,0100,0200,3300)

Return

////////////////////////////////////////////////////////////
//MONTA CABECALHO DE RETIRADA OU REMESSA DE EQUIPAMENTO
///////////////////////////////////////////////////////////
Static Function TECCabec2()


dbSelectArea("SA2")
SA2->(dbSetOrder(1))
SA2->(dbGotop())
SA2->(dbSeek(xFilial("SA2")+ZA7->ZA7_FORNEC+ZA7->ZA7_LOJA))

//If nPagina = 1
	
	oPrint:Line(0300,0100,0300,3300)
	oPrint:Say(0200,0100,Alltrim(SM0->M0_NOMECOM) ,ArialN10B)
	oPrint:Say(0250,0100,"P.F - PEDIDO FINANCEIRO",ArialN12B)
	oPrint:Say(0200,1050,"Numero" ,ArialN10B)
	oPrint:Say(0250,1050,ZA7->ZA7_NUM ,ArialN10B)	
	oPrint:Say(0200,1300,"TIPO" ,ArialN10B)
	oPrint:Say(0250,1300,ZA7->ZA7_TIPO ,ArialN10B)	
	oPrint:Say(0200,1600,"M�s Refer�ncia" ,ArialN10B)
	oPrint:Say(0250,1600,ZA7->ZA7_MESREF+"/"+ZA7->ZA7_ANOREF ,ArialN10B)	
	oPrint:Say(0200,1900,"Emiss�o" ,ArialN10B)
	oPrint:Say(0250,1900,dtoc(ZA7->ZA7_EMISSA) ,ArialN10B)	
	oPrint:Say(0200,2200,"Vencimento" ,ArialN10B)
	oPrint:Say(0250,2200,dtoc(ZA7->ZA7_VENCTO) ,ArialN10B)	

	
	
	oPrint:Say(0350,0100,"CONTRATADA : "+ZA7->ZA7_FORNEC+"/"+ZA7->ZA7_LOJA+" - "+Alltrim(SA2->A2_NOME) ,ArialN12B)
	oPrint:Say(0400,0100,"CNPJ / CPF : "+Transform(SA2->A2_CGC, "@R 99.999.999/9999-99") ,ArialN12)
	
	oPrint:Line(0450,0100,0450,3300) // Cria Linha __________________________________________________
		oPrint:Say(0500,0100,"DESCRI��O DOS SERVI�OS",ArialN10)
	oPrint:Line(0550,0100,0550,3300) // Cria Linha __________________________________________________

	nLinhas	:= MlCount(Alltrim(ZA7->ZA7_DESCRI),150)
	nLin:= 0600	
	For i:=1 To nLinhas
		oPrint:Say(nLin,0100,MEMOLINE(ZA7->ZA7_DESCRI,150,I),ArialN10)
		nLin := nLin+50		
	Next i

	
Return

///////////////////////////////////////////////////////////
//CABECALHO DO ITEM                                        
///////////////////////////////////////////////////////////
Static Function TECCabecItem()
oPrint:Line(900,0100,900,3300)//-------------------------

OPRINT:LINE(0900,0100,2000,0100 )// Natureza        	// HORIZONTAL 
 oPrint:Say(0900,0110,"Natureza" ,ArialN10B) 



OPRINT:LINE(0900,0330,2000,0330 )// Conta Cont�bil     // HORIZONTAL 
 oPrint:Say(0900,0340,"Desc Natureza" ,ArialN10B) 

//OPRINT:LINE(0900,0640,2000,0640 )// Desc da Conta  	// HORIZONTAL 
//oPrint:Say(0900,0650,"Descri��o Cta Conta Cont�bil",ArialN10B)


OPRINT:LINE(0900,1200,2000,1200 )// Centro de Custo    // HORIZONTAL 
 oPrint:Say(0900,1210,"Conta Cont�bil",ArialN10B)

OPRINT:LINE(0900,1480,2000,1480 )
 oPrint:Say(0900,1490,"Descri��o Cta Conta Cont�bil",ArialN10B)

OPRINT:LINE(0900,2150,2000,2150 )
 oPrint:Say(0900,2160,"C. Custo",ArialN10B)

OPRINT:LINE(0900,2390,2000,2390 )
oPrint:Say(0900,2400,"Descri��o Centro de Custo",ArialN10B)

OPRINT:LINE(0900,2900,2000,2900)
 oPrint:Say(0900,2910,"Valor",ArialN10B)

oPrint:Line(950,0100,950,3300)//--------------------------


OPRINT:LINE(0900,3300,2000,3300 )	        				// HORIZONTAL
oPrint:Line(2000,0100,2000,3300)			  				// VERTICAL

Return

*-------------------------
Static Function ImpItens()
*-------------------------
Local cCusto	:= ""
nLin	:= 950
dbSelectArea("ZA9")            
ZA9->(dbsetOrder(1))//ZA9_FILIAL+ZA9_PREFIX+ZA9_NUM+ZA9_CLIFOR+ZA9_LOJA+ZA9_NATCC 
ZA9->(dbGotop())
IF ZA9->(dbSeek(_xFilial+ZA7->ZA7_PREFIX+ZA7->ZA7_NUM+ZA7->ZA7_FORNEC+ZA7->ZA7_LOJA+"N"))
	While !ZA9->(EOF()) .AND. ZA9->ZA9_FILIAL == _xFilial .and. ZA7->ZA7_PREFIX == ZA9->ZA9_PREFIX .AND. ZA7->ZA7_NUM == ZA9->ZA9_NUM
		IF ZA9->ZA9_NATCC == "N"
			cNatureza	:= ZA9->ZA9_NATURE
			cDescNat	:= SubStr(Posicione("SED",1,xFilial("SED")+ZA9->ZA9_NATURE,"ED_DESCRIC"),1,36)			
						
			oPrint:Say(nLin,0110,Alltrim(ZA9->ZA9_NATURE),ArialN10)			
			oPrint:Say(nLin,0340,Alltrim(cDescNat),ArialN10)

			
			dbSelectArea("ZA9")
			ZA9->(dbsetOrder(3))//ZA9_FILIAL+ZA9_PREFIX+ZA9_NUM+ZA9_NATURE   
			ZA9->(dbGotop())
			IF ZA9->(dbSeek(_xFilial+ZA7->ZA7_PREFIX+ZA7->ZA7_NUM+cNatureza))
				While !ZA9->(eof()) .AND. ZA9->ZA9_FILIAL == _xFilial .AND. ZA7->ZA7_PREFIX == ZA9->ZA9_PREFIX .AND. ZA7->ZA7_NUM == ZA9->ZA9_NUM .AND. ZA9->ZA9_NATURE == cNatureza
					IF ZA9->ZA9_NATCC == "C" 
						IF SUBSTR(ZA9->ZA9_CUSTO,1,1) == "1"
							cConta	:= Posicione("SED",1,xFilial("SED")+cNatureza,"ED_CONTA")
						ELSEIF	SUBSTR(ZA9->ZA9_CUSTO,1,1) == "2"
							cConta	:= Posicione("SED",1,xFilial("SED")+cNatureza,"ED_YCTCUST")
						ELSEIF	SUBSTR(ZA9->ZA9_CUSTO,1,1) == "3"
							cConta	:= Posicione("SED",1,xFilial("SED")+cNatureza,"ED_YDESVEN")			 
						ENDIF
						cDescCta	:= Posicione("CT1",1,xFilial("CT1")+cConta,"CT1_DESC01")			
						oPrint:Say(nLin,1210,SubStr(Alltrim(cConta),1,27),ArialN10)			
						oPrint:Say(nLin,1490,SubStr(Alltrim(cDescCta),1,27),ArialN10)												
						//------------------------------------------- Centro de Custo --------------------------------------------------------
						oPrint:Say(nLin,2160,Alltrim(ZA9->ZA9_CUSTO),ArialN10)     
						oPrint:Say(nLin,2400,SubStr(Alltrim(Posicione("CTT",1,xFilial("CTT")+ZA9->ZA9_CUSTO,"CTT_DESC01")),1,21),ArialN10)
						//------------------------------------------- V A L O R---------------------------------------------------------------												
						oPrint:Say(nLin,3000,Transform(ZA9->ZA9_VALOR,"@E 999,999,999.99"),ArialN10)							                					
						
						nLin	+= 50
					ENDIF
						IF nLin > 1950
							//oPrint:SetRow()
							oPrint:EndPage()
							oPrint:StartPage()
							TECCabec(cNum)
							TECCabec2(cNum)
							TECCabecItem()
							nLin := 950
						ENDIF
				ZA9->(dbSkip())
				ENDDO
				oPrint:Line(nLin,0100,nLin,3300)//-------------------------
				//-- Volta Posicionamento Anterior do While
				dbselectArea("ZA9")
				ZA9->(dbSetOrder(1))
				ZA9->(dbGotop())
				ZA9->(dbSeek(_xFilial+ZA7->ZA7_PREFIX+ZA7->ZA7_NUM+ZA7->ZA7_FORNEC+ZA7->ZA7_LOJA+"N"+cNatureza))
				
		  	ENDIF		
		ENDIF		                             
		//nLin	+= 50		
		
		IF nLin > 1950
			oPrint:EndPage()
			oPrint:StartPage()
			TECCabec(cNum)
			TECCabec2(cNum)
			TECCabecItem()
			nLin := 950
		ENDIF

	ZA9->(dbSkip())                                          
	Enddo
ENDIF

IF ZA7->ZA7_VLABT	> 0
	IF nLin > 1800
		oPrint:EndPage()
		oPrint:StartPage()
		TECCabec(cNum)
		TECCabec2(cNum)
		TECCabecItem()
	ENDIF	
	
	nLin := 1800
	oPrint:Say(nLin,100,"Total : R$",ArialN12B)
	oPrint:Say(nLin,3000,Transform(ZA7->ZA7_VALOR,"@E 999,999,999.99"),ArialN10)
	nLin +=50
	oPrint:Line(nLin,0100,nLin,3300)//-------------------------
	
	//oPrint:Say(nLin,100,"1� Dedu��o",ArialN12B)
	//nLin += 50
	cConta		:= Posicione("SED",1,xFilial("SED")+ZA7->ZA7_NATABT,"ED_CONTA")
	cDescCta	:= Posicione("CT1",1,xFilial("CT1")+cConta,"CT1_DESC01")
	oPrint:Say(nLin,0110,Alltrim(ZA7->ZA7_NATABT),ArialN10)			
	oPrint:Say(nLin,0310,Alltrim(cConta),ArialN10)
	oPrint:Say(nLin,0650,SubStr(Alltrim(cDescCta),1,27),ArialN10)					
	oPrint:Say(nLin,1260,Alltrim(ZA7->ZA7_CCABT),ArialN10)     
	oPrint:Say(nLin,1490,SubStr(Alltrim(Posicione("CTT",1,xFilial("CTT")+ZA7->ZA7_CCABT,"CTT_DESC01")),1,30),ArialN10)
	oPrint:Say(nLin,2160,Alltrim(ZA7->ZA7_ITABT),ArialN10)     
	oPrint:Say(nLin,2400,SubStr(Alltrim(Posicione("CTD",1,xFilial("CTD")+ZA7->ZA7_ITABT,"CTD_DESC01")),1,20),ArialN10)            
	oPrint:Say(nLin,3000,Transform(ZA7->ZA7_VLABT*(-1),"@E 999,999,999.99"),ArialN10)	
	nLin += 50
	oPrint:Line(nLin,0100,nLin,3300)//-------------------------	
	
	//oPrint:Say(nLin,100,"2� Dedu��o",ArialN12B)
	//nLin += 50     
	cConta		:= Posicione("SED",1,xFilial("SED")+ZA7->ZA7_NTABT2,"ED_CONTA")
	cDescCta	:= Posicione("CT1",1,xFilial("CT1")+cConta,"CT1_DESC01")	
	oPrint:Say(nLin,0110,Alltrim(ZA7->ZA7_NTABT2),ArialN10)			
	oPrint:Say(nLin,0310,Alltrim(cConta),ArialN10)
	oPrint:Say(nLin,0650,SubStr(Alltrim(cDescCta),1,27),ArialN10)						
	oPrint:Say(nLin,1260,Alltrim(ZA7->ZA7_CCABT2),ArialN10)     
	oPrint:Say(nLin,1490,SubStr(Alltrim(Posicione("CTT",1,xFilial("CTT")+ZA7->ZA7_CCABT2,"CTT_DESC01")),1,30),ArialN10)
	oPrint:Say(nLin,2160,Alltrim(ZA7->ZA7_ITABT2),ArialN10)     
	oPrint:Say(nLin,2400,SubStr(Alltrim(Posicione("CTD",1,xFilial("CTD")+ZA7->ZA7_ITABT2,"CTD_DESC01")),1,20),ArialN10)            
	oPrint:Say(nLin,3000,Transform(ZA7->ZA7_VLABT2*(-1),"@E 999,999,999.99"),ArialN10)	
	nLin += 50
	oPrint:Line(nLin,0100,nLin,3300)//-------------------------	
	
	//oPrint:Say(nLin,100,"3� Dedu��o",ArialN12B)
	//nLin += 50
	cConta		:= Posicione("SED",1,xFilial("SED")+ZA7->ZA7_NTABT3,"ED_CONTA")
	cDescCta	:= Posicione("CT1",1,xFilial("CT1")+cConta,"CT1_DESC01")	
	oPrint:Say(nLin,0110,Alltrim(ZA7->ZA7_NTABT3),ArialN10)			
	oPrint:Say(nLin,0310,Alltrim(cConta),ArialN10)
	oPrint:Say(nLin,0650,SubStr(Alltrim(cDescCta),1,27),ArialN10)						
	oPrint:Say(nLin,1260,Alltrim(ZA7->ZA7_CCABT3),ArialN10)     
	oPrint:Say(nLin,1490,SubStr(Alltrim(Posicione("CTT",1,xFilial("CTT")+ZA7->ZA7_CCABT3,"CTT_DESC01")),1,30),ArialN10)
	oPrint:Say(nLin,2160,Alltrim(ZA7->ZA7_ITABT3),ArialN10)     
	oPrint:Say(nLin,2400,SubStr(Alltrim(Posicione("CTD",1,xFilial("CTD")+ZA7->ZA7_ITABT3,"CTD_DESC01")),1,20),ArialN10)            
	oPrint:Say(nLin,3000,Transform(ZA7->ZA7_VLABT3*(-1),"@E 999,999,999.99"),ArialN10)		
ENDIF



Return                                                   

                       
///////////////////////////////////////////////////////////
//DETALHES DO EQUIPAMENTO                                  
///////////////////////////////////////////////////////////
Static Function TECDetalhes()


Return

///////////////////////////////////////////////////////////
//RODAPE                                                   
///////////////////////////////////////////////////////////
Static Function TECRodape()        


oPrint:Say(2050,2500,"Liquido : R$",ArialN12B)
oPrint:Say(2050,3000,Transform(ZA7->ZA7_VALOR-ZA7->ZA7_VLABT-ZA7->ZA7_VLABT2-ZA7->ZA7_VLABT3,"@E 999,999,999.99"),ArialN10)

//oPrint:Say(2050,2500,"Total : R$",ArialN12B)
//oPrint:Say(2050,3000,Transform(ZA7->ZA7_VALOR,"@E 999,999,999.99"),ArialN10)

oPrint:Line(2100,0100,2100,3300)

IF ZA7->ZA7_TIPO <> "BL " // Boleto
	oPrint:Say(2100,0100,"Banco",ArialN12B)
	oPrint:Say(2100,0800,"Agencia - DV",ArialN12B)
	oPrint:Say(2100,1600,"Conta Corrente - DV",ArialN12B)
	//oPrint:Say(2150,2500,"Abatimento : R$",ArialN12B)
	//oPrint:Say(2150,3000,Transform(ZA7->ZA7_VLABT,"@E 999,999,999.99"),ArialN10)
	//--------------------------------------------------------------------------------
	oPrint:Say(2150,0100,SA2->A2_BANCO,ArialN10)
	oPrint:Say(2150,0800,SA2->A2_AGENCIA+" - "+SA2->A2_YDIGAG,ArialN10)
	oPrint:Say(2150,1600,SA2->A2_NUMCON+" - "+SA2->A2_YDIGCON,ArialN10)
	//oPrint:Say(3050,1800,"Dedu��es: R$",ArialN12B)
ENDIF
oPrint:Line(2200,0100,2200,3300)       

oPrint:Say(2200,0100,"Emissor:",ArialN10B)
oPrint:Say(2200,0310,UPPER(Alltrim(ZA7->ZA7_USECAD)),ArialN09)
oPrint:Say(2250,0100,"Em: ",ArialN10B)
oPrint:Say(2250,0200,DtoC(ZA7->ZA7_DTCAD)+" as "+ZA7->ZA7_HRCAD,ArialN10)

//oPrint:Say(2200,0800,"Gestor:",ArialN10B)
//oPrint:Say(2200,1020,UPPER(Alltrim(ZA7->ZA7_USEAPR)),ArialN09)
//oPrint:Say(2250,0800,"Em: ",ArialN10B)
//oPrint:Say(2250,0900,DtoC(ZA7->ZA7_DTAPRO)+" as "+ZA7->ZA7_HRAPRO,ArialN10)

oPrint:Say(2200,1600,"Financeiro:",ArialN10B)
oPrint:Say(2200,1870,UPPER(Alltrim(ZA7->ZA7_USEFIN)),ArialN09)
oPrint:Say(2250,1600,"Em: ",ArialN10B)
oPrint:Say(2250,1700,DtoC(ZA7->ZA7_DTFIN)+" as "+ZA7->ZA7_HRFIN,ArialN10)


//oPrint:Say(2200,2400,"Diretor:",ArialN10B)
//oPrint:Say(2200,2720,UPPER(Alltrim(ZA7->ZA7_USELIB)),ArialN09)
//oPrint:Say(2250,2400,"Em: ",ArialN10B)
//oPrint:Say(2250,2500,DtoC(ZA7->ZA7_DTLIB)+" as "+ZA7->ZA7_HRLIB,ArialN10)



oPrint:Line(2300,0100,2300,3300)

Return                     

*--------------------------------------------------------------
User Function RetCtaNat(_cFilial,_cNatCC,_cPrefixo,_cNum,_cNat)
*--------------------------------------------------------------
Local aArea		:= GetArea()
Local cChaveZA9 := _cFilial+_cPrefixo+_cNum
Local nValRatCC	:= 0
Local _cCusto	:= ""


dbSelectArea("ZA9")
ZA9->(dbSetOrder(3)) // ZA9_FILIAL+ZA9_PREFIX+ZA9_NUM+ZA9_NATURE
ZA9->(dbGotop())
IF ZA9->(dbSeek(cChaveZA9))
	WHILE !ZA9->(EOF()) .and. ZA9_FILIAL+ZA9_PREFIX+ZA9_NUM == _cFilial+_cPrefixo+_cNum
		IF ZA9->ZA9_NATURE == _cNat
			IF ZA9->ZA9_NATCC == "C" // s� centro de custo
				IF ZA9->ZA9_VALOR > nValRatCC
					nValRatCC 	:= ZA9->ZA9_VALOR
					_cCusto		:= ZA9->ZA9_CUSTO
				ENDIF	
			ENDIF
	    ENDIF
    ZA9->(dbSkip())    
    ENDDO    
ENDIF
RestArea(aArea)
Return _cCusto