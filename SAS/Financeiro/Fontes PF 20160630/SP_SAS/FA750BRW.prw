
#include "protheus.ch"
// Francisco Valdeni
// Sistema Ari de S� [SAS]
// Cria bot�o na tela (Fun��es do contas a Pagar)
// Impress�o do pedido financeiro

*-----------------------
User Function F050ROT() // Contas a Pagar
*-----------------------
Local aRet		:= ParamIxb
//Local aArea		:= GetArea()
    *------------------------------------------------------------------------------------------------------
  	aAdd(aRet, {'Imp. Pedido Financeiro',"U_IMPPFCta(SE2->E2_FILIAL,SE2->E2_NUM,SE2->E2_PREFIXO)",0,8,,.F.}) 	  
    *------------------------------------------------------------------------------------------------------
//RestArea(aArea)
Return aRet


*-----------------------
User Function FA750BRW() //Fun��es do contas a pagar. 
*-----------------------
Local aRet		:= {} //ParamIxb	
Local aArea		:= GetArea()
    *---------------------------------------------------------------------------------------------------------
	AAdd( aRet, {"Imp. P.F ", "U_IMPPFCta()", 0, 10, 0, Nil} )  
    *---------------------------------------------------------------------------------------------------------
RestArea(aArea)
Return aRet



*----------------------------------------------
User Function IMPPFCta(_cFilial,_cNum,_cPref)
*----------------------------------------------
Local lTlConf := .F. // N�o apresenta tela inicial de impress�o
	IF Alltrim(_cPref) == "PF" // Rotina de Pedido Financeiro.
		*--------------------------------------
		U_SASP066(_cNum,"","",_cFilial,lTlConf)
		*--------------------------------------
	ELSE
		MsgInfo("Op��o dispon�vel apenas para t�tulos gerado atrav�s da rotina [Pedido Financeiro - PF]","T�tulo gerado por outra rotina")
	ENDIF
Return

