#Include 'Protheus.ch'

//Posi��o do cabe�alho da PF
#define PF_NUMERO			01 //N�mero da PF
#define PF_VALOR			02 //Valor da PF
#define PF_FILIAL 			03 //Descri��o da filial origin�ria da PF
#define PF_SOLICITANTE		06 //Descri��o do Solicitante
#define PF_VENCIMENTO		07 //Vencimento da PF
#define PF_PRIORIDADE		08 //Prioridade baseada na data

//Posi��o da lista de Aprovadores
#define APR_COLOR_STATUS	01 //Cor do Status da Aprova��o do aprovador 
#define APR_APROV			02 //Nome do Aprovador
#define APR_DATA			03 //Data 
#define APR_NIVEL			04 //Nivel de aprova��o

//Posi��o da lista de Centro de Custo
#define CC_COD				01 //C�d. Centro de custo
#define CC_DESC				02 //Descri��o Centro de custo
#define CC_VALOR			03 //Valor por Centro de custo

User Function SASP073(aPF, aAprov, aCC, cCorStatus, cStatus, cJustificativa, cNumFilial, cCodUsr, cNivel, cAssunto, cEmail)
	Local cURL := SuperGetMV("SA_LNKWFPF",.F.,"http://10.15.1.68:8082/ws020101/") + "u_SASW010.APL?CHVPF="
	Local aDadosPF := {}
	
	Default aPF := {'000232016', '1.100', 'SAS 3333', 'Chico', '11/22/33', 'URGENTE'}
	
	Default aAprov := 	{;
							{'OK', 'Objetive TI', '11/22/33', '01'},;
							{'RJ', 'Objetive TI', '11/22/33', '01'} ;
						}
	Default aCC := 		{;
							{'0011', 'AABBCC', 300}, ;
							{'0021', 'AABBCC', 400}, ;
							{'0031', 'AABBCC', 500}  ;
						}
	Default cCorStatus := '3'
	Default cStatus := "TESTE"
	Default cJustificativa := ""
	Default cNumFilial := "010101"
	Default cCodUsr := "000005"
	Default cNivel := "01"
	Default cAssunto := "ENVIO DE EMAIL PEDIDO FINANCEIRO"
	Default cEmail := "francisco@objetiveti.com.br"
	
	AADD(aDadosPF,cNumFilial)
	AADD(aDadosPF,aPF[PF_NUMERO])
	AADD(aDadosPF,cCodUsr)
	AADD(aDadosPF,cNivel)
	AADD(aDadosPF,AllTrim(aPF[PF_VALOR]))
	 
	oProcess := TWFProcess():New("PFMAIL", "ENVIO DE EMAIL PEDIDO FINANCEIRO")
	oProcess:NewTask("FORMULARIO", "\workflow\WFPFSAS.html")
	ohtml := oProcess:oHtml
	
	oProcess:ohtml:ValByName("WF_DADOS_PF",		msgPF(aPF))

	oProcess:ohtml:ValByName("WF_DADOS_APROV",	msgAprov(aAprov))
	
	oProcess:ohtml:ValByName("WF_DADOS_CC",		msgCC(aCC))
	
	oProcess:ohtml:ValByName("WF_STATUS",			msgStatus(cCorStatus, cStatus))
	oProcess:ohtml:ValByName("WF_JUSTIFICATIVA",	msgJustif(cJustificativa))
	
	oProcess:ohtml:ValByName("WF_APROVA",		cURL + createAprChv(aDadosPF))
	oProcess:ohtml:ValByName("WF_REPROVA",		cURL + createRepChv(aDadosPF))

	oProcess:ohtml:ValByName("WF_LINK",		createLink(xFilial("ZA7"), aPF[PF_NUMERO]))

	//cEmail := "francisco@objetiveti.com.br"
	oProcess:cTo:= cEmail //USRRETMAIL(aAprovad[i])
	oProcess:cSubject	:= cAssunto
	
	oProcess:bReturn := "U_SASW010()"
	
	oProcess:Start()
Return

Static Function msgPF(aPF)
	Local cRet := ""
	
	cRet += ' <table class="responstable" style="margin: 1em 0;width: 100%;overflow: hidden;background: #FFF;color: #024457;border-radius: 10px;border: 1px solid #167F92;"> '
	cRet += '   <tr style="border: 1px solid #D9E4E6;"> '
	cRet += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Pedido Financeiro</th> '
	cRet += ' 	   <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Valor</th> '
	cRet += ' 	   <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Filial</th> '
	cRet += '   </tr> '
	cRet += '    '
	cRet += '   <tr style="border: 1px solid #D9E4E6;"> '
	cRet += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aPF[PF_NUMERO] +'</td> '
	cRet += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aPF[PF_VALOR] +'</td> '
	cRet += ' 	   <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aPF[PF_FILIAL] +'</td> '
	cRet += '   </tr> '
	cRet += '    '
	cRet += '   <tr style="border: 1px solid #D9E4E6;"> '
	cRet += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Solicitante</th> '
	cRet += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Vencimento</th> '
	cRet += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Prioridade</th> '
	cRet += '   </tr> '
	cRet += '    '
	cRet += '   <tr style="border: 1px solid #D9E4E6;"> '
	cRet += ' 	   <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aPF[PF_SOLICITANTE] +'</td> '
	cRet += ' 	   <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aPF[PF_VENCIMENTO] +'</td> '
	cRet += ' 	   <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aPF[PF_PRIORIDADE] +'</td> '
	cRet += '   </tr> '
	cRet += '    '
	cRet += '    '
	cRet += ' </table> '
Return cRet

Static Function msgAprov(aAprov)
	Local cret := ""
	cRet += ' <table class="responstable" style="margin: 1em 0;width: 100%;overflow: hidden;background: #FFF;color: #024457;border-radius: 10px;border: 1px solid #167F92;"> '
	cRet += '   <tr style="border: 1px solid #D9E4E6;"> '
	cRet += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Status</th> '
	cRet += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Aprovador</th> '
	cRet += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Data da Aprova��o</th> '
	cRet += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">N�vel</th> '
	cRet += '   </tr> '
	cRet += '    '
	For i := 1 to len(aAprov)
		cRet += '   <tr style="border: 1px solid #D9E4E6;"> '
		Do Case
			Case aAprov[i][APR_COLOR_STATUS] == 'OK'
				cCorAprov := "#024457"
				cNomeStatus := "Aprovado"
			Case aAprov[i][APR_COLOR_STATUS] == 'RJ'
				cCorAprov := "#FF0000"
				cNomeStatus := "Rejeitado"
			Otherwise
				cCorAprov := "#DB9600"
				cNomeStatus := "Aguardando Aprova��o"
		EndCase
		
		cRet += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;color:'+cCorAprov+';">'+ cNomeStatus +'</td> '
		cRet += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aAprov[i][APR_APROV] +'</td> '
		cRet += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aAprov[i][APR_DATA] +'</td> '
		cRet += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aAprov[i][APR_NIVEL] +'</td> '
		cRet += '   </tr> '
	Next i
	cRet += '    '
	cRet += '    '
	cRet += '    '
	cRet += ' </table> '
	cRet += '  '
Return cRet

Static Function msgCC(aCC)
	Local cRet := ""
	Local nTotal := 0
	
	cRet += ' <table class="responstable" style="margin: 1em 0;width: 100%;overflow: hidden;background: #FFF;color: #024457;border-radius: 10px;border: 1px solid #167F92;">    <tr style="border: 1px solid #D9E4E6;">       '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">CC</th>            '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Desc</th>       '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Valor por CC</th>     '
	cRet += ' 	</tr>      '
	nTotal := 0
	For i := 1 to len(aCC)
		cRet += ' 	<tr style="border: 1px solid #D9E4E6;">           '
		cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aCC[i][CC_COD]+'</td>       '
		cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aCC[i][CC_DESC]+'</td>       '
		cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+Transform(aCC[i][CC_VALOR], PesqPict("ZA7","ZA7_VALOR"))+'</td>   '
		cRet += ' 	</tr> '
		
		nTotal += aCC[i][CC_VALOR]
	Next i
	cRet += ' 	<tr style="border: 1px solid #D9E4E6;">           '
	cRet += ' 		<th  style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Total � aprovar</td> '
	cRet += ' 		<td colspan="2"  style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+Transform(nTotal, PesqPict("ZA7","ZA7_VALOR"))+'</td> '
	cRet += ' 	</tr> '
	cRet += ' </table>  '
Return cRet

Static Function msgStatus(cCorStatus, cStatus)
	Local cRet := ""
	
	if !empty(cStatus)
		cTpStatus := IIF(SUBSTR(cStatus,1,1)=='A','a','disa')
		
		cRet += '<div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">'
		cRet += '	<center> '
		Do Case
			Case cCorStatus $ '1;2'
				cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;">Status: <span class="sas-status-'+cTpStatus+'pproved" style="color: #DB9600;font-family: Verdana;font-weight: bold;">'+cStatus+'</span></h1> '
			Case cCorStatus == '3'
				cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;">Status: <span class="sas-status-'+cTpStatus+'pproved" style="color: #024457;font-family: Verdana;font-weight: bold;">'+cStatus+'</span></h1> '
			Case cCorStatus $ '4;5;6'
				cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;">Status: <span class="sas-status-'+cTpStatus+'pproved" style="color: #FF0000;font-family: Verdana;font-weight: bold;">'+cStatus+'</span></h1> '
			Otherwise
				cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;">Status: <span class="sas-status-'+cTpStatus+'pproved" style="color: #DB9600;font-family: Verdana;font-weight: bold;">'+cStatus+'</span></h1> '
		EndCase
		cRet += '	</center> '
		cRet += '</div> '
	EndIf
Return cRet

Static Function msgJustif(cJust)
	Local cRet := ""

	if !empty(cJust)
		cRet += '<div id="justification" class="justification" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">'
		cRet += '	<b>Observa��o:</b> '
		cRet += '	<br> '
		cRet += 		cJust
		cRet += '</div> '
	EndIf
return cRet

Static Function createAprChv(aDados)
	Local cRet := ""
	
	For i := 1 to len(aDados)
		cRet += aDados[i] + ';'
	Next i
	
	cRet += 'A'
	
	cRet := embaralha(cRet, 0)
return cRet

Static Function createRepChv(aDados)
	Local cRet := ""
	
	For i := 1 to len(aDados)
		cRet += aDados[i] + ';'
	Next i
	
	cRet += 'R'
	
	cRet := embaralha(cRet, 0)
return cRet

Static Function createLink(_cFil, _cNum)
	Local cLink := SuperGetMV("SA_LNKWFPF",.F.,"http://10.15.1.68:8082/ws020101/") + "u_SASW012.APL?KEYPF="+_cFil+';'+_cNum
return cLink