#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

User Function FA050INC()
	Local aArea 	:= Getarea()
	Local cConCtb	:= ""
	Local cCc	    := ""
	Local cHist	    := ""
	Local cNat		:= ""
	Local cMulNat	:= ""
	Local lRet		:= .T.
	Local aArea 	:= GetArea()
	Local aPergs 	:= {}
	Local aRet		:= {}

	IF (FUNNAME() == "FINA750") //S� faz a valida��o se for uma inclus�o manual
		IF !ApMsgNoYES("Data Base: " + DTOC(dDataBase) + ". Deseja continuar?", "Data Base")
			lRet := .F.
			Return(lRet)
		ENDIF
	ENDIF	
	cConCtb := M->E2_CONTAD
	cCc     := M->E2_CCD
	cHist   := M->E2_HIST
	cNat    := M->E2_NATUREZ
	cMulNat := M->E2_MULTNAT
	cLvd    := M->E2_CLVLDB

	IF ALLTRIM(M->E2_PREFIXO) <> "PF"
		if Empty(cCc) .AND. cMulNat<>"1"
			MsgStop ("Favor informar o Centro de Custo. " , "Aviso")
			lRet := .F.
		Else
			if Empty(cConCtb)
				MsgStop("Favor informar a Conta Contabil no cadastro da Natureza. ", "Aviso")
				lRet := .F.

			Else
				if Empty(cHist)
					MsgStop("Favor informar Historico. ", "Aviso")
					lRet := .F.
				Else 
					//if Empty(cLvd)	
					//	MsgStop("Favor informar a Classe de Valor no cadastro da Natureza. ", "Aviso")
					//	lRet := .F.
					//Endif	
				Endif
			Endif
		Endif

		if substr(cNat,1,4)$ "2145_2152" .and. cMulNat<>"1"
			MsgStop ("Para a Natureza " + substr(cNat,1,4) + " utilizar Multinatureza. " , "Aviso")
			lRet := .F.
		Endif
	ENDIF

	RestArea(aArea)
Return(lRet)
