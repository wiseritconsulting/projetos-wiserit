#Include 'Protheus.ch'
#INCLUDE "topconn.ch"

//Aprova Pedido Financeiro via WorkFlow
User Function SASP074(_cFil, _cNumPF, _cCodUse, _cNivelUsr, _nValor)
	Local nX 		:= 0
	Local cTexto 	:= ""
	Local cQry		:= ""
	Local cEmail	:= ""
	Local _cUsr		:= ""
	Local _cNivel	:= ""
	Local cHtml		:= ""
	Local cEOL 		:= CHR(10)+CHR(13)
	Local aSPSel	:= {}
	Local aAprv		:= {}
	Local aCabec	:= {}
	Local aCC		:= {}
	Local nValor	:= 0
	Local nLimite   := 0
	Local i			:= 0
	Local nCOnt		:= 0
	Local lAprovador:= .F.
	Local cNivelApr	:= ""
	Local cCodApr	:= ""
	Local cDescCC	:= ""
	Local cDescNat	:= ""
	Local cStatus	:= ""
	Local aSp		:= {}
	Local oDlg
	Local oMemo
	local _cNivelZA7	:= ""

	AADD(aSp,{_cFil,_cNumPF})
	dbSelectArea("ZA7")
	ZA7->(dbSetOrder(2)) //ZA7_FILIAL+ZA7_NUM
	ZA7->(dbGotop())
	IF ZA7->(dbSeek(_cFil+_cNumPF))
		dbSelectArea("ZA8")
		ZA8->(dbSetOrder(2)) // Filial + User + Nivel + Num SP
		ZA8->(dbGotop())
		IF	ZA8->(dbSeek(_cFil+_cCodUse+_cNivelUsr+_cNumPF))
			cNivelApr := _cNivelUsr // Guarda Nivel de Aprova��o
			cCodApr := _cCodUse//POSICIONE("SAK",2,xFilial("SAK")+_cCodUse, "SAK->AK_USER")
			
			//--------------Aprova Centro de Custo-------------------------------------------
			//--------------Samarony --------------------------------------------------------
			updCC(_cFil, _cNumPF, _cCodUse, cNivelApr, cCodApr)
			//-------------------------------------------------------------------------------
			
			//--------------Verifica Envio de E-mail-------------------------------------------
			cQry	:= " Select ZA8_FILIAL,ZA8_NUMSP,ZA8_USER,ZA8_NOME, ZA8_NIVEL, ZA8_STATUS "
			cQry	+= " FROM " + RetSqlName("ZA8") + " ZA8 (nolock)"
			cQry	+= " WHERE ZA8.D_E_L_E_T_ <> '*' "
			cQry	+= " AND ZA8.ZA8_FILIAL= '"+_cFil+"'"
			cQry	+= " AND ZA8.ZA8_NUMSP = '"+_cNumPF+"'"
			cQry	+= " AND ZA8.ZA8_STATUS <> 'OK' " // Filtra s� pendentes de aprova��o
			cQry	+= " AND ZA8.ZA8_USER <> '"+_cCodUse+"'	"
			cQry	+= " ORDER BY ZA8_NIVEL, ZA8_NOME "
			IF SELECT("QZA8")>0
				QZA8->(dbCloseArea())
			Endif
			*---------------------------
			TCQUERY cQry NEW ALIAS QZA8
			*---------------------------				
			dbSelectArea("QZA8")
			QZA8->(dbGotop())

			IF !QZA8->(Eof())
				IF QZA8->ZA8_NIVEL <> cNivelApr // Se for o mesmo nivel n�o necessita mandar e-mail
					dbSelectArea("ZA8")
					ZA8->(dbSetOrder(1)) // Filial + User + Nivel + Num SP
					ZA8->(dbGotop())
					IF	ZA8->(dbSeek(QZA8->ZA8_FILIAL+QZA8->ZA8_NUMSP+QZA8->ZA8_NIVEL))
						While !ZA8->(eof()) .And. ZA8->ZA8_NUMSP+ZA8->ZA8_NIVEL == QZA8->ZA8_NUMSP+QZA8->ZA8_NIVEL
							RecLock("ZA8",.F.)
							ZA8->ZA8_STATUS := QZA8->ZA8_NIVEL
							ZA8->(MsUnLock())
							ZA8->(dbSkip())
						ENDDO
					ENDIF
					_cNivelZA7	:= QZA8->ZA8_NIVEL
					While !QZA8->(Eof())
						//IF QZA8->ZA8_NIVEL == "01"
						AADD(aAprv,{QZA8->ZA8_STATUS,QZA8->ZA8_NOME,"",QZA8->ZA8_NIVEL}) // Verificar Status com Samarony
						IF !EMPTY(Alltrim(UsrRetMail(QZA8->ZA8_USER)))
							cEmail += Alltrim(UsrRetMail(QZA8->ZA8_USER))+";"
							_cUsr += Alltrim(QZA8->ZA8_USER)+";"
							_cNivel += Alltrim(QZA8->ZA8_NIVEL)+";"
						ENDIF
						//ENDIF																														
						QZA8->(dbskip())
					ENDDO
					dbSelectArea("ZA7")
					ZA7->(dbSetOrder(2)) // Filial + Numero
					ZA7->(dbgotop())
					ZA7->(dbSeek(_cFil+_cNumPF))
					RECLOCK("ZA7",.F.)
						ZA7->ZA7_NIVEL := _cNivelZA7
					ZA7->(MsUnLock())
						    
				    *--------------------------------------------------
					U_EMailPF(ZA7->ZA7_FILIAL,ZA7->ZA7_NUM,.F.,.T.,.F.)
				    *--------------------------------------------------								
				ENDIF
				QZA8->(dbCloseArea())
			ENDIF
								
			nCOnt++
		ELSE
			MsgInfo("Registro n�o localizado para aprova��o","Tente novamente")
			lRet := .T.
		ENDIF
	ELSE
		MsgInfo("Registro n�o localizado para aprova��o","Tente novamente")
		lRet := .T.
	ENDIF
	
	FOR i:=1 To Len(aSp)
		cQry	:="" // Zera Query Anterior
		cQry	:= " Select ZA8_FILIAL,ZA8_NUMSP,ZA8_USER,ZA8_NOME, ZA8_NIVEL "
		cQry	+= " FROM " + RetSqlName("ZA8") + " ZA8 (nolock)"
		cQry	+= " WHERE ZA8.D_E_L_E_T_ <> '*' "
		cQry	+= " AND ZA8.ZA8_FILIAL= '"+aSP[i][1]+"'" "
		cQry	+= " AND ZA8.ZA8_NUMSP = '"+aSP[i][2]+"'" "
		cQry	+= " AND ZA8.ZA8_STATUS <> 'OK' " // Filtra s� pendentes de aprova��o


		IF SELECT("QZA8")>0
			QZA8->(dbCloseArea())
		Endif
		*---------------------------
		TCQUERY cQry NEW ALIAS QZA8
		*---------------------------				
		dbSelectArea("QZA8")
		QZA8->(dbGotop())

		IF QZA8->(EOF()) // Se for fim de arquivo
			dbSelectArea("ZA7")
			ZA7->(dbSetOrder(2)) //ZA7_FILIAL+ZA7_NUM
			ZA7->(dbGotop())
			IF ZA7->(dbSeek(aSp[i][1]+aSp[i][2]))
				RecLock("ZA7",.F.)
					ZA7->ZA7_STATUS := "2" // Finalizado aprova��o por nivel de centrod e custo (Compras)
				ZA7->(MsUnLock())
				    *--------------------------------------------------
					U_EMailPF(ZA7->ZA7_FILIAL,ZA7->ZA7_NUM,.T.,.F.,.F.)
				    *--------------------------------------------------
			ENDIF
		ENDIF
		QZA8->(dbCloseArea())
	Next i
Return

/*/{Protheus.doc} updCC
Atualiza Centro de Custo de Aprova��o
/*/
static function updCC(cFilCli, cNumSP, cCodUsr, cNivel, cCodAprovador)
	Local cSQL := ""
	Local aArea := getArea()

	cSQL += " SELECT SAL.AL_APROV, SAL.AL_NIVEL, SAL.AL_USER  "
	cSQL += " FROM " + RetSqlName("ZA9") + " ZA9, "
	cSQL += " 	" + RetSqlName("SAL") + " SAL, "
	cSQL += " 	" + RetSqlName("CTT") + " CTT "
	cSQL += " WHERE 1=1 "
	cSQL += " AND ZA9.D_E_L_E_T_ = '' "
	cSQL += " AND CTT.D_E_L_E_T_ = '' "
	cSQL += " AND SAL.D_E_L_E_T_ = '' "
	cSQL += " AND ZA9.ZA9_NATCC = 'C' "
	cSQL += " AND ZA9.ZA9_FILIAL = '"+cFilCli+"' "
	cSQL += " AND ZA9.ZA9_CUSTO = CTT.CTT_CUSTO "
	cSQL += " AND CTT.CTT_YAPRSP = SAL.AL_COD "
	cSQL += " AND ZA9.ZA9_NUM = '"+cNumSP+"' "
	cSQL += " AND SAL.AL_NIVEL = '"+cNivel+"' "
	cSQL += " AND ZA9.ZA9_CUSTO IN ( "
	cSQL += " 	SELECT A.ZA9_CUSTO "
	cSQL += " 	FROM " + RetSqlName("ZA9") + " A, "
	cSQL += " 		" + RetSqlName("SAL") + " B, "
	cSQL += " 		" + RetSqlName("CTT") + " C "
	cSQL += " 	WHERE 1=1 "
	cSQL += " 	AND A.D_E_L_E_T_ = '' "
	cSQL += " 	AND C.D_E_L_E_T_ = '' "
	cSQL += " 	AND B.D_E_L_E_T_ = '' "
	cSQL += " 	AND A.ZA9_NATCC = 'C' "
	cSQL += " 	AND ZA9.ZA9_FILIAL = '"+cFilCli+"' "
	cSQL += " 	AND A.ZA9_CUSTO = C.CTT_CUSTO "
	cSQL += " 	AND C.CTT_YAPRSP = B.AL_COD "
	cSQL += " 	AND A.ZA9_NUM = '"+cNumSP+"' "
	cSQL += " 	AND B.AL_NIVEL = '"+cNivel+"' "
	cSQL += " 	AND B.AL_USER = '"+cCodUsr+"' "
	cSQL += " ) "
	
	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSQL), "QRYCC", .F., .T.)
	
	While QRYCC->(!EOF())
		dbSelectArea("ZA8")
		dbSetOrder(1) 
		dbSeek(xFilial("ZA8")+cNumSP+QRYCC->AL_NIVEL)
		If found()
			While !ZA8->(Eof()) .AND. ZA8->ZA8_NUMSP == cNumSP	
				if ZA8->ZA8_STATUS <> "OK" .and. ZA8->ZA8_USER == QRYCC->AL_USER
					IF ZA8->ZA8_NIVEL == cNivel // Ajuste Nivel Valdeni
						RecLock("ZA8", .F.)
							ZA8->ZA8_DTAPRO	:= DATE()
							ZA8->ZA8_HRAPRO	:= TIME()
							ZA8->ZA8_STATUS := "OK"
							ZA8->ZA8_APRNIV := cCodAprovador
						MsUnLock()			
					ENDIF
				EndIf
				ZA8->(dbSkip())
			Enddo
		EndIf
		ZA8->(dbCLoseArea())
		QRYCC->(dbSkip())
	EndDo
	QRYCC->(dbCloseArea())
	
	RestArea(aArea)
return cSQL