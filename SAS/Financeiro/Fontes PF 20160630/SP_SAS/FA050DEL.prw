#include "protheus.ch"

// Francisco Valdeni 
// Objetive - TI
// 30/06/2016 - [SAS] - Sistema Ari de S�
// Obj. Alterar o status do Pedido Financeiro caso seja exluido
// pela rotina do contas a pagar (FINA050).
*-----------------------
User Function FA050DEL()
*-----------------------
Local aArea	:= GetArea()
Local lRet	:= .T.

IF ALLTRIM(SE2->E2_PREFIXO) == "PF"
	dbSelectArea("ZA7")
	ZA7->(dbSetOrder(1))
	ZA7->(dbGotop())
	IF ZA7->(dbSeek(SE2->E2_FILIAL+SE2->E2_NUM))
		RecLock("ZA7",.F.)
			ZA7->ZA7_STATUS	:= "2" // Volta para a tela de "Aguardando Libera��o Financeira"	
        ZA7->(MsUnLock())
    ENDIF
ENDIF

RestArea(aArea)
Return lRet