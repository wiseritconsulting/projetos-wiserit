#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "COLORS.CH"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"

//----------------Programa para tratamento do rateio por centro de custo---------------------------------------------------------
User Function SPMulCC(nOpc) //MulNatCC(nOpc)
Local aCposDB := {}  // Campos para o arquivo temporario
Local aSaveArea := GetArea()
Local oDlg1
Local cIndice := ""
Local aHead := {}  // Aheader da Getdados nova
Local aColsNew := {} // aCols da Getdados nova 
Local aHeadOld := aClone(aHeader) //Guarda dados o Aheader da Getdados de onde foi chamada
Local aColsOld := aClone(aCols) // Guarda colunas da Getdados anterior
Local nOldPos := n  //Guard posi��o da GetDados de onde foi chamada
Local aAltera := {}  // Campos da Getdb
Local cCampo := "ZA7" // aTit[6] == SE1 ou SE2
Local nRecTmp := 0  // Recno do arquivo temporario
Local nCont := 0
Local lDelFirst := .F., aRegs := {}
Local aStruSez, nX
Local nPosRec :=0 
Local nPosArq:= n
Local lMnatcc := Existblock("MNATCC")
Local lConfirma := .T.
Local lValF050C	:= .T.
Local lF050VCMC	:= ExistBlock("F050VCMC")
Local lPanelFin := If (FindFunction("IsPanelFin"),IsPanelFin(),.F.)
Local oPanel, oPanel2
Local cArqSez	:= ""
Local aButton := {}
Local i			:= 0

Private aTELA		:= {}
Private aGETS		:= {}
Private oValRat
Private nValRat	:= 0		//Valor total rateado por C.Custo
Private nValNat   := aCols[n][2] // Valor da natureza na Getdados de MultiNat
Private cNatur		:= aCols[n][1] // Natureza
Private nOpcC := 0
Private nNumCol := 0	// Numero de colunas do aCols
Private oGetDB
PRIVATE bOk    		:= {||nOpca:=1,oDlg1:End()}
PRIVATE bCancel		:= {||nOpca:=0,oDlg1:End()}        
Private	aOldRatNat	:= {}
Private	j			:= 0

DEFAULT nOpc	:= 3          

IF  nValFal < 0	
    MSGINFO("Valor maior que saldo a distribuir","Verifique diferen�a no roda p� da tela")
    Return .F.
ENDIF



	//If Select("SEZTMP") = 0
		aCposDb := {}
		aadd(aCposDB,{"EZ_NATUREZ","C",10,0})
		aadd(aCposDB,{"EZ_CCUSTO","C",TamSx3("CTT_CUSTO")[1],0})
		aadd(aCposDB,{"EZ_ITEMCTA","C",TamSx3("CTD_ITEM")[1],0})
		aadd(aCposDB,{"EZ_CLVL","C",TamSx3("CTH_CLVL")[1],0})
		aadd(aCposDB,{"EZ_VALOR","N",17,2})
		aadd(aCposDB,{"EZ_PERC","N",11,7})	
		aadd(aCposDB,{"EZ_FLAG","L",1,0})	
		aadd(aCposDB,{"EZ_RECNO","N",6,0})	
		cArqSez := CriaTrab(aCposDB,.T.) // Nome do arquivo temporario do SEZ
		dbUseArea(.T.,__LocalDriver,cArqSez,"SEZTMP",.F.)
	
		cIndice := "EZ_NATUREZ+EZ_CCUSTO"
		dbSelectArea("SEZTMP")
		IndRegua ("SEZTMP",cArqSez,cIndice,,,"Selecionando Registros...") //"Selecionando Registros..."
		#IFNDEF TOP
			dbSetIndex(cArqSez+OrdBagExt())
		#ENDIF
		dbSetOrder(1)
		SEZ->(dbSetOrder(1))	
		SEZ->(MsSeek(xFilial("SEZ")+;
						(cAlias)->&(cCampo + "_PREFIX")+;
						(cAlias)->&(cCampo + "_NUM")+;
						(cAlias)->&(cCampo + "_PARCEL")+;
						(cAlias)->&(cCampo + "_TIPO")+;
						(cAlias)->&(cCampo + "_FORNECE")+;
						(cAlias)->&(cCampo + "_LOJA")))
						
		While 	SEZ->EZ_FILIAL + SEZ->EZ_PREFIXO + SEZ->EZ_NUM +;
			  	SEZ->EZ_PARCELA + SEZ->EZ_TIPO + SEZ->EZ_CLIFOR +;
				SEZ->EZ_LOJA == xFilial("SEZ")+;
						(cAlias)->&(cCampo + "_PREFIX")+;
						(cAlias)->&(cCampo + "_NUM")+;
						(cAlias)->&(cCampo + "_PARCEL")+;
						(cAlias)->&(cCampo + "_TIPO")+;
						(cAlias)->&(cCampo + "_FORNECE")+;
						(cAlias)->&(cCampo + "_LOJA")

			//Descarto rateios que nao sao de inclusao
			If SEZ->EZ_IDENT != "1"
				SEZ->(dbskip())
				Loop
			Endif				

			RecLock("SEZTMP", .T.)
			nPos := Ascan(aCols, { |x| x[1] = SEZ->EZ_NATUREZ })
			SEZTMP->EZ_NATUREZ	:= SEZ->EZ_NATUREZ
			SEZTMP->EZ_CCUSTO	:= SEZ->EZ_CCUSTO
			SEZTMP->EZ_ITEMCTA	:= SEZ->EZ_ITEMCTA
			SEZTMP->EZ_CLVL   	:= SEZ->EZ_CLVL
			SEZTMP->EZ_VALOR	:= Round(NoRound(SEZ->EZ_VALOR,3),2)
			SEZTMP->EZ_PERC		:= SEZ->EZ_PERC
			SEZTMP->EZ_RECNO	:= SEZ->(Recno())
			nTotSez += SEZTMP->EZ_VALOR
			nPerSez += SEZTMP->EZ_PERC
			MsUnLock()
			SEZ->(DbSkip())
		Enddo
	//Endif





// Se campos necessarios nao preenchidos
If Empty(aCols[n][1]) .or. Empty(aCols[n][2]) .or. Empty(aCols[n][3])
   aCols[n][4] := "2"
	Return .T.
Endif


__OPC := nOpc
//Campos a serem mostrados na Getdados
Aadd(aHead,{RetTitle("EZ_CCUSTO"),"EZ_CCUSTO","@!",;
				TamSx3("EZ_CCUSTO")[1],0,"Ctb105Cc()","�","C","SEZ" } )
Aadd(aHead,{RetTitle("EZ_VALOR"),"EZ_VALOR","@E 999,999,999.99",;
				17,2,"MnccCalP()","�","N","SEZ" } )
Aadd(aHead,{RetTitle("EZ_PERC"),"EZ_PERC","",;
				5,2,"MnccCalV()","�","C","SEZ" } )
Aadd(aHead,{RetTitle("EZ_ITEMCTA"),"EZ_ITEMCTA","@!",;
				TamSx3("EZ_ITEMCTA")[1],0,"Ctb105Item()","�","C","SEZ" } )
Aadd(aHead,{RetTitle("EZ_CLVL"),"EZ_CLVL","@!",;
				TamSx3("EZ_CLVL")[1],0,"Ctb105Clvl()","�","C","SEZ" } )
				
//Campos a serem digitados
aadd(aAltera,"EZ_CCUSTO")
aadd(aAltera,"EZ_ITEMCTA")
aadd(aAltera,"EZ_CLVL")
aadd(aAltera,"EZ_VALOR")
aadd(aAltera,"EZ_PERC")					

// Adiciona demais campos
SX3->(DbSetOrder(1))
SX3->(MsSeek("SEZ"))
While ! SX3->(EOF()) .And. (SX3->X3_Arquivo == "SEZ")
	If X3USO(SX3->X3_Usado) .And. cNivel >= SX3->X3_NIVEL .And.;
		Ascan(aHead, {|e| Alltrim(e[2]) == Alltrim(SX3->X3_CAMPO) } ) == 0
		Aadd(aHead,{ AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_ARQUIVO,SX3->X3_CONTEXT } )
		aadd(aAltera,SX3->X3_CAMPO)
	Endif
	SX3->(DbSkip())
End

SX3->(DbSeek("SEZ_FILIAL"))
cUsado := SX3->X3_USADO
AADD( aHead, { "Alias WT","EZ_ALI_WT", "", 09, 0,, cUsado, "C", "SEZ", "V"} )
AADD( aHead, { "Recno WT","EZ_REC_WT", "", 09, 0,, cUsado, "N", "SEZ", "V"} )


aHeader := aHead

//������������������������������������������������������Ŀ
//� Adiciona mais um elemento em aCOLS, indicando se a   �
//� a linha esta ou nao deletada									�
//��������������������������������������������������������
nNumCol := Len(aHeader)
Aadd(aColsNew,Array(nNumCol+1))
For nCont := 1 To nNumCol 
    If !aHeader[nCont,2] $ "EZ_REC_WT|EZ_ALI_WT"
		aColsNew[1,nCont] := CriaVar(aHeader[nCont,2],.T.)
	Else
		If AllTrim(aHeader[nCont,2]) == "EZ_ALI_WT"
			aColsNew[1,nCont] := "SEZ"				
		ElseIf AllTrim(aHeader[nCont,2]) == "EZ_REC_WT"
			aColsNew[1,nCont]:= 0
		EndIf
	EndIf		
	
Next nCntFor
aColsNew [1,(nNumCol+1)] := .F.
aColsNew [1,3] := "0.00"


aCols := aColsNew

//Campos a serem digitados
aadd(aAltera,"EZ_CCUSTO")
aadd(aAltera,"EZ_ITEMCTA")
aadd(aAltera,"EZ_CLVL")
aadd(aAltera,"EZ_VALOR")
aadd(aAltera,"EZ_PERC")	

// Criacao do arquivo temporario
If Select("SEZTMP") == 0
	//�������������������������������Ŀ
	//� Gera arquivo de Trabalho      �
	//���������������������������������
	aadd(aCposDB,{"EZ_NATUREZ","C",10,0})
	aadd(aCposDB,{"EZ_CCUSTO","C",TamSx3("EZ_CCUSTO")[1],0})
	aadd(aCposDB,{"EZ_ITEMCTA","C",TamSx3("EZ_ITEMCTA")[1],0})
	aadd(aCposDB,{"EZ_CLVL","C",TamSx3("EZ_CLVL")[1],0})
	aadd(aCposDB,{"EZ_VALOR","N",17,2})
	aadd(aCposDB,{"EZ_PERC","N",11,7})	
	aadd(aCposDB,{"EZ_FLAG","L",1,0})	
	aadd(aCposDB,{"EZ_RECNO","N",6,0})

	// Adiciona demais campos
	aStruSez := SEZ->(DbStruct())	
	For nX := 1 To Len(aStruSez)
		If Ascan(aCposDB, { |e| Alltrim(e[1]) == aStruSez[nX][1] } ) == 0
			Aadd(aCposDB,{aStruSez[nX][1],aStruSez[nX][2],aStruSez[nX][3],aStruSez[nX][4]})
		Endif
	Next		
	cArqSez := CriaTrab(aCposDB,.T.) // Nome do arquivo temporario do SEZ
	dbUseArea(.T.,__LocalDriver,cArqSez,"SEZTMP",.F.)
Endif
cIndice := "EZ_NATUREZ+EZ_CCUSTO"
dbSelectArea("SEZTMP")
IndRegua ("SEZTMP",cArqSez,cIndice,,,"Selecionando Registros...") //"Selecionando Registros..."
#IFNDEF TOP
	dbSetIndex(cArqSez+OrdBagExt())
#ENDIF
dbSetOrder(1)
dbSeek(cNatur)
// Calcula o valor rateado anteriormente e alimenta a Getdados
nValRat := 0

nRecTmp := Recno()
nPosRec:=Ascan(aHeader, {|e| AllTrim(e[2]) == AllTrim("EZ_REC_WT") } ) 
nPosArq:=Ascan(aHeader, {|e| AllTrim(e[2]) == AllTrim("EZ_ALI_WT") } ) 

If !Eof() .and. !Bof()
	While !Eof() .and. SEZTMP->EZ_NATUREZ == cNatur 
		aCols[Len(aCols)][1] := SEZTMP->EZ_CCUSTO  // Centro de Custo
		aCols[Len(aCols)][2] := SEZTMP->EZ_VALOR   // 
		aCols[Len(aCols)][3] := Transform(SEZTMP->EZ_PERC * 100, Pespict("EZ_PERC"))              
		aCols[Len(aCols)][4] := SEZTMP->EZ_ITEMCTA  // Item
		aCols[Len(aCols)][5] := SEZTMP->EZ_CLVL  // Classe de Valor
		aCols[Len(aCols)][nPosRec] := SEZTMP->(Recno())
		aCols[Len(aCols)][nPosArq] := "SEZ"  

		//Se primeira linha for deletada, Forco o ultimo elemento da acols para .F.
		//e acerto apos a montagem da mesma
		If Len(aCols) == 1 .and. SEZTMP->EZ_FLAG
			aCols[Len(aCols)][nNumCol+1] := .F.  // Controle de delecao
			lDelFirst := .T.
		Else	
			aCols[Len(aCols)][nNumCol+1] := SEZTMP->EZ_FLAG  // Controle de delecao
		Endif

		If !(SEZTMP->EZ_FLAG) .and. !lDelFirst
			nValRat += SEZTMP->EZ_VALOR
		Endif
		Aadd(aRegs, SEZTMP->(Recno()))
		dbSkip()
		If !Eof() .and. SEZTMP->EZ_NATUREZ == cNatur 
			Aadd(aCols,Array(nNumCol+1))
			For nCont := 1 To nNumCol
				 If !aHeader[nCont,2] $ "EZ_REC_WT|EZ_ALI_WT"
					aCols[Len(aCols),nCont] := CriaVar(aHeader[nCont,2],.T.)
				Else
					If AllTrim(aHeader[nCont,2]) == "EZ_ALI_WT"
						aCols[Len(aCols),nCont] := "SEZ"				
					ElseIf AllTrim(aHeader[nCont,2]) == "EZ_REC_WT"
						aCols[Len(aCols),nCont]:= 0
					EndIf
				EndIf		                
			Next nCont
			aCols [Len(aCols),(nNumCol+1)] := .F.
		Endif
	Enddo
	lNewLine := .F.
Endif 

//����������������������������������������������������Ŀ
//� Mostra o corpo da rateio 									 �
//������������������������������������������������������
nOpcC := 0

//����������������������������������������������������Ŀ
//� Ponto de entrada para n�o permitir cancelar a tela |
//������������������������������������������������������
If lMNatCC
	lConfirma := .F.
EndIf


IF Len(aRatNatCC) > 0
	aCols := {}
	For i:=1 To Len(aRatNatCC)	
		IF aRatNatCC[i][9] == cNatur
			AADD(aCols,{aRatNatCC[i][8],aRatNatCC[i][7],aRatNatCC[i][11],aRatNatCC[i][14],/*5*/,/*6*/,/*7*/,/*8*/,/*9*/,/*10*/,/*11*/,/*12*/,0,.F.})
			//                 1             2                 3            4               5     6     7     8     9     10     11     12  13 14
			nValRat	+= aRatNatCC[i][7]
		ENDIF
	Next i
ENDIF

While .T. 

	DEFINE MSDIALOG oDlg1 TITLE "Multipla Natureza por C.Custo (P.F) " From 00,00 To 500,800 OF oMainWnd PIXEL//"Multipla Natureza por C.Custo"

	oPanel := TPanel():New(0,0,'',oDlg1,, .T., .T.,, ,20,20,.T.,.T. )
	oPanel:Align := CONTROL_ALIGN_TOP

	@  002, 005 To  018,120 OF oPanel PIXEL
	@  002, 125 To  018,398 OF oPanel PIXEL


	
		@ 006 , 010		Say aTit[1] + M->&(cCampo + "_FORNEC") FONT oDlg1:oFont OF oPanel  Pixel
		@ 006 , 080		Say aTit[2] + M->&(cCampo + "_LOJA")													FONT oDlg1:oFont OF oPanel  Pixel	
		@ 006 , 130 	Say aTit[3] + M->&(cCampo + "_PREFIX")												FONT oDlg1:oFont OF oPanel  Pixel
		@ 006 , 180		Say aTit[4] + M->&(cCampo + "_NUM")													FONT oDlg1:oFont OF oPanel  Pixel
		@ 006 , 250		Say aTit[5] + M->&(cCampo + "_PARCEL")												FONT oDlg1:oFont OF oPanel  Pixel
		@ 006 , 310 	Say "Natureza " + cNatur															FONT oDlg1:oFont OF oPanel  Pixel//"Natureza "

	oPanel2 := TPanel():New(0,0,'',oDlg1,, .T., .T.,, ,20,20,.T.,.T. )

	@ 002, 005 To  018,200 OF oPanel2 PIXEL
	@ 002, 202 To  018,398 OF oPanel2 PIXEL
	

	@ 006 , 010 Say "Valor a Ratear " OF oPanel2 PIXEL FONT oDlg1:oFont	 //"Valor a Ratear "
	@ 006 , 055 Say nValNat Picture PesqPict("SEZ","EZ_VALOR",14)FONT oDlg1:oFont	 OF oPanel2 PIXEL
	@ 006 , 207 Say "Valor Rateado  " OF oPanel2 PIXEL FONT oDlg1:oFont //"Valor Rateado  "
	@ 006 , 252 Say oValRat VAR nValRat Picture PesqPict("SEZ","EZ_VALOR",14) OF oPanel2 PIXEL


	oGetDB := MSGetDados():New(34,5,128,315,nOpc,"u_SPccLin","AllWaysTrue",,)
	oGetDB:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
	
	// Caso a primeira linha do acols for deletada (anteriormente), restauro 
	// a situa��o de dele��o apos a montagem da Getdados
	If lDelFirst
		aCols[1][nNumCol+1] := .T.
	Endif
    
	oDlg1:refresh()

	ACTIVATE MSDIALOG oDlg1 ON INIT EnchoiceBar(oDlg1,{||IIF(_ValOK(),(oDlg1:End(),nOpcC:=1,oDlg1:refresh()),nOpcC := 1)};
								,{||nOpcC:=2,oDlg1:refresh(),,},,;
								If(nOpc # 2 .And. CtbInUse(),;
			{ {'AUTOM',{|| (.T.) },'Escolha de Rateio Pre-Configurado','Escolha de Rateio Pre-Configurado'} }, {}),oPanel2:Align := CONTROL_ALIGN_BOTTOM ) CENTERED //'Escolha de Rateio Pre-Configurado'
                                                               


	If nOpcC == 1                                                                                                 
	  	
	  	IF Len(aRatNatCC) > 0 
	  		aOldRatNat = aClone(aRatNatCC)
			ASort(aOldRatNat , , , { |x,y| x[9] < y[9] } )	  	   
	  		ASort(aRatNatCC  , , , { |x,y| x[9] < y[9] } )
	  		For j:=1 To Len(aRatNatCC)
	  			IF aRatNatCC[j][9] == cNatur
	  				aOldRatNat[j][9] := "excluir"
	  				//aDel(aOldRatNat,j)
	  			ENDIF
	  		Next j
		  	aRatNatCC := {}//= aClone(aOldRatNat)

	  	For i:=1 To Len(aOldRatNat)
	  	   IF aOldRatNat[i][9] <> "excluir"
	  	   		AADD(aRatNatCC,{aOldRatNat[i][1],aOldRatNat[i][2],aOldRatNat[i][3],aOldRatNat[i][4],aOldRatNat[i][5],aOldRatNat[i][6],aOldRatNat[i][7],aOldRatNat[i][8],aOldRatNat[i][9],aOldRatNat[i][10],aOldRatNat[i][11],aOldRatNat[i][12],aOldRatNat[i][13],aOldRatNat[i][14]})
	  	   ENDIF
	  	Next i
	  	ENDIF
		  	
	  	For i:=1 To Len(aCols)
			AADD(aRatNatCC,{M->ZA7_PREFIX,M->ZA7_NUM,M->ZA7_PARCEL,M->ZA7_FORNEC,M->ZA7_LOJA,M->ZA7_TIPO,aCols[I][2],aCols[I][1],cNatur,"P",aCols[I][3],"1","C",aCols[I][4]})		
		 	//Posicionamento      01            02         03            04            05          06         07          08       09   10        11    12   13       14
		NEXT i
	
		SEZTMP->(dbCloseArea())                                                                                                      
		Exit
	Else
		Loop
	EndIf

EndDo

// Grava a natureza no arquivo temporario
//	MnccGrv(aRegs)
//	dbSelectArea("SEZTMP")
//	dbSetOrder(1)
	aHeader			:= aHeadOld
	aCols			:= aColsOld
	n				:= nOldPos
	m->ev_rateicc 	:= "1"
//SEZTMP->(dbCloseArea())
nOpcC:=3
RestArea(aSaveArea)
Return .T.


User Function SPccLin()
Local lRet		:= .T.
Local _ccPco    := aCols[n][1]
Local _NatPco	:= cNatur
Local _cCtaPco	:= Posicione("SED",1,xFilial("SED")+cNatur,"ED_CONTA")
Local _nValPco	:= aCols[n][2]                  

//----Adapta��o Para Calculo de Or�amento no PCO---
//lRet := u_RtSlPCO(_ccPco,_cCtaPco,_nValPco)/**/
//-------------------------------------------------


Return lRet                                     


Static Function _ValOK()
Local lRet		:= .T.
Local i			:= 0
Local nPosCC	:= Ascan(aHeader, {|e| AllTrim(e[2]) == AllTrim("EZ_CCUSTO") } ) // Posi��o Centro de Custo
Local nPosItc	:= Ascan(aHeader, {|e| AllTrim(e[2]) == AllTrim("EZ_ITEMCTA") } ) // Posi��o Item cont�bil
Local cCtaNat	:= Posicione("SED",1,xFilial("SED")+cNatur,"ED_CONTA")
Local cItObrg	:= Posicione("CT1",1,xFilial("CT1")+cCtaNat,"CT1_ITOBRG")



                      
IF (nValNat - nValRat) <> 0
	MSGiNFO("Diferen�a de  (R$ "+Alltrim(Str(nValNat - nValRat))+") na distribui��o 'Ajuste Valores' !","Distribua o Saldo")
	lRet	:= .F.
ENDIF

For i:=1 To Len(aCols)
    IF !aCols[i][Len(aCols[i])]
    	IF Empty(Alltrim(aCols[i][1]))
    		MsgInfo("Campo c�digo do centro de custo n�o pode ser vazio","Preencha o centro de custo")
    		lRet	:= .F.
        ENDIF
	ENDIF                              
	IF cItObrg == "1"
		IF Empty(Alltrim(aCols[i][nPosItc]))
			MsgInfo("Falta preenchimento do item cont�bil para o centro de custo "+Alltrim(aCols[i][nPosCC]),"IT Cont�bil Obrigat�rio Nat: "+Alltrim(cNatur)+" Cta: "+Alltrim(cCtaNat))
			lRet	:= .F.
		ENDIF	
	ENDIF
Next i
                



Return	lRet