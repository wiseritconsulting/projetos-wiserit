#include 'rwmake.ch'
#include "tbiconn.ch"

#define DATA_FILIAL 	01
#define DATA_NUMPF		02
#define DATA_USER		03
#define DATA_NIVEL		04
#define DATA_VALOR		05
#define DATA_TP_APROV	06

User Function SASW010(__aCookies,__aPostParms,__nProcID,__aProcParms,__cHTTPPage)
	Local cHTML := ''
	Local cResApr := ''
	
	Private cURLW010 := ""
	Private cChavePF := __aProcParms[1,2]

	PREPARE ENVIRONMENT EMPRESA "01" FILIAL "01" MODULO "FIN"
	
	cURLW010 := SuperGetMV("SA_LNKWFPF",.F.,"http://10.15.1.68:8082/ws020101/") + "u_SASW011.APL="
     
	If Len(__aProcParms) = 0
		cHTML += "<p>Nenhum par�metro informado na linha de URL.</p>"
	Else
		nLinha := embaralha(__aProcParms[1,2], 1)
		aDados := StrTokArr(nLinha,";")
		
		dbSelectArea("ZA8")
		dbSetOrder(2)
		dbSeek(aDados[DATA_FILIAL] + aDados[DATA_USER] + aDados[DATA_NIVEL] + aDados[DATA_NUMPF])
		
		if ZA8->ZA8_STATUS == 'OK'
			cHTML += jaAproHTML(ZA8->ZA8_NUMSP,UsrRetName(ZA8->ZA8_APRNIV), DtoC(ZA8->ZA8_DTAPRO) + ' - ' + ZA8->ZA8_HRAPRO, "Aprovado")
		Elseif ZA8->ZA8_STATUS == 'RJ'
			cHTML += jaAproHTML(ZA8->ZA8_NUMSP,UsrRetName(ZA8->ZA8_APRNIV), DtoC(ZA8->ZA8_DTAPRO) + ' - ' + ZA8->ZA8_HRAPRO, "Rejeitado")
		Else
			if aDados[6] == 'R'
				cHTML += reprovaHTML()
			Else
				u_SASP074(	aDados[DATA_FILIAL], ;
							aDados[DATA_NUMPF], ;
							aDados[DATA_USER], ;
							aDados[DATA_NIVEL], ;
							Val(aDados[DATA_VALOR]))
				cHTML += aprovaHTML(aDados[DATA_NUMPF], aDados[DATA_USER])
			EndIF
		EndIf
	EndIf
Return cHTML

Static Function setMotRej()
	Local cRet := ""
	
	dbSelectArea("ZA6")
	dbSetOrder(1)
	dbGoTop()
	While !(ZA6->(EOF()))
		If ZA6->ZA6_MSBLQL <> '1'
			cRet += '    <option value="'+ZA6->ZA6_CODIGO+'">'+ZA6->ZA6_CODIGO+' - '+ZA6->ZA6_MOTIVO+'</option>'
		EndIf
		
		ZA6->(dbSkip())
	EndDo 
	ZA6->(dbCloseArea())
	
	cRet += "<div>"
Return cRet

Static function aprovaHTML(_cNumSP, _cUsr)
	Local cRet := ""
	
	cRet += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
	cRet += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '
	cRet += '  '
	cRet += ' <table class="responstable" style="margin: 1em 0;width: 100%;overflow: hidden;background: #FFF;color: #024457;border-radius: 10px;border: 1px solid #167F92;">    <tr style="border: 1px solid #D9E4E6;">       '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Pedido Financeiro</th>            '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Usuario</th>     '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Data - Hora</th>       '
	cRet += ' 	</tr>      '
	cRet += ' 	<tr style="border: 1px solid #D9E4E6;">           '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+_cNumSP+'</td>       '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+_cUsr+'</td>       '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ DtoC(Date()) +' - '+ Time() +'</td>   '
	cRet += ' 	</tr> '
	cRet += ' </table>  '
	cRet += '     '
	cRet += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
	cRet += ' 	<center>  		 '
	cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;"> '
	cRet += ' 			<span class="sas-status-approved" style="color: #0076AF;font-family: Verdana;font-weight: bold;">Pedido Financeiro Aprovado com Sucesso!</span> '
	cRet += ' 		</h1>  	 '
	cRet += ' 	</center>   '
	cRet += ' </div>  '
	cRet += '     '
	cRet += ' <br>    '
Return cRet

Static function reprovaHTML()
	Local cRet := ""
	Local cLink := cURLW010
	
	cRet += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
	cRet += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '
	cRet += '  '
	cRet += ' <br/><br/> '
	cRet += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
	cRet += ' 	<form action="'+cLink+'" method="GET"> '
	cRet += ' 	<h3>Selecione motivo da rejei&ccedil;&atilde;o: <h3> '
	cRet += ' 		<select name="motRej" style="font-family: Verdana;	width: 100%; max-width: 95%;font-size: 25px;padding:3px;margin: 15px;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;-webkit-box-shadow: 0 3px 0 #0076AF, 0 -1px #0076AF inset;-moz-box-shadow: 0 3px 0 #0076AF, 0 -1px #0076AF inset;box-shadow: 0 3px 0 #0076AF, 0 -1px #0076AF inset;background: #003364;color:#FFF;border:none;outline:none;display: inline-block;-webkit-appearance:none;-moz-appearance:none;appearance:none;cursor:pointer;box-shadow: 0 0 10px 100px ##DB9600 inset;"> '
	cRet += setMotRej()
	cRet += ' 		</select>  '
	cRet += '  '
	cRet += ' 		<br/><br/> '
	cRet += '  '
	//cRet += ' 		<textarea name="OBS" placeholder="Descreva alguma observacao sobre este Pedido Financeiro" style ="width: 100%; max-width: 95%;font-size: 20px;margin: 15px;border-radius:5px;border:1px solid #000000;background-color:#DBDBDB;-webkit-input-placeholder: #FFF;" '
	//cRet += ' 		></textarea> '
	cRet += '  '
	cRet += ' 		<center> '
	cRet += ' 			<input style="border-radius:5px;font-size: 25px;color:#FFF;background-color:#FF0000;" type="submit" value="REJEITAR"> '
	cRet += ' 			<input type="hidden" name="CHVPF" value="'+cChavePF+'"> '
	cRet += ' 		</center> '
	cRet += ' 	</form> '
	cRet += ' </div>     '
	cRet += ' <br>  '
Return cRet

Static Function jaAproHTML(cNumPf, cApro, cData, cTipo)
	Local cRet := ""
	
	cRet += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
	cRet += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '
	cRet += '  '
	cRet += ' <table class="responstable" style="margin: 1em 0;width: 100%;overflow: hidden;background: #FFF;color: #024457;border-radius: 10px;border: 1px solid #167F92;">    <tr style="border: 1px solid #D9E4E6;">       '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Pedido Financeiro</th>            '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Usuario</th>     '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Data - Hora</th>       '
	cRet += ' 	</tr>      '
	cRet += ' 	<tr style="border: 1px solid #D9E4E6;">           '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+cNumPF+'</td>       '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+cApro+'</td>       '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+cData+'</td>   '
	cRet += ' 	</tr> '
	cRet += ' </table>  '
	cRet += '     '
	cRet += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
	cRet += ' 	<center>  		 '
	cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;"> '
	cRet += ' 			<span class="sas-status-approved" style="color: #0076AF;font-family: Verdana;font-weight: bold;">Pedido Financeiro J&aacute; '+cTipo+'!</span> '
	cRet += ' 		</h1>  	 '
	cRet += ' 	</center>   '
	cRet += ' </div>  '
	cRet += '     '
	cRet += ' <br>    '
Return cRet