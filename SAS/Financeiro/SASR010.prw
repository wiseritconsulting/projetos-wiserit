#Include 'Protheus.ch'
#Include 'TopConn.ch'


//_____________________________________________________________________________
/*/{Protheus.doc} OSBQCTR
Relatorio de Inadiplencia

@author Weskley Silva	
@since 19 de Fevereiro de 2015
@version P11
/*/
//_____________________________________________________________________________

User Function SASR010()

Local aRegs			:= {}
Private oReport
Private cPergCont	:= 'SASR010' 
Private _astru		:={}
************************
*Monta pergunte do Log *
************************
AjustaSX1()

If Pergunte( cPergCont, .T. )    
	Processa( { || ProcCont() }, 'Processando....' )
EndIf



Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ProcCont
Carrega os dados conforme parametrizacao feita pelo usuario;
13/03/2015: Altera�~�ao realizada por Heliomar p/ incluir paramentro de data ini e fim e campo raz�o social e status.

@author Weskley Silva	
@since 19 de Fevereiro de 2015
@version P11
/*/
//_____________________________________________________________________________
Static Function ProcCont()
Local cQuery	:= ""
Local aPagto    := {}

BeginSql Alias "TMP"

SELECT E1_FILIAL                   AS 'FILIAL',
       CASE WHEN A1_YBLOQ ='S' 
       THEN 'SIM' ELSE 'N�O' END    AS 'STATUS',
       A1_YDTBLQ                    AS 'DT_BLOQUEIO', 
       E1_CLIENTE                   AS 'COD_CLIENTE',
       E1_NOMCLI                    AS 'CLIENTE',
       A1_NOME                      AS 'RAZAO_SOCIAL',
       E1_TIPO                      AS 'TIPO',
       E1_NUM                       AS 'NUM',
       E1_PREFIXO                   AS 'PREFIXO',
       E1_PARCELA                   AS 'PARCELA',
       E1_VENCREA                   AS 'VENCIMENTO',
       E1_VALOR                     AS 'VALOR',
       E1_HIST                      AS 'HISTORICO',
       E1_SALDO                     AS 'SALDO',
       A1_EST						AS 'ESTADO'
FROM SE1010 
JOIN SA1010 ON E1_CLIENTE = A1_COD
      WHERE SE1010.%NOTDEL% 
      AND E1_SALDO > 0 
      AND E1_VENCREA between  %EXP:MV_PAR01% AND %EXP:MV_PAR02%
      AND E1_CODBAR <> ''
      
EndSql

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()

TMP->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
13/03/2015: Altera�~�ao realizada por Heliomar p/ incluir paramentro de data ini e fim e campo raz�o social e status.
@author Weskley Silva
@since 19 de Fevereiro de 2015
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportDef()
Local oFirst
Local nOrd	:= 1


oReport := TReport():New( 'TMP', 'INADIMPLENCIA', cPergCont, {|oReport| ReportPrint( oReport ), 'INADIMPLENCIA' } )
oReport:SetLandScape()
oReport:lParamReadOnly := .T.

oReport:SetTotalInLine( .F. )

oFirst := TRSection():New( oReport, 'INADIMPLENCIA', { 'TMP', 'SE1' ,'SA1'},,, )

oFirst:SetTotalInLine( .F. )


TRCell():New( oFirst, 'FILIAL'		    ,'TMP', 'FILIAL',				"@!"						    ,06,,							{ || TMP->FILIAL 			} )
TRCell():New( oFirst, 'STATU'		    ,'TMP', 'STATU',				"@!"						    ,03,,							{ || TMP->STATUS			} )
TRCell():New( oFirst, 'Dt. Bloqueio'	,'TMP', 'Dt. Bloqueio',			" "							    ,10,,							{ || STOD(TMP->DT_BLOQUEIO) } )
TRCell():New( oFirst, 'COD_CLIENTE'     ,'TMP', 'COD_CLIENTE',          "@!"							,06,,					        { || TMP->COD_CLIENTE  	    } )
TRCell():New( oFirst, 'CLIENTE'	   	    ,'TMP', 'CLIENTE' ,		     	"@!"		                    ,20,,							{ || TMP->CLIENTE	   		} )
TRCell():New( oFirst, 'RAZ�O SOCIAL'	,'TMP', 'RAZ�O SOCIAL' ,	    "@!"		                    ,30,,							{ || TMP->RAZAO_SOCIAL  	} )
TRCell():New( oFirst, 'TIPO'        	,'TMP', 'TIPO',		     	    "@!"	       					,03,,							{ || TMP->TIPO 	   		    } )
TRCell():New( oFirst, 'NUM'   		    ,'TMP', 'NUM' ,			        "@!"	    					,09,,							{ || TMP->NUM 	 		   	} ) 
TRCell():New( oFirst, 'PREFIXO'		    ,'TMP', 'PREFIXO',	   	        "@!"				            ,03,,							{ || TMP->PREFIXO      	    } )
TRCell():New( oFirst, 'PARCELA'		    ,'TMP', 'PARCELA' ,	   	        "@!"				            ,01,,							{ || TMP->PARCELA       	} )
TRCell():New( oFirst, 'VENCIMENTO'	   	,'TMP', 'VENCIMENTO',			"@!"   	  						,10,,							{ || STOD(TMP->VENCIMENTO)	} )
TRCell():New( oFirst, 'VALOR'		    ,'TMP', 'VALOR',	            "@E 999,999.99"				    ,08,,							{ || TMP->VALOR			    } )
TRCell():New( oFirst, 'HISTORICO'	    ,'TMP', 'HISTORICO',			"@!"	  			            ,80,,							{ || TMP->HISTORICO		    } )
TRCell():New( oFirst, 'SALDO'		    ,'TMP', 'SALDO',				"@E 999,999.99"					,08,,							{ || TMP->SALDO			    } )
TRCell():New( oFirst, 'ESTADO'		    ,'TMP', 'ESTADO',				"@!"					        ,02,,							{ || TMP->ESTADO		    } )


//oBreakT := TRBreak():New( oFirst, { || }, 'Vlr. Total' )

//TRFunction():New( oFirst:Cell( 'ValFat2' ),	'', 'SUM', oBreakT,,,, .F., .F. )

//oFirst:Cell( 'TOTAL' ):SetHeaderAlign( 'RIGHT' )

//oFirst:SetColSpace(9)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;
13/03/2015: Altera�~�ao realizada por Heliomar p/ incluir paramentro de data ini e fim e campo raz�o social e status.
@author Weskley Silva
@since 19 de Fevereiro de 2015
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

MakeAdvplExpr( cPergCont )
oReport:Section(1):Enable()

dbSelectArea( 'TMP')
oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;
13/03/2015: Altera�~�ao realizada por Heliomar p/ incluir paramentro de data ini e fim e campo raz�o social e status.
@author Weskley Silva
@since 19 de Janeiro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function AjustaSX1()
Local aHelp	:= {}
Local aArea	:= GetArea()

SX1->( dbSetOrder(1) )

If !SX1->( dbSeek( cPergCont ) )
	Aadd( aHelp, { { 'Dt. Vcto Incial'	},	{ '' }, { ' ' } } )
    Aadd( aHelp, { { 'Dt. Vcto Final'	},	{ '' }, { ' ' } } )
	
	PutSx1( cPergCont, "01", "Dt. Vcto Incial ?	", "", "", "mv_ch1", "D", 08, 0, 0, "C", "", "", "", "", "MV_PAR01", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[1][1], aHelp[1][2], aHelp[1][3] )
	PutSx1( cPergCont, "02", "Dt. Vcto Final ?	", "", "", "mv_ch2", "D", 08, 0, 0, "C", "", "", "", "", "MV_PAR02", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[2][1], aHelp[2][2], aHelp[2][3] )
EndIf

RestArea( aArea )

Return( Nil)