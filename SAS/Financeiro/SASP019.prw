#Include 'Protheus.ch'

/*/{Protheus.doc} SASP019
Rotina de leitura do arquivo CNAB e baixa do t�tulo a receber
@author Diogo
@since 24/05/2015
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/User Function SASP019()

	Local aPergs:= {}
	Local c_Arquivo	:= Space(300)
	Local _nHa
	Private aRet	:= {}
	Private cBco	:= space(GetSx3Cache("A6_COD","X3_TAMANHO"))
	Private cAgBco:= space(GetSx3Cache("E5_AGENCIA","X3_TAMANHO"))
	Private cCCBco	:= space(GetSx3Cache("E5_CONTA","X3_TAMANHO"))
	Private cFilResp:= "020101" //Filial da baixa do T�tulo

	aAdd(aPergs,{6,"Arquivo de Retorno",c_Arquivo,"","","",60,.F.,"Arquivo RET (*.ret) |*.ret"})
	aAdd(aPergs,{1,"Banco"		,cBco		,GetSx3Cache("A6_COD","X3_PICTURE"),".T.","SA6",".T.",80,.F.})
	aAdd(aPergs,{1,"Ag�ncia"	,cAgBco		,GetSx3Cache("E5_AGENCIA","X3_PICTURE"),".T.","",".T.",80,.F.})
	aAdd(aPergs,{1,"Conta"		,cCCBco		,GetSx3Cache("E5_CONTA","X3_PICTURE"),".T.","",".T.",80,.F.})
	
	If ParamBox(aPergs,"Dados para baixa do T�tulo",@aRet)
		If !(cFilAnt $ cFilResp)
			Aviso("Aten��o","Verifique a filial logada no sistema",{"Ok"})
		Else
			Processa( {|| FProces19(aRet) }, "Aguarde...", "Processando arquivo...",.F.) //Processamento
		Endif
	Endif	

Return

Static Function FProces19(aRet)

	Local cLogerr	:= ""
	Private cConfirm	:= ""
	
		If (_nHa := FT_FUse(AllTrim(aRet[1])))== -1	//Abre Arquivo
			help(" ",1,"NOFILEIMPOR")
			Return
		EndIf

		ProcRegua(FT_FLastRec())
		FT_FGOTOP()
		FT_FSKIP()	//Pula primeira linha
		While !FT_FEOF()
			
			IncProc()
			
			cLinha 	:= FT_FREADLN()
			If substr(cLinha,1,1) = "1"
				cNUMBCO	:= 	substr(cLinha,135,11)+"-"+substr(cLinha,146,1)
				nValPgo	:= val(substr(cLinha,254,11)+"."+substr(cLinha,265,2))
				nJuros	:= val(substr(cLinha,267,11)+"."+substr(cLinha,278,2))
				cDtCred	:= substr(cLinha,296,6)
				cDtCred	:= cTod(substr(cDtCred,1,2)+"/"+substr(cDtCred,3,2)+"/"+substr(cDtCred,5,2)) 
				

				cSetSql:= 	" SELECT E1_NUMBCO,R_E_C_N_O_ RECNO "+;
							" FROM "+RetSqlName("SE1")+" SE1 "+;
							" WHERE SE1.D_E_L_E_T_ =' ' "+;
							" AND E1_NUMBCO LIKE '%"+cNUMBCO+"' "+;
							" AND E1_FILIAL = '"+xFilial("SE1",cFilAnt)+"'
				
				cSetSql := ChangeQuery(cSetSql)

				dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRYP19", .F., .T.)
				
				If QRYP19->(eof())
					cLogerr+= "N�O LOCALIZADO TITULO PARA O IDENTIFICADOR "+cNUMBCO+chr(13)+chr(10)
				Else
					
					dbSelectArea("SE1")
					SE1->(dbGoto(QRYP19->RECNO))
					
					If SE1->E1_SALDO = 0
						cLogerr+= "T�TULO "+SE1->E1_NUM+"/"+SE1->E1_PARCELA+" SE ENCONTRA BAIXADO"+chr(13)+chr(10)
					Else
						FBaixTit19(aRet)
					Endif	
					
				Endif
				
				QRYP19->(dbCloseArea())				
												
			Endif
			FT_FSKIP()
		EndDo
		
		FT_FUSE()
		
		If !empty(cLogerr)
			Aviso("Aten��o",cLogerr,{"ok"})
		Else
			Aviso("Confirma��o","Confirmado a baixa dos seguintes t�tulos :"+chr(13)+chr(10)+cConfirm,{"ok"})
		Endif

Return


/*/{Protheus.doc} FBaixTit19
Baixa dos T�tulos
@author Diogo
@since 24/05/2015
@version 1.0
@param aRet, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function FBaixTit19(aRet)

	Local nValPgo	:= iif(SE1->E1_VALOR >= nValPgo,nValPgo,SE1->E1_VALOR)
	Local aArea		:= getarea()
	Private aBaixa 	:= {}
	dDataBase		:= cDtCred
				
		aAdd(aBaixa, {"E1_PREFIXO"		,SE1->E1_PREFIXO	,Nil})
		aAdd(aBaixa, {"E1_NUM"			,SE1->E1_NUM		,Nil})
		aAdd(aBaixa, {"E1_PARCELA"		,SE1->E1_PARCELA	,Nil})
		aAdd(aBaixa, {"E1_TIPO"			,SE1->E1_TIPO		,Nil})
		aAdd(aBaixa, {"AUTMOTBX"		,"NOR"				,Nil})
		aAdd(aBaixa, {"AUTBANCO"		,aRet[2]			,Nil})
		aAdd(aBaixa, {"AUTAGENCIA"		,aRet[3]			,NIl})
		aAdd(aBaixa, {"AUTCONTA"		,aRet[4]			,NIl})
		aAdd(aBaixa, {"AUTDTBAIXA"		,cDtCred			,Nil})
		aAdd(aBaixa, {"AUTDTCREDITO"	,cDtCred			,Nil})
		aAdd(aBaixa, {"AUTHIST"			,"VALOR RECEBIDO S/ TITULO"	,Nil})
		aAdd(aBaixa, {"AUTJUROS"		,0					,NIL})
		aAdd(aBaixa, {"AUTMULTA"		,nJuros				,NIL})
		aAdd(aBaixa, {"AUTDESCONT"		,0					,NIL})
		aAdd(aBaixa, {"AUTVALREC"		,nValPgo			,NIl})

	Begin Transaction
			         
		lMsErroAuto	:= .F.
			
		MSExecAuto({|x,y| Fina070(x,y)},aBaixa,3)
		
		If lMsErroAuto
			MostraErro()
		Else
			cConfirm+="["+Alltrim(SE1->E1_NUM)+"/"+Alltrim(SE1->E1_PARCELA)+"]"+ chr(13)+chr(10)
		Endif
	End Transaction
	RestArea(aArea)
	
Return