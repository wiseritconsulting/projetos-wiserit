#Include 'Protheus.ch'
#Include 'TopConn.ch'


//_____________________________________________________________________________
/*/{Protheus.doc} OSBQCTR
Relatorio de Composição financeira - SAS 22

@author Carlos Castro	
@since 9 de Fevereiro de 2015
@version P11
/*/
//_____________________________________________________________________________

User Function SASR001()

Local aRegs			:= {}
Private oReport
Private cPergCont	:= 'SASR0001' 
Private _astru		:={}
************************
*Monta pergunte do Log *
************************
AjustaSX1()

If Pergunte( cPergCONT, .T. )    
	Processa( { || ProcCont() }, 'Processando....' )
EndIf



Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ProcCont
Carrega os dados conforme parametrizacao feita pelo usuario;

@author Weskley Silva
@since 15 de Dezembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ProcCont()
Local cQuery	:= ""
Local aPagto    := {}

tcsqlexec("exec nome")

BeginSql Alias "TMP"

SELECT 
	 COD_CLI
	,NOME_CLIENTE
	,VLR_CTR
	,ESTIM
	,FRETE
	,ENVIADOS
	,FRETE_ENVIO
	,VRL_DEVOL
	,DESCONTO
	,RECEBIDO
	,VLR_VENCIDOS
	,A_VENCER
	,FILIAL
 FROM VW_SAS22 
 WHERE  ANO = %EXP:MV_PAR01%
		AND FILIAL = %EXP:MV_PAR02%

EndSql





oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()

TMP->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 15 de Dezembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportDef()
Local oFirst
Local nOrd	:= 1


oReport := TReport():New( 'TMP', 'SAS-22 COMPOSIÇÃO FINANCEIRA', cPergCont, {|oReport| ReportPrint( oReport ), 'SAS-22 COMPOSIÇÃO FINANCEIRA' } )
oReport:SetLandScape()
oReport:lParamReadOnly := .T.

oReport:SetTotalInLine( .F. )

oFirst := TRSection():New( oReport, 'SAS-22 COMPOSIÇÃO FINANCEIRA', { 'TMP', 'SZ2', 'SZ1', 'SE1', 'SZ5', 'SZ1','SE5','SA1' },,, )

oFirst:SetTotalInLine( .F. )


TRCell():New( oFirst, 'COD_CLI'		    ,'TMP', 'COD_CLI',			"@!"						    ,06,,							{ || TMP->COD_CLI 			} )
TRCell():New( oFirst, 'NOME CLIENTE'    ,'TMP', 'NOME CLIENTE',    "@!"							,50,,					        { || TMP->NOME_CLIENTE  	} )
TRCell():New( oFirst, 'VLR_CTR'	   	    ,'TMP', 'VLR_CTR' ,		   	"@E 999,999,999.99"		        ,11,,							{ || TMP->VLR_CTR	   		} )
TRCell():New( oFirst, 'ESTIM'       	,'TMP', 'ESTIM',		     	"@E 999,999,999.99"	       		,11,,							{ || TMP->ESTIM 	   		} )
TRCell():New( oFirst, 'FRETE'   		,'TMP', 'FRETE' ,			    "@E 999,999,999.99"	    		,11,,							{ || TMP->FRETE 	    	} ) 
TRCell():New( oFirst, 'ENVIADOS'		,'TMP', 'ENVIADOS' ,	   	    "@E 999,999,999.99"				,11,,							{ || TMP->ENVIADOS      	} )
TRCell():New( oFirst, 'FRETE_ENVIO'		,'TMP', 'FRETE_ENVIO' ,	"@E 999,999,999.99"				,11,,							{ || TMP->FRETE_ENVIO      	} )
TRCell():New( oFirst, 'VRL_DEVOL'	   	,'TMP', 'VRL_DEVOL',			"@E 999,999,999.99"   	  		,11,,							{ || TMP->VRL_DEVOL	      	} )
TRCell():New( oFirst, 'DESCONTO'		,'TMP', 'DESCONTO' ,	       "@E 999,999,999.99"				,11,,							{ || TMP->DESCONTO			} )
TRCell():New( oFirst, 'RECEBIDO'		,'TMP', 'RECEBIDO' ,	       "@E 999,999,999.99"				,11,,							{ || TMP->RECEBIDO			} )
TRCell():New( oFirst, 'VLR_VENCIDOS'	,'TMP', 'VLR_VENCIDOS',		"@E 999,999,999.99"	  			,11,,							{ || TMP->VLR_VENCIDOS		} )
TRCell():New( oFirst, 'A_VENCER'		,'TMP', 'A_VENCER',			"@E 999,999,999.99"				,11,,							{ || TMP->A_VENCER			} )
TRCell():New( oFirst, 'FILIAL'	    	,'TMP', 'FILIAL',				"@!"							,02,,							{ || TMP->FILIAL			} )


//oBreakT := TRBreak():New( oFirst, { || }, 'Vlr. Total' )

//TRFunction():New( oFirst:Cell( 'ValFat2' ),	'', 'SUM', oBreakT,,,, .F., .F. )

//oFirst:Cell( 'TOTAL' ):SetHeaderAlign( 'RIGHT' )

//oFirst:SetColSpace(9)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author Castro
@since 03 de Fevereiro de 2015
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

MakeAdvplExpr( cPergCont )
oReport:Section(1):Enable()

dbSelectArea( 'TMP' )
oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author CASTRO
@since 28 de Janeiro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function AjustaSX1()
Local aHelp	:= {}
Local aArea	:= GetArea()

SX1->( dbSetOrder(1) )

If !SX1->( dbSeek( cPergCont ) )
	Aadd( aHelp, { { 'ANO DE VIGENCIA DO CONTRATO PESQUISADO.'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'CODIGO DA EMPRESA PESQUISADA.'	},	{ '' }, { ' ' } } )

	PutSx1( cPergCont, "01", "ANO ?		", "", "", "mv_ch1", "S", 04, 0, 0, "C", "", "", "", "", "MV_PAR01", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[1][1], aHelp[1][2], aHelp[1][3] )
	PutSx1( cPergCont, "02", "EMPRESA ?		", "", "", "mv_ch2", "S", 02, 0, 0, "C", "", "", "", "", "MV_PAR02", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[1][1], aHelp[1][2], aHelp[1][3] )

EndIf

RestArea( aArea )

Return( Nil)