#include 'protheus.ch'
#include 'parmtype.ch'

// #########################################################################################
// Projeto: 
// Modulo : SIGAFAT
// Fonte  : LeArq
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Weskley Silva           | Descricao Rotina de baixa automatica a pagar
// ---------+-------------------+-----------------------------------------------------------
// 12/09/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+----------------------------------------------------------


user function SASP127()

Private cArq := cGetFile("Arquivos|*.TXT|","Selecione o diretorio",,,,;
	GETF_LOCALHARD+GETF_LOCALFLOPPY+GETF_NETWORKDRIVE)

	// fOpen - Abertura de arquivo
	nHdl := fOpen(cArq)

	if nHdl == -1 
		MsgAlert("O arquivo: ' "+cArq+" ' nao pode ser aberto! Verifique os parametros do mesmo")
		return
	endif	

	if MsgYesNo("Deseja realizar as baixas ' " + cArq + " '?")
		Processa({|| RunArq()} , "Lendo Arquivo... ")
	endif	

return

// Processa a leitura do arquivo

Static Function RunArq()

	Private lMSErroAuto := .F.

	ProcRegua(0) // Controla a barra de progressao 

	FT_FUSE(cArq)
	FT_FGOTOP()
	nLin := 1
	While !FT_FEOF(cArq)

		IncProc("Lendo linha do arquivo!")
		cLinha := FT_FREADLN(cArq)


		if nLin == 1
			if !cLinha $ "FILIAL;PREFIXO;NUMERO;PARCELA;TIPO;FORNECEDOR;LOJA;SALDO"
				MsgAlert("Layout errado! ", "Verifique o arquivo ")
				FT_FUSE()
				return
			else 
				FT_FSKIP()
				nLin++ 
				Loop
			endif
		endif	
		
		DbSelectArea("SE2")
		SE2->(dbSetOrder(1))//FILIAL+PREFIXO+NUM+PARCELA+TIPO+FORNECE+LOJA
		SE2->(dbGoTop())
		
		aDados := separa(cLinha,";")
		
		If SE2->(dbSeek(PADR(aDados[1],6," ") + PADR(aDados[2],3," ") + aDados[3] + PADR(aDados[4],1," ") + PADR(aDados[5],3," ") + aDados[6] + aDados[7]))
		if SE2->E2_SALDO > 0
		cHistBaixa := "Baixa automatica via rotina"	

		
		aBaixa := {{"E2_FILIAL", SE2->E2_FILIAL,NIL},;
		{"E2_PREFIXO",  PADR(aDados[2],3," "),NIL},;
		{"E2_NUM", aDados[3],NIL},;
		{"E2_PARCELA",   PADR(aDados[4],1," "),NIL},;
		{"E2_TIPO",     PADR(aDados[5],3," "),NIL},;
		{"E2_FORNECE", aDados[6],NIL},;
		{"E2_LOJA", aDados[7],NIL},;
		{"AUTMOTBX", "DAC " ,NIL},;
		{"AUTDTBAIXA",   ddatabase,Nil},;
		{"AUTVLRPG", SE2->E2_SALDO,Nil},;
		{"AUTDTCREDITO",ddatabase,Nil},;
		{"AUTHIST",cHistBaixa,Nil}}
		
		 ACESSAPERG("FIN080", .F.)

		MsExecAuto({|x,y| FINA080(x,y)}, aBaixa, 3)
		if lMsErroAuto 
			MostraErro()
			return .F.
		else 
		//Msginfo("Baixa realizada, verifique as movimenta��es bancarias","SAS")	
		endif
		else
		//Msginfo("O titulo n�o possui saldo a pagar em aberto","SAS")
		endif
		else
		Alert("O titulo a pagar n�o foi localizado " + aDados[3] + "")
		endif
		
		FT_FSKIP()		
	Enddo	
	SE2->(DbCloseArea())
	FT_FUSE() // Fecha o arquivo
	FCLOSE(cArq)
	
return