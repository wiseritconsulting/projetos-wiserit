// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : FA080POS.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor             | Descricao
// -----------+-------------------+---------------------------------------------------------
// 30/08/2016 | rogeriojacome     | Gerado com aux�lio do Assistente de C�digo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} FA080POS
Manuten��o de dados em SRA-Funcionarios.

@author    rogeriojacome
@version   11.3.1.201605301307
@since     30/08/2016
/*/
//------------------------------------------------------------------------------------------
user function FA080POS()
	//-- vari�veis -------------------------------------------------------------------------

	//Indica a permiss�o ou n�o para a opera��o (pode-se utilizar 'ExecBlock')
	Local aArea 	:= GetArea()
	Local aPergs 	:= {}
	Local aRet		:= {}
	
	//IF alltrim(SE2->E2_TIPO) == "PA"
	
	/*
	aAdd( aPergs ,{1,"Data Base: ",dDatabase ,'','.T.',"",'.T.', TamSX3("E2_EMISSAO")[1], .T.})

	If ParamBox(aPergs ,"Parametros ",aRet)
	
	dDataBase := aRet[1]
	
	//ENDIF
	
	ENDIF
	*/
	//-- encerramento ----------------------------------------------------------------------
RestArea( aArea )
return
//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 30/08/2016 as 12:08:32
//-- fim de arquivo--------------------------------------------------------------------------

