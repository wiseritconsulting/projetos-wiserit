// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : F240TDOK.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor             | Descricao
// -----------+-------------------+---------------------------------------------------------
// 02/09/2016 | rogeriojacome     | Gerado com aux�lio do Assistente de C�digo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"
#include "Topconn.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} F240TDOK
Manuten��o de dados em SRA-Funcionarios.

@author    rogeriojacome
@version   11.3.1.201605301307
@since     02/09/2016
/*/
//------------------------------------------------------------------------------------------
user function F240TDOK()
	//-- vari�veis -------------------------------------------------------------------------
	Local aArea 	:= GetArea()
	Local cMsg 		:= ''
	Local cQuery 	:= ''
	Local lRetorno	:= .T.

	IF ApMsgNoYES("Data Base: " + DTOC(dDataBase) + ". Deseja continuar?", "Data Base")

		DbGoTop("SE2TMP")


		While !SE2TMP->(Eof()) 

			IF !empty(SE2TMP->E2_OK)

				cQuery := " SELECT E2_FILIAL, E2_NUM, E2_PARCELA, E2_PREFIXO, E2_FORNECE, E2_LOJA, E2_SALDO FROM "+RetSqlName("SE2")+" "
				cQuery += " WHERE D_E_L_E_T_ = '' "
				cQuery += " AND E2_TIPO = 'PA' "
				cQuery += " AND E2_FILIAL = '"+SUBSTR(cFilAnt,1,2)+"' "
				cQuery += " AND E2_FORNECE = '"+SE2TMP->E2_FORNECE+"' "
				cQuery += " AND E2_SALDO <> 0 "
				if Select("T01") > 0
					T01->(DbCloseArea())
				Endif
				TcQuery cQuery new Alias T01

				IF !T01->(EOF()) .AND. !empty(T01->E2_SALDO)

					cMsg := "Informa��es da PA " + CRLF + ;
					"Filial: " + T01->E2_FILIAL + CRLF + ;
					"Num Titulo: "	+ T01->E2_NUM + CRLF 			+ ;
					"Parcela: "	+ T01->E2_PARCELA + CRLF 			+ ;
					"Prefixo: " + T01->E2_PREFIXO	+ CRLF 	+ ;
					"Fornecedor:  " + T01->E2_FORNECE	+ CRLF 	+ ;
					"Loja: " + T01->E2_LOJA

					IF !ApMsgNoYes(cMsg,"Fonecedor com PA em Aberto. Deseja continuar?")

						lRetorno := .f.
						T01->(DbCloseArea())
						exit 

					endif

				ENDIF
				T01->(DbCloseArea())

			ENDIF

			SE2TMP->(DbSkip())
		EndDo 

	ELSE
		lRetorno := .F.
	ENDIF
	RestArea( aArea )
Return lRetorno
//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 02/09/2016 as 08:31:06
//-- fim de arquivo--------------------------------------------------------------------------

