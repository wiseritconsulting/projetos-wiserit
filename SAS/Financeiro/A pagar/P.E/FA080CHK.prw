// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : FA080CHK.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor             | Descricao
// -----------+-------------------+---------------------------------------------------------
// 02/09/2016 | rogeriojacome     | Gerado com aux�lio do Assistente de C�digo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"
#include "Topconn.ch"
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} FA080CHK
Manuten��o de dados em SRA-Funcionarios.

@author    rogeriojacome
@version   11.3.1.201605301307
@since     02/09/2016
/*/
//------------------------------------------------------------------------------------------
user function FA080CHK()
	//-- vari�veis -------------------------------------------------------------------------

	//Indica a permiss�o ou n�o para a opera��o (pode-se utilizar 'ExecBlock')
	Local aArea 	:= GetArea()
	local lRet := .T.
	Local cMsg 		:= ''
	Local cQuery := ''


	cQuery := " SELECT E2_FILIAL, E2_NUM, E2_PARCELA, E2_PREFIXO, E2_FORNECE, E2_LOJA, E2_SALDO FROM "+RetSqlName("SE2")+" "
	cQuery += " WHERE D_E_L_E_T_ = '' "
	cQuery += " AND E2_TIPO = 'PA' "
	cQuery += " AND E2_FILIAL = '"+SUBSTR(cFilAnt,1,2)+"' "
	cQuery += " AND E2_FORNECE = '"+SE2->E2_FORNECE+"' "
	cQuery += " AND E2_SALDO <> 0 "
	
	if Select("T01") > 0
			T01->(DbCloseArea())
	Endif
		
	TcQuery cQuery new Alias T01

	IF !T01->(EOF()) .AND. !empty(T01->E2_SALDO)

		cMsg := "Informa��es da PA " + CRLF + ;
		"Filial: " + T01->E2_FILIAL + CRLF + ;
		"Num Titulo: "	+ T01->E2_NUM + CRLF 			+ ;
		"Parcela: "	+ T01->E2_PARCELA + CRLF 			+ ;
		"Prefixo: " + T01->E2_PREFIXO	+ CRLF 	+ ;
		"Fornecedor:  " + T01->E2_FORNECE	+ CRLF 	+ ;
		"Loja: " + T01->E2_LOJA

		IF !ApMsgNoYes(cMsg,"Fonecedor com PA em Aberto. Deseja continuar?")

			lRet := .F.
			T01->(DbCloseArea())

		endif

		if Select("T01") > 0
			T01->(DbCloseArea())
		Endif

	ENDIF

RestArea( aArea )

	//-- encerramento ----------------------------------------------------------------------

return lRet
//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 02/09/2016 as 13:56:17
//-- fim de arquivo--------------------------------------------------------------------------

