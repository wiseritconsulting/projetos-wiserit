// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : FA080TIT.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor             | Descricao
// -----------+-------------------+---------------------------------------------------------
// 06/09/2016 | rogeriojacome     | Gerado com aux�lio do Assistente de C�digo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} FA080TIT
Manuten��o de dados em SRA-Funcionarios.

@author    rogeriojacome
@version   11.3.1.201605301307
@since     06/09/2016
/*/
//------------------------------------------------------------------------------------------
user function FA080TIT()
	//-- vari�veis -------------------------------------------------------------------------

	//Indica a permiss�o ou n�o para a opera��o (pode-se utilizar 'ExecBlock')
	Local lRet := .T.
	
	
	IF FUNNAME() == "SASP071"
		Return 
	endif
	
	IF !ApMsgNoYes("Data Base: " + DTOC(dDataBase) + ". Deseja continuar?", "Data Base")
	lRet := .F.
	ENDIF
	//-- encerramento ----------------------------------------------------------------------

return lRet
//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 06/09/2016 as 11:56:32
//-- fim de arquivo--------------------------------------------------------------------------

