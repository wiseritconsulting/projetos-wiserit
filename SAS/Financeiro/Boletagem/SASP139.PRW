#INCLUDE 'TOTVS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH' 

User Function SASP139(lAuto, cFil, cNumBord)
Local aArea			:= GETAREA()
Local lRet 			:= .T.
Local oBordero
Local oConfirm
Local oNumBord
Local cQuery		:= "" 		
Static oDlg
Default lAuto		:= .F.
Default cFil 		:= xFilial("SEA")
Default cNumBord 	:= SPACE(6)

//se n�o for automatico
If !(lAuto)
  DEFINE MSDIALOG oDlg TITLE "Cancelamento de Border�" FROM 000, 000  TO 200, 400 COLORS 0, 16777215 PIXEL
    @ 020, 025 SAY oBordero PROMPT "Num. Border�?" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 030, 025 MSGET oNumBord VAR cNumBord SIZE 087, 011 OF oDlg COLORS 0, 16777215 PIXEL
    @ 050, 025 BUTTON oConfirm PROMPT "Confirmar" SIZE 037, 012 OF oDlg ACTION {|| lRet := .T., oDlg:End()} PIXEL
    @ 050, 070 BUTTON oConfirm PROMPT "Cancelar" SIZE 037, 012 OF oDlg ACTION {|| lRet := .F., oDlg:End()} PIXEL
  ACTIVATE MSDIALOG oDlg CENTERED
  
EndIf

If lRet  
	DBSELECTAREA("SEA")
	DBSETORDER(1)
	If DBSEEK(cFil + cNumBord)
		BEGIN TRANSACTION
			RECLOCK("SEA",.F.)
			SEA->(DBDELETE())
			SEA->(MSUNLOCK())
			
			cQuery := " SELECT E1_FILIAL,E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO FROM " + RETSQLNAME("SE1")
			cQuery += " WHERE D_E_L_E_T_ <> '*' "
			cQuery += " AND E1_NUMBOR = '" + cNumBord + "' "
			TcQuery cQuery new Alias T37
			Count to nReg
			T37->(DBGOTOP())
			
			If nReg == 0
				T37->(DBCLOSEAREA())
				DisarmTransaction()
				lRet := .F.
			EndIf
			
			DBSELECTAREA("SE1")
			DBSETORDER(1)
			
			While T37->(!EOF())
				If DBSEEK(T37->E1_FILIAL + T37->E1_PREFIXO + T37->E1_NUM + T37->E1_PARCELA + T37->E1_TIPO)
					RECLOCK("SE1",.F.)
					SE1->E1_NUMBOR 	:= ""
					SE1->E1_PORTADO := ""
					SE1->E1_AGEDEP 	:= ""
					SE1->E1_NUMBCO 	:= ""
					SE1->E1_DATABOR := STOD("")
					SE1->E1_SITUACA := "0"
					SE1->E1_CONTA	:= ""
					SE1->(MSUNLOCK())
				Else
					T37->(DBCLOSEAREA())
					DisarmTransaction()
					lRet := .F.
				EndIf
				
				T37->(DBSKIP())
			EndDo
		END TRANSACTION
		T37->(DBCLOSEAREA())
	Else
		If !(lAuto)
			ALERT("BORDER� N�O ENCONTRADO")
			lRet := .F.
		EndIf	
	EndIf
EndIf

If lRet .AND. !(lAuto)
	MsgInfo ( "Border� cancelado com sucesso.", "Cancelamento de Border�" )
EndIf

If !(lAuto)
	aMarkedTit := {}
	oMark:Refresh(.T.)
EndIf

RESTAREA(aArea)
Return lRet
