#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#DEFINE FO_READ 0 //Open for reading (default) 
#DEFINE FO_WRITE 1 //Open for writing 
#DEFINE FO_READWRITE 2 //Open for reading or writing 
#DEFINE FO_SHARED 64 //Allow others to read or write
#DEFINE FS_END 2 //Allow others to read or write

 
WSRESTFUL pdf DESCRIPTION "Servi�o REST - PDF T�tulos a Receber"
 
WSDATA count      AS INTEGER
WSDATA startIndex AS INTEGER
WSDATA tipo AS INTEGER
 
WSMETHOD GET DESCRIPTION "Exemplo de retorno de entidade(s)" WSSYNTAX "/titulo/{pasta}/{arquivo}"
 
END WSRESTFUL
 
// O metodo GET nao precisa necessariamente receber parametros de querystring, por exemplo:
// WSMETHOD GET WSSERVICE sample 
WSMETHOD GET WSRECEIVE startIndex, count, tipo WSSERVICE pdf
Local i
Local cFileName := ""
Local cPath		:= ""
Local cFile		:= ""
Local cBuffer	:= ""
Local nTamFile 	:= 0
Local nLast 	:= 0
Local nHdl		:= 0
Local oFile
Local nHandler
Local nSize    	:= 0
Local xBuffer  	:= ''

  If Len(::aURLParms) > 1
  	  
  	  cFileName := ::aURLParms[2] + ".pdf"
  	  cPath		:= "financeiro\workflow\" + ::aURLParms[1] + "\"
  	  
	  cFile := cPath + cFileName
	  conout(cFile)
	  If File( cFile )
	  		nHdl := fOpen( cFile )
	  		nTamFile := fSeek(nHdl,0,2)
	  		fSeek(nHdl,0,0)
	  		cBuffer  := Space(nTamFile)                // Variavel para criacao da linha do registro para leitura
	    	fRead(nHdl,@cBuffer,nTamFile)
	    	::SetContentType( 'application/pdf' )
	    	::SetHeader( 'Content-Disposition', 'attachment; filename= ' + cFileName )
	    	::SetHeader( 'Pragma', 'no-cache' )
	  		::SetResponse( cBuffer )
	  Else
	  		::SetResponse('{')
	  		::SetResponse('"Erro":"Arquivo n�o encontrado"')
	  		::SetResponse('}')
	  EndIf
  Else
  		::SetResponse('{')
  		::SetResponse('"Erro":"Par�metros Inv�lidos"')
  		::SetResponse('}')
  EndIf
  
 
Return .T.
 
