#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
/*
calculo de juros
*/
User Function SASP155( dDataVenc, dNovaData, nSaldo, nQtdDias, nTxJuros)
Local nValJur 		:= 0
Default nQtdDias 	:= 0
Default dNovaData 	:= dDataBase
Default nTxJuros	:= GETMV("MV_TXPER")

If dNovaData >= dDataVenc
	If nQtdDias > 0
		nValJur := Round( ( nSaldo * (nTxJuros/100) * nQtdDias ) ,2)
	Else
		nValJur := Round( ( nSaldo * (nTxJuros/100) * DateDiffDay(dNovaData, dDataVenc) ) ,2)
	EndIf
EndIf


Return nValJur