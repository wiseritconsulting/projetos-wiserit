#include "ap5mail.ch"
#Include "TOTVS.CH"
#Include "TopConn.CH"

User Function SASP141(cDe,cPara,cAssunto,cBody, cAtach, cCopia,cCC)
Local lRet			:= .T.
Local cServer   	:= GetMV("MV_RELSERV")
Local cAccount  	:= GetMV("MV_RELACNT")
Local cPassword 	:= GetMV("MV_RELAPSW")
Local cCC       	:= ""
Default cDe       	:= GetMV("MV_WFMAIL")
Default cAtach		:= ""

CONNECT SMTP SERVER cServer ACCOUNT cAccount PASSWORD cPassword RESULT lOk

lAutent := mailauth(cAccount,cPassword)

If !lAutent
	Get MAIL ERROR cMenErr
	lRet := .F.
EndIf

If !lAutent
	qout("O programa nao conseguiu se autenticar ao servidor de e-mail."+ GetMV("MV_RELSERV"))
Else
	
	If !lOK
		lRet := .F.
		cErro:=MailGetErr()
		qout("Erro ao Conectar o Servidor - " + cErro)
		Return .F.
	EndIf
	/*
	if Empty(AllTrim(cDe))
	_aDe := U_UserEmail()
	_cDe := _aDe[2]
	else
	_cDe := alltrim(cDe)
	endif
	*/
	SEND MAIL from cDe to cPara CC cCC SUBJECT cAssunto BODY cBody ATTACHMENT cAtach RESULT lEnviado
	
	IF !lEnviado
		lRet := .F.
		Get MAIL ERROR cMenErr
		qout("Erro ao Enviar Mensagem - " + cMenErr)
	EndIf
	
EndIf

DISCONNECT SMTP SERVER RESULT lDisconectou

If !lOK
	lRet := .F.
	cErro:=MailGetErr()
	qout("Erro ao Enviar Mensagem - " + cErro)
	lOk := MailSmtpOff( )
	Return .F.
EndIf

lOk := MailSmtpOff()

Return(lRet)

User Function LAYOUTGER(sConteudo)
Local sRodape := "Powered by Mconsult"
Local sBody := ""

	
sBody := '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>SAS</title></head><body style="margin:0px; padding:0px;"><div style="margin:0 auto; width:1024px !important;"><div style="">'
sBody += '<img src="https://s22.postimg.org/umb729tlt/unnamed.png">'
sBody += '</div><div style="background:#E8F3FD; border-bottom:#003364 1px solid; height:20px !important;"></div><div style="padding:10px 0; font-family: Verdana; color:#1F3364;">'
sBody += sConteudo
/*
<div style="text-align:center;"><strong>Prezada Conveniada,</strong></div><div style="padding:20px 20px; text-align: justify; line-height: 160%;"><p>Seguem, anexos, boletos banc�rios referente ao contrato de utiliza��o, fornecimento e comercializa��o de material did�tico firmado entre o Sistema Ari de S� e (NOME DA ESCOLA).</p><p>Qualquer d�vida, estamos � disposi��o.</p>
    <p>Atenciosamente,<br>
    Sistema Ari de S�</p>
</div>
*/
sBody += '</div><div style="background:#E8F3FD; border-top:#003364 1px solid; height:100px !important; text-align:center; padding-top:20px;"><div style="margin:0 auto; width:220px !important;"><div style="float:left;">'
sBody += '<img src="https://s22.postimg.org/m604r95ox/sas.png">'
sBody += '</div><div style="float:right; padding-top:18px;">'
sBody += '<img style="padding:2px 5px;" src="https://s22.postimg.org/muuh3ppgh/image.png">'
sBody += '<img style="padding:2px 5px;" src="https://s17.postimg.org/x6jvxgurz/image.png">'
sBody += '<img style="padding:2px 5px;" src="https://s16.postimg.org/kgq7rkehx/image.png">'
sBody += '<img style="padding:2px 5px;" src="https://s22.postimg.org/xxoqv6is1/you.png">'
sBody += '<br><a href="#" style="color:#1155CC; font-size:12px; ">www.portalsas.com.br</a>'
sBody += '</div></div></div></div></body></html>'
		
Return sBody
