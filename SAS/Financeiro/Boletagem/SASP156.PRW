#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
/*
calculo de multa
*/
User Function SASP156( nSaldo, nTxMulta )
Local nValMul 		:= 0
Default nTxMulta 	:= GETMV("MV_LJMULTA")

nValMul := Round( ( nSaldo * nTxMulta ) /100 ,2)

Return nValMul