#Include 'Protheus.ch'
#Include 'TopConn.ch'

//_____________________________________________________________________________
/* / Relat�rio - [COMPOSI��O DE FINANCEIRA - ESTIMATIVA]

@author [FIT Gest�o & Tecnologia] Francisco Valdeni
@since 23 de Fevereiro de 2015
@version P11
@uso Ari de S�
/ */
//_____________________________________________________________________________

*----------------------
User Function SASR007()
*----------------------
Local aRegs			:= {}
Private oReport
Private cPergCont	:= 'SASR007'
Private _astru		:={}
************************
*Monta pergunte do Log *
************************
AjustaSX1()

If Pergunte( cPergCONT, .T. )    
	Processa( { || ProcCont() }, 'Processando Gera��o do Relat�rio... Aguarde...' )
EndIf



Return( Nil )
*-------------------------
Static Function ProcCont()
*-------------------------
Local cQuery	:= ""
Local cSerie	:= ""
Local oExcel 	:= FWMSEXCEL():New()
Local cDirDocs := MsDocPath()
Local cArquivo := CriaTrab(,.F.) 
Local cPath	   := AllTrim(GetTempPath())
Local nPos		:= 0 
Local cTab1		:= ""
Local cTab2		:= ""
Local nQtdDiv	:= 0
Local _astru1	:={}
Local _astru2	:={}
Local aVol1		:={}
Local aVol2		:={}
Local aVol3		:={}
Local aVol4		:={}
Local aTab		:= {}
Local _carq1
Local _carq2



//� Estrutura da tabela temporaria
AADD(_astru1,{"Serie"    	,"C",10,0})
AADD(_astru1,{"Produto"	 	,"C",30,0})
AADD(_astru1,{"V1"		 	,"N",10,2})
AADD(_astru1,{"V2"		  	,"N",10,2})
AADD(_astru1,{"V3"			,"N",10,2})
AADD(_astru1,{"V4"			,"N",10,2})
AADD(_astru1,{"V5"			,"N",10,2})
AADD(_astru1,{"V6"			,"N",10,2})
AADD(_astru1,{"Vrl_Kit"		,"N",15,2})
AADD(_astru1,{"Vrl_Volume"	,"N",15,2})

// cria a tabela tempor�ria
_carq1:="T_"+Criatrab(,.F.)
MsCreate(_carq1,_astru1,"DBFCDX") 

//�����������������������������������������������������������Ŀ
//�atribui a tabela tempor�ria ao alias TRB
//�������������������������������������������������������������
dbUseArea(.T.,"DBFCDX",_cARq1,"TRB1",.T.,.F.)

//� Estrutura da tabela temporaria
AADD(_astru2,{"Serie"    	,"C",10,0})
AADD(_astru2,{"Produto"	 	,"C",30,0})
AADD(_astru2,{"VL1"		 	,"N",10,2})
AADD(_astru2,{"VL2"		  	,"N",10,2})
AADD(_astru2,{"VL3"			,"N",10,2})
AADD(_astru2,{"VL4"			,"N",10,2})
AADD(_astru2,{"VL5"			,"N",10,2})
AADD(_astru2,{"VL6"			,"N",10,2})
AADD(_astru2,{"VL_TOTAL"	,"N",15,2})

// cria a tabela tempor�ria
_carq2:="T_"+Criatrab(,.F.)
MsCreate(_carq2,_astru2,"DBFCDX") 


//�����������������������������������������������������������Ŀ
//�atribui a tabela tempor�ria ao alias TRB
//�������������������������������������������������������������
dbUseArea(.T.,"DBFCDX",_cARq2,"TRB2",.T.,.F.)

//LEFT JOIN SZ3010  SZ3 ON SZ3.Z3_FILIAL = SZ2.Z2_FILIAL AND SZ3.Z3_CONTRAT = SZ2.Z2_CONTRAT AND SZ3.Z3_PRODUTO = SZ2.Z2_PRODUTO AND SZ3.D_E_L_E_T_ = ' '

BeginSql Alias "TMP1"
			SELECT Z1_FILIAL Filial, SUBSTRING(B1_DESC,1,17) Descricao, B1_YSERIE Serie, B1_YVOL Volume,SUM(Z2_QUANT) QtdCont, SUM(Z2_QTDADT) Aditivo, ISNULL(SUM(Z2_QTDMED),'')  Quantidade, ISNULL((SUM(Z2_QUANT)*SUM(Z2_VLUNIT)-SUM(Z2_DESCONT))/SUM(Z2_QUANT),'') ValorUnit,	Z2_TES 
			FROM SZ2010 SZ2		
			INNER JOIN SB1010 SB1 ON SZ2.Z2_PRODUTO = SB1.B1_COD AND SB1.D_E_L_E_T_ = ' '
			INNER JOIN SZ1010 SZ1 ON SZ1.Z1_FILIAL = SZ2.Z2_FILIAL AND SZ1.Z1_CONTRAT = SZ2.Z2_CONTRAT AND SZ1.D_E_L_E_T_ =  ' '
			INNER JOIN SA1010 SA1 ON SZ1.Z1_CLIENTE = SA1.A1_COD AND SZ1.Z1_LOJA = SA1.A1_LOJA
			WHERE SZ2.D_E_L_E_T_ = ' '
			AND SZ1.Z1_CLIENTE 	= %Exp:mv_par01%
			AND SZ1.Z1_ANOCOP 	= %Exp:mv_par02%	
			AND SZ2.Z2_MSFIL 	= %Exp:mv_par03%
			AND SZ2.Z2_TES = '502'	
			GROUP BY Z1_FILIAL, SUBSTRING(B1_DESC,1,17),B1_YVOL, B1_YSERIE, Z2_TES
			ORDER BY B1_YVOL, B1_YSERIE
EndSql

IF !TMP1->(EOF())
	cSerie	:= TABELA("Z1",TMP1->Serie,.F.)
ENDIF

AADD(aTab,{cSerie,TMP1->Descricao,0,0,0,0,0,0,0,0,TMP1->Serie})
//          1      2 3 4 5 6 7 8 9 10

//3 = VOLUME 1|2
//4 = VOLUME 3|4
//5 = VOLUME 5
//6 = VOLUME 6
dbSelectArea("TRB1")

While !TMP1->(EOF())
cSerie	:= Alltrim(TABELA("Z1",TMP1->Serie,.F.))

IF TMP1->Serie $ "01|05" //01|05 = ENV 01 E 02
	IF TMP1->VOLUME == '01'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF nPos <> 0		
	    	aTab[nPos][3]	+= 	TMP1->Quantidade
	    	aTab[nPos][9]	+= 	TMP1->ValorUnit
	    ELSE
	    	AADD(atab,{cSerie,TMP1->Descricao,TMP1->Quantidade,0,0,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})	    	
	    ENDIF		
	ELSEIF TMP1->VOLUME == '02'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][4]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,TMP1->Quantidade,0,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF 
		ELSE
			aTab[nPos][4]	:= TMP1->QtdCont+TMP1->Aditivo
			aTab[nPos][9]	+= 	TMP1->ValorUnit
		ENDIF					
	ENDIF
ELSEIF TMP1->Serie $ "02|03|04|08|09"  //02|03|04|08|09 = ENV 01, 02 E 03
	IF TMP1->VOLUME == '01'			
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF nPos <> 0		
	    	aTab[nPos][3]	+= 	TMP1->Quantidade
	    	aTab[nPos][9]	+= 	TMP1->ValorUnit
	    ELSE
	    	AADD(atab,{cSerie,TMP1->Descricao,TMP1->Quantidade,0,0,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
	    ENDIF		
	ELSEIF TMP1->VOLUME == '02'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][4]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,TMP1->Quantidade,0,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF
		ELSE
			aTab[nPos][4]	:= TMP1->QtdCont+TMP1->Aditivo
			aTab[nPos][9]	+= 	TMP1->ValorUnit
		ENDIF		  			
	ELSEIF TMP1->VOLUME == '03'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][5]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,0,TMP1->Quantidade,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF 
		ELSE
		 	aTab[nPos][5]	:= TMP1->QtdCont+TMP1->Aditivo
		 	aTab[nPos][9]	+= 	TMP1->ValorUnit
		ENDIF		    				    
	ENDIF
ELSEIF TMP1->Serie $ "06|07" //06|07 = ENV V(1_2) + 3
	IF TMP1->VOLUME = '01'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 		
		IF nPos <> 0		
	    	aTab[nPos][3]	+= 	TMP1->Quantidade 
	    	aTab[nPos][9]	+= 	TMP1->ValorUnit
	    ELSE
	    	AADD(atab,{cSerie,TMP1->Descricao,TMP1->Quantidade,0,0,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
	    ENDIF		
	ELSEIF TMP1->VOLUME == '02'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][4]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,TMP1->Quantidade,0,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF
		ELSE
			aTab[nPos][4]	:= TMP1->QtdCont+TMP1->Aditivo
			aTab[nPos][5]	:= TMP1->QtdCont+TMP1->Aditivo
			aTab[nPos][9]	+= 	TMP1->ValorUnit
		ENDIF		    			
	ELSEIF TMP1->VOLUME == '03'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][5]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,0,TMP1->Quantidade,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF
		ELSE
			aTab[nPos][5]	:= TMP1->QtdCont+TMP1->Aditivo
			aTab[nPos][9]	+= 	TMP1->ValorUnit
		ENDIF		    			
	ENDIF
ELSEIF TMP1->Serie $ "10|11|12|13|18" //10|11|12|13|18 = ENV 01,02,03 E 04	
	IF TMP1->VOLUME == '01'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF nPos <> 0		
	    	aTab[nPos][3]	+= 	TMP1->Quantidade 
	    	aTab[nPos][9]	+= 	TMP1->ValorUnit
	    ELSE
	    	AADD(atab,{cSerie,TMP1->Descricao,TMP1->Quantidade,0,0,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
	    ENDIF			    
	ELSEIF TMP1->VOLUME == '02'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0			
			IF nPos <> 0		
		    	aTab[nPos][4]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,TMP1->Quantidade,0,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF
		ELSE
	   		aTab[nPos][4]	+= TMP1->QtdCont+TMP1->Aditivo
	   		aTab[nPos][9]	+= 	TMP1->ValorUnit
	   	ENDIF	 			
	ELSEIF TMP1->VOLUME == '03'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][5]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,0,TMP1->Quantidade,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF 
		ELSE
	   		aTab[nPos][5]	+= TMP1->QtdCont+TMP1->Aditivo
	   		aTab[nPos][9]	+= 	TMP1->ValorUnit
	   	ENDIF	 					    				    	
	ELSEIF TMP1->VOLUME == '04'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][6]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,0,0,TMP1->Quantidade,0,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF				    
		ELSE
	   		aTab[nPos][6]	+= TMP1->QtdCont+TMP1->Aditivo
	   		aTab[nPos][9]	+= 	TMP1->ValorUnit
	   	ENDIF
	ENDIF	 					    				    			                                     
ELSEIF TMP1->Serie $ "14|15|17" // 14|15|17 = ENV V(1_2) + (3_4) + 05 E 06
	IF TMP1->VOLUME == '01'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF nPos <> 0		
	    	aTab[nPos][3]	+= 	TMP1->Quantidade 
	    	aTab[nPos][9]	+= 	TMP1->ValorUnit
	    ELSE
	    	AADD(atab,{cSerie,TMP1->Descricao,TMP1->Quantidade,0,0,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
	    ENDIF		
	ELSEIF TMP1->VOLUME == '02'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][4]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,TMP1->Quantidade,0,0,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF		            
		ELSE
			aTab[nPos][4]	+= TMP1->QtdCont+TMP1->Aditivo
			aTab[nPos][9]	+= 	TMP1->ValorUnit
		ENDIF		    					    
	ELSEIF TMP1->VOLUME $ '03'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][5]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,0,TMP1->Quantidade,0,0,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF
		ELSE
			aTab[nPos][5]	+= TMP1->QtdCont+TMP1->Aditivo			
			aTab[nPos][9]	+= 	TMP1->ValorUnit
		ENDIF		    			
	ELSEIF TMP1->VOLUME == '04'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][6]	+= 	TMP1->Quantidade
		    	aTab[nPos][7]	+= 	TMP1->Quantidade
		    	aTab[nPos][8]	+= 	TMP1->Quantidade 
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit		    	
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,0,0,TMP1->Quantidade,TMP1->Quantidade,TMP1->Quantidade,(3*TMP1->ValorUnit),0,TMP1->Serie})
		    ENDIF
		ELSE			
			aTab[nPos][6]	+= TMP1->QtdCont+TMP1->Aditivo
			aTab[nPos][9]	+= 	TMP1->ValorUnit
			IF "PRE" $ cSerie 
				aTab[nPos][7]	+= TMP1->QtdCont+TMP1->Aditivo
				aTab[nPos][8]	+= TMP1->QtdCont+TMP1->Aditivo
				//aTab[nPos][9]	+= TMP1->ValorUnit
				//aTab[nPos][9]	+= 	TMP1->ValorUnit
			ENDIF
		ENDIF		    				    	
	ELSEIF TMP1->VOLUME == '05'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][7]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,0,0,0,TMP1->Quantidade,0,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF
		ELSE
			aTab[nPos][7]	+= TMP1->QtdCont+TMP1->Aditivo
			aTab[nPos][9]	+= 	TMP1->ValorUnit
		ENDIF		    
	ELSEIF TMP1->VOLUME == '06'	
		nPos	:= aScan( aTab, {|x| x[1] == cSerie} ) 
		IF TMP1->Quantidade > 0				
			IF nPos <> 0		
		    	aTab[nPos][8]	+= 	TMP1->Quantidade
		    	aTab[nPos][9]	+= 	TMP1->ValorUnit
		    ELSE
		    	AADD(atab,{cSerie,TMP1->Descricao,0,0,0,0,0,TMP1->Quantidade,TMP1->ValorUnit,0,TMP1->Serie})
		    ENDIF
		ELSE
			aTab[nPos][8]	+= TMP1->QtdCont+TMP1->Aditivo
			aTab[nPos][9]	+= 	TMP1->ValorUnit
		ENDIF		    		
	ENDIF	 				    
ENDIF
TMP1->(dbSkip())
ENDDO





For i:=1 To Len(aTab)

IF aTab[i][11] $ "01|05"
	nQtdDiv	:= 2
ELSEIF	aTab[i][11] $ "02|03|04|08|09"
	nQtdDiv	:= 3
ELSEIF	aTab[i][11] $ "06|07"
	nQtdDiv	:= 3
ELSEIF	aTab[i][11] $ "10|11|12|13|18"
	nQtdDiv	:= 4
ELSEIF	aTab[i][11] $ "14|15|"
	nQtdDiv	:= 4
ELSEIF	aTab[i][11] $ "17"
	nQtdDiv	:= 6	 
ELSE 
	nQtdDiv	:= 0	
ENDIF	
		 
	RecLock("TRB1",.T.)
		TRB1->Serie		:= aTab[i][1]
		TRB1->Produto   := aTab[i][2]
		TRB1->V1		:= aTab[i][3]
		TRB1->V2		:= aTab[i][4]
		TRB1->V3		:= aTab[i][5]
		TRB1->V4		:= aTab[i][6]		
		TRB1->V5		:= aTab[i][7]
		TRB1->V6		:= aTab[i][8]
		TRB1->Vrl_Kit	:= aTab[i][9]
		TRB1->Vrl_VOLUME:= aTab[i][9]/nQtdDiv
	TRB1->(MsUnLock())

nVlVol := (aTab[i][9]/nQtdDiv)

	RecLOck("TRB2",.T.)
		TRB2->Serie 	:= aTab[i][1]
		TRB2->Produto   := aTab[i][2]
		TRB2->VL1		:= aTab[i][3] * nVlVol
		TRB2->VL2		:= aTab[i][4] * nVlVol
		TRB2->VL3		:= aTab[i][5] * nVlVol
		TRB2->VL4		:= aTab[i][6] * nVlVol
		TRB2->VL5		:= aTab[i][7] * nVlVol
		TRB2->VL6		:= aTab[i][8] * nVlVol
		TRB2->VL_TOTAL	:= (aTab[i][3] * nVlVol)+(aTab[i][4] * nVlVol)+(aTab[i][5] * nVlVol)+(aTab[i][6] * nVlVol)+(aTab[i][7] * nVlVol)+(aTab[i][8] * nVlVol)
    TRB2->(MsUnLock())
NExt i


cNomeCli	:= Alltrim(Posicione("SA1",1,xFilial("SA1")+MV_PAR01,"A1_NOME"))
cTab1		:= cNomeCli+" - ESTIMATIVA - QUANTIDADE"
cTab2		:= cNomeCli+" - ESTIMATIVA - VALOR"

    oExcel:AddworkSheet("Quantidade")
    oExcel:AddTable ("Quantidade",cTab1)
    oExcel:AddColumn("Quantidade",cTab1,"S�rie"		,1,1,.F.)
    oExcel:AddColumn("Quantidade",cTab1,"Item"			,1,1,.F.)
    oExcel:AddColumn("Quantidade",cTab1,"V1"			,3,2,.F.)
    oExcel:AddColumn("Quantidade",cTab1,"V2"			,3,2,.F.)
    oExcel:AddColumn("Quantidade",cTab1,"V3"			,3,2,.F.)
    oExcel:AddColumn("Quantidade",cTab1,"V4"			,3,2,.F.)
    oExcel:AddColumn("Quantidade",cTab1,"V5"			,3,2,.F.)
    oExcel:AddColumn("Quantidade",cTab1,"V6"			,3,2,.F.)
    oExcel:AddColumn("Quantidade",cTab1,"Vlr. Kit"		,3,3,.F.)
    oExcel:AddColumn("Quantidade",cTab1,"Vlr. Volume"	,3,3,.F.)        

    dbSelectArea("TRB1")
    TRB1->(dbGotop())    
    While !TRB1->(Eof())
    	oExcel:AddRow("Quantidade",cTab1,{TRB1->Serie,TRB1->Produto,TRB1->V1,TRB1->V2,TRB1->V3,TRB1->V4,TRB1->V5,TRB1->V6,TRB1->Vrl_Kit,TRB1->Vrl_VOLUME})
    TRB1->(dbSkip())
    ENDDO
        
    oExcel:AddworkSheet("Valor")
    oExcel:AddTable("Valor",cTab2)
    oExcel:AddColumn("Valor",cTab2,"S�rie"	,1,1,.F.)
    oExcel:AddColumn("Valor",cTab2,"Item"		,1,1,.F.)
    oExcel:AddColumn("Valor",cTab2,"V1"		,3,3,.T.)
    oExcel:AddColumn("Valor",cTab2,"V2"		,3,3,.T.)
    oExcel:AddColumn("Valor",cTab2,"V3"		,3,3,.T.)
    oExcel:AddColumn("Valor",cTab2,"V4"		,3,3,.T.)
    oExcel:AddColumn("Valor",cTab2,"V5"		,3,3,.T.)
    oExcel:AddColumn("Valor",cTab2,"V6"		,3,3,.T.)
    oExcel:AddColumn("Valor",cTab2,"Total"	,3,3,.T.)
                                                                                            	
    dbSelectArea("TRB2")
    TRB2->(dbGotop())    
    While !TRB2->(Eof())
    	oExcel:AddRow("Valor",cTab2,{TRB2->Serie,TRB2->Produto,TRB2->VL1,TRB2->VL2,TRB2->VL3,TRB2->VL4,TRB2->VL5,TRB2->VL6,TRB2->VL_TOTAL})
    TRB2->(dbSkip())
    ENDDO
    

    oExcel:Activate()
    oExcel:GetXMLFile(cPath+cArquivo)

	CpyS2T(cDirDocs + "\" + cArquivo + ".xml", cPath, .T.)
	
	If !ApOleClient('MsExcel') 
		MsgAlert("MsExcel nao instalado") 
		Return
	EndIf
	
	oExcelApp := MsExcel():New()
	oExcelApp:WorkBooks:Open(cPath + cArquivo) //Abre uma planilha
	oExcelApp:SetVisible(.T.)


TRB1->(dbCloseArea())
	TRB2->(dbCloseArea())
TMP1->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 12 de Novembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportDef()
Local oFirst1
Local oFirst2
Local nOrd	:= 1


oReport := TReport():New( 'TRB1', Posicione("SA1",1,xFilial("SA1")+MV_PAR01,"A1_NOME"), cPergCont , {|oReport| ReportPrint( oReport ), 'COMPOSI��O DE FINANCEIRA - ESTIMATIVA' } )

//oReport:SetLandScape()       
oReport:SetPortrait()    
oReport:SetTotalInLine(.F.)
oReport:lParamReadOnly := .F.


oFirst1 := TRSection():New( oReport,"Quantidades" , { 'TRB1' },,.f.,	.t. )



TRCell():New( oFirst1, 'Serie'			,'TRB1', 'Serie'		,"@!"   				,10,,		{ || TRB1->Serie 			} )
TRCell():New( oFirst1, 'Itens'  		,'TRB1', 'Itens' 		,"@!"					,30,,		{ || TRB1->Produto 			} )
TRCell():New( oFirst1, 'V1'     		,'TRB1', 'Volume 1'		,"@E 9999999999"		,10,,		{ || TRB1->V1				} )
TRCell():New( oFirst1, 'V2'   			,'TRB1', 'Volume 2'		,"@E 9999999999"  		,10,,		{ || TRB1->V2				} )
TRCell():New( oFirst1, 'V3'	   			,'TRB1', 'Volume 3'		,"@E 9999999999"		,10,,		{ || TRB1->V3				} )
TRCell():New( oFirst1, 'V4'   			,'TRB1', 'Volume 4'		,"@E 9999999999"		,10,,		{ || TRB1->V4	    		} ) 
TRCell():New( oFirst1, 'V5'   			,'TRB1', 'Volume 5'		,"@E 9999999999"		,10,,		{ || TRB1->V5	    		} ) 
TRCell():New( oFirst1, 'V6'   			,'TRB1', 'Volume 6'		,"@E 9999999999"		,10,,		{ || TRB1->V6	    		} ) 
TRCell():New( oFirst1, 'Vlr. Kit'		,'TRB1', 'Vlr. Kit'		,"@E 999,999,999.99"	,15,,		{ || TRB1->Vrl_Kit	 		} )
TRCell():New( oFirst1, 'Vlr. Volume'	,'TRB1', 'Vlr. Volume' 	,"@E 999,999,999.99"	,15,,		{ || TRB1->Vrl_Volume		} )

//oFirst2 := TRSection():New( oReport	, "Valores", { 'TRB2'},,.f.,.t. )
//TRCell():New( oFirst2, 'Serie'		,'TRB2', 'Serie'		,"@!"   				,10,,		{ || TRB2->Serie 			} )
//TRCell():New( oFirst2, 'Produto'  	,'TRB2', 'Itens' 		,"@!"					,30,,		{ || TRB2->Produto 			} )
//TRCell():New( oFirst2, 'VL1'     		,'TRB2', 'Valor 1'		,"@E 999,999,999.99"	,15,,		{ || TRB2->VL1				} )
//TRCell():New( oFirst2, 'VL2'   		,'TRB2', 'Valor 2'		,"@E 999,999,999.99"	,15,,		{ || TRB2->VL2				} )
//TRCell():New( oFirst2, 'VL3'   		,'TRB2', 'Valor 3'		,"@E 999,999,999.99"	,15,,		{ || TRB2->VL3				} )
//TRCell():New( oFirst2, 'VL4'   		,'TRB2', 'Valor 4'		,"@E 999,999,999.99"	,15,,		{ || TRB2->VL4	    		} ) 
//TRCell():New( oFirst2, 'VL5'   		,'TRB2', 'Valor 5'		,"@E 999,999,999.99"	,15,,		{ || TRB2->VL5	    		} ) 
//TRCell():New( oFirst2, 'VL6'   		,'TRB2', 'Valor 6'		,"@E 999,999,999.99"	,15,,		{ || TRB2->VL6	    		} ) 
//TRCell():New( oFirst2, 'Vl_Total'		,'TRB2', 'Total'		,"@E 999,999,999.99"	,15,,		{ || TRB2->Vl_TOTAL	 		} )


//oBreakT := TRBreak():New( oFirst, { || }, 'Vlr. Total' )
//TRFunction():New( oFirst:Cell( 'Vlr. Total' ),	'', 'SUM', oBreakT,,,, .F., .F. )
//oFirst:Cell( 'TOTAL' ):SetHeaderAlign( 'RIGHT' )
//oFirst:SetColSpace(9)


Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author Francisco Valdeni
@since 24 de Novembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

MakeAdvplExpr( cPergCont )
oReport:Section(1):Enable()

dbSelectArea( 'TMP1' )
oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 12 de Dezembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function AjustaSX1()
Local aHelp	:= {}
Local aArea	:= GetArea()

SX1->( dbSetOrder(1) )

If !SX1->( dbSeek( cPergCont ) )
	Aadd( aHelp, { { 'Informe o C�digo do Cliente <F3> Disponivel.'	}					,	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'Digite o ANO de Copetencia referente aos dados exemplo: 2015'}	, 	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'Defina a Filail (Coligada) geradora das informa��es'}				, 	{ '' }, { ' ' } } )	

	PutSx1( cPergCont, "01", "Cliente 	?			", "", "", "mv_ch1", "C", 06, 0, 0, "G", "", "SA1"	, "", "", "MV_PAR01", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[1][1], aHelp[1][2], aHelp[1][3] )
	PutSx1( cPergCont, "02", "Ano Cop.	?			", "", "", "mv_ch2", "C", 04, 0, 0, "G", "", ""		, "", "", "MV_PAR02", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[2][1], aHelp[2][2], aHelp[2][3] )
	PutSx1( cPergCont, "03", "Filial Colig. ?      	", "", "", "mv_ch3", "C", 06, 0, 0, "G", "", "SM0"	, "", "", "MV_PAR03", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[3][1], aHelp[3][2], aHelp[3][3] )
EndIf

RestArea( aArea )

Return( Nil)
