#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWIZARD.CH"
#include "Fileio.ch"
#include "Topconn.ch"
#INCLUDE "TBICONN.CH" 
#include "TOTVS.CH"
#INCLUDE "rwmake.ch"
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �SASP055   � Autor � Vileimar F Gadelha    � Data � 14.10.16 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Rotina de Compensacao de titulos a partir de csv           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � SIGAFIN - SAS - Financeiro - Contas a Receber              ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data     � BOPS �  Motivo da Alteracao                   ���
�������������������������������������������������������������������������Ĵ��
���            �          �------�                                        ��� 
�������������������������������������������������������������������������Ĵ��
���            �          �------�                                        ��� 
�������������������������������������������������������������������������Ĵ��
���            �          �------�                                        ��� 
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
/*/{Protheus.doc} SASP055
Compensar Titulos
@author Vileimar
@since 14/10/2015
@version 1.0
@example examples
@see (links_or_references)
/*/
User Function SASP055()

	Local aArea  := GetArea()
    Local aPergs   := {}
    Local cCodRec  := space(100)
    Local cArqbaixa := padr("",150)
    Local cArqcomp := padr("",150)
    Local aRet	   := {}
    Local aBaixar	:= {}
    Local aComp	:= {}
  	Local cCrLf 	:= Chr(13)+Chr(10)
  	
	Private nRecnoNDF
	Private nRecnoE1

    aAdd( aPergs ,{6,"Titulos baixar",cArqbaixa,"",,"", 90 ,.T.,"Arquivos .CSV |*.CSV","C:\",GETF_LOCALHARD+GETF_LOCALFLOPPY+GETF_NETWORKDRIVE})
    aAdd( aPergs ,{6,"Titulos compensar",cArqcomp,"",,"", 90 ,.T.,"Arquivos .CSV |*.CSV","C:\",GETF_LOCALHARD+GETF_LOCALFLOPPY+GETF_NETWORKDRIVE})

    If ParamBox(aPergs ,"Compensar Titulos",aRet)

    	//Ler dados do arquivo
        Processa({||aBaixar := ImportTitulos(aRet[1])},"Titulos a baixar","Lendo arquivo aguarde...", .F.)
        Processa({||aComp := ImportTitulos(aRet[2])},"Titulos a compensar","Lendo arquivo aguarde...", .F.)

    	If (Len(aBaixar) > 0 .and. Len(aComp) > 0)
    		Processa({||Compensar(aBaixar,aComp)},"Compensar Titulos","Favor aguarde...", .F.)
    	Else
    		MsgStop("Sem registros.","Compensar Titulos")
    		Return
    	ENDIF
    Else
        MsgStop("Processo cancelado","Compensar Titulos")
        Return
    EndIf

    RestArea(aArea)

Return


Static Function ImportTitulos(cArquivo)
	Local aArea2  := GetArea()
	Local cBuffer := ""
  	Local aTitulo := {}
  	Local nHandle
  	Local cCrLf 	:= Chr(13) + Chr(10)
  	Local n := 0
  	Local i := 0

	If !File(cArquivo)
		MsgStop("Arquivo texto: "+cArquivo+" n�o localizado","CSV")
		Return
	Endif
	
	FT_FUSE(cArquivo) //ABRIR
	
	//Define o Tamanho da R�gua de Processamento
	n := FT_FLastRec()
	ProcRegua(n)
	
	//Vai para a Primeira Linha do Arquivo
	FT_FGoTop()
	
	While !FT_FEOF() //FACA ENQUANTO NAO FOR FIM DE ARQUIVO
		i := i + 1
		// Capturar dados
		cBuffer := FT_FREADLN() //LENDO LINHA
        IncProc("Aguarde! Lendo linha: "+ AllTrim(Str(i)) + " de " + Alltrim(Str(n)))
		if cBuffer<>""
            //Alert(cBuffer)
			aAdd(aTitulo,StrTokArr(cBuffer,";"))  
		Endif
		//proximo registro no arquivo txt
		FT_FSKIP() 
	EndDo

	//Fecha o arquivo csv
	FT_FUSE()

	//Dormir para prevenir possiveis falhas no processamento
	Sleep(1000)
	RestArea(aArea2)
Return aTitulo

Static Function Compensar(aBaixar, aComp)

	Local aArea3  	:= GetArea()
	Local lRetOK 	:= .T.
	Local nTaxaCM 	:= 0
	Local aTxMoeda 	:= {}
    Local aRet	   	:= {}
  	Local cCrLf 	:= Chr(13) + Chr(10)
  	Local aRecSE1	:= {} //Titulos a baixar
  	Local aRecRA 	:= {} //Compensados
  	Local cSemB		:= "" //Baixado nao encontrados
  	Local cSemC		:= "" //Compensado nao encontrado
  	Local cMsg		:= ""
	Private nRecnoNDF
	Private nRecnoE1

		//������������������������������������������������������������������Ŀ
		//� Obtendo recnos dos titulos a baixar                              �
		//��������������������������������������������������������������������
	    If Len(aBaixar)>0 
			n:=Len(aBaixar)
			ProcRegua(n)
			For i:=1 to n
				cSQLE1 := "SELECT R_E_C_N_O_, E1_MOEDA FROM "+RetSqlName("SE1")+" WHERE E1_FILIAL='"+xFilial("SE1")+"' AND E1_CLIENTE='"+aBaixar[i][2]+"' AND E1_LOJA='"+aBaixar[i][3]+"' AND E1_PREFIXO='"+aBaixar[i][4]+"' AND E1_NUM='"+aBaixar[i][5]+"' AND E1_PARCELA='"+aBaixar[i][6]+"' AND E1_TIPO='"+aBaixar[i][7]+"' AND D_E_L_E_T_=''"
				TCQUERY cSQLE1 NEW ALIAS "QSE1"
				COUNT TO nRec		
				QSE1->(dbGoTop())
				//SE ENCONTAR O TITULO
				If nRec > 0
				   	aAdd(aRecSE1,QSE1->R_E_C_N_O_)
				Else
					cSemB += "E1_FILIAL='"+xFilial("SE1")+"' E1_CLIENTE='"+aBaixar[i][2]+"' E1_LOJA='"+aBaixar[i][3]+"' E1_PREFIXO='"+aBaixar[i][4]+"' E1_NUM='"+aBaixar[i][5]+"' E1_PARCELA='"+aBaixar[i][6]+"' E1_TIPO='"+aBaixar[i][7]+"'"+ cCrLf	
	           	ENDIF
	           	QSE1->(dbCloseArea())
				//Processar
		        IncProc("Aguarde! Obtendo dados do titulo baixado: "+Alltrim(str(i))+" de " + Alltrim(Str(n)))	
	        Next
        Else
           	MsgStop("N�o h� titulo a ser baixado .","Compensar Titulos")
           	return
	    EndIf

		//������������������������������������������������������������������Ŀ
		//� Obtendo recnos dos titulos a compensar                           �
		//��������������������������������������������������������������������
	    If Len(aComp)>0 
			n:=Len(aComp)
			ProcRegua(n)
			For i:=1 to n
				cSQLE1 := "SELECT R_E_C_N_O_, E1_MOEDA FROM "+RetSqlName("SE1")+" WHERE E1_FILIAL='"+xFilial("SE1")+"' AND E1_CLIENTE='"+aComp[i][2]+"' AND E1_LOJA='"+aComp[i][3]+"' AND E1_PREFIXO='"+aComp[i][4]+"' AND E1_NUM='"+aComp[i][5]+"' AND E1_PARCELA='"+aComp[i][6]+"' AND E1_TIPO='"+aComp[i][7]+"' AND D_E_L_E_T_=''"
				TCQUERY cSQLE1 NEW ALIAS "QSE1"
				COUNT TO nRec		
				QSE1->(dbGoTop())
				//SE ENCONTAR O TITULO
				If nRec > 0
				    aAdd(aRecRA,QSE1->R_E_C_N_O_)
				Else
					cSemC += "E1_FILIAL='"+xFilial("SE1")+"' E1_CLIENTE='"+aComp[i][2]+"' E1_LOJA='"+aComp[i][3]+"' E1_PREFIXO='"+aComp[i][4]+"' E1_NUM='"+aComp[i][5]+"' E1_PARCELA='"+aComp[i][6]+"' E1_TIPO='"+aComp[i][7]+"'"+ cCrLf			
	           	ENDIF
	           	QSE1->(dbCloseArea())
				//Processar
				IncProc("Aguarde! Obtendo dados do titulo compensado: "+Alltrim(str(i))+" de " + Alltrim(Str(n)))
	        Next
        Else
           	MsgStop("N�o h� titulo a ser compensado.","Compensar Titulos")
           	return
	    EndIf

		//������������������������������������������������������������������Ŀ
		//� Compensar titulos                                                �
		//��������������������������������������������������������������������	    
	    If (Len(aRecSE1)>0 .and. Len(aRecRA)>0) 
		   //nTaxaCM := RecMoeda(dDataBase,QSE1->E1_MOEDA)
		   //aAdd(aTxMoeda, {1, 1} )
		   //aAdd(aTxMoeda, {2, nTaxaCM} )
		   // E1_FILIAL, E1_CLIENTE, E1_LOJA, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO, R_E_C_N_O_, D_E_L_E_T_
		   //Nao exibe parametros
		   If (Len(alltrim(cSemB))>0 .or. Len(Alltrim(cSemC))>0)
		   		If Len(alltrim(cSemB))>0
					cMsg += "Titulos Baixar:"+cCrLf
					cMsg += cSemB+cCrLf
		   		EndIf
		   		If Len(Alltrim(cSemC))>0
					cMsg += "Titulos Compensar:"+cCrLf
					cMsg += cSemC+cCrLf
		   		EndIf
			   If MsgYesNo("Existem titulos n�o encontrados, deseja continuar?"+cCrLf+ cMsg, "Aten��o")
				   PERGUNTE("AFI340",.F.)
				   If !MaIntBxCR(3,aRecSE1,,aRecRA,,{.F.,.F.,.F.,.F.,.F.,.F.})
				      Help("XAFCMPAD",1,"HELP","XAFCMPAD","N�o foi poss�vel a compensa��o"+CRLF+" do titulo do adiantamento",1,0)
				   	  lRet := .F.
				   ENDIF
				   MsgInfo("Compensa��o finalizada com sucesso.","Compensar Titulos")
			   Else
			   		RestArea(aArea3)
			        MsgStop("Processo cancelado","Compensar Titulos")	   		
			   EndIf
		   Else
		   	   //ProcRegua(Len(aRecSE1)+Len(aRecRA))
			   //IncProc("Favor Aguarde! Compensando... ")		   	   
			   PERGUNTE("AFI340",.F.)	
			   If !MaIntBxCR(3,aRecSE1,,aRecRA,,{.F.,.F.,.F.,.F.,.F.,.F.})
			      Help("XAFCMPAD",1,"HELP","XAFCMPAD","N�o foi poss�vel a compensa��o"+CRLF+" do titulo do adiantamento",1,0)
			   	  lRet := .F.
			   ENDIF
			   MsgInfo("Compensa��o finalizada com sucesso.","Compensar Titulos")		   		
		   EndIf 	   		
       Else
           	MsgStop("N�o h� titulo a ser compensado.","Compensar Titulos")
	   EndIf
RestArea(aArea3)

Return