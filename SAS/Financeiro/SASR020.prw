#Include 'Protheus.ch'
#Include 'TopConn.ch'


//_____________________________________________________________________________
/*/{Protheus.doc} OSBQCTR
Relatorio de Devolu��es - Lojinha X Protheus

@author Carlos Castro	
@since 01 de Junho de 2015
@version P01
/*/
//_____________________________________________________________________________

User Function SASR020()

Local aRegs			:= {}
Private oReport
Private cPergCont	:= 'SASR020' 
Private _astru		:={}
************************
*Monta pergunte do Log *
************************
AjustaSX1()

If Pergunte( cPergCONT, .T. )    
	Processa( { || ProcCont() }, 'Processando....' )
EndIf


Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ProcCont
Carrega os dados conforme parametrizacao feita pelo usuario;

@author Carlos Castro
@since 01 de Junho de 2015
@version P01
/*/
//_____________________________________________________________________________
Static Function ProcCont()
Local cQuery	:= ""
Local aPagto    := {}

BeginSql Alias "TMP"

SELECT 
	  CODCONVENIADA 
	 ,RAZAOSOCIAL
	 ,KIT
	 ,L.SERIE
	 ,L.ENVIO
	 ,QTDACEITA
	 ,DATASOLICITACAO AS DATASOLICITACAO
	 ,ETAPA
	 ,D.FILIAL
	 ,D.NOTA1 AS NF_DEVOLUCAO1
	 ,D.NOTA2 AS NF_DEVOLUCAO2
	 ,D.NOTA3 AS NF_DEVOLUCAO3
	 ,D.D1_QUANT AS QUANTIDADE_GERADA
	 ,D.D1_EMISSAO AS DATA_EMISSAO
	 ,D.Z8_ENTRADA AS PROCESSADA
  FROM [54.94.250.138].LOJINHASAS.DBO.VW_DEVOLUCAO_LOJA L LEFT JOIN VW_DEVOL_PROTHEUS D ON L.CODCONVENIADA = D.Z8_CLIENTE collate Latin1_General_CI_AS AND L.ENVIO = D.ENVIO AND RIGHT(('0') + L.CODSERIE,2) = D.SERIE  collate Latin1_General_CI_AS
  WHERE L.CANCELADO = 0
  		AND (CODCONVENIADA = %EXP:MV_PAR01% OR %EXP:MV_PAR01% = '*')
  		AND DATASOLICITACAO BETWEEN  CONVERT(CHAR(8),%EXP:MV_PAR02%,112) AND CONVERT(CHAR(8),%EXP:MV_PAR03%,112)
  		AND D.D1_EMISSAO BETWEEN CONVERT(CHAR(8),%EXP:MV_PAR04%,112) AND CONVERT(CHAR(8),%EXP:MV_PAR05%,112)
  
 GROUP BY CODCONVENIADA 
		 ,RAZAOSOCIAL
		 ,KIT
		 ,L.SERIE
		 ,L.ENVIO
		 ,QTDACEITA
		 ,DATASOLICITACAO
		 ,ETAPA
		 ,D.FILIAL
		 ,D.NOTA1
		 ,D.NOTA2
		 ,D.NOTA3
		 ,D.D1_QUANT
		 ,D.D1_EMISSAO
		 ,D.Z8_ENTRADA

EndSql



oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()

TMP->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Carlos Castro
@since 01 de Junho de 2015
@version P01
/*/
//_____________________________________________________________________________
Static Function ReportDef()
Local oFirst
Local nOrd	:= 1


oReport := TReport():New( 'TMP', 'Relatorio de Devolu��es - Lojinha X Protheus', cPergCont, {|oReport| ReportPrint( oReport ), 'Relatorio de Devolu��es - Lojinha X Protheus' } )
oReport:SetLandScape()
oReport:lParamReadOnly := .T.

oReport:SetTotalInLine( .F. )

oFirst := TRSection():New( oReport, 'Relatorio de Devolu��es - Lojinha X Protheus', { 'TMP', 'SZ8', 'SZ5', 'SD1', 'SC6', 'SD1','SD2'},,, )

oFirst:SetTotalInLine( .F. )


TRCell():New( oFirst, 'CODCONVENIADA'   ,'TMP', 'CODCONVENIADA',	"@!"						    ,06,,							{ || TMP->CODCONVENIADA		} )
TRCell():New( oFirst, 'RAZAO SOCIAL'    ,'TMP', 'RAZAO SOCIAL',     "@!"							,50,,					        { || TMP->RAZAOSOCIAL  	} )
TRCell():New( oFirst, 'KIT'	   	        ,'TMP', 'KIT' ,		    	"@!"	       					,40,,							{ || TMP->KIT	   		    } )
TRCell():New( oFirst, 'SERIE'       	,'TMP', 'SERIE',		    "@!"	       					,20,,							{ || TMP->SERIE 	   		} )
TRCell():New( oFirst, 'ENVIO'   		,'TMP', 'ENVIO' ,		    "@E 999,999,999.99"	    		,01,,							{ || TMP->ENVIO         	} ) 
TRCell():New( oFirst, 'QTD_ACEITA'		,'TMP', 'QTD_ACEITA' ,	   	"@E 999,999,999.99"				,06,,							{ || TMP->QTDACEITA      	} )
TRCell():New( oFirst, 'DATA_SOLICITACAO','TMP', 'DATA_SOLICITACAO' ,""							    ,08,,							{ || TMP->DATASOLICITACAO   } )
TRCell():New( oFirst, 'ETAPA'	   	    ,'TMP', 'ETAPA',		    "@!"   	  						,12,,							{ || TMP->ETAPA		      	} )
TRCell():New( oFirst, 'FILIAL'			,'TMP', 'FILIAL' ,	    	"@!"							,06,,							{ || TMP->FILIAL			} )
TRCell():New( oFirst, 'NF_DEVOLUCAO1'	,'TMP', 'NF_DEVOLUCAO1' ,	"@!"							,09,,							{ || TMP->NF_DEVOLUCAO1		} )
TRCell():New( oFirst, 'NF_DEVOLUCAO2'	,'TMP', 'NF_DEVOLUCAO2' ,	"@!"							,09,,							{ || TMP->NF_DEVOLUCAO2		} )
TRCell():New( oFirst, 'NF_DEVOLUCAO3'	,'TMP', 'NF_DEVOLUCAO3' ,	"@!"							,09,,							{ || TMP->NF_DEVOLUCAO3		} )
TRCell():New( oFirst, 'QTD_GERADA'	    ,'TMP', 'QTD_GERADA',		"@E 999,999,999.99"	  			,06,,							{ || TMP->QUANTIDADE_GERADA	} )
TRCell():New( oFirst, 'DATA EMISSAO'	,'TMP', 'DATA EMISSAO',		""							    ,08,,						    { || STOD(TMP->DATA_EMISSAO)} )
TRCell():New( oFirst, 'PROCESSADA'	   	,'TMP', 'PROCESSADA',		"@!"							,01,,							{ || TMP->PROCESSADA		} )


//oBreakT := TRBreak():New( oFirst, { || }, 'Vlr. Total' )

//TRFunction():New( oFirst:Cell( 'ValFat2' ),	'', 'SUM', oBreakT,,,, .F., .F. )

//oFirst:Cell( 'TOTAL' ):SetHeaderAlign( 'RIGHT' )

//oFirst:SetColSpace(9)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author Castro
@since 01 de Junho de 2015
@version P01
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

MakeAdvplExpr( cPergCont )
oReport:Section(1):Enable()

dbSelectArea( 'TMP' )
oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author CASTRO
@since 01 de Junho de 2015
@version P01
/*/
//_____________________________________________________________________________
Static Function AjustaSX1()
Local aHelp	:= {}
Local aArea	:= GetArea()

SX1->( dbSetOrder(1) )

If !SX1->( dbSeek( cPergCont ) )
	Aadd( aHelp, { { 'C�DIGO DO CLIENTE PARA RODAR PARA 1 CLIENTE OU * PARA TODOS OS CLIENTES.'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'DATA DE SOLICITA��O INICIAL(REALIZADA NA LOJINHA).'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'DATA DE SOLICITA��O FINAL(REALIZADA NA LOJINHA).'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'DATA DE EMISS�O DA NF(EFETIVADAS NO PROTHEUS).'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'DATA DE EMISS�O DA NF(EFETIVADAS NO PROTHEUS).'	},	{ '' }, { ' ' } } )
	
	PutSx1( cPergCont, "01", "CLIENTE ?		", "", "", "mv_ch1", "S", 06, 0, 0, "C", "", "", "", "", "MV_PAR01", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , "", "", "" )
	PutSx1( cPergCont, "02", "DT_SOLIC_INI ?", "", "", "mv_ch2", "S", 08, 0, 0, "D", "", "", "", "", "MV_PAR02", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , "", "", "" )
	PutSx1( cPergCont, "03", "DT_SOLIC_FIM ?", "", "", "mv_ch3", "S", 08, 0, 0, "D", "", "", "", "", "MV_PAR03", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , "", "", "" )
	PutSx1( cPergCont, "04", "DT_EMISSAO_INI ? ", "", "", "mv_ch4", "S", 08, 0, 0, "D", "", "", "", "", "MV_PAR04", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , "", "", "" )
	PutSx1( cPergCont, "05", "DT_EMISSAO_FIM ?		", "", "", "mv_ch5", "S", 08, 0, 0, "D", "", "", "", "", "MV_PAR05", "",	"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , "", "", "" )

EndIf

RestArea( aArea )

Return( Nil)