#include "protheus.ch"
#INCLUDE "topconn.ch"

/*/
+-----------------------------------------------------------------------------
| Programa | SASP071() | Autor | Francisco Valdeni | Data |23/05/2016 		 |
+-----------------------------------------------------------------------------
| Desc.    | MarkBrowse Aprova��o Financeiro				     			 |
+-----------------------------------------------------------------------------
| Uso      | Sistema Ari de S� (SAS)                                         |
+-----------------------------------------------------------------------------
/*/
*----------------------
USER FUNCTION SASP071()
*----------------------
Local _astru:={}
Private _afields:={}                                             
Private cCodUse	:= RETCODUSR()
Private oMark 
Private arotina := {}
Private cCadastro
Private cMark:=GetMark()
private _cAlias
Private _RecTRB


//AADD(aRotina,{"Pesquisar" 		,"AxPesqui" 	,0,1})
AADD(aRotina,{"Visualizar" 		,"u_VsSPApr()" 	,0,2})
AADD(aRotina,{"Aprovar" 		,"u_AprFin()" 	,0,4})
AADD(aRotina,{"Rejeitar" 		,"u_RejFin()" 	,0,4})
AADD(aRotina,{"Imprime"			,"U_Imp_SP(.F.)" ,0,8})
									
AADD(aRotina,{"Rela��o de P.F"  ,"U_SASP067(8)"	,0,8})

cCadastro := "Integra��o Financeiro de P.F"

//� Estrutura da tabela temporaria
AADD(_astru,{"ZA7_OK"    	,"C",02,0})
AADD(_astru,{"ZA7_FILIAL" 	,"C",06,0})
AADD(_astru,{"ZA7_NUM"	 	,"C",09,0})
AADD(_astru,{"ZA7_PREFIX" 	,"C",03,0})
AADD(_astru,{"ZA7_TIPO" 	,"C",03,0})
AADD(_astru,{"ZA7_NOMFOR"  	,"C",30,0})
AADD(_astru,{"ZA7_EMISSA"	,"D",08,0})
AADD(_astru,{"ZA7_VLTELA"	,"C",20,0})
AADD(_astru,{"ZA7_VALOR"	,"N",16,2})
AADD(_astru,{"ZA7_DESCRI"	,"C",50,0})
//AADD(_astru,{"ZA7_VALPA"	,"N",16,2})
//AADD(_astru,{"ZA7_SALDO"	,"N",16,2})
// cria a tabela tempor�ria
_carq:="T_"+Criatrab(,.F.)
MsCreate(_carq,_astru,"DBFCDX") 
//�����������������������������������������������������������Ŀ
//�atribui a tabela tempor�ria ao alias TRB
//�������������������������������������������������������������
dbUseArea(.T.,"DBFCDX",_cARq,"TRB",.T.,.F.)

//�����������������������������������������������������������Ŀ
//� selecionando registros. 					              �
//�������������������������������������������������������������
cQuery:=" SELECT ZA7_FILIAL,ZA7_NUM, ZA7_PREFIX, ZA7_NOMFOR,ZA7_EMISSA,ZA7_VALOR,ZA7_TIPO,ISNULL(CONVERT(VARCHAR(2047), CONVERT(VARBINARY(2047), ZA7_DESCRI)),'') AS ZA7_DESCRI, ZA7_VALPA, ZA7_SALDO  "  "
cQuery+=" FROM " + RetSqlName("ZA7") + " ZA7 (nolock)"
cQuery+=" WHERE "
cQuery+=" ZA7.D_E_L_E_T_ <> '*' AND "
cQuery+=" ZA7.ZA7_STATUS = '2' OR ZA7.ZA7_STATUS = '7' "
cQuery+=" ORDER BY ZA7_NUM "

IF SELECT("TX")>0
	TX->(dbCloseArea())
Endif

TCQUERY cQuery NEW ALIAS TX

dbSelectArea("TX")
TX->(dbGotop())

While TX->(!Eof())

		RecLock("TRB",.T.)
			TRB->ZA7_FILIAL		:=  TX->ZA7_FILIAL
			TRB->ZA7_NUM 		:=  TX->ZA7_NUM
			TRB->ZA7_PREFIX		:= 	TX->ZA7_PREFIX
			TRB->ZA7_TIPO		:= 	TX->ZA7_TIPO
			TRB->ZA7_NOMFOR 	:=  TX->ZA7_NOMFOR
			TRB->ZA7_EMISSA		:=  sTOd(TX->ZA7_EMISSA)
			TRB->ZA7_VLTELA		:=  rtrim(Transform(TX->ZA7_VALOR,"@E 999,999,999,999.99"))
			TRB->ZA7_VALOR		:= 	TX->ZA7_VALOR
			TRB->ZA7_DESCRI     := 	TX->ZA7_DESCRI 	
			//TRB->ZA7_VALPA		:=  TX->ZA7_VALPA 
			//TRB->ZA7_SALDO      :=  TX->ZA7_SALDO
		TRB->(MSUNLOCK())
	TX->(dbSkip())
EndDo      

TX->(dbclosearea())

//�����������������������������������������������������������Ŀ
//�Colunas do browse [verificar necessidade de novos campos do browser]
//�������������������������������������������������������������
AADD(_afields,{"ZA7_OK"    		,"",""            		})
AADD(_afields,{"ZA7_FILIAL" 	,"","Filial"	   		})
AADD(_afields,{"ZA7_NUM" 		,"","Numero"	   		})
AADD(_afields,{"ZA7_PREFIX" 	,"","Prefixo"	   		})
AADD(_afields,{"ZA7_TIPO"	 	,"","Tipo"		   		})
AADD(_afields,{"ZA7_NOMFOR" 	,"","Nom. Fornecedor"	})
AADD(_afields,{"ZA7_EMISSA"		,"","Emissao"	  		})
AADD(_afields,{"ZA7_VLTELA"		,"","Valor" 			})
AADD(_afields,{"ZA7_DESCRI"		,"","Descri��o"			})
//AADD(_afields,{"ZA7_VALPA"		,"","Valor PA"			})
//AADD(_afields,{"ZA7_SALDO"		,"","Saldo"				})


DbSelectArea("TRB")
DbGotop()


//TCSetField ( "TRB" , "ZA7_VALOR", "N", 16, 2 )
*--------------------------------------------------------------------------------------------------
MarkBrow( 'TRB', 'ZA7_OK',,_afields,.F., cMark,'u_MkAllFIN()',,,,'u_MkSP()',{|| u_MkAllFIN()},,,,,,,.F.)
*--------------------------------------------------------------------------------------------------

ZA7->(dbCloseArea())
TRB->(dbCloseArea())
//�����������������������������������������������������������Ŀ
//�apaga a tabela tempor�rio
//�������������������������������������������������������������
MsErase(_carq+GetDBExtension(),,"DBFCDX")

Return()                    	

*----------------------
User Function MkAllFIN()
*----------------------
Local oMark := GetMarkBrow()

dbSelectArea('TRB')
TRB->(dbGotop())

While !TRB->(Eof())
   u_MkFIN()
TRB->(dbSkip())
End
MarkBRefresh( )
oMark:oBrowse:Gotop()

Return

*-------------------
User Function MkFIN()
*-------------------
If IsMark( 'ZA7_OK', cMark )

        RecLock( 'TRB', .F. )
           Replace ZA7_OK With Space(2)
        TRB->(MsUnLock())
Else
        RecLock( 'TRB', .F. )
          Replace ZA7_OK With cMark
        TRB->(MsUnLock())
EndIf

Return
                                            

*-----------------------
User Function AprFin()
*-----------------------
Local cMsg	:= ""    
Local nTotal:= 0
Local nCont	:= 0
	dbSelectArea("TRB")
	TRB->(dbGotop())
	While !TRB->(Eof())
		IF !Empty(Alltrim(TRB->ZA7_OK))        
	    	nTotal	+= TRB->ZA7_VALOR
	    	nCont ++
	    ENDIF
	TRB->(dbSkip())
	ENDDO
cMsg	:= "CONFIRMA APROVA��O FINANCEIRA DE ("+Alltrim(Str(nCont))+") PEDIDO(S) FINANCEIRO(S) ? "+CHR(13)+CHR(10)
cMsg	+= "TOTAL >>> R$"+Alltrim(Transform(nTotal,"@E 999,999,999,999.99"))+"<<<"+CHR(13)+CHR(10)
cMsg	+= "("+Alltrim(Extenso(nTotal))+")"


IF MsgYesNo(cMsg,"Confirma prova��o")
	Processa( {|| u_AprSPFI() }, "Aprovando..." ) 
ENDIF

	
Return 
*----------------------
User Function AprSPFI()
*----------------------
Local lRet		:= .T.
Local nX 		:= 0
Local cTexto 	:= ""
Local cEOL 		:= CHR(10)+CHR(13)
Local nValor	:= 0
Local nLimite   := 0
Local i			:= 0
Local nCOnt		:= 0
Local lAprovador:= .F.     
Local lPrimeira	:= .T.
Local aGrvSe2	:= {}
Local aRatEz  	:= {}
Local aAuxEz	:= {}
Local aAuxEv	:= {}
Local aRatEvEz	:= {} 
Local aVetor	:= {}
Local aCab		:= {}
Local cMaiorCC	:= ""
Local cHistPA	:= ""
Local nMaiorCC 	:= 0
Local oDlg
Local oMemo
Local _cFilAntOLD	:= cFilAnt 
Local _cEmpAntOLD	:= cEmpAnt 
Local Old_dDataB	:= dDataBase


ProcRegua(TRB->(RecCount())) //QTOS REGISTROS LER
Begin Transaction
	dbSelectArea("TRB")
	TRB->(dbGotop())
	While !TRB->(Eof())
		IF !Empty(Alltrim(TRB->ZA7_OK))        
			*-----------------------------------------
			cFilAnt	:= alltrim(TRB->ZA7_FILIAL)+"0101"					
			*-----------------------------------------	
			IncProc("P.F "+TRB->ZA7_NUM)
	        dbSelectArea("ZA7")
	        ZA7->(dbSetOrder(2)) //ZA7_FILIAL+ZA7_NUM    
	        ZA7->(dbGotop())
	        IF ZA7->(dbSeek(TRB->ZA7_FILIAL+TRB->ZA7_NUM))		

						aCab	:= {}
						aAuxEv	:= {}
						aAuxEz	:= {}
						aRatEz	:= {} 
						aRatEvEz:= {} 
						//--------------------------------------------------------------------------------------------------------------------------------------------------------						
						aadd( aCab ,{"E2_FILIAL" 	, ZA7->ZA7_FILIAL	, Nil })
						aadd( aCab ,{"E2_PREFIXO" 	, ZA7->ZA7_PREFIX	, Nil })            
						aadd( aCab ,{"E2_NUM" 		, ZA7->ZA7_NUM		, Nil })
						aadd( aCab ,{"E2_PARCELA" 	, ZA7->ZA7_PARCEL 	, Nil })
						aadd( aCab ,{"E2_TIPO" 		, ZA7->ZA7_TIPO		, Nil })						                      
						
						IF ZA7->ZA7_TIPO == "PA "						
							dDataBase	:= ZA7->ZA7_VENCTO 							
							_aArea	:= GetArea()
							*---------------------------------------
							cHistPA	:= u_SASP072(ZA7->ZA7_NUM,1,.T.)
							*----------------------------------------							
							RestArea(_aArea) 
							aadd( aCab ,{"AUTBANCO" 		, ZA7->ZA7_BANCO		, Nil })
							aadd( aCab ,{"AUTAGENCIA" 		, ZA7->ZA7_AGENCI		, Nil })
							aadd( aCab ,{"AUTCONTA" 		, ZA7->ZA7_CONTA		, Nil })																									
							aadd( aCab ,{"E2_HIST" 			, Alltrim(cHistPA)		, Nil })							
							aadd( aCab ,{"E2_EMISSAO" 		, ZA7->ZA7_VENCTO		, Nil })							
						ELSE						
							aadd( aCab ,{"E2_EMISSAO" 	, ZA7->ZA7_EMISSA			, Nil })
							aadd( aCab ,{"E2_HIST" 		, "PEDIDO FINANCEIRO"		, Nil })
						ENDIF		
																												
						aadd( aCab ,{"E2_FORNECE" 	, ZA7->ZA7_FORNEC			, Nil })
						aadd( aCab ,{"E2_LOJA" 		, ZA7->ZA7_LOJA				, Nil })
						aadd( aCab ,{"E2_NATUREZ" 	, ZA7->ZA7_NATURE			, Nil })
						aadd( aCab ,{"E2_VENCTO" 	, ZA7->ZA7_VENCTO			, Nil }) 
						aadd( aCab ,{"E2_VENCREA" 	, ZA7->ZA7_VENCRE			, Nil })
						aadd( aCab ,{"E2_VALOR" 	, ZA7->ZA7_VALOR			, Nil })
												
						aadd( aCab ,{"E2_CODBAR" 	, ZA7->ZA7_CODBAR	 		, Nil })												
						aadd( aCab ,{"E2_FILORIG"	, cFilAnt	 				, Nil })
						aadd( aCab ,{"E2_MULTNAT" 	, '1'						, Nil })//rateio multinaturezs = sim
						aadd( aCab ,{"E2_ORIGEM" 	, 'SASP071'					, Nil })
					
																																	
						dbSelectArea("SEV") // Cadastro de Multiplas Naturezas
						dbSelectArea("SEZ") // Cadastro de Rateio Por Centro de Custo
						dbselectArea("ZA9") // Cadatro de Multiplas Naturezas e Rateio de Centro de Custo Temporaria.
						ZA9->(dbSetOrder(3))// ZA9_FILIAL+ZA9_PREFIX+ZA9_NUM+ZA9_NATURE  ----//ZA9_FILIAL+ZA9_PREFIX+ZA9_NUM+ZA9_CLIFOR+ZA9_LOJA+ZA9_NATCC
						ZA9->(dbGotop())
						IF ZA9->(dbSeek(ZA7->ZA7_FILIAL+ZA7->ZA7_PREFIX+ZA7->ZA7_NUM))						
							While !ZA9->(EOF()) .AND. ZA9->ZA9_PREFIX == ZA7->ZA7_PREFIX .AND. ZA9->ZA9_NUM == ZA7->ZA7_NUM																
								IF ZA9->ZA9_NATCC == "N"    
											//-----Trata Multiplas Natutreza------------------
												IF !lPrimeira
												   	aadd(aAuxEv,{"AUTRATEICC" , aRatEz, Nil })  
											    	aAdd(aRatEvEz,aAuxEv)											    	
											    	aAuxEv	:= {} 
											    	aRatEz	:= {}												    											    
											    ENDIF
										        *----------------									        
										        lPrimeira	:= .F.
										        *----------------									        											    
											//------------------------------------------------
											
											// Adicionando o vetor da natureza										       										    										  										    
										    aadd( aAuxEv ,{"EV_FILIAL" ,  xFilial("SEV")					, Nil })
										    aadd( aAuxEv ,{"EV_NATUREZ" , ZA9->ZA9_NATURE					, Nil })
										    aadd( aAuxEv ,{"EV_VALOR" 	, ZA9->ZA9_VALOR					, Nil })
										    aadd( aAuxEv ,{"EV_PERC" 	, ZA9->ZA9_PERC/100					, Nil })
										    aadd( aAuxEv ,{"EV_RATEICC" , "1"							    , Nil })   
								ELSEIF ZA9->ZA9_NATCC == "C"  
											// Adicionando multiplos centros de custo																				    										
										    aAuxEz	:= {}                                                           
											aadd( aAuxEz ,{"EZ_FILIAL" 	, xFilial("SEZ")					, Nil })										    
										    aadd( aAuxEz ,{"EZ_CCUSTO" 	, ZA9->ZA9_CUSTO					, Nil })
										    aadd( aAuxEz ,{"EZ_VALOR" 	, ZA9->ZA9_VALOR					, Nil })										    														    										        					                											    				                					                					                					               				                	
											aadd(aRatEz,aAuxEz)
										   	aadd(aAuxEv,{"AUTRATEICC" , aRatEz, Nil })  
									    	//aAdd(aRatEvEz,aAuxEv)															                	
				                	/*
				                	RecLock("SEZ",.T.)
				                		SEZ->EZ_FILIAL	:= xFilial("SEZ")
				                		SEZ->EZ_PREFIXO := ZA9->ZA9_PREFIX
				                		SEZ->EZ_NUM		:= ZA9->ZA9_NUM
				                		SEZ->EZ_PARCELA	:= ZA9->ZA9_PARCEL
				                		SEZ->EZ_CLIFOR	:= ZA9->ZA9_CLIFOR
				                		SEZ->EZ_LOJA	:= ZA9->ZA9_LOJA
				                		SEZ->EZ_TIPO	:= ZA9->ZA9_TIPO
				                		SEZ->EZ_VALOR 	:= ZA9->ZA9_VALOR
				                		SEZ->EZ_NATUREZ	:= ZA9->ZA9_NATURE
				                		SEZ->EZ_CCUSTO	:= ZA9->ZA9_CUSTO
				                		SEZ->EZ_RECPAG	:= 'P'
				                		SEZ->EZ_PERC	:= ZA9->ZA9_PERC/100
				                		SEZ->EZ_IDENT	:= '1'
				                		SEZ->EZ_YINTPCO	:= ZA9->ZA9_INTPCO
				                		SEZ->EZ_YCO		:= ZA9->ZA9_YCO
				                	SEZ->(MsUnLock())
				                 	*/
				                	
				                	//---Grava Centro de Custo de Maior Rateio---
				                	IF ZA9->ZA9_VALOR > nMaiorCC
				                		cMaiorCC	:= ZA9->ZA9_CUSTO 
				                	ENDIF
				                	//-------------------------------------------									
								ENDIF								    
						    ZA9->(dbSkip())
						    ENDDO
						ENDIF
				   	aadd( aCab ,{"E2_CCD"  		, cMaiorCC			 		, Nil }) 	                                                          					
					nMaiorCC:= 0						
					lMSErroAuto := .F.
										        
	       			aadd(aAuxEv,{"AUTRATEICC" , aRatEz, Nil })//recebendo dentro do array da natureza os multiplos centros de custo
       				aAdd(aRatEvEz,aAuxEv)//adicionando a natureza ao rateio de multiplas naturezas
					aAdd(aCab,{"AUTRATEEV",ARatEvEz,Nil})//adicionando ao vetor aCab o vetor do rateio
					dbSelectArea("SE2")		
					dbSetOrder(1)
			            *-------------------------------------------
						MSExecAuto({|x,y,z| Fina050(x,y,z)},aCab,,3) 
			            *-------------------------------------------					

					If lMSErroAuto												
						dbSelectArea("SE2")
						SE2->(dbSetorder(1)) // Filial + Prefeixo + Num
						SE2->(dbGotop())
						IF SE2->(dbSeek(xFilial("SE2")+ZA7->ZA7_PREFIX+ZA7->ZA7_NUM))
							RecLock("ZA7",.F.)						
								ZA7->ZA7_STATUS	:= "3"
								ZA7->ZA7_CODLIB	:= cCodUse
								ZA7->ZA7_USELIB	:= USRRETNAME(RETCODUSR())
								ZA7->ZA7_DTLIB	:= Date()
								ZA7->ZA7_HRLIB  := TIME()
							ZA7->(MsUnLock())
							RecLock("TRB",.F.)
								TRB->(dbDelete())
							TRB->(MsUnLock())
							nCOnt++													        		        				        														
						ELSE
							MsgInfo("Erro no processo automatico","Verifique Help")
							MostraErro()
						ENDIF   
						
						lMSErroAuto	:= .F.
					Else 
						RecLock("ZA7",.F.)						
							ZA7->ZA7_STATUS	:= "3"
							ZA7->ZA7_CODLIB	:= cCodUse
							ZA7->ZA7_USELIB	:= USRRETNAME(RETCODUSR())
							ZA7->ZA7_DTLIB	:= Date()
							ZA7->ZA7_HRLIB  := TIME()
						ZA7->(MsUnLock())						
						
						RecLock("TRB",.F.)
							TRB->(dbDelete())
						TRB->(MsUnLock())
						nCOnt++													        		        				        
				    ENDIF			        			                           
				    	aAuxEv		:= {} 
				    	aRatEz		:= {}	
						aRatEvEz	:= {}
						aCab		:= {}
						lPrimeira   := .T.
						SE2->(dbCloseArea())
						SEV->(dbCloseArea())
						SEZ->(dbCloseArea())
						ZA9->(dbCloseArea())																											
			ENDIF			
			IF ZA7->ZA7_TIPO == "PA "
				dDataBase := Old_dDataB
			ENDIF
		ENDIF 
		*---------------------
		cFilAnt	:= _cFilAntOLD
		*---------------------				            
	TRB->(dbSkip())
	ENDDO
End Transaction

IF nCont > 0
	MsgInfo("Realizado com sucesso autoriza��o financeira de ("+Alltrim(Str(nCont))+") P.F","Autoriza��o em Lote OK")
ELSE
	MsgInfo("Nenhum Registro selecionado para Autoriza��o","Autoriza��o em Lote")
ENDIF 
MarkBRefresh()
oMark:oBrowse:Gotop()
oMark:oBrowse:nat := 1 
oMark:oBrowse:Refresh()

Return lRet           



*---------------------
User Function RejFin()
*---------------------
Local cMsg	:= ""    
Local nTotal:= 0
Local nCont	:= 0
	dbSelectArea("TRB")
	TRB->(dbGotop())
	While !TRB->(Eof())
		IF !Empty(Alltrim(TRB->ZA7_OK))        
	    	nTotal	+= TRB->ZA7_VALOR
	    	nCont ++
	    ENDIF
	TRB->(dbSkip())
	ENDDO
cMsg	:= "REJEITAR ("+Alltrim(Str(nCont))+") PEDIDO(S) FINANCEIRO(S) ? "+CHR(13)+CHR(10)
cMsg	+= "TOTAL >>> R$"+Alltrim(Transform(nTotal,"@E 999,999,999,999.99"))+"<<<"+CHR(13)+CHR(10)
cMsg	+= "("+Alltrim(Extenso(nTotal))+")"
	

IF MsgYesNo(cMsg,"Confirma Rejei��o Financeira ?")
	Processa( {|| u_BlSPFIN() }, "Bloqueando..." ) 
ENDIF
	
Return 

*---------------------
User Function BlSPFIN()
*---------------------
Local cMotivo	:= space(03)
Local cDescMot	:= space(30)
Local cDescCC	:= ""               	
Local cDescNat  := ""   
Local cEmail	:= ""
Local cStatus	:= ""
Local aCabec    := {}
Local aAprv     := {}
Local nOpcTp	:= 0
Local oDlg      

*--------------------------------------------------------------------------------------------------------------------------
DEFINE MSDIALOG oDlg TITLE OemtoAnsi("Informe o motivo da rejei��o para o(s) PF marcado(s)") FROM  356,330 TO 465,740 PIXEL  
@ 8,05 SAY OemToAnsi("Motivo ? ") SIZE 60, 8 OF oDlg PIXEL
@ 8,30 MSGET  cMotivo PICTURE "@!" F3 "ZA6" SIZE 20,9   When .T. valid !Empty(Alltrim(cMotivo)) OF oDlg PIXEL  
@ 8,60 MSGET cDescMot PICTURE "@!"  SIZE 140,9   When .F. OF oDlg PIXEL   
DEFINE SBUTTON FROM 25, 83  TYPE 1 ACTION ( cMotivo:=cMotivo,oDlg:End(), nOpcTp := 1 ) ENABLE OF oDlg
DEFINE SBUTTON FROM 25, 113 TYPE 2 ACTION ( oDlg:End(), nOpcTp := 2) ENABLE OF oDlg
ACTIVATE MSDIALOG oDlg  
*--------------------------------------------------------------------------------------------------------------------------

IF nOpcTp == 1 
	dbSelectArea("TRB")
	TRB->(dbGotop())
	ProcRegua(TRB->(RecCount())) //QTOS REGISTROS LER
		
	Begin Transaction   
		While !TRB->(Eof())
			IF !Empty(Alltrim(TRB->ZA7_OK))        
		        IncProc("P.F "+TRB->ZA7_NUM)
		        dbSelectArea("ZA7")
		        ZA7->(dbSetOrder(2)) //ZA7_FILIAL+ZA7_NUM    
		        ZA7->(dbGotop())
		        IF ZA7->(dbSeek(TRB->ZA7_FILIAL+TRB->ZA7_NUM)) 
					RecLock("ZA7",.F.)
						ZA7->ZA7_STATUS := '5'
						ZA7->ZA7_MSBLQL	:= '1'			
						ZA7->ZA7_DESCRI	:= Alltrim(ZA7->ZA7_DESCRI) + ' Pedido financeiro rejeitado em '+dToc(Date())+ ' �s '+TIME()+' por '+USRRETNAME(RETCODUSR())+' motivo '+cMotivo+' | ' +Alltrim(cDescMot)
						ZA7->ZA7_USEREJ	:= USRRETNAME(RETCODUSR())
						ZA7->ZA7_DTREJ  := DATE()
						ZA7->ZA7_HRREJ  := TIME()
						ZA7->ZA7_CODREJ := cMotivo
				    ZA7->(MsUnLock())   
					*--------------------------------------------------					
					U_EMailPF(ZA7->ZA7_FILIAL,ZA7->ZA7_NUM,.T.,.F.,.T.)
					*--------------------------------------------------										
					RecLock("TRB",.F.)
						TRB->(dbDelete())
					TRB->(MsUnLock()) 		    
				ENDIF
			ENDIF
		TRB->(dbSkip())
	ENDDO	
	End Transaction   
ELSE
	MsgInfo("Opera��o cancelada","Cancelado")
ENDIF
	
Return .T.
