#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "COLORS.CH"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"

// Fonte Para Realizar Processo de PA (Pagamento Antecipado) 
// no momento de inclus�o do PF (Pedido Financeiro).
// Francisco Valdeni
// SAS (Sistema Aria de S�).
// 06/06/2016
*----------------------------------------
User Function SASP072(cNumPF,nMoeda,lAdt)
*----------------------------------------
Local aArea	:= GetArea()
Local lRet	:= .T.
Local _cHist:= ""	
dbSelectArea("SA6")
// Chama Fun��o Original-------
	_cHist := FA050PF(cNumPF,nMoeda,lAdt)                        
//-----------------------------


RestArea(aArea)
Return _cHist

*-------------------------------------------
Static Function FA050PF(cTitulo,nMoeda,lAdt)
*------------------------------------------
Local aArea		:= GetArea()
Local oBcoAdt
Local oChqAdt
Local bAction
Local lFA050PA 	:= ExistBlock("FA050PA")
Local nMoedAux	:= nMoeda   
Local oDlgPA	
Local cBancoAdt		:= CriaVar("A6_COD")
Local cAgenciaAdt	:= CriaVar("A6_AGENCIA")
Local cNumCon	 	:= CriaVar("A6_NUMCON")
Local cChequeAdt	:= CriaVar("EF_NUM")
Local cHistor		:= CriaVar("EF_HIST")
Local cBenef		:= CriaVar("EF_BENEF")
Local cPictHist		:= ""

cTitulo := If(cTitulo == Nil, "", cTitulo)
mv_par05 := 1
If cPictHist = Nil
	//��������������������������������������������������������������Ŀ
	//� Recupera a Picture no SX3 para campo historico  			 �
	//����������������������������������������������������������������
	cAlias := Alias()
	dbSelectArea("SX3")
	dbSetOrder(2)
	dbSeek("EF_HIST")
	cPictHist := AllTrim(X3_PICTURE)
	dbSelectArea(cAlias)
Endif	

While .T.
	//������������������������������������������������������Ŀ
	//� Mostra Get do Banco de Entrada						 �
	//��������������������������������������������������������
	nOpca := 0
	DEFINE MSDIALOG oDlgPA FROM 10, 5 TO 26, 60 TITLE OemToAnsi("LOCAL de Entrada" + cTitulo) // "LOCAL de Entrada"
	@	.3,1 TO 07.3,26 OF oDlgPA  
	// BANCO
	@	1.0,2 	Say OemToAnsi("Banco :         ") //"Banco :         "


		@	1.0,8  	MSGET oBcoAdt 			VAR cBancoAdt F3 "SA6" 	Valid CarregaSa6(@cBancoAdt,,,,,,, @nMoedAux ) //.And. FaPrNumChq(cBancoAdt,cAgenciaAdt,cNumCon,"","")


	// AGENCIA
	@	2.0,2 	Say OemToAnsi("Ag�ncia :       ") //"Ag�ncia :       "
	@	2.0,8 	MSGET cAgenciaAdt 								Valid CarregaSa6(@cBancoAdt,@cAgenciaAdt) //.And. FaPrNumChq(cBancoAdt,cAgenciaAdt,cNumCon,"","")
	// CONTA
	@	3.0,2 	Say OemToAnsi("Conta :         ") //"Conta :         "
	@	3.0,8 	MSGET cNumCon 									Valid If(CarregaSa6(@cBancoAdt,@cAgenciaAdt,@cNumCon,,,.T.),/*FaPrNumChq(cBancoAdt,cAgenciaAdt,cNumCon,"","")*/,oBcoAdt:SetFocus())
	// NUMERO CHEQUE
	@	4.0,2 	Say OemToAnsi("Num Cheque :   ") // "N�m Cheque :   "
	@	4.0,8 	MSGET oChqAdt 			VAR cChequeAdt 			When (	.F. /*mv_par05 == 1 .And. substr(cBancoAdt,1,2)!="CX" .And. !(cBancoAdt$GEtMV("MV_CARTEIR"))*/) Valid   .T. // fa050Cheque(cBancoAdt,cAgenciaAdt,cNumCon,cChequeAdt,Iif(cPaisLoc $ "ARG",.F.,.T.))
	// HISTORICO
	@	5.0,2 	Say OemToAnsi("Historico :    ") // "Historico :    "
	@	5.0,8 	MSGET cHistor		Picture cPictHist	SIZE 135, 10 OF oDlgPA
	// BENEFICIARIO
	@	6.0,2 	Say OemToAnsi("Beneficiario : ") // "Beneficiario : "
	@	6.0,8 	MSGET cBenef		Picture "@S40"		SIZE 135, 10 OF oDlgPA
	
   bAction := {||	nOpca:=1,;
   Iif(!Empty(cBancoAdt).And.;
   CarregaSa6(@cBancoAdt,@cAgenciaAdt,@cNumCon,,,.T.).And.;
   Iif(lFA050PA,;
   ExecBlock("FA050PA",.F.,.F.,{cBancoAdt,cAgenciaAdt,cNumCon,cChequeAdt,cHistor,cBenef}),;
   .T.),;
   oDlgPA:End(),;
   nOpca:=0)}
	
	DEFINE SBUTTON FROM 105,180.1 TYPE 1 ACTION ( Eval(bAction) ) ENABLE OF oDlgPA
	ACTIVATE MSDIALOG oDlgPA CENTERED
	IF nOpca != 0
			RecLock("ZA7",.F.)
				ZA7->ZA7_BANCO		:= cBancoAdt
				ZA7->ZA7_AGENCIA  	:= cAgenciaAdt
				ZA7->ZA7_CONTA    	:= cNumCon
			ZA7->(MsUnLock())
		Exit
	EndIf
EndDo
RestArea(aArea)
Return Alltrim(cHistor)