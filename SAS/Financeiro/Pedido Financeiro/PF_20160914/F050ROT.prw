#include "protheus.ch"

// Francisco Valdeni 
// Impress�o do Pedido Financeiro
// Rotina de Contas a Pagar 

*--------------------
User Function F050ROT
*--------------------              
Local aRotina := ParamIxb
	AAdd( aRotina, { "Imp P.F", "U_PF050()", 0, 8,, .F. } )
Return aRotina                                        

*-------------------
User Function PF050()          
*-------------------
IF ALLTRIM(SE2->E2_ORIGEM) == "SASP071"
	U_SASP066(SE2->E2_NUM,"","",SE2->E2_FILIAL,.F.)
ELSE 
	MsgInfo("Op��o dispon�vel apenas para t�tulos gerado pela rotina Pedido Financeiro","T�tulo n�o e PF")
ENDIF

Return .T.