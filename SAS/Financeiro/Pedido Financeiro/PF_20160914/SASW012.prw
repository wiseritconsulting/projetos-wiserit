#Include 'Protheus.ch'
#include 'rwmake.ch'
#include "tbiconn.ch"

#define PF_NUM 			01
#define PF_TIPO 		02
#define PF_MESREF 		03
#define PF_SOLICITANTE 	04
#define PF_EMISSAO 		05
#define PF_VENCIMENTO 	06
#define PF_DESC 		07
#define PF_FORNECEDOR	08


User Function SASW012(__aCookies,__aPostParms,__nProcID,__aProcParms,__cHTTPPage)
	Local cRet := ""
	
	Local aPf := {}
	Local aNatCC := {}
	
	Private _aDados := strtokarr(__aProcParms[1,2],';')
	Private _cFil := _aDados[1]
	Private _cNumPF := _aDados[2]
	
	
	
	PREPARE ENVIRONMENT EMPRESA "01" FILIAL "01" MODULO "FIN"
	
	cFilAnt := _aDados[1]
	
	aPF := getDadosPF(_cFil, _cNumPF)
	aNatCC := getItensPF(_cFil, _cNumPF)
	
	cRet += ' <html>     '
	cRet += ' <head>     '
	cRet += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
	cRet += ' </head>     '
	cRet += ' <body>     '
	cRet += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '
	cRet += '  '
	cRet += ' <table class="responstable" style="margin: 1em 0;width: 100%;overflow: hidden;background: #FFF;color: #024457;border-radius: 10px;border: 1px solid #167F92;">    <tr style="border: 1px solid #D9E4E6;">       '
	cRet += ' 	<tr style="border: 1px solid #D9E4E6;">           '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Pedido Financeiro</th>            '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Tipo</th>     '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">M&ecirc;s de refer&ecirc;ncia</th>       '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Solicitante</th>            '
	cRet += ' 	</tr>      '
	cRet += ' 	<tr style="border: 1px solid #D9E4E6;">           '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aPF[PF_NUM]+'</td>       '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aPF[PF_TIPO]+'</td>       '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aPF[PF_MESREF]+'</td>   '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aPF[PF_SOLICITANTE]+'</td>       '
	cRet += ' 	</tr> '
	cRet += ' 	<tr style="border: 1px solid #D9E4E6;">           '
	cRet += ' 		<th colspan="2" style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Fornecedor</th>     '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Emiss&atilde;o</th>     '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Vencimento</th>       '
	cRet += ' 	</tr>      '
	cRet += ' 	<tr style="border: 1px solid #D9E4E6;">           '
	cRet += ' 		<td colspan="2" style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aPF[PF_FORNECEDOR]+'</td>       '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aPF[PF_EMISSAO]+'</td>       '
	cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aPF[PF_VENCIMENTO] +'</td>   '
	cRet += ' 	</tr> '
	cRet += ' </table>  '
	cRet += '     '
	cRet += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
	cRet += ' 	<h3>Descri&ccedil;&atilde;o</h3>  		 '
	//cRet += ' 	<center>  		 '
	cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;"> '
	cRet += ' 			<span class="sas-status-approved" style="color: #DB9600;font-family: Verdana;font-weight: bold;font-size:14px;">'+NoAcento(aPF[PF_DESC])+'</span> '
	cRet += ' 		</h1>  	 '
	//cRet += ' 	</center>   '
	cRet += ' </div>  '
	cRet += '     '
	
	cRet += ' <table class="responstable" style="margin: 1em 0;width: 100%;overflow: hidden;background: #FFF;color: #024457;border-radius: 10px;border: 1px solid #167F92;">    <tr style="border: 1px solid #D9E4E6;">       '
	cRet += ' 	<tr style="border: 1px solid #D9E4E6;">           '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Natureza</th>            '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Conta Cont&aacute;bil</th>     '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">C. Custo</th>       '
	cRet += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Valor</th>       '
	cRet += ' 	</tr>      '
	for i:= 1 to len(aNatCC)
		cRet += ' 	<tr style="border: 1px solid #D9E4E6;">           '
		cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aNatCC[i][1]+'</td>       '
		cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aNatCC[i][2]+'</td>       '
		cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aNatCC[i][3]+'</td>   '
		cRet += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aNatCC[i][4]+'</td>   '
		cRet += ' 	</tr> '
	Next i
	cRet += ' </table>  '
	cRet += ' <br>    '
	cRet += ' </body>     '
	cRet += ' </html>     '
	
Return cRet

Static Function getNomeMes(cMes)
	Local aMeses := {}
	Local nMes := Val(cMes)
	
	AADD(aMeses, "Janeiro")
	AADD(aMeses, "Fevereiro")
	AADD(aMeses, "Mar�o")
	AADD(aMeses, "Abril")
	AADD(aMeses, "Maio")
	AADD(aMeses, "Junho")
	AADD(aMeses, "Julho")
	AADD(aMeses, "Agosto")
	AADD(aMeses, "Setembro")
	AADD(aMeses, "Outubro")
	AADD(aMeses, "Novembro")
	AADD(aMeses, "Dezembro")
Return aMeses[nMes]

Static Function getDadosPF(_cFil, cNum)
	Local aRet := {}
	Local aMes := {}
	Local nLinhas := 0
	Local cDesc := "" 
	
	dbSelectArea("ZA7")
	dbSetOrder(2)
	dbSeek(_cFil + cNum)
	
	AADD(aRet, cNum)
	AADD(aRet, ZA7->ZA7_TIPO)
	AADD(aRet, ZA7->ZA7_MESREF + ' - ' + getNomeMes(ZA7->ZA7_MESREF))
	AADD(aRet, ZA7->ZA7_USECAD)
	AADD(aRet, DtoC(ZA7->ZA7_EMISSA))
	AADD(aRet, DtoC(ZA7->ZA7_VENCTO))
	
	nLinhas	:= MlCount(Alltrim(ZA7->ZA7_DESCRI),150)
	For i:=1 To nLinhas
		cDesc += MEMOLINE(ZA7->ZA7_DESCRI,150,I)
	Next i
	
	AADD(aRet, cDesc)
	AADD(aRet, ZA7->ZA7_FORNEC + "/" + ZA7->ZA7_LOJA + " - " + POSICIONE("SA2",1,xFilial("SA2")+ZA7->ZA7_FORNEC+ ZA7->ZA7_LOJA, "SA2->A2_NOME"))
	
	ZA7->(dbCloseArea())
Return aRet

Static Function getItensPF(_cFil, cNum)
	Local aRet := {}
	Local aAux := {}
	Local cSql := ""
	
	Local cCC := ""
	Local cConta := ""
	Local cValor := ""
	
	Local cAuxNat := "" 
	Local cContaAux := ""
	
	cSQL += " SELECT  "
	cSQL += " 	* "
	cSQL += " FROM "+RetSqlName("ZA9")+" ZA9 "
	cSQL += " WHERE D_E_L_E_T_ = '' "
	cSQL += " AND ZA9_NUM = '"+cNum+"' "
	cSQL += " AND ZA9_FILIAL = '"+_cFil+"' "
	cSQL += " ORDER BY  "
	cSQL += " 	ZA9_FILIAL, "
	cSQL += " 	ZA9_PREFIX, "
	cSQL += " 	ZA9_NUM, "
	cSQL += " 	ZA9_NATURE "
	
	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSQL), "QRYNAT", .F., .T.)
	While QRYNAT->(!EOF())
		if QRYNAT->ZA9_NATCC == 'N'
			AADD(aRet, {cAuxNat, cConta, cCC, cValor})
			cAuxNat := QRYNAT->ZA9_NATURE + " - " + POSICIONE("SED",1,xFilial("SED")+QRYNAT->ZA9_NATURE,"SED->ED_DESCRIC")
			cCC := ""
			cConta := ""
			cValor := ""
			QRYNAT->(dbSkip())
		EndIF
		
		cCC += QRYNAT->ZA9_CUSTO + " - " + POSICIONE("CTT",1,xFilial("CTT")+QRYNAT->ZA9_CUSTO,"CTT->CTT_DESC01") + '<br/>'
		
		IF SUBSTR(QRYNAT->ZA9_CUSTO,1,1) == "1"
			cContaAux := Posicione("SED",1,xFilial("SED")+cAuxNat,"ED_CONTA")
		ELSEIF	SUBSTR(QRYNAT->ZA9_CUSTO,1,1) == "2"
			cContaAux := Posicione("SED",1,xFilial("SED")+cAuxNat,"ED_YCTCUST")
		ELSEIF	SUBSTR(QRYNAT->ZA9_CUSTO,1,1) == "3"
			cContaAux := Posicione("SED",1,xFilial("SED")+cAuxNat,"ED_YDESVEN")
		ENDIF
		
		cConta += cContaAux + " - " + POSICIONE("CT1",1,xFilial("CT1")+cContaAux, "CT1->CT1_DESC01") + '<br/>'
		cValor += Transform(QRYNAT->ZA9_VALOR,PesqPict("ZA9", "ZA9_VALOR")) + '<br/>'

		QRYNAT->(dbSkip())
	EndDo
	
	AADD(aRet, {cAuxNat, cConta, cCC, cValor})
	
	QRYNAT->(dbCloseArea())
Return aRet