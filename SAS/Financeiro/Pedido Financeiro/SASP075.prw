#Include 'Protheus.ch'

//Rejeita Pedido Financeiro via WorkFlow
User Function SASP075(_cFil, _cNumPF, _cCodUse, _cNivelUsr, _nValor, _cMotivo, _cDescMot)
	Local cMotivo	:= _cMotivo//space(03)
	Local cDescMot	:= _cDescMot//space(30)
	Local cDescCC	:= ""
	Local cDescNat  := ""
	Local cEmail	:= ""
	Local _cUsr		:= ""
	Local _cNivel	:= ""
	Local cStatus	:= ""
	Local aCabec    := {}
	Local aCC	    := {}
	Local aAprv     := {}
	Local nOpcTp	:= 0
	Local oDlg
	
	dbSelectArea("ZA7")
	ZA7->(dbSetOrder(2)) //ZA7_FILIAL+ZA7_NUM
	ZA7->(dbGotop())
	IF ZA7->(dbSeek(_cFil+_cNumPF))
        *-----------------------------------------------------------------------------------------------------------------
		cDescCC	:= ALLTRIM(ZA7->ZA7_CUSTO) +" - "+ ALLTRIM(Posicione("CTT",1,xFilial("CTT")+ZA7->ZA7_CUSTO,"CTT_DESC01"))
		cDescNat:= ALLTRIM(ZA7->ZA7_NATURE)+" - "+ ALLTRIM(ZA7->ZA7_NOMNAT)
		cEmail 	:= cEmail := Alltrim(UsrRetMail(ZA7->ZA7_CODUSE))+";"
		aCabec :={ZA7->ZA7_NUM,; 						// [01] Numero
		TRANSFORM(ZA7->ZA7_VALOR, "@E 999,999,999.99"),;// [02] Valor
		FWFilialName(cEmpAnt,ZA7->ZA7_FILIAL,1),;		// [03] Nome da Filial
		cDescCC,;										// [04] Centro de Custo
		cDescNat,;										// [05] Descri��o Natureza
		ZA7->ZA7_USECAD,;								// [06] Usuario Solicitante
		DTOC(ZA7->ZA7_DTCAD),;							// [07] Data da Solicita��o PF
		u_getPriori(ZA7->ZA7_VENCTO)} 	     			// [08] Prioridade
        *-----------------------------------------------------------------------------------------------------------------		        
		RecLock("ZA7",.F.)
			ZA7->ZA7_STATUS := '4'
			ZA7->ZA7_MSBLQL	:= '1'
			ZA7->ZA7_DESCRI	:= Alltrim(ZA7->ZA7_DESCRI) + ' - Pedido financeiro rejeitado em '+dToc(Date())+ ' �s '+TIME()+' por '+USRRETNAME(_cCodUse)+' motivo '+cMotivo+' | ' +Alltrim(cDescMot)
			ZA7->ZA7_USEREJ	:= USRRETNAME(_cCodUse)
			ZA7->ZA7_DTREJ  := DATE()
			ZA7->ZA7_HRREJ  := TIME()
			ZA7->ZA7_CODREJ := cMotivo
		ZA7->(MsUnLock())
	    *--------------------------------------------------
		U_EMailPF(ZA7->ZA7_FILIAL,ZA7->ZA7_NUM,.T.,.F.,.T.)
	    *--------------------------------------------------	
		dbselectArea("ZA8")
		ZA8->(dbSetOrder(1)) // Filial + NumSP
		ZA8->(dbGotop())
		IF ZA8->(dbSeek(_cFil+_cNumPF))
			WHILE !ZA8->(EOF()) .AND. ZA8->ZA8_NUMSP == _cNumPF
				IF !Empty(Alltrim(UsrRetMail(alltrim(ZA8->ZA8_USER)))) // Verifica se o aprovador tem e-mail no cadastro.
					cEmail += Alltrim(UsrRetMail(alltrim(ZA8->ZA8_USER)))+";"
				ENDIF
				IF alltrim(ZA8->ZA8_USER) == alltrim(_cCodUse)
					AADD(aAprv,{"RJ",ZA8->ZA8_NOME,DTOC(ZA8->ZA8_DTAPRO),ZA8->ZA8_NIVEL})
					RECLOCK("ZA8",.F.)
						ZA8->ZA8_STATUS := "RJ"
						ZA8->ZA8_CODREJ	:= cMotivo
						ZA8->ZA8_OBS	:= cDescMot
						ZA8->ZA8_HRREJ	:= TIME()
						ZA8->ZA8_DTREJ	:= DATE()
					ZA8->(MsUnLock())
				ELSE
					AADD(aAprv,{ZA8->ZA8_STATUS,ZA8->ZA8_NOME,DTOC(ZA8->ZA8_DTAPRO),ZA8->ZA8_NIVEL})
					RECLOCK("ZA8",.F.)
					ZA8->ZA8_STATUS := "RJ"
					ZA8->ZA8_CODREJ	:= cMotivo
					ZA8->ZA8_OBS	:= cDescMot
					ZA8->(MsUnLock())
				ENDIF
				ZA8->(dbSkip())
			Enddo

		ENDIF
	ENDIF
Return


/*/{Protheus.doc} updCC
Atualiza Centro de Custo de Aprova��o
/*/
static function updCC(cFilCli, cNumSP, cCodUsr, cNivel, cCodAprovador)
	Local cSQL := ""
	Local aArea := getArea()

	cSQL += " SELECT SAL.AL_APROV, SAL.AL_NIVEL, SAL.AL_USER  "
	cSQL += " FROM " + RetSqlName("ZA9") + " ZA9, "
	cSQL += " 	" + RetSqlName("SAL") + " SAL, "
	cSQL += " 	" + RetSqlName("CTT") + " CTT "
	cSQL += " WHERE 1=1 "
	cSQL += " AND ZA9.D_E_L_E_T_ = '' "
	cSQL += " AND CTT.D_E_L_E_T_ = '' "
	cSQL += " AND SAL.D_E_L_E_T_ = '' "
	cSQL += " AND ZA9.ZA9_NATCC = 'C' "
	cSQL += " AND ZA9.ZA9_FILIAL = '"+cFilCli+"' "
	cSQL += " AND ZA9.ZA9_CUSTO = CTT.CTT_CUSTO "
	cSQL += " AND CTT.CTT_YAPRSP = SAL.AL_COD "
	cSQL += " AND ZA9.ZA9_NUM = '"+cNumSP+"' "
	cSQL += " AND SAL.AL_NIVEL = '"+cNivel+"' "
	cSQL += " AND ZA9.ZA9_CUSTO IN ( "
	cSQL += " 	SELECT A.ZA9_CUSTO "
	cSQL += " 	FROM " + RetSqlName("ZA9") + " A, "
	cSQL += " 		" + RetSqlName("SAL") + " B, "
	cSQL += " 		" + RetSqlName("CTT") + " C "
	cSQL += " 	WHERE 1=1 "
	cSQL += " 	AND A.D_E_L_E_T_ = '' "
	cSQL += " 	AND C.D_E_L_E_T_ = '' "
	cSQL += " 	AND B.D_E_L_E_T_ = '' "
	cSQL += " 	AND A.ZA9_NATCC = 'C' "
	cSQL += " 	AND ZA9.ZA9_FILIAL = '"+cFilCli+"' "
	cSQL += " 	AND A.ZA9_CUSTO = C.CTT_CUSTO "
	cSQL += " 	AND C.CTT_YAPRSP = B.AL_COD "
	cSQL += " 	AND A.ZA9_NUM = '"+cNumSP+"' "
	cSQL += " 	AND B.AL_NIVEL = '"+cNivel+"' "
	cSQL += " 	AND B.AL_USER = '"+cCodUsr+"' "
	cSQL += " ) "

	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSQL), "QRYCC", .F., .T.)

	While QRYCC->(!EOF())
		dbSelectArea("ZA8")
		dbSetOrder(1)
		dbSeek(xFilial("ZA8")+cNumSP+QRYCC->AL_NIVEL)
		If found()
			While !ZA8->(Eof()) .AND. ZA8->ZA8_NUMSP == cNumSP
				if ZA8->ZA8_STATUS <> "OK" .and. ZA8->ZA8_USER == QRYCC->AL_USER
					IF ZA8->ZA8_NIVEL == cNivel // Ajuste Nivel Valdeni
						RecLock("ZA8", .F.)
							ZA8->ZA8_DTAPRO	:= DATE()
							ZA8->ZA8_HRAPRO	:= TIME()
							ZA8->ZA8_STATUS := "OK"
							ZA8->ZA8_APRNIV := cCodAprovador
						ZA8->(MsUnLock())
					ENDIF					
				EndIf
				ZA8->(dbSkip())
			Enddo
		EndIf
		ZA8->(dbCLoseArea())
		QRYCC->(dbSkip())
	EndDo
	QRYCC->(dbCloseArea())

	RestArea(aArea)
return cSQL