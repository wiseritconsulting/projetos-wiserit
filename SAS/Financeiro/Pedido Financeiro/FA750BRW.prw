/*#include "protheus.ch"
// Francisco Valdeni
// Sistema Ari de S� [SAS]
// Cria bot�o na tela (Fun��es do contas a Pagar)
// Impress�o do pedido financeiro

*-----------------------
User Function F050ROT() // Contas a Pagar
*-----------------------
Local aRet		:= {}
Local aArea		:= GetArea()
    *-------------------------------------------------------------------------------------------------
	aAdd(aRet, {'Imp. Pedido Financeiro',"U_IMPPFCta(SE2->E2_FILIAL,SE2->E2_NUM,SE2->E2_ORIGEM)",0,10}) 
    *-------------------------------------------------------------------------------------------------
RestArea(aArea)
Return aRet


*-----------------------
User Function FA750BRW() //Fun��es do contas a pagar. 
*-----------------------
Local aRet		:= {}
Local aArea		:= GetArea()
    *-------------------------------------------------------------------------------------------------
	aAdd(aRet, {'Imp. Pedido Financeiro',"U_IMPPFCta(SE2->E2_FILIAL,SE2->E2_NUM,SE2->E2_ORIGEM)",0,10}) 
    *-------------------------------------------------------------------------------------------------
RestArea(aArea)
Return aRet

*----------------------------------------------
User Function IMPPFCta(_cFilial,_cNum,_cOrigem)
*----------------------------------------------
Local lTlConf := .F. // N�o apresenta tela inicial de impress�o
	IF Alltrim(_cOrigem) == "SASP071" // Rotina de Pedido Financeiro.
		*-------------------------------	-------
		U_SASP066(_cNum,"","",_cFilial,lTlConf)
		*--------------------------------------
	ELSE
		MsgInfo("Op��o dispon�vel apenas para t�tulos gerado atrav�s da rotina [Pedido Financeiro - PF]","T�tulo gerado por outra rotina")
	ENDIF
Return
*/