#Include 'Protheus.ch'
#include 'rwmake.ch'
#include "tbiconn.ch"

#define DATA_FILIAL 	01
#define DATA_NUMPF		02
#define DATA_USER		03
#define DATA_NIVEL		04
#define DATA_VALOR		05
#define DATA_TP_APROV	06

User Function SASW011(__aCookies,__aPostParms,__nProcID,__aProcParms,__cHTTPPage)
	Local cHTML := ''

	PREPARE ENVIRONMENT EMPRESA "01" FILIAL "01" MODULO "FIN"
	
	nLinha := embaralha(__aProcParms[2,2], 1)
	aDados := StrTokArr(nLinha,";")
	cFilAnt := aDados[1]
	u_SASP075(	aDados[DATA_FILIAL], ;
				aDados[DATA_NUMPF], ;
				aDados[DATA_USER], ;
				aDados[DATA_NIVEL], ;
				Val(aDados[DATA_VALOR]),;
				__aProcParms[1,2],;
				POSICIONE("ZA6",1,xFilial("ZA6")+__aProcParms[1,2], "ZA6->ZA6_MOTIVO"))
	
	cHTML += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
	cHTML += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '
	cHTML += '  '
	cHTML += ' <table class="responstable" style="margin: 1em 0;width: 100%;overflow: hidden;background: #FFF;color: #024457;border-radius: 10px;border: 1px solid #167F92;">    <tr style="border: 1px solid #D9E4E6;">       '
	cHTML += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Pedido Financeiro</th>            '
	cHTML += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Usuario</th>     '
	cHTML += ' 		<th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Data - Hora</th>       '
	cHTML += ' 	</tr>      '
	cHTML += ' 	<tr style="border: 1px solid #D9E4E6;">           '
	cHTML += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aDados[DATA_NUMPF]+'</td>       '
	cHTML += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+aDados[DATA_USER]+'</td>       '
	cHTML += ' 		<td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ DtoC(Date()) +' - '+ Time() +'</td>   '
	cHTML += ' 	</tr> '
	cHTML += ' </table>  '
	cHTML += '     '
	cHTML += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
	cHTML += ' 	<center>  		 '
	cHTML += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;"> '
	cHTML += ' 			<span class="sas-status-approved" style="color: #FF0000;font-family: Verdana;font-weight: bold;">Pedido Financeiro Rejeitado!</span> '
	cHTML += ' 		</h1>  	 '
	cHTML += ' 	</center>   '
	cHTML += ' </div>  '
	cHTML += '  '
	cHTML += ' <br>  '  
Return cHTML