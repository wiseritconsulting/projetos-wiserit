#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"      
#INCLUDE "RWMAKE.CH"

/*

�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa �RFINR07   � Autor � JOAO FILHO	      � Data � 26/05/15       ���
�������������������������������������������������������������������������͹��
���Descricao � IMPRESS�O DA STATUS DAS LISTA DE PRESENTES                 ���
���          �                               							  ���
�������������������������������������������������������������������������͹��
���Uso       � Financeiro                                                 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
//mconsult	
*/

User Function RFINR08()
	
	Local oReport
	Local cData  
	Local nTam
	Private cPerg	:= "RFINR08"
	
	//mconCriando as Perguntas (Parametros)
	AjustaSX1(cPerg)                   
	
	//Exibindo as Perguntas criadas
	Pergunte(cPerg,.T.)

	oReport := ReportDef()
	oReport:PrintDialog()
	
Return

Static Function ReportDef()
    
	Local oReport
	Local oSection 
	
	//Cria��o do Objeto oReport da classe TREPORT./	
	oReport := TReport():New(cPerg,"Status de Lista",cPerg,{|oReport| PrintReport(oReport)},"Status de Lista") 
	
	//Define orienta��o de p�gina do relat�rio como paisagem.
	oReport:SetPortrait()//oReport:SetLandscape()     
	oSection:= TRSection():New(oReport,"Listas",{},/*aOrdem*/)
	TRCell():New(oSection,"codLista" ,"","Cod Lista","@!",6) 
	TRCell():New(oSection,"nomeCliente" ,"","Nome Cliente","@!",40) 
	TRCell():New(oSection,"atores" ,"","Atores","@! ",30) 
	TRCell():New(oSection,"dtevento" ,"","Data Evento","@!",10) 
	TRCell():New(oSection,"dtinic" ,"","Data Dispon","@!",10)
	TRCell():New(oSection,"dtvalid" ,"","Data Validade","@!",10)
	TRCell():New(oSection,"status" ,"","Status","@!",10)
			
Return oReport

Static Function PrintReport(oReport)
  
    Local nTipo
    Local nAte 
    Local cNome := ""
    Local cAtores := "" 
	Local oSection   := oReport:Section(1)  
	//Local oSection2  := oReport:Section(2)
	//Local oSection3  := oReport:Section(3)
	//Local cCodClide    := mv_par01
	//Local dDtEvento  := mv_par02
	//Local cTpEvento  := mv_par03

    
//========================================================================//
//  REALCIONANDO TABELAS ME1(LISTAS) ,SA1(CLIENTES),  MEE(ATORES)         //
//========================================================================//   

cQuery := " SELECT ME1_CODIGO , ME1_CODCLI , A1_NOME, ME1_DTEVEN , ME1_DTDISP , ME1_DTVAL , STATUS "  
cQuery += " FROM ( "  
cQuery += " SELECT ME1_CODIGO , ME1_CODCLI , A1_NOME, ME1_DTEVEN , ME1_DTDISP , ME1_DTVAL, "  
cQuery += " CASE WHEN '"+DTOS(DDATABASE)+"' <= ME1_DTVAL  THEN '2' WHEN '"+DTOS(DDATABASE)+"' > ME1_DTVAL THEN '1' END AS 'STATUS' "
cQuery += " FROM "+RETSQLNAME("ME1")+" ME1 "
cQuery += " INNER JOIN "+RETSQLNAME("SA1")+" SA1 ON ME1_CODCLI = A1_COD AND SA1.D_E_L_E_T_='' "
cQuery += " WHERE ME1.D_E_L_E_T_=''	AND " 
cQuery += " ME1_CODIGO BETWEEN '"+mv_par03+"' AND '"+mv_par04+"' AND " 
cQuery += " ME1_DTVAL  BETWEEN '"+DTOS(mv_par01)+"' AND '"+DTOS(mv_par02)+"' "
IF MV_PAR05 == 1
	cQuery += " ) A  WHERE STATUS = '1'  ORDER BY ME1_DTVAL ,ME1_CODIGO "
ELSEIF MV_PAR05 == 2
	cQuery += " ) A  WHERE STATUS = '2' ORDER BY ME1_DTVAL ,ME1_CODIGO "
ELSEIF MV_PAR05 == 3
	cQuery += " ) A  ORDER BY ME1_DTVAL ,ME1_CODIGO "
ENDIF

TcQuery cQuery New Alias T01

WHILE !T01->(EOF())

	oSection:Init()    
				oSection:Cell("codLista"):SetValue(T01->ME1_CODIGO)
				oSection:Cell("nomeCliente"):SetValue(T01->A1_NOME)			
				
					cQuery := " SELECT MEE_NOME "
					cQuery += " FROM "+RETSQLNAME("MEE")+" MEE "
					cQuery += " WHERE MEE.D_E_L_E_T_='' AND "
					cQuery += " MEE_CODLIS = '"+T01->ME1_CODIGO+"' "
					TcQuery cQuery New Alias T02
					WHILE !T02->(EOF())
					 nAte    := at(" ",T02->MEE_NOME)
					 cNome   := substr(T02->MEE_NOME,1,nAte-1)
					 cAtores += cNome+" /"
					 T02->(DBSKIP())	
					ENDDO
					T02->(DbCloseArea())
				oSection:Cell("atores"):SetValue(ALLTRIM(substr(cAtores,1,len(cAtores)-1)))
				cAtores := ""
				oSection:Cell("dtevento"):SetValue(STOD(T01->ME1_DTEVEN))
				oSection:Cell("dtinic"):SetValue(STOD(T01->ME1_DTDISP))			
				oSection:Cell("dtvalid"):SetValue(STOD(T01->ME1_DTVAL))
				oSection:Cell("status"):SetValue(IIF(T01->STATUS=='1','VENCIDA','� VENCER'))			
				oSection:PrintLine()
				
				//Incrementando barra de progresso
				oReport:IncMeter()
							
				//Verifica se o Usuario clicou no bot�o cancelar
				If oReport:Cancel()
					Exit
				EndIf
	T01->(DBSKIP())	
ENDDO

oSection:Finish() 
T01->(DbCloseArea())
                        

Return

Static Function AjustaSX1(cPerg)

	aHelpPor	:= {}
	aHelpEng	:= {}
	aHelpSpa	:= {}
	
	Aadd( aHelpPor, "")
	Aadd( aHelpEng, "")
	Aadd( aHelpSpa, "")                                                                     
	
           
	PutSx1(cPerg, "01","Dt Vencto de","Data de","Data de","mv_ch1","D",10,0,0,"G","","","","",;
	"mv_par01"," ","","","","","","","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)
	
	PutSx1(cPerg, "02","Dt Vencto at�","Data at�","Data at�","mv_ch2","D",10,0,0,"G","","","","",;
	"mv_par02"," ","","","","","","","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)
	
  	PutSx1(cPerg, "03","Lista de",                "","","mv_ch3","",6,0,0,"G","","ME1C","","",;
	"mv_par03"," ","","","","","","","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)
	
	PutSx1(cPerg, "04","Lista at�",                     "","","mv_ch4","",6,0,0,"G","","ME1C","","",;
	"mv_par04"," ","","","","","","","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)
	
	PutSx1(cPerg, "05","Status",                     "","","mv_ch5","C",10,0,0,"C","","","","",;
	"mv_par05","Vencida","","","","� Vencer","","","Ambos","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)
	

Return                    
