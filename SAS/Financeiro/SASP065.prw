// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : SASP065
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor  Rog�rio J�come           | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 29/05/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo
Montagem da tela de processamento

@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     29/05/2016
/*/
//------------------------------------------------------------------------------------------

User Function SASP065(_cBloq,_cOBS,_dData,_cRisco)

	Local aArea 	:= GetArea()
	Local oObj
	Local cBloqueio := SA1->A1_MSBLQL 
	Local cMun		:= SA1->A1_MUNE
	oWS	:= WSSASWebServices():New() // instacia um objeto WSServidor, a declara��o deste objeto est� no fonte ProtheusLegadoFaturamento_Client.prw
	oWS:cTOKEN		:= "ebec2329-1c4b-4f1e-9dfa-ec56fe2986be"
	oWS:cCGUID		:= Alltrim(SA1->A1_YGUID)
	oWS:cCNOME		:= Alltrim(SA1->A1_NOME)
	oWS:cCPESSOA	:= Alltrim(SA1->A1_PESSOA)
	oWS:cCLOJA		:= Alltrim(SA1->A1_LOJA)
	oWS:cCNREDUZ	:= Alltrim(SA1->A1_NREDUZ)
	oWS:cCEND		:= Alltrim(SA1->A1_YEND)
	oWS:cCNUMEND		:= Alltrim(SA1->A1_YNUM)
	oWS:cCNENDENTR		:= Alltrim(SA1->A1_YNUMENT)
	oWS:cCCOMPLE		:= Alltrim(SA1->A1_COMPLEM)
	oWS:cCTIPO		:= Alltrim(SA1->A1_TIPO)
	oWS:cCEST		:= Alltrim(SA1->A1_EST)
	oWS:cCCODMUN	:= Alltrim(SA1->A1_COD_MUN)
	oWS:cCMUN		:= Alltrim(SA1->A1_MUN)
	oWS:cCBAIRRO	:= Alltrim(SA1->A1_BAIRRO)
	oWS:cCCEP		:= Alltrim(SA1->A1_CEP)
	oWS:cCPAIS		:= Alltrim(SA1->A1_PAIS)
	oWS:cCCGC		:= Alltrim(SA1->A1_CGC)
	oWS:cCEMAIL		:= Alltrim(SA1->A1_EMAIL)
	oWS:cCTEL		:= Alltrim(SA1->A1_TEL)
	oWS:cCCODPAIS	:= Alltrim(SA1->A1_PAIS)
	oWS:cCYCODTCF	:= Alltrim(SA1->A1_YCODTCF)
	oWS:cCYCODGLO	:= Alltrim(SA1->A1_YCODGLO)
	oWS:cCBLOCL		:= IIF(empty(_cBloq),Alltrim(SA1->A1_YBLOQ),_cBloq)
	oWS:cCTPPORT	:= Alltrim(SA1->A1_YPORTAL)
	oWS:cCBAIRROC	:= Alltrim(SA1->A1_BAIRROC)
	oWS:cCBAIRROE	:= Alltrim(SA1->A1_BAIRROE)
	//oWS:cCDATABLOQ	:= IIF(empty(_dData),STOD(""),_dData)
	oWS:cCUFENT		:= Alltrim(SA1->A1_ESTE)
	oWS:cCDTPCLI	:= Alltrim(SA1->A1_YTPCLIE)
	oWS:cOBS        := IIF(empty(_cOBS),Alltrim(SA1->A1_YMOTIV),_cOBS)
	oWS:cCENDENT    := Alltrim(SA1->A1_YENDENT)
	oWS:cCCEPE      := Alltrim(SA1->A1_CEPE)
	oWS:cCMUNE      := Alltrim(SA1->A1_MUNE)
	oWS:cCBASE		:= Alltrim(SA1->A1_YBASE)
	oWS:AtualizarClienteCRM() // chama o m�todo que retorna a quantidade do item que o legado retorna no webservice e preenche a propriedade creturn no objeto oWS
	
	RecLock("SA1",.F.)
	SA1->A1_YBLOQ	:= IIF(empty(_cBloq),Alltrim(SA1->A1_YBLOQ),_cBloq)
	SA1->A1_RISCO	:= IIF(empty(_cRisco),Alltrim(SA1->A1_RISCO),_cRisco)//cRisco
	SA1->A1_YDTBLQ	:= Date()
	SA1->A1_YMOTIV	:= IIF(empty(_cOBS),Alltrim(SA1->A1_YMOTIV),_cOBS)//cMotivo
	SA1->A1_MSBLQL	:= cBloqueio
	SA1->A1_MUNE	:= cMun
	SA1->(MsUnlOck())
	
	MsgAlert(oWS:cAtualizarClienteCRMResult)
	RestArea( aArea )
Return //(oObj:ResultDados[1])


//--< fim de arquivo >----------------------------------------------------------------------
