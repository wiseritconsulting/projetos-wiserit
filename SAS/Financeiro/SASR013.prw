#Include 'Protheus.ch'
#Include 'TopConn.ch'


//_____________________________________________________________________________
/*/{Protheus.doc} OSBQCTR
Relatorio de Composi��o financeira - SAS 22

@author Carlos Castro	
@since 13 de Maio de 2015
@version P01
/*/
//_____________________________________________________________________________

User Function SASR013

Local aRegs			:= {}
Private oReport
Private cPergCont	:= 'SASR013   ' 
Private _astru		:={}
************************
*Monta pergunte do Log *
************************
AjustaSX1()

If Pergunte( cPergCONT, .T. )    
	Processa( { || ProcCont() }, 'Processando....' )
EndIf



Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ProcCont
Carrega os dados conforme parametrizacao feita pelo usuario;

@author Carlos Castro
@since 13 de Maio de 2015
@version P01
/*/
//_____________________________________________________________________________
Static Function ProcCont()
Local cQuery	:= ""
Local aPagto    := {}

tcspexec("FN_SAS22", MV_PAR01, DTOS(MV_PAR02),DTOS(MV_PAR03),DTOS(MV_PAR04),DTOS(MV_PAR05))

BeginSql Alias "TMP"

SELECT 
	 COD_CLI
	,NOME_CLIENTE
	,VLR_CTR
	,ESTIM
	,FRETE
	,ENVIADOS
	,FRETE_ENVIO
	,VRL_DEVOL
	,DESCONTO
	,RECEBIDO
	,VLR_VENCIDOS
	,A_VENCER
	,FILIAL
 FROM ##FN_SAS22_T

EndSql



oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()

TMP->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Carlos Castro
@since 13 de Maio de 2015
@version P01
/*/
//_____________________________________________________________________________
Static Function ReportDef()
Local oFirst
Local nOrd	:= 1


oReport := TReport():New( 'TMP', 'SAS-22-V2 COMPOSI��O FINANCEIRA', cPergCont, {|oReport| ReportPrint( oReport ), 'SAS-22-V2 COMPOSI��O FINANCEIRA' } )
oReport:SetLandScape()
oReport:lParamReadOnly := .T.

oReport:SetTotalInLine( .F. )

oFirst := TRSection():New( oReport, 'SAS-22 COMPOSI��O FINANCEIRA', { 'TMP', 'SZ2', 'SZ1', 'SE1', 'SZ5', 'SZ1','SE5','SA1' },,, )

oFirst:SetTotalInLine( .F. )


TRCell():New( oFirst, 'COD_CLI'		    ,'TMP', 'COD_CLI',			"@!"						    ,06,,							{ || TMP->COD_CLI 			} )
TRCell():New( oFirst, 'NOME CLIENTE'    ,'TMP', 'NOME CLIENTE',    "@!"							,50,,					        { || TMP->NOME_CLIENTE  	} )
TRCell():New( oFirst, 'VLR_CTR'	   	    ,'TMP', 'VLR_CTR' ,		   	"@E 999,999,999.99"		        ,11,,							{ || TMP->VLR_CTR	   		} )
TRCell():New( oFirst, 'ESTIM'       	,'TMP', 'ESTIM',		     	"@E 999,999,999.99"	       		,11,,							{ || TMP->ESTIM 	   		} )
TRCell():New( oFirst, 'FRETE'   		,'TMP', 'FRETE' ,			    "@E 999,999,999.99"	    		,11,,							{ || TMP->FRETE 	    	} ) 
TRCell():New( oFirst, 'ENVIADOS'		,'TMP', 'ENVIADOS' ,	   	    "@E 999,999,999.99"				,11,,							{ || TMP->ENVIADOS      	} )
TRCell():New( oFirst, 'FRETE_ENVIO'		,'TMP', 'FRETE_ENVIO' ,	"@E 999,999,999.99"				,11,,							{ || TMP->FRETE_ENVIO      	} )
TRCell():New( oFirst, 'VRL_DEVOL'	   	,'TMP', 'VRL_DEVOL',			"@E 999,999,999.99"   	  		,11,,							{ || TMP->VRL_DEVOL	      	} )
TRCell():New( oFirst, 'DESCONTO'		,'TMP', 'DESCONTO' ,	       "@E 999,999,999.99"				,11,,							{ || TMP->DESCONTO			} )
TRCell():New( oFirst, 'RECEBIDO'		,'TMP', 'RECEBIDO' ,	       "@E 999,999,999.99"				,11,,							{ || TMP->RECEBIDO			} )
TRCell():New( oFirst, 'VLR_VENCIDOS'	,'TMP', 'VLR_VENCIDOS',		"@E 999,999,999.99"	  			,11,,							{ || TMP->VLR_VENCIDOS		} )
TRCell():New( oFirst, 'A_VENCER'		,'TMP', 'A_VENCER',			"@E 999,999,999.99"				,11,,							{ || TMP->A_VENCER			} )
TRCell():New( oFirst, 'FILIAL'	    	,'TMP', 'FILIAL',				"@!"							,02,,							{ || TMP->FILIAL			} )


//oBreakT := TRBreak():New( oFirst, { || }, 'Vlr. Total' )

//TRFunction():New( oFirst:Cell( 'ValFat2' ),	'', 'SUM', oBreakT,,,, .F., .F. )

//oFirst:Cell( 'TOTAL' ):SetHeaderAlign( 'RIGHT' )

//oFirst:SetColSpace(9)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author Castro
@since 03 de Fevereiro de 2015
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

MakeAdvplExpr( cPergCont )
oReport:Section(1):Enable()

dbSelectArea( 'TMP' )
oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author CASTRO
@since 28 de Janeiro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function AjustaSX1()
Local aHelp	:= {}
Local aArea	:= GetArea()

SX1->( dbSetOrder(1) )

If !SX1->( dbSeek( cPergCont ) )
	Aadd( aHelp, { { 'C�DIGO DO CLIENTE PARA RODAR PARA 1 CLIENTE OU * PARA TODOS OS CLIENTES.'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'CODIGO DA EMPRESA PESQUISADA.'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'ANO DE VIGENCIA DO CONTRATO PESQUISADO.'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'DATA INICIAL PARA PESQUISA DOS DADOS.'	},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'DATA FINAL PARA PESQUISA DOS DADOS.'	},	{ '' }, { ' ' } } )
	
	PutSx1( cPergCont, "01", "CLIENTE ?		", "", "", "mv_ch1", "S", 06, 0, 0, "C", "", "", "", "", "MV_PAR01", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , "", "", "" )
	PutSx1( cPergCont, "02", "FILIAL ?		", "", "", "mv_ch2", "S", 02, 0, 0, "C", "", "", "", "", "MV_PAR02", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , "", "", "" )
	PutSx1( cPergCont, "03", "ANO ?		    ", "", "", "mv_ch3", "S", 04, 0, 0, "N", "", "", "", "", "MV_PAR03", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , "", "", "" )
	PutSx1( cPergCont, "04", "DATAINI ?		", "", "", "mv_ch4", "S", 08, 0, 0, "D", "", "", "", "", "MV_PAR04", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , "", "", "" )
	PutSx1( cPergCont, "05", "DATAFIM ?		", "", "", "mv_ch5", "S", 08, 0, 0, "D", "", "", "", "", "MV_PAR05", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , "", "", "" )

EndIf

RestArea( aArea )

Return( Nil)