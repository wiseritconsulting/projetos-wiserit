#Include 'Protheus.ch'
#Include 'TopConn.ch'


//_____________________________________________________________________________
/*/{Protheus.doc} OSBQCTR
Relatorio de listagens de itens por contrato

@author Francisco Valdeni
@since 24 de Novembro de 2014
@version P11
/*/
//_____________________________________________________________________________

User Function SASR019()

Local aRegs			:= {}
Private oReport
Private cPergCont	:= 'SASR019'
Private _astru		:={}
************************
*Monta pergunte do Log *
************************
AjustaSX1()

If Pergunte( cPergCONT, .T. )    
	Processa( { || ProcCont() }, 'Processando....' )
EndIf



Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ProcCont
Carrega os dados conforme parametrizacao feita pelo usuario;

@author Francisco Valdeni
@since 24 de Novembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ProcCont()
Local cQuery	:= ""
Local aPagto    := {}


AADD(_astru,{"CONTRATO" 	,"C",06,0})
AADD(_astru,{"CLIENTE"	 	,"C",40,0})
AADD(_astru,{"NOME"		 	,"C",40,0})
AADD(_astru,{"VALOR"	  	,"N",18,2})
AADD(_astru,{"PERCFRETE"	,"N",05,2})
AADD(_astru,{"TPFRETE"		,"C",03,0})
AADD(_astru,{"VLFRETE" 		,"N",18,2})
AADD(_astru,{"QDTPARC"		,"C",02,0})
AADD(_astru,{"VLPARC"		,"N",18,2})
AADD(_astru,{"DTVENC" 		,"D",08,0})

// cria a tabela tempor�ria
_carq:="P_"+Criatrab(,.F.)
MsCreate(_carq,_astru,"DBFCDX") 

//�����������������������������������������������������������Ŀ
//�atribui a tabela tempor�ria ao alias TRB
//�������������������������������������������������������������
dbUseArea(.T.,"DBFCDX",_cARq,"TRB",.T.,.F.)


cQuery := "SELECT DISTINCT "
cQuery += "Z1_CONTRAT AS CONTRATO, "
cQuery += "Z1_NOME AS NOME, "
cQuery += "SUM(Z2_VLRLIQ) AS VALOR, "
cQuery += "Z1_FRETEC AS PERCFRETE, "
cQuery += "Z1_CPFIN AS CONDPAG, "
cQuery += "Z1_DTCOND AS EMISSAO, "
cQuery += "Z1_CLIENTE AS CLIENTE, "
cQuery += "CASE Z1_FRETE WHEN '1' THEN 'FOB' "
cQuery += "WHEN '2' THEN 'CIF' END AS 'TPFRETE', "
cQuery += "CASE Z1_FRETE WHEN '1' THEN CONVERT(DECIMAL(10,2),(SUM(Z2_VLRLIQ) * Z1_FRETEC) / 100)"
cQuery += "ELSE 0 END  AS VLFRETE "  
cQuery += "FROM "+RetSqlName('SZ1')+" SZ1  "
cQuery += "JOIN SZ2010 SZ2 ON(SZ1.Z1_MSFIL = SZ2.Z2_MSFIL AND SZ1.Z1_CONTRAT = SZ2.Z2_CONTRAT)"
cQuery += "WHERE SZ1.D_E_L_E_T_ <> '*' " 
cQuery += "AND SZ2.D_E_L_E_T_ <> '*' "        
cQuery += "AND SZ1.Z1_MSFIL BETWEEN '"+MV_PAR01+"' AND '"+MV_PAR02+"' "
cQuery += "AND SZ1.Z1_ANOCOP = '"+MV_PAR03+"' "
cQuery += "AND SZ2.Z2_TES = '502'"
cQuery += "AND SZ1.Z1_CONTRAT BETWEEN '"+MV_PAR04+"' AND '"+MV_PAR05+"' "
cQuery += "GROUP BY Z1_CONTRAT, "
cQuery += "Z1_NOME, "
cQuery += "Z1_VLADT,"
cQuery += "Z1_CPFIN,"
cQuery += "Z1_FRETE,"
cQuery += "Z1_FRETEC,"
cQuery += "Z1_CONDPAG,"
cQuery += "Z1_DTCOND, "
cQuery += "Z1_CLIENTE "
cQuery += "ORDER BY CONTRATO "

cQuery := ChangeQuery(cQuery)
dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), "QSZ1", .F., .T.)
dbSelectArea("QSZ1")
QSZ1->(dbGotop())


While !QSZ1->(EOF())
	IF QSZ1->VALOR > 0
		RecLock("TRB",.T.)	
			TRB->CONTRATO	:= QSZ1->CONTRATO
			TRB->CLIENTE	:= QSZ1->CLIENTE	
			TRB->NOME		:= QSZ1->NOME			
			TRB->VALOR		:= QSZ1->VALOR
			TRB->PERCFRETE	:= QSZ1->PERCFRETE
			TRB->TPFRETE	:= QSZ1->TPFRETE
			TRB->VLFRETE	:= QSZ1->VLFRETE	
			aPagto := Condicao(QSZ1->VALOR,QSZ1->CONDPAG,,stod(QSZ1->EMISSAO))				
			TRB->QDTPARC		:= Alltrim(Str(Len(aPagto)))
			IF Len(aPagto) > 0 			
				nCalculo			:= QSZ1->VALOR+ROUND((QSZ1->VALOR*QSZ1->PERCFRETE)/100,2)
				nCalculo			:= nCalculo/Len(aPagto)
				TRB->VLPARC			:= IIF(QSZ1->TPFRETE=="FOB",nCalculo,aPagto[1][2])
				TRB->DTVENC			:= aPagto[1][1]
			ELSE 
				TRB->VLPARC			:= 0
				TRB->DTVENC			:= ctod("")
			ENDIF
	    TRB->(MsUnLocK())
  	ENDIF
QSZ1->(dbSkip())
ENDDO

QSZ1->(dbCloseArea())


oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
TRB->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Francisco Valdeni
@since 24 de Novembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportDef()
Local oFirst
Local nOrd	:= 1


oReport := TReport():New( 'TRB', 'Descritivo de Contratos Sinteticos', cPergCont, {|oReport| ReportPrint( oReport ), 'Lista de contratos para conferencia no financeiro' } )
oReport:SetLandScape()
oReport:lParamReadOnly := .T.

oReport:SetTotalInLine( .F. )

oFirst := TRSection():New( oReport, 'Contratos Sinteticos', { 'TRB', 'SZ2', 'SZ1', 'SA1', 'SZ3', 'SB1', 'SZ5' },,, )

oFirst:SetTotalInLine( .F. )


TRCell():New( oFirst, 'Contrato','TRB', 'Contrato',					PesqPict( 'SZ2', 'Z2_CONTRAT')	,06,,							{ || TRB->CONTRATO 	} )
TRCell():New( oFirst, 'Cliente'	,'TRB', 'Cliente' ,					PesqPict( 'SA1', 'A1_COD')		,06,,							{ || TRB->CLIENTE	} )
TRCell():New( oFirst, 'Nome'	,'TRB', 'Nome' ,					PesqPict( 'SA1', 'A1_NOME')		,40,,							{ || TRB->NOME		} )
TRCell():New( oFirst, 'Valor'	,'TRB', 'Valor',					PesqPict( 'SZ1', 'Z1_VLADT' )  	,12,,							{ || TRB->VALOR		} )
TRCell():New( oFirst, '%FRETE'	,'TRB', '%FRETE',					"@E 99.99"  					,05,,							{ || TRB->PERCFRETE	} )
TRCell():New( oFirst, 'TPFRETE'	,'TRB', 'TPFRETE' ,					"@!"							,03,,							{ || TRB->TPFRETE	} )
TRCell():New( oFirst, 'VLFRETE'	,'TRB', 'VLFRETE',					PesqPict( 'SZ1', 'Z1_VLADT' )  	,12,,							{ || TRB->VLFRETE	} )
TRCell():New( oFirst, 'QTDPARC'	,'TRB', 'QTDPARC' ,					"@!"							,02,,							{ || TRB->QDTPARC	} )
TRCell():New( oFirst, 'Val. Parcela','TRB','Val. Parcela',			PesqPict( 'SZ1', 'Z1_VLADT' )  	,12,,							{ || TRB->VLPARC	} )
TRCell():New( oFirst, 'Dt 1� Parc','TRB', 'Dt 1� Parc' ,			"@!"							,08,,							{ || TRB->DTVENC	} )

//oBreakT := TRBreak():New( oFirst, { || }, 'Vlr. Total' )

//TRFunction():New( oFirst:Cell( 'ValFat2' ),	'', 'SUM', oBreakT,,,, .F., .F. )

//oFirst:Cell( 'TOTAL' ):SetHeaderAlign( 'RIGHT' )

//oFirst:SetColSpace(9)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author Francisco Valdeni
@since 24 de Novembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

MakeAdvplExpr( cPergCont )
oReport:Section(1):Enable()

dbSelectArea( 'TRB' )
oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Francisco Valdeni
@since 24 de Novembro de 2014
@version P11
/*/
//_____________________________________________________________________________
Static Function AjustaSX1()
Local aHelp	:= {}
Local aArea	:= GetArea()

SX1->( dbSetOrder(1) )

If !SX1->( dbSeek( cPergCont ) )
	Aadd( aHelp, { { 'C�digo da filia inicial a ser listada no relat�rio.'		},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'C�digo da filia Final a ser listada no relat�rio.' 		}, 	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'Informe o ano de copetencia dos contratos para listagem' 	}, 	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'c�digo do contrato inicial ?.'							},	{ '' }, { ' ' } } )
	Aadd( aHelp, { { 'c�digo do contrato final ?.'								},	{ '' }, { ' ' } } )			

	PutSx1( cPergCont, "01", "Da Filial ?		", "", "", "mv_ch1", "C", 06, 0, 0, "C", "", "SM0", "", "", "MV_PAR01", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[1][1], aHelp[1][2], aHelp[1][3] )
	PutSx1( cPergCont, "02", "At� a Filial ?	", "", "", "mv_ch2", "C", 06, 0, 0, "C", "", "SM0", "", "", "MV_PAR02", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[2][1], aHelp[2][2], aHelp[2][3] )
	PutSx1( cPergCont, "03", "Ano Copetencia ?	", "", "", "mv_ch3", "C", 04, 0, 0, "C", "", ""   , "", "", "MV_PAR03", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[3][1], aHelp[3][2], aHelp[3][3] )
	PutSx1( cPergCont, "04", "Do Contrato ?		", "", "", "mv_ch4", "C", 06, 0, 0, "C", "", "SZ1" ,"", "", "MV_PAR04", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[4][1], aHelp[4][2], aHelp[4][3] )	
	PutSx1( cPergCont, "05", "Ate o Contrato ?	", "", "", "mv_ch5", "C", 06, 0, 0, "C", "", "SZ1", "", "", "MV_PAR05", "",			"",		"",		"", "",		"",		"",		"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" ,"" , aHelp[5][1], aHelp[5][2], aHelp[5][3] )
EndIf

RestArea( aArea )

Return( Nil)
