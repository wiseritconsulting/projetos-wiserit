#include 'protheus.ch'
#include 'parmtype.ch'

user function SASR046()

	Private oReport
	Private cPergCont	:= 'SASR046' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)

	If Pergunte( cPergCont, .T. )    
		Processa( { || ProcCont() }, 'Processando....' )
	EndIf

Return( Nil )


Static Function ProcCont()
	Local cQuery	:= ""
	Local aPagto    := {}

	BeginSql Alias "TMP1"

	SELECT SA1.A1_COD AS CLIENTE,
	SA1.A1_NOME AS NOME,
	ZZ0.ZZ0_ADIANT AS VALOR,
	SA1.A1_CGC AS CNPJ,
	SA1.A1_YDTBLQ AS DATA,
	CASE A1_YBLOQ WHEN 'N' THEN 'DESBLOQUEADO'
	WHEN 'S' THEN 'BLOQUEADO' END STATUS,
	SA1.A1_RISCO AS RISCO,
	SA1.A1_YMOTIV AS MOTIVO
	FROM SA1010 SA1
	JOIN ZZ0010 ZZ0
	ON SA1.A1_COD = ZZ0.ZZ0_CLIENT
	AND SA1.A1_LOJA = ZZ0.ZZ0_LOJA
	AND ZZ0.%NOTDEL%
	WHERE SA1.%NOTDEL%
	AND ZZ0.ZZ0_STATUS <> 'I'
	AND SA1.A1_COD BETWEEN %EXP:MV_PAR01% AND %EXP:MV_PAR02%

	EndSql

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()

	TMP1->(dbCloseArea())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 08 de Abril de 2016
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportDef()
	Local oFirst
	Local nOrd	:= 1


	oReport := TReport():New( 'TMP1', 'BLOQUEIO\DESBLOQUEIO CLIENTES', cPergCont, {|oReport| ReportPrint( oReport ), 'BLOQUEIO\DESBLOQUEIO CLIENTES' } )
	oReport:SetLandScape()
	oReport:lParamReadOnly := .T.

	oReport:SetTotalInLine( .F. )

	oFirst := TRSection():New( oReport, 'BLOQUEIO\DESBLOQUEIO CLIENTES', { 'TMP1', 'SA1','ZZ0'},,, )

	oFirst:SetTotalInLine( .F. )


	TRCell():New( oFirst, 'CLIENTE'		    ,'TMP1', 'CLIENTE',				"@!"						    ,10,,							{ || TMP1->CLIENTE 			                  } )
	TRCell():New( oFirst, 'NOME'		    ,'TMP1', 'NOME',				"@!"						    ,80,,							{ || TMP1->NOME		     	                  } )
	TRCell():New( oFirst, 'VALOR'       	,'TMP1', 'VALOR',		        "@E 9,999,999.99"				,25,,				            { || TMP1->VALOR   } )
	TRCell():New( oFirst, 'CNPJ'            ,'TMP1', 'CNPJ',                "@!"							,30,,					        { || TMP1->CNPJ        	                      } )
	TRCell():New( oFirst, 'DATA'	   	    ,'TMP1', 'DATA' ,		     	"@!"		                    ,15,,							{ || STOD(TMP1->DATA) 	   		              } )
	TRCell():New( oFirst, 'STATUS'	        ,'TMP1', 'STATUS' ,	            "@!"		                    ,25,,							{ || TMP1->STATUS        	                  } )
	TRCell():New( oFirst, 'RISCO'	        ,'TMP1', 'RISCO' ,	            "@!"		                    ,02,,							{ || TMP1->RISCO        	                  } )
	TRCell():New( oFirst, 'MOTIVO'	        ,'TMP1', 'MOTIVO' ,	            "@!"		                    ,80,,							{ || TMP1->MOTIVO        	                  } )
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamentos dos dados a serem impressos;

@author Weskley Silva
@since 08 de Abril de 2016
@version P11
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	MakeAdvplExpr( cPergCont )
	oReport:Section(1):Enable()

	dbSelectArea( 'TMP1')
	oReport:Section(1):Print()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 08 de Abril de 2016
@version P11
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Cliente de"			,""		,""		,"mv_ch1","C",06,0,0,"G",""	,"SA1"	,"","","MV_PAR01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Cliente ate"		,""		,""		,"mv_ch2","C",06,0,0,"G",""	,"SA1"	,"","","MV_PAR02"," ","","","","","","","","","","","","","","","")
	
Return



