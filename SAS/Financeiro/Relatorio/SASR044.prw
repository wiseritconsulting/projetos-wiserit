// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : SASR044
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Rog�rio J�come            | Descricao Relat�rio Medi��es x NF (Wesley)
// ---------+-------------------+-----------------------------------------------------------
// 21/03/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include 'protheus.ch'
#include 'parmtype.ch'
#include 'TopConn.CH'
#include "tbiconn.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo
Montagem da tela de processamento

@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     21/03/2016
/*/
//------------------------------------------------------------------------------------------
user function SASR044()
	//--< vari�veis >---------------------------------------------------------------------------
	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local cLocal          := "c:\"
	//Local xArea := GetArea()
	Local Vez := 0
	
	local _ano := space(4)
	local _filial := space(6)
	local _client := space(TAMSX3("A1_COD")[1])
	local _loja := space(TAMSX3("A1_LOJA")[1])
	local _data := space(TamSX3("D1_DTDIGIT")[1])
	Local aPergs := {}
	Local aRet := {}
	Local lRet
	Local cQuery
	private cPerg := "SASR044"

	
	Private iX := 0
	Private oPrinter
	Private cFilePrint := "Composicao_Financeira.pdf"
	Private nQuant := 0
	Private qVol := 0
	Private qPesLiq := 0
	Private oFont1 	:= TFont():New( "Arial",,10,,.F.,,,,,.F.)
	Private oFont2 	:= TFont():New( "Arial",,9,,.F.,,,,,.F.)
	Private oFont3 	:= TFont():New( "Arial",,12,,.T.,,,,,.F.)
	Private oFont4 	:= TFont():New( "Arial",,10,,.T.,,,,,.F.)
	
	Private nLin   := 0
	Private nTmLin := 60
	Private nSalto := 20
	Private nPag	:= 0
	Private cContra := ""
	Private oBrush
	Private cRazao := ""
	Private nTContrato := 0
	Private nTEnviados := 0
	private nTAditivos := 0
	Private nTDevolvidos := 0
	
	
	
	Private nAdiant := 0
	
	Private nTSaldo := 0
	
	//Totalizadores se��o financeiro
	Private nTFin := 0
	Private nTFJur := 0
	Private nTFDesc := 0
	Private nTFBai := 0
	
	//Totalizadores se��o fiscal
	Private nTFis := 0
	Private nTFrete := 0
	Private nTJuros := 0
	Private nTDesconto := 0
	Private nTAdFisc := 0
	Private nTBaixaNF := 0
	
	Private TotalVL1 := 0
	Private TotalVL2 := 0
	Private TotalVL3 := 0
	Private TotalVL4 := 0
	Private TotalVL5 := 0
	Private TotalVL6 := 0


	Private DescVL1 := 0
	Private DescVL2 := 0
	Private DescVL3 := 0
	Private DescVL4 := 0
	Private DescVL5 := 0
	Private DescVL6 := 0
	AjustaSX1(cPerg)
	
	Pergunte("SASR044",.T.)
			
	oPrinter := FWMSPrinter():New(cFilePrint, IMP_SPOOL, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetPortrait()

	
	cQuery := " SELECT * FROM (SELECT "
	cQuery += " ZZO_DESC,A1_NOME, ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO,ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM, "
	cQuery += " ISNULL(ENVIO.[01],0) AS 'ENVIO1', ISNULL(ENVIO.[02],0) AS 'ENVIO2', ISNULL(ENVIO.[03],0)AS 'ENVIO3', "
	cQuery += " ISNULL(ENVIO.[04],0) AS 'ENVIO4', ISNULL(ENVIO.[05],0) AS 'ENVIO5', ISNULL(ENVIO.[06],0)AS 'ENVIO6' "
	cQuery += " FROM "
	cQuery += " (SELECT ZZO_DESC, A1_NOME, ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO, ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM, "
	cQuery += " (ZZ1_VLRUN-ZZ1_VLRDSC) ZZ1_VALOR,ZZ7_ENVIO "
	cQuery += " FROM "+RetSqlName("ZZ1")+" ZZ1 "
	cQuery += " INNER JOIN "+RetSqlName("SF4")+" SF4 ON SF4.F4_CODIGO = ZZ1.ZZ1_TES AND SF4.F4_FILIAL = SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) AND SF4.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("ZZ7")+" ZZ7 ON ZZ1.ZZ1_PRODUT = ZZ7.ZZ7_CODIGO AND ZZ7.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("ZZO")+" ZZO ON ZZ7.ZZ7_SERIE = ZZO.ZZO_COD AND ZZO.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("SA1")+" B1 ON ZZ1_CLIENT = A1_COD "
	cQuery += " INNER JOIN "+RetSqlName("ZZ0")+" Z0 ON ZZ0_FILIAL = ZZ1_FILIAL AND ZZ0_NUMERO = ZZ1_NUMERO AND ZZ0_CLIENT = ZZ1_CLIENT AND ZZ0_LOJA = ZZ1_LOJA "
	cQuery += " WHERE ZZ1.ZZ1_FILIAL='"+mv_par01+"' AND ZZ1.ZZ1_NUMERO='"+mv_par02+"' AND ZZ1.ZZ1_CLIENT='"+mv_par03+"' "
	
	cQuery += " AND SF4.F4_DUPLIC='S'
	
	cQuery += " AND ZZ1.D_E_L_E_T_ = '' "
	cQuery += " ) AS ENVIOS "
	cQuery += " PIVOT ( SUM(ZZ1_VALOR) FOR ZZ7_ENVIO IN ([01],[02],[03],[04],[05],[06])) AS ENVIO "
	cQuery += " ) T1 "
	cQuery += " INNER JOIN (SELECT "
	cQuery += " ZZO_DESC,A1_NOME,ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO,ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM, "
	cQuery += " ISNULL(QNTS.[01],0) AS 'QNTS1', ISNULL(QNTS.[02],0) AS 'QNTS2', ISNULL(QNTS.[03],0)AS 'QNTS3', "
	cQuery += " ISNULL(QNTS.[04],0) AS 'QNTS4', ISNULL(QNTS.[05],0) AS 'QNTS5', ISNULL(QNTS.[06],0)AS 'QNTS6' "
	cQuery += " FROM "
	cQuery += " (SELECT ZZO_DESC, A1_NOME,ZZ1_QUANT,ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO, ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM,ZZ7_ENVIO "
	cQuery += " FROM "+RetSqlName("ZZ1")+" ZZ1 "
	cQuery += " INNER JOIN "+RetSqlName("SF4")+" SF4 ON SF4.F4_CODIGO = ZZ1.ZZ1_TES AND SF4.F4_FILIAL = SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) AND SF4.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("ZZ7")+" ZZ7 ON ZZ1.ZZ1_PRODUT = ZZ7.ZZ7_CODIGO AND ZZ7.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("ZZO")+" ZZO ON ZZ7.ZZ7_SERIE = ZZO.ZZO_COD AND ZZO.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("SA1")+" B1 ON ZZ1_CLIENT = A1_COD "
	cQuery += " INNER JOIN "+RetSqlName("ZZ0")+" Z0 ON ZZ0_FILIAL = ZZ1_FILIAL AND ZZ0_NUMERO = ZZ1_NUMERO AND ZZ0_CLIENT = ZZ1_CLIENT AND ZZ0_LOJA = ZZ1_LOJA "
	cQuery += " WHERE ZZ1.ZZ1_FILIAL='"+mv_par01+"' AND ZZ1.ZZ1_NUMERO='"+mv_par02+"' AND ZZ1.ZZ1_CLIENT='"+mv_par03+"' "
	
	cQuery += " AND SF4.F4_DUPLIC='S'
	
	cQuery += " AND ZZ1.D_E_L_E_T_ = '' "
	cQuery += " ) AS QUANTIDADES "
	cQuery += " PIVOT ( SUM(ZZ1_QUANT) FOR ZZ7_ENVIO IN ([01],[02],[03],[04],[05],[06])) AS QNTS) T2 "
	cQuery += " ON T1.ZZO_DESC = T2.ZZO_DESC AND T1.A1_NOME = T2.A1_NOME AND T1.ZZ1_CLIENT = T2.ZZ1_CLIENT AND T1.ZZ1_FILIAL = T2.ZZ1_FILIAL "
	cQuery += " AND T1.ZZ1_NUMERO = T2.ZZ1_NUMERO AND T1.ZZ0_FRTCLI = T2.ZZ0_FRTCLI AND T1.ZZ0_FRTEMP = T2.ZZ0_FRTEMP AND T1.ZZ0_ANOCOM = T2.ZZ0_ANOCOM "
	cQuery += " INNER JOIN (SELECT "
	cQuery += " ZZO_DESC,A1_NOME, ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO,ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM, "
	cQuery += " ISNULL(ENVIO.[01],0) AS 'DESC1', ISNULL(ENVIO.[02],0) AS 'DESC2', ISNULL(ENVIO.[03],0)AS 'DESC3', "
	cQuery += " ISNULL(ENVIO.[04],0) AS 'DESC4', ISNULL(ENVIO.[05],0) AS 'DESC5', ISNULL(ENVIO.[06],0)AS 'DESC6' "
	cQuery += " FROM "
	cQuery += " (SELECT ZZO_DESC, A1_NOME, ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO, ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM, "
	cQuery += " ZZ1_VLRDSC,ZZ7_ENVIO "
	cQuery += " FROM "+RetSqlName("ZZ1")+" ZZ1 "
	cQuery += " INNER JOIN "+RetSqlName("SF4")+" SF4 ON SF4.F4_CODIGO = ZZ1.ZZ1_TES AND SF4.F4_FILIAL = SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) AND SF4.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("ZZ7")+" ZZ7 ON ZZ1.ZZ1_PRODUT = ZZ7.ZZ7_CODIGO AND ZZ7.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("ZZO")+" ZZO ON ZZ7.ZZ7_SERIE = ZZO.ZZO_COD AND ZZO.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("SA1")+" B1 ON ZZ1_CLIENT = A1_COD "
	cQuery += " INNER JOIN "+RetSqlName("ZZ0")+" Z0 ON ZZ0_FILIAL = ZZ1_FILIAL AND ZZ0_NUMERO = ZZ1_NUMERO AND ZZ0_CLIENT = ZZ1_CLIENT AND ZZ0_LOJA = ZZ1_LOJA "
	cQuery += " WHERE ZZ1.ZZ1_FILIAL='"+mv_par01+"' AND ZZ1.ZZ1_NUMERO='"+mv_par02+"' AND ZZ1.ZZ1_CLIENT='"+mv_par03+"' "
	
	cQuery += " AND SF4.F4_DUPLIC='S'

	cQuery += " AND ZZ1.D_E_L_E_T_ = '' "
	cQuery += " ) AS ENVIOS "
	cQuery += " PIVOT ( SUM(ZZ1_VLRDSC) FOR ZZ7_ENVIO IN ([01],[02],[03],[04],[05],[06])) AS ENVIO "
	cQuery += " ) T3 "
	cQuery += " ON T3.ZZO_DESC = T2.ZZO_DESC AND T3.A1_NOME = T2.A1_NOME AND T3.ZZ1_CLIENT = T2.ZZ1_CLIENT AND T3.ZZ1_FILIAL = T2.ZZ1_FILIAL "
	cQuery += " AND T3.ZZ1_NUMERO = T2.ZZ1_NUMERO AND T3.ZZ0_FRTCLI = T2.ZZ0_FRTCLI AND T3.ZZ0_FRTEMP = T2.ZZ0_FRTEMP AND T3.ZZ0_ANOCOM = T2.ZZ0_ANOCOM "
	
	
	cQuery += " INNER JOIN(SELECT "
	cQuery += " ZZO_DESC,A1_NOME, ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO,ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM, "
	cQuery += " ISNULL(ENVIO.[01],0) AS 'NOME1', ISNULL(ENVIO.[02],0) AS 'NOME2', ISNULL(ENVIO.[03],0)AS 'NOME3', "
	cQuery += " ISNULL(ENVIO.[04],0) AS 'NOME4', ISNULL(ENVIO.[05],0) AS 'NOME5', ISNULL(ENVIO.[06],0)AS 'NOME6' "
	cQuery += " FROM "
	cQuery += " (SELECT ZZO_DESC, A1_NOME, ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO, ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM, "
	cQuery += " ZZ7_DESCR,ZZ7_ENVIO "
	cQuery += " FROM "+RetSqlName("ZZ1")+" ZZ1 "
	cQuery += " INNER JOIN "+RetSqlName("SF4")+" SF4 ON SF4.F4_CODIGO = ZZ1.ZZ1_TES AND SF4.F4_FILIAL = SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) AND SF4.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("ZZ7")+" ZZ7 ON ZZ1.ZZ1_PRODUT = ZZ7.ZZ7_CODIGO AND ZZ7.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("ZZO")+" ZZO ON ZZ7.ZZ7_SERIE = ZZO.ZZO_COD AND ZZO.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName("SA1")+" B1 ON ZZ1_CLIENT = A1_COD "
	cQuery += " INNER JOIN "+RetSqlName("ZZ0")+" Z0 ON ZZ0_FILIAL = ZZ1_FILIAL AND ZZ0_NUMERO = ZZ1_NUMERO AND ZZ0_CLIENT = ZZ1_CLIENT AND ZZ0_LOJA = ZZ1_LOJA "
	cQuery += " WHERE ZZ1.ZZ1_FILIAL='"+mv_par01+"' AND ZZ1.ZZ1_NUMERO='"+mv_par02+"' AND ZZ1.ZZ1_CLIENT='"+mv_par03+"' "
	
	cQuery += " AND SF4.F4_DUPLIC='S'
	
	cQuery += " AND ZZ1.D_E_L_E_T_ = '' "
	cQuery += " ) AS ENVIOS "
	cQuery += " PIVOT ( MAX(ZZ7_DESCR) FOR ZZ7_ENVIO IN ([01],[02],[03],[04],[05],[06])) AS ENVIO "
	cQuery += " ) T4 "
	cQuery += " ON T3.ZZO_DESC = T4.ZZO_DESC AND T3.A1_NOME = T4.A1_NOME AND T3.ZZ1_CLIENT = T4.ZZ1_CLIENT AND T3.ZZ1_FILIAL = T4.ZZ1_FILIAL "
	cQuery += " AND T3.ZZ1_NUMERO = T4.ZZ1_NUMERO AND T3.ZZ0_FRTCLI = T4.ZZ0_FRTCLI AND T3.ZZ0_FRTEMP = T4.ZZ0_FRTEMP AND T3.ZZ0_ANOCOM = T4.ZZ0_ANOCOM "
	
	cQuery += " INNER JOIN (SELECT "
	cQuery += " ZZO_DESC,A1_NOME, ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO,ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM, "
	cQuery += " ISNULL(DEV.[01],0) AS 'DEV1', ISNULL(DEV.[02],0) AS 'DEV2', ISNULL(DEV.[03],0)AS 'DEV3', "
	cQuery += " ISNULL(DEV.[04],0) AS 'DEV4', ISNULL(DEV.[05],0) AS 'DEV5', ISNULL(DEV.[06],0)AS 'DEV6' "
	cQuery += " FROM "
	cQuery += " (SELECT ZZO_DESC, A1_NOME, ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO, ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM,ZZ3_QTDEV0 ,ZZ7_ENVIO "
	cQuery += " FROM ZZ1010 ZZ1 "
	cQuery += " INNER JOIN SF4010 SF4 ON SF4.F4_CODIGO = ZZ1.ZZ1_TES AND SF4.F4_FILIAL = SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) AND SF4.D_E_L_E_T_='' "
	cQuery += " INNER JOIN ZZ7010 ZZ7 ON ZZ1.ZZ1_PRODUT = ZZ7.ZZ7_CODIGO AND ZZ7.D_E_L_E_T_='' "
	cQuery += " INNER JOIN ZZO010 ZZO ON ZZ7.ZZ7_SERIE = ZZO.ZZO_COD AND ZZO.D_E_L_E_T_='' "
	cQuery += " INNER JOIN ZZ3010 Z3 "
	cQuery += " ON ZZ1_FILIAL = ZZ3_FILIAL "
	cQuery += " AND ZZ1_NUMERO = ZZ3_NUMERO "
	cQuery += " AND ZZ1_ITEM = ZZ3_ITEM "
	cQuery += " INNER JOIN SA1010 B1 ON ZZ1_CLIENT = A1_COD "
	cQuery += " INNER JOIN ZZ0010 Z0 ON ZZ0_FILIAL = ZZ1_FILIAL AND ZZ0_NUMERO = ZZ1_NUMERO AND ZZ0_CLIENT = ZZ1_CLIENT AND ZZ0_LOJA = ZZ1_LOJA "
	cQuery += " WHERE ZZ1.ZZ1_FILIAL='"+mv_par01+"' AND ZZ1.ZZ1_NUMERO='"+mv_par02+"' AND ZZ1.ZZ1_CLIENT='"+mv_par03+"' "
	cQuery += " AND SF4.F4_DUPLIC='S' AND ZZ1.D_E_L_E_T_ = '' "
	cQuery += " GROUP BY ZZO_DESC, A1_NOME, ZZ1_CLIENT,ZZ1_FILIAL,ZZ1_NUMERO, ZZ0_FRTEMP, ZZ0_FRTCLI, ZZ0_ANOCOM, ZZ3_QTDEV0,ZZ7_ENVIO "
	cQuery += " ) AS ENVIOS "
	cQuery += " PIVOT ( SUM(ZZ3_QTDEV0) FOR ZZ7_ENVIO IN ([01],[02],[03],[04],[05],[06])) AS DEV "
	cQuery += " ) T5 "
	cQuery += " ON T5.ZZO_DESC = T4.ZZO_DESC AND T5.A1_NOME = T4.A1_NOME AND T5.ZZ1_CLIENT = T4.ZZ1_CLIENT AND T5.ZZ1_FILIAL = T4.ZZ1_FILIAL "
	cQuery += " AND T5.ZZ1_NUMERO = T4.ZZ1_NUMERO AND T5.ZZ0_FRTCLI = T4.ZZ0_FRTCLI AND T5.ZZ0_FRTEMP = T4.ZZ0_FRTEMP AND T5.ZZ0_ANOCOM = T4.ZZ0_ANOCOM "
	TcQuery cQuery New Alias T01

	//for x := 1 to 4
			
	funCbc() //Chama o cabe�alho
			
	nTEnviados := 0
	nTDevolvidos := 0
	nTAditivos := 0
	nTFrete := 0
	nTJuros := 0
	nTDesconto := 0
	nAdiant := 0
	nTBaixaNF := 0
	nTSaldo := 0
			
	Secao1()//Chama os dados do contrato
	Secao2()//Chama os dados do contrato
	Secao3()

	oPrinter:EndPage()

	//next

	
	oPrinter:Preview()


Return
	
Static Function funCbc

	Local cLogo := "\system\sas.bmp"
	nPag	:= nPag + 1
	nLin   := 80
	
	DbSelectArea("T01")
	oPrinter:StartPage()
	
	oPrinter:Box( 65, 40, 130, 570, "-4")

	oPrinter:SayBitmap( nLin-010, 050, cLogo, 080, 058)

	oPrinter:Say ( nLin, 160, "COMPOSI��O FINANCEIRA - ESTIMATIVA",  oFont3 )
	oPrinter:Say ( nLin, 500, "Data: "+DTOC(DDATABASE),  oFont1 )
	oPrinter:Say ( nLin+10, 500, "Hora: "+Time(),  oFont1 )
	oPrinter:Say ( nLin+20, 500, "Pag: "+cValtoChar(nPag),  oFont1 )
	
	oPrinter:Say ( nLin+10, 160, "COD CLIENTE: "+ T01->ZZ1_CLIENT,  oFont4 )	
	oPrinter:Say ( nLin+25, 160, "CONTRATO: "+ T01->ZZ1_NUMERO,  oFont4 )
	oPrinter:Say ( nLin+25, 300, "CLIENTE: "+T01->A1_NOME+" ",  oFont4 )
	
	oPrinter:Say ( nLin+35, 160, "COMPET�NCIA: "+ T01->ZZ0_ANOCOM,  oFont4 )
	oPrinter:Say ( nLin+35, 300, "% FRETE EMPRESA: "+cvaltochar(T01->ZZ0_FRTEMP)+" ",  oFont4 )
	oPrinter:Say ( nLin+35, 420, "% FRETE CLIENTE: "+cvaltochar(T01->ZZ0_FRTCLI)+" ",  oFont4 )
	

	nLin := nLin + nTmLin
	

Return

Static Function Secao1()

	Local nTotal := 0
	Local espaco := space(1)
			
	nLin := nLin + 10
			
	oBrush1 := TBrush():New( , CLR_HGRAY)
	oPrinter:Fillrect( {155, 05, 143, 590 }, oBrush1, "-4")
	oPrinter:Say ( nLin, 020, "SERIE",  oFont4 )
			//oPrinter:Line( 10, 10, 10, 50, 0,"-4")
	oPrinter:Say ( nLin, 060, "ITENS",  oFont4 )
	oPrinter:Say ( nLin, 190, "Qnt.V1",  oFont4 )
	oPrinter:Say ( nLin, 220, "V1",  oFont4 )
	oPrinter:Say ( nLin, 250, "Qnt.V2",  oFont4 )
	oPrinter:Say ( nLin, 280, "V2",  oFont4 )
	oPrinter:Say ( nLin, 310, "Qnt.V3",  oFont4 )
	oPrinter:Say ( nLin, 340, "V3",  oFont4 )
	oPrinter:Say ( nLin, 370, "Qnt.V4",  oFont4 )
	oPrinter:Say ( nLin, 400, "V4",  oFont4 )
	oPrinter:Say ( nLin, 430, "Qnt.V5",  oFont4 )
	oPrinter:Say ( nLin, 460, "V5",  oFont4 )
	oPrinter:Say ( nLin, 490, "Qnt.V6",  oFont4 )
	oPrinter:Say ( nLin, 520, "V6",  oFont4 )
	
	oPrinter:Say ( nLin, 550, "Vlr Kit",  oFont4 )
	
	nLin := nLin + 15
	
	
	T01->(DbgoTop())
	While !T01->(EOF())
	
		oPrinter:Say ( nLin, 020, SUBSTR(T01->ZZO_DESC,0,8) ,  oFont1 )
		oPrinter:Say ( nLin, 060, SUBSTR(T01->NOME1,0,25) ,  oFont1 )
		oPrinter:Say ( nLin, 190, cvaltochar(T01->QNTS1 - T01->DEV1) ,  oFont1 )
		oPrinter:Say ( nLin, 220, PadR(alltrim(TRANSFORM(T01->ENVIO1,"@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 250, cvaltochar(T01->QNTS2 - T01->DEV2) ,  oFont1 )
		oPrinter:Say ( nLin, 280, PadR(alltrim(TRANSFORM(T01->ENVIO2,"@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 310, cvaltochar(T01->QNTS3 - T01->DEV3) ,  oFont1 )
		oPrinter:Say ( nLin, 340, PadR(alltrim(TRANSFORM(T01->ENVIO3,"@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 370, cvaltochar(T01->QNTS4 - T01->DEV4) ,  oFont1 )
		oPrinter:Say ( nLin, 400, PadR(alltrim(TRANSFORM(T01->ENVIO4,"@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 430, cvaltochar(T01->QNTS5 - T01->DEV5) ,  oFont1 )
		oPrinter:Say ( nLin, 460, PadR(alltrim(TRANSFORM(T01->ENVIO5,"@E 999,999,999.99")),12,) ,  oFont1 )
		oPrinter:Say ( nLin, 490, cvaltochar(T01->QNTS6 - T01->DEV6) ,  oFont1 )
		oPrinter:Say ( nLin, 520, PadR(alltrim(TRANSFORM(T01->ENVIO6,"@E 999,999,999.99")),12,) ,  oFont1 )
	
		oPrinter:Say ( nLin, 550, PadR(alltrim(TRANSFORM(T01->ENVIO1 + T01->ENVIO2 + T01->ENVIO3 + T01->ENVIO4 + T01->ENVIO5 + T01->ENVIO6   ,"@E 999,999,999.99")),12,) ,  oFont1 )
		
		nLin := nLin + 10
		T01->(DbSkip())
	EndDo
	
	nLin := nLin + 20
	nTotal := 0
return

Static Function Secao2()

	Local nTotal := 0
	Local espaco := space(1)
	
	nLin := nLin + 10
			
	oBrush1 := TBrush():New( , CLR_HGRAY)
	oPrinter:Fillrect( {nLin+5, 05, nLin-7, 590 }, oBrush1, "-4")
	oPrinter:Say ( nLin, 020, "SERIE",  oFont4 )
			//oPrinter:Line( 10, 10, 10, 50, 0,"-4")
	oPrinter:Say ( nLin, 060, "ITENS",  oFont4 )
	oPrinter:Say ( nLin, 190, "Vlr.V1",  oFont4 )
	oPrinter:Say ( nLin, 250, "Vlr.V2",  oFont4 )
	oPrinter:Say ( nLin, 310, "Vlr.V3",  oFont4 )
	oPrinter:Say ( nLin, 370, "Vlr.V4",  oFont4 )
	oPrinter:Say ( nLin, 430, "Vlr.V5",  oFont4 )
	oPrinter:Say ( nLin, 490, "Vlr.V6",  oFont4 )
	
	oPrinter:Say ( nLin, 550, "Vlr Kit",  oFont4 )
	
	nLin := nLin + 15
	
	T01->(DbgoTop())
	While !T01->(EOF())
	
		oPrinter:Say ( nLin, 020, SUBSTR(T01->ZZO_DESC,0,8) ,  oFont1 )
		oPrinter:Say ( nLin, 060, SUBSTR(T01->NOME1,0,25) ,  oFont1 )
		oPrinter:Say ( nLin, 190, PadR(alltrim(TRANSFORM(T01->ENVIO1 * (T01->QNTS1 - T01->DEV1),"@E 999,999,999.99")),12,) ,  oFont1 )
		TotalVL1 += T01->ENVIO1 * T01->QNTS1
		DescVL1 += T01->DESC1
		oPrinter:Say ( nLin, 250, PadR(alltrim(TRANSFORM(T01->ENVIO2 * (T01->QNTS2 - T01->DEV2),"@E 999,999,999.99")),12,) ,  oFont1 )
		TotalVL2 += T01->ENVIO2 * T01->QNTS2
		DescVL2 += T01->DESC2
		oPrinter:Say ( nLin, 310, PadR(alltrim(TRANSFORM(T01->ENVIO3 * (T01->QNTS3 - T01->DEV3),"@E 999,999,999.99")),12,) ,  oFont1 )
		TotalVL3 += T01->ENVIO3 * T01->QNTS3
		DescVL3 += T01->DESC3
		oPrinter:Say ( nLin, 370, PadR(alltrim(TRANSFORM(T01->ENVIO4 * (T01->QNTS4 - T01->DEV4),"@E 999,999,999.99")),12,) ,  oFont1 )
		TotalVL4 += T01->ENVIO4 * T01->QNTS4
		DescVL4 += T01->DESC4
		oPrinter:Say ( nLin, 430, PadR(alltrim(TRANSFORM(T01->ENVIO5 * (T01->QNTS5 - T01->DEV5),"@E 999,999,999.99")),12,) ,  oFont1 )
		TotalVL5 += T01->ENVIO5 * T01->QNTS5
		DescVL5 += T01->DESC5
		oPrinter:Say ( nLin, 490, PadR(alltrim(TRANSFORM(T01->ENVIO6 * (T01->QNTS6 - T01->DEV6),"@E 999,999,999.99")),12,) ,  oFont1 )
		TotalVL6 += T01->ENVIO6 * T01->QNTS6
		DescVL6 += T01->DESC6
		oPrinter:Say ( nLin, 550, PadR(alltrim(TRANSFORM((T01->ENVIO1 * (T01->QNTS1 - T01->DEV1)) + (T01->ENVIO2 * (T01->QNTS2 - T01->DEV2)) + (T01->ENVIO3 * (T01->QNTS3 - T01->DEV3)) + (T01->ENVIO4 * (T01->QNTS4 - T01->DEV4)) + (T01->ENVIO5 * (T01->QNTS5 - T01->DEV5)) + (T01->ENVIO6 * (T01->QNTS6 - T01->DEV6))   ,"@E 999,999,999.99")),12,) ,  oFont1 )
		
		nLin := nLin + 10
		T01->(DbSkip())
	
	EndDo
	
	nTotal := 0
return


Static Function Secao3()

	Local nTotal := 0
	Local espaco := space(1)
	
	nLin := nLin + 10
			
	nLin := nLin + 15
	T01->(DbgoTop())
	
	
	oPrinter:Say ( nLin, 020, "TOTAIS" ,  oFont4 )
		
	oPrinter:Say ( nLin, 190, PadR(alltrim(TRANSFORM(TotalVL1,"@E 999,999,999.99")),12,) ,  oFont1 )

	oPrinter:Say ( nLin, 250, PadR(alltrim(TRANSFORM(TotalVL2,"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 310, PadR(alltrim(TRANSFORM(TotalVL3,"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 370, PadR(alltrim(TRANSFORM(TotalVL4,"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 430, PadR(alltrim(TRANSFORM(TotalVL5,"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 490, PadR(alltrim(TRANSFORM(TotalVL6,"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 550, PadR(alltrim(TRANSFORM(TotalVL1 + TotalVL2 + TotalVL3 + TotalVL4+ TotalVL5 + TotalVL6   ,"@E 999,999,999.99")),12,) ,  oFont1 )
		
	oPrinter:Line( nLin+5, 05, nLin+5, 590)
			
	nLin := nLin + 20
	
	oPrinter:Say ( nLin, 020, "% FRETE" ,  oFont4 )
		
	oPrinter:Say ( nLin, 190, PadR(alltrim(TRANSFORM(TotalVL1 *  (T01->ZZ0_FRTCLI/100),"@E 999,999,999.99")),12,) ,  oFont1 )

	oPrinter:Say ( nLin, 250, PadR(alltrim(TRANSFORM(TotalVL2 *  (T01->ZZ0_FRTCLI/100),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 310, PadR(alltrim(TRANSFORM(TotalVL3 *  (T01->ZZ0_FRTCLI/100),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 370, PadR(alltrim(TRANSFORM(TotalVL4 *  (T01->ZZ0_FRTCLI/100),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 430, PadR(alltrim(TRANSFORM(TotalVL5 *  (T01->ZZ0_FRTCLI/100),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 490, PadR(alltrim(TRANSFORM(TotalVL6 *  (T01->ZZ0_FRTCLI/100),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 550, PadR(alltrim(TRANSFORM((TotalVL1 *  (T01->ZZ0_FRTCLI/100)) + (TotalVL2 *  (T01->ZZ0_FRTCLI/100)) + (TotalVL3 *  (T01->ZZ0_FRTCLI/100)) + (TotalVL4 *  (T01->ZZ0_FRTCLI/100)) + (TotalVL5 *  (T01->ZZ0_FRTCLI/100)) + (TotalVL6 *  (T01->ZZ0_FRTCLI/100))   ,"@E 999,999,999.99")),12,) ,  oFont1 )
		
	oPrinter:Line( nLin+5, 05, nLin+5, 590)
		
	nLin := nLin + 20
	oPrinter:Say ( nLin, 020, "TOTAL C/FRETE" ,  oFont4 )
		
	oPrinter:Say ( nLin, 190, PadR(alltrim(TRANSFORM(TotalVL1 + (TotalVL1 *  (T01->ZZ0_FRTCLI/100)),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 250, PadR(alltrim(TRANSFORM(TotalVL2+ (TotalVL2 *  (T01->ZZ0_FRTCLI/100)),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 310, PadR(alltrim(TRANSFORM(TotalVL3+ (TotalVL3 *  (T01->ZZ0_FRTCLI/100)),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 370, PadR(alltrim(TRANSFORM(TotalVL4+ (TotalVL4 *  (T01->ZZ0_FRTCLI/100)),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 430, PadR(alltrim(TRANSFORM(TotalVL5+ (TotalVL5 *  (T01->ZZ0_FRTCLI/100)),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 490, PadR(alltrim(TRANSFORM(TotalVL6+ (TotalVL6 *  (T01->ZZ0_FRTCLI/100)),"@E 999,999,999.99")),12,) ,  oFont1 )
	oPrinter:Say ( nLin, 550, PadR(alltrim(TRANSFORM((TotalVL1 + (TotalVL1 *  (T01->ZZ0_FRTCLI/100)) )+ (TotalVL2 + (TotalVL2 *  (T01->ZZ0_FRTCLI/100)) ) + (TotalVL3 + (TotalVL3 *  (T01->ZZ0_FRTCLI/100)) ) + (TotalVL4 + (TotalVL4 *  (T01->ZZ0_FRTCLI/100)) )+ (TotalVL5 + (TotalVL5 *  (T01->ZZ0_FRTCLI/100)) ) + (TotalVL6 + (TotalVL6 *  (T01->ZZ0_FRTCLI/100)) )   ,"@E 999,999,999.99")),12,) ,  oFont1 )
		
	oPrinter:Line( nLin+5, 05, nLin+5, 590)
		
	nLin := nLin + 20
	oPrinter:Say ( nLin, 020, "VALOR PAGO" ,  oFont4 )
		
	
		
	cQuery := " SELECT SUM(E1_VALOR-E1_SALDO) PAGO FROM "+RetSqlName("SE1")+" E1 "
	cQuery += " WHERE E1.D_E_L_E_T_ = '' "
	cQuery += " AND E1_YCONTRA = '"+mv_par02+"' "
	cQuery += " AND E1_FILORIG = '"+mv_par01+"' "
	cQuery += " AND E1_CLIENTE = '"+mv_par03+"' "
	cQuery += " AND E1_TIPO = 'ADT' "
	TcQuery cQuery new alias T02
	
	DbSelectArea("T02")
	oPrinter:Say ( nLin, 550, PadR(alltrim(TRANSFORM(T02->PAGO   ,"@E 999,999,999.99")),12,) ,  oFont1 )
	DbCloseArea("T02")
	oPrinter:Line( nLin+5, 05, nLin+5, 590)
		
	nLin := nLin + 20
	oPrinter:Say ( nLin, 020, "DESCONTOS CONCEDIDOS" ,  oFont4 )
		
	
	cQuery := " SELECT SUM(E1_DECRESC) DESCONTO FROM "+RetSqlName("SE1")+" E1 "
	cQuery += " WHERE E1.D_E_L_E_T_ = '' "
	cQuery += " AND E1_YCONTRA = '"+mv_par02+"' "
	cQuery += " AND E1_FILORIG = '"+mv_par01+"' "
	cQuery += " AND E1_CLIENTE = '"+mv_par03+"' "
	cQuery += " AND E1_TIPO = 'ADT' "
	cQuery += " AND E1_YACORDO = '2' "
	TcQuery cQuery  new Alias T03
	
	DbSelectArea("T03")
	oPrinter:Say ( nLin, 550, PadR(alltrim(TRANSFORM(T03->DESCONTO   ,"@E 999,999,999.99")),12,) ,  oFont1 )
	DbCloseArea("T03")
	oPrinter:Line( nLin+5, 05, nLin+5, 590)
		
	nLin := nLin + 20
			
	nTotal := 0
	
	T01->(DbCloseArea())
return


Static Function AjustaSX1(cPerg)

	PutSx1(cPerg, "01","Filial De"	,						  "","","mv_ch01","C",06,0,0,"G","","SM0_01","","","mv_par01","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Numero Contrato:",				  		  "","","mv_ch02","C",06,0,0,"G","","","","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "03","Cliente de:",						  "","","mv_ch03","C",09,0,0,"G","","SA1","","","mv_par03"," ","","","","","","","","","","","","","","","")
	
return
//--< fim de arquivo >----------------------------------------------------------------------
