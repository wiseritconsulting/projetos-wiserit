#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} $
(Cadastro de premissas)
@type function
@author Rayanne Meneses - rayannemeneses@mconsult.com.br
@since 21/10/2016
@version 1.0
/*/

User function SASP151()

Local oBrowse

// Instanciamento da Classe de Browse
oBrowse := FWMBrowse():New()

Private aRotina := MenuDef() //Chama rotina

// Defini��o da tabela do Browse
oBrowse:SetAlias('ZYA')

// Titulo da Browse
oBrowse:SetDescription('Premissa x Pontua��o')

// Opcionalmente pode ser desligado a exibi��o dos detalhes
//oBrowse:DisableDetails()
// Ativa��o da Classe
oBrowse:Activate()

Return NIL

/*/{Protheus.doc} ${Regras de neg�cio}
(cont�m as regras de neg�cio)
@type function
@author Rayanne Meneses
@since 24/10/2016
@version 1.0
@return ${oModel}, ${Objeto modelo de dados}
/*/
Static Function ModelDef()

// Cria a estrutura a ser usada no Modelo de Dados
Local oStruZYA := FWFormStruct( 1, 'ZYA' )
Local oStruZYB := FWFormStruct( 1, 'ZYB' )
Local aZYBRel       := {}
Local oModel // Modelo de dados que ser� constru�do

// Cria o objeto do Modelo de Dados
oModel := MPFormModel():New('COMP' )

// Adiciona ao modelo um componente de formul�rio
oModel:AddFields( 'ZYAMASTER', /*cOwner*/, oStruZYA)
oModel:AddGrid('ZYBDETAIL','ZYAMASTER',oStruZYB,/*bLinePre*/, /*bLinePost*/,/*bPre - Grid Inteiro*/,/*bPos - Grid Inteiro*/,/*bLoad - Carga do modelo manualmente*/)  //cOwner � para quem pertence

//Fazendo o relacionamento entre o Pai e Filho
aAdd(aZYBRel, {'ZYB_FILIAL','ZYA_FILIAL'} )
aAdd(aZYBRel, {'ZYB_CODPRE','ZYA_CODIGO'})

oModel:SetRelation("ZYBDETAIL", {{'ZYB_FILIAL', 'xFilial("ZYB")'}, {"ZYB_CODPRE", "ZYA_CODIGO"}}, ZYB->(IndexKey(1)))
oModel:GetModel('ZYBDETAIL'):SetUniqueLine({'ZYB_FILIAL','ZYB_CODPRE','ZYB_PONTO'})  //N�o repetir informa��es ou combina��es {"CAMPO1","CAMPO2","CAMPOX"}
oModel:SetPrimaryKey({'ZYB_FILIAL','ZYB_CODPRE','ZYB_PONTO'})

// Adiciona a descri��o do Modelo de Dados
oModel:SetDescription( 'Cadastros de Premissas' )

// Adiciona a descri��o do Componente do Modelo de Dados
oModel:GetModel('ZYAMASTER' ):SetDescription( 'Premissas' )
oModel:GetModel('ZYBDETAIL'):SetDescription('Pontua��es')

// Retorna o Modelo de dados
Return oModel

/*/{Protheus.doc} ViewDef
(Constru��o da interface)
@type function
@author Rayanne Meneses
@since 24/10/2016
@version 1.0
@return ${oView}, ${objeto da interface}
/*/

Static Function ViewDef()

// Cria um objeto de Modelo de dados baseado no ModelDef() do fonte informado
Local oModel := FWLoadModel( 'SASP151' )

// Cria a estrutura a ser usada na View
Local oStruZYA := FWFormStruct( 2, 'ZYA' )
Local oStruZYB := FWFormStruct( 2, 'ZYB' )

// Interface de visualiza��o constru�da
Local oView

// Cria o objeto de View
oView := FWFormView():New()

// Define qual o Modelo de dados ser� utilizado na View
oView:SetModel( oModel )

// Adiciona no nosso View um controle do tipo formul�rio
// (antiga Enchoice)
oView:AddField( 'VIEW_ZYA', oStruZYA, 'ZYAMASTER' )
oView:AddGrid('VIEW_ZYB', oStruZYB, 'ZYBDETAIL')

// Criar um "box" horizontal para receber algum elemento da view
oView:CreateHorizontalBox( 'SUPERIOR' , 50 )
oView:CreateHorizontalBox( 'INFERIOR' , 50 )

// Relaciona o identificador (ID) da View com o "box" para exibi��o
oView:SetOwnerView( 'VIEW_ZYA', 'SUPERIOR' )
oView:SetOwnerView( 'VIEW_ZYB', 'INFERIOR' )


// Retorna o objeto de View criado
Return oView

/*/{Protheus.doc} MenuDef
(Defini��o do Menu)
@type function
@author Rayanne Meneses
@since 24/10/2016
@version 1.0
@return ${aRotina}, ${Array com as rotinas do programa}
/*/
Static Function MenuDef()

aRotina := {}

ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PESQBRW"        	   OPERATION 1 ACCESS 0
ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.SASP151" OPERATION 2 ACCESS 0
ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.SASP151" OPERATION 3 ACCESS 0
ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.SASP151" OPERATION 4 ACCESS 0
ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.SASP151" OPERATION 5 ACCESS 0

Return aRotina