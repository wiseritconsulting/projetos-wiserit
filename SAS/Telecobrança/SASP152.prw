#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} $
(Cadastro de Classifica��o)
@type function
@author Rayanne Meneses - rayannemeneses@mconsult.com.br
@since 21/10/2016
@version 1.0
/*/

User function SASP152()

	Local oBrowse

	// Instanciamento da Classe de Browse
	oBrowse := FWMBrowse():New()

	Private aRotina := MenuDef() //Chama rotina

	// Defini��o da tabela do Browse
	oBrowse:SetAlias('ZYC')

	// Titulo da Browse
	oBrowse:SetDescription('Regras')

	// Opcionalmente pode ser desligado a exibi��o dos detalhes
	//oBrowse:DisableDetails()
	// Ativa��o da Classe
	oBrowse:Activate()

Return NIL

//---------------------------------FIM SASP152----------------------------------------------------

/*/{Protheus.doc} ${Regras de neg�cio}
(cont�m as regras de neg�cio)
@type function
@author Rayanne Meneses
@since 24/10/2016
@version 1.0
@return ${oModel}, ${Objeto modelo de dados}
/*/
Static Function ModelDef()

	// Cria a estrutura a ser usada no Modelo de Dados
	Local oStruZYC := FWFormStruct( 1, 'ZYC' )
	Local oModel // Modelo de dados que ser� constru�do

	// Cria o objeto do Modelo de Dados
	oModel := MPFormModel():New('COMP1' )

	// Adiciona ao modelo um componente de formul�rio
	oModel:AddFields( 'ZYCMASTER', /*cOwner*/, oStruZYC)

	//Setando a chave prim�ria da rotina
	oModel:SetPrimaryKey({'ZYC_FILIAL','ZYC_CODCLA'})


	// Adiciona a descri��o do Modelo de Dados
	oModel:SetDescription( 'Classifica��o' )

	// Adiciona a descri��o do Componente do Modelo de Dados
	oModel:GetModel('ZYCMASTER' ):SetDescription( 'Classifica��o' )

	// Retorna o Modelo de dados
Return oModel

//--------------------------------------FIM ModelDef--------------------------------------------------------

/*/{Protheus.doc} ViewDef
(Constru��o da interface)
@type function
@author Rayanne Meneses
@since 24/10/2016
@version 1.0
/*/

Static Function ViewDef()

	// Cria um objeto de Modelo de dados baseado no ModelDef() do fonte informado
	Local oModel := FWLoadModel( 'SASP152' )

	// Cria a estrutura a ser usada na View
	Local oStruZYC := FWFormStruct( 2, 'ZYC' )

	// Interface de visualiza��o constru�da
	Local oView

	// Cria o objeto de View
	oView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado na View
	oView:SetModel( oModel )

	// Adiciona no nosso View um controle do tipo formul�rio
	// (antiga Enchoice)
	oView:AddField( 'VIEW_ZYC', oStruZYC, 'ZYCMASTER' )

	// Criar um "box" horizontal para receber algum elemento da view
	oView:CreateHorizontalBox( 'SUPERIOR' , 100 )
	//oView:CreateHorizontalBox( 'INFERIOR' ,  )

	// Relaciona o identificador (ID) da View com o "box" para exibi��o
	oView:SetOwnerView( 'VIEW_ZYC', 'SUPERIOR' )
	//oView:SetOwnerView( 'VIEW_ZYC', 'INFERIOR' )

	// Retorna o objeto de View criado
Return oView

//-----------------------------------FIM ViewDef-------------------------------------------------------

/*/{Protheus.doc} MenuDef
(Defini��o do Menu)
@type function
@author Rayanne Meneses
@since 24/10/2016
@version 1.0
/*/
Static Function MenuDef()

	aRotina := {}

	ADD OPTION aRotina TITLE "Pesquisar"  ACTION "PESQBRW"         OPERATION 1 ACCESS 0
	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.SASP152" OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE "Gerar" ACTION "U_SASP152G()" OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE "Atualiza Cliente" ACTION "U_SASP1521()" OPERATION 3 ACCESS 0

Return aRotina

//-----------------------------------FIM MenuDef----------------------------------------------------------

User Function SASP1521
	
	Processa({|lEnd| SASP152A()})

Return

/*/{Protheus.doc} GararClass
(Gerar classifica��o automatica)
@type function
@author Rayanne Meneses
@since 25/10/2016
@version 1.0
/*/

User Function SASP152G()

	Local nTotPont := getMv("SA_QTCLSF") //Quantidade de pontos
	Local nTotClas  := 0
	Local nPeso := 0
	Local aArray := {}
	Local cClassif	:= Padr(0,1)

	//Pega a soma dos pesos de todas as premissa e divide pela quantidade de pontos
	//exempro soma de 9 pesos * 5 pontos - 45 - pontua��o maxima

	cQuery := " SELECT SUM(ZYA_PESO) [PESO] FROM "+RetSqlName("ZYA")+" WHERE
	cQuery += " D_E_L_E_T_ = '  ' AND ZYA_FILIAL = '"+xFilial("ZYA")+"' "

	dbUseArea(.t.,"TOPCONN",tcGenQry(,,cQuery),"T01",.t.,.t.)

	If !T01->(Eof())

		nPeso := T01->PESO

	EndIf

	T01->( dbCloseArea() )

	//Total de pontos
	nTotClas := nPeso*nTotPont //9*5 = 45

	cClassif := "1"

	//calcula o intervalo para cadas classifica��o
	While nTotClas > 0

		Aadd( aArray, { cClassif , if( (nTotClas+1)-nPeso == 1, 0, (nTotClas+1)-nPeso ) , nTotClas} )

		nTotClas := nTotClas-nPeso

		cClassif := PADR(Soma1( cClassif,, .T. ),1)

	EndDo //fim while

	cCodigo := GETSX8NUM("ZYC","ZYC_CODIGO")

	//alimento a tabela ZYC - Tabela de Classifica��o
	dbSelectArea("ZYC")
	dbSetOrder(2)
	ZYC->( DbSeek( xFilial("ZYC")+cCodigo ) )

	cUpd := "UPDATE "+RetSqlName("ZYC")+" SET D_E_L_E_T_='*' WHERE D_E_L_E_T_= ' ' "

	If TCSQLExec(cUpd) < 0
		Alert("Erro: "+cUpd)
		Return
	EndIf

	For nx := 1 to len(aArray)

		RecLock("ZYC", .T. )
		ZYC->ZYC_FILIAL 	:= xFilial('ZYC')
		ZYC->ZYC_CODCLA   	:= aArray[nx][1]
		ZYC->ZYC_CODIGO    	:= cCodigo
		ZYC->ZYC_DE			:= aArray[nx][2]
		ZYC->ZYC_ATE		:= aArray[nx][3]
		ZYC->ZYC_DATA   	:= dDatabase
		ZYC->(MsUnlock())

	Next nx

Return

//----------------------------------------------FIM SASP152G------------------------------------------------

/*/{Protheus.doc} SASP152A
(Atualiza Cliente)
@type function
@author Rayanne Meneses
@since 25/10/2016
@version 1.0
/*/

Static Function SASP152A()

	Local nDiasAtraso 	:= 0 //quantidades dias atrasou
	Local nQtdAtraso 	:= 0 //quantidade de vezes que atrasou
	Local nClieAh 		:= 0 //quantos anos � nosso cliente
	Local nVirgCont		:= 0 //virgencia do contrato
	Local nInadAnt 		:= 0 //inadiplencia cole��o anterior
	Local nValCont      := 0 //valor do contrato
	Local nInadAtu 		:= 0 //inadiplencia cole��o atual
	Local nPontos		:= 0 //pontua��o alcan�ada pelo cliente

	cQuery := " SELECT * FROM "+RetSqlName("ZYC")+" WHERE D_E_L_E_T_ = ' ' "

	dbUseArea(.t.,"TOPCONN",tcGenQry(,,cQuery),"T01",.t.,.t.)

	If T01->(Eof())
		MsgAlert("N�o h� dados, por favor efetuar gera��o de classifica��o.")
		Return
	EndIf

	T01->( dbCloseArea() )

	//Selecionar o cliente
	cQuery := " SELECT * FROM "+RetSqlName("SA1")+" WHERE D_E_L_E_T_ = '' "//AND A1_COD = '000978' AND A1_LOJA = '04' "

	dbUseArea(.t.,"TOPCONN",tcGenQry(,,cQuery),"T01",.t.,.t.)
	
	T01->( dbGoTop() )
	
	ProcRegua(T01->(recCount()))
	
	T01->( dbGoTop() )
	
	While !T01->( Eof() )
		
		IncProc('Processando... '+T01->A1_COD+" - "+T01->A1_LOJA+" - "+T01->A1_NOME)
		
		//Premissa 01 - Quantidades de Dias em Atraso - //Premissa 04 - Quantidade de Vezes que atrasou
		QtdDiasAtraso( @nDiasAtraso, T01->A1_COD, T01->A1_LOJA, @nQtdAtraso ) // Metodo responsavel por retornar a quantidade de dias que o clinte atrasou.

		//Premissa 03 - Cliente h�?
		nClieAh := ( Val( Year2Str( dDataBase ) ) - Val( Year2Str( if( !Empty( T01->A1_YCLIDSD ), T01->A1_YCLIDSD, Val( Year2Str( dDataBase ) ) ) ) ) ) + 1

		//Premissa 06 - Valor do Contrato
		nValCont := vlrContrato( T01->A1_COD, T01->A1_LOJA, Year2Str( dDataBase ) )

		//Premissa 4 - Virgencia do contrato
		nVirgCont := ( VAL( T01->A1_YVIGFIM ) - VAL( T01->A1_YVIGINI ) ) + 1

		//Premissa 05 - Inadiplencias cole��es passada %
		nInadAnt := InadiplCole(T01->A1_COD, T01->A1_LOJA, Year2Str( dDataBase ) , 'Ant'  )  /// Metodo responsavel por retornar inadiplencia

		//Premissa 07 - Inadiplencias cole��es atual %
		nInadAtu := InadiplCole( T01->A1_COD, T01->A1_LOJA, Year2Str( dDataBase ) , 'Atu'  )  /// Metodo responsavel por retornar inadiplencia

		//Metodo que ir� calculat a pontua��o do cliente e verificar e qual perfil do cliente...
		nPontos := CalcPerfil( nDiasAtraso, nQtdAtraso, nClieAh, nVirgCont, nValCont, nInadAnt, nInadAtu )

		//Verifico na tabela de classifica��o em qual perfil o cliente se encaixa
		cQuery := " SELECT * FROM "+RetSqlName("ZYC")+" WHERE D_E_L_E_T_ = ' ' AND ZYC_FILIAL = '"+xFilial("ZYC")+"'

		dbUseArea(.t.,"TOPCONN",tcGenQry(,,cQuery),"_ZYC",.t.,.t.)

		While !_ZYC->(Eof())

			if nPontos >= _ZYC->ZYC_DE .And. nPontos <= _ZYC->ZYC_ATE

				dbSelectArea("ZYD")
				DbSetOrder(3)

				If dbSeek( xFilial( "ZYD" )+T01->A1_COD+T01->A1_LOJA )

					cUpd := " UPDATE "+RetSqlName("ZYD")+" SET D_E_L_E_T_= '*' WHERE "
					cUpd += " ZYD_CODCLI = '"+T01->A1_COD+"' AND ZYD_LOJA = '"+T01->A1_LOJA+"' AND ZYD_FILIAL = '"+xFilial("ZYD")+"' "

					If TCSQLExec(cUpd) < 0
						Alert("Erro: "+cUpd)
						Return
					EndIf

					RecLock("ZYD", .T. )
					//ZYD->ZYD_CODIGO	:= GETSXENUM("ZYD","ZYD_CODIGO")
					ZYD->ZYD_CLASSI := _ZYC->ZYC_CODCLA
					ZYD->ZYD_CODCLI := T01->A1_COD
					ZYD->ZYD_LOJA	:= T01->A1_LOJA
					ZYD->ZYD_PONTUA	:= nPontos
					ZYD->ZYD_DATA	:= dDataBase
					ZYD->ZYD_HORA	:= Time()
					ZYD->(MsUnlock())

				Else

					dbSeek( xFilial( "ZYD" )+T01->A1_COD+T01->A1_LOJA )

					RecLock("ZYD", .T. )
					//ZYD->ZYD_CODIGO	:= GETSXENUM("ZYD","ZYD_CODIGO")
					ZYD->ZYD_CLASSI := _ZYC->ZYC_CODCLA
					ZYD->ZYD_CODCLI := T01->A1_COD
					ZYD->ZYD_LOJA	:= T01->A1_LOJA
					ZYD->ZYD_PONTUA	:= nPontos
					ZYD->ZYD_DATA	:= dDataBase
					ZYD->ZYD_HORA	:= Time()
					ZYD->(MsUnlock())

				EndIf

				//Pego a classifica��o e salvo na tabela de log's e no cadastro de cliente
				cUpd := " UPDATE "+RetSqlName("SA1")+" SET A1_TIPPRFL = '"+_ZYC->ZYC_CODCLA+"' WHERE D_E_L_E_T_= ' ' AND "
				cUpd += " A1_COD = '"+T01->A1_COD+"' AND A1_LOJA = '"+T01->A1_LOJA+"' AND A1_FILIAL = '"+xFilial("SA1")+"' "

				If TCSQLExec(cUpd) < 0
					Alert("Erro: "+cUpd)
					Return
				EndIf

				EXIT
			endif

			_ZYC->( dbSkip() )

		EndDo

		_ZYC->( dbCloseArea() )

		T01->( dbSkip() )
	EndDo

	T01->( dbCloseArea() )

Return

//------------------------------------------FIM SASP152A---------------------------------------------------------------------

/*/{Protheus.doc} QtdDiasAtraso
//TODO Metodo responsavel por retornar a quantidade de dias que o clinte atrasou.
@author Rayanne Meneses
@since 26/10/2016
@version undefined
@type function
/*/
Static Function QtdDiasAtraso( nDiasAtraso, cCliente, cLoja, nQtdAtraso )

	Local nDias := 0 //Soma dos dias de atraso
	Local nPgAtraso := 0 //Pago em atraso
	Local nVencido := 0 //Pago em atraso
	Local dPeriIni := FirstDate(MonthSub( dDatabase, 2 ) )
	Local dPeriFim := LastDate( MonthSub(dDatabase,1))

	//Fa�o uma query na SE1, buscando pelos titulos do cliente que est�o atrasados ou que tenham sido pago em atraso e somo,
	//para retornar a quantidade de dias

	//Primeiro somo os titlos pago em atraso
	cQuery := " SELECT SUM( DATEDIFF(day,E1_VENCREA, E1_BAIXA) )[PGTATRASO], COUNT(*) [QTDATRASO] FROM "+RetSqlName("SE1")+" WHERE D_E_L_E_T_ = '' AND E1_CLIENTE = '"+cCliente+"' AND "
	cQuery += " E1_LOJA = '"+cLoja+"' AND E1_VENCREA BETWEEN '"+Dtos( dPeriIni )+"' AND '"+DtoS( dPeriFim )+"' AND E1_BAIXA > E1_VENCREA AND E1_TIPO <> 'NCC'

	dbUseArea(.t.,"TOPCONN",tcGenQry(,,cQuery),"_SE1",.t.,.t.)

	If !_SE1->( Eof() )

		nDiasAtraso  := _SE1->PGTATRASO
		nQtdAtraso := _SE1->QTDATRASO

	EndIf

	_SE1->( dbCloseArea() )

	//Depois somo os dias de atraso dos vencidos e n�o pago
	cQuery := " SELECT E1_VENCREA FROM "+RetSqlName("SE1")+" WHERE D_E_L_E_T_ = '' AND E1_CLIENTE = '"+cCliente+"' AND "
	cQuery += " E1_LOJA = '"+cLoja+"' AND E1_VENCREA BETWEEN '"+Dtos( dPeriIni )+"' AND '"+DtoS( dPeriFim )+"' AND E1_SALDO > 0 AND E1_TIPO <> 'NCC' "

	dbUseArea(.t.,"TOPCONN",tcGenQry(,,cQuery),"_SE1",.t.,.t.)

	While !_SE1->( Eof() )

		nDiasAtraso += DateDiffDay( STOD(_SE1->E1_VENCREA), dPeriFim ) //diferen�a de datas
		nQtdAtraso++

		_SE1->( dbSkip() )

	EndDo

	_SE1->( dbCloseArea() )

Return

//------------------------------------------FIM QtdDiasAtraso--------------------------------------------------------------------

/*/{Protheus.doc} InadiplCole
//TODO Inadiplencia Contrato
@author Rayanne Meneses
@since 26/10/2016
@version undefined
@type function
/*/
Static Function InadiplCole( cCliente, cLoja, cAno, cFlag )

	Local nSE1Venc := 0
	Local nVlVencBai := 0
	Local nRetorno := 0
	Local dPeriIni := FirstYDate(dDatabase)
	Local dPeriFim := LastDate( MonthSub(dDatabase,1) )

	cQuery := " SELECT SUM( E1_VALOR ) [VALOR] FROM "+RetSqlName("SE1")+" WHERE D_E_L_E_T_ = '' AND "
	cQuery += " E1_CLIENTE = '"+cCliente+"' AND E1_LOJA = '"+cLoja+"' AND E1_TIPO <> 'NCC' "

	If cFlag == 'Ant'
		cQuery += " AND E1_VENCREA < '"+Dtos( LastYDate(  YearSub( dDatabase,1) ) )+"' "
	Else
		cQuery += " AND E1_VENCREA BETWEEN '"+Dtos(dPeriIni)+"' AND '"+Dtos(dPeriFim)+"' "
	EndIf

	dbUseArea(.t.,"TOPCONN",tcGenQry(,,cQuery),"T02",.t.,.t.)

	While !T02->( Eof() )

		nSE1Venc += T02->VALOR

		T02->( dbSkip() )

	EndDo

	T02->( dbCloseArea() )

	cQuery := " SELECT SUM( E1_SALDO ) [SALDO] FROM "+RetSqlName("SE1")+" WHERE D_E_L_E_T_ = '' AND "
	cQuery += " E1_CLIENTE = '"+cCliente+"' AND E1_LOJA = '"+cLoja+"' AND E1_SALDO > 0 AND E1_TIPO <> 'NCC' "

	If cFlag == 'Ant'
		cQuery += " AND E1_VENCREA < '"+Dtos( LastYDate(  YearSub( dDatabase,1) ) )+"' "
	Else
		cQuery += " AND E1_VENCREA BETWEEN '"+Dtos(dPeriIni)+"' AND '"+Dtos(dPeriFim)+"' "
	endIf

	dbUseArea(.t.,"TOPCONN",tcGenQry(,,cQuery),"_SE1",.t.,.t.)

	If !_SE1->( Eof() )
		nVlVencBai += _SE1->SALDO
	EndIf

	_SE1->( dbCloseArea() )

	nRetorno := ( nVlVencBai / nSE1Venc ) * 100

Return nRetorno

//----------------------------------------------------FIM InadiplCole-----------------------------------------------------

/*/{Protheus.doc} vlrContrato
//TODO retorna valor do contrato.
@author Rayanne Meneses
@since 26/10/2016
@version undefined
@param cCliente, characters, descricao
@param cLoja, characters, descricao
@param cAno, characters, descricao
@type function
/*/
Static Function vlrContrato( cCliente, cLoja, cAno )

	Local nValCont := 0

	cQuery := " SELECT ZZ1_NUMERO, SUM( ZZ1_VLRTOT - ZZ1_VLRDSC - ZZ1_QNTDEV ) [VLCONTR] FROM "+RetSqlName("ZZ1")+" ZZ1 "
	cQuery += " INNER JOIN ZZ0010 Z0 ON ZZ0_FILIAL = ZZ1_FILIAL AND ZZ0_NUMERO = ZZ1_NUMERO AND ZZ0_CLIENT = ZZ1_CLIENT AND ZZ0_LOJA = ZZ1_LOJA "
	cQuery += " INNER JOIN SF4010 SF4 ON SF4.F4_CODIGO = ZZ1.ZZ1_TES AND SF4.F4_FILIAL = SUBSTRING(ZZ1.ZZ1_FILIAL,1,2) AND SF4.D_E_L_E_T_='' "
	cQuery += " WHERE ZZ1.D_E_L_E_T_ = ' ' AND ZZ1.ZZ1_CLIENT = '"+cCliente+"' AND ZZ1.ZZ1_LOJA = '"+cLoja+"' AND "
	cQuery += " Z0.ZZ0_ANOCOM = '"+cAno+"' "
	cQuery += " AND SF4.F4_DUPLIC='S' GROUP BY ZZ1_NUMERO "

	dbUseArea(.t.,"TOPCONN",tcGenQry(,,cQuery),"T03",.t.,.t.)

	While !T03->(Eof())

		nValCont += T03->VLCONTR

		T03->( dbSkip() )

	EndDo

	T03->( dbCloseArea() )

Return nValCont

//------------------------------------------------------FIM vlrContrato----------------------------------------------------------------

/*/{Protheus.doc} CalcPerfil
//TODO Rotina para calculo do perfil do cliente.
@author Rayanne Meneses
@since 27/10/2016
@version v01
@param nDiasAtraso, numeric, descricao
@param nQtdAtraso, numeric, descricao
@param nClieAh, numeric, descricao
@param nValCont, numeric, descricao
@param nVirgCont, numeric, descricao
@param nInadAnt, numeric, descricao
@param nInadAtu, numeric, descricao
@type function
/*/
Static Function CalcPerfil( nDiasAtraso, nQtdAtraso, nClieAh, nVirgCont, nValCont, nInadAnt, nInadAtu )

Local nSomaPonto := 0

//Selecionar premissas cadastradas
cQuery := " SELECT * FROM "+RetSqlName("ZYA")+" ZYA, "+RetSqlName("ZYB")+" ZYB WHERE ZYA.D_E_L_E_T_ = ' ' AND ZYB.D_E_L_E_T_ = ' ' AND
cQuery += " ZYA_CODIGO = ZYB_CODPRE AND ZYA_FILIAL = '"+xFilial("ZYA")+"' AND ZYA_FILIAL = ZYB_FILIAL "

dbUseArea(.t.,"TOPCONN",tcGenQry(,,cQuery),"_ZYA",.t.,.t.)

While !_ZYA->( Eof() )

	If _ZYA->ZYA_CODIGO = '000001' // Premissa 01 - qtd dias atraso

		if nDiasAtraso >= _ZYA->ZYB_DE .and. nDiasAtraso <= _ZYA->ZYB_ATE

			If _ZYA->ZYB_CALC == "T"
				nSomaPonto += _ZYA->ZYB_PONTO * _ZYA->ZYA_PESO
			EndIf

		EndIf

	ElseIf _ZYA->ZYA_CODIGO = '000002' // Premissa 02- qtd de vezes q atrasou

		if nQtdAtraso >= _ZYA->ZYB_DE .and. nQtdAtraso <= _ZYA->ZYB_ATE

			If _ZYA->ZYB_CALC == "T"
				nSomaPonto += _ZYA->ZYB_PONTO * _ZYA->ZYA_PESO
			EndIf

		EndIf

	ElseIf _ZYA->ZYA_CODIGO = '000003' // Premissa 03 - h� quantos anos � cliente

		if nClieAh >= _ZYA->ZYB_DE .and. nClieAh <= _ZYA->ZYB_ATE

			If _ZYA->ZYB_CALC == "T"
				nSomaPonto += _ZYA->ZYB_PONTO * _ZYA->ZYA_PESO
			EndIf

		EndIf

	ElseIf _ZYA->ZYA_CODIGO = '000004' // Premissa 04 - virgencia contrato

		if nVirgCont >= _ZYA->ZYB_DE .and. nVirgCont <= _ZYA->ZYB_ATE

			If _ZYA->ZYB_CALC == "T"
				nSomaPonto += _ZYA->ZYB_PONTO * _ZYA->ZYA_PESO
			EndIf

		EndIf

	ElseIf _ZYA->ZYA_CODIGO = '000005' // Premissa 05 - inadiplencia cole��o passada

		if nInadAnt >= _ZYA->ZYB_DE .and. nInadAnt <= _ZYA->ZYB_ATE

			If _ZYA->ZYB_CALC == "T"
				nSomaPonto += _ZYA->ZYB_PONTO * _ZYA->ZYA_PESO
			EndIf

		EndIf

	ElseIf _ZYA->ZYA_CODIGO = '000006' // Premissa 06 - valor contrato

		if nValCont >= _ZYA->ZYB_DE .and. nValCont <= _ZYA->ZYB_ATE

			If _ZYA->ZYB_CALC == "T"
				nSomaPonto += _ZYA->ZYB_PONTO * _ZYA->ZYA_PESO
			EndIf

		EndIf

	ElseIf _ZYA->ZYA_CODIGO = '000007' // Premissa 07 - inadiplencia cole��o atual

		if nInadAtu >= _ZYA->ZYB_DE .and. nInadAtu <= _ZYA->ZYB_ATE

			If _ZYA->ZYB_CALC == "T"
				nSomaPonto += _ZYA->ZYB_PONTO * _ZYA->ZYA_PESO
			EndIf

		EndIf

	EndIf

	_ZYA->( dbSkip() )

EndDo

_ZYA->( dbCloseArea() )

Return nSomaPonto

//------------------------------------------------------FIM CalcPerfil----------------------------------------------------------------



































