#Include 'Protheus.ch'
#Include 'topconn.ch'

/*/{Protheus.doc} ImpContato
(Fun��o para povoar a tebela de contatos pegando os dados do cadastro de cliente)
@type function
@author Rayanne Meneses
@since 12/10/2016
@version 1.0
/*/

User FuncTion SASP154()

Local cArq    := "C:\import.txt"
Local cLinha  := ""
Local lPrim   := .T.
Local aCampos := {}
Local aDados  := {}


Private aErro := {}

FT_FUSE(cArq)
ProcRegua(FT_FLASTREC())
FT_FGOTOP()

While !FT_FEOF()

	IncProc("Lendo arquivo texto...")
	cLinha := FT_FREADLN()

	cCod 	:= substr(cLinha,1,21)
	cLoja 	:= substr(cLinha,22,13)
	cemail 	:= substr(cLinha,35,101)
	ctel 	:= substr(cLinha,135,20)

	If !Empty(cCod) .And. !Empty(cLoja) .And. !Empty(cemail) .And. !Empty(ctel)

		aAdd( aDados, { AllTrim(cCod), AllTrim(cLoja), AllTrim(cemail), AllTrim(ctel) } )

	EndIF

	FT_FSKIP()
EndDo

MsAguarde({|| ( impCon1(  aDados ) )},;
		"Incluido Contatos",;
		"Aguarde... atualizando informa��es.")

Return

Static Function impCon1( aDados )

Local cCod 	  := ""
Local cLoja   := ""
Local cemail  := ""
Local ctel 	  := ""
Local aContato := {}
Local aEndereco := {}
Local aTelefone := {}
Local aAuxDados := {}
Local cCodigo := 1
Local cQuery := "", _nx := 0
Private lMsErroAuto := .F.


for _nx := 1 to len(aDados)

	cQuery := " SELECT * FROM "+RetSqlName("SA1")+" SA1 WHERE "
	cQuery += " D_E_L_E_T_ = ' ' AND A1_FILIAL = '"+xFilial("SA1")+"' AND A1_COD = '"+aDados[_nx][1]+"' AND A1_LOJA = '"+aDados[_nx][2]+"' "

	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery),"T01", .F., .T.)

	While !T01->( Eof() )

		cCodigo++

		AAdd(aContato,{"U5_FILIAL", xFilial("SU5"),Nil})
		AAdd(aContato,{"U5_CODCONT",strzero(cCodigo,6), Nil})
		AAdd(aContato,{"U5_CONTAT",T01->A1_NREDUZ, Nil})
		AAdd(aContato,{"U5_EMAIL",aDados[_nx][3], Nil})

		AAdd(aAuxDados, {"AGB_TIPO", "2", Nil})
		AAdd(aAuxDados, {"AGB_PADRAO", "1", Nil})
		AAdd(aAuxDados, {"AGB_DDD", SubStr( aDados[_nx][4], 1, 3) , Nil} )
		AAdd(aAuxDados, {"AGB_TELEFO", SubStr( aDados[_nx][4], 4, Len( aDados[_nx][3] ) ) , Nil})

		AAdd(aTelefone, aAuxDados)


		aAuxDados := {}

		AAdd(aAuxDados, {"AGA_TIPO", "2", Nil})
		AAdd(aAuxDados, {"AGA_PADRAO", "1", Nil})
		AAdd(aAuxDados, {"AGA_END", T01->A1_END, Nil})
		AAdd(aAuxDados, {"AGA_CEP", T01->A1_CEP, Nil})
		AAdd(aAuxDados, {"AGA_BAIRRO", T01->A1_BAIRRO, Nil})
		AAdd(aAuxDados, {"AGA_MUNDES", T01->A1_MUN, Nil})
		AAdd(aAuxDados, {"AGA_EST", T01->A1_EST, Nil})


		AAdd(aEndereco, aAuxDados)


		MSExecAuto({|x,y,z,a,b|TMKA070(x,y,z,a,b)},aContato,3,aEndereco,aTelefone, .F.)


		If lMsErroAuto
		    //MsgStop("Erro na grava��o do contato")
		    MostraErro()
		EndIf



		T01->( dbSkip() )

	EndDo

	T01->( DBCloseArea() )

Next _nx

Return
//
//
////Tabela de Contatos - SU5
////Tabela de Clientes - SA1
//
//cQuery := " SELECT * FROM VW_TELECOBRANCAS "
//
//dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery),T01, .F., .T.)
//
//While !T01->(Eof())
//
//EndDo
//
//T01->(dbCloseArea())
//
//cQuery := " SELECT * FROM "+RetSqlName("SA1")+" SA1 WHERE "
//cQuery += " A1.D_E_L_E_T_ = ' ' AND A1_FILIAL = '"+xFilial("SA1")+"' "
//
//dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery),T01, .F., .T.)
//
//While T01->( Eof() )
//
//	cCodigo++
//
//	DbSelectArea("SU5")
//	dbSetOrder(1)
//	SU5->( dbSkip( xFilial("SU5")+cCodigo ) )
//	RecLock("SU5",.T.)
//		SU5->U5_FILIAL  := xFilial("SU5")
//		SU5->U5_CODCONT := cCodigo
//		SU5->U5_CONTAT  := T01->A1_NREDUZ
//		SU5->U5_EMAIL 	:= T01->A1_EMAIL
//	MsUnLock()
//
//	DbSelectArea("AGB")
//	dbSetOrder(1)
//	SU5->( dbSkip( xFilial("AGB")+"SU5"+cCodigo ) )
//	RecLock("AGB",.T.)
//		AGB->AGB_FILIAL  := xFilial("AGB")
//		AGB->AGB_ENTIDA  := "SU5"
//		AGB->AGB_CODCONT := cCodigo
//		AGB->AGB_TIPO  	 := T01->A1_NREDUZ
//		AGB->AGB_PADRAO  := 2
//		AGB->AGB_DDD 	 := T01->A1_DDD
//		AGB->AGB_TELEFO  := T01->A1_TEL
//	MsUnLock()
//
//	T01->( dbSkip() )
//
//EndDo
//
//T01->( DBCloseArea() )
//
Return