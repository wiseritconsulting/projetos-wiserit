#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} $
(Gatilho atendimento receptivo)
@type function
@author Rayanne Meneses - rayannemeneses@mconsult.com.br
@since 13/11\s/2016
@version 1.0
/*/

User function SASP133()

Local aArea 	:= GetArea()

//Verificar se � atendimento receptivo
If Empty( aCols[1][1] )
	aCols := {}

Else

	for nx := 1 to Len(aCols)
		
		If aCols[nx][8] > 0
			nJur := U_SASP155( , , aCols[nx][33] , aCols[nx][8], )
		
			aCols[nx][11] := Round( nJur, 2)  //Round( ( aCols[nx][33] * (0.03/100) * aCols[nx][8] ) ,2)
		EndIf
		
	Next

EndIf

IF ACF_OPERA == '1'

	cQuery := " SELECT * FROM "+RetSqlName("SE1")+" WHERE D_E_L_E_T_ = ' ' AND "
	cQuery += " E1_CLIENTE = '"+ACF_CLIENTE+"' AND E1_LOJA = '"+ACF_LOJA+"' AND E1_FILIAL = '"+xFilial("SE1")+"' AND "
	cQuery += " E1_VENCREA >= '"+DtoS( dDataBase )+"' AND E1_SALDO > 0  ORDER BY E1_PREFIXO, E1_NUM, E1_PARCELA "

	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery),"T01", .F., .T.)

	While !T01->( Eof() )

//		dbSelectArea("SK1")
//		dbSetOrder(1)
//		IF !dbSeek( xFilial("SK1")+T01->E1_PREFIXO+T01->E1_NUM+T01->E1_PARCELA+T01->E1_TIPO )
//			RecLock("SK1", .T. )
//				SK1->K1_FILIAL		:= xFilial('SK1')
//				SK1->K1_PREFIXO   	:= T01->E1_PREFIXO
//				SK1->K1_NUM    		:= T01->E1_NUM
//				SK1->K1_PARCELA		:= T01->E1_PARCELA
//				SK1->K1_TIPO		:= T01->E1_TIPO
//				SK1->K1_OPERAD   	:= ACF_OPERAD
//				SK1->K1_VENCTO  	:= Stod( T01->E1_VENCTO )
//				SK1->K1_VENCREA   	:= Stod(  T01->E1_VENCREA )
//				SK1->K1_CLIENTE  	:= T01->E1_CLIENTE
//				SK1->K1_LOJA   		:= T01->E1_LOJA
//				SK1->K1_NATUREZ   	:= T01->E1_NATUREZ
//				SK1->K1_PORTADO  	:= T01->E1_PORTADO
//				SK1->K1_SITUACA   	:= T01->E1_SITUACA
//				SK1->K1_FILORIG   	:= T01->E1_FILORIG
//				SK1->K1_SALDEC   	:= 100000 - T01->E1_SALDO
//				SK1->K1_SALDO	   	:= T01->E1_SALDO
//			SK1->(MsUnlock())
//
//		EndIf

		aAdd( aCols, { 	T01->E1_PREFIXO,; //1
						T01->E1_NUM,; //2
						T01->E1_PARCELA,; //3
						T01->E1_TIPO,; //4
						"",; //5
						stod(T01->E1_VENCTO),; //6
						Stod( T01->E1_VENCREA ),; //7
						0,; //8
						T01->E1_VALOR,; //9
						T01->E1_SALDO,; //10
						0,; //11
						0,; //12
						CTOD("  /  /    "),; //13
						0,; //14
						0,; //15
						"",; //16
						"",; //17
						"",; //18
						T01->E1_SITUACA,; //19
						"",; //20
						0,; //21
						0,; //22
						T01->E1_NATUREZ,; //23
						Posicione( "SED", 1, xFilial("SED")+T01->E1_NATUREZ, "ED_DESCRIC" ),; //24
						Posicione("SX5",1,xFilial("SX5")+"05"+ T01->E1_TIPO,"X5_DESCRI"),; //25
						T01->E1_NUMBCO,; //26
						0,; //27
						0,; //28
						0,; //29
						0,; //30
						0,; //31
						"",; //32
						T01->E1_SALDO,; //33
						"2",; //34
						"01",; //35
						0,; //36
						0,; //37
						"ACG",; //38
						0,; //39
						.F. } ) //40

		T01->( dbSkip() )

	EndDo

	T01->( dbCloseArea() )
	
	oGetTlc:oBrowse:Refresh()
	
ENDIF

RestArea(aArea)

Return