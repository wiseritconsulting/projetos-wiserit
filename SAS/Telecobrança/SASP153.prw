#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} $
(Cadastro de premissas)
@type function
@author Rayanne Meneses - rayannemeneses@mconsult.com.br
@since 21/10/2016
@version 1.0
/*/

User function SASP153()

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Declaracao de Variaveis                                             �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

Private cCadastro := "Cria mBrowse"

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Monta um aRotina proprio                                            �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

Private aRotina := { {"Pesquisar"  ,"AxPesqui",0,1},;
					   {"Visualizar" ,"AxVisual",0,2},;
				       {"Incluir"     ,"AxInclui",0,3},;
				       {"Alterar"     ,"AxAltera",0,4},;
				       {"Excluir"     ,"AxExclui",0,5} }

Private cString := "ZYE"

dbSelectArea("ZYE")
dbSetOrder(1)

dbSelectArea(cString)

mBrowse( 6,1,22,75,cString)

Return
