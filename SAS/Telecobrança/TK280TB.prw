#Include 'Protheus.ch'
#Include 'Rwmake.ch'
#Include 'Topconn.ch'

User Function TK280TB()

Local aBotao := {}

Aadd( aBotao, { "POSCLI"	, "Perfil", {|| infPerf() }			,"Perfil"		,	.T. } )

Return( aBotao )


Static function infPerf( )

Local cMsg 		:= ""
Local cPerfil 	:= ""
Local cDescri 	:= ""

if Empty(A1_COD)
	Alert("N�o h� cliente selecionado!")
	Return
EndIf

cQuery := " SELECT A1_TIPPRFL FROM "+RetSqlName("SA1")+" WHERE D_E_L_E_T_ = ' ' AND A1_COD = '"+A1_COD+"' AND A1_LOJA = '"+A1_LOJA+"' "

dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery),"T01", .F., .T.)

cPerfil := 	T01->A1_TIPPRFL

IF !T01->(Eof())

	If cPerfil == '1'
		cDescri := "Perfil A"
	elseIf cPerfil == '2'
		cDescri := "Perfil B"
	elseIf cPerfil == '3'
		cDescri := "Perfil C"
	elseIf cPerfil == '4'
		cDescri := "Perfil D"
	elseIf cPerfil == '5'
		cDescri := "Perfil E"
	elseIf cPerfil == '6'
		cDescri := "Perfil F"
	EndIf

cMsg := OemToAnsi( cDescri ) + chr(13)

alert( cMsg )

Else

	alert( "N�o a perfil cadastrado..." )

EndIf

T01->( dbCloseArea() )

Return