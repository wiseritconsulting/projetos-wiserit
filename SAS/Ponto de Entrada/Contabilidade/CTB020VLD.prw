#include 'protheus.ch'
#include 'parmtype.ch'
#include "vkey.ch"
#include "topconn.ch"


user function CTB020VLD()
	Local lRet := .T.
	
		If __cUserId $ GETMV("SA_USRCTB")
			lRet := .T.
		Else
			MsgInfo("Voc� n�o tem permiss�o para incluir/alterar/excluir um plano de conta.", "SA_USRCTB / CTB020VLD")
			lRet := .F.
		EndIf
return lRet