#Include 'Protheus.ch'

/*/{Protheus.doc} MS520VLD 
Exclus�o da Nota Fiscal de Saida
Obs: Caso o par�metro SA_USUEXC estiver preenchido com o c�digo do usu�rio corrente, 
a nota poder� ser exclu�da
@author Diogo
@since 09/11/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function MS520VLD()
	Local lRet	:= .T.
	Local aArea	:= GetArea()

	If !empty(SF2->F2_YCONTRA) .and. ;
		Alltrim(SuperGetMv("SA_USUEXC",.F.,"")) <> Alltrim(RetCodUsr())
		MsgBox("Nota n�o poder� ser exclu�da pois foi originada do Contrato")
		lRet:= .F.
	Endif

	if SF2->F2_YSTAUT != "A"
		lRet:= .F.
		MsgInfo('Documento ainda n�o liberado para exclus�o!!','Restricao')
	endif
	
	RestArea(aArea)
Return lRet