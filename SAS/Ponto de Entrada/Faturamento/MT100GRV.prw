#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} MT100GRV
Valida��o da exclus�o do Documento de Entrada
Obs: Caso o par�metro SA_USUEXC estiver preenchido com o c�digo do usu�rio corrente, 
a nota poder� ser exclu�da

@author Diogo
@since 09/11/2015
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User function MT100GRV()

	Local lExp01 := PARAMIXB[1]
	Local lRet := .T.
	 	
	If lExp01 //Exclus�o
		If !empty(SF1->F1_YCONTRA) .and. ;
			Alltrim(SuperGetMv("SA_USUEXC",.F.,"")) <> Alltrim(RetCodUsr())
			MsgBox("Nota n�o poder� ser exclu�da pois foi originada do Contrato")
			lRet:= .F.
		Endif
	Endif
	
return lRet