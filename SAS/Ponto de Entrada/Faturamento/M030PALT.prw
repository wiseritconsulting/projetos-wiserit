// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : M030PALT
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Rog�rio J�come            | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 29/05/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo
Permite a manuten��o de dados armazenados em .

@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     29/05/2016
/*/
//------------------------------------------------------------------------------------------
user function M030PALT()
	//--< vari�veis >---------------------------------------------------------------------------
	Local aArea  := GetArea()
	Local nOpcao := PARAMIXB[1]
	//Local lRet := .F.
	
	IF nOpcao == 1 //Se na altera��o for o usu�rio confirmar e n�o cancelar
		//__CINTERNET vari�vel global que guada se P.E foi chamado automaticamente ou n�o. Utilizada para o controle do WS do CRM
		IF !empty(SA1->A1_YCODGLO) .AND. !empty(SA1->A1_YGUID) .AND. __CINTERNET == NIL //S� chama o WS se caso o C�digo Global do CRM e o Guid estiverem preenchidos.
			U_SASP065()//Chama a fun��o que chama o WS do CRM
		//lRet := .T.
		ENDIF
	
	ENDIF
	RestArea( aArea )
	
return
//--< fim de arquivo >----------------------------------------------------------------------
