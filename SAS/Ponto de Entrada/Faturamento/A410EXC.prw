#INCLUDE "rwmake.ch"

// Exclus�o pedido venda
// Valida��o se pode excluir o pedido  

User Function A410EXC()

	Local bRet := .T.
	
	if !(__cUserID $ GetMV("SA_ALTPED"))
		bRet := .F.
		Msginfo("Usu�rio sem permiss�o para excluir o pedido ","Permissao de Acesso")
	endif

  If !empty(SC5->C5_YCONTRA) .and. ;
 	!(Alltrim(RetCodUsr()) $ SuperGetMv("SA_USUEXC",.F.,"")) .and. FUNNAME()=="MATA410"//Alltrim(SuperGetMv("SA_USUEXC",.F.,"")) <> Alltrim(RetCodUsr()) 
	bRet := .F.
    MsgBox("Pedido n�o poder� ser exclu�do devido ter sido originado do Contrato")	        
  EndIf
	
/*	_cEmail 	:= 'rafaelpinheiro@aridesa.com.br'
	cAssunto 	:= 'Exclus�o de Pedido de Venda: ' + SC5->C5_NUM
	cMsg 		:= 'Pedido de Venda Excluido pelo Usu�rio: '+ __cUserID
	
	U_SEndMail(_cEmail,cAssunto,cMsg,"")*/
	
	//log de inclusao de pedido solicitado pelo atendimento
/*	IF (u_ProcPilha("YSZ1002") .OR. u_ProcPilha("SASP018") .or. u_ProcPilha("CADSZ5"))
	
	ELSE
		U_SASP021(cFilAnt,SC5->C5_NUM,dDataBase,"E","PVA","")
	ENDIF	
	*/
Return bRet
  