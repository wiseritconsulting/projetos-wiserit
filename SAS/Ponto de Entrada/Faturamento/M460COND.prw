#Include 'Protheus.ch'

/*/{Protheus.doc} M460COND
Data inicial para a condi��o de pagamento
Caso seja de contrato, considerar cabe�alho do Contrato
@author Diogo
@since 01/10/2014
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
/*
//DESABILITADO
User Function M460COND()

dDataRet:= dDataBase

if alltrim(funname()) == "CADSZ5"
	dDataRet := dDatSZ5
endif

Return(dDataRet)
*/