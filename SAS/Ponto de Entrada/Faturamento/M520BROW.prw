#Include 'Protheus.ch'

/* +----------------------------------------------------------------
   | PROGRAMA: M520BROW						DATA: 03/03/2015 
   +----------------------------------------------------------------
   | AUTOR: RAFAEL PINHEIRO					MODULO: FATURAMENTO
   +----------------------------------------------------------------
   | OBJETIVO: PONTO DE ENTRADA UTILIZADO PARA INCLUIR ITENS AO
   |           MENU DA ROTINA DE EXCLUSAO DE NOTA FISCAL.
   +---------------------------------------------------------------*/

User Function M520BROW

	Local cAprovad := GETMV("SA_EXCLNF")

	AADD(aRotina, { "Solicitação","U_SASP010()", 0 , 5} )

	if __cuserid $ cAprovad
		AADD(aRotina, { "Liberação"  ,"U_SASP011()", 0 , 5} )
	end if

Return



