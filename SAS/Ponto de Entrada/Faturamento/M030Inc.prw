#include "protheus.ch"                                                  
#INCLUDE "colors.ch"
#include "topconn.ch"                
#include "rwmake.ch"        
#include "fivewin.ch"
#include "tcbrowse.ch"

//Francisco Valdeni
//04/12/2014
//Envia WorkFlow avisando de um novo cliente cadastrado
// Ari de S� (Solicitado por Aline)

*----------------------
User Function M030Inc()
*----------------------

Local cMsg		:= "" 
Local _CRLF 	:= chr(13)+chr(10)
Local _cEmail   := GetMv("SA_WFCLI")
Local cCodCli	:= SA1->A1_COD+" / "+SA1->A1_LOJA
Local cBloq		:= IIF(SA1->A1_MSBLQL=="1","Sim","N�o")
Local cAssunto	:= "Novo cliente cadastrado"

cMsg += '<html>'   //Monta corpo do e-mail em HTML
cMsg += '<head>'
cMsg += '<title></title>'
cMsg += '</head>'
cMsg += '<BODY>'
cMsg += '<font size="3" face="Arial"><b><br>Novo Cliente Cadastrado</b></br></font>' +_CRLF
cMsg += '<font size="2" face="Arial"><br>Cliente/Loja : '+alltrim(cCodCli)+'</br></font>' +_CRLF
cMsg += '<font size="2" face="Arial"><br>Bloqueado    : '+cBloq+'</br></font>' +_CRLF
cMsg += '<font size="2" face="Arial"><br>CNPJ/CPF     : '+SA1->A1_CGC+'</br></font>' +_CRLF
cMsg += '<font size="2" face="Arial"><br>Raz�o Social : '+alltrim(SA1->A1_NOME)+'</br></font>' +_CRLF
cMsg += '<font size="2" face="Arial"><br>Nome Fantazia: '+Alltrim(SA1->A1_NREDUZ)+'</br></font>' +_CRLF
cMsg += '<font size="2" face="Arial"><br>Usu�rio      : '+USRRETNAME(RETCODUSR())+'</br></font>' +_CRLF
cMsg += '<font size="2" face="Arial"><br>Data         : '+dtoc(date())+'</br></font>' +_CRLF
cMsg += '<font size="2" face="Arial"><br>Hora 	  	  : '+Time()+'</br></font>' +_CRLF
cMsg += '<font size="1" face="Arial"><br>Obs : E-mail enviado automaticamente pelo sistema.<br/></font>' +_CRLF
cMsg += '</BODY>'
cMsg += '</html>'

*----------------------------------------
U_SEndMail(_cEmail,cAssunto,cMsg,"")               '
*----------------------------------------

Return