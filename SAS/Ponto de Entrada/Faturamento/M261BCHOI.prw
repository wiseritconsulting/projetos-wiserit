#include 'protheus.ch'
#include 'parmtype.ch'
#include 'topconn.ch'
/*/{Protheus.doc} M261BCHOI
//TODO Bot�o para explos�o dos Kits
@author Diogo
@since 10/10/2016
@version undefined
@example
(examples)
@see (links_or_references)
/*/

User function M261BCHOI()

Local aButtons :=  { }

Aadd(aButtons ,{ "Cliente" , {|| u_FKitsSAS()}, 'Kits'})

Return (aButtons)	

User Function FKitsSAS

Local aArea     := GetArea()
Local aKits     := {}

Local nPProduto := aScan(aHeader,{|x| AllTrim(x[2]) == 'D3_COD'})
Local nPQtd 	:= aScan(aHeader,{|x| AllTrim(x[2]) == 'D3_QUANT'})
Local nPUMOrg   := aScan(aHeader,{|x| AllTrim(x[2]) == 'D3_UM'})
Local nPDscOrg	:= aScan(aHeader,{|x| AllTrim(x[2]) == 'D3_DESCRI'})
Local nPArmOrg	:= aScan(aHeader,{|x| AllTrim(x[2]) == 'D3_LOCAL'})
Local nY        := 0
Local nX        := 0
Local cItem     := ''
Private aRet	:= {}

	For i:=1 To len(aHeader)
		If Alltrim(aHeader[i][2]) == "D3_LOCAL"
			nPArmDes:= i //Armaz�m Destino	
		Endif

		If Alltrim(aHeader[i][2]) == "D3_COD"
			nProdDes:= i //Produto Destino	
		Endif

		If Alltrim(aHeader[i][2]) == "D3_UM"
			nPUMDes:= i //Unidade de Medida Destino
		Endif

		If Alltrim(aHeader[i][2]) == "D3_DESCRI"
			nPDscDes:= i //Descri��o Destino 
		Endif
		
	Next

	Processa({||aKits := MontaArr()},"Processando","Aguarde...",.T.)  
	
	if len(aCols) = 1
		If empty(aCols[1][nPProduto])
			asize(acols,0)
		Endif
	Endif
	
	dbSelectArea("SB1")
	SB1->(dbSetOrder(1))

	For nX := 1 To Len(aKits)

		aadd(aCOLS,Array(Len(aHeader)+1))

		For nY	:= 1 To Len(aHeader)
			If (aHeader[nY,2] <> "D3_REC_WT") .And. (aHeader[nY,2] <> "D3_ALI_WT")				
				aCols[Len(aCols)][nY] := CriaVar(aHeader[nY][2])
			EndIf	
		Next nY
		
		N := Len(aCols)
		aCOLS[N][Len(aHeader)+1] := .F.
		
		SB1->(dbSeek(xFilial("SB1")+aKits[nX][1]))

		If SB2->(!DbSeek(xFilial("SB2")+SB1->B1_COD+aKits[nX][4]))
			CriaSB2(SB1->B1_COD,aKits[nX][3],xFilial("SB2"))
			SB2->(MsUnLock())
		EndIf

		//Preenchimento dos campos
		aCols[N][nPProduto]     := aKits[nX][1]
		aCols[N][nProdDes]     	:= aKits[nX][1]
		
		aCols[N][nPQtd]      	:= aKits[nX][2]
		
		aCols[N][nPArmOrg]     	:= aKits[nX][3]
		aCols[N][nPArmDes]     	:= aKits[nX][4]
		
		aCols[N][nPDscOrg]     	:= SB1->B1_DESC
		aCols[N][nPDscDes]     	:= SB1->B1_DESC

		aCols[N][nPUMOrg]     	:= SB1->B1_UM
		aCols[N][nPUMDes]     	:= SB1->B1_UM
		

		If ExistTrigger("D3_COD")
			RunTrigger(2,N,Nil,,"D3_COD")
		Endif

		EvalTrigger()
		
	Next nX

	//n:= 1
	
	RestArea(aArea)
	
Return Nil


Static Function MontaArr()

	Local aKits		:= {}
	Local cCodKit	:= Space(GetSx3Cache("B1_COD","X3_TAMANHO"))
	Local cArmzORG	:= Space(GetSx3Cache("D3_LOCAL","X3_TAMANHO"))
	Local cArmzDES	:= Space(GetSx3Cache("D3_LOCAL","X3_TAMANHO"))
	Local aParam	:= {}
	Local cArea 	:=	GetArea()

	aAdd(aParam,{1,"Kit",cCodKit,GetSx3Cache("B1_COD","X3_PICTURE"),"U_FEXPKITS()","",".T.",80,.F.})
	aAdd(aParam,{1,"Armazem Origem",cArmzORG,GetSx3Cache("D3_LOCAL","X3_PICTURE"),".T.","NNR",".T.",80,.F.})
	aAdd(aParam,{1,"Armazem Destino",cArmzDES,GetSx3Cache("D3_LOCAL","X3_PICTURE"),".T.","NNR",".T.",80,.F.})
	
	If !ParamBox(aParam,"Pedido de Venda",@aRet,{||.T.},,,,,,"M261KIT",.T.,.T.)
		Aviso("Atencao","Cancelado pelo usuario.",{"Ok"})
		Return aKits
	Endif


	cQuery := "SELECT ZZ7_PRODUT PROD, SUM(ZZ7_QUANT) QUANT FROM FN_PEDIDOS_VENDA('"+aRet[1]+"') "
	cQuery += " GROUP BY ZZ7_PRODUT "
	
	If Select("CKIT") <> 0
		CKIT->(DbCloseArea())
	EndIf
	
	TCQuery cQuery new alias "CKIT"
	
	WHILE !CKIT->(eof())
		AADD(aKits,{ CKIT->PROD , CKIT->QUANT, aRet[2],aRet[3] })
		CKIT->(dbSkip())	
	ENDDO

	CKIT->(DbCloseArea())
	
	RestArea(cArea)

Return aKits