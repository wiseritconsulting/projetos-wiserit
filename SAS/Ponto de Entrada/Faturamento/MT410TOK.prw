//#include "rwmake.ch"
//#include "TOPCONN.CH"

/*/{Protheus.doc} M410TOK
//TODO Inclus�o da etapa quando for expedi��o
@author author
@since 03/11/2016
@version version
@example
(examples)
@see (links_or_references)
/*/

User Function MT410TOK()

	Local Ok   		:= .T.
	Local nPesBrt	:= 0
	Local nPesol	:= 0
	Local nPosProd	:= ascan(aheader,{|x|upper(alltrim(x[2]))=="C6_PRODUTO"})
	Local nPosQtd	:=ascan(aheader,{|x|upper(alltrim(x[2]))=="C6_QTDVEN"})
	
	
	If M->C5_YPEDIDO = 'I' .and. M->C5_YETAPA = ' ' .AND. M->C5_YEXPED = 'CE' .and. inclui //Intercompany - Com expedi��o

		M->C5_YETAPA := '10' //PLANEJAMENTO DE EXPEDICAO 	 

	Endif

	If M->C5_YPEDIDO = 'I' .OR. M->C5_YPEDIDO = 'N'
	
		For i:= 1 to Len(aCols)
    		If !aCols[i,Len(acols[n])]

				xPesoLiq:= Posicione("SB1",1,xFilial("SB1")+Acols[i,nPosProd],"B1_PESO")
		    	xPesoBrt:= Posicione("SB1",1,xFilial("SB1")+Acols[i,nPosProd],"B1_PESBRU")
		    	nPesol      += Acols[i,nPosQtd] * xPesoLiq		//PESO LIQUIDO
		    	nPesBrt     += Acols[i,nPosQtd] * xPesoBrt     //PESO BRUTO
         	Endif
    	Next i
	Endif

	If nPesol > 0 .and. nPesBrt > 0
		M->C5_PESOL  := nPesol
		M->C5_PBRUTO := nPesBrt
	Endif


Return(Ok)