#include "rwmake.ch"
#include "Topconn.ch"
#include "Protheus.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���P.Entrada �  FA040B01 � Autor �  Carlos Meneses      � Data � 10/10/14 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � No momento da cancelamento da exclus�o do t�tulo principal ���
��� o sistema dever� excluir NCC                                          ���
�������������������������������������������������������������������������Ĵ��
����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function FA040B01()

/*
Local aAreaSE1 := SE1->(GetArea())
Local aArray   := {}
Local _cChavSE1  := SE1->(E1_CLIENTE+E1_LOJA)+"MC"+SUBSTR(ALLTRIM(SE1->E1_FILIAL),2,1)+SE1->(E1_NUM+E1_PARCELA)+"NCC"

Private lMsErroAuto := .F.

If ALLTRIM(SE1->E1_PREFIXO) == "MAN"
	
	dbSelectArea("SE1")
	SE1->(dbgotop())
	SE1->(DbSetOrder(2))
	
	If SE1->(dbSeek(xFilial("SE1")+_cChavSE1))
		
		Begin Transaction
		
		aArray := {	{ "E1_FILIAL"  , SE1->E1_FILIAL  , NIL },;
		{ "E1_PREFIXO"  , SE1->E1_PREFIXO            , NIL },;
		{ "E1_NUM"      , SE1->E1_NUM				 , NIL },;
		{ "E1_PARCELA"  , SE1->E1_PARCELA   		 , NIL },;
		{ "E1_TIPO"     , SE1->E1_TIPO				 , NIL },;
		{ "E1_CLIENTE"  , SE1->E1_CLIENTE   		 , NIL },;
		{ "E1_LOJA"		, SE1->E1_LOJA	    		 , NIL } }
		
		MsExecAuto( { |x,y| FINA040(x,y)}, aArray, 5 )
		
		If lMsErroAuto
			Mostraerro()
			DisarmTransaction()
		Endif
		
		End Transaction
		
	EndIf
	
EndIf

lMsErroAuto := .F.

RestArea(aAreaSE1)
*/
Return(.t.)
