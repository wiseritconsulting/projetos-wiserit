// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : FA100OKR.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor             | Descricao
// -----------+-------------------+---------------------------------------------------------
// 24/08/2016 | rogeriojacome     | Gerado com aux�lio do Assistente de C�digo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} new_source6
Processa a tabela SRA-Funcionarios.

@author    rogeriojacome
@version   11.3.1.201605301307
@since     24/08/2016
/*/
//------------------------------------------------------------------------------------------
user function FA100OKR()
	//-- vari�veis -------------------------------------------------------------------------

	Local lRet := .T.
	
	IF alltrim(M->E5_NATUREZ) $ GETMV("SA_NATMREC") .AND. (empty(M->E5_YCLIENT) .AND. empty(M->E5_YLOJA)) 
		MsgAlert("Naturezas " + GETMV("SA_NATMREC") + " exigem cliente e loja!","ATEN��O!!!" )
		lRet := .F.
	ElseIf Substr(M-> E5_NATUREZ,1,1) == "2"
		MsgInfo("A Natureza Financeira " + Alltrim(M-> E5_NATUREZ) + " n�o pode ser utilizada na inclus�o de Movimentos a Receber.")
		If MsgYesNo("Deseja continuar?")
			lRet := .T.
		Else
			lRet := .F.
		EndIf
	Else
		lRet := .T.
	EndIf
	/*
	If Substr(M-> E5_NATUREZ,1,1) == "2"
		MsgInfo("A Natureza Financeira " + Alltrim(M-> E5_NATUREZ) + " n�o pode ser utilizada na inclus�o de Movimentos a Receber.")
		If MsgYesNo("Deseja continuar?")
			lRet := .T.
		Else
			lRet := .F.
		EndIf
	Else
		lRet := .T.
	EndIf
	*/
return lRet

//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 24/08/2016 as 17:50:12
//-- fim de arquivo--------------------------------------------------------------------------

