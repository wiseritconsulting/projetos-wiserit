/*/{Protheus.doc} F050CV4
Grava��o do campo vendedor
@author Diogo
@since 05/11/2014
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function F050CV4
	
    CV4->(RecLock("CV4",.F.))
    	CV4_YVEND	:= CV4_CCC
    	CV4_CCC		:= ''
    CV4->(MsUnlock())
Return