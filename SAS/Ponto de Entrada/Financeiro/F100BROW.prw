#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

user function F100BROW()
	
    AAdd( aRotina, { "Alterar 2� Natureza", "U_AltNatu", 0, 4} ) 
	
return aRotina

User Function AltNatu()
	
	Local aArea := GetArea()
	Local aAreaSE5 := SE5->(GetArea())
	Local cCpAlt := NIL
	Local oDlg
	Local nOpca := 0
	Local cNatAlt := "    "
	Private oGet := ""
	
	IF __cUserID # GETMV("SA_NATFLU")
	
		MsgInfo("Usuario sem permiss�o para essa rotina, contate o administrador")
		return
  else
	DEFINE MSDIALOG oDlg TITLE "Alterar 2� Natureza" From 010,000 To 130,345 OF oMainWnd PIXEL
	@ 015,007 SAY "2� Natureza" OF oDlg PIXEL SIZE 046,009 
	@ 014,041 MSGET cCpAlt VAR cNatAlt SIZE 055,007 OF oDlg PIXEL
	 ACTIVATE DIALOG oDlg ON INIT EnchoiceBar(oDlg,{||nOpca:=1,oDlg:End() },{||nOpca:=2,oDlg:End() }) CENTERED 
          If nOpca == 1 
               If !Empty(cNatAlt) 
                    RecLock("SE5",.F.) 
                    	SE5->E5_YNATFLU := cNatAlt 
          		    SE5->(msUnLock()) 
                MsgInfo("2� Natureza alterada com sucesso")
               EndIf 
                     
               Else 
                    Alert("Opera��o cancelada pelo Usu�rio") 
                    Return() 
               EndIf 
   endif            	
return 