#include "rwmake.ch"
#include "Topconn.ch"
#include "Protheus.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���P.Entrada �  F070BTOK � Autor �   Carlos Meneses     � Data � 19/09/14 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � No momento da baixa do t�tulo sistema dever� gerar NCC     ���
�������������������������������������������������������������������������Ĵ��
����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function FA070TIT()
Local aAreaSE1 := SE1->(GetArea())
Local _ret     := .T.
Local aArray   := {}
Local xQSE1

/*Private lMsErroAuto := .F.

xQSE1 := " SELECT '  '+MAX(E1_PARCELA) NUMPARC FROM "+RETSQLNAME("SE1")+" SE1 "
xQSE1 += " WHERE SE1.D_E_L_E_T_='' "
xQSE1 += " AND SE1.E1_FILIAL='"+SE1->E1_FILIAL+"' "
xQSE1 += " AND SE1.E1_NUM='"+SE1->E1_NUM+"' "
xQSE1 += " AND SE1.E1_PREFIXO='"+SE1->E1_PREFIXO+"' "
xQSE1 += " AND SE1.E1_TIPO='NCC'"

If Select("xQSE1")> 0
	xQSE1->(DBCLOSEAREA())
Endif

TCQUERY xQSE1 NEW ALIAS "xQSE1"

If alltrim(xQSE1->NUMPARC) == "0"
	_cParc := "1"
	_cParc := RETASC(_cParc,1,.T.)
Else
	_cParc := RETASC(alltrim(xQSE1->NUMPARC),2,.F.)
	_cParc := val(_cParc)+1
	_cParc := RETASC(_cParc,1,.T.)
EndIf

xQSE1->(DBCLOSEAREA())

If ALLTRIM(SE1->E1_TIPO)$"DP|BL|BOL|ADT|ACF" .AND. ALLTRIM(CMOTBX)$"NORMAL" 

	Begin Transaction

	aArray := {	{ "E1_FILIAL"  , SE1->E1_FILIAL     , NIL },;
	{ "E1_PREFIXO"  , SE1->E1_PREFIXO   			, NIL },;
	{ "E1_NUM"      , SE1->E1_NUM					, NIL },;
	{ "E1_PARCELA"  , _cParc            			, NIL },;
	{ "E1_TIPO"     , "NCC"  						, NIL },;
	{ "E1_NATUREZ"  , SE1->E1_NATUREZ				, NIL },;
	{ "E1_CLIENTE"  , SE1->E1_CLIENTE   			, NIL },;
	{ "E1_LOJA"		, SE1->E1_LOJA	    			, NIL },;
	{ "E1_NOMCLI"	, SE1->E1_NOMCLI				, NIL },;
	{ "E1_EMISSAO"  , dDATABASE 					, NIL },;
	{ "E1_VENCTO"   , dDATABASE 					, NIL },;
	{ "E1_VENCREA"  , dDATABASE 					, NIL },;
	{ "E1_BAIXA"  	, CTOD(" / / ")					, NIL },;
	{ "E1_MOVIMEN" 	, CTOD(" / / ")					, NIL },;
	{ "E1_VALLIQ" 	, 0        						, NIL },;
	{ "E1_OK" 		, SPACE(02)						, NIL },;
	{ "E1_SALDO" 	, nvalrec - (NMULTA+NJUROS)		, NIL },;
	{ "E1_VALOR"    , nvalrec - (NMULTA+NJUROS)		, NIL },;
	{ "E1_CCC"      , SE1->E1_CCC	    			, NIL },;
	{ "E1_HIST"     , SE1->E1_HIST					, NIL } }

	MsExecAuto( { |x,y| FINA040(x,y)}, aArray, 3 )

	If lMsErroAuto
		Mostraerro()
		DisarmTransaction()
	Endif

	End Transaction

EndIf

lMsErroAuto := .F.
*/
RestArea(aAreaSE1)

Return(_ret)