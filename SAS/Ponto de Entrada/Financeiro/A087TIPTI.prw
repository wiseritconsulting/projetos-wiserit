#Include 'Protheus.ch'
/*/{Protheus.doc} A087TIPTI
Tipos que considerar no recebimento do T�tulo
@author Diogo
@since 27/10/2014
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function A087TIPTI()
	Local cTpCons:= ""
	cTpCons+= GetMv("SA_TPREC") //Ex: "/BL /BOL/ "

Return cTpCons