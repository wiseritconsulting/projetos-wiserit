// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : F200VAR
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor  Rog�rio J�come           | Descricao Criar as NCC na baixa dos titulos pelo arquivo do banco.
// ---------+-------------------+-----------------------------------------------------------
// 21/08/15 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"
#include 'topconn.ch'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo
Montagem da tela de processamento

@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     21/08/2015
/*/
//------------------------------------------------------------------------------------------
User function F200VAR()
	//--< vari�veis >---------------------------------------------------------------------------
	Local aArea := GetArea()
	Local cHist := ""
	/*
	//Private lMsErroAuto := .F.


	DBSELECTAREA("SE1")
	DbSetOrder(16)
	IF DbSeek(__SE1->E1_FILIAL + ALLTRIM(cNumtit)) 
		If ALLTRIM(SE1->E1_TIPO)$"DP|BL|BOL|ADT"

			IF !(DbSeek(SE1->E1_FILIAL + SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA + "NCC"))

				cQuery := "SELECT * "
				cQuery += "FROM "+RETSQLNAME("SE1")+" E1 "
				cQuery += " WHERE E1_FILIAL = '"+__SE1->E1_FILIAL+"' AND E1_IDCNAB = '"+ALLTRIM(cNumtit)+"'  "
				cQuery += " AND D_E_L_E_T_ = ' ' "


				if Select("T05") > 0
					dbSelectArea("T05")
					dbCloseArea()
				endif

				TcQuery cQuery New Alias T05
				
				IF T05->E1_BAIXA <> ""
				
				aArray := {	{ "E1_FILIAL"  , T05->E1_FILIAL     , NIL },;
				{ "E1_PREFIXO"  , T05->E1_PREFIXO   			, NIL },;
				{ "E1_NUM"      , T05->E1_NUM					, NIL },;
				{ "E1_PARCELA"  , T05->E1_PARCELA            			, NIL },;
				{ "E1_TIPO"     , "NCC"  						, NIL },;
				{ "E1_NATUREZ"  , T05->E1_NATUREZ				, NIL },;
				{ "E1_CLIENTE"  , T05->E1_CLIENTE   			, NIL },;
				{ "E1_LOJA"		, T05->E1_LOJA	    			, NIL },;
				{ "E1_NOMCLI"	, T05->E1_NOMCLI				, NIL },;
				{ "E1_EMISSAO"  , dDATABASE 					, NIL },;
				{ "E1_VENCTO"   , dDATABASE 					, NIL },;
				{ "E1_VENCREA"  , dDATABASE 					, NIL },;
				{ "E1_BAIXA"  	, CTOD(" / / ")					, NIL },;
				{ "E1_MOVIMEN" 	, CTOD(" / / ")					, NIL },;
				{ "E1_VALLIQ" 	, 0        						, NIL },;
				{ "E1_OK" 		, SPACE(02)						, NIL },;
				{ "E1_SALDO" 	, T05->E1_SALDO		, NIL },;
				{ "E1_VALOR"    , T05->E1_VALOR		, NIL },;
				{ "E1_CCC"      , T05->E1_CCC	    			, NIL },;
				{ "E1_HIST"     , T05->E1_HIST	, NIL } }

				MsExecAuto( { |x,y| FINA040(x,y)}, aArray, 3 )
				
				ENDIF
				
				T05->(DbCloseArea())
				/*
				DBSELECTAREA("SE1")
				DbSetOrder(16)
				IF (DbSeek(__SE1->E1_FILIAL + cNumtit))

				RECLOCK("SE1",.F.)

				SE1->E1_HIST := IIF(ALLTRIM(cHist) == "",__SE1->E1_HIST, cHist)

				MSUNLOCK()     
				// Destrava o registro
				/*If lMsErroAuto
				Mostraerro()
				DisarmTransaction()
				Endif
		

			endif

		ENDIF

	ENDIF
	//lMsErroAuto := .F.
	*/
	
	RestArea(aArea)



return
//--< fim de arquivo >----------------------------------------------------------------------
