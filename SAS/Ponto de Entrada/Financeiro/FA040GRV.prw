#include "rwmake.ch"
#include "Topconn.ch"
#include "Protheus.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���P.Entrada �  FA040GRV � Autor �   Carlos Meneses     � Data � 15/10/14 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � No momento da inclus�o do t�tulo complementar o sistema    ���
��� dever� gerar NCC                                                      ���
�������������������������������������������������������������������������Ĵ��
����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function FA040GRV() 
/*
Local aArea    := GetArea()
Local aAreaSE1 := SE1->(GetArea())
Local _ret     := .T.
Local aArray   := {}
Local nCont

Private lMsErroAuto := .F.

If ALLTRIM(SE1->E1_PREFIXO+E1_TIPO)=="MANBOL"  //Prefixo for MAN e Tipo for BOL esses t�tulos foram inclu�dos direto no financeiro para complemento
	
	Begin Transaction
	
	aArray := {	{ "E1_FILIAL"  , SE1->E1_FILIAL     , NIL },;
	{ "E1_PREFIXO"  , "MC"+SUBSTR(ALLTRIM(SE1->E1_FILIAL),2,1)  , NIL },;
	{ "E1_NUM"      , SE1->E1_NUM					, NIL },;
	{ "E1_PARCELA"  , SE1->E1_PARCELA   			, NIL },;
	{ "E1_TIPO"     , "NCC"  						, NIL },;
	{ "E1_NATUREZ"  , SE1->E1_NATUREZ				, NIL },;
	{ "E1_CLIENTE"  , SE1->E1_CLIENTE   			, NIL },;
	{ "E1_LOJA"		, SE1->E1_LOJA	    			, NIL },;
	{ "E1_NOMCLI"	, SE1->E1_NOMCLI				, NIL },;
	{ "E1_EMISSAO"  , SE1->E1_EMISSAO				, NIL },;
	{ "E1_VENCTO"   , SE1->E1_VENCTO				, NIL },;
	{ "E1_VENCREA"  , SE1->E1_VENCREA				, NIL },;
	{ "E1_OK" 		, SE1->E1_OK						, NIL },;
	{ "E1_VALOR"    , SE1->E1_VALOR	    			, NIL },;
	{ "E1_HIST"     , SE1->E1_HIST					, NIL } }
	
	MsExecAuto( { |x,y| FINA040(x,y)}, aArray, 3 )
	
	If lMsErroAuto
		Mostraerro()
		DisarmTransaction()
	Endif
	
	End Transaction
	
EndIf

lMsErroAuto := .F.

RestArea(aAreaSE1)
RestArea(aArea)
*/
Return