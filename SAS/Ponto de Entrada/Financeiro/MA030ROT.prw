#include "protheus.ch"  
#include "rwmake.ch"  

User Function MA030ROT()

	Local _aRotina	:= {}
	AAdd( _aRotina, { "Bloq.\Lib. Auto.", "MsgRun('Processando Informações...',, {||U_SASP001()} )", 6, 0 } )
	AAdd( _aRotina, { "Bloq.\Lib. Man.", "U_SASP002()", 6, 0 } )
	AAdd( _aRotina, { "Token WPensar", "U_SASP022()", 6, 0 } )
	

Return( _aRotina )




