#include 'protheus.ch'
#include 'parmtype.ch'

User function FA010INC()

local bRet := .T.

		if !(__cUserID $ GetMV("SA_ALTNATU"))
		bRet := .F.
		Msginfo("Usu�rio sem permiss�o para Incluir Natureza Financeira, Favor clicar em Fechar ","Permissao de Acesso")
	endif
	
return bRet
