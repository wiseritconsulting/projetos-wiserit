#Include 'Protheus.ch'
#INCLUDE "colors.ch"
#include "topconn.ch"                
#include "rwmake.ch"        
#include "tcbrowse.ch"

User Function FIN010INC() 

	FIN0()
	WORKNAT()

Return

STATIC FUNCTION FIN0()

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � FIN010INC  � Autor � Carlos Meneses  � Data �  15/09/2014  ���
�������������������������������������������������������������������������͹��
���Descricao � Ponto de Entrada para replicar o cadastro de Natureza      ���
���          � Financeira automaticamente em todas as empresas.           ���
�������������������������������������������������������������������������͹��
���Uso       � Na inclus�o do Cadastro de Natureza Financeira             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/


Local aArea   := GetArea()
Local _cEmp := ""
Local _aEmp := {}
Local cFilBak	:= cFilAnt
Local cEmpBak	:= cEmpAnt

Local aSED		:= {}, aDadosSED := {}
Local nCont
Local nCtEmp

dbselectarea("SM0")
SM0->(dbgotop())

While SM0->(!Eof())
	If Substr(SM0->M0_CODFIL,1,2)<>_cEmp .AND. SM0->M0_CODIGO<>"99"
		AADD(_aEmp,{"M0_CODFIL",SM0->M0_CODFIL,nil })
	EndIf
	_cEmp := Substr(SM0->M0_CODFIL,1,2)
	SM0->(dbskip())
EndDo

If INCLUI
	
	Begin Transaction
	
	aSED	:= SED->(DBStruct())
	//COPIA TES SELECIONADO
	For nCont:=1 to Len(aSED)
		AADD(aDadosSED,{aSED[nCont][1],SED->(&(aSED[nCont][1]))})
	Next
	
	For nCtEmp:=1 to Len(_aEmp)
		
		cFilAnt := _aEmp[nCtEmp,2]
		
		If SUBSTR(cFilAnt,1,2) <> SUBSTR(cFilBak,1,2)
			
			RecLock("SED",.T.)
			For nCont:=1 to Len(aDadosSED)
				SED->(&(aDadosSED[nCont][1]))	:= aDadosSED[nCont][2]
			Next
			SED->ED_FILIAL := SUBSTR(cFilAnt,1,2)
			SED->(MsUnLock())
			
		EndIf
		
	Next
	
	End Transaction
	
EndIf

cFilAnt := cFilBak
cEmpAnt	:= cEmpBak
RestArea(aArea)	

Return 

STATIC FUNCTION WORKNAT()

/*/{Protheus.doc} WORKNAT
Ponto de entrada Workflow cadastro de natureza financeira
@author Weskley Silva
@since 11/09/2015
@version 1.0
/*/

Local cMsg		:= "" 
Local _CRLF 	:= chr(13)+chr(10)
Local _cEmail   := GetMv("SA_WFNAT")//"weskleysilva@aridesa.com.br"GetMv("SA_WFNAT")
Local cCodCli	:= SED->ED_CODIGO
Local cAssunto	:= "Nova Natureza Cadastrada"

cMsg +='<html>'   
cMsg +='<head>'
cMsg +='<title></title>'
cMsg +='</head>'
cMsg +='<BODY>'
cMsg +='<font size="3" face="Arial"><b><br>Nova Natureza Cadastrado</b></br></font>' +_CRLF
cMsg +='<font size="2" face="Arial"><br>Natureza : '+alltrim(cCodCli)+'</br></font>' +_CRLF
cMsg +='<font size="2" face="Arial"><br>Descri��o     : '+SED->ED_DESCRIC+'</br></font>' +_CRLF
cMsg +='<font size="2" face="Arial"><br>Data Inclus�o : '+dtoc(date())+'</br></font>' +_CRLF
cMsg +='<font size="1" face="Arial"><br>Obs : E-mail enviado automaticamente pelo sistema.<br/></font>' +_CRLF
cMsg +='</BODY>'
cMsg +='</html>'

U_SEndMail(_cEmail,cAssunto,cMsg,"")

Return
