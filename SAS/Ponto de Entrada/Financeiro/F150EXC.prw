// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : F150EXC.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor             | Descricao
// -----------+-------------------+---------------------------------------------------------
// 13/07/2016 | rogeriojacome     | Gerado com aux�lio do Assistente de C�digo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} F150EXC

@author    rogeriojacome
@version   11.3.1.201605301307
@since     13/07/2016
/*/
//------------------------------------------------------------------------------------------
user function F150EXC()
	//-- vari�veis -------------------------------------------------------------------------
	Local lRet := .T.
	Local cQuery := ''
	
	IF empty(SE1->E1_NUMBCO)
	
	lRet := .F.
	MsgAlert(SE1->E1_PARCELA + "� "+" Parcela do T�tulo: " + SE1->E1_NUM + " do Cliente: " +SE1->E1_CLIENTE+ " n�o ser� incluso no arquivo banc�rio pois n�o est� com as informa��es necess�rias preenchidas!!!","Inconsist�ncia no t�tulo")
	
	ENDIF
	
	//-- encerramento ----------------------------------------------------------------------

return lRet
//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 13/07/2016 as 13:49:42
//-- fim de arquivo--------------------------------------------------------------------------

