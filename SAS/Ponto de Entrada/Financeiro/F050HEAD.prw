/*/{Protheus.doc} F050HEAD
Retira valida��o do campo CTJ_CCC
@author Diogo
@since 05/11/2014
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function F050HEAD

 Local aAltCmp := Paramixb[1]
 
   nPos := aSCAN(aHEADER, {|x| AllTrim(Upper(X[2])) == "CTJ_CCC" })
   
   aHeader[nPos][6]:= ' '
Return aAltCmp