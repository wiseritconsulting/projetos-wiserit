#Include 'Protheus.ch'
#Include 'Protheus.ch'
/*/{Protheus.doc} FA087GRV
Grava��o do acr�scimo/decr�scimo dos t�tulos referente ao Recbto Diverso
@author Diogo
@since 27/10/2014
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
User Function FA087GRV()

	Local aArea:= getArea()
	Local cChav:= SEL->EL_FILIAL+SEL->EL_RECIBO

	SE1->(dbSetOrder(2))
	SEL->(dbSetOrder(1))
	SEL->(dbSeek(cChav))

	while SEL->(!eof()) .and. cChav = SEL->EL_FILIAL+SEL->EL_RECIBO
		
		If SEL->EL_YDECRES > 0 .or. SEL->EL_YACRES > 0
			
			nDescont:= SEL->EL_YDECRES
			nAcresc	:= SEL->EL_YACRES
			
			if SE1->(dbSeek(xFilial("SE1")+SEL->EL_CLIENTE+SEL->EL_LOJA+SEL->EL_PREFIXO+SEL->EL_NUMERO+SEL->EL_PARCELA))
				RecLock("SE1",.F.)
					SE1->E1_DECRESC	:= nDescont
					SE1->E1_SDDECRE	:= nDescont
					SE1->E1_SDACRES	:= nAcresc
					SE1->E1_ACRESC	:= nAcresc
				SE1->(MsUnlock())
			endif
		Endif
		
		
		if SE1->(dbSeek(xFilial("SE1")+SEL->EL_CLIENTE+SEL->EL_LOJA+SEL->EL_PREFIXO+SEL->EL_NUMERO+SEL->EL_PARCELA))
			RecLock("SE1",.F.)
			SE1->E1_SITUACA	:= "0"
			SE1->E1_NATUREZ	:= _xNaturez 
			SE1->(MsUnlock())
		EndIf
		dbSelectArea("SEL")

		SEL->(dbSkip())
	enddo
      
	_xNaturez:= ""
	RestArea(aArea)
Return