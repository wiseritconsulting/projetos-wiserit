#include "rwmake.ch"
#include "Topconn.ch"
#include "Protheus.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���P.Entrada �  FA070CAN � Autor �  Carlos Meneses     � Data � 19/09/14 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � No momento da cancelamento da baixa do t�tulo sistema      ���
��� dever� excluir NCC                                                    ���
�������������������������������������������������������������������������Ĵ��
����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function FA070CAN()
Local aArea    := GetArea()
Local aAreaSE1 := SE1->(GetArea())
Local aArray   := {}
Local nCont
Local xQSE1
Local _SE5Val

Private lMsErroAuto := .F.

_SE5Val := ALLTRIM(STR(SE5->E5_VALOR))

xQSE1 := " SELECT * FROM "+RETSQLNAME("SE1")+" "
xQSE1 += " WHERE D_E_L_E_T_ = '' "
xQSE1 += " AND E1_FILIAL = '"+SE1->E1_FILIAL+"' "
xQSE1 += " AND E1_NUM = '"+SE1->E1_NUM+"' "
xQSE1 += " AND E1_PREFIXO = '"+SE1->E1_PREFIXO+"' "
xQSE1 += " AND E1_CLIENTE = '"+SE1->E1_CLIENTE+"' "
xQSE1 += " AND E1_LOJA = '"+SE1->E1_LOJA+"' "
xQSE1 += " AND E1_TIPO = 'NCC' "
xQSE1 += " AND E1_BAIXA = '' "
xQSE1 += " AND E1_EMISSAO = '"+DTOS(SE5->E5_DTDIGIT)+"' "
xQSE1 += " AND E1_VALOR = '"+_SE5VAL+"' "


If Select("xQSE1")> 0
	xQSE1->(DBCLOSEAREA())
Endif

TCQUERY xQSE1 NEW ALIAS "xQSE1"

If ALLTRIM(SE1->E1_TIPO) $ "DP|BL|BOL|ADT"

	Begin Transaction

	If !EMPTY(xQSE1->E1_NUM)

		aArray := {	{ "E1_FILIAL"  , xQSE1->E1_FILIAL     , NIL },;
		{ "E1_PREFIXO"  , xQSE1->E1_PREFIXO     			, NIL },;
		{ "E1_NUM"      , xQSE1->E1_NUM					, NIL },;
		{ "E1_PARCELA"  , xQSE1->E1_PARCELA   			, NIL },;
		{ "E1_TIPO"     , "NCC"  						, NIL },;
		{ "E1_CLIENTE"  , xQSE1->E1_CLIENTE   			, NIL },;
		{ "E1_LOJA"		, xQSE1->E1_LOJA	    			, NIL } }

		MsExecAuto( { |x,y| FINA040(x,y)}, aArray, 5 )

		If lMsErroAuto
			Mostraerro()
			DisarmTransaction()
		Endif

		xQSE1->(DBCLOSEAREA())

	EndIf

	End Transaction

EndIf

lMsErroAuto := .F.

RestArea(aAreaSE1)
RestArea(aArea)

Return
