#include "protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#Include "RWMAKE.CH"
#INCLUDE "colors.ch"
#include "topconn.ch"
#include "vkey.ch"



Static aCacheTES	:= {}

User Function FA740BRW()

	Local aBotao :=  {}

	aAdd(aBotao,  { "Gerar Acordo" ,"U_DEV03", 0 , 4, ,.F.})
	aAdd(aBotao,  { "Estornar Acordo" ,"U_DEV03D", 0 , 4, ,.F.})	

Return aBotao

User Function DEV03()

	Local dDataIni	:= CTOD("")
	Local dDataFin	:= CTOD("")
	Local dDtVenI	:= CTOD("")
	Local dDtVenF	:= CTOD("")
	Local cNatu		:= SPACE(TAMSX3("E1_NATUREZ")[1])
	Local cNumFat		:= SPACE(TAMSX3("E1_NUM")[1])
	Local cPrefixo := SPACE(TAMSX3("E1_PREFIXO")[1])
	Local cCli		:= SPACE(TAMSX3("A1_COD")[1])
	Local cLjCli	:= SPACE(TAMSX3("A1_LOJA")[1])
	Local aPergs	:= {}

	Private aRetOpc := {}

	aAdd( aPergs ,{1,"Data Emissao De: 	" 			,dDataIni	,""					,'.T.',""			,'.T.',50	,.T.})
	aAdd( aPergs ,{1,"Data Emissao Ate: 	",dDataFin	,""					,'.T.',""			,'.T.',50	,.T.})
	aAdd( aPergs ,{1,"Data Vencimento De: 	" 			,dDtVenI	,""					,'.T.',""			,'.T.',50	,.T.})
	aAdd( aPergs ,{1,"Data Vencimento Ate: 	",dDtVenF	,""					,'.T.',""			,'.T.',50	,.T.})
	aAdd( aPergs ,{1,"Cliente:	" 			,cCli		,"@!"				,'ExistCpo("SA1")',"SA1"		,'.T.',50	,.T.})
	aAdd( aPergs ,{1,"Loja:	" 				,cLjCli		,"@!"				,'.T.',""			,'.F.',50	,.T.})
	aAdd( aPergs ,{1,"Natureza novo titulo:	" 			,cNatu		,"@!"				,'ExistCpo("SED")',"SED"		,'.T.',50	,.T.})
	aAdd( aPergs ,{1,"Prefixo novo titulo:	" 			,cPrefixo		,"@!"				,'.T.',""		,'.T.',50	,.T.})
	//aAdd( aPergs ,{1,"Numeracao novo titulo:" 	,cNumFat		,"@!"					,'.T.',""			,'.T.',50	,.T.})

	If ParamBox(aPergs,"Filtros:",aRetOpc,,,,,,,"_RBRL33",.T.,.T.)
		DEV03a()
	EndIf

Return

Static Function DEV03a()

	Local aArea := getarea()
	Local oBrowse
	Local aCoors	 := FwGetDialogSize( oMainWnd )
	Local oFWLayer,oPanelTop1,oView
	Local bBlockClique
	Local nE :=1
	Private aTitulos := {}
	Private oDlg
	Private nTotal := 0
	Private oGet2 := ""
	Static nFilial   := 1
	Static nPrefixo   := 2
	Static nNum    := 3
	Static nParcela   := 4
	Static nTipo   := 5
	Static nVencto   := 6
	Static nValor   := 7
	Static nok := 8

	aTitulos := U_RTITULB(aTitulos)

	oDlg		:= MSDialog():New( aCoors[1],aCoors[2],aCoors[3],aCoors[4],"Aglutinar titulos",,,.F.,,,,,,.T.,,,.T. )
	//oDlg		:= MSDialog():New( 0,0,500,700,"Planejamento e Controle de Expedicao",,,.F.,,,,,,.T.,,,.T. )/

	@275,10 SAY oSay1 PROMPT "Total :" SIZE 50,30 OF oDlg PIXEL
	@270,30 GET oGet2 VAR nTotal PICTURE GetSx3Cache("E1_VALOR","X3_PICTURE") SIZE 80, 12 WHEN .F. OF oDlg  PIXEL


	oFWLayer	:= FWLayer():New()
	oFWLayer:Init( oDlg, .F., .T. )
	oFWLayer:AddLine( 'TOP1', 90, .F. )
	oPanelTop1 := oFWLayer:getLinePanel( 'TOP1' )

	oBrowse	    := FWFormBrowse():New()
	oMarkBrow	:= oBrowse:FWBrowse()
	oMarkBrow:SetDataArray()
	oMarkBrow:lAdjust := .F.
	oMarkBrow:SetArray(@aTitulos)
	oMarkBrow:SetDescription('Aglutinacao')
	oMarkBrow:SetOwner( oPanelTop1 )
	oMarkBrow:SetProfileID("4")
	oMarkBrow:SetMenuDef("")
	oMarkBrow:SetDoubleClick({|| nil })

	bBlockClique := {|| U_CLICKDB() }

	oMarkBrow:AddButton("Confirmar",{||oDlg:End(),DEV03B()},,,)//aButtons
	oMarkBrow:AddButton("Sair",{||oDlg:End()},,,)//aButtons

	// legenda
	//oMarkBrow:AddLegend({||aTitulos[oMarkBrow:At()][nNF01]  == "" }  ,"BR_PRETO",     "Medicao nao Faturada")
	//oMarkBrow:AddLegend({||aTitulos[oMarkBrow:At()][nNF01]  <> "" }  ,"BR_VERDE",     "Medicao Faturada")
	//----------------------------------------------------------------------------------------------------------------


	// BOX
	oMarkBrow:AddMarkColumns(;
	{||If(aTitulos[oMarkBrow:At()][nOk]=="","LBNO","LBTICK")};	//Imagem da marca
	,bBlockClique/*{|| YINVERTE(oMarkBrow)}*/;
	,{|| YINVERTE(oMarkBrow)})

	//Filial		
	oMarkBrow:SetColumns({{;
	GetSx3Cache("E1_FILIAL","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nFilial],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_FILIAL","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_FILIAL","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_FILIAL","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_FILIAL","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};
	)

	//Prefixo		
	oMarkBrow:SetColumns({{;
	GetSx3Cache("E1_PREFIXO","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nPrefixo],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_PREFIXO","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_PREFIXO","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_PREFIXO","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_PREFIXO","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};
	)

	//Numero		
	oMarkBrow:SetColumns({{;
	GetSx3Cache("E1_NUM","X3_TITULO"),;  					   			                       // Tiiulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nNum],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_NUM","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_NUM","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_NUM","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_NUM","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};
	)

	//Parcela		
	oMarkBrow:SetColumns({{;
	GetSx3Cache("E1_PARCELA","X3_TITULO"),;  					   			                       // Tiiulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nParcela],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_PARCELA","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_PARCELA","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_PARCELA","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_PARCELA","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcoes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)

	//Tipo		
	oMarkBrow:SetColumns({{;
	GetSx3Cache("E1_TIPO","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nTipo],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_TIPO","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_TIPO","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_TIPO","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_TIPO","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcoes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)

	//Vencto	
	oMarkBrow:SetColumns({{;
	GetSx3Cache("E1_VENCTO","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nVencto],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_VENCTO","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_VENCTO","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_VENCTO","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_VENCTO","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcoes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)

	//Valor
	oMarkBrow:SetColumns({{;
	GetSx3Cache("E1_SALDO","X3_TITULO"),;  					   			                       // Titulo da coluna
	{|| If(!Empty(aTitulos),aTitulos[oMarkBrow:At()][nValor],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_SALDO","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_SALDO","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_SALDO","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_SALDO","X3_DECIMAL"),;                       	                      // Decimal
	.T.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execcao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};				       	       	                			                               // Opcaes de carga dos dados (Ex: 1=Sim, 2=Nao)
	)



	oMarkBrow:SetMenuDef( 'DEV03' )	 
	oMarkBrow:Activate()	
	oDlg:Activate(,,,.T.)	

	RestArea(aArea)

	//Reset Environment
	aTitulos := {}

Return

//Inclusao dos dados nos vetor
User function RTITULB(aTitulos)

	Local aTit2 := {}
	Local cQuery := ""

	cQuery := ""
	cQuery += " SELECT "
	cQuery += "    SE1.E1_FILIAL, "
	cQuery += "    SE1.E1_PREFIXO, "
	cQuery += "    SE1.E1_NUM, "
	cQuery += "    SE1.E1_PARCELA, "
	cQuery += "    SE1.E1_TIPO, "
	cQuery += "    SE1.E1_VENCTO, "
	cQuery += "    SE1.E1_SALDO "
	cQuery += "   FROM " + RETSQLNAME("SE1") + " SE1 "
	cQuery += "  WHERE SE1.D_E_L_E_T_ = '' "
	cQuery += "    AND SE1.E1_FILIAL = '" + XFILIAL("SE1") + "' "
	cQuery += "    AND SE1.E1_EMISSAO >= '" + dToS(aRetOpc[1]) + "' "
	cQuery += "    AND SE1.E1_EMISSAO <= '" + dToS(aRetOpc[2]) + "' "
	cQuery += "    AND SE1.E1_VENCTO >= '" + dToS(aRetOpc[3]) + "' "
	cQuery += "    AND SE1.E1_VENCTO <= '" + dToS(aRetOpc[4]) + "' "
	//cQuery += "    AND SE1.E1_NATUREZ = '" + aRetOpc[7] + "' "
	cQuery += "    AND SE1.E1_CLIENTE = '" + aRetOpc[5] + "' "
	cQuery += "    AND SE1.E1_LOJA = '" + aRetOpc[6] + "' "
	cQuery += "    AND SE1.E1_SALDO > 0 "
	cQuery += "    AND SE1.E1_TIPO NOT IN ('RA','NCC') "
	cQuery += "  ORDER BY " 
	cQuery += "    SE1.E1_FILIAL, "
	cQuery += "    SE1.E1_TIPO, "
	cQuery += "    SE1.E1_NUM, "
	cQuery += "    SE1.E1_PARCELA "

	TcQuery cQuery New Alias XE1
	
	If U_SASP142( aRetOpc[5],aRetOpc[6])
		XE1->(DbCloseArea())
		Return {}
	EndIf

	While XE1->(!Eof())

		aTit2:= {}

		aADD(aTit2,XE1->E1_FILIAL)
		aADD(aTit2,XE1->E1_PREFIXO)
		aADD(aTit2,XE1->E1_NUM)
		aADD(aTit2,XE1->E1_PARCELA)
		aADD(aTit2,XE1->E1_TIPO)
		aADD(aTit2,XE1->E1_VENCTO)
		aADD(aTit2,XE1->E1_SALDO)
		//aADD(aTit2,XE1->E1_HIST)
		//aADD(aTit2,XE1->E1_YMSGBO)
		AaDD(aTit2,"")

		aAdd(aTitulos,aTit2)
		XE1->(DBSKIP())
	EndDo

	XE1->(DBCLOSEAREA())

return aTitulos

User Function CLICKDB()

	IF aTitulos[oMarkBrow:At()][nOk]==""

		aTitulos[oMarkBrow:At()][nOk]:= "1"
		nTotal := nTotal + aTitulos[oMarkBrow:At()][nValor]

	Else
		aTitulos[oMarkBrow:At()][nOk] := ""
		nTotal := nTotal - aTitulos[oMarkBrow:At()][nValor]
	EndIf

Return

Static Function YATUALIZ(aTitulos,oMarkBrow)


	aTitulos:= {}
	U_RTITULB(aTitulos)
	oMarkBrow:SetArray(@aTitulos)
	oMarkBrow:refresh()
	oBrowse:refresh()
	oMarkBrow:goto(1,.T.)

Return

// FUNcaO PARA ATUALIZAR O BOX DE TODOS OS ITENS , COMO MARCADO
Static Function YINVERTE(oMarkBrow)

	For nAx := 1 to len(aTitulos)

		If aTitulos[nAx][nOK] == "1"
			aTitulos[nAx][nOK] := ""
			nTotal := nTotal - aTitulos[nAx][nValor]
		Else
			aTitulos[nAx][nOK] := "1"
			nTotal := nTotal + aTitulos[nAx][nValor]
		EndIf
	Next

	oGet2:Refresh()
	oMarkBrow:refresh()
	oDlg:Refresh()

Return

Static Function CriaVetor(cAliasT)

	Local _cAlias := cAliasT
	Local nUsado  := 0
	Local aCpoVis	:= {}
	Local aHeadT := {}
	Local nPos := 1

	aHeadT	:= {}

	DbSelectArea(_cAlias)
	RegToMemory(_cAlias, .T., .F. )

	//
	// Montagem do aHeadTer                                  
	//
	dbSelectArea("SX3")
	SX3->(dbSetOrder(1))
	MsSeek(_cAlias)
	Do While ( SX3->(!Eof()) .And. (SX3->X3_ARQUIVO == _cAlias) )
		If ( X3USO(SX3->X3_USADO) .And.	cNivel >= SX3->X3_NIVEL )
			nUsado++
			Aadd(aHeadT,{ TRIM(X3Titulo()),;
			SX3->X3_CAMPO,;
			SX3->X3_PICTURE,;
			SX3->X3_TAMANHO,;
			SX3->X3_DECIMAL,;
			"",;
			SX3->X3_USADO,;
			SX3->X3_TIPO,;
			SX3->X3_F3,;
			SX3->X3_CONTEXT,;
			nPos} )

			nPos++
		EndIf

		SX3->(dbSkip())
	EndDo

Return aHeadT

Static Function DEV03B()

	Local aPergs2 := {}
	Local cCond := Space(GETSX3CACHE("E4_CODIGO","X3_TAMANHO"))
	Local dDtVenc := CtoD("")
	Local nJuros := 0
	Local nMulta := 0
	Local nDesconto := 0
	Local nAcrescimo := 0
	Local nDecrescimo := 0
	Local aArea := getarea()
	Local oBrowse
	Local aCoors	 := FwGetDialogSize( oMainWnd )
	Local oFWLayer,oPanelTop1,oView
	Local bBlockClique
	Local nCont := 0
	Local aaCampos  	:= {"Historico","MsgBoleto","Valor","Data"} //Variavel contendo o campo editavel no Grid
	Local aBotoes	:= {}         //Variavel onde sera incluido o botao para a legenda
	Local _nOpca 		:= 0 											//Opcao da confirmacao
	Local oGet1
	Local cGet1 := "Define variable value"
	Local cTipo
	Local bOk 			:= {|| _nOpca:=1,_lRetorno:=.T.,oDlg2:End() } 	//botao de ok
	Local bCancel 		:= {|| _nOpca:=0,_lRetorno:=.T.,oDlg2:End() } 					//botao de cancelamento
	Local oRadMenu1
	Local nRadMenu1 := 1
	Local aButtons := {}

	Private oDlg2
	Private oGet2 := ""
	Private aRetOpc2 := {}
	Private aCondicao := {}
	Private cLinhaOk := {|| VACOLSLOK()}
	//Private cTudoOk := {|| VACOLSTOK()}
	//Private cMaisOk := {|| VACOLSMAIS()}



	Private oLista                    //Declarando o objeto do browser
	Private aCabecalho  := {}         //Variavel que montar o aHeader do grid
	Private aColsEx 	:= {}         //Variavel que receber os dados

	//Declarando os objetos de cores para usar na coluna de status do grid
	Private oVerde  	:= LoadBitmap( GetResources(), "BR_VERDE")
	Private oAzul  		:= LoadBitmap( GetResources(), "BR_AZUL")
	Private oVermelho	:= LoadBitmap( GetResources(), "BR_VERMELHO")
	Private oAmarelo	:= LoadBitmap( GetResources(), "BR_AMARELO")


	Static nVencto   := 1
	Static nValPar   := 2	
	//Static oDlg


	For nZ := 1 to len(aTitulos)

		If !empty(aTitulos[nZ][8])
			nCont++
		EndIf

	Next

	If nCont == 0

		Alert("N"+chr(65533)+"o h"+chr(65533)+" titulos selecionados, o processo ser"+chr(65533)+" interrompido.") 
		Return 

	EndIf


	aAdd( aPergs2 ,{1,"Forma de pagamento: " 			,cCond	,""					,'ExistCpo("SE4")',"SE4"			,'.T.',50	,.T.})
	aAdd( aPergs2 ,{1,"Data de Vencimento: 	" 			,dDtVenc	,""					,'.T.',""			,'.T.',50	,.T.})
	aAdd( aPergs2 ,{1,"Juros: 	",nJuros	,GETSX3CACHE("E1_JUROS","X3_PICTURE")				,'.T.',""			,'.T.',50	,.F.})
	aAdd( aPergs2 ,{1,"Multa: 	" 			,nMulta	,GETSX3CACHE("E1_MULTA","X3_PICTURE")				,'.T.',""			,'.T.',50	,.F.})
	aAdd( aPergs2 ,{1,"Desconto: ",nDesconto	,GETSX3CACHE("E1_DESCONT","X3_PICTURE")				,'.T.',""			,'.T.',50	,.F.})
	aAdd( aPergs2 ,{1,"Acrescimo: ",nAcrescimo	,GETSX3CACHE("E1_ACRESC","X3_PICTURE")				,'.T.',""			,'.T.',50	,.F.})
	aAdd( aPergs2 ,{1,"Decrescimo: ",nDecrescimo	,GETSX3CACHE("E1_DECRESC","X3_PICTURE")				,'.T.',""			,'.T.',50	,.F.})

	If ParamBox(aPergs2,"Filtros:",aRetOpc2,,,,,,,"_RBRL33",.T.,.T.)

		//oDlg2 := MSDialog():New(0, 020, 400, 900,"Entrada de Produtos no Estoque",,,.F.,,,,,,.T.,,,.T. )		
		DEFINE MSDIALOG oDlg2 TITLE "TITULO" FROM 000, 000  TO 400, 1000  PIXEL
		//chamar a funcao que cria a estrutura do aHeader
		CriaCabec()

		//Monta o browser com inclusao, removero e atualizacao
		oLista := MsNewGetDados():New( 053, 078, 415, 1200, GD_DELETE+GD_UPDATE, "Eval(cLinhaOk)", "AllwaysTrue", "AllwaysTrue", aACampos,1, 999, "AllwaysTrue", "AllwaysTrue", "AllwaysTrue", oDlg2, aCabecalho, aColsEx)

		aCondicao := Condicao(nTotal + aRetOpc2[3] + aRetOpc2[4] - aRetOpc2[5] + aRetOpc2[6] - aRetOpc2[7] ,aRetOpc2[1],,aRetOpc2[2])

		for nX := 1 to len(aCondicao)
			aadd(aColsEx,{oVerde,aCondicao[nX][1],aCondicao[nX][2],SPACE(GetSx3Cache("E1_HIST","X3_TAMANHO")),SPACE(GetSx3Cache("E1_YMSGBO","X3_TAMANHO")),.F.})
		next
		//Carregar os itens que irao compor o conteudo do grid
		oLista:SetArray(aColsEx,.T.)

		//oLista:LinhaOk()

		//Atualizo as informacoes no grid

		//Alinho o grid para ocupar todo o meu formulario
		oLista:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT

		//Crio o menu que ira aparece no botao Acoes relacionadas
		aadd(aBotoes,{"NG_ICO_LEGENDA", {||Legenda()},"Legenda","Legenda"})

		//EnchoiceBar(oDlg, {|| oDlg:End() }, {|| oDlg:End() },,aBotoes)

		//ACTIVATE MSDIALOG oDlg CENTERED


		//oLista:AddAction("Confirmar",{||oDlg:End(),DEV03Y()},,,)//aButtons
		//oLista:AddAction("Sair",{||oDlg:End()},,,)//aButtons

		oLista:Refresh()
		//Ao abrir a janela o cursor esta posicionado no meu objeto
		oLista:oBrowse:SetFocus()
		//oLista:TudoOk()
		oLista:AddLine()
		//oBtConf := TButton():New( 20 , 70,"Confirmar",oDlg2,{|| If(fValid("TODOS"),fGrava(),)},037,012,,,,.T.,,"",,,,.F. )

		Activate MsDialog oDlg2 On Init (EnchoiceBar(oDlg2,bOk,bCancel,,)) Centered VALID VACOLSTOK(oLista:Acols)

		IF _nOpca == 1
			DEV03Y()
		endif

	ENDIF
Return

Static Function VACOLSLOK()
	//	
	Local lRet := .T.
	IF EMPTY(aCols[n][4]) .AND. aCols[n][6] == .F.

		MSGINFO("Campo Historico e obrigado","Atencao")
		lRet := .F.
	ELSEIF EMPTY(aCols[n][5]) .AND. aCols[n][6] == .F.

		MSGINFO("Campo Mensagem do Boleto e obrigado","Atencao")
		lRet := .F.
	ENDIF

return lRet



Static Function VACOLSTOK(Acols)

	Local lRet := .T.
	Local nTotais := 0
	Local nSomat := 0

	for nT := 1 to len(Acols)

		IF (EMPTY(aCols[nT][4]) .OR. EMPTY(aCols[nT][5])) .AND. aCols[nT][6] == .F.

			MSGINFO("Algns campos obrigatorios nao foram preenchidos","Atencao")
			lRet := .F.
			exit
		ENDIF

	next

	IF lRet == .T.

		for nT := 1 to len(aCols)
			IF aCols[nT][6] == .F.
				nSomat += aCols[nT][3]
			ENDIF
		next

		nTotais := ((nTotal + aRetOpc2[6] + aRetOpc2[3] + aRetOpc2[4]) - (aRetOpc2[7] + aRetOpc2[5]))
		IF nSomat <> nTotais

			MSGINFO("Soma dos titulos a serem gerados nao e igual ao valor dos titulos selecionados para o acordo! " + "Valor total: " + TRANSFORM(nTotal,"@E 999,999,999.99"),"Atencao")
			lRet := .F.
		ENDIF
	ENDIF
return lRet

Static Function DEV03Y()

	Local aPergs2 := {}
	Local cCond := Space(GETSX3CACHE("E4_CODIGO","X3_TAMANHO"))
	Local dDtVenc := CtoD("")
	Local nJuros := 0
	Local nMulta := 0
	Local nDesconto := 0
	Local nAcrescimo := 0
	Local nDecrescimo := 0
	Local aArea := getarea()
	Local oBrowse
	Local aCoors	 := FwGetDialogSize( oMainWnd )
	Local oFWLayer,oPanelTop1,oView
	Local bBlockClique
	Local nCont := 0
	Private oDlg3
	Private oGet2 := ""
	Private aCondicao := {}
	private aConteudo	:= {}
	Static nVencto   := 2
	Static nVal   		:= 3
	Static nHist 		:= 4
	Static nMsgBo		:= 5
	Static nUltAcols	:= 6

	for nY := 1 to len(oLista:Acols)

		IF oLista:Acols[nY][nUltAcols] == .F.
			AADD(aConteudo,oLista:Acols[nY])
		ENDIF

	next

	oDlg3		:= MSDialog():New( aCoors[1],aCoors[2],aCoors[3],aCoors[4],"Aglutinar titulos",,,.F.,,,,,,.T.,,,.T. )

	oFWLayer	:= FWLayer():New()
	oFWLayer:Init( oDlg3, .F., .T. )
	oFWLayer:AddLine( 'TOP1', 90, .F. )
	oPanelTop1 := oFWLayer:getLinePanel( 'TOP1' )

	oBrowse	    := FWFormBrowse():New()
	oMarkBrow	:= oBrowse:FWBrowse()
	oMarkBrow:SetDataArray()
	oMarkBrow:lAdjust := .F.
	oMarkBrow:SetArray(aConteudo)
	oMarkBrow:SetDescription('Valores')
	oMarkBrow:SetOwner( oDlg3 )
	oMarkBrow:SetProfileID("4")
	oMarkBrow:SetMenuDef("")
	oMarkBrow:SetDoubleClick({|| nil })

	oMarkBrow:AddButton("Confirmar",{||oDlg3:End(),DEV03C()},,,)//aButtons

	//Data Vencimento	
	oMarkBrow:SetColumns({{;
	"Data Vencimento",;  					   			                       // Titulo da coluna
	{|| If(!empty(aConteudo) ,aConteudo[oMarkBrow:At()][nVencto],"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_VENCTO","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_VENCTO","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_VENCTO","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_VENCTO","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};
	)

	//Valor
	oMarkBrow:SetColumns({{;
	"Valor",;  					   			                       // Titulo da coluna
	{|| If(!empty(aConteudo),aConteudo[oMarkBrow:At()][nVal] ,"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_VALOR","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_VALOR","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_VALOR","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_VALOR","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};
	)

	//Historico	
	oMarkBrow:SetColumns({{;
	"Historico",;  					   			                       // Titulo da coluna
	{|| If(!empty(aConteudo),aConteudo[oMarkBrow:At()][nHist] ,"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_HIST","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_HIST","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_HIST","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_HIST","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};
	)
	//Mensagem Boleto	
	oMarkBrow:SetColumns({{;
	"MsgBoleto",;  					   			                       // Titulo da coluna
	{|| If(!empty(aConteudo),aConteudo[oMarkBrow:At()][nMsgBo] ,"")},;  // Code-Block de carga dos dados
	GetSx3Cache("E1_YMSGBO","X3_TIPO"),;                                                        	   // Tipo de dados
	GetSx3Cache("E1_YMSGBO","X3_PICTURE"),;                      			                           // Mascara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("E1_YMSGBO","X3_TAMANHO"),;                       	                      // Tamanho
	GetSx3Cache("E1_YMSGBO","X3_DECIMAL"),;                       	                      // Decimal
	.F.,;                                       			           // Indica se permite a edicao
	{||.T.},;                                   			           // Code-Block de validacao da coluna apos a edicao
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execucao do duplo clique
	NIL,;                                       			           // Variavel a ser utilizada na edicao (ReadVar)
	{||.T.},;                                   			           // Code-Block de execucao do clique no header
	.F.,;                                       			           // Indica se a coluna esta deletada
	.T.,;                     				   				           // Indica se a coluna sera exibida nos detalhes do Browse
	}};
	)

	oMarkBrow:SetMenuDef( 'DEV03' )
	oMarkBrow:Activate()

	oDlg3:Activate(,,,.T.)
	RestArea(aArea)



Return


Static Function CriaCabec()
	Aadd(aCabecalho, {;
	"",;//X3Titulo()
	"IMAGEM",;  //X3_CAMPO
	"@BMP",;		//X3_PICTURE
	3,;			//X3_TAMANHO
	0,;			//X3_DECIMAL
	".F.",;			//X3_VALID
	"",;			//X3_USADO
	"C",;			//X3_TIPO
	"",; 			//X3_F3
	"V",;			//X3_CONTEXT
	"",;			//X3_CBOX
	"",;			//X3_RELACAO
	"",;			//X3_WHEN
	"V"})			//

	Aadd(aCabecalho, {;
	"Data Vencimento",;//X3Titulo()
	"Data",;  //X3_CAMPO
	"",;		//X3_PICTURE
	8,;			//X3_TAMANHO
	0,;			//X3_DECIMAL
	"",;			//X3_VALID
	"",;			//X3_USADO
	"D",;			//X3_TIPO
	"",; 			//X3_F3
	"R",;			//X3_CONTEXT
	"",;			//X3_CBOX
	"",;			//X3_RELACAO
	""})			//X3_WHEN
	Aadd(aCabecalho, {;
	"Valor",;	//X3Titulo()
	"Valor",;  	//X3_CAMPO
	"@E 999,999,999.99",;		//X3_PICTURE
	12,;			//X3_TAMANHO
	2,;			//X3_DECIMAL
	"POSITIVO()",;			//X3_VALID
	"",;			//X3_USADO
	"N",;			//X3_TIPO
	"",;		//X3_F3
	"R",;			//X3_CONTEXT
	"",;			//X3_CBOX
	"",;			//X3_RELACAO
	""})			//X3_WHEN

	Aadd(aCabecalho, {;
	"Historico boleto",;	//X3Titulo()
	"Historico",;  	//X3_CAMPO
	GetSx3Cache("E1_HIST","X3_PICTURE"),;		//X3_PICTURE
	20/*GetSx3Cache("E1_HIST","X3_TAMANHO")*/,;			//X3_TAMANHO
	GetSx3Cache("E1_HIST","X3_DECIMAL"),;			//X3_DECIMAL
	"NAOVAZIO()"/*GetSx3Cache("E1_HIST","X3_VALID")*/,;			//X3_VALID
	GetSx3Cache("E1_HIST","X3_USADO"),;			//X3_USADO
	GetSx3Cache("E1_HIST","X3_TIPO"),;			//X3_TIPO
	GetSx3Cache("E1_HIST","X3_F3"),; 			//X3_F3
	GetSx3Cache("E1_HIST","X3_CONTEXT"),;			//X3_CONTEXT
	GetSx3Cache("E1_HIST","X3_CBOX"),;			//X3_CBOX
	GetSx3Cache("E1_HIST","X3_RELACAO"),;			//X3_RELACAO
	GetSx3Cache("E1_HIST","X3_WHEN")})			//X3_WHEN
	Aadd(aCabecalho, {;
	"Mensagem Boleto",;	//X3Titulo()
	"MsgBoleto",;  	//X3_CAMPO
	GetSx3Cache("E1_YMSGBO","X3_PICTURE"),;		//X3_PICTURE
	20/*GetSx3Cache("E1_YMSGBO","X3_TAMANHO")*/,;			//X3_TAMANHO
	GetSx3Cache("E1_YMSGBO","X3_DECIMAL"),;			//X3_DECIMAL
	GetSx3Cache("E1_YMSGBO","X3_VALID"),;			//X3_VALID
	GetSx3Cache("E1_YMSGBO","X3_USADO"),;			//X3_USADO
	GetSx3Cache("E1_YMSGBO","X3_TIPO"),;			//X3_TIPO
	GetSx3Cache("E1_YMSGBO","X3_F3"),; 			//X3_F3
	GetSx3Cache("E1_YMSGBO","X3_CONTEXT"),;			//X3_CONTEXT
	GetSx3Cache("E1_YMSGBO","X3_CBOX"),;			//X3_CBOX
	GetSx3Cache("E1_YMSGBO","X3_RELACAO"),;			//X3_RELACAO
	GetSx3Cache("E1_YMSGBO","X3_WHEN")})			//X3_WHEN


Return

Static function Legenda()
	Local aLegenda := {}
	AADD(aLegenda,{"BR_AMARELO"     ,"   Afastado" })
	AADD(aLegenda,{"BR_AZUL"    	,"   Ferias" })
	AADD(aLegenda,{"BR_VERDE"    	,"   Situacao Normal" })
	AADD(aLegenda,{"BR_VERMELHO" 	,"   Demitido" })

	BrwLegenda("Legenda", "Legenda", aLegenda)
Return Nil

//Baixa dos titulos selecionados e geracao dos titulos novos
Static Function DEV03C()

	Local aBaixa := {}
	Local nMulta := 0
	Local nJuros := 0
	Local nDesconto := 0
	Local nAcrescimo := 0
	Local nDecrescimo := 0
	Local nCont := 0
	Local cQuery := ""
	Local cNumTit := ""
	Local nErro := 0
	Local aArea := ""
	Local nNumero := 0
	Private lMsErroAuto := .F.


	//Percorre os titulos selecionados pelo usuario

	//Select para retornar o ultimo numero da SE1.
	cQuery := ""
	cQuery += " SELECT ISNULL(MAX(SE1.E1_NUM),0) AS NUMERO"
	cQuery += "   FROM "+ RETSQLNAME("SE1")  + " SE1 "
	cQuery += "  WHERE SE1.D_E_L_E_T_ = '' "
	cQuery += "    AND SE1.E1_TIPO = 'ACF' "
	cQuery += "    AND SE1.E1_FILIAL  = '" +XFILIAL("SE1") + "' "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XE1C",.F.,.T.)	

	cNumTit := PADL(soma1(XE1C->NUMERO),GETSX3CACHE("E1_NUM","X3_TAMANHO"),'0')

	XE1C->(DBCLOSEAREA())	

	BEGIN TRANSACTION
		For nX:= 1 to Len(aTitulos)

			IF aTitulos[nX][8] == '1'
				nNumero++
			ENDIF
			aArea := GetArea()

			If !empty(aTitulos[nX][8])

				//Calculo de valor de Juros
				If aRetOpc2[3] > 0

					nJuros := Round((((aTitulos[nX][7] * 100) / nTotal) * aRetOpc2[3])/100,2)

					DBSELECTAREA("SE1")
					DBSETORDER(1)
					DBSEEK(XFILIAL("SE1") + aTitulos[nX][2] + aTitulos[nX][3] + aTitulos[nX][4] + aTitulos[nX][5])

					If Found()

						RECLOCK("SE1",.F.)

						SE1->E1_JUROS := nJuros

						MSUNLOCK()

					EndIf

				EndIf

				//Calculo de valor de Multa
				If aRetOpc2[4] > 0

					nMulta := Round((((aTitulos[nX][7] * 100) / nTotal) * aRetOpc2[4])/100,2)

					DBSELECTAREA("SE1")
					DBSETORDER(1)
					DBSEEK(XFILIAL("SE1") + aTitulos[nX][2] + aTitulos[nX][3] + aTitulos[nX][4] + aTitulos[nX][5])

					If Found()

						RECLOCK("SE1",.F.)

						SE1->E1_MULTA := nMulta

						MSUNLOCK()

					EndIf

				EndIf

				//Calculo de valor de Desconto
				If aRetOpc2[5] > 0

					nDesconto :=  Round((((aTitulos[nX][7] * 100) / nTotal) * aRetOpc2[5])/100,2)


					DBSELECTAREA("SE1")
					DBSETORDER(1)
					DBSEEK(XFILIAL("SE1") + aTitulos[nX][2] + aTitulos[nX][3] + aTitulos[nX][4] + aTitulos[nX][5])

					If Found()

						RECLOCK("SE1",.F.)

						SE1->E1_DESCONT := nDesconto


						MSUNLOCK()

					EndIf

				EndIf

				//Calculo de valor de Acrescimo
				If aRetOpc2[6] > 0

					nAcrescimo :=  Round((((aTitulos[nX][7] * 100) / nTotal) * aRetOpc2[6])/100,2)

					DBSELECTAREA("SE1")
					DBSETORDER(1)
					DBSEEK(XFILIAL("SE1") + aTitulos[nX][2] + aTitulos[nX][3] + aTitulos[nX][4] + aTitulos[nX][5])

					If Found()

						RECLOCK("SE1",.F.)

						SE1->E1_ACRESC := Round(nAcrescimo,2)
						SE1->E1_SDACRES := Round(nAcrescimo,2)

						MSUNLOCK()

					EndIf

				EndIf

				//Calculo de valor de Decrescimo
				If aRetOpc2[7] > 0

					nDecrescimo :=  (((aTitulos[nX][7] * 100) / nTotal) * aRetOpc2[7])/100

					DBSELECTAREA("SE1")
					DBSETORDER(1)
					DBSEEK(XFILIAL("SE1") + aTitulos[nX][2] + aTitulos[nX][3] + aTitulos[nX][4] + aTitulos[nX][5])

					If Found()

						RECLOCK("SE1",.F.)

						SE1->E1_DECRESC := Round(nDecrescimo,2)
						SE1->E1_SDDECRE := Round(nDecrescimo,2)

						MSUNLOCK()

					EndIf

				EndIf

				DBSELECTAREA("SE1")
				DBSETORDER(1)
				DBSEEK(XFILIAL("SE1") + aTitulos[nX][2] + aTitulos[nX][3] + aTitulos[nX][4] + aTitulos[nX][5])


				If Found()

					Aadd(aBaixa, {"E1_PREFIXO"  ,aTitulos[nX][2]             ,nil})
					Aadd(aBaixa, {"E1_NUM"      ,aTitulos[nX][3]             ,nil})
					Aadd(aBaixa, {"E1_PARCELA"  ,aTitulos[nX][4]          ,nil})
					Aadd(aBaixa, {"E1_TIPO"     , ALLTRIM(aTitulos[nX][5])  ,nil})
					Aadd(aBaixa, {"E1_ACRESC"  ,Round(nAcrescimo,2)          ,nil})
					Aadd(aBaixa, {"E1_SDACRES"     , Round(nAcrescimo,2)  ,nil})
					Aadd(aBaixa, {"E1_DECRESC"  	,Round(nDecrescimo,2)          ,nil})
					Aadd(aBaixa, {"E1_SDDECRE"     , Round(nDecrescimo,2)  ,nil})
					Aadd(aBaixa, {"AUTMOTBX"    ,'ACD'                  ,nil})
					//Aadd(aBaixa, {"AUTBANCO"    ,""         ,Nil})
					//Aadd(aBaixa, {"AUTAGENCIA"  ,""                 ,Nil})               
					//Aadd(aBaixa, {"AUTCONTA"    ,""                   ,Nil})  
					Aadd(aBaixa, {"AUTDTBAIXA"  ,dDataBase              ,nil})
					aAdd(aBaixa, {"AUTDTCREDITO",dDataBase             ,Nil})
					Aadd(aBaixa, {"AUTHIST"     ,"ACORDO FINANCEIRO"     ,nil})
					Aadd(aBaixa, {"AUTJUROS"  ,nJuros              ,nil})
					aAdd(aBaixa, {"AUTMULTA",nMulta             ,Nil})
					Aadd(aBaixa, {"AUTDESCONT"  ,nDesconto              ,nil})
					Aadd(aBaixa, {"AUTVALREC"   ,(aTitulos[nX][7] + Round(nAcrescimo,2) + nJuros + nMulta ) - (Round(nDecrescimo,2) + nDesconto)  ,nil})

					//+ Round(nAcrescimo,2) + nJuros + nMulta ) - (Round(nDecrescimo,2) + nDesconto)

					aBaixa := WsAutoOpc(aBaixa)

					MSExecAuto({|x,y| Fina070(x,y)},aBaixa,3)

					If lMsErroAuto
						nErro++
						MostraErro()
						lMsErroAuto := .F.
						DISARMTRANSACTION()
					Else

						DBSELECTAREA("SE1")
						DBSETORDER(1)
						DBSEEK(XFILIAL("SE1") + aTitulos[nX][2] + aTitulos[nX][3] + aTitulos[nX][4] + aTitulos[nX][5])

						If Found()

							RECLOCK("SE1",.F.)

							SE1->E1_YNUMACO := cNumTit

							MSUNLOCK()

						EndIf

					EndIf


				EndIf



			EndIf

			RestArea(aArea)
			aBaixa := {}

		Next nX

		if nErro == 0	

			//Geracao de novo titulo a RECEBER a partir dos parametros informados pelo usuario.
			for nY := 1 to len(aConteudo)

				lMsErroAuto := .F.

				aArray := { 	{ "E1_PREFIXO"  , aRetOpc[8]          , NIL },;
				{ "E1_FILIAL"   , XFILIAL("SE1")           , NIL },;
				{ "E1_NUM"      , cNumTit           , NIL },;
				{ "E1_PARCELA"  , STRZERO(nY,GETSX3CACHE("E1_PARCELA","X3_TAMANHO")), NIL },;
				{ "E1_TIPO"     , "ACF"           , NIL },;
				{ "E1_NATUREZ"  , aRetOpc[7]           , NIL },;
				{ "E1_CLIENTE"  , aRetOpc[5]          , NIL },;
				{ "E1_LOJA"     , aRetOpc[6]          , NIL },;
				{ "E1_EMISSAO"  , dDataBase           , NIL },;
				{ "E1_HIST"     , ALLTRIM(aConteudo[nY][4])         , NIL },;
				{ "E1_YMSGBO"   , ALLTRIM(aConteudo[nY][5])         , NIL },;
				{ "E1_VENCTO"   , aConteudo[nY][2]     , NIL },;
				{ "E1_CCC"   	, SE1->E1_CCC     , NIL },;
				{ "E1_YCONTRA"  , SE1->E1_YCONTRA     , NIL },;
				{ "E1_YFILIAL"  , SE1->E1_YFILIAL     , NIL },;
				{ "E1_VALOR"    , aConteudo[nY][3]           , NIL } }

				MsExecAuto( { |x,y| FINA040(x,y)} , aArray, 3)  // 3 - Inclusao, 4 - Alteracao, 5 - Exclusao

				If lMsErroAuto
					MostraErro()
					DISARMTRANSACTION()



				EndIf


			next nY


		EndIf

	END TRANSACTION


	If !lMsErroAuto .And. nErro == 0
		MessageBox("Titulo" + SE1->E1_NUM + "criado no Contas a Receber.", "TOTVS", 64)
	EndIf


Return

//Funcao de estorno do titulo aglutinado.
User Function DEV03D()

	Local cQuery := ""
	Local aBaixa := {}
	local cTitFilial := SE1->E1_FILIAL
	Local cTitPrefix := SE1->E1_PREFIXO
	Local cTitNumero := SE1->E1_NUM
	Local cTitParcel := SE1->E1_PARCELA
	Local cTitTipo   := SE1->E1_TIPO
	Local cResp := ""

	Private lMsErroAuto := .F.

	cResp := MessageBox("Essa rotina ir"+chr(65533)+" desfazer o Acordo Financeiro do titulo selecionado. Deseja Continuar?", "TOTVS", 4)

	If cResp == 6

		//Verifica se o titulo selecionado possui outros titulos.
		cQuery := ""
		cQuery += " SELECT count(*) AS QTD "
		cQuery += "   FROM " + RETSQLNAME("SE1") + " SE1 "
		cQuery += "  WHERE SE1.D_E_L_E_T_ = '' "
		cQuery += "    AND SE1.E1_YNUMACO = '" + SE1->E1_NUM + "' "
		cQuery += "    AND SE1.E1_FILIAL = '" + SE1->E1_FILIAL + "' "
		//cQuery += "    AND SE1.E1_TIPO = 'BL' "

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XE1A",.F.,.T.)


		If XE1A->QTD == 0

			Alert("O titulo selecionado n"+chr(65533)+"o "+chr(65533)+" um acordo financeiro. Selecione outro titulo e tente novamente.")

		Else

			//Verifica se ha parcelas baixadas
			cQuery := ""
			cQuery += " SELECT count(*) AS QTD "
			cQuery += "   FROM " + RETSQLNAME("SE1") + " SE1 "
			cQuery += "  WHERE SE1.D_E_L_E_T_ = '' "
			cQuery += "    AND SE1.E1_NUM = '" + SE1->E1_NUM + "' "
			cQuery += "    AND SE1.E1_BAIXA <> '' "
			cQuery += "    AND SE1.E1_FILIAL = '" + SE1->E1_FILIAL + "' "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XE1B",.F.,.T.)

			If XE1A->QTD == 0

				Alert("O titulo j"+chr(65533)+" possui parcelas baixadas. Realize o estorno do pagamento e tente novamente.")

			Else

				cQuery := ""
				cQuery += " SELECT "
				cQuery += "    SE1.E1_FILIAL, "
				cQuery += "    SE1.E1_PREFIXO, "
				cQuery += "    SE1.E1_NUM, "
				cQuery += "    SE1.E1_PARCELA, "
				cQuery += "    SE1.E1_TIPO "
				cQuery += "   FROM " + RETSQLNAME("SE1") +  " SE1 "
				cQuery += "  WHERE SE1.D_E_L_E_T_ = '' "
				cQuery += "    AND SE1.E1_YNUMACO = '" + SE1->E1_NUM + "' "
				cQuery += "    AND SE1.E1_FILIAL = '" + SE1->E1_FILIAL +"' "

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"XE1C",.F.,.T.)

				Begin Transaction

					While XE1C->(!EOF())

						DBSELECTAREA("SE1")
						DBSETORDER(1)
						DBSEEK(XE1C->E1_FILIAL + XE1C->E1_PREFIXO + XE1C->E1_NUM + XE1C->E1_PARCELA + XE1C->E1_TIPO)

						If Found()

							aBaixa := {{"E1_PREFIXO" ,XE1C->E1_PREFIXO    ,Nil},;
							{"E1_NUM"     ,XE1C->E1_NUM        ,Nil},;
							{"E1_PARCELA" ,XE1C->E1_PARCELA      ,Nil},;
							{"AUTJUROS"    ,0             ,nil},;
							{"AUTMULTA"    ,0                      ,nil},;
							{"AUTDESCONT"  ,0                     ,NIL},;
							{"E1_TIPO"    ,XE1C->E1_TIPO           ,Nil}}  

							FINA070(aBaixa,5)

							If lMsErroAuto

								MostraErro()
								DisarmTransaction()

							Else

								RecLock("SE1",.F.)

								SE1->E1_YNUMACO := ""
								SE1->E1_ACRESC := 0
								SE1->E1_DECRESC := 0								

								MsUnlock()

							EndIf

						EndIf

						XE1C->(DBSKIP())
					EndDo
					XE1C->(DBCLOSEAREA())

				End Transaction

			EndIf

			XE1B->(DBCLOSEAREA())
		EndIf
		XE1A->(DBCLOSEAREA())

		If !lMsErroAuto

			cQuery := ""
			cQuery := " UPDATE " + RETSQLNAME("SE1")
			cQuery += "    SET D_E_L_E_T_ = '*',
			cQuery += "      R_E_C_D_E_L_ = R_E_C_N_O_ "
			cQuery += "  WHERE E1_FILIAL = '" + cTitFilial + "' "
			cQuery += "    AND E1_PREFIXO = '" + cTitPrefix + "' "
			cQuery += "    AND E1_NUM = '" + cTitNumero + "' "
			cQuery += "    AND E1_TIPO = '" + cTitTipo + "' "

			If (TCSQLExec(cQuery) < 0)
				MsgStop("TCSQLError() " + TCSQLError())
			EndIf

		EndIf
	EndIF

Return