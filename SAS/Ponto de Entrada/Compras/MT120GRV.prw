// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : MT120GRV.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor             | Descricao
// -----------+-------------------+---------------------------------------------------------
// 26/07/2016 | rogeriojacome     | Gerado com auxilio do Assistente de Codigo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"
#INCLUDE "Topconn.ch"
#include"rwmake.ch"


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MT120GRV
Manuten��o de dados em SB1-Descricao Generica do Produto.

@author    rogeriojacome
@version   11.3.1.201605301307
@since     26/07/2016
/*/
//------------------------------------------------------------------------------------------
user function MT120GRV()
	//-- variaveis -------------------------------------------------------------------------
	Local aArea 	:= GetArea()
	Local cQy 		:= ''
	Local cCampo	:= ''
	Local nMais		:= 0
	Local nValor	:= 0
	Local nAux		:= 0
	Local aAlt 		:= {}
	Local aAltera 	:= {}
	Local aInfo		:= {}
	Local aExcluido	:= {}

	if FUNNAME() == "DEV002" .OR. FUNNAME() == "SASP036_1" .OR. FUNNAME() == "SASP035" .OR. FUNNAME() == "SASP038" .OR. FUNNAME() == "SASP031" .OR. FUNNAME() == "DEV001"
		Return
	Endif
	IF (ALTERA)//Se for alteracao o compras ira receber um email com os dados que foram alterados

		IF __CUSERID $ GETMV("SA_USECOM") //verifica se o usuario esta no parametro que verifica as alteracoes

			cQy := " SELECT * FROM "+RetSqlName("SC7")+" C7 "
			cQy += " INNER JOIN "+RetSqlName("SA2")+" A2 "
			cQy += " ON C7_FORNECE = A2_COD "
			cQy += " AND A2.D_E_L_E_T_ = '' "
			cQy += " WHERE C7.D_E_L_E_T_ = '' "
			cQy += " AND C7_FILIAL = '"+CFILANT+"' "
			cQy += " AND C7_NUM = '"+CA120NUM+"' "
			cQy += " ORDER BY C7_ITEM"
			TcQuery cQy new Alias T10

			AADD(aInfo,{T10->C7_FILIAL,T10->C7_NUM,T10->C7_FORNECE,T10->A2_NOME,T10->C7_LOJA,T10->C7_CC,T10->C7_EMISSAO,date(),time(),cUserName})//Adiciona os campos do cabecalho do layout
			for nT := 1 to len(aCols)
				for nY := 1 to len(aHeader)

					IF !T10->(EOF())

						IF aCols[nT][aScan(aHeader,{|x| Upper(AllTrim(x[2]))== "C7_ITEM"})] == T10->C7_ITEM //Verifica se é o mesmo item
							cCampo := alltrim(aHeader[nY][2])
							//IF cCampo == "C7_IPI"
							//Alert("Chegou")
							//ENDIF
							IF GetSx3Cache(cCampo,"X3_CONTEXT") <> "V" .AND. !empty(GetSx3Cache(cCampo,"X3_Titulo")) .AND. GetSx3Cache(cCampo,"X3_TIPO") <> 'L'	//So os campos diferentes do virtual

								IF GetSx3Cache(cCampo,"X3_TIPO") == 'D' //Se o campo for tipo Data faca a verificacao por datas

									IF aCols[nT][aScan(aHeader,{|x| Upper(AllTrim(x[2])) == cCampo})] <> STOD(T10->&cCampo) //compara as datas para verificar se sao iguais
										AADD(aAlt,{cCampo,aCols[nT][aScan(aHeader,{|x| Upper(AllTrim(x[2]))== cCampo})],STOD(T10->&cCampo)})//Alteracao dos campos de data
									ENDIF

								ELSEIF cvaltochar(aCols[nT][aScan(aHeader,{|x| Upper(AllTrim(x[2])) == cCampo})]) <> cvaltochar(T10->&cCampo) //se for qlqr variavel diferente de uma data 
									AADD(aAlt,{cCampo,alltrim(TRANSFORM(aCols[nT][aScan(aHeader,{|x| Upper(AllTrim(x[2]))== cCampo})],X3Picture( cCampo ))),alltrim(TRANSFORM(T10->&cCampo,X3Picture( cCampo )))})
								ENDIF 

							endif
						ENDIF
					ENDIF
					//cAux++

				next
				IF !empty(aAlt)//So adiciona os itens que tiveram alteracao
					AADD(aAltera,{T10->C7_ITEM,aAlt})
				ENDIF
				aAlt := {}
				T10->(DbSkip())
			next




			T10->(DbGoTop())
			nAux := Contar("T10","!Eof()")
			IF nAux < len(Acols) //Se tiver sido adicionado um item diz quantos itens foram adicionados

				nMais := len(Acols) - nAux

			ENDIF


			IF !empty(aAltera) .OR. !empty(nMais) //So chama a funcao que passa o email caso tenha alguma alteracao no arquivo 
				U_SASP126(aAltera,aInfo,nMais,nAux)//Chama a funcao que envia o email com as alteracoes
			ENDIF
			T10->(DbCloseArea())

		ENDIF
	ENDIF
	RestArea(aArea)


	//-- encerramento ----------------------------------------------------------------------

return


//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 26/07/2016 as 11:09:06
//-- fim de arquivo--------------------------------------------------------------------------

