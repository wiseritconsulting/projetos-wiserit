/*#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "Topconn.ch"
#INCLUDE "Rwmake.ch"

user function SASP059(cPFil,cNum)

	Local aCond		:= {}
	Local nTotal 	:= 0
	Local nFrete 	:= 0
	Local lPrimeira := .T. 
	Local cMailAp   
	Local cNum 		:= IIF(VALTYPE(cNum)=='U',SC7->C7_NUM,cNum)
	Local aArea := GetArea()
	Local cEmissao 
	Local cPFil := IIF(VALTYPE(cPFil)=='U',SC7->C7_FILIAL,cPFil) 
	
	MsgInfo("Pedido de Compra gerado, Achei!!! " + SC7->C7_NUM  ,"Pedido de Compra" )

	//Procura nome do aprovador 
	_cQuery := " SELECT A.CR_USER,A.CR_NIVEL,A.CR_TIPO "
	_cQuery += " FROM SCR010 A "
	_cQuery += " WHERE A.D_E_L_E_T_ = '' "
	_cQuery += "  AND A.CR_TIPO='PC' "
	_cQuery += "  AND A.CR_NUM = '"+cNum+"' "
	_cQuery += "  AND A.CR_FILIAL='"+xFilial("SCR")+"' "
	_cQuery += "  AND CR_NIVEL = "
	_cQuery += "    ( SELECT MIN(CR_NIVEL) "
	_cQuery += "     FROM SCR010 B "
	_cQuery += "     WHERE B.D_E_L_E_T_ = '' "
	_cQuery += "       AND B.CR_TIPO='PC' "
	_cQuery += "       AND B.CR_NUM = '"+cNum+"' "
	_cQuery += "       AND B.CR_FILIAL='"+xFilial("SCR")+"' "
	_cQuery += "       AND B.CR_DATALIB='' ) "

	TCQUERY _cQuery NEW ALIAS "AAA" 


	MemoWrite( "c:/temp/sqlscr.txt", _cQuery )	
	AAA->(DBGotop())

	DO While !AAA->(Eof()) 	
		cAprov := AAA->CR_USER
		dbSelectArea('SC7')
		dbSetOrder(1)
		dbSeek(xFilial('SC7')+cNum+SC7->C7_ITEM+SC7->C7_SEQUEN) 	

		oProcess := TWFProcess():New( "000002", "Aprov. Pedido de Compras" )
		oProcess:NewTask( "Solicitação", "\WORKFLOW\HTM\WFW120P.HTM" )
		oProcess:cSubject := "Liberação PC "+cNum
		oHTML := oProcess:oHTML

		/*** Preenche os dados do cabecalho ***/
		/*
		cEmissao := dtoc(SC7->C7_EMISSAO)
		
		//AAdd( (oHTML:ValByName( "cEmissao", cEmissao)))	
		oHTML:ValByName( "cEmissao", cEmissao)
		dbCloseArea('SC7')
		
		dbSelectArea('SA2')
		dbSetOrder(1)
		dbSeek(xFilial('SA2')+SC7->C7_FORNECE+SC7->C7_LOJA)       
		oHtml:ValByName( "cFornecedor", SC7->C7_FORNECE +' - '+ SA2->A2_NREDUZ ) 
		oHtml:ValByName( "cCCusto", SC7->C7_CC ) 

		//Pego as condiicoes de Pagamento
		dbSelectArea('SE4')
		DBSETORDER(1)
		dbSeek(xFilial('SE4') + SC7->C7_COND)
		ccond := SE4->E4_DESCRI
		oHtml:ValByName( "cCondPagto", CCOND ) 

		nTotal := 0 
		nFrete := 0   

		
		//dbSelectArea('SC7')
		PswOrder(1)
		IF PswSeek(SC7->C7_USER,.T.)
			aInfo   := PswRet(1)
			ccompra := alltrim(aInfo[1,2])	       
		ENDIF
		//oHtml:ValByName( "nrcot"	, ncot )
		oHtml:ValByName( "cComprador", ccompra )
		oHtml:ValByName( "cPc"	, SC7->C7_NUM )
		oProcess:fDesc := "Pedido de Compras No "+ cNum                                                                        
		dbSetOrder(1)
		//dbSeek(xFilial('SC7')+cNum)
		SC7->(dbSeek(xFilial('SC7')+alltrim(SC7->C7_NUM)))
		While !Eof() .and. SC7->C7_NUM = cNum
			nTotal := nTotal + SC7->C7_TOTAL
			nFrete := nFrete + SC7->C7_VALFRE       
			AAdd( (oHtml:ValByName( "PC.Item"   )),SC7->C7_ITEM )		
			AAdd( (oHtml:ValByName( "PC.Produto" )),SC7->C7_PRODUTO )		       

			dbSelectArea('SB1')
			dbSetOrder(1)
			dbSeek(xFilial('SB1')+SC7->C7_PRODUTO)
			dbSelectArea('SC7')

			IF C7_DESCRI <> '0'
				AAdd( (oHtml:ValByName( "PC.Dsc" )),SC7->C7_DESCRI )		              
			ELSE
				AAdd( (oHtml:ValByName( "PC.Dsc" )), SB1->B1_DESC )		              
			ENDIF

			AAdd( (oHtml:ValByName( "PC.Qtd" )),TRANSFORM(SC7-> C7_QUANT,'@E 999,999.99' ) )		              
			AAdd( (oHtml:ValByName( "PC.PrcUn" )),TRANSFORM(SC7->C7_PRECO,'@E 999,999.99' ) )		                     
			AAdd( (oHtml:ValByName( "PC.Total" )),TRANSFORM(SC7->C7_TOTAL,'@E 999,999.99' ) )		                     
			AAdd( (oHtml:ValByName( "PC.Un" )),SC7->C7_UM )		              
			AAdd( (oHtml:ValByName( "PC.Entrega" )),dtoc(SC7->C7_DATPRF))		          
			AAdd( (oHtml:ValByName( "PC.Obs" )),SC7->C7_OBS )


			_cQuery := "SELECT TOP 1 D1_EMISSAO, D1_VUNIT"
			_cQuery += " FROM SD1010 D1"
			_cQuery += " WHERE D1.D_E_L_E_T_ = '' AND D1_COD = '"+SC7->C7_PRODUTO+"' AND D1_FILIAL = '"+xFilial("SC7")+"'"
			_cQuery += " ORDER BY D1_EMISSAO DESC "

			TCQUERY _cQuery NEW ALIAS "BBB" 			

			dbSelectArea("BBB")
			
			dUltcomp := DTOC(STOD( BBB->D1_EMISSAO ))
			cvlrcomp := TRANSFORM( BBB->D1_VUNIT ,'@E 999,999.99' )

			BBB->(dbCloseArea())
			dbSelectArea('SC7')

			AAdd( (oHtml:ValByName( "PC.ultcom" )),dUltcomp )
			AAdd( (oHtml:ValByName( "PC.vlrutlcom" )),cvlrcomp )

			DBSkip()
			
		Enddo

		oHtml:ValByName( "cTotal" ,TRANSFORM( nTotal,'@E 999,999.99' ) )		              	
		oHtml:ValByName( "cFrSg" ,TRANSFORM( nFrete,'@E 999,999.99' ) )		              	    
		oHtml:ValByName( "cTotGer" ,TRANSFORM( (nTotal + nFrete),'@E 999,999.99' ) )

		PswOrder(1)

		PswSeek(cAprov,.T.)

		aInfo   := PswRet(1)
		cMailAp := alltrim(aInfo[1,14])+";"
		
		cChave = MD5( cPFil + AAA->CR_TIPO + PADR(cNum,50) + AAA->CR_NIVEL , 2 )
		cAprov := AAA->CR_USER
		
		oHtml:ValByName( "cChave"	 	, cChave )
		oHtml:ValByName( "cRegistro"	, cPFil + AAA->CR_TIPO + PADR(cNum,50) + AAA->CR_NIVEL )
		oHtml:ValByName( "cAprov"	, cAprov )

		oProcess:cTo := cMailAp
		oProcess:Start() 

		AAA->(dBskip())      

	ENDDO 	
		
	AAA->(dbclosearea()) 
	BBB->(dbclosearea()) 

	RestArea(aArea)
	Return(.T.)

RETURN
*/