#Include 'Protheus.ch'
#include "Topconn.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � MT080GRV  � Autor � Carlos Meneses  � Data �  12/09/2014   ���
�������������������������������������������������������������������������͹��
���Descricao � Ponto de Entrada para excluir  o cadastro da TES           ���
���          � automaticamente em todas as empresas, de que n�o exista    ���
���          � movimenta��o.                                              ���
�������������������������������������������������������������������������͹��
���Uso       � Na exclus�o do Cadastro de TES                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function MA080VLD()

Local aArea   := GetArea()
Local _lExc := .t.
Local aDados  := {}
Local _cEmp := ""
Local cFilBak	:= cFilAnt
Local cEmpBak	:= cEmpAnt
Local cCdAlias  := "SF4"
Local nOpcao := ParamIXB[1]

Local nCont
Local nCtEmp
Private _aEmp := {}


If nOpcao == 2
	
	dbselectarea("SM0")
	SM0->(dbgotop())
	
	While SM0->(!Eof())
		If Substr(SM0->M0_CODFIL,1,2)<>_cEmp .AND. SM0->M0_CODIGO<>"99"
			AADD(_aEmp,{"M0_CODFIL",SM0->M0_CODFIL,nil })
		EndIf
		_cEmp := Substr(SM0->M0_CODFIL,1,2)
		SM0->(dbskip())
	EndDo
	
	dbSelectArea("SX3")
	dbSetOrder(1)
	dbSeek(cCdAlias)
	
	While SX3->(!Eof()) .And. (SX3->x3_arquivo == cCdAlias)
		IF X3USO(SX3->x3_usado) .and. Alltrim(SX3->x3_campo) <> "F4_FILIAL"
			AADD(aDados, {Alltrim(SX3->x3_campo),&("M->"+Alltrim(SX3->x3_campo)), NIL })
		Endif
		SX3->(dbSkip())
	Enddo
	
	cCodTes := SF4->F4_CODIGO
	
	If !FBusMov(SF4->F4_CODIGO) //SE N�O TIVER MOVIMENTA��O PERMITE A EXCLUS�O
		
		Begin Transaction
		
		For nCtEmp:=1 to Len(_aEmp)
			
			cFilAnt := _aEmp[nCtEmp,2]
			
			If cFilAnt <> cFilBak
				
				dbSelectArea("SF4")
				SF4->(dbgotop())
				dbSetOrder(1)
				
				If dbSeek(xFilial("SF4")+cCodTes)
					
					RecLock("SF4",.f.)
					SF4->(dbDelete())
					SF4->(MsUnLock())
					
				EndIf
				
			EndIf
			
		Next
		
		End Transaction
		
	Else
		Alert("Registro da TES n�o poder� ser exclu�da, pois j� existe movimenta��o!")	
		_lExc := .f.
		cFilAnt := cFilBak
		cEmpAnt	:= cEmpBak
		RestArea(aArea)
		Return(_lExc)
	EndIf
		
EndIf

cFilAnt := cFilBak
cEmpAnt	:= cEmpBak
RestArea(aArea)

Return(_lExc)

//======================================================
// Fun��o para verificar se existe movimenta��o como TES
//======================================================
Static Function FBusMov(TES)
Local _lret := .f.
Local _cNao := "S"
Local nCt

For nCt:=1 to Len(_aEmp)
	
	
	//CONSULTA PEDIDO VENDA
	cQSC6 := "	SELECT COUNT(*) AS CONT FROM " + RetSQLName("SC6")  + " SC6 WHERE D_E_L_E_T_='' AND SUBSTRING(C6_FILIAL,1,2)='"+SUBSTRING(_aEmp[nCt,2],1,2)+"' AND C6_TES='"+TES+"' "
	If Select("cQSC6")> 0
		cQSC6->(DBCLOSEAREA())
	Endif
	TCQUERY cQSC6 NEW ALIAS "cQSC6"
	
	
	//CONSULTA PEDIDO COMPRAS
	cQSC7 := "	SELECT COUNT(*) AS CONT FROM " + RetSQLName("SC7")  + " SC7 WHERE D_E_L_E_T_='' AND SUBSTRING(C7_FILIAL,1,2)='"+SUBSTRING(_aEmp[nCt,2],1,2)+"' AND C7_TES='"+TES+"' "
	If Select("cQSC7")> 0
		cQSC7->(DBCLOSEAREA())
	Endif
	TCQUERY cQSC7 NEW ALIAS "cQSC7"
	
	
	//ITEM NF COMPRA
	cQSD1 := "	SELECT COUNT(*) AS CONT FROM " + RetSQLName("SD1")  + " SD1 WHERE D_E_L_E_T_='' AND SUBSTRING(D1_FILIAL,1,2)='"+SUBSTRING(_aEmp[nCt,2],1,2)+"' AND D1_TES='"+TES+"' "
	If Select("cQSD1")> 0
		cQSD1->(DBCLOSEAREA())
	Endif
	TCQUERY cQSC6 NEW ALIAS "cQSD1"
	
	
	//ITEM NF VENDA
	cQSD2 := "	SELECT COUNT(*) AS CONT FROM " + RetSQLName("SD2")  + " SD2 WHERE D_E_L_E_T_='' AND SUBSTRING(D2_FILIAL,1,2)='"+SUBSTRING(_aEmp[nCt,2],1,2)+"' AND D2_TES='"+TES+"' "
	If Select("cQSD2")> 0
		cQSD2->(DBCLOSEAREA())
	Endif
	TCQUERY cQSD2 NEW ALIAS "cQSD2"
	
	If cQSC6->CONT > 0 .OR. cQSC7->CONT > 0 .OR. cQSD1->CONT > 0 .OR. cQSD2->CONT > 0 .AND. _cNao == "S"
		_lret := .t.
		_cNao := "N"
	Endif
	
Next

Return(_lret)
