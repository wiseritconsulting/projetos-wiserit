#Include 'Protheus.ch'

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � MT080GRV  � Autor � Carlos Meneses  � Data �  12/09/2014   ���
�������������������������������������������������������������������������͹��
���Descricao � Ponto de Entrada para replicar o cadastro da TES           ���
���          � automaticamente em todas as empresas.                      ���
�������������������������������������������������������������������������͹��
���Uso       � Na inclus�o do Cadastro de TES                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function MT080GRV() 

Local aArea   := GetArea()
Local _cEmp := ""
Local _aEmp := {}
Local cFilBak	:= cFilAnt
Local cEmpBak	:= cEmpAnt

Local aSF4		:= {}, aDadosSF4	:= {}
Local nCont
Local nCtEmp

dbselectarea("SM0")
SM0->(dbgotop())

While SM0->(!Eof())
	If Substr(SM0->M0_CODFIL,1,2)<>_cEmp .AND. SM0->M0_CODIGO<>"99"
		AADD(_aEmp,{"M0_CODFIL",SM0->M0_CODFIL,nil })
	EndIf
	_cEmp := Substr(SM0->M0_CODFIL,1,2)
	SM0->(dbskip())
EndDo

If INCLUI
	
	Begin Transaction
	
	aSF4	:= SF4->(DBStruct())
	//COPIA TES SELECIONADO
	For nCont:=1 to Len(aSF4)
		AADD(aDadosSF4,{aSF4[nCont][1],SF4->(&(aSF4[nCont][1]))})
	Next
	
	For nCtEmp:=1 to Len(_aEmp)
		
		cFilAnt := _aEmp[nCtEmp,2]
		
		If SUBSTR(cFilAnt,1,2) <> SUBSTR(cFilBak,1,2)
			
			RecLock("SF4",.T.)
			For nCont:=1 to Len(aDadosSF4)
				SF4->(&(aDadosSF4[nCont][1]))	:= aDadosSF4[nCont][2]
			Next
			SF4->F4_FILIAL := SUBSTR(cFilAnt,1,2)
			SF4->(MsUnLock())
			
		EndIf
		
	Next
	
	End Transaction
	
EndIf

cFilAnt := cFilBak
cEmpAnt	:= cEmpBak
RestArea(aArea)

Return
