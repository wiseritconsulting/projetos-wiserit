#Include 'Protheus.ch'
#Include 'Topconn.ch'

/*/{Protheus.doc} A250ETRAN
Ponto de entrada ap�s gera��o do PCP para gera��o das Notas de Sa�da/Entrada
@author Diogo/Saulo Martins
@since 03/11/2014
@version 1.0
@see http://tdn.totvs.com/display/mp/A250ETRAN
/*/
User Function A250ETRAN()
	Local aArea		:= GetArea()
	Local aAreaSC2	:= SC2->(GetArea())
	Local aAreaSD3	:= SD3->(GetArea())
	Local lOk			:= .F.
	Private lRet		:= .F.
	Private aDocFat	:= {}
	Private aInsumos	:= {}
	Private cPedPV	:= ""
	Private cPedPC	:= ""
	Private cMsgErro	:= ""
	Private c2Cod		:= SC2->C2_PRODUTO
	Private c2Qtd		:= SC2->C2_QUANT
	Private d3Op		:= SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN
	Private d3Loc		:= SC2->C2_LOCAL

	If !Empty(SC2->C2_DATRF) .AND. MsgYesNo("Deseja gerar notas?")
		SD3->(dbSetOrder(1))	//D3_FILIAL+D3_OP+D3_COD+D3_LOCAL
		if SD3->(dbSeek(xFilial("SD3")+Padr(d3Op,GetSx3Cache("D3_OP","X3_TAMANHO"))))
			While SD3->(!EOF()) .and. SD3->(D3_FILIAL+D3_OP) == xFilial("SD3")+Padr(d3Op,GetSx3Cache("D3_OP","X3_TAMANHO"))
				If SD3->D3_ESTORNO<>"S"
					Aadd(aInsumos,{SD3->D3_COD,SD3->D3_QUANT,SD3->D3_CUSTO1,SD3->D3_CF})
				EndIf
				SD3->(dbSkip())
			EndDo
		Endif
		If aScan(aInsumos,{|x| "RE" $ x[4]})==0
			MsgAlert("Insumos n�o encontrados!")
			Return
		EndIf
		Processa({|| lRet	:= FDadosPV(aInsumos)},"Gera��o do Pedido de Compra/Venda","Aguarde ...",.F.)
		If lRet
			Processa({|| lOk := StaticCall(CADSZ5,FGeraFat,cFilAnt,cPedPV,@aDocFat,@cMsgErro," ")},"Gerando Nota Fiscal de Sa�da","Aguarde ...",.F.)
			cDocEnt:= aDocFat[1]
			cSerEnt:= aDocFat[2]
			GravDest(cDocEnt,cSerEnt,cPedPV,cPedPC)
			If lOk
				Processa({|| lOk := StaticCall(CADSZ5,FatuPC,cFilAnt,cPedPC,0,"","",cDocEnt,cSerEnt) },"Gerando Nota Fiscal de Entrada","Aguarde ...",.F.)
			EndIf
			If !lOk
				Alert("Erro ao gerar Nota Fiscal! "+Transform(cMsgErro,""))
			Else
				MsgInfo("Nota Fiscal "+cDocEnt+" Serie "+cSerEnt+" Gerado com sucesso!")
			EndIf
		EndIf
	Endif
	RestArea(aAreaSD3)
	RestArea(aAreaSC2)
	RestArea(aArea)
Return

Static Function FDadosPV(aInsumos)
	Local aCab			:= {}
	Local aCabPC		:= {}
	Local aItens		:= {}
	Local aItensPC	:= {}
	Local nCont		:= 1
	Local cCNPJ		:= FWArrFilAtu("01",cFilAnt)[18]
	Local cTesPV		:= SuperGetMv("SA_TESAI",.F.,"",cFilAnt)
	Local cTesPC		:= SuperGetMv("SA_TESENT",.F.,"",cFilAnt)
	Local cCliCol,cLjCol := ""
	Local lRet

	SA1->(DbSetOrder(3))
	If SA1->(DbSeek(xFilial("SA1")+cCNPJ))
		cCliCol	:= SA1->A1_COD
		cLjCol		:= SA1->A1_LOJA
	Else
		Aviso("Aten��o","Cadastrar Cliente para efetuar a gera��o da Nota. CNPJ: "+cCNPJ,{"Ok"})
		Return .F.
	EndIf

	SA2->(DbSetOrder(3))
	If SA2->(DbSeek(xFilial("SA2")+cCNPJ))
		cForPed	:= SA2->A2_COD
		cForLj		:= SA2->A2_LOJA
	Else
		Aviso("Aten��o","Cadastrar Fornecedor para efetuar a gera��o da Nota. CNPJ: "+cCNPJ,{"Ok"})
		Return .F.
	EndIf

	AADD(aCab,{"C5_TIPO"			,"N"		,Nil})
	AADD(aCab,{"C5_CLIENTE"			,cCliCol	,Nil})
	AADD(aCab,{"C5_LOJACLI"			,cLjCol		,Nil})
	AADD(aCab,{"C5_CONDPAG"			,'001'		,Nil})
	AADD(aCab,{"C5_TPEDAV"			,'28'		,Nil}) 

	cPC	:= CriaVar('C7_NUM'		, .T.)
	aadd(aCabPC,{"C7_NUM"		,cPC})
	aadd(aCabPC,{"C7_EMISSAO"	,dDataBase})
	aadd(aCabPC,{"C7_FORNECE"	,cForPed})
	aadd(aCabPC,{"C7_LOJA"		,cForLj})
	aadd(aCabPC,{"C7_COND"		,"001"})
	aadd(aCabPC,{"C7_CONTATO"	,"AUTO"})
	aadd(aCabPC,{"C7_FILENT"	,cFilAnt})


	For i:= 1 To len(aInsumos)
		If "RE" $ aInsumos[i][4]
			AADD(aItens,{})		//Adiciona uma linha
			nTam:= Len(aItens)
			//nVlrItem:= FBuscaVlr(aInsumos[i][1])
			AADD(aItens[nTam],{"C6_ITEM"	,RetAsc(nCont,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.)	,nil})
			AADD(aItens[nTam],{"C6_PRODUTO"	,aInsumos[i][1]	,nil})
			AADD(aItens[nTam],{"C6_QTDVEN"	,aInsumos[i][2]	,nil})
			AADD(aItens[nTam],{"C6_QTDLIB"	,aInsumos[i][2]	,nil})
			AADD(aItens[nTam],{"C6_PRCVEN"	,aInsumos[i][3]/aInsumos[i][2]	,nil})
			AADD(aItens[nTam],{"C6_TES"		,cTesPV			,nil})
			nCont+= 1
		ElseIf "PR" $ aInsumos[i][4]
			//nVlrItem:= FBuscaVlr(c2Cod)
			aLinha := {}
			aadd(aLinha,{"C7_PRODUTO"	,aInsumos[i][1]	,Nil})
			aadd(aLinha,{"C7_QUANT"		,aInsumos[i][2]	,Nil})
			aadd(aLinha,{"C7_PRECO"		,aInsumos[i][3]/aInsumos[i][2]	,Nil})
			aadd(aLinha,{"C7_TES"		,cTesPC			,Nil})
			aadd(aItensPC,aLinha)
		EndIf
	Next

	lRet	:= Pedido(aCab,aItens,aCabPC,aItensPC)

Return lRet

Static Function Pedido(aCab,aItens,aCabPC,aItensPC)
	Local cFilBak			:= cFilAnt
	Local aCabCliente		:= aClone(aCab)
	Local aCabCol			:= aClone(aCab)
	Local nPosCliente		:= aScan(aCab,{|x| x[1]=="C5_CLIENTE"})
	Local nPosLoja		:= aScan(aCab,{|x| x[1]=="C5_LOJACLI"})
	Local nPosTes			:= aScan(aItens[1],{|x| x[1]=="C6_TES"})
	Local nCont
	Default aItens		:= {}
	Private lMsErroAuto	:= .F.
	Private cMat120Num	:= ""
	ProcRegua(2)

	IncProc("Criando pedido de vendas")

	ProcessMessage()

	MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabCliente,aItens,3)	// 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o

	If lMsErroAuto
		AutoGrLog("Erro na gera��o do pedido de vendas")
		MostraErro()
		Return .F.
	Else
		cPedPV	:= SC5->C5_NUM
	Endif

	//Gera��o do Pedido de Compra
	IncProc("Criando Pedido de Compra")
	lMsErroAuto	:= .F.
	cMat120Num		:= ""	//Variavel preenchida na A120Grava
	MsExecAuto({|x,y,z| MATA120(1,x,y,z)}, aCabPC,aItensPC,3)

	If lMsErroAuto
		AutoGrLog("Erro ao gerar pedido de compra!")
		MostraErro()
		Return .F.
	Else
		cPedPC:= cMat120Num
	Endif

Return .T.
/*
Static Function FBuscaVlr(cProd)
	Local nRet:= 0
	//Verifica se tem na SD1
	cSql := "SELECT TOP 1 D1_CUSTO AS CUSTO, SD1.R_E_C_N_O_ FROM " + RetSqlName("SD1")+ " SD1 "
	cSql += "JOIN " + RetSqlName("SF4")+" SF4 ON D1_TES = F4_CODIGO AND F4_FILIAL = '"+xFilial("SF4")+"'"
	cSql += " WHERE SF4.D_E_L_E_T_ =' ' AND "
	cSql += " SD1.D_E_L_E_T_ =' ' AND "
	cSql += " D1_TIPO<>'D' AND "
	cSql += " F4_ESTOQUE = 'S' AND "
	cSql += " D1_COD = '"+cProd+"' "
	cSql += " ORDER BY SD1.R_E_C_N_O_ "


	TCQUERY cSql NEW ALIAS "XD1TOT"

	If XD1TOT->(!EOF())
		nRet:= XD1TOT->CUSTO
	Else
		//Caso n�o tenha, buscar do Saldo Inicial
		nRet:= Posicione("SB9",1,xFilial("SB9")+cProd,"B9_CM1")
	EndIf
	XD1TOT->(dbCloseArea())
	If nRet <= 0
		nRet:= Posicione("SB2",1,xFilial("SB2")+cProd,"B2_CM1")
	EndIF

	If nRet <= 0
		Alert("Produto "+cProd+" sem valor de custo!")
	EndIf
Return nRet*/

Static Function GravDest(cDocEnt,cSerEnt,cPedidoV,cPedidoC)
	If SF2->(FieldPos("F2_FILDEST"))>0 .And. SF2->(FieldPos("F2_FORDES"))>0 .And. SF2->(FieldPos("F2_LOJADES"))>0
		SF2->(dbSetOrder(1))	//F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO
		SC5->(dbSetOrder(1))	//C5_FILIAL+C5_NUM
		SC7->(dbSetOrder(1))	//C7_FILIAL+C7_NUM+C7_ITEM+C7_SEQUEN
		If SC5->(DbSeek(xFilial("SC5")+cPedidoV))
			If SF2->(dbSeek(xFilial("SF2")+PadR(cDocEnt,TamSX3("D2_DOC")[1])+cSerEnt+SC5->(C5_CLIENTE+C5_LOJACLI)))
				If SC7->(DbSeek(xFilial("SC7")+cPedidoC))
					RecLock("SF2",.F.)
					SF2->F2_FILDEST:= cFilAnt
					SF2->F2_FORDES := SC7->C7_FORNECE
					SF2->F2_LOJADES:= SC7->C7_LOJA
					SF2->F2_FORMDES:= "N"
					MsUnlock()
				EndIf
			EndIf
		EndIf
	EndIf
Return