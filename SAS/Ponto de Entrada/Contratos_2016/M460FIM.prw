#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWMBROWSE.CH"
#include "dbtree.ch" 
#include "topconn.ch"

User Function M460FIM()

	U_Carregf2()

Return

user function Carregf2()

	Local _nota := ""
	Local _serie := ""
	Local cCodKit := ""
	Local cQuery := ""
	Local cEmpres := ""
	Local cArea := GetArea()

	_nota := SF2->F2_DOC
	_serie := SF2->F2_SERIE
	_pref := SF2->F2_PREFIXO

	
	cQuery := ""
	cQuery += " UPDATE " + RETSQLNAME("SE1")
	cQuery += " SET E1_YCONTRA = '" + SC5->C5_YCONTRA + "', "
	cQuery += "     E1_YMEDICA = '" + SC5->C5_YMEDICA + "', "
	cQuery += "     E1_YFILIAL = '" + SC5->C5_YFILIAL + "' "
	cQuery += " WHERE D_E_L_E_T_ = '' "
	cQuery += "   AND E1_FILIAL = '" + XFILIAL("SE1",SC5->C5_FILIAL) + "' "
	cQuery += "   AND E1_PREFIXO = '" + _pref + "' "
	cQuery += "   AND E1_NUM = '" + _nota + "' "
	cQuery += "   AND E1_PEDIDO = '" + SC5->C5_NUM + "' "
	cQuery += "   AND E1_ORIGEM = 'MATA460' "	

	TCSQLExec(cQuery) 

	DbSelectArea("SD2")
	DbSetOrder(3)
	DbSeek(xFilial("SD2")+ _nota+ _serie)

	WHILE !eof() .and. SF2->F2_FILIAL + _nota + _serie == SD2->D2_FILIAL+SD2->D2_DOC+SD2->D2_SERIE

		cCodKit := POSICIONE("SC6",2,XFILIAL("SD2") + SD2->D2_COD + SD2->D2_PEDIDO, "SC6->C6_YCODKIT")
		cCodPai := POSICIONE("SC6",2,XFILIAL("SD2") + SD2->D2_COD + SD2->D2_PEDIDO, "SC6->C6_YPAIKIT")
		cYco    := POSICIONE("SC6",2,XFILIAL("SD2") + SD2->D2_COD + SD2->D2_PEDIDO, "SC6->C6_YCO")
		cYinpco := POSICIONE("SC6",2,XFILIAL("SD2") + SD2->D2_COD + SD2->D2_PEDIDO, "SC6->C6_YINTPCO")
		cCusto  := POSICIONE("SC6",2,XFILIAL("SD2") + SD2->D2_COD + SD2->D2_PEDIDO, "SC6->C6_YCCUSTO")

		RECLOCK("SD2",.F.)

		SD2->D2_YCODKIT := cCodKit
		SD2->D2_YPAIKIT := cCodPai	
		SD2->D2_YFILIAL := SC5->C5_YFILIAL
		SD2->D2_YCONTRA := SC5->C5_YCONTRA
		SD2->D2_YMEDICA := SC5->C5_YMEDICA	
		SD2->D2_YCO     := cYco
		SD2->D2_YINTPCO := cYinpco
		SD2->D2_CCUSTO  := cCusto

		MSUNLOCK()

		SD2->(dbSkip())
	ENDDO

	
	RestArea(cArea)

return

