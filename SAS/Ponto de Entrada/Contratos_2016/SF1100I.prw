#Include 'Protheus.ch'

User Function SF1100I()

	CarregaF1()

Return

Static Function CarregaF1()

	Local _nota := ""
	Local _serie := ""
	Local cCodKit := ""
	Local cPedido := ""
	
	_nota := SF1->F1_DOC
	_serie := SF1->F1_SERIE
  
	DbSelectArea("SD1")
	DbSetOrder(3)
	DbSeek(xFilial("SD1")+ _nota+ _serie)
	 
	WHILE !eof() .and. SF1->F1_FILIAL + _nota + _serie == SD1->D1_FILIAL+SD1->D1_DOC+SD1->D1_SERIE
		
		cPedido := SD1->D1_PEDIDO
		
		cCodKit := POSICIONE("SC6",2,XFILIAL("SD1") + SD1->D1_COD + SD1->D1_PEDIDO, "SC6->C6_YCODKIT")
		cCodPai := POSICIONE("SC6",2,XFILIAL("SD1") + SD1->D1_COD + SD1->D1_PEDIDO, "SC6->C6_YPAIKIT")
		
		RECLOCK("SD1",.F.)
		
		SD1->D1_YCODKIT := cCodKit
		SD1->D1_YPAIKIT := cCodPai
		
		MSUNLOCK()
		
		SD1->(dbSkip())
	ENDDO	 
		
REturn
