#Include 'Protheus.ch'

/*/{Protheus.doc} MT410BRW
Ponto de entrada para alterar o Pergunte no Documento de entrada
@author Weskley Silva
@since 10/09/2015
@version 1.0
/*/

User Function M103BROW()

	Local aPerguntas

	PERGUNTE("MTA103",.F.,,,,,@aPerguntas)
	MV_PAR01        := 2
	MV_PAR02		:= 1
	MV_PAR03		:= 2
	MV_PAR04		:= 2
	MV_PAR05		:= 2
	MV_PAR06		:= 1
	MV_PAR07		:= 1
	MV_PAR08		:= 2
	MV_PAR09		:= 1	
	MV_PAR010		:= 2
	MV_PAR011		:= 1	
	MV_PAR012		:= 1
	MV_PAR013		:= 2
	MV_PAR014		:= 1
	MV_PAR015		:= 1
	MV_PAR016		:= 1
	MV_PAR017		:= 2
	MV_PAR018		:= 2
	MV_PAR019		:= 2
	MV_PAR020		:= 2
	MV_PAR021		:= 2
	MV_PAR022		:= 2
	__SaveParam("MTA103",@aPerguntas)
Return

