#include "rwmake.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MT100LOK�Autor  �Mconsult              � Data �  21/01/2016 ���
�������������������������������������������������������������������������͹��
���Desc.     �Validacao do centro de custo                                ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/


User Function MT100LOK

_cCc  	:= ascan(aheader,{|x|upper(alltrim(x[2]))=="D1_CC"})
_cRateio:= ascan(aheader,{|x|upper(alltrim(x[2]))=="D1_RATEIO"})
_cItem  := ascan(aheader,{|x|upper(alltrim(x[2]))=="D1_ITEM"})
_cCfOP  := ascan(aheader,{|x|upper(alltrim(x[2]))=="D1_CF"})
_cTes  := ascan(aheader,{|x|upper(alltrim(x[2]))=="D1_TES"})
_Ret    := .t.

// verifica se � transfer�ncia e n�o exige cc ou rateio
lTransf := procname(9) == "A310PROC"

DbSelectArea("SM0")                    
DbGoTop("SM0")
While !SM0->(EOF())

If( SM0->M0_CGC == POSICIONE("SA2", 1, xFilial("SA2") + CA100FOR + CLOJA, "A2_CGC")) .OR. (aCols[n,_cTes]== "048") .OR. (aCols[n,_cTes]== "047") .OR. (aCols[n,_cTes]== "054")

return(_Ret)

EndIf

SM0->(DbSkip())
EndDo
DbGoTop("SM0")


If !aCols[n,Len(acols[n])]
	If Empty(aCols[n,_cCc]) .AND. aCols[n,_cRateio] == '2' .and. !lTransf .and. substr(aCols[n,_cCfop],2,3) <> '202' .and. SF4->F4_ESTOQUE == 'N' //.and. !substr(SA2->A2_CGC,1,8)$ "06267630_19002277_20443574_20433296_20112038_19483034_22209244_22716940_23973137"
		MessageBox("Dever� ser informado o centro de custo ou rateio. Verifique! ","AVISO",32)
		_Ret := .F.
	ElseIf  Empty(aCols[n,_cCc]) .AND. aCols[n,_cRateio] == '1'
		ascan(aheader,{|x|upper(alltrim(x[2]))=="D1_ITEM"})
		_Ret := .F.
		For i := 1 to Len(aBackColsSDE)
			If aBackColsSDE[i][1] == aCols[n,_cItem]
				For x := 1 to Len(aBackColsSDE[i][2])
					If aBackColsSDE[i][2][x][1] <> " "
						_Ret := .T.
					EndIf
				Next x
			EndIf
		Next i
		If !_Ret .and. !lTransf
			MessageBox("Dever� ser informado o centro de custo ou rateio. Verifique! ","AVISO",32)
		EndIF
	EndIf
EndIf

Return(_Ret)
