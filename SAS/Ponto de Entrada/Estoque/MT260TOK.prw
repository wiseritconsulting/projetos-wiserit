#include 'protheus.ch'
#include 'parmtype.ch'

// Esse ponto de entrada tem por objetivo verificar se o produto origem e destino s�o iguais no momento da transferencia. 
// Weskley Silva 07/10/2016 

user function MT260TOK()

	Local lRet := .T.
	
	If (! Empty(CCODORIG) .And. ! Empty(CCODDEST))
		If (AllTrim(CCODORIG) == AllTrim(CCODDEST))
			lRet := .T.
		Else
			lRet := .F. 
			MsgAlert("Aten��o! Produto Origem e Destino deve ser iguais.")
		EndIf
	EndIf
	
return lRet