#Include 'Protheus.ch'
/*/{Protheus.doc} MT103IPC
Importao do Pedido para a Nota Fiscal de Entrada
Uso para gravacao dos campos customizados referente a conta contabil

@version 1.0
@see http://tdn.totvs.com/display/public/mp/MT103IPC+-+Atualiza+campos+customizados+no+Documento+de+Entrada

/*/

User Function MT103IPC()

		RCOMA001()
 
Return

Static Function RCOMA001()



 Local nYCO    := aScan(aHeader,{|x| Upper(AllTrim(x[2]))=="D1_YCO"}) 
 Local nYCLA    := aScan(aHeader,{|x| Upper(AllTrim(x[2]))=="D1_YCLAORC"})
 Local nPosPed  := aScan(aHeader,{|x| Upper(AllTrim(x[2]))=="D1_PEDIDO"})
 Local nPosItem := aScan(aHeader,{|x| Upper(AllTrim(x[2]))=="D1_ITEM"})
 Local nPosProd	:= aScan(aHeader,{|x| Upper(AllTrim(x[2]))=="D1_COD"})
 Local nPosDesc	:= aScan(aHeader,{|x| Upper(AllTrim(x[2]))=="D1_YDESCR"})
 Local nPosPro := ""

 Local nPedido  := ""
 Local nCont    := 1
 Local cDesc	:= ""
 
 nPedido :=  aCols[nCont,nPosPed]

  
 For nCont := 1 To Len(aCols)
  
  
  cItem := aCols[nCont,nPosItem]  
  //nPosPro	:= Posicione("SC7",1,xFilial("SC7")+nPedido+cItem,"C7_PRODUTO") 
  nPosPro := aCols[nCont,nPosProd]
  
  IF !(FUNNAME() $ "SASP057")//SE FOR DEVOLUCAO NAO ALTERA OS CAMPOS DE PCO POIS ELES JA VEM DO PARAMETRO SA_YCODEV
  ////aCols[nCont,nYCO]     := Posicione("SC7",1,xFilial("SC7")+nPedido+cItem,"C7_YCO")
  ////aCols[nCont,nYCLA]     := Posicione("SC7",1,xFilial("SC7")+nPedido+cItem,"C7_YCLAORC")
  ENDIF
  aCols[nCont,nPosDesc]     := Posicione("SB1",1,xFilial("SB1")+nPosPro,"B1_DESC")
 
 Next

Return