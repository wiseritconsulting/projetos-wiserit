#Include 'Protheus.ch'

/*/{Protheus.doc} MA261D3
Localizado na fun��o A261Grava() apos a  grava��o dos movimentos de origem e destino do SD3.
EM QUE PONTO : E chamado apos a grava��o dos movimentos de origem e destino de cada item de transferencia. 
Pode ser utilizado para atualizar campos no momento da gravacao.

	
@author TOTVS
@since 26/10/2016
@version 1.0		

@return Nil, Nulo 

@see http://tdn.totvs.com/pages/releaseview.action?pageId=6087617
/*/
User Function MA261D3()

	Local cDOCD3 := SD3->D3_DOC


	If Funname() == "MATA410"
		If empty(SC5->C5_YDOCTRF)
			RecLock("SC5",.F.)
				SC5->C5_YDOCTRF	:= cDOCD3
			MsUnlock()
		Endif
	Endif

Return

