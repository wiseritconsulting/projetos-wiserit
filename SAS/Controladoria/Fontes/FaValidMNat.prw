#include 'protheus.ch'
#include 'parmtype.ch'
/*/{Protheus.doc} FaValidMNat
O ponto de entrada FaValidMNat � destinado a permitir uma valida��o da GetDados de Multiplas Naturezas
@author Saulo Gomes Martins
@since 25/02/2016
@version P11
@return lRet, Valida��o OK
@see http://tdn.totvs.com/pages/releaseview.action?pageId=6071254
/*/

User function FaValidMNat()
	Local cFunName	:= Paramixb[1]
	Local lRet		:= .T.
	If !GDDeleted() .and. GDFieldGet("EV_RATEICC")!="1"
		MsgAlert("Preencher informa��es do rateio de centro de custo!")
		lRet	:= .F.
	EndIf
Return lRet