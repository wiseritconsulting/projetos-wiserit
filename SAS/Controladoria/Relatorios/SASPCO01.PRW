#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"

User Function SASPCO01


	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local oPrinter
	Local cLocal          := "c:\temp\"
	Local cFilePrint := ""
	Local oFont1    := TFont():New( "Tahoma",,8,,.F.,,,,,.F.) 
	Local oFont2B   := TFont():New( "Tahoma",,10,,.T.,,,,,.F.)
	Local oFont3    := TFont():New( "Tahoma",,14,,.T.,,,,,.F.)	
    Local aRet := {}
    Local aPergs := {}	
    Local aCC := {}
    Local cTitulo := "" 
    Local cCusto := ""
    Private _cSql := ""
    
        _cSql := "SELECT CTT_CUSTO FROM CTT010 CTT INNER JOIN SAL010 SAL ON AL_COD = CTT.CTT_YGPRES AND SAL.D_E_L_E_T_='' WHERE SAL.AL_USER='"+__cUserID+"' AND CTT.D_E_L_E_T_=''"

		IF Select("T01") > 0
			dbCloseArea("T01")
		EndIf

        TcQuery _cSql New Alias T01

        IF T01->(Eof()) 
        	Alert("Usu�rio sem CTT vinculado.")
        	Return
        EndIF
        
        While !Eof()
        	AADD(aCC,T01->CTT_CUSTO)
        	dbSkip()
        Enddo
        
        cOpc := {"EXCEL","PDF","EMAIL"}
		aAdd( aPergs,{1,"Data Base:"     ,Space(6),"","","","",50,.T.})
		aAdd( aPergs,{1,"Filial:"        ,Space(6),"","","SM0","",50,.T.})
		aAdd( aPergs,{2,"Centro de Custo",aCC[1],aCC,50,"",.T.})
		aAdd( aPergs,{2,"Formato"        ,cOpc[1],cOpc,50,"",.T.})
		aAdd( aPergs,{1,"E-Mail:"     ,Space(250),"","","","",200,.T.})
		

		
		If ParamBox(aPergs,"Pergunta",@aRet)
			dRefer := aRet[1]
			cFil := aRet[2]
			cCusto := aRet[3]
			cFormato := aRet[4]
			cEmail := aRet[5]
			cTitulo := alltrim(cCusto)+" - "+posicione("CTT",1,xFilial("CTT")+cCusto,"CTT_DESC01")
			 
		Else
			Alert("Cancelado pelo usu�rio.")
			return
		Endif


			_cSql  = "SELECT * FROM dbo.FUN_PCO_SAS('"+dRefer+"', '"+cFil+"', '"+cCusto+"')"
		
			IF Select("T01") > 0
				dbCloseArea("T01")
			EndIf
		
		    TcQuery _cSql New Alias T01
		    
		    IF T01->(Eof()) 
		    	Alert("Nenhum registro encontrado.")
		    	Return
		    EndIF

		IF cFormato == "PDF" // pdf

			oPrinter := FWMSPrinter():New('PCO.PDF', , lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
			oPrinter:SetPortrait()
			oPrinter:SetPaperSize(DMPAPER_A4)
			oPrinter:SetMargin(60,60,60,60)
			oPrinter:StartPage()
			
			oPrinter:SayBitmap( 20,10,"C:\temp2\lgsas.png",50 ,30 )
			oPrinter:Say(30,100,"ACOMPANHAMENTO OR�AMENTO - "+substr(dRefer,5,2)+"/"+substr(dRefer,1,4),oFont3)
			oPrinter:Say(50,100,cTitulo,oFont3)			
			
			oPrinter:Say(80,5,"CONTA",oFont2B)
			oPrinter:Say(80,55,"DESCRICAO",oFont2B)
			oPrinter:Say(80,265+20,"OR",oFont2B)
			oPrinter:Say(80,320+20,"PR",oFont2B)
			oPrinter:Say(80,375+20,"EM",oFont2B)
			oPrinter:Say(80,430+20,"RE",oFont2B)
			oPrinter:Say(80,500+20,"TOTAL",oFont2B)
					
			linha := 90
			WHILE !EOF()
				oPrinter:Say(linha,5 ,T01->CO			, oFont1)
				oPrinter:Say(linha,55,T01->CODESCRI 	,oFont1)
				oPrinter:Say(linha,265,TRANSFORM(T01->VLRORC	,"@E 999,999,999.99") 	    ,oFont1,,)
				oPrinter:Say(linha,320,TRANSFORM(T01->VLRPR	    ,"@E 999,999,999.99")		,oFont1,,)
				oPrinter:Say(linha,375,TRANSFORM(T01->VLREM	    ,"@E 999,999,999.99")		,oFont1,,)
				oPrinter:Say(linha,430,TRANSFORM(T01->VLRRE	    ,"@E 999,999,999.99")		,oFont1,,)
				oPrinter:Say(linha,500,TRANSFORM(T01->TOTAL	    ,"@E 999,999,999.99")		,oFont1,,iif(T01->TOTAL<0,CLR_HRED,))
				linha := linha + 10      
				DBSKIP()
			ENDDO
				
			oPrinter:EndPage()
				
			oPrinter:Preview()
		
		ElseIF cFormato == "EXCEL" 
		
				oExcel := FWMSEXCEL():New()
				oExcel:AddworkSheet(cCusto)
				oExcel:AddTable (cCusto,cTitulo)
				oExcel:AddColumn(cCusto,cTitulo,"Conta",1,1)
				oExcel:AddColumn(cCusto,cTitulo,"Descri��o",2,2)
				oExcel:AddColumn(cCusto,cTitulo,"Or�ado",3,3)
				oExcel:AddColumn(cCusto,cTitulo,"Previsto",1,1)
				oExcel:AddColumn(cCusto,cTitulo,"Empenhado",1,1)
				oExcel:AddColumn(cCusto,cTitulo,"Realizado",1,1)
				oExcel:AddColumn(cCusto,cTitulo,"Total",1,1)												

				dbSelectArea("T01")
				WHILE T01->(!EOF())
					oExcel:AddRow(cCusto,cTitulo,{T01->CO,T01->CODESCRI,T01->VLRORC,T01->VLRPR,T01->VLREM,T01->VLRRE,T01->TOTAL})
					T01->(DBSKIP())
				ENDDO
			
				oExcel:Activate()
				oExcel:GetXMLFile(cLocal+"PCO"+alltrim(cCusto)+".xml")		
		ElseIF cFormato == "EMAIL" 
		
			oProcess := TWFProcess():New("MAILPCO", "ACOMPANHAMENTO DE OR�AMENTO")
			oProcess:NewTask("FORMULARIO", "\workflow\MAILPCO.HTM")
			ohtml := oProcess:oHtml
			
			oProcess:ohtml:ValByName("cCabec", "ACOMPANHAMENTO OR�AMENTO - "+substr(dRefer,5,2)+"/"+substr(dRefer,1,4) )
			oProcess:ohtml:ValByName("cTitulo", cTitulo )
			
				dbSelectArea("T01")
				WHILE T01->(!EOF())
					AAdd( (oProcess:oHtml:ValByName( "OR.CONTA" ))    ,T01->CO )
					AAdd( (oProcess:oHtml:ValByName( "OR.DESCRI" ))   ,T01->CODESCRI )		              
					AAdd( (oProcess:oHtml:ValByName( "OR.ORCADO" ))   ,TRANSFORM(T01->VLRORC,'@E 999,999,999.99' ) )
					AAdd( (oProcess:oHtml:ValByName( "OR.PREVISTO" )) ,TRANSFORM(T01->VLRPR, '@E 999,999,999.99' ) )	
					AAdd( (oProcess:oHtml:ValByName( "OR.EMPENHADO" )),TRANSFORM(T01->VLREM, '@E 999,999,999.99' ) )	
					AAdd( (oProcess:oHtml:ValByName( "OR.REALIZADO" )),TRANSFORM(T01->VLRRE, '@E 999,999,999.99' ) )	
					AAdd( (oProcess:oHtml:ValByName( "OR.TOTAL" ))    ,TRANSFORM(T01->TOTAL, '@E 999,999,999.99' ) )			
					T01->(DBSKIP())
				ENDDO
			oProcess:ohtml:ValByName("dt", dtoc(dDatabase) )
			oProcess:ohtml:ValByName("hora", str(time(),1,5) )
			
			oProcess:cTo:= cEmail //USRRETMAIL(aAprovad[i])
			oProcess:cSubject	:= "ACOMPANHAMENTO OR�AMENTO - "+substr(dRefer,5,2)+"/"+substr(dRefer,1,4)
			oProcess:Start()
		
		
		Endif

Return