#Include "TopConn.CH"
#Include "Protheus.CH"

/*/
Autor.....: Gustavo Pedrosa - Mconsult
Data......: 10/02/2016
Descri��o.: ORCADO X REALIZADO QUEBRANDO POR EMPRESA E CC
/*/

User Function RCTBR02()
Private cPerg		:= "RCTBR02"

AjustaSX1(cPerg)

If !Pergunte(cPerg, .T.)
	Return
Endif

oReport := ReportDef(cPerg) //Caixa de dialogo do relatorio
oReport:PrintDialog()//Caixa de dialogo do relatorio
Return


Static Function ReportDef(cPerg)

Local oReport
Local oSection1
Local oBreak
Local aOrdem 	:= {}
Local cTitulo := 'ORCADO X REALIZADO QUEBRANDO POR EMPRESA E CC'

oReport := TReport():New(cPerg,cTitulo,cPerg,{|oReport| PrintReport(oReport)},cTitulo)
oReport:SetLandscape()
oReport:SetTotalinLine(.F.)
oReport:ShowHeader()

oSection1 := TRSection():New(oReport,cTitulo,{"CTT"})
oSection1:SetTotalinLine(.F.)

oSection2 := TRSection():New(oReport,cTitulo,{"CT3"})
oSection2:SetTotalinLine(.F.)

TRCell():New( oSection1, "EMPRESA",,"Empresa")	// CODIGO VENDEDOR

TRCell():New( oSection2, "CTT_CUSTO"	,,,,06,,)	// CLIENTE
TRCell():New( oSection2, "CTT_DESC01"		,,,,04,,)	// LOJA

TRCell():New( oSection2, "TOT_ORC"	,,"Total / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "TOT_REA"	,,"Total / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "TOT_VAR"	,,"Total / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_01"	,,"Jan / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_01"	,,"Jan / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_01"	,,"Jan / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_02"	,,"Fev / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_02"	,,"Fev / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_02"	,,"Fev / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_03"	,,"Mar / Orcado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_03"	,,"Mar / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_03"	,,"Mar / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_04"	,,"Abr / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_04"	,,"Abr / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_04"	,,"Abr / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_05"	,,"Mai / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_05"	,,"Mai / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_05"	,,"Mai / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_06"	,,"Jun / Orcado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_06"	,,"Jun / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_06"	,,"Jun / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_07"	,,"Jul / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_07"	,,"Jul / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_07"	,,"Jul / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_08"	,,"Ago / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_08"	,,"Ago / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_08"	,,"Ago / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_09"	,,"Set / Orcado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_09"	,,"Set / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_09"	,,"Set / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_10"	,,"Out / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_10"	,,"Out / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_10"	,,"Out / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_11"	,,"Nov / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_11"	,,"Nov / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_11"	,,"Nov / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection2, "ORC_12"	,,"Dez / Orcado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "REA_12"	,,"Dez / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "VAR_12"	,,"Dez / % Var","@E 999.99",6,,)	// TOTAL
/*
TRCell():New( oSection2, "ABR"		,,"Quantidade","@E 99,999,999.99",15,,)	// QUANTIDADE
TRCell():New( oSection2, "MAI"	,,"Total","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "JUN"	,,"Total","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "JUL"	,,"Total","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "AGO"	,,"Total","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "SET"	,,"Total","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "OUT"	,,"Total","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "NOV"	,,"Total","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection2, "DEZ"	,,"Total","@E 99,999,999.99",15,,)	// TOTAL
*/
/*
oBreak  := TRBreak():New(oSection1, oSection1:Cell("F2_VEND1"), "TOTAL DO VENDEDOR" )// Quebra por grupo de produto.
TRFunction():New(oSection1:Cell("QTD")	,NIL,"SUM",oBreak,,,,.F.,.T.,.F.)
TRFunction():New(oSection1:Cell("TOTAL")	,NIL,"SUM",oBreak,,,,.F.,.T.,.F.)
*/
Return oReport

/*Inicia Logica Print Report */

Static Function PrintReport(oReport)

Local oSection1 	:= oReport:Section(1)
Local oSection2 	:= oReport:Section(2)
Local oBreak
Local cGrupos		:= 	Implode(UsrRetGrp(cUserName),"/")

  
cQuery := " SELECT * FROM 
cQuery += " (SELECT CT3_FILIAL,CTT_CUSTO,CTT_DESC01,'A'+CT3_TPSALD+SUBSTRING(CT3_DATA,5,2) MES,CASE WHEN SUM(CT3_DEBITO) > SUM(CT3_CREDIT) THEN SUM(CT3_DEBITO - CT3_CREDIT) ELSE SUM((CT3_CREDIT - CT3_DEBITO)*-1) END  VALOR
cQuery += "   FROM "+RetSqlName("CTT")+" CTT"
cQuery += "  INNER JOIN "+RetSqlName("CT3")+" CT3 ON CT3_FILIAL='"+xFilial("CT3")+"' AND CTT_CUSTO=CT3_CUSTO AND CT3.D_E_L_E_T_=' ' AND SUBSTRING(CT3_DATA,1,4) = '"+MV_PAR03+"' AND CT3_TPSALD IN ('0','1')"+CRLF 
cQuery += " 		AND CT3_CUSTO BETWEEN '"+MV_PAR01+"' AND '"+MV_PAR02+"'"+CRLF
cQuery += " 		AND CT3_FILIAL BETWEEN '"+SUBSTR(MV_PAR04,1,4)+"' AND '"+SUBSTR(MV_PAR05,1,4)+"'"+CRLF
cQuery += "   WHERE CTT_FILIAL='      ' AND CTT.D_E_L_E_T_=' '  AND CTT_CUSTO=CT3_CUSTO"
IF !UPPER(cUserName) $ UPPER(GETNEWPAR("MV_YUSRPCO","")) //!("000000" $ cGrupos .Or. "000011" $ cGrupos .Or. "000024" $ cGrupos)
	cQuery += " AND CTT_YGPRES IN (SELECT AL_COD FROM "+RetSqlName("SAL")+" AL WHERE AL_FILIAL='"+xFilial("SAL")+"' AND AL_USER='"+RetCodUsr()+"' AND AL.D_E_L_E_T_=' ')"+CRLF
EndIF
cQuery += " GROUP BY CT3_FILIAL,CTT_CUSTO,CTT_DESC01,CT3_TPSALD,SUBSTRING(CT3_DATA,5,2)) AS TABELA
cQuery += " PIVOT
cQuery += "  (SUM(VALOR)
cQuery += "  FOR MES IN ( [A001], [A002], [A003], [A004],[A005],[A006],[A007],[A008],[A009],[A010],[A011],[A012],[A101], [A102], [A103], [A104],[A105],[A106],[A107],[A108],[A109],[A110],[A111],[A112])) AS PIVOTTABLE



 /*
cQuery := " SELECT"+CRLF
cQuery += " CT3_FILIAL,CTT_CUSTO,CTT_DESC01,ORC_01,ORC_02,ORC_03,ORC_04,ORC_05,ORC_06,ORC_07,ORC_08,ORC_09,ORC_10,ORC_11,ORC_12"+CRLF
cQuery += " ,[01] REA_01,[02] REA_02,[03] REA_03, [04] REA_04,[05] REA_05,[06] REA_06,[07] REA_07,[08] REA_08,[09] REA_09,[10] REA_10,[11] REA_11,[12] REA_12"+CRLF
cQuery += " FROM ("+CRLF
cQuery += " SELECT * FROM ("+CRLF
cQuery += " SELECT"+CRLF
cQuery += " ORCADO.CT3_FILIAL,CTT_CUSTO,CTT_DESC01,ORC_01,ORC_02,ORC_03,ORC_04,ORC_05,ORC_06,ORC_07,ORC_08,ORC_09,ORC_10,ORC_11,ORC_12"+CRLF
cQuery += " ,CASE WHEN SUM(CT3_DEBITO) > SUM(CT3_CREDIT) THEN SUM(CT3_DEBITO - CT3_CREDIT) ELSE SUM(CT3_CREDIT - CT3_DEBITO)*-1 END REALIZADO,SUBSTRING(CT3_DATA,5,2) MES"+CRLF
cQuery += " FROM ("+CRLF
cQuery += " SELECT CT3_FILIAL,CTT_CUSTO,CTT_DESC01,[01] ORC_01,[02] ORC_02,[03] ORC_03, [04] ORC_04,[05] ORC_05,[06] ORC_06,[07] ORC_07,[08] ORC_08,[09] ORC_09,[10] ORC_10,[11] ORC_11,[12] ORC_12"+CRLF
cQuery += " FROM "+CRLF
cQuery += " (SELECT CT3_FILIAL,CTT_CUSTO,CTT_DESC01,SUBSTRING(CT3_DATA,5,2) MES,CASE WHEN SUM(CT3_DEBITO) > SUM(CT3_CREDIT) THEN SUM(CT3_DEBITO - CT3_CREDIT) ELSE SUM((CT3_CREDIT - CT3_DEBITO)*-1) END  ORCADO"+CRLF
cQuery += "  FROM "+RetSqlName("CTT")+" CTT"+CRLF
cQuery += "  INNER JOIN "+RetSqlName("CT3")+" CT3 ON CT3_FILIAL='"+xFilial("CT3")+"' AND CTT_CUSTO=CT3_CUSTO AND CT3.D_E_L_E_T_=' ' AND SUBSTRING(CT3_DATA,1,4) = '"+MV_PAR03+"' AND CT3_TPSALD='0'"+CRLF 
cQuery += " 		AND CT3_CUSTO BETWEEN '"+MV_PAR01+"' AND '"+MV_PAR02+"'"+CRLF
cQuery += " 		AND CT3_FILIAL BETWEEN '"+SUBSTR(MV_PAR04,1,4)+"' AND '"+SUBSTR(MV_PAR05,1,4)+"'"+CRLF
cQuery += "  WHERE CTT_FILIAL='"+xFilial("CTT")+"' AND CTT.D_E_L_E_T_=' ' "+CRLF
IF !UPPER(cUserName) $ UPPER(GETNEWPAR("MV_YUSRPCO","")) //!("000000" $ cGrupos .Or. "000011" $ cGrupos .Or. "000024" $ cGrupos)
	cQuery += " AND CTT_YGPRES IN (SELECT AL_COD FROM "+RetSqlName("SAL")+" AL WHERE AL_FILIAL='"+xFilial("SAL")+"' AND AL_USER='"+RetCodUsr()+"' AND AL.D_E_L_E_T_=' ')"+CRLF
EndIF
cQuery += "  GROUP BY CT3_FILIAL,CTT_CUSTO,CTT_DESC01,SUBSTRING(CT3_DATA,5,2)) AS TABELA"+CRLF
cQuery += " PIVOT"+CRLF
cQuery += " (SUM(ORCADO)"+CRLF
cQuery += " FOR MES IN ( [01], [02], [03], [04],[05],[06],[07],[08],[09],[10],[11],[12])"+CRLF
cQuery += " ) AS PivotTable) ORCADO"+CRLF

cQuery += " INNER JOIN "+RetSqlName("CT3")+" CT32 ON CT32.CT3_FILIAL=ORCADO.CT3_FILIAL AND CT32.CT3_CUSTO=CTT_CUSTO AND CT3_TPSALD='1' AND SUBSTRING(CT3_DATA,1,4) = '"+MV_PAR03+"'"+CRLF
cQuery += " GROUP BY ORCADO.CT3_FILIAL,CTT_CUSTO,CTT_DESC01,ORC_01,ORC_02,ORC_03,ORC_04,ORC_05,ORC_06,ORC_07,ORC_08,ORC_09,ORC_10,ORC_11,ORC_12,SUBSTRING(CT3_DATA,5,2) "+CRLF
cQuery += " ) TABELA"+CRLF
cQuery += " ) AS TAB"+CRLF

cQuery += " PIVOT"+CRLF
cQuery += " (SUM(REALIZADO)"+CRLF
cQuery += " FOR MES IN ( [01], [02], [03], [04],[05],[06],[07],[08],[09],[10],[11],[12])"+CRLF
cQuery += " ) AS PIVOTT"+CRLF
   */
TcQuery cQuery New Alias T01

DbSelectArea("T01")

Count to nReg

T01->(dbGoTop())

oReport:SetMeter(nReg)
cFil	:= ""
cCC		:= ""

DO While !oReport:Cancel() .And. !T01->(EOF())
	
	oReport:IncMeter()
	IF cFil <> T01->CT3_FILIAL
		IF !Empty(cFil)
			oSection2:Finish()
			oSection1:Finish()
		EndIF
		oSection1:Init()
		DbSelectArea("SM0")
		DbSeek(cEmpAnt+SUBSTR(T01->CT3_FILIAL,1,4))
		oSection1:Cell("EMPRESA"  ):SetValue( SM0->M0_NOMECOM 	)
		oSection1:PrintLine()
		oSection2:Init()
		cFil := T01->CT3_FILIAL
	EndIF
	oSection2:Cell("CTT_CUSTO" 	):SetValue( T01->CTT_CUSTO 	)
	oSection2:Cell("CTT_DESC01" ):SetValue( T01->CTT_DESC01 )
	
	nTotalOrc	:= 0
	nTotalRea	:= 0
	
	For nCont := 1 To 12
		oSection2:Cell("ORC_"+STRZERO(nCont,2) 	):SetValue( T01->&("A0"+STRZERO(nCont,2)) 	)
		oSection2:Cell("REA_"+STRZERO(nCont,2) 	):SetValue( T01->&("A1"+STRZERO(nCont,2))	)
		oSection2:Cell("VAR_"+STRZERO(nCont,2) 	):SetValue( CalcVar(T01->&("A0"+STRZERO(nCont,2)) , T01->&("A1"+STRZERO(nCont,2)))	)
		
		nTotalOrc	+= T01->&("A0"+STRZERO(nCont,2))
		nTotalRea	+= T01->&("A1"+STRZERO(nCont,2))
	Next
	oSection2:Cell("TOT_ORC" 	):SetValue( nTotalOrc )
	oSection2:Cell("TOT_REA"    ):SetValue( nTotalRea 	)
	oSection2:Cell("TOT_VAR" 	):SetValue(CalcVar( nTotalOrc , nTotalRea 	))
	oSection2:PrintLine()
	
	T01->(dbSkip())
	
	IF cFil <> T01->CT3_FILIAL
		IF T01->(Eof())
			oSection2:Finish()
		EndIF
	EndIF
EndDo

oSection1:Finish()
T01->(dbCloseArea())

Return

Static Function CalcVar(nOrc,nRea)
Local nRet := 0

nRet := (nRea / nOrc) * 100

Return nRet
//-------------------------------------------------------------------------------------------------------------

Static Function Implode(array, char)
Local nCont
Local cRet	:= ""
For nCont := 1 To Len(array)
	cRet += array[ncont] + char
Next
Return(cRet)

Static Function AjustaSX1(cPerg)
PutSx1(cPerg, "01","Do C.Custo"			,""		,""		,"mv_ch01","C",TamSx3("CTT_CUSTO")[1],0,0,"G",""	,"CTT"	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
PutSx1(cPerg, "02","Ate C.Custo"		,""		,""		,"mv_ch02","C",TamSx3("CTT_CUSTO")[1],0,0,"G",""	,"CTT"	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
PutSx1(cPerg, "03","Ano"				,""		,""		,"mv_ch03","C",04,0,0,"G",""	,""		,"","","mv_par03"," ","","","","","","","","","","","","","","","")
PutSx1(cPerg, "04","Da Filial"			,""		,""		,"mv_ch01","C",TamSx3("CTT_FILIAL")[1],0,0,"G",""	,"SM0"	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
PutSx1(cPerg, "05","Ate Filial"			,""		,""		,"mv_ch02","C",TamSx3("CTT_FILIAL")[1],0,0,"G",""	,"SM0"	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
Return
