#Include "TopConn.CH"
#Include "Protheus.CH"

/*/
Autor.....: Gustavo Pedrosa - Mconsult
Data......: 10/02/2016
Descri��o.: ORCADO X REALIZADO SEM QUEBRAS
/*/

User Function RCTBR03()
Private cPerg		:= "RCTBR03"

AjustaSX1(cPerg)

If !Pergunte(cPerg, .T.)
	Return
Endif

oReport := ReportDef(cPerg) //Caixa de dialogo do relatorio
oReport:PrintDialog()//Caixa de dialogo do relatorio
Return


Static Function ReportDef(cPerg)

Local oReport
Local oSection1
Local oBreak
Local aOrdem 	:= {}
Local cTitulo := 'ORCADO X REALIZADO SEM QUEBRAS'

oReport := TReport():New(cPerg,cTitulo,cPerg,{|oReport| PrintReport(oReport)},cTitulo)
oReport:SetLandscape()
oReport:SetTotalinLine(.F.)
oReport:ShowHeader()

oSection1 := TRSection():New(oReport,cTitulo,{"CTT","CT3","CT1"})
oSection1:SetTotalinLine(.F.) 

TRCell():New( oSection1, "EMPRESA",,"Empresa")	// CODIGO VENDEDOR
TRCell():New( oSection1, "CTT_CUSTO")	// CODIGO VENDEDOR
TRCell():New( oSection1, "CTT_DESC01")	// NOME FANTASIA
TRCell():New( oSection1, "CT1_CONTA")	// CLIENTE
TRCell():New( oSection1, "CT1_DESC01")	// LOJA
TRCell():New( oSection1, "TOT_ORC"	,,"Total / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "TOT_REA"	,,"Total / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "TOT_VAR"	,,"Total / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_01"	,,"Jan / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_01"	,,"Jan / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_01"	,,"Jan / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_02"	,,"Fev / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_02"	,,"Fev / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_02"	,,"Fev / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_03"	,,"Mar / Orcado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_03"	,,"Mar / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_03"	,,"Mar / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_04"	,,"Abr / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_04"	,,"Abr / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_04"	,,"Abr / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_05"	,,"Mai / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_05"	,,"Mai / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_05"	,,"Mai / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_06"	,,"Jun / Orcado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_06"	,,"Jun / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_06"	,,"Jun / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_07"	,,"Jul / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_07"	,,"Jul / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_07"	,,"Jul / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_08"	,,"Ago / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_08"	,,"Ago / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_08"	,,"Ago / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_09"	,,"Set / Orcado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_09"	,,"Set / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_09"	,,"Set / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_10"	,,"Out / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_10"	,,"Out / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_10"	,,"Out / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_11"	,,"Nov / Orcaodo","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_11"	,,"Nov / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_11"	,,"Nov / % Var","@E 999.99",6,,)	// TOTAL
TRCell():New( oSection1, "ORC_12"	,,"Dez / Orcado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "REA_12"	,,"Dez / Realizado","@E 99,999,999.99",15,,)	// TOTAL
TRCell():New( oSection1, "VAR_12"	,,"Dez / % Var","@E 999.99",6,,)	// TOTAL
/*
oBreak  := TRBreak():New(oSection1, oSection1:Cell("F2_VEND1"), "TOTAL DO VENDEDOR" )// Quebra por grupo de produto.
TRFunction():New(oSection1:Cell("QTD")	,NIL,"SUM",oBreak,,,,.F.,.T.,.F.)
TRFunction():New(oSection1:Cell("TOTAL")	,NIL,"SUM",oBreak,,,,.F.,.T.,.F.)
*/
Return oReport

/*Inicia Logica Print Report */

Static Function PrintReport(oReport)

Local oSection1 	:= oReport:Section(1) 
Local oBreak
Local cGrupos		:= 	Implode(UsrRetGrp(cUserName),"/")


cQuery := " SELECT * FROM 
cQuery += " (SELECT CT3_FILIAL,CTT_CUSTO,CTT_DESC01,CT1_CONTA,CT1_DESC01,'A'+CT3_TPSALD+SUBSTRING(CT3_DATA,5,2) MES,CASE WHEN SUM(CT3_DEBITO) > SUM(CT3_CREDIT) THEN SUM(CT3_DEBITO - CT3_CREDIT) ELSE SUM((CT3_CREDIT - CT3_DEBITO)*-1) END  VALOR
cQuery += "   FROM "+RetSqlName("CT1")+" CT1"
cQuery += "  LEFT JOIN "+RetSqlName("CT3")+" CT3 ON CT1_CONTA=CT3_CONTA AND CT3.D_E_L_E_T_=' '  AND SUBSTRING(CT3_DATA,1,4) = '"+MV_PAR05+"' AND CT3_TPSALD IN ('0','1')"+CRLF
cQuery += " 		AND CT3_CUSTO BETWEEN '"+MV_PAR01+"' AND '"+MV_PAR02+"'"+CRLF
cQuery += " 		AND CT3_FILIAL BETWEEN '"+SUBSTR(MV_PAR06,1,4)+"' AND '"+SUBSTR(MV_PAR07,1,4)+"'"+CRLF
cQuery += "   INNER JOIN "+RetSqlName("CTT")+" CTT ON CTT_FILIAL='      ' AND CTT_CUSTO=CT3_CUSTO AND CTT.D_E_L_E_T_=' '
cQuery += "   WHERE CT1_FILIAL='      ' AND CT1.D_E_L_E_T_=' ' 
cQuery += " AND CT1_CONTA BETWEEN '"+MV_PAR03+"' AND '"+MV_PAR04+"'"+CRLF
IF !UPPER(cUserName) $ UPPER(GETNEWPAR("MV_YUSRPCO","")) //!("000000" $ cGrupos .Or. "000011" $ cGrupos .Or. "000024" $ cGrupos)
	cQuery += " AND CTT_YGPRES IN (SELECT AL_COD FROM "+RetSqlName("SAL")+" AL WHERE AL_FILIAL='"+xFilial("SAL")+"' AND AL_USER='"+RetCodUsr()+"' AND AL.D_E_L_E_T_=' ')"+CRLF
EndIF
cQuery += " GROUP BY CT3_FILIAL,CTT_CUSTO,CTT_DESC01,CT1_CONTA,CT1_DESC01,CT3_TPSALD,SUBSTRING(CT3_DATA,5,2)) AS TABELA
cQuery += " PIVOT
cQuery += "  (SUM(VALOR)
cQuery += "  FOR MES IN ( [A001], [A002], [A003], [A004],[A005],[A006],[A007],[A008],[A009],[A010],[A011],[A012],[A101], [A102], [A103], [A104],[A105],[A106],[A107],[A108],[A109],[A110],[A111],[A112])) AS PIVOTTABLE

TcQuery cQuery New Alias T01

DbSelectArea("T01")

Count to nReg

T01->(dbGoTop())

oReport:SetMeter(nReg)
cCC	:= ""
cConta	:= ""
cFil := ""
oSection1:Init()

DO While !oReport:Cancel() .And. !T01->(EOF())
	
	oReport:IncMeter()
		DbSelectArea("SM0")
		DbSeek(cEmpAnt+SUBSTR(T01->CT3_FILIAL,1,4))
		
		oSection1:Cell("EMPRESA"  	):SetValue( SM0->M0_NOMECOM 	)
	oSection1:Cell("CTT_CUSTO" 	):SetValue( T01->CTT_CUSTO 	)
	oSection1:Cell("CTT_DESC01" ):SetValue( T01->CTT_DESC01)
	oSection1:Cell("CT1_CONTA" 	):SetValue( T01->CT1_CONTA 	)
	oSection1:Cell("CT1_DESC01" ):SetValue( T01->CT1_DESC01 )
	
	nTotalOrc	:= 0
	nTotalRea	:= 0
	
	For nCont := 1 To 12
		oSection1:Cell("ORC_"+STRZERO(nCont,2) 	):SetValue( T01->&("A0"+STRZERO(nCont,2)) 	)
		oSection1:Cell("REA_"+STRZERO(nCont,2) 	):SetValue( T01->&("A1"+STRZERO(nCont,2))	)
		oSection1:Cell("VAR_"+STRZERO(nCont,2) 	):SetValue( CalcVar(T01->&("A0"+STRZERO(nCont,2)) , T01->&("A1"+STRZERO(nCont,2)))	)
		
		nTotalOrc	+= T01->&("A0"+STRZERO(nCont,2))
		nTotalRea	+= T01->&("A1"+STRZERO(nCont,2))
	Next
	oSection1:Cell("TOT_ORC" 	):SetValue( nTotalOrc )
	oSection1:Cell("TOT_REA"    ):SetValue( nTotalRea 	)
	oSection1:Cell("TOT_VAR" 	):SetValue(CalcVar( nTotalOrc , nTotalRea 	))
	oSection1:PrintLine()
	
	T01->(dbSkip())
EndDo
//oSection1:PrintLine()
oSection1:Finish()
T01->(dbCloseArea())
Return

Static Function CalcVar(nOrc,nRea)
Local nRet := 0

nRet := (nRea / nOrc) * 100

Return nRet
//-------------------------------------------------------------------------------------------------------------

Static Function Implode(array, char)
Local nCont
Local cRet	:= ""
For nCont := 1 To Len(array)
	cRet += array[ncont] + char
Next
Return(cRet)

Static Function AjustaSX1(cPerg)
PutSx1(cPerg, "01","Do C.Custo"			,""		,""		,"mv_ch01","C",TamSx3("CTT_CUSTO")[1],0,0,"G",""	,"CTT"	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
PutSx1(cPerg, "02","Ate C.Custo"		,""		,""		,"mv_ch02","C",TamSx3("CTT_CUSTO")[1],0,0,"G",""	,"CTT"	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
PutSx1(cPerg, "03","Da Conta"			,""		,""		,"mv_ch03","C",TamSx3("CT1_CONTA")[1],0,0,"G",""	,"CT1"	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
PutSx1(cPerg, "04","Ate Conta"			,""		,""		,"mv_ch04","C",TamSx3("CT1_CONTA")[1],0,0,"G",""	,"CT1"	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
PutSx1(cPerg, "05","Ano"				,""		,""		,"mv_ch05","C",04,0,0,"G",""	,""		,"","","mv_par05"," ","","","","","","","","","","","","","","","")
PutSx1(cPerg, "06","Da Filial"			,""		,""		,"mv_ch06","C",TamSx3("CTT_FILIAL")[1],0,0,"G",""	,"SM0"	,"","","mv_par06"," ","","","","","","","","","","","","","","","")
PutSx1(cPerg, "07","Ate Filial"			,""		,""		,"mv_ch07","C",TamSx3("CTT_FILIAL")[1],0,0,"G",""	,"SM0"	,"","","mv_par07"," ","","","","","","","","","","","","","","","")
Return
