#Include 'Protheus.ch'

#Include 'Protheus.ch'
/*/{Protheus.doc} MT103IPC
Importação da Solicitação de Compras para o Pedido de Compras
Uso para gravação dos campos customizados referente a conta orçamentária e classificação orçamentária.

@version 1.0
@see http://tdn.totvs.com/display/public/mp/MT120ISC+-++Manipula+o+acols+do+pedido+de+compras

/*/
User Function MT120ISC()

	RCOMA002()

Return

Static Function RCOMA002()

	Local nYCO	    := aScan(aHeader,{|x| Upper(AllTrim(x[2]))=="C7_YCO"})
	Local nYClaOrc  := aScan(aHeader,{|x| Upper(AllTrim(x[2]))=="C7_YCLAORC"})
	Local nPosPed 	:= aScan(aHeader,{|x| Upper(AllTrim(x[2]))=="C7_NUMSC"})
	Local nPosItem 	:= aScan(aHeader,{|x| Upper(AllTrim(x[2]))=="C7_ITEMSC"})
	
	Local nPosPrc    := aScan(aHeader,{|x| AllTrim(x[2])=="C7_PRECO"})
	Local nPosTot	 := aScan(aHeader,{|x| AllTrim(x[2])=="C7_TOTAL"})
	Local nPosAdi	 := aScan(aHeader,{|x| AllTrim(x[2])=="C7_YADIANT"})
	
	Local nPedido 	:= ""
	Local nCont 	:= 1
	
	nPedido :=  aCols[nCont,nPosPed]
	
	For nCont := 1 To Len(aCols)
			
		cItem := aCols[nCont,nPosItem]			
		
		aCols[nCont,nYCO] := Posicione("SC1",1,xFilial("SC1")+nPedido+cItem,"C1_YCO")
		aCols[nCont,nYClaOrc] := Posicione("SC1",1,xFilial("SC1")+nPedido+cItem,"C1_YCLAORC")
		
		aCols[n][nPosPrc]   := SC1->C1_YVUNIT
		aCols[n][nPosTot]	:= SC1->C1_YVUNIT * SC1->C1_QUANT
		aCols[n][nPosAdi]	:= SC1->C1_YADIANT

	next nCont
	
Return

