#include "protheus.ch"

//-------------------------------------------------------------------
/*/{Protheus.doc} PCOCTAE2

Gatilho do campo E2_NATUREZ para E2_YCO
 
Usado no validador do campo. 
@author Sam Barros
@since 03/08/2016
@version 1.0
@return bRet, Valida��o de exist�ncia de registro
@example
U_PCOCTAE2()
/*/
//-------------------------------------------------------------------
User Function PCOCTAE2()
	Local cRet := ""

	If AllTrim(FunName()) == "FINA050"
		dbSelectArea("SED")
		dbSetOrder(1)
		dbSeek(xFilial()+M->E2_NATUREZ)

		If SUBS(M->E2_CCD,1,1) $ "1_5"
			cRet := SED->ED_CONTA
		Else
			If SUBS(M->E2_CCD,1,1) $ "2"
				cRet := SED->ED_YCTCUST
			Else
				cRet := SED->ED_YDESVEN
			EndIf
		EndIf
	EndIf
Return cRet 