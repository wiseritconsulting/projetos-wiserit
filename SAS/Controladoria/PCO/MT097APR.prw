#include 'protheus.ch'
#include 'parmtype.ch'

/*/
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪哪勘�
北矲un鏰o � MT097APR � Autor � Plinio Nogueira TOTVS/CE � Data � 19.04.2016 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪哪幢�
北矰escri鏰o � Ponto de Entrada para gravar no PCO a libera玢o do pedido de 潮�
北� compra com rateio.                                                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
/*/

user function MT097APR 
       ItemRatCC()
       DtAprov()
Return()

Static function ItemRatCC()

Local _aArea := GetArea()
Local _aAreaSCH	:= SCH->(GetArea())
Local _lRet := IIF(SC7->C7_CONAPRO=="L",.T.,.F.)

dbSelectArea("SCH")
SCH->(dbSetOrder(1))
SCH->(dbGoTop())

If SCH->(dbSeek( xFilial("SCH")+SC7->(C7_NUM+C7_FORNECE+C7_LOJA+C7_ITEM) ) )
   	
   	Begin Transaction
   	
   	PcoIniLan("900002")
	
	While SCH->(!Eof()) .And. SCH->CH_PEDIDO==SC7->C7_NUM .And. SCH->CH_ITEMPD==SC7->C7_ITEM .And. _lRet  
	   	PcoDetLan("900002","01","MT097APR",.F.) // Grava os lancamentos nas contas orcamentarias SIGAPCO    
		SCH->(dbSkip())
	EndDo

	End Transaction                                   

	PcoFinLan("900002") //Finaliza a gravacao dos lancamentos do SIGAPCO
		
EndIf

RestArea(_aArea)
RestArea(_aAreaSCH)

return(_lRet)


Static function DtAprov()   

Local aAreaSC7 := GetArea("SC7") 
	
	Reclock("SC7",.F.)
	dbSelectArea("SC7")
	
    SC7->C7_YDTAPR := ddatabase
    
	MsUnlock()

RestArea( aAreaSC7 )

Return Nil
