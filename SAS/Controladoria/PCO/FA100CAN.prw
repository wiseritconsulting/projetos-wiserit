#Include 'Protheus.ch'

/*/{Protheus.doc} FA100CAN
Cancela movimento bancário - O ponto de entrada FA100CAN sera utilizado apos a exclusao da movimentação bancária e antes da contabilização.
@type function
@author Saulo Gomes Martins
@since 23/09/2015
@version P11
@see http://tdn.totvs.com/pages/releaseview.action?pageId=6071160
/*/
User Function FA100CAN()
	Local aArea 	:= GetArea()
	Local cYCO		:= ""
	Local cCCD		:= ""
	Local cCLVLDB   := ""
	Local cINTPCO   := ""
	Local nRecnoSE5
	nRecnoSE5 := SE5->(Recno())

	If Type("cYFA100Reg")=="N"	//Variavel vem do PE FA100CA2
		SE5->(dbGoTo(cYFA100Reg))
		cYCO := SE5->E5_YCO
		cCCD := SE5->E5_CCD
		cCLVLDB := SE5->E5_CLVLDB
		cINTPCO := SE5->E5_YINTPCO
		
		SE5->(dbGoTo(nRecnoSE5))
		RecLock("SE5",.F.)
		SE5->E5_YCO		:= cYCO
		SE5->E5_CCD		:= cCCD
		SE5->E5_CLVLDB  := cCLVLDB
		SE5->E5_YINTPCO := cINTPCO
		MsUnLock()
		cYFA100Reg		:= nil
	Endif

	RestArea(aArea)
Return