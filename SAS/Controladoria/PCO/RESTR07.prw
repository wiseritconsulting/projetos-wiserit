#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"      
#INCLUDE "RWMAKE.CH"

//=========================================================================//
// AUTOR : PLINIO NOGUEIRA
// DATA  : 13/04/16
// FONTE UTILIZADO COMO INICIALIZADOR PADR�O, PARA QUE O CAMPO DA SD3 (D3_YCO)  
// RECEBA A INFORMA��O DO CAMPO CT1_YCO
//=========================================================================//

user function RESTR07()

//Local cRet := ""
Local cCC  := ""
Local cSb1 := ""
//Local cYco := ""
Local sArea 

sArea := getarea() 

IF FUNNAME() == "MATA185"
	
	
	cCC := SUBSTR(SCP->CP_CC,1,1)
	
	IF cCC $ "1_5"
		cSb1 := POSICIONE("SB1",1,XFILIAL("SB1")+ACOLS[len(aCols)][1],"B1_YCTDESP")
	ELSEIF cCC == '2'
		cSb1 := POSICIONE("SB1",1,XFILIAL("SB1")+ACOLS[len(aCols)][1],"B1_YCTCUST") 
	ELSEIF cCC == '3'
		cSb1 := POSICIONE("SB1",1,XFILIAL("SB1")+ACOLS[len(aCols)][1],"B1_YDESVEN") 
	ENDIF
		
ENDIF
//DbCloseArea()
RestArea(sArea)
	
return cSb1