#Include 'Protheus.ch'

/*/{Protheus.doc} FA100CA2
Validar Cancela movimenta��o banc�ria
@type function
@author Saulo Gomes Martins
@since 23/09/2015
@version P11
@return lRet, .T./.F. - Se o retorno for .F. a movimenta��o n�o ser� exclu�da.
@see http://tdn.totvs.com/pages/releaseview.action?pageId=6071159
/*/
User Function FA100CA2()
	Local nOpc			:= PARAMIXB[1]
	Local lRet			:= .T.
	Public cYFA100Reg	:= SE5->(Recno())	//Grava em variavel Publica o registro que est� sendo estornado! usado no PE FA100CAN
Return lRet

