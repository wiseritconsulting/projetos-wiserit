#Include 'Protheus.ch'
#Include 'Topconn.ch'

/*/{Protheus.doc} MT106PRE
Grava��o da Conta Or�amentaria na gera��o da Pr�-Requisi��o
@author Diogo
@since 26/06/2015
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/User Function MT106PRE()

	Local aaRea:= getArea()
	
	
	
	For i := 1 To Len(ParamIxb)   
	
		cCQFilial:= PARAMIXB[i,1]    
		cCQNum   := PARAMIXB[i,2]  
	
		cQuery := " SELECT  CP_YCO , CP_PRODUTO , CQ_FILIAL , CQ_NUM"
		cQuery += " FROM "+RetSqlName("SCP")+" SCP , "+RetSqlName("SCQ")+" SCQ " 
		cQuery += " WHERE CQ_FILIAL= '"+cCQFilial+"'   AND " 
		cQuery += "       CQ_NUM   = '"+cCQNum+"'   AND " 
		cQuery += "       CQ_FILIAL= CP_FILIAL AND CQ_NUM = CP_NUM AND CQ_PRODUTO = CP_PRODUTO "
		cQuery += "       AND SCP.D_E_L_E_T_='' AND SCQ.D_E_L_E_T_='' "
		
		TcQuery cQuery New Alias T01PRE
	
		aSCP:= {}
		while !T01PRE->(EOF())
			aadd(aSCP,{T01PRE->CP_YCO , T01PRE->CP_PRODUTO, T01PRE->CQ_FILIAL , T01PRE->CQ_NUM})
			T01PRE->(DBSKIP())	
		enddo   
	
		FOR nAx := 1 to len(aSCP)
			cQuery2 := "UPDATE "+RetSqlName("SCQ")+" SET CQ_YCO ='"+aSCP[nAx][1]+"' "
			cQuery2 += "WHERE CQ_FILIAL= '"+aSCP[nAx][3]+"'     AND " 
			cQuery2 += "      CQ_NUM   = '"+aSCP[nAx][4]+"'          AND " 
			cQuery2 += "      CQ_PRODUTO   = '"+aSCP[nAx][2]+"'    AND "
			cQuery2 += "      D_E_L_E_T_=''
		TCSQLExec(cQuery2)
	
		next
	
	T01PRE->(DbCloseArea())
	
	Next
	
	RestArea(aaRea)
	
Return