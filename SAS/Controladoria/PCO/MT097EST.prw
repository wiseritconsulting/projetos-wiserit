#include 'protheus.ch'
#include 'parmtype.ch'

/*/
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪哪勘�
北矲un鏰o � MT097EST � Autor � Plinio Nogueira TOTVS/CE � Data � 19.04.2016 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪哪幢�
北矰escri鏰o � Ponto de Entrada para estornar do PCO o cancelmento da       潮�
北� libera玢o do pedido de compra com rateio.                               潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
/*/

user function MT097EST

Local _aArea := GetArea()
Local _aAreaSCH	:= SCH->(GetArea())
Local _aAreaSC7	:= SC7->(GetArea())

dbSelectArea("SCH")
SCH->(dbSetOrder(1))
SCH->(dbGoTop())

If SCH->(dbSeek( xFilial("SCH")+SUBSTR(SCR->CR_NUM,1,6) ) )
	
	lLibOk := A097Lock(Substr(SCR->CR_NUM,1,Len(SCH->CH_PEDIDO)),SCR->CR_TIPO) //Cancelamento for para Pedido com rateio
	
	If lLibOk
		
		PcoIniLan("900002")
		
		Begin Transaction
		
		While SCH->(!Eof()) .And. SCH->CH_PEDIDO==SUBSTR(SCR->CR_NUM,1,6) 
		
			dbSelectArea("SC7")
			SC7->(dbSetOrder(1))
			SC7->(dbGoTop())
			
			If SC7->(dbSeek( xFilial("SC7")+SCH->(CH_PEDIDO+CH_ITEMPD) ) )
						
			PcoDetLan("900002","01","MT097EST",.t.) // Excluir/Estornar registro do SIGAPCO
			
			EndIf
			
			SCH->(dbSkip())
		
		EndDo
		
		End Transaction
		
		PcoFinLan("900002") //Finaliza a gravacao da Exclus鉶/Cancelamento do SIGAPCO
		
	EndIf
	
EndIf

RestArea(_aArea)
RestArea(_aAreaSCH)
RestArea(_aAreaSC7)

return
