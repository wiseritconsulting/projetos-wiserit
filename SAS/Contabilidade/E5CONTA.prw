#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �E5Conta      �Autor  �Mconsult    � Data �  22/02/16        ���
�������������������������������������������������������������������������͹��
���Desc.     � Retorna a Conta da Natureza - Gatilho E5_CCD               ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function E5Conta()
	
	Local cRet := ""
	
	If  Alltrim(POSICIONE("CTT",1,xFilial("CTT")+M->E5_CCD,"CTT_CRGNV1"))=="3"
		cRet := POSICIONE("SED",1,xFilial("SED")+M->E5_NATUREZ,"SED->ED_CONTA")       
	ElseIf Alltrim(POSICIONE("CTT",1,xFilial("CTT")+M->E5_CCD,"CTT_CRGNV1"))=="9"
		cRet := POSICIONE("SED",1,xFilial("SED")+M->E5_NATUREZ,"SED->ED_CONTA")       
	ElseIf Alltrim(POSICIONE("CTT",1,xFilial("CTT")+M->E5_CCD,"CTT_CRGNV1"))=="4"
		cRet := POSICIONE("SED",1,xFilial("SED")+M->E5_NATUREZ,"SED->ED_YCTCUST")       
	ElseIf Alltrim(POSICIONE("CTT",1,xFilial("CTT")+M->E5_CCD,"CTT_CRGNV1"))=="5"
		cRet := POSICIONE("SED",1,xFilial("SED")+M->E5_NATUREZ,"SED->ED_YDESVEN")       
	Else
		cRet := ""
	EndIf

Return(cRet)