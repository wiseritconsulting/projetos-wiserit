#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �E2Conta      �Autor  �Mconsult    � Data �  24/02/16        ���
�������������������������������������������������������������������������͹��
���Desc.     � Retorna a Conta da Natureza - Gatilho E2_CCD               ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function E2Conta()
	
	Local cRet   := ""
	Local cCusto 
	Local cNat 

	IF !INCLUI
		 cCusto := SE2->E2_CCD
		 cNat   := SE2->E2_NATUREZ
	ELSE
		 cCusto := M->E2_CCD
		 cNat   := M->E2_NATUREZ
	ENDIF
	
	If  Alltrim(POSICIONE("CTT",1,xFilial("CTT")+cCusto,"CTT_CRGNV1"))=="3"
		cRet := POSICIONE("SED",1,xFilial("SED")+cNat,"SED->ED_CONTA")       
	ElseIf Alltrim(POSICIONE("CTT",1,xFilial("CTT")+cCusto,"CTT_CRGNV1"))=="9"
		cRet := POSICIONE("SED",1,xFilial("SED")+cNat,"SED->ED_CONTA")       
	ElseIf Alltrim(POSICIONE("CTT",1,xFilial("CTT")+cCusto,"CTT_CRGNV1"))=="4"
		cRet := POSICIONE("SED",1,xFilial("SED")+cNat,"SED->ED_YCTCUST")       
	ElseIf Alltrim(POSICIONE("CTT",1,xFilial("CTT")+cCusto,"CTT_CRGNV1"))=="5"
		cRet := POSICIONE("SED",1,xFilial("SED")+cNat,"SED->ED_YDESVEN")       
	Else
		cRet := ""
	EndIf

Return(cRet)