// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : SASW006.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor             | Descricao
// -----------+-------------------+---------------------------------------------------------
// 04/10/2016 | roger             | Gerado com aux�lio do Assistente de C�digo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"
#INCLUDE "Topconn.ch"
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} SASW006
Manuten��o de dados em SRA-Funcionarios.

@author    roger
@version   11.3.2.201607211753
@since     04/10/2016
/*/
//------------------------------------------------------------------------------------------
user function SASW006(cFil,cNumAE,cGrupAp,cFilC,cNumCon)
	//-- vari�veis -------------------------------------------------------------------------

	Local cDataHora 	:= DTOC(date())+ " - " + TIME()
	Local cQuery 		:= ""
	Local cQuery2 		:= ""
	Local nTotal		:= 0

	cQuery := " SELECT AL_USER FROM "+RetSqlName("SAL")+" (NOLOCK) "
	cQuery += " WHERE D_E_L_E_T_ = '' "
	cQuery += " AND AL_COD = '"+cGrupAp+"' "
	TcQuery cQuery new Alias T01

	WHILE !T01->(EOF())


		cQuery2 := " SELECT C7_FILIAL,B1_DESC, C7_NUM, C7_FORNECE,A2_NOME,C7_PRODUTO, C7_QUANT,C7_VALFRE,C7_VLDESC, C3_QUANT, C3_QUJE,C7_PRECO, C7_TOTAL,C7_OBS,C7_CC,CTT_DESC01,C7_DESC,E4_DESCRI,C7_FRETE,C7_EMISSAO FROM "+RetSqlName("SC7")+" (NOLOCK) C7 "
		cQuery2	+= " INNER JOIN "+RetSqlName("SA2")+" (NOLOCK) A2 "
		cQuery2 += " ON C7_FORNECE = A2_COD "
		cQuery2 += " AND A2.D_E_L_E_T_ = '' "
		cQuery2 += " INNER JOIN "+RetSqlName("SB1")+" (NOLOCK) B1 "
		cQuery2 += " ON C7_PRODUTO = B1_COD "
		cQuery2 += " AND B1.D_E_L_E_T_ = '' "
		cQuery2 += " INNER JOIN "+RetSqlName("CTT")+" (NOLOCK) CT "
		cQuery2 += " ON CTT_CUSTO = C7_CC "
		cQuery2 += " AND CT.D_E_L_E_T_ = '' "
		cQuery2 += " INNER JOIN "+RetSqlName("SC3")+" (NOLOCK) C3 "
		cQuery2 += " ON C3_FILIAL = '"+cFilC+"' "
		cQuery2 += " AND C3_NUM = '"+cNumCon+"' " 
		cQuery2 += " AND C3_PRODUTO = C7_PRODUTO "
		cQuery2 += " AND C3.D_E_L_E_T_ = '' "
		cQuery2 += " INNER JOIN "+RetSqlName("SE4")+" (NOLOCK) E4 ON C7_COND = E4_CODIGO "
		cQuery2 += " AND E4.D_E_L_E_T_ = '' "
		cQuery2 += " WHERE C7.D_E_L_E_T_ = '' "
		cQuery2 += " AND C7_FILIAL = '"+cFil+"' "
		cQuery2 += " AND C7_NUM = '"+cNumAE+"' "
		TcQuery cQuery2 new Alias T02

		oProcess := TWFProcess():New( "000002", "AUTORIZACAO DE ENTREGA" )
		oProcess :NewTask( "Fluxo de Compras", "\WORKFLOW\AUTENTREGA.HTM" )
		oHtml    := oProcess:oHTML

		oHtml:ValByName( "DATAHORA" 	,	cDataHora 	)


		oHtml:ValByName( "C7_FILIAL"	,	T02->C7_FILIAL  )
		oHtml:ValByName( "C7_NUM" 		,	T02->C7_NUM  	)
		oHtml:ValByName( "C7_PRODUTO" 	,	T02->C7_PRODUTO )
		oHtml:ValByName( "C7_EMISSAO" 	,	DtoC(StoD(T02->C7_EMISSAO)) )
		oHtml:ValByName( "C7_FORNECE"	,	T02->C7_FORNECE	)
		oHtml:ValByName( "A2_NOME"		,	T02->A2_NOME	)
		oHtml:ValByName( "C7_CC"		,	T02->C7_CC		)
		oHtml:ValByName( "CC_DESC"		,	T02->CTT_DESC01	)
		oHtml:ValByName( "DESC"			,TRANSFORM(T02->C7_VLDESC, "@E 999,999,999.99")	)
		oHtml:ValByName( "FRETE"		,TRANSFORM(T02->C7_VALFRE, "@E 999,999,999.99")	)
		oHtml:ValByName( "COND"			,	T02->E4_DESCRI	)
				
		
		WHILE !T02->(EOF())

			aAdd( (oHtml:ValByName( "it.B1_DESC"  	)), 		T02->B1_DESC 					)
			aAdd( (oHtml:ValByName( "it.C7_QUANT" 	)), 		T02->C7_QUANT 					)
			aAdd( (oHtml:ValByName( "it.C3_QUANT" 	)), 		T02->C3_QUANT 					)
			aAdd( (oHtml:ValByName( "it.C3_QUJE" 	)), 		T02->C3_QUJE - T02->C7_QUANT 	)
			aAdd( (oHtml:ValByName( "it.C3_SALDO" 	)), 		T02->C3_QUANT - (T02->C3_QUJE - T02->C7_QUANT)  	)
			aAdd( (oHtml:ValByName( "it.C7_PRECO" 	)),TRANSFORM(T02->C7_PRECO, "@E 999,999,999.99") 					)
			aAdd( (oHtml:ValByName( "it.C7_TOTAL" 	)),TRANSFORM(T02->C7_TOTAL, "@E 999,999,999.99") 					)
			aAdd( (oHtml:ValByName( "it.C7_OBS" 	)), 		T02->C7_OBS 					)
			aAdd( (oHtml:ValByName( "it.C7_FILIAL"  )), 		T02->C7_FILIAL 					)
			aAdd( (oHtml:ValByName( "it.C7_NUM" 	)), 		T02->C7_NUM 					)
			aAdd( (oHtml:ValByName( "it.C7_PRODUTO" )), 		T02->C7_PRODUTO 				)
			
			nTotal += T02->C7_TOTAL
			T02->(DBSKIP())
		ENDDO
		oHtml:ValByName( "TOTAL" 		, TRANSFORM(nTotal, "@E 999,999,999.99") 		)
		nTotal := 0
		T02->(DbCloseArea())
		
		
		
		PswOrder(1)
		PswSeek(T01->AL_USER,.T.)	
		oProcess:cSubject 	:= "CONTRATO DE PARCERIA" 
		oProcess:cTo 		:= PswRet(1)[1][14]
		oProcess:cFromAddr	:= GETMV("SA_MAILC")
		oProcess:cFromName	:= "SAS � AUTORIZACAO DE ENTREGA"
		oProcess:Start()

		T01->(DBSKIP())
		//-- encerramento ----------------------------------------------------------------------
	ENDDO
	AVISO("Autorizacao de Entrega "+cNumAE,"Autorizacao de entrega gerada: "+cNumAE, {"Ok"}, 1)
	T01->(DbCloseArea())
return
//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 04/10/2016 as 09:56:42
//-- fim de arquivo--------------------------------------------------------------------------

