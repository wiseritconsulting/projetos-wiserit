// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : MT097END.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor             | Descricao Envia email para o proximo nivel caso o documento seja aprovado pelo sistema.
// -----------+-------------------+---------------------------------------------------------
// 21/10/2016 | rogeriojacome     | Gerado com aux�lio do Assistente de C�digo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"
#include "topconn.ch"
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MT097END
Processa a tabela SRA-Funcionarios.

@author    rogeriojacome
@version   11.3.1.201605301307
@since     21/10/2016
/*/
//------------------------------------------------------------------------------------------
user function MT097END()
	//-- vari�veis -------------------------------------------------------------------------
	Local cQuery 	:= ''
	Local cQuery2	:= ''
	Local cCod		:= ''
	Local cCC		:= ''
	Local nTotal	:= 0
	Local nTotFre	:= 0
	Local nDesc		:= 0
	Local aDados	:= {}
	Local aItens	:= {}
	Local cNum		:= PARAMIXB[1] //Recebe o numero do documento
	Local cFilDoc	:= PARAMIXB[4]
	Local cTipo 	:= PARAMIXB[2] //Recebe o tipo de documento
	//trabalho/apoio

	//-- procedimentos ---------------------------------------------------------------------
	IF cTipo == "CP"
		cQuery := " SELECT C3_FORNECE, A2_NOME, C3_NUM, C3_EMISSAO, C3_FILIAL,C3_CC,CTT_DESC01,C3_QUANT,C3_TOTAL,C3_PRECO,C3_VALFRE,B1_DESC,C3_OBS,C3_PRODUTO,C3_COND,E4_DESCRI FROM "+RetSqlName("SC3")+" C3 (NOLOCK)"
		cQuery += " INNER JOIN "+RetSqlName("SA2")+" A2 (NOLOCK) "
		cQuery += " ON A2_COD = C3_FORNECE "
		cQuery += " AND A2.D_E_L_E_T_ = '' "
		cQuery += " INNER JOIN "+RetSqlName("SB1")+" B1 (NOLOCK) "
		cQuery += " ON B1_COD = C3_PRODUTO "
		cQuery += " AND B1.D_E_L_E_T_ = '' "
		cQuery += " INNER JOIN "+RetSqlName("SE4")+" E4 (NOLOCK) "
		cQuery += " ON E4_CODIGO = C3_COND "
		cQuery += " AND E4.D_E_L_E_T_ = '' "
		cQuery += " INNER JOIN "+RetSqlName("CTT")+" CT (NOLOCK) "
		cQuery += " ON CTT_CUSTO = C3_CC "
		cQuery += " AND CT.D_E_L_E_T_ = '' "
		cQuery += " WHERE C3.D_E_L_E_T_ = '' "
		cQuery += " AND C3_FILIAL = '"+cFilDoc+"' "
		cQuery += " AND C3_NUM = '"+cNum+"'"
		TcQuery cQuery New Alias T01

		If(!T01->(EOF()))
			cCond 	:= T01->C3_COND +" - "+T01->E4_DESCRI

			aadd(aDados, T01->C3_FORNECE)
			aadd(aDados, T01->A2_NOME)
			aadd(aDados, T01->C3_NUM)
			aadd(aDados, T01->C3_EMISSAO)
			aadd(aDados, T01->C3_FILIAL)
			cCC := alltrim(T01->C3_CC) + " - " + alltrim(T01->CTT_DESC01)
			While(!T01->(EOF()))
				nTotal 	+= T01->C3_TOTAL
				nTotFre += T01->C3_VALFRE
				nDesc 	+= 0

				aadd(aItens, {T01->B1_DESC, T01->C3_QUANT, T01->C3_PRECO, T01->C3_TOTAL, T01->C3_OBS, T01->C3_PRODUTO})

				T01->(DbSkip())
			EndDo
			aadd(aDados, aItens	)
			aadd(aDados, nTotal	)
			aadd(aDados, nTotFre)
			aadd(aDados, nDesc	)
			aadd(aDados, cCond	)
			aadd(aDados, ""		)
			aadd(aDados, ""		)
			aadd(aDados, cCC )
			aadd(aDados, "" )
			
			//aadd(aDados, nAdiant)
		EndIf

		T01->(DbCloseArea())
		//Considerar adi��o do tipo na condi��o da consulta abaixo
		cQuery2 := " SELECT * FROM "+RetSqlName("SCR")+" (NOLOCK) "
		cQuery2 += " WHERE CR_FILIAL = '"+cFilDoc+"' AND CR_NUM = '"+cNum+"' AND CR_STATUS = '02' AND CR_TIPO = '"+cTipo+"' AND D_E_L_E_T_ = '' "
		TCQuery cQuery2 New Alias T02
		
		IF !T02->(EOF())
		T02->(DbGoTop())
		While(!T02->(EOF()))
			aDados[11] := T02->CR_USER
			
			U_SASP123(UsrRetMail(T02->CR_USER),"\WORKFLOW\ABERTURAAEAPROV.HTM",aDados,"Contrato de Parceria - Aprova��o")
	
			T02->(DbSkip())
		EndDo	
		ELSE
		
		U_SASP123(SuperGetMv( "SA_MAILCOM",,, cFilDoc ),"\WORKFLOW\ABERTURACP.HTM",aDados, "Contrato de Parceria Liberado")
		ENDIF


		T02->(DbCloseArea())
		
	ELSEIF cTipo == "PC"
		cQuery := " SELECT C7_FORNECE, A2_NOME, C7_NUM, C7_EMISSAO, C7_FILIAL,C7_CC,CTT_DESC01,C7_QUANT,C7_TOTAL,C7_PRECO,C7_VALFRE,B1_DESC,C7_OBS,C7_PRODUTO,C7_COND,E4_DESCRI,C7_NUMSC FROM "+RetSqlName("SC7")+" C7 (NOLOCK)"
		cQuery += " INNER JOIN "+RetSqlName("SA2")+" A2 (NOLOCK) "
		cQuery += " ON A2_COD = C7_FORNECE "
		cQuery += " AND A2.D_E_L_E_T_ = '' "
		cQuery += " INNER JOIN "+RetSqlName("SB1")+" B1 (NOLOCK) "
		cQuery += " ON B1_COD = C7_PRODUTO "
		cQuery += " AND B1.D_E_L_E_T_ = '' "
		cQuery += " INNER JOIN "+RetSqlName("SE4")+" E4 (NOLOCK) "
		cQuery += " ON E4_CODIGO = C7_COND "
		cQuery += " AND E4.D_E_L_E_T_ = '' "
		cQuery += " INNER JOIN "+RetSqlName("CTT")+" CT (NOLOCK) "
		cQuery += " ON CTT_CUSTO = C7_CC "
		cQuery += " AND CT.D_E_L_E_T_ = '' "
		cQuery += " WHERE C7.D_E_L_E_T_ = '' "
		cQuery += " AND C7_FILIAL = '"+cFilDoc+"' "
		cQuery += " AND C7_NUM = '"+cNum+"'"
		TcQuery cQuery New Alias T01
		
		IF !T01->(EOF())
		T01->(dbGoTop())
		cQuery := " Select TOP 1 C1_YADIANT, C1_USER From " + RetSqlName("SC1")
		cQuery += " Where C1_FILIAL = '"+xFilial("SC1")+"' AND C1_NUM = '"+T01->C7_NUMSC+"' AND D_E_L_E_T_ = ''"
		TcQuery cQuery New Alias T02
		IF !T02->(EOF())
		nAdiant := T02->C1_YADIANT
		ENDIF
		T02->(DbCloseArea())
		ENDIF

		If(!T01->(EOF()))
			cCond 	:= T01->C7_COND +" - "+T01->E4_DESCRI

			aadd(aDados, T01->C7_FORNECE)
			aadd(aDados, T01->A2_NOME)
			aadd(aDados, T01->C7_NUM)
			aadd(aDados, T01->C7_EMISSAO)
			aadd(aDados, T01->C7_FILIAL)
			cCC := alltrim(T01->C7_CC) + " - " + alltrim(T01->CTT_DESC01)
			While(!T01->(EOF()))
				nTotal 	+= T01->C7_TOTAL
				nTotFre += T01->C7_VALFRE
				nDesc 	+= 0

				aadd(aItens, {T01->B1_DESC, T01->C7_QUANT, T01->C7_PRECO, T01->C7_TOTAL, T01->C7_OBS, T01->C7_PRODUTO})

				T01->(DbSkip())
			EndDo
			aadd(aDados, aItens	)
			aadd(aDados, nTotal	)
			aadd(aDados, nTotFre)
			aadd(aDados, nDesc	)
			aadd(aDados, cCond	)
			aadd(aDados, ""		)
			aadd(aDados, nAdiant)
			aadd(aDados, cCC )
			
			
			//aadd(aDados, nAdiant)
		EndIf

		T01->(DbCloseArea())
		//Considerar adi��o do tipo na condi��o da consulta abaixo
		cQuery2 := " SELECT * FROM "+RetSqlName("SCR")+" (NOLOCK) "
		cQuery2 += " WHERE CR_FILIAL = '"+cFilDoc+"' AND CR_NUM = '"+cNum+"' AND CR_STATUS = '02' AND CR_TIPO = '"+cTipo+"' AND D_E_L_E_T_ = '' "
		TCQuery cQuery2 New Alias T02
		
		IF !T02->(EOF())
		T02->(DbGoTop())
		While(!T02->(EOF()))
			aDados[11] := T02->CR_USER
			
			U_SASP123(UsrRetMail(T02->CR_USER),"\WORKFLOW\ABERTURAPCAPROV.HTM",aDados,"Pedido de Compra - Aprova��o")			
			
			T02->(DbSkip())
		EndDo	
		ELSE
			U_SASP123(SuperGetMv( "SA_MAILCOM",,, cFilDoc ),"\WORKFLOW\ABERTURAPC.HTM",aDados,"Libera��o de Pedido de Compra")
		ENDIF
		T02->(DbCloseArea())
	ENDIF
		
	//-- encerramento  ---------------------------------------------------------------------

return

//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 21/10/2016 as 10:10:26
//-- fim de arquivo--------------------------------------------------------------------------

