// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : MT110LOK.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor Rog�rio J�come            | Descricao Utilizado para preencher o centro de custo em todos os itens da SC
 // -----------+-------------------+---------------------------------------------------------
// 28/05/2016 | Rogerio           | Gerado com aux�lio do Assistente de C�digo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MT110LOK

@author    Rogerio
@version   11.3.0.201603221954
@since     28/05/2016
/*/
//------------------------------------------------------------------------------------------
user function MT125LOK()

	Local aArea 		:= GetArea() 
	Local nPosCTT 		:= aScan(aHeader,{|x| AllTrim(x[2])=='C3_CC'})//Pega a posi��o do centro de custo no acols
	Local nPosPro 		:= aScan(aHeader,{|x| AllTrim(x[2])=='C3_YDESC'})
	Local nPosCC 		:= aScan(aHeader,{|x| AllTrim(x[2])=='C3_YCTT'})
	Local nPosProduto 	:= aScan(aHeader,{|x| AllTrim(x[2])=='C3_PRODUTO'})
	Local cAprov		:= aScan(aHeader,{|x| AllTrim(x[2])=='C3_APROV'})
	Local lRet			:= .T.

	IF empty(_CT___)
	MsgAlert("Preencha o Centro de Custo", "Aten��o!!!")
	return .F.
	ENDIF
	for x := 1 to len(aCols)
	
	aCols[x][nPosCTT] 	:= _CT___
	aCols[x][nPosCC] 	:= POSICIONE("CTT", 1, xFilial("CTT") + _CT___, "CTT_DESC01")   
	aCols[x][nPosPro] 	:= POSICIONE("SB1", 1, xFilial("SB1") + aCols[x][nPosProduto], "B1_DESC")                                    
	aCols[x][cAprov]	:= POSICIONE("CTT", 1, xFilial("CTT") + _CT___, "CTT_YGPRES") 
	next
	
	RestArea( aArea )
return lRet

//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 28/05/2016 as 11:28:42
//-- fim de arquivo--------------------------------------------------------------------------

