// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : MT125TEL
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Rog�rio J�come            | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 27/05/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#INCLUDE 'Protheus.ch'
#INCLUDE "rwmake.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo

@author    Rog�rio J�come TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     27/05/2016
/*/
//------------------------------------------------------------------------------------------
user function MT125TEL()
	//--< vari�veis >---------------------------------------------------------------------------
	
	Local oNewDialog	:= PARAMIXB[1]
	public _CT___ 		:= IIF(PARAMIXB[4]==3 .or. Empty(SC3->C3_CC),Space(TamSx3("C3_CC")[1]),SC3->C3_CC)
	Static oDlg

	@ 035,240 Say 'Centro de Custo' Pixel Size 40,9 Of oNewDialog
	@ 035,292 MSGET 	oCodMot  	VAR _CT___ 		SIZE 027,010 OF oDlg COLORS 0,16777215 F3 "CTT" PIXEL 
return
//--< fim de arquivo >----------------------------------------------------------------------
