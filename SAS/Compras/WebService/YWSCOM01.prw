#Include 'Protheus.ch'
#INCLUDE "APWEBSRV.CH"

User Function YWSCOM01()
Return


WSSERVICE Conhec_Frete DESCRIPTION "Servi�o de NF-Conhecimento de Frete"
	WSDATA Retorno			AS Retorno_Conhec_frete
	WSDATA cDoc				AS String
	WSDATA cSerie				AS String
	WSDATA cTes				AS String
	WSDATA cCondicao			AS String
	WSDATA cCNPJ				AS String
	WSDATA cCNPJFrete			AS String
	WSDATA cEst				AS String OPTIONAL
	WSDATA cEspecie			AS String
	WSDATA cNatureza			AS String
	WSDATA dEmissao			AS Date
	WSDATA nValor				AS Float
	WSDATA nBaseRet			AS Float
	WSDATA nIcmRet			AS Float
	WSDATA Documentos		AS aDocumentos_Conhec_frete
	WSMETHOD Incluir
ENDWSSERVICE

WSSTRUCT Retorno_Conhec_frete
	WSDATA cMsg		AS String	OPTIONAL
	WSDATA lOk			AS Boolean
ENDWSSTRUCT

WSSTRUCT aDocumentos_Conhec_frete
	WSDATA aDocumentos	AS Array Of aDocumentos_Array_Conhec_frete
ENDWSSTRUCT

WSSTRUCT aDocumentos_Array_Conhec_frete
	WSDATA cDocumento		AS String
	WSDATA cSerie			AS String
ENDWSSTRUCT

WSMETHOD Incluir WSRECEIVE cDoc,cSerie,cEst,nValor,nBaseRet,nIcmRet,cTes,cCondicao,dEmissao,cEspecie,cNatureza,cCNPJ,cCNPJFrete,Documentos WSSEND Retorno WSSERVICE Conhec_Frete
	Local aCabec := {}
	Local aItens := {}
	Local aLinha := {}
	Local nX     := 0
	Local nY     := 0
	Local cDoc   := ""
	Local cFornece	:= ""
	Local cLoja		:= ""
	Local cFornFrete	:= ""
	Local cLojaFrete	:= ""
	Local nBakMod			:= nModulo
	Local cBakMod			:= cModulo
	Private lMsErroAuto := .F.
	Private lMsHelpAuto := .T.

	::Retorno			:= WsClassNew("Retorno_Conhec_frete")
	::Retorno:cMsg	:= ""
	::Retorno:lOk 	:= .T.

	SF4->(dbSetOrder(1))	//F4_FILIAL+F4_CODIGO
	SE4->(dbSetOrder(1))	//E4_FILIAL+E4_CODIGO
	SA2->(dbSetOrder(3))	//A2_FILIAL+A2_CGC
	SF1->(dbSetOrder(1))	//F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA+F1_TIPO

	If !SF4->(MsSeek(xFilial("SF4")+cTes))
		::Retorno:lOk 	:= .F.
		::Retorno:cMsg	+= "Cadastrar TES: "+cTes+". "
	EndIf

	If !SE4->(MsSeek(xFilial("SE4")+cCondicao))
		::Retorno:lOk 	:= .F.
		::Retorno:cMsg	+= "Cadastrar condicao de pagamento: "+cCondicao+". "
	EndIf

	If !SA2->(MsSeek(xFilial("SA2")+cCNPJ))
		::Retorno:lOk 	:= .F.
		::Retorno:cMsg	+= "Cadastrar fornecedor: "+cCNPJ+". "
	Else
		cFornece	:= SA2->A2_COD
		cLoja		:= SA2->A2_LOJA
	EndIf

	If !SA2->(MsSeek(xFilial("SA2")+cCNPJFrete))
		::Retorno:lOk 	:= .F.
		::Retorno:cMsg	+= "Cadastrar fornecedor: "+cCNPJFrete+". "
	Else
		cFornFrete	:= SA2->A2_COD
		cLojaFrete	:= SA2->A2_LOJA
	EndIf

	For nCont:=1 to Len(::Documentos:aDocumentos)
		If SF1->(DbSeek(xFilial("SF1")+PadR(::Documentos:aDocumentos[nCont]:cDocumento,GetSx3Cache("F1_DOC","X3_TAMANHO"))+PadR(::Documentos:aDocumentos[nCont]:cSerie,GetSx3Cache("F1_SERIE","X3_TAMANHO"))+cFornece+cLoja))
			aadd(aItens,{{"PRIMARYKEY",SubStr(SF1->(&(IndexKey())),FwSizeFilial()+1)}})
		Else
			::Retorno:lOk 	:= .F.
			::Retorno:cMsg	+= "Documento nao localizado para esse fornecedor: "+PadR(::Documentos:aDocumentos[nCont]:cDocumento,GetSx3Cache("F1_DOC","X3_TAMANHO"))+"|"+PadR(::Documentos:aDocumentos[nCont]:cSerie,GetSx3Cache("F1_SERIE","X3_TAMANHO"))+". "
		EndIf
	Next nX

	If !::Retorno:lOk
		Return .T.
	EndIf
	aadd(aCabec,{"",dDataBase-90})      	 	//01//Data Inicial
	aadd(aCabec,{"",dDataBase})         	 	//02//Data Final
	aadd(aCabec,{"",2})                	  	//03//2-Inclusao;1=Exclusao
	aadd(aCabec,{"",cFornece})          	 	//04//Fornecedor do documento de Origem
	aadd(aCabec,{"",cLoja})             	 	//05//Loja de origem
	aadd(aCabec,{"",1})                 		//06//Tipo da nota de origem: 1=Normal;2=Devol/Benef
	aadd(aCabec,{"",1})                 	 	//07//1=Aglutina;2=Nao aglutina
	aadd(aCabec,{"F1_EST",cEst})				//08//Estado
	aadd(aCabec,{"",::nValor})					//09//Valor do conhecimento
	aadd(aCabec,{"F1_FORMUL",1})				//10//Formulario
	aadd(aCabec,{"F1_DOC",::cDoc})				//11//Documento
	aadd(aCabec,{"F1_SERIE",::cSerie})			//12//Serie
	aadd(aCabec,{"F1_FORNECE",cFornFrete})		//13//Fornecedor do frete
	aadd(aCabec,{"F1_LOJA",cLojaFrete})		//14//Loja do Fornecedor do frete
	aadd(aCabec,{"",::cTes})						//15//TES
	aadd(aCabec,{"F1_BASERET",::nBaseRet})		//16//Base de ICM Retido
	aadd(aCabec,{"F1_ICMRET",::nIcmRet})		//17//ICM Retido
	aadd(aCabec,{"F1_COND",::cCondicao})		//18//Condi��o de pagamento
	aadd(aCabec,{"F1_EMISSAO",::dEmissao})		//19//Emissao
	aadd(aCabec,{"F1_ESPECIE",PADR(::cEspecie,GetSx3Cache("F1_ESPECIE","X3_TAMANHO"))})		//20//Especie
	aadd(aCabec,{"NF_NATUREZA",PadR(::cNatureza,GetSx3Cache("ED_CODIGO","X3_TAMANHO"))})	//21//Natureza
	If Len(aItens)>0
		lMsErroAuto    := .F.
		nModulo	:= 2
		cModulo	:= "COM"
		DbSelectArea("SF1")
		MATA116(aCabec,aItens)
		nModulo	:= nBakMod
		cModulo	:= cBakMod
		If !lMsErroAuto
			::Retorno:lOk		:= .T.
			::Retorno:cMsg	+= "Incluido com sucesso!"
		Else
			::Retorno:lOk		:= .F.
			::Retorno:cMsg	+= "Erro na inclusao! "+MostraErro()
		EndIf
	Else
		::Retorno:lOk		:= .F.
		::Retorno:cMsg	+= "Sem Itens"
	EndIf

Return .T.