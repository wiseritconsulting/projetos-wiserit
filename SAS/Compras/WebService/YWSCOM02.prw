#Include 'Protheus.ch'
#INCLUDE "APWEBSRV.CH"

/*/{Protheus.doc} YWSCOM02
WebService de entrada de documentos
@author Saulo Gomes Martins
@since 29/10/2014
@version 1.0
/*/
User Function YWSCOM02()
Return

WSSERVICE Doc_Entrada DESCRIPTION "Documento de entrada"
	WSDATA Dados			AS Doc_Entrada_Dados
	WSDATA Retorno		AS Doc_Entrada_Retorno
	WSMETHOD Incluir
ENDWSSERVICE

WSSTRUCT Doc_Entrada_Retorno
	WSDATA cMsg		AS String	OPTIONAL
	WSDATA lOk			AS Boolean
ENDWSSTRUCT

WSSTRUCT Doc_Entrada_Dados
	WSDATA SF1			AS Array Of Doc_Entrada_SF1
	WSDATA SD1			AS Array Of Doc_Entrada_SD1
ENDWSSTRUCT

WSSTRUCT Doc_Entrada_SF1
	WSDATA cCampo		AS String
	WSDATA cConteudo	AS String
ENDWSSTRUCT

WSSTRUCT Doc_Entrada_SD1
	WSDATA Itens		AS Array Of Doc_Entrada_SD1_Item
ENDWSSTRUCT

WSSTRUCT Doc_Entrada_SD1_Item
	WSDATA cCampo		AS String
	WSDATA cConteudo	AS String
ENDWSSTRUCT

WSMETHOD Incluir;
	WSRECEIVE Dados;
	WSSEND Retorno;
	WSSERVICE Doc_Entrada
	Local aCab		:= {}
	Local aItens	:= {}
	Local nCont,nCont2,nTam
	Local xValor,cCampo
	Private lMsErroAuto := .F.
	::Retorno		:= WsClassNew("Doc_Entrada_Retorno")
	SA2->(DbSetOrder(3))
	For nCont:=1 to Len(::Dados:SF1)
		cCampo	:= ::Dados:SF1[nCont]:cCampo
		If cCampo=="CNPJ"
			If SA2->(DbSeek(xFilial("SA2")+PadR(::Dados:SF1[nCont]:cConteudo,GetSx3Cache("A2_CGC","X3_TAMANHO"))))
				AADD(aCab,{"F1_FORNECE"	,SA2->A2_COD,nil})
				AADD(aCab,{"F1_LOJA"		,SA2->A2_LOJA,nil})
				Loop
			Else
				::Retorno:lOk		:= .F.
				::Retorno:cMsg	:= "CNPJ "+::Dados:SF1[nCont]:cConteudo+" nao encontrado no cadastro de fornecedores!"
				Return .T.
			EndIf
		EndIf
		xValor	:= ConvConteudo(cCampo,::Dados:SF1[nCont]:cConteudo)
		AADD(aCab,{cCampo,xValor,nil})
	Next
	For nCont:=1 to Len(::Dados:SD1)
		AADD(aItens,{})
		nTam	:= Len(aItens)
		For nCont2:=1 to Len(::Dados:SD1[nCont]:Itens)
			cCampo	:= ::Dados:SD1[nCont]:Itens[nCont2]:cCampo
			xValor	:= ConvConteudo(cCampo,::Dados:SD1[nCont]:Itens[nCont2]:cConteudo)
			AADD(aItens[nTam],{cCampo,xValor,nil})
		Next
	Next
	MsExecAuto({|a,b,c| MATA103(a,b,c) },aCab,aItens,3)
	If lMsErroAuto
		::Retorno:lOk		:= .F.
		::Retorno:cMsg	:= Mostraerro()
	Else
		::Retorno:lOk		:= .T.
	EndIf
Return .T.

Static Function ConvConteudo(cCampo,cConteudo)
	Local xRet	:= cConteudo
	Local cTipo	:= GetSx3Cache(UPPER(cCampo),"X3_TIPO")
	If cTipo=="N"
		xRet	:= Val(cConteudo)
	ElseIf cTipo=="D"
		xRet	:= STOD(Replace(cConteudo,"-",""))
	ElseIf ctipo=="L"
		xRet	:= Alltrim(lower(cConteudo))=="true"
	Else
		xRet	:= cConteudo
	Endif
Return xRet