#include "rwmake.ch"

User Function SASP123(cEmail, cArq, aDados, cSub)
	Local cDataHora 	:= DTOC(date())+ " - " + TIME()
	
	oProcess := TWFProcess():New( "000003", "Abertura de PC" )
	oProcess :NewTask( "Fluxo de Compras", cArq )
	oHtml    := oProcess:oHTML
	
	oHtml:ValByName( "DATAHORA" 	,	cDataHora			 	)	
	
	oHtml:ValByName( "C7_FORNECE"	,	aDados[1]  				)
	oHtml:ValByName( "A2_NOME" 		,	aDados[2]  				)
	oHtml:ValByName( "C7_NUM"		,	aDados[3]				)
	oHtml:ValByName( "C7_EMISSAO"	,	DtoC(StoD(aDados[4]))	)
	
	oHtml:ValByName( "C7_FILIAL"	,	aDados[5]	)
	oHtml:ValByName( "CC"			,	aDados[13]	)
	IIF(len(aDados) > 13 ,oHtml:ValByName( "RJ"			,	aDados[14]	),.T.)
	oHtml:ValByName( "FILIAL"		,	aDados[5] + " - " + alltrim(FWFilialName(cEmpAnt,aDados[5],2))	)
	
	
	for x := 1 to len(aDados[6])	
		aAdd( (oHtml:ValByName( "it.B1_DESC" 	)),	aDados[6][x][1] 	)
		aAdd( (oHtml:ValByName( "it.C7_QUANT"	)),	str(aDados[6][x][2]))
		aAdd( (oHtml:ValByName( "it.C7_PRECO"	)),	TRANSFORM(aDados[6][x][3], "@E 999,999,999.99"))
		aAdd( (oHtml:ValByName( "it.C7_TOTAL"	)),	TRANSFORM(aDados[6][x][4], "@E 999,999,999.99"))
		aAdd( (oHtml:ValByName( "it.C7_OBS"	 	)),	aDados[6][x][5] 	)
		
		aAdd( (oHtml:ValByName( "it.C7_PRODUTO" )),	aDados[6][x][6] 	)
		aAdd( (oHtml:ValByName( "it.C7_NUM"	 	)),	aDados[3]		 	)
		aAdd( (oHtml:ValByName( "it.C7_FILIAL"	)),	aDados[5]		 	)
		
	next
	
	oHtml:ValByName( "TOTAL"		,	TRANSFORM(aDados[7], "@E 999,999,999.99")	)
	oHtml:ValByName( "FRETE"		,	TRANSFORM(aDados[8], "@E 999,999,999.99")	)
	oHtml:ValByName( "DESC"			,	TRANSFORM(aDados[9], "@E 999,999,999.99")	)
	oHtml:ValByName( "COND"			,	aDados[10]	)	
	oHtml:ValByName( "CR_USER"		,	aDados[11]	)
	oHtml:ValByName( "ADIANT"		,	TRANSFORM(aDados[12], "@E 999,999,999.99")	)
	
	oProcess:cSubject := cSub 
	oProcess:cTo      := cEmail
	oProcess:cFromAddr:= GETMV("SA_MAILC")//Getmv("SA_MAILCOM")
	oProcess:cFromName:= "SAS � Compras"
	oProcess:Start()
	
Return

