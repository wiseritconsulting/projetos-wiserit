// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : SASP120
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Rog�rio J�come            | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 27/05/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo

@author    Rog�rio J�come - TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     27/05/2016
/*/
//------------------------------------------------------------------------------------------
User Function SASP120(cFil,cSolicitacao,cDataHora,cSolicitante,_cCTT___,aProdutos, cCodFor, cLoja, cCot,cMailEnv)

	Local cCTT 		:= _cCTT___
	Local cCC		:= POSICIONE("CTT",1, xFilial("CTT") + cCTT, "CTT_DESC01" )
	Local codUser	:= '' 
	Local cEmail	:= cMailEnv
	Local cNomeFor  := posicione("SA2",1,xFilial("SA2") + cCodFor + cLoja,"A2_NOME")
	Local cProd		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_DESCRI"})
	Local cQuant	:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_QUANT"})
	Local cArq		:= aProdutos[1][Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YURGEN"})]
	Local cMsg		:= ""
	
	
	IF alltrim(cArq) == 'S'
	cArq := "\WORKFLOW\AVISOSCURG.HTM"
	cMsg := "SAS � Compras Urgente"
	ELSEIF alltrim(cArq) == 'N'
	cArq := "\WORKFLOW\AVISOSC.HTM"
	cMsg := "SAS � Compras"
	ENDIF
	oProcess := TWFProcess():New( "000001", "Solicita��o de Compra" )
	oProcess :NewTask( "Fluxo de Compras", cArq )
	oHtml    := oProcess:oHTML

	oHtml:ValByName( "SC_FILIAL" 		,	cFil     				)
	oHtml:ValByName( "SC_NUMSOL" 		,	cSolicitacao  			)
	oHtml:ValByName( "SC_DATAHORA" 		,	cDataHora				)
	oHtml:ValByName( "SC_SOLICITANTE" 	, 	cSolicitante  			)
	oHtml:ValByName( "SC_CTT" 			,	cCTT + cCC				)
	oHtml:ValByName( "SC_CODFOR" 			,	cCodFor				)
	oHtml:ValByName( "SC_NOMFOR" 			,	cNomeFor			)
	oHtml:ValByName( "SC_COTAR" 			,	cCot		)	
		
	for x := 1 to len(aProdutos)	
		aAdd( (oHtml:ValByName( "it.PROD"  )), 		aProdutos[x][cProd] 	)
		aAdd( (oHtml:ValByName( "it.QUANT" )), 		aProdutos[x][cQuant] 	)		
	next
	
	codUser := POSICIONE("CTT",1, xFilial("CTT") + cCTT, "CTT_YAPVSA")
	oProcess:cSubject := "Inclus�o de Solicita��o de Compra " 
	oProcess:cTo      := cEmail
	oProcess:cFromAddr:= GETMV("SA_MAILC")//Getmv("SA_MAILCOM")
	oProcess:cFromName:= cMsg
	oProcess:Start()
return

