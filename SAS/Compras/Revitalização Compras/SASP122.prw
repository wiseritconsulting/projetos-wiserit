#include "rwmake.ch"

User Function SASP122(cEmail, cArq, aDados)
	Local cDataHora 	:= DTOC(date())+ " - " + TIME()
	
	oProcess := TWFProcess():New( "000002", "Libera��o de PC" )
	oProcess :NewTask( "Fluxo de Compras", cArq )
	oHtml    := oProcess:oHTML
	
	oHtml:ValByName( "DATAHORA" 	,	DtoC(StoD(cDataHora)) 	)
	
	
	oHtml:ValByName( "C7_FILIAL"	,	aDados[1]  	)
	oHtml:ValByName( "C7_NUM" 		,	aDados[2]  	)
	oHtml:ValByName( "C7_CODFOR"	,	aDados[3]	)
	oHtml:ValByName( "C7_NOMFOR"	,	aDados[4]	)
	oHtml:ValByName( "C7_YADIANT"	,	aDados[5] 	)
	oHtml:ValByName( "TOTAL" 		, 	aDados[6] 	)
	
	for x := 1 to len(aDados[7])	
		aAdd( (oHtml:ValByName( "it.PROD"  )), 		aDados[7][x][1] 	)
		aAdd( (oHtml:ValByName( "it.QUANT" )), 		aDados[7][x][2] 	)		
	next
	
	oProcess:cSubject := "Libera��o de Pedido de Compras" 
	oProcess:cTo      := cEmail
	oProcess:cFromAddr:= Getmv("SA_MAILCOM")
	oProcess:cFromName:= "SAS � Compras"
	oProcess:Start()
	
Return

