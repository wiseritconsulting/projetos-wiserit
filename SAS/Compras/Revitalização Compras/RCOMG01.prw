#INCLUDE "rwmake.ch"
#INCLUDE "protheus.ch"
#INCLUDE "topconn.ch"

#define DS_MODALFRAME 128

User Function RCOMG01 

Local cMsg := ""

IF FUNNAME() == "MATA010" .OR. FUNNAME() == "MATA121"
	IF !empty(M->B1_YSUBGR) .AND. !empty(M->B1_GRUPO)
	return GatProd()
	ELSE
	return
	ENDIF
	
ENDIF	


cQuery := "SELECT TOP 1 * FROM "+RetsqlName("SD1")+" NOLOCK "
cQuery += "WHERE D1_COD = '"+Alltrim(aCols[n, aScan(aHeader,{|x| "C8_PRODUTO" $ x[2]})])+"'"
cQuery += "AND D_E_L_E_T_ = '' ORDER BY D1_EMISSAO "
TcQuery cQuery New Alias T01

If !(EOF("T01")) .AND. T01->D1_VUNIT < M->C8_PRECO
	cMsg += "O valor do produto na ultima compra foi de " 
	cMsg += "R$ "+Alltrim(TRANSFORM(T01->D1_VUNIT,"@E 999,999,999.99"))+" "
	cMsg += "do fornecedor "+POSICIONE("SA2",1, xFilial("SA2")+T01->D1_FORNECE+T01->D1_LOJA, "A2_NOME")
	cMsg += " na data "+dtoc(STOD(T01->D1_EMISSAO))+". Favor informar a justificativa."
	
	Aviso("Justificar Aumento do Pre�o",cMsg, {"Ok"}, 1)
	aCols[n, aScan(aHeader,{|x| "C8_YULTCOM" $ x[2]})] := T01->D1_VUNIT
	aCols[n, aScan(aHeader,{|x| "C8_YJUSTPR" $ x[2]})] := U_RCOMG02()
	
	cMsg := ""
EndIf
T01->(DBCloseArea())

// Fun��o para atualizar os valores do total do item;
A150Total(aCols[n, aScan(aHeader,{|x| "C8_TOTAL" $ x[2]})])
MaFisRef("IT_VALMERC","MT150",aCols[n, aScan(aHeader,{|x| "C8_TOTAL" $ x[2]})])

RETURN (M->C8_PRECO)


User Function RCOMG02()

	Local oButton1
	Local oSay1
	Local oValor
	Local cValor := SPACE(80)
	Static oDlg
		
  DEFINE MSDIALOG oDlg TITLE "Justificativa Valor" FROM 000, 000  TO 180, 300 COLORS 0, 16777215 PIXEL STYLE DS_MODALFRAME 

    @ 017, 022 SAY oSay1 PROMPT "Justificar Valor" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 026, 022 MSGET oValor VAR cValor SIZE 100, 010 VALID !VAZIO(cValor) OF oDlg COLORS 0, 16777215 PIXEL
    @ 061, 048 BUTTON oButton1 PROMPT "Confirmar" SIZE 060, 012 OF oDlg PIXEL Action Close(oDlg)

	oDlg:lEscClose := .F.

  ACTIVATE MSDIALOG oDlg CENTERED

Return cValor

/*
Autor: Weskle Silva 
Data: 17/08/2016
Rotina: Gatilho Sequencial com base no grupo do produto
*/
Static Function GatProd()

	Local aArea  := GetArea()
	Local cCod   := ""
	Local cSubgru := M->B1_YSUBGR
	Local cGrup   := M->B1_GRUPO
	Local cQuery 

	cSubgru := Alltrim(StrTran(cSubgru, '"'," "))
	cSubgru := Alltrim(StrTran(cSubgru, "'"," "))
	cGrup :=   Alltrim(StrTran(cGrup, "'"," "))
	cGrup :=   Alltrim(StrTran(cGrup, '"'," "))
	cQuery := "SELECT Max(B1_COD) as B1_COD FROM "+RetSqlName("SB1")+" WHERE B1_FILIAL='"+xFilial("SB1")+"' AND D_E_L_E_T_=' ' AND B1_GRUPO='"+cGrup+"' AND B1_YSUBGR='"+cSubgru+"' AND B1_COD NOT IN ('100001')"	
	
	TcQuery cQuery New Alias T01	
	
	IF T01->(!Eof()) 

		cCod :=    ALLTRIM(SubStr(cGrup,3,2)+(SubStr(cSubgru,1,2))+SOMA1(SUBSTR(T01->B1_COD,5,4)))
	Else
		cCod := ALLTRIM(SubStr(cGrup,1,4)+"0001")
	Endif

	dbSelectArea("T01")

	DbCloseArea("T01")
	//T01->(DbCloseArea())
	RestArea(aArea)

return cCod

