#Include 'Protheus.ch'
#Include 'topconn.ch'

User Function MT120APV()
	
	Local cRet	:= SC7->C7_APROV
	
	IF FUNNAME() == "MATA122"
	cRet := ""
	Return cRet
	ENDIF
	
	cQuery := " Select TOP 1 C1_CC From " + RetSqlName("SC1")
	cQuery += " Where C1_FILIAL = '"+xFilial("SC1")+"' AND C1_NUM = '"+SC7->C7_NUMSC+"' AND D_E_L_E_T_ = ''"
	TcQuery cQuery New Alias TCC
	
	If !(EOF("TCC"))				
		cRet := POSICIONE("CTT",1,xFilial("CTT")+TCC->C1_CC, "CTT_YGPRES")			
	EndIf
	
	TCC->(DBCLOSEAREA())
	
Return cRet
