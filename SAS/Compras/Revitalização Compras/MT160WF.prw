#INCLUDE "topconn.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWEBEX.CH"
#INCLUDE "Ap5Mail.ch"
#INCLUDE "rwmake.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � MT120FIM    �Autor  � Rafael Xavier   � Data �  28/12/15   ���
�������������������������������������������������������������������������͹��
���Desc.     � Ponto de Entrada que trata que unifica PC                  ���
�������������������������������������������������������������������������͹��
���Uso       �                                                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������

*/

User Function MT160WF()	
	Local aDados  := {}
	Local aItens  := {}
	Local nTotal  := 0
	Local nTotFre := 0
	Local nDesc	  := 0
	Local nAdiant := 0
	Local cCond   := ""
	Local cUsrSol := ""	
	Local cCC	  := ""
	
	cQuery := " SELECT DISTINCT C8_FILIAL, C8_NUMPED, C8_NUMSC FROM " + RetSqlName("SC8")
	cQuery += " WHERE C8_FILIAL ='"+xFilial("SC8")+"' AND C8_NUM ='"+SC8->C8_NUM+"' AND C8_NUMPED <> 'XXXXXX' AND C8_NUMPED <> '' AND D_E_L_E_T_ = '' "
	TcQuery cQuery New Alias COT
	
	While(!COT->(Eof()))
		
		AVISO("Pedido de Compras "+COT->C8_NUMPED,"A Cota��o gerou o Pedido de Compras:"+COT->C8_NUMPED, {"Ok"}, 1)
		
		cQuery := " Select TOP 1 C1_YADIANT, C1_USER From " + RetSqlName("SC1")
		cQuery += " Where C1_FILIAL = '"+xFilial("SC1")+"' AND C1_NUM = '"+COT->C8_NUMSC+"' AND D_E_L_E_T_ = ''"
		TcQuery cQuery New Alias T01
		
		If !(EOF("T01"))
			cUsrSol := T01->C1_USER
			nAdiant := T01->C1_YADIANT				
			If TCSQLExec("UPDATE "+RETSQLNAME("SC7")+" SET C7_YADIANT = "+STR(T01->C1_YADIANT)+" WHERE C7_FILIAL='"+xFilial("SC7")+"' AND C7_NUM='"+COT->C8_NUMPED+"' AND D_E_L_E_T_=' '")<0
				Alert("Erro na atualiza��o do adiantamento no pedido de compra")
			EndIf			
		EndIf
		
		T01->(DBCLOSEAREA())	
		
			
		cQuery := " SELECT C7_FORNECE, A2_NOME, C7_NUM, C7_FILIAL, C7_PRODUTO,B1_DESC, C7_QUANT, C7_PRECO,CTT_DESC01,C7_CC, C7_OBS, C7_EMISSAO, C7_TOTAL, C7_VALFRE, C7_VLDESC, C7_COND, C7_YADIANT, E4_DESCRI"
		cQuery += " FROM "+RetSqlName("SC7")+" SC7, "+RetSqlName("SA2")+" SA2, "+RetSqlName("SB1")+" SB1, "+RetSqlName("SE4")+" SE4, "+RetSqlName("CTT")+" CTT (NOLOCK)"
		cQuery += " WHERE SC7.C7_FILIAL = '"+ COT->C8_FILIAL +"' AND SC7.C7_NUM = '"+ COT->C8_NUMPED +"' AND SC7.C7_FORNECE = SA2.A2_COD AND SC7.C7_LOJA = SA2.A2_LOJA AND SC7.C7_CC = CTT.CTT_CUSTO AND SC7.D_E_L_E_T_ = '' AND SA2.D_E_L_E_T_ = '' "
		cQuery += " AND SC7.C7_PRODUTO = SB1.B1_COD AND SC7.C7_COND = SE4.E4_CODIGO "
		TcQuery cQuery New Alias T01
		
		If(!T01->(EOF()))			
			cCond 	:= T01->C7_COND +" - "+T01->E4_DESCRI
			
			aadd(aDados, T01->C7_FORNECE)
			aadd(aDados, T01->A2_NOME)
			aadd(aDados, T01->C7_NUM)
			aadd(aDados, T01->C7_EMISSAO)
			aadd(aDados, T01->C7_FILIAL)
			cCC := alltrim(T01->C7_CC) + " - " + alltrim(T01->CTT_DESC01)
			While(!T01->(EOF()))
				nTotal 	+= T01->C7_TOTAL
				nTotFre += T01->C7_VALFRE
				nDesc 	+= T01->C7_VLDESC
				
				aadd(aItens, {T01->B1_DESC, T01->C7_QUANT, T01->C7_PRECO, T01->C7_TOTAL, T01->C7_OBS, T01->C7_PRODUTO})
				
				T01->(DbSkip())
			EndDo
			aadd(aDados, aItens	)
			aadd(aDados, nTotal	)
			aadd(aDados, nTotFre)
			aadd(aDados, nDesc	)
			aadd(aDados, cCond	)
			aadd(aDados, ""		)
			aadd(aDados, nAdiant)
			aadd(aDados, cCC )
		EndIf
		
		
		
		T01->(DbCloseArea())
		
		cQuery2 := " SELECT * FROM "+RetSqlName("SCR")
		cQuery2 += " WHERE CR_FILIAL = '"+COT->C8_FILIAL+"' AND CR_NUM = '"+COT->C8_NUMPED+"' AND CR_STATUS = '02' AND D_E_L_E_T_ = '' "
		TCQuery cQuery2 New Alias T02
		
		While(!T02->(EOF()))		
			aDados[11] := T02->CR_USER
			U_SASP123(UsrRetMail(T02->CR_USER),"\WORKFLOW\ABERTURAPCAPROV.HTM",aDados,"Abertura de Pedido de Compra - Aprova��o")
			
			T02->(DbSkip())
		EndDo
		
		T02->(DbCloseArea())
		
		aDados[11] := "compras"
		//Email para o compras
		U_SASP123(GetMv("SA_MAILCOM"),"\WORKFLOW\ABERTURAPC.HTM",aDados,"Abertura de Pedido de Compra")
		//Email para o solicitante
		U_SASP123(UsrRetMail(cUsrSol),"\WORKFLOW\ABERTURAPC.HTM",aDados,"Abertura de Pedido - Solicitante: "+ UsrRetName(cUsrSol))
		
		aDados := {}
		aItens := {}
		nTotal  := 0
		nTotFre := 0
		nDesc	  := 0
		COT->(DbSkip())	
	EndDo
	COT->(DbCloseArea())	
Return