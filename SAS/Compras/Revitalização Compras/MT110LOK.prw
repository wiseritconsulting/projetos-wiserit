// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : MT110LOK.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor Rog�rio J�come            | Descricao Utilizado para preencher o centro de custo em todos os itens da SC
// -----------+-------------------+---------------------------------------------------------
// 28/05/2016 | Rogerio           | Gerado com aux�lio do Assistente de C�digo do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"
#INCLUDE "topconn.ch"
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MT110LOK

@author    Rogerio
@version   11.3.0.201603221954
@since     28/05/2016
/*/
//------------------------------------------------------------------------------------------
user function MT110LOK()

	Local aArea 		:= GetArea() 
	Local nPosCTT 		:= aScan(aHeader,{|x| AllTrim(x[2])=='C1_CC'})//Pega a posi��o do centro de custo no acols
	Local nPosValDes 	:= aScan(aHeader,{|x| AllTrim(x[2])=='C1_YADIANT'})
	Local nPosPerDes 	:= aScan(aHeader,{|x| AllTrim(x[2])=='C1_YPERCEN'})
	Local lRet			:= .T.
	Local nPosPrc 		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YVUNIT"})
	Local nPosFor 		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_FORNECE"})
	Local nPosCot 		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YTEMCOT"})
	Local nPosAdi 		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YTEMADI"})
	Local nPosProd		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_PRODUTO"})
	Local nPosUrg		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YURGEN"})
	
	Local cDtPag		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YDTPAG"})
	Local cFormPag		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YFORMPA"})
	
	Local nDespes		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YDESPES"})
	Local nFrete		:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YFRETE"})	
	Local nDesc			:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YDESCON"})
	
	Local cQuery 		:= ''

	If(Empty(_cCTT___))
		MsgAlert("Por favor, preencha o centro de custo.", "Centro de Custo obrigat�rio")
		Return .F.
	Endif

	if(_nComboAdi == "S" .AND. (Empty(_cValDes_) .AND. Empty(_cPerDes_)))
		MsgAlert("Para solicita��es com adiantamento, preencher o valor do adiantamento.")
		Return .F.
	Endif

	if(_nComboCot == "N")
		for x := 1 to len(aCols)
			if(Empty(ALLTRIM(aCols[x][nPosFor])))
				MsgAlert("Para solicita��es que n�o geram cota��es � necess�ria a digita��o do fornecedor.")
				Return .F.
			endif
		next
	Endif

	for x := 1 to len(aCols)

		/*cQuery := "SELECT D1_VUNIT FROM "+RetsqlName("SD1")+" NOLOCK "
		cQuery += "WHERE D1_COD = '"+aCols[x][nPosProd]+"'"
		cQuery += " AND D_E_L_E_T_ = '' ORDER BY D1_EMISSAO, D1_COD "
		TcQuery cQuery new Alias T01
		*/
		
		aCols[x][nPosCTT] := _cCTT___
		aCols[x][nPosValDes] := _cValDes_
		aCols[x][nPosPerDes] := _cPerDes_
		aCols[x][nPosCot]	 := _nComboCot
		aCols[x][nPosAdi]	 := _nComboAdi
		aCols[x][nPosUrg]	 := _nComboUrg
		
		aCols[x][cDtPag]	 := __dDataPag_
		aCols[x][cFormPag]	 := _cFoPag_
		
		
		
		/*aCols[x][nDespes]	 := _aColsex_[1][3]
		aCols[x][nFrete]	 := _aColsex_[2][3]
		aCols[x][nDesc]		:= _aColsex_[3][3]*/
	/*	
		IF !T01->(EOF())
		aCols[x][nPosPrc]	 := T01->D1_VUNIT
		ENDIF
		T01->(DbCloseArea())*/
	next

	RestArea( aArea )
return lRet

//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de c�digo do TDS tds_version em 28/05/2016 as 11:28:42
//-- fim de arquivo--------------------------------------------------------------------------

