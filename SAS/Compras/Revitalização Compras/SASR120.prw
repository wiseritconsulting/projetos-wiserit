#INCLUDE "rwmake.ch"
#INCLUDE "protheus.ch"
#INCLUDE "topconn.ch"

User Function SASR120()
Private cPerg	:= "SASR120"
AjustaSX1(cPerg)
Pergunte(cPerg,.F.)
oReport := ReportDef(cPerg)
oReport:PrintDialog()

Return


Static Function ReportDef(cPerg)
Local oReport
Local oSection
Local oBreak
//Local aOrdem 	:= {}

oReport := TReport():New(cPerg,"Pedidos de Compras Sem SC",cPerg,{|oReport| PrintReport(oReport)},"Pedidos de Compras Sem SC")
oReport:SetLandscape()
oReport:SetTotalInLine(.F.)

oSection := TRSection():New(oReport,"Pedidos de Compras Sem SC",{"SC7"})

TRCell():New(oSection,"C7_FILIAL"	)
TRCell():New(oSection,"C7_NUM"		)
TRCell():New(oSection,"C7_NUMSC"	)
TRCell():New(oSection,"C7_FORNECE"	)
TRCell():New(oSection,"C7_LOJA"		)
TRCell():New(oSection,"A2_NOME"		)
TRCell():New(oSection,"C7_PRODUTO"	)
TRCell():New(oSection,"B1_DESC"		)
TRCell():New(oSection,"C7_QUANT"	)
TRCell():New(oSection,"C7_PRECO"	)
TRCell():New(oSection,"C7_TOTAL"	)
TRCell():New(oSection,"C7_EMISSAO"	)
TRCell():New(oSection,"C7_CC"		)
TRCell():New(oSection,"CTT_DESC01"	)

Return oReport

/*Inicia Logica Print Report */

Static Function PrintReport(oReport)
Local oSection := oReport:Section(1)
Local cQuery

oSection:Init()

Pergunte(cPerg,.F.)

cQuery := " SELECT C7_FILIAL, C7_NUM, C7_NUMSC, C7_FORNECE, C7_LOJA, A2_NOME, C7_PRODUTO, B1_DESC, C7_QUANT, "
cQuery += " C7_EMISSAO, C7_PRECO, C7_TOTAL, C7_CC, CTT_DESC01 "
cQuery += " FROM "+RetSqlName("SC7")+" SC7, "+RetSqlName("SB1")+" SB1, "+RetSqlName("SA2")+" SA2, "+RetSqlName("CTT")+" CTT WHERE "
cQuery += " SC7.D_E_L_E_T_ = '' AND SB1.D_E_L_E_T_ = '' AND SA2.D_E_L_E_T_ = '' AND CTT.D_E_L_E_T_ = '' AND "
cQuery += " C7_PRODUTO = B1_COD AND C7_FORNECE = A2_COD AND C7_LOJA = A2_LOJA AND CTT_CUSTO = C7_CC AND " 
cQuery += " C7_FILIAL	BETWEEN '"+MV_PAR01+"' 		 AND '"+MV_PAR02+"' AND " 
cQuery += " C7_FORNECE	BETWEEN '"+MV_PAR03+"' 		 AND '"+MV_PAR04+"' AND " 
cQuery += " C7_LOJA 	BETWEEN '"+MV_PAR05+"' 		 AND '"+MV_PAR06+"' AND " 
cQuery += " C7_PRODUTO	BETWEEN '"+MV_PAR07+"' 		 AND '"+MV_PAR08+"' AND " 
cQuery += " C7_EMISSAO 	BETWEEN '"+DTOS(MV_PAR09)+"' AND '"+DTOS(MV_PAR10)+"' " 
If MV_PAR11 == 1
	cQuery += " AND C7_NUMSC = '' " 
ElseIF MV_PAR11 == 2
	cQuery += " AND C7_NUMSC <> '' " 
EndIf 
cQuery += " ORDER BY 1,2,4 "
TcQuery cQuery New Alias T01

oReport:SetMeter(T01->(RecCount())) //Metodo para Dizer o tamanho da barra de progresso baseado no RecCount( Quantidade de Registros )
oSection:Init()	//Inicializando a Sec��o

DO While !oReport:Cancel() .And. !T01->(EOF())

	oSection:Cell("C7_FILIAL"	):SetBlock( { || T01->C7_FILIAL			})
	oSection:Cell("C7_NUM"		):SetBlock( { || T01->C7_NUM			})
	oSection:Cell("C7_NUMSC"	):SetBlock( { || T01->C7_NUMSC	   		})
	oSection:Cell("C7_FORNECE"	):SetBlock( { || T01->C7_FORNECE   		})
	oSection:Cell("C7_LOJA"		):SetBlock( { || T01->C7_LOJA			})
	oSection:Cell("A2_NOME"		):SetBlock( { || T01->A2_NOME		 	})
	oSection:Cell("C7_PRODUTO"	):SetBlock( { || T01->C7_PRODUTO 		})
	oSection:Cell("B1_DESC"		):SetBlock( { || T01->B1_DESC 			})
	oSection:Cell("C7_QUANT"	):SetBlock( { || T01->C7_QUANT			})
	oSection:Cell("C7_PRECO"	):SetBlock( { || T01->C7_PRECO			})
	oSection:Cell("C7_TOTAL"	):SetBlock( { || T01->C7_TOTAL			})
	oSection:Cell("C7_EMISSAO"	):SetBlock( { || StoD(T01->C7_EMISSAO)	})
	oSection:Cell("C7_CC"		):SetBlock( { || T01->C7_CC				})
	oSection:Cell("CTT_DESC01"	):SetBlock( { || T01->CTT_DESC01		})
	oSection:PrintLine()
	T01->(dbSkip())     
	
ENDDO

oSection:Finish()
T01->(DbCloseArea())

Return


Static Function AjustaSX1(cPerg)
PutSx1(cPerg, "01","De Filial?"		,"De Filial?"		,"De Filial?"		,"mv_ch01","C",02,0,1,"G","","SM0","","","mv_par01","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "02","Ate Filial?"	,"Ate Filial?"		,"Ate Filial?"		,"mv_ch02","C",02,0,1,"G","","SM0","","","mv_par02","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "03","De Fornecedor?"	,"De Fornecedor?"	,"De Fornecedor?"	,"mv_ch03","C",06,0,1,"G","","SA2","","","mv_par03","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "04","Ate Fornecedor?","Ate Fornecedor?"	,"Ate Fornecedor?"	,"mv_ch04","C",06,0,1,"G","","SA2","","","mv_par04","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "05","De Loja Forn.?"	,"De Loja Forn.?"	,"De Loja Forn.?"	,"mv_ch05","C",02,0,1,"G","",""   ,"","","mv_par05","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "06","Ate Loja Forn?"	,"Ate Loja Forn?"	,"Ate Loja Forn?"	,"mv_ch06","C",02,0,1,"G","",""   ,"","","mv_par06","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "07","De Produto?"	,"De Produto?"		,"De Produto?"		,"mv_ch07","C",09,0,1,"G","","SB1","","","mv_par07","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "08","Ate Produto?"	,"Ate Produto?"		,"Ate Produto?"		,"mv_ch08","C",09,0,1,"G","","SB1","","","mv_par08","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "09","De Emissao?" 	,"De Emissao?"		,"De Emissao?"		,"MV_CH09","D",10,0,1,"G","",""   ,"","","MV_PAR09","","","","","","","","","","","","","","","","")
PutSx1(cPerg, "10","Ate Emiss�o?"	,"Ate Emiss�o?"		,"Ate Emiss�o?"		,"MV_CH10","D",10,0,1,"G","",""   ,"","","MV_PAR10","","","","","","","","","","","","","","","","")
PutSx1(cPerg, "11","Imprimir?"      ,"Imprimir?"        ,"Imprimir?"        ,"MV_CH11","C",01,0,1,"C","",""   ,"","","MV_PAR11","PC s/ SC","","","","PC c/ SC","","","Ambos","","","","","","","","")
Return nil