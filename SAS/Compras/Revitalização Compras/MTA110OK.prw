// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : MTA110OK
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Rog�rio J�come            | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 27/05/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo

@author    Rog�rio J�come TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     27/05/2016
/*/
//------------------------------------------------------------------------------------------
user function MTA110OK()
	//--< vari�veis >---------------------------------------------------------------------------
	Local lRet			:= .T.
	Local cFil 			:= cFilent
	Local cSolicitacao	:= CA110NUM
	Local cDataHora 	:= DTOC(date())+ " - " + TIME()
	Local cSolicitante	:= CUSERNAME
	Local aProdutos		:= ACLONE(aCols)
	Local nPosFor := Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_FORNECE"})
	Local nPosLoj := Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_LOJA"})
	Local nPosCot := Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YTEMCOT"})
	Local cCodFor := aCols[1][nPosFor]
	Local cLoja	  := aCols[1][nPosLoj]
	Local cCot	  := IIF(aCols[1][nPosCot] == 'N', "Nao", "Sim")
	
	
	//U_SASP123(UsrRetMail(cUsrSol),"\WORKFLOW\ABERTURAPC.HTM",aProdutos,"Abertura de Pedido - Solicitante: "+ UsrRetName(cUsrSol))
	
	U_SASP120(cFil,cSolicitacao,cDataHora,cSolicitante,_cCTT___,aProdutos, cCodFor, cLoja, cCot,UsrRetMail(__CUSERID))
	U_SASP120(cFil,cSolicitacao,cDataHora,cSolicitante,_cCTT___,aProdutos, cCodFor, cLoja, cCot,GetMv("SA_MAILCOM"))
	
	AVISO("Solicita��o de Compras "+CSOLICITACAO,"Solicita��o de Compras gerada: "+CSOLICITACAO, {"Ok"}, 1)
		
	
return lRet
