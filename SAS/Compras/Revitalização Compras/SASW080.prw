#include 'rwmake.ch'
#include "tbiconn.ch"
#include "topconn.ch"

User Function SASW080(__aCookies,__aPostParms,__nProcID,__aProcParms,__cHTTPPage)
	Local cRet 	  := ""
	Local aGet	  := aClone(__aProcParms)
	Local cAcao   := aGet[1][2]
	Local cFil
	Local cPedido
	Local cProduto
	Local nVence
	Local nTotal  := 0
	Local nTotFre := 0
	Local nDesc	  := 0
	Local cCond   := ""
	Local dDescPro:= ""

	if(cAcao == "detalhe")
	
		cFil	:= aGet[2][2]
		cPedido := aGet[3][2]
		cProduto:= aGet[4][2]
		
		PREPARE ENVIRONMENT EMPRESA "01" FILIAL "01"
		
		cQuery	:= " SELECT C7_NUM, C8_NUM, A2_COD, A2_LOJA, A2_NOME, B1_DESC, "
		cQuery	+= " (SELECT TOP 1 D1_EMISSAO FROM "+RetSqlName("SD1")+" SD1 WHERE D1_COD = C8_PRODUTO AND D1_FORNECE = C8_FORNECE AND D1_LOJA = C8_LOJA AND SD1.D_E_L_E_T_ = '') "
		cQuery	+= " AS ULTCOMPRA, C8_VALFRE, C8_PRECO, C8_QUANT, C8_TOTAL, '' AS Dif, C8_OBS, C8_YJUSTPR, B1_COD, B1_DESC, "
		cQuery	+= " CTT_CUSTO, CTT_DESC01 "
		cQuery	+= " FROM "+RetSQLName("SC8")+" SC8, "+RetSQLName("SC7")+" SC7, "+RetSQLName("SA2")+" SA2, "+RetSQLName("SB1")+" SB1, "+RetSQLName("CTT")+" CTT "
		cQuery	+= " WHERE SC7.D_E_L_E_T_ = '' AND SC8.D_E_L_E_T_ = '' AND SA2.D_E_L_E_T_ = '' AND SB1.D_E_L_E_T_ = '' AND CTT.D_E_L_E_T_ = '' "
		cQuery	+= " AND C7_NUMCOT = C8_NUM AND C7_NUM = '"+cPedido+"' AND C7_FILIAL = '"+cFil+"' AND C7_PRODUTO = '"+cProduto+"' AND C8_FILIAL = C7_FILIAL "
		cQuery	+= " AND C8_PRODUTO = C7_PRODUTO AND C8_FORNECE = A2_COD AND C8_LOJA = A2_LOJA AND B1_COD = C8_PRODUTO AND C7_CC = CTT_CUSTO "
		cQuery	+= " ORDER BY C8_NUMPED " 
		
		cQuery2 := " SELECT TOP 3 A2_COD, A2_LOJA, A2_NOME, D1_EMISSAO, D1_QUANT, D1_VUNIT, D1_TOTAL FROM "+RetSQLName("SD1")+" SD1 , "+RetSQLName("SA2")+" SA2 "
		cQuery2 += " WHERE D1_COD = '"+cProduto+"' " 
		cQuery2 += " AND SD1.D_E_L_E_T_ = '' AND SA2.D_E_L_E_T_ = '' AND D1_FORNECE = A2_COD AND D1_LOJA = A2_LOJA " 
		cQuery2 += " ORDER BY D1_EMISSAO DESC, D1_COD "
		
		dDescPro := POSICIONE("SB1",1,xFilial("SB1") + cProduto, "B1_DESC")
		cRet +='<html>
		cRet +='<head>
		cRet +='<meta http-equiv="Content-Language" content="en-us">
		cRet +='<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		cRet +='<title>Pedido de Compras</title>
		cRet +='</head>
		
		cRet +='<body bgcolor="#FFFFFF" bgproperties="fixed" style="font-family:Arial;font-size:12px;" >
		cRet +='<center><img src="http://www.portalsas.com.br/images/Logo-SAS-4.png" /></center>
		cRet +='<h3 style="font-family:Arial;font-size:20px;" align="center">DETALHES DO PRODUTO</h3>
		cRet +='<br>
		
		cRet +='<table style="font-family:Arial;font-size:14px;" width="70%" align="center">
		cRet +='	<tr style="height:25px;text-align:center;">
		cRet +='		<td bgcolor="#00004d" style="color:#ffffff" colspan="2"><b>DADOS DO PEDIDO/CONTRATO</b></td>	
		cRet +='</tr>
		cRet +='<tr style="height:25px;">
		cRet +='<td bgcolor="#d9d9d9"><b>FILIAL:</b> '+cFil+' &nbsp; <b>NUM:</b> '+cPedido+' &nbsp; <b>PRODUTO:</b> '+(cProduto + " - " + dDescPro)+'</td>
		cRet +='</tr>
		cRet +='</table>
		
		TCQuery cQuery New Alias T01
		TCQuery cQuery2 New Alias T02
		
		If(!T01->(Eof()))
			nVence := T01->C8_TOTAL		
			cRet +='<table style="font-family:Arial;font-size:14px;" width="70%" align="center">
			cRet +='<tr style="height:25px;text-align:center;">
			cRet +='<td bgcolor="#00004d" style="color:#ffffff" colspan="2"><b>'+T01->B1_DESC+'</b></td>	
			cRet +='</tr>
			cRet +='<tr style="height:25px;">
			cRet +='<td bgcolor="#d9d9d9">&nbsp;&nbsp; Especifica&ccedil&otildees do Material </td>		
			cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+T01->B1_COD+' - '+T01->B1_DESC+' &nbsp;&nbsp;</td>       
			cRet +='</tr>
			cRet +='<tr style="height:25px;">		
			cRet +='<td bgcolor="#d9d9d9">&nbsp;&nbsp; Centro de Custo </td>
			cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+T01->CTT_CUSTO+' - '+T01->CTT_DESC01+' &nbsp;&nbsp;</td> 
			cRet +='</tr>
			cRet +='</table>	
		
			cRet +='<br />
			cRet +='<br />
			
				cRet +='<table style="font-family:Arial;font-size:14px;" width="70%" align="center">
				cRet +='<tr style="height:25px;text-align:center;">
				cRet +='<td bgcolor="#00004d" style="color:#ffffff" colspan="9" ><b>COTA&Ccedil;&OtildeES</b></td>
				cRet +='</tr>
				cRet +='<tr style="height:25px;text-align:center;">
				cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Fornecedores</b></td>	
				cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>&Uacuteltima Compra</b></td>
				cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Valor Unit.</b></td>
				cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Frete</b></td>
				cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Qtde.</b></td>
				cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Valor Total</b></td>
				cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Dif. R$</b></td>
				cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Dif. %</b></td>
				cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Observa&ccedil&otildees</b></td>
				cRet +='</tr>
			While(!T01->(Eof()))	
				cRet +='<tr style="height:25px;">				
				cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+T01->A2_NOME+' &nbsp;&nbsp;</td>       
				cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+DTOC(STOD(T01->ULTCOMPRA))+' &nbsp;&nbsp;</td>
				cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+TRANSFORM(T01->C8_PRECO, "@E 999,999,999.99")+' &nbsp;&nbsp;</td>
				cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+TRANSFORM(T01->C8_VALFRE, "@E 999,999,999.99")+' &nbsp;&nbsp;</td>       
				cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+str(T01->C8_QUANT)+' &nbsp;&nbsp;</td>
				cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+TRANSFORM(T01->C8_TOTAL, "@E 999,999,999.99")+' &nbsp;&nbsp;</td>
				If(!T02->(Eof()))
					cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+TRANSFORM(T01->C8_PRECO-T02->D1_VUNIT, "@E 999,999,999.99")+' &nbsp;&nbsp;</td>       
					cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+TRANSFORM(((T01->C8_PRECO/T02->D1_VUNIT)-1)*100, "@E 999.99")+' &nbsp;&nbsp;</td>
				Else
					cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+TRANSFORM(T01->C8_TOTAL-nVence, "@E 999,999,999.99")+' &nbsp;&nbsp;</td>       
					cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+TRANSFORM(((T01->C8_TOTAL/nVence)-1)*100, "@E 999.99")+' &nbsp;&nbsp;</td>
				EndIf
				cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+T01->C8_OBS+' &nbsp;&nbsp;</td>
				cRet +='</tr>
				T01->(DbSkip())
			EndDo
			T01->(dbclosearea())
		EndIf				
		cRet +='</table>
			
		cRet +='<br />
		cRet +='<br />
		cRet +='<table style="font-family:Arial;font-size:14px;" width="70%" align="center">
		cRet +='<tr style="height:25px;text-align:center;">
		cRet +='<td bgcolor="#00004d" style="color:#ffffff" colspan="5"><b>&UacuteLTIMAS 03 COMPRAS</b></td>
		cRet +='</tr>
		cRet +='<tr style="height:25px;text-align:center;">
		cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Fornecedores</b></td>	
		cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Data</b></td>
		cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Qtde.</b></td>
		cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Valor Unit.</b></td>
		cRet +='<td bgcolor="#00004d" style="color:#ffffff" ><b>Valor Total</b></td>
		cRet +='</tr>
		
		WHILE(!T02->(EOF()))
			cRet +='<tr style="height:25px;">				
			cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+T02->A2_NOME+' &nbsp;&nbsp;</td>       
			cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+DTOC(STOD(T02->D1_EMISSAO))+' &nbsp;&nbsp;</td>
			cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+str(T02->D1_QUANT)+' &nbsp;&nbsp;</td>
			cRet +='<td bgcolor="#d9d9d9" style="text-align:right;">  '+TRANSFORM(T02->D1_VUNIT, "@E 999,999,999.99")+' &nbsp;&nbsp;</td>       
			cRet +='<td bgcolor="#d9d9d9" style="text-align:right;"> '+TRANSFORM(T02->D1_TOTAL, "@E 999,999,999.99")+' &nbsp;&nbsp;</td>
			cRet +='</tr>
			T02->(DbSkip())
		ENDDO
		T02->(DbCloseArea())
		
		cRet +='</table>
		
		cRet +='</body>
		cRet +='</html>'
	ElseIf(cAcao == "pedido")
	
		cFil	:= aGet[2][2]
		cPedido := aGet[3][2]		
		
		PREPARE ENVIRONMENT EMPRESA "01" FILIAL "01"
		
		cQuery := " SELECT C7_FORNECE, A2_NOME,C7_PRODUTO,B1_DESC, C7_QUANT, C7_PRECO, C7_OBS, C7_EMISSAO, C7_TOTAL, C7_VALFRE, C7_VLDESC, C7_COND, E4_DESCRI"
		cQuery += " FROM "+RetSqlName("SC7")+" SC7, "+RetSqlName("SA2")+" SA2, "+RetSqlName("SB1")+" SB1, "+RetSqlName("SE4")+" SE4"
		cQuery += " WHERE SC7.C7_FILIAL = '"+ cFil +"' AND SC7.C7_NUM = '"+ cPedido+"' AND SC7.C7_FORNECE = SA2.A2_COD AND SC7.C7_LOJA = SA2.A2_LOJA AND SC7.D_E_L_E_T_ = '' AND SA2.D_E_L_E_T_ = '' "
		cQuery += " AND SC7.C7_PRODUTO = SB1.B1_COD AND SC7.C7_COND = SE4.E4_CODIGO " 
		
		cRet +='<html>
		cRet +='<head>
		cRet +='<meta http-equiv="Content-Language" content="en-us">
		cRet +='<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		cRet +='<title>Produto</title>
		cRet +='</head>
		
		cRet +='<body bgcolor="#FFFFFF" bgproperties="fixed" style="font-family:Arial;font-size:12px;" >
		cRet +='<center><img src="http://www.portalsas.com.br/images/Logo-SAS-4.png" /></center>
		cRet +='<br>
		
		cRet +='<table style="font-family:Arial;font-size:14px;" width="70%" align="center">
		cRet +='<tr style="height:25px;text-align:center;">
		cRet +='	<td bgcolor="#00004d" style="color:#ffffff" colspan="2"><b>Aprova&ccedil&atildeo</b></td>	
		cRet +='</tr>
		TCQuery cQuery New Alias T01
		
		If(!T01->(Eof()))
			cCond := T01->E4_DESCRI
			
			cRet +='<tr style="height:25px;">
			cRet +='<td bgcolor="#d9d9d9"><b>Fornecedor:</b> '+T01->C7_FORNECE +' - '+T01->A2_NOME+' &nbsp;</td>
			cRet +='<td bgcolor="#d9d9d9"><b>Num.:</b> '+cPedido+' &nbsp;</td>
			cRet +='</tr>
			cRet +='<tr style="height:25px;">
			cRet +='<td bgcolor="#d9d9d9"><b>Data Emiss&atildeo:</b> '+DTOC(STOD(T01->C7_EMISSAO))+'</td>
			cRet +='<td bgcolor="#d9d9d9"></td>
			cRet +='</tr>
			cRet +='</table>
			
			cRet +='<br />
			
			cRet +='<table style="font-family:Arial;font-size:14px;" width="70%" align="center">
			cRet +='<tr style="height:25px;text-align:center;">
			cRet +='<td bgcolor="#00004d" style="color:#ffffff"><b>Produtos</b></td>
			cRet +='<td bgcolor="#00004d" style="color:#ffffff"><b>Quantidade</b></td>
			cRet +='<td bgcolor="#00004d" style="color:#ffffff"><b>Valor Unit.</b></td>
			cRet +='<td bgcolor="#00004d" style="color:#ffffff"><b>Valor Total</b></td>
			cRet +='<td bgcolor="#00004d" style="color:#ffffff"><b>Observa&ccedil&atildeo</b></td>
			cRet +='<td bgcolor="#00004d" style="color:#ffffff"><b></b></td>		
			cRet +='</tr>
			While(!T01->(Eof()))
				nTotal += T01->C7_TOTAL
				nTotFre += T01->C7_VALFRE
				nDesc 	+= T01->C7_VLDESC
				
				cRet +='<tr style="height:25px;">
				cRet +='<td bgcolor="#d9d9d9" style="text-align:center;"> '+T01->B1_DESC+' &nbsp;&nbsp;</td> 
				cRet +='<td bgcolor="#d9d9d9" style="text-align:center;"> '+STR(T01->C7_QUANT)+' &nbsp;&nbsp;</td>
				cRet +='<td bgcolor="#d9d9d9" style="text-align:center;"> '+TRANSFORM(T01->C7_PRECO, "@E 999,999,999.99")+' &nbsp;&nbsp;</td>
				cRet +='<td bgcolor="#d9d9d9" style="text-align:center;"> '+TRANSFORM(T01->C7_TOTAL, "@E 999,999,999.99")+' &nbsp;&nbsp;</td>
				cRet +='<td bgcolor="#d9d9d9" style="text-align:center;"> '+T01->C7_OBS+' &nbsp;&nbsp;</td>
				cRet +='<td bgcolor="#d9d9d9" style="text-align:center;">
				cRet +='<a href="http://201.49.59.101:8082/ws010101/u_sasw080.apl?action=detalhe&filial='+cFil+'&pedido='+cPedido+'&produto='+T01->C7_PRODUTO+'"><b>Detalhe</b><a>
				cRet +='</td>	
				cRet +='</tr>
				T01->(DbSkip())
			EndDo
			cRet +='<table style="font-family:Arial;font-size:14px;" width="70%" align="center">
			cRet +='<tr style="height:25px;">
			cRet +='<td bgcolor="#d9d9d9"><b>Valor Total:</b> '+TRANSFORM(nTotal, "@E 999,999,999.99")+' &nbsp;</td>
			cRet +='<td bgcolor="#d9d9d9"><b>Frete: </b> '+TRANSFORM(nTotFre, "@E 999,999,999.99")+'</td>
			
			cRet +='</tr>
			cRet +='<tr style="height:25px;">			
			cRet +='<td bgcolor="#d9d9d9"><b>Desconto:</b> '+TRANSFORM(nDesc, "@E 999,999,999.99")+' &nbsp;</td>
			cRet +='<td bgcolor="#d9d9d9"><b>Condi&ccedil&atildeo de Pgto.: </b> '+cCond+'</td>			
			cRet +='</tr>
			cRet +='</table>
			
			/*cRet +='<table style="font-family:Arial;font-size:14px;" width="70%" align="center">					
			cRet +='<tr style="height:25px;">		
			cRet +='<td style="text-align:left;">
			cRet +='<a href="http://201.49.59.102:8087/u_sasw081.apl?action=aprovar?filial='+cFil+'&pedido='+cPedido+'&usuario=">
			cRet += '<input type="submit" value="Confirmar" style="background-color: #003364;color: #FFF;padding: 1em;text-align: center;border-radius: 10px;" ></input>
			cRet +='</a>'
			cRet +='&nbsp;
			cRet +='&nbsp;
			cRet +='&nbsp;
			cRet +='<a href="http://201.49.59.102:8087/u_sasw081.apl?action=rejeitar?filial='+cFil+'&pedido='+cPedido+'&usuario=">
			cRet += '<input type="submit" value="Rejeitar" style="background-color: #B22222;color: #FFF;padding: 1em;text-align: center;border-radius: 10px;" ></input>
			cRet +='</a></td>
			cRet +='</tr>*/
		EndIf
		T01->(DbCloseArea())
		//cRet +='</table>
		cRet +='</body>
		cRet +='</html>'
	Else
		cRet += ' <html>     '
		cRet += ' <head>     '
		cRet += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
		cRet += ' </head>     '
		cRet += ' <body>     '
		cRet += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '
		cRet += '  '	
		cRet += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
		cRet += ' 	<center>  		 '
		cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;"> '
		cRet += ' 			<span class="sas-status-approved" style="color: #DB9600;font-family: Verdana;font-weight: bold;font-size:24px;">Opera&ccedil&atildeo n&atildeo reconhecida!</span> '
		cRet += ' 		</h1>  	 '
		cRet += ' 	</center>   '
		cRet += ' </div>  '
		cRet += '     '		
		cRet += ' <br>    '
		cRet += ' </body>     '
		cRet += ' </html>     '			
	Endif					
Return(cRet)
