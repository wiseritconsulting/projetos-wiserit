#include 'rwmake.ch'
#include "tbiconn.ch"
#include "topconn.ch"

//Libera��o de Pedido de Compras

User Function SASW081(__aCookies,__aPostParms,__nProcID,__aProcParms,__cHTTPPage)	
	Local lOp	 := .F.
	Local cRet 	 := ""
	Local aGet 	 := aClone(__aProcParms)	
	Local cAcao  := aGet[1][2]
	Local cFil	 := aGet[2][2]
	Local cPed	 := aGet[3][2]
	Local cUsu	 := aGet[4][2]
	Local cTipo	 := aGet[5][2]
	Local cGrupo := ""
	Local nStatus:= 0
	Local cAprov := "Pedido n&atildeo foi aprovado."	
	Local cRejei := "Pedido n&atildeo foi rejeitado."
	private aPost	 := aClone(__aPostParms)
	
	if(cAcao == "aprovar")
		PREPARE ENVIRONMENT EMPRESA "01" FILIAL "01"

		If(!U_CHKSCR(cFil, cTipo, cPed, 'A') .AND. !U_CHKSCR(cFil, cTipo, cPed, 'R'))
			DbSelectArea("SCR")
			SCR->(dbSetOrder(2))
			If SCR->(dbSeek(cFil+cTipo + cPed + Space(TamSx3("CR_NUM")[1]-Len(cPed)) + cUsu))
				If(SCR->CR_STATUS == "02")
					IF cTipo == "PC"
					cGrupo := POSICIONE("SC7",1,cFil+cPed, "C7_APROV")
					ELSEIF cTipo == "CP"
					cGrupo := POSICIONE("SC3",1,cFil+cPed, "C3_APROV")
					ENDIF
					lOp := MaAlcDoc({SCR->CR_NUM,SCR->CR_TIPO,SCR->CR_TOTAL,SCR->CR_APROV,,cGrupo,,,,,},dDataBase,4)
					nStatus := 2
					IF(SCR->CR_STATUS == "03")
						//cVldQry := " SELECT CR_STATUS FROM " +RetSqlName("SCR")+" "
						//cVldQry += " WHERE CR_FILIAL = '"+cFil+"' AND CR_NUM = '"+cPed+"' "
						//cVldQry += " CR_NIVEL = '"+ SCR->CR_NIVEL +"' AND CR_STATUS = '2' AND D_E_L_E_T_ = '' "

						U_APROVANIVEL(cFil,cPed, SCR->CR_NIVEL, SCR->CR_APROV, cUsu,cTipo)																							
					EndIf
				Else
					IF cTipo == "PC"
					cAprov := "Pedido n&atildeo encontra-se dispon&iacutevel para seu n&iacutevel!"
					ELSEIF cTipo == "CP"
					cAprov := "Contrato n&atildeo encontra-se dispon&iacutevel para seu n&iacutevel!"
					ENDIF
				EndIf
			EndIf
			SCR->(DbCloseArea())

			IF cTipo == "PC"
			
				IF ( lOp .AND. U_CHKSCR(cFil, cTipo, cPed, 'A'))
					cQuery := " SELECT C7_ITEM, C7_SEQUEN FROM " + RetSqlName("SC7")
					cQuery += " WHERE C7_FILIAL = '"+cFil+"' AND C7_NUM = '"+cPed+"' AND D_E_L_E_T_ = '' "
					TcQuery cQuery New Alias T01

					While(!T01->(Eof()))
						DbSelectArea("SC7")

						If SC7->(DbSeek(cFil+cPed+T01->C7_ITEM+T01->C7_SEQUEN))
							RecLock("SC7", .F.)
							SC7->C7_CONAPRO := "L"
							MsUnlock()
						EndIf

						SC7->(DbCloseArea())									
						T01->(DbSkip())
					EndDo								
					T01->(DbCloseArea())

					dbSelectArea("SC7")
					dbSetOrder(1)      // C7_FILIAL + C7_NUM			
					dbSeek(cFil + cPed)			 

					If Found()    // Avalia o retorno da pesquisa realizada				
						IF(SC7->C7_YADIANT > 0)
							U_MailAprov(cFil, cPed, "FIN",cTipo)
						EndIf

						U_MailAprov(cFil, cPed, "COM",cTipo)	
						cAprov := "Pedido foi aprovado!"		
					EndIf

					SC7->(dbCloseArea())
				ENDIF
			ELSEIF cTipo == "CP"

				IF ( lOp .AND. U_CHKSCR(cFil, cTipo, cPed, 'A'))
					cQuery := " SELECT C3_ITEM FROM " + RetSqlName("SC3")+" "
					cQuery += " WHERE C3_FILIAL = '"+cFil+"' AND C3_NUM = '"+cPed+"' AND D_E_L_E_T_ = '' "
					TcQuery cQuery New Alias T01

					While(!T01->(Eof()))
						DbSelectArea("SC3")
						DbSetOrder(1)

						If SC3->(DbSeek(cFil+cPed+T01->C3_ITEM))
							RecLock("SC3", .F.)
							SC3->C3_CONAPRO := "L"
							MsUnlock()
						EndIf

						SC3->(DbCloseArea())									
						T01->(DbSkip())
					EndDo
					//U_MailAprov(cFil, cPed, "COM",cTipo)		
					cAprov := "Contrato foi aprovado!"		 			
					T01->(DbCloseArea())

				ENDIF

			Endif

			If(nStatus == 2 .AND. U_MailAprov(cFil, cPed, "APROV",cTipo))
				IF cTipo == "PC"
				cAprov := "Pedido liberado, aguardando libera&ccedil&atildeo do pr&oacuteximo n&iacutevel."
				ELSEIF cTipo == "CP"				
				cAprov := "Contrato liberado, aguardando libera&ccedil&atildeo do pr&oacuteximo n&iacutevel."
				ENDIF		
			endif
		ElseIf(U_CHKSCR(cFil, cTipo, cPed, 'R'))
		
			IF cTipo == "PC"
			cAprov := "Pedido bloqueado por outro aprovador!"
			ELSEIF cTipo == "CP"
			cAprov := "Contrato bloqueado por outro aprovador!"
			ENDIF
		Else
			IF cTipo == "PC"
			cAprov := "Pedido j&aacute aprovado anteriormente!"	
			ELSEIF cTipo == "CP"
			cAprov := "Contrato j&aacute aprovado anteriormente!"
			ENDIF		
		EndIf
		cRet += ' <html>     '
		cRet += ' <head>     '
		cRet += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
		cRet += ' </head>     '
		cRet += ' <body>     '
		cRet += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '
		cRet += '  '	
		cRet += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
		cRet += ' 	<center>  		 '
		cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;"> '
		cRet += ' 			<span class="sas-status-approved" style="color: #DB9600;font-family: Verdana;font-weight: bold;font-size:24px;">'+cAprov+'</span> '
		cRet += ' 		</h1>  	 '
		cRet += ' 	</center>   '
		cRet += ' </div>  '
		cRet += '     '		
		cRet += ' <br>    '
		cRet += ' </body>     '
		cRet += ' </html>     '
	Else
	If(cAcao == "rejeitar")

	cRet += ' <html>     '
	cRet += ' <head>     '
	cRet += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
	cRet += ' </head>     '
	cRet += ' <body>     '
	cRet += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '		
	cRet += ' <form action="u_sasw081.apl?action=rejeitado&filial='+cFil+'&pedido='+cPed+'&usuario='+cUsu+'&tipo='+cTipo+'" method="post">    '
	cRet += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
	cRet += ' 	<h3>Motivo da Rejei&ccedil;&atilde;o</h3>  		 '
	//			cRet += ' 	 <input list="motivos" name="rejeicao" class="sas-status-approved" style="margin: 1em 0;width: 100%;overflow: hidden;font-family: Verdana;font-weight: bold;font-size:14px;border-radius: 10px;">
	cRet += ' 	    <select name="rejeicao" class="sas-status-approved" style="margin: 1em 0;width: 100%;overflow: hidden;font-family: Verdana;font-weight: bold;font-size:14px;border-radius: 10px;">
	cRet += '		   <option value="">(Selecione um motivo!)</option>
	cRet += '		   <option value="CONDICAO DE PAGAMENTO">CONDICAO DE PAGAMENTO</option>
	cRet += '		   <option value="DADOS INSUFICIENTES">DADOS INSUFICIENTES</option>
	cRet += '		   <option value="CENTRO DE CUSTO INCORRETO">CENTRO DE CUSTO INCORRETO</option>
	cRet += '		   <option value="FILIAL INCORRETA">FILIAL INCORRETA</option>
	cRet += '		   <option value="VALOR ACIMA DO ESPERADO">VALOR ACIMA DO ESPERADO</option>
	cRet += '		   <option value="FORNECEDOR INCOMPATIVEL COM A NECESSIDADE">FORNECEDOR INCOMPATIVEL COM A NECESSIDADE</option>
	cRet += '		   <option value="PRODUTO NAO ATENDE A NECESSIDADE">PRODUTO NAO ATENDE A NECESSIDADE</option>
	cRet += '		   <option value="NAO HA MAIS NECESSIDADE DA COMPRA">NAO HA MAIS NECESSIDADE DA COMPRA</option>
	cRet += '		</select>
	cRet += ' </div>  '
	cRet += '     '
	cRet += '<input type="submit" value="Enviar" style="background-color: #003364;color: #FFF;padding: 1em;text-align: center;border-radius: 10px;" ></input>			
	cRet += '</form> <br>    '
	cRet += ' </body>     '
	cRet += ' </html>     '

	Else
	If(cAcao == "rejeitado")
	If(!Empty(aPost[1][2]))
	PREPARE ENVIRONMENT EMPRESA "01" FILIAL "01"

	If(!U_CHKSCR(cFil, cTipo, cPed, 'A'))
	DbSelectArea("SCR")
	SCR->(dbSetOrder(2))
	If SCR->(dbSeek(cFil+cTipo + cPed + Space(TamSx3("CR_NUM")[1]-Len(cPed)) + cUsu))				
	If(SCR->CR_STATUS == "02")
	IF cTipo == "PC"
	cGrupo := POSICIONE("SC7",1,cFil+cPed, "C7_APROV")
	ELSEIF cTipo == "CP"
	cGrupo := POSICIONE("SC3",1,cFil+cPed, "C3_APROV")
	ENDIF
	
	
	lOp := MaAlcDoc({SCR->CR_NUM,SCR->CR_TIPO,SCR->CR_TOTAL,SCR->CR_APROV,,cGrupo,,,,,},dDataBase,6)
	cRejei := "O pedido foi rejeitado!"
	U_MailAprov(cFil, cPed, "COMR",cTipo)

	cQuery := " SELECT C7_ITEM, C7_SEQUEN FROM " + RetSqlName("SC7")
	cQuery += " WHERE C7_FILIAL = '"+cFil+"' AND C7_NUM = '"+cPed+"' AND D_E_L_E_T_ = '' "
	TcQuery cQuery New Alias T01

	While(!T01->(Eof()))
	DbSelectArea("SC7")

	If SC7->(DbSeek(cFil+cPed+T01->C7_ITEM+T01->C7_SEQUEN))
	RecLock("SC7", .F.)
	SC7->C7_YMOTREJ := aPost[1][2]
	MsUnlock()
	EndIf

	SC7->(DbCloseArea())									
	T01->(DbSkip())
	EndDo								
	T01->(DbCloseArea())						
	Else
	cRejei := "O pedido n&atildeo encontra-se dispon&iacutevel!"
	EndIf							
	EndIf
	SCR->(DbCloseArea())
	ElseIf(U_CHKSCR(cFil, cTipo, cPed, 'R'))
	cAprov := "Pedido bloqueado por outro aprovador!"
	Else
	cRejei := "Pedido j&aacute aprovado anteriormente!"
	EndIf
	cRet += ' <html>     '
	cRet += ' <head>     '
	cRet += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
	cRet += ' </head>     '
	cRet += ' <body>     '
	cRet += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '
	cRet += '  '	
	cRet += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
	cRet += ' 	<center>  		 '
	cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;"> '
	cRet += ' 			<span class="sas-status-approved" style="color: #DB9600;font-family: Verdana;font-weight: bold;font-size:24px;">'+cRejei+'</span> '
	cRet += ' 		</h1>  	 '
	cRet += ' 	</center>   '
	cRet += ' </div>  '
	cRet += '     '		
	cRet += ' <br>    '
	cRet += ' </body>     '
	cRet += ' </html>     '
	Else
	cRet += ' <html>     '
	cRet += ' <head>     '
	cRet += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
	cRet += ' </head>     '
	cRet += ' <body>     '
	cRet += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '		
	cRet += ' <form action="u_sasw081.apl?action=rejeitado&filial='+cFil+'&pedido='+cPed+'&usuario='+cUsu+'&tipo='+cTipo+'" method="post">    '
	cRet += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
	cRet += ' 	<h3>Motivo da Rejei&ccedil;&atilde;o</h3>  		 '
	cRet += ' 	    <select name="rejeicao" class="sas-status-approved" style="margin: 1em 0;width: 100%;overflow: hidden;font-family: Verdana;font-weight: bold;font-size:14px;border-radius: 10px;">
	cRet += '		   <option value="">(Selecione um motivo!)</option>
	cRet += '		   <option value="CONDICAO DE PAGAMENTO">CONDICAO DE PAGAMENTO</option>
	cRet += '		   <option value="DADOS INSUFICIENTES">DADOS INSUFICIENTES</option>
	cRet += '		   <option value="CENTRO DE CUSTO INCORRETO">CENTRO DE CUSTO INCORRETO</option>
	cRet += '		   <option value="FILIAL INCORRETA">FILIAL INCORRETA</option>
	cRet += '		   <option value="VALOR ACIMA DO ESPERADO">VALOR ACIMA DO ESPERADO</option>
	cRet += '		   <option value="FORNECEDOR INCOMPATIVEL COM A NECESSIDADE">FORNECEDOR INCOMPATIVEL COM A NECESSIDADE</option>
	cRet += '		   <option value="PRODUTO NAO ATENDE A NECESSIDADE">PRODUTO NAO ATENDE A NECESSIDADE</option>
	cRet += '		   <option value="NAO HA MAIS NECESSIDADE DA COMPRA">NAO HA MAIS NECESSIDADE DA COMPRA</option>
	cRet += '		</select>
	cRet += ' </div>  '
	cRet += '     '
	cRet += '<input type="submit" value="Enviar" style="background-color: #003364;color: #FFF;padding: 1em;text-align: center;border-radius: 10px;" ></input>			
	cRet += '<h4>O campo de rejei&ccedil&atildeo n&atildeo pode estar vazio!</h4>     '
	cRet += '</form> <br>    '
	cRet += ' </body>     '
	cRet += ' </html>     '
	EndIf
	Else
	cRet += ' <html>     '
	cRet += ' <head>     '
	cRet += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8">     '
	cRet += ' </head>     '
	cRet += ' <body>     '
	cRet += ' <center><img src="http://www.portalsas.com.br/images/Logo-SAS.png" style="width: 30%;height: auto;"></center>     '
	cRet += '  '	
	cRet += ' <div class="sas-box-status" style="background-color: #FFF;padding: 1%;border: 10px solid #024457;border-radius: 20px;">  	 '
	cRet += ' 	<center>  		 '
	cRet += ' 		<h1 style="font-family: Verdana;font-weight: normal;color: #003364;"> '
	cRet += ' 			<span class="sas-status-approved" style="color: #DB9600;font-family: Verdana;font-weight: bold;font-size:24px;">Opera&ccedil&atildeo n&atildeo reconhecida!</span> '
	cRet += ' 		</h1>  	 '
	cRet += ' 	</center>   '
	cRet += ' </div>  '
	cRet += '     '		
	cRet += ' <br>    '
	cRet += ' </body>     '
	cRet += ' </html>     '
	Endif
	EndIf
	EndIf
	Return(cRet)

	User Function CHKSCR(cFil, cTipo, cNum, cOp)
	Local aAreaSCR := SCR->(GetArea())
	Local lRet := .F.

	DbSelectArea("SCR")	
	DbSetOrder(1)
	If( DbSeek(cFil+cTipo+cNum) )                 

	While !SCR->(Eof()) .And. SCR->(CR_FILIAL+CR_TIPO+CR_NUM) == cFil+cTipo+ALLTRIM(cNum)+Space(TamSx3("CR_NUM")[1]-Len(ALLTRIM(cNum)))
	//se Op for A checa se est� totalmente liberado
	if(cOp == 'A')		
	If !Empty(SCR->CR_DATALIB) .And. SCR->CR_STATUS$"03#05"
	lRet := .T.
	ElseIf SCR->CR_STATUS$"01#02#04"
	lRet := .F.
	EndIf
	//se Op for R, checa se j� foi rejeitado
	ElseIf(cOp == 'R')
	If !Empty(SCR->CR_DATALIB) .And. SCR->CR_STATUS$"04"
	Return .T.
	ElseIf SCR->CR_STATUS$"01#02#03#05"
	lRet := .F.
	EndIf
	Endif
	SCR->( dbSkip() )
	EndDo

	EndIf           

	RestArea(aAreaSCR)
	Return lRet

	//Fun��o envia email para aprovadores de acordo com status 2 na SCR
	//Retorna .T. se for o �ltimo 
	User Function MailAprov(cFil, cPed, cSetor,cTipo)	
	Local cEmailSol := ""
	Local lRet := .F.
	Local cCC		:= ''
	Local cNumSc := "" 
	Local aDados  := {}
	Local aItens  := {}
	Local nTotal  := 0
	Local nTotFre := 0
	Local nDesc	  := 0
	Local nAdiant := 0
	Local cCond   := ""

	IF cTipo == "PC"

	cQuery := " SELECT C7_FORNECE, A2_NOME, C7_NUM, C7_FILIAL, C7_PRODUTO,B1_DESC, C7_QUANT, C7_PRECO,CTT_DESC01,C7_CC, C7_OBS, C7_EMISSAO, C7_TOTAL, C7_VALFRE, C7_VLDESC, C7_NUMSC, C7_COND, C7_YADIANT, E4_DESCRI "
	cQuery += " FROM "+RetSqlName("SC7")+" SC7, "+RetSqlName("SA2")+" SA2, "+RetSqlName("SB1")+" SB1, "+RetSqlName("SE4")+" SE4, "+RetSqlName("CTT")+" CTT (NOLOCK)"
	cQuery += " WHERE SC7.C7_FILIAL = '"+ cFil +"' AND SC7.C7_NUM = '"+ cPed +"' AND SC7.C7_FORNECE = SA2.A2_COD AND SC7.C7_LOJA = SA2.A2_LOJA AND SC7.C7_CC = CTT.CTT_CUSTO AND SC7.D_E_L_E_T_ = '' AND SA2.D_E_L_E_T_ = '' "
	cQuery += " AND SC7.C7_PRODUTO = SB1.B1_COD AND SC7.C7_COND = SE4.E4_CODIGO "
	TcQuery cQuery New Alias T01

	If(!T01->(EOF()))
	If(!Empty(T01->C7_NUMSC))
	cNumSc := T01->C7_NUMSC 
	EndIf

	nAdiant := T01->C7_YADIANT
	cCond := T01->C7_COND +" - "+T01->E4_DESCRI

	aadd(aDados, T01->C7_FORNECE)
	aadd(aDados, T01->A2_NOME)
	aadd(aDados, T01->C7_NUM)
	aadd(aDados, T01->C7_EMISSAO)
	aadd(aDados, T01->C7_FILIAL)
	cCC := alltrim(T01->C7_CC) + " - " + alltrim(T01->CTT_DESC01)
	While(!T01->(EOF()))
	nTotal 	+= T01->C7_TOTAL
	nTotFre += T01->C7_VALFRE
	nDesc 	+= T01->C7_VLDESC

	aadd(aItens, {T01->B1_DESC, T01->C7_QUANT, T01->C7_PRECO, T01->C7_TOTAL, T01->C7_OBS, T01->C7_PRODUTO})

	T01->(DbSkip())
	EndDo
	aadd(aDados, aItens	)
	aadd(aDados, nTotal	)
	aadd(aDados, nTotFre)
	aadd(aDados, nDesc	)
	aadd(aDados, cCond	)
	aadd(aDados, ""		)
	aadd(aDados, nAdiant)
	aadd(aDados, cCC )
	EndIf		

	T01->(DbCloseArea())
	If(cSetor == "APROV")
	cQuery2 := " SELECT * FROM "+RetSqlName("SCR")
	cQuery2 += " WHERE CR_FILIAL = '"+cFil+"' AND CR_NUM = '"+cPed+"' AND CR_STATUS = '02' AND CR_TIPO = '"+cTipo+"' AND D_E_L_E_T_ = '' "
	TCQuery cQuery2 New Alias T02

	If(!T02->(EOF()))
	lRet := .T.
	EndIf

	While(!T02->(EOF()))		
	aDados[11] := T02->CR_USER
	U_SASP123(UsrRetMail(T02->CR_USER),"\WORKFLOW\ABERTURAPCAPROV.HTM",aDados, "Aprova��o de Pedido de Compra")

	T02->(DbSkip())
	EndDo

	T02->(DbCloseArea())
	ElseIf(cSetor == "FIN")
	U_SASP123(GetMv("SA_MAILFIN"),"\WORKFLOW\ABERTURAPC.HTM",aDados, "Pedido de Compra Liberado com Adiantamento")
	ElseIf(cSetor == "COM")
	cQuery3 := " SELECT C1_USER FROM " + RetSqlName("SC1")
	cQuery3 += " WHERE C1_FILIAL = '"+cFil+"' AND C1_NUM = '"+cNumSc+"' AND D_E_L_E_T_ = '' "
	TcQuery cQuery3 New Alias T03

	If(!T03->(EOF()))
	cEmailSol := UsrRetMail(T03->C1_USER)
	U_SASP123(cEmailSol,"\WORKFLOW\ABERTURAPC.HTM",aDados, "Libera��o de Pedido de Compra")
	EndIf

	U_SASP123(SuperGetMv( "SA_MAILCOM",,, cFil ),"\WORKFLOW\ABERTURAPC.HTM",aDados,"Libera��o de Pedido de Compra")

	ElseIf(cSetor == "COMR")
	cQuery3 := " SELECT C1_USER FROM " + RetSqlName("SC1")
	cQuery3 += " WHERE C1_FILIAL = '"+cFil+"' AND C1_NUM = '"+cNumSc+"' AND D_E_L_E_T_ = '' "
	TcQuery cQuery3 New Alias T03
	
	AADD(aDados,aPost[1][2])

	If(!T03->(EOF()))
	cEmailSol := UsrRetMail(T03->C1_USER)
	U_SASP123(cEmailSol,"\WORKFLOW\REJEICAOPC.HTM",aDados, "Pedido de compras Rejeitado")
	EndIf

	U_SASP123(SuperGetMv( "SA_MAILCOM",,, cFil ),"\WORKFLOW\REJEICAOPC.HTM",aDados,"Pedido de compras Rejeitado")
	EndIf

	ELSEIF cTipo == "CP"

	cQuery := " SELECT C3_FORNECE, A2_NOME, C3_NUM, C3_EMISSAO, C3_FILIAL,C3_CC,CTT_DESC01,C3_QUANT,C3_TOTAL,C3_PRECO,C3_VALFRE,B1_DESC,C3_OBS,C3_PRODUTO,C3_COND,E4_DESCRI FROM "+RetSqlName("SC3")+" C3"
	cQuery += " INNER JOIN "+RetSqlName("SA2")+" A2 (NOLOCK) "
	cQuery += " ON A2_COD = C3_FORNECE "
	cQuery += " AND A2.D_E_L_E_T_ = '' "
	cQuery += " INNER JOIN "+RetSqlName("SB1")+" B1 (NOLOCK) "
	cQuery += " ON B1_COD = C3_PRODUTO "
	cQuery += " AND B1.D_E_L_E_T_ = '' "
	cQuery += " INNER JOIN "+RetSqlName("SE4")+" E4 (NOLOCK) "
	cQuery += " ON E4_CODIGO = C3_COND "
	cQuery += " AND E4.D_E_L_E_T_ = '' "
	cQuery += " INNER JOIN "+RetSqlName("CTT")+" CT (NOLOCK) "
	cQuery += " ON CTT_CUSTO = C3_CC "
	cQuery += " AND CT.D_E_L_E_T_ = '' "
	cQuery += " WHERE C3.D_E_L_E_T_ = '' "
	cQuery += " AND C3_FILIAL = '"+cFil+"' "
	cQuery += " AND C3_NUM = '"+cPed+"'"
	
	IF Select("T01") > 0
	T01->(DbCloseArea())
	ENDIF
	TcQuery cQuery New Alias T01

	If(!T01->(EOF()))
	cCond 	:= T01->C3_COND +" - "+T01->E4_DESCRI

	aadd(aDados, T01->C3_FORNECE)
	aadd(aDados, T01->A2_NOME)
	aadd(aDados, T01->C3_NUM)
	aadd(aDados, T01->C3_EMISSAO)
	aadd(aDados, T01->C3_FILIAL)
	cCC := alltrim(T01->C3_CC) + " - " + alltrim(T01->CTT_DESC01)
	While(!T01->(EOF()))
	nTotal 	+= T01->C3_TOTAL
	nTotFre += T01->C3_VALFRE
	nDesc 	+= 0

	aadd(aItens, {T01->B1_DESC, T01->C3_QUANT, T01->C3_PRECO, T01->C3_TOTAL, T01->C3_OBS, T01->C3_PRODUTO})

	T01->(DbSkip())
	EndDo
	aadd(aDados, aItens	)
	aadd(aDados, nTotal	)
	aadd(aDados, nTotFre)
	aadd(aDados, nDesc	)
	aadd(aDados, cCond	)
	aadd(aDados, ""		)
	aadd(aDados, ""		)
	aadd(aDados, cCC )
	//aadd(aDados, nAdiant)
	EndIf

	T01->(DbCloseArea())

	If(cSetor == "APROV")
	cQuery2 := " SELECT * FROM "+RetSqlName("SCR")
	cQuery2 += " WHERE CR_FILIAL = '"+cFil+"' AND CR_NUM = '"+cPed+"' AND CR_STATUS = '02'  AND CR_TIPO = '"+cTipo+"' AND D_E_L_E_T_ = '' "
	IF Select("T02") > 0
	T02->(DbCloseArea())
	ENDIF
	TCQuery cQuery2 New Alias T02
	
	If(!T02->(EOF()))
	lRet := .T.
	EndIf

	While(!T02->(EOF()))		
	aDados[11] := T02->CR_USER
	U_SASP123(UsrRetMail(T02->CR_USER),"\WORKFLOW\ABERTURAAEAPROV.HTM",aDados, "Aprova��o Contrato de Parceria")

	T02->(DbSkip())
	EndDo

	T02->(DbCloseArea())
	
	
	ELSEIF (cSetor == "COM")
	U_SASP123(SuperGetMv( "SA_MAILCOM",,, cFil ),"\WORKFLOW\ABERTURACP.HTM",aDados, "Contrato de Parceria Liberado")
	
	ElseIf(cSetor == "COMR")
	cQuery3 := " SELECT C3_USER FROM " + RetSqlName("SC1")
	cQuery3 += " WHERE C3_FILIAL = '"+cFil+"' AND C3_NUM = '"+cNumSc+"' AND D_E_L_E_T_ = '' "
	TcQuery cQuery3 New Alias T03
	
	AADD(aDados,aPost[1][2])

	If(!T03->(EOF()))
	cEmailSol := UsrRetMail(T03->C1_USER)
	U_SASP123(cEmailSol,"\WORKFLOW\REJEICAOCP.HTM",aDados, "Contrato Rejeitado")
	EndIf

	U_SASP123(SuperGetMv( "SA_MAILCOM",,, cFil ),"\WORKFLOW\REJEICAOCP.HTM",aDados,"Contrato Rejeitado")
	
	EndIf


	ENDIF
	Return lRet

	User Function APROVANIVEL(cFil, cPed, cNivel, cAprov, cUsu,cTipo)
	Local lRet		:= .F.
	Local nCount	:= 0
	Local cNxNivel	:= strzero(Val(cNivel)+1, 2)	

	cQuery := " SELECT CR_NUM, CR_TIPO, CR_TOTAL, CR_APROV, CR_STATUS, CR_USER "
	cQuery += " FROM "+RetSqlName("SCR") +" " 
	cQuery += " WHERE CR_FILIAL = '"+cFil+"' AND CR_NUM = '"+cPed+"' AND CR_NIVEL = '"+cNivel+"' "
	cQuery += " AND CR_STATUS = '02' AND CR_TIPO = '"+cTipo+"' AND D_E_L_E_T_ = '' "
	TcQuery cQuery New Alias T01

	While(!T01->(Eof()))
	lRet := .T.
	nCount++

	SCR->(dbSeek(cFil+cTipo + cPed + Space(TamSx3("CR_NUM")[1]-Len(cPed)) + T01->CR_USER))
	//MaAlcDoc({T01->CR_NUM,T01->CR_TIPO,T01->CR_TOTAL,cAprov,,POSICIONE("SC7",1,cFil+cPed, "C7_APROV"),,,,,},dDataBase,4)		
	Reclock("SCR",.F.)
	CR_STATUS	:= "05"
	CR_DATALIB	:= dDataBase
	CR_USERLIB	:= cUsu
	CR_APROV	:= cAprov
	CR_OBS		:= ""
	MsUnlock()	

	T01->(DbSkip())
	EndDo
	T01->(DbCloseArea())

	cQuery2 := " SELECT CR_NUM, CR_TIPO, CR_TOTAL, CR_APROV, CR_STATUS, CR_USER "
	cQuery2 += " FROM "+RetSqlName("SCR") +" " 
	cQuery2 += " WHERE CR_FILIAL = '"+cFil+"' AND CR_NUM = '"+cPed+"' AND CR_NIVEL = '"+cNxNivel+"' "
	cQuery2 += " AND CR_STATUS = '01' AND CR_TIPO = '"+cTipo+"' AND D_E_L_E_T_ = '' "
	TcQuery cQuery2 New Alias T02

	While(!T02->(Eof()))
	lRet := .T.
	nCount++

	SCR->(dbSeek(cFil+cTipo + cPed + Space(TamSx3("CR_NUM")[1]-Len(cPed)) + T02->CR_USER))				

	Reclock("SCR",.F.)
	CR_STATUS	:= "02"
	MsUnlock()	

	T02->(DbSkip())
	EndDo
	T02->(DbCloseArea())

	Return lRet