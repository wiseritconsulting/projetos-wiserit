#INCLUDE "rwmake.ch"
#INCLUDE "protheus.ch"
#INCLUDE "Topconn.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �SICOMA19  �Autor  �Claudinei Ferreira  � Data �  09/12/11   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao chamada pelo PE MT110TEL (Inclusao de campo no 	  ���
���          �cabecalho de SC) 			                                  ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico CNI (GAP126)                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

//	#########################################################################################
//	Projeto: PMC - Banco de horas - Melhoria campo comprador PC
//	Modulo : Compras
//	Fonte  : SICOMA19.PRW
//	---------+--------------------------+-----------------------------------------------------
//	Data     | Autor                    | Descricao
//	---------+--------------------------+-----------------------------------------------------
//	08/09/15 | Rafael Xavier MConsult   | Alterado para apresentar o nome do comprador em tela de SC.
//	---------+--------------------------+-----------------------------------------------------
//	#########################################################################################

User Function SASP124(oNewDialog,aPosGet,_nOpcx,_nReg)
Local _aArea := GetArea()
Local aItemsCot := {"N=N�o","S=Sim"}
Local aItemsAdi := {"N=N�o","S=Sim"}
public __dDataPag_ := IIF(_nOpcx==3 .or. Empty(SC1->C1_YDTPAG),STOD(""),SC1->C1_YDTPAG)
Public _nComboCot := IIF(_nOpcx==3 .or. Empty(SC1->C1_YTEMCOT),Space(TamSx3("C1_YTEMCOT")[1]),SC1->C1_YTEMCOT)
Public _nComboAdi := IIF(_nOpcx==3 .or. Empty(SC1->C1_YTEMADI),Space(TamSx3("C1_YTEMADI")[1]),SC1->C1_YTEMADI)
Public _nComboUrg := IIF(_nOpcx==3 .or. Empty(SC1->C1_YURGEN),Space(TamSx3("C1_YURGEN")[1]),SC1->C1_YTEMADI)
public _cCTT___ 	:= IIF(_nOpcx==3 .or. Empty(SC1->C1_CC),Space(TamSx3("C1_CC")[1]),SC1->C1_CC)
public _cFoPag_ 	:= IIF(_nOpcx==3 .or. Empty(SC1->C1_YFORMPA),Space(TamSx3("C1_YFORMPA")[1]),SC1->C1_YFORMPA)
public _cValDes_ 	:= IIF(_nOpcx==3 .or. Empty(SC1->C1_YADIANT),0,SC1->C1_YADIANT)
public _cPerDes_	:= IIF(_nOpcx==3 .or. Empty(SC1->C1_YPERCEN),0,SC1->C1_YPERCEN)
Static oDlg

//Campo de Tem Cota��o
@ 33,aPosGet[1,1]  SAY "Neces. Cotar?" OF oNewDialog PIXEL SIZE 40,9
@ 33, aPosGet[1,2] MSCOMBOBOX oComboCot VAR _nComboCot ITEMS aItemsCot OF oNewDialog ON CHANGE AtuCot(_nComboCot) PIXEL SIZE 030, 006

//Centro de custo
@ 33,aPosGet[1,1]+100 Say 'Centro de Custo' Pixel Size 40,9 Of oNewDialog
@ 33,aPosGet[1,2]+100 MSGET 	oCodMot  	VAR _cCTT___ 		SIZE 37,05 OF oDlg COLORS 0,16777215 F3 "CTT" PIXEL


//Campo de Tem Adiantamento
@ 33,aPosGet[1,1]+225  SAY Alltrim(RetTitle("C1_YTEMADI")) OF oNewDialog PIXEL SIZE 40,9
@ 33, aPosGet[1,2]+225 MSCOMBOBOX oComboAdi VAR _nComboAdi ITEMS aItemsAdi OF oNewDialog ON CHANGE AtuAdiant(_nComboAdi) PIXEL SIZE 030, 006


//Valor do Adiantamento
@ 33,aPosGet[1,1]+325 Say Alltrim(RetTitle("C1_YADIANT")) Pixel Size 40,9 Of oNewDialog
@ 33,aPosGet[1,2]+325 MSGET 	oCodMot  	VAR _cValDes_ 		SIZE 37,05 OF oDlg COLORS 0,16777215 PIXEL PICTURE PESQPICT("SC1","C1_YADIANT")

//Valor Percentual do Adiantamento,
@ 33,aPosGet[1,1]+475 Say Alltrim(RetTitle("C1_YPERCEN")) Pixel Size 40,9 Of oNewDialog
@ 33,aPosGet[1,2]+475 MSGET 	oCodMot  	VAR _cPerDes_		SIZE 37,05 OF oDlg COLORS 0,16777215 PIXEL PICTURE PESQPICT("SC1","C1_YPERCEN")

@ 45,aPosGet[1,1] Say 'Data.Pag' Pixel Size 40,9 Of oNewDialog
@ 45,aPosGet[1,2] MSGET 	oCodMot  	VAR __dDataPag_ 		SIZE 37,05 OF oDlg COLORS 0,16777215 PIXEL

@ 45,aPosGet[1,1]+100 Say 'Forma.Pag' Pixel Size 40,9 Of oNewDialog
@ 45,aPosGet[1,2]+100 MSGET 	oCodMot  	VAR _cFoPag_ 		SIZE 37,05 OF oDlg COLORS 0,16777215 F3 "SE4" PIXEL

@ 45,aPosGet[1,1]+475  SAY Alltrim(RetTitle("C1_YURGEN")) OF oNewDialog PIXEL SIZE 40,9
@ 45, aPosGet[1,2]+475 MSCOMBOBOX oComboAdi VAR _nComboUrg ITEMS aItemsAdi OF oNewDialog ON CHANGE AtuAdiant(_nComboUrg) PIXEL SIZE 030, 006


//@ 45,aPosGet[1,1]+475 BUTTON Avancar PROMPT "Lancar Despesas" SIZE 071, 012 OF oDlg PIXEL ACTION Despesas()

RestArea(_aArea)

Return()

Static function AtuCot(cItem)
	Local nPosCot := Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YTEMCOT"})
	
	for x := 1 to len(aCols)
		If( cItem == "N")
			aCols[x][nPosCot] := "N"
		else
			aCols[x][nPosCot] := "S"
		Endif
	next
Return

Static function AtuAdiant(cItem)
	Local nPosAdi := Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C1_YTEMADI"})
	
	for x := 1 to len(aCols)
		If( cItem == "N")
			aCols[x][nPosAdi] := "N"
		else
			aCols[x][nPosAdi] := "S"
		Endif
	next
Return

Static Function Despesas()

	Local aaCampos  	:= {"Verba","Valor","Data"} //Vari�vel contendo o campo edit�vel no Grid
	Local aBotoes	:= {}         //Vari�vel onde ser� incluido o bot�o para a legenda
	Local _nOpca 		:= 0 											//Opcao da confirmacao
	Local oGet1
	Local cGet1 := "Define variable value"
	Local cTipo
	Local bOk 			:= {|| _nOpca:=1,_lRetorno:=.T.,oDlg:End() } 	//botao de ok
	Local bCancel 		:= {|| _nOpca:=0,_lRetorno:=.T.,oDlg:End() } 					//botao de cancelamento
	Local oRadMenu1
	Local nRadMenu1 := 1
	Local aButtons := {}
	Static oDlg
	Private oLista                    //Declarando o objeto do browser
	Private aCabecalho  := {}         //Variavel que montar� o aHeader do grid
	         //Vari�vel que receber� os dados
   
    //Declarando os objetos de cores para usar na coluna de status do grid
	Private oVerde  	:= LoadBitmap( GetResources(), "BR_VERDE")
	Private oAzul  	:= LoadBitmap( GetResources(), "BR_AZUL")
	Private oVermelho	:= LoadBitmap( GetResources(), "BR_VERMELHO")
	Private oAmarelo	:= LoadBitmap( GetResources(), "BR_AMARELO")
		
	DEFINE MSDIALOG oDlg TITLE "TITULO" FROM 000, 000  TO 120, 300  PIXEL
        //chamar a fun��o que cria a estrutura do aHeader
	CriaCabec()

        //Monta o browser com inclus�o, remo��o e atualiza��o
	oLista := MsNewGetDados():New( 053, 078, 415, 775, GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "AllwaysTrue", aACampos,1, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aCabecalho, _aColsEx_)

        //Carregar os itens que ir�o compor o conteudo do grid
	Carregar(cTipo)

        //Alinho o grid para ocupar todo o meu formul�rio
	oLista:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT

        //Ao abrir a janela o cursor est� posicionado no meu objeto
	oLista:oBrowse:SetFocus()

        //Crio o menu que ir� aparece no bot�o A��es relacionadas
	aadd(aBotoes,{"NG_ICO_LEGENDA", {||Legenda()},"Legenda","Legenda"})
	 
        //EnchoiceBar(oDlg, {|| oDlg:End() }, {|| oDlg:End() },,aBotoes)

    //ACTIVATE MSDIALOG oDlg CENTERED
	
	
	//@ 130, 069 MSGET oGet1 VAR cGet1 SIZE 073, 010 OF oDlg COLORS 0, 16777215 PIXEL //Campo de pesquisa
	
	Activate MsDialog oDlg On Init (EnchoiceBar(oDlg,bOk,bCancel,,)) Centered VALID _lRetorno
	
	IF _nOpca == 1
	for nT := 1 to len(_aColsEx_)
	
	_aColsEx_[nT][3] := oLista:Acols[nT][3]
	
	next
	
	ENDIF

return


Static Function CriaCabec()
	Aadd(aCabecalho, {;
		"",;//X3Titulo()
	"IMAGEM",;  //X3_CAMPO
	"@BMP",;		//X3_PICTURE
	3,;			//X3_TAMANHO
	0,;			//X3_DECIMAL
	".F.",;			//X3_VALID
	"",;			//X3_USADO
	"C",;			//X3_TIPO
	"",; 			//X3_F3
	"V",;			//X3_CONTEXT
	"",;			//X3_CBOX
	"",;			//X3_RELACAO
	"",;			//X3_WHEN
	"V"})			//
	Aadd(aCabecalho, {;
		"Campo",;//X3Titulo()
	"Campo",;  //X3_CAMPO
	"@!",;		//X3_PICTURE
	8,;			//X3_TAMANHO
	0,;			//X3_DECIMAL
	"",;			//X3_VALID
	"",;			//X3_USADO
	"D",;			//X3_TIPO
	"",; 			//X3_F3
	"R",;			//X3_CONTEXT
	"",;			//X3_CBOX
	"",;			//X3_RELACAO
	""})			//X3_WHEN
	Aadd(aCabecalho, {;
		"Valor",;	//X3Titulo()
	"Valor",;  	//X3_CAMPO
	"@E 999,999,999.99",;		//X3_PICTURE
	12,;			//X3_TAMANHO
	2,;			//X3_DECIMAL
	"POSITIVO()",;			//X3_VALID
	"",;			//X3_USADO
	"N",;			//X3_TIPO
	"",;		//X3_F3
	"R",;			//X3_CONTEXT
	"",;			//X3_CBOX
	"",;			//X3_RELACAO
	""})			//X3_WHEN
Return

Static Function Carregar(cTipo)
	Local aProdutos := {}
	Local cQuery := ''
	Local cAux := 0
	Local cTipo
	Local cTipC := ''
	Local i := 0
			
			IF empty(_aColsEx_)
			aadd(aProdutos,{"Despesa: " ,0})
			aadd(aProdutos,{"Frete: " ,0})
			aadd(aProdutos,{"Desconto: " ,0})
			ELSE
			aadd(aProdutos,{"Despesa: " ,_aColsEx_[1][3]})
			aadd(aProdutos,{"Frete: " ,_aColsEx_[2][3]})
			aadd(aProdutos,{"Desconto: " ,_aColsEx_[3][3]})
			endif
	_aColsEx_ := {}				
	For i := 1 to len(aProdutos)
			aadd(_aColsEx_,{oVerde,aProdutos[i,1],aProdutos[i,2],.F.})
	Next

    //Setar array do aCols do Objeto.
	oLista:SetArray(_aColsEx_,.T.)

    //Atualizo as informa��es no grid
	oLista:Refresh()
Return

Static function Legenda()
	Local aLegenda := {}
	AADD(aLegenda,{"BR_AMARELO"     ,"   Afastado" })
	AADD(aLegenda,{"BR_AZUL"    	,"   Ferias" })
	AADD(aLegenda,{"BR_VERDE"    	,"   Situacao Normal" })
	AADD(aLegenda,{"BR_VERMELHO" 	,"   Demitido" })

	BrwLegenda("Legenda", "Legenda", aLegenda)
Return Nil
