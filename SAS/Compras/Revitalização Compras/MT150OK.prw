#INCLUDE "topconn.ch"

User Function MT150OK()
	Local nOpcx		:= Paramixb[1]
	Local lRet		:= .T.
	Local lMsg		:= .F.
	Local nPosJus	:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C8_YJUSTPR"})
	Local nPosPrc	:= Ascan(aHeader,{|x|Upper(Alltrim(x[2])) == "C8_PRECO"})
	Local cMsg := "Favor justificar o(s) seguinte(s) item(s): "
	
	for y := 1 to len(aCols)
		If(Empty(aCols[y][nPosJus]))			
			
			cQuery2 := "SELECT TOP 1 * FROM "+RetsqlName("SD1")+" NOLOCK "
			cQuery2 += "WHERE D1_COD = '"+Alltrim(aCols[n, aScan(aHeader,{|x| "C8_PRODUTO" $ x[2]})])+"'"
			cQuery2 += "AND D_E_L_E_T_ = '' ORDER BY D1_EMISSAO, D1_COD "
			TcQuery cQuery2 New Alias T02
			
			If !(EOF("T01")) .AND. T02->D1_VUNIT < aCols[y][nPosPrc]
				cMsg += aCols[y][1]+ " "
				lMsg := .T.
				lRet := .F.				
			EndIf
			T02->(DBCloseArea())
			
			
		Endif
	next
	
	If(lMsg)
		Aviso("Justificar Aumento do Pre�o",cMsg, {"OK"}, 1)
	EndIf
	
Return lRet

