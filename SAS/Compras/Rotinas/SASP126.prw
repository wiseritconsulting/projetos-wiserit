// Projeto:
// Modulo :
// Fonte  : SASP126.prw
// -----------+-------------------+---------------------------------------------------------
// Data       | Autor             | Descricao
// -----------+-------------------+---------------------------------------------------------
// 19/09/2016 | rogeriojacome     | Gerado com auxílio do Assistente de Código do TDS.
// -----------+-------------------+---------------------------------------------------------

#include "protheus.ch"
#include "vkey.ch"
#include"rwmake.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} SASP126
ENVIA O EMAIL PARA OS GRUPO DO COMPRAS QUANDO O PEDIDO TEM ALTERA��O.
@author    rogeriojacome
@version   11.3.2.201607211803
@since     19/09/2016
/*/
//------------------------------------------------------------------------------------------
user function SASP126(aAltera,aInfo,nMais,nAux)
	//-- variáveis -------------------------------------------------------------------------
	Local aItens 	:= aClone(aAltera)
	Local aForn		:= aClone(aInfo)
	Local cItens 	:= ''
	Local cItens2 	:= ''
	Local cCab		:= ''
	Local cHtml		:= ''
	local cHtml2	:= ''
	//Trabalho/apoio

	//-- procedimentos ---------------------------------------------------------------------
	oProcess 	:= TWFProcess():New("000004","Alteracao Pedido de Compra")
	oProcess 	:NewTask( "Fluxo de Compras", "\WORKFLOW\ALTPEDIDO.HTM")
	oHtml		:= oProcess:oHTML

	for nX := 1 to len(aForn)

		oHtml:ValByName( "C7_FORNECE"	,	aForn[nX][3])
		oHtml:ValByName( "A2_NOME"		,	aForn[nX][4])	
		oHtml:ValByName( "C7_NUM"		,	aForn[nX][2])	
		oHtml:ValByName( "DATAHORA"		,	DTOC(aForn[1][8]) + " - " + aForn[1][9])
		oHtml:ValByName( "C7_EMISSAO"	,	DTOC(STOD(aForn[1][7])))
		oHtml:ValByName( "C7_FILIAL"	,	aForn[nX][1])	
		oHtml:ValByName( "C7_CC"		,	aForn[nX][6] + " - " + aForn[nX][6])
		oHtml:ValByName( "C7_USER"		,	aForn[nX][10])

	next
	for x := 1 to len(aItens)

		cCab := "<tr bgcolor='#EE7F08' style='color:#ffffff;text-align:center'> <td colspan='3'><b>Item</b> " + " - " + aItens[x][1] + "</td> </tr>"
		cCab += "<tr bgcolor='#d9d9d9' style='text-align:center'> <td><b>Campo</b> </td> <td><b>Conteudo Anterior</b></td> <td><b>Conteudo Atual</b></td> </tr> "
		//aAdd( (oHtml:ValByName( "it.ItemP" 		)),	aItens[x][1] 		)
		//aAdd( (oHtml:ValByName( "it.Campo" 		)),	"Campo" 			)
		//aAdd( (oHtml:ValByName( "it.ContAnt" 	)),	"Conteudo Anterior"	)
		//aAdd( (oHtml:ValByName( "it.ContAtu" 	)), "Conteudo Atual"	)

		for xT := 1 to len(aItens[x][2])

			//aAdd( (oProcess:oHtml:ValByName( "it.CampoI" 	)),	GetSx3Cache(aItens[x][2][xT][1], "X3_Titulo") 	)
			//aAdd( (oProcess:oHtml:ValByName( "it.ContAntI" 	)),	aItens[x][2][xT][3] 							)
			//aAdd( (oProcess:oHtml:ValByName( "it.ContAtuI" 	)),	aItens[x][2][xT][2] 							)
			cItens += "<tr bgcolor='#d9d9d9' style='text-align:center' > <td>" + GetSx3Cache(aItens[x][2][xT][1], "X3_Titulo") + "</td> <td> " + cvaltochar(aItens[x][2][xT][3]) + "</td> <td> " + cvaltochar(aItens[x][2][xT][2]) + "</td> </tr>"

		next
		cHtml += cCab + cItens
		cItens := ''
	next

	//aAdd( (oProcess:oHtml:ValByName( "it.CampoI" 	)),	cHtml 	)
	oHtml:ValByName( "CampoI"		,	cHtml)//Imprimi os itens alterados

	IF nMais > 0 //Imprimi o cabecalho dos itens adicionados
		cItens2 += "<tr style='height:25px;text-align:center;'> <td bgcolor='#EE7F08' style='color:#00004d' colspan='2'><b>Novos Itens Incluidos</b></td></tr>"
		for nY := 1 to nMais

			cItens2 += "<tr bgcolor='#d9d9d9' style='text-align:center' > <td><b>" + "Item " + "</b></td> <td> " + STRZERO(nAux + nY,4,0) + "</td> </tr>"

		next
	ENDIF
	oHtml:ValByName( "Inclusao"		,	cItens2)


	oProcess:cSubject 	:= "SAS - Alteracao do Pedido de Compra"
	oProcess:cTo 		:= GETMV("SA_MALCOM")
	oProcess:cFromAddr	:= GETMV("SA_MAILC")
	oProcess:cFromName	:= "SAS - COMPRAS"
	oProcess:Start()
	//-- encerramento ----------------------------------------------------------------------

return
//-------------------------------------------------------------------------------------------
// Gerado pelo assistente de código do TDS tds_version em 19/09/2016 as 14:51:30
//-- fim de arquivo--------------------------------------------------------------------------

