#include "tbiconn.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "protheus.ch"
#include "topconn.ch"

//  ETIQUETA VERTICAL

User Function TEMPETQ()
	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local oPrinter
	Local cLocal          := "c:\TEMP\"
	Local cCodINt25 := "050101 000999 009999 99A"					
	Local oFont1 	:= TFont():New( "Calibri",,9,,.F.,,,,,  .F. )
	Local oFont2 	:= TFont():New( "Calibri",,9,,.T.,,,,,  .F. )	
	Local oFont3 	:= TFont():New( "Calibri",,11,,.T.,,,,,  .F. )
	Local oFont4 	:= TFont():New( "Calibri",,12,,.T.,,,,,  .F. )
	Local cFilePrint := ""
	Local nLin   := 20
	Local nTmLin := 10
	Local nSalto := 20
	Local xArea := GetArea()
	Local nQuant := 1
	Local cQuery := ""
	
		PREPARE ENVIRONMENT EMPRESA "01" FILIAL "01"


	IF Select("QTDE") > 0
		QTDE->(dbCloseArea())
	Endif

	cQuery := "SELECT SUM(VOLUMES) TVOL FROM ETIQMANUAL WHERE PEDIDO='99AZXP' AND FILIAL='010105'"
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"QTDE",.F.,.T.)
	dbSelectArea("QTDE")
	qTot := QTDE->TVOL
	
	IF Select("ETIQ") > 0
		ETIQ->(dbCloseArea())
	Endif

	cQuery := "SELECT FILIAL, PEDIDO, VOLUMES, SERIE, C5_FILIAL, C5_NUM, C5_CLIENTE, C5_LOJACLI, A1_NOME, A1_END, A1_EST, A1_MUN, A4_COD, A4_NOME FROM ETIQMANUAL INNER JOIN SC5010 ON C5_NUM = PEDIDO AND C5_FILIAL=FILIAL INNER JOIN SA1010 ON A1_COD=C5_CLIENTE AND A1_LOJA=C5_LOJACLI  INNER JOIN SA4010 ON A4_COD='38    ' WHERE C5_NUM='99AZXP' AND C5_FILIAL='010105'"
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"ETIQ",.F.,.T.)


	dbSelectArea("ETIQ")
	
	cCodINt25 := ETIQ->C5_FILIAL + ETIQ->C5_NUM

	oPrinter := FWMSPrinter():New('etique_000000.PD_', IMP_SPOOL, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetPortrait()
	
	While !Eof()
					
			FOR X=1 TO ETIQ->VOLUMES
			
			oPrinter:StartPage()
			
			// CODIGO DE BARRAS
			
			oPrinter:FWMSBAR("CODE128" /*cTypeBar*/,28/*nRow*/ ,1/*nCol*/ ,cCodINt25  /*cCode*/,oPrinter/*oPrint*/,.F./*lCheck*/,/*Color*/,.T./*lHorz*/,0.04 /*nWidth*/,2/*nHeigth*/,.F./*lBanner*/,"Arial"/*cFont*/,NIL/*cMode*/,.F./*lPrint*/,3/*nPFWidth*/,3/*nPFHeigth*/,.F./*lCmtr2Pix*/)
						

			// REMETENTE 

			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, "REMETENTE",  oFont2 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, replicate("-",50),  oFont1 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, "SAS EDUCA��O SA",  oFont1 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, "RUA NENZINHA PARENTE 580. JANGURUSU",  oFont1 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, "BR-116, KM 11 - FORTALEZA - CE",  oFont1 )

			// DESTINATARIO
			
			nLin := nLin + nTmLin + nSalto
			oPrinter:Say ( nLin, 20, "DESTINATARIO",  oFont4 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, replicate("-",50),  oFont4 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, ETIQ->C5_CLIENTE + "/" + ETIQ->C5_LOJACLI +"-"+ ETIQ->A1_NOME,  oFont4 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, ETIQ->A1_END ,  oFont4 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, ETIQ->A1_EST +"-"+ ETIQ->A1_MUN ,  oFont4 )		
		
			// TRANSPORTADORA			
			nLin := nLin + nTmLin + nSalto
			oPrinter:Say ( nLin, 20, "TRANSPORTADORA",  oFont2 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, replicate("-",50),  oFont2 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, ETIQ->A4_COD +" - "+ ETIQ->A4_NOME,  oFont1 )
						
			// ENVIO
			
			nLin := nLin + nTmLin + nSalto
			oPrinter:Say ( nLin, 20, "PEDIDO: " + ETIQ->C5_FILIAL + ETIQ->C5_NUM,  oFont3 )
			nLin := nLin + nTmLin + 10
			oPrinter:Say ( nLin, 20, "ENVIO: " + "01",  oFont3 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, "SERIE: " + ETIQ->SERIE,  oFont3 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, "VOL. DA S�RIE:" + ALLTRIM(STR(ETIQ->VOLUMES)),  oFont3 )
			nLin := nLin + nTmLin
			oPrinter:Say ( nLin, 20, "TOTAL DE VOLUMES: " + ALLTRIM(STR(qTot)),  oFont3 )
			nLin := nLin + nTmLin	
			oPrinter:Say ( nLin, 20, "VOLUME: " + cValTochar(X) + "/" + alltrim(str(ETIQ->VOLUMES))  ,  oFont3 )
			
			
			nQuant++
			nLin := nLin + nTmLin + nSalto			
			nLin := nLin + nTmLin

			
			nLin := nLin + nTmLin + 85
			oPrinter:Say ( nLin, 20, cCodINt25,  oFont2 )
		    
		    oPrinter:EndPage()

		    nLin := 20
		    
		next x
		
		ETIQ->(dbSkip())	
	
	Enddo
	

	oPrinter:Preview()
	
Return
