#include 'protheus.ch'
#include 'parmtype.ch'
#include "dbtree.ch"
#include "topconn.ch"

/*/{Protheus.doc} SaldIni
Saldo Inicial Automatico
@author Weskley Silva
@since 20/10/2016
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User function SaldIni()

	Local cAreaAnterior := getarea()
	Local lMsErroAuto  := .F.
	
	
	cQuery := " SELECT B1_COD,B1_FILIAL FROM  " + RETSQLNAME('SB1') + " SB1 " 
	cQuery += " WHERE NOT EXISTS (SELECT * FROM " + RETSQLNAME('SB9') + " SB9 "
	cQuery += " WHERE B9_COD = B1_COD AND  SB1.D_E_L_E_T_ = '' AND SB9.D_E_L_E_T_ = '') "
	cQuery += " AND SB1.D_E_L_E_T_ = '' AND SB1.B1_COD LIKE '%99%' "
	
	
	TCQuery cQuery new alias T01
	
	WHILE !T01->(EOF())
	dbSelectArea("SM0")
	dbGOTOP() 
	
	ProcRegua(0)
	
    WHILE !SM0->(EOF())
	
					cFilanterior := cfilant

					Begin Transaction
						PARAMIXB1 := {}
						aadd(PARAMIXB1,{"B9_FILIAL",xfilial("SB9",SM0->M0_CODFIL),})
						aadd(PARAMIXB1,{"B9_COD",alltrim(T01->B1_COD),})
						aadd(PARAMIXB1,{"B9_LOCAL", GETMV('SA_LOCFAT'),})
						aadd(PARAMIXB1,{"B9_QINI",0,})
						cfilant := SM0->M0_CODFIL
						MSExecAuto({|x,y| mata220(x,y)},PARAMIXB1,3)
						cfilant := cFilanterior
						If lMsErroAuto
							mostraerro()
						EndIf
					End Transaction

					SM0->(DbSkip())
		ENDDO	
		T01->(DbSkip())
	ENDDO
	T01->(DBCLOSEAREA())
	
	MsgInfo("Saldos Iniciais executado","SAS")
	RestArea(cAreaAnterior)
return