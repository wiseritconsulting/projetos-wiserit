#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "Topconn.ch"
#INCLUDE "Rwmake.ch"


User Function SASU001()  
 
	Local aRotAdic :={} 

	aadd(aRotAdic,{ "Imprimir","U_Z0Proc", 0 , 6 })
	
	AxCadastro("ZS0", "Relatorio", /*"U_DelOk()"*/, /*"U_COK()"*/, aRotAdic, , , , , , , /*aButtons*/, , )    
	
Return(.T.)       
                
User Function Z0Proc()
	Processa( {|| U_GeraRel() }, "Aguarde...", "Processando ...",.F.)
Return
	
	
User Function GeraRel() 

	Local aRet    := {} 
	Local aRet1   := {} 
	Local Campos  := {}
	Local nRegAtu := 0 
	Local x       := 0 
	Local cQuery  := ZS0->ZS0_SQL
	
	if !(__cUserid $ ZS0->ZS0_ACESSO)
		Alert("Usuario Sem Acesso!!!")
		return
	endif
	
	cQuery := ChangeQuery(cQuery) 
	TCQUERY cQuery NEW ALIAS "_TRB" 
	

	
	dbSelectArea("_TRB") 
	aRet1   := Array(Fcount()) 
	ProcRegua(RecCount()*2)
	nRegAtu := 1 
	y=0
	While !Eof() 
	     y++
		 For x:=1 To Fcount() 
		 
		     if y = 1
		     	aadd(Campos,FIELD(x))
		     endif
		     
		      aRet1[x] := FieldGet(x) 
		 Next 
		 Aadd(aRet,aclone(aRet1)) 
		 IncProc()
		  
		 dbSkip() 
		 nRegAtu += 1 
	Enddo 
	
	MontaXML(Campos,aRet)
	
	dbSelectArea("_TRB") 
	_TRB->(DbCloseArea()) 
	
	

Return() 


Static Function MontaXML(colunas, dados)

	Local oExcel := FWMSEXCEL():New()
	
	oExcel:AddworkSheet("Relato")
	oExcel:AddTable ("Relato","Dados")
	
	for x := 1 to len(colunas)
		oExcel:AddColumn("Relato","Dados",colunas[x],1,1)
	next x
	
	for x := 1 to len(dados)
		IncProc()
		oExcel:AddRow("Relato","Dados",dados[x])
	next x
	  
	oExcel:Activate()
	oExcel:GetXMLFile("c:\temp\"+ALLTRIM(ZS0->ZS0_CODIGO)+".xml")

Return