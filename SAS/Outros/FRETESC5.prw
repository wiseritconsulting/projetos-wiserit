#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "Topconn.ch"
#INCLUDE "Rwmake.ch"

/*
Rotina para gerar os pedidos complementares das notas faturadas sem valor de frete 
Weskley Silva
13/12/2016
*/

User function FRETESC5()


	 Processa( {|| U_SC5FRET1() }, "Aguarde...", "Gerando Pedidos de Venda...",.F.)	
	
return

User function SC5FRET1() 

	aCabec := {}
	aLinha := {} 
	aItens := {}
	aCabec2 := {}
	aLinha2 := {} 
	aItens2 := {} 
	cQuery := ""
	
	if Select("FRT") > 0
		FRT->(dbCloseArea())
	Endif
	
	cQuery := " SELECT * FROM FRETES WHERE INTEGRADO IS NULL AND Filial <> '' "
	TcQuery cQuery new Alias FRT
	
	
	While !FRT->(Eof())
	  IncProc()	
	  begin transaction
	  
	// ------------------------------------------------
	// CABEC DO PEDIDO DE VENDA
	// ------------------------------------------------
			cString := FRT->FreteFiscal
			cValor := StrTran(cString,".","")
			cValor2 := StrTran(cValor,",",".")
			
			
			aCabec := {}
			aadd(aCabec,{"C5_FILIAL" 	,FRT->Filial	              ,Nil})			
			aadd(aCabec,{"C5_TIPO" 		,"C"	 	                  ,Nil}) 
			aadd(aCabec,{"C5_CLIENTE"	,ALLTRIM(FRT->CodigoCliente)  ,Nil})     
			aadd(aCabec,{"C5_LOJACLI"	,ALLTRIM(FRT->Loja)           ,Nil}) 
			aadd(aCabec,{"C5_TIPOCLI"	,"F"		                  ,Nil})
			aadd(aCabec,{"C5_TABELA"	,"   "		                  ,Nil})      
			aadd(aCabec,{"C5_CONDPAG"	,"001"		                  ,Nil}) 
			aadd(aCabec,{"C5_TPEDAV"	,"02"		                  ,Nil})
			aadd(aCabec,{"C5_YCONTRA"	,FRT->Contrato                ,Nil}) 
			aadd(aCabec,{"C5_TPFRETE"	, " "	                      ,Nil}) 
			aadd(aCabec,{"C5_DESCONT"   ,0                            ,Nil})
			aadd(aCabec,{"C5_YFILIAL"   ,FRT->Filial                  ,Nil})
			aadd(aCabec,{"C5_TPFRETE"   ,"S"                          ,Nil})
			aadd(aCabec,{"C5_MENNOTA"   ,"Nota fiscal complementar de valor referente a NF "+ FRT->NF01 +" "   ,Nil})
						
			aCabec2:=ACLONE(aCabec)
			
			
			// ------------------------------------------------
	        // ITENS DO PEDIDO DE VENDA
	        // ------------------------------------------------
			
			aLinha := {}    
			AADD(aLinha,{"C6_ITEM"		,"01"			            ,Nil})    
			AADD(aLinha,{"C6_PRODUTO"	,'99110002'	 	            ,Nil})    
			AADD(aLinha,{"C6_QTDVEN"	,0					        ,Nil})
			AADD(aLinha,{"C6_PRCVEN"	,val(ALLTRIM(cValor2))      ,Nil})    
			AADD(aLinha,{"C6_PRUNIT"	,val(ALLTRIM(cValor2))      ,Nil})
			AADD(aLinha,{"C6_VALOR"		,val(ALLTRIM(cValor2))      ,Nil})
			AADD(aLinha,{"C6_TES"		,FRT->TES02  			    ,Nil})
			AADD(aLinha,{"C6_YCONTRA"	,FRT->Contrato		        ,Nil})
			AADD(aLinha,{"C6_LOCAL"		,"01"	                    ,Nil})
			AADD(aLinha,{"C6_NFORI"		,FRT->NF01  	            ,Nil})
			AADD(aLinha,{"C6_SERIORI"	,"1"	                    ,Nil})
			AADD(aLinha,{"C6_FILIAL"	,FRT->Filial 	            ,Nil})
			AADD(aItens,aLinha) 
			AADD(aItens2,aLinha)
			
			lMsErroAuto := .F. 
			
			cFilAnt := FRT->Filial
						
			bkpfil := cFilAnt
			MsExecAuto({|x,y,z| MATA410(x,y,z) }, aCabec,aItens,3)	
			cFilAnt	:= bkpfil
				
			If lMsErroAuto 
				AutoGrLog("Erro ao incluir registro!")
				AutoGrLog("Opera��o cancelada!")
				MostraErro()
				aErro := GetAutoGRLog()  
				cErro:= "Erro no MSEXECAUTO do Pedido de Venda. Verifique o Log no Server."
				For nX := 1 To Len(aErro)
					cErro += aErro[nX] + Chr(13)+Chr(10)
		  		Next nX
		  	else 
		  	TCSQLEXEC("UPDATE FRETES SET INTEGRADO = 'S' WHERE Filial = '"+FRT->Filial+"' AND Contrato = '"+FRT->Contrato+"' AND CodigoCliente = '"+FRT->CodigoCliente+"' AND Loja = '"+FRT->Loja+"'" )	
			endif
			
			aItens := {}
			
			end transaction
			FRT->(dbSkip())
		enddo	
		FRT->(dbCloseArea())
return