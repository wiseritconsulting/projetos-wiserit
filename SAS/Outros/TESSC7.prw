#include 'protheus.ch'
#include 'parmtype.ch'

user function TESSC7()

	aCabPC := {}

	IncProc("Criando pedido de compra para a Matriz")
		cPedido4	:= CriaVar('C7_NUM', .T.)
		aadd(aCabPC,{"C7_FILIAL"	,"050101"})
		aadd(aCabPC,{"C7_NUM"		,"056919"})
		aadd(aCabPC,{"C7_EMISSAO"	,dDataBase})
		aadd(aCabPC,{"C7_FORNECE"	,"300081"})
		aadd(aCabPC,{"C7_LOJA"		,"05"})
		aadd(aCabPC,{"C7_COND"		,"001"})		//TODO VERIFICAR CONDI��O PAG
		aadd(aCabPC,{"C7_CONTATO"	,"AUTO"})
		aadd(aCabPC,{"C7_FILENT"	,"050101"})
	
				aLinha := {}
				aItensPC := {}

			aadd(aLinha,{"C7_PRODUTO"	,"99400243 "	,Nil})
			aadd(aLinha,{"C7_QUANT"		,1	          ,Nil})
			aadd(aLinha,{"C7_PRECO"		,10		      ,Nil})
			aadd(aLinha,{"C7_YCODKIT", "10101120103"  ,NIL})
			aadd(aLinha,{"C7_YPAIKIT", "101011201", NIL})

			aadd(aLinha,{"C7_YTES"		,"016"						,Nil})
			aadd(aLinha,{"C7_TES"		,"400"						    ,Nil})
			aadd(aLinha,{"C7_YCONTRA"	,"000256"	 					,Nil})
			aadd(aLinha,{"C7_YMEDICA"	,"000042"					    ,Nil})
			aadd(aLinha,{"C7_LOCAL"		,"12"		                    ,Nil})
			aadd(aLinha,{"C7_YCLAORC"	,"E"		                    ,Nil})	//E-Estoque
			aadd(aLinha,{"C7_CC"		,"1025"		                    ,Nil})	//1025-Almoxarifado
			aadd(aItensPC,aLinha)
		lMsErroAuto	:= .F.
		
		
		MsExecAuto({|x,y,z| MATA120(1,x,y,z)}, aCabPC,aItensPC,3)

			If lMsErroAuto
				AutoGrLog("Erro ao gerar pedido de compra!")
				MostraErro()
				Return .F.
			endif 	

return