// #########################################################################################
// Projeto: 
// Modulo : SIGAFAT
// Fonte  : LeArq
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Weskley Silva           | Descricao Rotina de importa��o de dados
// ---------+-------------------+-----------------------------------------------------------
// 10/08/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+----------------------------------------------------------

#include 'protheus.ch'
#include 'parmtype.ch'

user function LerArqSB1()

	Private cArq := cGetFile("Arquivos|*.TXT|","Selecione o diretorio",,,,;
	GETF_LOCALHARD+GETF_LOCALFLOPPY+GETF_NETWORKDRIVE)

	// fOpen - Abertura de arquivo
	nHdl := fOpen(cArq)

	if nHdl == -1 
		MsgAlert("O arquivo: ' "+cArq+" ' nao pode ser aberto! Verifique os parametros do mesmo")
		return
	endif	

	if MsgYesNo("Deseja importar os dados do arquivo ' " + cArq + " '?")
		Processa({|| RunArq()} , "Lendo Arquivo... ")
	endif	

return

// Processa a leitura do arquivo

Static Function RunArq()

	Private lMSErroAuto := .F.

	ProcRegua(0) // Controla a barra de progressao 

	FT_FUSE(cArq)
	FT_FGOTOP()
	nLin := 1
	While !FT_FEOF(cArq)

		IncProc("Lendo linha do arquivo!")
		cLinha := FT_FREADLN(cArq)


		if nLin == 1
			if !cLinha $ "CODIGO;DESC;GRUPO;SUBGRUPO;TIPO;UNIDADE;ARMAZEM;NCM;INSS;ORIGEM;IRRF;PIS;CSLL;CONFINS;RETOPER;GARANT;CATEGORIA;CLASSE;SERIE;ENVIO;VOLUME;TIPO"
				MsgAlert("Layout errado! ", "Verifique o arquivo ")
				FT_FUSE()
				return
			else 
				FT_FSKIP()
				nLin++ 
				Loop
			endif
		endif		

		aDados := separa(cLinha,";")
		aCadPro := {{"B1_COD",    aDados[1],NIL},;
		{"B1_DESC",   aDados[2],NIL},;
		{"B1_GRUPO",  aDados[3],NIL},;
		{"B1_YSUBGR", aDados[4],NIL},;
		{"B1_TIPO",   aDados[5],NIL},;
		{"B1_UM",     aDados[6],NIL},;
		{"B1_LOCPAD", aDados[7],NIL},;
		{"B1_POSIPI", aDados[8],NIL},;
		{"B1_INSS",   aDados[9],Nil},;
		{"B1_ORIGEM", aDados[10],Nil},;
		{"B1_IRRF",   aDados[11],Nil},;
		{"B1_PIS",    aDados[12],Nil},;
		{"B1_CSLL",   aDados[13],Nil},;
		{"B1_CONFINS",aDados[14],Nil},;
		{"B1_RETOPER",aDados[15],Nil},;
		{"B1_GARANT", aDados[16],Nil},;
		{"B1_YCATE",  aDados[17],Nil},;
		{"B1_YCLASEN",aDados[18],Nil},;
		{"B1_YSERIE", aDados[19],Nil},;
		{"B1_YVOL",   aDados[20],Nil},;
		{"B1_YVOLUME",aDados[21],Nil},;
		{"B1_YTIPO",  aDados[22],Nil}}

		MsExecAuto({|x,y| Mata010(x,y)}, aCadPro, 3)
		if lMsErroAuto 
			MostraErro()
		endif
		FT_FSKIP() // Pula para o proximo registro			
	Enddo	

	FT_FUSE() // Fecha o arquivo
	FCLOSE(cArq)
return