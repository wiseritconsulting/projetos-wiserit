#Include 'Protheus.ch'
#INCLUDE 'FWMBROWSE.CH'
#INCLUDE 'FWMVCDEF.CH'
#include 'TopConn.CH'

User Function SASP090(A)
	Local aArea := getArea()
	Local oDlgNF
	Local oBrwNF
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Local cTextHead	:= "QLabel { color: rgb(39,82,102); font-size: 12px; font-weight: bold;  } "
	Private aObjTela	:= {}
	Private aCampos		:= {}
	
	Private _nNumber 
	Private _cTmp 

	Private _cSeqImp 
	Private _cChvCTE 
	Private _cTomador 
	Private _cRemetente 
	Private _cDestinatario
	Private _cTransportadora 
	
	Private _nSkip
	Private _nLin 	  
	
	Private _nColTit1 
	Private _nColDad1
	
	Private _nColTit2 
	Private _nColDad2 
	
	Private _yp_cNrImp := A
		  
	oDlgNF := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Dados NFE',,,,nOr(WS_VISIBLE,WS_POPUP),,,,,.T.,,,,)

	@00,00 MSPANEL oBarNFE SIZE 50,50 of oDlgNF
	oBarNFE:Align := CONTROL_ALIGN_TOP
	
	//-------------------VARIAVEIS---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	_nNumber := 1
	_cTmp := StrZero(_nNumber,5,0)
	
	_cSeqImp 		:= ""
	_cChvCTE 		:= ""
	_cTomador 		:= ""
	_cRemetente 	:= ""
	_cDestinatario 	:= ""
	
	_cCTE 			:= ""
	_cValFrete 		:= ""
	_cPeso 			:= ""
	_cVolume 		:= ""
	_cEmissao 		:= ""
	
	_nSkip	  := 007
	_nLin 	  := 001
	
	_nColTit1 := 010
	_nColDad1 := 080
	
	_nColTit2 := 290
	_nColDad2 := 330
	
	AtualTela()
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColTit1, {||"Sequ�ncia Importa��o: "}	,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColTit1, {||"Chave CTE: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColTit1, {||"Transportador: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColTit1, {||"Remetente: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColTit1, {||"Destinat�rio: "}			,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 5*_nSkip,_nColTit1, {||"Tomador: "}			,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColDad1, {||_cSeqImp}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColDad1, {||_cChvCTE}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColDad1, {||_cTransportador}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColDad1, {||_cRemetente}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)	
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColDad1, {||_cDestinatario}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 5*_nSkip,_nColDad1, {||_cTomador}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColTit2, {||"CTE: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColTit2, {||"Valor Frete: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColTit2, {||"Peso Total: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColTit2, {||"Volume: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColTit2, {||"Emiss�o: "}			,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColDad2, {||_cCTE}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColDad2, {||_cValFrete}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColDad2, {||_cPeso}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColDad2, {||_cVolume}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)	
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColDad2, {||_cEmissao}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	//Instaciamento
	oBrwNF := FWMBrowse():New()
	oBrwNF:SetMenuDef("SASP090") 
	
	//Disabilita a tela de detalhes
	oBrwNF:disableDetails()
	
	//tabela que ser� utilizadaadmin
	oBrwNF:SetAlias( "ZB9" )
	If Type(_yp_cNrImp) <> 'U'
		SET FILTER TO (ZB9_NRIMP = _yp_cNrImp)
	endIf 
	
	//Titulo
	oBrwNF:SetDescription( "Itens NFE" )
	
	//ativa
	oBrwNF:Activate(oDlgNF)
	
	oBrwNF:Refresh()
	
	oDlgNF:Activate(,,,.T.)
Return


Static Function MenuDef()
	Private aRotinaNF := {}
		
	ADD OPTION aRotinaNF	TITLE 'Ocorr�ncia'	 	ACTION 'U_SASP090A()' OPERATION 10 ACCESS 0
	ADD OPTION aRotinaNF	TITLE 'Abrir. NFE'	 	ACTION 'U_SASP090B()' OPERATION 2 ACCESS 0
Return aRotinaNF

Static Function AtualTela()
	Local cAlias	:= GetNextAlias()
	Local aArea		:= GetArea()
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	dbGoTop()
	dbSeek(xFilial("ZB8")+_yp_cNrImp)
	
	_cSeqImp 		:= ZB8->ZB8_NRIMP
	_cChvCTE 		:= ZB8->ZB8_CTE

	//Remetente
	if !(Empty(AllTrim(ZB8->ZB8_CDREM)))
		_cRemetente 	:= AllTrim(ZB8->ZB8_CDREM)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDREM,"SA2->A2_NREDUZ")
		_cAux := POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDREM,"SA2->A2_NREDUZ")
		If Empty(AllTrim(_cAux)) //DEVOLU��O: Se n�o for FORNECEDOR, ent�o � CLIENTE
			_cRemetente 	:= AllTrim(ZB8->ZB8_CDREM)	+ " - " + POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDREM,"SA1->A1_NREDUZ")
		EndIf
	Else
		_cRemetente 		:= ""
	EndIf
	
	//Transportador
	if !(Empty(AllTrim(ZB8->ZB8_EMISDF)))
		_cTransportador	:= AllTrim(ZB8->ZB8_EMISDF)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_EMISDF,"SA2->A2_NREDUZ")
	Else
		_cTransportador 		:= ""
	EndIf
	
	//Destinat�rio
	if !(Empty(AllTrim(ZB8->ZB8_CDDEST)))
		_cDestinatario 	:= AllTrim(ZB8->ZB8_CDDEST)	+ " - " + POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDDEST,"SA1->A1_NREDUZ")
		_cAux := POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDDEST,"SA1->A1_NREDUZ")
		If Empty(AllTrim(_cAux))//DEVOLU��O: Se n�o for CLIENTE, ent�o � FORNECEDOR
			_cDestinatario 	:= AllTrim(ZB8->ZB8_CDDEST)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDDEST,"SA2->A2_NREDUZ")
		EndIf
	Else
		_cDestinatario 		:= ""
	EndIf
	
	//Tomador
	if !(Empty(AllTrim(ZB8->ZB8_TOMADO)))
		_cTomador 		:= AllTrim(ZB8->ZB8_TOMADO)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_TOMADO,"SA2->A2_NREDUZ")
		_cAux := POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_TOMADO,"SA2->A2_NREDUZ")
		If Empty(AllTrim(_cAux))//DEVOLU��O: Se n�o for FORNECEDOR, ent�o � CLIENTE
			_cTomador 		:= AllTrim(ZB8->ZB8_TOMADO)	+ " - " + POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_TOMADO,"SA1->A1_NREDUZ")
		EndIf
	Else
		_cTomador 		:= ""
	EndIf
	
	_cCTE 			:= AllTrim(ZB8->ZB8_NRDF)+"/"+ZB8->ZB8_SERDF
	_cValFrete 		:= AllTrim(Transform(ZB8->ZB8_VLDF	,PesqPict("ZB8"	, "ZB8_VLDF")))
	_cPeso 			:= AllTrim(Transform(ZB8->ZB8_PREAL	,PesqPict("ZB8"	, "ZB8_PREAL")))
	_cVolume 		:= AllTrim(Transform(ZB8->ZB8_VOLUM,PesqPict("ZB8"	, "ZB8_VOLUM")))
	_cEmissao 		:= DtoC(ZB8->ZB8_DTEMIS)

	ZB8->(dbCloseArea())
	RestArea(aArea)
	ProcessMessage()
Return

Static Function CssbtnImg(cImg,cCor)
	Local cRet	:= " QPushButton{ background-image: url(rpo:"+cImg+")"+;
	" ;border-style:solid"+;
	" ;border-width:5px"+;
	" ;background-repeat: none; margin: 1px "+;
	" ;background-color: "+cCor+""+;
	" ;border-radius: 6px"+;
	" ;font-weight: bold;font-size: 12px"+;
	" ;color: rgb(255,255,255) "+;
	"}"+;
	"QPushButton:hover { "+;
	" ;background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop:0 "+cCor+", stop:0.5 "+cCor+", stop:1 white)"+;
	"}"
Return cRet

User function SASP090A()
	//MsgInfo("Implementar Fun��o Ocorr�ncias","[SASP090] - Gest�o de Fretes")
	U_SASP091(_yp_cNrImp)
Return

User function SASP090B()
	Local xArea 	:= getArea()
	Local cQrySF2	:= ""        
	Local cFilOld	:= cFilAnt
	Local cDevol	:= ""
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	If dbSeek(xFilial("ZB8") + ZB9->ZB9_NRIMP)
		cDevol := ZB8->ZB8_DEVOL
	EndIf
	ZB8->(dbCloseArea())
	
	
	If cDevol <> 'S'
		cQrySF2	:= "  SELECT F2_FILIAL as Filial, R_E_C_N_O_ as SF2Recno "
		cQrySF2	+= "  FROM "+RetSqlName("SF2")+" SF2 " "
		cQrySF2	+= "  WHERE SF2.D_E_L_E_T_ = '' "
		cQrySF2	+= "  AND SF2.F2_CHVNFE = '"+AllTrim(ZB9->ZB9_DANFE)+"' "					
		
		TCQUERY cQrySF2 NEW ALIAS QSF2	
		
		dbSelectArea("QSF2")
		QSF2->(DBGOTOP())	
		
		If !QSF2->(EOF())
			cFilAnt	:= QSF2->Filial
				dbSelectArea("SF2")   
				SF2->(dbSetOrder(10)) // Filail + Chave
				SF2->(dbGotop())
				SF2->(dbSeek(QSF2->Filial+AllTrim(ZB9->ZB9_DANFE)))
				*----------------------------------
				Mc090Visual("SF2",QSF2->SF2Recno,2)	
				*----------------------------------
			cFilAnt	:= cFilOld
		Else
			Alert("N�o existe registro de nota fiscal com a chave: " + CRLF + ZB9->ZB9_DANFE)
		EndIf
	Else
		cQrySF1	:= "  SELECT F1_FILIAL as Filial, R_E_C_N_O_ as SF1Recno "
		cQrySF1	+= "  FROM "+RetSqlName("SF1")+" SF1 " "
		cQrySF1	+= "  WHERE SF1.D_E_L_E_T_ = '' "
		cQrySF1	+= "  AND SF1.F1_CHVNFE = '"+AllTrim(ZB9->ZB9_DANFE)+"' "					
		
		TCQUERY cQrySF1 NEW ALIAS QSF1	
		
		dbSelectArea("QSF1")
		QSF1->(DBGOTOP())	
		
		If !QSF1->(EOF())
			cFilAnt	:= QSF1->Filial
				dbSelectArea("SF1")   
				SF1->(dbSetOrder(8)) // Filail + Chave
				SF1->(dbGotop())
				SF1->(dbSeek(QSF1->Filial+AllTrim(ZB9->ZB9_DANFE)))
				*----------------------------------
				Mc090Visual("SF1",QSF1->SF1Recno,2)	
				*----------------------------------
			cFilAnt	:= cFilOld
		Else
			Alert("N�o existe registro de nota fiscal com a chave: " + CRLF + ZB9->ZB9_DANFE)
		EndIf

	EndIf
	
	QSF2->(dbCloseArea())	
	RestArea(xArea)
Return

Static Function getFilDoc(cSeqImp)
	Local cRet := ""
	Local cSql 
	dbSelectArea("ZB8")
	dbSetOrder(1)
	If dbSeek(xFilial("ZB8")+cSeqImp)
		cRet := ZB8->ZB8_FILDOC
	EndIf
return cRet	