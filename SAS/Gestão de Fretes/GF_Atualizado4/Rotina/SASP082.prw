#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP082

Cadastro de Configurações Ocorrências

@type function
@author Sam Barros
@since 07/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SASP082()
	AxCadastro("ZB2", "Cadastro de Configurações Ocorrências", ".T.", ".T.")
Return