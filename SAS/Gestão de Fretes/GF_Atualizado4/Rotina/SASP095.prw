#include "protheus.ch"
#INCLUDE "topconn.ch"

User Function SASP095()
	Local _astru:={}
	Local aRet := {}
	Local aParamBox := {}
	
	Private _afields:={}
	Private cCodUse	:= RETCODUSR()
	Private oMark
	Private arotina := {}
	Private cCadastro
	Private cMark:=GetMark()
	private _cAlias
	Private _RecTRB
	
	Private _aCdUsWF
	
	aAdd(aParamBox,{1,"Transportadora",Space(15),"","","SA2","",0,.F.}) // Tipo caractere
	
	aAdd(aParamBox,{1,"Emiss�o De"  ,Ctod(Space(8)),"","","","",50,.F.}) // Tipo data
	aAdd(aParamBox,{1,"Emiss�o At�"  ,dDataBase,"","","","",50,.F.}) // Tipo data 
	
	aAdd(aParamBox,{1,"Remetente De",Space(15),"","","SA2","",0,.F.}) // Tipo caractere
	aAdd(aParamBox,{1,"Remetente At�",Replicate('Z',(15)),"","","SA2","",0,.F.}) // Tipo caractere
	
	aAdd(aParamBox,{1,"Destinat�rio De",Space(15),"","","SA2","",0,.F.}) // Tipo caractere
	aAdd(aParamBox,{1,"Destinat�rio At�",Replicate('Z',(15)),"","","SA2","",0,.F.}) // Tipo caractere
	
	aAdd(aParamBox,{11,"N�mero do CTE","",".T.",".T.",.F.}) //Tipo Memo

	AADD(aRotina,{"Gerar Fatura" 		,"u_makeFatura()" ,0,4})

	If ParamBox(aParamBox,"Par�metros Fatura",@aRet)
	
		cCadastro := "Gera��o de Fatura CTE"
	
		//Estrutura da tabela temporaria
		AADD(_astru,{"ZZ_OK"    	,"C",02,0})
		AADD(_astru,{"ZZ_FILIAL" 	,"C",06,0})
		AADD(_astru,{"ZZ_NRIMP"	 	,"C",16,0})
		AADD(_astru,{"ZZ_MODAL"	 	,"C",02,0})
		AADD(_astru,{"ZZ_TITULO" 	,"C",09,0})
		AADD(_astru,{"ZZ_NRDF"	 	,"C",16,0})
		AADD(_astru,{"ZZ_SERDF"	 	,"C",05,0})
		AADD(_astru,{"ZZ_CTE"	 	,"C",44,0})
		AADD(_astru,{"ZZ_DTEMIS" 	,"C",10,0})
		AADD(_astru,{"ZZ_TOMADO" 	,"C",06,0})
		AADD(_astru,{"ZZ_LOJA" 		,"C",02,0})
		AADD(_astru,{"ZZ_VLDF" 		,"N",12,2})
		AADD(_astru,{"ZZ_EMISDF" 	,"C",08,0})
		AADD(_astru,{"ZZ_DESEMI" 	,"C",30,0})
		
		//----------------------------------------------------------
		// cria a tabela tempor�ria
		//----------------------------------------------------------
		_carq:="T_"+Criatrab(,.F.)
		MsCreate(_carq,_astru,"DBFCDX")
		
		//----------------------------------------------------------
		//atribui a tabela tempor�ria ao alias TRB
		//----------------------------------------------------------
		if Select("TRB")>0
			dbSelectArea("TRB")
			TRB->(dbCLoseArea())
		EndIf
		dbUseArea(.T.,"DBFCDX",_cARq,"TRB",.T.,.F.)
	
		//----------------------------------------------------------
		// selecionando registros. 					            
		//----------------------------------------------------------
		cQuery:=" SELECT ZB8_FILDOC, "
		cQuery+=" ZB8_FILTOM, "
		cQuery+=" ZB8_NRIMP, "
		cQuery+=" ZB8_NRDF, "
		cQuery+=" ZB8_SERDF, "
		cQuery+=" ZB8_MODAL, "
		cQuery+=" ZB8_CTE, "
		cQuery+=" ZB8_VLDF, "
		cQuery+=" ZB8_TITULO, "
		cQuery+=" ZB8_EMISDF, "
		cQuery+=" ZB8_DTEMIS "
		cQuery+=" FROM " + RetSqlName("ZB8") + " ZB8 (nolock)"
		cQuery+=" WHERE 1=1 "
		cQuery+=" AND ZB8.D_E_L_E_T_ = '' "
		cQuery+=" AND ZB8.ZB8_FILIAL = '"+xFilial("ZB8")+"' "
		cQuery+=" AND ZB8.ZB8_STATUS = 'DE' "
		cQuery+=" AND ZB8.ZB8_FATURA = '" + Space(TamSx3("ZB8_FATURA")[1]) + "' "
		//------------------------------------------------------------------------------------
		If !empty(aRet[1])
			cQuery+=" AND SUBSTRING(ZB8.ZB8_EMISDF,1,6) = '" + aRet[1] + "' "
		EndIf
		cQuery+=" AND ZB8.ZB8_DTEMIS >= '" + DtoS(aRet[2]) + "' "
		cQuery+=" AND ZB8.ZB8_DTEMIS <= '" + DtoS(aRet[3]) + "' "
		cQuery+=" AND ZB8.ZB8_CDREM >= '" + aRet[4] + "' "
		cQuery+=" AND ZB8.ZB8_CDREM <= '" + aRet[5] + "' "
		cQuery+=" AND ZB8.ZB8_CDDEST >= '" + aRet[6] + "' "
		cQuery+=" AND ZB8.ZB8_CDDEST <= '" + aRet[7] + "' "
		If !empty(aRet[8])
			cQuery+=" AND ZB8.ZB8_NRDF IN ('" + StrTran(aRet[8],";","','") + "') "
		EndIf
		//------------------------------------------------------------------------------------
	
		IF SELECT("TX")>0
			TX->(dbCloseArea())
		Endif
	
		TCQUERY cQuery NEW ALIAS TX
	
		dbSelectArea("TX")
		TX->(dbGotop())
	
		While TX->(!Eof())
	
			RecLock("TRB",.T.)
				TRB->ZZ_FILIAL		:=  TX->ZB8_FILTOM//TX->ZB8_FILDOC
				TRB->ZZ_NRIMP 		:=  TX->ZB8_NRIMP
				TRB->ZZ_MODAL		:= 	TX->ZB8_MODAL
				TRB->ZZ_TITULO 		:=  TX->ZB8_TITULO
				TRB->ZZ_NRDF		:=  TX->ZB8_NRDF
				TRB->ZZ_SERDF		:=  TX->ZB8_SERDF
				TRB->ZZ_CTE     	:= 	TX->ZB8_CTE
				TRB->ZZ_DTEMIS		:= 	DtoC(StoD(TX->ZB8_DTEMIS))
				TRB->ZZ_TOMADO     	:= 	SUBS(TX->ZB8_EMISDF,1,6)
				TRB->ZZ_LOJA     	:= 	SUBS(TX->ZB8_EMISDF,7,2)
				TRB->ZZ_VLDF 		:=  TX->ZB8_VLDF
				TRB->ZZ_EMISDF 		:=  TX->ZB8_EMISDF
				TRB->ZZ_DESEMI 		:=  POSICIONE("SA2",1,xFilial("SA2")+TX->ZB8_EMISDF, "SA2->A2_NREDUZ")
			TRB->(MSUNLOCK())
	
			TX->(dbSkip())
		EndDo
	
		TX->(dbclosearea())
	
		//----------------------------------------------------------
		//Colunas do browse [verificar necessidade de novos campos do browser]
		//----------------------------------------------------------
		AADD(_afields,{"ZZ_OK"    		,"",""            		})
		AADD(_afields,{"ZZ_FILIAL" 		,"","Filial"	   		})
		AADD(_afields,{"ZZ_NRIMP" 		,"","Seq Import"   		})
		AADD(_afields,{"ZZ_MODAL" 		,"","Modal"	   			})
		AADD(_afields,{"ZZ_TITULO" 		,"","Titulo"	   		})
		AADD(_afields,{"ZZ_NRDF" 		,"","CTE"				})
		AADD(_afields,{"ZZ_SERDF"		,"","Serie"	  			})
		AADD(_afields,{"ZZ_CTE"			,"","Chave CTE"		  	})
		AADD(_afields,{"ZZ_DTEMIS"		,"","Data Emissao" 		})
		AADD(_afields,{"ZZ_TOMADO"		,"","Fornecedor" 		})
		AADD(_afields,{"ZZ_LOJA"		,"","Loja" 				})
		AADD(_afields,{"ZZ_EMISDF"		,"","Transportadora" 	})
		AADD(_afields,{"ZZ_DESEMI"		,"","Desc Transp" 		})
	
	
		DbSelectArea("TRB")
		DbGotop()
	
		MarkBrow( 'TRB', 'ZZ_OK',,_afields,.F., cMark,'u_MkAllFatura()',,,,'u_MkFatura()',{|| u_MkAllFatura()},,,,,,,.F.)
	
		TRB->(dbCloseArea())
		//----------------------------------------------------------
		//apaga a tabela tempor�rio
		//----------------------------------------------------------
		MsErase(_carq+GetDBExtension(),,"DBFCDX")
	Else
		Return
	EndIf

Return()

User Function MkAllFatura()
	Local oMark := GetMarkBrow()

	dbSelectArea('TRB')
	TRB->(dbGotop())

	While !TRB->(Eof())
		u_MkFatura()
		TRB->(dbSkip())
	EndDo
	MarkBRefresh( )
	oMark:oBrowse:Gotop()

Return

User Function MkFatura()
	If IsMark( 'ZZ_OK', cMark )

		RecLock( 'TRB', .F. )
		Replace ZZ_OK With Space(2)
		TRB->(MsUnLock())
	Else
		RecLock( 'TRB', .F. )
		Replace ZZ_OK With cMark
		TRB->(MsUnLock())
	EndIf

Return

User Function makeFatura()
	Local cMsg	:= ""
	Local nTotal:= 0
	Local nCont	:= 0

	dbSelectArea("TRB")
	TRB->(dbGotop())
	While !TRB->(Eof())
		IF !Empty(Alltrim(TRB->ZZ_OK))
			nTotal	+= TRB->ZZ_VLDF
			nCont ++
		ENDIF
		TRB->(dbSkip())
	ENDDO
	
	If nCont > 1
		cMsg	:= "CONFIRMA GERA��O DE ("+Alltrim(Str(nCont))+") FATURAS ? "+CHR(13)+CHR(10)
	Else
		cMsg	:= "CONFIRMA GERA��O DE ("+Alltrim(Str(nCont))+") FATURA ? "+CHR(13)+CHR(10)
	EndIf
	cMsg	+= "TOTAL >>> R$ "+Alltrim(Transform(nTotal,"@E 999,999,999,999.99"))+"<<<"+CHR(13)+CHR(10)
	cMsg	+= "("+Alltrim(Extenso(nTotal))+")"
	
	If MsgYesNo(cMsg,"Gerar fatura")
		Processa( {|| u_createFatura(nCont) }, "Gerando fatura..." )
	EndIf
	
	MarkBRefresh()
	oMark:oBrowse:Gotop()
	oMark:oBrowse:nat := 1
	oMark:oBrowse:Refresh(.T.)
Return

User Function createFatura(nQtdReg)
	Local aArray:={}
	Local aTit :={}
	Local dData
	Local cFornece
	Local cLoja
	Local cSeq := U_objSomaSeq(GetMv("SA_SEQGFFT"),6)
	Local cDoc := "" //'FGF' + cSeq
	
	Local cTES 		:= GetMV("SA_TESCTE")
	Local cCP 		:= GetMV("SA_CPCTE")
	Local cNatureza := GetMV("SA_NATCTE")
	Local cCusto	:= GetMV("SA_CCCTE")
	
	Local cCPFat	:= getMV("SA_CPFATGF")
	
	Local cFilReal := ""
	
	Local aRet := {}
	Local aParamBox := {}
	
	Private lMsErroAuto := .F.
	
	PutMV("SA_SEQGFFT", cSeq)
	
	Begin Transaction
	
	dbSelectArea("TRB")
	TRB->(dbGotop())
	While !TRB->(Eof())
		IF !Empty(Alltrim(TRB->ZZ_OK))
			AADD(aTit, {SUBS(TRB->ZZ_SERDF,1,3), SUBS(TRB->ZZ_TITULO,1,9), " ", "NF ", .F.})
			
			cFilReal := POSICIONE("ZB8",1,xFilial("ZB8")+TRB->ZZ_NRIMP, "ZB8_FILTOM")
			
			dData 	 := TRB->ZZ_DTEMIS
			cFornece := TRB->ZZ_TOMADO
			cLoja 	 := TRB->ZZ_LOJA
		ENDIF
		TRB->(dbSkip())
	ENDDO
	
	//TRB->(dbCloseArea())
	
	cFilBkp := cFilAnt
	cFilAnt := cFilReal
	
	//cDoc := GetSXENum("SE2", "E2_NUM")
	aAdd(aParamBox,{1,"Num Fatura",Space(TamSX3("E2_NUM")[1]),"","","","",0,.F.}) // Tipo caractere
	_bInfNumFat := .F.
	While !_bInfNumFat
		If ParamBox(aParamBox,"Par�metros Fatura",@aRet)
			cDoc := aRet[1]//StrZero(Val(aRet[1]),TamSX3("E2_NUM")[1],0)
			_bInfNumFat := .T.
		EndIf
	EndDo
	
	aArray := { "FAT", "CTE", cDoc, cNatureza, CtoD("01/01/2010"), dDataBase, cFornece, cLoja, cFornece, cLoja, cCPFat, 01, aTit , 0, 0}
	
	MsExecAuto( { |x,y| FINA290(x,y)},3,aArray,)
	
	cFilAnt := cFilBkp
	If lMsErroAuto
		MostraErro()
	Else
		dbSelectArea("TRB")
		TRB->(dbGotop())
		While !TRB->(Eof())
			IF !Empty(Alltrim(TRB->ZZ_OK))
				dbSelectArea("ZB8")
				dbSetOrder(1)
				If dbSeek(xFilial("ZB8") + TRB->ZZ_NRIMP)
					_cNrImp := ZB8->ZB8_NRIMP
					RecLock("ZB8", .F.)
						ZB8->ZB8_FATURA := cDoc
						ZB8->ZB8_STATUS := "FG"
					MsUnLock()
				EndIf
				ZB8->(dbCloseArea())
				
				RecLock("TRB", .F.)
					TRB->(dbDelete())
				MsUnLock()
			ENDIF
			
			TRB->(dbSkip())
		ENDDO
		
		//TRB->(dbCloseArea())
	
		MsgInfo("Fatura " + AllTrim(cDoc) + " gerada com sucesso")
		
		U_emailGF(_cNrImp,'F',cDoc)
	Endif

	End Transaction	

	MarkBRefresh()
	oMark:oBrowse:Gotop()
	oMark:oBrowse:nat := 1
	oMark:oBrowse:Refresh()

Return