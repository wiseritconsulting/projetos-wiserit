#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP081

Cadastro do UF x Grupo de Aprova��o

@type function
@table ZB1
@author Sam Barros
@since 30/06/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SASP081()
	//---------------------------------------------------------//
	//Declara��o de vari�veis
	//---------------------------------------------------------//
	Private cPerg   	:= "1"
	Private cCadastro 	:= "Cadastro do UF x Grupo de Aprova��o"
	Private cDelFunc 	:= ".T." // Validacao para a exclusao. Pode-se utilizar ExecBlock
	Private cString 	:= "ZB1"
	Private cFiltro		:= ""
	Private	aIndexZB1	:= {}
	Private aIndex		:= {}
	Private cCodUse		:= RetCodUsr()
	Private bFiltraBrw 	:= { || FilBrowse( "ZB1" , @aIndex , @cFiltro ) } //Determina a Expressao do Filtro
	Private aRotina 	:= {}
	Private aRotAprov 	:= {}
	Private aRotStts 	:= {}
	Private aCores		:= { }
		
	AADD(aRotina, {"Pesquisar"		,"AxPesqui"		,0,1})
	AADD(aRotina, {"Visualizar"		,"AxVisual"		,0,2})
	//AADD(aRotina, {"Incluir"		,"AxInclui"		,0,3})
	AADD(aRotina, {"Cria UF"		,"U_povoaZB1"	,0,3})
	AADD(aRotina, {"Alterar"		,"AxAltera"		,0,4})
	//AADD(aRotina, {"Excluir"		,"AxDeleta"		,0,5})
	AADD(aRotina, {"Legenda"      	,"u_legZB1()"	,0,6})
	
	AADD(aCores, { 'ZB1->ZB1_TIPO ==   "1"'	    , 'BR_AZUL'			})
	AADD(aCores, { 'ZB1->ZB1_TIPO ==   "2"'    	, 'BR_LARANJA'		})
	
	dbSelectArea("ZB1")
	dbSetOrder(1)

	cPerg   := "1"

	Pergunte(cPerg,.F.)

	dbSelectArea(cString)
	mBrowse( 6,1,22,75,cString,,,,,6,aCores)

	Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros

	ZB1->(dbCloseArea())
Return


//-------------------------------------------------------------------
/*/{Protheus.doc} LegZB1

Legenda da tabela ZB1
[AZUL] 		- Grupo de Aprova��o por val
[LARANJA]	- Grupo de Aprova��o R$/Kg  

@type function
@author Sam Barros
@since 06/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
USER Function LegZB1()
	Local aLegenda := 	{;
							{"BR_AZUL"			, "Grupo de Aprova��o por valor"},;
							{"BR_LARANJA" 		, "Grupo de Aprova��o R$/Kg"};
						}
	BrwLegenda(cCadastro, "Legenda", aLegenda)
Return .T.

User function povoaZB1()
	Local cSql := ""
	
	dbSelectArea("SX5")
	dbSetOrder(1)
	dbSeek(xFilial("SX5")+'12')
	
	While !(SX5->(EOF())) .AND. SX5->X5_TABELA == '12'
		RecLock("ZB1", .T.)
			ZB1->ZB1_FILIAL := xFilial("ZB1")
			ZB1->ZB1_UF := SX5->X5_CHAVE
			ZB1->ZB1_DESCUF := SX5->X5_DESCRI 
			ZB1->ZB1_TIPO := '1'
			ZB1->ZB1_GRUPO := ""
		ZB1->(MsUnlock())
		
		RecLock("ZB1", .T.)
			ZB1->ZB1_FILIAL := xFilial("ZB1")
			ZB1->ZB1_UF := SX5->X5_CHAVE
			ZB1->ZB1_DESCUF := SX5->X5_DESCRI 
			ZB1->ZB1_TIPO := '2'
			ZB1->ZB1_GRUPO := ""
		ZB1->(MsUnlock())
		
		SX5->(dbSkip())
	EndDO
	
	SX5->(dbCloseArea())
	
Return