#Include 'Protheus.ch'

User Function SASP097()
	Private oMark 
	Private cMark := GetMark()
	Private aRotina := {}
	Private cCadastro
	Private cCodUsr := RetCodUsr()
	Private cFiltro := "ZBE_USER = '" + cCodUsr + "' AND ZBE_STATUS = 'B' AND ZBE_ANULA = 'S'"
	
	cCadastro := "Aprova��o Anula��o"
	
	AADD(aRotina,{"Aprovar" 		,"u_AprZBEAnu()" ,0,4})
	AADD(aRotina,{"Rejeitar" 		,"u_ReprZBEAnu()" 	,0,4})
	
	MarkBrow( 'ZBE', 'ZBE_OK',,,.F., cMark,'u_MkAllSA()',,,,'u_MkSA()',{|| u_MkAllSA()},cFiltro,,,,,,.F.)
Return

User Function MkAllSA()
	Local oMark := GetMarkBrow()

	dbSelectArea('ZBE')
	ZBE->(dbGotop())

	While !ZBE->(Eof())
		u_MkSA()
		ZBE->(dbSkip())
	End
	MarkBRefresh( )
	oMark:oBrowse:Gotop()
Return

User Function MkSA()
	If IsMark( 'ZBE_OK', cMark )
		RecLock( 'ZBE', .F. )
		Replace ZBE_OK With Space(2)
		ZBE->(MsUnLock())
	Else
		RecLock( 'ZBE', .F. )
		Replace ZBE_OK With cMark
		ZBE->(MsUnLock())
	EndIf
Return

User Function AprZBEAnu()
	If MsgYesNo("Deseja aprovar a anula��o dos CTEs selecionados","Aprova��o")
		makeVisto('A')
	EndIf
Return

User Function ReprZBEAnu()
	If MsgYesNo("Deseja reprovar a anula��o dos CTEs selecionados","Reprova��o")
		makeVisto('R')
	EndIf
Return

Static Function makeVisto(cTipo)
	Local xArea := getArea()
	Local cSql := ""
	Local nQnt := 0
	Local cImport := ""
	Local cSubstantivo 	:= IIF(cTipo=='A', "Aprova��o"	, "Rejei��o")
	Local cInfinitivo	:= IIF(cTipo=='A', "Aprovar"	, "Rejeitar")
	
	cSql += " 	SELECT X.ZBE_NRIMP IMP, X.ZBE_OCORR OCO "
	cSql += " 	FROM  " + RetSqlName("ZBE") + " X "
	cSql += " 	WHERE X.D_E_L_E_T_ = '' "
	cSql += " 	AND X.ZBE_OK = '" + cMark + "' "
	cSql += " 	AND X.ZBE_STATUS = 'B' "
	cSql += " 	AND X.ZBE_ANULA = 'S' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		dbSelectArea("ZBE")
		dbSetOrder(1)
		dbGoTop()
		if dbSeek(xFilial("ZBE") + QRY->IMP + QRY->OCO)
			cImport := QRY->IMP
			nQnt++
			While !(ZBE->(EOF())) .AND. ZBE->ZBE_NRIMP == QRY->IMP .AND. ZBE->ZBE_OCORR == QRY->OCO
				RecLock("ZBE", .F.)
					ZBE->ZBE_APUSER := cCodUsr
					ZBE->ZBE_STATUS := cTipo
					ZBE->ZBE_DTAPRO := dDataBase
					ZBE->ZBE_HRAPRO := time()
				MsUnLock()
				
				ZBE->(dbSkip())
			EndDo
			
			if cTipo == 'A'
				altStaCTE(cImport, 'AA')
			Else
				altStaCTE(cImport, 'BL')
			EndIf
		EndIf
		QRY->(dbSkip())
	EndDo
	QRY->(dbCloseArea())
	
	If nQnt > 0
		if nQnt == 1
			MsgInfo(cSubstantivo + " de " + Alltrim(Str(nQnt)) + " anula��o realizada com sucesso")
		Else
			MsgInfo(cSubstantivo + " de " + Alltrim(Str(nQnt)) + " anula��es realizadas com sucesso")
		EndIf
	Else
		MsgAlert("N�o foram selecionadas anula��es para " + cInfinitivo)
	EndIf
	
	RestArea(xArea)
Return

Static Function altStaCTE(cSeqImp, cStatus)
	DbSelectArea("ZB8")
	dbSetOrder(1)
	If dbSeek(xFilial("ZB8") + cSeqImp)
		RecLock("ZB8", .F.)
			ZB8->ZB8_STATUS := cStatus
		MsUnLock()
	EndIf
Return