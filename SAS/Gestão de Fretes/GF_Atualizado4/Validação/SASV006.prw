#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASV006

Valida��o do campo ZB2_FORM no cadastro modal.
Verifica se j� existe formula cadastrada para 
 
Usado no validador do campo. 
@author Sam Barros
@since 03/08/2016
@version 1.0
@return bRet, Valida��o de exist�ncia de registro
@example
U_SASV006()
/*/
//-------------------------------------------------------------------
User Function SASV006()
	Local bRet := .T.
	Local aArea := getArea()
	
	dbSelectArea("ZB2")
	dbSetOrder(3)
	If dbSeek(xFilial("ZB2")+M->ZB2_FORM)
		MsgAlert("Esta f�rmula j� existe no cadastro de ocorr�ncias!")
		bRet := .F.
	Else
		dbSelectArea("SM4")
		dbSetOrder(1)
		If !(dbSeek(xFilial("SM4")+M->ZB2_FORM))
			MsgAlert("A f�rmula informada n�o existe!")
			bRet := .F.
		EndIf
		
		SM4->(dbCloseArea())
	EndIf
	
	ZB2->(dbCLoseArea())
	
	RestArea(aArea)
Return bRet