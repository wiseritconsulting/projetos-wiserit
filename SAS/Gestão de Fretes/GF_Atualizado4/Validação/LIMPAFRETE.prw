#Include 'Protheus.ch'

User Function LIMPAFRETE()
	Local cSql := ""
	Local aRet := {}
	Local aParamBox := {}
	Local aTab := {"ZB0", "ZB1", "ZB2", "ZB3", "ZB4", "ZB5", "ZB6", "ZB7", "ZB8", "ZB9", "ZBA", "ZBB", "ZBC", "ZBD", "ZBE", "ZBF"}
	Local nTam := len(aTab)
	Local cDesc := ""
	
	ProcRegua(nTam)
	
	For i := 1 to nTam
		cDesc := aTab[i] + ' - '
		dbSelectArea("Sx2")     
		dbSetOrder(1)
		MsSeek(aTab[i])
		
		cDesc += SX2->X2_NOME
		
		IncProc("Deletando: " + cDesc)
		
		aAdd(aParamBox,{5,cDesc,.T.,150,"",.F.})
	Next i 
	
	SX2->(dbCloseArea())
	
	If ParamBox(aParamBox,"Par�metros...",@aRet)
		For i:=1 To Len(aRet)
			//MsgInfo(aRet[i],"Op��o escolhida")
			If aRet[i]
				cSql := " DELETE FROM  " + RETSQLNAME(aTab[i])
				TcSqlExec(cSql)
			EndIf
		Next
	Endif


	MsgInfo("Dele��o Finalizada")
	/*
	cSql := " DELETE FROM  " + RETSQLNAME("ZB8")
	TcSqlExec(cSql)
	cSql := " DELETE FROM  " + RETSQLNAME("ZB9")
	TcSqlExec(cSql)
	cSql := " DELETE FROM  " + RETSQLNAME("ZBC")
	TcSqlExec(cSql)
	cSql := " DELETE FROM  " + RETSQLNAME("ZBE")
	TcSqlExec(cSql)
	cSql := " DELETE FROM  " + RETSQLNAME("ZBF")
	TcSqlExec(cSql)
	*/
Return
