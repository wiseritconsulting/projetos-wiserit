#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASV005

Valida��o do campo ZBA_CORLEG no cadastro modal.
Verifica se j� existe cor de legenda cadastrado para o modal.
 
Usado no validador do campo. 
@author Sam Barros
@since 03/08/2016
@version 1.0
@return bRet, Valida��o de exist�ncia de registro
@example
U_SASV005()
/*/
//-------------------------------------------------------------------
User Function SASV005()
	Local bRet := .T.
	Local aArea := getArea()
	
	dbSelectArea("ZBA")
	dbSetOrder(4)
	If dbSeek(xFilial("ZBA")+M->ZBA_CORLEG)
		MsgAlert("J� existe cor de legenda relacionado a este c�digo!")
		bRet := .F.
	EndIf
	
	ZBA->(dbCloseArea())
	
	RestArea(aArea)
Return bRet