#include 'protheus.ch'
#include 'parmtype.ch'
#include 'tbiconn.ch'
#include 'topconn.ch'

user function SASJ001()
	Local cSetSql := ""
	Local _aEstimativa := {}
	 
	cSetSql := qryDefault()
	cSetSql := ChangeQuery(cSetSql) // Converte o texto num Sql
	
	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY_FRETE", .F., .T.)
	    
	While QRY_FRETE->(!EOF())
		RecLock("ZBG", .T.)
			ZBG->ZBG_FILIAL 	:= QRY_FRETE->ZB8_FILDOC
			ZBG->ZBG_NUMNF 		:= AllTrim(QRY_FRETE->F2_DOC)
			ZBG->ZBG_SERNF 		:= AllTrim(QRY_FRETE->F2_SERIE)
			ZBG->ZBG_CHVNF 		:= AllTrim(QRY_FRETE->ZB9_DANFE)
			ZBG->ZBG_NUMCTE 	:= AllTrim(QRY_FRETE->ZB8_NRDF)
			ZBG->ZBG_SERCTE 	:= AllTrim(QRY_FRETE->ZB8_SERDF)
			ZBG->ZBG_CHVCTE 	:= AllTrim(QRY_FRETE->ZB8_CTE)
			ZBG->ZBG_TPPED 		:= getTipoPedido(AllTrim(QRY_FRETE->ZB9_DANFE), 'COD')
			ZBG->ZBG_DSTPPE 	:= getTipoPedido(AllTrim(QRY_FRETE->ZB9_DANFE), 'DESC')
			ZBG->ZBG_DTEMIS 	:= QRY_FRETE->ZB8_EMISSAO
			ZBG->ZBG_CDTRAN 	:= QRY_FRETE->ZB8_EMISDF
			ZBG->ZBG_DSTRAN 	:= POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_EMISDF, "A2_NOME")
			ZBG->ZBG_TOMADO 	:= QRY_FRETE->ZB8_TOMADO
			ZBG->ZBG_DESTOM 	:= POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_TOMADO, "A2_NOME")
			ZBG->ZBG_UFTOM 		:= POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_TOMADO, "A2_EST")
			ZBG->ZBG_CDMUTO 	:= POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_TOMADO, "A2_COD_MUN")
			ZBG->ZBG_MUNTOM 	:= POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_TOMADO, "A2_MUN")
			ZBG->ZBG_REMETE 	:= QRY_FRETE->ZB8_CDREM
			ZBG->ZBG_DESREM 	:= POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_CDREM, "A2_NOME")
			ZBG->ZBG_UFREM 		:= POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_CDREM, "A2_EST")
			ZBG->ZBG_CDMURE 	:= POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_CDREM, "A2_COD_MUN")
			ZBG->ZBG_MUNREM 	:= POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_CDREM, "A2_MUN")
			ZBG->ZBG_DESTIN 	:= QRY_FRETE->ZB8_CDDEST
			ZBG->ZBG_DESDES 	:= POSICIONE("SA1", 1, xFilial("SA1") + QRY_FRETE->ZB8_CDDEST, "A1_NOME")
			ZBG->ZBG_UFDES 		:= POSICIONE("SA1", 1, xFilial("SA1") + QRY_FRETE->ZB8_CDDEST, "A1_EST")
			ZBG->ZBG_CDMUDE 	:= POSICIONE("SA1", 1, xFilial("SA1") + QRY_FRETE->ZB8_CDDEST, "A1_COD_MUN")
			ZBG->ZBG_MUNDES 	:= POSICIONE("SA1", 1, xFilial("SA1") + QRY_FRETE->ZB8_CDDEST, "A1_MUN")
			ZBG->ZBG_VOLUME 	:= QRY_FRETE->ZB8_VOLUM
			ZBG->ZBG_VLCARGA	:= QRY_FRETE->ZB8_VLCARG
			ZBG->ZBG_PESO 		:= QRY_FRETE->ZB8_PREAL
			ZBG->ZBG_VLFRET 	:= QRY_FRETE->ZB8_VLDF
			ZBG->ZBG_FF 		:= QRY_FRETE->ZB8_VLDF / QRY_FRETE->ZB8_VLCARG
			ZBG->ZBG_RK 		:= QRY_FRETE->ZB8_VLDF / QRY_FRETE->ZB8_PREAL
			
			_aEstimativa 		:= getEstimativa(QRY_FRETE->ZB9_DANFE)
			ZBG->ZBG_CHVEST		:= _aEstimativa[1]
			ZBG->ZBG_VALEST		:= _aEstimativa[2]
		MsUnLock()
		
		QRY_FRETE->(DBSkip())
	ENDDO
	QRY_FRETE->(dbCloseArea())
return

Static function qryDefault()
	Local cSql := ""

	cSql += " SELECT "
	cSql += " 	ZB8_FILDOC, "
	cSql += " 	F2_DOC, "
	cSql += " 	F2_SERIE, "
	cSql += " 	ZB8_NRDF, "
	cSql += " 	ZB8_SERDF, "
	cSql += " 	ZB8_EMISDF, "
	cSql += " 	ZB8_CDDEST, "
	cSql += " 	ZB8_CDREM, "
	cSql += " 	ZB8_TOMADO, "
	cSql += " 	ZB8_VOLUM, "
	cSql += " 	ZB8_VLCARG, "
	cSql += " 	ZB8_PREAL, "
	cSql += " 	ZB8_CTE, "
	cSql += " 	ZB9_DANFE, "
	cSql += " 	ZB8_DTEMIS, "
	cSql += " 	ZB8_DTIMP, "
	cSql += " 	ZB8_VLDF "
	cSql += " FROM  "
	cSql += " 	" + RetSqlName("ZB8") + " ZB8, "
	cSql += " 	" + RetSqlName("ZB9") + " ZB9, "
	cSql += " 	" + RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	//****************************************************************************************************
	cSql += " AND ZB8.ZB8_NRIMP NOT IN ( "
	cSql += " 	SELECT ZBG_NRIMP FROM " + RetSqlName("ZBG") + " ZBG " + " WHERE ZBG.D_E_L_E_T_ = '' "
	cSql += " ) "
	//****************************************************************************************************
	cSql += "  "
	cSql += " AND ZB8.ZB8_FILIAL = ZB9.ZB9_FILIAL "
	cSql += " AND ZB8.ZB8_NRIMP = ZB9.ZB9_NRIMP "
	cSql += "  "
	cSql += " AND ZB9.ZB9_DANFE = SF2.F2_CHVNFE "
return cSql

Static Function getTipoPedido(cDanfe, cTipo)
	Local cRet := ""
	Local cSql := ""
	Local _cAlias := GetNextAlias()
		
	cSql += " SELECT DISTINCT ZZG.ZZG_COD CODIGO, ZZG.ZZG_DESCRI AS DESCRI "
	cSql += " FROM " + RetSqlName("SF2") + " SF2, "
	cSql += " 	" + RetSqlName("SD2") + " SD2, "
	cSql += " 	" + RetSqlName("SC5") + " SC5, "
	cSql += " 	" + RetSqlName("ZZG") + " ZZG "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = ' ' "
	cSql += " AND SD2.D_E_L_E_T_ = ' ' "
	cSql += " AND SC5.D_E_L_E_T_ = ' ' "
	cSql += " AND ZZG.D_E_L_E_T_ = ' ' "
	cSql += "  "
	cSql += " AND SF2.F2_FILIAL = SD2.D2_FILIAL "
	cSql += " AND SF2.F2_DOC 	 = SD2.D2_DOC "
	cSql += " AND SD2.D2_SERIE   = SF2.F2_SERIE "
	cSql += " AND SD2.D2_CLIENTE = SF2.F2_CLIENTE "
	cSql += " AND SD2.D2_LOJA    = SF2.F2_LOJA "
	cSql += "  "
	cSql += " AND SC5.C5_FILIAL = SD2.D2_FILIAL "
	cSql += " AND SC5.C5_NUM = SD2.D2_PEDIDO "
	cSql += "  "
	cSql += " AND SC5.C5_YTPVEN = ZZG.ZZG_COD "
	cSql += "  "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If cTipo == 'COD'
			cRet := (_cAlias)->CODIGO
		Else //cTipo == 'DESC'
			cRet := (_cAlias)->DESCRI
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return cRet

Static Function getEstimativa(cDanfe)
	Local cQrySASR051	:= ""
	Local aRet := {}
	Local cSql := ""
	Local cChaveZB4 := ""
	Local bMedicao := .F.
	
	//--------------------------query------------------------------------
	cQrySASR051	:= "  SELECT F2_FILIAL as Filial, R_E_C_N_O_ as SF2Recno "
	cQrySASR051	+= "  FROM "+RetSqlName("SF2")+" SF2 " "
	cQrySASR051	+= "  WHERE SF2.D_E_L_E_T_ = '' "
	cQrySASR051	+= "  AND SF2.F2_CHVNFE = '" + AllTrim(cDanfe) + "' "
	
	IF SELECT("QSASJ001") > 0
		QSASJ001->(dbCloseArea())
	Endif
						
	TCQUERY cQrySASR051 NEW ALIAS QSASJ001
	//-------------------------------------------------------------------
	dbSelectArea("QSASJ001")
	QSASJ001->(DBGOTOP())
		
	If !QSASJ001->(EOF())
		dbSelectArea("SF2")
		SF2->(dbSetOrder(10))
		if SF2->(dbSeek(QSASJ001->FILIAL+AllTrim(ZB9->ZB9_DANFE)))
			//Se for Medi��o Triangular/Direta
			dbSelectArea("ZZ2")
			ZZ2->(dbSetOrder(8))
			ZZ2->(dbGoTop())
			If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
				cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
				bMedicao := .T.
			Else
				ZZ2->(dbSetOrder(9))
				ZZ2->(dbGoTop())
				If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
					cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
					AADD(aMedicao, cChaveZB4)
					bMedicao := .T.
				Else
					ZZ2->(dbSetOrder(10))
					ZZ2->(dbGoTop())
					If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
						cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
						bMedicao := .T.
					EndIf
				EndIf
			EndIf
			
			ZZ2->(dbCloseArea())
			
			//Se for Venda Avulsa
			if !bMedicao
				dbSelectArea("SD2")
				SD2->(dbSetOrder(3))
				SD2->(dbGoTop())
				If SD2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA)))
					dbSelectArea("SC5")
					SC5->(dbSetOrder(1))
					SC5->(dbGoTop())
					If SC5->(dbSeek(SD2->(D2_FILIAL+D2_PEDIDO)))
						cChaveZB4 := SC5->(C5_FILIAL+C5_NUM)
					EndIf 
					SC5->(dbCloseArea())
				EndIf
				SD2->(dbCloseArea())
			EndIf
			
			dbSelectArea("ZB4")
			ZB4->(dbSetOrder(2))
			if ZB4->(dbSeek(cChaveZB4))
				AADD(aRet, cChaveZB4)
				AADD(aRet, ZB4->ZB4_VALOR)
			EndIf
			
			ZB4->(dbCloseArea())
		EndIf
	EndIF
	SF2->(dbCLoseArea())
	ZB9->(dbSkip())

	QSASJ001->(dbCloseArea())
	
	if empty(aRet)
		aRet := {"",0}
	EndIf
Return aRet
