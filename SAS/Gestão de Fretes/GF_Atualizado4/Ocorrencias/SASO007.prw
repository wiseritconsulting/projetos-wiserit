#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO007

Valida��o de Ocorr�ncia

VALOR DO FRETE DIFERE DO VALOR COTADO                                                           

Valida se o valor do documento de frete � o mesmo
informado na estimativa

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO007()
/*/
//-------------------------------------------------------------
User Function SASO007()
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()
	Local nTotVer := 0
	Local nTotOcor := 0

	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		nTotVer++
		If !(u_ValDifCot((_cAlias)->ZB8_NRIMP, (_cAlias)->ZB8_FILDOC, (_cAlias)->ZB8_VLDF))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			nTotOcor++
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO007", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	
	AutoGrLog("-------------- Total Verificados: " + AllTrim(Str(nTotVer)))
	AutoGrLog("-------------- Total Ocorr�ncias: " + AllTrim(Str(nTotOcor)))
	
	(_cAlias)->( DBCloseArea() )
Return 
