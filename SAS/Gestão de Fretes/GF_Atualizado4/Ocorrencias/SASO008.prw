#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO008

Valida��o de Ocorr�ncia

CTE N�O POSSUI NFE                                                    

Valida se o remetente est� correto, ou seja, se n�o exite
nenhuma pendencia no CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO008()
/*/
//-------------------------------------------------------------
User Function SASO008()
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()
	Local nTotVer := 0
	Local nTotOcor := 0

	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		nTotVer++
		If !u_temNFE((Alltrim((_cAlias)->ZB8_NRIMP)), (Alltrim((_cAlias)->ZB8_TPDF)))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			nTotOcor++
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO008", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	
	AutoGrLog("-------------- Total Verificados: " + AllTrim(Str(nTotVer)))
	AutoGrLog("-------------- Total Ocorr�ncias: " + AllTrim(Str(nTotOcor)))
	
	(_cAlias)->( DBCloseArea() )
Return 
