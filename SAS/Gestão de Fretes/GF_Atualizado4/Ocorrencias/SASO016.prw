#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO016

Valida��o de Ocorr�ncia

NFE PERTENCE A UMA MEDICAO JA LANCADA EM OUTRO CTE                                         

Valida se j� existe NFE em outro CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO016()
/*/
//-------------------------------------------------------------
User Function SASO016()
	Local _cNrImp := ZB8->ZB8_NRIMP
	Local cSql := u_getCTEbyNFE()
	Local _cAlias := GetNextAlias()
	Local nTotVer := 0
	Local nTotOcor := 0
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		IF _cNrImp == (_cAlias)->ZB8_NRIMP
			nTotVer++
			If u_TriangErr((_cAlias)->ZB9_DANFE, (_cAlias)->ZB8_NRIMP, (_cAlias)->ZB8_TPDF)
				AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
				nTotOcor++
				u_createZBC((_cAlias)->ZB8_NRIMP, "SASO016", _p_cJust)
			EndIf
		EndIf
		(_cAlias)->(dbSkip())
	Enddo
	
	AutoGrLog("-------------- Total Verificados: " + AllTrim(Str(nTotVer)))
	AutoGrLog("-------------- Total Ocorr�ncias: " + AllTrim(Str(nTotOcor)))
	
	(_cAlias)->( DBCloseArea() )
Return