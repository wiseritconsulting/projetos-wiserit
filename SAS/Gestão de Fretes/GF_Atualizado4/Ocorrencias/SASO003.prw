#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO003

Valida��o de Ocorr�ncia

NF PERTENCE A OUTRO CTE                                              

Valida se j� existe NFE em outro CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO003()
/*/
//-------------------------------------------------------------
User Function SASO003()
	Local cSql := u_getCTEbyNFE()
	Local _cAlias := GetNextAlias()
	Local cDupCte := ""
	Local nTotVer := 0
	Local nTotOcor := 0
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		nTotVer++
		If u_getNFE((_cAlias)->ZB9_DANFE, (_cAlias)->ZB8_NRIMP, (_cAlias)->ZB8_TPDF)
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			nTotOcor++
			cDupCte := getCteDup((_cAlias)->ZB9_DANFE, (_cAlias)->ZB8_NRIMP, (_cAlias)->ZB8_TPDF)
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO003", _p_cJust, cDupCte)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	
	AutoGrLog("-------------- Total Verificados: " + AllTrim(Str(nTotVer)))
	AutoGrLog("-------------- Total Ocorr�ncias: " + AllTrim(Str(nTotOcor)))
	
	(_cAlias)->( DBCloseArea() )
Return

Static Function getCteDup(cChvNFE, cSeqImp, cTpDf)
	Local xArea := getArea()
	Local cRet := ""
	Local cSql := ""
	Local _cAlias := GetNextAlias()
	
	//1=CTE Complementar;
	//2=CTE Complementar Valor;
	//3=CTE Complementar Imposto;
	//4=Reentrega;
	//5=Devolucao;
	//6=Redespacho
	
	//Se for do tipo complementar, n�o precisa validar j� que n�o
	//existem notas fiscais vinculadas a estes CTEs
	If cTpDf $ '1;2;3'
		Return cRet
	EndIf
	
	cSql += " SELECT ZB8_NRDF, ZB8_SERDF "
	cSql += " FROM "
	cSql += RetSqlName("ZB9") + " ZB9, "
	cSql += RetSqlName("ZB8") + " ZB8 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	
	cSql += " AND ZB8.ZB8_NRIMP = ZB9.ZB9_NRIMP "
	
	cSql += " AND ZB8.ZB8_TPDF = '" + cTpDf + "' "
	
	cSql += " AND ZB9.ZB9_FILIAL = '"+xFilial("ZB9")+"' "
	cSql += " AND ZB8.ZB8_FILIAL = '"+xFilial("ZB8")+"' "
	
	cSql += " AND ZB9.ZB9_DANFE = '"+cChvNFE+"' "
	cSql += " AND ZB9.ZB9_NRIMP < '"+cSeqImp+"' "
	
	cSql += " AND ZB8.ZB8_STATUS <> 'BL' "
	
	cSql += " AND ZB8.ZB8_SA <> 'S' "
	
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		cRet := ' - ' + AllTrim((_cAlias)->ZB8_NRDF) + '/' + AllTrim((_cAlias)->ZB8_SERDF)
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
	
	RestArea(xArea)
Return cRet