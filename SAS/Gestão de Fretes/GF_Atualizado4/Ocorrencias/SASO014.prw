#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO014

Valida��o de Ocorr�ncia

ESTIMATIVA N�O ENCONTRADA                   

Valida se o CFOP da ocorr�ncia est� contido no par�metro 

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO014()
/*/
//-------------------------------------------------------------
User Function SASO014()
	Local cSql :=  u_getCTERedP()
	Local _cAlias := GetNextAlias()
	Local nTotVer := 0
	Local nTotOcor := 0
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		nTotVer++
		If !(u_temEstimativa((_cAlias)->ZB9_DANFE))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			nTotOcor++
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO014", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	
	AutoGrLog("-------------- Total Verificados: " + AllTrim(Str(nTotVer)))
	AutoGrLog("-------------- Total Ocorr�ncias: " + AllTrim(Str(nTotOcor)))
		
	(_cAlias)->( DBCloseArea() )
Return 