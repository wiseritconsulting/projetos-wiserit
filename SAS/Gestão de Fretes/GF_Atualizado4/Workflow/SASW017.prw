#include "protheus.ch"

//Posi��o do cabe�alho da SP
#define CTE_PREFIXO			01 //N�mero da Fatura
#define CTE_NUM   			02 //N�mero da Fatura
#define CTE_TIPO  			03 //N�mero da Fatura
#define CTE_NATUREZA		04 //N�mero da Fatura
#define CTE_FORNECE			05 //N�mero da Fatura
#define CTE_NOMFOR			06 //N�mero da Fatura
#define CTE_VALOR 			07 //N�mero da Fatura
#define CTE_VENCREA			08 //N�mero da Fatura


//Posi��o da lista de Aprovadores
#define TIT_PREFIXO			01 //Prefixo
#define TIT_TITULO			02 //T�tulo
#define TIT_CTE				03 //CTE
#define TIT_VALOR			04 //Valor
#define TIT_VENCTO			05 //Vencimento
 
//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} SASW017
Monta a Estrutura HTML para Envio de E-mail da Solicita��o de Pagamento SP 
@type function
 
@author Samarony Barros
@since 25/05/2016
@version P11 R8
 
@param aCTE, Array, Array com os dados do cabe�alho da SP
@param aAprov, Array, Array com os dados dos aprovadores

@return cHTML, Caracter, c�digo completo HTML para envio de email
/*/
//------------------------------------------------------------------------------------------------------
User function SASW017(aFatura, aTitulo, cCorStatus)
	Local cHTML := "  "
	Local cImgSAS := "http://www.portalsas.com.br/images/Logo-SAS.png"
	Local _nTotal := 0
	
	//Local cLink := SuperGetMV("SA_LNKWFPF",.F.,"http://10.15.1.68:8082/ws020101/") + "u_SASW012.APL?KEYPF="+xFilial("ZA7")+';'+aCTE[CTE_SEQIMP]
	
	Default cCorStatus := "1"
	
	cHTML += ' <style> '
	cHTML += makeCSS()
	cHTML += ' </style> '
	cHTML += '  '
	cHTML += ' <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> '
	cHTML += '  '
	cHTML += ' <center><img src="' + cImgSAS + '" style="width: 30%;height: auto;"></center> '
	cHTML += '  '
	//////////////////////////////////////////////////////////////////////////////////////////////////
	//TABELA DE FATURA
	//////////////////////////////////////////////////////////////////////////////////////////////////
	cHTML += ' <table class="responstable" style="margin: 1em 0;width: 100%;overflow: hidden;background: #FFF;color: #024457;border-radius: 10px;border: 1px solid #167F92;"> '
	cHTML += '   <tr style="border: 1px solid #D9E4E6;"> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Prefixo</th> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">N�mero da Fatura</th> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Tipo</th> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Natureza</th> '
	cHTML += '   </tr> '
	cHTML += '    '
	cHTML += '   <tr style="border: 1px solid #D9E4E6;"> '
	cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aFatura[CTE_PREFIXO] +'</td> '
	cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aFatura[CTE_NUM] +'</td> '
	cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aFatura[CTE_TIPO] +'</td> '
	cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aFatura[CTE_NATUREZA] +'</td> '
	cHTML += '   </tr> '
	cHTML += '   <tr style="border: 1px solid #D9E4E6;"> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Fornecedor</th> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Nome Fornecedor</th> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Valor</th> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Vencimento</th> '
	cHTML += '   </tr> '
	cHTML += '    '
	cHTML += '   <tr style="border: 1px solid #D9E4E6;"> '
	cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aFatura[CTE_FORNECE] +'</td> '
	cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aFatura[CTE_NOMFOR] +'</td> '
	cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aFatura[CTE_VALOR] +'</td> '
	cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aFatura[CTE_VENCREA] +'</td> '
	cHTML += '   </tr> '
	cHTML += '    '
	cHTML += ' </table> '
	//////////////////////////////////////////////////////////////////////////////////////////////////
	// TABELA DE TITULO
	//////////////////////////////////////////////////////////////////////////////////////////////////
	cHTML += ' <table class="responstable" style="margin: 1em 0;width: 100%;overflow: hidden;background: #FFF;color: #024457;border-radius: 10px;border: 1px solid #167F92;"> '
	cHTML += '   <tr style="border: 1px solid #D9E4E6;"> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">T�tulo</th> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">CTE</th> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Valor</th> '
	cHTML += '     <th style="display: table-cell;border: 1px solid #FFF;background-color: #003364;color: #FFF;padding: 1em;text-align: center;margin: .5em 1em;border-radius: 10px;">Vencto</th> '
	cHTML += '   </tr> '
	cHTML += '    '
	For i := 1 to len(aTitulo)
		cHTML += '   <tr style="border: 1px solid #D9E4E6;"> '
		cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aTitulo[i][TIT_TITULO] +'</td> '
		cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aTitulo[i][TIT_CTE] +'</td> '
		cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aTitulo[i][TIT_VALOR] +'</td> '
		cHTML += '     <td style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">'+ aTitulo[i][TIT_VENCTO] +'</td> '
		cHTML += '   </tr> '
		
		_nTotal += val(Replace(replace(alltrim(aTitulo[i][TIT_VALOR]),".",""),",","."))
	Next i
	cHTML += '<tr style="border: 1px solid #D9E4E6;">          '
	cHTML += '<th  colspan="2"  style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">Total da fatura</td>'
	cHTML += '<td  style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;">' + Alltrim(TransForm(_nTotal, "@E 999,999,999,999.99"))  + '</td>'
	cHTML += '<td  style="display: table-cell;word-wrap: break-word;max-width: 7em;text-align: center;margin: .5em 1em;border-right: 1px solid #D9E4E6;border-radius: 10px;"></td>'
	cHTML += '</tr>'
	cHTML += '    '
	cHTML += '    '
	cHTML += '    '
	cHTML += ' </table> '
	cHTML += '  '
Return cHTML

/*/{Protheus.doc} makeCSS
Fun��o com os dados CSS para layout do email
@type function
@author Samarony Barros
@since 25/05/2016
@version P11 R8
@example cHTML := makeCSS()
@return cCSS, caracter, C�digo CSS
/*/
Static Function makeCSS()
	Local cCSS := ""
	
	//Formato circulo Legenda
	cCSS += " .legend { list-style: none;  } "
	cCSS += " .legend li { float: left; margin-right: -10px;} "
	cCSS += " .legend span { border: 1px solid #ccc; float: left; width: 20px; height: 20px; margin: 2px;  border-radius: 10px;} "
	//Cores da legenda
	cCSS += " .legend .legred { background-color: #ff0000; } "
	cCSS += " .legend .legblue { background-color: #0000ff; } "
	cCSS += " .legend .leggreen { background-color: #00ff00; } "
	cCSS += " .legend .legyellow { background-color: #ffff00; } "
	//Tabela de aprovadores e dados do CTE
	cCSS += " .responstable { "
	cCSS += "   margin: 1em 0; "
	cCSS += "   width: 100%; "
	cCSS += "   overflow: hidden; "
	cCSS += "   background: #FFF; "
	cCSS += "   color: #024457; "
	cCSS += "   border-radius: 10px; "
	cCSS += "   border: 1px solid #167F92; "
	cCSS += " } "
	cCSS += " .responstable tr { "
	cCSS += "   border: 1px solid #D9E4E6; "
	cCSS += " } "
	cCSS += " .responstable tr:nth-child(odd) { "
	cCSS += "   background-color: #EAF3F3; "
	cCSS += " } "
	cCSS += " .responstable th { "
	cCSS += "   display: none; "
	cCSS += "   border: 1px solid #FFF; "
	cCSS += "   background-color: #003364; "
	cCSS += "   color: #FFF; "
	cCSS += "   padding: 1em; "
	cCSS += " } "
	cCSS += " .responstable th:first-child { "
	cCSS += "   display: table-cell; "
	cCSS += "   text-align: center; "
	cCSS += " } "
	cCSS += " .responstable th:nth-child(2) { "
	cCSS += "   display: table-cell; "
	cCSS += " } "
	cCSS += " .responstable th:nth-child(2) span { "
	cCSS += "   display: none; "
	cCSS += " } "
	cCSS += " .responstable th:nth-child(2):after { "
	cCSS += "   content: attr(data-th); "
	cCSS += " } "
	cCSS += " @media (min-width: 480px) { "
	cCSS += "   .responstable th:nth-child(2) span { "
	cCSS += "     display: block; "
	cCSS += "   } "
	cCSS += "   .responstable th:nth-child(2):after { "
	cCSS += "     display: none; "
	cCSS += "   } "
	cCSS += " } "
	cCSS += " .responstable td { "
	cCSS += "   display: block; "
	cCSS += "   word-wrap: break-word; "
	cCSS += "   max-width: 7em; "
	cCSS += " } "
	cCSS += " .responstable td:first-child { "
	cCSS += "   display: table-cell; "
	cCSS += "   text-align: center; "
	cCSS += "   border-right: 1px solid #D9E4E6; "
	cCSS += " } "
	cCSS += " @media (min-width: 480px) { "
	cCSS += "   .responstable td { "
	cCSS += "     border: 1px solid #D9E4E6; "
	cCSS += "   } "
	cCSS += " } "
	cCSS += " .responstable th, .responstable td { "
	cCSS += "   text-align: left; "
	cCSS += "   margin: .5em 1em; "
	cCSS += " } "
	cCSS += " @media (min-width: 480px) { "
	cCSS += "   .responstable th, .responstable td { "
	cCSS += "     display: table-cell; "
	cCSS += "     padding: 1em; "
	cCSS += "   } "
	cCSS += " } "
	//Informa��es da tela de aprova��o
	cCSS += " body { "
	cCSS += "   padding: 0 2em; "
	cCSS += "   font-family: Arial, sans-serif; "
	cCSS += "   color: #024457; "
	cCSS += "   background: #dbdbdb; "
	cCSS += " } "
	cCSS += "  "
	cCSS += " h1 { "
	cCSS += "   font-family: Verdana; "
	cCSS += "   font-weight: normal; "
	cCSS += "   color: #024457; "
	cCSS += " } "
	cCSS += " h1 span { "
	cCSS += "   color: #167F92; "
	cCSS += " } "
	cCSS += " img { "
	cCSS += "     width: 30%; "
	cCSS += "     height: auto; "
	cCSS += " } "
	cCSS += "  "
	//Status da aprova��o
	cCSS += " .sas-status h1 { "
	cCSS += "   font-family: Verdana; "
	cCSS += "   font-weight: normal; "
	cCSS += "   color: #024457; "
	cCSS += " } "
	cCSS += "  "
	cCSS += " div.sas-box-status { "
	cCSS += "     background-color: lightgrey; "
	cCSS += "     padding: 1%; "
	cCSS += "     border: 10px solid #024457; "
	cCSS += " 	border-radius: 20px; "
	cCSS += " } "
	cCSS += "  "
	cCSS += " .sas-status-approved { "
	cCSS += "   font-family: Verdana; "
	cCSS += "   font-weight: normal; "
	cCSS += "   color: #0076AF; "
	cCSS += "   font-weight: bold; "
	cCSS += " } "
	cCSS += "  "
	cCSS += " .sas-status-disapproved { "
	cCSS += "   font-family: Verdana; "
	cCSS += "   font-weight: normal; "
	cCSS += "   color: #FF0000; "
	cCSS += "   font-weight: bold; "
	cCSS += " } "
	cCSS += " .sas-status-waiting { "
	cCSS += "   font-family: Verdana; "
	cCSS += "   font-weight: normal; "
	cCSS += "   color: #DB9600; "
	cCSS += "   font-weight: bold; "
	cCSS += " } "
	cCSS += "  "
	//Justificativa
	cCSS += " div.justification{ "
	cCSS += "     background-color: lightgrey; "
	cCSS += "     padding: 1%; "
	cCSS += "     border: 10px solid #024457; "
	cCSS += " 	border-radius: 20px; "
	cCSS += " } "
Return cCSS

