#Include 'Protheus.ch'

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} EmailGF

Envia email para aprovadores de ocorr�ncia, valor e R$/Kg
e para os usu�rio do financeiro que receber�o informa��es sobre a fatura

@type function
@author Sam Barros
@since 07/08/2016
@version 1.0
@param aEmail, array, Array contendo os emails que receber�o as informa��es
@param cSeqImp, character, Sequencia do CTE
@param cTipo, character, Informa se ser� O-Ocorr�ncia, A-AProvador, F-Fatura(Financeiro)
@param cFatura, character, Se o tipo for F, ent�o deve ser informado o n�mero da fatura
@return bRet, Informa se o email foi enviado
@example
(examples)
/*/
//------------------------------------------------------------------------------------------------------
User Function EmailGF(cSeqImp, cTipo, cFatura)
	Local bRet := .T.
	
	Default cSeqImp := ""
	Default cFatura := ""
	
	If !(SuperGetMV("SA_MAILCTE", .F., .F.))
		Return
	EndIf
	
	Do Case
		Case cTipo == 'O'
			ocorrencia(cSeqImp)
		Case cTipo == 'A'
			aprovador(cSeqImp)
		Case cTipo == 'F'
			fatura(cSeqImp, cFatura)
	EndCase
Return bRet

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ocorrencia

Fun��o que envia email para os aprovadores de ocorr�ncia

@author Sam Barros 
@since 28/08/2016
@version 1.0
@param cSeqImp, character, Sequencial de importa��o
@example
ocorrencia("0000000000000001")
/*/
//------------------------------------------------------------------------------------------------------
Static Function ocorrencia(cSeqImp)
	Local aCTE := getDadosCTE(cSeqImp)
	Local aAprov := getAprOco(cSeqImp)
	Local nTamOco := len(aAprov) 
	Local i
	Local cHTML := ""
	Local cList := ""
	Local cEmail := ""
	
	cHTML := U_SASW015(aCTE, aAprov) 
	
	For i := 1 to nTamOco
		cEmail := U_InfoUser(aAprov[i][7], "EMAIL")
		If !(cEmail $ cList)
			cList += cEmail + ';'
			//U_SendMail("samarony@objetiveti.com.br", "[Gest�o de Fretes] Visto Ocorr�ncia - CTE:" + aCTE[2],cHtml,,)
			U_SendMail(cEmail, "[Gest�o de Fretes] Visto Ocorr�ncia - CTE:" + aCTE[2],cHtml,,)
		EndIf
	Next i 
Return

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} aprovador

Fun��o que envia email para os aprovadores de al�ada

@author Sam Barros
@since 28/08/2016
@version 1.0
@param cSeqImp, character, Sequencial de importa��o
@example
aprovador("0000000000000001")
/*/
//------------------------------------------------------------------------------------------------------
Static Function aprovador(cSeqImp)
	Local aCTE := getDadosCTE(cSeqImp)
	Local aAprov := getAprVal(cSeqImp)
	Local nTamVal := len(aAprov) 
	Local i
	Local cHTML := ""
	Local cList := ""
	Local cEmail := ""
		
	cHTML := U_SASW016(aCTE, aAprov) 
	
	For i := 1 to nTamVal
		cEmail := U_InfoUser(aAprov[i][6], "EMAIL")
		
		If !(cEmail $ cList)
			cList += cEmail + ';'
			//U_SendMail("samarony@objetiveti.com.br", "[Gest�o de Fretes] Aprova��o Al�ada - CTE:" + aCTE[2],cHtml,,)
			U_SendMail(cEmail, "[Gest�o de Fretes] Aprova��o Al�ada - CTE:" + aCTE[2],cHtml,,)
		EndIf
	Next i 
Return

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} fatura

Fun��o que envia email para o email do financeiro informado no parametro SA_FINCTE

@author Sam Barros
@since 28/08/2016
@version 1.0
@param cSeqImp, character, Sequencial de importa��o
@example
fatura("0000000000000001")
/*/
//------------------------------------------------------------------------------------------------------
Static Function fatura(cSeqImp, cFatura)
	Local cHTML := ""
	Local cEmail := SuperGetMv("SA_FATCTE",.F.,"samarony@objetiveti.com.br")
	Local aFatura := getDadosFatura(cFatura, cSeqImp)
	Local aTitulo := getCTEFatura(cFatura, cSeqImp)
		
	cHTML := U_SASW017(aFatura, aTitulo) 
	
	//U_SendMail("samarony@objetiveti.com.br", "[Gest�o de Fretes] Aprova��o Al�ada - CTE:" + aCTE[2],cHtml,,)
	U_SendMail(cEmail, "[Gest�o de Fretes] Dados da Fatura - CTE:" + getNumCTE(cSeqImp),cHtml,,)
Return

Static Function getNumCTE(cSeqImp)
	Local xArea := getArea()
	Local cRet := ""
	
	dbSelectArea("ZB8")
	ZB8->(dbSetOrder(1))
	If ZB8->(dbSeek(xFilial("ZB8") + cSeqImp))
		cRet := AllTrim(ZB8->ZB8_NRDF) + '/' + AllTrim(ZB8->ZB8_SERDF)
	EndIf
	ZB8->(dbCloseArea())
	
	RestArea(xArea)
Return cRet

Static Function getDadosFatura(cFatura, cSeqImp)
	Local aRet := {}
	Local cSql := ""
	Local cFilTom := ""
	
	Local xArea := getArea()
	
	dbSelectArea("ZB8")
	ZB8->(dbSetOrder(1))
	If ZB8->(dbSeek(xFilial("ZB8")+cSeqImp))
		cFilTom := ZB8->ZB8_FILTOM
	EndIf
	ZB8->(dbCloseArea())
	
	
	cSql += " SELECT E2_PREFIXO, E2_NUM, E2_TIPO, E2_FORNECE, E2_LOJA, E2_NOMFOR, E2_VENCREA, E2_VALOR, E2_NATUREZ "
	cSql += " FROM "
	cSql += RetSqlName("SE2") + " SE2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SE2.D_E_L_E_T_ = '' "
	cSql += " AND SE2.E2_FILIAL = '" + SUBS(cFilTom,1,2) + Space(4) + "' "
	cSql += " AND SE2.E2_NUM = '" + cFatura + "' "
	cSql += " AND SE2.E2_PREFIXO = 'FAT' "
	cSql += " AND SE2.E2_TIPO = 'CTE' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	dbselectarea("QRY")
	QRY->(dbGotop())
	
	While !(QRY->(EOF()))
		AADD(aRet, QRY->E2_PREFIXO)
		AADD(aRet, QRY->E2_NUM)
		AADD(aRet, QRY->E2_TIPO)
		AADD(aRet, QRY->E2_NATUREZA + POSICIONE("SED",1,xFilial("SED") + QRY->E2_NATUREZ, "SED->ED_DESCRIC"))
		AADD(aRet, QRY->E2_FORNECE + '/' + QRY->E2_LOJA)
		AADD(aRet, QRY->E2_NOMFOR)
		AADD(aRet, Transform(QRY->E2_VALOR, PesqPict("SE2", "E2_VALOR")))
		AADD(aRet, DtoC(StoD(QRY->E2_VENCREA)))
		
		QRY->(dbSkip())
	EndDO
	
	QRY->(dbCloseArea())
	
	RestArea(xArea)
Return aRet 

Static Function getCTEFatura(cFatura, cSeqImp)
	Local aRet := {}
	Local aAux := {}
	Local cSql := ""
	Local cFilTom := ""
	
	Local xArea := getArea()
	
	dbSelectArea("ZB8")
	ZB8->(dbSetOrder(1))
	If ZB8->(dbSeek(xFilial("ZB8")+cSeqImp))
		cFilTom := ZB8->ZB8_FILTOM
	EndIf
	ZB8->(dbCloseArea())
	
	dbSelectArea("ZB8")
	ZB8->(dbSetOrder(8))
	If ZB8->(dbSeek(xFilial("ZB8") + cFatura))
	
		cSql += " SELECT E2_PREFIXO, E2_NUM, E2_VALOR, E2_VENCREA "
		cSql += " FROM "
		cSql += RetSqlName("SE2") + " SE2 "
		cSql += " WHERE 1=1 "
		cSql += " AND SE2.D_E_L_E_T_ = '' "
		cSql += " AND SE2.E2_FILIAL = '" + SUBS(cFilTom,1,2) + Space(4) + "' "
		cSql += " AND SE2.E2_FATURA = '" + cFatura + "' "
		
		DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
		dbselectarea("QRY")
		QRY->(dbGotop())
		
		While !(QRY->(EOF()))
			aAux := {}
			AADD(aAux, QRY->E2_PREFIXO)
			AADD(aAux, QRY->E2_NUM)
			AADD(aAux, ZB8->ZB8_CTE)
			AADD(aAux, Transform(QRY->E2_VALOR, PesqPict("SE2", "E2_VALOR")))
			AADD(aAux, DtoC(StoD(QRY->E2_VENCREA)))
			
			AADD(aRet, aAux)
			QRY->(dbSkip())
			ZB8->(dbSkip())
		EndDO
	EndIf
	
	QRY->(dbCloseArea())
	
	ZB8->(dbCloseArea())
	
	RestArea(xArea)
Return aRet

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} getAprOco

Fun��o que busca os aprovadores de ocorr�ncia

@author Sam Barros
@since 07/08/2016
@version 1.0
@param cSeqImp, character, N�mero sequencial de importa��o
@return aRet, Array com os dados dos aprovadores
@example
getAprOco("0000000000000001")
/*/
//------------------------------------------------------------------------------------------------------

Static Function getAprOco(cSeqImp)
	Local aRet := {}
	Local aAux := {}
	Local xArea := getArea()
	
	dbSelectArea("ZBE")
	dbSetOrder(1)
	If dbSeek(xFilial("ZBE")+cSeqImp)
		While !(ZBE->(EOF())) .AND. ZBE->ZBE_NRIMP == cSeqImp
			aAux := {}
			 
			AADD(aAux, ZBE->ZBE_STATUS)
			AADD(aAux, UsrRetName(ZBE->ZBE_USER))
			AADD(aAux, DtoC(ZBE->ZBE_DTAPROV))
			AADD(aAux, ZBE->ZBE_OCORR + ' - ' + ZBE_DESOCO)
			AADD(aAux, ZBE->ZBE_TPJUST + ' - ' + ZBE_JUST)
			AADD(aAux, ZBE->ZBE_OBS)
			AADD(aAux, ZBE->ZBE_USER)
			AADD(aAux, UsrRetName(ZBE->ZBE_APUSER))
			
			AADD(aRet, aAux)
			
			ZBE->(dbSkip())
		EndDo
		
		ZBE->(dbCloseArea())
	EndIf
	
	RestArea(xArea)
Return aRet

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} getAprVal

Fun��o que busca os aprovadores de valor ou R$/Kg

@author Sam Barros
@since 07/08/2016
@version 1.0
@param cSeqImp, character, N�mero sequencial de importa��o
@return aRet, Array com os dados dos aprovadores
@example
getAprVal("0000000000000001")
/*/
//------------------------------------------------------------------------------------------------------

Static Function getAprVal(cSeqImp)
	Local aRet := {}
	Local aAux := {}
	Local xArea := getArea()
	
	dbSelectArea("ZBF")
	dbSetOrder(1)
	If dbSeek(xFilial("ZBF")+cSeqImp)
		While !(ZBF->(EOF())) .AND. ZBF->ZBF_NRIMP == cSeqImp
			aAux := {}
			 
			AADD(aAux, ZBF->ZBF_STATUS)
			AADD(aAux, UsrRetName(ZBF->ZBF_USER))
			AADD(aAux, DtoC(ZBF->ZBF_DTAPRO))
			if ZBF->ZBF_TPAPRO == '1'
				AADD(aAux, Transform(ZBF->ZBF_VALFRE,PesqPict("ZB8", "ZB8_VLDF")))
			Else
				AADD(aAux, Transform(ZBF->ZBF_PESO/ZBF->ZBF_VALFRE,PesqPict("ZB8", "ZB8_VLDF")))
			EndIf
			AADD(aAux, Transform(getValAnt(ZBF->ZBF_NRIMP, ZBF->ZBF_TPAPRO),PesqPict("ZB8", "ZB8_VLDF")))
			AADD(aAux, ZBF->ZBF_USER)
			
			AADD(aRet, aAux)
			
			ZBF->(dbSkip())
		EndDo
		
		ZBF->(dbCloseArea())
	EndIf
	
	RestArea(xArea)
Return aRet

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} getDadosCTE

Pega dados do CTE para inclus�o no cabe�alho do email via WF

@author Sam Barros
@since 07/08/2016
@version 1.0
@param cSeqImp, character, (Descri��o do par�metro)
@return aRet, Informa��es do CtE
@example
getAprOco("0000000000000001")
/*/
//------------------------------------------------------------------------------------------------------
Static Function getDadosCTE(cSeqImp)
	Local aRet := {}
	Local xArea := getArea()

	dbSelectArea("ZB8")
	dbSetOrder(1)
	if dbSeek(xFilial("ZB8") + cSeqImp)
		AADD(aRet, ZB8->ZB8_FILDOC)
		AADD(aRet, ZB8->ZB8_NRDF + '/' + ZB8->ZB8_SERDF)
		AADD(aRet, DtoC(ZB8->ZB8_DTEMIS))
		AADD(aRet, Transform(ZB8->ZB8_VLDF, PesqPict("ZB8","ZB8_VLDF")))
		If ZB8->ZB8_DEVOL == 'S'
			AADD(aRet, ZB8->ZB8_EMISDF  + " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_EMISDF,"SA2->A2_NREDUZ"))
			AADD(aRet, ZB8->ZB8_CDREM   + " - " + POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDREM,"SA1->A1_NREDUZ"))
			AADD(aRet, ZB8->ZB8_TOMADO  + " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_TOMADO,"SA2->A2_NREDUZ"))
			AADD(aRet, ZB8->ZB8_CDDEST  + " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDDEST,"SA2->A2_NREDUZ"))
		Else
			AADD(aRet, ZB8->ZB8_EMISDF  + " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_EMISDF,"SA2->A2_NREDUZ"))
			AADD(aRet, ZB8->ZB8_CDREM   + " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDREM,"SA2->A2_NREDUZ"))
			AADD(aRet, ZB8->ZB8_TOMADO  + " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_TOMADO,"SA2->A2_NREDUZ"))
			AADD(aRet, ZB8->ZB8_CDDEST  + " - " + POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDDEST,"SA1->A1_NREDUZ"))
		EndIf
		AADD(aRet, )
	EndIf
	ZB8->(dbCloseArea())
	
	RestArea(xArea)
Return aRet

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} getValAnt

Fun��o que busca o ultimo valor do hist�rico de fretes 

@author Sam Barros
@since 28/08/2016
@version 1.0
@param cSeqImp, character, N�mero sequencial de importa��o
@param cTipo, character, Tipo de aprova��o
@return nRet, Valor
@example
getValAnt("0000000000000001", '1')
/*/
//------------------------------------------------------------------------------------------------------
Static Function getValAnt(cSeqImp, cTipo)
	Local nRet := 0
	
	Local cRem := ""
	Local cDest := ""
	
	Local xArea := getArea()
	
	dbSelectArea("ZB8")
	ZB8->(dbSetOrder(1))
	If ZB8->(dbSeek(xFilial("ZB8") + cSeqImp))
		cRem 	:= POSICIONE("SA2",1,xFilial("SA2") + ZB8->ZB8_CDREM , "SA2->A2_COD_MUN")
		cDest 	:= POSICIONE("SA1",1,xFilial("SA1") + ZB8->ZB8_CDDEST, "SA1->A1_COD_MUN")
		
		dbSelectArea("ZBB")
		ZBB->(dbSetOrder(2))
		If ZBB->(dbSeek(xFilial("ZBB") + cRem + cDest))
			If cTipo == '1'
				nRet := ZBB->ZBB_VALOR
			Else
				nRet := ZBB->ZBB_RK
			EndIf 
		EndIf
	EndIf
	
	ZB8->(dbCloseArea())  
	
	RestArea(xArea) 
Return nRet