#Include 'Protheus.ch'

User Function SASF009(cSeqImp, oDlgAprov)
	Local cStatus 	:= ""
	
	Local oBold
	Local oMainWnd
	
	Local nLin		:= 0
	
	Local oFolder := nil
	
	Local aRet := {}
	Local aParamBox := {}
	Local aTpAprov := {"Ocorr�ncia", "Al�ada"}
	
	Local aSttsAprov := {}
	
	Local xArea
	
	Private aRotina := {}
	
	Private aCols	:= {}
	Private AHEADER	:= {}
	
	xArea := getArea()
	
	aAdd(aParamBox,{2,"Qual Aprova��o?",1,aTpAprov,50,"",.F.})
	
	If ParamBox(aParamBox,"Par�metros",@aRet)
		//oFolder := MSDIALOG():New(000,000,500,900, "Aprova��o",,,,,,,,,.T.)
		
		If ValType(aRet[1]) == 'N'
			Aadd(AHEADER,{" "						,"ZZ_LEGENDA"	,"@BMP"				, 3	,0,""		,"S"	,"C",""			,"V"} )
			AADD(AHEADER,{"Aprovador"				,"ZZ_NOME"  	,"@!"				,15 ,0,".F."	,"�"	,"C",""			,"V"} )
			AADD(AHEADER,{"Ocorrencia"				,"ZZ_OCORR" 	,"@!"				,02 ,0,".F."	,"�"	,"C",""			,"V"} )
			AADD(AHEADER,{"Desc Ocorr."				,"ZZ_DESOCO" 	,"@!"				,09 ,0,".F."	,"�"	,"C",""			,"V"} )
			AADD(AHEADER,{"Dt. Emiss�o"				,"ZZ_DTEMIS" 	,""					,08 ,0,".F."	,"�"	,"D",""			,"V"} )
			AADD(AHEADER,{"Dt. Aprova��o"			,"ZZ_DTAPRO" 	,""					,08 ,0,".F."	,"�"	,"D",""			,"V"} )
			AADD(AHEADER,{"Hr. Aprova��o"			,"ZZ_HRAPRO"	,"@!"				,08 ,0,".F."	,"�"	,"C",""			,"V"} )
			AADD(AHEADER,{"User Aprovador"			,"ZZ_APUSER"	,"@!"				,15 ,0,".F."	,"�"	,"C",""			,"V"} )
			AADD(AHEADER,{"Status"					,"ZZ_STATUS"	,"@!"				,02 ,0,".F."	,"�"	,"C",""			,"V"} )
		  	
		   	DBSELECTAREA("ZBE")
			ZBE->(DBSETORDER(1))
			ZBE->(DBGOTOP())
			IF ZBE->(DBSEEK(XFILIAL("ZBE")+cSeqImp))
				WHILE ZBE->(!EOF()).AND. ZBE->ZBE_NRIMP == cSeqImp				
					AADD(aCols,Array(Len(AHEADER)+1))
					nLin := Len(aCols)
					aCols[nLin,01]	:= getColor(ZBE->ZBE_STATUS)
					aCols[nLin,02]	:= UsrRetName(ZBE->ZBE_USER)
					aCols[nLin,03]	:= ZBE->ZBE_OCORR
					aCols[nLin,04]	:= ZBE->ZBE_DESOCO
					aCols[nLin,05]	:= ZBE->ZBE_EMISS
					aCols[nLin,06]	:= ZBE->ZBE_DTAPRO
					aCols[nLin,07]	:= ZBE->ZBE_HRAPRO
					aCols[nLin,08]	:= UsrRetName(ZBE->ZBE_APUSER)
					aCols[nLin,09]	:= ZBE->ZBE_STATUS
					aCols[nLin,Len(AHEADER)+1] := .F.
					
					AADD(aSttsAprov, ZBE->ZBE_STATUS)
					
					ZBE->(DBSKIP())
				ENDDO
			Else
				MsgAlert("N�o Existem aprovadores de ocorr�ncia para este CTE")
				Return
			ENDIF
			
			cStatus := getSit(aSttsAprov, 'O')
			
			DEFINE FONT oBold NAME "Arial" SIZE 0, -12 BOLD
			
			DEFINE MSDIALOG oFolder TITLE "Ocorr�ncias" From 109,095 To 400,600 OF oDlgAprov PIXEL
			@ 005,003 TO 032,250 LABEL "" OF oFolder PIXEL
			@ 015,007 SAY "Seq. Imp." OF oFolder FONT oBold PIXEL SIZE 046,009
			@ 014,048 MSGET cSeqImp PICTURE "" WHEN .F. PIXEL SIZE 150,009 OF oFolder FONT oBold
			
			@ 132,008 SAY 'Situacao :' OF oFolder PIXEL SIZE 052,009
			@ 132,038 SAY cStatus OF oFolder PIXEL SIZE 120,009 FONT oBold
			@ 132,205 BUTTON 'Fechar' SIZE 035 ,010  FONT oFolder:oFont ACTION (oFolder:End()) OF oFolder PIXEL
						
			GridDados:= MsNewGetDados():New(038,003,120,250,,,,,,,,,,,oFolder,aHeader,aCols)
			
			ACTIVATE MSDIALOG oFolder CENTER	
			
		Else
			Aadd(AHEADER,{" "						,"ZZ_LEGENDA"	,"@BMP"				, 3	,0,""		,"S"	,"C",""			,"V"} )
			AADD(AHEADER,{"Aprovador"				,"ZZ_NOME"  	,"@!"				,15 ,0,".F."	,"�"	,"C",""			,"V"} )
			AADD(AHEADER,{"Al�ada"					,"ZZ_TPAPRO" 	,"@!"				,05 ,0,".F."	,"�"	,"C",""			,"V"} )
			AADD(AHEADER,{"Dt. Emiss�o"				,"ZZ_DTEMIS" 	,""					,08 ,0,".F."	,"�"	,"D",""			,"V"} )
			AADD(AHEADER,{"Dt. Aprova��o"			,"ZZ_DTAPRO" 	,""					,08 ,0,".F."	,"�"	,"D",""			,"V"} )
			AADD(AHEADER,{"Hr. Aprova��o"			,"ZZ_HRAPRO"	,"@!"				,08 ,0,".F."	,"�"	,"C",""			,"V"} )
			AADD(AHEADER,{"Status"					,"ZZ_STATUS"	,"@!"				,02 ,0,".F."	,"�"	,"C",""			,"V"} )
		  	
		   	DBSELECTAREA("ZBF")
			ZBF->(DBSETORDER(1))
			ZBF->(DBGOTOP())
			IF ZBF->(DBSEEK(XFILIAL("ZBF")+cSeqImp))
				WHILE ZBF->(!EOF()).AND. ZBF->ZBF_NRIMP == cSeqImp
					AADD(aCols,Array(Len(AHEADER)+1))
					nLin := Len(aCols)
					aCols[nLin,01]	:= getColor(ZBF->ZBF_STATUS)
					aCols[nLin,02]	:= UsrRetName(ZBF->ZBF_USER)
					aCols[nLin,03]	:= IIF(ZBF->ZBF_TPAPRO=='1',"Valor","R$/Kg")
					aCols[nLin,04]	:= ZBF->ZBF_EMISS
					aCols[nLin,05]	:= ZBF->ZBF_DTAPRO
					aCols[nLin,06]	:= ZBF->ZBF_HRAPRO
					aCols[nLin,07]	:= ZBF->ZBF_STATUS
					aCols[nLin,Len(AHEADER)+1] := .F.
					
					AADD(aSttsAprov, ZBE->ZBE_STATUS)
					
					ZBF->(DBSKIP())
				ENDDO
			Else
				MsgAlert("N�o Existem aprova��o de al�ada para este CTE")
				Return
			ENDIF
			
			cStatus := getSit(aSttsAprov, 'A')
			
			DEFINE FONT oBold NAME "Arial" SIZE 0, -12 BOLD
			DEFINE MSDIALOG oFolder TITLE "Al�ada" From 109,095 To 400,600 OF oDlgAprov PIXEL
			@ 005,003 TO 032,250 LABEL "" OF oFolder PIXEL
			@ 015,007 SAY "Seq. Imp." OF oFolder FONT oBold PIXEL SIZE 046,009
			@ 014,048 MSGET cSeqImp PICTURE "" WHEN .F. PIXEL SIZE 150,009 OF oFolder FONT oBold
			
			@ 132,008 SAY 'Situacao :' OF oFolder PIXEL SIZE 052,009
			@ 132,038 SAY cStatus OF oFolder PIXEL SIZE 120,009 FONT oBold
			@ 132,205 BUTTON 'Fechar' SIZE 035 ,010  FONT oFolder:oFont ACTION (oFolder:End()) OF oFolder PIXEL
						
			GridDados:= MsNewGetDados():New(038,003,120,250,,,,,,,,,,,oFolder,aHeader,aCols)
			
			ACTIVATE MSDIALOG oFolder CENTERED
		EndIf
	Else
		Return
	EndIf
	
	RestArea(xArea)
Return

Static Function getColor(cStatus)
	Local cRet := ""
	
	IF cStatus == "A"
		cRet := "BR_VERDE"	
	ElseIf  cStatus == "R"
		cRet := "BR_LARANJA"
	ElseIF cStatus == "P"
		cRet := "BR_AMARELO"
	ElseIF cStatus == "B"
		cRet := "BR_VERMELHO"
	EndIf
Return cRet

Static Function getSit(aVet, cTipo)
	Local cRet := ""
	Local nCnt := 0
	
	if cTipo == 'O'
		cRet := getStatusApr(aVet[1])
	Else
		For nCnt := 1 to len(aVet)
			If aVet[nCnt] $ 'R_B'
				cRet := getStatusApr(aVet[nCnt])
				exit
			ElseIf aVet[nCnt] == 'A'
				cRet := getStatusApr(aVet[nCnt])
				exit
			EndIf
		Next nCnt
	EndIf
Return cRet

Static Function getStatusApr(cStatus)
	Local cRet := ""
	
	IF cStatus == "A"
		cRet := "Aprovado"
	ElseIf  cStatus == "R"
		cRet := "Reprovado"
	ElseIF cStatus == "P"
		cRet := "Pendente"
	ElseIF cStatus == "B"
		cRet := "Bloqueado"
	EndIf
Return cRet

Static Function getAprOco(cSeqImp)
	Local aRet := {}
	Local aAux := {}
	
	dbSelectArea("ZBE")
	dbSetOrder(1)
	If dbSeek(xFilial("ZBE")+cSeqImp)
		While !(ZBE->(EOF())) .AND. ZBE->ZBE_NRIMP == cSeqImp
			aAux := {}
			 
			AADD(aAux, ZBE->ZBE_STATUS)
			AADD(aAux, UsrRetName(ZBE->ZBE_USER))
			AADD(aAux, DtoC(ZBE->ZBE_DTAPROV))
			AADD(aAux, ZBE->ZBE_OCORR + ' - ' + ZBE_DESOCO)
			AADD(aAux, ZBE->ZBE_TPJUST + ' - ' + ZBE_JUST)
			AADD(aAux, ZBE->ZBE_OBS)
			
			AADD(aRet, aAux)
			
			ZBE->(dbSkip())
		EndDo
		
		ZBE->(dbCloseArea())
	EndIf
Return aRet
