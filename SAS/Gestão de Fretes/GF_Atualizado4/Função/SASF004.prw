#Include 'Protheus.ch'
#include "totvs.ch"

//Descrevendo Defines
#define POS_UFORI	01
#define POS_CDMUOR	02
#define POS_UFDES	03
#define POS_CDMUDE	04
#define POS_QTDCTE	05
#define POS_VALOR	06
#define POS_VALNF	07
#define POS_PESO	08
#define POS_MODAL	09
#define POS_FF		10
#define POS_RK		11
//------------------------------------------------------------
/*/{Protheus.doc} SASF004

Fun��o para mostrar a tela de processamento de toda a rotina

@type function
@author Sam Barros
@since 11/07/2016
@version 1.0
/*/
//------------------------------------------------------------
User Function SASF004()
	Processa( {|| U_SASFO04() }, "Aguarde...", "Importando hist�rico de frete...",.F.)
Return 

//------------------------------------------------------------
/*/{Protheus.doc} SASFO04

Fun��o para realizar a importa��o da estimativa de frete

@type function
@todo valida��es do arquivo
@author Sam Barros
@since 11/07/2016
@version 1.0
/*/
//------------------------------------------------------------
User Function SASFO04()
	Local cDir 		:= "C:\SAS\"
	Local cArq    	:= "historico.csv"
	Local cLinha  	:= ""
	Local bFirst   	:= .T.
	Local aCampos 	:= {}
	Local aDados  	:= {}
	
	Private cFile 	:= ""
 
	Private aErro 		:= {}
	Private _cExtens   	:= "Arquivo CSV ( *.CSV ) |*.csv|"
	Private aErrFile 	:= {}
	
	cFile := cGetFile( _cExtens, "Selecione o Arquivo",1,, .T., GETF_LOCALHARD+GETF_LOCALFLOPPY)//GETF_NETWORKDRIVE + GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_RETDIRECTORY )
 
	If !File(cFile)//!File(cDir + cArq)
		MsgStop("O arquivo " + cFile/*cDir+cArq*/ + " n�o foi encontrado. A importa��o ser� abortada!","[SAS] - ATENCAO")
		Return
	EndIf
 
	FT_FUSE(cFile)//FT_FUSE(cDir+cArq)
	ProcRegua(FT_FLASTREC())
	FT_FGOTOP()
	
	While !FT_FEOF()
		IncProc("Lendo arquivo CSV...")
 
		cLinha := FT_FREADLN()
 
		If bFirst
			aCampos := Separa(cLinha,";",.T.)
			For i := 1 to len(aCampos)
				aCamposDiv := Separa(aCampos[i],"-",.T.)
				aCampos[i] := AllTrim(aCamposDiv[2])
			Next i
			bFirst := .F.
		Else
			aDadosDiv := Separa(cLinha,";",.T.) 
			AADD(aDados, aDadosDiv)
		EndIf
 
		FT_FSKIP()
	EndDo
	
	_nTam := len(AllTrim(STR(len(aDados))))
	
	If temMunDuplicado(aDados)
		Return
	EndIf
	
	limpaHist()
 
	Begin Transaction
		ProcRegua(Len(aDados))
		For i:=1 to Len(aDados)
 
			IncProc("Importando hist�rico de fretes [Linha " + Alltrim(STRZERO(i,_nTam,0)) + "]")
 
			dbSelectArea("ZBB")
			dbSetOrder(1)
			dbGoTop()
			Reclock("ZBB",.T.)
			ZBB->ZBB_FILIAL	:= xFilial("ZBB")
			ZBB->ZBB_COD 	:= GETSXENUM("ZBB","ZBB_COD")
			ZBB->ZBB_CDMUOR := aDados[i][POS_CDMUOR] 
			ZBB->ZBB_UFORI 	:= aDados[i][POS_UFORI]
			ZBB->ZBB_MUNORI := POSICIONE("CC2",1,XFILIAL("CC2") + aDados[i][POS_UFORI] + aDados[i][POS_CDMUOR],"CC2->CC2_MUN")
			ZBB->ZBB_UFDES 	:= aDados[i][POS_UFDES]
			ZBB->ZBB_CDMUDE := aDados[i][POS_CDMUDE]
			ZBB->ZBB_MUNDES := POSICIONE("CC2",1,XFILIAL("CC2") + aDados[i][POS_UFDES] + aDados[i][POS_CDMUDE],"CC2->CC2_MUN") 
			ZBB->ZBB_QTDCTE := VAL(aDados[i][POS_QTDCTE])
			ZBB->ZBB_VALOR 	:= VAL(aDados[i][POS_VALOR])
			ZBB->ZBB_VALNF 	:= VAL(aDados[i][POS_VALNF])
			ZBB->ZBB_PESO 	:= VAL(aDados[i][POS_PESO])
			ZBB->ZBB_MODAL 	:= aDados[i][POS_MODAL]
			ZBB->ZBB_FF 	:= VAL(aDados[i][POS_VALOR])/VAL(aDados[i][POS_VALNF])
			ZBB->ZBB_RK 	:= VAL(aDados[i][POS_VALOR])/VAL(aDados[i][POS_PESO])
			
			ZBB->ZBB_DATA 	:= dDataBase
			
			ConfirmSx8()
			
			ZBB->(MsUnlock())
		Next i
	End Transaction
 
	FT_FUSE()
 
	ApMsgInfo("Importa��o do hist�rico de fretes conclu�da com sucesso!","[SAS] - SUCESSO")
     
Return

Static Function limpaHist()
	dbSelectArea("ZBB")
	dbGoTop()
	
	ProcRegua(ZBB->(RecCount()))
	
	While !(ZBB->(EOF()))
		IncProc("Removendo dados do Banco...")
		RecLock("ZBB", .F.)
			dbDelete()
		MsUnLock()
		
		ZBB->(dbSkip())
	EndDo
	
	ZBB->(dbCloseArea())
Return

Static Function temMunDuplicado(aDados)
	Local bRet := .F.
	Local i := 0
	Local j := 0
	Local nTam := len(aDados)
	Local bSair := .F.
	
	Local cMunOri := ""
	Local cMunDes := ""
	
	For i := 1 to nTam-1
		For j := i+1 to nTam 
			If 	aDados[i][POS_UFORI] 	== aDados[j][POS_UFORI] 	.AND. ;
				aDados[i][POS_CDMUOR] 	== aDados[j][POS_CDMUOR] 	.AND. ;
				aDados[i][POS_UFDES] 	== aDados[j][POS_UFDES] 	.AND. ;
				aDados[i][POS_CDMUDE] 	== aDados[j][POS_CDMUDE] 	.AND. ;
				aDados[i][POS_MODAL] 	== aDados[j][POS_MODAL] 
				bRet := .T.
				
				cMunOri := "[" + aDados[i][POS_UFORI] + "/" + aDados[i][POS_CDMUOR]+"] " + AllTrim(POSICIONE("CC2",1,XFILIAL("CC2") + aDados[i][POS_UFORI] + aDados[i][POS_CDMUOR],"CC2->CC2_MUN"))
				cMunDes := "[" + aDados[i][POS_UFDES] + "/" + aDados[i][POS_CDMUDE]+"] " + AllTrim(POSICIONE("CC2",1,XFILIAL("CC2") + aDados[i][POS_UFDES] + aDados[i][POS_CDMUDE],"CC2->CC2_MUN"))
				
				MsgInfo("Os munic�pios " + cMunOri + " e " + cMunDes + " est�o duplicados no arquivo","Duplicidade [Linha " + AllTrim(STR(i+1)) + "/" + AllTrim(STR(j+1)) + "]")
				
				bSair := .T.
				exit
			EndIf    
		Next j
		
		If bSair
			exit
		EndIf
	next i 
Return bRet