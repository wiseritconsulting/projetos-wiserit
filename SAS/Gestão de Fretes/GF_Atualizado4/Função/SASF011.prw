#INCLUDE "rwmake.ch"
#Include "Protheus.Ch"
#Include "TopConn.Ch"

//Seq Importa��o
//Modal
//
*---------------------------
User Function SASF011(_nOpc)
*---------------------------
	Local aArea	:= GetArea()

	Local nOpca	   		:= 0
	Local bOk    		:= {||nOpca:=1,oDlg:End()}
	Local bCancel		:= {||nOpca:=2,oDlg:End()}
	Local aExibe		:= {}
	Local aAltera		:= {}
	Local nRecno		:= 0
	Local _cNrImp 		:= ""
	Private _nRot		:= _nOpc
	Private cAlias 		:= "ZB8"
	Private oDlg
	Private oFolder
	Private oProd
	Private aObjects  	:= {}
	Private aInfo     	:= {}
	Private aPosGet   	:= {}
	Private aPosObj   	:= {}
	Private acol	 	:= {}
	Private cChaveZB8	:= ZB8->ZB8_FILIAL+ZB8->ZB8_NRIMP
	
	If _nOpc == 4 .AND. ZB8->ZB8_ORIGEM <> '1'
		MsgAlert("Esta op��o s� permite alterar Notas Fiscais de Servi�o (NFS-e)")
		Return
	EndIf
	
	If _nOpc == 2 .AND. ZB8->ZB8_ORIGEM <> '1'
		MsgAlert("Esta op��o s� permite Visualizar Notas Fiscais de Servi�o (NFS-e)")
		Return
	EndIf

	dbSelectArea("ZB8")
	RegToMemory("ZB8",.T.)
                                                                              
	IF _nRot == 2 .or. _nRot == 4 // Visualizar ou Alterar
		dbSelectArea("SX3")
		SX3->(dbSetOrder(1))
		SX3->(dbGotop())
		IF SX3->(dbSeek(cAlias))
			While SX3->(!Eof()) .and. SX3->X3_ARQUIVO == cAlias
				If SX3->X3_CONTEXT <> "V"
					If (nX := (cAlias)->(FieldPos(SX3->X3_CAMPO))) > 0
						cCpo := "ZB8->"+SX3->X3_CAMPO
						If TYPE(cCpo)==SX3->X3_TIPO
							M->&(SX3->X3_CAMPO) := &(cCpo) //	M->(FieldPut(nX,&cCpo))
						EndIf
					EndIf
				EndIf
				SX3->(DbSkip())
			EndDo
		ENDIF
	ENDIF



	aSizeAut 	 := MsAdvSize()

	aAdd(aObjects,{100,050,.T.,.F.})
	aAdd(aObjects,{100,050,.T.,.F.})
	aAdd(aObjects,{100,050,.T.,.T.})

	aInfo 	:= {aSizeAut[1],aSizeAut[2],aSizeAut[3],aSizeAut[4],3,3}
	aPosObj := MsObjSize(aInfo,aObjects)
	aPosGet := MsObjGetPos((aSizeAut[3]-aSizeAut[1]),315,{{004,024,240,470}} )


//�Montagem da tela que sera apresentada para usuario (lay-out)            �
	Define MsDialog oDlg From aSizeAut[7],aSizeAut[1] TO aSizeAut[6],aSizeAut[5] Title OemToAnsi('Inclus�o Manual - GEST�O DE FRETES') Of oMainWnd Pixel

	EnChoice(cAlias,nRecno,_nRot, , , ,/*aExibe*/,{15,1,260,650},/*aAltera*/,3,,,,,.T.)//aAltera


	Activate MsDialog oDlg On Init EnchoiceBar(oDlg,{|| nOpca:=if(.T.,1,0),if(nOpcA==1,oDlg:End(),Nil) },bCancel) Centered


	If nOpca == 1
		IF _nRot == 3 .OR. _nRot == 4
			GravaZB8(_nRot)
			_cNrImp := ZB8->ZB8_NRIMP
			LimpaAlc(_cNrImp)
			U_mkAlcNoOc(ZB8->ZB8_NRIMP)
			chgStatus(_cNrImp)
		ELSE
			NIL
		ENDIF
	else
		IF _nRot == 3
			SZ8->(rollbacksx8())
		ENDIF
	EndIf

	RestArea(aArea)
Return

/*
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$$$ �Programa GravaZB8	Autor� FranciscoData� 28/08/2016		   $$$
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$$$ Descri��o: Grava na Tabela ZB8 (Nfs-e de servi�o)			   $$$
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
*/

STATIC FUNCTION GravaZB8(_nRot)
	Local i			:= 0
	Local lIncAlt	:= IIF(_nRot==3,.T.,.F.)
	Local _aCampos	:= {}
	Local nX		:= 0
	Local cCpo		:= ""
	Begin Transaction
		dbSelectArea("SX3")
		SX3->(dbSetOrder(1))
		SX3->(dbGotop())
		IF SX3->(dbSeek(cAlias))
			dbSelectArea("ZB8")
			ZB8->(dbSetOrder(1))
			ZB8->(dbGotop())
			ZB8->(dbSeek(cChaveZB8))
			RecLock(cAlias,lIncAlt)
			While SX3->(!Eof()) .and. SX3->X3_ARQUIVO == cAlias
				If SX3->X3_CONTEXT <> "V"
					If (nX := (cAlias)->(FieldPos(SX3->X3_CAMPO))) > 0
						cCpo := "M->"+SX3->X3_CAMPO
						If TYPE(cCpo)==SX3->X3_TIPO
							(cAlias)->(FieldPut(nX,&cCpo))
						EndIf
					EndIf
				EndIf
				SX3->(DbSkip())
			EndDo
			(cAlias)->(MsUnlock())
	   //	ZB8->(CONFIRMSXE())	// Confirma o n�mero alocado atrav�s do �ltimo comando GETSXENUM()	
		ENDIF
		
	End Transaction

RETURN

Static Function chgStatus(cNrImp)
	Local xArea := getArea()
	
	dbSelectArea("ZB8")
	ZB8->(dbSetOrder(1))
	If ZB8->(dbSeek(xFilial("ZB8") + cNrImp))
		RecLock("ZB8", .F.)
			ZB8->ZB8_STATUS := 'AG'
		ZB8->(MsUnLock())
	EndIf
	ZB8->(dbCloseArea())
	
	RestArea(xArea)
Return

Static Function LimpaAlc(cNrImp)
	Local xArea := getArea()
	
	dbSelectArea("ZBF")
	ZBF->(dbSetOrder(1))
	If ZBF->(dbSeek(xFilial("ZBF") + cNrImp))
		While !(ZBF->(EOF())) .AND. ZBF->ZBF_NRIMP == cNrImp 
			RecLock("ZBF", .F.)
				ZBF->(dbDelete())
			ZBF->(MsUnLock())
			
			ZBF->(dbSkip())
		EndDo
	EndIf
	ZBF->(dbCloseArea())
	
	RestArea(xArea)
Return