#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "XMLXFUN.CH"
#include "FILEIO.CH"


#DEFINE DIRXML  "XMLNFE\"
#DEFINE DIRALER "NEW\"
#DEFINE DIRLIDO "OLD\"
#DEFINE DIRERRO "ERR\"
#DEFINE TOTVS_COLAB_ONDEMAND 3100
#DEFINE SB2  Replicate(' ', 2 )
#DEFINE SB4  Replicate(' ', 4 )
#DEFINE SB6  Replicate(' ', 6 )
#DEFINE SB8  Replicate(' ', 8 )
#DEFINE SB15 Replicate(' ', 15)

//-------------------------------------------------------------------
/*/{Protheus.doc} GFEA115PS

Rotina de Pos valida��o
Uso Geral.

@type function
@author Sam Barros
@since 08/07/16
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function SF005PS(oModel, oView)
	Local nOpc := (oModel:GetOperation())

	If nOpc == MODEL_OPERATION_UPDATE .And. oView:lModify
		RecLock("ZB8",.F.)
		ZB8->ZB8_ALTER := "1"
		MsUnlock("ZB8")
	EndIf

Return .F.

//-------------------------------------------------------------------
/*/{Protheus.doc} SF005EXC
Elimina��o dos registros importados selecionados

@type function
@author Sam Barros
@since 08/07/16
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SF005EXC(cSeqImp)
	Local xArea := getArea()
	
	dbSelectArea("ZB8")
	dbGoTop()
	dbSetorder(1)
	If dbSeek(xFilial("ZB8") + cSeqImp)
		U_SF005DEL(cSeqImp)
		
		RecLock("ZB8", .F.)
		dbDelete()
		MsUnlock("ZB8")
	EndIf
	
	ZB8->(dbCloseArea())
	
	RestArea(xArea)
Return

User Function SF005DEL(cSeqImp)
	Local yArea := getArea()
	
	dbSelectArea("ZB9")
	//dbSeek(ZB8->ZB8_FILIAL + ZB8->ZB8_NRIMP)
	dbSeek(xFilial("ZB9") + cSeqImp )
	While !ZB9->( EOF() ) .AND.;
			ZB9->ZB9_FILIAL = ZB8->ZB8_FILIAL .AND. ;
			ZB9->ZB9_NRIMP  = ZB8->ZB8_NRIMP

		RecLock("ZB9", .F.)
		dbDelete()
		MsUnlock("ZB9")

		dbSelectArea("ZB9")
		ZB9->(dbSkip())
	EndDo
	
	dbSelectArea("ZBC")
	dbSeek(xFilial("ZBC") + cSeqImp )
	While !ZBC->( EOF() ) .AND.;
			ZBC->ZBC_FILIAL = ZB8->ZB8_FILIAL .AND. ;
			ZBC->ZBC_NRIMP  = ZB8->ZB8_NRIMP

		RecLock("ZBC", .F.)
		dbDelete()
		MsUnlock("ZBC")

		dbSelectArea("ZBC")
		ZBC->(dbSkip())
	EndDo
	
	dbSelectArea("ZBE")
	dbSeek(xFilial("ZBE") + cSeqImp )
	While !ZBE->( EOF() ) .AND.;
			ZBE->ZBE_FILIAL = ZB8->ZB8_FILIAL .AND. ;
			ZBE->ZBE_NRIMP  = ZB8->ZB8_NRIMP

		RecLock("ZBE", .F.)
		dbDelete()
		MsUnlock("ZBE")

		dbSelectArea("ZBE")
		ZBE->(dbSkip())
	EndDo
	
	dbSelectArea("ZBF")
	dbSeek(xFilial("ZBF") + cSeqImp )
	While !ZBF->( EOF() ) .AND.;
			ZBF->ZBF_FILIAL = ZB8->ZB8_FILIAL .AND. ;
			ZBF->ZBF_NRIMP  = ZB8->ZB8_NRIMP

		RecLock("ZBF", .F.)
		dbDelete()
		MsUnlock("ZBF")

		dbSelectArea("ZBF")
		ZBF->(dbSkip())
	EndDo
	
	RestArea(yArea)
Return

//-------------------------------------------------------
//Fun��o SchedDef
//-------------------------------------------------------
Static Function SchedDef()
	Local aparam := {}
	Local aOrd   := {}

	aparam := {;
		"P"                                 	,;  // Tipo: R para relatorio P para processo
	PadR( "PARAMDEF", Len(SX1->X1_GRUPO) ) 	,;  // Pergunte do relatorio, caso nao use passar "PARAMDEF"
	"ZB8"                               	,;  // Alias
	aOrd                                	,;  // Array de ordens
	}
Return aParam

//-------------------------------------------------------------------
/*/{Protheus.doc} SF005IMP

Chamada para importa��o

@type function
@author Sam Barros
@since 08/07/16
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SF005IMP()

	Private nDupl := 0
	Private nQtCTE := 0
	Private bDelCte := .F.
	Private cDevol := 'N'
 
	AutoGrLog( Replicate( "-", 77 ) )
	If IsBlind()
		SASF0051P1()
	Else
		Processa({|| SASF0051P1()},"Importando arquivos", "")
	EndIf
	
	AutoGrLog(CRLF)
	AutoGrLog("-------------------Quantidade Total de Registros importados: " + AllTrim(STR(nQtCTE)))
	AutoGrLog(CRLF)
	AutoGrLog("-------------------Quantidade de Registros j� importados: " + AllTrim(STR(nDupl)))

Return .T.
//-------------------------------------------------------------------
/*/{Protheus.doc} SASF0051P1

Verica o caminho que esta sendo importado, e se � um arquivo nao valido
coloca o nome do arquivo num relat�rio de erro ao final da importa��o

@type function
@author Sam Barros
@since 08/07/16
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function SASF0051P1()
	Local cXMLDir
	
	Local xArea := getArea()

	//TC 2.0
	Local lTC20 		:= u_GA118TC20() //Indica se Totvs Colabora��o 2.0
	Local cDir20
	Local cDirLido20

	Local nCont
	Local aProc		:= {} , aErros := {}

	Private GFEResult	:= GFEViewProc():New()
	Private lImportaCte	:= .F.

	If lTC20
		If ((!FindFunction("FWLSEnable") .Or. !FWLSEnable(TOTVS_COLAB_ONDEMAND)) .And. !FwEmpTeste())
			Help( ,, 'HELP',, "Ambiente n�o licenciado para o modelo TOTVS Colabora��o 2.0.", 1, 0,)
			Return
		EndIf

		cDir20		:= SF005Bar(AllTrim(GetNewPar("MV_NGINN","\NeoGrid\IN\")))
		cDirLido20	:= SF005Bar(AllTrim(GetNewPar("MV_NGLIDOS","\NeoGrid\LIDOS\")))

		If !GA118CrDir(cDir20)
			Return
		EndIf
		If !GA118CrDir(cDirLido20)
			Return
		EndIf

		aDirImpor := DIRECTORY(AllTrim(cDir20) + "214*.XML" ) //somente arquivos de recebimento de CTe
		aDirCanc  := DIRECTORY(AllTrim(cDir20) + "384*.XML" ) //somente arquivos de cancelamento de CTe por evento
		For nCont := 1 to Len(aDirCanc)
			AADD(aDirImpor, aDirCanc[nCont])
		Next aDirImpor

		If Len(aDirImpor) < 1
			Help( ,, 'HELP',, "N�o foram encontrados arquivos XML v�lidos no diret�rio " + cDir20 + ".", 1, 0,)
			Return
		EndIf
	Else
		//Verifica e cria, se necess�rio, a estrutura de diret�rios
		cXMLDir := SF005Bar(AllTrim(SuperGetMv("MV_XMLDIR", .F., "")))

		If Empty(cXMLDir)
			Help( ,, 'HELP',, "N�o foi especificado um diret�rio para importa��o no par�metro " + cParDir + ".",1,0)
			Return
		Else
			If !GA118CrDir(cXMLDIR)
				Return
			EndIf
			If !GA118CrDir(cXMLDIR + DIRLIDO)
				Return
			EndIf
			If !GA118CrDir(cXMLDIR + DIRERRO)
				Return
			EndIf
		EndIf

		aDirImpor := DIRECTORY(Alltrim(cXMLDir) + "*.XML" )
		If Len(aDirImpor) < 1
			Help( ,, 'HELP',, "N�o foram encontrados arquivos XML no diret�rio " + cXMLDir + ".", 1, 0,)
			Return
		Endif
	EndIf

	If !IsBlind()
		ProcRegua(0)
	EndIf

	For nCont := 1 to Len(aDirImpor)
		IIf(lTC20, cXMLArq := cDir20 + aDirImpor[nCont][1], cXMLArq := cXMLDIR + aDirImpor[nCont][1])

		//Quando a tabela ZB8 for compartilhada o programa rodar� normalmente.
		//Quando a tabela ZB8 for Exclusiva s� ser� feita a importa��o dos arquivos que forem da
		//filial corrente, os arquivos que n�o forem da filial corrente n�o ser�o alterados.
		//Para TC 2.0:
		//Quando executado via SASF005, o arquivo dever� existir. Assim, o SASF005 mover� o arquivo para pasta lidos.
		//Quando executado via ComXCol, o Job ColAutoRead "consome" o arquivo, criando um registro na
		//tabela CKO, e move o arquivo para pasta lidos. O Job SchdComCol far� a leitura dos registros dessa tabela,
		//para ent�o chamar o SASF005, que n�o ir� mover o arquivo, apenas criar os registros ZB8 e ZB9.

		//S� retornar� falso quando o arquivo for inv�lido
		If !u_SF005CHA(cXMLArq,@aProc,@aErros)
			If lTC20
				GA118MoveFile(cXMLArq, cDirLido20 + aDirImpor[nCont][1])
			Else
				GA118MoveFile(cXMLArq, cXMLDIR + DIRERRO + aDirImpor[nCont][1])
			EndIf
		EndIf

		If !IsBlind()
			IncProc(aDirImpor[nCont][1])
		EndIf
	Next nCont

/*
	If Len(cXMLArq) > 0 .And. !lImportaCte
		MsgInfo("N�o foram encontrados arquivos para a filial corrente. Para importar"+;
		" arquivos de todas as filiais altere o modo de acesso de filiais das tabelas ZB8 e ZB9 para compartilhado.")
	EndIf
*/
	If !IsBlind() .And. !Empty(aErros)
		For nCont := 1 To Len(aErros)
			GFEResult:AddErro("Arquivo: " + aErros[nCont][1] + CRLF + aErros[nCont][2])
			GFEResult:AddErro(Replicate("-",50) + CRLF)
		Next nCont

		//GFEResult:AddErro("Ocorreram erros na importa��o de um ou mais arquivos. Poss�veis motivos:" + CRLF + "- Erros nos arquivos XML;" + CRLF + "- Arquivos incompat�veis com o formato XML;" + CRLF + "- Chave de CTE j� importada/processada." + CRLF)
		//GFEResult:Show("Importa��o de arquivos Ct-e", "Arquivos", "Erros", "Clique no bot�o 'Erros' para mais detalhes.")
	EndIf
	
	RestArea(xArea)
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SF005CHA

Verifica o caminho que est� sendo importado, verifica se � valido ou nao se
for continua o processo se nao vai para lista de arquivos com erro.

@param cXMLFile, caminho do arquivo que esta sendo importado
@param aErros, armazena o caminho dos aquivos com erro
@param aProc, array para guardar os arquivos processados (M-Mess)

@type function
@author Sam Barros
@since 08/07/16
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SF005CHA(cXMLFile,aProc,aErros)
	Local cError   	:= ""
	Local cWarning 	:= ""
	Local nHandle 	:= 0
	Local lRet			:= .T.
	Default aProc   	:= {}
	Default aErros  	:= {}
	Private oXML 		:= NIL
	Private cBuffer 	:= ''
	Private nSize

	nHandle := FOpen(cXMLFile,FO_READ+FO_SHARED) //Parametros: Arquivo, Leitura - Escrita, Servidor
	If nHandle < 0
		Return .F.
	EndIf

	nSize := FSeek(nHandle,FS_SET,FS_END)
	FSeek(nHandle,0)
	FRead(nHandle,@cBuffer,nSize)

	oXML  := XmlParser( cBuffer , "_", @cError, @cWarning)
	FClose(nHandle)
	nHandle   := -1

	If ValType(oXML)=="O"
		If ValType(XmlChildEx(oXML,"_CTEPROC")) == "O" //-- Arquivo de RETORNO de Nota de transporte
			u_SF005XML(cXMLFile,@aProc,@aErros,oXML:_CTeProc:_Cte,.F.)
		ElseIf ValType(XmlChildEx(oXML,"_ENVICTE")) == "O" //-- Arquivo de Evento Cte
			u_SF005XML(cXMLFile,@aProc,@aErros,oXML:_enviCTe:_Cte,.F.)
		ElseIf ValType(XmlChildEx(oXML,"_CTE")) == "O" //-- Arquivo de REMESSA de Nota de transporte
			u_SF005XML(cXMLFile,@aProc,@aErros,oXML:_Cte,.F.)
		ElseIf ValType(XmlChildEx(oXML,"_PROCEVENTOCTE")) == "O" //-- Arquivo de RETORNO de Evento Cte/Cancelamento
			u_SF005XML(cXMLFile,@aProc,@aErros,oXML:_procEventoCTe,.F.)
		Else
			If cError = ''
				cError := 'Arquivo com tag principal inv�lida.'
			EndIf

			aAdd(aErros,{cXMLFile,"Erro >> Arquivo: " + cError + CHR(13)+CHR(10), ""})
			lRet := .F.
		EndIf
	EndIf
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} SF005XML

Valida��es do arquivo Cte

@param cXMLFile, caminho do arquivo que esta sendo importado
@param aErros, armazena o caminho dos aquivos com erro
@param aProc, array para guardar os arquivos processados (M-Mess)
@param oXml:, Objeto XML do CTE
@param lTotvsColab:, se a chamada da fun��o vem do COMXCOL (importa��o via TSS;Totvs Colabora��o)


@type function
@author Sam Barros
@since 08/07/16
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SF005XML(cXMLFile,aProc,aErros, oXML, lTotvsColab)
	Local aZB8            := Array(49)
	Local aZB9            := {}
	Local nCont, cFilEmit, cFilVal := ''
	Local nCountZerosNF   := 0
	Local lNrDf           := .F.
	Local nX              := 0
	Local nNF             := 0
	Local oInfNFE         // Tag infNFe
	Local oInfNF          // Tag infNF
	Local aVldNF          := {} // Hierarquia de valida��o da tag NF
	Local cComp           := ""
	Local aTemp           := {}
	Local cChaveRel       := ""
	Local aOriDoc         := {'','',.F.} // Uso da substitui��o cte
	Local cCgcRem         := ""
	Local lApuIcm         := .F.
	Local nPossuiIcms     := 0
	Local dvlIcmsTRet     := 0
	Local dvCred          := ""
	Local codCidIni       := 0
	Local codCidFim       := 0
	Local aRetICMS        := Array(4)
	Local cVTPrest        := ''
	Local cVRec           := ''
	Local nVlTaxas		 := 0
	Local cCliDes			 := ''
	Local cCliIdFed		 := ''
	Local cCliDesFed		 := ''
	Local lFindGW1
	Local lGravaZB8		 := .T.
	Local cEmi
	Local cLock
	Local cLockTime

	Local cDoc := ""
	Local cSerie := ""
	Local cObs := ""
	Local lTC20 			:= u_GA118TC20() //Indica se Totvs Colabora��o 2.0

	Local lRetFuncao     := .T. //retorno desta fun��o.
	Local lMoveArq	 	:= .T. //indica se dever� ser feita movimenta��o (c�pia/exclus�o) do arquivo.
	Local lProcArq		:= .T. //indica se deve continuar o processamento do arquivo. Serve para, mesmo ap�s processar o arquivo, gravar logs e eliminar vari�veis da mem�ria
	Local lCteOutFil		:= .F. //indica se CTe � de outra filial.

	Local aRetDocTrechoPago[2]

	Local nTipoXML


	Private cTpCte			:= ""
	
	Private cCdMunIni 	:= ""
	Private cDsMunIni   := ""
	Private cUfIni 		:= ""
	
	Private cCdMunFim	:= ""
	Private cDsMunFim	:= ""
	Private cUfFim 		:= ""
	
	Default lTotvsColab		:= .T. // Parametro n�o � informado passado na chamada pela fun��o COMXCOL (Materiais)

	Default aProc				:= {}
	Default aErros			:= {}
	Private cMsgPreVal		:= ''
	Private cMsgAux
	Private oCTe
	Private cDIROK
	Private cDirDest

	Private GFELog118		:= GFELog():New("SASF005", "CTe Importa��o", SuperGetMV('MV_GFEEDIL',,'1'))

	oCTe  :=  oXML

	GFELog118:Add('HORA         MENSAGEM')
	GFELog118:Add(Replicate('-', 12) + ' ' + Replicate('-',107))
	
	cLock := 'SASF005_' + GA118ExtDirOrFileName(cXMLFile,2) //inclui SASF005_ para evitar uma poss�vel concorr�ncia com outros programas

//Inclui sem�foro para evitar de um arquivo ser importado mais de uma vez por processos executados em paralelo.
	If LockByName(cLock, .F., .F.)
		GFELog118:Add(GFENOW(.F.,,,':','.') + ' - Cria��o de sem�foro para o arquivo ' + AllTrim(GA118ExtDirOrFileName(cXMLFile,2)) + '.')
	Else
	//1 de 2 pontos desta fun��o em que o log � encerrado e retorna sem continuar at� o fim da fun��o.
		GFELog118:Add(GFENOW(.F.,,,':','.') + ' - Arquivo ' + AllTrim(GA118ExtDirOrFileName(cXMLFile,2)) + ' j� est� sendo importado.')
		GFELog118:EndLog()
		Return .F.
	EndIf

//Inclusa esta verifica��o pois h� casos, quando processado via schedule/ComXCol, em que duas threads importam
//o mesmo arquivo com uma pequena diferen�a de tempo. Mesmo ap�s a primeira gravar o registro e remover o 
//sem�foro, a segunda cria o sem�foro mas n�o enxerga o registro na tabela ZB8 criado pela primeira.
//Assim, optou-se por criar mais um controle por vari�vel global e atraso de processamento. Somente ap�s
//determinado tempo um arquivo importado poder� ser importado novamente.

	ClearGlbValue('SASF005*', 10) //Limpa todas as vari�veis criadas por esta rotina que foram acessadas pela �ltima vez h� mais de 10s.

	cLockTime := GetGlbValue(cLock)
	If Vazio(cLockTime) .Or. Val(GFENow(.T.,,'','','')) - Val(cLockTime) > 3000
		PutGlbValue(cLock, GFENow(.T.,,'','',''))
		GFELog118:Add(GFENOW(.F.,,,':','.') + ' - Cria��o de vari�vel de controle de importa��o para ' + AllTrim(GA118ExtDirOrFileName(cXMLFile,2)) + '.')
	Else
	//2 de 2 pontos desta fun��o em que o log � encerrado e retorna sem continuar at� o fim da fun��o.
		GFELog118:Add(GFENOW(.F.,,,':','.') + ' - Arquivo ' + AllTrim(GA118ExtDirOrFileName(cXMLFile,2)) + ' em processo de importa��o.')
		GFELog118:EndLog()
		UnLockByName(cLock, .F., .F.) //Tira o lock caso contr�rio numa poss�vel importa��o posterior ocasionar� trava indevida pelo sem�foro.
		Return .F.
	EndIf

	GFELog118:Add(GFENOW(.F.,,,':','.') + SB2 + ' - In�cio da importa��o do arquivo.')

	If ExistBlock("SASF0053")
	// quando retornar .F. o processo deve ser abortado.
		lRetFuncao := ExecBlock("SASF0053",.F.,.F.,{oCTE})
		If !lRetFuncao
			GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - Processo cancelado por chamada espec�fica SASF0053. Arquivo n�o importado.')
			lProcArq := .F.
		EndIf
	EndIf

//Se for Evento de transporte, desvia para o tratamento
//Atualmente � tratado somente o tipo evento de cancelamento
	If lProcArq
		If ValType(XmlChildEx(oCTe,"_EVENTOCTE")) == "O"
			nTipoXML := 3 //Arquivo de evento de cancelamento

			If !ValidSIX("ZB8","5") .Or. !ValidSIX("GW3","E")
				If !IsBlind()
					MsgInfo("Para efetuar o cancelamento do CT-e, faz-se necess�rio aplica��o do Update U_GFE11I37")
				EndIf

				GFELog118:Add(GFENOW(.F.,,,':','.') + ' - Falta aplica��o do Update U_GFE11I37. Arquivo n�o importado.')
				lProcArq := .F.
				lMoveArq := .F.
			EndIf

			If lProcArq
				lRetFuncao := u_SF005ANU(cXMLFile,aProc,@cMsgPreVal, oCTE, lTotvsColab,XmlValid(oCTE,{"_EVENTOCTE","_INFEVENTO"},"_TPEVENTO"))[3]
				lProcArq := .F.
			EndIf
		EndIf
	EndIf

	If lProcArq
		cTpCte 		:= XmlValid(oCTE,{"_INFCTE","_IDE"},"_TPCTE")
		
		cCdMunIni 	:= XmlValid(oCTE,{"_INFCTE","_IDE"},"_CMUNINI")
		cDsMunIni   := XmlValid(oCTE,{"_INFCTE","_IDE"},"_XMUNINI")
		cUfIni 		:= XmlValid(oCTE,{"_INFCTE","_IDE"},"_UFINI")
		
		cCdMunFim	:= XmlValid(oCTE,{"_INFCTE","_IDE"},"_CMUNFIM")
		cDsMunFim	:= XmlValid(oCTE,{"_INFCTE","_IDE"},"_XMUNFIM")
		cUfFim 		:= XmlValid(oCTE,{"_INFCTE","_IDE"},"_UFFIM")
		
		cCdMunIni 	:= SUBSTR(cCdMunIni,3,len(cCdMunIni))
		cCdMunFim 	:= SUBSTR(cCdMunFim,3,len(cCdMunFim))

		If XmlChildEx(oCTE:_INFCTE,"_COMPL") != Nil .And. XmlChildEx(oCTE:_INFCTE:_COMPL,"_XOBS") != Nil
			cObs := XmlValid(oCTE,{"_INFCTE","_COMPL"},"_XOBS")
		EndIf

	// Se for Ct-e de anula��o ou substitui��o, desvia para o tratamento de anula��o/substitui��o
		If cTpCte $ '2;3'
			nTipoXML := 2 //Arquivo de CTe de anula��o/substitui��o

			If !ValidSIX("ZB8","5") .Or. !ValidSIX("GW3","E")
				If !IsBlind()
					MsgInfo("Para efetuar a anula��o/substitui��o do CT-e, faz-se necess�rio aplica��o do Update U_GFE11I37")
				EndIf

				lProcArq := .F.
				lMoveArq := .F.
				lRetFuncao := .F.
			EndIf

			If lProcArq
				aOriDoc := u_SF005ANU(cXMLFile,aProc,@cMsgPreVal, oCTE, lTotvsColab,cTpCte)
				If cTpCte == '2'
					lProcArq := .F.
					lRetFuncao := aOriDoc[3]
				EndIf
			EndIf
		EndIf
	EndIf

	If lProcArq
		nTipoXML := 1 //Arquivo de CTe normal
		
		nQtCTE++

		GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - In�cio da valida��o do conte�do do arquivo.')

	//
	// Verifica��o para manter compatibilidade das vers�es.
	// Hierarquia das tags infNFe e infNF mudou na vers�o 2.0 do CT-e
	//
		If oCTe:_INFCTE:_VERSAO:TEXT >= "2.00"
		// Tag _INFCTENORM n�o existe em arquivos Ct-e de copmlemento de valores e anula��o de valores
			If cTpCte $ "0;3"
				oInfDoc := XmlChildEx(oCTe:_INFCTE:_INFCTENORM, "_INFDOC")
				If (oInfDoc != nil)
					oInfNFE := oCTe:_INFCTE:_INFCTENORM:_INFDOC
					oInfNF  := oCTe:_INFCTE:_INFCTENORM:_INFDOC
				Else
					cMsgPreVal += "- "+"O arquivo CT-e importado n�o � v�lido."+CRLF
					cMsgPreVal += "- "+"A tag _INFDOC n�o foi encontrada."+CRLF
				EndIf
			EndIf

			aVldNF  := {"_INFCTE","_INFCTENORM","_INFDOC","_INFNF"}
		Else
		// Vers�o 1.04
			oInfNFE := oCTe:_INFCTE:_REM
			oInfNF  := oCTe:_INFCTE:_REM
			aVldNF  := {"_INFCTE","_REM","_INFNF"}
		EndIf

		aZB8[2] :=  GetMv("MV_ESPDF3") //ZB8->ZB8_CDESP - (Especie de Documento)

	//Valida��o de Emissor
		If XmlValid(oCTe,{"_INFCTE","_EMIT","_CNPJ"},"TEXT",.T.)== 'CNPJ'
			cEmi := ValidEmis(3,xFilial("SA2")+XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CNPJ"))
		Else
			cEmi := ValidEmis(3,xFilial("SA2")+XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CPF"))
		EndIf

		If Empty(cEmi)
			If XmlValid(oCTe,{"_INFCTE","_EMIT","_CNPJ"},"TEXT",.T.)== 'CNPJ'
				cMsgPreVal += "- " + "Transportador n�o encontrado com o CNPJ: " + XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CNPJ") + CRLF
			Else
				cMsgPreVal += "- " + "Transportador n�o encontrado com o CPF: " + XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CPF") + CRLF
			EndIf
			aZB8[3] := ""
		Else
			aZB8[3] := cEmi  //ZB8->ZB8_EMISDF - (Emissor)
			If (GU3->GU3_CTE == "2" .Or. Empty(GU3->GU3_CTE)) .And. GU3->GU3_TRANSP == "1" //Emitente de transporte posicionado deve emitir CT-e
				RecLock("GU3",.F.)
				GU3->GU3_CTE := "1"
				MsUnlock("GU3")
			EndIf
		EndIf

		aZB8[4] :=  XmlValid(oCTe,{"_INFCTE","_IDE"},"_SERIE")	//ZB8->ZB8_SERDF - (Serie)
		aZB8[5] :=  XmlValid(oCTe,{"_INFCTE","_IDE"},"_NCT")  	//ZB8->ZB8_NRDF	 - (Numero do Conhecimento)

	//Nesse ponto ser� inclusa verifica��o de registro j� existente nas tabelas ZB8.
	//Isso para os casos de execu��o via schedule, em que o COLAUTOREAD l� o arquivo, grava na
	//tabela CKO e exclui o arquivo. Depois, em outro processo, o COMXCOL processa esse registro.
	//Com isso, o controle transacional que antes era feito verificando a exist�ncia do arquivo
	//n�o pode mais ser feito dessa forma.
	//Assim, para evitar a inclus�o de registros repetidos, ser� feita uma busca na tabela
	//antes da inclus�o.
	//Mesmo com a inclus�o do sem�foro, ser� mantida a busca pela chave.
		aZB8[33] := SubStr(oCTe:_INFCTE:_ID:TEXT,4,44)                //ZB8->ZB8_CTE

		bDelCte := .F.
		dbSelectArea("ZB8")
		ZB8->(dbSetOrder(5))
		If ZB8->(dbSeek(aZB8[33]))
			nDupl++
			bDelCte := .T.
			If ZB8->ZB8_ORIGEM == "2"
				AutoGrLog('Conhecimento com a chave "' + aZB8[33] + '" j� foi importado. Registro: ' + ZB8->ZB8_NRIMP + '.')
				aAdd(aErros,{cXMLFile, 'Conhecimento com a chave "' + aZB8[33] + '" j� foi importado. Registro: ' + ZB8->ZB8_NRIMP + '. Arquivo n�o ser� importado.', ""})
			Else
				//GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Conhecimento com a chave "' + aZB8[33] + '" j� foi importado via EDI atrav�s do programa "Importar Conemb". Registro: ' + ZB8->ZB8_NRIMP + '.')
				//GFELog118:Add(SB15 + SB6 + 'Arquivo n�o ser� importado.')
				AutoGrLog('Conhecimento com a chave "' + aZB8[33] + '" j� foi importado. Registro: ' + ZB8->ZB8_NRIMP + '.')
				aAdd(aErros,{cXMLFile, 'Conhecimento com a chave "' + aZB8[33] + '" j� foi importado via EDI atrav�s do programa "Importar Conemb". Registro: ' + ZB8->ZB8_NRIMP + '. Arquivo n�o ser� importado.', ""})
			EndIf
//			Else
		EndIf
		bImporta := .T.
		if SuperGetMV("SA_PGCTEXC", .F., .T.)
			If bDelCte
				If MsgYesNo("Deseja Excluir o CTE anterior: " + AllTrim(ZB8->ZB8_NRDF) + "/" + AllTrim(ZB8->ZB8_SERDF),"Gest�o de Fretes")
					U_SF005EXC(ZB8->ZB8_NRIMP)
				Else
					bImporta := .F.
				EndIf
			Endif
		Else
			U_SF005EXC(ZB8->ZB8_NRIMP)
		EndIf
		If bImporta
		//(Data da Emiss�o) - Formato CTe AAAA-MM-DDTHH:MM:DD
			dDtEmis := StoD( SUBSTRING(XmlValid(oCTe,{"_INFCTE","_IDE"},"_DHEMI"),1,4) + ;
				SUBSTRING(XmlValid(oCTe,{"_INFCTE","_IDE"},"_DHEMI"),6,2) + ;
				SUBSTRING(XmlValid(oCTe,{"_INFCTE","_IDE"},"_DHEMI"),9,2) )
			aZB8[6] := dDtEmis	//ZB8->ZB8_DTEMIS

		//Valida��o do Remetente e Destinat�rio
			If XmlValid(oCTe,{"_INFCTE","_REM","_CNPJ"},"TEXT",.T.) == 'CNPJ'
				cRem := POSICIONE("SA2",3,xFilial("SA2")+XmlValid(oCTe,{"_INFCTE","_REM"}, "_CNPJ")  ,"A2_COD")
			Else
				cRem := POSICIONE("SA2",3,xFilial("SA2")+XmlValid(oCTe,{"_INFCTE","_REM"}, "_CPF")  ,"A2_COD")
			EndIf

			If XmlValid(oCTe,{"_INFCTE","_DEST","_CNPJ"},"TEXT",.T.)== 'CNPJ'
				cDes := POSICIONE("SA1",3,xFilial("SA1")+XmlValid(oCTe,{"_INFCTE","_DEST"},"_CNPJ")  ,"A1_COD")
			Else
				cDes := POSICIONE("SA1",3,xFilial("SA1")+XmlValid(oCTe,{"_INFCTE","_DEST"},"_CPF")  ,"A1_COD")
			EndIf

			If Empty(cRem)
				If XmlValid(oCTe,{"_INFCTE","_REM","_CNPJ"},"TEXT",.T.)== 'CNPJ'
					cMsgPreVal += "- " + "Remetente n�o encontrado com o CNPJ/CPF: " + XmlValid(oCTe,{"_INFCTE","_REM"}, "_CNPJ") + CRLF
				Else
					cMsgPreVal += "- " + "Remetente n�o encontrado com o CNPJ/CPF: " + XmlValid(oCTe,{"_INFCTE","_REM"}, "_CPF") + CRLF
				EndIf
			Else
				If XmlValid(oCTe,{"_INFCTE","_REM","_CNPJ"},"TEXT",.T.)== 'CNPJ'
					aZB8[7] := POSICIONE("SA2",3,xFilial("SA2")+XmlValid(oCTe,{"_INFCTE","_REM"}, "_CNPJ")  ,"A2_COD")  //ZB8->ZB8_CDREM
				Else
					aZB8[7] := POSICIONE("SA2",3,xFilial("SA2")+XmlValid(oCTe,{"_INFCTE","_REM"}, "_CPF")  ,"A2_COD")
				EndIf
			EndIf

			If Empty(cDes)
				If XmlValid(oCTe,{"_INFCTE","_DEST","_CNPJ"},"TEXT",.T.)== 'CNPJ'
					cMsgPreVal += "- " + "Destinat�rio n�o encontrado com o CNPJ/CPF: " + XmlValid(oCTe,{"_INFCTE","_DEST"},"_CNPJ") + CRLF
				Else
					cMsgPreVal += "- " + "Destinat�rio n�o encontrado com o CNPJ/CPF: " + XmlValid(oCTe,{"_INFCTE","_DEST"},"_CPF") + CRLF
				EndIf
			Else
				If XmlValid(oCTe,{"_INFCTE","_DEST","_CNPJ"},"TEXT",.T.)== 'CNPJ'
					aZB8[8] := POSICIONE("SA1",3,xFilial("SA1")+XmlValid(oCTe,{"_INFCTE","_DEST"},"_CNPJ")  ,"A1_COD")  //ZB8->ZB8_CDDEST
				Else
					aZB8[8] := POSICIONE("SA1",3,xFilial("SA1")+XmlValid(oCTe,{"_INFCTE","_DEST"},"_CPF")  ,"A1_COD")
				EndIf
			EndIf

			aZB8[10] :=  DDATABASE  	//ZB8->ZB8_DTENT - (Data de entrada)Data atual
			aZB8[11] :=  '' 				//ZB8->ZB8_CDCONS - Deixar em branco

		//CFOP do Documento
			If  XmlValid(oCTe,{"_INFCTE","_REM" ,"_ENDERREME"},"_UF")  == ;
					XmlValid(oCTe,{"_INFCTE","_DEST","_ENDERDEST"},"_UF")    //Verifica se a UF do Remetente � a mesma do Destinatario
				aZB8[12] := oCTE:_INFCTE:_IDE:_CFOP:TEXT//GetMv("MV_CFOFR1") 	     	  //ZB8->ZB8_CFOP	- Estadual
			Else
				aZB8[12] := oCTE:_INFCTE:_IDE:_CFOP:TEXT//GetMv("MV_CFOFR2")		     //ZB8->ZB8_CFOP - Interestadual
			EndIf

			dbSelectArea("GU3")
			dbSetOrder(1)
			If dbSeek(XFILIAL("GU3") + cEmi)
				If GU3->GU3_APUICM == "4"
					lApuIcm := .T.
				EndIf
			EndIf

		//Tipo de Tributacao
		//1=Tributado;2=Isento/N�o-tributado;3=Subs Tribut�ria;4=Diferido;5=Reduzido;6=Outros;7=Presumido
			If XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS00"},'',.T.) == 'ICMS00' .OR. ;
					XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS20"},'',.T.) == 'ICMS20'       //Tributado
				aZB8[13] := '1' //ZB8->ZB8_TRBIMP - Tipo de Tributacao

				If XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS00"},'',.T.) == 'ICMS00'
					aZB8[14] :=	 XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS00"},"_VICMS")//ZB8->ZB8_VLIMP - Valor do Imposto
					aZB8[15] :=  XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS00"},"_VBC")     //ZB8->ZB8_BASIMP - Base de Calculo Imposto
					aZB8[16] :=  XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS00"},"_PICMS")   //ZB8->ZB8_PCIMP - Aliquota do Imposto
				Else
					aZB8[14] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS20"},"_VICMS") //ZB8->ZB8_VLIMP - Valor do Imposto
					aZB8[15] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS20"},"_VBC")   //ZB8->ZB8_BASIMP - Base de Calculo Imposto
					aZB8[16] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS20"},"_PICMS") //ZB8->ZB8_PCIMP - Aliquota do Imposto
				EndIf
			ElseIf  XmlValid(oCTe , {"_INFCTE","_IMP","_ICMS","_ICMS40"} , '' , .T. ) == 'ICMS40'  .OR.;//Isento/N�o Tributado
				XmlValid(oCTe , {"_INFCTE","_IMP","_ICMS","_ICMS41"} , '' , .T. ) == 'ICMS41'  .OR.;
					XmlValid(oCTe , {"_INFCTE","_IMP","_ICMS","_ICMS45"} , '' , .T. ) == 'ICMS45' .OR.;
					XmlValid(oCTe , {"_INFCTE","_IMP","_ICMS","_ICMSSN"} , '' , .T. ) == 'ICMSSN'
				aZB8[13] := '2' 	//ZB8->ZB8_TRBIMP - Tipo de Tributacao
			ElseIf XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS51"},'',.T.) == 'ICMS51' //Diferido
				aZB8[13] := '4' 	//ZB8->ZB8_TRBIMP - Tipo de Tributacao
				aZB8[14] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS51"},"_VICMS")  //ZB8->ZB8_VLIMP - Valor do Imposto
				aZB8[15] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS51"},"_VBC")    //ZB8->ZB8_BASIMP - Base de Calculo Imposto
				aZB8[16] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS51"},"_PICMS")  //ZB8->ZB8_PCIMP - Aliquota do Imposto
			ElseIf  XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS60"},'',.T.) == 'ICMS60' .And. lApuIcm   //Subst. Tributaria e apura��o do ICMS por parte do emitente do documento de frete igual a presumido
				aZB8[13] := '7' 	//ZB8->ZB8_TRBIMP - Tipo de Tributacao
				aZB8[14] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS60"},"_vICMSSTRet") //ZB8->ZB8_VLIMP - Valor do Imposto
				aZB8[15] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS60"},"_vBCSTRet")   //ZB8->ZB8_BASIMP - Base de Calculo Imposto
				aZB8[16] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS60"},"_pICMSSTRet") //ZB8->ZB8_PCIMP - Aliquota do Imposto
				If aZB8[16] == '0.00'
					codCidIni   := XmlValid(oCTe,{"_INFCTE","_IDE"},"_cMunIni")
					codCidFim   := XmlValid(oCTe,{"_INFCTE","_IDE"},"_cMunFim")
					aRetICMS := GFEFnIcms(aZB8[03], ; // C�digo do transportador
					aZB8[07], ; // C�digo do remetente
					aZB8[08],; // C�digo do destinatario
					codCidIni,; // N�mero da cidade de origem
					codCidFim,; // N�mero da cidade de destino
					"0", ; // Forma de utiliza��o da mercadoria
					"0", ; // Tipo de item
					"0", ; // Classifica��o de frete
					"1", ; // Mercadoria � tributada de ICMS?
					"0",; //Tipo de Opera��o do Agrupador do Documento de Carga
					xFilial("ZB8")) // Filial do c�lculo - Usado no par�metro MV_GFECRIC para as exce��es das filiais que n�o tem direito a cr�dito

				
					aZB8[16] := (Transform((aRetICMS[1]), '@R 99.99'))  // Retorno do valor do percentual de ICMS
					If aZB8[14] == "0.00"
						aZB8[14] := CValToChar(VAL(aZB8[15]) * VAL(aZB8[16]) / 100)
					EndIf
				Endif
		
			ElseIf  XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS60"},'',.T.) == 'ICMS60'   //Subst. Tributaria
				aZB8[13] := '3' 	//ZB8->ZB8_TRBIMP - Tipo de Tributacao
				aZB8[14] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS60"},"_vICMSSTRet") //ZB8->ZB8_VLIMP - Valor do Imposto
				aZB8[15] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS60"},"_vBCSTRet")   //ZB8->ZB8_BASIMP - Base de Calculo Imposto
				aZB8[16] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS60"},"_pICMSSTRet") //ZB8->ZB8_PCIMP - Aliquota do Imposto
				If aZB8[16] == '0.00'
					codCidIni   := XmlValid(oCTe,{"_INFCTE","_IDE"},"_cMunIni")
					codCidFim   := XmlValid(oCTe,{"_INFCTE","_IDE"},"_cMunFim")
					aRetICMS := GFEFnIcms(aZB8[03], ; // C�digo do transportador
					aZB8[07], ; // C�digo do remetente
					aZB8[08],; // C�digo do destinatario
					codCidIni,; // N�mero da cidade de origem
					codCidFim,; // N�mero da cidade de destino
					"0", ; // Forma de utiliza��o da mercadoria
					"0", ; // Tipo de item
					"0", ; // Classifica��o de frete
					"1", ; // Mercadoria � tributada de ICMS?
					"0",; //Tipo de Opera��o do Agrupador do Documento de Carga
					xFilial("ZB8")) // Filial do c�lculo - Usado no par�metro MV_GFECRIC para as exce��es das filiais que n�o tem direito a cr�dito

				
					aZB8[16] := (Transform((aRetICMS[1]), '@R 99.99'))  // Retorno do valor do percentual de ICMS
					If aZB8[14] == "0.00"
						aZB8[14] := CValToChar(VAL(aZB8[15]) * VAL(aZB8[16]) / 100)
					EndIf
				Endif
			ElseIf XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS90"},'',.T.) == 'ICMS90' //Outros
				aZB8[13] := '6' //ZB8->ZB8_TRBIMP - Tipo de Tributacao
				aZB8[14] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS90"},"_VICMS")  //ZB8->ZB8_VLIMP - Valor do Imposto
				aZB8[15] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS90"},"_VBC")    //ZB8->ZB8_BASIMP - Base de Calculo Imposto
				aZB8[16] := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS90"},"_PICMS")  //ZB8->ZB8_PCIMP - Aliquota do Imposto
			ElseIf XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMSOUTRAUF"},'',.T.) == 'ICMSOutraUF'//Outros
				aZB8[13] := '6'  //ZB8->ZB8_TRBIMP - Tipo de Tributacao
				aZB8[14] := XmlValid(oCte,{"_INFCTE","_IMP","_ICMS","_ICMSOUTRAUF"},"_vICMSOutraUF",.F.) //ZB8->ZB8_VLIMP - Valor do Imposto
				aZB8[15] := XmlValid(oCte,{"_INFCTE","_IMP","_ICMS","_ICMSOUTRAUF"},"_vBCOutraUF",  .F.) //ZB8->ZB8_BASIMP - Base de Calculo Imposto
				aZB8[16] := XmlValid(oCte,{"_INFCTE","_IMP","_ICMS","_ICMSOUTRAUF"},"_pICMSOutraUF",.F.) //ZB8->ZB8_PCIMP - Aliquota do Imposto
			EndIf

			lFindGW1 := .F.

			/******************/
		//Tratamento para transformar a tag de NF em um array, validando se � NF ou NFe
			If !Empty(oInfNF) .And. ValType(XmlChildEx(oInfNF,"_INFNF")) $ "O/A"

			//Verifica as informa��es da nota vinculada
				If ValType(XmlChildEx(oInfNF,"_INFNF")) == "O"
					XmlNode2Arr( oInfNF:_INFNF , "_INFNF" )
				EndIf

				aNF := oInfNF:_INFNF

				aZB8[18]  := XmlValid(oCTe,aVldNF,"_NPESO") //ZB8->ZB8_PESOR - Peso Real

				For nCont := 1 To Len(aNF)

					If XmlValid(oCTe,{"_INFCTE","_REM","_CNPJ"},"TEXT",.T.)== 'CNPJ'
						cCgcRem := XmlValid(oCTe,{"_INFCTE","_REM"}, "_CNPJ")
					Else
						cCgcRem := XmlValid(oCTe,{"_INFCTE","_REM"}, "_CPF")
					EndIf

					If XmlValid(oCTe,{"_INFCTE","_DEST","_CNPJ"},"TEXT",.T.)== 'CNPJ'
						cCgcDest := XmlValid(oCTe,{"_INFCTE","_DEST"}, "_CNPJ")
					Else
						cCgcDest := XmlValid(oCTe,{"_INFCTE","_DEST"},"_CPF")
					EndIf

					dbSelectArea("GU3")
					GU3->( dbSetOrder(11) )
					If GU3->( dbSeek(xFilial("GU3") + cCgcRem) )

						cDoc 	:= Replicate("0",nCountZerosNF) + aNF[nCont]:_NDOC:TEXT
						cSerie := PADR(Alltrim(aNF[nCont]:_SERIE:TEXT) , TamSX3("ZB9_SERDC")[1],'')

						If IsInCallStack("COMXCOLImp") .OR. IsInCallStack("ImportCol")
							If !(ValidCFOP( cDoc,	; //cDoc
								cSerie,	; //cSerie
								cCgcRem,	; //cCliFor
								cCgcDest,; //cCgcDest
								"" ) )	  //NFE

							//Se a Faixa do CFOP N�O Estiver como integra GFE
							//ou se N�o Listado N�O Estiver como Integra GFE ent�o Retorna para integrar no ImpXML_CTe
								GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - CFOP do documento de carga configurado para n�o integrar com GFE.')
								lProcArq := .F.
								Exit
							EndIf
						EndIf

						While !GU3->(Eof()) .And. GU3->GU3_FILIAL == xFilial("GU3") .And. AllTrim(GU3->GU3_IDFED) ==  AllTrim(cCgcRem)
							cNumDC := CValToChar(val( aNF[nCont]:_NDOC:TEXT))
							nCountZerosNF := 0
							lNrDf := .F.
						// Busca o Documento de Carga do CT-e
						// Se n�o encontrar, tenta procurar documentos com zeros a esquerda
							While nCountZerosNF <= 6
								cNumeroNF := Replicate("0",nCountZerosNF) + cNumDC
								aRetDocTrechoPago := BuscaDocTrechoPago("", GU3->GU3_CDEMIT, Upper(Alltrim(aNF[nCont]:_SERIE:TEXT)), AllTrim(cNumeroNF))
								If aRetDocTrechoPago[1]
									lFindGW1 := .T.

									aAdd(aZB9, {GW1->GW1_FILIAL,;
										Alltrim(STR(nCont)),;
										GW1->GW1_EMISDC, ;
										GW1->GW1_SERDC,;
										GW1->GW1_NRDC,;
										GW1->GW1_CDTPDC,;
										GW1->GW1_DANFE,;
										Posicione("GU3",1,xFilial("GU3")+GW1->GW1_EMISDC,"GU3_IDFED")})

									lNrDf := .T.
									Exit

								EndIf

								nCountZerosNF++
							EndDo

							If !lNrDf
								aAdd(aZB9, {"",;
									Alltrim(STR(nCont)),;
									"", ;
									Alltrim(aNF[nCont]:_SERIE:TEXT),;
									cValToChar(Val(cNumeroNF)),;
									"",;
									"",;
									cCgcRem/*cnpj do emissor da nota.*/})
							EndIf

							If Len(aZB9) > 0
								Exit
							EndIf
							GU3->(dbSkip())
						EndDo

						If !lNrDf
							cMsgPreVal += "- " + "N�o foi encontrado a NF com n�mero: " + aNF[1]:_NDOC:TEXT + " e com a s�rie : " + aNF[1]:_SERIE:TEXT + CRLF
						EndIf

					EndIf
				Next nCont
				nNF := Len(aNF)

			ElseIf !Empty(oInfNFE) .And. ValType(XmlChildEx(oInfNFE,"_INFNFE")) $ "O/A"

			//Verifica as informa��es da nota vinculada
				If ValType(XmlChildEx(oInfNFE,"_INFNFE")) == "O"
					XmlNode2Arr( oInfNFE:_INFNFE  , "_INFNFE" )
				EndIf
				aNFe := oInfNFE:_INFNFE

				cDoc 	:= ""
				cSerie := ""

				If IsInCallStack("COMXCOLImp") .OR. IsInCallStack("ImportCol")
					If !( ValidCFOP( cDoc,			; // cDoc
						cSerie,			; // cSerie
						'',				; // cCliFor
						'',				; // cCgcDest
						aNFe[1]:_CHAVE:TEXT ) )// Chave NFE
					//Se a Faixa do CFOP N�O Estiver como integra GFE
					//ou se N�o Listado N�O Estiver como Integra GFE ent�o Retorna para integrar no ImpXML_CTe
						GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - CFOP do documento de carga configurado para n�o integrar com GFE.')
						lProcArq := .F.
					EndIf
				EndIf

				If lProcArq
					For nCont := 1 To Len(aNFe)
						aRetDocTrechoPago := BuscaDocTrechoPago(aNFe[nCont]:_CHAVE:TEXT)

						If aRetDocTrechoPago[1]
							lFindGW1 := .T.

							aTemp := Array(8)
							aTemp[1] := GW1->GW1_FILIAL		//ZB9_FILIAL
							aTemp[2] := nCont   				//ZB9_SEQ
							aTemp[3] := GW1->GW1_EMISDC		//ZB9_EMISDC
							aTemp[4] := GW1->GW1_SERDC 		//ZB9_SERDC
							aTemp[5] := GW1->GW1_NRDC  		//ZB9_NRDC
							aTemp[6] := GW1->GW1_CDTPDC		//ZB9_TPDC
							aTemp[7] := GW1->GW1_DANFE		//ZB9_TPDC
							aTemp[8] := Posicione("GU3",1,xFilial("GU3")+GW1->GW1_EMISDC,"GU3_IDFED")
							aADD(aZB9, aTemp)
						Else
							aTemp 		:= Array(8)
							aTemp[7] 	:= aNFe[nCont]:_CHAVE:TEXT

							AAdd(aZB9,aTemp)
							cMsgPreVal += "- " + "Documento de carga n�o encontrado com a chave da NFe: " + aNFe[nCont]:_CHAVE:TEXT + CRLF
						EndIf
					Next nCont

					nNF := Len(aNFe)
				EndIf
			Else
			//Verifica se o tipo do CTe for complementar
				If cTpCte = "1"

				//Ler o DC a partir do conhecimento normal.
				/*
					dbSelectArea("GW3")
					GW3->(dbSetOrder(14))
					If dbSeek(XmlValid(oCTe,{'_INFCTE','_INFCTECOMP'},'_CHAVE'))
						aOriDoc[1] := GW3->GW3_SERDF
						aOriDoc[2] := GW3->GW3_NRDF
						aOriDoc[3] := .T.
						dbSelectArea("GW4")
						GW4->(dbSetOrder(1))
						If GW4->(dbSeek(GW3->(GW3_FILIAL+GW3_EMISDF+GW3_CDESP+GW3_SERDF+GW3_NRDF)))
							dbSelectArea("GW1")
							GW1->(dbSetOrder(1))
							If GW1->(dbSeek(GW4->(GW4_FILIAL+GW4_TPDC+GW4_EMISDC+GW4_SERDC+GW4_NRDC)))
								lFindGW1 := .T.
							EndIf
						EndIf
					EndIf
				*/
					lFindGW1 := .F.

				// Se n�o encontrar nos documentos de frete busca na ZB8
					If !lFindGW1
						If Len(aZB9) == 0 .And. ValidSIX("ZB8", "5")
							dbSelectArea("ZB8")
							ZB8->(dbSetOrder(5))
							If ZB8->(dbSeek(XmlValid(oCTe,{'_INFCTE','_INFCTECOMP'},'_CHAVE')))
								aOriDoc[1] := ZB8->ZB8_SERDF
								aOriDoc[2] := ZB8->ZB8_NRDF
								aOriDoc[3] := .T.
								dbSelectArea("ZB9")
								ZB9->(dbSetOrder(1))
								If ZB9->(dbSeek(ZB8->ZB8_FILIAL+ZB8->ZB8_NRIMP))
									dbSelectArea("GW1")
									GW1->(dbSetOrder(1))
									If GW1->(dbSeek(ZB9->(ZB9_FILDC+ZB9_TPDC+ZB9_EMISDC+ZB9_SERDC+ZB9_NRDC)))
										lFindGW1 := .T.
									EndIf
								EndIf
							EndIf
						EndIf
					EndIf
				EndIf
			EndIf

			If lProcArq
			//Regra de preenchimento da filial: verifica o tomador e busca o cod da filial usando o CNPJ,
			//comparando com a filial do documento de carga, j� que pode haver mais de uma filial com mesmo CNPJ
				If XmlValid(oCTe,{"_INFCTE","_IDE","_TOMA03"},"",.T.) == 'toma03'
					If oCTe:_INFCTE:_IDE:_TOMA03:_TOMA:TEXT == '0'              // Remetente
						cCliDes := 'Remetente'

						If XmlValid(oCTe,{"_INFCTE","_REM","_CNPJ"},"TEXT",.T.) == 'CNPJ'
							cCliIdFed := oCTe:_INFCTE:_REM:_CNPJ:TEXT
							cCliDesFed := 'CNPJ'
						Else
							cCliIdFed := oCTe:_INFCTE:_REM:_CPF:TEXT
							cCliDesFed := 'CPF'
						EndIf
					ElseIf oCTe:_INFCTE:_IDE:_TOMA03:_TOMA:TEXT == '1'          // Expedidor
						cCliDes := 'Expedidor'

						If XmlValid(oCTe,{"_INFCTE","_EXPED","_CNPJ"},"TEXT",.T.) == 'CNPJ'
							cCliIdFed := oCTe:_INFCTE:_EXPED:_CNPJ:TEXT
							cCliDesFed := 'CNPJ'
						Else
							cCliIdFed := oCTe:_INFCTE:_EXPED:_CPF:TEXT
							cCliDesFed := 'CPF'
						EndIf
					ElseIf oCTe:_INFCTE:_IDE:_TOMA03:_TOMA:TEXT == '2'          // Recebedor
						cCliDes := 'Recebedor'

						If XmlValid(oCTe,{"_INFCTE","_RECEB","_CNPJ"},"TEXT",.T.) == 'CNPJ'
							cCliIdFed := oCTe:_INFCTE:_RECEB:_CNPJ:TEXT
							cCliDesFed := 'CNPJ'
						Else
							cCliIdFed := oCTe:_INFCTE:_RECEB:_CPF:TEXT
							cCliDesFed := 'CPF'
						EndIf
					ElseIf oCTe:_INFCTE:_IDE:_TOMA03:_TOMA:TEXT == '3' 			// Destinatario
						cCliDes := 'Destinat�rio'

						If XmlValid(oCTe,{"_INFCTE","_DEST","_CNPJ"},"TEXT",.T.) == 'CNPJ'
							cCliIdFed := oCTe:_INFCTE:_DEST:_CNPJ:TEXT
							cCliDesFed := 'CNPJ'
						Else
							cCliIdFed := oCTe:_INFCTE:_DEST:_CPF:TEXT
							cCliDesFed := 'CPF'
						EndIf
					EndIf
				Else  //Outros
					cCliDes := 'Emitente outros'

					If XmlValid(oCTe,{"_INFCTE","_IDE","_TOMA4","_CNPJ"},"TEXT",.T.) == 'CNPJ'
						cCliIdFed := oCTe:_INFCTE:_IDE:_TOMA4:_CNPJ:TEXT
						cCliDesFed := 'CNPJ'
					Else
						cCliIdFed := oCTe:_INFCTE:_IDE:_TOMA4:_CPF:TEXT
						cCliDesFed := 'CPF'
					EndIf
				EndIf

			//Valida��o da filial do DF contra filial do DC.
			//Se for Exclusivo, ser� verificado se a filial selecionada em tela possui CNPJ do tomador do DF.
			//Se for Compartilhado, n�o h� sele��o de filial em tela e todos os XML ser�o importados. Assim, ser� verificado
			//se a filial do DC possui CNPJ do tomador do DF.
			//Caso n�o seja encontrado no sistema nenhum dos DC existentes no XML, n�o ser� poss�vel validar filiais. A importa��o ser� feita, mas com erro.

				If X2MODO("ZB8") == "E"
					cFilVal := cFilAnt
				Else
					If lFindGW1
						cFilVal := GW1->GW1_FILIAL
					Else
						cFilVal := ''
					EndIf
				EndIf

				cFilEmit := GFEA115BF (cCliIdFed,, cFilVal)
				If Empty(cFilEmit)
					If cFilVal == ''
						cMsgPreVal += "- Tomador do Frete (" + cCliDes + ") com " + cCliDesFed + " " + cCliIdFed + " n�o cadastrado como filial." + CRLF
					Else
						cMsgPreVal += "- Tomador do Frete (" + cCliDes + ") com " + cCliDesFed + " " + cCliIdFed + " n�o possui o mesmo " + cCliDesFed + " da filial '" + cFilVal + "'." + CRLF
					EndIf

					If X2MODO("ZB8") == "E"
					//Verificar se o arquivo pertence a outra filial e n�o excluir da pasta. Assim:
					//1-Quando for schedule e a thread da filial do arquivo for executada, o arquivo ser� importado.
					//2-Quando for via ERP, ser� importado quando for selecionada a filial correta.

						If AllTrim(GFEA115BF (cCliIdFed)) == AllTrim('')
							cMsgAux := "Tomador do Frete (" + cCliDes + ") com " + cCliDesFed + " " + cCliIdFed + " n�o cadastrado como filial."
							cMsgAux += CRLF + SB15 + SB6 + "Arquivo n�o ser� importado."
						Else
							cMsgAux := "Tomador do Frete (" + cCliDes + ") com " + cCliDesFed + " " + cCliIdFed + " pertence a outra filial."
							If lTotvsColab
								cMsgAux += CRLF + SB15 + SB6 + "Aguarde o processamento da filial do documento para que a importa��o seja efetuada."
							EndIf
							lMoveArq := .F.
							lCteOutFil := .T.
						EndIf

						cMsgPreVal += "- " + cMsgAux
						GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - ' + cMsgAux)

						lGravaZB8 := .F. //erro que impede a importa��o
					EndIf
				Else
					aZB8[1] := cFilEmit
				EndIf

				If lGravaZB8
				//Se n�o achar nenhuma nota, indica que s�o notas de compras em que haver� somente o pagamento do frete.
				//Nesse caso dever� ser permitido importar com erro, para dar entrada nas notas posteriormente.
					If !lFindGW1
						cMsgPreVal += "- Nenhuma nota fiscal do CT-e encontrada como Documento de Carga. N�o foi poss�vel fazer compara��o entre filiais do Documento de Carga e Documento de Frete." + CRLF
					Else
						If cFilEmit <> GW1->GW1_FILIAL
							cMsgPreVal += "- " + "Filiais diferentes entre Documento de Carga: '" + GW1->GW1_FILIAL + "' e Documento de Frete: '" + cFilEmit + "'." + CRLF
							If X2MODO("ZB8") == "E"
								cMsgAux := "Filiais diferentes entre Documento de Carga: '" + GW1->GW1_FILIAL + "' e Documento de Frete: '" + cFilEmit + "'."
								cMsgAux += CRLF + SB15 + SB6 + "N�o ser� importado."
								GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - ' + cMsgAux)

								lGravaZB8 := .F. //erro que impede a importa��o
							EndIf
						EndIf
					EndIf
				EndIf

				If lGravaZB8
					aZB8[9] := CteTPDF(oCTE, aZB8, aZB9)

				// Complemento de Valores / Imposto
					If Len(aZB9) == 0 .And. cTpCte == "1"
						cChaveRel := XmlValid(oCTe,{'_INFCTE','_INFCTECOMP'},'_CHAVE')
						nCont     := 1

						If !Empty(cChaveRel)
						// Adicionar notas do CT-e relacionados
							dbSelectArea("GW3")

							If ValidSIX("GW3", "E")
								GW3->(dbSetOrder(14))

								If GW3->( dbSeek( cChaveRel ) )
									dbSelectArea("GW4")
									GW4->(dbSetOrder(1))
									If GW4->(dbSeek(GW3->GW3_FILIAL+GW3->GW3_EMISDF+GW3->GW3_CDESP+GW3->GW3_SERDF+GW3->GW3_NRDF))
										While GW4->(!Eof()) .And. ;
												GW3->GW3_FILIAL == GW4->GW4_FILIAL .And. ;
												GW3->GW3_EMISDF == GW4->GW4_EMISDF .And. ;
												GW3->GW3_CDESP == GW4->GW4_CDESP .And. ;
												GW3->GW3_SERDF == GW4->GW4_SERDF .And. ;
												GW3->GW3_NRDF == GW4->GW4_NRDF

											aTemp := Array(8)
											aTemp[1] := GW4->GW4_FILIAL		//ZB9_FILIAL
											aTemp[2] := nCont   				//ZB9_SEQ
											aTemp[3] := GW4->GW4_EMISDC		//ZB9_EMISDC
											aTemp[4] := GW4->GW4_SERDC 		//ZB9_SERDC
											aTemp[5] := GW4->GW4_NRDC  		//ZB9_NRDC
											aTemp[6] := GW4->GW4_TPDC		//ZB9_TPDC
											aTemp[7] := ""
											aTemp[8] := Posicione("GU3",1,xFilial("GU3")+GW4->GW4_EMISDC,"GU3_IDFED")
											aADD(aZB9,aTemp)

											nCont++
											GW4->(dbSkip())
										EndDo
									EndIf
								EndIf
							EndIf

						// Se n�o encontrar nos documentos de frete busca na ZB8
							If Len(aZB9) == 0 .And. ValidSIX("ZB8", "5")
								dbSelectArea("ZB8")
								ZB8->( dbSetOrder(5) )

								If ZB8->( dbSeek( cChaveRel ) )

									dbSelectArea("ZB9")
									ZB9->( dbSetOrder(1) )

									If ZB9->(dbSeek(ZB8->ZB8_FILIAL+ZB8->ZB8_NRIMP))
										While ZB9->(!Eof()) .And. ;
												ZB9->ZB9_FILIAL == ZB8->ZB8_FILIAL .And. ;
												ZB9->ZB9_NRIMP == ZB8->ZB8_NRIMP

											aTemp := Array(8)
											aTemp[1] := ZB9->ZB9_FILDC		//ZB9_FILDC - Deve ser do documento e n�o da importa��o
											aTemp[2] := nCont   				//ZB9_SEQ
											aTemp[3] := ZB9->ZB9_EMISDC		//ZB9_EMISDC
											aTemp[4] := ZB9->ZB9_SERDC 		//ZB9_SERDC
											aTemp[5] := ZB9->ZB9_NRDC  		//ZB9_NRDC
											aTemp[6] := ZB9->ZB9_TPDC		//ZB9_TPDC
											aTemp[7] := ZB9->ZB9_DANFE
											aTemp[8] := Posicione("GU3",1,xFilial("GU3")+GW4->GW4_EMISDC,"GU3_IDFED")
											aADD(aZB9,aTemp)

											nCont++
											ZB9->(dbSkip())
										EndDo
									EndIf
								EndIf
							EndIf
						EndIf
					EndIf

				//
				// Valida��o de Emissor
				//
					If Empty(cEmi)
						If XmlValid(oCTe,{"_INFCTE","_EMIT","_CNPJ"},"TEXT",.T.)== 'CNPJ'
							cEmi := u_A118EMIT("1",XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CNPJ"),aZB9)//ValidEmis(3,xFilial("SA2")+XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CNPJ"))
						Else
							cEmi := u_A118EMIT("1",XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CPF"),aZB9)//ValidEmis(3,xFilial("SA2")+XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CPF"))
						EndIf
					EndIf
					If Empty(cEmi)
						If XmlValid(oCTe,{"_INFCTE","_EMIT","_CNPJ"},"TEXT",.T.)== 'CNPJ'
							cMsgPreVal += "- " + "N�o foi encontrado transportador v�lido com o CNPJ: " + XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CNPJ") + CRLF
						Else
							cMsgPreVal += "- " + "N�o foi encontrado transportador v�lido com o CPF: " + XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CPF") + CRLF
						EndIf
						aZB8[3] := ""
					Else
						aZB8[3] := cEmi  //ZB8->ZB8_EMISDF - (Emissor)
						If (GU3->GU3_CTE == "2" .Or. Empty(GU3->GU3_CTE)) .And. GU3->GU3_TRANSP == "1" //Emitente de transporte posicionado deve emitir CT-e
							RecLock("GU3",.F.)
							GU3->GU3_CTE := "1"
							MsUnlock("GU3")
						EndIf
					EndIf

				//
				// Valida��o do Remetente e Destinat�rio
				//
					If XmlValid(oCTe,{"_INFCTE","_REM","_CNPJ"},"TEXT",.T.)== 'CNPJ'
						aZB8[7] := u_A118EMIT("2",XmlValid(oCTe,{"_INFCTE","_REM"},"_CNPJ"),aZB9) //ZB8->ZB8_CDREM
					Else
						aZB8[7] := u_A118EMIT("2",XmlValid(oCTe,{"_INFCTE","_REM"},"_CPF"),aZB9) //ZB8->ZB8_CDREM
					EndIf
				
					If XmlValid(oCTe,{"_INFCTE","_DEST","_CNPJ"},"TEXT",.T.)== 'CNPJ'
						aZB8[8] := u_A118EMIT("3",XmlValid(oCTe,{"_INFCTE","_DEST"},"_CNPJ"),aZB9) //ZB8->ZB8_CDREM
					Else
						aZB8[8] := u_A118EMIT("3",XmlValid(oCTe,{"_INFCTE","_DEST"},"_CPF"),aZB9) //ZB8->ZB8_CDREM
					EndIf
				
					_bCLiTom := .F.
					If (XmlChildEx ( oCTe ,"_INFCTE") <> Nil)
						If (XmlChildEx ( oCTe:_INFCTE ,"_IDE") <> Nil)
							If (XmlChildEx ( oCTe:_INFCTE:_IDE ,"_TOMA03") <> Nil)
								If XmlValid(oCTe,{"_INFCTE","_IDE","_TOMA03"},"_TOMA") == '3'
									aZB8[38] := aZB8[8]
									_bCLiTom := .T.
								Else
									aZB8[38] := aZB8[7]
									_bCLiTom := .F.
								EndIf
							ElseIf (XmlChildEx ( oCTe:_INFCTE:_IDE ,"_TOMA4") <> Nil)
								If XmlValid(oCTe,{"_INFCTE","_IDE","_TOMA4","_CNPJ"},"TEXT",.T.) == 'CNPJ'
									aZB8[38] := u_A118EMIT("4",XmlValid(oCTe,{"_INFCTE","_IDE","_TOMA4"},"_CNPJ"),aZB9, aZB8) //ZB8->ZB8_CDREM
									//aZB8[39] := U_zSM0CNPJ(XmlValid(oCTe,{"_INFCTE","_IDE","_TOMA4"},"_CNPJ"))
								Else
									aZB8[38] := u_A118EMIT("4",XmlValid(oCTe,{"_INFCTE","_IDE","_TOMA4"},"_CPF"),aZB9, aZB8) //ZB8->ZB8_CDREM
									//aZB8[39] := U_zSM0CNPJ(XmlValid(oCTe,{"_INFCTE","_IDE","_TOMA4"},"_CNPJ"))
								EndIf
							Else
								aZB8[38] := IIF(cDevol=='S',U_wSM0CNPJ(aZB8[1]),aZB8[7])
							EndIf
						Else
							aZB8[38] := IIF(cDevol=='S',U_wSM0CNPJ(aZB8[1]),aZB8[7])
						EndIf
					Else
						aZB8[38] := IIF(cDevol=='S',U_wSM0CNPJ(aZB8[1]),aZB8[7])
					//aZB8[39] := U_zSM0CNPJ(XmlValid(oCTe,{"_INFCTE","_REM"},"_CNPJ"))
					EndIf
				
					aZB8[39] := SM0CNPJbyCOD(aZB8[38], _bCLiTom)

					If Empty(aZB8[7]) .And. Empty(aZB8[8])
						cMsgPreVal += "- " + "Remetente e Destinat�rio n�o definidos: Doc. Carga n�o encontrado. " + CRLF
					EndIf

					cComp := ""

					If XmlChildEx(oCTE:_INFCTE:_VPREST,"_COMP") != Nil
						cComp := oCTe:_INFCTE:_VPREST:_COMP
					EndIf

					aZB8[22] := 0
					nVlTaxas := 0

					If !Empty(cComp) .And. ValType(cComp) == "O"
						If 'FRETE PESO' == UPPER(cComp:_XNOME:TEXT)
							aZB8[19] := cComp:_VCOMP:TEXT
						ElseIf 'FRETE VALOR' ==  UPPER(cComp:_XNOME:TEXT)
							aZB8[20] := cComp:_VCOMP:TEXT
						ElseIf 'PEDAGIO' $  UPPER(cComp:_XNOME:TEXT)
							aZB8[21] := cComp:_VCOMP:TEXT
						Else
							nVlTaxas := val(cComp:_VCOMP:TEXT) + nVlTaxas
						EndIf
					ElseIf !Empty(cComp) .And. ValType(cComp) == "A"
						For nX := 1 To Len(cComp)
							If 'FRETE PESO' ==  UPPER(cComp[nX]:_XNOME:TEXT)
								aZB8[19] := cComp[nX]:_VCOMP:TEXT// XmlValid(oCTe,{"_INFCTE","_VPREST","_COMP"},"_VCOMP")    ZB8->ZB8_FRPESO - Frete Peso
							ElseIf  'FRETE VALOR' ==  UPPER(cComp[nX]:_XNOME:TEXT)
								aZB8[20] := cComp[nX]:_VCOMP:TEXT//XmlValid(oCTe,{"_INFCTE","_INFCTECOMP","_VPRESCOMP","_COMPCOMP"},"_VCOMP")    //ZB8->ZB8_FRVAL - Frete Valor
							ElseIf  'PEDAGIO' $ UPPER(cComp[nX]:_XNOME:TEXT)
								aZB8[21] := cComp[nX]:_VCOMP:TEXT//XmlValid(oCTe,{"_INFCTE","_INFCTECOMP","_VPRESCOMP","_COMPCOMP"},"_VCOMP") //ZB8->ZB8_PEDAG - Pegadio
							Else
								nVlTaxas := val(cComp[nX]:_VCOMP:TEXT) + nVlTaxas // XmlValid(oCTe,{"_INFCTE","_INFCTECOMP","_VPRESCOMP","_COMPCOMP"},"_VCOMP")    //ZB8->ZB8_TAXAS - Taxas
							EndIf
						Next nX
					EndIf

					cVTPrest := XmlValid(oCTe,{"_INFCTE","_VPREST"},"_VTPREST")
					cVRec    := XmlValid(oCTE,{"_INFCTE","_VPREST"},"_VREC")

					aZB8[22] := Str(nVlTaxas)
					aZB8[23] := IIf(QtdComp(Val(cVTPrest)) >= QtdComp(Val(cVRec)), cVTPrest, cVRec) //ZB8->ZB8_VLDF
					aZB8[24] := aOriDoc[1]                                        //ZB8->ZB8_ORISER
					aZB8[25] := aOriDoc[2]                                        //ZB8->ZB8_ORINR
					aZB8[26] :='I'                                                //ZB8->ZB8_ACAO - Inclus�o
					aZB8[28] :=''                                                 //ZB8->ZB8_EDIMSG
					aZB8[29] :=''                                                 //ZB8->ZB8_EDINRL
					aZB8[30] :=''                                                 //ZB8->ZB8_EDILIN
					aZB8[31] := cXMLFile                                           //ZB8->ZB8_EDIARQ
					aZB8[32] := DDATABASE                                         //ZB8->ZB8_DTIMP
				//aZB8[33] := SubStr(oCTe:_INFCTE:_ID:TEXT,4,44)                //ZB8->ZB8_CTE
					aZB8[34] := XmlValid(oCTe,{"_INFCTE","_IDE"},"_TPCTE")
					aZB8[35] := 0
					aZB8[36] := 0
					aZB8[37] := POSICIONE("ZBA",3,xFilial("ZBA")+XmlValid(oCTe,{"_INFCTE","_IDE"},"_MODAL"), "ZBA->ZBA_COD")
				
					aZB8[40] := getPeso(oCTE, aZB8[03], aZB8[19])
					aZB8[41] := getVolume(oCTE)
					aZB8[42] := cDevol
					
					aZB8[44] := cCdMunIni
					aZB8[45] := cDsMunIni
					aZB8[46] := cUfIni
					aZB8[47] := cCdMunFim
					aZB8[48] := cDsMunFim
					aZB8[49] := cUfFim

					If (aZB8[13] == '7')
						dvlIcmsTRet := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS60"},"_vICMSSTRet")
						If XmlChildEx(oCTE:_INFCTE:_IMP:_ICMS:_ICMS60,"_vCred") != Nil
							dvCred 	  := XmlValid(oCTe,{"_INFCTE","_IMP","_ICMS","_ICMS60"},"_vCred")
						EndIf
						aZB8[35]    := VAL(dvlIcmsTRet)-VAL(dvCred)
						codCidIni   := XmlValid(oCTe,{"_INFCTE","_IDE"},"_cMunIni")
						codCidFim   := XmlValid(oCTe,{"_INFCTE","_IDE"},"_cMunFim")

						If (VAL(dvlIcmsTRet) <> 0)
							nPossuiIcms := 1
						Else
							nPossuiIcms := 2
						EndIf

						aRetICMS := GFEFnIcms(aZB8[03], ; // C�digo do transportador
						aZB8[07], ; // C�digo do remetente
						aZB8[08],; // C�digo do destinatario
						codCidIni,; // N�mero da cidade de origem
						codCidFim,; // N�mero da cidade de destino
						"0", ; // Forma de utiliza��o da mercadoria
						"0", ; // Tipo de item
						"0", ; // Classifica��o de frete
						CHR(nPossuiIcms), ; // Mercadoria � tributada de ICMS?
						"0",; //Tipo de Opera��o do Agrupador do Documento de Carga
						xFilial("ZB8")) // Filial do c�lculo - Usado no par�metro MV_GFECRIC para as exce��es das filiais que n�o tem direito a cr�dito

						aZB8[36] := aRetICMS[4] // Retorno do valor do percentual de ICMS
					EndIf

				//Valida��es***********************************************************************

				//Formata��o de valores
					If !Empty(aZB8[14])
						aZB8[14] := VAL(Transform(VAL(aZB8[14]), '@R 99999.99'))
					EndIf
					If !Empty(aZB8[15])
						aZB8[15] := VAL(Transform(VAL(aZB8[15]), '@R 9999999999999.99'))
					EndIf
					If !Empty(aZB8[16])
						aZB8[16] := VAL(Transform(VAL(aZB8[16]), '@R 99.99'))
					EndIf
					If !Empty(aZB8[18])
						aZB8[18] := VAL(Transform(VAL(aZB8[18]), '@R 99999.99'))
					EndIf
					If !Empty(aZB8[19])
						aZB8[19] := VAL(Transform(VAL(aZB8[19]), '@R 9999999999999.99'))
					EndIf
					If !Empty(aZB8[20])
						aZB8[20] := VAL(Transform(VAL(aZB8[20]), '@R 9999999999999.99'))
					EndIf
					If !Empty(aZB8[21])
						aZB8[21] := VAL(Transform(VAL(aZB8[21]), '@R 9999999999999.99'))
					EndIf
					If !Empty(aZB8[22])
						aZB8[22] := VAL(Transform(VAL(aZB8[22]), '@R 9999999999999.99'))
					EndIf
					If !Empty(aZB8[23])
						aZB8[23] := VAL(Transform(VAL(aZB8[23]), '@R 9999999999999.99'))
					EndIf
			//Fim Valida��es***********************************************************************

					GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - In�cio da grava��o dos registros de importa��o.')

					lImportaCte := .T.
					RecLock("ZB8",.T.)
					nNRIMP := GETSXENUM("ZB8", "ZB8_NRIMP") 	//(Numero da Importa��o) - Busca o numero sequencial
					ZB8->ZB8_NRIMP  :=  nNRIMP
					ConfirmSX8()
					//If !IsBlind()
					//	ZB8->ZB8_MARKBR := oBrowse115:Mark()
					//EndIf
					ZB8->ZB8_FILIAL :=	xFilial("ZB8")
					ZB8->ZB8_FILDOC :=	aZB8[01]
					ZB8->ZB8_CDESP  :=	aZB8[02]
					ZB8->ZB8_EMISDF :=	aZB8[03]
					ZB8->ZB8_SERDF  :=	aZB8[04]
					ZB8->ZB8_NRDF   :=	aZB8[05]
					ZB8->ZB8_DTEMIS :=	aZB8[06]
					ZB8->ZB8_CDREM  :=	aZB8[07]
					ZB8->ZB8_CDDEST :=	aZB8[08]

					ZB8->ZB8_DTENT  :=	aZB8[10]
					ZB8->ZB8_CDCONS :=	aZB8[11]
					ZB8->ZB8_CFOP   :=	aZB8[12]
					ZB8->ZB8_TRBIMP :=	aZB8[13]
					ZB8->ZB8_VLIMP  :=	SetField(aZB8[14] ,"ZB8_VLIMP" )
					ZB8->ZB8_BASIMP := 	SetField(aZB8[15] ,"ZB8_BASIMP")
					ZB8->ZB8_PCIMP  :=	SetField(aZB8[16] ,"ZB8_PCIMP" )
					ZB8->ZB8_PESOR  :=	SetField(aZB8[18] ,"ZB8_PESOR" )
					ZB8->ZB8_FRPESO :=	SetField(aZB8[19] ,"ZB8_FRPESO")
					ZB8->ZB8_FRVAL  :=	SetField(aZB8[20] ,"ZB8_FRVAL" )
					ZB8->ZB8_PEDAG  :=	SetField(aZB8[21] ,"ZB8_PEDAG" )
					ZB8->ZB8_TAXAS  :=	SetField(aZB8[22] ,"ZB8_TAXAS" )
					ZB8->ZB8_ORISER :=	aZB8[24]
					ZB8->ZB8_ORINR  :=	aZB8[25]
					ZB8->ZB8_ACAO   :=	aZB8[26]
					ZB8->ZB8_EDIARQ :=	aZB8[31]
					ZB8->ZB8_DTIMP  :=	aZB8[32]
					ZB8->ZB8_CTE    :=	aZB8[33]
					ZB8->ZB8_TPCTE  :=	aZB8[34]
					ZB8->ZB8_IMPRET :=	SetField(aZB8[35] ,"ZB8_IMPRET")
					ZB8->ZB8_PCRET  :=	SetField(aZB8[36] ,"ZB8_PCRET" )
					ZB8->ZB8_ORIGEM :=	'2' //CT-e
					ZB8->ZB8_ALTER  :=	'2'
					ZB8->ZB8_CONF	:=  'N'
					ZB8->ZB8_STATUS :=  'IM'
					ZB8->ZB8_MODAL	:= aZB8[37]
					ZB8->ZB8_TOMADO := aZB8[38]
					ZB8->ZB8_FILTOM := aZB8[39]
					ZB8->ZB8_PREAL  := aZB8[40]
					
					ZB8->ZB8_VOLUM	:= aZB8[41]
					ZB8->ZB8_DEVOL	:= aZB8[42]
					
					
					ZB8->ZB8_MUNINI := aZB8[44]
					ZB8->ZB8_MUNDIN	:= aZB8[45]
					ZB8->ZB8_UFINI 	:= aZB8[46]
					ZB8->ZB8_MUNFIM	:= aZB8[47]
					ZB8->ZB8_MUNDFI := aZB8[48]
					ZB8->ZB8_UFFIM 	:= aZB8[49]
					
					ZB8->ZB8_TES 	:= GetMV("SA_TESCTE")
					ZB8->ZB8_CODPAG := GetMV("SA_CPCTE")
					ZB8->ZB8_NATURE := GetMV("SA_NATCTE")
					ZB8->ZB8_CC 	:= GetMV("SA_CCCTE")
					
					ZB8->ZB8_USUIMP := RETCODUSR()+" - "+ALLTRIM(USRRETNAME(RETCODUSR()))
					
					ZB8->ZB8_HRIMP := Time()
					
					If GFEVerCmpo({"ZB8_OBS"})
						ZB8->ZB8_OBS :=	cObs
					EndIf

					If aZB8[09] == '2' .and. SetField(aZB8[23] ,"ZB8_VLDF"  ) == 0
						ZB8->ZB8_TPDF  :=	'3'
						ZB8->ZB8_VLDF   :=	ZB8->ZB8_BASIMP
					Else
						ZB8->ZB8_TPDF  :=	aZB8[09]
						ZB8->ZB8_VLDF   :=	SetField(aZB8[23] ,"ZB8_VLDF"  )
					EndIf

					//If GXGTIDCBI()
					ZB8->ZB8_QTDCS  := nNF
					ZB8->ZB8_VLCARG := SetField(CalcVlCar(), "ZB8_VLCARG")
					CalcVolum()
					//EndIf

					If (aZB8[13] == '6')	.and. empty(ZB8->ZB8_BASIMP)
						ZB8->ZB8_BASIMP := ZB8->ZB8_VLDF
					EndIf

					If (aZB8[13] == '3')
						ZB8->ZB8_IMPRET := ZB8->ZB8_VLIMP
					EndIf

					If !Empty(cMsgPreVal)
						ZB8->ZB8_EDISIT := '2'
					Else
						ZB8->ZB8_EDISIT := '1'
					EndIf
					ZB8->ZB8_EDIMSG := cMsgPreVal

					MsUnlock()

					For nCont := 1 To Len(aZB9)
						RecLock("ZB9",.T.)
						ZB9->ZB9_FILIAL  := xFilial("ZB9")
						ZB9->ZB9_FILDC   := ZB8->ZB8_FILDOC//aZB9[nCont][1]
						ZB9->ZB9_NRIMP   := nNRIMP
						ZB9->ZB9_SEQ     := PADL(Alltrim(Transform(nCont, '@R 99999')),5,"0")
						ZB9->ZB9_EMISDC  := ZB8->ZB8_EMISDF//aZB9[nCont][3]
						ZB9->ZB9_SERDC   := ZB8->ZB8_SERDF//aZB9[nCont][4]
						ZB9->ZB9_NRDC    := ZB8->ZB8_NRDF//aZB9[nCont][5]
						ZB9->ZB9_TPDC    := ZB8->ZB8_TPDF//aZB9[nCont][6]
						ZB9->ZB9_DANFE := aZB9[nCont][7]

						MsUnlock()
					Next

					If ExistBlock("SASF0051")
						ExecBlock("SASF0051",.F.,.F.,{oCTE})
					EndIf

					GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Fim da grava��o dos registros de importa��o.')
				EndIf
			EndIf
		EndIf

		GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - Fim da valida��o do conte�do do arquivo.')

	EndIf


//N�o deve mover o arquivo quando for CTe de outra filial ou houver problemas de falta de patch etc.	
	If lMoveArq
		If lTotvsColab

			GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - In�cio da movimenta��o do arquivo por COMXCOL.')

			If lTC20
			//Para TC 2.0:
			//Quando executado via SASF005, o arquivo dever� existir. Assim, o SASF005 mover� o arquivo para pasta lidos.
			//Quando executado via ComXCol, o Job ColAutoRead "consome" o arquivo, criando um registro na
			//tabela CKO, e move o arquivo para pasta lidos. O Job SchdComCol far� a leitura dos registros dessa tabela,
			//para ent�o chamar o SASF005, que n�o ir� mover o arquivo, apenas criar os registros ZB8 e ZB9.
			//No TC 2.0, sempre move para pasta de lidos, independente de haver erro ou n�o.
			//Neste ponto n�o h� necessidade de manipula��o de arquivo f�sico.

				GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Utilizando configura��o do TOTVS COLABORACAO 2.0.')
				GFELog118:Add(SB15 + SB6 + 'N�o h� movimenta��o de arquivo.')
			Else
				If ExistBlock("SASF0052")
					cDIROK := ExecBlock("SASF0052",.F.,.F.)
				EndIf

				GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Utilizando configura��o do TOTVS COLABORACAO 1.0.')

				If !Empty(cDIROK)
					GA118MoveFile(DIRXML+DIRALER+AllTrim(cXMLFile), cDIROK+AllTrim(cXMLFile), @GFELog118)
				Else
					GA118MoveFile(DIRXML+DIRALER+AllTrim(cXMLFile), DIRXML+DIRLIDO+AllTrim(cXMLFile), @GFELog118)
				EndIf
			EndIf

		//O programa sempre vai adicionar o XML mesmo se houver erros (cMsgPreVal)
			Do Case
			Case nTipoXML = 1
				aAdd(aProc,{aZB8[05],aZB8[04],aZB8[03]})
			Case nTipoXML = 2
				If XmlValid(oCTe,{"_INFCTE","_IDE","_NCT"},'TEXT',.T.) == 'NCT'

					If XmlValid(oCTe,{"_INFCTE","_EMIT","_CNPJ"},"TEXT",.T.)== 'CNPJ'
						cEmi := ValidEmis(3,xFilial("SA2")+XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CNPJ"))
					Else
						cEmi := ValidEmis(3,xFilial("SA2")+XmlValid(oCTe,{"_INFCTE","_EMIT"},"_CPF"))
					EndIf
					aZB8[3] := ""
					aAdd(aProc,{ XmlValid(oCTe,{"_INFCTE","_IDE"},"_NCT"),XmlValid(oCTe,{"_INFCTE","_IDE"},"_SERIE"),cEmi})
				EndIf
			Case nTipoXML = 3
				If XmlValid(oCTE,{"_INFEVENTO","_CHCTE"},'TEXT',.T.) == 'CHCTE'
					If XmlValid(oCTe,{"_INFEVENTO","_CNPJ"},"TEXT",.T.)== 'CNPJ'
						cEmi := ValidEmis(3,xFilial("SA2")+XmlValid(oCTe,{"_INFEVENTO"},"_CNPJ"))
					Else
						cEmi := ValidEmis(3,xFilial("SA2")+XmlValid(oCTe,{"_INFEVENTO"},"_CPF"))
					EndIf
					// N�o existem informa��es de n�mero e s�rie no xml do evento de cancelamento 2.00
					// Ent�o s�o usadas do documento de frete localizado ou em branco caso n�o encontre o DF pela chave
					If lRetFuncao
						aAdd(aProc,{ZB8->ZB8_NRDF,ZB8->ZB8_SERDF,cEmi})
					Else
						aAdd(aProc,{" "," ",cEmi})
					EndIf
				EndIf
			EndCase

			GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - Fim da movimenta��o do arquivo.')
		Else
			GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - In�cio da movimenta��o do arquivo pelo SASF005.')

		//No TC 2.0, move para pasta de lidos se for erro ou for importado.
			If lTC20
				cDirDest := SF005Bar(AllTrim(GetNewPar("MV_NGLIDOS","\NeoGrid\LIDOS\")))
				GA118MoveFile(cXMLFile, cDirDest+GA118ExtDirOrFileName(cXMLFile,2), @GFELog118)
			Else
				cDirDest := SF005Bar(AllTrim(SuperGetMv("MV_XMLDIR", .F., "")))

				If Empty(cMsgPreVal)
					GA118MoveFile(cXMLFile, cDirDest+DIRLIDO+GA118ExtDirOrFileName(cXMLFile,2), @GFELog118)
				Else
					GA118MoveFile(cXMLFile, cDirDest+DIRERRO+GA118ExtDirOrFileName(cXMLFile,2), @GFELog118)
				EndIf
			EndIf

			GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - Fim da movimenta��o do arquivo.')
		EndIf
	EndIf

	GFELog118:Add(GFENOW(.F.,,,':','.') + SB2 + ' - Fim da importa��o do arquivo.')

//Elimina o registro de controle de concorr�ncia de importa��o
	UnLockByName(cLock, .F., .F.)
	GFELog118:Add(GFENOW(.F.,,,':','.') + ' - Exclus�o de sem�foro para o arquivo.')
//Redefine a vari�vel de forma a atualizar a hora em que ela teve o valor atribu�do.
	PutGlbValue(cLock, GFENow(.T.,,'','',''))

	If !lTotvsColab
	// Par�metros s�o passados pelo programa COMXCOL, n�o podem ser "limpos" aqui.
		FreeObj(oCte)

		aSize( aProc, 0 )
		aProc := Nil

		aSize( aTemp, 0 )
		aTemp := Nil
	EndIf

	aSize( aZB9, 0 )
	aZB9 := Nil

	aSize( aZB8, 0 )
	aZB8 := Nil

	GFELog118:EndLog()

	If !Empty(cMsgPreVal)
	//Quando for CTe de outra filial, deve setar vari�vel para vazio de forma que o ComXCol entenda que
	//esse arquivo dever� ser reprocessado.
		If lCteOutFil .And. lTotvsColab
			aErros := {}
		Else
			AAdd(aErros, {cXMLFile, cMsgPreVal, ""})
		EndIf

		lRetFuncao := .F.
	EndIf

Return lRetFuncao

//-------------------------------------------------------------------
/*/{Protheus.doc} SF005ANU

Valida��es do arquivo Cte ou evento de cancelamento
Cria registro de exclus�o

@param cXMLFile, caminho do arquivo que esta sendo importado
@param aErros, armazena o caminho dos aquivos com erro
@param aProc, array para guardar os arquivos processados (M-Mess)
@param oCTE:, Objeto XML do CTE
@param lTotvsColb:, se a chamada da fun��o vem do COMXCOL (importa��o via TSS;Totvs Colabora��o)
@param cTpCte:, Indicando se � cTE de anula��o,substitui��o ou evento de cancelamento
@type function
@author Sam Barros
@since 08/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function SF005ANU(cXMLFile,aProc,cMsgPreVal, oCTE, lTotvsColab,cTpCte)
	Local cChaveCte
	Local cStat
	Local nNRIMP
	Local aOriDoc := {'','',.T.} //Possui a ref�rencia do documento anulado/subtituido
	Local aAreaZB8 := ZB8->(GetArea())
	Local lTemReg := .F.
	Local lExcIncl := .F.
	Local aAreaGW3 := GW3->(GetArea())
	Local nCont := 1
	Local cMsg

	If cTpCte == '2' .Or. cTpCte =='3' .Or. cTpCte == "110111"  // Cte de Anula��o/Substitui��o de Valores/Cancelamento

		If cTpCte == '2' // Anula��o de valores
			cChaveCte := XmlValid(oCTE,{"_INFCTE","_INFCTEANU"},"_CHCTE")
		EndIf

		If cTpCte == '3' // Substitui��o de valores
			cChaveCte := XmlValid(oCTE,{"_INFCTE","_INFCTENORM","_INFCTESUB"},"_CHCTE")
		EndIf

		If cTpCte == "110111" // Evento de Cancelamento
			cStat := XmlValid(oCTE, {"_RETEVENTOCTE","_INFEVENTO"},"_CSTAT")
			If cStat = '135' //indica que o evento de cancelamento foi autorizado
				cChaveCte := XmlValid(oCTE,{"_EVENTOCTE","_INFEVENTO"},"_CHCTE")
			Else
				aOriDoc[3] := .F.
				cMsg := 'Cancelamento n�o autorizado. Motivo: ' + XmlValid(oCTE, {"_RETEVENTOCTE","_INFEVENTO"},"_XMOTIVO")
				GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - ' + cMsg)
				cMsgPreVal += '- ' + cMsg
			EndIf
		EndIf

		If aOriDoc[3] == .T.
			If !Empty(cChaveCte)//Possui uma chave Ct-e
				dbSelectArea("GW3")
				GW3->(dbSetOrder(14))//GW3_CTE
//Encontra o documento baseado na chave, se encontrar, faz uma c�pia
//para tabela intermedi�ria, permitindo futura elimina��o
				If GW3->(dbSeek(cChaveCte))
// Caso o CT-e Anulado,Substitu�do ou cancelado exista na tabela de processamento
// e ele est� pendente, exclu� o reg�stro
					dbSelectArea("ZB8")
					ZB8->(dbSetOrder(5))
					If ZB8->(dbSeek(cChaveCte))
						While !ZB8->(Eof()) .And. AllTrim(ZB8->ZB8_CTE) == AllTrim(cChaveCte)
							If ZB8->ZB8_EDISIT != '4'
								lImportaCTe := .T.
								GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - Cancelamento ou substitui��o eliminou o registro pendente de processamento')
								GFELog118:Add(SB15 + SB4 + 'com a chave "' + cChaveCte + '".')
								u_SF005DEL()
							EndIf

//Se j� encontrar registro de exclus�o, n�o precisa criar outro registro.
							If ZB8->ZB8_ACAO = 'E' .And. !(ZB8->ZB8_EDISIT $ '2;4')
								lTemReg := .T.
							EndIf

							ZB8->(dbSkip())
						EndDo
					EndIf

					If !lTemReg
						lImportaCte := .T.
//Faz um c�pia do documento de frete para ZB8
						RecLock("ZB8",.T.)
						nNRIMP := GETSXENUM("ZB8", "ZB8_NRIMP") 	//(Numero da Importa��o) - Busca o numero sequencial
						ZB8->ZB8_NRIMP  :=  nNRIMP
						ConfirmSX8()
//If !IsBlind()
//	ZB8->ZB8_MARKBR := oBrowse115:Mark()
//EndIf
						ZB8->ZB8_FILIAL :=	xFilial("ZB8")
						ZB8->ZB8_FILDOC :=	GW3->GW3_FILIAL
						ZB8->ZB8_CDESP  :=	GW3->GW3_CDESP
						ZB8->ZB8_EMISDF :=	GW3->GW3_EMISDF
						ZB8->ZB8_SERDF  :=	GW3->GW3_SERDF
						ZB8->ZB8_NRDF   :=	GW3->GW3_NRDF
						ZB8->ZB8_DTEMIS :=	GW3->GW3_DTEMIS
						ZB8->ZB8_CDREM  :=	GW3->GW3_CDREM
						ZB8->ZB8_CDDEST :=	GW3->GW3_CDDEST
						ZB8->ZB8_TPDF   :=	GW3->GW3_TPDF
						ZB8->ZB8_DTENT  :=	GW3->GW3_DTENT
						ZB8->ZB8_CDCONS :=	''
						ZB8->ZB8_CFOP   :=	GW3->GW3_CFOP
						ZB8->ZB8_TRBIMP :=	GW3->GW3_TRBIMP
						ZB8->ZB8_VLIMP  :=	GW3->GW3_VLIMP
						ZB8->ZB8_BASIMP :=	GW3->GW3_BASIMP
						ZB8->ZB8_PCIMP  :=	GW3->GW3_PCIMP
						ZB8->ZB8_PESOR  :=	GW3->GW3_PESOR
						ZB8->ZB8_FRPESO :=	GW3->GW3_FRPESO
						ZB8->ZB8_FRVAL  :=	GW3->GW3_FRVAL
						ZB8->ZB8_PEDAG  :=	GW3->GW3_PEDAG
						ZB8->ZB8_TAXAS  :=	GW3->GW3_TAXAS
						ZB8->ZB8_VLDF   :=	GW3->GW3_VLDF
						ZB8->ZB8_ORISER :=	GW3->GW3_ORISER
						ZB8->ZB8_ORINR  :=	GW3->GW3_ORINR
						ZB8->ZB8_ACAO   :=	'E' //Exclus�o
						ZB8->ZB8_EDISIT :=	'1' //1=Importado com sucesso,2=Importado com erros
						ZB8->ZB8_EDIMSG :=	"" //Mensagem informativa
						ZB8->ZB8_EDIARQ :=	cXMLFile
						ZB8->ZB8_DTIMP  :=	DDATABASE
						ZB8->ZB8_CTE    :=	cChaveCte

						ZB8->ZB8_ORIGEM :=	'2' //CT-e
						ZB8->ZB8_ALTER  :=	'2'

						If Len(cTpCte) == 1
							ZB8->ZB8_TPCTE	:= cTpCte
						ElseIf cTpCte == "110111"
							ZB8->ZB8_TPCTE	:= "2"
						EndIf

						If GXGTIDCBI()
							ZB8->ZB8_QTDCS  := GW3->GW3_QTDCS
							ZB8->ZB8_VLCARG := GW3->GW3_VLCARG
							ZB8->ZB8_VOLUM	:= GW3->GW3_VOLUM
							ZB8->ZB8_QTVOL	:= GW3->GW3_QTVOL
						EndIf
						MsUnlock()

						aOriDoc[1] := GW3->GW3_SERDF
						aOriDoc[2] := GW3->GW3_NRDF
						dbSelectArea("GW4")
						GW4->(dbSetOrder(1))

						If GW4->(dbSeek(GW3->GW3_FILIAL+GW3->GW3_EMISDF+GW3->GW3_CDESP+GW3->GW3_SERDF+GW3->GW3_NRDF))
							While GW4->(!Eof()) .And. ;
									GW3->GW3_FILIAL	== GW4->GW4_FILIAL	.And. ;
									GW3->GW3_EMISDF	== GW4->GW4_EMISDF	.And. ;
									GW3->GW3_CDESP	== GW4->GW4_CDESP	.And. ;
									GW3->GW3_SERDF	== GW4->GW4_SERDF	.And. ;
									GW3->GW3_NRDF		== GW4->GW4_NRDF

								RecLock("ZB9",.T.)
								ZB9->ZB9_NRIMP	:= nNRIMP
								ZB9->ZB9_FILIAL	:= ZB8->ZB8_FILIAL	//ZB9_FILIAL
								ZB9->ZB9_SEQ		:= StrZero(nCont,5)	//ZB9_SEQ
								ZB9->ZB9_EMISDC	:= GW4->GW4_EMISDC	//ZB9_EMISDC
								ZB9->ZB9_SERDC	:= GW4->GW4_SERDC 	//ZB9_SERDC
								ZB9->ZB9_NRDC		:= GW4->GW4_NRDC  	//ZB9_NRDC
								ZB9->ZB9_TPDC		:= GW4->GW4_TPDC		//ZB9_TPDC
								ZB9->ZB9_FILDC	:= GW4->GW4_FILIAL
								nCont++
								MsUnLock("ZB9")
								GW4->(dbSkip())
							EndDo
						EndIf
					EndIf

				Else // N�o encontrou documento de frete para anular/substituir ou cancelar
					dbSelectArea("ZB8")
					ZB8->(dbSetOrder(5))

//Procura registro intermedi�rio com a mesma chave
					If ZB8->(dbSeek(cChaveCte))
						While !ZB8->(Eof()) .And. AllTrim(ZB8->ZB8_CTE) == AllTrim(cChaveCte)
							If ZB8->ZB8_EDISIT != '4' .And. ZB8->ZB8_ACAO == 'I'
//Elimina registro n�o processado de inclus�o
								lExcIncl := .T.
								GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - Cancelamento ou substitui��o eliminou o registro de inclus�o pendente de processamento')
								GFELog118:Add(SB15 + SB4 + 'com a chave "' + cChaveCte + '".')
								u_SF005DEL()
							ElseIf ZB8->ZB8_EDISIT != '4' .And. ZB8->ZB8_ACAO == 'E'
								If ZB8->ZB8_EDISIT != '2'
									lTemReg := .T.
								Else
//Elimina registro importado com erro de exclus�o
									GFELog118:Add(GFENOW(.F.,,,':','.') + SB4 + ' - Cancelamento ou substitui��o eliminou o registro de exclus�o pendente de processamento')
									GFELog118:Add(SB15 + SB4 + 'com a chave "' + cChaveCte + '".')
									u_SF005DEL()
								EndIf
							EndIf
							ZB8->(dbSkip())
						EndDo
					EndIf
					If lTemReg == .F.
//Caso n�o exista ser� criado um regristro de exclus�o com a chave do Conhecimento
//para processamento futuro
						RecLock("ZB8",.T.)
						nNRIMP := GETSXENUM("ZB8", "ZB8_NRIMP") 	//(Numero da Importa��o) - Busca o numero sequencial
						ZB8->ZB8_NRIMP  :=  nNRIMP
						ConfirmSX8()
//If !IsBlind()
//	ZB8->ZB8_MARKBR := oBrowse115:Mark()
//EndIf
						ZB8->ZB8_FILIAL :=	xFilial("ZB8")
						ZB8->ZB8_ACAO   :=	'E' //Exclus�o
						ZB8->ZB8_EDISIT :=	If(lExcIncl,'4','2') //1=Importado com sucesso,2=Importado com erros,4=Processado com sucesso
						ZB8->ZB8_CTE    :=	cChaveCte
						If lExcIncl
							ZB8->ZB8_EDIMSG :=	"N�o existe o documento de frete com a Chave CT-e " + cChaveCte  + "." + CRLF + "Os conhecimentos relacionados, pendentes de processamento, foram eliminados." //Mensagem informativa
						Else
							ZB8->ZB8_EDIMSG :=	"N�o existe o documento de frete com a Chave CT-e " + cChaveCte //Mensagem informativa
						EndIf
						ZB8->ZB8_EDIARQ :=	cXMLFile
						ZB8->ZB8_DTIMP  :=	DDATABASE
						ZB8->ZB8_ORIGEM :=	'2' //CT-e
						ZB8->ZB8_ALTER  :=	'2'
						MsUnlock()
					EndIf

					lImportaCte := .T.

				EndIf
			Else //Deveria possuir a chave ct-e anulada
				aOriDoc[3] := .F.
				cMsgPreVal += "- Chave do CTe n�o encontrada no arquivo.
			EndIf
		EndIf
	EndIf

	RestArea(aAreaZB8)
	RestArea(aAreaGW3)
Return aOriDoc

//-------------------------------------------------------------------
/*/{Protheus.doc} CteTPDF
Retorna qual o tipo de documento de frete a ser atribu�do ao CT-e sendo importado.

O tipo � definido utilizando uma combina��o das seguintes tags do arquivo Ct-e:
- tpCte
- tpServ
- xCaracAd
- vRec

@param oCte, Arquivo Ct-e

@version 1.0
/*/
//-------------------------------------------------------------------
Static Function CteTPDF(oCTE, aZB8, aZB9)
	Local cTPDF 	:= '0'
	Local cTpCte	:= ''
	Local cTpServ	:= ''
	Local cXCaracAd	:= ''
	Local cVRec		:= ''

	cTpCte 		:= XmlValid(oCTE,{"_INFCTE","_IDE"},"_TPCTE")
	cTpServ 	:= XmlValid(oCTE,{"_INFCTE","_IDE"},"_TPSERV")
	cVRec 		:= XmlValid(oCTE,{"_INFCTE","_VPREST"},"_VREC")

	If XmlChildEx(oCTE:_INFCTE,"_COMPL") != Nil .And. XmlChildEx(oCTE:_INFCTE:_COMPL,"_XCARACAD") != Nil
		cXCaracAd := XmlValid(oCTE,{"_INFCTE","_COMPL"},"_XCARACAD")
	EndIf

	Do Case
		// TPDF = Devolu��o
	Case cTpCte == '0' .And. cTpServ == '0' .And. Upper(cXCaracAd) $ 'DEVOLUCAO'
		cTPDF := '5'

		// TPDF = Reentrega
	Case cTpCte == '0' .And. cTpServ == '0' .And. AT('REENTREGA', Upper(cXCaracAd)) <> 0
		cTPDF := '4'


		// TPDF = Redespacho
	Case cTpCte == '0' .And. cTpServ $ '1;2;3;4'
		cTPDF := "6" // Redespacho
		If  cTpServ == '1'
			dbSelectArea("GWH")
			dbSetOrder(2)
			If !Empty(aZB9)
				If !Empty(aZB9[1][1])
					If dbSeek(aZB9[1][1]+aZB9[1][6]+aZB9[1][3]+aZB9[1][4]+aZB9[1][5])
						dbSelectArea("GWU")
						GWU->( dbSetOrder(1) )
						GWU->( dbSeek(GWH->GWH_FILIAL +GWH->GWH_CDTPDC+GWH->GWH_EMISDC +GWH->GWH_SERDC+GWH->GWH_NRDC))
						While !GWU->( Eof() ) .And. GWU->GWU_FILIAL == GWH->GWH_FILIAL .And. GWU->GWU_CDTPDC == GWH->GWH_CDTPDC .And. ;
								GWU->GWU_EMISDC == GWH->GWH_EMISDC .And. GWU->GWU_SERDC == GWH->GWH_SERDC .And. ;
								GWU->GWU_NRDC == GWH->GWH_NRDC
							dbSelectArea("GWF")
							dbSetOrder(1)
							If dbSeek(xFilial("GWF")+GWH->GWH_NRCALC)
								If AllTrim(GWU->GWU_CDTRP) == AllTrim(aZB8[03])
									If GWU->GWU_SEQ == "01"
										cTPDF := "1" // Normal
									Else
										cTPDF := "6" // Redespacho
									EndIf
									Exit
								EndIf
								DbSelectArea("GWU")
								GWU->( DbSkip() )
							EndIf
						EndDo
					EndIf
				Endif
			Endif
		Endif

		// CTE COMPLEMENTAR
	Case cTpCte == '1'
		If !Empty(cVRec) .And. !Empty(aZB8[14]) .And. Val(aZB8[14]) == Val(cVRec)
				// TPDF = Complementar Imposto
			cTPDF := '3'
		Else
				// TPDF = Complementar Valor
			cTPDF := '2'
		EndIf

		// TPDF Padr�o = Normal
	Otherwise
		cTPDF := '0'

			//Chamado TQCIIG.
			//Ajuste para que em casos onde o CT-e venha com Tipo de Servi�o normal, por�m � tratado
			//no GFE como um redespacho, faz a altera��o do Tipo de Servi�o para Redespacho.
			//posiciona a GW1 com base na aZB9.
		If !(Upper(cXCaracAd) $ 'DEVOLUCAO') .Or. !(AT('REENTREGA', Upper(cXCaracAd)) <> 0)
			If !Empty(aZB9)
				If !Empty(aZB9[1][1])
					dbSelectArea("GW1")
					GW1->( dbSetOrder(1) )
					If GW1->( dbSeek(aZB9[1][1]+aZB9[1][6]+aZB9[1][3]+aZB9[1][4]+aZB9[1][5]) )

						dbSelectArea("GWU")
						GWU->( DbSetOrder(1) )
						If GWU->( DbSeek(GW1->GW1_FILIAL + GW1->GW1_CDTPDC + GW1->GW1_EMISDC + GW1->GW1_SERDC + GW1->GW1_NRDC) )

							While !GWU->( Eof() ) .And. GWU->GWU_FILIAL == GW1->GW1_FILIAL .And. GWU->GWU_CDTPDC == GW1->GW1_CDTPDC .And. ;
									GWU->GWU_EMISDC == GW1->GW1_EMISDC .And. GWU->GWU_SERDC == GW1->GW1_SERDC .And. ;
									GWU->GWU_NRDC == GW1->GW1_NRDC

								If AllTrim(GWU->GWU_CDTRP) == AllTrim(aZB8[03])
									If GWU->GWU_SEQ != "01"
										cTPDF := "6" // Redespacho
									EndIf
									Exit
								EndIf

								DbSelectArea("GWU")
								GWU->( DbSkip() )
							EndDo

						EndIf
					EndIf
				EndIf
			EndIf
		EndIf

	EndCase

Return cTPDF

//-------------------------------------------------------------------
/*/{Protheus.doc} XmlValid

Fun��o responsavel por validar se o caminho informado corresponde a um objeto valido no xml
Caso seja, ele grava a informa��o TEXT do XML no campos informado

@param cXMLDir, caminho do arquivo que esta sendo importado
@param oXML, Objeto   - XML
@param cChild, Caracter - Caminho para valida��o

@version 1.0
/*/
//-------------------------------------------------------------------
Static Function XmlValid(oTEMP,aNode,cTag,lREALNAME)
	Local nCont
	Private oXML := oTEMP
	Default lREALNAME := .F.

	//Navega dentro do objeto XML usando a variavel aNode como base, retornando o conteudo do TEXT ou o
	For nCont := 1 to Len(aNode)

		If ValType( XmlChildEx( oXML,aNode[nCont]  ) ) == 'O'
			oXML :=  XmlChildEx( oXML,aNode[nCont]  )
		Else
			Return
		Endif

		If nCont == Len(aNode)
			If !lREALNAME
				cReturn := &("oXML:"+cTag+':TEXT')
				Return cReturn
			Else
				cReturn := &("oXML:REALNAME")
				Return cReturn
			Endif
		EndIf

	Next nCont

	FreeObj(oXML)
	FreeObj(xRet)
	FreeObj(xRet1)

Return ''

//-------------------------------------------------------------------
/*/{Protheus.doc} A118EMIT

Busca remetente, destinat�rio ou transportador do CT-e/EDI no cadastro de emitentes do GFE (GU3)

aZB9[n][1] //ZB9_FILDC
aZB9[n][2] //ZB9_SEQ
aZB9[n][3] //ZB9_EMISDC
aZB9[n][4] //ZB9_SERDC
aZB9[n][5] //ZB9_NRDC
aZB9[n][6] //ZB9_TPDC
aZB9[n][7] //ZB9_NFE
aZB9[n][8] //ZB9_CNPJEM

@type function
@author Sam Barros
@since 08/07/2016
@param cTipo, 1-Emitente;2-Remetente;3-Destinat�rio
@param cIDFED, CNPJ Ou CPF do emitente;remetente;destinat�rio
@param aZB9, Array contendo as notas sendo importadas

@version 1.0
/*/
//-------------------------------------------------------------------
User Function A118EMIT(cTipo, cIDFED, aZB9, aZB8)
	Local cEmit		:= ""
	
	//cDevol := 'N'
	
	Do Case
	Case cTipo == "1" // 1=Emitente
		cEmit := ValidEmis(3,xFilial("SA2")+cIDFED)
	Case cTipo == "2" // 2=Remetente
		cEmit := ValidEmis(3,xFilial("SA2")+cIDFED)
		
		//Devolu��o
		if empty(cEmit)// .OR. cDevol == 'S'
			cEmit := ValidDest(3,xFilial("SA1")+cIDFED)
			cDevol := 'S'
		EndIf
	Case cTipo == "3" // 3=Destinat�rio
		cEmit := ValidDest(3,xFilial("SA1")+cIDFED)
		
		//Devolu��o
		if empty(cEmit)// .OR. cDevol == 'S'
			cEmit := ValidDest(3,xFilial("SA2")+cIDFED)
			cDevol := 'S'
		EndIf
	Case cTipo == "4" // 4=Tomador
		cEmit := ValidEmis(3,xFilial("SA2")+cIDFED)
		
		if empty(cEmit)
			cIDFED := U_wSM0CNPJ(aZB8[1])
			ValidEmis(3,xFilial("SA2")+cIDFED)
		EndIf
		
		//Devolu��o
		/*
		if empty(cEmit) .OR. cDevol == 'S'
			cEmit := ValidDest(3,xFilial("SA1")+cIDFED)
			cDevol := 'S'
		EndIf
		*/
	EndCase
Return cEmit

Static Function X2MODO(cTab)
	Local cRet := ''

	dbSelectArea("SX2")
	SX2->(dbSetOrder(1))
	If SX2->( dbSeek(cTab) )
		cRet := SX2->X2_MODO
	EndIf
Return cRet

//
// Busca emitente do tipo transportador e ativo na tabela GU3
// nIndi = Qual �ndice a ser usado na busca
// cChave = chave do �ndice
// Ex: ValidEmis(11,"000000000000") // Busca por CPF
//
Static Function ValidEmis(nIndi,cChave)
	dbSelectArea("SA2")
	SA2->( dbSetOrder(nIndi) )

	If SA2->( dbSeek(cChave) )
		While !SA2->( EoF() ) .And. AllTrim(&(SA2->(IndexKey(nIndi)))) == AllTrim(cChave)
			IF SA2->A2_MSBLQL <> '1'
				Return SA2->A2_COD + SA2->A2_LOJA
			ENDIF
			SA2->( dbSkip())
		EndDo
	EndIf
Return ""

Static Function ValidDest(nIndi,cChave)
	dbSelectArea("SA1")
	SA1->( dbSetOrder(nIndi) )

	If SA1->( dbSeek(cChave) )
		While !SA1->( EoF() ) .And. AllTrim(&(SA1->(IndexKey(nIndi)))) == AllTrim(cChave)
			IF SA2->A2_MSBLQL <> '1'
				Return SA1->A1_COD + SA1->A1_LOJA
			ENDIF
			SA1->( dbSkip())
		EndDo
	EndIf
Return ""

/***********************************************************************************************************
Fun�oes para buscar as informa��es no XML
 ***********************************************************************************************************/

Static Function CalcVolum()// Carrega os dois campos ZB8_VOLUM e ZB8_QTVOL com informa��es do XML
	Local oXml
	Local nX
	Local aPesos := {0,0,0,0,0} //1-Bruto;2-Cubado;3-Aferido;4-Declarado;5-Aforado

	// Tag _INFCTENORM n�o existe em arquivos Ct-e de copmlemento de valores e anula��o de valores
	If cTpCte $ "1;2"
		Return
	EndIf

	oXml := oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ

	If ValType(oXml) == "A"
		For nX := 1 To Len(oXml)
			If oXml[nX]:_CUNID:TEXT == '00' // Volume em M�
				ZB8->ZB8_VOLUM := SetField(Val(oXml[nX]:_QCARGA:TEXT), "ZB8_VOLUM")
			ElseIf oXml[nX]:_CUNID:TEXT == '01' // KG
				If oXml[nX]:_TPMED:TEXT == 'PESO BRUTO'
					aPesos[1] := Val(oXml[nX]:_QCARGA:TEXT)
				ElseIf oXml[nX]:_TPMED:TEXT == 'PESO CUBADO'
					aPesos[2] := Val(oXml[nX]:_QCARGA:TEXT)
				ElseIf oXml[nX]:_TPMED:TEXT == 'PESO AFERIDO'
					aPesos[3] := Val(oXml[nX]:_QCARGA:TEXT)
				ElseIf oXml[nX]:_TPMED:TEXT == 'PESO DECLARADO'
					aPesos[4] := Val(oXml[nX]:_QCARGA:TEXT)
				ElseIf oXml[nX]:_TPMED:TEXT == 'PESO AFORADO'
					aPesos[5] := Val(oXml[nX]:_QCARGA:TEXT)
				EndIf
			ElseIf oXml[nX]:_CUNID:TEXT == '03' // Unidades
				ZB8->ZB8_QTVOL := SetField(NoRound(Val(oXml[nX]:_QCARGA:TEXT)), "ZB8_VOLUM")
			EndIf
		Next nX
		If Empty(ZB8->ZB8_PESOR)
			For nX := 1 To Len(aPesos)
				If aPesos[nX] > 0
					ZB8->ZB8_PESOR := SetField(aPesos[nX], "ZB8_PESOR")
					Exit
				EndIf
			Next nX
		EndIf
	ElseIf ValType(oXml) == "O"
		If oXml:_CUNID:TEXT == '00' // Volume em M�
			ZB8->ZB8_VOLUM := Val(oXml:_QCARGA:TEXT)
		ElseIf oXml:_CUNID:TEXT == '01' //KG
			If oXml:_TPMED:TEXT $ 'PESO BRUTO|PESO CUBADO|PESO AFERIDO|PESO DECLARADO|PESO AFORADO'
				ZB8->ZB8_PESOR := SetField(Val(oXml:_QCARGA:TEXT), "ZB8_PESOR")
			EndIf
		ElseIf oXml:_CUNID:TEXT == '03' // Unidades
			ZB8->ZB8_QTVOL := SetField(NoRound(Val(oXml:_QCARGA:TEXT)), "ZB8_VOLUM")
		EndIf
	EndIf

Return

// ----
Static Function SetField(nValor, cCampo, lValid)
	Local cValor
	Local nRet
	Local aTamSX3 := TamSX3(cCampo)

	Default nValor := 0
	Default lValid := .T.

	cValor := cValToChar(Round(nValor, 0))
	cValor := StrTran(cValor, ".", "")
	cValor := StrTran(cValor, ",", "")

	If aTamSX3[2] > 0
		cValor := AllTrim(Transform(nValor, Replicate("9", aTamSX3[1]) + "." + Replicate("9", aTamSX3[2])))
	Else
		cValor := AllTrim(Transform(nValor, Replicate("9", aTamSX3[1])))
	EndIf


	If Len(cValor) > aTamSX3[1]

		If lValid
			cMsgPreVal += "- " + "Erro no campo: " + cCampo + ". Valor '" + cValToChar(nValor) + "' com formato inv�lido ou n�o suportado." + CRLF
		EndIf

		Return(0)

	EndIf
  
	nRet   := Val(cValor)

Return(nRet)

Static Function CalcVlCar()
	Local nVl

	If XmlValid(oCTe,{'_INFCTE','_INFCTENORM','_INFCARGA','_VCARGA'},'_VCARGA',.T.) == "vCarga"
		nVl := Val(XmlValid(oCTe,{'_INFCTE','_INFCTENORM','_INFCARGA'},'_VCARGA'))
	ElseIf XmlValid(oCTe,{'_INFCTE','_INFCTENORM','_INFCARGA','_VMERC'},'_VMERC',.T.) == "vMerc"
		nVl := Val(XmlValid(oCTe,{'_INFCTE','_INFCTENORM','_INFCARGA'},'_VMERC'))
	Else
		nVl := 0
	EndIf
Return nVl

Static function getValCarg(cSeqImp)
	Local nRet := 0
	Local xArea := getArea()
	Local _cAlias := GetNextAlias()
	
	cSql += " SELECT SUM(D2_TOTAL) AS VLCARGA "
	cSql += " FROM " + RetSqlName("ZB9") + " ZB9, "
	cSql += " 	" + RetSqlName("SF2") + " SF2, "
	cSql += " 	" + RetSqlName("SD2") + " SD2 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SD2.D_E_L_E_T_ = '' "
	cSql += "  "
	cSql += " AND SF2.F2_CHVNFE = ZB9.ZB9_DANFE "
	cSql += "  "
	cSql += " AND SF2.F2_FILIAL = SD2.D2_FILIAL "
	cSql += " AND SF2.F2_DOC = SD2.D2_DOC "
	cSql += " AND SF2.F2_SERIE = SD2.D2_SERIE "
	cSql += " AND SF2.F2_CLIENTE = SD2.D2_CLIENTE "
	cSql += " AND SF2.F2_LOJA = SD2.D2_LOJA "
	cSql += "  "
	cSql += " AND ZB9.ZB9_NRIMP = '" + cSeqImp + "' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		nRet += (_cAlias)->VLCARGA
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
	
	RestArea(xArea)
Return nRet

//-------------------------------------------------------------------
/*/{Protheus.doc} SF005MARK

Marca��es do browse
@type function
@author Sam Barros
@since 08/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SF005MARK()
	Local aAreaZB8 := GetArea()

	dbSelectArea("ZB8")
	ZB8->(dbGoTop())

	While ZB8->(!Eof())
		If (ZB8_MARKBR <> oBrowse115:Mark())
			RecLock("ZB8", .F.)
			ZB8->ZB8_MARKBR := oBrowse115:Mark()
			ZB8->(MsUnlock())
		ElseIf (ZB8_MARKBR == oBrowse115:Mark())
			RecLock("ZB8", .F.)
			ZB8->ZB8_MARKBR := "  "
			ZB8->(MsUnlock())
		EndIf
		ZB8->(dbSkip())
	EndDo

	RestArea(aAreaZB8)

	oBrowse115:Refresh()
	oBrowse115:GoTop()
Return Nil

/*****************************************************************************************************
Fim Das Valida��es do XML
 *****************************************************************************************************/

 // Verifica exist�ncia de um �ndice no dicion�rio
Static Function ValidSIX(cTab, cOrdem)
	dbSelectArea("SIX")
	SIX->(dbSetOrder(1))
	If SIX->(dbSeek(cTab+cOrdem))
		Return .T.
	EndIf
Return .F.

User Function SF005GW3(cChaveCte)
	Local aAreaGW3 := GW3->(GetArea())
	dbSelectArea("GW3")
	GW3->(dbSetOrder(14))
	If Empty(cChaveCte)
		Help(,,'HELP',,'Chave do CT-e n�o informado',1,0)
	ElseIf !GW3->(dbSeek(cChaveCte))
		Help(,,'HELP',,"N�o existe o documento de frete com a Chave CT-e " + cChaveCte,1,0)
	Else
		FWExecView("Visualizar", "GFEC065", 1,, {||.T.})
	EndIf
	RestArea(aAreaGW3)
Return


 // Consultar documento nas tabelas de Notas fiscais
Static Function ValidCFOP( cDoc, cSerie, cCgcRem, cCgcDest, cNFE )
	Local lRet := .F.
	Local cQuery := ""

	Local cFil		:= xFilial("GW1")

	Local cQueryA:= ""
	Local cQueryB:= ""
	Local cRemet	:= ""
	Local cDestn	:= ""
	Local cLojaR	:= ""
	Local cLojaD	:= ""
	Local cInteg	:= ""
	Local cD1_CfOP := ""
	Local cD2_CfOP := ""

	If cCgcRem <> ""
		//Busca Codigo do Cliente/Fornecedor e da Loja na tabela SA2
		cQuery := "SELECT SA2.A2_COD, SA2.A2_LOJA "
		cQuery += " FROM " + RetSqlName("SA2") + " SA2 "
		cQuery += " WHERE SA2.A2_CGC = '" + cCgcRem + "' "

		cRemet := Ret1ValSql(cQuery, "A2_COD")
		cLojaR  := Ret1ValSql(cQuery, "A2_LOJA")
	EndIf

	//PESQUISAR NA TABELA SF1/SD1
	cQueryA := "SELECT SD1.D1_CF D1CFOP"
	cQueryA += " FROM " + RetSqlName("SF1") + " SF1 , " + RetSqlName("SD1") + " SD1 "
	cQueryA += " WHERE SF1.F1_FILIAL = SD1.D1_FILIAL "
	cQueryA += " AND SF1.F1_DOC = SD1.D1_DOC "
	cQueryA += " AND SF1.F1_SERIE = SD1.D1_SERIE "
	cQueryA += " AND SF1.F1_FORNECE = SD1.D1_FORNECE "
	cQueryA += " AND SF1.F1_LOJA = SD1.D1_LOJA "
	cQueryA += " AND SF1.F1_FILIAL = '" + cFil + "' "

	If (AllTrim(cDoc) != '')
		cQueryA += " AND SF1.F1_DOC = '" + cDoc+ "' "
		cQueryA += " AND SF1.F1_SERIE = '" + cSerie + "' "
		cQueryA += " AND SF1.F1_FORNECE = '" + cRemet + "' "
		cQueryA += " AND SF1.F1_LOJA = '" + cLojaR + "' "
	EndIf

	If AllTrim(cNFE) != ""
		cQueryA += " AND SF1.F1_CHVNFE = '" + cNFE + "' "
	EndIf

	cD1_CfOP := Ret1ValSql(cQueryA, "D1CFOP")

	If AllTrim(cD1_CfOP) != "" //Se encontrou registro na tabela SF1
		// Procura o CFOP na tabela GW0
		lRet := ValFxCFOP( cD1_CfOP, cFil, @cInteg )
	Else

		If cCgcDest <> ""
			//Busca Codigo do Cliente/Fornecedor e da Loja na tabela SA2
			cQuery := "SELECT SA2.A2_COD, SA2.A2_LOJA "
			cQuery += " FROM " + RetSqlName("SA2") + " SA2 "
			cQuery += " WHERE SA2.A2_CGC = '" + cCgcDest + "' "

			cDestn := Ret1ValSql(cQuery, "A2_COD")
			cLojaD  := Ret1ValSql(cQuery, "A2_LOJA")
		EndIf

		//PESQUISAR NA TABELA SF2/SD2
		cQueryB := "SELECT SD2.D2_CF D2CFOP "
		cQueryB += " FROM " + RetSqlName("SF2") + " SF2, " + RetSqlName("SD2") + " SD2 "
		cQueryB += " WHERE SF2.F2_FILIAL = SD2.D2_FILIAL "
		cQueryB += " AND SF2.F2_DOC = SD2.D2_DOC "
		cQueryB += " AND SF2.F2_SERIE = SD2.D2_SERIE "
		cQueryB += " AND SF2.F2_CLIENTE = SD2.D2_CLIENTE "
		cQueryB += " AND SF2.F2_LOJA = SD2.D2_LOJA "
		cQueryB += " AND SF2.F2_FILIAL = '" + cFil + "' "

		If (AllTrim(cDoc) != '')
			cQueryB += " AND SF2.F2_DOC = '" + cDoc+ "' "
			cQueryB += " AND SF2.F2_SERIE = '" + cSerie + "' "
			cQueryB += " AND SF2.F2_CLIENTE = '" + cDestn + "' "
			cQueryB += " AND SF2.F2_LOJA = '" + cLojaD + "' "
			lQ := .T.
		EndIf

		If AllTrim(cNFE) != ""
			cQueryB += " AND SF2.F2_CHVNFE = '" + cNFE + "' "
			lQ := .T.
		EndIf

		if lQ
			cD2_CfOP := Ret1ValSql(cQueryB, "D2CFOP")
		Else
			cD2_CfOP :=  ""
		EndIf

		If AllTrim(cD2_CfOP) != ""
			lRet := ValFxCFOP( cD2_CfOP, cFil, @cInteg )
		EndIf

	EndIf

	If lRet
		If cInteg = "G"
			//Ent�o Integra GFE
			lRet := .T.
		Else
			//Ent�o Integra Compras
			lRet := .F.
		EndIf
	Else
		//CFOP N�o encontrado na tabela GW0 para a filial, Ver GW0_TABELA = 'INTTOTVSCOLAB'
		lRet := .T.
	EndIf

Return lRet


// Verifica a Faixa de CFOP para integra��o
Static Function ValFxCFOP( cCFOP, cFil, cInteg  )

	 //Valores para cInteg=> "C=>Compras", "G=>GFE"

	Local lRet := .T.
	Local cQuery := ""
	Local cCHAR01 := ""
	Local cCHAR02 := ""

	cQuery :=	"SELECT GW0.GW0_CHAR02 "
	cQuery +=	" From " + RetSqlName("GW0") + " GW0 "
	cQuery	+=	" where GW0.GW0_FILIAL = '" + cFil + "' "
	cQuery +=	" and GW0.GW0_TABELA = 'INTTOTVSCOLAB' "
	cQuery +=	" and '" + AllTrim(cCFOP) + "' between GW0.GW0_CHAR03 and GW0.GW0_CHAR04 "

	cCHAR02  := Ret1ValSql(cQuery, "GW0_CHAR02")

	If AllTrim(cChar02) != ""
		cInteg := AllTrim(cChar02)
	Else
		//Procurar o tipo de integra��o para "N�o Listado"
		cQuery :=	"SELECT GW0.GW0_CHAR01 "
		cQuery +=	" From " + RetSqlName("GW0") + " GW0 "
		cQuery	+=	" where GW0.GW0_FILIAL = '" + cFil + "' "
		cQuery +=	" and GW0.GW0_TABELA = 'INTTOTVSCOLAB' "
		cQuery +=	" and RTrim(GW0.GW0_CHAVE) = '001' "

		cCHAR01  := Ret1ValSql(cQuery, "GW0_CHAR01")

		If AllTrim(cCHAR01) != ""
			cInteg := AllTrim(cCHAR01)
		Else
			cInteg := ""
			lRet := .F.
		EndIf
	EndIf

Return lRet

Static Function Ret1ValSql(cQuery, cCampo)

	local cRet := ""
	Local cAlias := GetNextAlias()

	cQuery := ChangeQuery(cQuery)
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),cAlias,.F.,.T.)

	dbSelectArea(cAlias)
	(cAlias)->( dbGoTop() )
	While !(cAlias)->( Eof() )
		cRet := (cAlias)->&(cCampo)
		dbSkip()
	Enddo
	(cAlias)->( DBCloseArea() )

Return cRet

//Indica se a forma de recebimento de arquivos de CTe ser� por TOTVS Colabora��o 2.0
User Function GA118TC20()

	Local cTotvsColab	:= SuperGetMV("MV_SPEDCOL", .F., "N")
	Local cTotvsCol20	:= Alltrim( SuperGetMv("MV_TCNEW", .F. ,"" ) )

	//Valida��o de utiliza��o do Totvs Colabora��o 2.0
	//A mesma regra existe nos GFEA065/SASF005/GFEX000.
	//L�gica copiada da fun��o ColabGeneric.prw
	If cTotvsColab == "S" .And. ( ("0" $ cTotvsCol20) .Or. ("6" $ cTotvsCol20)) //0-Todos / 6-Recebimento
		Return .T.
	End If

Return .F.

Static Function GA118CrDir(cDir)

	If !ExistDir(cDir)
		If MakeDir(cDir) <> 0
			Help( ,, 'HELP',, "N�o foi poss�vel criar diret�rio " + cDir + " (Erro " + cValToChar(FError()) + ").",1,0)
			Return .F.
		EndIf
	EndIf

Return .T.

//Inclui a barra ('/' ou '\') no fim do diret�rio, caso haja necessidade.
Static Function SF005Bar(cDir)

	Local cBarra := If(isSrvUnix(),"/","\")

	If SubStr(cDir, Len(cDir), 1) != '/' .And. SubStr(cDir, Len(cDir), 1) != '\'
		cDir += cBarra
	EndIf

return cDir



//--------------------------------------------------------------------
/* BuscaDocTrechoPago
Busca o documento de carga que possuir trecho pago em todas filiais.
Reposiciona a GW1 no documento encontrado.

Par�metros:
	cDANFE, cEmisDC, cSerDC, cNrDc e cFilDC (opcional)

Retorno:
	Array[2]:
		[1] Sucesso
		[2] Filial do documento de carga
*/
//--------------------------------------------------------------------
Static Function BuscaDocTrechoPago(cDANFE, cEmisDC, cSerDC, cNrDc, cFilDC)
	Local aRet[2]
	Local cAliasGW1 := GetNextAlias()
	Local cQuery
	Default cFilDC := ""
	Default cDANFE := ""
	Default cEmisDC := ""
	Default cSerDC := ""
	Default cNrDc := ""

	cQuery := "SELECT GW1_FILIAL, GW1.R_E_C_N_O_ FROM " + RetSQLName("GW1") + " GW1 "
	cQuery += "RIGHT JOIN " + RetSQLName("GWU") + " GWU ON "
	cQuery += "GWU_FILIAL = GW1_FILIAL AND "
	cQuery += "GWU_CDTPDC = GW1_CDTPDC AND "
	cQuery += "GWU_EMISDC = GW1_EMISDC AND "
	cQuery += "GWU_SERDC  = GW1_SERDC AND "
	cQuery += "GWU_NRDC   = GW1_NRDC  AND "
	cQuery += "GWU_PAGAR = '1' AND "
	cQuery += "GWU.D_E_L_E_T_ = '' "
	cQuery += "WHERE "
	If !Empty(cFilDC)
		cQuery += "GW1_FILIAL = '" + cFilDC + "' AND "
	EndIf

	If !Empty(cDANFE)
		cQuery += "GW1_DANFE = '" + cDANFE + "' AND "
	Else
		cQuery += "GW1_EMISDC = '" + cEmisDC + "' AND GW1_SERDC = '" + cSerDC + "' AND GW1_NRDC = '" + cNrDc + "' AND "
	EndIf
	cQuery += "GW1.D_E_L_E_T_ = ''"
	cQuery := ChangeQuery(cQuery)

	If IsInCallStack("u_SF005XML")
		GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + " - Busca documento com trecho pago. Danfe: " + cDANFE)
		GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + " - Busca documento com trecho pago. SQL: [" + cQuery + "]")
	EndIf

	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery),cAliasGW1, .F., .T.)

	dbSelectArea(cAliasGW1)
	(cAliasGW1)->( dbGoTop() )
	If !(cAliasGW1)->(Eof())
		dbSelectArea("GW1")
		dbGoTo((cAliasGW1)->R_E_C_N_O_)

		If IsInCallStack("u_SF005XML")
			GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + " - Busca documento com trecho pago. Filial: " + (cAliasGW1)->GW1_FILIAL)
		EndIf

		aRet[1] := .T.
		aRet[2] := (cAliasGW1)->GW1_FILIAL
	Else
		aRet[1] := .F.
		aRet[2] := ""
	EndIf

	(cAliasGW1)->(DBCloseArea())

Return aRet

//--------------------------------------------------------------------
/* GA118ExtDirOrFileName
Extrai o nome do arquivo de um caminho completo
Par�metros:
	Char: caminho completo do arquivo
	Number: indica se retornar� o caminho (1) ou o nome do arquivo (2).
Retorno:
	Char: nome do arquivo
*/
//--------------------------------------------------------------------
Static Function GA118ExtDirOrFileName(cFile,nRet)
	Local nPos

	nPos := RAt(IIf(IsSrvUnix(), '/', '\'), cFile)
	If nPos > 0
		If nRet = 1
			cFile := Substr(cFile, 1, nPos)
		Else
			cFile := Substr(cFile, nPos+1, Len(cFile))
		EndIf
	EndIf

Return cFile

/*-------------------------------------------------------------------
{Protheus.doc} SF005MKT
Selecionar Todos

@type function
@author Sam Barros
@since 08/07/2016
@version 1.0
-------------------------------------------------------------------  */
User Function SF005MKT()
	Local aAreaZB8 := GetArea()

	dbSelectArea("ZB8")
	ZB8->(dbGoTop())

	While ZB8->(!Eof())
		If (ZB8_MARKBR <> oBrowse115:Mark())
			RecLock("ZB8", .F.)
			ZB8->ZB8_MARKBR := oBrowse115:Mark()
			ZB8->(MsUnlock())
		EndIf
		ZB8->(dbSkip())
	EndDo

	RestArea(aAreaZB8)

	oBrowse115:Refresh()
	oBrowse115:GoTop()
Return Nil

//--------------------------------------------------------------------
/* GA118MoveFile
Move para um diret�rio destino.
Grava no log os textos.

Retorno:
	Boolean: indica se a exclus�o foi efetuada com sucesso
*/
//--------------------------------------------------------------------

Static Function GA118MoveFile(cOrigem, cDestino, GFELog118)
	Local lRet := .T.

	//S� pode eliminar o arquivo caso consiga copi�-lo para pasta destino.
	IIf (GFELog118 <> nil, GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Tentativa de c�pia do arquivo da pasta ' + GA118ExtDirOrFileName(cOrigem,1) + ' para ' + GA118ExtDirOrFileName(cDestino,1) + '.'), '')
	Copy File &(cOrigem) To &(cDestino)

	If FError() <> 0
		IIf (GFELog118 <> nil, GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Erro ao copiar arquivo (' + STR(FERROR()) + ').'), '')
	Else
		IIf (GFELog118 <> nil, GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - C�pia efetuada com sucesso.'), '')

		If File(cDestino)
			IIf (GFELog118 <> nil, GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Arquivo ' + cDestino + ' encontrado.'), '')
		Else
			IIf (GFELog118 <> nil, GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Arquivo ' + cDestino + ' n�o encontrado.'), '')
		EndIf

		IIf (GFELog118 <> nil, GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Tentativa de exclus�o do arquivo da pasta ' + GA118ExtDirOrFileName(cOrigem,1) + '.'), '')
		If FErase(cOrigem) == -1
			IIf (GFELog118 <> nil, GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Erro ao excluir arquivo (' + STR(FERROR()) + ').'), '')
			lRet := .F.
		Else
			IIf (GFELog118 <> nil, GFELog118:Add(GFENOW(.F.,,,':','.') + SB6 + ' - Exclus�o efetuada com sucesso.'), '')
		EndIf
	EndIf

Return lRet

Static Function getTomador(cChvNFE, cFildoc)
	Local cSql := ""
	Local cChave := ""
	
	Local cFilTom := ""
	Local cFilRet := ""
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cChvNFE+"' "
	cSql += "  "
	cSql += "  "
	cSql += "  "
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	dbSelectArea("ZZ2")
	ZZ2->(dbSetOrder(8))
	ZZ2->(dbGoTop())
	If ZZ2->(dbSeek(cChave))
		If ZZ2->ZZ2_TIPCTR = '2'
			If !empty(ZZ2->ZZ2_PV01FL) .AND. !empty(ZZ2->ZZ2_PV03FL)
				cFilRet := ZZ2->ZZ2_FILIAL
			Else
				cFilRet := cFildoc
			EndIf
		Else
			If !empty(ZZ2->ZZ2_PV01FL)
				cFilRet := ZZ2->ZZ2_PV01FL
			EndIf
		EndIf
	Else
		ZZ2->(dbSetOrder(10))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChave))
			If ZZ2->ZZ2_TIPCTR = '2'
				If !empty(ZZ2->ZZ2_PV01FL) .AND. !empty(ZZ2->ZZ2_PV03FL)
					cFilRet := ZZ2->ZZ2_FILIAL
				Else
					cFilRet := cFildoc
				EndIf
			Else
				If !empty(ZZ2->ZZ2_PV01FL)
					cFilRet := ZZ2->ZZ2_PV01FL
				EndIf
			EndIf
		Else
			cFilRet := cFildoc
		EndIf
	EndIf
	
	ZZ2->(dbCloseArea())
	
Return cFilRet

User Function wSM0CNPJ(cCod)
	Local aArea := GetArea()
	Local aAreaM0 := SM0->(GetArea())
	Local cCGC := ""
	Local cRet := ""
 
	//Percorrendo o grupo de empresas
	SM0->(DbGoTop())
	While !SM0->(EoF())
		//Se a filial for encontrado, atualiza o CNPJ e finaliza
		If cCod == AllTrim(SM0->M0_CODFIL)
			cCGC := SM0->M0_CGC
			cRet := POSICIONE("SA2",3,xFilial("SA2")+cCGC,"A2_COD") + POSICIONE("SA2",3,xFilial("SA2")+cCGC,"A2_LOJA")
			Exit
		EndIf
     
		SM0->(DbSkip())
	EndDo
 
	RestArea(aAreaM0)
	RestArea(aArea)
Return cRet

User Function zSM0CNPJ(cCGC)
	Local aArea := GetArea()
	Local aAreaM0 := SM0->(GetArea())
	Local cFilRet := ""
 
//Percorrendo o grupo de empresas
	SM0->(DbGoTop())
	While !SM0->(EoF())
//Se o CNPJ for encontrado, atualiza a filial e finaliza
		If cCGC == SM0->M0_CGC
			cFilRet := SM0->M0_CODFIL
			Exit
		EndIf
     
		SM0->(DbSkip())
	EndDo
 
	RestArea(aAreaM0)
	RestArea(aArea)
Return cFilRet

Static Function SM0CNPJbyCOD(cCod, bTipo)
	Local aArea := GetArea()
	Local aAreaM0 := SM0->(GetArea())
	Local cFilRet := ""
	Local cCGC := ""
	
	if !bTipo
		cCGC := POSICIONE("SA2",1,xFilial("SA2")+cCod, "SA2->A2_CGC")
	Else
		cCGC := POSICIONE("SA1",1,xFilial("SA1")+cCod, "SA1->A1_CGC")
	EndIf
 
//Percorrendo o grupo de empresas
	SM0->(DbGoTop())
	While !SM0->(EoF())
//Se o CNPJ for encontrado, atualiza a filial e finaliza
		If cCGC == SM0->M0_CGC
			cFilRet := SM0->M0_CODFIL
			Exit
		EndIf
     
		SM0->(DbSkip())
	EndDo
 
	RestArea(aAreaM0)
	RestArea(aArea)
Return cFilRet

Static Function getPeso(oObj, cFornece, nPesoSubstituto)
	Local nPeso := 0
	Local cDescPeso := UPPER(ALLTRIM(POSICIONE("SA2", 1, xFilial("SA2")+cFornece, "SA2->A2_YPESCTE")))
	Local aDescPeso := Strtokarr(cDescPeso, ';')

	If (XmlChildEx ( oCTe ,"_INFCTE") <> Nil)
		If (XmlChildEx ( oCTe:_INFCTE, "_INFCTENORM") <> Nil)
			If (XmlChildEx ( oCTe:_INFCTE:_INFCTENORM, "_INFCARGA") <> Nil)
				If (XmlChildEx ( oCTe:_INFCTE:_INFCTENORM:_INFCARGA, "_INFQ") <> Nil)
					aPesos := oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ
					if valtype(oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ) == 'O'
						For x := 1 to len(aDescPeso)
							If UPPER(AllTRIM(oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ:_TPMED:TEXT)) == AllTrim(aDescPeso[x])//cDescPeso
								nPeso := VAL(oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ:_QCARGA:TEXT)
								exit
							EndIf
						Next
					Else
						_bEncerra := .F.
						For i := 1 to len(aPesos)
							For x:= 1 to len(aDescPeso)
								If UPPER(AllTRIM(oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ[i]:_TPMED:TEXT)) == AllTrim(aDescPeso[x])//cDescPeso
									nPeso := VAL(oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ[i]:_QCARGA:TEXT)
									_bEncerra := .T.
									exit
								EndIf
							Next x
							If _bEncerra
								exit
							EndiF
						Next i
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf

Return nPeso

Static Function getVolume(oObj)
	Local nVolume := 0
	Local cDescVolume := "03" //VOLUME

	If (XmlChildEx ( oCTe ,"_INFCTE") <> Nil)
		If (XmlChildEx ( oCTe:_INFCTE, "_INFCTENORM") <> Nil)
			If (XmlChildEx ( oCTe:_INFCTE:_INFCTENORM, "_INFCARGA") <> Nil)
				If (XmlChildEx ( oCTe:_INFCTE:_INFCTENORM:_INFCARGA, "_INFQ") <> Nil)
					aVolumes := oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ
					if valtype(oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ) == 'O'
						If UPPER(AllTRIM(oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ:_CUNID:TEXT)) == cDescVolume
							nVolume := VAL(oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ:_QCARGA:TEXT)
						EndIf
					Else
						For i := 1 to len(aVolumes)
							If UPPER(AllTRIM(oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ[i]:_CUNID:TEXT)) == cDescVolume
								nVolume := VAL(oCTe:_INFCTE:_INFCTENORM:_INFCARGA:_INFQ[i]:_QCARGA:TEXT)
								exit
							EndIf
						Next i
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf
Return nVolume