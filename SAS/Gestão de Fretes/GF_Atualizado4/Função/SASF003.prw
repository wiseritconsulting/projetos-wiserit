#Include 'Protheus.ch'

//------------------------------------------------------------------------
/*/{Protheus.doc} isNumeric

Valida se o conte�do da string s� tem n�meros

@type function
@author Sam Barros
@since 05/07/2016
@version 1.0
@param cNum, character, N�mero no formato string
@return bRet, .T. - Se n�mero ; .F. - Caso contr�rio
@example
U_isNumeric("123,456.78") 	//Retorna .T.
U_isNumeric("456.78") 		//Retorna .T.
U_isNumeric("123A") 		//Retorna .F.
U_isNumeric("ABC") 			//Retorna .F.
/*/
//------------------------------------------------------------------------
User function isNumeric(cNum)
	Local bRet := .T.
	Local nTam := Len(cNum)
	Local cDigit := "0123456789,."
	
	For i := 1 to nTam
		If !(SUBSTR(cNum, i, 1) $ cDigit)
			Return .F.
		EndIf
	Next i
Return bRet


User Function SomaArray(A1, A2)
	Local aRet := A1
	
	For i:=1 to len(A2)
		AADD(aRet, A2[i])
	Next i
Return aRet

user Function objSomaSeq(cSeq, nZeros)
	Local nAux := Val(cSeq) + 1
	Local cRet := StrZero(nAux,nZeros,0)
Return cRet

User Function InfoUser(cCodUsr, cTipo)
	Local aArray := {}
	Local cRet := ""
	PswOrder(1)
	If PswSeek( cCodUsr, .T. )
		aArray := PSWRET() // Retorna vetor com informa��es do usu�rio
	EndIf
	
	Do Case
		Case cTipo == "COD"
			cRet := aArray[1][1]
		Case cTipo == "NOME"
			cRet := aArray[1][4]
		Case cTipo == "LOGIN"
			cRet := aArray[1][2]
		Case cTipo == "EMAIL"
			cRet := aArray[1][14]
	EndCase
Return cRet