#Include 'Protheus.ch'
#include 'TopConn.CH'

User Function SASF006()
	Local xArea := getArea()
	Local cForm := ""
	
	public _p_cOcorr
	public _p_cDescOcorr
	public _p_cJust
	
	dbSelectArea("ZB2")
	dbGoTop()
	
	AutoGrLog( Replicate( "-", 77 ) )
	AutoGrLog( "Verifica��o de Ocorr�ncias: " )
	
	While !(ZB2->(EOF()))
		bRetOcorr		:= .F.
		_p_cOcorr 		:= ZB2->ZB2_OCORR
		_p_cDescOcorr 	:= POSICIONE("ZB3",1,xFilial("ZB3")+ZB2->ZB2_OCORR, "ZB3->ZB3_DESC")
		_p_cJust 		:= POSICIONE("ZB3",1,xFilial("ZB3")+ZB2->ZB2_OCORR, "ZB3->ZB3_JUST")
		cForm  			:= ZB2->ZB2_FORM
		
		AutoGrLog("-- Verifica��o da Ocorr�ncia: " + _p_cOcorr + ' - ' + _p_cDescOcorr)
		&(Formula(cForm))
		
		ZB2->(dbSkip())
	EndDo
	
	ZB2->(dbCloseArea())
	
	RestArea(xArea)
Return

User Function confere()
	Local xArea := getArea()
	Local yArea := ""
	Local wArea := ""
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()
	Local _cStatus := 'AA'
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		yArea := getArea()
		U_mkAlcNoOc((_cAlias)->ZB8_NRIMP)
		RestArea(yArea)
		
		wArea := getArea()
		_cStatus := getStatus((_cAlias)->ZB8_NRIMP)
		RestArea(wArea)
		
		ConfStatus((_cAlias)->ZB8_NRIMP,_cStatus)
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
	
	changeIMporAA()
	
	RestArea(xArea)
Return

Static Function ConfStatus(cSeqImp,_cStatus)
	Local xArea := getArea()
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	dbSeek(xFilial("ZB8")+cSeqImp)

	RecLock("ZB8", .F.)
	ZB8->ZB8_CONF 	:= 'S'
	ZB8->ZB8_STATUS	:= _cStatus
	MsUnLock()
	
	ZB8->(dbCloseArea())
	
	RestArea(xArea)
Return

User Function createZBC(cSeq, cFunc, cJust, cCte)
	Local yArea
	default cCte := ""
	
	yArea := getArea()
	
	RecLock("ZBC", .T.)
	ZBC->ZBC_NRIMP  := cSeq
	ZBC->ZBC_OCORR  := _p_cOcorr
	ZBC->ZBC_DESOCO := AllTrim(_p_cDescOcorr) + cCte
	ZBC->ZBC_LIBBLO := IIF(cJust == 'S', 'B', 'R')
	ZBC->ZBC_ROTINA := cFunc
	ZBC->ZBC_ALCADA := 'N'
	MsUnLock()
	
	RestArea(yArea)
Return

User Function getCTE()
	Local cSql := ""
	
	cSql += " SELECT * "
	cSql += " FROM " + RetSqlName("ZB8") + " ZB8 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB8.ZB8_FILIAL = '"+xFilial("ZB8")+"' "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	cSql += " AND ZB8.ZB8_CONF <> 'S' "
	cSql += " AND ZB8.ZB8_STATUS <> 'BL' "
	cSql += "  "
	cSql += "  "
Return cSql

User Function getCTEbyNFE()
	Local cSql := ""
	
	cSql += " SELECT * "
	cSql += " FROM " + RetSqlName("ZB8") + " ZB8, "
	cSql += "  " + RetSqlName("ZB9") + " ZB9 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB8.ZB8_FILIAL = '"+xFilial("ZB8")+"' "
	cSql += " AND ZB9.ZB9_FILIAL = '"+xFilial("ZB9")+"' "
	cSql += " AND ZB8.ZB8_FILIAL = ZB9.ZB9_FILIAL "
	cSql += " AND ZB8.ZB8_NRIMP = ZB9.ZB9_NRIMP "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB8.ZB8_CONF <> 'S' "
	cSql += " AND ZB8.ZB8_TPDF = '0' "
	cSql += " AND ZB8.ZB8_STATUS <> 'BL' "
	cSql += "  "
Return cSql

User Function getCTERedP() // Query com Redespacho
	Local cSql := ""
 
	cSql += " SELECT * "
	cSql += " FROM " + RetSqlName("ZB8") + " ZB8, "
	cSql += "  " + RetSqlName("ZB9") + " ZB9 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB8.ZB8_FILIAL = '"+xFilial("ZB8")+"' "
	cSql += " AND ZB9.ZB9_FILIAL = '"+xFilial("ZB9")+"' "
	cSql += " AND ZB8.ZB8_FILIAL = ZB9.ZB9_FILIAL "
	cSql += " AND ZB8.ZB8_NRIMP = ZB9.ZB9_NRIMP "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB8.ZB8_CONF <> 'S' "
	cSql += "  "
	cSql += "  "
Return cSql

User Function getNFE(cChvNFE, cSeqImp, cTpDf)
	Local cSql := ""
	Local _cAlias := GetNextAlias()
	
	//0=Normal            
	//If cTpDf $ '4;5;6'
	//	Return .T.
	//EndIf
	
	//1=CTE Complementar;
	//2=CTE Complementar Valor;
	//3=CTE Complementar Imposto;
	//4=Reentrega;
	//5=Devolucao;
	//6=Redespacho
	
	//Se for do tipo complementar, n�o precisa validar j� que n�o
	//existem notas fiscais vinculadas a estes CTEs
	If cTpDf $ '1;2;3'
		Return .T.
	EndIf
	
	cSql += " SELECT COUNT(*) QTD_NFE "
	cSql += " FROM "
	cSql += RetSqlName("ZB9") + " ZB9, "
	cSql += RetSqlName("ZB8") + " ZB8 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	
	cSql += " AND ZB8.ZB8_NRIMP = ZB9.ZB9_NRIMP "
	
	cSql += " AND ZB8.ZB8_TPDF = '" + cTpDf + "' "
	
	cSql += " AND ZB9.ZB9_FILIAL = '"+xFilial("ZB9")+"' "
	cSql += " AND ZB8.ZB8_FILIAL = '"+xFilial("ZB8")+"' "
	
	cSql += " AND ZB9.ZB9_DANFE = '"+cChvNFE+"' "
	cSql += " AND ZB9.ZB9_NRIMP < '"+cSeqImp+"' "
	
	cSql += " AND ZB8.ZB8_STATUS <> 'BL' "
	
	cSql += " AND ZB8.ZB8_SA <> 'S' "
	
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If (_cAlias)->QTD_NFE >= 1
			Return .T.
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return .F.

User Function getDadosNFE(cChvNFE)
	Local cSql := ""
	Local _cAlias := GetNextAlias()
	
	cSql += " SELECT COUNT(*) QTD_NFE "
	cSql += " FROM " + RetSqlName("ZB9") + " ZB9 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.ZB9_FILIAL = '"+xFilial("ZB9")+"' "
	cSql += " AND ZB9.ZB9_DANFE = '"+cChvNFE+"' "
	cSql += "  "
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If (_cAlias)->QTD_NFE > 1
			Return .T.
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return .F.

Static Function temOcorr(cSeqImp, cSqlStatus)
	Local cSql := ""
	Local bRet := .F.
	Local _cAlias := GetNextAlias()
	
	cSql += " SELECT COUNT(*) AS QNT "
	cSql += " FROM " + RetSqlName("ZBC") + " ZBC "
	cSql += " WHERE D_E_L_E_T_ = '' "
	cSql += " AND ZBC.ZBC_NRIMP = '" + cSeqImp + "' "
	cSql += cSqlStatus
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	If (_cAlias)->QNT > 0
		bRet := .T.
	EndIf
	
	(_cAlias)->(dbCLoseArea())
Return bRet

Static Function temAlcada(cSeqImp, cSqlStatus)
	Local cSql := ""
	Local bRet := .F.
	Local _cAlias := GetNextAlias()
	
	cSql += " SELECT COUNT(*) AS QNT "
	cSql += " FROM " + RetSqlName("ZBF") + " ZBF "
	cSql += " WHERE D_E_L_E_T_ = '' "
	cSql += " AND ZBF.ZBF_NRIMP = '" + cSeqImp + "' "
	cSql += cSqlStatus
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	If (_cAlias)->QNT > 0
		bRet := .T.
	EndIf
	
	(_cAlias)->(dbCLoseArea())
Return bRet

Static Function getStatus(cSeqImp)
	//Local cRet := 'AA'
	Local cRet := 'AG'
	
	if temAlcada(cSeqImp, " AND ZBF.ZBF_STATUS = 'R' ")
		Return 'BL'
	EndIf
	
	if temAlcada(cSeqImp, " AND ZBF.ZBF_STATUS IN ('L', 'B') AND ZBF.ZBF_STATUS NOT IN ('R') ")
		Return 'AG'
	EndIf

	if temOcorr(cSeqImp, " AND ZBC.ZBC_LIBBLO = 'R' ")
		Return 'BL'
	EndIf
	
	if temOcorr(cSeqImp, " AND ZBC.ZBC_LIBBLO IN ('L', 'P', 'B') AND ZBC.ZBC_LIBBLO NOT IN ('R') ")
		Return 'PE'
	EndIf
	
Return cRet

User Function mkAlcNoOc(cSeqImp)
	Local xArea := getArea()
	
	Local cUF := ""
	
	Local cGrpFF := ""
	Local cGrpRK := ""
	
	Local _aAprovFF := {}
	Local _aAprovRK := {}
	Local _aAprov := {}
	
	If !temOcorr(cSeqImp, "")
		cDest := POSICIONE("ZB8",1,xFilial("ZB8")+cSeqImp, "ZB8->ZB8_CDDEST")
		
		cUF := POSICIONE("ZB8",1,xFilial("ZB8")+cSeqImp, "ZB8->ZB8_UFFIM")//POSICIONE("SA1",1,xFilial("SA1")+cDest, "SA1->A1_EST")
		cGrpFF := POSICIONE("ZB1", 1, xFilial("ZB1") + cUF + '1',"ZB1->ZB1_GRUPO")
		cGrpRK := POSICIONE("ZB1", 1, xFilial("ZB1") + cUF + '2',"ZB1->ZB1_GRUPO")
		
		dbSelectArea("ZB8")
		dbGoTop()
		dbSetOrder(1)
		dbSeek(xFilial("ZB8")+cSeqImp)
		if ZB8->ZB8_VLDF > 0
			nValFrete := ZB8->ZB8_VLDF
		Else
			nValFrete := ZB8->ZB8_VLCARG
		EndIf
		
		if ZB8->ZB8_PESOR > 0
			nRealKilo := nValFrete / ZB8->ZB8_PESOR
		Else
			nRealKilo := 0.00
		EndIf
		ZB8->(dbCloseArea())
		
		_aAprovFF := u_getAprov(cGrpFF, nValFrete, '1')
		_aAprovRK := u_getAprov(cGrpRK, nRealKilo, '2')
		
		_aAprov :=  u_SomaArray(_aAprovFF,_aAprovRK)
		
		u_makeAprov(cSeqImp, _aAprov, .F.)
	EndIf
	
	RestArea(xArea)
Return

user function existNFE(cFildoc, cSeqImp)
	Local bRet := .T.
	Local aDanfe := getChvNFEdoCTE(cSeqImp)
	Local nTam := len(aDanfe)
	Local i := 0
	
	Local cSql := ""
	Local _cAlias := GetNextAlias()
	
	Local lNoAchou := .F.
	Local bDevol := .T.
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	dbSeek(xFilial("ZB8")+cSeqImp)
	if ZB8->ZB8_TPCTE <> '0'
		Return .T.
	EndIf
	ZB8->(dbCloseArea())
	
	For i := 1 to len(aDanfe)
		If bDevol := getDevol(cSeqImp)
			cSql := ""
			cSql += " SELECT COUNT(*) QTD_NFE "
			cSql += " FROM " + RetSqlName("SF1") + " SF1 "
			cSql += " WHERE 1=1 "
			cSql += " AND SF1.D_E_L_E_T_ = '' "
			cSql += " AND SF1.F1_CHVNFE = '" + AllTrim(aDanfe[i]) + "' "
			cSql += "  "
			
			DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
			
			dbSelectArea(_cAlias)
			(_cAlias)->( dbGoTop() )
			IF !(_cAlias)->( Eof() )
				IF (_cAlias)->QTD_NFE > 0
					bRet := .T.
				Else
					lNoAchou := .T.
				ENDIF
			Endif
			(_cAlias)->( DBCloseArea() )	
			
			IF lNoAchou
				bRet := .F.
				lNoAchou := .F.
				
				_cAlias := GetNextAlias()
				
				cSql := ""
				cSql += " SELECT COUNT(*) QTD_NFE "
				cSql += " FROM " + RetSqlName("SF2") + " SF2 "
				cSql += " WHERE 1=1 "
				cSql += " AND SF2.D_E_L_E_T_ = '' "
				cSql += " AND SF2.F2_CHVNFE = '"+AllTrim(aDanfe[i])+"' "
				cSql += "  "
				cSql += "  "
				
				DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
				
				dbSelectArea(_cAlias)
				(_cAlias)->( dbGoTop() )
				IF !(_cAlias)->( Eof() )
					IF (_cAlias)->QTD_NFE > 0
						bRet := .T.
					Else
						lNoAchou := .T.
					ENDIF
				Endif
				(_cAlias)->( DBCloseArea() )
			Endif		
		Else
			cSql := ""
			cSql += " SELECT COUNT(*) QTD_NFE "
			cSql += " FROM " + RetSqlName("SF2") + " SF2 "
			cSql += " WHERE 1=1 "
			cSql += " AND SF2.D_E_L_E_T_ = '' "
			cSql += " AND SF2.F2_CHVNFE = '"+AllTrim(aDanfe[i])+"' "
			cSql += "  "
			cSql += "  "
			
			DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
			
			dbSelectArea(_cAlias)
			(_cAlias)->( dbGoTop() )
			IF !(_cAlias)->( Eof() )
				IF (_cAlias)->QTD_NFE > 0
					bRet := .T.
				Else
					lNoAchou := .T.
				ENDIF
			Endif
			(_cAlias)->( DBCloseArea() )
			
			IF lNoAchou
				bRet := .F.
				lNoAchou := .F.
				
				_cAlias := GetNextAlias()
				
				cSql := ""
				cSql += " SELECT COUNT(*) QTD_NFE "
				cSql += " FROM " + RetSqlName("SF1") + " SF1 "
				cSql += " WHERE 1=1 "
				cSql += " AND SF1.D_E_L_E_T_ = '' "
				cSql += " AND SF1.F1_CHVNFE = '"+AllTrim(aDanfe[i])+"' "
				cSql += "  "
				cSql += "  "
				
				DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
				
				dbSelectArea(_cAlias)
				(_cAlias)->( dbGoTop() )
				IF !(_cAlias)->( Eof() )
					IF (_cAlias)->QTD_NFE > 0
						bRet := .T.
					Else
						lNoAchou := .T.
					ENDIF
				Endif
				(_cAlias)->( DBCloseArea() )
			Endif
		EndIf
	Next i
		
	IF lNoAchou
		bRet := .F.
	Endif
Return bRet

Static Function getFilMed(cChvNFE, cFildoc)
	Local cSql := ""
	Local cChave := ""
	
	Local cFilTom := ""
	Local cFilRet := ""
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cChvNFE+"' "
	cSql += "  "
	cSql += "  "
	cSql += "  "
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	cFilTom := POSICIONE("ZZ2",8,cChave, "ZZ2->ZZ2_FILIAL")
	If Empty(AllTrim(cFilTom))
		cFilTom := POSICIONE("ZZ2",10,cChave, "ZZ2->ZZ2_FILIAL")
	EndIf
	
	If !(Empty(AllTrim(cFilTom)))
		cFilRet := cFilTom
	Else
		cFilRet := cFildoc
	EndIf
Return cFilRet

Static Function getChvNFEdoCTE(cSeqImp)
	Local aRet := {}
	Local xArea := getArea()
	
	dbSelectArea("ZB9")
	dbSetOrder(1)
	dbSeek(xFilial("ZB9")+cSeqImp)
	While !(ZB9->(EOF())) .AND. ZB9->ZB9_NRIMP == cSeqImp
		AADD(aRet, ZB9->ZB9_DANFE)
		ZB9->(dbSkip())
	EndDo
	ZB9->(dbCloseArea())
	
	RestArea(xArea)
Return aRet

User function ValDifCot(cSeqImp, cFilDoc, nTotalFrete)
	Local bRet := .T.
	Local cSql := ""
	Local bSair := .F.
	Local nValEstFrete := 0
	Local bFaltaEst := .T.
	Local cQrySF2006	:= ""
	Local cChaveZB4 := ""
	
	Local aMedicao := {}
	Local bMedicao := .F.
	
	Local bVenAvul := .F.
	Local aAvulsa := {}
	
	Local aDevolucao := {}
	
	dbSelectArea("ZB9")
	ZB9->(dbSetOrder(1))
	if ZB9->(dbSeek(xFilial("ZB9")+cSeqImp))
		While !(ZB9->(EOF())) .AND. ZB9->ZB9_NRIMP == cSeqImp
			//--------------------------query------------------------------------
			cQrySF2006	:= "  SELECT F2_FILIAL as Filial, R_E_C_N_O_ as SFXRecno "
			cQrySF2006	+= "  FROM "+RetSqlName("SF2")+" SF2 " 
			cQrySF2006	+= "  WHERE SF2.D_E_L_E_T_ = '' "
			cQrySF2006	+= "  AND SF2.F2_CHVNFE = '"+AllTrim(ZB9->ZB9_DANFE)+"' "
			cQrySF2006	+= "  UNION "
			cQrySF2006	+= "  SELECT F1_FILIAL as Filial, R_E_C_N_O_ as SFXRecno "
			cQrySF2006	+= "  FROM "+RetSqlName("SF1")+" SF1 " 
			cQrySF2006	+= "  WHERE SF1.D_E_L_E_T_ = '' "
			cQrySF2006	+= "  AND SF1.F1_CHVNFE = '"+AllTrim(ZB9->ZB9_DANFE)+"' "
			
			IF SELECT("QSF2006") > 0
				QSF2006->(dbCloseArea())
			Endif
								
			TCQUERY cQrySF2006 NEW ALIAS QSF2006
			//-------------------------------------------------------------------
			dbSelectArea("QSF2006")
			QSF2006->(DBGOTOP())
				
			If !QSF2006->(EOF())
				dbSelectArea("SF2")
				SF2->(dbSetOrder(10))
				if SF2->(dbSeek(QSF2006->FILIAL+AllTrim(ZB9->ZB9_DANFE)))
					//Se for Medi��o Triangular/Direta
					dbSelectArea("ZZ2")
					ZZ2->(dbSetOrder(8))
					ZZ2->(dbGoTop())
					If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
						cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
						AADD(aMedicao, cChaveZB4)
						bMedicao := .T.
					Else
						ZZ2->(dbSetOrder(9))
						ZZ2->(dbGoTop())
						If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
							cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
							AADD(aMedicao, cChaveZB4)
							bMedicao := .T.
						Else
							ZZ2->(dbSetOrder(10))
							ZZ2->(dbGoTop())
							If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
								cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
								AADD(aMedicao, cChaveZB4)
								bMedicao := .T.
							EndIf
						EndIf
					EndIf
					
					ZZ2->(dbCloseArea())
					
					//Se for Venda Avulsa
					bVenAvul := .F.
					if !bMedicao
						dbSelectArea("SD2")
						SD2->(dbSetOrder(3))
						SD2->(dbGoTop())
						If SD2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA)))
							dbSelectArea("SC5")
							SC5->(dbSetOrder(1))
							SC5->(dbGoTop())
							If SC5->(dbSeek(SD2->(D2_FILIAL+D2_PEDIDO)))
								cChaveZB4 := SC5->(C5_FILIAL+C5_NUM)
								AADD(aAvulsa, cChaveZB4)
								bVenAvul := .T.
							EndIf 
							SC5->(dbCloseArea())
						EndIf
						SD2->(dbCloseArea())
					EndIf
					
					dbSelectArea("ZB4")
					ZB4->(dbSetOrder(2))
					if ZB4->(dbSeek(cChaveZB4))
						bFaltaEst := .F.
					Else
						bFaltaEst := .T.
						bSair := .T.
						exit
					EndIf
					
					ZB4->(dbCloseArea())
				Else
					dbSelectArea("SF1")
					SF1->(dbSetOrder(8))
					if SF1->(dbSeek(QSF2006->FILIAL+AllTrim(ZB9->ZB9_DANFE)))
						//Se For Devolu��o
						dbSelectArea("ZZH")
						ZZH->(dbSetOrder(4))
						ZZH->(dbGoTop())
						If ZZH->(dbSeek(SF1->(F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE)))
							cChaveZB4 := ZZH->(ZZH_FILIAL + ZZH_NUMERO + ZZH_MEDICA + ZZH_CLIENT + ZZH_LOJA)
							AADD(aDevolucao, cChaveZB4)
							bMedicao := .T.
						Else
							ZZH->(dbSetOrder(5))
							ZZH->(dbGoTop())
							If ZZH->(dbSeek(SF1->(F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE)))
								cChaveZB4 := ZZH->(ZZH_FILIAL + ZZH_NUMERO + ZZH_MEDICA + ZZH_CLIENT + ZZH_LOJA)
								AADD(aDevolucao, cChaveZB4)
								bMedicao := .T.
							Else
								ZZH->(dbSetOrder(6))
								ZZH->(dbGoTop())
								If ZZH->(dbSeek(SF1->(F1_FILIAL+F1_DOC+F1_SERIE+F1_FORNECE)))
									cChaveZB4 := ZZH->(ZZH_FILIAL + ZZH_NUMERO + ZZH_MEDICA + ZZH_CLIENT + ZZH_LOJA)
									AADD(aDevolucao, cChaveZB4)
								EndIf
							EndIf
						EndIf
						dbSelectArea("ZB4")
						ZB4->(dbSetOrder(2))
						if ZB4->(dbSeek(cChaveZB4))
							bFaltaEst := .F.
						Else
							bFaltaEst := .T.
							bSair := .T.
							exit
						EndIf
						
						ZB4->(dbCloseArea())
					EndIf
					SF1->(dbCLoseArea())
				EndIf
			EndIF
			SF2->(dbCLoseArea())
			ZB9->(dbSkip())
			if bSair
				exit
			EndIf
			QSF2006->(dbCloseArea())
		EndDO
	EndIf
	ZB9->(dbCLoseArea())
		
	If !bFaltaEst
		if bMedicao
			nValEstFrete := calcMedEst(aMedicao)
		Else
			If bVenAvul
				nValEstFrete := calcMedEst(aAvulsa)
			Else
				nValEstFrete := calcMedEst(aDevolucao)
			EndIf
		EndIf
		if Alltrim(GetMV("SA_TPVAL")) == 'P'
			If !( nTotalFrete < nValEstFrete * (1 + GetMV("SA_VALMAR")/100) .AND.  ;
					nTotalFrete > nValEstFrete * (1 - GetMV("SA_VALMAR")/100)  )
				bRet := .F.
			EndIf
		Else
			If !( nTotalFrete < nValEstFrete + GetMV("SA_VALMAR") .AND. ;
					nTotalFrete > nValEstFrete - GetMV("SA_VALMAR")  )
				bRet := .F.
			EndIf
		EndIf
	EndIf
Return bRet

static function calcMedEst(aMedicao)
	Local nRet := 0
	Local aMed := {}
	Local cBkp := ""
	Local i
	
	aMedicao := aSort(aMedicao)
	
	For i := 1 to len(aMedicao)
		if cBkp <> aMedicao[i]
			cBkp := aMedicao[i]
			AADD(aMed, cBkp)
		EndIf
	Next i
	
	For i := 1 to len(aMed)
		dbSelectArea("ZB4")
		ZB4->(dbSetOrder(2))
		if ZB4->(dbSeek(aMed[i]))
			nRet += ZB4->ZB4_VALOR
		EndIf
		ZB4->(dbCloseArea())
	Next i
Return nRet

User Function temNFE(cSeqImp, cTpDf)
	Local bRet := .T.
	
	if cTpDf $ '1;2;3'
		Return bRet
	EndIf
	
	dbSelectArea("ZB9")
	dbSetOrder(1)
	If !(dbSeek(xFilial("ZB9")+cSeqImp))
		bRet := .F.
	EndIf
	ZB9->(dbCloseArea())
/*
if cTpDf == "2" 
	// Ct-e Complementar 
	// Francisco Valdeni em 09/11/2016 as 10:41hr
	bRet := .T.
ENDIF
*/
Return bRet

User Function MedTriang(cDanfe, cSeqImp)
	Local bRet := .T.
	Local cSql := ""
	Local cChave := ""
	
	Local cChaveNF3 := ""
	
	Local cFilNF	:= "" //ZZ2_PV0XFL
	Local cPvNF 	:= "" //ZZ2_PV0XNF
	Local cSerNF 	:= "" //ZZ2_SER0X
	Local cCliNF 	:= "" //ZZ2_CLNF0X

	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	dbSelectArea("ZZ2")
	ZZ2->(dbSetOrder(8))
	ZZ2->(dbGoTop())
	If ZZ2->(dbSeek(cChave))
		If ZZ2->ZZ2_TIPCTR = '2'
			cFilNF	:= ZZ2->ZZ2_PV03FL	//ZZ2_PV0XFL
			cPvNF 	:= ZZ2->ZZ2_PV03NF	//ZZ2_PV0XNF
			cSerNF 	:= ZZ2->ZZ2_SER03	//ZZ2_SER0X
			cCliNF 	:= ZZ2->ZZ2_CLNF03	//ZZ2_CLNF0X
			
			cChaveNF3 := chvNfeMed(cFilNF, cPvNF, cSerNF, cCliNF, cSeqImp)
			
			If Empty(AllTrim(cChaveNF3))
				Return .F.
			EndIf
		EndIf
	/*
	Else 
		ZZ2->(dbSetOrder(10))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChave))
			If ZZ2->ZZ2_TIPCTR = '2'
				cFilNF	:= ZZ2->ZZ2_PV01FL	//ZZ2_PV0XFL
				cPvNF 	:= ZZ2->ZZ2_PV01NF	//ZZ2_PV0XNF
				cSerNF 	:= ZZ2->ZZ2_SER01	//ZZ2_SER0X
				cCliNF 	:= ZZ2->ZZ2_CLNF01	//ZZ2_CLNF0X
				
				cChave := chvNfeMed(cFilNF, cPvNF, cSerNF, cCliNF, cSeqImp)
				If Empty(AllTrim(cChave))
					Return .F.
				EndIf
			EndIf
		EndIf
		*/
	EndIf
	
	ZZ2->(dbCloseArea())
Return bRet

Static Function SM0CNPJbyCOD(cCod)
	Local aArea := GetArea()
	Local aAreaM0 := SM0->(GetArea())
	Local cFilRet := ""
	Local cCGC := POSICIONE("SA2",1,xFilial("SA2")+cCod, "SA2->A2_CGC")
     
    //Percorrendo o grupo de empresas
	SM0->(DbGoTop())
	While !SM0->(EoF())
        //Se o CNPJ for encontrado, atualiza a filial e finaliza
		If cCGC == SM0->M0_CGC
			cFilRet := SM0->M0_CODFIL
			Exit
		EndIf
         
		SM0->(DbSkip())
	EndDo

	RestArea(aAreaM0)
	RestArea(aArea)
Return cFilRet

User Function MedTomInc(cDanfe, cTomador)
	Local bRet := .T.
	Local cSql := ""
	Local cChave := ""
	
	Local cFilTom :=  SM0CNPJbyCOD(cTomador)
	
	Local cFilNF := ""
	
	Local bMedicao := .F.
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '" + cDanfe + "' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		cFilNF := QRY->F2_FILIAL
		
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	//Medi��o
	dbSelectArea("ZZ2")
	ZZ2->(dbSetOrder(8))
	ZZ2->(dbGoTop())
	If ZZ2->(dbSeek(cChave))
		If ZZ2->ZZ2_TIPCTR = '2' //Medi��o Triangular
			bMedicao := .T.
			if AllTrim(ZZ2->ZZ2_PV01FL) <> AllTrim(cFilTom)
				bRet := .F.
			EndIf
		EndIf
	Else
		ZZ2->(dbSetOrder(10))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChave))
			If ZZ2->ZZ2_TIPCTR = '2'
				bMedicao := .T.
				if AllTrim(ZZ2->ZZ2_PV01FL) <> AllTrim(cFilTom)
					bRet := .F.
				EndIf
			EndIf
		EndIf
	EndIf
	
	//Venda Avulsa
	If !bMedicao
		if AllTrim(cFilNF) <> AllTrim(cFilTom)
			bRet := .F.
		EndIf
	EndIf
	
	ZZ2->(dbCloseArea())
Return bRet

User Function MedRemInc(cDanfe, cRemetente)
	Local bRet := .T.
	Local cSql := ""
	Local cChave := ""
	
	Local cFilRem :=  SM0CNPJbyCOD(cRemetente)
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	dbSelectArea("ZZ2")
	ZZ2->(dbSetOrder(8))
	ZZ2->(dbGoTop())
	If ZZ2->(dbSeek(cChave))
		If ZZ2->ZZ2_TIPCTR = '2'
			if AllTrim(ZZ2->ZZ2_PV03FL) <> AllTrim(cFilRem)
				bRet := .F.
			EndIf
		EndIf
	Else
		ZZ2->(dbSetOrder(10))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChave))
			If ZZ2->ZZ2_TIPCTR = '2'
				if AllTrim(ZZ2->ZZ2_PV03FL) <> AllTrim(cFilRem)
					bRet := .F.
				EndIf
			EndIf
		EndIf
	EndIf
	
	ZZ2->(dbCloseArea())
Return bRet

Static function changeIMporAA()
	dbSelectArea("ZB8")
	dbSetOrder(1)
	dbGoTop()
	While !(ZB8->(EOF()))
		If ZB8->(ZB8_STATUS == 'IM')
			ZB8->ZB8_STATUS := 'AA'
		EndIf
		ZB8->(dbSkip())
	EndDo
	ZB8->(dbCloseArea())
Return

User Function MedDireta(cDanfe, cTomador, cRemetente)
	Local bRet := .T.
	Local cSql := ""
	Local cChave := ""
	
	Local cFilTom :=  SM0CNPJbyCOD(cTomador)
	Local cFilRem :=  SM0CNPJbyCOD(cRemetente)
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	If Empty(AllTrim(cChave))
		Return bRet
	EndIf
	
	dbSelectArea("ZZ2")
	ZZ2->(dbSetOrder(8))
	ZZ2->(dbGoTop())
	If ZZ2->(dbSeek(cChave))
		If ZZ2->ZZ2_TIPCTR = '1' //Medi��o Direta
			if AllTrim(ZZ2->ZZ2_PV01FL) <> AllTrim(cFilTom) .OR. AllTrim(ZZ2->ZZ2_PV01FL) <> AllTrim(cFilRem)
				bRet := .F.
			EndIf
		EndIf
	Else
		ZZ2->(dbSetOrder(10))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChave))
			If ZZ2->ZZ2_TIPCTR = '1'
				if AllTrim(ZZ2->ZZ2_PV01FL) <> AllTrim(cFilTom) .OR. AllTrim(ZZ2->ZZ2_PV01FL) <> AllTrim(cFilRem)
					bRet := .F.
				EndIf
			EndIf
		EndIf
	EndIf
	
	ZZ2->(dbCloseArea())
Return bRet

User function temEstimativa(cDanfe)
	Local bRet := .F.
	Local cSql := ""
	Local cChaveZZ2 := ""
	Local cChaveZB4 := ""
	Local xArea := getArea()
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRYZZ2",.F.,.T.)
	
	While !(QRYZZ2->(EOF()))
		bRet := .T.
		cChaveZZ2 := QRYZZ2->(F2_FILIAL + F2_DOC + F2_SERIE + F2_CLIENTE + F2_LOJA)
		dbSelectArea("ZZ2")
		ZZ2->(dbSetOrder(8))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChaveZZ2))
			cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
		Else
			ZZ2->(dbSetOrder(9))
			ZZ2->(dbGoTop())
			If ZZ2->(dbSeek(cChaveZZ2))
				cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
			Else
				ZZ2->(dbSetOrder(10))
				ZZ2->(dbGoTop())
				If ZZ2->(dbSeek(cChaveZZ2))
					cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
				EndIf
			EndIf
		EndIf
		
		ZZ2->(dbCloseArea())
		QRYZZ2->(dbSkip())
	EndDo
	
	QRYZZ2->(dbCloseArea())
	
	If !bRet
		//Return bRet
		cSql := " SELECT * "
		cSql += " FROM "
		cSql += RetSqlName("SF1") + " SF1 "
		cSql += " WHERE 1=1 "
		cSql += " AND SF1.D_E_L_E_T_ = '' "
		cSql += " AND SF1.F1_CHVNFE = '"+cDanfe+"' "
		
		DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRYZZH",.F.,.T.)
		
		While !(QRYZZH->(EOF()))
			bRet := .T.
			cChaveZZH := QRYZZH->(F1_FILIAL + F1_DOC + F1_SERIE + F1_FORNECEE + F1_LOJA)
			dbSelectArea("ZZH")
			ZZH->(dbSetOrder(4))
			ZZH->(dbGoTop())
			If ZZH->(dbSeek(cChaveZZH))
				cChaveZB4 := ZZH->(ZZH_FILIAL + ZZH_NUMERO + ZZH_MEDICA + ZZH_CLIENT + ZZH_LOJA)
			Else
				ZZH->(dbSetOrder(5))
				ZZH->(dbGoTop())
				If ZZH->(dbSeek(cChaveZZH))
					cChaveZB4 := ZZH->(ZZH_FILIAL + ZZH_NUMERO + ZZH_MEDICA + ZZH_CLIENT + ZZH_LOJA)
				Else
					ZZH->(dbSetOrder(6))
					ZZH->(dbGoTop())
					If ZZH->(dbSeek(cChaveZZH))
						cChaveZB4 := ZZH->(ZZH_FILIAL + ZZH_NUMERO + ZZH_MEDICA + ZZH_CLIENT + ZZH_LOJA)
					EndIf
				EndIf
			EndIf
			
			ZZH->(dbCloseArea())
			QRYZZH->(dbSkip())
		EndDo
		
		QRYZZH->(dbCloseArea())
		
		If !bRet
			Return bRet
		EndIf
	EndIf
	
	dbSelectArea("ZB4")
	dbSetOrder(2)
	If !(dbSeek(cChaveZB4))
		Return .F.
	EndIF
	ZB4->(dbCLoseArea())
	
	RestArea(xArea)
Return bRet

User Function ComplSemNF(cSeqImp)
	Local cSql := ""
	Local aVet := {}
	Local bRet := .T.
	Local xArea := getArea()
	
	//bRet := !U_existNFE(cDanfe) .AND. cTpDF == '2'
	dbSelectArea("ZB8")
	dbSetOrder(1)
	If dbSeek(xFilial("ZB8") + cSeqImp)
		If ZB8->ZB8_TPDF <> '0'
			dbSelectArea("ZB9")
			dbSetOrder(1)
			if !(dbSeek(xFilial("ZB9") + cSeqImp))
				Return .F.
			EndIf
		EndIf
	EndIf
		
	RestArea(xArea)
Return bRet

User Function TriangErr(cDanfe, cSeqImp,_cTpDf)
	Local bRet := .F.
	Local cSql := ""
	Local cChave := ""
	
	Local cFilNF	:= "" //ZZ2_PV0XFL
	Local cPvNF 	:= "" //ZZ2_PV0XNF
	Local cSerNF 	:= "" //ZZ2_SER0X
	Local cCliNF 	:= "" //ZZ2_CLNF0X
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	dbSelectArea("ZZ2")
	ZZ2->(dbSetOrder(8))
	ZZ2->(dbGoTop())
	If ZZ2->(dbSeek(cChave))
		If ZZ2->ZZ2_TIPCTR = '2' //Medi��o Triangular
			cFilNF	:= ZZ2->ZZ2_PV03FL	//ZZ2_PV0XFL
			cPvNF 	:= ZZ2->ZZ2_PV03NF	//ZZ2_PV0XNF
			cSerNF 	:= ZZ2->ZZ2_SER03	//ZZ2_SER0X
			cCliNF 	:= ZZ2->ZZ2_CLNF03	//ZZ2_CLNF0X
			
			bRet := MedJaImp(cFilNF, cPvNF, cSerNF, cCliNF, cSeqImp,_cTpDf)
		EndIf
	Else
		ZZ2->(dbSetOrder(10))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChave))
			If ZZ2->ZZ2_TIPCTR = '2'
				cFilNF	:= ZZ2->ZZ2_PV01FL	//ZZ2_PV0XFL
				cPvNF 	:= ZZ2->ZZ2_PV01NF	//ZZ2_PV0XNF
				cSerNF 	:= ZZ2->ZZ2_SER01	//ZZ2_SER0X
				cCliNF 	:= ZZ2->ZZ2_CLNF01	//ZZ2_CLNF0X
				
				bRet := MedJaImp(cFilNF, cPvNF, cSerNF, cCliNF, cSeqImp,_cTpDf)
			EndIf
		EndIf
	EndIf
	
	ZZ2->(dbCloseArea())
Return bRet

Static Function MedJaImp(cFilNF, cPvNF, cSerNF, cCliNF, cSeqImp,_cTpDf)
	Local bRet := .F.
	Local cSql := ""
	
	Local xAlias := getNextAlias()
	
	cSql += " SELECT COUNT(*) QTD "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2, "
	cSql += RetSqlName("ZB9") + " ZB9, "
	cSql += RetSqlName("ZB8") + " ZB8 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.ZB9_DANFE  = SF2.F2_CHVNFE "
	cSql += " AND ZB9.ZB9_NRIMP = ZB8.ZB8_NRIMP "
	cSql += " AND ZB8_STATUS <> 'BL' "
	cSql += " AND ZB8_TPDF = '"+_cTpDf+"' "
	cSql += " AND ZB9.ZB9_NRIMP <> '" + cSeqImp + "' "
	cSql += " AND SF2.F2_FILIAL  = '" + cFilNF + "' "
	cSql += " AND SF2.F2_DOC   = '" + cPvNF + "' "
	cSql += " AND SF2.F2_SERIE   = '" + cSerNF + "' "
	cSql += " AND SF2.F2_CLIENTE = '" + cCliNF + "' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),xAlias,.F.,.T.)
	
	While !((xAlias)->(EOF()))
		If (xAlias)->QTD > 0
			Return .T.
		EndIf
		(xAlias)->(dbSkip())
	EndDo
	
	(xAlias)->(dbCloseArea())
Return bRet

Static Function chvNfeMed(cFilNF, cPvNF, cSerNF, cCliNF, cSeqImp)
	Local cChave := ""
	Local cSql := ""
	
	Local xAlias := getNextAlias()
	
	cSql += " SELECT F2_CHVNFE "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2, "
	cSql += RetSqlName("ZB9") + " ZB9 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.ZB9_DANFE  = SF2.F2_CHVNFE "
	cSql += " AND ZB9.ZB9_NRIMP  = '" + cSeqImp + "' "
	cSql += " AND SF2.F2_FILIAL  = '" + cFilNF + "' "
	cSql += " AND SF2.F2_DOC 	 = '" + cPvNF + "' "
	cSql += " AND SF2.F2_SERIE 	 = '" + cSerNF + "' "
	cSql += " AND SF2.F2_CLIENTE = '" + cCliNF + "' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),xAlias,.F.,.T.)
	
	While !((xAlias)->(EOF()))
		cChave := (xAlias)->F2_CHVNFE
		(xAlias)->(dbSkip())
	EndDo
	
	(xAlias)->(dbCloseArea())
Return cChave

Static Function getDevol(cSeqImp)
	Local xArea :=getArea()
	Local bRet := .F.
	
	dbSelectArea("ZB8")
	ZB8->(dbSetOrder(1))
	If ZB8->(dbSeek(xFilial("ZB8") + cSeqImp))
		bRet := ZB8->ZB8_DEVOL == 'S'
	EndIf
	ZB8->(dbCloseArea())
	RestArea(xArea)
Return bRet