#Include 'Protheus.ch'

User Function SASF012(cSeqImp)
	Local cStatus 	:= ""
	
	Local oBold
	Local oMainWnd
	
	Local nLin		:= 0
	
	Local oFolder := nil
	
	Local aRet := {}
	Local aParamBox := {}
	Local aTpAprov := {"Ocorr�ncia", "Al�ada", "Fatura"}
	
	Local aSttsAprov := {}
	
	Local xArea
	
	Private aRotina := {}
	
	Private aCols	:= {}
	Private AHEADER	:= {}
	
	xArea := getArea()
	
	aAdd(aParamBox,{2,"Qual Aprova��o?",1,aTpAprov,50,"",.F.})
	
	If ParamBox(aParamBox,"Par�metros",@aRet)
		If ValType(aRet[1]) == 'N' //Ocorr�ncia - Bug do Protheus
			U_EmailGF(ZB8->ZB8_NRIMP, 'O')
		ElseIf aRet[1] == "Al�ada"
			U_EmailGF(ZB8->ZB8_NRIMP, 'A')
		Else
			U_EmailGF(ZB8->ZB8_NRIMP, 'F', ZB8->ZB8_FATURA)
		EndIf
	EndIf
Return