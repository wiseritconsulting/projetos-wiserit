#Include 'Protheus.ch'

User Function SASF007(aCabec, aItens, cSeqImp, bShowMsg)
	Local aLinha := {}
	Local cDoc   := StrZero(Val(aCabec[2][2]),9,0)
	Local cSer 	 := AllTrim(aCabec[3][2])
	Local lOk    := .T.        
	Local xArea  := getArea()
	
	Local cFilBkp := ""
	
	Private lMsHelpAuto := .T.
	PRIVATE lMsErroAuto := .F.
	
	aCabec[2][2] := cDoc
	
	cFilBkp := cFilAnt
	cFilAnt := aCabec[1][2]
	
	MSExecAuto({|x,y| mata103(x,y)},aCabec,aItens)
	
	cFilAnt := cFilBkp
		
	If !lMsErroAuto			
		ConOut(OemToAnsi("Incluido com sucesso! ")+cDoc)
		dbSelectArea("ZB8")
		dbSetorder(1)
		If dbSeek(xFilial("ZB8")+cSeqImp)
			RecLock("ZB8", .F.)
				ZB8->ZB8_STATUS := 'DE'
				ZB8->ZB8_TITULO := cDoc
			MsUnLock()
		EndIf	
		if bShowMsg	
			MsgInfo("Documento " + cDoc + '/' + cSer + " gerado com Sucesso!")
		EndIf
	Else			
		ConOut(OemToAnsi("Erro na inclusao!"))		
		mostraerro()
	EndIf
	
	restArea(xArea)
Return lMsErroAuto