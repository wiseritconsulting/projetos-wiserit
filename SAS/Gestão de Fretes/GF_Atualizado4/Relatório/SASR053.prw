#include "rwmake.ch"
#include "protheus.ch"
#include "topconn.ch"

#define DMPAPER_A4 9

//---------------------------------------------------------
/*/{Protheus.doc} SASR053

Custo por Frete

Desenvolvido em TReport.
@author Samarony Barros
@since 05/10/2016
@version 1.0
/*/
//---------------------------------------------------------
User Function SASR053()
	local oReport
	Private cPerg := PadR('SASR053',10)
		
	//---------------------------------------------
	//------CONFIGURAÇÕES-DO RELATÓRIO-------------
	//---------------------------------------------
	Private _cTabela 	:= "ZBG"
	Private _cIniCampo 	:= SUBS(_cTabela, IIF(SUBS(_cTabela,1,1)=='S',2,1), 3)
	Private _cTitRel 	:= "Custo por Frete"
	//--------------------------------------------- 

	CriaSX1(cPerg)
	Pergunte(cPerg,.T.)
	
	oReport := reportDef()
	oReport:printDialog()
Return
//---------------------------------------------------------
/*/{Protheus.doc} reportDef
Definição do relatório
@author Samarony Barros
@since 05/10/2016
@version 1.0
@return oReport, Objeto com os parametros da classe 
/*/
//---------------------------------------------------------
static function reportDef()
	local oReport
	Local oSection01

	Local nMult := 1
	
	local cTitulo := '[SASR053] - Custo por Frete'
	
	Local cDescri 	:= "X3_DESCRIC"
	Local cOrdem 	:= "X3_ORDEM"
	Local cCampo 	:= "X3_CAMPO"

	
	oReport := TReport():New('SASR053', cTitulo,cPerg, {|oReport| PrintReport(oReport)},"Custo por Frete")
	oReport:SetPortrait()

	oSection01 := TRSection():New(oReport,_cTitRel,{"T01"})
	
	oSection01:SetTotalInLine(.F.)
	
	dbSelectArea("SX3")
	SX3->(dbSetorder(1))
	While !(SX3->(EOF()))
		If SX3->X3_ARQUIVO == _cTabela
			TRCell():New(oSection01, AllTrim(GetSX3Cache(SX3->X3_CAMPO, cCampo)), "T01", AllTrim(GetSX3Cache(SX3->X3_CAMPO, cDescri)),,TamSX3(SX3->X3_CAMPO)[1]+1,,)
		EndIf
		
		SX3->(dbSkip())
	EndDo
	SX3->(dbCloseArea())
return (oReport)

Static Function PrintReport(oReport)
	Local oSection01 := oReport:Section(1)
	
	Local aEstrutura := {}
	Local i := 0
	
	oSection01:Init()
	oSection01:SetHeaderSection(.T.)

	cSetSql := qryDefault()
	cSetSql := ChangeQuery(cSetSql) // Converte o texto num Sql
	
	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY_FRETE", .F., .T.)
	
	oReport:SkipLine(2)
	
	aEstrutura := QRY_FRETE->(dbStruct())    
	While QRY_FRETE->(!EOF())
		If oReport:Cancel()
			Exit
		EndIf
		
		nTam := len(aEstrutura)
		
		For i := 1 to nTam
			IF aEstrutura[i][1] == 'R_E_C_N_O_' .OR. aEstrutura[i][1] == 'D_E_L_E_T_'
				loop
			EndIf
		
			If aEstrutura[i][2] == 'C'
				oSection01:Cell(aEstrutura[i][1]):SetValue(&("QRY_FRETE->" + aEstrutura[i][1]))
				oSection01:Cell(aEstrutura[i][1]):SetAlign("LEFT")
			ElseIf aEstrutura[i][2] == 'N'
				oSection01:Cell(aEstrutura[i][1]):SetValue(Transform(&("QRY_FRETE->" + aEstrutura[i][1]), GetSX3Cache(aEstrutura[i][1], "X3_PICTURE")))
				oSection01:Cell(aEstrutura[i][1]):SetAlign("RIGHT")
			ElseIf aEstrutura[i][2] == 'D'
				oSection01:Cell(aEstrutura[i][1]):SetValue(DtoC(StoD(&("QRY_FRETE->" + aEstrutura[i][1]))))
				oSection01:Cell(aEstrutura[i][1]):SetAlign("RIGHT")
			Else
				oSection01:Cell(aEstrutura[i][1]):SetValue(&("QRY_FRETE->" + aEstrutura[i][1]))
				oSection01:Cell("FILIAL"):SetAlign("LEFT")
			EndIf
		Next i

		oSection01:PrintLine()
		
		QRY_FRETE->(DBSkip())
	ENDDO
	QRY_FRETE->(dbCloseArea())
	oSection01:Finish()
Return

static function CriaSX1(cPerg)
	PutSx1(cPerg, "01","Tomador De"           ,".",".","mv_ch01","C",08,0,1,"G","","SM0","","","mv_par01","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Tomador Ate"          ,".",".","mv_ch02","C",08,0,1,"G","","SM0","","","mv_par02","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "03","Data Emissao Cte De"  ,".",".","mv_ch03","D",08,0,1,"G","",""   ,"","","mv_par03","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "04","Data Emissao Cte Ate" ,".",".","mv_ch04","D",08,0,1,"G","",""   ,"","","mv_par04","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "05","Codigo Importacao De" ,".",".","mv_ch05","C",16,0,1,"G","","ZBGSEQ"   ,"","","mv_par05","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "06","Codigo Importacao Ate",".",".","mv_ch06","C",16,0,1,"G","","ZBGSEQ"   ,"","","mv_par06","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "07","Destinatario De"      ,".",".","mv_ch07","C",14,0,1,"G","","SA1","","","mv_par07","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "08","Destinatario Ate"     ,".",".","mv_ch08","C",14,0,1,"G","","SA1","","","mv_par08","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "09","Remetente De"         ,".",".","mv_ch09","C",14,0,1,"G","","SA2","","","mv_par09","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "10","Remetente Ate"        ,".",".","mv_ch10","C",14,0,1,"G","","SA2","","","mv_par10","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "11","Emissor De"           ,".",".","mv_ch11","C",14,0,1,"G","","SA2","","","mv_par11","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "12","Emissor Ate"          ,".",".","mv_ch12","C",14,0,1,"G","","SA2","","","mv_par12","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "13","Chave CTE De"         ,".",".","mv_ch13","C",44,0,1,"G","",""   ,"","","mv_par13","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "14","Chave CTE Ate"        ,".",".","mv_ch14","C",44,0,1,"G","",""   ,"","","mv_par14","" ,"","","","","","","","","","","","","","","")
Return


Static function qryDefault()
	Local cSql := ""

	cSql += " SELECT "
	cSql += " 	* "
	cSql += " FROM  "
	cSql += " 	" + RetSqlName(_cTabela) + " ZBG "
	cSql += " WHERE 1=1 "
	cSql += " AND ZBG.D_E_L_E_T_ = '' "
	//****************************************************************************************************
	cSql += " AND ZBG.ZBG_FILIAL >= '" + MV_PAR01 + "' "
	cSql += " AND ZBG.ZBG_FILIAL <= '" + MV_PAR02 + "' "
	
	cSql += " AND ZBG.ZBG_DTEMIS >= '" + DTOS(MV_PAR03) + "' "
	cSql += " AND ZBG.ZBG_DTEMIS <= '" + DTOS(MV_PAR04) + "' "
	
	//cSql += " AND ZBG.ZBG_NRIMP >= '" + MV_PAR05 + "' "
	//cSql += " AND ZBG.ZBG_NRIMP <= '" + MV_PAR06 + "' "
	
	cSql += " AND ZBG.ZBG_DESTIN >= '" + MV_PAR07 + "' "
	cSql += " AND ZBG.ZBG_DESTIN <= '" + MV_PAR08 + "' "
	
	cSql += " AND ZBG.ZBG_REMETE >= '" + MV_PAR09 + "' "
	cSql += " AND ZBG.ZBG_REMETE <= '" + MV_PAR10 + "' "
	
	cSql += " AND ZBG.ZBG_CDTRAN >= '" + MV_PAR11 + "' "
	cSql += " AND ZBG.ZBG_CDTRAN <= '" + MV_PAR12 + "' "
	
	cSql += " AND ZBG.ZBG_CHVNF >= '" + MV_PAR13 + "' "
	cSql += " AND ZBG.ZBG_CHVNF <= '" + MV_PAR14 + "' "
	//****************************************************************************************************
return cSql