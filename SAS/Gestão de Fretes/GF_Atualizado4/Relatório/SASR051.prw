#include "rwmake.ch"
#include "protheus.ch"
#include "topconn.ch"

#define DMPAPER_A4 9

//---------------------------------------------------------
/*/{Protheus.doc} SASR051

Custo por Frete

Desenvolvido em TReport.
@author Samarony Barros
@since 01/09/2016
@version 1.0
/*/
//---------------------------------------------------------
User Function SASR051()
	local oReport
	Private cPerg := PadR('SASR051',10)
		
	CriaSX1(cPerg)
	Pergunte(cPerg,.T.)
	
	oReport := reportDef()
	oReport:printDialog()
Return
//---------------------------------------------------------
/*/{Protheus.doc} reportDef
Defini��o do relat�rio
@author Samarony Barros
@since 01/09/2016
@version 1.0
@return oReport, Objeto com os parametros da classe 
/*/
//---------------------------------------------------------
static function reportDef()
	local oReport
	Local oSection01

	Local nMult := 1
	
	local cTitulo := '[SASR051] - Custo por Frete'
	
	oReport := TReport():New('SASR051', cTitulo,cPerg, {|oReport| PrintReport(oReport)},"Custo por Frete")
	oReport:SetPortrait()

	oSection01 := TRSection():New(oReport,"Totais",{"T01"})
	
	oSection01:SetTotalInLine(.F.)
	TRCell():New(oSection01, "FILIAL"			, "T01", 'FILIAL'			,,nMult*TamSX3("ZB8_FILIAL")[1]+1,,)
	TRCell():New(oSection01, "NUMSERNF"			, "T01", 'NUM/SER NF'		,,nMult*TamSX3("F2_DOC")[1]+TamSX3("F2_SERIE")[1]+1,,)
	TRCell():New(oSection01, "CHVNFE"			, "T01", 'CHAVE NFE'		,,nMult*TamSX3("ZB9_DANFE")[1]+1,,)
	TRCell():New(oSection01, "NUMSERCTE"		, "T01", 'NUM/SER CTE'		,,nMult*TamSX3("ZB8_NRDF")[1]+TamSX3("ZB8_SERDF")[1]+1,,)
	TRCell():New(oSection01, "CHVCTE"			, "T01", 'CHAVE CTE'		,,nMult*TamSX3("ZB8_CTE")[1]+1,,)
	TRCell():New(oSection01, "TPPED"			, "T01", 'TIPO PEDIDO'		,,nMult*TamSX3("ZZG_DESCRI")[1]+1,,)
	TRCell():New(oSection01, "DTEMISSAO"		, "T01", 'EMISSAO'			,,nMult*TamSX3("ZZ8_DTEMIS")[1]+1,,)
	
	TRCell():New(oSection01, "TRANSPORTADOR"	, "T01", 'TRANSPORTADOR'	,,nMult*TamSX3("A2_COD")[1]+TamSX3("A2_LOJA")[1]+1,,)
	TRCell():New(oSection01, "NMTRANSP"			, "T01", 'NMTRANSP'			,,nMult*TamSX3("A2_NOME")[1]+1,,)
	
	TRCell():New(oSection01, "TOMADOR"			, "T01", 'TOMADOR'			,,nMult*TamSX3("A2_COD")[1]+TamSX3("A2_LOJA")[1]+1,,)
	TRCell():New(oSection01, "NMTOM"			, "T01", 'NM TOM'			,,nMult*TamSX3("A2_NOME")[1]+1,,)
	TRCell():New(oSection01, "UFTOM"			, "T01", 'UF TOM'			,,nMult*TamSX3("A2_EST")[1]+1,,)
	TRCell():New(oSection01, "MUNTOM"			, "T01", 'MUN TOM'			,,nMult*TamSX3("A2_MUN")[1]+1,,)
	
	TRCell():New(oSection01, "REMETENTE"		, "T01", 'REMETENTE'		,,nMult*TamSX3("A2_COD")[1]+TamSX3("A2_LOJA")[1]+1,,)
	TRCell():New(oSection01, "NMREM"			, "T01", 'NM REM'			,,nMult*TamSX3("A2_NOME")[1]+1,,)
	TRCell():New(oSection01, "UFREM"			, "T01", 'UF REM'			,,nMult*TamSX3("A2_EST")[1]+1,,)
	TRCell():New(oSection01, "MUNREM"			, "T01", 'MUN REM'			,,nMult*TamSX3("A2_MUN")[1]+1,,)
	
	TRCell():New(oSection01, "DESTINATARIO"		, "T01", 'DESTINATARIO'		,,nMult*TamSX3("A2_COD")[1]+TamSX3("A2_LOJA")[1]+1,,)
	TRCell():New(oSection01, "NMDEST"			, "T01", 'NM DEST'			,,nMult*TamSX3("A2_NOME")[1]+1,,)
	TRCell():New(oSection01, "UFDEST"			, "T01", 'UF DEST'			,,nMult*TamSX3("A2_EST")[1]+1,,)
	TRCell():New(oSection01, "MUNDEST"			, "T01", 'MUN DEST'			,,nMult*TamSX3("A2_MUN")[1]+1,,)
	
	TRCell():New(oSection01, "VOLUME"			, "T01", 'VOLUME'			,,nMult*TamSX3("ZB8_VOLUM")[1]+1,,)
	TRCell():New(oSection01, "VALORCARGA"		, "T01", 'VALOR DA CARGA'	,,nMult*TamSX3("ZB8_VLCARG")[1]+1,,)
	TRCell():New(oSection01, "PESO"				, "T01", 'PESO'				,,nMult*TamSX3("ZB8_PREAL")[1]+1,,)
	TRCell():New(oSection01, "VALORFRETE"		, "T01", 'VALOR DO FRETE'	,,nMult*TamSX3("ZB8_VLDF")[1]+1,,)
	TRCell():New(oSection01, "FF"				, "T01", 'FRETE FATURA'		,,nMult*TamSX3("ZBF_FF")[1]+1,,)
	TRCell():New(oSection01, "RK"				, "T01", 'R$/KG'			,,nMult*TamSX3("ZBF_RK")[1]+1,,)
	
	TRCell():New(oSection01, "CHVEST"			, "T01", 'CHV ESTIMATIVA'	,,nMult*TamSX3("ZB4_ID")[1]+1,,)
	TRCell():New(oSection01, "VALEST"			, "T01", 'VAL ESTIMATIVA'	,,nMult*TamSX3("ZB4_VALOR")[1]+1,,)
  
return (oReport)

Static Function PrintReport(oReport)
	Local oSection01 := oReport:Section(1)
	
	oSection01:Init()
	oSection01:SetHeaderSection(.T.)

	cSetSql := qryDefault()
	cSetSql := ChangeQuery(cSetSql) // Converte o texto num Sql
	
	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY_FRETE", .F., .T.)
	
	oReport:SkipLine(2)
	    
	While QRY_FRETE->(!EOF())
		If oReport:Cancel()
			Exit
		EndIf
		
		//oReport:SkipLine()

		oSection01:Cell("FILIAL"):SetValue(QRY_FRETE->ZB8_FILDOC)
		oSection01:Cell("FILIAL"):SetAlign("LEFT")
		
		oSection01:Cell("NUMSERNF"):SetValue(AllTrim(QRY_FRETE->F2_DOC) + '/' + AllTrim(QRY_FRETE->F2_SERIE))
		oSection01:Cell("NUMSERNF"):SetAlign("LEFT")
		
		oSection01:Cell("CHVNFE"):SetValue(AllTrim(QRY_FRETE->ZB9_DANFE))
		oSection01:Cell("CHVNFE"):SetAlign("LEFT")
		
		oSection01:Cell("NUMSERCTE"):SetValue(AllTrim(QRY_FRETE->ZB8_NRDF) + '/' + AllTrim(QRY_FRETE->ZB8_SERDF))
		oSection01:Cell("NUMSERCTE"):SetAlign("LEFT")
		
		oSection01:Cell("CHVCTE"):SetValue(AllTrim(QRY_FRETE->ZB8_CTE))
		oSection01:Cell("CHVCTE"):SetAlign("LEFT")
		
		oSection01:Cell("TPPED"):SetValue(getTipoPedido(AllTrim(QRY_FRETE->ZB9_DANFE)))
		oSection01:Cell("TPPED"):SetAlign("LEFT")
		
		oSection01:Cell("EMISSAO"):SetValue(StoD(DtoC(QRY_FRETE->ZB8_EMISSAO)))
		oSection01:Cell("EMISSAO"):SetAlign("LEFT")
		
		oSection01:Cell("TRANSPORTADOR"):SetValue(QRY_FRETE->ZB8_EMISDF)
		oSection01:Cell("TRANSPORTADOR"):SetAlign("LEFT")
		
		oSection01:Cell("NMTRANSP"):SetValue(POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_EMISDF, "A2_NOME"))
		oSection01:Cell("NMTRANSP"):SetAlign("LEFT")
		
		oSection01:Cell("TOMADOR"):SetValue(QRY_FRETE->ZB8_TOMADO)
		oSection01:Cell("TOMADOR"):SetAlign("LEFT")
		
		oSection01:Cell("NMTOM"):SetValue(POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_TOMADO, "A2_NOME"))
		oSection01:Cell("NMTOM"):SetAlign("LEFT")
		
		oSection01:Cell("UFTOM"):SetValue(POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_TOMADO, "A2_EST"))
		oSection01:Cell("UFTOM"):SetAlign("LEFT")
		
		oSection01:Cell("MUNTOM"):SetValue(POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_TOMADO, "A2_MUN"))
		oSection01:Cell("MUNTOM"):SetAlign("LEFT")
		
		oSection01:Cell("REMETENTE"):SetValue(QRY_FRETE->ZB8_CDREM)
		oSection01:Cell("REMETENTE"):SetAlign("LEFT")
		
		oSection01:Cell("NMREM"):SetValue(POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_CDREM, "A2_NOME"))
		oSection01:Cell("NMREM"):SetAlign("LEFT")
		
		oSection01:Cell("UFREM"):SetValue(POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_CDREM, "A2_EST"))
		oSection01:Cell("UFREM"):SetAlign("LEFT")
		
		oSection01:Cell("MUNREM"):SetValue(POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_CDREM, "A2_MUN"))
		oSection01:Cell("MUNREM"):SetAlign("LEFT")
		
		oSection01:Cell("DESTINATARIO"):SetValue(QRY_FRETE->ZB8_CDDEST)
		oSection01:Cell("DESTINATARIO"):SetAlign("LEFT")
		
		oSection01:Cell("NMDEST"):SetValue(POSICIONE("SA1", 1, xFilial("SA1") + QRY_FRETE->ZB8_CDDEST, "A1_NOME"))
		oSection01:Cell("NMDEST"):SetAlign("LEFT")
		
		oSection01:Cell("UFDEST"):SetValue(POSICIONE("SA1", 1, xFilial("SA1") + QRY_FRETE->ZB8_CDDEST, "A1_EST"))
		oSection01:Cell("UFDEST"):SetAlign("LEFT")
		
		oSection01:Cell("MUNDEST"):SetValue(POSICIONE("SA1", 1, xFilial("SA1") + QRY_FRETE->ZB8_CDDEST, "A1_MUN"))
		oSection01:Cell("MUNDEST"):SetAlign("LEFT")
		
		oSection01:Cell("VOLUME"):SetValue(Transform(QRY_FRETE->ZB8_VOLUM, PesqPict("ZB8", "ZB8_VOLUM")))
		oSection01:Cell("VOLUME"):SetAlign("RIGHT")
		
		oSection01:Cell("VALORCARGA"):SetValue(Transform(QRY_FRETE->ZB8_VLCARG, PesqPict("ZB8", "ZB8_VLCARG")))
		oSection01:Cell("VALORCARGA"):SetAlign("RIGHT")
		
		oSection01:Cell("PESO"):SetValue(Transform(QRY_FRETE->ZB8_PREAL, PesqPict("ZB8", "ZB8_PREAL")))
		oSection01:Cell("PESO"):SetAlign("RIGHT")
		
		oSection01:Cell("VALORFRETE"):SetValue(Transform(QRY_FRETE->ZB8_VLDF, PesqPict("ZB8", "ZB8_VLDF")))
		oSection01:Cell("VALORFRETE"):SetAlign("RIGHT")
		
		oSection01:Cell("FF"):SetValue(Transform(QRY_FRETE->ZB8_VLDF / QRY_FRETE->ZB8_VLCARG, PesqPict("ZB8", "ZB8_VLDF")))
		oSection01:Cell("FF"):SetAlign("RIGHT")
		
		oSection01:Cell("RK"):SetValue(Transform(QRY_FRETE->ZB8_VLDF / QRY_FRETE->ZB8_PREAL, PesqPict("ZB8", "ZB8_VLDF")))
		oSection01:Cell("RK"):SetAlign("RIGHT")
		
		_aEstimativa := getEstimativa(QRY_FRETE->ZB9_DANFE)
		oSection01:Cell("CHVEST"):SetValue(_aEstimativa[1])
		oSection01:Cell("CHVEST"):SetAlign("LEFT")
		
		oSection01:Cell("VALEST"):SetValue(_aEstimativa[2])
		oSection01:Cell("VALEST"):SetAlign("RIGHT")
		
		oSection01:PrintLine()
		
		QRY_FRETE->(DBSkip())
	ENDDO
	QRY_FRETE->(dbCloseArea())
	oSection01:Finish()
Return

static function CriaSX1(cPerg)
	PutSx1(cPerg, "01","Tomador De"           ,".",".","mv_ch01","C",08,0,1,"G","","SM0","","","mv_par01","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Tomador Ate"          ,".",".","mv_ch02","C",08,0,1,"G","","SM0","","","mv_par02","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "03","Data Emissao Cte De"  ,".",".","mv_ch03","D",08,0,1,"G","",""   ,"","","mv_par03","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "04","Data Emissao Cte Ate" ,".",".","mv_ch04","D",08,0,1,"G","",""   ,"","","mv_par04","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "05","Codigo Importacao De" ,".",".","mv_ch05","C",16,0,1,"G","","ZB8SEQ"   ,"","","mv_par05","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "06","Codigo Importacao Ate",".",".","mv_ch06","C",16,0,1,"G","","ZB8SEQ"   ,"","","mv_par06","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "07","Destinatario De"      ,".",".","mv_ch07","C",14,0,1,"G","","SA1","","","mv_par07","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "08","Destinatario Ate"     ,".",".","mv_ch08","C",14,0,1,"G","","SA1","","","mv_par08","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "09","Remetente De"         ,".",".","mv_ch09","C",14,0,1,"G","","SA2","","","mv_par09","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "10","Remetente Ate"        ,".",".","mv_ch10","C",14,0,1,"G","","SA2","","","mv_par10","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "11","Emissor De"           ,".",".","mv_ch11","C",14,0,1,"G","","SA2","","","mv_par11","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "12","Emissor Ate"          ,".",".","mv_ch12","C",14,0,1,"G","","SA2","","","mv_par12","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "13","Chave CTE De"         ,".",".","mv_ch13","C",44,0,1,"G","",""   ,"","","mv_par13","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "14","Chave CTE Ate"        ,".",".","mv_ch14","C",44,0,1,"G","",""   ,"","","mv_par14","" ,"","","","","","","","","","","","","","","")
	
	//PutSx1(cPerg, "15","UF De"                ,".",".","mv_ch15","C",02,0,1,"G","","12"   ,"","","mv_par15","" ,"","","","","","","","","","","","","","","")
	//PutSx1(cPerg, "16","UF Ate"               ,".",".","mv_ch16","C",02,0,1,"G","","12"   ,"","","mv_par16","" ,"","","","","","","","","","","","","","","")
Return


Static function qryDefault()
	Local cSql := ""

	cSql += " SELECT "
	cSql += " 	ZB8_FILDOC, "
	cSql += " 	F2_DOC, "
	cSql += " 	F2_SERIE, "
	cSql += " 	ZB8_NRDF, "
	cSql += " 	ZB8_SERDF, "
	cSql += " 	ZB8_EMISDF, "
	cSql += " 	ZB8_CDDEST, "
	cSql += " 	ZB8_CDREM, "
	cSql += " 	ZB8_TOMADO, "
	cSql += " 	ZB8_VOLUM, "
	cSql += " 	ZB8_VLCARG, "
	cSql += " 	ZB8_PREAL, "
	cSql += " 	ZB8_CTE, "
	cSql += " 	ZB9_DANFE, "
	cSql += " 	ZB8_DTEMIS, "
	cSql += " 	ZB8_DTIMP, "
	cSql += " 	ZB8_VLDF "
	cSql += " FROM  "
	cSql += " 	" + RetSqlName("ZB8") + " ZB8, "
	cSql += " 	" + RetSqlName("ZB9") + " ZB9, "
	cSql += " 	" + RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	//****************************************************************************************************
	cSql += " AND ZB8.ZB8_FILDOC >= '" + MV_PAR01 + "' "
	cSql += " AND ZB8.ZB8_FILDOC <= '" + MV_PAR02 + "' "
	
	cSql += " AND ZB8.ZB8_DTEMIS >= '" + DTOS(MV_PAR03) + "' "
	cSql += " AND ZB8.ZB8_DTEMIS <= '" + DTOS(MV_PAR04) + "' "
	
	cSql += " AND ZB8.ZB8_NRIMP >= '" + MV_PAR05 + "' "
	cSql += " AND ZB8.ZB8_NRIMP <= '" + MV_PAR06 + "' "
	
	cSql += " AND ZB8.ZB8_CDDEST >= '" + MV_PAR07 + "' "
	cSql += " AND ZB8.ZB8_CDDEST <= '" + MV_PAR08 + "' "
	
	cSql += " AND ZB8.ZB8_CDREM >= '" + MV_PAR09 + "' "
	cSql += " AND ZB8.ZB8_CDREM <= '" + MV_PAR10 + "' "
	
	cSql += " AND ZB8.ZB8_EMISDF >= '" + MV_PAR11 + "' "
	cSql += " AND ZB8.ZB8_EMISDF <= '" + MV_PAR12 + "' "
	
	cSql += " AND ZB9.ZB9_DANFE >= '" + MV_PAR13 + "' "
	cSql += " AND ZB9.ZB9_DANFE <= '" + MV_PAR14 + "' "
	//****************************************************************************************************
	cSql += "  "
	cSql += " AND ZB8.ZB8_FILIAL = ZB9.ZB9_FILIAL "
	cSql += " AND ZB8.ZB8_NRIMP = ZB9.ZB9_NRIMP "
	cSql += "  "
	cSql += " AND ZB9.ZB9_DANFE = SF2.F2_CHVNFE "
	cSql += " ORDER BY  "
	cSql += " 	ZB8_FILDOC, "
	cSql += " 	F2_DOC, "
	cSql += " 	F2_SERIE, "
	cSql += " 	ZB8_NRDF, "
	cSql += " 	ZB8_SERDF, "
	cSql += " 	ZB8_EMISDF, "
	cSql += " 	ZB8_CDDEST, "
	cSql += " 	ZB8_CDREM, "
	cSql += " 	ZB8_TOMADO, "
	cSql += " 	ZB8_VOLUM, "
	cSql += " 	ZB8_VLCARG, "
	cSql += " 	ZB8_PREAL, "
	cSql += " 	ZB8_VLDF "
	
return cSql

Static Function getTipoPedido(cDanfe)
	Local cRet := ""
	Local cSql := ""
	Local _cAlias := GetNextAlias()
		
	cSql += " SELECT DISTINCT ZZG.ZZG_COD + ' - ' + ZZG.ZZG_DESCRI AS DESCRI "
	cSql += " FROM " + RetSqlName("SF2") + " SF2, "
	cSql += " 	" + RetSqlName("SD2") + " SD2, "
	cSql += " 	" + RetSqlName("SC5") + " SC5, "
	cSql += " 	" + RetSqlName("ZZG") + " ZZG "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = ' ' "
	cSql += " AND SD2.D_E_L_E_T_ = ' ' "
	cSql += " AND SC5.D_E_L_E_T_ = ' ' "
	cSql += " AND ZZG.D_E_L_E_T_ = ' ' "
	cSql += "  "
	cSql += " AND SF2.F2_FILIAL = SD2.D2_FILIAL "
	cSql += " AND SF2.F2_DOC 	 = SD2.D2_DOC "
	cSql += " AND SD2.D2_SERIE   = SF2.F2_SERIE "
	cSql += " AND SD2.D2_CLIENTE = SF2.F2_CLIENTE "
	cSql += " AND SD2.D2_LOJA    = SF2.F2_LOJA "
	cSql += "  "
	cSql += " AND SC5.C5_FILIAL = SD2.D2_FILIAL "
	cSql += " AND SC5.C5_NUM = SD2.D2_PEDIDO "
	cSql += "  "
	cSql += " AND SC5.C5_YTPVEN = ZZG.ZZG_COD "
	cSql += "  "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		cRet := (_cAlias)->DESCRI
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return cRet

Static Function getEstimativa(cDanfe)
	Local cQrySASR051	:= ""
	Local aRet := {}
	Local cSql := ""
	Local cChaveZB4 := ""
	Local bMedicao := .F.
	
	//--------------------------query------------------------------------
	cQrySASR051	:= "  SELECT F2_FILIAL as Filial, R_E_C_N_O_ as SF2Recno "
	cQrySASR051	+= "  FROM "+RetSqlName("SF2")+" SF2 " "
	cQrySASR051	+= "  WHERE SF2.D_E_L_E_T_ = '' "
	cQrySASR051	+= "  AND SF2.F2_CHVNFE = '" + AllTrim(cDanfe) + "' "
	
	IF SELECT("QSASR051") > 0
		QSASR051->(dbCloseArea())
	Endif
						
	TCQUERY cQrySASR051 NEW ALIAS QSASR051
	//-------------------------------------------------------------------
	dbSelectArea("QSASR051")
	QSASR051->(DBGOTOP())
		
	If !QSASR051->(EOF())
		dbSelectArea("SF2")
		SF2->(dbSetOrder(10))
		if SF2->(dbSeek(QSASR051->FILIAL+AllTrim(ZB9->ZB9_DANFE)))
			//Se for Medi��o Triangular/Direta
			dbSelectArea("ZZ2")
			ZZ2->(dbSetOrder(8))
			ZZ2->(dbGoTop())
			If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
				cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
				bMedicao := .T.
			Else
				ZZ2->(dbSetOrder(9))
				ZZ2->(dbGoTop())
				If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
					cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
					AADD(aMedicao, cChaveZB4)
					bMedicao := .T.
				Else
					ZZ2->(dbSetOrder(10))
					ZZ2->(dbGoTop())
					If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
						cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
						bMedicao := .T.
					EndIf
				EndIf
			EndIf
			
			ZZ2->(dbCloseArea())
			
			//Se for Venda Avulsa
			if !bMedicao
				dbSelectArea("SD2")
				SD2->(dbSetOrder(3))
				SD2->(dbGoTop())
				If SD2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA)))
					dbSelectArea("SC5")
					SC5->(dbSetOrder(1))
					SC5->(dbGoTop())
					If SC5->(dbSeek(SD2->(D2_FILIAL+D2_PEDIDO)))
						cChaveZB4 := SC5->(C5_FILIAL+C5_NUM)
					EndIf 
					SC5->(dbCloseArea())
				EndIf
				SD2->(dbCloseArea())
			EndIf
			
			dbSelectArea("ZB4")
			ZB4->(dbSetOrder(2))
			if ZB4->(dbSeek(cChaveZB4))
				AADD(aRet, cChaveZB4)
				AADD(aRet, Transform(ZB4->ZB4_VALOR,PesqPict("ZB4","ZB4_VALOR")))
			EndIf
			
			ZB4->(dbCloseArea())
		EndIf
	EndIF
	SF2->(dbCLoseArea())
	ZB9->(dbSkip())

	QSASR051->(dbCloseArea())
	
	if empty(aRet)
		aRet := {"",""}
	EndIf
Return aRet