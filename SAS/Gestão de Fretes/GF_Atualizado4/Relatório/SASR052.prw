#include "rwmake.ch"
#include "protheus.ch"
#include "topconn.ch"

#define DMPAPER_A4 9

#define SALTO CHR(13) + CHR(10)

//---------------------------------------------------------
/*/{Protheus.doc} SASR052

Registro de Aprova��es

Desenvolvido em TReport.
@author Samarony Barros
@since 01/09/2016
@version 1.0
/*/
//---------------------------------------------------------
User Function SASR052()
	local oReport
	Private cPerg := PadR('SASR052',10)
		
	CriaSX1(cPerg)
	Pergunte(cPerg,.T.)
	
	oReport := reportDef()
	oReport:printDialog()
Return
//---------------------------------------------------------
/*/{Protheus.doc} reportDef
Defini��o do relat�rio
@author Samarony Barros
@since 01/09/2016
@version 1.0
@return oReport, Objeto com os parametros da classe 
/*/
//---------------------------------------------------------
static function reportDef()
	local oReport
	Local oSection01

	Local nMult := 1
	
	local cTitulo := '[SASR052] - Registro de Aprova��es'
	
	oReport := TReport():New('SASR052', cTitulo,cPerg, {|oReport| PrintReport(oReport)},"Registro de Aprova��es")
	oReport:SetPortrait()

	oSection01 := TRSection():New(oReport,"Registro de Aprova��es",{"T01"})
	
	oSection01:SetTotalInLine(.F.)
	TRCell():New(oSection01, "FILIAL"			, "T01", 'FILIAL'			,,nMult*TamSX3("ZB8_FILIAL")[1]+1,,,,.T.)
	TRCell():New(oSection01, "NUMSERCTE"		, "T01", 'NUM/SER CTE'		,,nMult*TamSX3("ZB8_NRDF")[1]+TamSX3("ZB8_SERDF")[1]+1,,,,.T.)
	TRCell():New(oSection01, "CHVCTE"			, "T01", 'CHAVE CTE'		,,nMult*TamSX3("ZB8_CTE")[1]+1,,,,.T.)
	TRCell():New(oSection01, "EMISSAO"			, "T01", 'EMISSAO CTE'		,,nMult*TamSX3("ZB8_DTEMIS")[1]+1,,,,.T.)
	TRCell():New(oSection01, "STATUS"			, "T01", 'STATUS CTE'		,,nMult*TamSX3("ZB8_CTE")[1]+1,,,,.T.)
	
	TRCell():New(oSection01, "TRANSPORTADOR"	, "T01", 'TRANSPORTADOR'	,,nMult*TamSX3("A2_COD")[1]+TamSX3("A2_LOJA")[1]+1,,,,.T.)
	TRCell():New(oSection01, "NMTRANSP"			, "T01", 'NMTRANSP'			,,nMult*TamSX3("A2_NOME")[1]+1,,,,.T.)
	
	TRCell():New(oSection01, "QTDCTE"			, "T01", 'QTD NFE'			,,nMult*TamSX3("ZB8_PREAL")[1]+1,,,,.T.)
	
	TRCell():New(oSection01, "OCORRENCIA"		, "T01", 'OCORRENCIA'		,,nMult*TamSX3("ZBC_OCORR")[1]+1,,,,.T.)
	TRCell():New(oSection01, "DESOCORR"			, "T01", 'DESC OCORR'		,,nMult*TamSX3("ZBC_DESOCO")[1]+1,,,,.T.)
	TRCell():New(oSection01, "STATUSOCORR"		, "T01", 'STATUS OCORR'		,,nMult*TamSX3("ZBC_DESOCO")[1]+1,,,,.T.)
	TRCell():New(oSection01, "APROCORR"			, "T01", 'APROV OCORR'		,,nMult*TamSX3("ZBC_DESOCO")[1]+1,,,,.T.)
	TRCell():New(oSection01, "APROCORRPOR"		, "T01", 'OCORR APR POR'	,,nMult*TamSX3("ZBC_DESOCO")[1]+1,,,,.T.)
	TRCell():New(oSection01, "DTORRPOR"			, "T01", 'DT APR OCORR'		,,nMult*TamSX3("ZBC_DESOCO")[1]+1,,,,.T.)
	
	TRCell():New(oSection01, "TPAPROVALCADA"	, "T01", 'TIPO APROV'		,,nMult*TamSX3("ZBF_DESREM")[1]+1,,,,.T.)
	TRCell():New(oSection01, "STATUSAPROV"		, "T01", 'STATUS APROV'		,,nMult*TamSX3("ZBF_DESREM")[1]+1,,,,.T.)
	TRCell():New(oSection01, "APROVALCADA"		, "T01", 'APROV ALCADA'		,,nMult*TamSX3("ZBF_DESREM")[1]+1,,,,.T.)
	TRCell():New(oSection01, "DTAPROV"			, "T01", 'DT APROV'			,,nMult*TamSX3("ZBF_DESREM")[1]+1,,,,.T.)  
return (oReport)

Static Function PrintReport(oReport)
	Local oSection01 := oReport:Section(1)
	
	Local _cStatus 		:= ""
	Local _nQtdCte 		:= 0
	Local aOcorrencia 	:= {}
	Local aAlcada 		:= {}
	
	oSection01:Init()
	oSection01:SetHeaderSection(.T.)

	cSetSql := qryDefault()
	cSetSql := ChangeQuery(cSetSql) // Converte o texto num Sql
	
	dbUseArea(.T., "TOPCONN", TCGENQRY(, ,cSetSql), "QRY_FRETE", .F., .T.)
	
	oReport:SkipLine(2)
	    
	While QRY_FRETE->(!EOF())
		If oReport:Cancel()
			Exit
		EndIf
		
		//oReport:SkipLine()

		oSection01:Cell("FILIAL"):SetValue(QRY_FRETE->ZB8_FILDOC)
		oSection01:Cell("FILIAL"):SetAlign("LEFT")
		
		oSection01:Cell("NUMSERCTE"):SetValue(AllTrim(QRY_FRETE->ZB8_NRDF) + '/' + AllTrim(QRY_FRETE->ZB8_SERDF))
		oSection01:Cell("NUMSERCTE"):SetAlign("LEFT")
		
		oSection01:Cell("CHVCTE"):SetValue(AllTrim(QRY_FRETE->ZB8_CTE))
		oSection01:Cell("CHVCTE"):SetAlign("LEFT")
		
		_cStatus := getStatus(QRY_FRETE->ZB8_STATUS)
		oSection01:Cell("STATUS"):SetValue(_cStatus)
		oSection01:Cell("STATUS"):SetAlign("LEFT")
		
		oSection01:Cell("EMISSAO"):SetValue(AllTrim(DtoC(StoD(QRY_FRETE->ZB8_DTEMIS))))
		oSection01:Cell("EMISSAO"):SetAlign("LEFT")
		
		oSection01:Cell("TRANSPORTADOR"):SetValue(QRY_FRETE->ZB8_EMISDF)
		oSection01:Cell("TRANSPORTADOR"):SetAlign("LEFT")
		
		oSection01:Cell("NMTRANSP"):SetValue(POSICIONE("SA2", 1, xFilial("SA2") + QRY_FRETE->ZB8_EMISDF, "A2_NOME"))
		oSection01:Cell("NMTRANSP"):SetAlign("LEFT")
		
		_nQtdCTE := getQtdCte(QRY_FRETE->ZB8_NRIMP)
		oSection01:Cell("QTDCTE"):SetValue(Transform(_nQtdCte, PesqPict("ZB8", "ZB8_PREAL")))
		oSection01:Cell("QTDCTE"):SetAlign("RIGHT")

		aOcorrencia := getOcorrencia(ZB8->ZB8_NRIMP)
		oSection01:Cell("OCORRENCIA"):SetValue(aOcorrencia[1])
		oSection01:Cell("OCORRENCIA"):SetAlign("LEFT")
		
		oSection01:Cell("DESOCORR"):SetValue(aOcorrencia[2])
		oSection01:Cell("DESOCORR"):SetAlign("LEFT")
		
		oSection01:Cell("STATUSOCORR"):SetValue(aOcorrencia[3])
		oSection01:Cell("STATUSOCORR"):SetAlign("LEFT")
		
		oSection01:Cell("APROCORR"):SetValue(aOcorrencia[4])
		oSection01:Cell("APROCORR"):SetAlign("LEFT")
		
		oSection01:Cell("APROCORRPOR"):SetValue(aOcorrencia[5])
		oSection01:Cell("APROCORRPOR"):SetAlign("LEFT")
		
		oSection01:Cell("DTORRPOR"):SetValue(aOcorrencia[6])
		oSection01:Cell("DTORRPOR"):SetAlign("LEFT")
		
		aAlcada := getAlcada(ZB8->ZB8_NRIMP)
		oSection01:Cell("APROVALCADA"):SetValue(aAlcada[1])
		oSection01:Cell("APROVALCADA"):SetAlign("LEFT")
		
		oSection01:Cell("TPAPROVALCADA"):SetValue(aAlcada[2])
		oSection01:Cell("TPAPROVALCADA"):SetAlign("LEFT")
		
		oSection01:Cell("STATUSAPROV"):SetValue(aAlcada[3])
		oSection01:Cell("STATUSAPROV"):SetAlign("LEFT")
		
		oSection01:Cell("DTAPROV"):SetValue(aAlcada[4])
		oSection01:Cell("DTAPROV"):SetAlign("LEFT")

		//oSection01:Cell("RK"):SetValue(Transform(QRY_FRETE->ZB8_VLDF / QRY_FRETE->ZB8_PREAL, PesqPict("ZB8", "ZB8_VLDF")))
		//oSection01:Cell("RK"):SetAlign("RIGHT")
		
		oSection01:PrintLine()
		
		QRY_FRETE->(DBSkip())
	ENDDO
	QRY_FRETE->(dbCloseArea())
	oSection01:Finish()
Return

static function CriaSX1(cPerg)
	PutSx1(cPerg, "01","Tomador De"           ,".",".","mv_ch01","C",08,0,1,"G","","SM0","","","mv_par01","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Tomador Ate"          ,".",".","mv_ch02","C",08,0,1,"G","","SM0","","","mv_par02","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "03","Data Emissao Cte De"  ,".",".","mv_ch03","D",08,0,1,"G","",""   ,"","","mv_par03","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "04","Data Emissao Cte Ate" ,".",".","mv_ch04","D",08,0,1,"G","",""   ,"","","mv_par04","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "05","Codigo Importacao De" ,".",".","mv_ch05","C",16,0,1,"G","","ZB8SEQ"   ,"","","mv_par05","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "06","Codigo Importacao Ate",".",".","mv_ch06","C",16,0,1,"G","","ZB8SEQ"   ,"","","mv_par06","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "07","Destinatario De"      ,".",".","mv_ch07","C",14,0,1,"G","","SA1","","","mv_par07","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "08","Destinatario Ate"     ,".",".","mv_ch08","C",14,0,1,"G","","SA1","","","mv_par08","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "09","Remetente De"         ,".",".","mv_ch09","C",14,0,1,"G","","SA2","","","mv_par09","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "10","Remetente Ate"        ,".",".","mv_ch10","C",14,0,1,"G","","SA2","","","mv_par10","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "11","Emissor De"           ,".",".","mv_ch11","C",14,0,1,"G","","SA2","","","mv_par11","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "12","Emissor Ate"          ,".",".","mv_ch12","C",14,0,1,"G","","SA2","","","mv_par12","" ,"","","","","","","","","","","","","","","")
	
	PutSx1(cPerg, "13","Chave CTE De"         ,".",".","mv_ch13","C",44,0,1,"G","",""   ,"","","mv_par13","" ,"","","","","","","","","","","","","","","")
	PutSx1(cPerg, "14","Chave CTE Ate"        ,".",".","mv_ch14","C",44,0,1,"G","",""   ,"","","mv_par14","" ,"","","","","","","","","","","","","","","")
	
	//PutSx1(cPerg, "15","UF De"                ,".",".","mv_ch15","C",02,0,1,"G","","12"   ,"","","mv_par15","" ,"","","","","","","","","","","","","","","")
	//PutSx1(cPerg, "16","UF Ate"               ,".",".","mv_ch16","C",02,0,1,"G","","12"   ,"","","mv_par16","" ,"","","","","","","","","","","","","","","")
Return


Static function qryDefault()
	Local cSql := ""

	cSql += " SELECT "
	cSql += " 	* "
	cSql += " FROM  "
	cSql += " 	" + RetSqlName("ZB8") + " ZB8, "
	cSql += " 	" + RetSqlName("ZB9") + " ZB9 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	//****************************************************************************************************
	cSql += " AND ZB8.ZB8_FILDOC >= '" + MV_PAR01 + "' "
	cSql += " AND ZB8.ZB8_FILDOC <= '" + MV_PAR02 + "' "
	
	cSql += " AND ZB8.ZB8_DTEMIS >= '" + DTOS(MV_PAR03) + "' "
	cSql += " AND ZB8.ZB8_DTEMIS <= '" + DTOS(MV_PAR04) + "' "
	
	cSql += " AND ZB8.ZB8_NRIMP >= '" + MV_PAR05 + "' "
	cSql += " AND ZB8.ZB8_NRIMP <= '" + MV_PAR06 + "' "
	
	cSql += " AND ZB8.ZB8_CDDEST >= '" + MV_PAR07 + "' "
	cSql += " AND ZB8.ZB8_CDDEST <= '" + MV_PAR08 + "' "
	
	cSql += " AND ZB8.ZB8_CDREM >= '" + MV_PAR09 + "' "
	cSql += " AND ZB8.ZB8_CDREM <= '" + MV_PAR10 + "' "
	
	cSql += " AND ZB8.ZB8_EMISDF >= '" + MV_PAR11 + "' "
	cSql += " AND ZB8.ZB8_EMISDF <= '" + MV_PAR12 + "' "
	
	cSql += " AND ZB9.ZB9_DANFE >= '" + MV_PAR13 + "' "
	cSql += " AND ZB9.ZB9_DANFE <= '" + MV_PAR14 + "' "
	//****************************************************************************************************
	cSql += "  "
	cSql += " AND ZB8.ZB8_FILIAL = ZB9.ZB9_FILIAL "
	cSql += " AND ZB8.ZB8_NRIMP = ZB9.ZB9_NRIMP "
	cSql += "  "
	
return cSql

Static Function getStatus(cStatus)
	Do Case
		Case cStatus=='IM'
			cRet := "Importando"
		Case cStatus=='PE'
			cRet := "Pedente"
		Case cStatus=='BL'
			cRet := "Bloqueado"
		Case cStatus=='AV'
			cRet := "Aguardando Visto Ocorr�ncia"
		Case cStatus=='AG'
			cRet := "Aguardando Aprova��o"
		Case cStatus=='AA'
			cRet := "Aprovado"
		Case cStatus=='SA'
			cRet := "Solicitado Anula��o"
		Case cStatus=='DE'
			cRet := "Documento de Entrada Gerado"
		Case cStatus=='FG'
			cRet := "Fatura Gerada"
	EndCase
Return cRet

Static Function getQtdCte(cSeqImp)
	Local nRet := 0
	Local cSql := ""
	Local _cAlias := GetNextAlias()
	
	cSql += " SELECT COUNT(*) QTD_NFE "
	cSql += " FROM " + RetSqlName("ZB9") + " ZB9 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.ZB9_FILIAL = '"+xFilial("ZB9")+"' "
	cSql += " AND ZB9.ZB9_NRIMP = '"+cSeqImp+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		nRet := (_cAlias)->QTD_NFE 
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return nRet

Static Function getOcorrencia(cSeqImp)
	Local aRet := {}
	
	Local cOcorr := ""
	Local cDesOco := ""
	Local cStOco := ""
	Local cAprOco := ""
	Local cAprPorOco := ""
	Local cDtOco := ""
	
	dbSelectArea("ZBE")
	ZBE->(dbSetOrder(1))
	ZBE->(dbGoTop())
	If ZBE->(dbSeek(xFilial("ZBE") + cSeqImp))
		While !(ZBE->(EOF())) .AND. ZBE->ZBE_NRIMP == cSeqImp
			cOcorr 		+= ZBE->ZBE_OCORR + SALTO
			cDesOco 	+= ZBE->ZBE_DESOCO + SALTO
			cStOco 		+= getSttOcorr(ZBE->ZBE_STATUS) + SALTO
			cAprOco 	+= UsrRetName(ZBE->ZBE_USER) + SALTO
			cAprPorOco 	+= UsrRetName(ZBE->ZBE_APUSER) + SALTO
			cDtOco 		+= DtoC(ZBE->ZBE_DTAPRO) + '-' + ZBE->ZBE_HRAPRO + SALTO
			
			ZBE->(dbSkip())
		EndDo
	EndIf
	ZBE->(dbCloseArea())
	
	AADD(aRet,cOcorr)
	AADD(aRet,cDesOco)
	AADD(aRet,cStOco)
	AADD(aRet,cAprOco)
	AADD(aRet,cAprPorOco)
	AADD(aRet,cDtOco)
Return aRet

Static Function getSttOcorr(cStatus)	
	Local cRet := ""
	
	Do Case
		Case cStatus == 'A'
			cRet := "Aprovado"
		Case cStatus == 'B'
			cRet := "Bloqueado"
		Case cStatus == 'R'
			cRet := "Reprovado"
		Case cStatus == 'L'
			cRet := "Liberado"
	End Case
Return cRet

Static Function getAlcada(cSeqImp)
	Local aRet := {}
	
	Local cStAlc 	:= ""
	Local cTpAlc 	:= ""
	Local cAprAlc 	:= ""
	Local cDtAlc 	:= ""
	
	dbSelectArea("ZBF")
	ZBF->(dbSetOrder(1))
	ZBF->(dbGoTop())
	If ZBF->(dbSeek(xFilial("ZBF") + cSeqImp))
		While !(ZBF->(EOF())) .AND. ZBF->ZBF_NRIMP == cSeqImp
			cStAlc 		+= getSttAlc(ZBF->ZBF_STATUS) + SALTO
			cTpAlc 		+= getTpAlc(ZBF->ZBF_TPAPRO) + SALTO
			cAprAlc 	+= UsrRetName(ZBF->ZBF_USER) + SALTO
			cDtAlc 		+= DtoC(ZBF->ZBF_DTAPRO) + '-' + ZBF->ZBF_HRAPRO + SALTO
			
			ZBF->(dbSkip())
		EndDo
	EndIf
	ZBF->(dbCloseArea())
	
	AADD(aRet,cStAlc)
	AADD(aRet,cTpAlc)
	AADD(aRet,cAprAlc)
	AADD(aRet,cDtAlc)	
Return aRet

Static Function getSttAlc(cStatus)	
	Local cRet := ""
	
	Do Case
		Case cStatus == 'A'
			cRet := "Aprovado"
		Case cStatus == 'B'
			cRet := "Bloqueado"
		Case cStatus == 'R'
			cRet := "Reprovado"
		Case cStatus == 'L'
			cRet := "Liberado"
	End Case
Return cRet

Static Function getTpAlc(cTipo)	
	Local cRet := ""
	
	Do Case
		Case cTipo == '1'
			cRet := "Aprova��o por valor"
		Case cTipo == '2'
			cRet := "Aprova��o R$/Kg"
	End Case
Return cRet