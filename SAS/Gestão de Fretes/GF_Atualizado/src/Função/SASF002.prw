#Include 'Protheus.ch'
#include "totvs.ch"

//------------------------------------------------------------
/*/{Protheus.doc} SASF002

Fun��o para mostrar a tela de processamento de toda a rotina

@type function
@author Sam Barros
@since 04/07/2016
@version 1.0
/*/
//------------------------------------------------------------
User Function SASF002()
	Processa( {|| U_SASFO02() }, "Aguarde...", "Importando estimativa de frete...",.F.)
Return 

//------------------------------------------------------------
/*/{Protheus.doc} SASFO02

Fun��o para realizar a importa��o da estimativa de frete

@author Sam Barros
@since 04/07/2016
@version 1.0
/*/
//------------------------------------------------------------
User Function SASFO02()
	Local cLinha  	:= ""
	Local bFirst   	:= .T.
	Local aCampos 	:= {}
	Local aDados  	:= {}
	
	Private cFile 	:= ""
 
	Private aErro 		:= {}
	Private _cExtens   	:= "Arquivo CSV ( *.CSV ) |*.csv|"
	Private aErrFile 	:= {}
	
	cFile := cGetFile( _cExtens, "Selecione o Arquivo",1,, .T., GETF_LOCALHARD+GETF_LOCALFLOPPY)//GETF_NETWORKDRIVE + GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_RETDIRECTORY )
 
	If !File(cFile)
		MsgStop("O arquivo " + cFile + " n�o foi encontrado. A importa��o ser� abortada!","[SAS] - ATENCAO")
		Return
	EndIf
 
	FT_FUSE(cFile)
	ProcRegua(FT_FLASTREC())
	FT_FGOTOP()
	
	While !FT_FEOF()
		IncProc("Lendo arquivo CSV...")
 
		cLinha := FT_FREADLN()
 
		If bFirst
			aCampos := Separa(cLinha,";",.T.)
			For i := 1 to len(aCampos)
				aCamposDiv := Separa(aCampos[i],"-",.T.)
				aCampos[i] := AllTrim(aCamposDiv[2])
			Next i
			bFirst := .F.
			
			aErrFile := validFile(aCampos, 'C') 
			if !aErrFile[1]
				MsgStop(aErrFile[3], aErrFile[2])
				return 
			EndIf
		Else
			aDadosDiv := Separa(cLinha,";",.T.) 
			AADD(aDados, aDadosDiv)
			
			aErrFile := validFile(aDadosDiv, 'D', len(aDados) + 1) 
			if !aErrFile[1]
				MsgStop(aErrFile[3], aErrFile[2])
				return 
			EndIf
		EndIf
 
		FT_FSKIP()
	EndDo
	
	
	
	_nTam := len(AllTrim(STR(len(aDados))))
 
	Begin Transaction
		ProcRegua(Len(aDados))
		For i:=1 to Len(aDados)
 
			IncProc("Importando Estimativa de fretes [Linha " + Alltrim(STRZERO(i,_nTam,0)) + "]")
 
			dbSelectArea("ZB4")
			dbSetOrder(2)
			dbGoTop()
			If !dbSeek(xFilial("ZB4")+aDados[i,1])
				Reclock("ZB4",.T.)
				ZB4->ZB4_FILIAL	:= xFilial("ZB4")
				ZB4->ZB4_COD 	:= GETSXENUM("ZB4","ZB4_COD")
				ZB4->ZB4_DATA 	:= dDataBase
				For j:=1 to Len(aCampos)
					cCampo  := "ZB4->" + aCampos[j]
					if j%2 <> 0//ValType(aDados[i,j]) == 'C'
						&cCampo := aDados[i,j]
					Else
						&cCampo := Val(aDados[i,j])
					EndIf
				Next j
				
				ConfirmSx8()
				
				ZB4->(MsUnlock())
			EndIf
		Next i
	End Transaction
 
	FT_FUSE()
 
	ApMsgInfo("Importa��o da estimativa de fretes conclu�da com sucesso!","[SAS] - SUCESSO")
     
Return

//------------------------------------------------------------
/*/{Protheus.doc} ValidFile

Fun��o para validar se o arquivo est� nos padr�es 
de importa��o da estimativa de frete.

@author Sam Barros
@since 04/07/2016
@version 1.0
@param aDados, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
//------------------------------------------------------------
Static Function ValidFile(aVetor, cTipo, nLinha)
	Local aRet := {.T.,"",""}
	
	Default nLinha := 0
	
	if cTipo == 'C'
		//------------------------------------------------------------------------
		//Valida Campos
		//Valida��o do erro de nomeclatura do arquivo.
		//Verifica se os itens s�o da tabela ZB4
		//------------------------------------------------------------------------
		for i := 1 to len(aVetor)
			if (SUBS(aVetor[i],1,4)<>'ZB4_')
				Return {.F., "Erro: Nomeclatura de campos", "O arquivo informado n�o est� no padr�o de importa��o de estimativa." + CRLF + "A opera��o ser� cancelada."}
			EndIf
		Next i
	Else
		//------------------------------------------------------------------------
		//Valida Dados
		//Valida��o de linha/c�lula em branco
		//------------------------------------------------------------------------
		if len(aVetor) == 0
			return aRet
		Else
			For i := 1 to len(aVetor)
				If Empty(aVetor[i])
					Return {.F., "Erro: C�lula em branco", ;
									"Uma ou mais c�lulas do arquivo est�o em branco" + ;
									CRLF  + "Linha ("+cFile+"): " + AllTrim(STR(nLinha))}
				EndIf
			Next i
		EndIf
		//------------------------------------------------------------------------
		//Valida Dados
		//Valida��o do erro string em campo num�ricos.
		//------------------------------------------------------------------------
		if !u_isNumeric(aVetor[2])
			Return {.F., "Erro: Num�rico", ;
							"A coluna de campos VALOR possui itens com valores caracteres" + ;
							CRLF + aVetor[2] + CRLF + "Linha ("+cFile+"): " + AllTrim(STR(nLinha))}
		EndIf
		//------------------------------------------------------------------------
		//Valida Dados
		//Valida��o se a chave j� foi importada
		//------------------------------------------------------------------------
		If jaImportado(aVetor[1])
			Return {.F., "Erro: J� Importador", ;
							"A chave informada j� foi importada" + ;
							CRLF + aVetor[1] + CRLF + "Linha ("+cFile+"): " + AllTrim(STR(nLinha))}
		EndIf
	EndIf
	
	//------------------------------------------------------------------------
	//TODO: 2 - Verificar se j� importou o arquivo
	//TODO: 3 - Verificar Se a chave est� no padr�o
	//TODO: 4 - Verificar se os campos num�ricos est�o corretamente inseridos
	//------------------------------------------------------------------------
Return aRet

Static Function jaImportado(cChave)
	Local bRet := .F.
	Local xArea := getArea()
	
	dbSelectArea("ZB4")
	dbSetOrder(2)
	If dbSeek(cChave)
		bRet := .T.
	EndIf
	
	ZB4->(dbCLoseArea())
	
	RestArea(xArea)
Return bRet 