#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASV002

Valida��o do campo ZB0_USER no cadastro de importadores.
Verifica se j� existe importador cadastrado com o mesmo 
c�digo de usu�rio.
 
Usado no validador do campo. 
@author Sam Barros
@since 03/08/2016
@version 1.0
@return bRet, Valida��o de exist�ncia de registro
@example
U_SASV002()
/*/
//-------------------------------------------------------------------
User Function SASV002()
	Local bRet := .T.
	Local aArea := getArea()
	
	dbSelectArea("ZB0")
	dbSetOrder(2)
	If dbSeek(xFilial("ZB0")+M->ZB0_USER)
		MsgAlert("J� existe importador relacionado a este usu�rio!")
		bRet := .F.
	EndIf
	
	ZB0->(dbCloseArea())
	
	RestArea(aArea)
Return bRet