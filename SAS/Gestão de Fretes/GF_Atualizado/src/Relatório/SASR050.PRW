#Include "Protheus.ch"
#Include "TopConn.ch"
#include "Rwmake.ch"
 
//******************************************************************//
//Autor: Elicio Junior							      Data:28/07/16 //
//******************************************************************//
//Fun��o: Relat�rio CTE X NFE			   					   	    //
//******************************************************************//
User Function SASR050()
Local oReport

oReport:=ReportDef()
oReport:PrintDialog()


//------------------------------------------------//
//FUN��O: CRIACAO DA SE�AO E CELULAS			  //
//------------------------------------------------//
Static Function ReportDef()
Local oReport
Local oSection
Local oSection2
Local oSection3
Local cPerg     :="SASR050"

AjustaSX1(cPerg)
Pergunte(cPerg,.F.)

oReport := TReport():New("SASR050","Relat�rio CTE X NFE","SASR050", {|oReport| ReportPrint(oReport)},"Relat�rio CTE X NFE")
// oReport:SetLandscape() //Paisagem

//������������������������������������������������������������������������Ŀ
//� Section 1			                                                  �
//��������������������������������������������������������������������������
oSection:= TRSection():New(oReport,OemToAnsi("Filial"),{"ZB8"})
TRCell():New(oSection,"ZB8_FILDOC"		," ","Filial"    ,,50)
TRCell():New(oSection,"NOMEFILIAL"    ," ","Nome Filial" ,,90)

//������������������������������������������������������������������������Ŀ
//� Section 1			                                                  �
//��������������������������������������������������������������������������
oSection1:= TRSection():New(oReport,OemToAnsi("Transportador"),{"ZB8","SA2"})
TRCell():New(oSection1,"SPACE1"		," ",SPACE(10)             ,,10)
TRCell():New(oSection1,"ZB8_EMISDF"	," ","Transportadora"      ,,30)
TRCell():New(oSection1,"A2_NOME"    ," ","Nome Transportadora" ,,80)
TRCell():New(oSection1,"A2_CGC"     ," ","Cnpj"                ,,20)

//������������������������������������������������������������������������Ŀ
//� Section 2			                                                  �
//��������������������������������������������������������������������������
oSection2:= TRSection():New(oReport,OemToAnsi("CTE"),{"ZB8"})
//oSection2:SetTotalInLine(.F.)
TRCell():New(oSection2,"SPACE2"		," ",SPACE(10)                            ,,10)
TRCell():New(oSection2,"SPACE3"		," ",SPACE(10)                            ,,10)
TRCell():New(oSection2,"ZB8_CTE"	," ","Chave CTE"       					  ,,55)
TRCell():New(oSection2,"ZB8_NRDF"	," ","CTE"             					  ,,15)
TRCell():New(oSection2,"ZB8_VLDF"  ," ","Valor" ,  PesqPict("ZB8","ZB8_FRVAL") ,30)
TRCell():New(oSection2,"ZB8_DTEMIS" ," ","Emiss�o"							  ,,20)

//������������������������������������������������������������������������Ŀ
//� Section 3			                                                  �
//��������������������������������������������������������������������������
oSection3:= TRSection():New(oReport,OemToAnsi("NFE"),{"ZB8","SF2","SA1"})
//oSection3:SetTotalInLine(.F.)
TRCell():New(oSection3,"SPACE4"		," ",SPACE(10)                              ,,10)
TRCell():New(oSection3,"SPACE5"		," ",SPACE(10)                              ,,10)
TRCell():New(oSection3,"SPACE6"		," ",SPACE(10)                              ,,10)
TRCell():New(oSection3,"F2_CHVNFE" ," ","Chave NFE"         					,,50)
TRCell():New(oSection3,"ZB8_CDDEST" ," ","Cliente"								,,10)
TRCell():New(oSection3,"A1_NOME"    ," ","Nome Cliente"							,,50)
TRCell():New(oSection3,"TOTALNFE"   ," ","Total NFE"  ,PesqPict("SE1","E1_VALOR"),20)
TRCell():New(oSection3,"F2_EMISSAO" ," ","Emiss�o"								,,20)

return(oReport)

//------------------------------------------------//
//FUN��O: IMPRESSAO								  //
//------------------------------------------------//
Static Function ReportPrint(oReport)

Local oSection   := oReport:Section(1)
Local oSection1  := oReport:Section(2)
Local oSection2  := oReport:Section(3)
Local oSection3  := oReport:Section(4)

Private cNfe    
Private cfilex 
Private cTransp 
Private cCte    


oReport:SetTitle( "Relat�rio CTE x NFE Periodo "+DTOC(MV_PAR03)+" a "+DTOC(MV_PAR04)+".")

oSection:BeginQuery() //Metodo para iniciar a Sele��o dos Registros.

cQuery:=" SELECT ZB8_FILDOC,ZB8_EMISDF,ZB8_NRDF,ZB8_CTE,ZB8_SERDF,ZB8_DTEMIS,ZB8_CDREM,ZB8_CDDEST,ZB8_CFOP,"
cQuery+=" ZB8_VLDF,ZB8_NRIMP,ZB9_DANFE,F2_CHVNFE,A1_NOME,A2_NOME,F2_EMISSAO,A2_CGC,"
cQuery+=" (SELECT SUM(D2_TOTAL) FROM "+RetSqlName("SD2")+" WHERE D2_DOC=F2_DOC AND  D_E_L_E_T_='') AS TOTALNFE "
cQuery+=" FROM "+ RetSqlName("ZB8") + " ZB8 " 
cQuery+=" LEFT OUTER  JOIN "+RetSqlName("SA1") + " SA1 ON A1_COD+A1_LOJA = ZB8.ZB8_CDDEST  AND  SA1.D_E_L_E_T_='' "
cQuery+=" LEFT OUTER  JOIN "+RetSqlName("SA2") + " SA2 ON A2_COD+A2_LOJA = ZB8.ZB8_EMISDF  AND  SA2.D_E_L_E_T_='' "
cQuery+=" LEFT OUTER  JOIN "+RetSqlName("ZB9") + " ZB9 ON ZB9_NRIMP      = ZB8_NRIMP       AND  ZB9.D_E_L_E_T_='' "
cQuery+=" LEFT OUTER  JOIN "+RetSqlName("SF2") + " SF2 ON F2_CHVNFE      = ZB9_DANFE       AND  SF2.D_E_L_E_T_='' "
cQuery+=" WHERE "
cQuery+=" ZB8.D_E_L_E_T_='' AND  "
cQuery+=" ZB8_FILDOC    BETWEEN '"+MV_PAR01       +"' AND '"+MV_PAR02	     +"' AND "
cQuery+=" ZB8_DTEMIS    BETWEEN '"+DTOS(MV_PAR03) +"' AND '"+DTOS(MV_PAR04)  +"' AND "
cQuery+=" ZB8_NRIMP     BETWEEN '"+MV_PAR05       +"' AND '"+MV_PAR06	     +"' AND "
cQuery+=" ZB8_CDDEST    BETWEEN '"+MV_PAR07       +"' AND '"+MV_PAR08	     +"' AND "
cQuery+=" ZB8_CDREM     BETWEEN '"+MV_PAR09       +"' AND '"+MV_PAR10	     +"' AND "
cQuery+=" ZB8_EMISDF    BETWEEN '"+MV_PAR11       +"' AND '"+MV_PAR12	     +"' AND "
cQuery+=" ZB9_DANFE     BETWEEN '"+MV_PAR13       +"' AND '"+MV_PAR14	     +"' " 
cQuery+=" ORDER BY ZB8_FILDOC,ZB8_EMISDF,ZB8_CTE,ZB9_DANFE "

IF SELECT("T1")>0
	T1->(dbCloseArea())
Endif
TCQUERY cQuery NEW ALIAS T1

oSection:EndQuery()

oReport:SetMeter(RecCount()) //Metodo para Dizer o tamanho da barra de progresso baseado no RecCount( Quantidade de Registros )

cfilex  := ''
cTransp := ''
cCte    := ''

dbSelectArea("T1")
dbgotop()
While T1->(!EOF())  //Se n�o for Final do Arquivo
    
	If oReport:Cancel()  //Se o Relatorio for Cancelado
		Exit   //Sai do la�o
	EndIf
        
   	if cfilex <> T1->ZB8_FILDOC .OR. T1->(EOF())

		oReport:EndPage() //Comecar do Topo
		oSection:Init()
		oSection:Cell("ZB8_FILDOC" ):SetValue(T1->ZB8_FILDOC)
		oSection:Cell("NOMEFILIAL"):SetValue(FWFilialName(cEmpAnt,T1->ZB8_FILDOC,1))
		oSection:PrintLine()  //Imprimir a Sec��o
		oReport:SkipLine()    //Salto de linha na sec��o
		oSection:Finish()
		cfilex := T1->ZB8_FILDOC

	Endif

   	if cTransp <> T1->ZB8_EMISDF .OR. T1->(EOF())

		oSection1:Init()
		oSection1:Cell("SPACE1" ):SetValue(SPACE(10))
		oSection1:Cell("ZB8_EMISDF" ):SetValue(T1->ZB8_EMISDF)
		oSection1:Cell("A2_NOME"):SetValue(T1->A2_NOME)
		oSection1:Cell("A2_CGC" ):SetValue(trans(strzero(val(T1->A2_CGC),14),"@R 99.999.999/9999-99"))
		oSection1:PrintLine()  //Imprimir a Sec��o
		oReport:SkipLine()    //Salto de linha na sec��o
		oSection1:Finish()
		cTransp := T1->ZB8_EMISDF
	Endif

	if cCte <> T1->ZB8_CTE .OR. T1->(EOF())

		oSection2:Init()
		oSection2:Cell("SPACE2" ):SetValue(SPACE(10))
		oSection2:Cell("SPACE3" ):SetValue(SPACE(10))
		oSection2:Cell("ZB8_CTE" ):SetValue(T1->ZB8_CTE)
		oSection2:Cell("ZB8_NRDF" ):SetValue(T1->ZB8_NRDF)
		oSection2:Cell("ZB8_VLDF"):SetValue(T1->ZB8_VLDF)
		oSection2:Cell("ZB8_DTEMIS"):SetValue(STOD(ZB8_DTEMIS))
		oSection2:PrintLine()  //Imprimir a Sec��o
		oReport:SkipLine()    //Salto de linha na sec��o
		oSection2:Finish()
		cCte := T1->ZB8_CTE
	Endif

	nSeq:=1
	oSection3:Init()
   	While 	cCte = T1->ZB8_CTE .and. T1->(!EOF())

		oSection3:Cell("SPACE4" ):SetValue(SPACE(10))
		oSection3:Cell("SPACE5" ):SetValue(SPACE(10))
		oSection3:Cell("SPACE6" ):SetValue(SPACE(10))
		oSection3:Cell("F2_CHVNFE"):SetValue(T1->F2_CHVNFE)
		oSection3:Cell("ZB8_CDDEST"    ):SetValue(T1->ZB8_CDDEST)
		oSection3:Cell("A1_NOME"    ):SetValue(T1->A1_NOME)
		oSection3:Cell("TOTALNFE"     ):SetValue(T1->TOTALNFE)
		oSection3:Cell("F2_EMISSAO"    ):SetValue(STOD(T1->F2_EMISSAO))
		oSection3:PrintLine()  //Imprimir a Sec��o
		nSeq++
        T1->(dbSkip())

	Enddo
	oSection3:Finish()

	oReport:SkipLine()    //Salto de linha na sec��o

Enddo

T1->(dbCloseArea())

return

//|----------------------------------------------------------|//
//|Fun��o: Cria��o das Perguntas							 |//
//|----------------------------------------------------------|//
Static Function AjustaSX1(cPerg)

PutSx1(cPerg, "01","Tomador De"           ,".",".","mv_ch01","C",06,0,1,"G","","SM0","","","mv_par01","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "02","Tomador Ate"          ,".",".","mv_ch02","C",06,0,1,"G","","SM0","","","mv_par02","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "03","Data Emissao Cte De"  ,".",".","mv_ch03","D",08,0,1,"G","",""   ,"","","mv_par03","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "04","Data Emissao Cte Ate" ,".",".","mv_ch04","D",08,0,1,"G","",""   ,"","","mv_par04","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "05","Codigo Importacao De" ,".",".","mv_ch05","C",06,0,1,"G","",""   ,"","","mv_par05","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "06","Codigo Importacao Ate",".",".","mv_ch06","C",06,0,1,"G","",""   ,"","","mv_par06","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "07","Destinatario De"      ,".",".","mv_ch07","C",06,0,1,"G","","SA1","","","mv_par07","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "08","Destinatario Ate"     ,".",".","mv_ch08","C",06,0,1,"G","","SA1","","","mv_par08","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "09","Remetente De"         ,".",".","mv_ch09","C",06,0,1,"G","","SA2","","","mv_par09","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "10","Remetente Ate"        ,".",".","mv_ch10","C",06,0,1,"G","","SA2","","","mv_par10","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "11","Emissor De"           ,".",".","mv_ch11","C",06,0,1,"G","","SA2","","","mv_par11","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "12","Emissor Ate"          ,".",".","mv_ch12","C",06,0,1,"G","","SA2","","","mv_par12","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "13","Chave CTE De"         ,".",".","mv_ch13","C",06,0,1,"G","",""   ,"","","mv_par13","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "14","Chave CTE Ate"        ,".",".","mv_ch14","C",06,0,1,"G","",""   ,"","","mv_par14","" ,"","","","","","","","","","","","","","","")

Return()

