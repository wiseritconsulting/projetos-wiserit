#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO009

Valida��o de Ocorr�ncia

TRANSPORTADORA N�O CADASTRADA                                            

Valida se o remetente est� correto, ou seja, se n�o exite
nenhuma pendencia no CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO009()
/*/
//-------------------------------------------------------------
User Function SASO009()
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()
	Local nTotVer := 0
	Local nTotOcor := 0

	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		nTotVer++
		If Empty(Alltrim((_cAlias)->ZB8_EMISDF))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			nTotOcor++
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO009", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	
	AutoGrLog("-------------- Total Verificados: " + AllTrim(Str(nTotVer)))
	AutoGrLog("-------------- Total Ocorr�ncias: " + AllTrim(Str(nTotOcor)))
		
	(_cAlias)->( DBCloseArea() )
Return 
