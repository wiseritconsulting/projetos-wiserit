#Include 'Protheus.ch'

//------------------------------------------------------------
/*/{Protheus.doc} SASP088

Cadastro Modal

@type function
@author Sam Barros
@since 11/07/2016
@version 1.0
/*/
//------------------------------------------------------------
User Function SASP088()
	//---------------------------------------------------------//
	//Declara��o de vari�veis
	//---------------------------------------------------------//
	Private cPerg   	:= "1"
	Private cCadastro 	:= "Cadastro Modal"
	Private cDelFunc 	:= ".T." // Validacao para a exclusao. Pode-se utilizar ExecBlock
	Private cString 	:= "ZBA"
	Private cFiltro		:= ""
	Private	aIndexZBA	:= {}
	Private aIndex		:= {}
	Private cCodUse		:= RetCodUsr()
	Private bFiltraBrw 	:= { || FilBrowse( "ZBA" , @aIndex , @cFiltro ) } //Determina a Expressao do Filtro
	Private aRotina 	:= {}
	Private aRotAprov 	:= {}
	Private aRotStts 	:= {}
	Private aCores		:= { }
		
	AADD(aRotina, {"Pesquisar"		,"AxPesqui"		,0,1})
	AADD(aRotina, {"Visualizar"		,"AxVisual"		,0,2})
	//AADD(aRotina, {"Incluir"		,"AxInclui"		,0,3})
	AADD(aRotina, {"Alterar"		,"AxAltera"		,0,4})
	//AADD(aRotina, {"Excluir"		,"AxDeleta"		,0,5})
	//AADD(aRotina, {"Legenda"      	,"u_legZBA()"	,0,6})
	
	dbSelectArea("ZBA")
	dbSetOrder(1)

	cPerg   := "1"

	Pergunte(cPerg,.F.)

	dbSelectArea(cString)
	mBrowse( 6,1,22,75,cString,,,,,6,)

	Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros

	ZBA->(dbCloseArea())
Return