#Include 'Protheus.ch'
#INCLUDE 'FWMBROWSE.CH'
#INCLUDE 'FWMVCDEF.CH'
#include 'TopConn.CH'

User Function SASP091(A)
	Local aArea := getArea()
	Local oDlgNFE
	Local oBrwNFE
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Local cTextHead	:= "QLabel { color: rgb(39,82,102); font-size: 12px; font-weight: bold;  } "
	Private aObjTela	:= {}
	Private aCampos		:= {}
	
	Private _nNumber 
	Private _cTmp 

	Private _cSeqImp 
	Private _cChvCTE 
	Private _cTomador 
	Private _cRemetente 
	Private _cDestinatario
	Private _cTransportadora 

	Private _nSkip
	Private _nLin 	  
	
	Private _nColTit1 
	Private _nColDad1
	
	Private _nColTit2 
	Private _nColDad2 
	
	Private _xp_cNrImp := A
		  
	oDlgNFE := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Dados Ocorr�ncia',,,,nOr(WS_VISIBLE,WS_POPUP),,,,,.T.,,,,)

	@00,00 MSPANEL oBarNFE SIZE 50,50 of oDlgNFE
	oBarNFE:Align := CONTROL_ALIGN_TOP
	
	//-------------------VARIAVEIS---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	_nNumber := 1
	_cTmp := StrZero(_nNumber,5,0)
	
	_cSeqImp 		:= ""
	_cChvCTE 		:= ""
	_cTomador 		:= ""
	_cRemetente 	:= ""
	_cDestinatario 	:= ""
	
	_cCTE 			:= ""
	_cValFrete 		:= ""
	_cPeso 			:= ""
	_cVolume 		:= ""
	_cEmissao 		:= ""
	
	_nSkip	  := 007
	_nLin 	  := 001
	
	_nColTit1 := 010
	_nColDad1 := 080
	
	_nColTit2 := 290
	_nColDad2 := 330
	
	AtualTela()
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColTit1, {||"Sequ�ncia Importa��o: "}	,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColTit1, {||"Chave CTE: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColTit1, {||"Transportador: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColTit1, {||"Remetente: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColTit1, {||"Destinat�rio: "}			,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 5*_nSkip,_nColTit1, {||"Tomador: "}			,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)

	
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColDad1, {||_cSeqImp}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColDad1, {||_cChvCTE}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColDad1, {||_cTransportador}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColDad1, {||_cRemetente}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)	
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColDad1, {||_cDestinatario}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 5*_nSkip,_nColDad1, {||_cTomador}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColTit2, {||"CTE: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColTit2, {||"Valor Frete: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColTit2, {||"Peso Total: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColTit2, {||"Volume: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColTit2, {||"Emiss�o: "}			,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColDad2, {||_cCTE}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColDad2, {||_cValFrete}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColDad2, {||_cPeso}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColDad2, {||_cVolume}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)	
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColDad2, {||_cEmissao}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//oGetSED:=TGet():New( 001,400,{|u| If(PCount()>0,cGetSED :=u,cGetSED )},oGrp3,028,008,'999999',{|| ValCons(@cGetSED)},CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SED","cGetCodCons" ,,)
	//oGetCTT:=TGet():New( 011,400,{|u| If(PCount()>0,cGetCTT :=u,cGetCTT )},oGrp3,028,008,'999999',{|| ValCons(@cGetSED)},CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SED","cGetCodCons" ,,)
	
	//Instaciamento
	oBrwNFE := FWMBrowse():New()
	oBrwNFE:SetMenuDef("SASP091") 
	
	//Disabilita a tela de detalhes
	oBrwNFE:disableDetails()
	
	//tabela que ser� utilizadaadmin
	oBrwNFE:SetAlias( "ZBC" )
	if Valtype(_xp_cNrImp) <> 'U'
		SET FILTER TO (ZBC_NRIMP = _xp_cNrImp)
	EndIf 
	
	//Titulo
	oBrwNFE:SetDescription( "Itens Ocorr�ncia" )
	
	oBrwNFE:AddLegend("ZBC_LIBBLO=='L'", "BR_VERDE"		,"Liberado")
	oBrwNFE:AddLegend("ZBC_LIBBLO=='B'", "BR_VERMELHO"	,"Bloqueado")
	oBrwNFE:AddLegend("ZBC_LIBBLO=='P'", "BR_AMARELO"	,"Pendente")
	oBrwNFE:AddLegend("ZBC_LIBBLO=='R'", "BR_LARANJA"	,"Rejeitado")
	
	//ativa
	oBrwNFE:Activate(oDlgNFE)
	
	oBrwNFE:Refresh()
	
	oDlgNFE:Activate(,,,.T.)
Return


Static Function MenuDef()
	Private aRotinaNFE := {}
		
	ADD OPTION aRotinaNFE	TITLE 'Justificar'	 			ACTION 'U_SASP091A()' OPERATION 3 ACCESS 0
	if cteBloq(_xp_cNrImp)
		ADD OPTION aRotinaNFE	TITLE 'Solicitar Anula��o'	 	ACTION 'U_SASP091B()' OPERATION 3 ACCESS 0
	EndIf
Return aRotinaNFE

Static Function AtualTela()
	Local cAlias	:= GetNextAlias()
	Local aArea		:= GetArea()
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	dbGoTop()
	dbSeek(xFilial("ZB8")+_xp_cNrImp)
	
	_cSeqImp 		:= ZB8->ZB8_NRIMP
	_cChvCTE 		:= ZB8->ZB8_CTE
	if !(Empty(AllTrim(ZB8->ZB8_CDREM)))
		_cRemetente 	:= AllTrim(ZB8->ZB8_CDREM)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDREM,"SA2->A2_NREDUZ")
	Else
		_cRemetente 		:= ""
	EndIf
	if !(Empty(AllTrim(ZB8->ZB8_EMISDF)))
		_cTransportador	:= AllTrim(ZB8->ZB8_EMISDF)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_EMISDF,"SA2->A2_NREDUZ")
	Else
		_cTransportador 		:= ""
	EndIf
	if !(Empty(AllTrim(ZB8->ZB8_CDDEST)))
		_cDestinatario 	:= AllTrim(ZB8->ZB8_CDDEST)	+ " - " + POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDDEST,"SA1->A1_NREDUZ")
	Else
		_cDestinatario 		:= ""
	EndIf
	if !(Empty(AllTrim(ZB8->ZB8_TOMADO)))
		_cTomador 		:= AllTrim(ZB8->ZB8_TOMADO)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_TOMADO,"SA2->A2_NREDUZ")
	Else
		_cTomador 		:= ""
	EndIf
	
	_cCTE 			:= AllTrim(ZB8->ZB8_NRDF)+"/"+ZB8->ZB8_SERDF
	_cValFrete 		:= AllTrim(Transform(ZB8->ZB8_VLDF	,PesqPict("ZB8"	, "ZB8_VLDF")))
	_cPeso 			:= AllTrim(Transform(ZB8->ZB8_PREAL	,PesqPict("ZB8"	, "ZB8_PREAL")))
	_cVolume 		:= AllTrim(Transform(ZB8->ZB8_QTVOL,PesqPict("ZB8"	, "ZB8_QTVOL")))
	_cEmissao 		:= DtoC(ZB8->ZB8_DTEMIS)

	ZB8->(dbCloseArea())
	RestArea(aArea)
	ProcessMessage()
Return

Static Function CssbtnImg(cImg,cCor)
	Local cRet	:= " QPushButton{ background-image: url(rpo:"+cImg+")"+;
	" ;border-style:solid"+;
	" ;border-width:5px"+;
	" ;background-repeat: none; margin: 1px "+;
	" ;background-color: "+cCor+""+;
	" ;border-radius: 6px"+;
	" ;font-weight: bold;font-size: 12px"+;
	" ;color: rgb(255,255,255) "+;
	"}"+;
	"QPushButton:hover { "+;
	" ;background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop:0 "+cCor+", stop:0.5 "+cCor+", stop:1 white)"+;
	"}"
Return cRet


User function SASP091A()
	Local bPodeEditar := .T.
	Local bTemBloq := .T.
	Local aParamBox := {}
	Local aRet :={}
	
	Local cOcorre := ""
	Local cDescOco := ""
	
	Local cDescJus
	Local cSeqImp := ZBC->ZBC_NRIMP
	
	if ZBC->ZBC_LIBBLO == 'B'
		aAdd(aParamBox,{1,"Justificativa"	,Space(6),"","","ZBD","",10,.T.}) // Tipo caractere
		aAdd(aParamBox,{1,"Observa��o"		,Space(250),"","","","",120,.T.}) // Tipo caractere
		
		dbSelectArea("ZB3")
		dbSetOrder(1)
		dbSeek(xFilial("ZB3")+ZBC->ZBC_OCORR)
		
		cOcorre 	:= ZB3->ZB3_COD
		cDescOco 	:= ZB3->ZB3_DESC
		bPodeEditar := ZB3->ZB3_JUST == 'S' 
		bTemBloq 	:= temOcorrBloq(ZBC->ZBC_NRIMP)
		cAprov 		:= ZB3->ZB3_APROV
		
		ZB3->(dbCloseArea())
		
		If bPodeEditar 
			If bTemBloq
				MsgAlert("j� existe ocorr�ncia bloqueada para este CTE!")
			Else
				If ParamBox(aParamBox,"Justificativa",@aRet)
					cDescJus := POSICIONE("ZBD", 1, xFilial("ZBD")+aRet[1], "ZBD->ZBD_DESC")
					
					dbSelectArea("ZBC")
					dbSetOrder(1)
					If dbSeek(xFilial("ZBC")+cSeqImp+cOcorre)
						RecLock("ZBC", .F.)
							ZBC->ZBC_TPJUST := aRet[1]
							ZBC->ZBC_JUST := cDescJus
							ZBC->ZBC_OBS := aRet[2]
							ZBC->ZBC_LIBBLO := 'P'
						MsUnLock()
					EndIf
					
					if tudoJustificado(cSeqImp)
						ZBC->(dbGoTop())
						dbSeek(xFilial("ZBC")+cSeqImp)
						While !(ZBC->(EOF())) .AND. ZBC->ZBC_NRIMP == cSeqImp
							//criaVisto(cOcorre,cDescOco, cAprov, aRet[1], cDescJus, aRet[2], 'N')
							criaVisto(	ZBC->ZBC_OCORR,;
										POSICIONE("ZB3", 1, xFilial("ZB3")+ZBC->ZBC_OCORR, "ZB3->ZB3_DESC"), ;
										POSICIONE("ZB3", 1, xFilial("ZB3")+ZBC->ZBC_OCORR, "ZB3->ZB3_APROV"), ;
										ZBC->ZBC_TPJUST, ;
										ZBC->ZBC_JUST, ;
										ZBC->ZBC_OBS;
										, 'N')
							ZBC->(dbSkip())
						EndDo
						
						dbSelectArea("ZB8")
						dbSetOrder(1)
						dbSeek(xFilial("ZB8")+cSeqImp)
						
						RecLock("ZB8", .F.)
							ZB8->ZB8_STATUS := 'AV'
						MsUnLock()
						
						ZB8->(dbCloseArea())
						
						U_EmailGF(cSeqImp, 'O')
					EndIf
				EndIf
			EndIf
		Else
			MsgAlert("Esta ocorr�ncia n�o permite justificativa!")
		EndIf
	Else
		If ZBC->ZBC_LIBBLO == 'P'
			MsgInfo("Esta ocorr�ncia est� pendente e n�o permite mais justificativa!")
		ElseIf ZBC->ZBC_LIBBLO == 'A'
			MsgInfo("Esta ocorr�ncia est� aprovada e n�o permite mais justificativa!")
		ElseIf ZBC->ZBC_LIBBLO == 'R'
			MsgInfo("Esta ocorr�ncia est� rejeitada e n�o permite mais justificativa!")
		EndIf
	EndIf
Return

User function SASP091B()
	Local xArea := getArea()
	Local cAprov := ""
	Local cCodAnula := '3'
	Local i := 0
	Local cAnuOco := "999999"
	Local cAnuJus := "999999"
	Local cDesOco := POSICIONE("ZB3", 1, xFilial("ZB3")+cAnuOco, "ZB3->ZB3_DESC")
	Local cDesJus := POSICIONE("ZBD", 1, xFilial("ZBD")+cAnuJus, "ZBD->ZBD_DESC")
	
	Local bPodeEditar 	:= .T.
	Local bTemBloq 		:= .T.
	Local aParamBox 	:= {}
	Local aRet 			:={}
	
	Local cSeqImp := ZBC->ZBC_NRIMP
	
	Local cMsg := ""
	
	dbSelectArea("ZBE")
	dbSetOrder(1)
	If dbSeek(xFilial("ZBE")+cSeqImp)
		MsgAlert("J� Existe solicita��o de anula��o para este CTE")
		return
	EndIF
	
	ZBE->(dbCloseArea())
	
	If MsgYesNo("Deseja solicitar anula��o deste CTE?","Anula��o do CTE")
		if ZBC->ZBC_LIBBLO $ 'B_R'
			aAdd(aParamBox,{1,"Justificativa"	,cAnuJus,"","","ZBD",".F.",10,.T.}) // Tipo caractere
			aAdd(aParamBox,{1,"Observa��o"		,Space(250),"","","","",120,.T.}) // Tipo caractere
			
			dbSelectArea("ZB5")
			dbSetOrder(6)
			dbSeek(xFilial("ZB5")+cCodAnula)
			
			While !(ZB5->(EOF())) .AND. ZB5->ZB5_TIPO == cCodAnula
				cAprov += ZB5->ZB5_USER + ';'
				ZB5->(dbSkip())
			EndDo
			ZB5->(dbCloseArea())
			
			cAprov := SUBSTR(cAprov, 1, len(cAprov)-1)
			
			If bPodeEditar 
				If ParamBox(aParamBox,"Justificativa para anula��o",@aRet)
					criaVisto(cAnuOco, cDesOco, cAprov, aRet[1], cDesJus, aRet[2], 'S')
				EndIf
			Else
				MsgAlert("Esta ocorr�ncia n�o permite justificativa!")
			EndIf
		EndIf
	EndIf
	
	altStaCTE(cSeqImp, 'SA')
	
	RestArea(xArea)
	
	MsgInfo("Solicita��o de Anula��o realizada","Anula��o CTE")
Return


/*
User function SASP091C()
	MsgInfo("Implementar Fun��o Aprovadores","[SASP091] - Gest�o de Fretes")
Return

User function SASP091D()
	MsgInfo("Implementar Fun��o Ger. Fatura","[SASP091] - Gest�o de Fretes")
Return

User function SASP091E()
	MsgInfo("Implementar Fun��o Refaz Conf.","[SASP091] - Gest�o de Fretes")
Return
*/

Static Function criaVisto(cOcorre, cDescOco, cAprov, cJust, cDescJus, cOBS, cAnula)
	Local xArea := getArea()
	Local aTomador 		:= StrToKArr(_cTomador, 		'-')
	Local aDestinatario := StrToKArr(_cDestinatario, 	'-')
	Local aRemetente 	:= StrToKArr(_cRemetente, 		'-')
	Local aCTE 			:= StrToKArr(_cCTE, 			'/')
	Local aAprov 		:= {}
	
	aAprov := StrTokArr(AllTrim(cAprov),';')
	For i := 1 to len(aAprov) 
		RecLock("ZBE", .T.)
			ZBE->ZBE_FILIAL := xFilial("ZBE") 
			ZBE->ZBE_NRIMP 	:= _cSeqImp
			ZBE->ZBE_CTE 	:= _cChvCTE
			ZBE->ZBE_NRDF 	:= aCTE[1]
			ZBE->ZBE_SERDF 	:= aCTE[2]
			ZBE->ZBE_USER 	:= aAprov[i]
			ZBE->ZBE_STATUS := 'B'
			ZBE->ZBE_OCORR 	:= cOcorre
			ZBE->ZBE_DESOCO := cDescOco
			ZBE->ZBE_REMET 	:= Alltrim(aRemetente[1])
			ZBE->ZBE_DESREM := Alltrim(aRemetente[2])
			ZBE->ZBE_TOMADO := Alltrim(aTomador[1])
			ZBE->ZBE_DESTOM := Alltrim(aTomador[2])
			ZBE->ZBE_DEST 	:= Alltrim(aDestinatario[1])
			ZBE->ZBE_DESDES := Alltrim(aDestinatario[2])
			ZBE->ZBE_VALFRE := VAL(_cValFrete)
			ZBE->ZBE_PESO 	:= VAL(_cPeso)
			ZBE->ZBE_VOLUME	:= VAL(_cVolume)
			ZBE->ZBE_EMISS 	:= CtoD(_cEmissao)
			ZBE->ZBE_TPJUST	:= cJust
			ZBE->ZBE_JUST 	:= cDescJus
			ZBE->ZBE_OBS 	:= cOBS
			ZBE->ZBE_ALCADA := 'N'
			ZBE->ZBE_ANULA	:= cAnula
		MsUnLock()
	Next i
	
	RestArea(xArea)
Return

Static Function temOcorrBloq(cSeqImp)
	Local bRet := .F.
	Local aArea := getArea()
	
	dbSelectArea("ZBC")
	dbSetOrder(1)
	dbSeek(xFilial("ZBC")+cSeqImp)
	
	While !(ZBC->(EOF())) .AND. ZBC->ZBC_NRIMP == cSeqImp
		If ZBC->ZBC_LIBBLO == 'R'
			bRet := .T.
		 	exit
		EndIf
		
		ZBC->(dbSkip())
	EndDo
	
	ZBC->(dbCloseArea())
	
	RestArea(aArea)
Return bRet

Static function cteBloq(cSeqImp)
	Local bRet := .F.
	Local xArea := getArea()
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	If DbSeek(xFilial("ZB8")+cSeqImp)
		If ZB8->ZB8_STATUS == "BL"
			bRet := .T.
		EndIf
	EndIf
	
	ZB8->(dbCLoseArea())
	restArea(xArea)
Return bRet

Static Function altStaCTE(cSeqImp, cStatus)
	DbSelectArea("ZB8")
	dbSetOrder(1)
	If dbSeek(xFilial("ZB8") + cSeqImp)
		RecLock("ZB8", .F.)
			ZB8->ZB8_STATUS := cStatus
		MsUnLock()
	EndIf
Return

Static Function tudoJustificado(cSeqImp)
	Local bRet := .T.
	Local xArea := getArea()
	
	dbSelectArea("ZBC")
	dbSetOrder(1)
	dbSeek(xFilial("ZBC")+cSeqImp)
	
	While !(ZBC->(EOF())) .AND. ZBC->ZBC_NRIMP == cSeqImp
		If Empty(AllTrim(ZBC->ZBC_TPJUST))
			bRet := .F.
			exit
		EndIf
		ZBC->(dbSkip())
	EndDo
	ZBC->(dbCloseArea())
	RestArea(xArea)
Return bRet