#include "protheus.ch"
#INCLUDE "topconn.ch"

User Function SASP096()
	Local _astru:={}
	Local aRet := {}
	Local aParamBox := {}
	
	Private _afields:={}
	Private cCodUse	:= RETCODUSR()
	Private oMark
	Private arotina := {}
	Private cCadastro
	Private cMark:=GetMark()
	private _cAlias
	Private _RecTRB
	
	Private _aCdUsWF
	
	Private _cMultDoc := ""
	
	aAdd(aParamBox,{1,"Transportadora De",Space(15),"","","SA2","",0,.F.}) // Tipo caractere
	aAdd(aParamBox,{1,"Transportadora At�",Replicate('Z',(15)),"","","SA2","",0,.F.}) // Tipo caractere
	
	aAdd(aParamBox,{1,"Emiss�o De"  ,Ctod(Space(8)),"","","","",50,.F.}) // Tipo data
	aAdd(aParamBox,{1,"Emiss�o At�"  ,dDataBase,"","","","",50,.F.}) // Tipo data 
	
	aAdd(aParamBox,{1,"Remetente De",Space(15),"","","SA2","",0,.F.}) // Tipo caractere
	aAdd(aParamBox,{1,"Remetente At�",Replicate('Z',(15)),"","","SA2","",0,.F.}) // Tipo caractere
	
	aAdd(aParamBox,{1,"Destinat�rio De",Space(15),"","","SA2","",0,.F.}) // Tipo caractere
	aAdd(aParamBox,{1,"Destinat�rio At�",Replicate('Z',(15)),"","","SA2","",0,.F.}) // Tipo caractere

	AADD(aRotina,{"Gerar Doc. Entrada" 		,"u_makeDocEnt()" ,0,4})

	If ParamBox(aParamBox,"Par�metros Doc. Entrada",@aRet)
	
		cCadastro := "Gera��o de Doc. Entrada CTE"
	
		//Estrutura da tabela temporaria
		AADD(_astru,{"ZZ_OK"    	,"C",02,0})
		AADD(_astru,{"ZZ_FILIAL" 	,"C",06,0})
		AADD(_astru,{"ZZ_NRIMP"	 	,"C",16,0})
		AADD(_astru,{"ZZ_MODAL"	 	,"C",02,0})
		AADD(_astru,{"ZZ_VLTELA"	,"C",20,0})
		AADD(_astru,{"ZZ_NRDF"	 	,"C",16,0})
		AADD(_astru,{"ZZ_SERDF"	 	,"C",05,0})
		AADD(_astru,{"ZZ_CTE"	 	,"C",44,0})
		AADD(_astru,{"ZZ_DTEMIS" 	,"C",10,0})
		AADD(_astru,{"ZZ_TOMADO" 	,"C",06,0})
		AADD(_astru,{"ZZ_LOJA" 		,"C",02,0})
		AADD(_astru,{"ZZ_VLDF" 		,"N",12,2})
		
		AADD(_astru,{"ZZ_NATURE" 	,"C",10,0})
		AADD(_astru,{"ZZ_CC" 		,"C",09,0})
		AADD(_astru,{"ZZ_CODPAG" 	,"C",03,0})
		AADD(_astru,{"ZZ_TES" 		,"C",03,0})
		
		//----------------------------------------------------------
		// cria a tabela tempor�ria
		//----------------------------------------------------------
		_carq:="T_"+Criatrab(,.F.)
		MsCreate(_carq,_astru,"DBFCDX")
		
		//----------------------------------------------------------
		//atribui a tabela tempor�ria ao alias TRB
		//----------------------------------------------------------
		if Select("TRB")>0
			dbSelectArea("TRB")
			TRB->(dbCLoseArea())
		EndIf
		dbUseArea(.T.,"DBFCDX",_cARq,"TRB",.T.,.F.)
	
		//----------------------------------------------------------
		// selecionando registros. 					            
		//----------------------------------------------------------
		cQuery:=" SELECT ZB8_FILDOC, "
		cQuery+=" ZB8_FILTOM, "
		cQuery+=" ZB8_NRIMP, "
		cQuery+=" ZB8_NRDF, "
		cQuery+=" ZB8_SERDF, "
		cQuery+=" ZB8_MODAL, "
		cQuery+=" ZB8_CTE, "
		cQuery+=" ZB8_VLDF, "
		cQuery+=" ZB8_EMISDF, "
		cQuery+=" ZB8_NRIMP, "
		cQuery+=" ZB8_NATURE, "
		
		cQuery+=" ZB8_CC, "
		cQuery+=" ZB8_TES, "
		cQuery+=" ZB8_CODPAG, "
		cQuery+=" ZB8_DTEMIS "
		cQuery+=" FROM " + RetSqlName("ZB8") + " ZB8 (nolock)"
		cQuery+=" WHERE 1=1 "
		cQuery+=" AND ZB8.D_E_L_E_T_ = '' "
		cQuery+=" AND ZB8.ZB8_FILIAL = '"+xFilial("ZB8")+"' "
		cQuery+=" AND ZB8.ZB8_STATUS = 'AA' "
		cQuery+=" AND ZB8.ZB8_TITULO = '" + Space(TamSx3("ZB8_TITULO")[1]) + "' "
		//------------------------------------------------------------------------------------
		cQuery+=" AND SUBSTRING(ZB8.ZB8_EMISDF,1,6) >= '" + aRet[1] + "' "
		cQuery+=" AND SUBSTRING(ZB8.ZB8_EMISDF,1,6) <= '" + aRet[2] + "' "
		cQuery+=" AND ZB8.ZB8_DTEMIS >= '" + DtoS(aRet[3]) + "' "
		cQuery+=" AND ZB8.ZB8_DTEMIS <= '" + DtoS(aRet[4]) + "' "
		cQuery+=" AND ZB8.ZB8_CDREM >= '" + aRet[5] + "' "
		cQuery+=" AND ZB8.ZB8_CDREM <= '" + aRet[6] + "' "
		cQuery+=" AND ZB8.ZB8_CDDEST >= '" + aRet[7] + "' "
		cQuery+=" AND ZB8.ZB8_CDDEST <= '" + aRet[8] + "' "
		//------------------------------------------------------------------------------------
		
		IF SELECT("TX")>0
			TX->(dbCloseArea())
		Endif
	
		TCQUERY cQuery NEW ALIAS TX
	
		dbSelectArea("TX")
		TX->(dbGotop())
	
		While TX->(!Eof())
	
			RecLock("TRB",.T.)
				TRB->ZZ_FILIAL		:=  TX->ZB8_FILTOM//TX->ZB8_FILDOC
				TRB->ZZ_NRIMP 		:=  TX->ZB8_NRIMP
				TRB->ZZ_MODAL		:= 	TX->ZB8_MODAL
				TRB->ZZ_VLTELA 		:=  RTrim(Transform(TX->ZB8_VLDF, PesqPict("ZB8", "ZB8_VLDF")))
				TRB->ZZ_NRDF		:=  TX->ZB8_NRDF
				TRB->ZZ_SERDF		:=  TX->ZB8_SERDF
				TRB->ZZ_CTE     	:= 	TX->ZB8_CTE
				TRB->ZZ_DTEMIS		:= 	DtoC(StoD(TX->ZB8_DTEMIS))
				TRB->ZZ_TOMADO     	:= 	SUBS(TX->ZB8_EMISDF,1,6)
				TRB->ZZ_LOJA     	:= 	SUBS(TX->ZB8_EMISDF,7,2)
				TRB->ZZ_VLDF 		:=  TX->ZB8_VLDF
				
				TRB->ZZ_NATURE 		:=  TX->ZB8_NATURE
				TRB->ZZ_TES 		:=  TX->ZB8_TES
				TRB->ZZ_CODPAG 		:=  TX->ZB8_CODPAG
				TRB->ZZ_CC	 		:=  TX->ZB8_CC
			TRB->(MSUNLOCK())
	
			TX->(dbSkip())
		EndDo
	
		TX->(dbclosearea())
	
		//----------------------------------------------------------
		//Colunas do browse [verificar necessidade de novos campos do browser]
		//----------------------------------------------------------
		AADD(_afields,{"ZZ_OK"    		,"",""            		})
		AADD(_afields,{"ZZ_FILIAL" 		,"","Filial"	   		})
		AADD(_afields,{"ZZ_NRIMP" 		,"","Seq Import"   		})
		AADD(_afields,{"ZZ_MODAL" 		,"","Modal"	   			})
		AADD(_afields,{"ZZ_VLTELA" 		,"","Val Frete"	   		})
		AADD(_afields,{"ZZ_NRDF" 		,"","CTE"				})
		AADD(_afields,{"ZZ_SERDF"		,"","Serie"	  			})
		AADD(_afields,{"ZZ_CTE"			,"","Chave CTE"		  	})
		AADD(_afields,{"ZZ_DTEMIS"		,"","Data Emissao" 		})
		AADD(_afields,{"ZZ_TOMADO"		,"","Fornecedor" 		})
		AADD(_afields,{"ZZ_LOJA"		,"","Loja" 				})
		
		AADD(_afields,{"ZZ_NATURE"		,"","Natureza"			})
		AADD(_afields,{"ZZ_TES"			,"","TES" 				})
		AADD(_afields,{"ZZ_CODPAG"		,"","Cond. Pagto"		})
		AADD(_afields,{"ZZ_CC"			,"","Centro de Custo"	})
	
		DbSelectArea("TRB")
		DbGotop()
	
		MarkBrow( 'TRB', 'ZZ_OK',,_afields,.F., cMark,'u_MkAllDocEnt()',,,,'u_MkDocEnt()',{|| u_MkAllDocEnt()},,,,,,,.F.)
	
		TRB->(dbCloseArea())
		//----------------------------------------------------------
		//apaga a tabela tempor�rio
		//----------------------------------------------------------
		MsErase(_carq+GetDBExtension(),,"DBFCDX")
	Else
		Return
	EndIf

Return()

User Function MkAllDocEnt()
	Local oMark := GetMarkBrow()

	dbSelectArea('TRB')
	TRB->(dbGotop())

	While !TRB->(Eof())
		u_MkDocEnt()
		TRB->(dbSkip())
	EndDo
	MarkBRefresh( )
	oMark:oBrowse:Gotop()

Return

User Function MkDocEnt()
	If IsMark( 'ZZ_OK', cMark )

		RecLock( 'TRB', .F. )
		Replace ZZ_OK With Space(2)
		TRB->(MsUnLock())
	Else
		RecLock( 'TRB', .F. )
		Replace ZZ_OK With cMark
		TRB->(MsUnLock())
	EndIf

Return

User Function makeDocEnt()
	Local cMsg	:= ""
	Local nTotal:= 0
	Local nCont	:= 0

	dbSelectArea("TRB")
	TRB->(dbGotop())
	While !TRB->(Eof())
		IF !Empty(Alltrim(TRB->ZZ_OK))
			nTotal	+= TRB->ZZ_VLDF
			nCont ++
		ENDIF
		TRB->(dbSkip())
	ENDDO
	
	If nCont > 1
		cMsg	:= "CONFIRMA GERA��O DE ("+Alltrim(Str(nCont))+") DOCUMENTOS DE ENTRADA ? "+CHR(13)+CHR(10)
	Else
		cMsg	:= "CONFIRMA GERA��O DE ("+Alltrim(Str(nCont))+") DOCUMENTO DE ENTRADA ? "+CHR(13)+CHR(10)
	EndIf
	cMsg	+= "TOTAL >>> R$ "+Alltrim(Transform(nTotal,"@E 999,999,999,999.99"))+"<<<"+CHR(13)+CHR(10)
	cMsg	+= "("+Alltrim(Extenso(nTotal))+")"
	
	If MsgYesNo(cMsg,"Gerar documento de entrada")
		Processa( {|| u_createDocEnt(nCont) }, "Gerando documento de entrada..." )
	EndIf
	
	MarkBRefresh()
	oMark:oBrowse:Gotop()
	oMark:oBrowse:nat := 1
	oMark:oBrowse:Refresh(.T.)
Return

User function createDocEnt(nQtdReg)
	Local aArray:={}
	Local aTit :={}
	Local dData
	Local cFornece
	Local cLoja
	
	Local aCabec := {}
	Local aItens := {}
	Local aLinha := {}
	
	Local cProd := "" //POSICIONE("ZBA", 1, xFIlial("ZBA")+ZB8->ZB8_MODAL, "ZBA->ZBA_PROD")
	Local cUm 	:= "" //POSICIONE("SB1",1,xFilial("SB1")+cProd, "SB1->B1_UM")
	Local cTp 	:= "" //POSICIONE("SB1",1,xFilial("SB1")+cProd, "SB1->B1_TIPO")
	
	//Local cTES 		:= GetMV("SA_TESCTE")
	//Local cCP 		:= GetMV("SA_CPCTE")
	//Local cNatureza := GetMV("SA_NATCTE")
	//Local cCusto	:= GetMV("SA_CCCTE")
	
	Local cFornece := ""
	Local cLoja := ""
	Local cUF 	:= ""
	
	//Local cDoc := 'GF' + cSeq
	
	Private lMsErroAuto := .F.
	
	Begin Transaction
	
	dbSelectArea("TRB")
	TRB->(dbGotop())
	ProcRegua(nQtdReg)
	While !TRB->(Eof())
		IF !Empty(Alltrim(TRB->ZZ_OK))
			IncProc("Gerando documento:  " + AllTrim(TRB->ZZ_NRDF) + "/" + AllTrim(TRB->ZZ_SERDF))
			
			cProd 	:= POSICIONE("ZBA", 1, xFIlial("ZBA")+TRB->ZZ_MODAL, "ZBA->ZBA_PROD")
			cUm 	:= POSICIONE("SB1",1,xFilial("SB1")+cProd, "SB1->B1_UM")
			cTp 	:= POSICIONE("SB1",1,xFilial("SB1")+cProd, "SB1->B1_TIPO")
			
			cFornece 	:= SUBS(TRB->ZZ_TOMADO,1,6)
			cLoja 		:= SUBS(TRB->ZZ_TOMADO,7,2)
			cUF 		:= POSICIONE("SA2",1,xFilial("SA2")+cFornece+cLoja, "SA2->A2_EST")
			
			aCabec := {}
			aItens := {}
			aLinha := {}
			
			Aadd(aCabec,{"F1_FILIAL"	,TRB->ZZ_FILIAL				,Nil})
			Aadd(aCabec,{"F1_DOC"		,TRB->ZZ_NRDF 				,Nil})
			Aadd(aCabec,{"F1_SERIE"		,TRB->ZZ_SERDF				,Nil})
			Aadd(aCabec,{"F1_FORNECE"	,cFornece					,Nil})
			Aadd(aCabec,{"F1_LOJA"		,cLoja						,Nil})
			Aadd(aCabec,{"F1_COND"		,TRB->ZZ_CODPAG				,Nil})
			Aadd(aCabec,{"F1_EMISSAO"	,CtoD(TRB->ZZ_DTEMIS)		,Nil})
			Aadd(aCabec,{"F1_FORMUL"	,"N"               			,Nil})
			Aadd(aCabec,{"F1_ESPECIE"	,"CTE"               		,Nil})
			Aadd(aCabec,{"F1_TIPO"		,"N"               			,Nil})
			Aadd(aCabec,{"F1_DTDIGIT"	,dDataBase         			,Nil})
			Aadd(aCabec,{"F1_EST"		,cUF         				,Nil})
			aadd(aCabec,{"E2_NATUREZ"	,TRB->ZZ_NATURE				,Nil})
			//Aadd(aLinha,{"D1_FILIAL"	,TRB->ZZ_FILDOC			,Nil})
			//Aadd(aLinha,{"D1_DOC"		,""     					,Nil})
			//Aadd(aLinha,{"D1_ITEM"		,"001"						,Nil})
			Aadd(aLinha,{"D1_COD"		,cProd						,Nil})
			//Aadd(aLinha,{"D1_UM"        ,cUm          				,Nil})
			Aadd(aLinha,{"D1_QUANT"     ,1          				,Nil})
			Aadd(aLinha,{"D1_VUNIT"     ,TRB->ZZ_VLDF          		,Nil})
			Aadd(aLinha,{"D1_TOTAL"     ,TRB->ZZ_VLDF				,Nil})
			Aadd(aLinha,{"D1_TES"		,TRB->ZZ_TES				,Nil})
			//Aadd(aLinha,{"D1_FORNECE"	,SUBS(TRB->ZZ_CDREM,1,6)	,Nil})
			//Aadd(aLinha,{"D1_LOJA"		,SUBS(TRB->ZZ_CDREM,7,2)	,Nil})
			//Aadd(aLinha,{"D1_LOCAL"     ,"01"         				,Nil})
			//Aadd(aLinha,{"D1_EMISSAO"	,TRB->ZZ_DTEMIS			,Nil})
			//Aadd(aLinha,{"D1_FORMUL"	,"N"               			,Nil})
			//Aadd(aLinha,{"D1_CF"		,TRB->ZZ_CFOP				,Nil})
			Aadd(aLinha,{"D1_CC"		,TRB->ZZ_CC					,Nil})
			//Aadd(aCabec,{"D1_DTDIGIT"	,dDataBase         			,Nil})
			//Aadd(aCabec,{"D1_TP"		,cTp	         			,Nil})
			//Aadd(aLinha,{"AUTDELETA"	,"N"               			,Nil})
			                
			Aadd(aItens,aLinha)
		
			U_SASF007(aCabec, aItens, TRB->ZZ_NRIMP, .F.)
			
			If lMsErroAuto
				MostraErro()
			Else
				dbSelectArea("ZB8")
				dbSetOrder(1)
				If dbSeek(xFilial("ZB8")+TRB->ZZ_NRIMP)
					_cMultDoc += + AllTrim(TRB->ZZ_NRDF) + "/" + AllTrim(TRB->ZZ_SERDF) + CRLF
					
					RecLock("ZB8", .F.)
						ZB8->ZB8_TITULO := TRB->ZZ_NRDF
						ZB8->ZB8_STATUS := "DE"
					MsUnLock()
				EndIf
				ZB8->(dbCloseArea())
				RecLock("TRB", .F.)
					TRB->(dbDelete())
				MsUnLock()
			Endif
		EndIf
		
		TRB->(dbSkip())
	ENDDO

	End Transaction	
	
	MsgInfo("Doc. Entrada gerados com sucesso:" + CRLF + _cMultDoc)

	MarkBRefresh()
	oMark:oBrowse:Gotop()
	oMark:oBrowse:nat := 1
	oMark:oBrowse:Refresh(.T.)

Return