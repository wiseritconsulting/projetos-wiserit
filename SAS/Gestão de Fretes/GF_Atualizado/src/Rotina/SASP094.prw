#Include 'Protheus.ch'

User Function SASP094()
	Private oMark 
	Private cMark := GetMark()
	Private aRotina := {}
	Private aCores := {}
	Private cCadastro
	Private cCodUsr := RetCodUsr()
	Private cFiltro := "ZBF_USER = '" + cCodUsr + "' AND ZBF_STATUS = 'B'"
	
	cCadastro := "Aprova��o CTE"
	
	AADD(aRotina,{"Aprovar" 		,"u_AprZBF()" 	,0,4})
	AADD(aRotina,{"Rejeitar" 		,"u_ReprZBF()" 	,0,4})
	AADD(aRotina,{"Legenda" 		,"u_LegZBF()" 	,0,4})
	
	AADD(aCores, { 'ZBF->ZBF_TPAPRO ==   "1"'	    , 'BR_AZUL'			})
	AADD(aCores, { 'ZBF->ZBF_TPAPRO ==   "2"'    	, 'BR_LARANJA'		})
	
	MarkBrow( 'ZBF', 'ZBF_OK',,,.F., cMark,'u_MkAllAP()',,,,'u_MkAP()',{|| u_MkAllAP()},cFiltro,,aCores,,,,.F.)
Return

User Function MkAllAP()
	Local oMark := GetMarkBrow()

	dbSelectArea('ZBF')
	ZBF->(dbGotop())

	While !ZBF->(Eof())
		u_MkAP()
		ZBF->(dbSkip())
	End
	MarkBRefresh( )
	oMark:oBrowse:Gotop()
Return

User Function MkAP()
	If IsMark( 'ZBF_OK', cMark )
		RecLock( 'ZBF', .F. )
		Replace ZBF_OK With Space(2)
		ZBF->(MsUnLock())
	Else
		RecLock( 'ZBF', .F. )
		Replace ZBF_OK With cMark
		ZBF->(MsUnLock())
	EndIf
Return

User Function AprZBF()
	If MsgYesNo("Deseja aprovar os CTEs marcados","Aprova��o")
		makeAprov('A')
	EndIf
Return

User Function ReprZBF()
	If MsgYesNo("Deseja reprovar os CTEs marcados","Aprova��o")
		makeAprov('R')
	EndIf
Return

Static Function makeAprov(cTipo)
	Local xArea := getArea()
	Local cSql := ""
	Local nQnt := 0
	Local cImport := ""
	Local cSubstantivo 	:= IIF(cTipo=='A', "Aprova��o"	, "Rejei��o")
	Local cInfinitivo	:= IIF(cTipo=='A', "Aprovar"	, "Rejeitar")
	Local cSeqImp := ""
	
	cSql += " 	SELECT X.ZBF_NRIMP IMP "
	cSql += " 	FROM  " + RetSqlName("ZBF") + " X "
	cSql += " 	WHERE X.D_E_L_E_T_ = '' "
	cSql += " 	AND X.ZBF_OK = '" + cMark + "' "
	cSql += " 	AND X.ZBF_STATUS = 'B' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cSeqImp := QRY->IMP
		dbSelectArea("ZBF")
		dbSetOrder(2)
		if dbSeek(xFilial("ZBF") + QRY->IMP + cCodUsr)
			nQnt++
			While !(ZBF->(EOF())) .AND. ZBF->ZBF_NRIMP == QRY->IMP .AND. IIF(cTipo=='A', ZBF->ZBF_USER == cCodUsr, 1==1)
				RecLock("ZBF", .F.)
					ZBF->ZBF_STATUS := cTipo
					ZBF->ZBF_DTAPRO := dDataBase
					ZBF->ZBF_HRAPRO := time()
				MsUnLock()
				
				ZBF->(dbSkip())
			EndDo
		EndIf
		QRY->(dbSkip())
		
		if !temAprov(cSeqImp)
			updCTEAprov(cSeqImp)
		EndIf
	EndDo
	QRY->(dbCloseArea())
	
	RestArea(xArea)
Return

USER Function LegZBF()
	Local aLegenda := 	{;
							{"BR_AZUL"			, "Grupo de Aprova��o por valor"},;
							{"BR_LARANJA" 		, "Grupo de Aprova��o R$/Kg"};
						}
	BrwLegenda(cCadastro, "Legenda", aLegenda)
Return .T.

Static Function temAprov(cSeqImp)
	Local bRet := .F.
	Local aArea := getArea()
	
	dbSelectArea("ZBF")
	dbSetOrder(1)
	dbSeek(xFilial("ZBF") + cSeqImp)
	
	While !(ZBF->(EOF())) .AND. ZBF->ZBF_NRIMP == cSeqImp
		if (ZBF->ZBF_STATUS == 'B')
			bRet := .T.
			exit
		EndIf
		ZBF->(dbSkip())
	EndDo
	
	ZBF->(dbCloseArea())

	RestArea(aArea)
Return bRet 

Static Function updCTEAprov(cSeqImp)
	Local bRet := .F.
	Local aArea := getArea()
	Local cTpAprov := ""
	
	dbSelectArea("ZBF")
	dbSetOrder(1)
	If dbSeek(xFilial("ZBF") + cSeqImp)
		cTpAprov := ZBF->ZBF_STATUS
	EndIf
	
	ZBF->(dbCloseArea())

	dbSelectArea("ZB8")
	dbSetOrder(1)
	if dbSeek(xFilial("ZB8")+cSeqImp)
		RecLock("ZB8", .F.)
			ZB8->ZB8_STATUS := IIF(cTpAprov=='A','AA','BL')
		MsUnLock()
	EndIf
	ZB8->(dbCloseArea())
	
	RestArea(aArea)
Return
