#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO007

Valida��o de Ocorr�ncia

VALOR DO FRETE DIFERE DO VALOR COTADO                                                           

Valida se o valor do documento de frete � o mesmo
informado na estimativa

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO007()
/*/
//-------------------------------------------------------------
User Function SASO007()
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()

	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If !(u_VldNFEMed((_cAlias)->ZB8_NRIMP, (_cAlias)->ZB8_FILDOC, (_cAlias)->ZB8_VLDF))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO007", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return 
