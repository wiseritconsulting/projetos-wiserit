#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO003

Valida��o de Ocorr�ncia

NF PERTENCE A OUTRO CTE                                              

Valida se j� existe NFE em outro CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO003()
/*/
//-------------------------------------------------------------
User Function SASO003()
	Local cSql := u_getCTEbyNFE()
	Local _cAlias := GetNextAlias()
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If u_getNFE((_cAlias)->ZB9_DANFE, (_cAlias)->ZB8_NRIMP)
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO003", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return