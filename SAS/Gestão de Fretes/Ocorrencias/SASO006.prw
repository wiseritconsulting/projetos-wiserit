#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO006

Valida��o de Ocorr�ncia

NOTA FISCAL NAO ENCONTRADA                                                                            

Valida se a NF informada est� na base de Dados
nenhuma pendencia no CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO006()
/*/
//-------------------------------------------------------------
User Function SASO006()
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()

	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If !(u_existNFE((_cAlias)->ZB8_FILDOC, (_cAlias)->ZB8_NRIMP))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO006", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return 
