#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO005

Valida��o de Ocorr�ncia

DESTINATARIO INCONSISTENTE                                                                           

Valida se o destinat�rio est� correto, ou seja, se n�o exite
nenhuma pendencia no CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO005()
/*/
//-------------------------------------------------------------
User Function SASO005()
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()

	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If Empty(Alltrim((_cAlias)->ZB8_CDDEST))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO005", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return 
