#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO009

Valida��o de Ocorr�ncia

TRANSPORTADORA N�O CADASTRADA                                            

Valida se o remetente est� correto, ou seja, se n�o exite
nenhuma pendencia no CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO009()
/*/
//-------------------------------------------------------------
User Function SASO009()
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()

	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If Empty(Alltrim((_cAlias)->ZB8_EMISDF))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO009", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return 
