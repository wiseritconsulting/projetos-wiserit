#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO012

Valida��o de Ocorr�ncia

REMETENTE DIFERENTE DA MEDI��O                                          

Valida se j� existe NFE em outro CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO012()
/*/
//-------------------------------------------------------------
User Function SASO012()
	Local cSql := u_getCTEbyNFE()
	Local _cAlias := GetNextAlias()
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If !(u_MedRemInc((_cAlias)->ZB9_DANFE, (_cAlias)->ZB8_CDREM))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO012", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return