#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO008

Valida��o de Ocorr�ncia

CTE N�O POSSUI NFE                                                    

Valida se o remetente est� correto, ou seja, se n�o exite
nenhuma pendencia no CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO008()
/*/
//-------------------------------------------------------------
User Function SASO008()
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()

	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If !u_temNFE((Alltrim((_cAlias)->ZB8_NRIMP)))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO008", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return 
