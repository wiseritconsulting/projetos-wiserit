#Include 'Protheus.ch'

User Function SASF006()
	Local xArea := getArea()
	Local cForm := ""
	
	public _p_cOcorr
	public _p_cDescOcorr
	public _p_cJust
	
	dbSelectArea("ZB2")
	dbGoTop()
	
	AutoGrLog( Replicate( "-", 77 ) )
	AutoGrLog( "Cerifica��o de Ocorr�ncias: " )
	
	While !(ZB2->(EOF()))
		bRetOcorr		:= .F.
		_p_cOcorr 		:= ZB2->ZB2_OCORR
		_p_cDescOcorr 	:= POSICIONE("ZB3",1,xFilial("ZB3")+ZB2->ZB2_OCORR, "ZB3->ZB3_DESC")
		_p_cJust 		:= POSICIONE("ZB3",1,xFilial("ZB3")+ZB2->ZB2_OCORR, "ZB3->ZB3_JUST")
		cForm  			:= ZB2->ZB2_FORM
		
		AutoGrLog("-- Verifica��o da Ocorr�ncia: " + _p_cOcorr + ' - ' + _p_cDescOcorr)
		&(Formula(cForm))
		
		ZB2->(dbSkip())
	EndDo
	
	ZB2->(dbCloseArea())
	
	RestArea(xArea)
Return

User Function confere()
	Local xArea := getArea()
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()
	Local _cStatus := 'AA'
	Local wArea 
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		makeAlcNoOc((_cAlias)->ZB8_NRIMP)
		_cStatus := getStatus((_cAlias)->ZB8_NRIMP)
		
		ConfStatus((_cAlias)->ZB8_NRIMP,_cStatus)
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
	
	changeIMporAA()
	
	RestArea(xArea)
Return

Static Function ConfStatus(cSeqImp,_cStatus)
	Local xArea := getArea()
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	dbSeek(xFilial("ZB8")+cSeqImp)

	RecLock("ZB8", .F.)
		ZB8->ZB8_CONF 	:= 'S'
		ZB8->ZB8_STATUS	:= _cStatus 
	MsUnLock()
	
	ZB8->(dbCloseArea())
	
	RestArea(xArea)
Return

User Function createZBC(cSeq, cFunc, cJust)
	Local yArea
	
	yArea := getArea()
	
	RecLock("ZBC", .T.)
		ZBC->ZBC_NRIMP  := cSeq
		ZBC->ZBC_OCORR  := _p_cOcorr
		ZBC->ZBC_DESOCO := _p_cDescOcorr
		ZBC->ZBC_LIBBLO := IIF(cJust == 'S', 'B', 'R')
		ZBC->ZBC_ROTINA := cFunc
		ZBC->ZBC_ALCADA := 'N'
	MsUnLock()
	
	RestArea(yArea)
Return

User Function getCTE()
	Local cSql := ""
	
	cSql += " SELECT * "
	cSql += " FROM " + RetSqlName("ZB8") + " ZB8 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB8.ZB8_FILIAL = '"+xFilial("ZB8")+"' "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	cSql += " AND ZB8.ZB8_CONF <> 'S' "
	cSql += "  "
	cSql += "  "
	cSql += "  "
Return cSql

User Function getCTEbyNFE()
	Local cSql := ""
	
	cSql += " SELECT * "
	cSql += " FROM " + RetSqlName("ZB8") + " ZB8, "
	cSql += "  " + RetSqlName("ZB9") + " ZB9 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB8.ZB8_FILIAL = '"+xFilial("ZB8")+"' "
	cSql += " AND ZB9.ZB9_FILIAL = '"+xFilial("ZB9")+"' "
	cSql += " AND ZB8.ZB8_FILIAL = ZB9.ZB9_FILIAL "
	cSql += " AND ZB8.ZB8_NRIMP = ZB9.ZB9_NRIMP "
	cSql += " AND ZB8.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB8.ZB8_CONF <> 'S' "
	cSql += " AND ZB8.ZB8_TPCTE = '0' "
	cSql += "  "
	cSql += "  "
Return cSql

User Function getNFE(cChvNFE, cSeqImp)
	Local cSql := ""
	Local _cAlias := GetNextAlias()
	
	cSql += " SELECT COUNT(*) QTD_NFE "
	cSql += " FROM " + RetSqlName("ZB9") + " ZB9 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.ZB9_FILIAL = '"+xFilial("ZB9")+"' "
	cSql += " AND ZB9.ZB9_DANFE = '"+cChvNFE+"' "
	cSql += " AND ZB9.ZB9_NRIMP < '"+cSeqImp+"' "
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If (_cAlias)->QTD_NFE >= 1
			Return .T.
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return .F.

User Function getDadosNFE(cChvNFE)
	Local cSql := ""
	Local _cAlias := GetNextAlias()
	
	cSql += " SELECT COUNT(*) QTD_NFE "
	cSql += " FROM " + RetSqlName("ZB9") + " ZB9 "
	cSql += " WHERE 1=1 "
	cSql += " AND ZB9.D_E_L_E_T_ = '' "
	cSql += " AND ZB9.ZB9_FILIAL = '"+xFilial("ZB9")+"' "
	cSql += " AND ZB9.ZB9_DANFE = '"+cChvNFE+"' "
	cSql += "  "
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		If (_cAlias)->QTD_NFE > 1
			Return .T.
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	(_cAlias)->( DBCloseArea() )
Return .F.

Static Function temOcorr(cSeqImp, cSqlStatus)
	Local cSql := ""
	Local bRet := .F.
	Local _cAlias := GetNextAlias()
	
	cSql += " SELECT COUNT(*) AS QNT "
	cSql += " FROM " + RetSqlName("ZBC") + " ZBC "
	cSql += " WHERE D_E_L_E_T_ = '' "
	cSql += " AND ZBC.ZBC_NRIMP = '" + cSeqImp + "' "
	cSql += cSqlStatus
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	If (_cAlias)->QNT > 0
		bRet := .T.
	EndIf
	
	(_cAlias)->(dbCLoseArea())
Return bRet

Static Function temAlcada(cSeqImp, cSqlStatus)
	Local cSql := ""
	Local bRet := .F.
	Local _cAlias := GetNextAlias()
	
	cSql += " SELECT COUNT(*) AS QNT "
	cSql += " FROM " + RetSqlName("ZBF") + " ZBF "
	cSql += " WHERE D_E_L_E_T_ = '' "
	cSql += " AND ZBF.ZBF_NRIMP = '" + cSeqImp + "' "
	cSql += cSqlStatus
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	If (_cAlias)->QNT > 0
		bRet := .T.
	EndIf
	
	(_cAlias)->(dbCLoseArea())
Return bRet

Static Function getStatus(cSeqImp) 
	Local cRet := 'AA'
	
	if temAlcada(cSeqImp, " AND ZBF.ZBF_STATUS = 'R' ")
		Return 'BL'
	EndIf
	
	if temAlcada(cSeqImp, " AND ZBF.ZBF_STATUS IN ('L', 'B') AND ZBF.ZBF_STATUS NOT IN ('R') ")
		Return 'AG'
	EndIf

	if temOcorr(cSeqImp, " AND ZBC.ZBC_LIBBLO = 'R' ")
		Return 'BL'
	EndIf
	
	if temOcorr(cSeqImp, " AND ZBC.ZBC_LIBBLO IN ('L', 'P', 'B') AND ZBC.ZBC_LIBBLO NOT IN ('R') ")
		Return 'PE'
	EndIf
	
Return cRet

Static Function makeAlcNoOc(cSeqImp)
	Local xArea := getArea() 
	
	Local cUF := ""
	
	Local cGrpFF := ""
	Local cGrpRK := "" 
	
	Local _aAprovFF := {}
	Local _aAprovRK := {}
	Local _aAprov := {}
	
	If !temOcorr(cSeqImp, "")
		cDest := POSICIONE("ZB8",1,xFilial("ZB8")+cSeqImp, "ZB8->ZB8_CDDEST")
		
		cUF := POSICIONE("SA1",1,xFilial("SA1")+cDest, "SA1->A1_EST")
		cGrpFF := POSICIONE("ZB1", 1, xFilial("ZB1") + cUF + '1',"ZB1->ZB1_GRUPO")
		cGrpRK := POSICIONE("ZB1", 1, xFilial("ZB1") + cUF + '2',"ZB1->ZB1_GRUPO")
		
		dbSelectArea("ZB8")
		dbGoTop()
		dbSetOrder(1)
		dbSeek(xFilial("ZB8")+cSeqImp)
		if ZB8->ZB8_VLDF > 0
			nValFrete := ZB8->ZB8_VLDF
		Else
			nValFrete := ZB8->ZB8_VLCARG
		EndIf 
		
		if ZB8->ZB8_PESOR > 0
			nRealKilo := nValFrete / ZB8->ZB8_PESOR
		Else
			nRealKilo := 0.01
		EndIf
		ZB8->(dbCloseArea())
		
		_aAprovFF := u_getAprov(cGrpFF, nValFrete, '1') 
		_aAprovRK := u_getAprov(cGrpRK, nRealKilo, '2')
		
		_aAprov :=  u_SomaArray(_aAprovFF,_aAprovRK)
		
		u_makeAprov(cSeqImp, _aAprov, .F.)
	EndIf
	
	RestArea(xArea)
Return

user function existNFE(cFildoc, cSeqImp)
	Local bRet := .T.
	Local aDanfe := getChvNFEdoCTE(cSeqImp)
	Local nTam := len(aDanfe)
	Local i := 0
	
	Local cSql := ""
	Local _cAlias := GetNextAlias()
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	dbSeek(xFilial("ZB8")+cSeqImp)
	if ZB8->ZB8_TPCTE <> '0'
		Return .T.
	EndIf
	ZB8->(dbCloseArea())
	
	For i := 1 to len(aDanfe)
		cSql := ""
		cSql += " SELECT COUNT(*) QTD_NFE "
		cSql += " FROM " + RetSqlName("SF2") + " SF2 "
		cSql += " WHERE 1=1 "
		cSql += " AND SF2.D_E_L_E_T_ = '' "
		cSql += " AND SF2.F2_CHVNFE = '"+AllTrim(aDanfe[i])+"' "
		cSql += "  "
		cSql += "  "
		
		DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
		
		dbSelectArea(_cAlias)
		(_cAlias)->( dbGoTop() )
		While !(_cAlias)->( Eof() )
			If (_cAlias)->QTD_NFE == 0
				Return .F.
			EndIf
			
			(_cAlias)->(dbSkip())
		Enddo
		(_cAlias)->( DBCloseArea() )
	Next i
Return .T.

Static Function getFilMed(cChvNFE, cFildoc)
	Local cSql := ""
	Local cChave := ""
	
	Local cFilTom := ""
	Local cFilRet := ""
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cChvNFE+"' "
	cSql += "  "
	cSql += "  "
	cSql += "  "
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	cFilTom := POSICIONE("ZZ2",8,cChave, "ZZ2->ZZ2_FILIAL")
	If Empty(AllTrim(cFilTom))
		cFilTom := POSICIONE("ZZ2",10,cChave, "ZZ2->ZZ2_FILIAL")
	EndIf
	
	If !(Empty(AllTrim(cFilTom)))
		cFilRet := cFilTom
	Else
		cFilRet := cFildoc
	EndIf
Return cFilRet

Static Function getChvNFEdoCTE(cSeqImp)
	Local aRet := {}
	Local xArea := getArea()
	
	dbSelectArea("ZB9")
	dbSetOrder(1)
	dbSeek(xFilial("ZB9")+cSeqImp)
	While !(ZB9->(EOF())) .AND. ZB9->ZB9_NRIMP == cSeqImp
		AADD(aRet, ZB9->ZB9_DANFE)
		ZB9->(dbSkip())
	EndDo
	ZB9->(dbCloseArea())
	
	RestArea(xArea)
Return aRet

User function VldNFEMed(cSeqImp, cFilDoc, nValEstFrete)
	Local bRet := .T.
	Local cSql := ""
	Local bSair := .F.
	Local nTotalFrete := 0
	
	Local cChaveZB4 := ""
	
	dbSelectArea("ZB9")
	ZB9->(dbSetOrder(1))
	if ZB9->(dbSeek(xFilial("ZB9")+cSeqImp))
		While !(ZB9->(EOF())) .AND. ZB9->ZB9_NRIMP == cSeqImp
			dbSelectArea("SF2")
			SF2->(dbSetOrder(10))
			if SF2->(dbSeek(cFilDoc+ZB9->ZB9_DANFE))	
				dbSelectArea("ZZ2")
				//If dbSeek(cFilDOc + SF2->SF2_NUM)  //FILIAL + NF + SERIE + CLIENT
				ZZ2->(dbSetOrder(8))
				ZZ2->(dbGoTop())
				If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
					//cChaveZB4 := SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
					cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
				Else
					ZZ2->(dbSetOrder(9))
					ZZ2->(dbGoTop())
					If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))
						cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
					Else
						ZZ2->(dbSetOrder(9))
						ZZ2->(dbGoTop())
						If ZZ2->(dbSeek(SF2->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)))	
							cChaveZB4 := ZZ2->(ZZ2_FILIAL + ZZ2_NUMERO + ZZ2_MEDICA + ZZ2_CLIENT + ZZ2_LOJA)
						EndIf
					EndIf
				EndIf
				
				ZZ2->(dbCloseArea())
				
				dbSelectArea("ZB4")
				ZB4->(dbSetOrder(2))
				if ZB4->(dbSeek(cChaveZB4))
					nTotalFrete += ZB4->ZB4_VALOR
				Else
					bRet := .F.
					bSair := .T.
					exit
				EndIf		
				
				ZB4->(dbCloseArea())
				//EndIf
				//ZZ2->(dbCloseArea())
			EndIf
			
			SF2->(dbCLoseArea())
			ZB9->(dbSkip())
			if bSair 
				exit
			EndIf
		EndDO
	EndIf
	ZB9->(dbCLoseArea())
	
	if Alltrim(GetMV("SA_TPVAL")) == 'P'
		If nTotalFrete > nValEstFrete * (1 + GetMV("SA_VALMAR")/100) .OR.  ;
		   nTotalFrete < nValEstFrete * (1 - GetMV("SA_VALMAR")/100)
			bRet := .F.
		EndIf
	Else
		If nTotalFrete > nValEstFrete + GetMV("SA_VALMAR") .OR. ;
		   nTotalFrete < nValEstFrete - GetMV("SA_VALMAR")
			bRet := .F.
		EndIf
	EndIf
	
Return bRet

User Function temNFE(cSeqImp)
	Local bRet := .T.
	
	dbSelectArea("ZB9")
	dbSetOrder(1)
	If !(dbSeek(xFilial("ZB9")+cSeqImp))
		bRet := .F.
	EndIf	
	ZB9->(dbCloseArea())
Return bRet

User Function MedTriang(cDanfe)
	Local bRet := .T.
	Local cSql := ""
	Local cChave := ""
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	dbSelectArea("ZZ2")
	ZZ2->(dbSetOrder(8))
	ZZ2->(dbGoTop())
	If ZZ2->(dbSeek(cChave))
		If ZZ2->ZZ2_TIPCTR = '2'
			If empty(ZZ2->ZZ2_PV03FL)
				bRet := .F.
			EndIf
		EndIf
	Else 
		ZZ2->(dbSetOrder(10))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChave))
			If ZZ2->ZZ2_TIPCTR = '2'
				If empty(ZZ2->ZZ2_PV01FL)
					bRet := .F.
				EndIf
			EndIf
		EndIf
	EndIf
	
	ZZ2->(dbCloseArea())
Return bRet

Static Function SM0CNPJbyCOD(cCod)
    Local aArea := GetArea()
    Local aAreaM0 := SM0->(GetArea())
    Local cFilRet := ""
    Local cCGC := POSICIONE("SA2",1,xFilial("SA2")+cCod, "SA2->A2_CGC")
     
    //Percorrendo o grupo de empresas
    SM0->(DbGoTop())
    While !SM0->(EoF())
        //Se o CNPJ for encontrado, atualiza a filial e finaliza
        If cCGC == SM0->M0_CGC
            cFilRet := SM0->M0_CODFIL
            Exit
        EndIf
         
        SM0->(DbSkip())
    EndDo

    RestArea(aAreaM0)
    RestArea(aArea)
Return cFilRet

User Function MedTomInc(cDanfe, cTomador)
	Local bRet := .T.
	Local cSql := ""
	Local cChave := ""
	
	Local cFilTom :=  SM0CNPJbyCOD(cTomador)
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	dbSelectArea("ZZ2")
	ZZ2->(dbSetOrder(8))
	ZZ2->(dbGoTop())
	If ZZ2->(dbSeek(cChave))
		If ZZ2->ZZ2_TIPCTR = '2' //Medi��o Triangular
			if AllTrim(ZZ2->ZZ2_PV01FL) <> AllTrim(cFilTom)
				bRet := .F.
			EndIf
		EndIf
	Else 
		ZZ2->(dbSetOrder(10))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChave))
			If ZZ2->ZZ2_TIPCTR = '2'
				if AllTrim(ZZ2->ZZ2_PV01FL) <> AllTrim(cFilTom)
					bRet := .F.
				EndIf
			EndIf
		EndIf
	EndIf
	
	ZZ2->(dbCloseArea())
Return bRet

User Function MedRemInc(cDanfe, cRemetente)
	Local bRet := .T.
	Local cSql := ""
	Local cChave := ""
	
	Local cFilRem :=  SM0CNPJbyCOD(cRemetente)
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	dbSelectArea("ZZ2")
	ZZ2->(dbSetOrder(8))
	ZZ2->(dbGoTop())
	If ZZ2->(dbSeek(cChave))
		If ZZ2->ZZ2_TIPCTR = '2'
			if AllTrim(ZZ2->ZZ2_PV03FL) <> AllTrim(cFilRem)
				bRet := .F.
			EndIf
		EndIf
	Else 
		ZZ2->(dbSetOrder(10))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChave))
			If ZZ2->ZZ2_TIPCTR = '2'
				if AllTrim(ZZ2->ZZ2_PV03FL) <> AllTrim(cFilRem)
					bRet := .F.
				EndIf
			EndIf
		EndIf
	EndIf
	
	ZZ2->(dbCloseArea())
Return bRet

Static function changeIMporAA()
	dbSelectArea("ZB8")
	dbSetOrder(1)
	dbGoTop()
	While !(ZB8->(EOF()))
		If ZB8->(ZB8_STATUS == 'IM')
			ZB8->ZB8_STATUS := 'AA'
		EndIf
		ZB8->(dbSkip())
	EndDo
	ZB8->(dbCloseArea())
Return

User Function MedDireta(cDanfe, cTomador, cRemetente)
	Local bRet := .T.
	Local cSql := ""
	Local cChave := ""
	
	Local cFilTom :=  SM0CNPJbyCOD(cTomador)
	Local cFilRem :=  SM0CNPJbyCOD(cRemetente)
	
	If cFilTom <> cFilRem
		Return .F.
	EndIf
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCLoseArea())
	
	dbSelectArea("ZZ2")
	ZZ2->(dbSetOrder(8))
	ZZ2->(dbGoTop())
	If ZZ2->(dbSeek(cChave))
		If ZZ2->ZZ2_TIPCTR = '1' //Medi��o Direta
			if AllTrim(ZZ2->ZZ2_PV01FL) <> AllTrim(cFilTom)
				bRet := .F.
			EndIf
		EndIf
	Else 
		ZZ2->(dbSetOrder(10))
		ZZ2->(dbGoTop())
		If ZZ2->(dbSeek(cChave))
			If ZZ2->ZZ2_TIPCTR = '1'
				if AllTrim(ZZ2->ZZ2_PV01FL) <> AllTrim(cFilTom)
					bRet := .F.
				EndIf
			EndIf
		EndIf
	EndIf
	
	ZZ2->(dbCloseArea())
Return bRet

User function temEstimativa(cDanfe)
	Local bRet := .F.
	Local cSql := ""
	Local cChave := ""
	
	cSql += " SELECT * "
	cSql += " FROM "
	cSql += RetSqlName("SF2") + " SF2 "
	cSql += " WHERE 1=1 "
	cSql += " AND SF2.D_E_L_E_T_ = '' "
	cSql += " AND SF2.F2_CHVNFE = '"+cDanfe+"' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		bRet := .T.
		cChave := QRY->(F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENT)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCloseArea())
	
	If !bRet
		Return bRet
	EndIf
	
	dbSelectArea("ZB4")
	dbSetOrder(2)
	If !(dbSeek(cChave))
		Return .F.
	EndIF
	ZB4->(dbCLoseArea())
	
Return bRet

User Function ComplSemNF(cSeqImp)
	Local cSql := ""
	Local aVet := {}
	Local bRet := .T.
	Local xArea := getArea()
	
	//bRet := !U_existNFE(cDanfe) .AND. cTpDF == '2'
	dbSelectArea("ZB8")
	dbSetOrder(1)
	If dbSeek(xFilial("ZB8") + cSeqImp)
		If ZB8->ZB8_TPCTE <> '0'
			dbSelectArea("ZB9")
			dbSetOrder(1)
			if !(dbSeek(xFilial("ZB9") + cSeqImp))
				Return .F.
			EndIf
		EndIf 
	EndIf
		
	RestArea(xArea)
	/*
	cSQL += " SELECT ZB9_TPDC "
	cSQL += " FROM " + RetSqlName("ZB9") + " ZB9 "
	cSQL += " WHERE ZB9.D_E_L_E_T_ = '' "
	cSQL += " AND ZB9_DANFE = '" + cDanfe + "' "
	cSQL += " ORDER BY ZB9_TPDC "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		AADD(aVet, QRY->ZB9_TPDC)
		QRY->(dbSkip())
	EndDo
	
	QRY->(dbCloseArea())
	
	If Len(aVet) > 1
		For i := 2 to len(aVet)
			If aVet[1] == aVet[i]
				bRet := .F.
				exit
			EndIf 
		Next i
	Else
		if aVet[1] <> '0'
			bRet := .F.
		EndIf
	EndIf
	*/
Return bRet