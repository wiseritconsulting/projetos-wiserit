#Include 'Protheus.ch'
#include "totvs.ch"

//------------------------------------------------------------
/*/{Protheus.doc} SASF004

Fun��o para mostrar a tela de processamento de toda a rotina

@type function
@author Sam Barros
@since 11/07/2016
@version 1.0
/*/
//------------------------------------------------------------
User Function SASF004()
	Processa( {|| U_SASFO04() }, "Aguarde...", "Importando hist�rico de frete...",.F.)
Return 

//------------------------------------------------------------
/*/{Protheus.doc} SASFO04

Fun��o para realizar a importa��o da estimativa de frete

@type function
@todo valida��es do arquivo
@author Sam Barros
@since 11/07/2016
@version 1.0
/*/
//------------------------------------------------------------
User Function SASFO04()
	Local cDir 		:= "C:\SAS\"
	Local cArq    	:= "historico.csv"
	Local cLinha  	:= ""
	Local bFirst   	:= .T.
	Local aCampos 	:= {}
	Local aDados  	:= {}
	
	Private cFile 	:= ""
 
	Private aErro 		:= {}
	Private _cExtens   	:= "Arquivo CSV ( *.CSV ) |*.csv|"
	Private aErrFile 	:= {}
	
	cFile := cGetFile( _cExtens, "Selecione o Arquivo",1,, .T., GETF_LOCALHARD+GETF_LOCALFLOPPY)//GETF_NETWORKDRIVE + GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_RETDIRECTORY )
 
	If !File(cFile)//!File(cDir + cArq)
		MsgStop("O arquivo " + cFile/*cDir+cArq*/ + " n�o foi encontrado. A importa��o ser� abortada!","[SAS] - ATENCAO")
		Return
	EndIf
 
	FT_FUSE(cFile)//FT_FUSE(cDir+cArq)
	ProcRegua(FT_FLASTREC())
	FT_FGOTOP()
	
	While !FT_FEOF()
		IncProc("Lendo arquivo CSV...")
 
		cLinha := FT_FREADLN()
 
		If bFirst
			aCampos := Separa(cLinha,";",.T.)
			For i := 1 to len(aCampos)
				aCamposDiv := Separa(aCampos[i],"-",.T.)
				aCampos[i] := AllTrim(aCamposDiv[2])
			Next i
			bFirst := .F.
		Else
			aDadosDiv := Separa(cLinha,";",.T.) 
			AADD(aDados, aDadosDiv)
		EndIf
 
		FT_FSKIP()
	EndDo
	
	_nTam := len(AllTrim(STR(len(aDados))))
	
	If temMunDuplicado(aDados)
		Return
	EndIf
	
	limpaHist()
 
	Begin Transaction
		ProcRegua(Len(aDados))
		For i:=1 to Len(aDados)
 
			IncProc("Importando hist�rico de fretes [Linha " + Alltrim(STRZERO(i,_nTam,0)) + "]")
 
			dbSelectArea("ZBB")
			dbSetOrder(1)
			dbGoTop()
			Reclock("ZBB",.T.)
			ZBB->ZBB_FILIAL	:= xFilial("ZBB")
			ZBB->ZBB_COD 	:= GETSXENUM("ZBB","ZBB_COD")
			ZBB->ZBB_CDMUOR := aDados[i][1] 
			ZBB->ZBB_MUNORI := POSICIONE("CC2",3,XFILIAL("CC2")+aDados[i][1],"CC2->CC2_MUN") 
			ZBB->ZBB_UFORI 	:= POSICIONE("CC2",3,XFILIAL("CC2")+aDados[i][1],"CC2->CC2_EST")
			ZBB->ZBB_CDMUDE := aDados[i][2] 
			ZBB->ZBB_MUNDES := POSICIONE("CC2",3,XFILIAL("CC2")+aDados[i][2],"CC2->CC2_MUN") 
			ZBB->ZBB_UFDES 	:= POSICIONE("CC2",3,XFILIAL("CC2")+aDados[i][2],"CC2->CC2_EST")
			ZBB->ZBB_QTDCTE := VAL(aDados[i][3])
			ZBB->ZBB_VALOR 	:= VAL(aDados[i][4])
			ZBB->ZBB_VALNF 	:= VAL(aDados[i][5])
			ZBB->ZBB_PESO 	:= VAL(aDados[i][6])
			ZBB->ZBB_MODAL 	:= aDados[i][7]
			ZBB->ZBB_FF 	:= VAL(aDados[i][4])/VAL(aDados[i][5]) //VAL(aDados[i][8])
			ZBB->ZBB_RK 	:= VAL(aDados[i][4])/VAL(aDados[i][6]) //VAL(aDados[i][9])
			
			ZBB->ZBB_DATA 	:= dDataBase
			
			ConfirmSx8()
			
			ZBB->(MsUnlock())
		Next i
	End Transaction
 
	FT_FUSE()
 
	ApMsgInfo("Importa��o do hist�rico de fretes conclu�da com sucesso!","[SAS] - SUCESSO")
     
Return

Static Function limpaHist()
	dbSelectArea("ZBB")
	dbGoTop()
	
	ProcRegua(ZBB->(RecCount()))
	
	While !(ZBB->(EOF()))
		IncProc("Removendo dados do Banco...")
		RecLock("ZBB", .F.)
			dbDelete()
		MsUnLock()
		
		ZBB->(dbSkip())
	EndDo
	
	ZBB->(dbCloseArea())
Return

Static Function temMunDuplicado(aDados)
	Local bRet := .F.
	Local i := 0
	Local j := 0
	Local nTam := len(aDados)
	Local bSair := .F.
	
	Local cMunOri := ""
	Local cMunDes := ""
	
	For i := 1 to nTam-1
		For j := i+1 to nTam 
			If aDados[i][1] == aDados[j][1] .AND. aDados[i][2] == aDados[j][2]
				bRet := .T.
				
				cMunOri := "["+aDados[i][1]+"] " + AllTrim(POSICIONE("CC2",3,XFILIAL("CC2")+aDados[i][1],"CC2->CC2_MUN"))
				cMunDes := "["+aDados[i][2]+"] " + AllTrim(POSICIONE("CC2",3,XFILIAL("CC2")+aDados[i][2],"CC2->CC2_MUN"))
				
				MsgInfo("Os munic�pios " + cMunOri + " e " + cMunDes + " est�o duplicados no arquivo","Duplica��o [Linha " + AllTrim(STR(i)) + "]")
				
				bSair := .T.
				exit
			EndIf    
		Next j
		
		If bSair
			exit
		EndIf
	next i 
Return bRet