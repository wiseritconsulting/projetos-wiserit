#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASV004

Valida��o do campo ZBA_SEQCTE no cadastro modal.
Verifica se j� existe sequencial cadastrado para o modal.
 
Usado no validador do campo. 
@author Sam Barros
@since 03/08/2016
@version 1.0
@return bRet, Valida��o de exist�ncia de registro
@example
U_SASV004()
/*/
//-------------------------------------------------------------------
User Function SASV004()
	Local bRet := .T.
	Local aArea := getArea()
	
	dbSelectArea("ZBA")
	dbSetOrder(3)
	If dbSeek(xFilial("ZBA")+M->ZBA_SEQCTE)
		MsgAlert("J� existe sequencial relacionado a este c�digo!")
		bRet := .F.
	EndIf
	
	ZBA->(dbCloseArea())
	
	RestArea(aArea)
Return bRet