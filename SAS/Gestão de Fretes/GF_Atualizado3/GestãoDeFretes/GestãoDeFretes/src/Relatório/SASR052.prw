#Include "Protheus.ch"
#Include "TopConn.ch"
#include "Rwmake.ch"
 
//******************************************************************//
//Autor: Elicio Junior							      Data:09/08/16 //
//******************************************************************//
//Fun��o: Relat�rio Registro de Aprova��es					   	    //
//******************************************************************//
User Function SASR052()
Local oReport

oReport:=ReportDef()
oReport:PrintDialog()


//------------------------------------------------//
//FUN��O: CRIACAO DA SE�AO E CELULAS			  //
//------------------------------------------------//
Static Function ReportDef()
Local oReport
Local oSection
Local oSection2
Local oSection3
Local cPerg     :="SASR052"

AjustaSX1(cPerg)
Pergunte(cPerg,.F.)

oReport := TReport():New("SASR052","Relat�rio Registro de Aprova��es","SASR052", {|oReport| ReportPrint(oReport)},"Relat�rio Registro de Aprova��es")
oReport:SetLandscape() //Paisagem

//������������������������������������������������������������������������Ŀ
//� Section 1			                                                  �
//��������������������������������������������������������������������������
oSection:= TRSection():New(oReport,OemToAnsi("Notas"),{"ZB8"})
TRCell():New(oSection,"ZB8_FILDOC" ," ","Filial"                   ,,50)
TRCell():New(oSection,"ZB8_CTE"    ," ","Num Cte" 			       ,,75)
TRCell():New(oSection,"TRANSP"     ," ","Transportador"            ,,30) 
TRCell():New(oSection,"QTDNFE"	   ," ","Qtde. Nfe"                ,,10) 
TRCell():New(oSection,"STATUS"	   ," ","Status"                   ,,40) 
TRCell():New(oSection,"CDGOCOR"    ," ","Ocorrencia"               ,,10) 
TRCell():New(oSection,"STAOCOR"    ," ","Status Ocorrencia"        ,,10) 
TRCell():New(oSection,"APROCOR"    ," ","Aprovadores Ocorrencia"   ,,40) 
TRCell():New(oSection,"POROCOR"    ," ","Ocorrencia Aprovada Por"  ,,40) 
TRCell():New(oSection,"DAPOCOR"    ," ","Data Aprova��o Ocorrencia",,10) 
TRCell():New(oSection,"APRALCA"    ," ","Aprova��o Al�ada"         ,,40) 
TRCell():New(oSection,"PORALCA"    ," ","Aprovado Por"             ,,40) 
TRCell():New(oSection,"STAALCA"    ," ","Status Aprova��o"         ,,10) 
TRCell():New(oSection,"DAPALCA"    ," ","Aprova��o"                ,,20) 
TRCell():New(oSection,"DATEMIS"    ," ","Emiss�o"                  ,,20) 


//������������������������������������������������������������������������Ŀ
//� Section 1			                                                  �
//��������������������������������������������������������������������������
oSection1:= TRSection():New(oReport,OemToAnsi("Resumo"),{"ZB8"})
//oSection2:SetTotalInLine(.F.)
TRCell():New(oSection1,"FILIAL"	," ","Filial"   	                                  ,,50)
TRCell():New(oSection1,"QTDCTE"	," ","Qtd Cte"                                        ,,10)
TRCell():New(oSection1,"STATUS"	," ","Status"       	                              ,,05)
TRCell():New(oSection1,"DESSTA"	," ","Descri��o"       	                              ,,30)
TRCell():New(oSection1,"QTDSTA"	," ","Qtde Status"     	                              ,,05)

return(oReport)

//------------------------------------------------//
//FUN��O: IMPRESSAO								  //
//------------------------------------------------//
Static Function ReportPrint(oReport)

Local oSection   := oReport:Section(1)
Local oSection1  := oReport:Section(2)

Private aZB8:={}    
Private aZBC:={}    
Private aZBE:={}    
Private aZBF:={}    
Private aDados:={}   
Private aStatus:={}   

Private aResfil:={}   
Private aRessta:={}   
Private aResumo:={}   
Private aResum1:={}   
 
Private iB8fild :=1
Private iB8ncte :=2
Private iB8nome :=3
Private iB8qtnf :=4
Private iB8stat :=5
Private iB8emis :=6

Private iB8cocor:=7
Private iB8clibb:=8

Private iB8euser:=9
Private iB8eapus:=10
Private iB8edtap:=11

Private iB8fuser:=12
Private iB8fapus:=13
Private iB8fstat:=14
Private iB8fdtap:=15

Private iBCocor:=1
Private iBClibb:=2

Private iBEuser:=1
Private iBEapus:=2
Private iBEdtap:=3

Private iBFuser:=1
Private iBFapus:=2
Private iBFstat:=3
Private iBFdtap:=4

aadd(aStatus,{'FG','FATURA GERADA              '})
aadd(aStatus,{'AG','AGUARDANDO APROVACAO       '})
aadd(aStatus,{'AA','APROVADO                   '})
aadd(aStatus,{'PE','PENDENTE                   '})
aadd(aStatus,{'AV','AGUARDANDO VISTO OCORRENCIA'})
aadd(aStatus,{'BL','BLOQUEADO                  '})
aadd(aStatus,{'DE','DOCUMENTO DE ENTRADA GERADO'})
aadd(aStatus,{'SA','SOLICITADO ANULACAO        '})

oReport:SetTitle( "Relat�rio Registro de Aprova��es "+DTOC(MV_PAR03)+" a "+DTOC(MV_PAR04)+".")

oSection:BeginQuery() //Metodo para iniciar a Sele��o dos Registros.

cQuery:=" SELECT ZB8_FILDOC,ZB8_CTE,ZB8_NRIMP,ZB8_DTEMIS,A2_NOME,A2_EST,ZB8_STATUS,"
cQuery+=" (SELECT COUNT(*) FROM "+RetSqlName("ZB9")+" WHERE ZB9_NRIMP = ZB8_NRIMP AND  D_E_L_E_T_='') AS QTDNFE "
cQuery+=" FROM "+ RetSqlName("ZB8") + " ZB8 " 
cQuery+=" LEFT OUTER  JOIN "+RetSqlName("SA2") + " SA2 ON A2_COD+A2_LOJA = ZB8.ZB8_EMISDF  AND  SA2.D_E_L_E_T_='' "
cQuery+=" WHERE "
cQuery+=" ZB8.D_E_L_E_T_='' AND  "
cQuery+=" ZB8_FILDOC    BETWEEN '"+MV_PAR01       +"' AND '"+MV_PAR02	     +"' AND "
cQuery+=" ZB8_DTEMIS    BETWEEN '"+DTOS(MV_PAR03) +"' AND '"+DTOS(MV_PAR04)  +"' AND "
cQuery+=" ZB8_NRIMP     BETWEEN '"+MV_PAR05       +"' AND '"+MV_PAR06	     +"' AND "
cQuery+=" ZB8_CDDEST    BETWEEN '"+MV_PAR07       +"' AND '"+MV_PAR08	     +"' AND "
cQuery+=" ZB8_CDREM     BETWEEN '"+MV_PAR09       +"' AND '"+MV_PAR10	     +"' AND "
cQuery+=" ZB8_EMISDF    BETWEEN '"+MV_PAR11       +"' AND '"+MV_PAR12	     +"' AND "
cQuery+=" A2_EST        BETWEEN '"+MV_PAR13       +"' AND '"+MV_PAR14	     +"' " 
cQuery+=" ORDER BY ZB8_FILDOC,ZB8_CTE "

IF SELECT("Z8")>0
	Z8->(dbCloseArea())
Endif
TCQUERY cQuery NEW ALIAS Z8

dbSelectArea("Z8")
dbgotop()
While Z8->(!EOF())  
	aZB8:={}    
	aZBC:={}    
	aZBE:={}    
	aZBF:={}    

	aadd(aZB8,{ZB8_FILDOC,ZB8_CTE,A2_NOME,QTDNFE,ZB8_STATUS,ZB8_DTEMIS})

	cQuery:=" SELECT ZBC_OCORR,ZBC_LIBBLO "
	cQuery+=" FROM "+ RetSqlName("ZBC") + " ZBC " 
	cQuery+=" WHERE "
	cQuery+=" ZBC.D_E_L_E_T_='' AND  "
	cQuery+=" ZBC_NRIMP = '"+Z8->ZB8_NRIMP+"' "
	IF SELECT("BC")>0
		BC->(dbCloseArea())
	Endif
	TCQUERY cQuery NEW ALIAS BC
	dbSelectArea("BC")
	dbgotop()
	While BC->(!EOF())  
		aadd(aZBC,{ZBC_OCORR,ZBC_LIBBLO})
		BC->(dbSkip())
	Enddo
	BC->(dbCloseArea())

	cQuery:=" SELECT ZBE_USER,ZBE_APUSER,ZBE_DTAPRO "
	cQuery+=" FROM "+ RetSqlName("ZBE") + " ZBE " 
	cQuery+=" WHERE "
	cQuery+=" ZBE.D_E_L_E_T_='' AND  "
	cQuery+=" ZBE_NRIMP = '"+Z8->ZB8_NRIMP+"' "
	IF SELECT("BE")>0
		BE->(dbCloseArea())
	Endif
	TCQUERY cQuery NEW ALIAS BE
	dbSelectArea("BE")
	dbgotop()
	While BE->(!EOF())  
		aadd(aZBE,{ZBE_USER,ZBE_APUSER,ZBE_DTAPRO})
		BE->(dbSkip())
	Enddo
	BE->(dbCloseArea())


	cQuery:=" SELECT ZBF_USER,ZBF_STATUS,ZBF_DTAPRO "
	cQuery+=" FROM "+ RetSqlName("ZBF") + " ZBF " 
	cQuery+=" WHERE "
	cQuery+=" ZBF.D_E_L_E_T_='' AND  "
	cQuery+=" ZBF_NRIMP = '"+Z8->ZB8_NRIMP+"' "
	IF SELECT("BF")>0
		BF->(dbCloseArea())
	Endif
	TCQUERY cQuery NEW ALIAS BF
	dbSelectArea("BF")
	dbgotop()
	While BF->(!EOF())  
		aadd(aZBF,{ZBF_USER,'',ZBF_STATUS,ZBF_DTAPRO})
		BF->(dbSkip())
	Enddo
	BF->(dbCloseArea())

	nMaior:=len(aZB8)
	nMaior:=max(nMaior,len(aZBC))
	nMaior:=max(nMaior,len(aZBE))
	nMaior:=max(nMaior,len(aZBF))

	for nCont=1 to nMaior
		if nCont=1
			AADD(aDados,{aZB8[nCont,iB8fild],aZB8[nCont,iB8ncte],aZB8[nCont,iB8nome],aZB8[nCont,iB8qtnf],aZB8[nCont,iB8stat],aZB8[nCont,iB8emis],'','','','','','','','',''})
		else
			AADD(aDados,{'','','','','','','','','','','','','','',''})
		endif
		if nCont<=len(aZBC)
			aDados[nCont,iB8cocor]:=aZBC[nCont,iBCocor]
			aDados[nCont,iB8clibb]:=aZBC[nCont,iBClibb]
		endif

		if nCont<=len(aZBE)
			aDados[nCont,iB8euser]:=aZBE[nCont,iBEuser]
			aDados[nCont,iB8eapus]:=aZBE[nCont,iBEapus]
			aDados[nCont,iB8edtap]:=aZBE[nCont,iBEdtap]
		endif

		if nCont<=len(aZBF)
			aDados[nCont,iB8fuser]:=aZBF[nCont,iBFuser]
			aDados[nCont,iB8fapus]:=aZBF[nCont,iBFapus]
			aDados[nCont,iB8fstat]:=aZBF[nCont,iBFstat]
			aDados[nCont,iB8fdtap]:=aZBF[nCont,iBFdtap]
		endif
	next
	AADD(aDados,{'','','','','','','','','','','','','','',''})


	If empty(nPos:= aScan(aResfil,{|x| x[1]=Z8->ZB8_FILDOC}))
		AADD(aResfil,{Z8->ZB8_FILDOC,1})
		nPos:=len(aResfil)
	Else
		aResfil[nPos,2]++
	Endif

	If empty(nPos:= aScan(aRessta,{|x| x[1]=Z8->ZB8_FILDOC+Z8->ZB8_STATUS}))

		if empty(nPos:=aScan(aStatus,{|x| x[1]=Z8->ZB8_STATUS}))
			cDesSt:=space(10)
		else
			cDesSt:=aStatus[nPos,2]
		endif                                

		AADD(aRessta,{Z8->ZB8_FILDOC+Z8->ZB8_STATUS,Z8->ZB8_FILDOC,Z8->ZB8_STATUS,cDesSt,1})
		nPos:=len(aRessta)
	Else
		aRessta[nPos,5]++
	Endif


	Z8->(dbSkip())
Enddo
Z8->(dbCloseArea())

oSection:EndQuery()

oReport:SetMeter(LEN(aDados)) //Metodo para Dizer o tamanho da barra de progresso baseado no RecCount( Quantidade de Registros )

oReport:EndPage() //Comecar do Topo
oSection:Init()

for nCont=1 to len(aDados)
	If oReport:Cancel()  //Se o Relatorio for Cancelado
		Exit   //Sai do la�o
	EndIf

	if empty(nPos:=aScan(aStatus,{|x| x[1]=aDados[nCont,iB8stat]}))
		cDesSt:=space(10)
	else
		cDesSt:=aStatus[nPos,2]
	endif                                

	if empty(aDados[nCont,iB8fild])
		cNfil:=space(10)
	else
		cNfil:=alltrim(FWFilialName(cEmpAnt,aDados[nCont,iB8fild],1))
	endif

	if empty(aDados[nCont,iB8euser])
		nNeuser:=space(10)
	else
		nNeuser:=u_infoUser(aDados[nCont,iB8euser],"NOME")
	endif
	
	if empty(aDados[nCont,iB8eapus])
		nNeapus:=space(10)
	else
		nNeapus:=u_infoUser(aDados[nCont,iB8eapus],"NOME") 
	endif

	if empty(aDados[nCont,iB8fuser])
		nNfuser:=space(10)
	else
		nNfuser:=u_infoUser(aDados[nCont,iB8fuser],"NOME") 
	endif

	if empty(aDados[nCont,iB8fapus])
		nNfapus:=space(10)
	else
		nNfapus:=u_infoUser(aDados[nCont,iB8fapus],"NOME") 
	endif

	oSection:Cell("ZB8_FILDOC" ):SetValue(aDados[nCont,iB8fild]+' '+cNfil)
	oSection:Cell("ZB8_CTE" ):SetValue(aDados[nCont,iB8ncte])
	oSection:Cell("TRANSP" ):SetValue(alltrim(aDados[nCont,iB8nome]))
	oSection:Cell("QTDNFE"):SetValue(aDados[nCont,iB8qtnf])
	oSection:Cell("STATUS"):SetValue(aDados[nCont,iB8stat]+' '+cDesSt)
	oSection:Cell("CDGOCOR" ):SetValue(aDados[nCont,iB8cocor])
	oSection:Cell("STAOCOR" ):SetValue(aDados[nCont,iB8clibb])
	oSection:Cell("APROCOR" ):SetValue(aDados[nCont,iB8euser]+' '+nNeuser)
	oSection:Cell("POROCOR" ):SetValue(aDados[nCont,iB8eapus]+' '+nNeapus)
	oSection:Cell("DAPOCOR" ):SetValue(if(empty(aDados[nCont,iB8edtap]),space(10),stod(aDados[nCont,iB8edtap])))
	oSection:Cell("APRALCA"):SetValue(aDados[nCont,iB8fuser]+' '+nNfuser)
	oSection:Cell("PORALCA"):SetValue(aDados[nCont,iB8fapus]+' '+nNfapus)
	oSection:Cell("STAALCA"):SetValue(aDados[nCont,iB8fstat])
	oSection:Cell("DAPALCA"):SetValue(if(empty(aDados[nCont,iB8fdtap]),space(10),stod(aDados[nCont,iB8fdtap])))  
	oSection:Cell("DATEMIS"):SetValue(if(empty(aDados[nCont,iB8emis]),space(10),stod(aDados[nCont,iB8emis])))
	oSection:PrintLine()  //Imprimir a Sec��o  
next

oReport:SkipLine()    //Salto de linha na sec��o
oSection:Finish()

aDados:={}
for nFil:=1 to len(aResfil)   
	aadd(aDados,{aResfil[nFil,1],aResfil[nFil,2],'','',''})
	nPos:=len(aDados)
	nVez:=1
	for nSta:=1 to len(aRessta)
		if aResfil[nFil,1]=aRessta[nSta,2]
			if nVez=1  
				aDados[nPos,3]:=aRessta[nSta,3]
				aDados[nPos,4]:=aRessta[nSta,4]
				aDados[nPos,5]:=aRessta[nSta,5]
				nVez=2
			else
				aadd(aDados,{'','',aRessta[nSta,3],aRessta[nSta,4],aRessta[nSta,5]})
			endif
		endif
	next
	aadd(aDados,{'','','','',''})
next

oSection1:Init()
for nCont:=1 to len(aDados)
	oSection1:Cell("FILIAL"):SetValue(aDados[nCont,1]+' '+if(empty(aDados[nCont,1]),space(30),alltrim(FWFilialName(cEmpAnt,aDados[nCont,1],1))))
	oSection1:Cell("QTDCTE"):SetValue(aDados[nCont,2])
	oSection1:Cell("STATUS"):SetValue(aDados[nCont,3])
	oSection1:Cell("DESSTA"):SetValue(aDados[nCont,4])
	oSection1:Cell("QTDSTA"):SetValue(aDados[nCont,5])
	oSection1:PrintLine()  //Imprimir a Sec��o
Next
oReport:SkipLine()    //Salto de linha na sec��o
oSection1:Finish()


return

//|----------------------------------------------------------|//
//|Fun��o: Cria��o das Perguntas							 |//
//|----------------------------------------------------------|//
Static Function AjustaSX1(cPerg)

PutSx1(cPerg, "01","Tomador De"           ,".",".","mv_ch01","C",08,0,1,"G","","SM0","","","mv_par01","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "02","Tomador Ate"          ,".",".","mv_ch02","C",08,0,1,"G","","SM0","","","mv_par02","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "03","Data Emissao Cte De"  ,".",".","mv_ch03","D",08,0,1,"G","",""   ,"","","mv_par03","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "04","Data Emissao Cte Ate" ,".",".","mv_ch04","D",08,0,1,"G","",""   ,"","","mv_par04","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "05","Codigo Importacao De" ,".",".","mv_ch05","C",16,0,1,"G","",""   ,"","","mv_par05","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "06","Codigo Importacao Ate",".",".","mv_ch06","C",16,0,1,"G","",""   ,"","","mv_par06","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "07","Destinatario De"      ,".",".","mv_ch07","C",14,0,1,"G","","SA1","","","mv_par07","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "08","Destinatario Ate"     ,".",".","mv_ch08","C",14,0,1,"G","","SA1","","","mv_par08","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "09","Remetente De"         ,".",".","mv_ch09","C",14,0,1,"G","","SA2","","","mv_par09","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "10","Remetente Ate"        ,".",".","mv_ch10","C",14,0,1,"G","","SA2","","","mv_par10","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "11","Emissor De"           ,".",".","mv_ch11","C",14,0,1,"G","","SA2","","","mv_par11","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "12","Emissor Ate"          ,".",".","mv_ch12","C",14,0,1,"G","","SA2","","","mv_par12","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "13","UF De"                ,".",".","mv_ch13","C",02,0,1,"G","","12"   ,"","","mv_par13","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "14","UF Ate"               ,".",".","mv_ch14","C",02,0,1,"G","","12"   ,"","","mv_par14","" ,"","","","","","","","","","","","","","","")



Return()
