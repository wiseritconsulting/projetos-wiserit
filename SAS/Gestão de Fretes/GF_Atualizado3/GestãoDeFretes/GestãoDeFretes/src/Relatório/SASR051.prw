#Include "Protheus.ch"
#Include "TopConn.ch"
#include "Rwmake.ch"
 
//******************************************************************//
//Autor: Elicio Junior							      Data:28/07/16 //
//******************************************************************//
//Fun��o: Relat�rio Custo por Frete		   					   	    //
//******************************************************************//
User Function SASR051()
Local oReport

oReport:=ReportDef()
oReport:PrintDialog()


//------------------------------------------------//
//FUN��O: CRIACAO DA SE�AO E CELULAS			  //
//------------------------------------------------//
Static Function ReportDef()
Local oReport
Local oSection
Local oSection2
Local oSection3
Local cPerg     :="SASR051"

AjustaSX1(cPerg)
Pergunte(cPerg,.F.)

oReport := TReport():New("SASR051","Relat�rio Custo por Frete","SASR051", {|oReport| ReportPrint(oReport)},"Relat�rio Custo por Frete")
oReport:SetLandscape() //Paisagem

//������������������������������������������������������������������������Ŀ
//� Section 1			                                                  �
//��������������������������������������������������������������������������
oSection:= TRSection():New(oReport,OemToAnsi("Notas"),{"ZB8"})
TRCell():New(oSection,"ZB8_FILDOC" ," ","Filial"             ,,40)
TRCell():New(oSection,"DOCUME"	   ," ","Documento/Serie"    ,,30) 
TRCell():New(oSection,"ZB8_CTE"    ," ","Num Cte" 			 ,,30)
TRCell():New(oSection,"TRANSP"     ," ","Transportador"      ,,40) 
//TRCell():New(oSection,"TIPOPD"     ," ","Tipo Pedido"        ,,30)
TRCell():New(oSection,"A1_EST"     ," ","UF Cliente"         ,,04)
TRCell():New(oSection,"ZB8_VOLUM"  ," ","Volume"             ,PesqPict("ZB8","ZB8_VOLUM"),20)
TRCell():New(oSection,"A1_MUN"     ," ","Cidade Cliente"     ,,40)
TRCell():New(oSection,"VALNFE"     ," ","Valor Nfe"          ,PesqPict("SD2","D2_TOTAL")  ,20) 
TRCell():New(oSection,"QTDNFE"     ," ","Peso"               ,PesqPict("SD2","D2_PESO")   ,20) 
TRCell():New(oSection,"ZB8_FRPESO" ," ","Peso Ajustado"      ,PesqPict("ZB8","ZB8_FRPESO"),20)
TRCell():New(oSection,"ZB4_VALOR"  ," ","Frete Estimado"     ,PesqPict("ZB4","ZB4_VALOR") ,20)
TRCell():New(oSection,"ZB8_VLDF"   ," ","Frete Ajustado"     ,PesqPict("ZB8","ZB8_VLDF")  ,20) 
TRCell():New(oSection,"PERFAT"     ," ","% Faturamento"      ,PesqPict("SB1","B1_CONV")   ,20)
TRCell():New(oSection,"VALOKG"     ," ","Valor Kg"           ,PesqPict("SD2","D2_TOTAL")  ,20)

//������������������������������������������������������������������������Ŀ
//� Section 1			                                                  �
//��������������������������������������������������������������������������
oSection1:= TRSection():New(oReport,OemToAnsi("Resumo"),{"ZB8"})
//oSection2:SetTotalInLine(.F.)
TRCell():New(oSection1,"FILIAL"	," ","Filial"   	                                  ,,50)
TRCell():New(oSection1,"QTDNFE"	," ","Qtd Nfe"            ,PesqPict("ZB8","ZB8_FRVAL") ,20)
TRCell():New(oSection1,"VLRNFE"	," ","Valor Nfe"       	  ,PesqPict("ZB8","ZB8_FRVAL") ,20)
TRCell():New(oSection1,"PESNFE"	," ","Peso"            	  ,PesqPict("ZB8","ZB8_FRPESO"),20)
TRCell():New(oSection1,"FRENFE" ," ","Frete"              ,PesqPict("ZB8","ZB8_FRVAL") ,20)
TRCell():New(oSection1,"FRTFAT" ," ","% Faturamento"      ,PesqPict("SB1","B1_CONV")   ,20)
TRCell():New(oSection1,"VLRKIG" ," ","Valor Kg"			  ,PesqPict("SD2","D2_TOTAL")  ,20)

return(oReport)

//------------------------------------------------//
//FUN��O: IMPRESSAO								  //
//------------------------------------------------//
Static Function ReportPrint(oReport)

Local oSection   := oReport:Section(1)
Local oSection1  := oReport:Section(2)

Private cNfe    
Private cfilex 
Private cTransp 
Private cCte    
Private aResumo:={}    
Private aResum1:={}    

Private iChave:=1    
Private iCdFil:=2
Private iCdCte:=3
Private iQtnfe:=4
Private iVlNfe:=5    
Private iPsNfe:=6
Private iVlFre:=7    
Private iFrFat:=8   
Private iVlrKg:=9    


Private xCdFil:=1
Private xQtnfe:=2
Private xVlNfe:=3    
Private xPsNfe:=4
Private xVlFre:=5    
Private xFrFat:=6   
Private xVlrKg:=7    


oReport:SetTitle( "Relat�rio Custo por Frete Periodo "+DTOC(MV_PAR03)+" a "+DTOC(MV_PAR04)+".")

oSection:BeginQuery() //Metodo para iniciar a Sele��o dos Registros.

cQuery:=" SELECT ZB8_FILDOC,F2_DOC,F2_SERIE,ZB8_CTE,ZB8_EMISDF,A2_NOME,ZB8_CDDEST,A1_EST,ZB8_VOLUM,A1_MUN,ZB8_PESOR,ZB8_VLDF,ZB8_FRPESO,ZB8_NRDF,ZB8_SERDF, "
cQuery+=" (SELECT SUM(D2_TOTAL) FROM "+RetSqlName("SD2")+" WHERE D2_DOC=F2_DOC AND  D_E_L_E_T_='') AS VALNFE, "
cQuery+=" (SELECT SUM(D2_PESO) FROM "+RetSqlName("SD2")+" WHERE D2_DOC=F2_DOC AND  D_E_L_E_T_='') AS QTDNFE "
cQuery+=" FROM "+ RetSqlName("ZB8") + " ZB8 " 
cQuery+=" LEFT OUTER  JOIN "+RetSqlName("SA1") + " SA1 ON A1_COD+A1_LOJA = ZB8.ZB8_CDDEST  AND  SA1.D_E_L_E_T_='' "
cQuery+=" LEFT OUTER  JOIN "+RetSqlName("SA2") + " SA2 ON A2_COD+A2_LOJA = ZB8.ZB8_EMISDF  AND  SA2.D_E_L_E_T_='' "
cQuery+=" LEFT OUTER  JOIN "+RetSqlName("ZB9") + " ZB9 ON ZB9_NRIMP      = ZB8_NRIMP       AND  ZB9.D_E_L_E_T_='' "
cQuery+=" LEFT OUTER  JOIN "+RetSqlName("SF2") + " SF2 ON F2_CHVNFE      = ZB9_DANFE       AND  SF2.D_E_L_E_T_='' "
cQuery+=" WHERE "
cQuery+=" ZB8.D_E_L_E_T_='' AND  "
cQuery+=" ZB8_FILDOC    BETWEEN '"+MV_PAR01       +"' AND '"+MV_PAR02	     +"' AND "
cQuery+=" ZB8_DTEMIS    BETWEEN '"+DTOS(MV_PAR03) +"' AND '"+DTOS(MV_PAR04)  +"' AND "
cQuery+=" ZB8_NRIMP     BETWEEN '"+MV_PAR05       +"' AND '"+MV_PAR06	     +"' AND "
cQuery+=" ZB8_CDDEST    BETWEEN '"+MV_PAR07       +"' AND '"+MV_PAR08	     +"' AND "
cQuery+=" ZB8_CDREM     BETWEEN '"+MV_PAR09       +"' AND '"+MV_PAR10	     +"' AND "
cQuery+=" ZB8_EMISDF    BETWEEN '"+MV_PAR11       +"' AND '"+MV_PAR12	     +"' AND "
cQuery+=" ZB9_DANFE     BETWEEN '"+MV_PAR13       +"' AND '"+MV_PAR14	     +"' AND " 
cQuery+=" A1_EST        BETWEEN '"+MV_PAR15       +"' AND '"+MV_PAR16	     +"' " 
cQuery+=" ORDER BY ZB8_FILDOC,F2_DOC,F2_SERIE,ZB8_CTE "

IF SELECT("T1")>0
	T1->(dbCloseArea())
Endif
TCQUERY cQuery NEW ALIAS T1

oSection:EndQuery()

oReport:SetMeter(RecCount()) //Metodo para Dizer o tamanho da barra de progresso baseado no RecCount( Quantidade de Registros )

cfilex  := ''
cTransp := ''
cCte    := ''
oReport:EndPage() //Comecar do Topo
oSection:Init()

dbSelectArea("T1")
dbgotop()
While T1->(!EOF())  //Se n�o for Final do Arquivo
    
	If oReport:Cancel()  //Se o Relatorio for Cancelado
		Exit   //Sai do la�o
	EndIf

	If empty(nPos:= aScan(aResumo,{|x| x[iChave]=T1->ZB8_FILDOC+T1->ZB8_CTE}))
		AADD(aResumo,{T1->ZB8_FILDOC+T1->ZB8_CTE,T1->ZB8_FILDOC,T1->ZB8_CTE,1,T1->VALNFE,T1->QTDNFE,T1->ZB8_VLDF,0,0})
		nPos:=len(aResumo)
	Else
		aResumo[nPos,iQtnfe]++
		aResumo[nPos,iVlNfe]+=T1->VALNFE
		aResumo[nPos,iPsNfe]+=T1->QTDNFE
	Endif

	oSection:Cell("ZB8_FILDOC" ):SetValue(T1->ZB8_FILDOC+' '+if(empty(T1->ZB8_FILDOC),space(30),alltrim(FWFilialName(cEmpAnt,T1->ZB8_FILDOC,1))))
	oSection:Cell("DOCUME" ):SetValue(ALLTRIM(T1->F2_DOC)+'/'+T1->F2_SERIE)
	oSection:Cell("ZB8_CTE" ):SetValue(ALLTRIM(T1->ZB8_NRDF)+'/'+T1->ZB8_SERDF)
	oSection:Cell("TRANSP"):SetValue(ALLTRIM(T1->ZB8_EMISDF)+' '+alltrim(T1->A2_NOME))
//	oSection:Cell("TIPOPD"):SetValue(SPACE(10))
	oSection:Cell("A1_EST" ):SetValue(T1->A1_EST)
	oSection:Cell("ZB8_VOLUM" ):SetValue(T1->ZB8_VOLUM)
	oSection:Cell("A1_MUN" ):SetValue(alltrim(T1->A1_MUN))
	oSection:Cell("VALNFE" ):SetValue(T1->VALNFE)
	oSection:Cell("QTDNFE" ):SetValue(T1->QTDNFE)
	oSection:Cell("ZB8_FRPESO"):SetValue(T1->ZB8_PESOR)
	oSection:Cell("ZB4_VALOR"):SetValue(0) //TODO: FALTA DECIDIR A CHAVE PARA ACESSAR A ZB4_ID
	oSection:Cell("ZB8_VLDF"):SetValue(T1->ZB8_VLDF)  
	oSection:Cell("PERFAT"):SetValue((T1->ZB8_VLDF/T1->VALNFE)*100)
	oSection:Cell("VALOKG"):SetValue(T1->ZB8_VLDF/T1->ZB8_FRPESO)
	oSection:PrintLine()  //Imprimir a Sec��o

	T1->(dbSkip())

Enddo
oReport:SkipLine()    //Salto de linha na sec��o
oSection:Finish()

T1->(dbCloseArea())

for nCont:=1 to len(aResumo)
	If empty(nPos:= aScan(aResum1,{|x| x[iChave]=aResumo[nCont,iCdFil]}))
		AADD(aResum1,{aResumo[nCont,iCdFil],aResumo[nCont,iQtnfe],aResumo[nCont,iVlNfe],aResumo[nCont,iPsNfe],aResumo[nCont,iVlFre],0,0})
		nPos:=len(aResum1)
	Else
		aResum1[nPos,xQtnfe]+=aResumo[nCont,iQtnfe]
		aResum1[nPos,xVlNfe]+=aResumo[nCont,iVlNfe]
		aResum1[nPos,xPsNfe]+=aResumo[nCont,iPsNfe]
		aResum1[nPos,xVlFre]+=aResumo[nCont,iVlFre]
	Endif
next

for nCont:=1 to len(aResum1)
	aResum1[nCont,xFrFat]:=round((aResum1[nCont,xVlFre]/aResum1[nCont,xVlNfe])*100,2)
	aResum1[nCont,xVlrKg]:=round(aResum1[nCont,xVlFre]/aResum1[nCont,xPsNfe],2)
Next

oSection1:Init()
for nCont:=1 to len(aResum1)
	oSection1:Cell("FILIAL"):SetValue(aResum1[nCont,xCdFil]+' '+if(empty(aResum1[nCont,xCdFil]),space(30),alltrim(FWFilialName(cEmpAnt,aResum1[nCont,xCdFil],1))))
	oSection1:Cell("QTDNFE"):SetValue(aResum1[nCont,xQtnfe])
	oSection1:Cell("VLRNFE"):SetValue(aResum1[nCont,xVlNfe])
	oSection1:Cell("PESNFE"):SetValue(aResum1[nCont,xPsNfe])
	oSection1:Cell("FRENFE"):SetValue(aResum1[nCont,xVlFre])
	oSection1:Cell("FRTFAT"):SetValue(aResum1[nCont,xFrFat])
	oSection1:Cell("VLRKIG"):SetValue(aResum1[nCont,xVlrKg])
	oSection1:PrintLine()  //Imprimir a Sec��o
Next
oReport:SkipLine()    //Salto de linha na sec��o
oSection1:Finish()

return

//|----------------------------------------------------------|//
//|Fun��o: Cria��o das Perguntas							 |//
//|----------------------------------------------------------|//
Static Function AjustaSX1(cPerg)

PutSx1(cPerg, "01","Tomador De"           ,".",".","mv_ch01","C",08,0,1,"G","","SM0","","","mv_par01","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "02","Tomador Ate"          ,".",".","mv_ch02","C",08,0,1,"G","","SM0","","","mv_par02","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "03","Data Emissao Cte De"  ,".",".","mv_ch03","D",08,0,1,"G","",""   ,"","","mv_par03","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "04","Data Emissao Cte Ate" ,".",".","mv_ch04","D",08,0,1,"G","",""   ,"","","mv_par04","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "05","Codigo Importacao De" ,".",".","mv_ch05","C",16,0,1,"G","",""   ,"","","mv_par05","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "06","Codigo Importacao Ate",".",".","mv_ch06","C",16,0,1,"G","",""   ,"","","mv_par06","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "07","Destinatario De"      ,".",".","mv_ch07","C",14,0,1,"G","","SA1","","","mv_par07","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "08","Destinatario Ate"     ,".",".","mv_ch08","C",14,0,1,"G","","SA1","","","mv_par08","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "09","Remetente De"         ,".",".","mv_ch09","C",14,0,1,"G","","SA2","","","mv_par09","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "10","Remetente Ate"        ,".",".","mv_ch10","C",14,0,1,"G","","SA2","","","mv_par10","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "11","Emissor De"           ,".",".","mv_ch11","C",14,0,1,"G","","SA2","","","mv_par11","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "12","Emissor Ate"          ,".",".","mv_ch12","C",14,0,1,"G","","SA2","","","mv_par12","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "13","Chave CTE De"         ,".",".","mv_ch13","C",44,0,1,"G","",""   ,"","","mv_par13","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "14","Chave CTE Ate"        ,".",".","mv_ch14","C",44,0,1,"G","",""   ,"","","mv_par14","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "15","UF De"                ,".",".","mv_ch15","C",02,0,1,"G","","12"   ,"","","mv_par15","" ,"","","","","","","","","","","","","","","")
PutSx1(cPerg, "16","UF Ate"               ,".",".","mv_ch16","C",02,0,1,"G","","12"   ,"","","mv_par16","" ,"","","","","","","","","","","","","","","")

Return()

