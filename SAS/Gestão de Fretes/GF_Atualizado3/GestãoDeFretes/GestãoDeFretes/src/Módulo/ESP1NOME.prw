#Include 'Protheus.ch'

//--------------------------------------------------------------------
/*/{Protheus.doc} ES1PNOME

Fun��o para criar novo menu para o m�dulo de Gest�o de Fretes

@type function
@author Sam Barros
@since 07/08/2016
@version 1.0
@return cRet, Nome do novo m�dulo criado
@see http://goo.gl/T3NPG3
/*/
//--------------------------------------------------------------------
User Function ESP1NOME()
	Local cRet := "SAS - Gest�o de Fretes"
Return cRet