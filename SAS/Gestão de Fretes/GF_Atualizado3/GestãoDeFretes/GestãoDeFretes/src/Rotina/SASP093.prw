#Include 'Protheus.ch'

User Function SASP093()
	Private oMark 
	Private cMark := GetMark()
	Private aRotina := {}
	Private cCadastro
	Private cCodUsr := RetCodUsr()
	Private cFiltro := "ZBE_USER = '" + cCodUsr + "' AND ZBE_STATUS = 'B' AND ZBE_ANULA = 'N'"
	
	cCadastro := "Visto Ocorr�ncia"
	
	AADD(aRotina,{"Aprovar" 		,"u_AprZBE()" ,0,4})
	AADD(aRotina,{"Rejeitar" 		,"u_ReprZBE()" 	,0,4})
	
	MarkBrow( 'ZBE', 'ZBE_OK',,,.F., cMark,'u_MkAllVO()',,,,'u_MkVO()',{|| u_MkAllVO()},cFiltro,,,,,,.F.)
Return

User Function MkAllVO()
	Local oMark := GetMarkBrow()

	dbSelectArea('ZBE')
	ZBE->(dbGotop())

	While !ZBE->(Eof())
		u_MkVO()
		ZBE->(dbSkip())
	End
	MarkBRefresh( )
	oMark:oBrowse:Gotop()
Return

User Function MkVO()
	If IsMark( 'ZBE_OK', cMark )
		RecLock( 'ZBE', .F. )
		Replace ZBE_OK With Space(2)
		ZBE->(MsUnLock())
	Else
		RecLock( 'ZBE', .F. )
		Replace ZBE_OK With cMark
		ZBE->(MsUnLock())
	EndIf
Return

User Function AprZBE()
	If MsgYesNo("Deseja aprovar as ocorr�ncias marcadas","Aprova��o")
		makeVisto('A')
	EndIf
Return

User Function ReprZBE()
	If MsgYesNo("Deseja reprovar as ocorr�ncias marcadas","Reprova��o")
		makeVisto('R')
	EndIf
Return

Static Function makeVisto(cTipo)
	Local xArea := getArea()
	Local cSql := ""
	Local nQnt := 0
	Local cImport := ""
	Local cSubstantivo 	:= IIF(cTipo=='A', "Aprova��o"	, "Rejei��o")
	Local cInfinitivo	:= IIF(cTipo=='A', "Aprovar"	, "Rejeitar")
	
	cSql += " 	SELECT X.ZBE_NRIMP IMP, X.ZBE_OCORR OCO "
	cSql += " 	FROM  " + RetSqlName("ZBE") + " X "
	cSql += " 	WHERE X.D_E_L_E_T_ = '' "
	cSql += " 	AND X.ZBE_OK = '" + cMark + "' "
	cSql += " 	AND X.ZBE_STATUS = 'B' "
	cSql += " 	AND X.ZBE_ANULA = 'N' "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	While !(QRY->(EOF()))
		dbSelectArea("ZBE")
		dbSetOrder(1)
		if dbSeek(xFilial("ZBE") + QRY->IMP + QRY->OCO)
			cImport := QRY->IMP
			nQnt++
			While !(ZBE->(EOF())) .AND. ZBE->ZBE_NRIMP == QRY->IMP .AND. ZBE->ZBE_OCORR == QRY->OCO
				RecLock("ZBE", .F.)
					ZBE->ZBE_APUSER := cCodUsr
					ZBE->ZBE_STATUS := cTipo
					ZBE->ZBE_DTAPRO := dDataBase
					ZBE->ZBE_HRAPRO := time()
				MsUnLock()
				
				dbSelectArea("ZBC")
				dbSetOrder(1)
				If dbSeek(xFilial("ZBC") + QRY->IMP + QRY->OCO)
					RecLock("ZBC", .F.)
						ZBC->ZBC_LIBBLO := 'L'
					MsUnLock()
				EndIf
				
				ZBC->(dbCloseArea())
				
				ZBE->(dbSkip())
			EndDo
		EndIf
		QRY->(dbSkip())
	EndDo
	QRY->(dbCloseArea())
	
	If nQnt > 0
		if nQnt == 1
			MsgInfo(cSubstantivo + " de " + Alltrim(Str(nQnt)) + " ocorr�ncia realizada com sucesso")
		Else
			MsgInfo(cSubstantivo + " de " + Alltrim(Str(nQnt)) + " ocorr�ncias realizadas com sucesso")
		EndIf
	Else
		MsgAlert("N�o foram selecionadas ocorr�ncias para " + cInfinitivo)
	EndIf
	
	if cTipo == 'A'
		u_geraAlcada()
	Else
		RejCTE(cImport)
	EndIf
	
	RestArea(xArea)
Return

User Function geraAlcada()
	Local bRet := .T.
	Local cSql := ""
	
	Local cUF := ""
	
	Local cGrpFF := ""
	Local cGrpRK := "" 
	
	Local _aAprovFF := {}
	Local _aAprovRK := {}
	Local _aAprov := {}
	
	Local zArea := getArea()
	
	cSql += " SELECT DISTINCT X.ZBC_NRIMP IMP "
	cSql += " FROM  " + RetSqlName("ZBC") + " X "
	cSql += " WHERE X.D_E_L_E_T_ = '' "
	cSql += " AND X.ZBC_ALCADA <> 'S' " //Ainda n�o gerou al�ada
	cSql += " AND X.ZBC_NRIMP IN 		(SELECT Y.ZBC_NRIMP FROM " + RetSqlName("ZBC") + " Y WHERE Y.D_E_L_E_T_ = '' AND Y.ZBC_ALCADA <> 'S' AND Y.ZBC_LIBBLO = 'L') "
	cSql += " AND X.ZBC_NRIMP NOT IN 	(SELECT W.ZBC_NRIMP FROM " + RetSqlName("ZBC") + " W WHERE W.D_E_L_E_T_ = '' AND W.ZBC_ALCADA <> 'S' AND W.ZBC_LIBBLO IN ('B','R','P')) "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRY",.F.,.T.)
	
	cDest := POSICIONE("ZB8",1,xFilial("ZB8")+QRY->IMP, "ZB8->ZB8_CDDEST")
	
	While !(QRY->(EOF()))
		cUF := POSICIONE("SA1",1,xFilial("SA1")+cDest, "SA1->A1_EST")
		cGrpFF := POSICIONE("ZB1", 1, xFilial("ZB1") + cUF + '1',"ZB1->ZB1_GRUPO")
		cGrpRK := POSICIONE("ZB1", 1, xFilial("ZB1") + cUF + '2',"ZB1->ZB1_GRUPO")
		
		dbSelectArea("ZB8")
		dbSetOrder(1)
		dbSeek(xFilial("ZB8")+QRY->IMP)
		if ZB8->ZB8_VLDF > 0
			nValFrete := ZB8->ZB8_VLDF
		Else
			nValFrete := ZB8->ZB8_VLCARG
		EndIf 
		
		if ZB8->ZB8_PESOR > 0
			nRealKilo := nValFrete / ZB8->ZB8_PESOR
		Else
			nRealKilo := 0.01
		EndIf
		
		RecLock("ZB8", .F.)
			ZB8->ZB8_STATUS := 'AG'
		MsUnLock()
		
		ZB8->(dbCloseArea())
		
		_aAprovFF := u_getAprov(cGrpFF, nValFrete, '1') 
		_aAprovRK := u_getAprov(cGrpRK, nRealKilo, '2')
		
		_aAprov :=  u_SomaArray(_aAprovFF,_aAprovRK)
		
		u_makeAprov(QRY->IMP, _aAprov, .T.)
		
		U_EmailGF(ZB8->ZB8_NRIMP, 'A')
		
		QRY->(dbSkip())
	EndDo
	QRY->(dbCloseArea())
	
	RestArea(zArea)
Return bRet

User Function getAprov(cGrupo, nVal, cTipo)
	Local aRet := {}
	Local cSql := ""
	Local cCdUsr := ""
	
	cSql += " SELECT X.ZB7_APROV APROV"
	cSql += " FROM " + RetSqlName("ZB7") + " X " 
	cSql += " WHERE X.D_E_L_E_T_ = '' "
	cSql += " AND X.ZB7_COD = '"+cGrupo+"' "
	cSql += " AND X.ZB7_LIMMIN <= '" + Str(nVal) + "' "
	cSql += " AND X.ZB7_LIMMAX >= '" + Str(nVal) + "' "
	cSql += "  "
	cSql += "  "
	cSql += "  "
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),"QRYAPROV",.F.,.T.)
	
	While !(QRYAPROV->(EOF()))
		cCdUsr := POSICIONE("ZB5",1,xFilial("ZB5")+QRYAPROV->APROV, "ZB5->ZB5_USER")
	
		AADD(aRet, {cCdUsr, cTipo})
		QRYAPROV->(dbSkip())
	EndDo
	QRYAPROV->(dbCloseArea())
Return aRet

User Function makeAprov(cSeqImp, aAprov, bOcorr)
	DbSelectArea("ZB8")
	DbSetOrder(1)
	if DbSeek(xFilial("ZB8")+cSeqImp)
		For i := 1 to len(aAprov) 
			RecLock("ZBF", .T.)
				ZBF->ZBF_FILIAL := xFilial("ZBF") 
				ZBF->ZBF_NRIMP 	:= ZB8->ZB8_NRIMP
				ZBF->ZBF_CTE 	:= ZB8->ZB8_CTE
				ZBF->ZBF_NRDF 	:= ZB8->ZB8_NRDF 
				ZBF->ZBF_SERDF 	:= ZB8->ZB8_SERDF 
				ZBF->ZBF_USER 	:= aAprov[i,1]
				ZBF->ZBF_TPAPRO	:= aAprov[i,2]
				ZBF->ZBF_STATUS := 'B'
				ZBF->ZBF_REMET 	:= ZB8->ZB8_CDREM
				ZBF->ZBF_DESREM := POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDREM,"SA2->A2_NREDUZ")
				ZBF->ZBF_TOMADO := ZB8->ZB8_FILDOC
				ZBF->ZBF_DESTOM := FWFilialName(SUBS(ZB8->ZB8_FILDOC,1,2),ZB8->ZB8_FILDOC,1)
				ZBF->ZBF_DEST 	:= ZB8->ZB8_CDDEST
				ZBF->ZBF_DESDES := POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDDEST,"SA1->A1_NREDUZ")
				ZBF->ZBF_VALFRE := ZB8->ZB8_FRVAL
				ZBF->ZBF_PESO 	:= ZB8->ZB8_FRPESO
				ZBF->ZBF_VOLUME	:= ZB8->ZB8_QTVOL
				ZBF->ZBF_EMISS 	:= ZB8->ZB8_DTEMIS
				ZBF->ZBF_FF		:= ZB8->ZB8_FRVAL
				ZBF->ZBF_RK		:= ZB8->ZB8_FRPESO
			MsUnLock()
			
			if bOcorr
				dbSelectArea("ZBC")
				dbSetOrder(1)
				If dbSeek(xFilial("ZBC")+cSeqImp)
					RecLock("ZBC", .F.)
						ZBC->ZBC_ALCADA := 'S'
					MsUnlock()
				EndIf
				ZBC->(dbCloseArea())
			EndIf
		Next i
	EndIf
	ZB8->(DbCloseArea())
Return

Static Function RejCTE(cSeqImp)
	DbSelectArea("ZB8")
	dbSetOrder(1)
	If dbSeek(xFilial("ZB8") + cSeqImp)
		RecLock("ZB8", .F.)
			ZB8->ZB8_STATUS := 'BL'
		MsUnLock()
	EndIf
Return