#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP084

Cadastro de estimativa de frete

@type function
@author Sam Barros
@since 30/06/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SASP084()
	//---------------------------------------------------------//
	//Declara��o de vari�veis
	//---------------------------------------------------------//
	Local cImportadores := U_getCodImport("1;4;5;7")
	
	Private cPerg   	:= "1"
	Private cCadastro 	:= "Cadastro de estimativa de frete"
	Private cDelFunc 	:= ".T." // Validacao para a exclusao. Pode-se utilizar ExecBlock
	Private cString 	:= "ZB4"
	Private cFiltro		:= ""
	Private	aIndexZB4	:= {}
	Private aIndex		:= {}
	Private cCodUse		:= RetCodUsr()
	Private bFiltraBrw 	:= { || FilBrowse( "ZB4" , @aIndex , @cFiltro ) } //Determina a Expressao do Filtro
	Private aRotina 	:= {}
	Private aRotAprov 	:= {}
	Private aRotStts 	:= {}
	Private aCores		:= { }
		
	AADD(aRotina, {"Pesquisar"		,"AxPesqui"		,0,1})
	AADD(aRotina, {"Visualizar"		,"AxVisual"		,0,2})
	//if RetCodUsr() $ GetMV("SA_USRGFRT")
	if RetCodUsr() $ cImportadores	
		AADD(aRotina, {"Incluir"		,"AxInclui"		,0,3})
		AADD(aRotina, {"Alterar"		,"AxAltera"		,0,4})
		AADD(aRotina, {"Excluir"		,"AxDeleta"		,0,5})
		AADD(aRotina, {"Import Estim"   ,"u_SASF002()"	,0,3})
	EndIf
	AADD(aRotina, {"Legenda"      	,"u_legZB4()"	,0,6})

	AADD(aCores, { 'ZB4->ZB4_TIPO ==   "MT"'	    , 'BR_VERDE'		})
	AADD(aCores, { 'ZB4->ZB4_TIPO ==   "MD"'    	, 'BR_AMARELO'		})
	AADD(aCores, { 'ZB4->ZB4_TIPO ==   "VA"'    	, 'BR_AZUL'			})
	AADD(aCores, { 'ZB4->ZB4_TIPO ==   "CO"'    	, 'BR_PRETO'		})
	AADD(aCores, { 'ZB4->ZB4_TIPO ==   "DF"'    	, 'BR_VIOLETA'		})
	
	dbSelectArea("ZB4")
	dbSetOrder(1)

	cPerg   := "1"

	Pergunte(cPerg,.F.)

	dbSelectArea(cString)
	mBrowse( 6,1,22,75,cString,,,,,6,aCores)

	Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros

	ZB4->(dbCloseArea())
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} LegZB4

Legenda da tabela ZB4
[VERDE] 	- Medi��o Triangular
[AMARELO] 	- Medi��o Direta
[AZUL] 		- Venda Avulsa
[PRETO] 	- Compras
[VIOLETA]	- Devolu��o de formul�rio pr�prio  

@type function
@author Sam Barros
@since 06/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
USER Function LegZB4()
	Local aLegenda := 	{;
							{"BR_VERDE"			, "Medi��o Triangular"},;
							{"BR_AMARELO"		, "Medi��o Direta"},;
							{"BR_AZUL"			, "Venda Avulsa"},;
							{"BR_PRETO"			, "Compras"},;
							{"BR_VIOLETA" 		, "Devolu��o de formul�rio pr�prio"};
						}
	BrwLegenda(cCadastro, "Legenda", aLegenda)
Return .T.