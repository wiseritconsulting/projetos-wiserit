#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP083

Cadastro de Ocorrências

@type function
@author Sam Barros
@since 07/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SASP083()
	AxCadastro("ZB3", "Cadastro de Ocorrências", ".T.", ".T.")
Return