#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP080

Cadastro do importador CTE

@type function
@author Sam Barros
@since 30/06/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SASP080()
	//---------------------------------------------------------//
	//Declara��o de vari�veis
	//---------------------------------------------------------//
	Private cPerg   	:= "1"
	Private cCadastro 	:= "Cadastro do importador CTE"
	Private cDelFunc 	:= ".T." // Validacao para a exclusao. Pode-se utilizar ExecBlock
	Private cString 	:= "ZB0"
	Private cFiltro		:= ""
	Private	aIndexZB0	:= {}
	Private aIndex		:= {}
	Private cCodUse		:= RetCodUsr()
	Private bFiltraBrw 	:= { || FilBrowse( "ZB0" , @aIndex , @cFiltro ) } //Determina a Expressao do Filtro
	Private aRotina 	:= {}
	Private aRotAprov 	:= {}
	Private aRotStts 	:= {}
	Private aCores		:= { }
		
	AADD(aRotina, {"Pesquisar"		,"AxPesqui"		,0,1})
	AADD(aRotina, {"Visualizar"		,"AxVisual"		,0,2})
	AADD(aRotina, {"Incluir"		,"AxInclui"		,0,3})
	AADD(aRotina, {"Alterar"		,"AxAltera"		,0,4})
	AADD(aRotina, {"Excluir"		,"AxDeleta"		,0,5})
	AADD(aRotina, {"Legenda"      	,"u_legZB0()"	,0,6})
	
	AADD(aCores, { 'ZB0->ZB0_MSBLQL ==   "1"'	    , 'BR_VERMELHO'		})
	AADD(aCores, { 'ZB0->ZB0_MSBLQL ==   "2"'    	, 'BR_VERDE'		})
	
	dbSelectArea("ZB0")
	dbSetOrder(1)

	cPerg   := "1"

	Pergunte(cPerg,.F.)

	dbSelectArea(cString)
	mBrowse( 6,1,22,75,cString,,,,,6,aCores)

	Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros

	ZB0->(dbCloseArea())
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} LegZB0

Legenda da tabela ZB0
[Verde] 	- Ativo
[Vermelho]	- Inativo  

@type function
@author Sam Barros
@since 06/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
USER Function LegZB0()
	Local aLegenda := 	{;
							{"BR_VERDE"			, "Ativo"},;
							{"BR_VERMELHO" 		, "Inativo"};
						}
	BrwLegenda(cCadastro, "Legenda", aLegenda)
Return .T.