#Include 'Protheus.ch'
#INCLUDE 'FWMBROWSE.CH'
#INCLUDE 'FWMVCDEF.CH'
#include 'TopConn.CH'

User Function SASP090(A)
	Local aArea := getArea()
	Local oDlgNF
	Local oBrwNF
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Local cTextHead	:= "QLabel { color: rgb(39,82,102); font-size: 12px; font-weight: bold;  } "
	Private aObjTela	:= {}
	Private aCampos		:= {}
	
	Private _nNumber 
	Private _cTmp 

	Private _cSeqImp 
	Private _cChvCTE 
	Private _cTomador 
	Private _cRemetente 
	Private _cDestinatario
	Private _cTransportadora 
	
	Private _nSkip
	Private _nLin 	  
	
	Private _nColTit1 
	Private _nColDad1
	
	Private _nColTit2 
	Private _nColDad2 
	
	Private _p_cNrImp := A
		  
	oDlgNF := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Dados NFE',,,,nOr(WS_VISIBLE,WS_POPUP),,,,,.T.,,,,)

	@00,00 MSPANEL oBarNFE SIZE 50,50 of oDlgNF
	oBarNFE:Align := CONTROL_ALIGN_TOP
	
	//-------------------VARIAVEIS---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	_nNumber := 1
	_cTmp := StrZero(_nNumber,5,0)
	
	_cSeqImp 		:= ""
	_cChvCTE 		:= ""
	_cTomador 		:= ""
	_cRemetente 	:= ""
	_cDestinatario 	:= ""
	
	_cCTE 			:= ""
	_cValFrete 		:= ""
	_cPeso 			:= ""
	_cVolume 		:= ""
	_cEmissao 		:= ""
	
	_nSkip	  := 007
	_nLin 	  := 001
	
	_nColTit1 := 010
	_nColDad1 := 080
	
	_nColTit2 := 290
	_nColDad2 := 330
	
	AtualTela()
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColTit1, {||"Sequ�ncia Importa��o: "}	,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColTit1, {||"Chave CTE: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColTit1, {||"Transportador: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColTit1, {||"Remetente: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColTit1, {||"Destinat�rio: "}			,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 5*_nSkip,_nColTit1, {||"Tomador: "}			,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColDad1, {||_cSeqImp}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColDad1, {||_cChvCTE}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColDad1, {||_cTransportador}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColDad1, {||_cRemetente}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)	
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColDad1, {||_cDestinatario}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 5*_nSkip,_nColDad1, {||_cTomador}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColTit2, {||"CTE: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColTit2, {||"Valor Frete: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColTit2, {||"Peso Total: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColTit2, {||"Volume: "}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColTit2, {||"Emiss�o: "}			,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	
	oSayNFE := TSay():New(_nLin + 0*_nSkip,_nColDad2, {||_cCTE}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 1*_nSkip,_nColDad2, {||_cValFrete}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 2*_nSkip,_nColDad2, {||_cPeso}					,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 3*_nSkip,_nColDad2, {||_cVolume}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)	
	AADD(aObjTela,oSayNFE)
	
	oSayNFE := TSay():New(_nLin + 4*_nSkip,_nColDad2, {||_cEmissao}				,oBarNFE, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSayNFE:SetCss(cTextHead)
	AADD(aObjTela,oSayNFE)
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	//Instaciamento
	oBrwNF := FWMBrowse():New()
	oBrwNF:SetMenuDef("SASP090") 
	
	//Disabilita a tela de detalhes
	oBrwNF:disableDetails()
	
	//tabela que ser� utilizadaadmin
	oBrwNF:SetAlias( "ZB9" )
	SET FILTER TO (ZB9_NRIMP = _p_cNrImp) 
	
	//Titulo
	oBrwNF:SetDescription( "Itens NFE" )
	
	//ativa
	oBrwNF:Activate(oDlgNF)
	
	oBrwNF:Refresh()
	
	oDlgNF:Activate(,,,.T.)
Return


Static Function MenuDef()
	Private aRotinaNF := {}
		
	ADD OPTION aRotinaNF	TITLE 'Ocorr�ncia'	 	ACTION 'U_SASP090A()' OPERATION 10 ACCESS 0
	ADD OPTION aRotinaNF	TITLE 'Abrir. NFE'	 	ACTION 'U_SASP090B()' OPERATION 3 ACCESS 0
Return aRotinaNF

Static Function AtualTela()
	Local cAlias	:= GetNextAlias()
	Local aArea		:= GetArea()
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	dbGoTop()
	dbSeek(xFilial("ZB8")+_p_cNrImp)
	
	_cSeqImp 		:= ZB8->ZB8_NRIMP
	_cChvCTE 		:= ZB8->ZB8_CTE
	if !(Empty(AllTrim(ZB8->ZB8_CDREM)))
		_cRemetente 	:= AllTrim(ZB8->ZB8_CDREM)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDREM,"SA2->A2_NREDUZ")
	Else
		_cRemetente 		:= ""
	EndIf
	if !(Empty(AllTrim(ZB8->ZB8_EMISDF)))
		_cTransportador	:= AllTrim(ZB8->ZB8_EMISDF)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_EMISDF,"SA2->A2_NREDUZ")
	Else
		_cTransportador 		:= ""
	EndIf
	if !(Empty(AllTrim(ZB8->ZB8_CDDEST)))
		_cDestinatario 	:= AllTrim(ZB8->ZB8_CDDEST)	+ " - " + POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDDEST,"SA1->A1_NREDUZ")
	Else
		_cDestinatario 		:= ""
	EndIf
	if !(Empty(AllTrim(ZB8->ZB8_TOMADO)))
		_cTomador 		:= AllTrim(ZB8->ZB8_TOMADO)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_TOMADO,"SA2->A2_NREDUZ")
	Else
		_cTomador 		:= ""
	EndIf
	
	_cCTE 			:= AllTrim(ZB8->ZB8_NRDF)+"/"+ZB8->ZB8_SERDF
	_cValFrete 		:= AllTrim(Transform(ZB8->ZB8_VLDF	,PesqPict("ZB8"	, "ZB8_VLDF")))
	_cPeso 			:= AllTrim(Transform(ZB8->ZB8_FRPESO	,PesqPict("ZB8", "ZB8_PESOR")))
	_cVolume 		:= AllTrim(Transform(ZB8->ZB8_QTVOL,PesqPict("ZB8", "ZB8_QTVOL")))
	_cEmissao 		:= DtoC(ZB8->ZB8_DTEMIS)

	ZB8->(dbCloseArea())
	RestArea(aArea)
	ProcessMessage()
Return

Static Function CssbtnImg(cImg,cCor)
	Local cRet	:= " QPushButton{ background-image: url(rpo:"+cImg+")"+;
	" ;border-style:solid"+;
	" ;border-width:5px"+;
	" ;background-repeat: none; margin: 1px "+;
	" ;background-color: "+cCor+""+;
	" ;border-radius: 6px"+;
	" ;font-weight: bold;font-size: 12px"+;
	" ;color: rgb(255,255,255) "+;
	"}"+;
	"QPushButton:hover { "+;
	" ;background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop:0 "+cCor+", stop:0.5 "+cCor+", stop:1 white)"+;
	"}"
Return cRet

User function SASP090A()
	//MsgInfo("Implementar Fun��o Ocorr�ncias","[SASP090] - Gest�o de Fretes")
	U_SASP091(_p_cNrImp)
Return

User function SASP090B()
	dbSelectArea("SF2")
	SF2->(dbSetOrder(10))
	SF2->(dbGotop())
	cChaveBusca := getFilDoc(ZB9->ZB9_NRIMP)+ZB9->ZB9_DANFE
	
	If SF2->(dbSeek(alltrim(cChaveBusca)))
		Mc090Visual( "SF2", SF2->( recno() ), 1 )	
	Else
		Alert("N�o existe registro de nota fiscal com a chave: " + CRLF + ZB9->ZB9_DANFE)
	EndIf
	
	SF2->(dbCloseArea())
Return
/*
User function SASP090C()
	MsgInfo("Implementar Fun��o Aprovadores","[SASP090] - Gest�o de Fretes")
Return

User function SASP090D()
	MsgInfo("Implementar Fun��o Ger. Fatura","[SASP090] - Gest�o de Fretes")
Return

User function SASP090E()
	MsgInfo("Implementar Fun��o Refaz Conf.","[SASP090] - Gest�o de Fretes")
Return
*/

Static Function getFilDoc(cSeqImp)
	Local cRet := ""
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	If dbSeek(xFilial("ZB8")+cSeqImp)
		cRet := ZB8->ZB8_FILDOC
	EndIf
return cRet	