#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP083

Cadastro de aprovadores

@type function
@author Sam Barros
@since 07/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SASP085()
	AxCadastro("ZB5", "Cadastro de aprovadores", ".T.", ".T.")
Return