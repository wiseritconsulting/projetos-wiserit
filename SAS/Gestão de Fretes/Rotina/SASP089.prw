#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP089

Cadastro hist�rico de fretes

@type function
@author Sam Barros
@since 30/06/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SASP089()
	//---------------------------------------------------------//
	//Declara��o de vari�veis
	//---------------------------------------------------------//
	Local cImportadores := U_getCodImport("2;4;6;7")
	
	Private cPerg   	:= "1"
	Private cCadastro 	:= "Cadastro hist�rico de fretes"
	Private cDelFunc 	:= ".T." // Validacao para a exclusao. Pode-se utilizar ExecBlock
	Private cString 	:= "ZBB"
	Private cFiltro		:= ""
	Private	aIndexZBB	:= {}
	Private aIndex		:= {}
	Private cCodUse		:= RetCodUsr()
	Private bFiltraBrw 	:= { || FilBrowse( "ZBB" , @aIndex , @cFiltro ) } //Determina a Expressao do Filtro
	Private aRotina 	:= {}
	Private aRotAprov 	:= {}
	Private aRotStts 	:= {}
	Private aCores		:= { }
		
	AADD(aRotina, {"Pesquisar"		,"AxPesqui"		,0,1})
	AADD(aRotina, {"Visualizar"		,"AxVisual"		,0,2})
	if RetCodUsr() $ cImportadores	
		AADD(aRotina, {"Incluir"		,"AxInclui"		,0,3})
		//AADD(aRotina, {"Alterar"		,"AxAltera"		,0,4})
		AADD(aRotina, {"Excluir"		,"AxDeleta"		,0,5})
		AADD(aRotina, {"Import Hist"    ,"u_SASF004()"	,0,3})
	EndIf
	AADD(aRotina, {"Legenda"      	,"u_legZBB()"	,0,6})
	
	aCores := getModal()
	
	dbSelectArea("ZBB")
	dbSetOrder(1)

	cPerg   := "1"

	Pergunte(cPerg,.F.)

	dbSelectArea(cString)
	mBrowse( 6,1,22,75,cString,,,,,6,aCores)

	Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros

	ZBB->(dbCloseArea())
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} LegZBB

Legenda da tabela ZBB 

@type function
@author Sam Barros
@since 06/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
USER Function LegZBB()
	Local aLegenda := {}
						
	dbSelectArea("ZBA")
	dbSetOrder(1)
	dbGoTop()
	while !(ZBA->(EOF()))
		AADD(aLegenda, {'BR_'+getCor(val(ZBA->ZBA_CORLEG))+'', ZBA->ZBA_DESC})
		ZBA->(dbSkip())
	EndDo
	ZBA->(dbCloseArea())
	
	BrwLegenda(cCadastro, "Legenda", aLegenda)
Return .T.

Static Function getModal()
	Local aRet := {}
	
	dbSelectArea("ZBA")
	dbSetOrder(1)
	dbGoTop()
	while !(ZBA->(EOF()))
		AADD(aRet, { 'ZBB->ZBB_MODAL ==   "'+ZBA->ZBA_COD+'"'	    , 'BR_'+getCor(val(ZBA->ZBA_CORLEG))+''		})
		ZBA->(dbSkip())
	EndDo
	ZBA->(dbCloseArea())
Return aRet

Static Function getCor(nPos)
	Local aVet := {}
	
	AADD(aVet,"AMARELO")
	AADD(aVet,"AZUL")
	AADD(aVet,"BRANCO")
	AADD(aVet,"CINZA")
	AADD(aVet,"LARANJA")
	AADD(aVet,"MARROM")
	AADD(aVet,"PINK")
	AADD(aVet,"PRETO")
	AADD(aVet,"VERDE")
	AADD(aVet,"VERMELHO") 
Return aVet[nPos]