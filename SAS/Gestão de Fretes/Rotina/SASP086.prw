#Include "Protheus.ch"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP086

Cadastro de Produto por Produção 

@type function
@author Samarony Barros
@since 18/02/2014
@version 1.0	
/*/
//-------------------------------------------------------------------

User Function SASP086()
	Local oBrowse
	
	Private cFSWRotina	:= "SASP086"
	Private cTabela 	:= "ZB6"
	Private cIniCampo 	:= IIF(SUBSTR(cTabela,1,1)=='S',SUBSTR(cTabela,2,2),cTabela)
	
	Private cGrid 		:= "ZB7"
	Private cIniGrid 	:= IIF(SUBSTR(cGrid,1,1)=='S',SUBSTR(cGrid,2,2),cGrid)
	
	Private cDescRot 	:= "Cadastro Grupo de AProvação"
	
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias(cTabela)
	oBrowse:SetDescription(cDescRot)
	oBrowse:SetMenuDef( 'SASP086' )
	oBrowse:Activate()
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef

Monta o menu
	
@type function
@author Samarony Barros
@since 18/02/2014
@version 1.0	
/*/
//-------------------------------------------------------------------
Static Function MenuDef()
	Private aRotina := {}
	
	ADD OPTION aRotina TITLE "Pesquisar"  ACTION "VIEWDEF.SASP086" OPERATION 1 ACCESS 0// DISABLE MENU
	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.SASP086" OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.SASP086" OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.SASP086" OPERATION 4 ACCESS 143
	ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.SASP086" OPERATION 5 ACCESS 144
	ADD OPTION aRotina TITLE "Copiar"     ACTION "VIEWDEF.SASP086" OPERATION 9 ACCESS 0
	ADD OPTION aRotina TITLE "Imprimir"   ACTION "VIEWDEF.SASP086" OPERATION 8 ACCESS 0
Return aRotina
//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef

Gera as definições do modelo

@type function	
@author Samarony Barros
@since 18/02/2014
@version 1.0		
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	Local oStructHead	:= Nil
	Local oStructBody	:= Nil
	Local oModel    	:= Nil
	Local cDefCampos	:= ""

	cDefCampos		:= cIniCampo+"_COD|"+cIniCampo+"_DESC|"
	cDefGrid		:= cIniGrid+"_ITEM|" + cIniGrid+"_APROV|"+cIniGrid+"_DESCAP|"+cIniGrid+"_LIMMIN|"+cIniGrid+"_LIMMAX|"
	
	oStructHead 	:= FWFormStruct(1,cTabela,	{|cCampo|  AllTrim(cCampo)+"|" $ cDefCampos})
	oStructBody 	:= FWFormStruct(1,cGrid,	{|cGrid |  AllTrim(cGrid )+"|" $ cDefGrid})

	oModel:= MPFormModel():New("SAS086", /*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)

	oModel:AddFields("SAS086_HEAD", /*cOwner*/, oStructHead ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)
	oModel:GetModel("SAS086_HEAD"):SetDescription(cDescRot)
	
	oModel:AddGrid("SAS086_BODY", "SAS086_HEAD", oStructBody , ,{ |oModelGrid, nLine, cAction, cField| .T. },/*bPre*/,/*bPost*/,/*Carga*/)
	oModel:GetModel("SAS086_BODY"):SetUseOldGrid()
	oModel:GetModel("SAS086_BODY"):SetUniqueLine({cIniGrid+"_APROV"})
	
	oModel:SetPrimaryKey({cIniCampo+"_FILIAL",cIniCampo+"_COD", cIniGrid+"_APROV"})
	oModel:SetRelation("SAS086_BODY",{{cIniGrid+"_FILIAL",'xFilial("'+cTabela+'")'},{cIniGrid+"_COD",cIniCampo+"_COD"}},(cGrid)->(IndexKey()))	
Return(oModel)

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef

Gera as definições de visualização
	
@type function
@author Samarony Barros
@since 18/02/2014
@version 1.0		
/*/
//-------------------------------------------------------------------
Static Function ViewDef()
	Local oView		:= Nil
	Local oStructHead	:= Nil
	Local oStructBody	:= Nil
	Local oModel		:= FWLoadModel("SASP086")
	Local cDefCampos	:= ""

	cDefCampos		:= cIniCampo+"_COD|"+cIniCampo+"_DESC|"
	cDefGrid		:= cIniGrid+"_ITEM|" + cIniGrid+"_APROV|"+cIniGrid+"_DESCAP|"+cIniGrid+"_LIMMIN|"+cIniGrid+"_LIMMAX|"
	
	oStructHead 	:= FWFormStruct(2,cTabela,	{|cCampo|  AllTrim(cCampo)+"|" $ cDefCampos})
	oStructBody 	:= FWFormStruct(2,cGrid,	{|cGrid |  AllTrim(cGrid )+"|" $ cDefGrid})

	oStructHead:RemoveField( cIniCampo+"_FILIAL" )
	oStructBody:RemoveField( cIniGrid+"_FILIAL" )

	oView := FWFormView():New()

	oView:SetModel(oModel)

	oView:AddField( "SAS086_HEAD",oStructHead)
	oView:CreateHorizontalBox("CABEC",20)
	oView:SetOwnerView( "SAS086_HEAD","CABEC")
	oView:EnableControlBar(.T.)

	oView:AddGrid("SAS086_BODY",oStructBody)
	oView:AddIncrementField("SAS086_BODY",cIniGrid+"_ITEM")
	oView:CreateHorizontalBox("GRID",80)
	oView:SetOwnerView( "SAS086_BODY","GRID")
Return oView