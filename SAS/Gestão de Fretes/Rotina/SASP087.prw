#Include 'Protheus.ch'
#INCLUDE 'FWMBROWSE.CH'
#INCLUDE 'FWMVCDEF.CH'
#include 'TopConn.CH'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087

Rotina de importa��o do CTE.
Esta rotina � a principall rotina do m�dulo de gest�o de fretes.
A partir daqui � poss�vel realizar a importa��o e 
valida��es dos CTEs

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SASP087()
	
	Local aCoors	:= FWGetDialogSize( oMainWnd )
	Local cTextHead	:= "QLabel { color: rgb(39,82,102); font-size: 12px; font-weight: bold;  } "
	Private aObjTela	:= {}
	Private aCampos		:= {}
	Private oDlg
	Private oBrw
	
	Private _nNumber
	Private _cTmp

	Private _cSeqImp
	Private _cChvCTE
	Private _cTransportador
	Private _cRemetente
	Private _cDestinatario
	Private _cTomador

	Private _nSkip
	Private _nLin
	
	Private _nColTit1
	Private _nColDad1
	
	Private _nColTit2
	Private _nColDad2
	
	//Private aRotina   := MenuDef() 
		  
	oDlg := MSDialog():New(aCoors[1],aCoors[2],aCoors[3],aCoors[4],'Importa XML',,,,nOr(WS_VISIBLE,WS_POPUP),,,,,.T.,,,,)

	@00,00 MSPANEL oBar SIZE 50,50 of oDlg
	oBar:Align := CONTROL_ALIGN_TOP
	
	//-------------------VARIAVEIS---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	_nNumber := 1
	_cTmp := StrZero(_nNumber,5,0)
	
	_cSeqImp 		:= ""
	_cChvCTE 		:= ""
	_cTransportador 		:= ""
	_cRemetente 	:= ""
	_cDestinatario 	:= ""
	_cTomador	 	:= ""
	
	_cCTE 			:= ""
	_cValFrete 		:= ""
	_cPeso 			:= ""
	_cVolume 		:= ""
	_cEmissao 		:= ""
	
	_nSkip	  := 007
	_nLin 	  := 001
	
	_nColTit1 := 010
	_nColDad1 := 080
	
	_nColTit2 := 290
	_nColDad2 := 330
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	oSay := TSay():New(_nLin + 0*_nSkip,_nColTit1, {||"Sequ�ncia Importa��o: "}	,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 1*_nSkip,_nColTit1, {||"Chave CTE: "}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 2*_nSkip,_nColTit1, {||"Transportador: "}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 3*_nSkip,_nColTit1, {||"Remetente: "}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 4*_nSkip,_nColTit1, {||"Destinat�rio: "}			,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 5*_nSkip,_nColTit1, {||"Tomador: "}			,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 0*_nSkip,_nColDad1, {||_cSeqImp}					,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	
	oSay := TSay():New(_nLin + 1*_nSkip,_nColDad1, {||_cChvCTE}					,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	
	oSay := TSay():New(_nLin + 2*_nSkip,_nColDad1, {||_cTransportador}					,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	
	oSay := TSay():New(_nLin + 3*_nSkip,_nColDad1, {||_cRemetente}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	
	oSay := TSay():New(_nLin + 4*_nSkip,_nColDad1, {||_cDestinatario}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	
	oSay := TSay():New(_nLin + 5*_nSkip,_nColDad1, {||_cTomador}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	oSay := TSay():New(_nLin + 0*_nSkip,_nColTit2, {||"CTE: "}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 1*_nSkip,_nColTit2, {||"Valor Frete: "}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 2*_nSkip,_nColTit2, {||"Peso Total: "}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 3*_nSkip,_nColTit2, {||"Volume: "}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 4*_nSkip,_nColTit2, {||"Emiss�o: "}			,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	
	oSay := TSay():New(_nLin + 0*_nSkip,_nColDad2, {||_cCTE}					,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	
	oSay := TSay():New(_nLin + 1*_nSkip,_nColDad2, {||_cValFrete}					,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	
	oSay := TSay():New(_nLin + 2*_nSkip,_nColDad2, {||_cPeso}					,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	
	oSay := TSay():New(_nLin + 3*_nSkip,_nColDad2, {||_cVolume}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	
	oSay := TSay():New(_nLin + 4*_nSkip,_nColDad2, {||_cEmissao}				,oBar, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/,.T./*<.lPixel.>*/,/* <nClrText>*/,/* <nClrBack>*/, 500, 11,/*<.design.>*/,/* <.update.>*/,/* <.lShaded.>*/,/* <.lBox.>*/,/* <.lRaised.>*/,/*<.lHtml.>*/)
	oSay:SetCss(cTextHead)
	AADD(aObjTela,oSay)
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	//Instaciamento
	oBrw := FWMBrowse():New()
	oBrw:SetMenuDef("SASP087")
	
	//Disabilita a tela de detalhes
	oBrw:disableDetails()
	
	//tabela que ser� utilizada
	oBrw:SetAlias( "ZB8" )
	//SET FILTER TO (ZB8_FILDOC = cFilAnt) 
	
	//Titulo
	oBrw:SetDescription( "Itens CTE" )
	
	oBrw:AddLegend("ZB8_STATUS=='FG'", "BR_PRETO"		,"Fatura Gerada")
	oBrw:AddLegend("ZB8_STATUS=='AG'", "BR_AZUL"		,"Aguardando Aprova��o")
	oBrw:AddLegend("ZB8_STATUS=='AA'", "BR_VERDE"		,"Aprovado")
	oBrw:AddLegend("ZB8_STATUS=='PE'", "BR_AMARELO"		,"Pedente")
	oBrw:AddLegend("ZB8_STATUS=='AV'", "BR_AZUL_CLARO"	,"Aguardando Visto Ocorr�ncia")
	oBrw:AddLegend("ZB8_STATUS=='BL'", "BR_VERMELHO"	,"Bloqueado")
	oBrw:AddLegend("ZB8_STATUS=='DE'", "BR_VIOLETA"		,"Documento de Entrada Gerado")
	oBrw:AddLegend("ZB8_STATUS=='SA'", "BR_BRANCO"		,"Solicitado Anula��o")
	
	oBrw:setChange({||AtualTela(),aEval(aObjTela,{|x| x:Refresh()})})
	
	//ativa
	oBrw:Activate(oDlg)
	
	//oBrw:FWBrowse():Browse():bCursorMove := ({||AtualTela(),aEval(aObjTela,{|x| x:Refresh()})})
	//oTimer := TTimer():New(1,{||AtualTela(),aEval(aObjTela,{|x| x:Refresh()})}, oDlg )
	//oTimer:Activate()
	
	oDlg:Activate(,,,.T.)
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef

Cria o menu

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------

Static Function MenuDef()
	Local aNota	:= {}
	Local aPedido	:= {}
	Local aDevol	:= {}
	
	Local cImportadores := U_getCodImport("3;5;6;7")
	
	Private aRotCTE := {}
		
	ADD OPTION aRotCTE	TITLE 'Visualizar'	 		ACTION 'U_SASP087A()' OPERATION 2 ACCESS 0
	ADD OPTION aRotCTE	TITLE 'Aprovadores'	 		ACTION 'U_SASP087C()' OPERATION 2 ACCESS 0
	//ADD OPTION aRotCTE	TITLE 'Ger. Fatura'	 	ACTION 'U_SASP087D()' OPERATION 2 ACCESS 0
	if RetCodUsr() $ cImportadores
		ADD OPTION aRotCTE	TITLE 'Imp. Xml. CTE'	ACTION 'U_SASP087E()' OPERATION 3 ACCESS 0
	EndIf
	ADD OPTION aRotCTE  TITLE 'Re-Envia E-mail'		ACTION 'U_SASP087F()' OPERATION 2 ACCESS 0
	ADD OPTION aRotCTE  TITLE 'Ocorr�ncias'			ACTION 'U_SASP087G()' OPERATION 2 ACCESS 0
	ADD OPTION aRotCTE  TITLE 'Gera Doc. Ent.'		ACTION 'U_SASP087H()' OPERATION 2 ACCESS 0
	If RetCodUsr() == GetMV("SA_MASTER")
		ADD OPTION aRotCTE  TITLE 'Libera Master'	ACTION 'U_SASP087I()' OPERATION 2 ACCESS 0
	EndIf
	ADD OPTION aRotCTE  TITLE 'Ajustar Modal'		ACTION 'U_SASP087J()' OPERATION 2 ACCESS 0
	ADD OPTION aRotCTE  TITLE 'Refaz Conferencia'	ACTION 'U_SASP087K()' OPERATION 2 ACCESS 0
	ADD OPTION aRotCTE  TITLE 'Classificar'			ACTION 'U_SASP087L()' OPERATION 2 ACCESS 0
	//ADD OPTION aRotCTE  TITLE 'MSG'	ACTION 'U_msgLogCTE()' OPERATION 2 ACCESS 0
Return aRotCTE

//-------------------------------------------------------------------
/*/{Protheus.doc} AtualTela

Atualiza tela com os dados do CTE corrente

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function AtualTela()
	Local cAlias	:= GetNextAlias()
	Local aArea		:= GetArea()
	
	_xp_cNrImp := ZB8->ZB8_NRIMP
	
	_cSeqImp 		:= ZB8->ZB8_NRIMP
	_cChvCTE 		:= ZB8->ZB8_CTE
	if !(Empty(AllTrim(ZB8->ZB8_CDREM)))
		_cRemetente 	:= AllTrim(ZB8->ZB8_CDREM)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDREM,"SA2->A2_NREDUZ")
	Else
		_cRemetente 		:= ""
	EndIf
	if !(Empty(AllTrim(ZB8->ZB8_EMISDF)))
		_cTransportador	:= AllTrim(ZB8->ZB8_EMISDF)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_EMISDF,"SA2->A2_NREDUZ")
	Else
		_cTransportador 		:= ""
	EndIf
	if !(Empty(AllTrim(ZB8->ZB8_CDDEST)))
		_cDestinatario 	:= AllTrim(ZB8->ZB8_CDDEST)	+ " - " + POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDDEST,"SA1->A1_NREDUZ")
	Else
		_cDestinatario 		:= ""
	EndIf
	if !(Empty(AllTrim(ZB8->ZB8_TOMADO)))
		_cTomador 		:= AllTrim(ZB8->ZB8_TOMADO)	+ " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_TOMADO,"SA2->A2_NREDUZ")
	Else
		_cTomador 		:= ""
	EndIf
	
	_cCTE 			:= AllTrim(ZB8->ZB8_NRDF)+"/"+ZB8->ZB8_SERDF
	_cValFrete 		:= AllTrim(Transform(ZB8->ZB8_VLDF	,PesqPict("ZB8"	, "ZB8_VLDF")))
	_cPeso 			:= AllTrim(Transform(ZB8->ZB8_PREAL	,PesqPict("ZB8"	, "ZB8_PREAL")))
	_cVolume 		:= AllTrim(Transform(ZB8->ZB8_QTVOL,PesqPict("ZB8"	, "ZB8_QTVOL")))
	_cEmissao 		:= DtoC(ZB8->ZB8_DTEMIS)

	RestArea(aArea)
	ProcessMessage()
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087A

Fun��o que chama a visualiza��o das NF-e

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User function SASP087A()
	U_SASP090(_cSeqImp)
Return


User function SASP087B()
	MsgInfo("Implementar Fun��o Incluir","[SASP087] - Gest�o de Fretes")
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087B

Fun��o que vai chamar a tela de gera��o de aprovadores

@type function 
@author Sam Barros
@since 24/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User function SASP087C()
	U_SASF009(ZB8->ZB8_NRIMP, oDlg)
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087D

Fun��o que vai chamar a tela de gera��o de faturas

@type function 
@author Sam Barros
@since 24/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------

User function SASP087D()
	//MsgInfo("Implementar Fun��o Ger. Fatura","[SASP087] - Gest�o de Fretes")
	U_SASP095()
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087E

Fun��o respons�vel por realizar a importa��o dos CTE atrav�s dos
XMLs contidos na pasta cadastrada no par�metro.

Ao final, realiza a valida��o dos XML atrav�s das informa��es
contidas na tabela de ocorr�ncias.

EM seguida, marca todos os CTE como verificados. 

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User function SASP087E()
	Local xArea
	Local yArea
	if MsgYesNo("Deseja Importar os XMLs contidos na pasta " + GetMv("MV_XMLDIR") + "?","Importa��o CTE")
		xArea := getArea()
		U_SF005IMP()
		RestArea(xArea)
		yArea := getArea()
		MAKELOGCABEC()
		U_SASF006()
		U_CONFERE()
		U_MSGLOGCTE()
		RestArea(yArea)
	EndIF
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087E

Re-envia email para aprovadores
Ocorrencia ou Al�ada 
@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User function SASP087F()
	U_EmailGF(ZB8->ZB8_NRIMP, 'O')
	/*
	Local aAprov := getAprOco(ZB8->ZB8_NRIMP)
	
	If !Empty(aAprov)
		cHTML := U_SASW015({ZB8->ZB8_NRIMP, ;
							ZB8->ZB8_NRDF + '/' + ZB8->ZB8_SERDF, ;
							ZB8->ZB8_FILDOC, ;
							Transform(ZB8->ZB8_VLDF, PesqPict("ZB8","ZB8_VLDF")), ;
							DtoC(ZB8->ZB8_DTEMIS), ;
							_cTransportador, ;
							_cRemetente, ;
							_cDestinatario}, ;
							aAprov)
							
		u_SendMail("samarony@objetiveti.com.br","Gest�o de Fretes ",cHtml,,)
	Else 
		Alert("Este CTE n�o tem aprovadores")
	EndIf
	*/
Return


//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087G

Fun��o que mostra as Ocorr�ncias

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User function SASP087G()
	U_SASP091(_cSeqImp)
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087H

Gerar documento de Entrada

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User function SASP087H()
	Processa( {|| U_SASPO87H() }, "Aguarde...", "Gerando documento de entrada...",.F.)
Return

User Function SASPO87H()
	Local aCabec := {}
	Local aItens := {}
	Local aLinha := {}
	
	Local cProd := POSICIONE("ZBA", 1, xFIlial("ZBA")+ZB8->ZB8_MODAL, "ZBA->ZBA_PROD")
	Local cUm 	:= POSICIONE("SB1",1,xFilial("SB1")+cProd, "SB1->B1_UM")
	Local cTp 	:= POSICIONE("SB1",1,xFilial("SB1")+cProd, "SB1->B1_TIPO")
	
	//Local cTES 		:= GetMV("SA_TESCTE")
	//Local cCP 		:= GetMV("SA_CPCTE")
	//Local cNatureza := GetMV("SA_NATCTE")
	//Local cCusto	:= GetMV("SA_CCCTE")
	
	Local cFornece := ""
	Local cLoja := ""
	Local cUF 	:= ""
	
	//TODO: Natureza e Centor de custo
	
	if ZB8->ZB8_STATUS == 'AA'
		if MsgYesNo("Deseja Gerar documento de entrada para o CTE: " + AllTrim(ZB8->ZB8_NRDF) + "/" + AllTrim(ZB8->ZB8_SERDF) )
			cFornece 	:= SUBS(ZB8->ZB8_CDREM,1,6)
			cLoja 		:= SUBS(ZB8->ZB8_CDREM,7,2)
			cUF 		:= POSICIONE("SA2",1,xFilial("SA2")+cFornece+cLoja, "SA2->A2_EST")
			
			Aadd(aCabec,{"F1_FILIAL"	,ZB8->ZB8_FILDOC			,Nil})
			Aadd(aCabec,{"F1_DOC"		,ZB8->ZB8_NRDF 				,Nil})
			Aadd(aCabec,{"F1_SERIE"		,ZB8->ZB8_SERDF				,Nil})
			Aadd(aCabec,{"F1_FORNECE"	,cFornece					,Nil})
			Aadd(aCabec,{"F1_LOJA"		,cLoja						,Nil})
			Aadd(aCabec,{"F1_COND"		,ZB8->ZB8_CODPAG			,Nil})
			Aadd(aCabec,{"F1_EMISSAO"	,ZB8->ZB8_DTEMIS			,Nil})
			Aadd(aCabec,{"F1_FORMUL"	,"N"               			,Nil})
			Aadd(aCabec,{"F1_ESPECIE"	,"CTE"               		,Nil})
			Aadd(aCabec,{"F1_TIPO"		,"N"               			,Nil})
			Aadd(aCabec,{"F1_DTDIGIT"	,dDataBase         			,Nil})
			Aadd(aCabec,{"F1_EST"		,cUF         				,Nil})
			aadd(aCabec,{"E2_NATUREZ"	,ZB8->ZB8_NATURE			,Nil})
			
			//Aadd(aLinha,{"D1_FILIAL"	,ZB8->ZB8_FILDOC			,Nil})
			//Aadd(aLinha,{"D1_DOC"		,""     					,Nil})
			//Aadd(aLinha,{"D1_ITEM"		,"001"						,Nil})
			Aadd(aLinha,{"D1_COD"		,cProd						,Nil})
			//Aadd(aLinha,{"D1_UM"        ,cUm          				,Nil})
			Aadd(aLinha,{"D1_QUANT"     ,1          				,Nil})
			Aadd(aLinha,{"D1_VUNIT"     ,ZB8->ZB8_VLDF          	,Nil})
			Aadd(aLinha,{"D1_TOTAL"     ,ZB8->ZB8_VLDF				,Nil})
			Aadd(aLinha,{"D1_TES"		,ZB8->ZB8_TES				,Nil})
			//Aadd(aLinha,{"D1_FORNECE"	,SUBS(ZB8->ZB8_CDREM,1,6)	,Nil})
			//Aadd(aLinha,{"D1_LOJA"		,SUBS(ZB8->ZB8_CDREM,7,2)	,Nil})
			//Aadd(aLinha,{"D1_LOCAL"     ,"01"         				,Nil})
			//Aadd(aLinha,{"D1_EMISSAO"	,ZB8->ZB8_DTEMIS			,Nil})
			//Aadd(aLinha,{"D1_FORMUL"	,"N"               			,Nil})
			//Aadd(aLinha,{"D1_CF"		,ZB8->ZB8_CFOP				,Nil})
			Aadd(aLinha,{"D1_CC"		,ZB8->ZB8_CC				,Nil})
			//Aadd(aCabec,{"D1_DTDIGIT"	,dDataBase         			,Nil})
			//Aadd(aCabec,{"D1_TP"		,cTp	         			,Nil})
			//Aadd(aLinha,{"AUTDELETA"	,"N"               			,Nil})
			                
			Aadd(aItens,aLinha)
			
			U_SASF007(aCabec, aItens, ZB8->ZB8_NRIMP, .T.)
			//Processa( {|| U_SASF007(aCabec, aItens, ZB8->ZB8_NRIMP) }, "Aguarde...", "Gerando documento de entrada...",.F.)
		EndIf
	Else
		cStatus := getStatus(ZB8->ZB8_STATUS)
		MsgAlert("o Documento marcado encontra-se " + cStatus + " e n�o pode gerar documento de entrada")
	EndIf
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087I

Libera��o do usu�rio master

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User function SASP087I()
	Local _cMsg := ""
	
	_cMsg += "Deseja Liberar este CTE para a gera��o do documento de entrada?" + CRLF + CRLF
	_cMsg += "Aten��o: Todo o fluxo do Processo ser� interrompido saltando diretamente para "
	_cMsg += "o documento de entrada, tornando o documento de fretes apto para a fatura. "
	
	if ZB8->ZB8_STATUS <> 'FG'
		If MsgYesNo(_cMsg)
			RecLock("ZB8", .F.)
			ZB8->ZB8_STATUS := 'AA'
			MsUnLock()
		EndIf
	ElSe
		MsgAlert("O Documento de Fretes selecionado j� foi encerrado, pois j� existe fatura gerada." + CRLF + "Favor selecionar outro CTE!")
	EndIf
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087J

Ajustar Modal

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User function SASP087J()
	Local aModal := {}
	Local aParamBox := {}
	Local aRet :={}
	
	If Empty(ZB8->ZB8_MODAL)
		
		dbSelectArea("ZBA")
		dbGoTop()
		While !(ZBA->(EOF()))
			AADD(aModal, ZBA->ZBA_COD + ' - ' + ZBA->ZBA_DESC)
			ZBA->(dbSkip())
		EndDo
		
		ZBA->(dbCLoseArea())
		
		aAdd(aParamBox,{2,"Informe o modal",1,aModal,70,"",.F.})
		
		If ParamBox(aParamBox,"Justificativa",@aRet)
			cModal := SUBS(aRet[1],1,2)
			
			dbSelectArea("ZB8")
			dbSetOrder(1)
			If dbSeek(xFilial("ZB8")+ZB8->ZB8_NRIMP)
				RecLock("ZB8", .F.)
				ZB8->ZB8_MODAL := cModal
				MsUnLock()
			EndIf
		EndIf
	Else
		MsgInfo("N�o � necess�rio ajustar o modal deste CTE")
	EndIf
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087K

Refaz confer�ncia
1. altera o campo ZB8_CONF para 'N'
2. Limpa as ocorr�ncias da tabela ZBC
3. Executa a fun��o U_CONFERE

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User function SASP087K()
	Local xArea
	Local yArea
	If ZB8->ZB8_STATUS $ "BL;PE"
		If MsgNoYes("Deseja refazer as confer�ncias do CTE selecionado", "Refaz Confer�ncia")
			xArea := getArea()
			altCTEConf(ZB8->ZB8_NRIMP)
			limpaOcorr(ZB8->ZB8_NRIMP)
			RestArea(xArea)
			
			yArea := getArea()
			U_SASF006()
			U_CONFERE()
			RestArea(yArea)
			
			MsgInfo("Conferencias refeitas com sucesso!","Refaz confer�ncia")
		EndIf
	Else
		MsgAlert("O CTE selecionado n�o est� apto a refazer confer�ncia!" + CRLF + Capital(getStatus(ZB8->ZB8_STATUS)))
	EndIf
Return

Static function altCTEConf(cSeqImp)
	Local xArea := getArea()
	
	dbSelectArea("ZB8")
	dbSetOrder(1)
	If dbSeek(xFilial("ZB8")+cSeqImp)
		RecLock("ZB8", .F.)
		ZB8->ZB8_CONF := 'N'
		MsUnLock()
	EndIf
	
	RestArea(xArea)
Return

Static Function limpaOcorr(cSeqImp)
	Local xArea := getArea()
	
	dbSelectArea("ZBC")
	dbSetOrder(1)
	dbSeek(xFilial("ZBC")+cSeqImp)
	
	While !(ZBC->(EOF())) .AND. ZBC->ZBC_NRIMP == cSeqImp
		RecLock("ZBC", .F.)
		ZBC->(dbDelete())
		MsUnLock()
		
		ZBC->(dbSkip())
	EndDo
	ZBC->(dbCloseArea())
	
	RestArea(xArea)
Return
//-------------------------------------------------------------------
/*/{Protheus.doc} getStatus

Fun��o que converte o c�digo do status em descri��o leg�vel 
para o usu�rio

@author Sam Barros
@since 06/08/2016
@version 1.0
@param cStatus, character, (Descri��o do par�metro)
@return cRet, Descri��o do Status
@example
getStatus('BL') --> Bloqueado
/*/
//-------------------------------------------------------------------
Static Function getStatus(cStatus)
	Local cRet
	Do Case
	Case cStatus == 'BL'
		cRet := "bloqueado"
	Case cStatus == 'PE'
		cRet := "pendente"
	Case cStatus == 'AG'
		cRet := "aguardando aprova��o"
	Case cStatus == 'DE'
		cRet := "com documento de entrada gerado"
	Case cStatus == 'FG'
		cRet := "com fatura j� gerada"
	otherwise
		cRet := "diferente do padr�o"
	EndCase
			
Return cRet

User Function getCodImport(cTipo)
	Local cRet := ""

	dbSelectArea("ZB0")
	dbGoTop()
	While !(ZB0->(EOF()))
		if ZB0->ZB0_TIPO $ cTipo
			cRet += ZB0->ZB0_USER + ';'
		EndIf
		
		ZB0->(dbSkip())
	EndDo
	ZB0->(dbCloseArea())
Return cRet

Static Function makeLogCabec()
	Local nPontos := 77
	
	AutoGrLog( Replicate( "-", nPontos ) )
	AutoGrLog( Replicate( " ", nPontos ) )
	AutoGrLog( "LOG DA Importa��o" )
	AutoGrLog( Replicate( " ", nPontos ) )
	AutoGrLog( Replicate( "-", nPontos ) )
	AutoGrLog( " " )
	AutoGrLog( " Dados Ambiente" )
	AutoGrLog( " --------------------" )
//AutoGrLog( " Empresa / Filial...: " + cEmpAnt + "/" + cFilAnt )
//AutoGrLog( " Nome Empresa.......: " + Capital( AllTrim( GetAdvFVal( "SM0", "M0_NOMECOM", cEmpAnt + cFilAnt, 1, "" ) ) ) )
//AutoGrLog( " Nome Filial........: " + Capital( AllTrim( GetAdvFVal( "SM0", "M0_FILIAL" , cEmpAnt + cFilAnt, 1, "" ) ) ) )
	AutoGrLog( " Data / Hora �nicio.: " + DtoC( Date() )  + " / " + Time() )
	AutoGrLog( " Usu�rio Protheus...: " + __cUserId + " " +  cUserName )
	AutoGrLog( " Nome computador....: " + GetComputerName() )
Return

User function msgLogCTE()
	Local 	cTexto    := NIL
	Local 	oMsgCTE	  := NIL
	Local   oFont     := NIL
	Local   oMemo     := NIL
		
	cTexto := LeLog()
	
	Define Font oFont Name "Mono AS" Size 5, 12

	Define MsDialog oMsgCTE Title "Atualiza��o concluida." From 3, 0 to 340, 417 Pixel

	@ 5, 5 Get oMemo Var cTexto Memo Size 200, 145 Of oMsgCTE Pixel
	oMemo:bRClicked := { || AllwaysTrue() }
	oMemo:oFont     := oFont

	Define SButton From 153, 175 Type  1 Action oMsgCTE:End() Enable Of oMsgCTE Pixel // Apaga
//Define SButton From 153, 145 Type 13 Action ( cFile := cGetFile( cMask, "" ), If( cFile == "", .T., ;
//MemoWrite( cFile, cTexto ) ) ) Enable Of oMsgCTE Pixel

	Activate MsDialog oMsgCTE Center
Return

Static Function LeLog()
	Local cRet  := ""
	Local cFile := NomeAutoLog()
	Local cAux  := ""

	FT_FUSE( cFile )
	FT_FGOTOP()

	While !FT_FEOF()

		cAux := FT_FREADLN()

		If Len( cRet ) + Len( cAux ) < 1048000
			cRet += cAux + CRLF
		Else
			cRet += CRLF
			cRet += Replicate( "=" , 128 ) + CRLF
			cRet += "Tamanho de exibi��o maxima do LOG alcan�ado." + CRLF
			cRet += "LOG Completo no arquivo " + cFile + CRLF
			cRet += Replicate( "=" , 128 ) + CRLF
			Exit
		EndIf

		FT_FSKIP()
	End

	FT_FUSE()

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP087L

Classificar

@type function 
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User function SASP087L()
	U_SASF010()
Return
