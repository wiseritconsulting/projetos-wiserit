#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASP092

Cadastro de Tipos de Justificativas

@type function
@author Sam Barros
@since 18/07/2016
@version 1.0
/*/
//-------------------------------------------------------------------
User Function SASP092()
	AxCadastro("ZBD", "Cadastro de Tipos de Justificativas", ".T.", ".T.")
Return