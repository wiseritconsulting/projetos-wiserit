#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO001

Valida��o de Ocorr�ncia

CFOP FORA DO PADRAO '353'                         

Valida se o CFOP da ocorr�ncia est� contido no par�metro 
informado - SA_CFOCORR

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO001()
/*/
//-------------------------------------------------------------
User Function SASO001()
	Local cSql := u_getCTE()
	Local _cAlias := GetNextAlias()
	Local nTotVer := 0
	Local nTotOcor := 0
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		nTotVer++
		If !(SUBS((_cAlias)->ZB8_CFOP,2,3) $ GetMV("SA_CFOCORR"))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			nTotOcor++
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO001", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	
	AutoGrLog("-------------- Total Verificados: " + AllTrim(Str(nTotVer)))
	AutoGrLog("-------------- Total Ocorr�ncias: " + AllTrim(Str(nTotOcor)))
		
	(_cAlias)->( DBCloseArea() )
Return 