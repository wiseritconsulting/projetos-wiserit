#Include 'Protheus.ch'

//-------------------------------------------------------------
/*/{Protheus.doc} SASO011

Valida��o de Ocorr�ncia

TOMADOR DIFERENTE DA MEDI��O                                          

Valida se j� existe NFE em outro CTE

@type function
@author Sam Barros
@since 05/08/2016
@version 1.0
@example
U_SASO011()
/*/
//-------------------------------------------------------------
User Function SASO011()
	Local cSql := u_getCTEbyNFE()
	Local _cAlias := GetNextAlias()
	Local nTotVer := 0
	Local nTotOcor := 0
	
	DBUseArea(.T.,'TOPCONN',TCGENQRY(,,cSql),_cAlias,.F.,.T.)
	
	dbSelectArea(_cAlias)
	(_cAlias)->( dbGoTop() )
	While !(_cAlias)->( Eof() )
		nTotVer++
		If !(u_MedTomInc((_cAlias)->ZB9_DANFE, (_cAlias)->ZB8_TOMADO))
			AutoGrLog("------"+(_cAlias)->ZB8_NRDF + '/' + (_cAlias)->ZB8_SERDF)
			nTotOcor++
			u_createZBC((_cAlias)->ZB8_NRIMP, "SASO011", _p_cJust)
		EndIf
		
		(_cAlias)->(dbSkip())
	Enddo
	
	AutoGrLog("-------------- Total Verificados: " + AllTrim(Str(nTotVer)))
	AutoGrLog("-------------- Total Ocorr�ncias: " + AllTrim(Str(nTotOcor)))
		
	(_cAlias)->( DBCloseArea() )
Return