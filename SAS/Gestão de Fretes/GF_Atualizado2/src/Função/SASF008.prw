#Include 'Protheus.ch'

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} EmailGF

Envia email para aprovadores de ocorr�ncia, valor e R$/Kg
e para os usu�rio do financeiro que receber�o informa��es sobre a fatura

@type function
@author Sam Barros
@since 07/08/2016
@version 1.0
@param aEmail, array, Array contendo os emails que receber�o as informa��es
@param cSeqImp, character, Sequencia do CTE
@param cTipo, character, Informa se ser� O-Ocorr�ncia, A-AProvador, F-Fatura(Financeiro)
@param cFatura, character, Se o tipo for F, ent�o deve ser informado o n�mero da fatura
@return bRet, Informa se o email foi enviado
@example
(examples)
/*/
//------------------------------------------------------------------------------------------------------
User Function EmailGF(cSeqImp, cTipo, cFatura)
	Local bRet := .T.
	
	Default cSeqImp := ""
	Default cFatura := ""
	
	Do Case
		Case cTipo == 'O'
			ocorrencia(cSeqImp)
		Case cTipo == 'A'
			aprovador(cSeqImp)
		Case cTipo == 'F'
			fatura(cFatura)
	EndCase
Return bRet

Static Function ocorrencia(cSeqImp)
	Local aCTE := getDadosCTE(cSeqImp)
	Local aAprov := getAprOco(cSeqImp)
	Local nTamOco := len(aAprov) 
	Local i
	Local cHTML := ""
	
	cHTML := U_SASW015(aCTE, aAprov) 
	
	For i := 1 to nTamOco
		//U_SendMail(U_InfoUser(aAprov[i][7], "EMAIL"), "[Gest�o de Fretes] Visto Ocorr�ncia - CTE:" + aCTE[2],cHtml,,)
		U_SendMail("samarony@objetiveti.com.br", "[Gest�o de Fretes] Visto Ocorr�ncia - CTE:" + aCTE[2],cHtml,,)
	Next i 
Return

Static Function aprovador(cSeqImp)
	Local aCTE := getDadosCTE(cSeqImp)
	Local aAprov := getAprVal(cSeqImp)
	Local nTamVal := len(aAprov) 
	Local i
	Local cHTML := ""
	
	cHTML := U_SASW016(aCTE, aAprov) 
	
	For i := 1 to nTamVal
		U_SendMail(U_InfoUser(aAprov[i][6], "EMAIL"), "[Gest�o de Fretes] Aprova��o Al�ada - CTE:" + aCTE[2],cHtml,,)
		//U_SendMail("samarony@objetiveti.com.br", "[Gest�o de Fretes] Aprova��o Al�ada - CTE:" + aCTE[2],cHtml,,)
	Next i 
Return

Static Function fatura(cFatura)
Return

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} getAprOco

Fun��o que busca os aprovadores de ocorr�ncia

@author Sam Barros
@since 07/08/2016
@version 1.0
@param cSeqImp, character, N�mero sequencial de importa��o
@return aRet, Array com os dados dos aprovadores
@example
getAprOco("0000000000000001")
/*/
//------------------------------------------------------------------------------------------------------

Static Function getAprOco(cSeqImp)
	Local aRet := {}
	Local aAux := {}
	Local xArea := getArea()
	
	dbSelectArea("ZBE")
	dbSetOrder(1)
	If dbSeek(xFilial("ZBE")+cSeqImp)
		While !(ZBE->(EOF())) .AND. ZBE->ZBE_NRIMP == cSeqImp
			aAux := {}
			 
			AADD(aAux, ZBE->ZBE_STATUS)
			AADD(aAux, UsrRetName(ZBE->ZBE_USER))
			AADD(aAux, DtoC(ZBE->ZBE_DTAPROV))
			AADD(aAux, ZBE->ZBE_OCORR + ' - ' + ZBE_DESOCO)
			AADD(aAux, ZBE->ZBE_TPJUST + ' - ' + ZBE_JUST)
			AADD(aAux, ZBE->ZBE_OBS)
			AADD(aAux, ZBE->ZBE_USER)
			
			AADD(aRet, aAux)
			
			ZBE->(dbSkip())
		EndDo
		
		ZBE->(dbCloseArea())
	EndIf
	
	RestArea(xArea)
Return aRet

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} getAprVal

Fun��o que busca os aprovadores de valor ou R$/Kg

@author Sam Barros
@since 07/08/2016
@version 1.0
@param cSeqImp, character, N�mero sequencial de importa��o
@return aRet, Array com os dados dos aprovadores
@example
getAprVal("0000000000000001")
/*/
//------------------------------------------------------------------------------------------------------

Static Function getAprVal(cSeqImp)
	Local aRet := {}
	Local aAux := {}
	Local xArea := getArea()
	
	dbSelectArea("ZBF")
	dbSetOrder(1)
	If dbSeek(xFilial("ZBF")+cSeqImp)
		While !(ZBF->(EOF())) .AND. ZBF->ZBF_NRIMP == cSeqImp
			aAux := {}
			 
			AADD(aAux, ZBF->ZBF_STATUS)
			AADD(aAux, UsrRetName(ZBF->ZBF_USER))
			AADD(aAux, DtoC(ZBF->ZBF_DTAPRO))
			AADD(aAux, Transform(0,PesqPict("ZB8", "ZB8_VLDF")))
			AADD(aAux, Transform(0,PesqPict("ZB8", "ZB8_VLDF")))
			//AADD(aAux, ZBE->ZBE_OCORR + ' - ' + ZBE_DESOCO)
			//AADD(aAux, ZBE->ZBE_TPJUST + ' - ' + ZBE_JUST)
			//AADD(aAux, ZBE->ZBE_OBS)
			AADD(aAux, ZBF->ZBF_USER)
			
			AADD(aRet, aAux)
			
			ZBF->(dbSkip())
		EndDo
		
		ZBF->(dbCloseArea())
	EndIf
	
	RestArea(xArea)
Return aRet

//------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} getDadosCTE

Pega dados do CTE para inclus�o no cabe�alho do email via WF

@author Sam Barros
@since 07/08/2016
@version 1.0
@param cSeqImp, character, (Descri��o do par�metro)
@return aRet, Informa��es do CtE
@example
getAprOco("0000000000000001")
/*/
//------------------------------------------------------------------------------------------------------
Static Function getDadosCTE(cSeqImp)
	Local aRet := {}
	Local xArea := getArea()

	dbSelectArea("ZB8")
	dbSetOrder(1)
	if dbSeek(xFilial("ZB8") + cSeqImp)
		AADD(aRet, ZB8->ZB8_FILDOC)
		AADD(aRet, ZB8->ZB8_NRDF + '/' + ZB8->ZB8_SERDF)
		AADD(aRet, DtoC(ZB8->ZB8_DTEMIS))
		AADD(aRet, Transform(ZB8->ZB8_VLDF, PesqPict("ZB8","ZB8_VLDF")))
		AADD(aRet, ZB8->ZB8_CDREM  + " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDREM,"SA2->A2_NREDUZ"))
		AADD(aRet, ZB8->ZB8_FILDOC + " - " + FWFilialName(cEmpAnt,ZB8->ZB8_FILDOC,1))
		AADD(aRet, ZB8->ZB8_TOMADO + " - " + FWFilialName(cEmpAnt,ZB8->ZB8_TOMADO,1))
		AADD(aRet, ZB8->ZB8_CDDEST + " - " + POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDDEST,"SA1->A1_NREDUZ"))
		/*
		AADD(aRet, ZB8->ZB8_NRIMP)
		AADD(aRet, ZB8->ZB8_NRDF + '/' + ZB8->ZB8_SERDF)
		AADD(aRet, ZB8->ZB8_FILDOC)
		AADD(aRet, Transform(ZB8->ZB8_VLDF, PesqPict("ZB8","ZB8_VLDF")))
		AADD(aRet, DtoC(ZB8->ZB8_DTEMIS))
		AADD(aRet, ZB8->ZB8_CDREM  + " - " + POSICIONE("SA2",1,xFilial("SA2")+ZB8->ZB8_CDREM,"SA2->A2_NREDUZ"))
		AADD(aRet, ZB8->ZB8_FILDOC + " - " + FWFilialName(cEmpAnt,ZB8->ZB8_FILDOC,1))
		AADD(aRet, ZB8->ZB8_CDDEST + " - " + POSICIONE("SA1",1,xFilial("SA1")+ZB8->ZB8_CDDEST,"SA1->A1_NREDUZ"))
		*/
	EndIf
	ZB8->(dbCloseArea())
	
	RestArea(xArea)
Return aRet