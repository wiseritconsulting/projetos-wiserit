#Include 'Protheus.ch'

User Function SASF010()
	Local cTes 		:= ZB8->ZB8_TES
	Local cCP 		:= ZB8->ZB8_CODPAG
	Local cNature 	:= ZB8->ZB8_NATURE
	Local cCC 		:= ZB8->ZB8_CC
	
	Local aRet := {}
	Local aParamBox := {}
	
	aAdd(aParamBox,{1,"TES"						,cTes		,"","","SF4","",0,.T.}) // Tipo caractere
	aAdd(aParamBox,{1,"Condi��o de Pagamento"	,cCP		,"","","SE4","",0,.T.}) // Tipo caractere
	aAdd(aParamBox,{1,"Natureza"				,cNature	,"","","SED","",0,.T.}) // Tipo caractere
	aAdd(aParamBox,{1,"Centor de Custos"		,cCC		,"","","CTT","",0,.T.}) // Tipo caractere
	
	If !(ZB8->ZB8_STATUS $ "DE;FG")
		If ParamBox(aParamBox,"Deseja Classificar o CTE " + AllTrim(ZB8->ZB8_NRDF) + '/' + ZB8->ZB8_SERDF ,@aRet)
			RecLock("ZB8", .F.)
			ZB8->ZB8_TES 	:= aRet[1]
			ZB8->ZB8_CODPAG	:= aRet[2]
			ZB8->ZB8_NATURE := aRet[3]
			ZB8->ZB8_CC 	:= aRet[4]
			MsUnLock()
			
			MsgInfo("CTE classificado com sucesso!!!","Classificar CTE")
		EndIf
	Else
		MsgAlert("O CTE n�o se encontra apto a ser classificado")
	EndIf
Return