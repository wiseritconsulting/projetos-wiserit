#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASV003

Valida��o do campo ZBA_PROD no cadastro Modal.
Verifica se o produto informado existe na SB1  
 
Usado no validador do campo. 
@author Sam Barros
@since 03/08/2016
@version 1.0
@return bRet, Valida��o de exist�ncia de registro
@example
U_SASV003()
/*/
//-------------------------------------------------------------------

User Function SASV003()
	Local bRet := .T.
	Local aArea := getArea()
	Local cMsg := ""
	
	dbSelectArea("SB1")
	dbSetOrder(1)
	If dbSeek(xFilial("SB1")+M->ZBA_PROD)
		If !("FRETE" $ SB1->B1_DESC)
			cMsg += "O produto " + AllTrim(SB1->B1_DESC) + " n�o cont�m a palavra FRETE "
			cMsg += "em sua descri��o" + CRLF
			cMsg += "Deseja incluir esse produto mesmo assim?"
			If MsgYesNo(cMsg)
				bRet := .T.
			Else
				bRet := .F.
			EndIf
		EndIf
	Else
		MsgAlert("O produto informado n�o existe na base!")
		bRet := .F.
	EndIf
	
	SB1->(dbCloseArea())
	
	RestArea(aArea)
Return bRet