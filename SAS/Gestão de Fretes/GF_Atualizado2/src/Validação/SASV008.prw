#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASV008

Valida��o do campo ZBB_MODAL na importa��o do hist�rico 
de fretes.
Verifica se o modal est� cadastrado
 
Usado no validador do campo. 
@author Sam Barros
@since 03/08/2016
@version 1.0
@return bRet, Valida��o de exist�ncia de registro
@example
U_SASV008()
/*/
//-------------------------------------------------------------------
User Function SASV008()
	Local bRet := .T.
	Local aArea := getArea()
	
	dbSelectArea("ZBA")
	dbSetOrder(1)
	If !(dbSeek(xFilial("ZBA")+M->ZBB_MODAL))
		MsgAlert("O modal informado n�o est� cadastro.!")
		bRet := .F.
	EndIf
	
	ZBB->(dbCloseArea())
	
	RestArea(aArea)
Return bRet