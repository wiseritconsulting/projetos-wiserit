#Include 'Protheus.ch'
//-------------------------------------------------------------------
/*/{Protheus.doc} SASV001

Valida��o do campo ZB2_OCORR no cadastro de ocorr�ncias.
 
Usado no validador do campo. 
@author Sam Barros
@since 01/07/2016
@version 1.0
@return bRet, Valida��o de exist�ncia de registro
@example
U_SASV001()
/*/
//-------------------------------------------------------------------
User Function SASV001()
	Local bRet := .T.
	Local aArea := getArea()
	
	//--------------------------------------------------------
	//Verifica se o registro existe na tabela de ocorr�ncias
	//--------------------------------------------------------
	dbSelectArea("ZB3")
	dbSetOrder(1) //ZB3_FILIAL + ZB3_COD
	If !dbSeek(xFilial("ZB3")+M->ZB2_OCORR)
		MsgAlert("N�o Existe este registro no cadastro de ocorr�ncias")
		bRet := .F.
	EndIf
	
	ZB3->(dbCLoseArea())
	
	if select("ZB2") > 0
		ZB2->(dbCloseArea())
	EndIf
	
	//--------------------------------------------------------
	//Verifica se o registro n�o possui cadastro
	//--------------------------------------------------------
	dbSelectArea("ZB2")
	dbSetOrder(2) //ZB2_FILIAL + ZB2_OCORR
	if dbSeek(xFilial("ZB2")+M->ZB2_OCORR)
		MsgAlert("J� existe cadastro para esta ocorr�ncia")
		bRet := .F.
	EndIf
	
	RestArea(aArea)
Return bRet
