#Include 'Protheus.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} SASV007

Valida��o do campo ZB5_USER no cadastro de aprovadores.
Verifica se j� existe aprovador cadastrado com o mesmo 
c�digo de usu�rio e tipo de aprova��o.
 
Usado no validador do campo. 
@author Sam Barros
@since 03/08/2016
@version 1.0
@return bRet, Valida��o de exist�ncia de registro
@example
U_SASV007()
/*/
//-------------------------------------------------------------------
User Function SASV007()
	Local bRet := .T.
	Local aArea := getArea()
	
	dbSelectArea("ZB5")
	dbSetOrder(3)
	If dbSeek(xFilial("ZB5")+M->ZB5_USER+M->ZB5_TIPO)
		MsgAlert("J� existe aprovador de "+getDescTipo(M->ZB5_TIPO)+" relacionado a este usu�rio!")
		bRet := .F.
	EndIf
	
	ZB5->(dbCloseArea())
	
	RestArea(aArea)
Return bRet

Static function getDescTipo(cTipo)
	Local cRet := ""
	
	cRet := IIF(cTipo=='1',"Ocorr�ncia","Al�ada")
Return cRet