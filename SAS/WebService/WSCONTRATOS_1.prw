#INCLUDE "APWEBSRV.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "RWMAKE.CH"
#INCLUDE "PRTOPDEF.CH"

/*                               
Autor.....: Rog�rio J�come
Data......: 14/04/16
Descri��o.: WebService de consulta dos contratos
*/

WSSERVICE WSCONTRATOS DESCRIPTION "WebService de consulta dos contratos"
	
	
	WSDATA cEmpre 				AS String
	WSDATA cContra 				AS String
	WSDATA cFili				AS String
	WSDATA cGuid 				AS String
	
	
	
	WSDATA nVlrTot        		AS Float
	WSDATA nDesc          		AS Float
	WSDATA nVlrLiq	    		AS Float
	//WSDATA ARETORNO			AS aRetorno_Array
	
	/* VARI�VEIS M�TODO CLIENTE*/
	WSDATA cLoja 			AS String
	WSDATA cPessoa			AS String
	WSDATA cNome 			AS String
	WSDATA cNReduz 			AS String
	WSDATA cEnd 			AS String
	WSDATA cMun 			AS String
	WSDATA cEst 			AS String
	WSDATA cCodMun			AS String
	WSDATA cCep				AS String
	WSDATA cPais			AS String
	WSDATA cCGC				AS String
	WSDATA cConta			AS String
	WSDATA cTpPes			AS String
	WSDATA cEmail			AS String
	WSDATA cBairro			AS String
	WSDATA cRecISS			AS String
	WSDATA cIncISS			AS String
	WSDATA cRecINSS			AS String
	WSDATA cRecIRRF			AS String
	WSDATA cRecCOFI			AS String
	WSDATA cRecCSLL			AS String
	WSDATA cIRBAX			AS String
	WSDATA cRecPIS			AS String
	WSDATA cMINIRF			AS String
	WSDATA cABATIMP			AS String
	WSDATA cCodVend			AS String
	WSDATA CTEL 			AS String
	WSDATA CCODPAIS 		AS String
	WSDATA CYCODTCF 		AS String
	WSDATA cYCodGlo 		AS String
	
	WSDATA cBloCo 			AS String
	WSDATA cBloCl 			AS String
	WSDATA cTpPort 			AS String
	WSDATA cCBASE 			AS String
	WSDATA cBairroC 		AS String
	WSDATA cBairroE 		AS String
	WSDATA cUFent 			AS String
	WSDATA cDTpCli 			AS String
	WSDATA cTipo   			AS String
	WSDATA cDataBloq	 	AS String
	WSDATA Obs				AS String
	WSDATA cEndENT			AS String
	WSDATA cCEPE	 		AS String
	WSDATA cMUNE			AS String
	WSDATA Retorno			AS String
	/*-------------------------*/
	
	
	
	WSDATA aTitulos				AS sTITULOS_ARRAY // Array de t�tulos que ir� ser retornado
	WSDATA aMedicoes			AS sMEDICOES_ARRAY // Array de t�tulos que ir� ser retornado
	
	WSMETHOD CONTRATOS 			DESCRIPTION "Esse m�todo retorna os dados do contrato."
	WSMETHOD MEDICOES 			DESCRIPTION "Esse m�todo � respons�vel pela consulta das medi��es."
	WSMETHOD CLIENTES 			DESCRIPTION "Esse m�todo � respons�vel por incluir um novo cliente."
	
ENDWSSERVICE


WSSTRUCT sTITULO   // Esturura do T�tulo a Receber
	
	WSDATA cFil					AS String
	WSDATA cNum					AS String
	WSDATA cClient				AS String
	WSDATA cLoja				AS String
	WSDATA cNome				AS String
	WSDATA cEst					AS String
	WSDATA cProdut				AS String
	WSDATA cDescPro				AS String
	WSDATA cQuant				AS String
	WSDATA cVlrU				AS String
	WSDATA cVlrT				AS String
	WSDATA cTes					AS String
	WSDATA cTipo				AS String
	WSDATA cEnvio				AS String
	
	
ENDWSSTRUCT


WSSTRUCT sMedicoes   // Esturura do T�tulo a Receber
	
	WSDATA cClient				AS String
	WSDATA cNum					AS String
	WSDATA cMedic				AS String
	WSDATA cMaterial			AS String
	WSDATA cQuant				AS String
	WSDATA cFase				AS String
	WSDATA cTipo				AS String
	WSDATA cEspeci				AS String
	WSDATA cNF					AS String
	WSDATA cBloqueo				AS String
	WSDATA cTransp				AS String
	WSDATA cDTEnt				AS String
	WSDATA cEndEn				AS String
	WSDATA cNMREC				AS String
	
	
	
ENDWSSTRUCT


WSSTRUCT sTITULOS_ARRAY // Esturura do array de T�tulos a receber
	WSDATA oTitulos				AS Array OF sTITULO OPTIONAL // Estutura do t�tulo
	//WSDATA Query1		AS String  // Query que foi executada na SE1
	//WSDATA Query2		AS String  // Query que foi executada na SC5
ENDWSSTRUCT


WSSTRUCT sMEDICOES_ARRAY // Esturura do array de T�tulos a receber
	WSDATA oMedicoes			AS Array OF sMedicoes OPTIONAL // Estutura do t�tulo
	//WSDATA Query3		AS String  // Query que foi executada na SE1
	//WSDATA Query2		AS String  // Query que foi executada na SC5
ENDWSSTRUCT

//Metodo para Inclusao da Venda Assistida

//--------------------------------------------------------------------------------------------------------------
//Metodo para Inclusao no Cadastro de Clientes
WSMETHOD CONTRATOS WSRECEIVE cGuid   WSSEND aTitulos WSSERVICE WSCONTRATOS

	Local aAuto 	:= {}
	Local aErro		:= {}
	Local aConteu 	:= {}
	Local nCont
	Local lRet		:= .T.
	Local cBuffer 	:= ""
	Local cBody		:= ""
	Local nHdL 		:= -1
	Local cClient 	:= ""
	Local cQuery 	:= ""

	
	Private lMsErroAuto := .F.
//::Retorno := "TRUE"
	//::aTitulos:Query1 := ""
	//::aTitulos:Query2 := ""
	PREPARE ENVIRONMENT EMPRESA ::cEmpre FILIAL ::cFili MODULO "SIGAFAT"
//--- Exemplo: Inclusao ---
	
	cQuery := " SELECT ZZ0_FILIAL, "
	cQuery += " ZZ0_NUMERO, "
	cQuery += " A1_COD, "
	cQuery += " A1_LOJA, "
	cQuery += " A1_NOME, "
	cQuery += " A1_EST, "
	cQuery += " ZZ1_PRODUT, "
	cQuery += " ZZ1_DESC, "
	cQuery += " ZZ1_QUANT, "
	cQuery += " ZZ1_VLRUN, "
	cQuery += " ZZ1_VLRTOT, "
	cQuery += " ZZ1_TES, "
	cQuery += " ZZ1_TIPO, "
	cQuery += " CASE ZZ1_TIPO "
	cQuery += " WHEN 'K' THEN "
	cQuery += " (SELECT ZZ7_ENVIO "
	cQuery += " FROM ZZ7010 "
	cQuery += " WHERE ZZ7_CODIGO=ZZ1_PRODUT "
	cQuery += " AND D_E_L_E_T_='') "
	cQuery += " ELSE "
	cQuery += " (SELECT B1_YVOL "
	cQuery += " FROM SB1010 "
	cQuery += " WHERE B1_COD=ZZ1_PRODUT "
	cQuery += " AND D_E_L_E_T_='') "
	cQuery += " END AS ENVIO "
	cQuery += " FROM ZZ0010 ZZ0 "
	cQuery += " INNER JOIN ZZ1010 ZZ1 ON ZZ0.ZZ0_FILIAL = ZZ1.ZZ1_FILIAL "
	cQuery += " AND ZZ0.ZZ0_NUMERO = ZZ1.ZZ1_NUMERO "
	cQuery += " AND ZZ0.ZZ0_CLIENT = ZZ1.ZZ1_CLIENT "
	cQuery += " AND ZZ0.ZZ0_LOJA = ZZ1.ZZ1_LOJA "
	cQuery += " AND ZZ1.D_E_L_E_T_='' "
	cQuery += " INNER JOIN SA1010 SA1 ON SA1.A1_COD = ZZ0.ZZ0_CLIENT "
	cQuery += " AND SA1.A1_LOJA = ZZ0.ZZ0_LOJA "
	cQuery += " AND SA1.D_E_L_E_T_='' "
	cQuery += " AND SA1.A1_YGUID = '"+cGuid+"' "
	cQuery += " AND ZZ0.D_E_L_E_T_='' "
	cQuery += " AND ZZ0.ZZ0_STATUS IN ('A','L') "
	cQuery += " ORDER BY 1,2 "
	TcQuery cQuery New Alias T01
	 
	
	//::aTitulos:Query1 := AllTrim(cQuery)
	
	WHILE !T01->(EOF())
	
		oTitulo := WsClassNew("sTITULO")
	
		oTitulo:cFil			:= T01->ZZ0_FILIAL
		oTitulo:cNum			:= T01->ZZ0_NUMERO
		oTitulo:cClient			:= T01->A1_COD
		oTitulo:cLoja			:= AllTrim(T01->A1_LOJA)
		oTitulo:cNome			:= AllTrim(T01->A1_NOME)
		oTitulo:cEst			:= T01->A1_EST
		oTitulo:cProdut			:= AllTrim(T01->ZZ1_PRODUT)
		oTitulo:cDescPro		:= AllTrim(T01->ZZ1_DESC)
		oTitulo:cQuant			:= AllTrim(cvaltochar(T01->ZZ1_QUANT))
		oTitulo:cVlrU			:= AllTrim(cvaltochar(T01->ZZ1_VLRUN))
		oTitulo:cVlrT			:= alltrim(cvaltochar(T01->ZZ1_VLRTOT))
		oTitulo:cTes			:= AllTrim(T01->ZZ1_TES)
		oTitulo:cTipo			:= AllTrim(T01->ZZ1_TIPO)
		oTitulo:cEnvio			:= AllTrim(T01->ENVIO)
			
		
		aAdd(::aTitulos:oTitulos,oTitulo) // Inclui na estrutura que armaneza o retorno para o legado
	//aAdd(::ARETORNO,"OK")
	//AADD(aConteu,{T01->ZZ0_FILIAL,T01->ZZ0_NUMERO,T01->A1_COD,T01->A1_LOJA,T01->A1_NOME,T01->A1_EST,T01->ZZ1_PRODUT,T01->ZZ1_DESC,T01->ZZ1_QUANT,T01->ZZ1_VLRUN,T01->ZZ1_VLRTOT,T01->ZZ1_TES,T01->ZZ1_TIPO,T01->ENVIO})
			//	::Retorno += "OK " + "CNPJ: " + cvaltochar(ZZ0_NUMERO) + "Loja: " + cvaltochar(ZZ1_QUANT)
		T01->(DBSKIP())
	ENDDO
	
	
	
	DbCloseArea("T01")
	
Return(lRet)

WSMETHOD MEDICOES WSRECEIVE cGuid,cFili,cContra WSSEND aMedicoes WSSERVICE WSCONTRATOS

	Local aAuto 	:= {}
	Local aErro		:= {}
	Local aConteu 	:= {}
	Local nCont
	Local lRet		:= .T.
	Local cBuffer 	:= ""
	Local cBody		:= ""
	Local nHdL 		:= -1
	Local cClient 	:= ""
	Local cQuery 	:= ""

	
	Private lMsErroAuto := .F.
//::Retorno := "TRUE"
	//::aMedicoes:Query3 := ""
	//::aTitulos:Query2 := ""
	PREPARE ENVIRONMENT EMPRESA ::cEmpre FILIAL ::cFili MODULO "SIGAFAT"
//--- Exemplo: Inclusao ---

	cQuery := " SELECT ZZ2_FILIAL, "
	cQuery += " A1_NOME, "
	cQuery += " ZZ2_NUMERO, "
	cQuery += " ZZ2_MEDICA, "
	cQuery += " ZZ3_ITEM, "
	cQuery += " ZZ3_DESCRI, "
	cQuery += " ZZ3_QUANT, "
	cQuery += " ZZR_NOMETP, "
	cQuery += " ZZ2_TIPO, "
	cQuery += " ZZR_ETAPA, "
	cQuery += " A1_MSBLQL, "
	cQuery += " ZZ2_DESTRA, "
	cQuery += " A1_ENDENT, "
	cQuery += " ZZ2_DTGER "
	cQuery += " FROM ZZ2010 ZZ2 "
	cQuery += " INNER JOIN ZZR010 ZZR ON ZZ2_FILIAL = ZZR_FILIAL "
	cQuery += " AND ZZ2_NUMERO = ZZR_CONTRA "
	cQuery += " AND ZZ2_MEDICA = ZZR_MEDICA "
	cQuery += " AND ZZ2_CLIENT = ZZR_CODCLI "
	cQuery += " INNER JOIN ZZ3010 ZZ3 ON ZZ3_FILIAL = ZZ2_FILIAL "
	cQuery += " AND ZZ3_NUMERO = ZZ2_NUMERO "
	cQuery += " AND ZZ3_MEDICA = ZZ2_MEDICA "
	cQuery += " AND ZZ3_CLIENT = ZZ2_CLIENT "
	cQuery += " AND ZZ3_LOJA = ZZ2_LOJA "
	cQuery += " AND ZZ3.D_E_L_E_T_ = '' "
	cQuery += " INNER JOIN SA1010 A1 ON A1_COD = ZZ2_CLIENT "
	cQuery += " AND A1_LOJA = ZZ2_LOJA "
	cQuery += " AND A1.D_E_L_E_T_ = '' "
	cQuery += " WHERE ZZ2.D_E_L_E_T_ = '' "
	cQuery += " AND ZZR_ETAPA = "
	cQuery += " (SELECT MAX(ZZR_ETAPA) "
	cQuery += " FROM ZZR010 AB "
	cQuery += " WHERE D_E_L_E_T_ = '' "
	cQuery += " AND ZZR.ZZR_FILIAL = AB.ZZR_FILIAL "
	cQuery += " AND ZZR.ZZR_CONTRA = AB.ZZR_CONTRA "
	cQuery += " AND ZZR.ZZR_MEDICA = AB.ZZR_MEDICA)
	cQuery += " AND ZZ2_FILIAL = '"+cFili+"' "
	cQuery += " AND ZZ2_NUMERO = '"+cContra+"' "
	cQuery += " AND A1.A1_YGUID = '"+cGuid+"' "
	cQuery += " GROUP BY ZZ2_FILIAL, "
	cQuery += " A1_NOME, "
	cQuery += " ZZ2_NUMERO, "
	cQuery += " ZZ2_MEDICA, "
	cQuery += " ZZ3_ITEM, "
	cQuery += " ZZ3_DESCRI, "
	cQuery += " ZZ3_QUANT, "
	cQuery += " ZZR_NOMETP, "
	cQuery += " ZZ2_TIPO, "
	cQuery += " ZZR_ETAPA, "
	cQuery += " A1_MSBLQL, "
	cQuery += " ZZ2_DESTRA, "
	cQuery += " A1_ENDENT, "
	cQuery += " ZZ2_DTGER "
	TcQuery cQuery new Alias T01
	
	
		//::aMedicoes:Query3 := AllTrim(cQuery)
	
	WHILE !T01->(EOF())
	
		oMedicao := WsClassNew("sMedicoes")
	
		oMedicao:cClient			:= T01->A1_NOME
		oMedicao:cNum				:= T01->ZZ2_NUMERO
		oMedicao:cMedic				:= T01->ZZ2_MEDICA
		oMedicao:cMaterial			:= AllTrim(T01->ZZ3_DESCRI)
		oMedicao:cQuant				:= AllTrim(cvalToChar(T01->ZZ3_QUANT))
		oMedicao:cFase				:= T01->ZZR_NOMETP
		oMedicao:cTipo				:= AllTrim(T01->ZZ2_TIPO)
		oMedicao:cEspeci			:= AllTrim(T01->ZZ2_TIPO)
		oMedicao:cNF				:= AllTrim(cvaltochar(T01->ZZ2_NUMERO))
		oMedicao:cBloqueo			:= AllTrim(cvaltochar(T01->A1_MSBLQL))
		oMedicao:cTransp			:= alltrim(cvaltochar(T01->ZZ2_DESTRA))
		oMedicao:cDTEnt				:= AllTrim(T01->ZZ2_DTGER)
		oMedicao:cEndEn				:= AllTrim(T01->A1_ENDENT)
		oMedicao:cNMREC				:= AllTrim(T01->A1_ENDENT)
			
	
		aAdd(::aMedicoes:oMedicoes,oMedicao) // Inclui na estrutura que armaneza o retorno para o legado
		T01->(DBSKIP())
	ENDDO
	
	DbCloseArea("T01")
	
	
Return(lRet)


WSMETHOD CLIENTES WSRECEIVE cLoja,cPessoa,cNome,cNReduz,cEnd,cTipo,cEst,cCodMun,cMun,cBairro,cCep,cPais,cCGC,cEmail,cTel, cCodPais, cYCODTCF,cYCodGlo, cBloCo, cBloCl, cTpPort, cBairroC, cBairroE, cUFent, cDTpCli, cGuid, cDataBloq, Obs, cEndENT, cCEPE, cMUNE, cCBASE WSSEND Retorno WSSERVICE WSCONTRATOS
	
	Local aArea 	:= GetArea()
	Local aAuto 	:= {}
	Local aErro		:= {}
	Local nCont
	Local lRet		:= .T.
	Local cBuffer 	:= ""
	Local cBody		:= ""
	Local nHdL 		:= -1
	Local cClient 	:= ""
	Local cLoja		:= ''
	Local cQuery 	:= ''
	Private lMsErroAuto := .F.
//::Retorno := "TRUE"

	PREPARE ENVIRONMENT EMPRESA "01" FILIAL "01"// MODULO "SIGAFAT"

	ConOut("Inicio do processamento do Job CRM: " + cGuid)
/*
	dbSelectArea("SA1")
	dbSetOrder(3)
	If dbSeek(xFilial("SA1")+Alltrim(cCGC))*/
	
	IF SELECT("T01") > 0
	
	T01->(DbCloseArea())
	
	ENDIF

	cQuery := " SELECT TOP 1 * FROM SA1010 A1 "
	cQuery += " WHERE A1.D_E_L_E_T_ = '' "
	cQuery += " AND A1_CGC = '"+cCGC+"' "
	cQuery += " AND A1_YGUID = '"+cGuid+"' "
	TcQuery cQuery new Alias T01
	
	IF T01->(EOF()) //SE O CLIENTE J� EXISTIR, A QUERY RETORNAR EM BRANCO    
	
	aAuto :=	{	 	 {"A1_LOJA"      ,IIF(!Empty(cLoja),alltrim(cLoja),Criavar("A1_LOJA"))               	,Nil},; // Loja OK
	{"A1_PESSOA"    ,IIF(!Empty(cPessoa),alltrim(cPessoa),Criavar("A1_PESSOA"))           	,Nil},; // Pessoa OK
	{"A1_NOME"      ,IIF(!Empty(cNome),alltrim(cNome),Criavar("A1_NOME"))				  	,Nil},; // Nome OK
	{"A1_NREDUZ"    ,IIF(!Empty(cNReduz),alltrim(cNReduz),Criavar("A1_NREDUZ"))			,Nil},; // Nome reduz. OK
	{"A1_END"       ,IIF(!Empty(cEnd),alltrim(cEnd),Criavar("A1_END"))						,Nil},; // Endereco OK
	{"A1_TIPO"      ,IIF(!Empty(cTipo),alltrim(cTipo),Criavar("A1_TIPO"))					,Nil},; // Tipo OK
	{"A1_EST"       ,IIF(!Empty(cEst),alltrim(cEst),Criavar("A1_EST"))		    			,Nil},; // Estado OK
	{"A1_COD_MUN"   ,IIF(!Empty(cCodMun),alltrim(cCodMun),Criavar("A1_COD_MUN"))			,Nil},; // Codigo Municipio OK
	{"A1_MUN"       ,IIF(!Empty(cMun),alltrim(cMun),Criavar("A1_MUN"))						,Nil},; // Cidade OK
	{"A1_BAIRRO"    ,IIF(!Empty(cBairro),alltrim(cBairro),Criavar("A1_BAIRRO"))			,Nil},; // Bairro OK
	{"A1_CEP"       ,IIF(!Empty(cCep),alltrim(cCep),Criavar("A1_CEP"))						,Nil},; // CEP OK
	{"A1_PAIS"      ,IIF(!Empty(cPais),alltrim(cPais),Criavar("A1_PAIS"))					,Nil},; // Pais OK
	{"A1_CGC"       ,IIF(!Empty(cCGC),alltrim(cCGC),Criavar("A1_CGC"))						,Nil},; // CGC OK
	{"A1_TEL"     ,IIF(!Empty(cTel),alltrim(cTel),Criavar("A1_TEL"))				,Nil},; // Conta Contabil
	{"A1_CODPAIS"   ,IIF(!Empty(cCodPais),alltrim(cCodPais),Criavar("A1_CODPAIS"))				,Nil},; // Tipo de Pessoa
	{"A1_EMAIL"     ,IIF(!Empty(cEmail),alltrim(cEmail),Criavar("A1_EMAIL"))				,Nil},; // Email OK
	{"A1_YCODTCF"   ,IIF(!Empty(cYCODTCF),alltrim(cYCODTCF),Criavar("A1_YCODTCF"))		,Nil},;  // Cod do Cliente no SHIFT
	{"A1_MSBLQL"   ,IIF(!Empty(CBLOCO),alltrim(CBLOCO),Criavar("A1_MSBLQL"))		,Nil},;
		{"A1_YBLOQ"   ,IIF(!Empty(CBLOCL),alltrim(CBLOCL),Criavar("A1_YBLOQ"))		,Nil},;
		{"A1_YPORTAL"   ,IIF(!Empty(CTPPORT),alltrim(CTPPORT),Criavar("A1_YPORTAL"))		,Nil},;
		{"A1_YBASE"   ,IIF(!Empty(cCBASE),alltrim(cCBASE),Criavar("A1_YBASE"))		,Nil},;
		{"A1_BAIRROC"   ,IIF(!Empty(CBAIRROC),alltrim(CBAIRROC),Criavar("A1_BAIRROC"))		,Nil},;
		{"A1_BAIRROE"   ,IIF(!Empty(CBAIRROE),alltrim(CBAIRROE),Criavar("A1_BAIRROE"))		,Nil},;
		{"A1_ESTE"   ,IIF(!Empty(CUFENT),alltrim(CUFENT),Criavar("A1_ESTE"))		,Nil},;
		{"A1_YDTBLQ"   ,IIF(!Empty(cDataBloq),alltrim(cDataBloq),Criavar("A1_YDTBLQ"))		,Nil},;
		{"A1_OBSERV"   ,IIF(!Empty(Obs),alltrim(Obs),Criavar("A1_OBSERV"))		,Nil},;
		{"A1_YTPCLIE"   ,IIF(!Empty(cDTpCli),alltrim(cDTpCli),Criavar("A1_ESTE"))		,Nil},;
		{"A1_ENDENT"   ,IIF(!Empty(cEndENT),alltrim(cEndENT),Criavar("A1_ENDENT"))		,Nil},;
		{"A1_CEPE"   ,IIF(!Empty(cCEPE),alltrim(cCEPE),Criavar("A1_CEPE"))		,Nil},;
		{"A1_MUNE"   ,IIF(!Empty(cMUNE),alltrim(cMUNE),Criavar("A1_MUNE"))		,Nil},;
		{"A1_YGUID"   ,IIF(!Empty(cGuid),alltrim(cGuid),Criavar("A1_ESTE"))		,Nil},;
		{"A1_YCODGLO"   ,IIF(!Empty(cYCodGlo),alltrim(cYCodGlo),Criavar("A1_YCODGLO"))		,Nil}}  // Cod do Cliente no SHIFT
							 

	IF Len(aAuto) > 0
		MSExecAuto({|x,y| Mata030(x,y)},aAuto,3)
		If lMsErroAuto
	   			
			::Retorno := "ERRO," + cvaltochar(cClient) + "," + cvaltochar(cLoja)
		ELSE
	  		
			::Retorno := "OK," + SA1->A1_COD + "," + SA1->A1_LOJA

		EndIf

	EndIF
	
	T01->(DbCloseArea())
	
Else 
	cClient := T01->A1_COD
	cLoja	:= T01->A1_LOJA

	aAuto :=	{	 	 {"A1_COD"    ,IIF(!Empty(cClient),cClient,Criavar("A1_COD"))           	,Nil},; // Pessoa OK
	{"A1_LOJA"      ,IIF(!Empty(cLoja),alltrim(cLoja),Criavar("A1_LOJA"))               	,Nil},; // Loja OK
	{"A1_PESSOA"    ,IIF(!Empty(cPessoa),alltrim(cPessoa),Criavar("A1_PESSOA"))           	,Nil},; // Pessoa OK
	{"A1_NOME"      ,IIF(!Empty(cNome),alltrim(cNome),Criavar("A1_NOME"))				  	,Nil},; // Nome OK
	{"A1_NREDUZ"    ,IIF(!Empty(cNReduz),alltrim(cNReduz),Criavar("A1_NREDUZ"))			,Nil},; // Nome reduz. OK
	{"A1_END"       ,IIF(!Empty(cEnd),alltrim(cEnd),Criavar("A1_END"))						,Nil},; // Endereco OK
	{"A1_TIPO"      ,IIF(!Empty(cTipo),alltrim(cTipo),Criavar("A1_TIPO"))					,Nil},; // Tipo OK
	{"A1_EST"       ,IIF(!Empty(cEst),alltrim(cEst),Criavar("A1_EST"))		    			,Nil},; // Estado OK
	{"A1_COD_MUN"   ,IIF(!Empty(cCodMun),alltrim(cCodMun),Criavar("A1_COD_MUN"))			,Nil},; // Codigo Municipio OK
	{"A1_MUN"       ,IIF(!Empty(cMun),alltrim(cMun),Criavar("A1_MUN"))						,Nil},; // Cidade OK
	{"A1_BAIRRO"    ,IIF(!Empty(cBairro),alltrim(cBairro),Criavar("A1_BAIRRO"))			,Nil},; // Bairro OK
	{"A1_CEP"       ,IIF(!Empty(cCep),alltrim(cCep),Criavar("A1_CEP"))						,Nil},; // CEP OK
	{"A1_PAIS"      ,IIF(!Empty(cPais),alltrim(cPais),Criavar("A1_PAIS"))					,Nil},; // Pais OK
	{"A1_CGC"       ,IIF(!Empty(cCGC),alltrim(cCGC),Criavar("A1_CGC"))						,Nil},; // CGC OK
	{"A1_TEL"     ,IIF(!Empty(cTel),alltrim(cTel),Criavar("A1_TEL"))				,Nil},; // Conta Contabil
	{"A1_CODPAIS"   ,IIF(!Empty(cCodPais),alltrim(cCodPais),Criavar("A1_CODPAIS"))				,Nil},; // Tipo de Pessoa
	{"A1_EMAIL"     ,IIF(!Empty(cEmail),alltrim(cEmail),Criavar("A1_EMAIL"))				,Nil},; // Email OK
	{"A1_YCODTCF"   ,IIF(!Empty(cYCODTCF),alltrim(cYCODTCF),Criavar("A1_YCODTCF"))		,Nil},;  // Cod do Cliente no SHIFT
	{"A1_MSBLQL"   ,IIF(!Empty(CBLOCO),alltrim(CBLOCO),Criavar("A1_MSBLQL"))		,Nil},;
		{"A1_YBLOQ"   ,IIF(!Empty(CBLOCL),alltrim(CBLOCL),Criavar("A1_YBLOQ"))		,Nil},;
		{"A1_YPORTAL"   ,IIF(!Empty(CTPPORT),alltrim(CTPPORT),Criavar("A1_YPORTAL"))		,Nil},;
		{"A1_YBASE"   ,IIF(!Empty(cCBASE),alltrim(cCBASE),Criavar("A1_YBASE"))		,Nil},;
		{"A1_BAIRROC"   ,IIF(!Empty(CBAIRROC),alltrim(CBAIRROC),Criavar("A1_BAIRROC"))		,Nil},;
		{"A1_BAIRROE"   ,IIF(!Empty(CBAIRROE),alltrim(CBAIRROE),Criavar("A1_BAIRROE"))		,Nil},;
		{"A1_ESTE"   ,IIF(!Empty(CUFENT),alltrim(CUFENT),Criavar("A1_ESTE"))		,Nil},;
		{"A1_YDTBLQ"   ,IIF(!Empty(cDataBloq),alltrim(cDataBloq),Criavar("A1_YDTBLQ"))		,Nil},;
		{"A1_OBSERV"   ,IIF(!Empty(Obs),alltrim(Obs),Criavar("A1_OBSERV"))		,Nil},;
		{"A1_YTPCLIE"   ,IIF(!Empty(cDTpCli),alltrim(cDTpCli),Criavar("A1_ESTE"))		,Nil},;
		{"A1_ENDENT"   ,IIF(!Empty(cEndENT),alltrim(cEndENT),Criavar("A1_ENDENT"))		,Nil},;
		{"A1_CEPE"   ,IIF(!Empty(cCEPE),alltrim(cCEPE),Criavar("A1_CEPE"))		,Nil},;
		{"A1_MUNE"   ,IIF(!Empty(cMUNE),alltrim(cMUNE),Criavar("A1_MUNE"))		,Nil},;
		{"A1_YGUID"   ,IIF(!Empty(cGuid),alltrim(cGuid),Criavar("A1_ESTE"))		,Nil},;
		{"A1_YCODGLO"   ,IIF(!Empty(cYCodGlo),alltrim(cYCodGlo),Criavar("A1_YCODGLO"))		,Nil}}  // Cod do Cliente no SHIFT
							 		
	IF Len(aAuto) > 0
		MSExecAuto({|x,y| Mata030(x,y)},aAuto,4) 	//&("MSExecAuto({|x,y| "+::Rotina+"(x,y)},aAuto,"+cValToChar(::Opcao)+")")
		If lMsErroAuto
			
			::Retorno := "ERRO," + cvaltochar(cClient) + "," + cvaltochar(cLoja)
	  		
		ELSE
			::Retorno := "OK," + cvaltochar(cClient) + "," + cvaltochar(cLoja)
	  		

		EndIf
	EndIf
		

	T01->(DbCloseArea())
		//RESET ENVIRONMENT
		
		
endif
  
  
	ConOut("FIM do processamento do Job CRM: " + cGuid)
RestArea( aArea )
  
Return(lRet)


//--------------------------------------------------------------------------------------------------------------
