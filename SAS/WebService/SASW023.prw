#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "APWEBSRV.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#include "topconn.ch"
#include "vkey.ch"

//---------------------------------------------------------------//
// AUTOR: JOAO FILHO TOTVS-CE               DATA 13/07/16        //
//---------------------------------------------------------------//
// OBJ  : INTEGRA�AO WEBSERVICE , CADASTRO DE CONTRATOS          //
//        ONDE PARA CADA CONTRATO VAI SER FEITA UMA REQUISI��O   //
//        NO M�TODO DESENVOLVIDO                                 //
//---------------------------------------------------------------//



WsService sas_integracao	Description "Integra��o Contratos SAS"

	WsData Contratos							AS sas_integracao_Contratos
	WsData Retorno								AS sas_integracao_Retorno

	WsMethod CadastrarContrato		            Description "Cadastro de Contratos"


EndWsService


WsStruct sas_integracao_Retorno
	WsData dData					AS DATE
	WsData cHora				    AS String		
	WsData cOcorrencia			    AS String	
	WsData cObs 				    AS String OPTIONAL
	/*
	01 � INCLUIDO COM SUCESSO
	02 � CONTRATO J� CADASTRADO
	03 � FILIAL NAO ENCONTRADA
	04 � CLIENTE NAO ENCONTRADO
	05 � PRODUTO NAO ENCONTRATO
	06 � TES NAO ENCONTRADA
	07 � NATUREZA NAO ENCONTRADA
	08 � CENTRO DE CUSTO NAO ENCONTRATO
	09 � CONDI��O DE PAGAMENTO NAO ENCONTRADA
	10 � VALOR UNITARIO INVALIDO
	11 � VALOR DESCONTO INVALIDO
	12 � VALOR TOTAL INVALIDO
	99 � ERRO GEN�RIO	
	*/

EndWsStruct

WsStruct sas_integracao_Contratos

	WsData Filial_Origem		AS String   
	WsData Cliente_cod			AS STRING
	WsData Loja_cod             AS STRING
	WsData Natureza_finan       AS STRING
	WsData Dt_emissao           AS DATE
	WsData Dt_iniVir            AS DATE
	WsData Dt_finVir            AS DATE
	WsData Ano_competencia      AS STRING // TAMANHO 4
	WsData Dt_assinatura		AS DATE 
	WsData Frtcli               AS FLOAT  
	WsData FrtEmp               AS FLOAT  
	WsData Cond_Paga            AS STRING //TAMANHO 3
	WsData Dt_ini_cp            AS DATE
	WsData Centro_custo         AS STRING
	WsData Tipo_fat             AS STRING // 1 DIRETO 2 TRIANGULAR 3 AMBOS
	WsData Conforme_nota        AS STRING // 1 NAO 2 SIM 
	WsData Tipo_contrato        AS STRING // P PROSPEC��O V VENDA B BONIFICACAO L LIVRARIAS I INTERCOMPANY M MANIFESTO 
	WsData c6_ano               AS STRING // S SIM N NAO
	WsData c7_ano               AS STRING // S SIM N NAO
	WsData c8_ano               AS STRING // S SIM N NAO
	WsData c9_ano               AS STRING // S SIM N NAO
	WsData c1_SERIE             AS STRING // S SIM N NAO
	WsData c2_SERIE             AS STRING // S SIM N NAO
	WsData PREUNI               AS STRING // S SIM N NAO
	WsData IDPRECTR             AS STRING // S SIM N NAO		
	WsData itens                AS Array Of sas_integracao_ItemContratos

EndWsStruct

WsStruct sas_integracao_ItemContratos
	WsData item_filial            AS STRING 
	//WsData item_contrato          AS STRING 
	WsData item_CodCli            AS STRING 
	WsData item_LojaCli           AS STRING 
	//WsData item_NomeCli           AS STRING 
	WsData item_ItemSec		      AS STRING 
	WsData item_Produto			  AS STRING 
	//WsData item_DescProd          AS STRING 	
	WsData item_Quantida          AS FLOAT	
	WsData item_TES               AS STRING
	//WsData item_ValUn             AS FLOAT 	
	WsData item_ValTot            AS FLOAT 	
	WsData item_ValDesc			  AS FLOAT 	 OPTIONAL
	//WsData item_ValLic			  AS FLOAT 		
	WsData item_tipo              AS STRING // S SIM N NAO	
EndWsStruct



WsMethod CadastrarContrato	wsReceive Contratos  wsSend Retorno	wsService sas_integracao

	Local oModel   
	Local cNumContra := ""
	Local aProd := {}
	Local nSec  := 0 
	Local nTotDesc := 0
	Local nAdianta := 0
	Local aArea := getarea()
	Local lc6_ano := IIF(Contratos:c6_ano == 'T',.T.,.F.)
	Local lc7_ano := IIF(Contratos:c7_ano == 'T',.T.,.F.) 
	Local lc8_ano := IIF(Contratos:c8_ano == 'T',.T.,.F.)
	Local lc9_ano := IIF(Contratos:c9_ano == 'T',.T.,.F.)
	Local lc1_SERIE := IIF(Contratos:c1_SERIE == 'T',.T.,.F.)
	Local lc2_SERIE := IIF(Contratos:c2_SERIE == 'T',.T.,.F.) 
	Local lPREUNI	:= IIF(Contratos:PREUNI == 'T',.T.,.F.) 		
	Public aKitws := {}


	// PEGAR O ULTIMO DO CONTRATO E SOMA 1 PARA INTEGRAR
	cQuery := " SELECT  MAX(ZZ0_NUMERO) ZZ0_NUMERO "
	cQuery += " FROM  "+RETSQLNAME("ZZ0")+" ZZ0 "
	cQuery += " WHERE ZZ0_FILIAL = '"+ ALLTRIM(Contratos:Filial_Origem) +"' "
	cQuery += " AND ZZ0.D_E_L_E_T_ = '' "
	TcQuery cQuery New Alias TZZ0

	IF !TZZ0->(EOF())		
		cNumContra :=  CVALTOCHAR(VAL(TZZ0->ZZ0_NUMERO) + 1)
		cNumContra :=  STRZERO(VAL(cNumContra),6)
	ENDIF

	TZZ0->(DbCloseArea())
	
	IF !existCpo("SM0",cEmpAnt+ALLTRIM(Contratos:Filial_Origem)) 

		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()				   		
		::Retorno:cOcorrencia	:= "03 � FILIAL NAO ENCONTRADA"
		::Retorno:cObs	:= "Filial do cabe�alho nao encontrada"

		RESTAREA( aArea )
		return .T.
	ENDIF

	for nAux := 1 to len(Contratos:itens)
		//VALIDA��O DAS FILIAL (itens)
		IF !existCpo("SM0",cEmpAnt+ALLTRIM(Contratos:itens[nAux]:item_filial)) 

			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()				   		
			::Retorno:cOcorrencia	:= "03 � FILIAL NAO ENCONTRADA"
			::Retorno:cObs	:= "Filial do item "+ Contratos:itens[nAux]:item_ItemSec +" nao encontrada"

			RESTAREA( aArea )
			return .T.
		ENDIF

		//VALIDA��O DAS CLIENTE (itens)
		IF !existCpo("SA1",ALLTRIM(Contratos:itens[nAux]:item_CodCli)+ ALLTRIM(Contratos:itens[nAux]:item_LojaCli),1) 

			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()				   		
			::Retorno:cOcorrencia	:= "04 � CLIENTE NAO ENCONTRADO"
			::Retorno:cObs	:= "Cliente do item "+ Contratos:itens[nAux]:item_ItemSec +" nao encontrado"

			RESTAREA( aArea )
			return .T.
		ENDIF
		//DBCLOSEAREA("SA1")

		// VALIDA PRODUTOS (ITENS)
		if  Contratos:itens[nAux]:item_tipo	= 'P'
			IF !existCpo("SB1",ALLTRIM(Contratos:itens[nAux]:item_Produto)) 

				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()				   		
				::Retorno:cOcorrencia	:= "05 � PRODUTO NAO ENCONTRATO " + Contratos:itens[nAux]:item_ItemSec  
				::Retorno:cObs	:= "Produto do item "+ Contratos:itens[nAux]:item_ItemSec +" nao encontrado"

				RESTAREA( aArea )
				return .T.
			ENDIF

		elseif  Contratos:itens[nAux]:item_tipo	= 'K' 
			cQuery := " SELECT  *
			cQuery += " FROM  "+RETSQLNAME("ZZ7")+" ZZ7 "
			cQuery += " WHERE ZZ7_TIPO = 'S' "
			cQuery += " AND   ZZ7_CODIGO = '"+ ALLTRIM(Contratos:itens[nAux]:item_Produto) +"' "
			cQuery += " AND   D_E_L_E_T_ = '' "

			TcQuery cQuery New Alias TZZ7
			IF TZZ7->(EOF())
				::Retorno:dData := DATE()
				::Retorno:cHora := TIME()				   		
				::Retorno:cOcorrencia	:= "05 � PRODUTO NAO ENCONTRATO " + ALLTRIM(Contratos:itens[nAux]:item_Produto)
				::Retorno:cObs	:= "Kit do item "+ Contratos:itens[nAux]:item_ItemSec +" nao encontrado"
				TZZ7->(DbCloseArea())

				RESTAREA( aArea )
				return .T.
			ENDIF
			TZZ7->(DbCloseArea())
		else	
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()				   		
			::Retorno:cOcorrencia	:= "99 � ERRO GENERICO"
			::Retorno:cObs	:= " Tipo do produto do item "+ Contratos:itens[nAux]:item_ItemSec +" nao definido"

			RESTAREA( aArea )
			return .T.
		endif

		//VALIDA��O DAS TES (itens)
		cQuery := "  SELECT * "
		cQuery += "  FROM "+RETSQLNAME("SF4")+" SF4 "
		cQuery += "  WHERE F4_FILIAL = '" + ALLTRIM(XFILIAL("SED",ALLTRIM(Contratos:Filial_Origem))) + "' "
		cQuery += "  AND F4_CODIGO = '" + ALLTRIM(Contratos:itens[nAux]:item_TES) + "' "
		TcQuery cQuery New Alias TSF4

		IF TSF4->(EOF())
			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()				   		
			::Retorno:cOcorrencia	:= "06 � TES NAO ENCONTRADA"
			::Retorno:cObs	:= "TES do item "+ Contratos:itens[nAux]:item_ItemSec +" nao encontrado"
			TSF4->(DbCloseArea())

			RESTAREA( aArea )
			return .T.
		ENDIF
		TSF4->(DbCloseArea())

		// VALIDA O VALOR total ( val unitario * quantidade = valor total)
		IF Contratos:itens[nAux]:item_ValTot == 0

			::Retorno:dData := DATE()
			::Retorno:cHora := TIME()				   		
			::Retorno:cOcorrencia	:= "12 � VALOR TOTAL INVALIDO"
			::Retorno:cObs	:= "Valor total do item "+ Contratos:itens[nAux]:item_ItemSec +" est� zerado "

			RESTAREA( aArea )
			return .T.
		ENDIF
	next

	// VALIDA��O DO CLIENTE 

	IF !existCpo("SA1",ALLTRIM(Contratos:Cliente_cod)+ALLTRIM(Contratos:Loja_cod),1)

		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()				   		
		::Retorno:cOcorrencia	:= "04 � CLIENTE NAO ENCONTRADO"
		::Retorno:cObs	:= "Cliente do item "+ Contratos:itens[nAux]:item_ItemSec +" nao encontrado"

		RESTAREA( aArea )
		return .T.
	ENDIF
	//DBCLOSEAREA("SA1")
	//VALIDA��O DAS NATUREZA (CABE�ALHO)

	cQuery := "  SELECT * "
	cQuery += "  FROM "+RETSQLNAME("SED")+" SED "
	cQuery += "  WHERE ED_FILIAL = '" + ALLTRIM(XFILIAL("SED",ALLTRIM(Contratos:Filial_Origem))) + "' "
	cQuery += "  AND ED_CODIGO = '" + ALLTRIM(Contratos:Natureza_finan) + "' "
	TcQuery cQuery New Alias TSED

	IF TSED->(EOF())

		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()				   		
		::Retorno:cOcorrencia	:= "07 � NATUREZA NAO ENCONTRADA"
		TSED->(DbCloseArea())

		RESTAREA( aArea )
		return .T.
	ENDIF
	TSED->(DbCloseArea())

	//VALIDA��O DE CENTRO DE CUSTO

	cQuery := "  SELECT * "
	cQuery += "  FROM "+RETSQLNAME("CTT")+" CTT "
	cQuery += "  WHERE CTT_FILIAL = '" + ALLTRIM(XFILIAL("CTT",ALLTRIM(Contratos:Filial_Origem))) + "' "
	cQuery += "  AND CTT_CUSTO = '" + ALLTRIM(Contratos:Centro_custo) + "' "
	TcQuery cQuery New Alias TCTT

	IF TCTT->(EOF())

		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()				   		
		::Retorno:cOcorrencia	:= "08 � CENTRO DE CUSTO NAO ENCONTRADO"
		::Retorno:cObs	:= "Centro de Custo "+ ALLTRIM(Contratos:Centro_custo) +" nao encontrado"
		TCTT->(DbCloseArea())

		RESTAREA( aArea )
		return .T.
	ENDIF
	TCTT->(DbCloseArea())

	//VALIDA��O DE CONDICAO DE PAGAMENTO

	cQuery := "  SELECT * "
	cQuery += "  FROM "+RETSQLNAME("SE4")+" SE4 "
	cQuery += "  WHERE E4_FILIAL = '" + ALLTRIM(XFILIAL("SE4",ALLTRIM(Contratos:Filial_Origem))) + "' "
	cQuery += "  AND E4_CODIGO = '" + ALLTRIM(Contratos:Cond_Paga) + "' "
	TcQuery cQuery New Alias TSE4

	IF TSE4->(EOF())  

		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()				   		
		::Retorno:cOcorrencia	:= "09 � CONDI��O DE PAGAMENTO NAO ENCONTRADA"
		::Retorno:cObs	:= "Condicao de pagamento "+ ALLTRIM(Contratos:Cond_Paga) +" nao encontrado"
		TSE4->(DbCloseArea())

		RESTAREA( aArea )
		return .T.
	ENDIF
	TSE4->(DbCloseArea())

	cQuery := " SELECT * "
	cQuery += " FROM "+RETSQLNAME("ZZ0")+" ZZ0 "
	cQuery += " WHERE ZZ0_FILIAL = '"+ ALLTRIM(Contratos:Filial_Origem) +"' "
	cQuery += " AND   ZZ0_NUMERO = '"+ cNumContra  +"' " 
	//cQuery += " AND   ZZ0_CLIENT = '"+ ALLTRIM(Contratos:Cliente_cod)   +"' "
	//cQuery += " AND   ZZ0_LOJA   = '"+ ALLTRIM(Contratos:Loja_cod)      +"' "
	cQuery += " AND   D_E_L_E_T_ = '' "
	TcQuery cQuery New Alias TZZ0

	IF !TZZ0->(EOF())
		//JA EXISTE ESSE CONTRATO

		TZZ0->(DbCloseArea())
		::Retorno:dData := DATE()
		::Retorno:cHora := TIME()				   		
		::Retorno:cOcorrencia	:= "02 � CONTRATO J� CADASTRADO"

		RESTAREA( aArea )

		return .T.
	ENDIF
	//VALIDA��O DAS FILIAL (CABE�ALHO)
		TZZ0->(DbCloseArea())
	conout('teste passou')

	//MONTAGEM DO ARRAY DE KTIS
	for nAux := 1 to len(Contratos:itens)
		if Contratos:itens[nAux]:item_tipo == "K" 
			MONTAKIT(Contratos:itens[nAux]:item_Produto)			
			for nBux := 1 to len(aKitws)
				nSec ++

				aadd(aProd,{Contratos:itens[nAux]:item_filial,;
				cNumContra,;
				Contratos:itens[nAux]:item_CodCli,;
				Contratos:itens[nAux]:item_LojaCli,;
				strzero(nSec,3),;
				aKitws[nBux][1] ,;
				Contratos:itens[nAux]:item_Quantida,;
				Contratos:itens[nAux]:item_TES,;
				ROUND((Contratos:itens[nAux]:item_ValTot / Contratos:itens[nAux]:item_Quantida)  * aKitws[nBux][2],2) + aKitws[nBux][3] + aKitws[nBux][5],;
				ROUND(((Contratos:itens[nAux]:item_ValTot / Contratos:itens[nAux]:item_Quantida)  * aKitws[nBux][2]) + aKitws[nBux][3] + aKitws[nBux][5],2) * Contratos:itens[nAux]:item_Quantida,;
				aKitws[nBux][3],;
				'A',;
				Contratos:itens[nAux]:item_tipo,;
				aKitws[nBux][4],;
				aKitws[nBux][6],;
				})
				nTotDesc += aKitws[nBux][3]
				nAdianta += (aProd[nAux][9] - aProd[nAux][11]) * aProd[nAux][7]
			next

			nTotDesc := 0
			aKitws := {}
		elseif 	Contratos:itens[nAux]:item_tipo == "P"
			nSec ++
			aadd(aProd,{Contratos:itens[nAux]:item_filial,;
			cNumContra,;
			Contratos:itens[nAux]:item_CodCli,;
			Contratos:itens[nAux]:item_LojaCli,;
			strzero(nSec,3),;
			Contratos:itens[nAux]:item_Produto,;
			Contratos:itens[nAux]:item_Quantida,;
			Contratos:itens[nAux]:item_TES,;
			Contratos:itens[nAux]:item_ValTot / Contratos:itens[nAux]:item_Quantida,;
			Contratos:itens[nAux]:item_ValTot,;
			0,;
			'A',;
			Contratos:itens[nAux]:item_tipo,;
			0,;	
			0,;
			})
			nAdianta += Contratos:itens[nAux]:item_ValTot * Contratos:itens[nAux]:item_Quantida
		endif
	next

	// INCLUIDO O CABE�ALHO DO CONTRATO
	dbSelectArea("ZZ0")
	RECLOCK("ZZ0",.T.)

	ZZ0->ZZ0_FILIAL := Contratos:Filial_Origem
	ZZ0->ZZ0_NUMCRM := ""
	ZZ0->ZZ0_NUMERO := cNumContra
	ZZ0->ZZ0_CLIENT := Contratos:Cliente_cod
	ZZ0->ZZ0_LOJA   := Contratos:Loja_cod
	ZZ0->ZZ0_NOME   := posicione("SA1",1,XFILIAL("SA1",aProd[nAux][1]) + aProd[nAux][3] + aProd[nAux][4] , "A1_NOME")
	IF ZZ0->ZZ0_FILIAL == "030101" .OR. ZZ0->ZZ0_FILIAL == "030102"
		ZZ0->ZZ0_NATURE := "1109"
	ELSEIF  ZZ0->ZZ0_FILIAL == "040101"
		ZZ0->ZZ0_NATURE := "1109"
	ELSE 	
		ZZ0->ZZ0_NATURE := "1108"
	ENDIF
	ZZ0->ZZ0_EMISSA := ddatabase //Contratos:Dt_emissao
	ZZ0->ZZ0_VIGINI := ddatabase //Contratos:Dt_iniVir
	ZZ0->ZZ0_VIGFIM := CTOD('31/12/2017')//Contratos:Dt_finVir
	ZZ0->ZZ0_ANOCOM := Contratos:Ano_competencia
	ZZ0->ZZ0_CODCRM := ""
	ZZ0->ZZ0_DTASSI := Contratos:Dt_assinatura
	ZZ0->ZZ0_FRTCLI	:= Contratos:Frtcli
	ZZ0->ZZ0_FRTEMP	:= Contratos:FrtEmp
	ZZ0->ZZ0_ADIANT := nAdianta
	ZZ0->ZZ0_DTCOND := Contratos:Dt_ini_cp
	ZZ0->ZZ0_CPFIN  := Contratos:Cond_Paga
	ZZ0->ZZ0_CCUSTO := "9017"//GETMV("SA_CCCTR")
	ZZ0->ZZ0_FATDIR := Contratos:Tipo_fat
	IF Contratos:Conforme_nota == 'N' .OR. Contratos:Conforme_nota == '1'
	ZZ0->ZZ0_CONFNF := '1'
	ELSE
	ZZ0->ZZ0_CONFNF := '2'
	ENDIF
	ZZ0->ZZ0_TIPCTR := Contratos:Tipo_contrato
	ZZ0->ZZ0_SITUAC := "N"
	ZZ0->ZZ0_STATUS := "A"
	ZZ0->ZZ0_CONORI := ""
	ZZ0->ZZ0_CLIORI := ""
	ZZ0->ZZ0_LOJORI := ""
	ZZ0->ZZ0_APROV  := ""
	ZZ0->ZZ0_IDPRCT := Contratos:IDPRECTR
	ZZ0->ZZ0_6ANO   := lc6_ano
	ZZ0->ZZ0_7ANO   := lc7_ano
	ZZ0->ZZ0_8ANO   := lc8_ano
	ZZ0->ZZ0_9ANO   := lc9_ano
	ZZ0->ZZ0_1SERIE := lc1_SERIE
	ZZ0->ZZ0_2SERIE := lc2_SERIE
	ZZ0->ZZ0_PREUNI := lPREUNI 

	ZZ0->(MSUNLOCK())

	for nAux := 1 TO len(aProd)
		dbSelectArea("ZZ1")
		RECLOCK("ZZ1",.T.)

		ZZ1->ZZ1_FILIAL  := aProd[nAux][1]
		ZZ1->ZZ1_NUMERO  := aProd[nAux][2]
		ZZ1->ZZ1_CLIENT  := aProd[nAux][3]
		ZZ1->ZZ1_LOJA    := aProd[nAux][4]
		ZZ1->ZZ1_NOME    := posicione("SA1",1,XFILIAL("SA1",aProd[nAux][1]) + aProd[nAux][3] + aProd[nAux][4] , "A1_NOME")
		ZZ1->ZZ1_ITEM    := aProd[nAux][5]
		ZZ1->ZZ1_PRODUT  := aProd[nAux][6]
		IF	ALLTRIM(aProd[nAux][13]) == "P"
			ZZ1->ZZ1_DESC    := posicione("SB1",1,XFILIAL("SB1",aProd[nAux][1]) + aProd[nAux][6] , "B1_DESC")
		ELSEIF ALLTRIM(aProd[nAux][13]) == "K"
			cQuery := " SELECT ZZ7_CODIGO, ZZ7_DESCR "
			cQuery += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7 "
			cQuery += "  WHERE D_E_L_E_T_ = '' "
			cQuery += "    AND ZZ7_CODIGO = '" + aProd[nAux][6] +  "' "
			cQuery += "    AND ZZ7_TIPO = 'S' "
			TcQuery cQuery New Alias TKIT

			ZZ1->ZZ1_DESC    := TKIT->ZZ7_DESCR

			TKIT->(DbCloseArea())
		ENDIF
		ZZ1->ZZ1_QUANT   := aProd[nAux][7]
		ZZ1->ZZ1_SALDO   := aProd[nAux][7]
		ZZ1->ZZ1_TES     := aProd[nAux][8]

		cQuery := " SELECT ZZ7_CODIGO, ZZ7_DESCR,ZZ7_CATEGO "
		cQuery += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7 "
		cQuery += "  WHERE D_E_L_E_T_ = '' "
		cQuery += "    AND ZZ7_CODIGO = '" + aProd[nAux][6] +  "' "
		cQuery += "    AND ZZ7_TIPO = 'S' "
		//cQuery += "    AND ZZ7_CODIGO NOT IN ('2010210041001','2010211060801','2010212041001','2010213011301','2010214011401','2010215011401','2010216011401')"
		//cQuery += "    AND ZZ7_CATEGO = 'P' "
		TcQuery cQuery New Alias TKIT1

		IF ALLTRIM(aProd[nAux][13]) == "K" .AND. TKIT1->ZZ7_CATEGO == "P" .AND. !(ALLTRIM(TKIT1->ZZ7_CODIGO) $ GETMV("SA_KITTURM"))
			ZZ1->ZZ1_VLRUN   := aProd[nAux][14]
			ZZ1->ZZ1_VLRTOT  := aProd[nAux][7] * aProd[nAux][14]
			ZZ1->ZZ1_VLRLIQ  := (aProd[nAux][7] * aProd[nAux][14]) - aProd[nAux][11]  //(aProd[nAux][9] - aProd[nAux][11]) * aProd[nAux][7]
		ELSEIF ALLTRIM(TKIT1->ZZ7_CODIGO) $ GETMV("SA_KITTURM")
			ZZ1->ZZ1_VLRUN   := aProd[nAux][15]
			ZZ1->ZZ1_VLRTOT  := aProd[nAux][7] * aProd[nAux][15]
			ZZ1->ZZ1_VLRLIQ  := (aProd[nAux][7] * aProd[nAux][15]) - aProd[nAux][11]
		ELSE
			ZZ1->ZZ1_VLRUN   := aProd[nAux][9]
			ZZ1->ZZ1_VLRTOT  := aProd[nAux][10]
			ZZ1->ZZ1_VLRLIQ  := (aProd[nAux][9] - aProd[nAux][11]) * aProd[nAux][7]
		ENDIF

		TKIT1->(DbCloseArea())

		ZZ1->ZZ1_VLRDSC  := aProd[nAux][11]	
		ZZ1->ZZ1_STATUS  := aProd[nAux][12]
		ZZ1->ZZ1_TIPO    := aProd[nAux][13]	
		ZZ1->(MSUNLOCK())
	next

	::Retorno:dData := DATE()
	::Retorno:cHora := TIME()				   		
	::Retorno:cOcorrencia	:= "01 � INCLUIDO COM SUCESSO"
	::Retorno:cObs	:= "SUCESSO NA INTEGRACAO"

	RESTAREA( aArea )
return .T.

STATIC FUNCTION MONTAKIT(cKit)

	Local nRegistros := 0
	Local cTipo :=  ""
	Local cDesc := ""
	Local nRateio := 0
	Local nPerDes := 0
	Local cQuery8 := ""
	Local nValorDes := 0
	Local cAliasT := GetNextAlias()
	Local nRateio
	Local nDesconto

	cQuery := ""
	cQuery += " SELECT ZZ7_CODIGO, ZZ7_DESCR,ZZ7_CATEGO "
	cQuery += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7 "
	cQuery += "  WHERE D_E_L_E_T_ = '' "
	cQuery += "    AND ZZ7_CODPAI = '" + cKit +  "' "
	cQuery += "    AND ZZ7_TIPO = 'S' "
	cQuery += "    AND ZZ7_CODIGO <> '" + cKit + "' "  
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasT,.F.,.T.)

	Count To nRegistros

	(cAliasT)->(DBGOTOP())

	if nRegistros > 0

		While (cAliasT)->(!EOF())												

			MONTAKIT((cAliasT)->ZZ7_CODIGO)								

			(cAliasT)->(DBSKIP())				

		EndDo

	Else
		nValor    := ValProf(cKit)		
		nRateio   := RATEIOKIT(cKit)
		nDesconto := DESCKIT(cKit)
		nValor1   := Turma(cKit)
		nAcres    := Acres(cKit)

		AADD(aKitws, {cKit,nRateio,nDesconto,nValor,nAcres,nValor1,(cAliasT)->ZZ7_DESCR})

		(cAliasT)->(DBCLOSEAREA())

	EndIf

RETURN 

static function RATEIOKIT(cKit)

	LOCAL nRateio 

	cQuery := ""
	cQuery += " SELECT ZZK_PORCEN "
	cQuery += "   FROM " + RETSQLNAME("ZZK") + " ZZK "
	cQuery += "  INNER JOIN  " + RETSQLNAME("ZZ7") + " ZZ7 "
	cQuery += "     ON ZZK.ZZK_FILIAL = ZZ7.ZZ7_FILIAL "
	cQuery += "    AND ZZK.ZZK_SERIE = ZZ7.ZZ7_SERIE "
	cQuery += "    AND ZZK.ZZK_ENVIO = ZZ7.ZZ7_ENVIO "
	cQuery += "  WHERE ZZK.D_E_L_E_T_ = '' "
	cQuery += "    AND ZZ7.D_E_L_E_T_ = '' "
	cQuery += "    AND ZZ7.ZZ7_CODIGO = '" + cKit + "' "
	TcQuery cQuery New Alias TRAT

	If TRAT->(!EOF())
		nRateio := TRAT->ZZK_PORCEN / 100
	Else
		nRateio := 1
	EndIf

	TRAT->(DBCLOSEAREA())

RETURN nRateio

static function DESCKIT(cKit)

	LOCAL nDesconto 

	cQuery8 := ""
	cQuery8 += " SELECT COUNT(*) AS QTD "
	cQuery8 += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7"
	cQuery8 += "  WHERE D_E_L_E_T_ = '' "
	cQuery8 += "    AND ZZ7.ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
	cQuery8 += "    AND ZZ7.ZZ7_CODPAI = '" + cKit + "' "
	cQuery8 += "    AND ZZ7.ZZ7_SUPLEM = 'S' " 
	cQuery8 += "    AND ZZ7.ZZ7_CODPAI NOT IN ('2010210011001','2010211011001','2010212011001','2010213011301','2010214011401','2010215011401','2010216011401','2010217011401')" 
	TcQuery cQuery8 New Alias TDESC

	If TDESC->(!EOF())

		nDesconto := TDESC->QTD * 10
	Else 
		nDesconto := 0
	EndIf
	TDESC->(DBCLOSEAREA())

RETURN nDesconto


// Fun��o para calcular o valor do Professor
// Weskley Silva 31.08.2016
Static Function ValProf(cKit)

	Local cQrySAS := ""
	Private nValor := 0 

	if Select("QSASPrf") > 0
		dbCloseArea("QSASPrf")
	endif

	nProf = 0

	cQrySAS := ""
	cQrySAS += " SELECT COUNT(*) AS QTD "
	cQrySAS += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7"
	cQrySAS += "  WHERE D_E_L_E_T_ = '' "
	cQrySAS += "    AND ZZ7.ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
	cQrySAS += "    AND ZZ7.ZZ7_CODPAI = '" + cKit + "' "
	cQrySAS += "    AND ZZ7.ZZ7_CATEGO = 'P' "  
	cQrySAS += "    AND ZZ7.ZZ7_CODPAI NOT IN ('2010210011001','2010211011001','2010212011001','2010213011301','2010214011401','2010215011401','2010216011401','2010217011401')

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySAS),"QSASPrf",.F.,.T.)

	nProf:= QSASPrf->QTD

	if nProf > 0
		nValor := nProf * 10
	endif

	dbCloseArea("QSASPrf")

return nValor


// Fun��o para Somar 10 reais nos Kits de Aluno ou Coordena��o que cont�m um material de professor
// Weskley Silva 01.09.2016

Static Function Acres(cKit) 

	Local nAcres := 0
	Local cQuery := ""

	if Select("QUERY") > 0
		dbCloseArea("QUERY")
	endif 

	nCont := 0

	cQuery := ""
	cQuery += " SELECT COUNT(*) AS QTD1 FROM FN_PEDIDOS_VENDA('" + cKit +"')  "
	cQuery += " WHERE ZZ7_DESCR LIKE '%PROF%' AND ZZ7_CATEGO IN ('A','C') "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"QUERY",.F.,.T.)

	nCont:= QUERY->QTD1

	IF nCont > 0 
		nAcres := nCont * 10
	endif	

	dbCloseArea("QUERY") 

Return nAcres

Static Function Turma(cKit)
	
	Local cQrySAS := ""
	Private nValor := 0 

	if Select("QSASTur") > 0
		dbCloseArea("QSASTur")
	endif

	nTurma = 0

	cQrySAS := ""
	cQrySAS += " SELECT COUNT(*) AS QTD "
	cQrySAS += "   FROM " + RETSQLNAME("ZZ7") + " ZZ7"
	cQrySAS += "  WHERE D_E_L_E_T_ = '' "
	cQrySAS += "    AND ZZ7.ZZ7_FILIAL = '" + XFILIAL("ZZ7") + "' "
	cQrySAS += "    AND ZZ7.ZZ7_CODIGO = '" + cKit + "' "
	cQrySAS += "    AND ZZ7.ZZ7_CODIGO IN ('2010210011001','2010211011001','2010212011001','2010213011301','2010214011401','2010215011401','2010216011401','2010217011401') "  

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrySAS),"QSASTur",.F.,.T.)

	nTurma:= QSASTur->QTD

	if nTurma > 0
		nValor1 := nTurma * 20
	else 
		nValor1 := 0
	endif

	dbCloseArea("QSASTur")
	
return nValor1

