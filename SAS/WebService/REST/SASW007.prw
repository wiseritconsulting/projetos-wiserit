#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"


WSRESTFUL CONTRATOS DESCRIPTION "Contratos"

WSDATA count      AS INTEGER
WSDATA startIndex AS INTEGER

WSMETHOD GET DESCRIPTION "Exemplo de retorno de entidade(s)" WSSYNTAX "/ZZ1 || /cGuid"

END WSRESTFUL

// O metodo GET nao precisa necessariamente receber parametros de querystring, por exemplo:
// WSMETHOD GET WSSERVICE sample 
WSMETHOD GET WSRECEIVE startIndex, count WSSERVICE CONTRATOS
	Local i
	Local nTotReg   	:= 0 
	Local nCont			:= 0
	Local nCont2		:= 0
	Local nContratos	:= 0

	// define o tipo de retorno do m�todo
	::SetContentType("application/json")

	// verifica se recebeu parametro pela URL
	// exemplo: http://localhost:8080/sample/1
	If Len(::aURLParms) > 0

		cQuery := " SELECT ZZ0_FILIAL,ZZ0_CLIENT,ZZ0_LOJA,ZZ0_NUMERO,A1_NOME,A1_EST,ZZ0_ANOCOM FROM "+RetSqlName("ZZ0")+" ZZ0(NOLOCK) "
		cQuery += " INNER JOIN "+RetSqlName("SA1")+" SA1(NOLOCK) "
		cQuery += " ON ZZ0_CLIENT = A1_COD AND ZZ0_LOJA = A1_LOJA AND SA1.D_E_L_E_T_ = '' "
		cQuery += " WHERE ZZ0.D_E_L_E_T_ = '' "
		cQuery += " AND SA1.A1_YGUID = '"+self:aQueryString[1][2]+"' "
		cQuery += " AND ZZ0.ZZ0_STATUS IN ('A','L') "
		IF Select("T01") > 0
			T01->(DbCloseArea())
		endif
		TcQuery cQuery new Alias T01
		nContratos := Contar("T01","!Eof()")
		T01->(DbGoTop())
		::SetResponse('[')
		WHILE !T01->(EOF())
			nCont2++
			cQuery := " SELECT ZZ0_FILIAL, "
			cQuery += " ZZ0_NUMERO, "
			cQuery += " ZZ0_CLIENT, "
			cQuery += " ZZ0_LOJA, "
			cQuery += " ZZ1_PRODUT, "
			cQuery += " ZZ1_ITEM, "
			cQuery += " ZZ1_DESC, "
			cQuery += " ZZ1_QUANT, "
			cQuery += " ZZ1_VLRUN, "
			cQuery += " ZZ1_VLRTOT, "
			cQuery += " ZZ1_TES, "
			cQuery += " ZZ1_TIPO, "
			cQuery += " CASE ZZ1_TIPO "
			cQuery += " WHEN 'K' THEN "
			cQuery += " (SELECT TOP 1 ZZ7_ENVIO "
			cQuery += " FROM "+RetSqlName("ZZ7")+" "
			cQuery += " WHERE ZZ7_CODIGO=ZZ1_PRODUT "
			cQuery += " AND D_E_L_E_T_='') "
			cQuery += " ELSE "
			cQuery += " (SELECT TOP 1 B1_YVOL "
			cQuery += " FROM "+RetSqlName("SB1")+" "
			cQuery += " WHERE B1_COD=ZZ1_PRODUT "
			cQuery += " AND D_E_L_E_T_='') "
			cQuery += " END AS ENVIO "
			cQuery += " FROM "+RetSqlName("ZZ0")+" ZZ0 "
			cQuery += " INNER JOIN "+RetSqlName("ZZ1")+" ZZ1 ON ZZ0.ZZ0_FILIAL = ZZ1.ZZ1_FILIAL "
			cQuery += " AND ZZ0.ZZ0_NUMERO = ZZ1.ZZ1_NUMERO "
			cQuery += " AND ZZ0.ZZ0_CLIENT = ZZ1.ZZ1_CLIENT "
			cQuery += " AND ZZ0.ZZ0_LOJA = ZZ1.ZZ1_LOJA "
			cQuery += " AND ZZ1.D_E_L_E_T_='' "
			cQuery += " AND ZZ0.ZZ0_FILIAL = '"+T01->ZZ0_FILIAL+"' "
			cQuery += " AND ZZ0.ZZ0_NUMERO = '"+T01->ZZ0_NUMERO+"' "
			cQuery += " AND ZZ0.ZZ0_CLIENT = '"+T01->ZZ0_CLIENT+"' "
			cQuery += " AND ZZ0.ZZ0_LOJA = '"+T01->ZZ0_LOJA+"' "
			cQuery += " AND ZZ0.D_E_L_E_T_='' "
			cQuery += " AND ZZ0.ZZ0_STATUS IN ('A','L') "
			cQuery += " ORDER BY 2,1 "
			IF Select("T02") > 0
				T02->(DbCloseArea())
			endif
			TcQuery cQuery New Alias T02

			nTotReg := Contar("T02","!Eof()")
			T02->(DbGoTop())
			::SetResponse('{')
			::SetResponse('"NumContrato": "'		+ T01->ZZ0_NUMERO 									+'",')
			::SetResponse('"Filial": "'			+ T01->ZZ0_FILIAL 									+'",')   
			::SetResponse('"CodigoCliente": "'	+ T01->ZZ0_CLIENT										+'",')
			::SetResponse('"Loja": "'			+ AllTrim(T01->ZZ0_LOJA) 							+'",')
			::SetResponse('"RazaoSocial": "'		+ AllTrim(T01->A1_NOME) 							+'",')
			::SetResponse('"Estado": "'			+ T01->A1_EST										+'",')
			::SetResponse('"AnoContrato": "'			+ T01->ZZ0_ANOCOM										+'",')   
			::SetResponse('"Itens_Contrato":')
			::SetResponse('[')
			While !T02->(EOF())
				nCont++

				::SetResponse('{')
				::SetResponse('"Item": "'				+ AllTrim(T02->ZZ1_ITEM) 							+'",')
				::SetResponse('"Kit": "'				+ AllTrim(T02->ZZ1_PRODUT) 							+'",')
				::SetResponse('"Desc.Kit": "'		+ AllTrim(T02->ZZ1_DESC) 							+'",')
				::SetResponse('"Quantidade": '			+ STR(T02->ZZ1_QUANT)								+',') 
				::SetResponse('"ValorUnitario": '			+ STR(T02->ZZ1_VLRUN)								+',') 
				::SetResponse('"ValorTotal": '			+ STR(T02->ZZ1_VLRTOT)								+',') 
				::SetResponse('"Tes": "'				+ AllTrim(T02->ZZ1_TES)								+'",')
				::SetResponse('"Tipo": "'			+ AllTrim(T02->ZZ1_TIPO)							+'",')
				::SetResponse('"Envio": "'			+ AllTrim(T02->ENVIO) 								+'"')

				IF nCont == nTotReg .AND. nCont2 <> nContratos
					::SetResponse('}]},') 
				ELSEIF nCont == nTotReg .AND. nCont2 == nContratos
					::SetResponse('}]}]') 
				ELSE
					::SetResponse('},') 
				ENDIF

				T02->(DbSkip())


			EndDo

			T02->(DbCloseArea())
			nCont := 0
			T01->(DbSkip())
		ENDDO
		T01->(DbCloseArea())

	Else
		// as propriedades da classe receber�o os valores enviados por querystring
		// exemplo: http://localhost:8080/sample?startIndex=1&count=10
		DEFAULT ::startIndex := 1, ::count := 5

		// exemplo de retorno de uma lista de objetos JSON
		::SetResponse('[')
		For i := ::startIndex To ::count + 1
			If i > ::startIndex
				::SetResponse(',')
			EndIf
			::SetResponse('{"id":' + Str(i) + ', "name":"sample"}')
		Next
		::SetResponse(']')
	EndIf
Return .T.



// O metodo PUT pode receber parametros por querystring, por exemplo:
// WSMETHOD PUT WSRECEIVE startIndex, count WSSERVICE sample