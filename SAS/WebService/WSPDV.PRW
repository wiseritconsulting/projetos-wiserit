#INCLUDE "APWEBSRV.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "RWMAKE.CH"
#INCLUDE "PRTOPDEF.CH"

/*                               
Autor.....: Rog�rio J�come
Data......: 14/04/16
Descri��o.: WebService de consulta dos contratos
*/

WSSERVICE WSPDV DESCRIPTION "WebService Protheus x PDV"
	
	
	WSDATA cEmpre 				AS String
	WSDATA cFili				AS String
	WSDATA CCODPAIS 				AS String
	
	WSDATA Id 				AS String
	WSDATA CGUID 				AS String
	
	WSDATA cContra 				AS String
	
	
	WSDATA nVlrTot        	AS Float
	WSDATA nDesc          	AS Float
	WSDATA nVlrLiq	    	AS Float
	//WSDATA ARETORNO			AS aRetorno_Array
	
	WSDATA cEmp				AS String
	WSDATA cFil	 			AS String
	WSDATA cClient			AS String
	WSDATA cLoja 			AS String
	WSDATA cPessoa			AS String
	WSDATA cNome 			AS String
	WSDATA cNReduz 			AS String
	WSDATA cEnd 			AS String
	WSDATA cMun 			AS String
	WSDATA cEst 			AS String
	WSDATA cCodMun			AS String
	WSDATA cCep				AS String
	WSDATA cPais			AS String
	WSDATA cCGC				AS String
	WSDATA cConta			AS String
	WSDATA cTpPes			AS String
	WSDATA cEmail			AS String
	WSDATA cBairro			AS String
	WSDATA cRecISS			AS String
	WSDATA cIncISS			AS String
	WSDATA cRecINSS			AS String
	WSDATA cRecIRRF			AS String
	WSDATA cRecCOFI			AS String
	WSDATA cRecCSLL			AS String
	WSDATA cIRBAX			AS String
	WSDATA cRecPIS			AS String
	WSDATA cMINIRF			AS String
	WSDATA cABATIMP			AS String
	WSDATA cCodVend			AS String
	//WSDATA oProduto			AS ADDITEM_oPRODUTO
	//WSDATA oProduto1		AS ADDITEM_nPRODUTO
	//WSDATA oParcela			AS ADDITEM_oPARCELA
	WSDATA cObs				AS String OPTIONAL
	WSDATA cObs1			AS String OPTIONAL
	WSDATA cObs2			AS String OPTIONAL
	WSDATA cTipo   			AS String
	WSDATA cNumPed			AS String OPTIONAL
	WSDATA dData			AS String
	WSDATA cCond   			AS String
	WSDATA cMennota			AS String OPTIONAL
	WSDATA cNaturez			AS String
	WSDATA cCodAtend 		AS String
	WSDATA cNomAten 		AS String
	WSDATA cNRedAten 		AS String
	WSDATA cAtenShift 		AS String
	WSDATA cCliShift 		AS String
	WSDATA cPedShift 		AS String
	WSDATA cSerie	   	 	AS String
	WSDATA cReferFin		AS String
	WSDATA cCPFRF			AS String
	WSDATA Retorno			AS String
	
	WSDATA CTEL 				AS String
	
	
	
	WSDATA cBairroC 				AS String
	WSDATA cBairroE 				AS String
	WSDATA cUFent 				AS String
	WSDATA cDTpCli 				AS String
	
	
	
	WSMETHOD PDV 		DESCRIPTION "Esse m�todo faz a integra��o do Protheus x PDV"
	WSMETHOD CLIENTES 		DESCRIPTION "Esse m�todo faz a altera��o e inclus�o do cadastro de cliente"
ENDWSSERVICE


WSMETHOD PDV WSRECEIVE Id   WSSEND Retorno WSSERVICE WSPDVasdd

	Local aAuto := {}
	Local aErro	:= {}
	Local aConteu := {}
	Local nCont
	Local lRet	:= .T.
	Local cBuffer := ""
	Local cBody	:= ""
	Local nHdL 	:= -1
	Local cClient := ""
	Local cQuery := ""
    Conout("")
	
	Private lMsErroAuto := .F.
//::Retorno := "TRUE"
	
	PREPARE ENVIRONMENT EMPRESA ::cEmpre FILIAL ::cFili MODULO "SIGAFAT"
//--- Exemplo: Inclusao ---

IF cQuery == ''

::Retorno := U_FPDVPed(Id)

ENDIF
	
//::Retorno := "ERRO"
	
Return(lRet)


WSMETHOD CLIENTES WSRECEIVE cLoja,cPessoa,cNome,cNReduz,cEnd,cTipo,cEst,cCodMun,cMun,cBairro,cCep,cPais,cCGC,cEmail,cTel, cCodPais, cBairroC, cBairroE, cUFent, cDTpCli      WSSEND Retorno WSSERVICE WSPDV

	Local aAuto := {}
	Local aErro	:= {}
	Local nCont
	Local lRet	:= .T.
	Local cBuffer := ""
	Local cBody	:= ""
	Local nHdL 	:= -1
	Local cClient := ""

	Private lMsErroAuto := .F.
//::Retorno := "TRUE"

	PREPARE ENVIRONMENT EMPRESA ::cEmp FILIAL ::cFil MODULO "SIGAFAT"
//--- Exemplo: Inclusao ---

	dbSelectArea("SA1")
	dbSetOrder(3)
	If !SA1->(dbSeek(xFilial("SA1")+Alltrim(::cCGC)))
	    
		//{"A1_COD"       ,IIF(!Empty(cClient),cClient,Criavar("A1_COD"))           	,Nil},; // Codigo				 
		//{"A1_COD"       ,Criavar("A1_COD")           									,Nil},; // Codigo				 
	
		aAuto :=	{	 	 {"A1_LOJA"      ,IIF(!Empty(cLoja),cLoja,Criavar("A1_LOJA"))               	,Nil},; // Loja OK
		{"A1_PESSOA"    ,IIF(!Empty(cPessoa),cPessoa,Criavar("A1_PESSOA"))           	,Nil},; // Pessoa OK
		{"A1_NOME"      ,IIF(!Empty(cNome),cNome,Criavar("A1_NOME"))				  	,Nil},; // Nome OK
		{"A1_NREDUZ"    ,IIF(!Empty(cNReduz),cNReduz,Criavar("A1_NREDUZ"))			,Nil},; // Nome reduz. OK
		{"A1_END"       ,IIF(!Empty(cEnd),cEnd,Criavar("A1_END"))						,Nil},; // Endereco OK
		{"A1_TIPO"      ,IIF(!Empty(cTipo),cTipo,Criavar("A1_TIPO"))					,Nil},; // Tipo OK
		{"A1_EST"       ,IIF(!Empty(cEst),cEst,Criavar("A1_EST"))		    			,Nil},; // Estado OK
		{"A1_COD_MUN"   ,IIF(!Empty(cCodMun),cCodMun,Criavar("A1_COD_MUN"))			,Nil},; // Codigo Municipio OK 
		{"A1_MUN"       ,IIF(!Empty(cMun),cMun,Criavar("A1_MUN"))						,Nil},; // Cidade OK
		{"A1_BAIRRO"    ,IIF(!Empty(cBairro),cBairro,Criavar("A1_BAIRRO"))			,Nil},; // Bairro OK
		{"A1_CEP"       ,IIF(!Empty(cCep),cCep,Criavar("A1_CEP"))						,Nil},; // CEP OK
		{"A1_PAIS"      ,IIF(!Empty(cPais),cPais,Criavar("A1_PAIS"))					,Nil},; // Pais OK
		{"A1_CGC"       ,IIF(!Empty(cCGC),cCGC,Criavar("A1_CGC"))						,Nil},; // CGC OK
		{"A1_TEL"     ,IIF(!Empty(cTel),cTel,Criavar("A1_TEL"))				,Nil},; // Conta Contabil
		{"A1_CODPAIS"   ,IIF(!Empty(cCodPais),cCodPais,Criavar("A1_CODPAIS"))				,Nil},; // Tipo de Pessoa
		{"A1_EMAIL"     ,IIF(!Empty(cEmail),cEmail,Criavar("A1_EMAIL"))				,Nil},; // Email OK
		 {"A1_BAIRROC"   ,IIF(!Empty(CBAIRROC),CBAIRROC,Criavar("A1_BAIRROC"))		,Nil},; 
		 {"A1_BAIRROE"   ,IIF(!Empty(CBAIRROE),CBAIRROE,Criavar("A1_BAIRROE"))		,Nil},; 
		 {"A1_ESTE"   ,IIF(!Empty(CUFENT),CUFENT,Criavar("A1_ESTE"))		,Nil},; 
		 {"A1_YCODTCF"   , "500"		,Nil},; 
		 {"A1_YTPCLIE"   ,IIF(!Empty(cDTpCli),cDTpCli,Criavar("A1_ESTE"))		,Nil}}  // Cod do Cliente no SHIFT 
							 
							 
	
		IF Len(aAuto) > 0
			MSExecAuto({|x,y| Mata030(x,y)},aAuto,3) 	//&("MSExecAuto({|x,y| "+::Rotina+"(x,y)},aAuto,"+cValToChar(::Opcao)+")")
			If lMsErroAuto
	   			
				::Retorno := "ERRO," + cvaltochar(cClient) + "," + cvaltochar(cLoja)
	  		ELSE
	  		
	  			::Retorno := "OK," + SA1->A1_COD + "," + cvaltochar(cLoja) 
			EndIf
		EndIF
	ElseIf SA1->(dbSeek(xFilial("SA1")+Alltrim(::cCGC)))
		
		cClient := SA1->A1_COD
		cLoja 	:= SA1->A1_LOJA
		cRecISS	:= SA1->A1_RECISS
		cIncISS := SA1->A1_INCISS
		cRecINSS:= SA1->A1_RECINSS
		cRecIRRF:= SA1->A1_RECIRRF
		cRecCOFI:= SA1->A1_RECCOFI
		cRecCSLL:= SA1->A1_RECCSLL
		cIRBAX	:= SA1->A1_IRBAX
		cRecPIS	:= SA1->A1_RECPIS
		cMINIRF	:= SA1->A1_MINIRF
		cABATIMP:= SA1->A1_ABATIMP
		
		aAuto :=	{	 	 {"A1_LOJA"      ,IIF(!Empty(cLoja),cLoja,Criavar("A1_LOJA"))               	,Nil},; // Loja OK
		{"A1_COD"    ,IIF(!Empty(cClient),cClient,Criavar("A1_COD"))           	,Nil},; // Pessoa OK
		{"A1_PESSOA"    ,IIF(!Empty(cPessoa),cPessoa,Criavar("A1_PESSOA"))           	,Nil},; // Pessoa OK
		{"A1_NOME"      ,IIF(!Empty(cNome),cNome,Criavar("A1_NOME"))				  	,Nil},; // Nome OK
		{"A1_NREDUZ"    ,IIF(!Empty(cNReduz),cNReduz,Criavar("A1_NREDUZ"))			,Nil},; // Nome reduz. OK
		{"A1_END"       ,IIF(!Empty(cEnd),cEnd,Criavar("A1_END"))						,Nil},; // Endereco OK
		{"A1_TIPO"      ,IIF(!Empty(cTipo),cTipo,Criavar("A1_TIPO"))					,Nil},; // Tipo OK
		{"A1_EST"       ,IIF(!Empty(cEst),cEst,Criavar("A1_EST"))		    			,Nil},; // Estado OK
		{"A1_COD_MUN"   ,IIF(!Empty(cCodMun),cCodMun,Criavar("A1_COD_MUN"))			,Nil},; // Codigo Municipio OK 
		{"A1_MUN"       ,IIF(!Empty(cMun),cMun,Criavar("A1_MUN"))						,Nil},; // Cidade OK
		{"A1_BAIRRO"    ,IIF(!Empty(cBairro),cBairro,Criavar("A1_BAIRRO"))			,Nil},; // Bairro OK
		{"A1_CEP"       ,IIF(!Empty(cCep),cCep,Criavar("A1_CEP"))						,Nil},; // CEP OK
		{"A1_PAIS"      ,IIF(!Empty(cPais),cPais,Criavar("A1_PAIS"))					,Nil},; // Pais OK
		{"A1_CGC"       ,IIF(!Empty(cCGC),cCGC,Criavar("A1_CGC"))						,Nil},; // CGC OK
		{"A1_TEL"     ,IIF(!Empty(cTel),cTel,Criavar("A1_TEL"))				,Nil},; // Conta Contabil
		{"A1_CODPAIS"   ,IIF(!Empty(cCodPais),cCodPais,Criavar("A1_CODPAIS"))				,Nil},; // Tipo de Pessoa
		{"A1_EMAIL"     ,IIF(!Empty(cEmail),cEmail,Criavar("A1_EMAIL"))				,Nil},; // Email OK
		 {"A1_BAIRROC"   ,IIF(!Empty(CBAIRROC),CBAIRROC,Criavar("A1_BAIRROC"))		,Nil},; 
		 {"A1_BAIRROE"   ,IIF(!Empty(CBAIRROE),CBAIRROE,Criavar("A1_BAIRROE"))		,Nil},; 
		 {"A1_ESTE"   ,IIF(!Empty(CUFENT),CUFENT,Criavar("A1_ESTE"))		,Nil},; 
		  {"A1_YCODTCF"   , "500"		,Nil},; 
		 {"A1_YTPCLIE"   ,IIF(!Empty(cDTpCli),cDTpCli,Criavar("A1_ESTE"))		,Nil}}	
							 //{"A1_YCODSHI"   ,IIF(!Empty(cCliShift),cCliShift,Criavar("A1_YCODSHI"))		,Nil}}  // Cod do Cliente no SHIFT
							 		
		IF Len(aAuto) > 0
			MSExecAuto({|x,y| Mata030(x,y)},aAuto,4) 	//&("MSExecAuto({|x,y| "+::Rotina+"(x,y)},aAuto,"+cValToChar(::Opcao)+")")
			If lMsErroAuto
			
				::Retorno := "ERRO," + cvaltochar(cClient) + "," + cvaltochar(cLoja)
	  		
			ELSE
				::Retorno := "OK," + cvaltochar(cClient) + "," + cvaltochar(cLoja)
	  		
			EndIf
		EndIf
		

		//RESET ENVIRONMENT
  endif

Return(lRet)


//--------------------------------------------------------------------------------------------------------------
