#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} SASV013
//TODO Descri��o auto-gerada.
@author caiolima
@since 02/09/2016
@version undefined

@type function
/*/
user function SASV013()
	Local lRet := .F.
	Local cTeste := ""
	dbSelectArea("SE5")
	
	cTeste := M-> E5_RECPAG
	MsgInfo(cTeste)
	
	
	If Alltrim(M -> E5_RECPAG) == "P"
		If Substr(M->E5_NATUREZ,1,1) == "1"
			MsgInfo("A Natureza Financeira " + Alltrim(M->E5_NATUREZ) + " n�o pode ser utilizada no Pagamento de Movimento Banc�rio.")
			If MsgYesNo("Deseja continuar?")
				lRet := .T.
			Else
				lRet := .F.
			EndIf
		Else
			lRet := .T.
		EndIf
	ElseIf Alltrim(M-> E5_RECPAG) == "R"
		If Substr(M-> E5_NATUREZ,1,1) == "2"
			MsgInfo("A Natureza Financeira " + Alltrim(M-> E5_NATUREZ) + " n�o pode ser utilizada no Recebimento de Movimento Banc�rio.")
			If MsgYesNo("Deseja continuar?")
				lRet := .T.
			Else
				lRet := .F.
			EndIf
		EndIf
	EndIf
	
return lRet