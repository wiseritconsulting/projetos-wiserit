#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} SASV012
//TODO Descri��o auto-gerada.
@author caiolima
@since 02/09/2016
@version undefined

@type function
/*/
user function SASV012()
	Local lRet := .F.
	dbSelectArea("SE1")
	//dbSetOrder(H
	
	If SubStr(M-> E1_NATUREZ,1,1) == "2"
		MsgInfo("A Natureza Financeira " + Alltrim(M->E1_NATUREZ) + " n�o pode ser utilizada no Contas a Receber.", "Protheus SAS")
		If MsgYesNo("Deseja Continuar?")
			lRet := .T.		
		Else
			lRet := .F.
		EndIf
	Else
		lRet := .T.
	EndIf
return lRet