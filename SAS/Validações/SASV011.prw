#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "rwmake.ch"
#INCLUDE "TOPCONN.CH"

/*/{Protheus.doc} SASV011
//TODO Descri��o auto-gerada.
@author caiolima
@since 02/09/2016
@version undefined

@type function
/*/
user function SASV011()

	Local lRet := .F.
	dbSelectArea("SE2")
	dbSelectArea("SE1")
	dbSelectArea("SE5")


	IF FUNNAME() == "SASP071"
		Return 
	endif
		
	If SubStr(M -> E2_NATUREZ,1,1) == "1"
		MsgInfo("A Natureza Financeira " + Alltrim(M->E2_NATUREZ) + " n�o pode ser utilizada no Contas a Pagar.", "Protheus SAS")
		If MsgYesNo("Deseja Continuar?")
			lRet := .T.
		Else
			lRet := .F.
		EndIf
	Else
		lRet := .T.
	EndIf
return lRet