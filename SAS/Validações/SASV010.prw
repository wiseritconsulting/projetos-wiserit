#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "TOPCONN.CH"

user function SASV010()
	Local lRet := .F.
	dbSelectArea("SA1")
	
	If Len(Alltrim(SA1->A1_COD))>1
		lRet := .T.
	Else
		MsgInfo("O c�digo da loja precisa ter 2 d�gitos.")
		lRet := .F.
	EndIf
return lRet