#INCLUDE "rwmake.ch"
#INCLUDE "TOPCONN.CH"

User Function SASV009()

Local lRet := .F.
dbSelectArea("SA2")

If Len(Alltrim(M->A2_LOJA))>1
	lRet := .T.
Else
	MsgInfo("O c�digo da loja precisa ter 2 d�gitos.")
	lRet := .F.
EndIf

Return lRet