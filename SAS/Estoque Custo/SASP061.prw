#include 'protheus.ch'
#include 'parmtype.ch'

/* 
---------------------------------------------------
Weskley Silva 
---------------------------------------------------
24.07.2015 
---------------------------------------------------
*/

user function SASP061()

Local cDblqMov := ddatabase
Local dDtAtu   := GETMV("MV_DBLQMOV")
Local aPergs := {} // Perguntas do Parambox 
Local aRet := {} // Perguntas Preenchidas com suas respostas

//TODO Verifica se o usuario est� contindo no parametro.
If !(__cUserID $ GetMV("SA_BLQEST"))
	MsgInfo("Usuario sem permissao","Permiss�o de Acesso")
	Return
EndIf

//TODO Preenche as perguntas do Parambox
aAdd( aPergs ,{1,"Data Atual",dDtAtu,"",'.T.',"",".F.",60,.T.}) 
aAdd( aPergs ,{1 ,"Data do Bloqueio de Estoque ",cDblqMov,"",'.T.',"","",60,.T.})

//TODO Verifica se os parametros foram alterados
If !Parambox(aPergs ,"Parametro",aRet)
	Alert("Cancelado pelo Usu�rio")
	Return
EndIf	

//TODO Altera o conteudo do Parametro	
PutMV("MV_DBLQMOV",aRet[2])

MsgInfo("Altera��o realizada com exito","Aten��o")

	
return