#include 'protheus.ch'
#include 'parmtype.ch'

user function SASTI01()

	Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
        
	Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.

	Private cString := "TEC" 

	dbSelectArea("TEC")

	dbSetOrder(1)

	AxCadastro(cString,"Cadastro de Colaboradores",cVldExc,cVldAlt)
return