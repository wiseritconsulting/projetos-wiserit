#include "protheus.ch"

/* +----------------------------------------------------------------
   | PROGRAMA: SASG002						DATA: 10/08/2015 
   +----------------------------------------------------------------
   | AUTOR: RAFAEL PINHEIRO					MODULO: ESTOQUE
   +----------------------------------------------------------------
   | OBJETIVO: BLOQUEIO DE GERA��O DE OP PARA PRODUTO SE ESTRUTURA
   +----------------------------------------------------------------
   | 10/08/2015 - (EM HOMOLOGA��O)  
   |           
   +---------------------------------------------------------------*/

User Function SASG002(produto)

	Local lRet := .F.
	Local _SASArea := getArea()
	
	dbSelectArea("SG1")
	dbSetOrder(1)
	lRet := SG1->(MsSeek(xFilial("SG1")+produto))
	
	if !lRet 
		Alert("O produto selecionado n�o possui estrutura cadastrada...")
	endif
	
	RestArea(_SASArea)


Return lRet