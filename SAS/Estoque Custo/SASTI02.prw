#include 'protheus.ch'
#include 'parmtype.ch'

user function SASTI02()

	Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.

	Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.


	Private cString := "TIC"


	dbSelectArea("TIC")

	dbSetOrder(2)

	AxCadastro(cString,"Cadastro de Licen�as",cVldExc,cVldAlt)
return
  
//user function VALCHAV(cChave)

	//Local lVal := .T.
	
	//if ExistChav("TIC", cChave)
		//MsgInfo("A chave de registro j� � existente")
		//lVal := .F.
	//EndIf
	//return lVal
	
user function VALIDCHAV(cChave)
	Local lRet := .F.
	
	dbSelectArea("TIC")
	dbSetOrder(2)
	
	If dbSeek(M->TIC_CHAVE)
		MsgInfo("A chave de registro j� � existente", "Protheus SAS")
		lRet := .F.
	Else
		lRet := .T.
	EndIf
	
return lRet
